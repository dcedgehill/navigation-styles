<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->





    
<!-- Mirrored from www.nngroup.com/articles/overuse-of-overlays/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:13:01 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1026.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"beacon":"bam.nr-data.net","queueTime":7,"applicationTime":857,"agent":"","licenseKey":"1a30ef9931","errorBeacon":"bam.nr-data.net","applicationID":"2048992","transactionName":"NFxSMRBUX0EDUkIPDA0WdhAMVkVbDV8ZBxEXUFMJB0YfRAtUQRVZIktEDAFZVHYHRVcPD01eVRE="}</script>
        <title>Overuse of Overlays: How to Avoid Misusing Lightboxes</title><meta property="og:title" content="Overuse of Overlays: How to Avoid Misusing Lightboxes" />
  
        
        <meta name="description" content="Use the 5 Ws – Who, What, When, Where, and Why – to determine if an overlay will help or harm your user experience.">
        <meta property="og:description" content="Use the 5 Ws – Who, What, When, Where, and Why – to determine if an overlay will help or harm your user experience." />
        
  
        
	
        
        <meta name="keywords" content="overlay, lightbox. modal window, dialog box">
        

        

        <meta name="robots" content="noarchive">
        <meta name="viewport" content="width=device-width">

        <link rel="search" type="application/opensearchdescription+xml" title="NN/g Search" href="../../opensearch.xml" />
        <link rel="shortcut icon" href="http://media.nngroup.com/static/img/favicon.ico" />

        <link rel="stylesheet" href="https://media.nngroup.com/static/vendor/font-awesome/css/font-awesome.min.css">

        
        <link rel="stylesheet" href="https://media.nngroup.com/static/css/7232a960f1df.css" type="text/css" />
        <link rel="stylesheet" href="http://fast.fonts.com/t/1.css?apiType=css&amp;projectid=c5846260-9def-46ec-9407-0f1349bd2d64" type="text/css">

        <script src="https://media.nngroup.com/static/vendor/modernizr/js/modernizr.min.js"></script>

        <script type="text/javascript" src="https://media.nngroup.com/static/js/3283be5def74.js"></script>
        

        

        
        
        <!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=229034,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('http://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->


<!-- Google Analytics Tracking Code for http://nngroup.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23132681-1', 'nngroup.com');
  ga('require', 'linkid', 'https://www.nngroup.com/articles/overuse-of-overlays/linkid.js');
  ga('require', 'displayfeatures');

// Optimizely Universal Analytics Integration
 window.optimizely = window.optimizely || [];
 window.optimizely.push("activateUniversalAnalytics");

  ga('send', 'pageview');

</script>
<script>
/**
* Function to track a click on an outbound link in Google Analytics.
*/
function trackOutboundLink(label, url) {
    if(window.ga && ga.loaded) {
        ga('send', 'event', 'outbound', 'click', label, {'hitCallback': function(){document.location = url;}
        });
    } else {
        document.location = url;
    }
}
/**
* Function to track a click on a link in the article announcement area.
*/
$('#articleAnnouncement a').on('click', function(){
  ga('send', 'event', 'link', 'click', 'articleAnnouncement');
});
</script>

<script type="text/javascript">
 setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2421.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



<!-- Hotjar Tracking Code for http://nngroup.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:121605,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

        <script src="https://media.nngroup.com/static/js/analytics/track_outbound_link.js"></script>
        
        
        
    </head>
    <body class="location-articles articles-detail">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
    <header id="page-header">
        <div id="cart" class="shopping-cart mobile-cart show-for-small-down"></div>
        <div class="row">
            <div id="masthead" class="masthead small-12 medium-7 column">

                <!-- The button to toggles menu on small screen -->

                <h1><a href="../../index.php"><img src="http://media.nngroup.com/static/img/logo-wordmark.png" width="74" height="30"> Nielsen <span class="norman">Norman</span> Group</a></h1>
                <p class="tagline show-for-medium-up">Evidence-Based User Experience Research, Training, and Consulting</p>

                <!-- The button to toggles form search on small screen -->


                <div style="clear: right;width:  100%;height: 1px;"></div><?php define('ROOT_PATH',$_SERVER["DOCUMENT_ROOT"] . '/apps/navigation_styles/navigations/3'); ?><?php include(ROOT_PATH)."/navigation.php"; ?>

                <!-- Drop down menu on small screen -->
                <div id="hideNav" class="show-for-small-down">
                  
                  <ul class="menu-content">
                    <li><a href="../../index.php">Home</a></li>
                    <li class="has-submenu">
                      <a>Training</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../training/index.php">Overview</a></li>
                        
<li id="nav-ux-conference"><a href="../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../online-seminars/index.php"><span>Online Seminars</span></a></li>

                      </ul>
                    </li>
                    <li class="has-submenu">
                      <a>Consulting</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
  <li><a href="../../consulting/index.php">Overview</a></li>
  
  <li><a href="../../consulting/benchmarking/index.php">Benchmarking</a></li>
  
  <li><a href="../../consulting/expert-design-review/index.php">Expert Design Review</a></li>
  
  <li><a href="../../consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
  
  <li><a href="../../consulting/usability-testing/index.php">Usability Testing </a></li>
  
  <li><a href="../../consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
  
  <li><a href="../../in-house-training/index.php">In-House Training</a></li>
</ul>

                    </li>

                    <li><a href="../../reports/index.php">Reports</a></li>
                    <li><a href="../index.php">Articles</a></li>
                    <li class="has-submenu"><a>About NN/G</a><i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../about/index.php">Overview</a></li>
                        <li><a href="../../people/index.php">People</a></li>
                        <li><a href="../../about/why-nng/index.php">Why NN/g?</a></li>
                        <li><a href="../../about/contact/index.php">Contact</a></li>
                        <li><a href="../../news/index.php">News</a></li>
                        <li><a href="../../about/history/index.php">History</a></li>
                        <li><a href="../../books/index.php">Books</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                </div>

                <hr class="header-divider show-for-small-only"/>


            </div>
       </div>

        <div id="hideNav">            <nav class="primary-nav small-12 column">
                <ul>
                    <li><a class="navitem" id="nav-home" href="../../index.php">Home</a></li>
                    <li><a class="navitem" id="nav-training" href="../../training/index.php">Training</a></li>
                    <li><a class="navitem" id="nav-consulting" href="../../consulting/index.php">Consulting</a></li>
                    <li><a class="navitem" id="nav-reports" href="../../reports/index.php">Reports</a></li>
                    <li><a class="navitem" id="nav-articles" href="../index.php">Articles</a></li>
                    <li><a class="navitem" id="nav-about" href="../../about/index.php">About NN/G</a></li>
                </ul>
            </nav>
            <nav class="secondary-nav small-12 medium-12 large-12 columns">
                
                
            </nav>
        </div>
    </header>
    <div id="mainContent" class="l-content">
    
  

  

  <div class="row">
    
      
      <section class="small-12 medium-3 columns collapsable-content l-navrail articles-left-column">
        <h1 class="show-for-small-down collapse-link mobile-browse-header">Browse by Topic and Author</h1>
          <div class="row collapsable">

            
            <div class="small-6 medium-12 columns">
              <section id="article-popular-topics" style="display: none;">
                
                <h1>Topics</h1>
                <ul id="article-popular-topics">
                    
                    <li>
                        
                        <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/intranets/all/index.php">Intranets</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/user-testing/all/index.php">User Testing</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                        
                    </li>
                    
                    <li><a href="#" id="article-see-all-topics"><strong>See all topics&hellip;</strong></a></li>
                </ul>
                
              </section>
              <section id="article-all-topics" style="display: none;">
                
                <h1>
                  <span class="show-for-medium-up">All Article Topics</span>
                  <span class="show-for-small-down">All Topics</span>
                  (<a href="#" id="article-hide-all-topics">hide</a>)
                </h1>
                <ul>
                  
                  <li>
                    
                    <a href="../../topic/accessibility/all/index.php">Accessibility</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/agile/all/index.php">Agile</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/analytics-and-metrics/all/index.php">Analytics &amp; Metrics</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/applications/all/index.php">Application Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/b2b-websites/all/index.php">B2B Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/behavior-patterns/all/index.php">Behavior Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/branding/all/index.php">Branding</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/content-strategy/all/index.php">Content Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/corporate-websites/all/index.php">Corporate Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/design-patterns/all/index.php">Design Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-design-process/all/index.php">Design Process</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/email/all/index.php">Email</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/eyetracking/all/index.php">Eyetracking</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/heuristic-evaluation/all/index.php">Heuristic Evaluation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/human-computer-interaction/all/index.php">Human Computer Interaction</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-ideation/all/index.php">Ideation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/information-architecture/all/index.php">Information Architecture</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/interaction-design/all/index.php">Interaction Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/international-users/all/index.php">International Users</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/intranets/all/index.php">Intranets</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-management/all/index.php">Management</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/navigation/all/index.php">Navigation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/non-profit-websites/all/index.php">Non-Profit Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/personas/all/index.php">Personas</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/persuasive-design/all/index.php">Persuasive Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/prototyping/all/index.php">Prototyping</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/psychology-and-ux/all/index.php">Psychology and UX</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/research-methods/all/index.php">Research Methods</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/search/all/index.php">Search</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/social-media/all/index.php">Social Media</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/strategy/all/index.php">Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/user-testing/all/index.php">User Testing</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-teams/all/index.php">UX Teams</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/visual-design/all/index.php">Visual Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/writing-web/all/index.php">Writing for the Web</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/young-users/all/index.php">Young Users</a>
                    
                  </li>
                  
                </ul>
                
              </section>
            </div>
            <script>
              /* Determine which topics list will be displayed: the full or the
               * partial.  We are inlining this close to the list itself to
               * prevent rendering delays on the client
               */
              var topic_list = $.cookie("article-topics");

              var popular_topics = $('#article-popular-topics');
              var all_topics = $('#article-all-topics');

              if(topic_list == 1) {
                  popular_topics.hide();
                  all_topics.show();
              } else {
                  popular_topics.show();
                  all_topics.hide();
              }
            </script>
            

            
            <div class="small-6 medium-12 columns">
              
              <section id="article-initial-authors">
                  <h1>Author</h1>
                  <ul>
                    
                    <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                    
                    <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                    
                    <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                    
                    <li><a href="#" onClick="$('#article-initial-authors').hide(); $('#article-all-authors').show();"><strong>See all authors&hellip;</strong></a></li>
                  </ul>
              </section>
              <section id="article-all-authors" style="display: none;">
                <h1>
                  All Authors
                  (<a href="#" id="article-hide-all-authors">hide</a>)
                </h1>
                <ul>
                  
                  <li><a href="../author/raluca-budiu/index.php">Raluca Budiu</a></li>
                  
                  <li><a href="../author/patty-caya/index.php">Patty Caya</a></li>
                  
                  <li><a href="../author/susan-farrell/index.php">Susan Farrell</a></li>
                  
                  <li><a href="../author/therese-fessenden/index.php">Therese Fessenden</a></li>
                  
                  <li><a href="../author/kim-flaherty/index.php">Kim Flaherty</a></li>
                  
                  <li><a href="../author/sarah-gibbons/index.php">Sarah Gibbons</a></li>
                  
                  <li><a href="../author/aurora-harley/index.php">Aurora Harley</a></li>
                  
                  <li><a href="../author/kate-kaplan/index.php">Kate Kaplan</a></li>
                  
                  <li><a href="../author/page-laubheimer/index.php">Page Laubheimer</a></li>
                  
                  <li><a href="../author/angela-li/index.php">Angie Li</a></li>
                  
                  <li><a href="../author/hoa-loranger/index.php">Hoa Loranger</a></li>
                  
                  <li><a href="../author/kate-meyer/index.php">Kate Meyer</a></li>
                  
                  <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                  
                  <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                  
                  <li><a href="../author/kara-pernice/index.php">Kara Pernice</a></li>
                  
                  <li><a href="../author/christian-rohrer/index.php">Christian Rohrer</a></li>
                  
                  <li><a href="../author/amy-schade/index.php">Amy Schade</a></li>
                  
                  <li><a href="../author/katie-sherwin/index.php">Katie Sherwin</a></li>
                  
                  <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                  
                  <li><a href="../author/kathryn-whitenton/index.php">Kathryn Whitenton</a></li>
                  
                </ul>
              </section>
              
            </div>
            
          </div>

          
          <section class="hide-for-small-down">
            
              
              <h1>Recent Articles</h1>
              
              <ul>
                  
                  <li><a href="../seamless-cross-channel/index.php">Seamlessness in the Omnichannel User Experience</a></li>
                  
                  <li><a href="../brand-intention-interpretation/index.php">Experience Design: Bridging Brand Intention and Brand Interpretation</a></li>
                  
                  <li><a href="../eyetracking-tasks-efficient-scanning/index.php">Scanning Patterns on the Web Are Optimized for the Current Task</a></li>
                  
                  <li><a href="../flat-design-best-practices/index.php">Flat-Design Best Practices</a></li>
                  
                  <li><a href="../ux-user-stories/index.php">Accounting for UX Work with User Stories in Agile Projects</a></li>
                  
                  <li><a href="../index.php"><strong>See all articles&hellip;</strong></a></li>
              </ul>
              
            
          </section>
          

          
          
          <section class="hide-for-small-down">
            <h1>Popular Articles</h1>
            
            <ul>
                
                <li><a href="../usability-101-introduction-to-usability/index.php">Usability 101: Introduction to Usability</a></li>
                
                <li><a href="../top-10-mistakes-web-design/index.php">Top 10 Mistakes in Web Design</a></li>
                
                <li><a href="../how-users-read-on-the-web/index.php">How Users Read on the Web</a></li>
                
                <li><a href="../f-shaped-pattern-reading-web-content/index.php">F-Shaped Pattern For Reading Web Content</a></li>
                
                <li><a href="../ten-usability-heuristics/index.php">10 Usability Heuristics for User Interface Design</a></li>
                
                <li><a href="../intranet-design-2016/index.php">10 Best Intranets of 2016</a></li>
                
                <li><a href="../which-ux-research-methods/index.php">When to Use Which User-Experience Research Methods</a></li>
                
                <li><a href="../response-times-3-important-limits/index.php">Response Times: The 3 Important Limits</a></li>
                
                <li><a href="../why-you-only-need-to-test-with-5-users/index.php">Why You Only Need to Test with 5 Users</a></li>
                
                <li><a href="../page-fold-manifesto/index.php">The Fold Manifesto: Why the Page Fold Still Matters</a></li>
                
            </ul>
          </section>
          
          

          
            <section class="hide-for-small-down">
              
            </section>
          

      </section>
      

      
        <div class="small-12 medium-9 columns content-body">
          




<article class="l-subsection alertbox-article-content article-detail-bl">
  
  <header>
    <div class="l-subsection">
      <h1>Overuse of Overlays: How to Avoid Misusing Lightboxes</h1>
      <div class="author-meta">by
        
          
            
            <a href="../author/kathryn-whitenton/index.php">Kathryn Whitenton</a>
            
          
        on  May 25, 2015
        
      </div>

      

      
      <div class="topic-links">
        Topics:
        
          <ul>

  <li><a href="../../topic/web-usability/index.php">Web Usability</a></li>

</ul>
        
      </div>
      
    </div>
  </header>
  

  
  
  <section>
    <p class="lede"><strong>Summary:</strong> Poorly implemented overlays and lightboxes are not only frustrating for users, but can also be disastrous for conversion and task completion. Use the five W’s – Who, What, When, Where, and Why – to determine whether an overlay is truly the most appropriate design solution, and how you should implement it. </p>
    <br />
  </section>
  
  

  
  <section id="article-body"><p>When lightboxes and overlays first appeared a few years ago, they provided an elegant solution to a tricky interaction-design challenge: how to convey important information without losing the context of the current screen, while minimizing some of the problems created by pop-ups and dialog boxes. We even recognized the lightbox as the application-design <a href="../10-best-application-uis/index.php">technique of the year</a> in 2008.</p>

<blockquote>
<p>Definition: An <strong>overlay</strong> refers to a content box that is displayed on top of another page. The overlay box is usually noticeably smaller than the underlying page. Often the rest of the background page is obscured, leading to the use of the term ‘lightbox’ to describe this visual effect of heightened focus on the overlay content.    </p>
</blockquote>

<p>Since this technique first appeared, designers have adopted lightboxes with a vengeance. Only last week at our <a href="../../training/index.php">Usability Week conference</a>, a designer approached me to ask, ‘is it ok to use anchor links within a lightbox?’ The overlay in question had so much content that it required a scrollbar, and getting all the way to the bottom required a LOT of scrolling. In order to minimize the need to scroll, the team was now considering adding anchor links – with a side benefit of creating link targets so that other pages could send users directly to the overlay content. In case you’re having trouble visualizing this, here’s an example of a similar design, in the <em>Support </em>section of Apple.com. </p>

<p><img alt="Screenshot of the support overlay window for delayed email on Apple.com" height="593" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2015/05/12/apple-mobile-me-support-overlay.png" width="802"/></p>

<p><em>Apple’s MobileMe support section used to present help content in an overlay window, including a link to another help article about spam and a scrollbar to access lower-level content. </em></p>

<p>Scrolling and anchor links make sense on very long web pages. But if you find yourself adding <strong>multiple navigation mechanisms within an overlay</strong>, it’s time to <strong>stop</strong> and reevaluate your approach. Apple has since modified its design to completely eliminate the overlay, and instead simply display the content as a (new) full page. As a result, the viewable content area is much larger and uses the normal browser scrollbar, allowing users all the standard controls they're used to. The standalone page also allows users to easily save and share links to this content — a very important usage scenario for help content. Apple has even avoided using an overlay for a survey invitation, and instead shows it in a feature box at the top of the page. </p>

<p><img alt="Screenshot of Apple.com email delay support page with no overlay" height="583" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2015/05/12/apple-mobile-me-support-no-overlay.png" width="787"/></p>

<p><em>The current version of Apple’s email-delay support content completely eliminates the overlay and instead displays support information as a new page. </em></p>

<p>Overlays are sometimes appropriate, but as illustrated in the example above, all too often they are <strong>overused or misused</strong>. Sometimes this happens with the best of intentions: designers want to provide quick access to important information. In other cases, lightboxes appear to be used purely for the designer’s convenience, as a way to provide more features or content without having to create a new page or modify the navigation.</p>

<h2>Considerations When Using Overlays</h2>

<p>Many designers are familiar with a few standard <strong>guidelines for using overlays</strong>, such as:</p>

<ul>
	<li>Provide a <strong>visible ‘close’</strong> command to allow users to return to the underlying page.</li>
	<li>Ensure that the overlay box is visually<strong> distinct from the background</strong> page by making it smaller and using translucent shading to obscure the background (which indicates that the underlying content isn’t currently interactive, and also lets the user know that they haven’t actually navigated to a new page).</li>
	<li>Make overlay content <strong>accessible</strong> to keyboard users (by allowing the <em>Escape</em> key to close the overlay, and by providing keyboard access to content and fields within the overlay).</li>
</ul>

<p>But you can follow all of these rules and still end up with a frustrating experience — sometimes one that is so bad that users are not just annoyed, but literally prevented from accessing content and completing tasks. To avoid these situations, use a systematic process to analyze your design problem and make sure an overlay is the most appropriate solution.  Just because you <strong>can </strong>use an overlay doesn’t mean you <strong>should. </strong></p>

<p>When considering an overlay, lightbox, or modal window, ask yourself the following questions:</p>

<ul>
	<li><strong>Who </strong>is the target audience?</li>
	<li><strong>What </strong>action is the user supposed to take?</li>
	<li><strong>When </strong>will the overlay appear, and will it be an interruption?</li>
	<li><strong>Where </strong>will the overlay appear on the page?</li>
	<li><strong>Why</strong> does this need to be in an overlay instead of within a page?</li>
</ul>

<h2>WHO Is the Target Audience?</h2>

<p>If your users are on a mobile device, showing them an overlay designed for a large desktop screen is at best disorienting; at worse, it can completely block users from accessing your content.</p>

<ul>
	<li><strong>Disorientation</strong>.  The Viafoura.com website illustrates the disorienting effect of overlay conversion forms for mobile users; after clicking a button named <em>Request a Free Trial</em>, the next screen may show just a small portion of the form. Users have to immediately zoom out to understand what is being displayed. 

	<p><img alt="Screenshot of Viafoura.com free trial overlay" height="667" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2015/05/12/viafoura-overlay-zoomed-in-and-zoomed-out.png" width="820"/></p>

	<p><strong><em>Left</em></strong><em>: what users actually see after clicking ‘</em>Request a Free Trial<em>’ on viafoura.com; <strong>Right</strong>: the full overlay form, visible after closing the keyboard and zooming the screen out. </em></p>
	</li>
	<li><strong>Blocking access to content</strong>. Another all-to-common – and much more severe – problem occurs when either an incorrect implementation or a bug <strong>disables the scrolling and zooming features</strong> needed to dismiss an overlay. You’ve probably experienced this before: you visit a page using a mobile device, only to find that there’s an overlay blocking the page content, and the controls to dismiss the overlay are located off the screen. In this situation the user literally cannot access the underlying page content. In some cases, the page rendering even prevents pinching and zooming, or makes it so difficult that it is nearly impossible to expand the page enough to access the overlay controls.</li>
</ul>

<p>If users on mobile devices are an important part of your audience, think long and hard before putting important content in an overlay. If you do go with an overlay, prepare for both an initial and ongoing investment in testing and QA, to ensure the design works properly at different screen resolutions and in different device orientations.</p>

<p>Accessibility is another factor to consider when evaluating who will be using your overlay. Low-vision users with full-sized monitors often employ screen magnifiers to vastly enlarge the screen content. After zooming in, the user can only see a small part of the page, so overlays can create the same disorienting effects for screen-magnifier users as they do for mobile-device users.</p>

<h2>WHAT Action Is The User Supposed To Take?</h2>

<p>In order to create a good overlay, you need to first clearly articulate the purpose: what are users supposed to do in response to the content? Should they just glance at a short message, or should  they browse through a large set of images or read lengthy text? Do they need to make a decision between different options, or simply acknowledge the receipt of the information? Or do you need people to actually input content through the lightbox —and if so, what kind of content, and how much?</p>

<p>Experiences that feel quick and simple to users are usually <strong>not</strong> the result of a quick and simple design process. Instead, creating a design that feels easy to your audience means taking the time to clearly articulate the purpose, circumstances, and desired outcomes.</p>

<p>Overlays are typically intended to get users to <strong>view content</strong>, <strong>complete a form</strong>, or <strong>make a decision</strong>.</p>

<p><strong>Read text or view images</strong>: If the sole function of the modal window is to present a brief status message, a simple overlay with a single button for users to acknowledge and dismiss the notification may work very well. But anything more than a sentence or two creates complexity:</p>

<ol>
	<li>If the content is too long to fit into a small lightbox, will you add a scrollbar? If so, consider disabling the browser scrollbar to prevent users from mistakenly scrolling the background page instead of the lightbox. (Though disabling the browser scrollbar can have unintended consequences, especially for mobile users, if they are unable to scroll enough to see the overlay controls.)</li>
	<li>Will users want to bookmark the overlay content or share it? When an overlay contains actual content rather than a simple status message, users are more likely to want to do things with that content – like bookmark it for future reference, or share the link with others. You may need to do extra design work, to create special <em>save </em>and <em>share </em>functions.</li>
	<li>Will the content be presented better if you had a larger window (like the full-size browser window)? If users have to scroll a lot more to read the content just because it’s in an overlay, you’re <a href="../utilize-available-screen-space/index.php">wasting screen space</a>.</li>
</ol>

<p><strong>Complete a form:</strong> Overlays that include form fields make it possible to provide access to the form from any screen, without needing to load a new page. This is a very attractive option for things like login and subscription forms. But keep in mind, any bugs can make it impossible for users to access these essential functions. Again, either plan for both initial and ongoing testing (across browsers, operating systems, and devices) or stick to the tried and true login screen. Some issues to look for:</p>

<ol>
	<li>Overlays that ask users to provide input, even if it is just a single field, need an <strong>explicit ‘Cancel’ option</strong>. Too often designers assume that users will know they can just click outside of the modal to dismiss it. In fact, many users won’t know that; others (especially those using touchscreens) may dismiss the overlay accidentally. In the Bluefly example below, even clicking the gray area of the screen had no effect. The only way to dismiss this overlay was to click the black Shop Now button, but the placement of this button implies that it will submit the form—not dismiss it.

	<p><img alt="Screenshot of Bluefly.com subscription overlay" height="694" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2015/05/12/bluefly-overlay.png" width="800"/></p>

	<p><em>The only way to dismiss the Bluefly’s overlay is to click the black ‘</em>Shop Now’ <em>button. </em></p>
	</li>
	<li>To help users recover from accidentally closing the overlay, the field inputs <strong>should be saved</strong> and the overlay should be <strong>easy to reactivate</strong>.</li>
</ol>

<p><strong>Make a decision:</strong> Overlays are also commonly used to confirm user actions or to help users make simple selections. This is problematic because we’ve been trained by the many <a href="../making-hated-design-elements-work/index.php">pop-ups</a> and unimportant overlays we encounter on other websites to dismiss a modal window as soon as it appears. When we actually run into one that is important and useful, we may dismiss it automatically, before even reading the contents. This tendency is exacerbated by action buttons with generic labels, like US Airways’ payment-confirmation buttons labeled simply ‘<em>OK’ </em>and ‘<em>Cancel’. </em></p>

<p><img alt="Screenshot of USAirways purchase confirmation overlay" height="546" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2015/05/12/usairways-confirmation-overlay.png" width="800"/></p>

<p><em>US Airways uses an overlay to confirm before a credit card charge, but the action button labels don’t specifically state what action you are confirming. </em></p>

<p>To help users avoid dismissing a lightbox without understanding the consequences of their actions, use <strong>an explicit  button label</strong> that states exactly what will happen when the user clicks it, as LinkedIn does with their <em>‘Yes, Remove them’ </em>confirmation button on the lightbox to confirm deleting a contact.</p>

<p><img alt="Screenshot of Linkedin.com remove contacts confirmation overlay" height="477" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2015/05/12/linkedin-confirmation-overlay.png" width="800"/></p>

<p><em>LinkedIn uses the specific button label ‘Yes, remove them’ to confirm deleting a contact. </em></p>

<p>You should also consider what will happen if the user decides NOT to complete any of your target actions. Of course you should include an explicit way to dismiss the lightbox, but another common user strategy is to click the browser’s <em>Back </em>button in an attempt to revert to the underlying screen. By default, web browsers don’t treat overlays like separate pages, so the <em>Back </em>button takes users not to the underlying screen, but to whatever page preceded that screen. If your analytics show users clicking <em>Back </em>after reaching an overlay, consider adjusting the <em>Back </em>button so that when an overlay is displayed, the <em>Back </em>button closes the lightbox instead of navigating to the previous page in browser history.</p>

<h2>WHEN Will the Overlay Appear, and Will It Interrupt an Important Task?</h2>

<p>One of the biggest challenges with overlays is that they interrupt users from whatever they were doing before the overlay appeared. How can this interruption be avoided?</p>

<p>One strategy is to present overlays during times when users are less likely to be immersed in a task. For example, many applications use overlays for <a href="../mobile-instructional-overlay/index.php">introductory instructions</a> or tips for using an interface. Users may have a specific task in mind when launching an application, but this intrusion at least comes at the beginning the task, rather than right in the middle. The overlay format is especially helpful on small screens, where there is not enough room to show the tips anywhere but on top of the underlying screen.  </p>

<p><img alt="Screenshot of Zappos iphone app help overlay" height="525" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2015/05/12/zappos-help-overlay.png" width="350"/></p>

<p><em>The Zappos iPhone application uses an introductory overlay to explain the meaning of their icons. </em></p>

<ol>
	<li> <strong>User-initiated overlays</strong>  can be a great way to implement <a href="../progressive-disclosure/index.php">progressive disclosure</a>: giving people more information or options exactly at the time when they need those options. Often these types of overlays are closely related to the user’s current task, which takes advantage of the main benefit of overlays: the ability to<strong> maintain the context of the current screen</strong> while providing some additional content or features. For example, the Facebook iPhone application uses an overlay treatment to display various options for sharing content. This is actually just a modified visual treatment for a drop-down menu, which increases visual emphasis on the active workflow while maintaining the task context. When deciding how you want to share content it’s helpful to have the title of that piece of content   visible in the background – especially if you have just returned to the screen after an interruption.

	<p><img alt="Screenshot of Facebook sharing options overlay" height="621" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2015/05/12/facebook-sharing-overlay.png" width="350"/></p>

	<p><em>Facebook shows ‘share’ options as an overlay on top of an article; being able to see the article title ‘behind’ the overlay can help users remember what they were about to share if they are interrupted in the middle of sharing. </em></p>
	</li>
	<li><strong>Overlays that are not user initiated</strong>, however, are an entirely different story. When the website unexpectedly obscures the page someone was viewing, the experience ranges from distracting to obnoxious. Sometimes these notifications contain critical information that actually justifies the interruption, but more often these types of overlays are deliberate attempts to entice the user to take some action that benefits the organization (such as subscribing to a newsletter).  (Many web marketers seem to operate under the ‘ends justify the means’ principle, and believe that the benefit of increasing your subscriber base outweighs the negative consequences of annoying your users. If you attempt this strategy, make sure to follow up and measure whether those extra subscribers are actually qualified leads. Do they really end up buying your products? If not, you may be aggravating your true audience for no reason at all.)
	<p>Sometimes, overlays that are not user initiated seem to be more benign, but due to inappropriate timing can end up being just as irritating as the pushy subscription pitches.  Randomly timed <em>Help </em>chat windows are a frequent offender in this category. On the Bank of the West screen shown below, the <em>Live Chat Support </em>overlay indiscriminately appears on every page – even on forms where the user is actually in the middle of completing a form to open an account. In this case the overlay is actually making it <strong>more difficult </strong>for users to convert, by requiring them to click through the overlay before they can proceed to the next form field.</p>

	<p><img alt="Scerenshot of Bank of the West Chat overlay" height="837" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2015/05/12/bank-of-the-west-chat-overlay.png" width="800"/></p>

	<p><em>The Bank of the West website interrupts users who are attempting to complete the form to open an account with an overlay offering chat support. </em></p>

	<p>The safest strategy is to restrict overlays to only user-initiated actions, or truly urgent messages, and to use alternate means (like in-page feature boxes in the Apple.com example) for less-critical content. If you do decide to interrupt users with lightboxes, at the very least experiment with timing and target pages to ensure that you minimize the interruption—for example, by presenting the overlays only when users are ready to leave the site, like  DigitalMarketer.com does with  their ‘free gift’ offer. </p>

	<p><img alt="Screenshot of digital Marketer exit overlay" height="703" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2015/05/12/digital-marketer-exit-overlay.png" width="800"/></p>

	<p><em>This overlay on the DigitalMarketer.com website avoids interrupting visitors by waiting until they are about to navigate away from the site before displaying this overlay.</em></p>
	</li>
</ol>

<h2>WHERE Will the Overlay Appear on the Page?</h2>

<p>Too often, overlays appear too low on the page. This is especially prevalent on mobile devices, but also happens on full-size screens. When planning your design, remember that many users have browser toolbars installed, and the available browser viewing area may be much shorter than what you preview in a browser with no toolbars. The <strong>lightbox content should appear directly in the users’ line of sight</strong>, or err on the side of being higher on the page. On Verizon’s mobile broadband page below, most of the lightbox content is not visible.  </p>

<p><img alt="Screenshot of Verizon Mobile Broadband overlay" height="469" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2015/05/12/mobile-broadband-overlay.png" width="800"/></p>

<p><em>The overlay on Verizon’s mobile broadband site is centered on the page, but due to the user’s browser toolbars, the lower section of the lightbox isn’t visible. </em></p>

<p>Placing your overlay nearer to the top of the page (rather than in the exact center) minimizes the risk of having it cut off at the bottom, and keeping the browser scrollbar available ensures that even in very small browser windows, users will still be able to scroll down to view the entire lightbox. </p>

<p><img alt="Screenshot of USPS overlay" height="546" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2015/05/12/usps-overlay.png" width="800"/></p>

<p><em>On USPS, the overlay is closer to the top of the page, increasing the likelihood that the full contents will be immediately visible. </em></p>

<p>Another important consideration, especially for larger lightboxes, is <strong>where the controls and content should be placed</strong> within the overlay. Sometimes advertising overlays will deliberately place controls in unusual locations (apparently to force users to spend more time interacting with the ad content) as if tormenting customers is a way to engender warm feelings toward a brand. But you also often see control placement that is just poorly laid out. This can be especially challenging if a single standardized overlay design is used to present content with varying dimensions. In the example below, American Furniture Warehouse presents the content (a photograph of a desk) at the top of the lightbox, while the button to close the lightbox appears at the bottom, leaving a vast amount of empty space that users must scroll through in order to dismiss the overlay. </p>

<p><img alt="Screenshot of American Furniture Warehouse overlay" height="960" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2015/05/12/american-furniture-warehouse-overlay.png" width="800"/></p>

<p><em>American furniture warehouse has the ‘</em>Close<em>’ control at bottom and overlay is so big it might as well be a whole page.</em></p>

<h2>WHY Does This Content Need to Be in an Overlay Instead of Being Within a Page?</h2>

<p>Last but definitely not least: don’t use an overlay unless you have a clear, compelling case for why this content should not be presented within a regular page. Good reasons for using an overlay could include:</p>

<ul>
	<li>The user is about to take an action that has <strong>serious consequences</strong> and is difficult to reverse.</li>
	<li>It’s <strong>essential to collect</strong> a small amount of information before letting users proceed to the next step in a process.</li>
	<li>The content in the overlay is <strong>urgent</strong>, and users are more likely to notice it in an overlay.</li>
</ul>

<p>The first instance – using an overlay to confirm user actions – should be used with restraint, and only when it truly is difficult to reverse an action. Even then, you may want to offer users a ‘<em>Don’t ask me this again’ </em>option, in case it is an action they need to repeat many times.</p>

<p>Quickly collecting essential information with an overlay is aptly illustrated by the Chase.com screen below. The user has requested auto-loan rates, but the site can’t display the correct rates without knowing the user’s location. </p>

<p><img alt="Screenshot of Chase Zipcode overlay" height="444" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2015/05/12/chase-zip-code-form-overlay.png" width="800"/></p>

<p><em>Chase.com appropriately uses an overlay to collect critical information that is required to complete a user-requested action, which needs to be explained to the user</em></p>

<p>The last condition – giving users urgent information – is the most open to interpretation. ‘Urgent’ can mean different things to different people, after all. What seems urgent to a marketer interested in promoting a new product may seem completely irrelevant to a user who just wants to get to the search box to look for a completely different product.</p>

<p>It’s easiest to determine urgency (and to communicate it to your audience) when the organization’s interests and the users’ interests are closely aligned. For example, back in December, the Environmental Defense Fund website presented incoming visitors with an overlay soliciting donations and explaining that donations made prior to an upcoming deadline would be matched by a sponsor. Given the timing, this information would probably be considered urgent by many site visitors, especially those interested in making their end-of-the-year tax-deductible charitable gifts. But what about the second example? This overlay, presented in April, does not reference any specific deadline, and appears to be simply a generic sales pitch. <a href="../donation-usability/index.php">Collecting donations</a> is still urgent for the organization, but is much less likely to be considered urgent by visitors. </p>

<p><img alt="Screenshot of Environmental Defense Fund overlay 1" height="586" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2015/05/12/environmental-defense-fund-1.png" width="800"/></p>

<p><em>The Environmental Defense Fund used an overlay to convey time-sensitive information.</em></p>

<p><img alt="Screenshot of no deadline Environmental Defense Fund overlay" height="626" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2015/05/12/environmental-defense-fund-2.png" width="800"/></p>

<p><em>The Environmental Defense Fund's call to action is much less relevant to the user when the urgency is not convincingly communicated. </em></p>

<p>Notice that our list of good reasons to use an overlay does not include ‘so we won’t have to set up a new page template’. (If setting up a new page is so difficult that you find yourself sticking content into overlays, perhaps it’s time to reconsider your website platform.) Recruiting user-research participants is also not included. As much as we value user research, the number of sites bombarding incoming visitors with survey invitations is reaching near-epidemic proportions. Surely there are less intrusive ways to collect user feedback.</p>

<h2>Conclusion</h2>

<p>Overlays can be effective in certain contexts, but too often they are used without enough planning, as a convenient way to insert additional content that may or may not be appropriate. They frequently break on mobile or on smaller browser windows, create confusion and annoyance, cause people to work harder to achieve the same task, and make content hard to access at later times. In other words, the overlay benefits are often overwhelmed by disadvantages. </p>

<p>In many cases presenting the content within a page instead of an overlay can get around these problems.  Before considering an overlay, carefully investigate your motivations and think about the five Ws. If you still think that overlays are the right design solution for you, proceed with care. Test repeatedly on a variety of browsers and devices to make sure that your design does not break.</p>

<p>Learn more about how to effectively present content and facilitate task workflows in our full-day seminar on <a href="../../courses/web-ux-design-guidelines/index.php">Top Web UX Design Guidelines</a></p>
</section>
  

  
  <section>
    
        
    
  </section>
  
  
  
  
  

  


  <section>
    <br />
    <p class="share"><b>Share <span class="hide-for-small-down">this article</span>:</b> <a href="https://twitter.com/intent/tweet?url=http://www.nngroup.com/articles/overuse-of-overlays/&amp;text=Overuse%20of%20Overlays:%20How%20to%20Avoid%20Misusing%20Lightboxes&amp;via=nngroup">Twitter</a> | <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=http://www.nngroup.com/articles/overuse-of-overlays/&amp;title=Overuse%20of%20Overlays:%20How%20to%20Avoid%20Misusing%20Lightboxes&amp;source=Nielsen%20Norman%20Group">LinkedIn</a> | <a href="https://plus.google.com/share?url=http://www.nngroup.com/articles/overuse-of-overlays/">Google+</a> | <a href="mailto:?subject=NN/g Article: Overuse of Overlays: How to Avoid Misusing Lightboxes&amp;body=http://www.nngroup.com/articles/overuse-of-overlays/">Email</a></p>
    <hr />
  </section>

  <footer class="l-subsection">
    
    
    <div class="learn-more">
      <h2>Learn More</h2>
      <p><a href="../subscribe/index.php">Subscribe to the weekly newsletter</a> to get notified about future articles.</p>
      </div>
    
    

    <div class="row show-for-small-down related-content">
      
      
      

      
      
        <div class="column small-12 topic-links">All articles about:
          <ul>
            
            <li><a href="../../topic/web-usability/index.php">Web Usability</a></li>
            
          </ul>
        </div>
      
      

      
      
        <div class="column small-12">
          <h3>UX Conference Training Courses</h3>
           <ul class="no-bullet">
    
        <li><a href="../../courses/ux-basic-training/index.php">UX Basic Training</a></li>
    
        <li><a href="../../courses/human-mind/index.php">The Human Mind and Usability</a></li>
    
        <li><a href="../../courses/emerging-patterns-web-design/index.php">Emerging Patterns for Web Design</a></li>
    
        <li><a href="../../courses/web-page-design/index.php">Web Page UX Design</a></li>
    
</ul>
        </div>
      
      

      
      
        <div class="small-12 column">
          <h3>Research Reports</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../reports/b2b-websites-usability/index.php">B2B Website Usability for Converting Users into Leads and Customers</a></li>
              
            
              
                <li><a href="../../reports/strategic-design-faqs/index.php">Strategic Design for Frequently Asked Questions</a></li>
              
            
          </ul>
        </div>
      
      

      
      
        <div class="small-12 column">
          <h3>Online Seminars</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../online-seminars/fundamental-flaw-flat-design/index.php">The Fundamental Flaw in Flat Design</a></li>
              
            
              
                <li><a href="../../online-seminars/millennials-online/index.php">Millennials Online</a></li>
              
            
              
                <li><a href="../../online-seminars/university-websites/index.php">University Website UX Essentials</a></li>
              
            
          </ul>
        </div>
      
      
    </div>

    <div class="hide-for-small-down row related-content">
      <div class="medium-6 columns">
        
        
          <div>
            <h3>Articles</h3>
            <ul class="no-bullet">
              
                
                <li><a href="../ux-thanks/index.php">Give Thanks for Good UX</a></li>
                
              
                
                <li><a href="../top-10-enduring/index.php">Top 10 Enduring Web-Design Mistakes</a></li>
                
              
                
                <li><a href="../university-sites/index.php">University Websites: Top 10 Design Guidelines</a></li>
                
              
                
                <li><a href="../pop-up-adaptive-help/index.php">Pop-ups and Adaptive Help Get a Refresh</a></li>
                
              
                
                <li><a href="../phone-plan-ux/index.php">Shopping Online for an iPhone Plan Is Painful</a></li>
                
              
            </ul>
          </div>
        
        

        
          
          <div>
            <h3>Research Reports</h3>
            <ul class="no-bullet">
              
                
                  <li><a href="../../reports/b2b-websites-usability/index.php">B2B Website Usability for Converting Users into Leads and Customers</a></li>
                
              
                
                  <li><a href="../../reports/strategic-design-faqs/index.php">Strategic Design for Frequently Asked Questions</a></li>
                
              
            </ul>
          </div>
          
        

      </div>
      <div class="medium-6 columns">
        
          
          <div>
            <h3>UX Conference Training Courses</h3>
             <ul class="no-bullet">
    
        <li><a href="../../courses/ux-basic-training/index.php">UX Basic Training</a></li>
    
        <li><a href="../../courses/human-mind/index.php">The Human Mind and Usability</a></li>
    
        <li><a href="../../courses/emerging-patterns-web-design/index.php">Emerging Patterns for Web Design</a></li>
    
        <li><a href="../../courses/web-page-design/index.php">Web Page UX Design</a></li>
    
</ul>
          </div>
          
        
        
          
          <div>
            <h3>Online Seminars</h3>
            <ul class="no-bullet">
            
                <li><a href="../../online-seminars/fundamental-flaw-flat-design/index.php">The Fundamental Flaw in Flat Design</a></li>
            
                <li><a href="../../online-seminars/millennials-online/index.php">Millennials Online</a></li>
            
                <li><a href="../../online-seminars/university-websites/index.php">University Website UX Essentials</a></li>
            
            </ul>
          </div>
          
        
      </div>
    </div>
  </footer>
</article>

        </div>

      

    
  </div>
  <div class="row">
    
      <div class="column small-12 show-for-small-down l-navrail announcements">
        
          
        
      </div>
    
    
  </div>

    </div>
    




<footer id="page-footer">
    <div class="row hide-for-print">
        <div class="column small-12 medium-6">
            <section class="footer-popular">
                
                <h1>Popular Topics</h1>
                <div style="float: left;  width: 15em; display: block;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/applications/index.php">Application Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/e-commerce/index.php">E-Commerce Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/eyetracking/index.php">Eyetracking </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/information-architecture/index.php">Information Architecture </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/intranets/index.php">Intranet Design </a></li>
	<li><a href="../../topic/interaction-design/index.php">Interaction Design </a></li>
</ul>
</div>

<div style="float: left;  width: 15em; display: block; list-style: none;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/mobile-and-tablet-design/index.php">Mobile and Tablet Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/credibility-persuasion/index.php">Persuasive Design</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/research-methods/index.php">Research Methods</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/user-testing/index.php">User Testing </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/web-usability/index.php">Web Usability </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/writing-web/index.php">Web Writing &amp; Content Strategy </a></li>
</ul>
</div>

<p>&nbsp;</p>

                
            </section>
        </div>

        <div class="column small-12 medium-6">
            <div class="row">
                <div class="column small-6">
                    <section class="footer-about">
                        <h1>Follow Us</h1>
                        <ul>
	<li><a href="../subscribe/index.php">Subscribe to our weekly UX newsletter</a>,&nbsp;Jakob Nielsen&#39;s Alertbox</li>
	<li>Visit Don Norman&#39;s website, <a href="http://www.jnd.org/"> www.jnd.org </a></li>
	<li>Visit Bruce Tognazzini&#39;s website, <a href="http://www.asktog.com/"> www.asktog.com </a></li>
	<li style="padding-top: 6px;"><a class="twitter-follow-button" data-show-count="false" data-size="large" href="https://twitter.com/nngroup"><img alt="Follow @nngroup on Twitter" height="20" src="https://media.nngroup.com/media/editor/2016/12/30/twitter-follow-button.png" style="width: 117px; height: 20px;" width="117" /></a></li>
	<li style="padding-top: 6px;"><img alt="" height="14" src="https://media.nngroup.com/media/editor/2016/07/26/feed-icon-14x14.png" style="vertical-align:text-top;width:14px;margin-right:.25em;" width="14" /><a href="../../feed/rss/index.php">Subscribe to our RSS feed</a></li>
</ul>

                    </section>
                </div>

                <div class="column small-6">
                    <section class="footer-about right-column">
                        <h1>About
</h1>
                        <ul class="no-bullet">
	<li><a href="../../about/contact/index.php">Contact information</a></li>
	<li><a href="../../about/index.php">About Us</a></li>
	<li><a href="../../about/why-nng/index.php">Why NN/g? </a></li>
	<li><a href="../../return-policy/index.php">Return Policy</a></li>
</ul>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="copyright column small-12">
            <p><a href="../../copyright-and-reprint-info/index.php">Copyright</a> &copy; 1998-2017 Nielsen Norman Group, All Rights Reserved.</p>
        </div>
    </div>
</footer>


    


        <script src="https://media.nngroup.com/static/vendor/fastclick/js/fastclick.min.js"></script>
        <script src="https://media.nngroup.com/static/vendor/foundation/js/foundation.min.js"></script>
        <script src="http://media.nngroup.com/static/js/app.js"></script>
        <script src="http://media.nngroup.com/static/js/scroll-to-fixed.js"></script><script>var externalLinks = document.querySelectorAll('a[href^="http"]');for (var i = externalLinks.length-1; i >= 0; i--) {externalLinks[i].addEventListener("click", function(event) { alert("External links are disabled for this test."); event.preventDefault(); }, false);}var mailLinks = document.querySelectorAll('a[href^="mailto"]');for (var i = mailLinks.length-1; i >= 0; i--) {mailLinks[i].addEventListener("click", function(event) { alert("Email links are disabled for this test."); event.preventDefault(); }, false);}var externalForms = document.querySelectorAll('form');for (var i = externalForms.length-1; i >= 0; i--) {externalForms[i].addEventListener("submit", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false); externalForms[i].addEventListener("click", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false);}</script>

        <script>
            
            $(function() {
                $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
                $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
            });
        </script>

        
<script src="https://media.nngroup.com/static/js/articles.js"></script>


        
        <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1072699960;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072699960/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
    </body>

<!-- Mirrored from www.nngroup.com/articles/overuse-of-overlays/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:13:02 GMT -->
</html>
