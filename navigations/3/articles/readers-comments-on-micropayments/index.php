<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->





    
<!-- Mirrored from www.nngroup.com/articles/readers-comments-on-micropayments/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:17:25 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1026.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"beacon":"bam.nr-data.net","queueTime":9,"applicationTime":551,"agent":"","licenseKey":"1a30ef9931","errorBeacon":"bam.nr-data.net","applicationID":"2048992","transactionName":"NFxSMRBUX0EDUkIPDA0WdhAMVkVbDV8ZBxEXUFMJB0YfRAtUQRVZIktEDAFZVHYHRVcPD01eVRE="}</script>
        <title>Readers&#39; Comments on Micropayments  </title><meta property="og:title" content="Readers&#39; Comments on Micropayments  " />
  
        
        <meta name="description" content="Case against micropayments; corporate charge-backs will be difficult to manage; international microcurrency conversion; need an &#39;alarm&#39; UI for sudden multiple charges.">
        <meta property="og:description" content="Case against micropayments; corporate charge-backs will be difficult to manage; international microcurrency conversion; need an &#39;alarm&#39; UI for sudden multiple charges." />
        
  
        
	
        
        <meta name="keywords" content="micropayments, microtransactions, international payments, currency conversion, flat-rate charging, bandwidth pricing">
        

        

        <meta name="robots" content="noarchive">
        <meta name="viewport" content="width=device-width">

        <link rel="search" type="application/opensearchdescription+xml" title="NN/g Search" href="../../opensearch.xml" />
        <link rel="shortcut icon" href="http://media.nngroup.com/static/img/favicon.ico" />

        <link rel="stylesheet" href="https://media.nngroup.com/static/vendor/font-awesome/css/font-awesome.min.css">

        
        <link rel="stylesheet" href="https://media.nngroup.com/static/css/7232a960f1df.css" type="text/css" />
        <link rel="stylesheet" href="http://fast.fonts.com/t/1.css?apiType=css&amp;projectid=c5846260-9def-46ec-9407-0f1349bd2d64" type="text/css">

        <script src="https://media.nngroup.com/static/vendor/modernizr/js/modernizr.min.js"></script>

        <script type="text/javascript" src="https://media.nngroup.com/static/js/3283be5def74.js"></script>
        

        

        
        
        <!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=229034,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('http://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->


<!-- Google Analytics Tracking Code for http://nngroup.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23132681-1', 'nngroup.com');
  ga('require', 'linkid', 'https://www.nngroup.com/articles/readers-comments-on-micropayments/linkid.js');
  ga('require', 'displayfeatures');

// Optimizely Universal Analytics Integration
 window.optimizely = window.optimizely || [];
 window.optimizely.push("activateUniversalAnalytics");

  ga('send', 'pageview');

</script>
<script>
/**
* Function to track a click on an outbound link in Google Analytics.
*/
function trackOutboundLink(label, url) {
    if(window.ga && ga.loaded) {
        ga('send', 'event', 'outbound', 'click', label, {'hitCallback': function(){document.location = url;}
        });
    } else {
        document.location = url;
    }
}
/**
* Function to track a click on a link in the article announcement area.
*/
$('#articleAnnouncement a').on('click', function(){
  ga('send', 'event', 'link', 'click', 'articleAnnouncement');
});
</script>

<script type="text/javascript">
 setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2421.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



<!-- Hotjar Tracking Code for http://nngroup.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:121605,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

        <script src="https://media.nngroup.com/static/js/analytics/track_outbound_link.js"></script>
        
        
        
    </head>
    <body class="location-articles articles-detail">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
    <header id="page-header">
        <div id="cart" class="shopping-cart mobile-cart show-for-small-down"></div>
        <div class="row">
            <div id="masthead" class="masthead small-12 medium-7 column">

                <!-- The button to toggles menu on small screen -->

                <h1><a href="../../index.php"><img src="http://media.nngroup.com/static/img/logo-wordmark.png" width="74" height="30"> Nielsen <span class="norman">Norman</span> Group</a></h1>
                <p class="tagline show-for-medium-up">Evidence-Based User Experience Research, Training, and Consulting</p>

                <!-- The button to toggles form search on small screen -->


                <div style="clear: right;width:  100%;height: 1px;"></div><?php define('ROOT_PATH',$_SERVER["DOCUMENT_ROOT"] . '/apps/navigation_styles/navigations/3'); ?><?php include(ROOT_PATH)."/navigation.php"; ?>

                <!-- Drop down menu on small screen -->
                <div id="hideNav" class="show-for-small-down">
                  
                  <ul class="menu-content">
                    <li><a href="../../index.php">Home</a></li>
                    <li class="has-submenu">
                      <a>Training</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../training/index.php">Overview</a></li>
                        
<li id="nav-ux-conference"><a href="../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../online-seminars/index.php"><span>Online Seminars</span></a></li>

                      </ul>
                    </li>
                    <li class="has-submenu">
                      <a>Consulting</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
  <li><a href="../../consulting/index.php">Overview</a></li>
  
  <li><a href="../../consulting/benchmarking/index.php">Benchmarking</a></li>
  
  <li><a href="../../consulting/expert-design-review/index.php">Expert Design Review</a></li>
  
  <li><a href="../../consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
  
  <li><a href="../../consulting/usability-testing/index.php">Usability Testing </a></li>
  
  <li><a href="../../consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
  
  <li><a href="../../in-house-training/index.php">In-House Training</a></li>
</ul>

                    </li>

                    <li><a href="../../reports/index.php">Reports</a></li>
                    <li><a href="../index.php">Articles</a></li>
                    <li class="has-submenu"><a>About NN/G</a><i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../about/index.php">Overview</a></li>
                        <li><a href="../../people/index.php">People</a></li>
                        <li><a href="../../about/why-nng/index.php">Why NN/g?</a></li>
                        <li><a href="../../about/contact/index.php">Contact</a></li>
                        <li><a href="../../news/index.php">News</a></li>
                        <li><a href="../../about/history/index.php">History</a></li>
                        <li><a href="../../books/index.php">Books</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                </div>

                <hr class="header-divider show-for-small-only"/>


            </div>
       </div>

        <div id="hideNav">            <nav class="primary-nav small-12 column">
                <ul>
                    <li><a class="navitem" id="nav-home" href="../../index.php">Home</a></li>
                    <li><a class="navitem" id="nav-training" href="../../training/index.php">Training</a></li>
                    <li><a class="navitem" id="nav-consulting" href="../../consulting/index.php">Consulting</a></li>
                    <li><a class="navitem" id="nav-reports" href="../../reports/index.php">Reports</a></li>
                    <li><a class="navitem" id="nav-articles" href="../index.php">Articles</a></li>
                    <li><a class="navitem" id="nav-about" href="../../about/index.php">About NN/G</a></li>
                </ul>
            </nav>
            <nav class="secondary-nav small-12 medium-12 large-12 columns">
                
                
            </nav>
        </div>
    </header>
    <div id="mainContent" class="l-content">
    
  

  

  <div class="row">
    
      
      <section class="small-12 medium-3 columns collapsable-content l-navrail articles-left-column">
        <h1 class="show-for-small-down collapse-link mobile-browse-header">Browse by Topic and Author</h1>
          <div class="row collapsable">

            
            <div class="small-6 medium-12 columns">
              <section id="article-popular-topics" style="display: none;">
                
                <h1>Topics</h1>
                <ul id="article-popular-topics">
                    
                    <li>
                        
                        <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/intranets/all/index.php">Intranets</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/user-testing/all/index.php">User Testing</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                        
                    </li>
                    
                    <li><a href="#" id="article-see-all-topics"><strong>See all topics&hellip;</strong></a></li>
                </ul>
                
              </section>
              <section id="article-all-topics" style="display: none;">
                
                <h1>
                  <span class="show-for-medium-up">All Article Topics</span>
                  <span class="show-for-small-down">All Topics</span>
                  (<a href="#" id="article-hide-all-topics">hide</a>)
                </h1>
                <ul>
                  
                  <li>
                    
                    <a href="../../topic/accessibility/all/index.php">Accessibility</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/agile/all/index.php">Agile</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/analytics-and-metrics/all/index.php">Analytics &amp; Metrics</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/applications/all/index.php">Application Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/b2b-websites/all/index.php">B2B Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/behavior-patterns/all/index.php">Behavior Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/branding/all/index.php">Branding</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/content-strategy/all/index.php">Content Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/corporate-websites/all/index.php">Corporate Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/design-patterns/all/index.php">Design Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-design-process/all/index.php">Design Process</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/email/all/index.php">Email</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/eyetracking/all/index.php">Eyetracking</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/heuristic-evaluation/all/index.php">Heuristic Evaluation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/human-computer-interaction/all/index.php">Human Computer Interaction</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-ideation/all/index.php">Ideation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/information-architecture/all/index.php">Information Architecture</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/interaction-design/all/index.php">Interaction Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/international-users/all/index.php">International Users</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/intranets/all/index.php">Intranets</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-management/all/index.php">Management</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/navigation/all/index.php">Navigation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/non-profit-websites/all/index.php">Non-Profit Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/personas/all/index.php">Personas</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/persuasive-design/all/index.php">Persuasive Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/prototyping/all/index.php">Prototyping</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/psychology-and-ux/all/index.php">Psychology and UX</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/research-methods/all/index.php">Research Methods</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/search/all/index.php">Search</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/social-media/all/index.php">Social Media</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/strategy/all/index.php">Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/user-testing/all/index.php">User Testing</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-teams/all/index.php">UX Teams</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/visual-design/all/index.php">Visual Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/writing-web/all/index.php">Writing for the Web</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/young-users/all/index.php">Young Users</a>
                    
                  </li>
                  
                </ul>
                
              </section>
            </div>
            <script>
              /* Determine which topics list will be displayed: the full or the
               * partial.  We are inlining this close to the list itself to
               * prevent rendering delays on the client
               */
              var topic_list = $.cookie("article-topics");

              var popular_topics = $('#article-popular-topics');
              var all_topics = $('#article-all-topics');

              if(topic_list == 1) {
                  popular_topics.hide();
                  all_topics.show();
              } else {
                  popular_topics.show();
                  all_topics.hide();
              }
            </script>
            

            
            <div class="small-6 medium-12 columns">
              
              <section id="article-initial-authors">
                  <h1>Author</h1>
                  <ul>
                    
                    <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                    
                    <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                    
                    <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                    
                    <li><a href="#" onClick="$('#article-initial-authors').hide(); $('#article-all-authors').show();"><strong>See all authors&hellip;</strong></a></li>
                  </ul>
              </section>
              <section id="article-all-authors" style="display: none;">
                <h1>
                  All Authors
                  (<a href="#" id="article-hide-all-authors">hide</a>)
                </h1>
                <ul>
                  
                  <li><a href="../author/raluca-budiu/index.php">Raluca Budiu</a></li>
                  
                  <li><a href="../author/patty-caya/index.php">Patty Caya</a></li>
                  
                  <li><a href="../author/susan-farrell/index.php">Susan Farrell</a></li>
                  
                  <li><a href="../author/therese-fessenden/index.php">Therese Fessenden</a></li>
                  
                  <li><a href="../author/kim-flaherty/index.php">Kim Flaherty</a></li>
                  
                  <li><a href="../author/sarah-gibbons/index.php">Sarah Gibbons</a></li>
                  
                  <li><a href="../author/aurora-harley/index.php">Aurora Harley</a></li>
                  
                  <li><a href="../author/kate-kaplan/index.php">Kate Kaplan</a></li>
                  
                  <li><a href="../author/page-laubheimer/index.php">Page Laubheimer</a></li>
                  
                  <li><a href="../author/angela-li/index.php">Angie Li</a></li>
                  
                  <li><a href="../author/hoa-loranger/index.php">Hoa Loranger</a></li>
                  
                  <li><a href="../author/kate-meyer/index.php">Kate Meyer</a></li>
                  
                  <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                  
                  <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                  
                  <li><a href="../author/kara-pernice/index.php">Kara Pernice</a></li>
                  
                  <li><a href="../author/christian-rohrer/index.php">Christian Rohrer</a></li>
                  
                  <li><a href="../author/amy-schade/index.php">Amy Schade</a></li>
                  
                  <li><a href="../author/katie-sherwin/index.php">Katie Sherwin</a></li>
                  
                  <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                  
                  <li><a href="../author/kathryn-whitenton/index.php">Kathryn Whitenton</a></li>
                  
                </ul>
              </section>
              
            </div>
            
          </div>

          
          <section class="hide-for-small-down">
            
              
              <h1>Recent Articles</h1>
              
              <ul>
                  
                  <li><a href="../seamless-cross-channel/index.php">Seamlessness in the Omnichannel User Experience</a></li>
                  
                  <li><a href="../brand-intention-interpretation/index.php">Experience Design: Bridging Brand Intention and Brand Interpretation</a></li>
                  
                  <li><a href="../eyetracking-tasks-efficient-scanning/index.php">Scanning Patterns on the Web Are Optimized for the Current Task</a></li>
                  
                  <li><a href="../flat-design-best-practices/index.php">Flat-Design Best Practices</a></li>
                  
                  <li><a href="../ux-user-stories/index.php">Accounting for UX Work with User Stories in Agile Projects</a></li>
                  
                  <li><a href="../index.php"><strong>See all articles&hellip;</strong></a></li>
              </ul>
              
            
          </section>
          

          
          
          <section class="hide-for-small-down">
            <h1>Popular Articles</h1>
            
            <ul>
                
                <li><a href="../usability-101-introduction-to-usability/index.php">Usability 101: Introduction to Usability</a></li>
                
                <li><a href="../top-10-mistakes-web-design/index.php">Top 10 Mistakes in Web Design</a></li>
                
                <li><a href="../how-users-read-on-the-web/index.php">How Users Read on the Web</a></li>
                
                <li><a href="../f-shaped-pattern-reading-web-content/index.php">F-Shaped Pattern For Reading Web Content</a></li>
                
                <li><a href="../ten-usability-heuristics/index.php">10 Usability Heuristics for User Interface Design</a></li>
                
                <li><a href="../intranet-design-2016/index.php">10 Best Intranets of 2016</a></li>
                
                <li><a href="../which-ux-research-methods/index.php">When to Use Which User-Experience Research Methods</a></li>
                
                <li><a href="../response-times-3-important-limits/index.php">Response Times: The 3 Important Limits</a></li>
                
                <li><a href="../why-you-only-need-to-test-with-5-users/index.php">Why You Only Need to Test with 5 Users</a></li>
                
                <li><a href="../page-fold-manifesto/index.php">The Fold Manifesto: Why the Page Fold Still Matters</a></li>
                
            </ul>
          </section>
          
          

          
            <section class="hide-for-small-down">
              
            </section>
          

      </section>
      

      
        <div class="small-12 medium-9 columns content-body">
          




<article class="l-subsection alertbox-article-content article-detail-bl">
  
  <header>
    <div class="l-subsection">
      <h1>Readers&#39; Comments on Micropayments  </h1>
      <div class="author-meta">by
        
          
            
            <a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a>
            
          
        on  January 25, 1998
        
      </div>

      

      
      <div class="topic-links">
        Topics:
        
          <ul>

</ul>
        
      </div>
      
    </div>
  </header>
  

  
  
  

  
  <section id="article-body"><p>
 <em>
  Sidebar to
  <a href="../../people/jakob-nielsen/index.php" title="Author biography">
   Jakob Nielsen
  </a>
  &#39;s column on
  <a href="../the-case-for-micropayments/index.php">
   micropayments
  </a>
  .
 </em>
</p>
<p>
 The Alertbox on micropayments was one of the most controversial I have written, judging from the email I received. In fact, I got so many good email comments that I thought it would be a waste not to share them (after getting permission from the authors, of course).
</p>
<h2>
 Micropayments Won&#39;t Work
</h2>
<p>
 <em>
  Kevin Crossman writes:
 </em>
</p>
<blockquote>
 I love reading your Alertboxes, but on this one you are wrong.
 <p>
  If I&#39;m at work, who&#39;s gonna pay for my micropayments? I&#39;m going to have to get some sort of overhead charge number to surf the Web? Is my manager going to want to pay for these micropayments? What about his manager???
 </p>
 <p>
  I know it sounds
  <em>
   totally stupid
  </em>
  , but this is the mentality is the business world. I could wait for some extra long download for &quot;free&quot; or I could use a micropayment to get the download off a fast mirror. My boss would choose the &quot;free&quot; scenario.
  <em>
   Even though
  </em>
  the company loses in the long run. That&#39;s just the way things are.
 </p>
 <p>
  Micropayments will work fine for things like articles on news sites, porno sites (obviously), and others... but please don&#39;t let me hear you want regular old pages to have micropayments. That&#39;s crazy and people will RESIST!
 </p>
</blockquote>
<p>
 <em>
  Jakob&#39;s reply:
 </em>
 There are no doubt many companies with &quot;
 <a href="http://www.dilbert.com/comics/dilbert/archive/" title="Official Dilbert website with recent comic strips">
  Dilbert
 </a>
 &quot; style management that makes irrational decisions. However, such companies will eventually be driven out of business since wasting money
 <em>
  is
 </em>
 wasting money, even if it doesn&#39;t appear as an official budget line item. If you waste 3 cents&#39; worth of employee productivity to save one cent in direct expenses, then your company will be ill-suited for the coming fast-paced network economy.
 <br />
 (For the sake of overseas&#39; readers I should explain that &quot;Dilbert&quot; is a popular cartoon figure who has a clueless boss. If micropayments were widely used, I would now show a Dilbert cartoon that was particularly illustrative of my point, but doing so is currently impossible: Copyright law prevents me from showing the cartoon since there is no revenue source to provide the royalty payments for a legal copy.)
 <br />
 <strong>
  Corporate accounting for micropayments
 </strong>
 will happen along the same lines as corporate accounting for telephone calls: it will automatically be charged back to your department. If you use much more than the average $20 or so per month, then your boss may ask you to back off from the Web a little. Access to personal-interest pages might be covered by the company (just like most companies allow you to make a small number of short personal telephone calls), or employees may be asked to charge such content to their personal accounts. Future Web-access software should allow users to define multiple micropayment accounts to be used for different kinds of content. Companies that wanted to manage their micropayments tightly could subscribe to a rating service that would classify sites as work-related or personal-interest and could automatically block payments for the latter category unless the user provided a personal charge account.
</p>
<p>
 <em>
  Andrew Odlyzko writes:
 </em>
</p>
<blockquote>
 I saw you interesting column on micropayments. I am very interested in the subject, and am a coinventor of a scheme of this type. However, extensive study of the economics and psychology have convinced me that while micropayments will play a useful role, it will be a small one. If you are interested in the details of the arguments, they can be found in my papers &quot;The bumpy road of electronic commerce&quot; and &quot;Fixed fee versus unit pricing for information goods: competition, equilibria, and price wars.&quot;
</blockquote>
<p>
 <em>
  The
  <a href="http://www.ocrat.com/" title="Ocrat: site with Chinese-related web applications">
   Ocrat Webmaster
  </a>
  writes:
 </em>
</p>
<blockquote>
 Preventing fraud will be very difficult. Think of the trickery people already use today to get a high listing in search engines, and then imagine adding an extra financial incentive to do so! I have some pages that show
 <a href="http://www.ocrat.com/chargif/hkmovie.php" title="ocrat.com: animated Hong Kong names">
  how to write the names of Hong Kong movie stars in Chinese
 </a>
 ... lots of people come from search engines looking for dirty pictures (I can tell from the referer and search-engine query strings in my logs) and then back out immediately... I&#39;d be happy if they all left a penny behind. Actually, some follow the link to my home page and then back out... another penny.
 <p>
  Also, HTML mail is already a problem due to privacy issues (if I send you mail with &lt;IMG SRC=&quot;http://www.ocrat.com/jakob_useit.gif&quot;&gt;, I can tell when/if you read my mail or maybe even discover your identity if you were anonymous). Imagine if spammers start embedding pay-per-view 1x1 GIFs into their junk mail.... 57 million pennies add up pretty quickly.
 </p>
 <p>
  Most people will fall victim to these schemes because they&#39;ll set their payment thresholds to automatically accept payment below a certain amount... just like the &quot;warn before accepting cookies&quot; option in Netscape, the alternative is clicking on a million dialog boxes.
 </p>
 <p>
  You&#39;ve
  <a class="old" href="../the-telephone-is-the-best-metaphor-for-the-web/index.php" title="Alertbox May 1997: the telephone is a better metaphor for the Web than television">
   compared the Internet to the telephone
  </a>
  in a previous column, and many dubious semi-legal practices exist there already: hotel phone surcharges, 900-number ripoffs, pager or fax callback scams to a stealth pay-per-minute number, etc. Similar schemes on the Internet will open the door for governments to march in with clumsy and heavy-handed regulation. Scammers lack ethics but not imagination.
 </p>
 <p>
  These problems would be eliminated if micropayments were simply voluntary (the &quot;PBS&quot; or &quot;panhandling&quot; approach). A small button at the top of each page could say &quot;Click here to send 25 cents&quot;, or during PBS-style fundraising drives, you could even interrupt regular navigation with annoying interstitial pages begging for money. Browsers would have to prevent this button from being &quot;clicked&quot; automatically, but there is prior experience here in such security issues (preventing e-mail from being sent automatically, etc).
 </p>
 <p>
  This fits the main criterion stated in your first sentence ( &quot;those who pay for something control it&quot; ), without all the overhead. And the benefit would be greatest for the small, one-person sites where even $20/month would let them recover their hosting fees or charges for extra bandwidth and diskspace.
 </p>
</blockquote>
<p>
 <em>
  Jakob&#39;s reply:
 </em>
 You have discovered an additional argument not to read email in a Web browser. If the user was using an email client, then its default setting would presumably be to refuse all micropayments or to ask the user explicitly before paying even the smallest amount. After all, you don&#39;t want to give anybody in the world a license to charge you money simply by sending you an unsolicited email message.
 <br />
 The voluntary approach to payments may work, though the history of shareware makes me doubt it. A compromise might be a system where users were forced to pay a certain amount of money (say, $10 per month) to the payment service which would distribute payments to content providers proportional to their share of &quot;fundraising&quot; clicks. Thus, if a user only clicked on a single pay-this-site button during the month, then that site would collect the full $10. But the user would not be discouraged from clicking on these buttons on other sites since the charge would be the same. More commonly, the user would click maybe a hundred times in a month, and each site would get 10 cents.
 <br />
 By the way, animating the stroke order for the Hong Kong movie stars&#39; names is a rare example of good use of animation on the Web. Bravo! As you have shown,
 <a class="old" href="../guidelines-for-multimedia-on-the-web/index.php" title="Alertbox Dec. 1995: guidelines for multimedia on the Web">
  multimedia
 </a>
 really does have its place on the Web, but only when used to
 <em>
  enhance
 </em>
 communication; not to distract.
</p>
<h2>
 Payment Services
</h2>
<p>
 <em>
  John Heiland writes:
 </em>
</p>
<blockquote>
 While comparison to payment thresholds observed in phone usage patterns seems valid, the model of payment does not extend to the Web. The phone company does not redistribute my payment to the people I have called, or, in the case of 900 numbers, the cost associated is closer to a subscription than that of a micropayment. What system do you hypothesize for handling the transaction of one to ten cent payments? Existing clearing houses such as credit card companies incur too much overhead to be distribution hubs for micropayment.
</blockquote>
<p>
 <em>
  Patrick Breitenbach writes:
 </em>
</p>
<blockquote>
 Jakob, but what about a subscription model where you get access to numerous &quot;sites&quot; on a flatter-fee basis. A virtual AOL or an MSN with more non-Microsoft content? Depending on what would constitute a non-free page, I&#39;m not sure I&#39;d pay a cent. I view far more than 100 pages of value a day.
</blockquote>
<p>
 <em>
  Jakob&#39;s reply:
 </em>
 John Heiland and Patrick Breitenbach are right, there will have to be a highly automated payment service to sit between the users and the websites. There is no way it will be possible to have users pay sites directly and keep overhead down. This payment service will also have to operate much more efficiently than current credit card companies. This is possible since everything is computerized and since the security requirements are much smaller when handling payments on the order of one cent per transaction (whereas a credit card allows you to buy a $10,000 diamond bracelet in one transaction).
</p>
<p>
 <em>
  David Long writes:
 </em>
</p>
<blockquote>
 There was a thread on Seidman&#39;s (In, Around, and Online) discussion boards a while back about how flat-fee access wiped out the content industry&#39;s existing business model (and to some minds, failed to replace it with anything satisfactory)
 <p>
  In summary, back when online services charged hourly fees, they would split revenues with the content providers according to time spent. (the member-hour was the unit for AOL, IIRC) However, now, with the reliance on advertising reach, there is only revenue support for those who can aggregate very large numbers of eyeballs.
 </p>
 <p>
  So, at least at one time, at least some segments of the online world actually had micropayments, but lost them!
 </p>
</blockquote>
<p>
 <em>
  Jakob&#39;s reply:
 </em>
 It was a
 <strong>
  black day when AOL went to flat-rate pricing
 </strong>
 : quality of service dropped like a stone. The percentage of failed AOL connection attempts in the evening was 80% in January 1997 (shortly after the switch); even after boosting equipment, the failure rate was 16% in December 1997. If you succeed in getting a connection, it is almost impossible to fight your way through intrusive pop-up ads ads to useful content. The original AOL pricing scheme was wrong too: charging by the minute places considerable stress on users and is less fair than charging by the number of pages that are consumed.
</p>
<h2>
 Payment Usability
</h2>
<p>
 <em>
  John Brewer writes (Bruce Onder proposed a similar idea in a later email):
 </em>
</p>
<blockquote>
 I liked your micropayment UI ideas, and would like to propose one of my own:
 <p>
  The browser window contains a &quot;money&quot; thermometer that represents a small amount of money (say $1 or $10) that the user has pre-authorized the browser to spend on his behalf. This thermometer would work like the &quot;life force&quot; thermometers in video games, starting out green in color, then turning yellow, and finally red. Once the thermometer runs out, your browser wouldn&#39;t take you to pay pages until you refill it. The refilling action could be as simple as double-clicking on the thermometer. In conjunction with the thermometer, you could have sound effects ranging from a short, quiet penny &quot;clink&quot; for under-a-cent transactions, to an old-style mechanical cash register &quot;ka-CHUNG&quot; for more expensive hits.
 </p>
 <p>
  I propose this interface because my biggest concern isn&#39;t individual hits, but how they add up over time. The thermometer would give me graphical feedback on how much money I&#39;ve spent lately.
 </p>
 <p>
  This interface would also address my other big concern, which is unscrupulous operators using JavaScript or similar technology to trick my browser into paying for additional pages. According to Salon, adult sites are already using tricks like this to up their ad impressions and referral fees. Under my UI scheme, if someone tried this:
 </p>
 <ol>
  <li>
   The maximum loss would be limited to the (small) amount of money currently in the thermometer.
  </li>
  <li>
   The constantly shrinking thermometer would indicate that something was amiss.
  </li>
  <li>
   The continuous &quot;clink, clink, clink&quot; would further serve to alert the user that money was being spent.
  </li>
 </ol>
</blockquote>
<p>
 <em>
  <a href="http://www.holzem.de/" title="Consultancy in Visual Design and System Analysis">
   Markus Holzem
  </a>
  writes:
 </em>
</p>
<blockquote>
 Now I&#39;ve just read your article about micropayments and am wondering whether this scheme is really necessary. I&#39;ve found two separate providers - one for Internet access and one hosting email, www, and ftp. I&#39;ll have to pay about $75 per month for
 <ul>
  <li>
   unlimited Internet access for reading and download
  </li>
  <li>
   own domain www.something.de
  </li>
  <li>
   20 MB space for WWW and FTP
  </li>
  <li>
   6 GB transfer from my FTP- and WEB-site
  </li>
  <li>
   $5 per 300 MB over 6 GB.
  </li>
  <li>
   no limits to commercial use
  </li>
 </ul>
 That are the most interesting values - and everything for $75.
 <p>
  Is it really necessary to charge for a site this cheap? Even if I set up tutorials for GUI-design and cross platform programming which are likely to be frequently accessed I don&#39;t think that a mostly text based WEB-site will be too expensive.
 </p>
 <p>
  And a further thought comes to my mind:
 </p>
 <p>
  Currently I&#39;m still a modem user moving to ISDN in about 2 weeks. If I access a page with download times more than 5-10 seconds I stop the transfer. Will this page be charged? How can I now, whether I have to wait for years to get the information I paid? A friend of mine needed more than half a day to download the trial version of WinCE (they didn&#39;t say it was 90MB large) and if I think I had to pay say $10 I&#39;d feel robbed. And it wasn&#39;t worth the money...
 </p>
 <p>
  Links are often outdated and I don&#39;t think, that all people providing links will scan them whether some of them will cost the reader a fee. Wouldn&#39;t even micropayments discourage the use of the WEB?
 </p>
</blockquote>
<p>
 <em>
  Jakob&#39;s reply:
 </em>
 The charges are not so much to pay for the hardware as to pay for the human time needed to develop a comprehensive website. Yes, people will write small sites for free because they like doing so, and such pages may continue to be free. But you cannot run a big site in your spare time.
 <br />
 An outdated link would not incur a charge if the user got a 404 error message instead of real content. If the user did get a page (just not a relevant one), then the user would have to pay and would have wasted a cent or so. If it happens once, then no big deal. If it happens several times, then the user would stop using the site and would give it a low rating in the
 <a href="../the-reputation-manager/index.php" title="Alertbox Feb. 1998: Reputation management">
  reputation manager
 </a>
 , thus warning other users. Once links become worth money, sites will have a large incentive to keep them updated; they will also have more of an incentive not to aggravate users since they are the paying customers.
</p>
<h2>
 International Payments
</h2>
<p>
 <em>
  Vladimir Menkov writes:
 </em>
</p>
<blockquote>
 I think you may find it amusing that the &quot;micropayment&quot; model you advocate appears to be similar to the system that has long been common in Russia with respect to many Internet-related services. For example, while a typical US Internet service provider would charge you a flat monthly fee regardless of how much time you have spent on-line or how much e-mail you have saved or received, a provider in Russia would usually bill its customers a dollar or so for each hour of on-line time (i.e. almost at the level of your &quot;less than a cent per minute and people use as much as they need&quot;), or (in case of a UUCP-only service) a few cents per kilobyte of mail, on top of the a-few-dollars-a-month monthly subscription fee.
 <p>
  Similarly, it is common for a newspaper site, a legal database site, or a general-information (classified etc.) site in Russia to charge either a small (a few $$ a month) subscription fee plus a per page fee, or a per page fee. Sometimes it is a per-kilobyte rather than per-page fee. It is common to allow non-paying customers to view tables of contents and to do site search, but one needs to pay to access the full text of the selected document.
 </p>
 <p>
  Of course, the actual payment mechanism is a technological problem, since credit cards are not common in the country, and electronic-money things like Digicash are still in their infancy. The common solution used in Russia is essentially the same used by New York City subway, campus copy-machine operators, or many phone companies, i.e. prepayment: once you find a site useful, you register and send the content provider some $20-40 as your advance, to be credited to your account. Your account is debited as you use the company&#39;s services, and you need to add money to it so that you don&#39;t overdraw it.
 </p>
 <p>
  Remembering that &quot;
  <a href="../the-telephone-is-the-best-metaphor-for-the-web/index.php" title="Alertbox May 1997">
   The Telephone is the Best Metaphor for the Web
  </a>
  &quot; (according to another of your columns), we should not be surprised that it works the other way too, and the mainstream Russian phone companies jump on the pay-as-you go bandwagon, planning to introduce a penny-a-minute or so charges for
  <em>
   local
  </em>
  (voice or data) calls above some threshold (15-20 hours a months), in addition to the traditional per-month charge. Actually they&#39;ve been trying to do it for some 20 years, but now they seem to be more confident in their ultimate success. No surprise, the main protesters against the new rate proposals are not those talkative lonely little ladies, but rather local Internet users. The telcos, of course say: &quot;we just do it the way advanced Western countries do&quot; (referring to much of Western Europe, I suppose), while the consumers bring about the US example :-)
 </p>
</blockquote>
<p>
 <em>
  John Walters writes:
 </em>
</p>
<blockquote>
 I fully agree with you on the subject of micropayments and anyone who can get a workable system up and running should do very well. However there will have to be some sensible way of overcoming foreign currency movements so that non US-based web users will not feel they are being disadvantaged. I think that a reasonably sophisticated and not too greedy bank will need to be behind the system for it to work properly globally.
 <p>
  Incidentally, on the subject of advertisements I remember when I lived in Italy in the early 1960&#39;s (yes, some of us are that old) on the national TV channel, RAI, they started the TV transmission about 8 pm and they had a solid 20 minutes or so of advertisements and then only commenced normal programming uninterrupted by advertising. So the ad&#39;s had to be good or people simply wouldn&#39;t bother to watch them. Maybe if the web advertisers want people to click on their ad&#39;s they had better do something about the content to make them worth reading.
 </p>
 <p>
  Thanks again for your column, keep it going even if it does mean paying a micropayment!
 </p>
</blockquote>
<p>
 <em>
  Bernard McCartan writes:
 </em>
</p>
<blockquote>
 Your views on payment for web page access doesn&#39;t take into account one important factor which operates in most countries outside the US, payment for local telephone calls. I pay 11.5 pence (about 16 US cents) for each three minutes that I am online. This &quot;micropayment&quot; mounts up rapidly. My employer, a dental teaching hospital, probably has a large(ish) number of its staff online at any time. Frequently staff close their browsers in the evening but forget to hang up; although the evening rate for local calls is 11.5 pence for 15 minutes, a few nights of an unattended PC online costs us quite a lot. It is unlikely that European countries will adopt free local calls. The demography of Europe means that there are fewer long distance calls and the phone companies must get their micropayments somehow.
</blockquote>
<p>
 <em>
  Jakob&#39;s reply:
 </em>
 European phone rates are definitely too high for Internet use: it doesn&#39;t make sense to charge by the minute and phone companies will either have to lower their rates or be driven out of business. But I do think that it will be necessary to charge by the megabyte (i.e., for the size of downloads) and it is likely that American users will have to start paying for their resource consumption along similar lines. Currently, websites have to pay between 1 and 8 cents for each megabyte they send out on the Internet, and it would be reasonable to charge users a similar amount for the burden they place on the infrastructure through their downloads. Web pages should be
 <a class="old" href="../size-limits-for-web-pages/index.php" title="Sidebar: recommended size limits for Web pages at different bandwidths">
  less than 30 kilobytes
 </a>
 (while bandwidth remains scarce), so even at the high rate, physical download charges would be a quarter of a cent. The micropayments discussed in my column are intended to pay for content development and for the website&#39;s cost of uploading the bits to the Internet. All these costs are real and they will eventually have to be paid for the Internet to have a realistic business model.
</p>
<p>
 <em>
  Jonathan O&#39;Donnell writes:
 </em>
</p>
<blockquote>
 I was struck by the fact that your column on micropayments didn&#39;t mention international currency conversion costs. I know next to nothing about this issue, but I imagine that the difference between $A0.01 and $US0.01 will be vastly outweighed by the cost of converting $A0.01 to $US0.01.
 <p>
  Following your logic though, where there is demand, a service will follow. Presumably, in this case, it would be a conversion cost as a percentage of the amount, rather than a flat fee.
 </p>
 <p>
  I guess that this is another international usability issue.
 </p>
 <p>
  Anyway, thanks for the column. I would buy it anytime. :-)
 </p>
</blockquote>
<p>
 <em>
  Jakob&#39;s reply:
 </em>
 Most of the micropayment schemes use a payment service that sits in the middle between the user and the website: the user runs up a bill with the service and pays in local currency. A typical monthly bill would be around US$20 or the equivalent in the user&#39;s currency. The payment service would accumulate payments from all its users and forward to the receiving websites at regular intervals, possibly using an international clearinghouse in case of foreign currency transactions. Since everything would be fully automated, there would be essentially no overhead due to currency conversions.
</p>
</section>
  

  
  <section>
    
        
    
  </section>
  
  
  
  
  

  


  <section>
    <br />
    <p class="share"><b>Share <span class="hide-for-small-down">this article</span>:</b> <a href="https://twitter.com/intent/tweet?url=http://www.nngroup.com/articles/readers-comments-on-micropayments/&amp;text=Readers'%20Comments%20on%20Micropayments%20%20&amp;via=nngroup">Twitter</a> | <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=http://www.nngroup.com/articles/readers-comments-on-micropayments/&amp;title=Readers'%20Comments%20on%20Micropayments%20%20&amp;source=Nielsen%20Norman%20Group">LinkedIn</a> | <a href="https://plus.google.com/share?url=http://www.nngroup.com/articles/readers-comments-on-micropayments/">Google+</a> | <a href="mailto:?subject=NN/g Article: Readers&#39; Comments on Micropayments  &amp;body=http://www.nngroup.com/articles/readers-comments-on-micropayments/">Email</a></p>
    <hr />
  </section>

  <footer class="l-subsection">
    
    
    

    <div class="row show-for-small-down related-content">
      
      
      

      
      
      

      
      
      

      
      
      

      
      
      
    </div>

    <div class="hide-for-small-down row related-content">
      <div class="medium-6 columns">
        
        
        

        

      </div>
      <div class="medium-6 columns">
        
        
      </div>
    </div>
  </footer>
</article>

        </div>

      

    
  </div>
  <div class="row">
    
      <div class="column small-12 show-for-small-down l-navrail announcements">
        
          
        
      </div>
    
    
  </div>

    </div>
    




<footer id="page-footer">
    <div class="row hide-for-print">
        <div class="column small-12 medium-6">
            <section class="footer-popular">
                
                <h1>Popular Topics</h1>
                <div style="float: left;  width: 15em; display: block;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/applications/index.php">Application Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/e-commerce/index.php">E-Commerce Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/eyetracking/index.php">Eyetracking </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/information-architecture/index.php">Information Architecture </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/intranets/index.php">Intranet Design </a></li>
	<li><a href="../../topic/interaction-design/index.php">Interaction Design </a></li>
</ul>
</div>

<div style="float: left;  width: 15em; display: block; list-style: none;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/mobile-and-tablet-design/index.php">Mobile and Tablet Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/credibility-persuasion/index.php">Persuasive Design</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/research-methods/index.php">Research Methods</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/user-testing/index.php">User Testing </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/web-usability/index.php">Web Usability </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/writing-web/index.php">Web Writing &amp; Content Strategy </a></li>
</ul>
</div>

<p>&nbsp;</p>

                
            </section>
        </div>

        <div class="column small-12 medium-6">
            <div class="row">
                <div class="column small-6">
                    <section class="footer-about">
                        <h1>Follow Us</h1>
                        <ul>
	<li><a href="../subscribe/index.php">Subscribe to our weekly UX newsletter</a>,&nbsp;Jakob Nielsen&#39;s Alertbox</li>
	<li>Visit Don Norman&#39;s website, <a href="http://www.jnd.org/"> www.jnd.org </a></li>
	<li>Visit Bruce Tognazzini&#39;s website, <a href="http://www.asktog.com/"> www.asktog.com </a></li>
	<li style="padding-top: 6px;"><a class="twitter-follow-button" data-show-count="false" data-size="large" href="https://twitter.com/nngroup"><img alt="Follow @nngroup on Twitter" height="20" src="https://media.nngroup.com/media/editor/2016/12/30/twitter-follow-button.png" style="width: 117px; height: 20px;" width="117" /></a></li>
	<li style="padding-top: 6px;"><img alt="" height="14" src="https://media.nngroup.com/media/editor/2016/07/26/feed-icon-14x14.png" style="vertical-align:text-top;width:14px;margin-right:.25em;" width="14" /><a href="../../feed/rss/index.php">Subscribe to our RSS feed</a></li>
</ul>

                    </section>
                </div>

                <div class="column small-6">
                    <section class="footer-about right-column">
                        <h1>About
</h1>
                        <ul class="no-bullet">
	<li><a href="../../about/contact/index.php">Contact information</a></li>
	<li><a href="../../about/index.php">About Us</a></li>
	<li><a href="../../about/why-nng/index.php">Why NN/g? </a></li>
	<li><a href="../../return-policy/index.php">Return Policy</a></li>
</ul>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="copyright column small-12">
            <p><a href="../../copyright-and-reprint-info/index.php">Copyright</a> &copy; 1998-2017 Nielsen Norman Group, All Rights Reserved.</p>
        </div>
    </div>
</footer>


    


        <script src="https://media.nngroup.com/static/vendor/fastclick/js/fastclick.min.js"></script>
        <script src="https://media.nngroup.com/static/vendor/foundation/js/foundation.min.js"></script>
        <script src="http://media.nngroup.com/static/js/app.js"></script>
        <script src="http://media.nngroup.com/static/js/scroll-to-fixed.js"></script><script>var externalLinks = document.querySelectorAll('a[href^="http"]');for (var i = externalLinks.length-1; i >= 0; i--) {externalLinks[i].addEventListener("click", function(event) { alert("External links are disabled for this test."); event.preventDefault(); }, false);}var mailLinks = document.querySelectorAll('a[href^="mailto"]');for (var i = mailLinks.length-1; i >= 0; i--) {mailLinks[i].addEventListener("click", function(event) { alert("Email links are disabled for this test."); event.preventDefault(); }, false);}var externalForms = document.querySelectorAll('form');for (var i = externalForms.length-1; i >= 0; i--) {externalForms[i].addEventListener("submit", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false); externalForms[i].addEventListener("click", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false);}</script>

        <script>
            
            $(function() {
                $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
                $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
            });
        </script>

        
<script src="https://media.nngroup.com/static/js/articles.js"></script>


        
        <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1072699960;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072699960/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
    </body>

<!-- Mirrored from www.nngroup.com/articles/readers-comments-on-micropayments/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:17:26 GMT -->
</html>
