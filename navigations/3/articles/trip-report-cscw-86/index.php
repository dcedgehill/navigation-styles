<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->





    
<!-- Mirrored from www.nngroup.com/articles/trip-report-cscw-86/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:14:37 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1026.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"beacon":"bam.nr-data.net","queueTime":4,"applicationTime":1174,"agent":"","licenseKey":"1a30ef9931","errorBeacon":"bam.nr-data.net","applicationID":"2048992","transactionName":"NFxSMRBUX0EDUkIPDA0WdhAMVkVbDV8ZBxEXUFMJB0YfRAtUQRVZIktEDAFZVHYHRVcPD01eVRE="}</script>
        <title>CSCW&#39;86 Trip Report: Article by Jakob Nielsen</title><meta property="og:title" content="CSCW&#39;86 Trip Report: Article by Jakob Nielsen" />
  
        
        <meta name="description" content="Jakob Nielsen&#39;s trip report for the 1986 Computer-supported Cooperative Work conference (CSCW &#39;86).">
        <meta property="og:description" content="Jakob Nielsen&#39;s trip report for the 1986 Computer-supported Cooperative Work conference (CSCW &#39;86)." />
        
  
        
	
        

        

        <meta name="robots" content="noarchive">
        <meta name="viewport" content="width=device-width">

        <link rel="search" type="application/opensearchdescription+xml" title="NN/g Search" href="../../opensearch.xml" />
        <link rel="shortcut icon" href="http://media.nngroup.com/static/img/favicon.ico" />

        <link rel="stylesheet" href="https://media.nngroup.com/static/vendor/font-awesome/css/font-awesome.min.css">

        
        <link rel="stylesheet" href="https://media.nngroup.com/static/css/7232a960f1df.css" type="text/css" />
        <link rel="stylesheet" href="http://fast.fonts.com/t/1.css?apiType=css&amp;projectid=c5846260-9def-46ec-9407-0f1349bd2d64" type="text/css">

        <script src="https://media.nngroup.com/static/vendor/modernizr/js/modernizr.min.js"></script>

        <script type="text/javascript" src="https://media.nngroup.com/static/js/3283be5def74.js"></script>
        

        

        
        
        <!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=229034,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('http://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->


<!-- Google Analytics Tracking Code for http://nngroup.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23132681-1', 'nngroup.com');
  ga('require', 'linkid', 'https://www.nngroup.com/articles/trip-report-cscw-86/linkid.js');
  ga('require', 'displayfeatures');

// Optimizely Universal Analytics Integration
 window.optimizely = window.optimizely || [];
 window.optimizely.push("activateUniversalAnalytics");

  ga('send', 'pageview');

</script>
<script>
/**
* Function to track a click on an outbound link in Google Analytics.
*/
function trackOutboundLink(label, url) {
    if(window.ga && ga.loaded) {
        ga('send', 'event', 'outbound', 'click', label, {'hitCallback': function(){document.location = url;}
        });
    } else {
        document.location = url;
    }
}
/**
* Function to track a click on a link in the article announcement area.
*/
$('#articleAnnouncement a').on('click', function(){
  ga('send', 'event', 'link', 'click', 'articleAnnouncement');
});
</script>

<script type="text/javascript">
 setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2421.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



<!-- Hotjar Tracking Code for http://nngroup.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:121605,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

        <script src="https://media.nngroup.com/static/js/analytics/track_outbound_link.js"></script>
        
        
        
    </head>
    <body class="location-articles articles-detail">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
    <header id="page-header">
        <div id="cart" class="shopping-cart mobile-cart show-for-small-down"></div>
        <div class="row">
            <div id="masthead" class="masthead small-12 medium-7 column">

                <!-- The button to toggles menu on small screen -->

                <h1><a href="../../index.php"><img src="http://media.nngroup.com/static/img/logo-wordmark.png" width="74" height="30"> Nielsen <span class="norman">Norman</span> Group</a></h1>
                <p class="tagline show-for-medium-up">Evidence-Based User Experience Research, Training, and Consulting</p>

                <!-- The button to toggles form search on small screen -->


                <div style="clear: right;width:  100%;height: 1px;"></div><?php define('ROOT_PATH',$_SERVER["DOCUMENT_ROOT"] . '/apps/navigation_styles/navigations/3'); ?><?php include(ROOT_PATH)."/navigation.php"; ?>

                <!-- Drop down menu on small screen -->
                <div id="hideNav" class="show-for-small-down">
                  
                  <ul class="menu-content">
                    <li><a href="../../index.php">Home</a></li>
                    <li class="has-submenu">
                      <a>Training</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../training/index.php">Overview</a></li>
                        
<li id="nav-ux-conference"><a href="../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../online-seminars/index.php"><span>Online Seminars</span></a></li>

                      </ul>
                    </li>
                    <li class="has-submenu">
                      <a>Consulting</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
  <li><a href="../../consulting/index.php">Overview</a></li>
  
  <li><a href="../../consulting/benchmarking/index.php">Benchmarking</a></li>
  
  <li><a href="../../consulting/expert-design-review/index.php">Expert Design Review</a></li>
  
  <li><a href="../../consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
  
  <li><a href="../../consulting/usability-testing/index.php">Usability Testing </a></li>
  
  <li><a href="../../consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
  
  <li><a href="../../in-house-training/index.php">In-House Training</a></li>
</ul>

                    </li>

                    <li><a href="../../reports/index.php">Reports</a></li>
                    <li><a href="../index.php">Articles</a></li>
                    <li class="has-submenu"><a>About NN/G</a><i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../about/index.php">Overview</a></li>
                        <li><a href="../../people/index.php">People</a></li>
                        <li><a href="../../about/why-nng/index.php">Why NN/g?</a></li>
                        <li><a href="../../about/contact/index.php">Contact</a></li>
                        <li><a href="../../news/index.php">News</a></li>
                        <li><a href="../../about/history/index.php">History</a></li>
                        <li><a href="../../books/index.php">Books</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                </div>

                <hr class="header-divider show-for-small-only"/>


            </div>
       </div>

        <div id="hideNav">            <nav class="primary-nav small-12 column">
                <ul>
                    <li><a class="navitem" id="nav-home" href="../../index.php">Home</a></li>
                    <li><a class="navitem" id="nav-training" href="../../training/index.php">Training</a></li>
                    <li><a class="navitem" id="nav-consulting" href="../../consulting/index.php">Consulting</a></li>
                    <li><a class="navitem" id="nav-reports" href="../../reports/index.php">Reports</a></li>
                    <li><a class="navitem" id="nav-articles" href="../index.php">Articles</a></li>
                    <li><a class="navitem" id="nav-about" href="../../about/index.php">About NN/G</a></li>
                </ul>
            </nav>
            <nav class="secondary-nav small-12 medium-12 large-12 columns">
                
                
            </nav>
        </div>
    </header>
    <div id="mainContent" class="l-content">
    
  

  

  <div class="row">
    
      
      <section class="small-12 medium-3 columns collapsable-content l-navrail articles-left-column">
        <h1 class="show-for-small-down collapse-link mobile-browse-header">Browse by Topic and Author</h1>
          <div class="row collapsable">

            
            <div class="small-6 medium-12 columns">
              <section id="article-popular-topics" style="display: none;">
                
                <h1>Topics</h1>
                <ul id="article-popular-topics">
                    
                    <li>
                        
                        <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/intranets/all/index.php">Intranets</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/user-testing/all/index.php">User Testing</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                        
                    </li>
                    
                    <li><a href="#" id="article-see-all-topics"><strong>See all topics&hellip;</strong></a></li>
                </ul>
                
              </section>
              <section id="article-all-topics" style="display: none;">
                
                <h1>
                  <span class="show-for-medium-up">All Article Topics</span>
                  <span class="show-for-small-down">All Topics</span>
                  (<a href="#" id="article-hide-all-topics">hide</a>)
                </h1>
                <ul>
                  
                  <li>
                    
                    <a href="../../topic/accessibility/all/index.php">Accessibility</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/agile/all/index.php">Agile</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/analytics-and-metrics/all/index.php">Analytics &amp; Metrics</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/applications/all/index.php">Application Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/b2b-websites/all/index.php">B2B Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/behavior-patterns/all/index.php">Behavior Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/branding/all/index.php">Branding</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/content-strategy/all/index.php">Content Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/corporate-websites/all/index.php">Corporate Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/design-patterns/all/index.php">Design Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-design-process/all/index.php">Design Process</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/email/all/index.php">Email</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/eyetracking/all/index.php">Eyetracking</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/heuristic-evaluation/all/index.php">Heuristic Evaluation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/human-computer-interaction/all/index.php">Human Computer Interaction</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-ideation/all/index.php">Ideation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/information-architecture/all/index.php">Information Architecture</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/interaction-design/all/index.php">Interaction Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/international-users/all/index.php">International Users</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/intranets/all/index.php">Intranets</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-management/all/index.php">Management</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/navigation/all/index.php">Navigation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/non-profit-websites/all/index.php">Non-Profit Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/personas/all/index.php">Personas</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/persuasive-design/all/index.php">Persuasive Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/prototyping/all/index.php">Prototyping</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/psychology-and-ux/all/index.php">Psychology and UX</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/research-methods/all/index.php">Research Methods</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/search/all/index.php">Search</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/social-media/all/index.php">Social Media</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/strategy/all/index.php">Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/user-testing/all/index.php">User Testing</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-teams/all/index.php">UX Teams</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/visual-design/all/index.php">Visual Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/writing-web/all/index.php">Writing for the Web</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/young-users/all/index.php">Young Users</a>
                    
                  </li>
                  
                </ul>
                
              </section>
            </div>
            <script>
              /* Determine which topics list will be displayed: the full or the
               * partial.  We are inlining this close to the list itself to
               * prevent rendering delays on the client
               */
              var topic_list = $.cookie("article-topics");

              var popular_topics = $('#article-popular-topics');
              var all_topics = $('#article-all-topics');

              if(topic_list == 1) {
                  popular_topics.hide();
                  all_topics.show();
              } else {
                  popular_topics.show();
                  all_topics.hide();
              }
            </script>
            

            
            <div class="small-6 medium-12 columns">
              
              <section id="article-initial-authors">
                  <h1>Author</h1>
                  <ul>
                    
                    <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                    
                    <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                    
                    <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                    
                    <li><a href="#" onClick="$('#article-initial-authors').hide(); $('#article-all-authors').show();"><strong>See all authors&hellip;</strong></a></li>
                  </ul>
              </section>
              <section id="article-all-authors" style="display: none;">
                <h1>
                  All Authors
                  (<a href="#" id="article-hide-all-authors">hide</a>)
                </h1>
                <ul>
                  
                  <li><a href="../author/raluca-budiu/index.php">Raluca Budiu</a></li>
                  
                  <li><a href="../author/patty-caya/index.php">Patty Caya</a></li>
                  
                  <li><a href="../author/susan-farrell/index.php">Susan Farrell</a></li>
                  
                  <li><a href="../author/therese-fessenden/index.php">Therese Fessenden</a></li>
                  
                  <li><a href="../author/kim-flaherty/index.php">Kim Flaherty</a></li>
                  
                  <li><a href="../author/sarah-gibbons/index.php">Sarah Gibbons</a></li>
                  
                  <li><a href="../author/aurora-harley/index.php">Aurora Harley</a></li>
                  
                  <li><a href="../author/kate-kaplan/index.php">Kate Kaplan</a></li>
                  
                  <li><a href="../author/page-laubheimer/index.php">Page Laubheimer</a></li>
                  
                  <li><a href="../author/angela-li/index.php">Angie Li</a></li>
                  
                  <li><a href="../author/hoa-loranger/index.php">Hoa Loranger</a></li>
                  
                  <li><a href="../author/kate-meyer/index.php">Kate Meyer</a></li>
                  
                  <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                  
                  <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                  
                  <li><a href="../author/kara-pernice/index.php">Kara Pernice</a></li>
                  
                  <li><a href="../author/christian-rohrer/index.php">Christian Rohrer</a></li>
                  
                  <li><a href="../author/amy-schade/index.php">Amy Schade</a></li>
                  
                  <li><a href="../author/katie-sherwin/index.php">Katie Sherwin</a></li>
                  
                  <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                  
                  <li><a href="../author/kathryn-whitenton/index.php">Kathryn Whitenton</a></li>
                  
                </ul>
              </section>
              
            </div>
            
          </div>

          
          <section class="hide-for-small-down">
            
              
              <h1>Recent Articles</h1>
              
              <ul>
                  
                  <li><a href="../seamless-cross-channel/index.php">Seamlessness in the Omnichannel User Experience</a></li>
                  
                  <li><a href="../brand-intention-interpretation/index.php">Experience Design: Bridging Brand Intention and Brand Interpretation</a></li>
                  
                  <li><a href="../eyetracking-tasks-efficient-scanning/index.php">Scanning Patterns on the Web Are Optimized for the Current Task</a></li>
                  
                  <li><a href="../flat-design-best-practices/index.php">Flat-Design Best Practices</a></li>
                  
                  <li><a href="../ux-user-stories/index.php">Accounting for UX Work with User Stories in Agile Projects</a></li>
                  
                  <li><a href="../index.php"><strong>See all articles&hellip;</strong></a></li>
              </ul>
              
            
          </section>
          

          
          
          <section class="hide-for-small-down">
            <h1>Popular Articles</h1>
            
            <ul>
                
                <li><a href="../usability-101-introduction-to-usability/index.php">Usability 101: Introduction to Usability</a></li>
                
                <li><a href="../top-10-mistakes-web-design/index.php">Top 10 Mistakes in Web Design</a></li>
                
                <li><a href="../how-users-read-on-the-web/index.php">How Users Read on the Web</a></li>
                
                <li><a href="../f-shaped-pattern-reading-web-content/index.php">F-Shaped Pattern For Reading Web Content</a></li>
                
                <li><a href="../ten-usability-heuristics/index.php">10 Usability Heuristics for User Interface Design</a></li>
                
                <li><a href="../intranet-design-2016/index.php">10 Best Intranets of 2016</a></li>
                
                <li><a href="../which-ux-research-methods/index.php">When to Use Which User-Experience Research Methods</a></li>
                
                <li><a href="../response-times-3-important-limits/index.php">Response Times: The 3 Important Limits</a></li>
                
                <li><a href="../why-you-only-need-to-test-with-5-users/index.php">Why You Only Need to Test with 5 Users</a></li>
                
                <li><a href="../page-fold-manifesto/index.php">The Fold Manifesto: Why the Page Fold Still Matters</a></li>
                
            </ul>
          </section>
          
          

          
            <section class="hide-for-small-down">
              
            </section>
          

      </section>
      

      
        <div class="small-12 medium-9 columns content-body">
          




<article class="l-subsection alertbox-article-content article-detail-bl">
  
  <header>
    <div class="l-subsection">
      <h1>CSCW&#39;86 Trip Report</h1>
      <div class="author-meta">by
        
          
            
            <a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a>
            
          
        on  December 31, 1986
        
      </div>

      

      
      <div class="topic-links">
        Topics:
        
          <ul>

  <li><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction</a></li>

</ul>
        
      </div>
      
    </div>
  </header>
  

  
  
  

  
  <section id="article-body"><p>Austin, TX, 3-5 December 1986<br/>
 </p>

<p>At CHI'86 it was a general impression that the area of several people working together using computers were starting to become the next hot topic as a research area. At the same time the European Community is considering changing the focus of the human factors part of the next ESPRIT research program from the so-called <strong> human factors-1 </strong> (interaction between a single human and a single computer) to <strong> human factors-2 </strong> (interaction between several humans and several computers).</p>

<p>On the basis of this atmosphere of mounting excitement, I participated in the conference on Computer-Supported Cooperative Work (abbreviated CSCW in the rest of this report) in Austin, Texas 3-5 December 1986. The conference was sponsored by MCC, the Microelectronics and Computer Technology Corporation which is a commercial research center established as a collaborative effort (!) by several large US companies to avoid the fate of being overtaken by the Japanese Fifth Generation project.</p>

<p>Unfortunately MCC had not organized an "open house" event as was done by ICOT at the <a href="../trip-report-fifth-generation/index.php"> Fifth Generation conference</a> in Tokyo in 1984. But anyway I got the impression that MCC by now has achieved the status of a dynamic research environment producing novel results. Frankly, I had not been that impressed with MCC's results before attending this conference, but I just guess that it takes some time to get up to speed for a new research center (Rome was not built in one day...).</p>

<p>The 293 conference came mostly from the United States, even though there was significant participation from other parts of the world also (especially from Europe). The European participants were mainly from Scandinavia, the United Kingdom, and Italy, while the Asiatic participants were mainly from Japan. The distribution of participants' affiliation showed a very large number of participants from MCC (the company organizing the conference) and a rather large number from Xerox, while IBM only had a few participants at this conference even though it is normally strongly represented at human factors conferences.</p>

<p>My general impression of the conference was that there was a pervasive "pioneering spirit": Everybody was very excited about participating in this new field. However, it must also be admitted that the conference showed that CSCW is not a very "cultivated" field at the moment. Most results presented at the conference were preliminary in one sense or another. Also, in some sense the field is not that new: As the program chair, Irene Greif, noted in her opening remark, Doug <strong> Engelbart's augmentation project </strong> 20 years ago had included a lot of early work on cooperative work. Some of his inventions such as windowing systems and the mouse had "made it" out to the rest of the world while other ideas such as the computerized conference room had not.</p>

<p>In many cases, people had built neat systems and showed very impressing videotapes, but they did not know whether their systems in fact supported (i.e. helped/facilitated) cooperative work, as the systems had not been used for such work (or at best had been used by the system's authors themselves in their own project meetings). In other cases, people did present empirical studies of real subjects working together, but it was often difficult to relate those results to how new potential computer systems could improve the situation.</p>

<p>I do not intend to sound negative: There was a lot of interesting and good work presented, it was just not conclusive-which is a very natural situation at the first big conference in a new field.</p>

<p>The next CSCW conference is planned for September 1988 with Irene Greif from MIT as conference chair and Lucy Suchman from Xerox as program chair. One experienced conference organizer told me that she feared that the second conference would be at the opposite extreme from this conference: A lot of (too) detailed empirical evidence, but no excitement about improving the world. And then one might hope for a synthesis at the third conference.</p>

<h2>Definition of CSCW</h2>

<p>In his opening remarks, the conference chair, Herb Krasner from MCC gave a brief overview of the spectrum of different types of CSCW:</p>

<ul>
	<li>Different <strong> kinds of work </strong> being performed by the group:

	<ul>
		<li>authoring</li>
		<li>research</li>
		<li>design</li>
		<li>office work</li>
	</ul>
	</li>
	<li>Different <strong> modes of cooperation</strong>:
	<ul>
		<li>size of group</li>
		<li>organization of group</li>
		<li>level of synergy (are they just sharing interests or working on the same project?)</li>
		<li>communication patterns</li>
		<li>time (real-time or time-delayed communication)</li>
		<li>space constraints on interaction between group members (there is e.g. a great difference between supporting people who are physically present in the same room and supporting people in different locations).</li>
	</ul>
	</li>
	<li>Types of <strong> computer support</strong>:
	<ul>
		<li>passive/active.</li>
		<li>intelligence.</li>
		<li>level of aid offered.</li>
	</ul>
	</li>
</ul>

<p>Many different types of computer systems were talked about at the conference:</p>

<ul>
	<li>Simple electronic mail, perhaps augmented by some kind of structure (e.g. The Coordinator) or filter to avoid information overload (e.g. Malone's Information Lens).</li>
	<li>Computer conferencing systems.</li>
	<li>System prototyping tools to help designers collaborate (e.g. Trillium from Xerox).</li>
	<li>Electronic meeting rooms to support face-to-face meetings of different kinds-e.g. brainstorming sessions (Colab from Xerox), project review meetings (NICK from MCC), or group decision making (Plexsys from Harvard).</li>
	<li>Construction/authoring of a coherent set of overheads for a talk (Cognoter from Xerox).</li>
	<li>Information sharing among research collaborators (NoteCards from Xerox).</li>
</ul>

<h2>When Is CSCW No Good?</h2>

<p>We should of course realize that computer-support is not necessarily always good for collaboration. Ted Nelson claimed that a good feature of non-bureaucratic organizations is the deniability for everyone: You are not controlled too much-i.e. the computer does not record everything. He asked whether we risked creating just the kind of non-deniable organizations where we as scientists would not want to work.</p>

<p>Julian Orr from Xerox PARC told of his studies of "narratives at work": It turns out that Xerox repair technicians tell each other a lot of "war stories" in the form of anecdotes of difficult repair situations. This has been discouraged as a waste of time by some managers, but these anecdotes are actually useful for the other techs who take these experiences and try to apply them to solving their present problem. The point is that having a casual attitude is important to telling war stories, so that it is doubtful whether one could introduce a computerized system to support the distribution and use of these anecdotes.</p>

<p>Robert Kraut from Bellcore had studied how scientists collaborate. His conclusions were that the really hard problems are the interpersonal relationships-and it is not clear that computers can help here. Scientists working together really have two major goals: 1) Accomplishing what they set out to do. 2) Keeping a good relationship. In some cases these collaborations take the form of partnerships over 30 years. The central problem (and the major source of complaints in 50 interviews with scientists) is equitable division of labor.</p>

<h2>The Computerized Meeting</h2>

<p>Many different systems for computer-filled meeting rooms were presented at the conference. In most cases there was a workstation for each meeting participant and in some cases there was also a wall-sized "electronic blackboard" projection screen. The problem with these electronic meeting rooms was that they ended up looking like old-fashioned university terminal rooms instead of a meeting room. For instance it would often be the case that the physical size of a Lisp-machine would make it impossible or inconvenient to look other meeting participants in the eye.</p>

<p>Actually one of the conference participants came from one of the major U.S. manufacturers of office furniture. I talked with him during one of the breaks and learned that they are trying to design furniture and computers to fit each other so that the end result would be ergonomic environments (and not just ergonomic individual components). Maybe they will succeed but at the moment you have to be a computer freak to enjoy taking part in a meeting in the electronic meeting rooms.</p>

<p>Project NICK from MCC was presented by Clarence ("Skip") Ellis. They used both personal computers for meeting participants and an electronic blackboard which was controlled by a special person: The facilitator who is not part of the group having the meeting but who tries to keep the meeting on the right track. The blackboard can show a (dynamically changing) agenda and several special meters: Yes/No meters of participant agreement with proposals and "mood" meters showing whether the audience thinks that the speaker is boring/presenting great stuff, etc. Some of these ideas sound interesting, but I am not sure how I would like to be the speaker greeted with a mood meter reading 100% "boring."</p>

<p>There was no single paper explicitly describing the Xerox PARC <strong> Colab</strong>; but several papers referred to this exciting environment. It is an experimental meeting room designed for small working groups of two to six people using Xerox workstations connected over a network. Colab is intended to bring the (hoped for) benefits of office automation to people in the situation when they are in meetings away from their office (indeed, office workers may spend 30-70% of their time in meetings).</p>

<p>Gregg Foster talked about the <strong> Cognoter </strong> system which is one of the tools available at the Colab. It is a meeting tool for structuring and preparing presentations (i.e. talks to other people than the meeting participants) using a 3-phase brainstorm model:</p>

<ol>
	<li>Uncritical generation of ideas for things to include in the presentation</li>
	<li>Organization of the ideas to a linear order</li>
	<li>Pruning ideas and transferring them to text. This kind of tool may of course also be used as an idea processor by a single person and actually it had been used more as such than as a meeting tool.</li>
</ol>

<p>Foster related some experience from the use of Cognoter as a collaboration tool: There seemed to be an ebb and flow of parallelism (5-10 minutes cycles) where people sometimes worked together on the same information and sometimes worked individually on refining different pieces of the total picture. They had the problem of cluttered screens when several people contended for the same "real estate" and they also had to deal with the traditional concurrency problem of multiple accesses to the same data: They just locked objects with a busy signal-this was not a problem as the grain size of the objects of information was small and each interaction tended to be short.</p>

<p>They had performed an experiment comparing generation of presentations on paper and using Cognoter. The results were judged to be about equal in quality but the subjects using Cognoter had had to spend most of their time just learning the system and there is thus hope that Cognoter could result in actual improvements for more experienced users.</p>

<p>An interesting side effect of the Colab was that it has a video switch allowing any display to be slaved to another so that it will show exactly the same. This facility was put in to enable demonstrations but it turned out that it was actually used by the users of Cognoter to "look each other over the shoulder."</p>

<p>The situation of users seeing exactly the same as other users in Colab was discussed in more detail by Mark Stefik in a paper on the so-called <strong> WYSIWIS </strong> (= What You See Is What I See; though a member of the audience comment ed: Nobody ever <em> sees </em> exactly what I see. It is not just a question of the pixels displayed but also of how you <em> look </em> at them.). They started out having this feature but gradually found that it was not so desirable. Stefik defined strict WYSIWIS as having all wiews identical-including having all displays show all cursors: But it turned out to be too distracting with all the cursors zipping around so they went to tele-pointing instead: A user could request a special large tele-cursor which would then (and only then) be displayed on the other screens.</p>

<p>And again they found the real estate problem if the same size display had to be shared between several users at the same time. So instead they had tended to move to systems where they relaxed WYSIWIS to only apply to individual windows (and not to entire display screens). The new principle would be that if two people were seeing the same window (defined in some way) then they would also be seeing the same window contents. In addition to these "public" windows, the system also included "private" windows like notepads etc. which were not shared among users.</p>

<p>Two ways of solving the screen real estate problem with the new relaxed WYSIWIS are:</p>

<dl>
	<dt><strong>Stampsheets </strong></dt>
	<dd> </dd>
	<dd>Some windows are shrunk to icons ("stamps"). Only the windows in which the individual user has an interest at any given point in time are expanded to full size. Subgroups of users will then be implicit in the set of windows open at any one time. People who have approximately the same windows open will belong to the same subgroup since they will tend to be working together on the same problem.</dd>
	<dt><strong>Rooms </strong></dt>
	<dd> </dd>
	<dd>A room is a kind of "super-window" that takes up the entire display and contains a collection of ordinary windows. The idea is that you organize different rooms for different purposes (just like the rooms in a building) and then have "doors" (icons) in each room leading to other rooms. Subgroups are formed by having one room for each subgroup (since a user can only have one room displayed at any one time, the room displayed defines the subgroup to which this user belongs and the room approach tends to foster firmer subgroups than the stampsheet approach).</dd>
</dl>

<h2>Long-Distance Coffee Breaks</h2>

<p>The systems for computerized meetings described above might be helpful in the situation where people are able to get together physically in the same location. But another approach is needed in the situation where the group is distributed across several geographical locations.</p>

<p>Xerox has established a second site (<strong>PARC Northwest</strong>) of the PARC laboratory in Portland, Oregon which is more than 1,000 kilometers from the main PARC site in Palo Alto, California. George Goodman explained how they are trying to make sure that the NW people remain in tight collaboration with the PA people. First of all, they have established powerful electronic links between the two sites: Both audio, video, and an Ethernet extension, all running 24 hours/day. Video cameras and monitors are present in all offices (of course there has to be some convention of "knocking on the door" before establishing a video link to another office!). They have also tried to make the two sites look the same in the use of furniture etc.</p>

<p>The most interesting aspect of this video link is perhaps that they keep a constant video link open between a "common area" at each site. This means that people who just drop by to get a cup of coffee will see those of their colleagues who happen to be doing the same thing at the opposite site. And this will again encourage informal conversations which would not occur if the same people had had to establish an explicit video link between their respective offices. Ben Shneiderman commented that he was looking forward to systems offering other kinds of cooperative experiences besides just work, such as <strong> cooperative entertainment </strong> and play. Some such enjoyable opportunities have been available for a long time on e.g. the PLATO system where people in different locations can participate real time in the same game. Shneiderman said that about 10 % of the time in PLATO had been spent playing the cooperative game Empire.</p>

<p>The use of the video link has mostly been for brief conversations: 70% of all interactions lasted less than 5 minutes and 20% lasted between 5 and 30 minutes. The users do get a feeling of physical presence ("as if people were down the hall") because of the system but they still think that the overhead of starting a communication across sites is too high.</p>

<p>Even though there is a long distance between Portland and Palo Alto, it is a North-South one so the two cities are still in the same time zone. Now that Xerox is starting an EuroPARC laboratory in Cambridge, England under the leadership of Tom Moran, they will face much greater difficulties in establishing the same kind of collaboration in spite of the 8 hour time difference between California and Britain.</p>

<p>(Note added 1988: In fact, PARC Northwest was closed shortly before the CSCW'88 conference.)</p>

<h2>The Low Road</h2>

<p>Ben Shneiderman defined most of the systems presented at the conference as belonging to a "high road" approach requiring specialized, expensive equipment (the Xerox PARC system described above for video links between Palo Alto and Portland had a set-up cost of $ 60,000 per site for video compression and an additional $ 1,000 per office. And after that, renting the communication line costs $ 3,000 each month.). But some of the systems belonged to the "low road" approach, running on cheap standard hardware and software. Many electronic mail systems belonged to this class.</p>

<p>One really low road approach was presented by Tony Fanning and Bert Raphael from Hewlett-Packard. They had 2,000 people using a computer conferencing system via text-only terminals and phone-based X.25 access. One reason for not going to a more fancy system is that the pre-existing environment is hard to move at HP: They are 80,000 people at 100+ organizational units. So instead of trying to move the mountain they had chosen the simple goal of just getting people together and talk. One problem they hoped to alleviate was the fairly standard situation of a "council" (working group in charge of some problem): Normally it will meet once a month, having several people fly in from far away for a few hours of meeting. And nothing much would get done at the individual meetings.</p>

<p>So they tried to introduce an established conferencing system - CONFER developed at the University of Michigan. CONFER had been used successfully in many situations:</p>

<ul>
	<li>Task force efforts to achieve a specific goal - discussed on the system.</li>
	<li>Establishing contact between "lonely" people in common disciplines at scattered sites (e.g. 15 computer center managers at HP sites in East Asia had a conference). This gave rise to ongoing conferences rather than having a single goal as the task force conferences.</li>
	<li>Broad discussions of e.g. tips for using PCs. The problem here was to entice people to log on every day (often people logged on for the "entertainment value" of seeing what X had replied to Y's comment the day before). It was also a question of reaching a critical mass of conference participants to keep it moving. 20 active participants seemed to be the minimum.</li>
</ul>

<p>The primary problem was that of access: Modems etc. were simply too difficult (and if this is the case in a technologically advanced company as HP, imagine what the situation must be in other places). Conferences also were not very successful if the participants were selected by someone else to participate instead of volunteering.</p>

<p>Bonnie Johnson from Aetna Insurance told of yet another case of actual field use in a commercial company. In this case it was the Coordinator System which is a commercial available product. The basic idea was that communication constitutes collaboration, and therefore to support communication. The Coordinator is really an electronic mail system with some added features. The sender of a message can e.g. set a date for the expected reply to the message or completion of the action described in the message. And the system will then automatically keep track of these dates.</p>

<p>It seemed that The Coordinator was not really helpful in the idea generation phase ("it is a pain to type in all your ideas"). It helped somewhat in the evaluation phase for those people who had also used the system to generate those ideas being evaluated-but it did not help those people who had talked face-to-face when generating the ideas and only afterwards typed in a summary. And the system was very helpful in the action phase (where the ideas are carried out)-probably because project management is built into the messages.</p>

<p>Electronic mail was also the subject of a study by Martha Feldman from the University of Michigan. People have a lot of weak ties with other people with whom they do not work really closely but with whom they could still have communication (e.g., ties connecting groups which are themselves strongly tied internally). She studied a company which had had electronic mail for 11 years (and thus presumably had grown used to it). In a set of 1,249 messages, 574 were between people who did not know each other (and they estimated that 82% of these messages would not have been sent without electronic mail). 155 of the messages were between people who only communicate electronically.</p>

<p>At Rand their system had by accident for 1.5 years kept on collecting the headers of all messages sent in a log file. So now they had the following data for 69,000 messages: To, From, Copies, and Date/time-but not the subject. For privacy reasons they had recoded all the userids to random IDs (but they had kept information about organizational status, department, and geographical office location, thus probably compromising privacy anyway for a determined analyst). J.D. Eveland.D.; discussed a number of results on the basis of this large amount of fairly shallow data. It turned out that 20% of the senders generated 80% of the traffic. Most messages were sent between people who were geographically close to each other, even though you might have thought that electronic mail was especially useful for sending messages across long distances. It alto turned out that most messages were sent within project groups and not across project groups. In summary, mail usage tended to follow strong ties and not weak ties.</p>

<p>Eveland also presented "sociograms" of the closeness of different department within Rand on the basis of their cross-department mail usage. The specific sociograms are probably mostly of interest for Rand insiders but the principle of generating them on the basis of an extensive message log is probably of general validity.</p>

<h2>Hypertext</h2>

<p>Hypertext is the generalized footnote. The reader may have observed that I really like this non-sequential style of writing, and I have actually for a long time been interested in the hypertext concept. A really fine overview of current hypertext thinking and systems was distributed at the conference in the form of a report by Jeff Conklin (it was not presented as a talk and it is not part of the proceedings).</p>

<p>Mayer Schwartz from Tektronix defined Hypertext as follows: A document consists of a set of nodes and directed links between these nodes. Each node can be text, graphics, animations, etc. Hypertext is meant to be viewed and accessed interactively on a computer (i.e. a printout is not hypertext) and it should be possible for multiple people to access the document concurrently. Also he wanted some kind of version control in the system so that it would be possible to update some nodes and not others and having the system keep track of the status of the overall system (including perhaps keeping some of the old versions of the nodes). Version control would be especially important for software engineering use of hypertext as in their Neptune system which among other things is used to provide source code management. It is also used for a network news browser allowing annotations.</p>

<p><strong>NoteCards </strong> is a system from Xerox PARC which has been mostly used for individual work. It is a hypertext system in that each "card" may contain pointers to other cards. It is not a browsing system as many other hypertext systems where the user basically reads existing information. Instead it is an idea structuring system where the user is supposed to generate new cards and link them to an evolving structure. Randy Trigg discussed the (limited) experience with using NoteCards for collaborative work where two users were generating and editing cards in the same document data base. They introduced special history cards which recorded the changes made during a session with the system so that the other user later could see what had been done. They also adopted a convention where each of the two users used a different typefont when entering text into the cards. In other cases, they decided not to have any convention-e.g., for reshaping windows. Other conventions are rather primitive such as the "message box" which is just a special card which they use to post messages to each other.</p>

<p>These kinds of conventions result from procedural activities which are discussions about how to proceed with the work. The two other kinds of activities considered by Trigg were substantive (those that constitute the work itself) and annotative (about the work, including comments, critiques, and questions). Substantive activities are supported by many systems (even to some degree by a traditional text processor supplemented by printouts or electronic mail). Annotative activities lack support in traditional systems, but more and more systems include some possibility for having annotations kept separate from the text itself (and of course annotations are a basic feature of hypertext systems).</p>

<p>One problem with using NoteCards for cooperation is that it requires too early commitments in the form of how to make up cards, which title to use for things, etc.</p>

<p>Many of the talks described systems for advanced non-linear structuring of ideas but were presented by a human speaker who of necessity had to linearize them. Norman Meyrowitz from Brown University noted this problem and said that he had actually designed his talk in his hypertext system, <strong> Intermedia </strong> , and was unsure whether he himself was a good linearizer of it. Intermedia is intended for use in education and research. Typical applications are in English literature where the system will contain a number of literary works and in biology where the data are digitized microscope slides. In addition to the basic data which is shared among several students and/or researchers it is possible to impose a set of links called a web on the documents. Several webs may exist for the same underlying data-e.g. one for each student who build his/her own understanding of the relationships among the data. It should also be possible to start with a web supplied by the professor and then have each student work on refining it. A problem now exists when the underlying data changes (perhaps because the professor inserts new material). What will happen to the links constructed by the students? In some cases they can easily be updated because they link two unchanged parts of the document (there might be some work to do for the computer but it is at least conceptually easy to determine what must be done). But if the change is made to a so-called anchor, it is more difficult to determine what to do, and Meyrowitz discussed different strategies.</p>

<h2>Shifts in Research Perspective (John Seely Brown)</h2>

<p>John Seely Brown who is vice president of advanced research at Xerox PARC gave the invited speech ending the conference. He warned against the trap of building tools that support exactly the collaborative style that you yourself (or your group) like. This would be like the problems with some early AI work trying to automate the researcher's own cognitive style.</p>

<p>Seely Brown stressed the occurrence of watershed events in research (shifts of paradigm/perspective). One such shift was implied at the conference: The shift of focus from personal computing to interpersonal cooperation and computing.</p>

<p>He listed four important shifts in research perspective which had caused us to be aware of totally new points which would have been missed by researchers working from the old perspectives:</p>

<ol>
	<li>User Interfaces: Traditionally one had designed for the absence of trouble by users (sometimes known as "idiot-proof systems"), but now we should design for management of trouble (i.e. systems where the users can easily correct errors on the fly) because trouble will occur-novice users will always make some mistakes even in the best system.</li>
	<li>Office procedures: Originally procedures have been seen as plans (or even recipes) for office work. But now they are seen as descriptions of goals.</li>
	<li>Language: First it was seen as descriptions - now it is seen as action-a process.</li>
	<li>Narratives: At first they were viewed as anecdotes or "war stories" which people were telling for their own pleasure (and which were a waste of time from a management perspective). But as shown by Julian Orr, they can now be viewed as information carriers having powerful potential for transmitting knowledge in a community.</li>
</ol>

<p>Seely Brown emphasized that it is difficult for researchers to observe the evidence for the new perspective before they have already identified the new perspective. I.e. researchers who do not think of narratives as information carriers may study how much time repair people spend talking and how it affects their informal relationships. But they will not find out how the stories later on help the technicians when they are in the field making repairs.</p>

<h2>Other Papers</h2>

<p>A large number of additional papers were presented at the conference and I cannot cover them all. Just a few comments on some additional papers:</p>

<p><strong>Fred Lakin </strong> from Stanford had measured his graphic agility in his vmacs (visual emacs) which is a system to generate visual presentations: The slowdown factor from drawing on paper to drawing with vmacs was a factor six for purely graphic images and a factor three for images with a high content of text (which is fast to type). The advantage of the computer system is of course that once the images are generated, they are easier to manipulate. He was also testing a gesture recognition system to buy him agility at construction time: Sketch first and then let the system clean up the image later.</p>

<p><strong>Tom Malone </strong> from MIT presented the <strong> Information Lens </strong> intended to help users of electronic mail to cope with information overload. The general principle was to get messages into a semi-structured form (based on templates for different purposes, e.g. for announcing meetings) and then have the system prioritize messages on the basis of a knowledge representation of user preferences. In this system it was possible to address a message "to: anyone" and then have an (AI) intelligent postmaster compare the message to the interest profiles and forward it to the individual users. The problem with this approach is that user groups will have to be able to define their own new templates based on their specific needs and (perhaps more problematic) that individual users will have to be able to write down the production rules to enable the system to prioritize their messages. There are facilities to help users in this process (the user can e.g. see which rules fired on a given message) but I am still not sure that the average business professional would like to perform what really amounts to AI programming and debugging.</p>

<p>One of the last of the papers presented at the conference was a survey paper of group decision support systems and is as such recommended for additional overview information and 119 literature references. It was presented by <strong> John King </strong> from the University of California, Irvine. The conclusions were that there is a lot more talk than action in this field: There has not been a surge in the number of sites really active in research and there are essentially no commercially viable systems. But on the other hand people are still working and there are "good vibes in the field."</p>

<h2>Other Comments</h2>

<p>In New York I saw an exhibition at the International Center for Photography of photographs by David Seymour. I did not know this, but it turns out that he was Ben Shneiderman's uncle and a famous photographer in the period around the Second World War. So now we know how Ben got the impetus to become the semi-official photographer of the CHI community.</p>

<p>Anyway, the museum had an installation of Shneiderman's <strong> TIES</strong>-hypertext system explaining the history of Seymour and his photographs. I had seen TIES during my visit to the University of Maryland a few months earlier and I of course took this unexpected opportunity to observe field use of the system. It seemed that museum-goers in general were able to use the system without too much trouble, but there was one interesting glitch. Instead of operating the computer through a regular keyboard, users faced a special layout with only very few buttons: The four arrow-keys used to select pointers on the display, and an enter-key used to perform the selection. The problem was that the legend for the enter-key had been worn down by the many users and was illegible. Since the (very minimalist) instructions for the system referred to this enter-key by name, it led to some trouble that it was not clearly marked. This fairly trivial practical problem of the durability of the inscription probably meant that the learning time for this otherwise well crafted system was almost doubled. Once again: Laboratory usability and field usability are not identical.</p>
</section>
  

  
  <section>
    
        
    
  </section>
  
  
  
  
  

  


  <section>
    <br />
    <p class="share"><b>Share <span class="hide-for-small-down">this article</span>:</b> <a href="https://twitter.com/intent/tweet?url=http://www.nngroup.com/articles/trip-report-cscw-86/&amp;text=CSCW'86%20Trip%20Report&amp;via=nngroup">Twitter</a> | <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=http://www.nngroup.com/articles/trip-report-cscw-86/&amp;title=CSCW'86%20Trip%20Report&amp;source=Nielsen%20Norman%20Group">LinkedIn</a> | <a href="https://plus.google.com/share?url=http://www.nngroup.com/articles/trip-report-cscw-86/">Google+</a> | <a href="mailto:?subject=NN/g Article: CSCW&#39;86 Trip Report&amp;body=http://www.nngroup.com/articles/trip-report-cscw-86/">Email</a></p>
    <hr />
  </section>

  <footer class="l-subsection">
    
    
    <div class="learn-more">
      <h2>Learn More</h2>
      <p><a href="../subscribe/index.php">Subscribe to the weekly newsletter</a> to get notified about future articles.</p>
      </div>
    
    

    <div class="row show-for-small-down related-content">
      
      
      

      
      
        <div class="column small-12 topic-links">All articles about:
          <ul>
            
            <li><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction</a></li>
            
          </ul>
        </div>
      
      

      
      
        <div class="column small-12">
          <h3>UX Conference Training Courses</h3>
           <ul class="no-bullet">
    
        <li><a href="../../courses/ux-basic-training/index.php">UX Basic Training</a></li>
    
        <li><a href="../../courses/hci/index.php">User Interface Principles Every Designer Must Know</a></li>
    
        <li><a href="../../courses/interaction-design-3-day-course/index.php">Interaction Design: 3-Day Course</a></li>
    
</ul>
        </div>
      
      

      
      
      

      
      
      
    </div>

    <div class="hide-for-small-down row related-content">
      <div class="medium-6 columns">
        
        
          <div>
            <h3>Articles</h3>
            <ul class="no-bullet">
              
                
                <li><a href="../embarrassment/index.php">Computer-Assisted Embarrassment</a></li>
                
              
                
                <li><a href="../enhancement/index.php">The Role of Enhancement in Web Design</a></li>
                
              
                
                <li><a href="../lazy-users/index.php">Why Designers Think Users Are Lazy: 3 Human Behaviors</a></li>
                
              
                
                <li><a href="http://www.jnd.org/dn.mss/apples_products_are.php">Apple&#39;s products are getting harder to use because they ignore principles of design</a></li>
                
              
                
                <li><a href="../efficiency-vs-expectations/index.php">Don’t Prioritize Efficiency Over Expectations</a></li>
                
              
            </ul>
          </div>
        
        

        

      </div>
      <div class="medium-6 columns">
        
          
          <div>
            <h3>UX Conference Training Courses</h3>
             <ul class="no-bullet">
    
        <li><a href="../../courses/ux-basic-training/index.php">UX Basic Training</a></li>
    
        <li><a href="../../courses/hci/index.php">User Interface Principles Every Designer Must Know</a></li>
    
        <li><a href="../../courses/interaction-design-3-day-course/index.php">Interaction Design: 3-Day Course</a></li>
    
</ul>
          </div>
          
        
        
      </div>
    </div>
  </footer>
</article>

        </div>

      

    
  </div>
  <div class="row">
    
      <div class="column small-12 show-for-small-down l-navrail announcements">
        
          
        
      </div>
    
    
  </div>

    </div>
    




<footer id="page-footer">
    <div class="row hide-for-print">
        <div class="column small-12 medium-6">
            <section class="footer-popular">
                
                <h1>Popular Topics</h1>
                <div style="float: left;  width: 15em; display: block;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/applications/index.php">Application Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/e-commerce/index.php">E-Commerce Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/eyetracking/index.php">Eyetracking </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/information-architecture/index.php">Information Architecture </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/intranets/index.php">Intranet Design </a></li>
	<li><a href="../../topic/interaction-design/index.php">Interaction Design </a></li>
</ul>
</div>

<div style="float: left;  width: 15em; display: block; list-style: none;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/mobile-and-tablet-design/index.php">Mobile and Tablet Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/credibility-persuasion/index.php">Persuasive Design</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/research-methods/index.php">Research Methods</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/user-testing/index.php">User Testing </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/web-usability/index.php">Web Usability </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/writing-web/index.php">Web Writing &amp; Content Strategy </a></li>
</ul>
</div>

<p>&nbsp;</p>

                
            </section>
        </div>

        <div class="column small-12 medium-6">
            <div class="row">
                <div class="column small-6">
                    <section class="footer-about">
                        <h1>Follow Us</h1>
                        <ul>
	<li><a href="../subscribe/index.php">Subscribe to our weekly UX newsletter</a>,&nbsp;Jakob Nielsen&#39;s Alertbox</li>
	<li>Visit Don Norman&#39;s website, <a href="http://www.jnd.org/"> www.jnd.org </a></li>
	<li>Visit Bruce Tognazzini&#39;s website, <a href="http://www.asktog.com/"> www.asktog.com </a></li>
	<li style="padding-top: 6px;"><a class="twitter-follow-button" data-show-count="false" data-size="large" href="https://twitter.com/nngroup"><img alt="Follow @nngroup on Twitter" height="20" src="https://media.nngroup.com/media/editor/2016/12/30/twitter-follow-button.png" style="width: 117px; height: 20px;" width="117" /></a></li>
	<li style="padding-top: 6px;"><img alt="" height="14" src="https://media.nngroup.com/media/editor/2016/07/26/feed-icon-14x14.png" style="vertical-align:text-top;width:14px;margin-right:.25em;" width="14" /><a href="../../feed/rss/index.php">Subscribe to our RSS feed</a></li>
</ul>

                    </section>
                </div>

                <div class="column small-6">
                    <section class="footer-about right-column">
                        <h1>About
</h1>
                        <ul class="no-bullet">
	<li><a href="../../about/contact/index.php">Contact information</a></li>
	<li><a href="../../about/index.php">About Us</a></li>
	<li><a href="../../about/why-nng/index.php">Why NN/g? </a></li>
	<li><a href="../../return-policy/index.php">Return Policy</a></li>
</ul>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="copyright column small-12">
            <p><a href="../../copyright-and-reprint-info/index.php">Copyright</a> &copy; 1998-2017 Nielsen Norman Group, All Rights Reserved.</p>
        </div>
    </div>
</footer>


    


        <script src="https://media.nngroup.com/static/vendor/fastclick/js/fastclick.min.js"></script>
        <script src="https://media.nngroup.com/static/vendor/foundation/js/foundation.min.js"></script>
        <script src="http://media.nngroup.com/static/js/app.js"></script>
        <script src="http://media.nngroup.com/static/js/scroll-to-fixed.js"></script><script>var externalLinks = document.querySelectorAll('a[href^="http"]');for (var i = externalLinks.length-1; i >= 0; i--) {externalLinks[i].addEventListener("click", function(event) { alert("External links are disabled for this test."); event.preventDefault(); }, false);}var mailLinks = document.querySelectorAll('a[href^="mailto"]');for (var i = mailLinks.length-1; i >= 0; i--) {mailLinks[i].addEventListener("click", function(event) { alert("Email links are disabled for this test."); event.preventDefault(); }, false);}var externalForms = document.querySelectorAll('form');for (var i = externalForms.length-1; i >= 0; i--) {externalForms[i].addEventListener("submit", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false); externalForms[i].addEventListener("click", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false);}</script>

        <script>
            
            $(function() {
                $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
                $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
            });
        </script>

        
<script src="https://media.nngroup.com/static/js/articles.js"></script>


        
        <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1072699960;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072699960/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
    </body>

<!-- Mirrored from www.nngroup.com/articles/trip-report-cscw-86/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:14:37 GMT -->
</html>
