<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->





    
<!-- Mirrored from www.nngroup.com/articles/breaking-web-conventions/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:13:15 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1026.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"licenseKey":"1a30ef9931","queueTime":4,"beacon":"bam.nr-data.net","errorBeacon":"bam.nr-data.net","applicationID":"2048992","transactionName":"NFxSMRBUX0EDUkIPDA0WdhAMVkVbDV8ZBxEXUFMJB0YfRAtUQRVZIktEDAFZVHYHRVcPD01eVRE=","applicationTime":813,"agent":""}</script>
        <title>Breaking Web Design Conventions = Breaking the User Experience</title><meta property="og:title" content="Breaking Web Design Conventions = Breaking the User Experience" />
  
        
        <meta name="description" content="Bucknell University caused a stir with its unconventional responsive redesign, but at a high cost to usability, as shown in tests with students and parents.">
        <meta property="og:description" content="Bucknell University caused a stir with its unconventional responsive redesign, but at a high cost to usability, as shown in tests with students and parents." />
        
  
        
	
        
        <meta name="keywords" content="university websites, higher education, universities, web design, web conventions, design conventions, web design trends">
        

        

        <meta name="robots" content="noarchive">
        <meta name="viewport" content="width=device-width">

        <link rel="search" type="application/opensearchdescription+xml" title="NN/g Search" href="../../opensearch.xml" />
        <link rel="shortcut icon" href="http://media.nngroup.com/static/img/favicon.ico" />

        <link rel="stylesheet" href="https://media.nngroup.com/static/vendor/font-awesome/css/font-awesome.min.css">

        
        <link rel="stylesheet" href="https://media.nngroup.com/static/css/7232a960f1df.css" type="text/css" />
        <link rel="stylesheet" href="http://fast.fonts.com/t/1.css?apiType=css&amp;projectid=c5846260-9def-46ec-9407-0f1349bd2d64" type="text/css">

        <script src="https://media.nngroup.com/static/vendor/modernizr/js/modernizr.min.js"></script>

        <script type="text/javascript" src="https://media.nngroup.com/static/js/3283be5def74.js"></script>
        

        

        
        
        <!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=229034,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('http://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->


<!-- Google Analytics Tracking Code for http://nngroup.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23132681-1', 'nngroup.com');
  ga('require', 'linkid', 'https://www.nngroup.com/articles/breaking-web-conventions/linkid.js');
  ga('require', 'displayfeatures');

// Optimizely Universal Analytics Integration
 window.optimizely = window.optimizely || [];
 window.optimizely.push("activateUniversalAnalytics");

  ga('send', 'pageview');

</script>
<script>
/**
* Function to track a click on an outbound link in Google Analytics.
*/
function trackOutboundLink(label, url) {
    if(window.ga && ga.loaded) {
        ga('send', 'event', 'outbound', 'click', label, {'hitCallback': function(){document.location = url;}
        });
    } else {
        document.location = url;
    }
}
/**
* Function to track a click on a link in the article announcement area.
*/
$('#articleAnnouncement a').on('click', function(){
  ga('send', 'event', 'link', 'click', 'articleAnnouncement');
});
</script>

<script type="text/javascript">
 setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2421.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



<!-- Hotjar Tracking Code for http://nngroup.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:121605,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

        <script src="https://media.nngroup.com/static/js/analytics/track_outbound_link.js"></script>
        
        
        
    </head>
    <body class="location-articles articles-detail">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
    <header id="page-header">
        <div id="cart" class="shopping-cart mobile-cart show-for-small-down"></div>
        <div class="row">
            <div id="masthead" class="masthead small-12 medium-7 column">

                <!-- The button to toggles menu on small screen -->

                <h1><a href="../../index.php"><img src="http://media.nngroup.com/static/img/logo-wordmark.png" width="74" height="30"> Nielsen <span class="norman">Norman</span> Group</a></h1>
                <p class="tagline show-for-medium-up">Evidence-Based User Experience Research, Training, and Consulting</p>

                <!-- The button to toggles form search on small screen -->


                <div style="clear: right;width:  100%;height: 1px;"></div><?php define('ROOT_PATH',$_SERVER["DOCUMENT_ROOT"] . '/apps/navigation_styles/navigations/3'); ?><?php include(ROOT_PATH)."/navigation.php"; ?>

                <!-- Drop down menu on small screen -->
                <div id="hideNav" class="show-for-small-down">
                  
                  <ul class="menu-content">
                    <li><a href="../../index.php">Home</a></li>
                    <li class="has-submenu">
                      <a>Training</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../training/index.php">Overview</a></li>
                        
<li id="nav-ux-conference"><a href="../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../online-seminars/index.php"><span>Online Seminars</span></a></li>

                      </ul>
                    </li>
                    <li class="has-submenu">
                      <a>Consulting</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
  <li><a href="../../consulting/index.php">Overview</a></li>
  
  <li><a href="../../consulting/benchmarking/index.php">Benchmarking</a></li>
  
  <li><a href="../../consulting/expert-design-review/index.php">Expert Design Review</a></li>
  
  <li><a href="../../consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
  
  <li><a href="../../consulting/usability-testing/index.php">Usability Testing </a></li>
  
  <li><a href="../../consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
  
  <li><a href="../../in-house-training/index.php">In-House Training</a></li>
</ul>

                    </li>

                    <li><a href="../../reports/index.php">Reports</a></li>
                    <li><a href="../index.php">Articles</a></li>
                    <li class="has-submenu"><a>About NN/G</a><i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../about/index.php">Overview</a></li>
                        <li><a href="../../people/index.php">People</a></li>
                        <li><a href="../../about/why-nng/index.php">Why NN/g?</a></li>
                        <li><a href="../../about/contact/index.php">Contact</a></li>
                        <li><a href="../../news/index.php">News</a></li>
                        <li><a href="../../about/history/index.php">History</a></li>
                        <li><a href="../../books/index.php">Books</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                </div>

                <hr class="header-divider show-for-small-only"/>


            </div>
       </div>

        <div id="hideNav">            <nav class="primary-nav small-12 column">
                <ul>
                    <li><a class="navitem" id="nav-home" href="../../index.php">Home</a></li>
                    <li><a class="navitem" id="nav-training" href="../../training/index.php">Training</a></li>
                    <li><a class="navitem" id="nav-consulting" href="../../consulting/index.php">Consulting</a></li>
                    <li><a class="navitem" id="nav-reports" href="../../reports/index.php">Reports</a></li>
                    <li><a class="navitem" id="nav-articles" href="../index.php">Articles</a></li>
                    <li><a class="navitem" id="nav-about" href="../../about/index.php">About NN/G</a></li>
                </ul>
            </nav>
            <nav class="secondary-nav small-12 medium-12 large-12 columns">
                
                
            </nav>
        </div>
    </header>
    <div id="mainContent" class="l-content">
    
  

  

  <div class="row">
    
      
      <section class="small-12 medium-3 columns collapsable-content l-navrail articles-left-column">
        <h1 class="show-for-small-down collapse-link mobile-browse-header">Browse by Topic and Author</h1>
          <div class="row collapsable">

            
            <div class="small-6 medium-12 columns">
              <section id="article-popular-topics" style="display: none;">
                
                <h1>Topics</h1>
                <ul id="article-popular-topics">
                    
                    <li>
                        
                        <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/intranets/all/index.php">Intranets</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/user-testing/all/index.php">User Testing</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                        
                    </li>
                    
                    <li><a href="#" id="article-see-all-topics"><strong>See all topics&hellip;</strong></a></li>
                </ul>
                
              </section>
              <section id="article-all-topics" style="display: none;">
                
                <h1>
                  <span class="show-for-medium-up">All Article Topics</span>
                  <span class="show-for-small-down">All Topics</span>
                  (<a href="#" id="article-hide-all-topics">hide</a>)
                </h1>
                <ul>
                  
                  <li>
                    
                    <a href="../../topic/accessibility/all/index.php">Accessibility</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/agile/all/index.php">Agile</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/analytics-and-metrics/all/index.php">Analytics &amp; Metrics</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/applications/all/index.php">Application Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/b2b-websites/all/index.php">B2B Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/behavior-patterns/all/index.php">Behavior Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/branding/all/index.php">Branding</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/content-strategy/all/index.php">Content Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/corporate-websites/all/index.php">Corporate Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/design-patterns/all/index.php">Design Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-design-process/all/index.php">Design Process</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/email/all/index.php">Email</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/eyetracking/all/index.php">Eyetracking</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/heuristic-evaluation/all/index.php">Heuristic Evaluation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/human-computer-interaction/all/index.php">Human Computer Interaction</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-ideation/all/index.php">Ideation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/information-architecture/all/index.php">Information Architecture</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/interaction-design/all/index.php">Interaction Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/international-users/all/index.php">International Users</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/intranets/all/index.php">Intranets</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-management/all/index.php">Management</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/navigation/all/index.php">Navigation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/non-profit-websites/all/index.php">Non-Profit Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/personas/all/index.php">Personas</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/persuasive-design/all/index.php">Persuasive Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/prototyping/all/index.php">Prototyping</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/psychology-and-ux/all/index.php">Psychology and UX</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/research-methods/all/index.php">Research Methods</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/search/all/index.php">Search</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/social-media/all/index.php">Social Media</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/strategy/all/index.php">Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/user-testing/all/index.php">User Testing</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-teams/all/index.php">UX Teams</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/visual-design/all/index.php">Visual Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/writing-web/all/index.php">Writing for the Web</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/young-users/all/index.php">Young Users</a>
                    
                  </li>
                  
                </ul>
                
              </section>
            </div>
            <script>
              /* Determine which topics list will be displayed: the full or the
               * partial.  We are inlining this close to the list itself to
               * prevent rendering delays on the client
               */
              var topic_list = $.cookie("article-topics");

              var popular_topics = $('#article-popular-topics');
              var all_topics = $('#article-all-topics');

              if(topic_list == 1) {
                  popular_topics.hide();
                  all_topics.show();
              } else {
                  popular_topics.show();
                  all_topics.hide();
              }
            </script>
            

            
            <div class="small-6 medium-12 columns">
              
              <section id="article-initial-authors">
                  <h1>Author</h1>
                  <ul>
                    
                    <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                    
                    <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                    
                    <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                    
                    <li><a href="#" onClick="$('#article-initial-authors').hide(); $('#article-all-authors').show();"><strong>See all authors&hellip;</strong></a></li>
                  </ul>
              </section>
              <section id="article-all-authors" style="display: none;">
                <h1>
                  All Authors
                  (<a href="#" id="article-hide-all-authors">hide</a>)
                </h1>
                <ul>
                  
                  <li><a href="../author/raluca-budiu/index.php">Raluca Budiu</a></li>
                  
                  <li><a href="../author/patty-caya/index.php">Patty Caya</a></li>
                  
                  <li><a href="../author/susan-farrell/index.php">Susan Farrell</a></li>
                  
                  <li><a href="../author/therese-fessenden/index.php">Therese Fessenden</a></li>
                  
                  <li><a href="../author/kim-flaherty/index.php">Kim Flaherty</a></li>
                  
                  <li><a href="../author/sarah-gibbons/index.php">Sarah Gibbons</a></li>
                  
                  <li><a href="../author/aurora-harley/index.php">Aurora Harley</a></li>
                  
                  <li><a href="../author/kate-kaplan/index.php">Kate Kaplan</a></li>
                  
                  <li><a href="../author/page-laubheimer/index.php">Page Laubheimer</a></li>
                  
                  <li><a href="../author/angela-li/index.php">Angie Li</a></li>
                  
                  <li><a href="../author/hoa-loranger/index.php">Hoa Loranger</a></li>
                  
                  <li><a href="../author/kate-meyer/index.php">Kate Meyer</a></li>
                  
                  <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                  
                  <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                  
                  <li><a href="../author/kara-pernice/index.php">Kara Pernice</a></li>
                  
                  <li><a href="../author/christian-rohrer/index.php">Christian Rohrer</a></li>
                  
                  <li><a href="../author/amy-schade/index.php">Amy Schade</a></li>
                  
                  <li><a href="../author/katie-sherwin/index.php">Katie Sherwin</a></li>
                  
                  <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                  
                  <li><a href="../author/kathryn-whitenton/index.php">Kathryn Whitenton</a></li>
                  
                </ul>
              </section>
              
            </div>
            
          </div>

          
          <section class="hide-for-small-down">
            
              
              <h1>Recent Articles</h1>
              
              <ul>
                  
                  <li><a href="../seamless-cross-channel/index.php">Seamlessness in the Omnichannel User Experience</a></li>
                  
                  <li><a href="../brand-intention-interpretation/index.php">Experience Design: Bridging Brand Intention and Brand Interpretation</a></li>
                  
                  <li><a href="../eyetracking-tasks-efficient-scanning/index.php">Scanning Patterns on the Web Are Optimized for the Current Task</a></li>
                  
                  <li><a href="../flat-design-best-practices/index.php">Flat-Design Best Practices</a></li>
                  
                  <li><a href="../ux-user-stories/index.php">Accounting for UX Work with User Stories in Agile Projects</a></li>
                  
                  <li><a href="../index.php"><strong>See all articles&hellip;</strong></a></li>
              </ul>
              
            
          </section>
          

          
          
          <section class="hide-for-small-down">
            <h1>Popular Articles</h1>
            
            <ul>
                
                <li><a href="../usability-101-introduction-to-usability/index.php">Usability 101: Introduction to Usability</a></li>
                
                <li><a href="../top-10-mistakes-web-design/index.php">Top 10 Mistakes in Web Design</a></li>
                
                <li><a href="../how-users-read-on-the-web/index.php">How Users Read on the Web</a></li>
                
                <li><a href="../f-shaped-pattern-reading-web-content/index.php">F-Shaped Pattern For Reading Web Content</a></li>
                
                <li><a href="../ten-usability-heuristics/index.php">10 Usability Heuristics for User Interface Design</a></li>
                
                <li><a href="../intranet-design-2016/index.php">10 Best Intranets of 2016</a></li>
                
                <li><a href="../which-ux-research-methods/index.php">When to Use Which User-Experience Research Methods</a></li>
                
                <li><a href="../response-times-3-important-limits/index.php">Response Times: The 3 Important Limits</a></li>
                
                <li><a href="../why-you-only-need-to-test-with-5-users/index.php">Why You Only Need to Test with 5 Users</a></li>
                
                <li><a href="../page-fold-manifesto/index.php">The Fold Manifesto: Why the Page Fold Still Matters</a></li>
                
            </ul>
          </section>
          
          

          
            <section class="hide-for-small-down">
              
            </section>
          

      </section>
      

      
        <div class="small-12 medium-9 columns content-body">
          




<article class="l-subsection alertbox-article-content article-detail-bl">
  
  <header>
    <div class="l-subsection">
      <h1>Breaking Web Design Conventions = Breaking the User Experience</h1>
      <div class="author-meta">by
        
          
            
            <a href="../author/katie-sherwin/index.php">Katie Sherwin</a>
            
          
        on  July 20, 2014
        
      </div>

      

      
      <div class="topic-links">
        Topics:
        
          <ul>

  <li><a href="../../topic/design-patterns/index.php">Design Patterns</a></li>

  <li><a href="../../topic/web-usability/index.php">Web Usability</a></li>

</ul>
        
      </div>
      
    </div>
  </header>
  

  
  
  <section>
    <p class="lede"><strong>Summary:</strong> Bucknell University caused a stir with its unconventional responsive redesign, but at a high cost to usability, as shown in tests with students and parents.</p>
    <br />
  </section>
  
  

  
  <section id="article-body"><p>If I asked you to picture the homepage for a university website, what would it look like? You might come up with something similar to SUNY Cortland’s homepage: images of students on a lawn, navigation at the top with sections for <em>Academics</em>, <em>Admissions</em>, <em>Prospective</em> <em>Students</em>, <em>Alumni</em>, and so on. There would probably also be sections for <em>News</em> and <em>Events</em>. Indeed, university websites tend to follow a certain set of conventions, many of which haven’t changed much over the years.</p>

<div style="text-align:left">
<figure class="caption"><img alt="SUNY Cortland's traditional homepage" height="546" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2014/07/16/cortlandedu_traditional-homepage.png" style="border: 1px solid rgb(169, 169, 169);" width="720"/>
<figcaption><em>If asked to picture a typical university website, you would probably think of something like this homepage, from SUNY Cortland (cortland.edu).</em></figcaption>
</figure>
</div>

<p>Playing it safe has usability benefits, but some site designers may argue that websites must go beyond the status quo to differentiate themselves from the competition and communicate a more progressive brand.</p>

<h2>Bucknell University’s Controversial Redesign</h2>

<p>When Bucknell University redesigned its website and broke away from some long-established conventions, it generated a lot of buzz in the higher-education web community. Applauded by some and condemned by others, the design has been hotly debated on Twitter and on blogs. And sure enough, attendees in my University Websites course wondered about it.</p>

<div style="text-align:left">
<figure class="caption"><img alt="Bucknell University's redesigned homepage" height="460" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2014/07/16/bucknelledu_homepage.png" style="border: 1px solid rgb(169, 169, 169);" width="693"/>
<figcaption><em>The redesigned homepage for Bucknell University.</em></figcaption>
</figure>
</div>

<p>Before reading any further, <strong>try this mini user test </strong>for yourself:</p>

<ul>
	<li>In the top line, what does the round icon between <em>The Everything Directory</em> and the search box symbolize? What will you get if you click it?</li>
	<li>Also on that top line, what will you get when clicking <em>Start Exploring</em>?</li>
	<li>In the left margin, what will you get if you click one of the dates?</li>
	<li>What information is on the other side of the link “Bucknell is under the sea” within the big photo? (Did you recognize this as a link and not just a caption?) Would you click this if you were considering applying to this university?</li>
</ul>

<p>Among the design changes that received the most attention are the following:</p>

<ol>
	<li>
	<p><strong>Minimalist and hidden navigation.</strong> Instead of the traditional global navigation bar with several categories, the Bucknell site has an extremely <a href="../flat-vs-deep-hierarchy/index.php">narrow and deep hierarchy</a>, with just 2 menu items: <em>Start Exploring</em>, (which has a drop-down for audience-based navigation) and <em>The</em> <em>Everything</em> <em>Directory</em> (which leads to an A-Z index). On internal pages, the local navigation is located under drop-down menus and thus not immediately visible.</p>
	</li>
	<li>
	<p><strong>Tools to customize the homepage and view browsing history. </strong>Users can select which content they want displayed on the homepage, by turning certain categories “on” or “off.” The clock icon in the top navigation opens a drawer that shows users the most recent pages that they’ve viewed.</p>
	</li>
	<li>
	<p><strong>Glossy visual-design trends.</strong> The site uses several visual-design trends that are popular now, such as minimal content and a full-width image that fills the page above the fold. Long-scrolling, image-heavy pages feature vertically stacked containers, card, and grid layouts, and mimic horizontal swipe on desktop. While these styles aren’t unique to this university, they do impact the user experience in ways conventional sites don’t.</p>
	</li>
</ol>

<p>Bucknell’s redesign is <a href="../responsive-web-design-definition/index.php">responsive</a>, and some of the changes (specifically, the first and the last ones) reflect practices that are ubiquitous on the responsive web nowadays. In this article I will focus on how these trends impact desktop user experience.</p>

<h2>Where There Is Controversy, There Must Be Data</h2>

<p>When innovative products or interfaces excite or upset people, we at Nielsen Norman Group love to add data to the conversation, however brutal the results may be (you can read examples in our archives, for example, <a href="../windows-8-disappointing-usability/index.php">Windows 8</a>, <a href="../flash-99-percent-bad/index.php">Flash</a>, <a href="../pdf-unfit-for-human-consumption/index.php">PDFs</a>).</p>

<p>Following this tradition, I conducted a simple usability study of Bucknell’s redesigned website to help answer some of the questions that designers had about the controversial design. The goal of the study focused on how the desktop design impacted visitors’ ability to find the information that they needed and to gauge the usability of the site.</p>

<p>We ran an unmoderated <a href="../remote-usability-tests/index.php">remote usability study</a> with 5 participants: 3 prospective students and 2 parents of prospective students. (<a href="../why-you-only-need-to-test-with-5-users/index.php">Even with just a few users</a>, it’s possible to identify most of the main usability issues in a design.) We asked participants to explore the site to see if the university is a good match for their needs. Then we asked participants to find answers to questions that are typical for users of such sites (e.g., the cost of tuition, the application deadline). </p>

<h2>The Results</h2>

<h3>Key facts and information effectively caught users’ attention and created a positive impression of the academic quality of the university.</h3>

<p><a href="../write-interesting-facts/index.php">Users love relevant facts</a>, so it’s great that the site effectively surfaced key facts and figures with concise, eye-catching, and informative content, which users stopped to read. After the test was over, we asked participants what their impressions of the university were, and their comments about the academic quality were positive: </p>

<ul>
	<li>“I would say Bucknell is an excellent school. I like how they teach better ways of thinking.  It sounds like a really hard school.”</li>
	<li>"It seems like it would be an interesting place to attend, being very focused on student-faculty interactions, which are always a good thing.”</li>
</ul>

<div style="text-align:left">
<figure class="caption"><img alt="Key facts displayed prominently" height="374" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2014/07/16/bucknelledu_facts.png" style="border: 1px solid rgb(169, 169, 169);" width="720"/>
<figcaption><em>Prominently displayed key figures caught people’s attention and got them to stop and read the information, thereby adding to their understanding of the quality of the university. However, the pages with these facts were not easily found.</em></figcaption>
</figure>
</div>

<p>Unfortunately, <strong>these useful key facts were hard to find</strong>, because insufficient and hidden navigation prevented users from discovering the pages that showcased this information. Your website may have great content and a nice visual design, but none of that matters if users cannot actually <em>find</em> your content. As one of our users put it:</p>

<p style="margin-left: 40px;">“It looks nice but there's not that much information about anything.”</p>

<p>As we’ve said before, the first law of e-commerce is that <a href="../ia-task-failures-remain-costly/index.php">if the user can’t find the product, the user can’t buy the product</a>. There’s a similar law for information-oriented sites: if the user can’t find the content, the user can’t read the content.</p>

<p><a href="../killing-global-navigation-one-trend-avoid/index.php">Don’t make global navigation hard to find</a>. Everything else on your site will suffer, including your business goals.</p>

<h3>Controversy #1: Minimalist and hidden navigation<br/>
Our finding: Minimalist and hidden navigation unanimously frustrated users and prevented them from finding information.</h3>

<p style="margin-left: 40px;">“I'm a college grad, and I had a hard time with this site. It would be so much easier if things would be where you think they would be. You have to hunt for everything.”</p>

<p>To give an idea of some of the trouble that users encountered when completing tasks, here were a few pain points:</p>

<ul>
	<li>
	<p>Many participants were <strong>not able to find the list of majors</strong> that the school offered. One user found the list once, but was unable to retrace his steps to find it again.</p>
	</li>
	<li>
	<p>Several participants were <strong>not able</strong> <strong>to find the cost of tuition</strong>.</p>
	</li>
	<li>
	<p>Several participants <strong>did not realize that the university offered their program</strong> of choice. (Sadly, this is consistent with our other <a href="../university-sites/index.php">research on university websites</a>, which found that 48% of visitors on university websites did not realize that their program of choice was offered at that university. Every student who leaves your site mistakenly thinking that you don’t have a program for her is a missed opportunity— one with an enormous financial impact.)</p>
	</li>
</ul>

<p>These results can be mainly attributed to 3 aspects of Bucknell University’s navigation:</p>

<ul>
	<li>
	<p><strong>Deep hierarchy and vague categories</strong>. Sites with deep hierarchies are usually more difficult for users than sites with moderately broad structures, as we know from <a href="../../courses/hci/index.php">research in Human-Computer Interaction</a>. A deep hierarchy usually ends up with more generic labels at the top. By reducing the top-level navigation to only two categories, the Bucknell site forces users to choose between two vague labels (<em>Start Exploring</em> and <em>The Everything Directory</em>), which have no <a href="../information-scent/index.php">information scent</a>: users cannot predict what they will get when they click on either of these.</p>
	</li>
</ul>

<div style="text-align:left;">
<figure class="caption"><img alt="Bucknell's global navigation categories" height="594" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2014/07/16/bucknelledu_start-exploring-menu.jpg" style="border: 1px solid rgb(169, 169, 169);" width="1208"/>
<figcaption><em>The </em>Start Exploring<em> category reveals a drop-down menu with audience-based navigation, adding an extra level to an already deep site hierarchy.</em></figcaption>
</figure>
</div>

<p> </p>

<ul style="list-style: none; ">
	<li>
	<p>(A note about audience-based navigation: Bucknell’s audiences in the <em>Start Exploring </em>menu are distinct, which is good. But role-based navigation like this poses the same problems it does on other university sites. Users don’t necessarily self-identify, and they often don’t understand which audience category contains the content that they want. For example, many parents view the page for prospective students well before they view the page for parents. Evaluate whether topic-based organization might be more efficient for your users. And if you are using audience-based organization, be sure that each audience is specific and distinct, and include the information that is most relevant to that audience.)</p>

	<p>Regarding “<em>The Everything Directory</em>” (the 2<sup>nd</sup> category in the global navigation) this alphabetical listing of the university’s online content was not helpful for users. Alphabetical listings <em>can</em> work well when there is a well-known, common vocabulary for the terms used. At a university, however, most visitors don’t know the correct vocabulary or official names for things. When visitors don’t know your terminology, or when your site uses a lot of jargon, an <a href="../alphabetical-sorting-must-mostly-die/index.php"><strong>alphabetical listing is not helpful.</strong></a></p>
	</li>
</ul>

<div style="text-align:left">
<figure class="caption"><img alt="'Cost' is not in the A–Z index" height="476" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2014/07/16/bucknelledu_cost-not-in-index.jpg" style="border: 1px solid rgb(169, 169, 169);" width="720"/>
<figcaption><em>One participant used </em>The Everything Directory<em> to find the cost of tuition, so she went to the letter “C” in the directory, yet there were no results. Maybe “T” for “tuition” would work? Nope, it’s not there either.</em></figcaption>
</figure>
</div>

<p> </p>

<ul>
	<li>
	<p><strong>Hidden navigation.</strong> Unfortunately, Bucknell hides their main navigation under a vague label, <em>Start Exploring</em>; the local navigation and related content are hidden under equally unenticing labels: <em>In this section</em> and <em>Related Sections</em>.  Although hiding navigation can be a reasonable compromise for mobile, it does not work well for desktop: it makes content less discoverable, requires additional <a href="../interaction-cost-definition/index.php">interaction cost</a> for the users to view their menu options, and it increases <a href="../minimize-cognitive-load/index.php">cognitive load</a> because users must <a href="../recognition-and-recall/index.php">recall</a> what will become available if they call up the navigation options.</p>

	<p>In our test, users consistently could not find content because they either did not notice the local navigation, or they scanned the drop-down options quickly and missed the key links. Once they hovered the mouse off the menus, they were back to guessing where to go next and how to get there.</p>
	</li>
</ul>

<p> </p>

<div style="text-align:left">
<figure class="caption"><img alt="Hidden local navigation" height="401" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2014/07/16/bucknelledu_admissions-local.png" style="border: 1px solid rgb(169, 169, 169);" width="600"/>
<figcaption><em>Users were unable to find the</em> Tuition and Financial Aid <em>page from the </em>Admissions &amp; Aid<em> section because the local navigation was hidden in a drop-down menu. One user commented, </em>“It’s really not addressing the key questions I have.”</figcaption>
</figure>
</div>

<ul>
	<li>
	<p><strong>Reliance on search as primary navigation. </strong>With the trend of <a href="../characteristics-minimalism/index.php">minimalist and clutter-free design</a>, sometimes it is assumed that users prefer to search above all else. But often that’s not the case. Typically, people use search if they know exactly what they’re looking for, or if they cannot find something through browsing.</p>

	<p>After using the site for a while, one prospective student explained,</p>

	<p>"I would much prefer to click two or three things <strong>rather than having to always go to search and hope desperately</strong> that what I need comes up in the results.”</p>

	<p>Search should only be the primary navigation for a website if the site’s main function is to be a search engine. For example, Google, Bing, and job search boards can all use this approach. But in the context of an information-heavy site like a university, browsing is essential for increasing discoverability of content.</p>

	<p>Unfortunately, Bucknell’s search engine was not even very helpful for those participants that tried it. A parent typed “tution” instead of “tuition” and the search returned zero results. A different user searched for the phrase “Cost of going Bucknell,” and again, received no results related to tuition.</p>
	</li>
</ul>

<p>After they finished the activities that we gave them, people told us what they would improve about the site.<strong>Everybody complained about the navigation; they all wanted to see more categories.</strong> The comments below are just a sample from different users:</p>

<ul>
	<li>"I think it’s cool, it’s very modern looking. But I really wish that it was like a conventional college site that has at the top Academics, Financial Aid, everything like that. This was much harder to navigate.”</li>
	<li>“They should have more tabs, rather than put everything in here [Start Exploring]”</li>
	<li>"It seems like it’s trying too hard to be different, and different is simply making me confused. It needs to be easier to navigate."</li>
</ul>

<p><strong>Recommendations</strong>:</p>

<ol style="list-style-type:lower-alpha;">
	<li>Design navigation that is always visible, with a level of depth and breadth that is appropriate for your content. Navigation helps users understand where they can move on the site, so make sure users can see it. In this way, you will <a href="../navigation-ia-tests/index.php">increase findability and discoverability</a>, so that visitors can glance at their choices without having to physically act or guess where the content is located.</li>
	<li>Choose clear labels to represent your content. In Bucknell’s case, <em>The</em> <em>Everything</em> <em>Directory</em> should have been labeled to represent exactly what it is: an A–Z Index.</li>
	<li>Search should be able to recognize common keywords, queries, synonyms, and misspellings. Use your analytics data to identify what people search for on your site and make sure your search engine is optimized to help them answer those queries.</li>
</ol>

<h3>Controversy #2: Customization<br/>
Our finding: Tools to customize homepage content and view browsing history are underutilized.</h3>

<p>In our testing, the 2 customization tools were largely ignored.</p>

<ul>
	<li>
	<p><strong>Most users never commented on or interacted with the <em>Customize</em> <em>the</em> <em>Homepage</em> feature. </strong>Of the few users that did, only one user tried to understand the feature (another user clicked a category, but did not see anything happen and quickly moved on to another area). This is consistent with other research that found that users <a href="../customization-of-uis-and-products/index.php">typically don’t use customization tools</a>; they leave the default setting as is. Users don’t like to spend their time figuring out how part of a website works or what the benefit to them is. They very rarely interact with fancy widgets or elaborate tools, because they want to get their information or complete their task as quickly and possible, and be done.<br/>
	<br/>
	Even though most users did not engage with this feature, the one that did had a very negative experience:<br/>
	​“I find the navigation on the homepage to be cumbersome, and it really didn’t give me a good impression when I first arrived on the site.”</p>
	</li>
</ul>

<div style="text-align:left">
<figure class="caption"><img alt="Bucknell's customize the homepage section" height="207" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2014/07/16/bucknelledu_customize-home.jpg" style="border: 1px solid rgb(169, 169, 169);" width="720"/>
<figcaption><em>The </em>Customize this Homepage<em> tool on the homepage was underutilized. This feature works by selecting or deselecting a category; that content is either added or hidden from the homepage.</em></figcaption>
</figure>
</div>

<p> </p>

<ul>
	<li>
	<p><strong>None of the users noticed or interacted with the <em>Recently</em> <em>viewed</em> <em>pages</em> tool.</strong> This tool is represented by the clock icon in the global navigation. The icon didn’t harm or get in the way of any of the users’ experience, but it didn’t enhance it either. Users likely were not drawn to click the icon, because the image of a clock isn’t familiar on websites, it doesn’t look actionable, and there is no label indicating its purpose.</p>
	</li>
</ul>

<div style="text-align:left">
<figure class="caption"><img alt="Clock icon represents recently viewed pages" border="0" height="138" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2014/07/16/bucknelledu_clock.png" style="border: 1px solid rgb(169, 169, 169);" width="418"/>
<figcaption><em>No participants noticed or engaged with the clock icon.</em></figcaption>
</figure>
</div>

<p><strong style="line-height: 1.6;">Recommendation: </strong><span style="line-height: 1.6;">Before investing in creating fancy tools like these, consider how necessary they are for your users, how likely they are to be utilized, and whether the resources that will be spent on developing the tools would be better spent on improving content. (And <a href="../icon-usability/index.php">design better icons</a>.)</span></p>

<h3>Controversy #3: Glossy visual-design trends<br/>
Our finding: Glossy visual design made the site look nice, but they hurt usability.</h3>

<p>Users appreciated the aesthetics of the website, but overall, the costs of this design (measured in usability issues encountered) outweighed the benefits.</p>

<p>“It's not user friendly, even though it would appear to be, just by having the friendly faces and happy-looking people.”</p>

<ul>
	<li>
	<p><strong>Minimal content above the fold left users wanting more information, sooner.</strong> This low information density wastes the capacity of the <a href="../scaling-user-interfaces/index.php">human–desktop communication channel</a>. Presented with a background image, one headline, and sparse navigation on the homepage, users commented that they wanted more information up front. As one student put it,<br/>
	“I feel like they should put more on the starting page, rather than making the page longer.”</p>
	</li>
	<li>
	<p><strong>Use of large images slowed down page-load time. </strong>Part of our study included a 5-second test where users looked at the website for 5 seconds and then commented on what they remembered. In one case, the main hero image never loaded, so there wasn’t much for the user to see. A different user tried scrolling down the homepage but gave up:<em> </em><br/>
	“It’s slow to load because of all the pictures.”</p>
	</li>
	<li>
	<p><strong>Horizontal scrolling was rarely utilized. </strong>We know that <a href="../horizontal-scrolling/index.php">users don’t like horizontal scrolling</a>. Here, users were not forced to scroll sideways to view the content (fortunately), and most users did not click the arrows to view the rest of the content. A parent commented,<br/>
	"Either the site should move up or down or right or left, but not both. It was a very confusing sense of navigation on the site.”</p>
	</li>
</ul>

<p><strong>Recommendations: </strong>As with any site going through a redesign and evaluating which trends to adopt or not, it’s important to consider how each trend will impact users. Will it enhance your visitors’ ability to accomplish their goals? Usability testing will help you learn what works and what is causing more trouble than it’s worth.</p>

<h2>Conclusion</h2>

<p>Bucknell University isn’t alone in its desire to stand out and create a modern look and feel. Many websites in all kinds of industries do this, hoping to stick out among the crowd and impress their users. Moreover, when designing a responsive site that works for multiple devices, it’s easy to ignore the different demands of those individual devices and produce a design that works poorly on some.</p>

<p>Unfortunately, the reality is that too often, resources are spent on making the site look great or creating an innovative widget, and usability is neglected until the very end of development (if it’s even ever looked at). Ideally, you’ll be doing testing throughout the project, be it testing your information architecture, creating and testing <a href="../paper-prototyping/index.php">wireframes and paper prototypes</a>, and conducting usability tests with real users on all the devices that you’re targeting with your design, all with enough time before the launch so that you can iterate your designs and test them again. </p>

<p>Any budget that has room for an expensive glossy redesign should also have room for usability testing. As demonstrated in this article, a single day of user research easily identifies the worst design flaws. This tiny investment protects you from wasted investments in trendy design that will damage your business goals and lose hundreds of times more than the cost of user testing. Instead of <a href="../guesses-vs-data/index.php">guessing what customers want</a>, find out. Then design for your users, not for glitz.</p>

<p> </p>

<p>Learn how to embrace new design trends without hurting your business in the full-day course on <a href="../../courses/emerging-patterns-web-design/index.php">Emerging Patterns for Web Design</a>. </p>

<h3>Full Report</h3>

<p>The full <a href="../../reports/university/index.php">University Websites report</a> with actionable guidelines on how to improve higher education websites is available for download. </p>
</section>
  

  
  <section>
    
        
    
  </section>
  
  
  
  
  

  


  <section>
    <br />
    <p class="share"><b>Share <span class="hide-for-small-down">this article</span>:</b> <a href="https://twitter.com/intent/tweet?url=http://www.nngroup.com/articles/breaking-web-conventions/&amp;text=Breaking%20Web%20Design%20Conventions%20=%20Breaking%20the%20User%20Experience&amp;via=nngroup">Twitter</a> | <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=http://www.nngroup.com/articles/breaking-web-conventions/&amp;title=Breaking%20Web%20Design%20Conventions%20=%20Breaking%20the%20User%20Experience&amp;source=Nielsen%20Norman%20Group">LinkedIn</a> | <a href="https://plus.google.com/share?url=http://www.nngroup.com/articles/breaking-web-conventions/">Google+</a> | <a href="mailto:?subject=NN/g Article: Breaking Web Design Conventions = Breaking the User Experience&amp;body=http://www.nngroup.com/articles/breaking-web-conventions/">Email</a></p>
    <hr />
  </section>

  <footer class="l-subsection">
    
    
    <div class="learn-more">
      <h2>Learn More</h2>
      <p><a href="../subscribe/index.php">Subscribe to the weekly newsletter</a> to get notified about future articles.</p>
      </div>
    
    

    <div class="row show-for-small-down related-content">
      
      
      

      
      
        <div class="column small-12 topic-links">All articles about:
          <ul>
            
            <li><a href="../../topic/design-patterns/index.php">Design Patterns</a></li>
            
            <li><a href="../../topic/web-usability/index.php">Web Usability</a></li>
            
          </ul>
        </div>
      
      

      
      
        <div class="column small-12">
          <h3>UX Conference Training Courses</h3>
           <ul class="no-bullet">
    
        <li><a href="../../courses/ux-basic-training/index.php">UX Basic Training</a></li>
    
        <li><a href="../../courses/human-mind/index.php">The Human Mind and Usability</a></li>
    
        <li><a href="../../courses/emerging-patterns-web-design/index.php">Emerging Patterns for Web Design</a></li>
    
        <li><a href="../../courses/web-page-design/index.php">Web Page UX Design</a></li>
    
</ul>
        </div>
      
      

      
      
        <div class="small-12 column">
          <h3>Research Reports</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../reports/b2b-websites-usability/index.php">B2B Website Usability for Converting Users into Leads and Customers</a></li>
              
            
              
                <li><a href="../../reports/strategic-design-faqs/index.php">Strategic Design for Frequently Asked Questions</a></li>
              
            
          </ul>
        </div>
      
      

      
      
        <div class="small-12 column">
          <h3>Online Seminars</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../online-seminars/fundamental-flaw-flat-design/index.php">The Fundamental Flaw in Flat Design</a></li>
              
            
              
                <li><a href="../../online-seminars/millennials-online/index.php">Millennials Online</a></li>
              
            
              
                <li><a href="../../online-seminars/university-websites/index.php">University Website UX Essentials</a></li>
              
            
          </ul>
        </div>
      
      
    </div>

    <div class="hide-for-small-down row related-content">
      <div class="medium-6 columns">
        
        
          <div>
            <h3>Articles</h3>
            <ul class="no-bullet">
              
                
                <li><a href="../common-ux-deliverables/index.php">Which UX Deliverables Are Most Commonly Created and Shared?</a></li>
                
              
                
                <li><a href="../fight-right-rail-blindness/index.php">Fight Against “Right-Rail Blindness”</a></li>
                
              
                
                <li><a href="../web-form-design/index.php">Website Forms Usability:  Top 10 Recommendations</a></li>
                
              
                
                <li><a href="../duplicate-links/index.php">The Same Link Twice on the Same Page: Do Duplicates Help or Hurt?</a></li>
                
              
                
                <li><a href="../illusion-of-completeness/index.php">The Illusion of Completeness: What It Is and how to Avoid It</a></li>
                
              
            </ul>
          </div>
        
        

        
          
          <div>
            <h3>Research Reports</h3>
            <ul class="no-bullet">
              
                
                  <li><a href="../../reports/b2b-websites-usability/index.php">B2B Website Usability for Converting Users into Leads and Customers</a></li>
                
              
                
                  <li><a href="../../reports/strategic-design-faqs/index.php">Strategic Design for Frequently Asked Questions</a></li>
                
              
            </ul>
          </div>
          
        

      </div>
      <div class="medium-6 columns">
        
          
          <div>
            <h3>UX Conference Training Courses</h3>
             <ul class="no-bullet">
    
        <li><a href="../../courses/ux-basic-training/index.php">UX Basic Training</a></li>
    
        <li><a href="../../courses/human-mind/index.php">The Human Mind and Usability</a></li>
    
        <li><a href="../../courses/emerging-patterns-web-design/index.php">Emerging Patterns for Web Design</a></li>
    
        <li><a href="../../courses/web-page-design/index.php">Web Page UX Design</a></li>
    
</ul>
          </div>
          
        
        
          
          <div>
            <h3>Online Seminars</h3>
            <ul class="no-bullet">
            
                <li><a href="../../online-seminars/fundamental-flaw-flat-design/index.php">The Fundamental Flaw in Flat Design</a></li>
            
                <li><a href="../../online-seminars/millennials-online/index.php">Millennials Online</a></li>
            
                <li><a href="../../online-seminars/university-websites/index.php">University Website UX Essentials</a></li>
            
            </ul>
          </div>
          
        
      </div>
    </div>
  </footer>
</article>

        </div>

      

    
  </div>
  <div class="row">
    
      <div class="column small-12 show-for-small-down l-navrail announcements">
        
          
        
      </div>
    
    
  </div>

    </div>
    




<footer id="page-footer">
    <div class="row hide-for-print">
        <div class="column small-12 medium-6">
            <section class="footer-popular">
                
                <h1>Popular Topics</h1>
                <div style="float: left;  width: 15em; display: block;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/applications/index.php">Application Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/e-commerce/index.php">E-Commerce Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/eyetracking/index.php">Eyetracking </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/information-architecture/index.php">Information Architecture </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/intranets/index.php">Intranet Design </a></li>
	<li><a href="../../topic/interaction-design/index.php">Interaction Design </a></li>
</ul>
</div>

<div style="float: left;  width: 15em; display: block; list-style: none;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/mobile-and-tablet-design/index.php">Mobile and Tablet Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/credibility-persuasion/index.php">Persuasive Design</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/research-methods/index.php">Research Methods</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/user-testing/index.php">User Testing </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/web-usability/index.php">Web Usability </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/writing-web/index.php">Web Writing &amp; Content Strategy </a></li>
</ul>
</div>

<p>&nbsp;</p>

                
            </section>
        </div>

        <div class="column small-12 medium-6">
            <div class="row">
                <div class="column small-6">
                    <section class="footer-about">
                        <h1>Follow Us</h1>
                        <ul>
	<li><a href="../subscribe/index.php">Subscribe to our weekly UX newsletter</a>,&nbsp;Jakob Nielsen&#39;s Alertbox</li>
	<li>Visit Don Norman&#39;s website, <a href="http://www.jnd.org/"> www.jnd.org </a></li>
	<li>Visit Bruce Tognazzini&#39;s website, <a href="http://www.asktog.com/"> www.asktog.com </a></li>
	<li style="padding-top: 6px;"><a class="twitter-follow-button" data-show-count="false" data-size="large" href="https://twitter.com/nngroup"><img alt="Follow @nngroup on Twitter" height="20" src="https://media.nngroup.com/media/editor/2016/12/30/twitter-follow-button.png" style="width: 117px; height: 20px;" width="117" /></a></li>
	<li style="padding-top: 6px;"><img alt="" height="14" src="https://media.nngroup.com/media/editor/2016/07/26/feed-icon-14x14.png" style="vertical-align:text-top;width:14px;margin-right:.25em;" width="14" /><a href="../../feed/rss/index.php">Subscribe to our RSS feed</a></li>
</ul>

                    </section>
                </div>

                <div class="column small-6">
                    <section class="footer-about right-column">
                        <h1>About
</h1>
                        <ul class="no-bullet">
	<li><a href="../../about/contact/index.php">Contact information</a></li>
	<li><a href="../../about/index.php">About Us</a></li>
	<li><a href="../../about/why-nng/index.php">Why NN/g? </a></li>
	<li><a href="../../return-policy/index.php">Return Policy</a></li>
</ul>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="copyright column small-12">
            <p><a href="../../copyright-and-reprint-info/index.php">Copyright</a> &copy; 1998-2017 Nielsen Norman Group, All Rights Reserved.</p>
        </div>
    </div>
</footer>


    


        <script src="https://media.nngroup.com/static/vendor/fastclick/js/fastclick.min.js"></script>
        <script src="https://media.nngroup.com/static/vendor/foundation/js/foundation.min.js"></script>
        <script src="http://media.nngroup.com/static/js/app.js"></script>
        <script src="http://media.nngroup.com/static/js/scroll-to-fixed.js"></script><script>var externalLinks = document.querySelectorAll('a[href^="http"]');for (var i = externalLinks.length-1; i >= 0; i--) {externalLinks[i].addEventListener("click", function(event) { alert("External links are disabled for this test."); event.preventDefault(); }, false);}var mailLinks = document.querySelectorAll('a[href^="mailto"]');for (var i = mailLinks.length-1; i >= 0; i--) {mailLinks[i].addEventListener("click", function(event) { alert("Email links are disabled for this test."); event.preventDefault(); }, false);}var externalForms = document.querySelectorAll('form');for (var i = externalForms.length-1; i >= 0; i--) {externalForms[i].addEventListener("submit", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false); externalForms[i].addEventListener("click", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false);}</script>

        <script>
            
            $(function() {
                $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
                $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
            });
        </script>

        
<script src="https://media.nngroup.com/static/js/articles.js"></script>


        
        <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1072699960;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072699960/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
    </body>

<!-- Mirrored from www.nngroup.com/articles/breaking-web-conventions/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:13:15 GMT -->
</html>
