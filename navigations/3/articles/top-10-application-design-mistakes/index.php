<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->





    
<!-- Mirrored from www.nngroup.com/articles/top-10-application-design-mistakes/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:12:56 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1026.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"beacon":"bam.nr-data.net","queueTime":25,"applicationTime":1815,"agent":"","licenseKey":"1a30ef9931","errorBeacon":"bam.nr-data.net","applicationID":"2048992","transactionName":"NFxSMRBUX0EDUkIPDA0WdhAMVkVbDV8ZBxEXUFMJB0YfRAtUQRVZIktEDAFZVHYHRVcPD01eVRE="}</script>
        <title>Top 10 Application-Design Mistakes</title><meta property="og:title" content="Top 10 Application-Design Mistakes" />
  
        
        <meta name="description" content="Application usability is enhanced when users know how to operate the UI and it guides them through the workflow. Violating common guidelines prevents both.">
        <meta property="og:description" content="Application usability is enhanced when users know how to operate the UI and it guides them through the workflow. Violating common guidelines prevents both." />
        
  
        
	
        
        <meta name="keywords" content="applications, functionality design, application usability, design mistakes, requirements, requirement specifications, specs, consistency, expectations, scrollbars, buttons, GUI controls, inconsistency, calendars, date selection, drag and drop, click target, click zones, clicking, affordances, perceived affordance, feedback, system state visibility, response times, busy cursor, progress bar, percent-done indicator, error messages, defaults, default values, ephemeral applications, conceptual model, workflow, display inertia">
        

        

        <meta name="robots" content="noarchive">
        <meta name="viewport" content="width=device-width">

        <link rel="search" type="application/opensearchdescription+xml" title="NN/g Search" href="../../opensearch.xml" />
        <link rel="shortcut icon" href="http://media.nngroup.com/static/img/favicon.ico" />

        <link rel="stylesheet" href="https://media.nngroup.com/static/vendor/font-awesome/css/font-awesome.min.css">

        
        <link rel="stylesheet" href="https://media.nngroup.com/static/css/7232a960f1df.css" type="text/css" />
        <link rel="stylesheet" href="http://fast.fonts.com/t/1.css?apiType=css&amp;projectid=c5846260-9def-46ec-9407-0f1349bd2d64" type="text/css">

        <script src="https://media.nngroup.com/static/vendor/modernizr/js/modernizr.min.js"></script>

        <script type="text/javascript" src="https://media.nngroup.com/static/js/3283be5def74.js"></script>
        

        

        
        
        <!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=229034,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('http://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->


<!-- Google Analytics Tracking Code for http://nngroup.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23132681-1', 'nngroup.com');
  ga('require', 'linkid', 'https://www.nngroup.com/articles/top-10-application-design-mistakes/linkid.js');
  ga('require', 'displayfeatures');

// Optimizely Universal Analytics Integration
 window.optimizely = window.optimizely || [];
 window.optimizely.push("activateUniversalAnalytics");

  ga('send', 'pageview');

</script>
<script>
/**
* Function to track a click on an outbound link in Google Analytics.
*/
function trackOutboundLink(label, url) {
    if(window.ga && ga.loaded) {
        ga('send', 'event', 'outbound', 'click', label, {'hitCallback': function(){document.location = url;}
        });
    } else {
        document.location = url;
    }
}
/**
* Function to track a click on a link in the article announcement area.
*/
$('#articleAnnouncement a').on('click', function(){
  ga('send', 'event', 'link', 'click', 'articleAnnouncement');
});
</script>

<script type="text/javascript">
 setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2421.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



<!-- Hotjar Tracking Code for http://nngroup.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:121605,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

        <script src="https://media.nngroup.com/static/js/analytics/track_outbound_link.js"></script>
        
        
        
    </head>
    <body class="location-articles articles-detail">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
    <header id="page-header">
        <div id="cart" class="shopping-cart mobile-cart show-for-small-down"></div>
        <div class="row">
            <div id="masthead" class="masthead small-12 medium-7 column">

                <!-- The button to toggles menu on small screen -->

                <h1><a href="../../index.php"><img src="http://media.nngroup.com/static/img/logo-wordmark.png" width="74" height="30"> Nielsen <span class="norman">Norman</span> Group</a></h1>
                <p class="tagline show-for-medium-up">Evidence-Based User Experience Research, Training, and Consulting</p>

                <!-- The button to toggles form search on small screen -->


                <div style="clear: right;width:  100%;height: 1px;"></div><?php define('ROOT_PATH',$_SERVER["DOCUMENT_ROOT"] . '/apps/navigation_styles/navigations/3'); ?><?php include(ROOT_PATH)."/navigation.php"; ?>

                <!-- Drop down menu on small screen -->
                <div id="hideNav" class="show-for-small-down">
                  
                  <ul class="menu-content">
                    <li><a href="../../index.php">Home</a></li>
                    <li class="has-submenu">
                      <a>Training</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../training/index.php">Overview</a></li>
                        
<li id="nav-ux-conference"><a href="../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../online-seminars/index.php"><span>Online Seminars</span></a></li>

                      </ul>
                    </li>
                    <li class="has-submenu">
                      <a>Consulting</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
  <li><a href="../../consulting/index.php">Overview</a></li>
  
  <li><a href="../../consulting/benchmarking/index.php">Benchmarking</a></li>
  
  <li><a href="../../consulting/expert-design-review/index.php">Expert Design Review</a></li>
  
  <li><a href="../../consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
  
  <li><a href="../../consulting/usability-testing/index.php">Usability Testing </a></li>
  
  <li><a href="../../consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
  
  <li><a href="../../in-house-training/index.php">In-House Training</a></li>
</ul>

                    </li>

                    <li><a href="../../reports/index.php">Reports</a></li>
                    <li><a href="../index.php">Articles</a></li>
                    <li class="has-submenu"><a>About NN/G</a><i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../about/index.php">Overview</a></li>
                        <li><a href="../../people/index.php">People</a></li>
                        <li><a href="../../about/why-nng/index.php">Why NN/g?</a></li>
                        <li><a href="../../about/contact/index.php">Contact</a></li>
                        <li><a href="../../news/index.php">News</a></li>
                        <li><a href="../../about/history/index.php">History</a></li>
                        <li><a href="../../books/index.php">Books</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                </div>

                <hr class="header-divider show-for-small-only"/>


            </div>
       </div>

        <div id="hideNav">            <nav class="primary-nav small-12 column">
                <ul>
                    <li><a class="navitem" id="nav-home" href="../../index.php">Home</a></li>
                    <li><a class="navitem" id="nav-training" href="../../training/index.php">Training</a></li>
                    <li><a class="navitem" id="nav-consulting" href="../../consulting/index.php">Consulting</a></li>
                    <li><a class="navitem" id="nav-reports" href="../../reports/index.php">Reports</a></li>
                    <li><a class="navitem" id="nav-articles" href="../index.php">Articles</a></li>
                    <li><a class="navitem" id="nav-about" href="../../about/index.php">About NN/G</a></li>
                </ul>
            </nav>
            <nav class="secondary-nav small-12 medium-12 large-12 columns">
                
                
            </nav>
        </div>
    </header>
    <div id="mainContent" class="l-content">
    
  

  

  <div class="row">
    
      
      <section class="small-12 medium-3 columns collapsable-content l-navrail articles-left-column">
        <h1 class="show-for-small-down collapse-link mobile-browse-header">Browse by Topic and Author</h1>
          <div class="row collapsable">

            
            <div class="small-6 medium-12 columns">
              <section id="article-popular-topics" style="display: none;">
                
                <h1>Topics</h1>
                <ul id="article-popular-topics">
                    
                    <li>
                        
                        <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/intranets/all/index.php">Intranets</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/user-testing/all/index.php">User Testing</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                        
                    </li>
                    
                    <li><a href="#" id="article-see-all-topics"><strong>See all topics&hellip;</strong></a></li>
                </ul>
                
              </section>
              <section id="article-all-topics" style="display: none;">
                
                <h1>
                  <span class="show-for-medium-up">All Article Topics</span>
                  <span class="show-for-small-down">All Topics</span>
                  (<a href="#" id="article-hide-all-topics">hide</a>)
                </h1>
                <ul>
                  
                  <li>
                    
                    <a href="../../topic/accessibility/all/index.php">Accessibility</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/agile/all/index.php">Agile</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/analytics-and-metrics/all/index.php">Analytics &amp; Metrics</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/applications/all/index.php">Application Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/b2b-websites/all/index.php">B2B Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/behavior-patterns/all/index.php">Behavior Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/branding/all/index.php">Branding</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/content-strategy/all/index.php">Content Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/corporate-websites/all/index.php">Corporate Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/design-patterns/all/index.php">Design Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-design-process/all/index.php">Design Process</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/email/all/index.php">Email</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/eyetracking/all/index.php">Eyetracking</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/heuristic-evaluation/all/index.php">Heuristic Evaluation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/human-computer-interaction/all/index.php">Human Computer Interaction</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-ideation/all/index.php">Ideation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/information-architecture/all/index.php">Information Architecture</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/interaction-design/all/index.php">Interaction Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/international-users/all/index.php">International Users</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/intranets/all/index.php">Intranets</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-management/all/index.php">Management</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/navigation/all/index.php">Navigation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/non-profit-websites/all/index.php">Non-Profit Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/personas/all/index.php">Personas</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/persuasive-design/all/index.php">Persuasive Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/prototyping/all/index.php">Prototyping</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/psychology-and-ux/all/index.php">Psychology and UX</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/research-methods/all/index.php">Research Methods</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/search/all/index.php">Search</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/social-media/all/index.php">Social Media</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/strategy/all/index.php">Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/user-testing/all/index.php">User Testing</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-teams/all/index.php">UX Teams</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/visual-design/all/index.php">Visual Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/writing-web/all/index.php">Writing for the Web</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/young-users/all/index.php">Young Users</a>
                    
                  </li>
                  
                </ul>
                
              </section>
            </div>
            <script>
              /* Determine which topics list will be displayed: the full or the
               * partial.  We are inlining this close to the list itself to
               * prevent rendering delays on the client
               */
              var topic_list = $.cookie("article-topics");

              var popular_topics = $('#article-popular-topics');
              var all_topics = $('#article-all-topics');

              if(topic_list == 1) {
                  popular_topics.hide();
                  all_topics.show();
              } else {
                  popular_topics.show();
                  all_topics.hide();
              }
            </script>
            

            
            <div class="small-6 medium-12 columns">
              
              <section id="article-initial-authors">
                  <h1>Author</h1>
                  <ul>
                    
                    <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                    
                    <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                    
                    <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                    
                    <li><a href="#" onClick="$('#article-initial-authors').hide(); $('#article-all-authors').show();"><strong>See all authors&hellip;</strong></a></li>
                  </ul>
              </section>
              <section id="article-all-authors" style="display: none;">
                <h1>
                  All Authors
                  (<a href="#" id="article-hide-all-authors">hide</a>)
                </h1>
                <ul>
                  
                  <li><a href="../author/raluca-budiu/index.php">Raluca Budiu</a></li>
                  
                  <li><a href="../author/patty-caya/index.php">Patty Caya</a></li>
                  
                  <li><a href="../author/susan-farrell/index.php">Susan Farrell</a></li>
                  
                  <li><a href="../author/therese-fessenden/index.php">Therese Fessenden</a></li>
                  
                  <li><a href="../author/kim-flaherty/index.php">Kim Flaherty</a></li>
                  
                  <li><a href="../author/sarah-gibbons/index.php">Sarah Gibbons</a></li>
                  
                  <li><a href="../author/aurora-harley/index.php">Aurora Harley</a></li>
                  
                  <li><a href="../author/kate-kaplan/index.php">Kate Kaplan</a></li>
                  
                  <li><a href="../author/page-laubheimer/index.php">Page Laubheimer</a></li>
                  
                  <li><a href="../author/angela-li/index.php">Angie Li</a></li>
                  
                  <li><a href="../author/hoa-loranger/index.php">Hoa Loranger</a></li>
                  
                  <li><a href="../author/kate-meyer/index.php">Kate Meyer</a></li>
                  
                  <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                  
                  <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                  
                  <li><a href="../author/kara-pernice/index.php">Kara Pernice</a></li>
                  
                  <li><a href="../author/christian-rohrer/index.php">Christian Rohrer</a></li>
                  
                  <li><a href="../author/amy-schade/index.php">Amy Schade</a></li>
                  
                  <li><a href="../author/katie-sherwin/index.php">Katie Sherwin</a></li>
                  
                  <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                  
                  <li><a href="../author/kathryn-whitenton/index.php">Kathryn Whitenton</a></li>
                  
                </ul>
              </section>
              
            </div>
            
          </div>

          
          <section class="hide-for-small-down">
            
              
              <h1>Recent Articles</h1>
              
              <ul>
                  
                  <li><a href="../seamless-cross-channel/index.php">Seamlessness in the Omnichannel User Experience</a></li>
                  
                  <li><a href="../brand-intention-interpretation/index.php">Experience Design: Bridging Brand Intention and Brand Interpretation</a></li>
                  
                  <li><a href="../eyetracking-tasks-efficient-scanning/index.php">Scanning Patterns on the Web Are Optimized for the Current Task</a></li>
                  
                  <li><a href="../flat-design-best-practices/index.php">Flat-Design Best Practices</a></li>
                  
                  <li><a href="../ux-user-stories/index.php">Accounting for UX Work with User Stories in Agile Projects</a></li>
                  
                  <li><a href="../index.php"><strong>See all articles&hellip;</strong></a></li>
              </ul>
              
            
          </section>
          

          
          
          <section class="hide-for-small-down">
            <h1>Popular Articles</h1>
            
            <ul>
                
                <li><a href="../usability-101-introduction-to-usability/index.php">Usability 101: Introduction to Usability</a></li>
                
                <li><a href="../top-10-mistakes-web-design/index.php">Top 10 Mistakes in Web Design</a></li>
                
                <li><a href="../how-users-read-on-the-web/index.php">How Users Read on the Web</a></li>
                
                <li><a href="../f-shaped-pattern-reading-web-content/index.php">F-Shaped Pattern For Reading Web Content</a></li>
                
                <li><a href="../ten-usability-heuristics/index.php">10 Usability Heuristics for User Interface Design</a></li>
                
                <li><a href="../intranet-design-2016/index.php">10 Best Intranets of 2016</a></li>
                
                <li><a href="../which-ux-research-methods/index.php">When to Use Which User-Experience Research Methods</a></li>
                
                <li><a href="../response-times-3-important-limits/index.php">Response Times: The 3 Important Limits</a></li>
                
                <li><a href="../why-you-only-need-to-test-with-5-users/index.php">Why You Only Need to Test with 5 Users</a></li>
                
                <li><a href="../page-fold-manifesto/index.php">The Fold Manifesto: Why the Page Fold Still Matters</a></li>
                
            </ul>
          </section>
          
          

          
            <section class="hide-for-small-down">
              
            </section>
          

      </section>
      

      
        <div class="small-12 medium-9 columns content-body">
          




<article class="l-subsection alertbox-article-content article-detail-bl">
  
  <header>
    <div class="l-subsection">
      <h1>Top 10 Application-Design Mistakes</h1>
      <div class="author-meta">by
        
          
            
            <a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a>
            
          
        on  February 19, 2008
        
      </div>

      

      
      <div class="topic-links">
        Topics:
        
          <ul>

  <li><a href="../../topic/applications/index.php">Application Design</a></li>

  <li><a href="../../topic/design-patterns/index.php">Design Patterns</a></li>

</ul>
        
      </div>
      
    </div>
  </header>
  

  
  
  <section>
    <p class="lede"><strong>Summary:</strong> Application usability is enhanced when users know how to operate the UI and it guides them through the workflow. Violating common guidelines prevents both.</p>
    <br />
  </section>
  
  

  
  <section id="article-body"><p>It's hard to write a general article about application design mistakes because the very <strong> worst mistakes are domain-specific </strong> and idiosyncratic. Usually, applications fail because they (a) solve the <strong> wrong problem</strong>, (b) have the <strong> wrong features </strong> for the right problem, or (c) make the right features too <strong> complicated </strong> for users to understand.</p>

<p>Any of these three mistakes will doom your app, and yet I still can't tell you what to do. What's the right problem? What are the right features? What complicating curlicues can safely be cut from those features? For each domain and user category, these questions have specific and very different answers.</p>

<p>The only generalizable advice is this: rather than <a class="old" href="../the-myth-of-the-genius-designer/index.php" title="Alertbox: The Myth of the Genius Designer"> rely on your own best guesses</a>, <strong> base your decisions on user research</strong>:</p>

<ul>
	<li>Conduct field studies and task analysis before deciding what your app should do.</li>
	<li><a class="new" href="../../courses/wireframing-and-prototyping/index.php" title="Nielsen Norman Group: 32-minute training video on Paper Prototyping ">Paper prototype</a> your initial ideas before doing any detailed design — and definitely before wasting resources implementing something you'd have to change as soon as you get user feedback.</li>
	<li><a class="old" href="http://www.useit.com/papers/iterative_design/" title="Jakob Nielsen: 4 case studies of iterative design">Design iteratively</a>, conducting many rounds of <a class="old" href="../why-you-only-need-to-test-with-5-users/index.php" title="Alertbox: Why You Only Need to Test With 5 Users"> quick user testing</a> as you refine your features.</li>
</ul>

<p>Of course, people don't want to hear me say that they need to <em> test </em> their UI. And they definitely don't want to hear that they have to actually move their precious butts to a customer location to watch real people do the work the application is supposed to support.</p>

<p>The general idea seems to be that real programmers can't be let out of their cages. My view is just the opposite: no one should be allowed to work on an application unless they've spent a day observing a few end users.</p>

<p>(Whatever you do, at least promise me this: Don't just implement feature requests from "user representatives" or "business analysts." The most common way to get usability wrong is to <a class="old" href="../first-rule-of-usability-dont-listen-to-users/index.php" title="Alertbox: First Rule of Usability? Don't Listen to Users "> listen to what users <em> say</em></a> rather than actually watching what they <em> do </em> . <strong> Requirement specifications are always wrong</strong>. You must prototype the requirements quickly and show users something concrete to find out what they really need.)</p>

<p>All that said, there are still plenty of general guidelines for application UIs — so many, in fact, that we have a hard time cramming the most important into our <a class="new" href="../../courses/topic/applications/index.php" title="Nielsen Norman Group training tutorial: Application Usability"> two-day course</a>. Here's my list of 10 usability violations that are both particularly egregious and often seen in a wide variety of applications.</p>

<h2>1. Non-Standard GUI Controls</h2>

<p>Basic GUI widgets — <a class="old" href="../command-links/index.php" title="Alertbox: Command Links"> command links</a> and buttons, <a class="old" href="../checkboxes-vs-radio-buttons/index.php" title="Alertbox: Checkboxes vs. Radio Buttons"> radio buttons and checkboxes</a>, <a class="old" href="../scrolling-and-scrollbars/index.php" title="Alertbox: Scrolling and Scrollbars"> scrollbars</a>, close boxes, and so on — are the lexical units that form <strong> dialog design's vocabulary</strong>. If you change the appearance or behavior of these units, it's like suddenly injecting foreign words into a natural-language communication. Det vil gøre læserne forvirrede (or, to revert to English: <em> Doing so will confuse readers</em>).</p>

<p>For some reason, homemade design's most common victims are <a class="old" href="../scrolling-and-scrollbars/index.php" title="Alertbox: Scrolling and Scrollbars"> scrollbars</a>. For years, we've encountered non-standard scrollbars in our studies, and they almost always cause users to <strong> overlook some of their options</strong>. We're <a class="old" href="../user-skills-improving-only-slightly/index.php" title="Alertbox: User Skills Improving, But Only Slightly"> seeing this again this year</a>, in the studies we're conducting to update our course on <a class="new" href="../../courses/web-page-design/index.php" title="Nielsen Norman Group: outline of full-day training course"> Fundamental Guidelines for Web Usability</a>. (The linked article includes screenshots of offending scroll controls.)</p>

<p>Some of the world's best interaction designers have refined the standard look-and-feel of GUI controls over 30 years, supported by thousands of user-testing hours. It's unlikely that you'll invent a better button over the weekend.</p>

<p>But even if your homemade design, seen in isolation, were hypothetically better than the standard, it's <strong> never seen in isolation </strong> in the real world. Your dialog controls will be used by people with years of experience operating standard GUIs.</p>

<p>If <strong> Jakob's Law </strong> is "users spend most of their time on <em> other </em> websites," then <strong> Jakob's Second Law </strong> is even more critical: "Users have several thousand times more experience with standard GUI controls than with any individual new design."</p>

<p>Users will most likely fail if you deviate from expectations on something as basic as the controls to operate a UI. And, even if they don't fail, they'll expend substantial brainpower trying to operate something that shouldn't require a second thought. Users' cognitive resources are better spent understanding how your application's features can help them achieve their goals.</p>

<h3>1.a. Looking Like a GUI Control Without Being One</h3>

<p>The opposite problem — having something that looks like a GUI control when it isn't one — can reduce usability even more. We often see text and headlines that look like links (by being <span style="color: blue"> colored </span> or <span style="text-decoration: underline"> underlined</span>, for example) but aren't clickable. When users click these look-alikes and nothing happens, they think the site is broken. (So please comply with guidelines for <a class="old" href="../guidelines-for-visualizing-links/index.php" title="Alertbox: Guidelines for Visualizing Links"> visualizing links</a>.)</p>

<p>A similar problem occurs when something <strong> looks like a button but doesn't initiate an action </strong> , or looks like a radio button but isn't a choice. We found an example of this in our current round of studies.</p>

<p>To design a custom-tailored shirt on Liste Rouge Paris, you must provide your measurements. As the following screenshot shows, there are two different paths through the application here, depending on whether your measurements are already on file with the tailor.</p>

<p style="text-align:center"><img alt="Partial screenshot of ordering process for custom-tailored shirts at www.listerouge-paris.com" height="521" src="http://media.nngroup.com/media/editor/alertbox/listerouge-buttons-as-headings.gif" width="529"/></p>

<p>Our test user clicked incessantly on the <em> New Customer </em> button to indicate that he was indeed a new customer. Unfortunately, this screen element was not a button at all, but rather a non-clickable heading.</p>

<p>He was the only user to test this site because he encountered it during a task in which users could choose a site to visit (usually from a search listing). In this case, the user eventually overcame the confusion and proceeded to enter his measurements. If we had tested more users, a small percentage would have likely failed at this point. Each small error in dialog design reduces usage only by a small amount, but most UIs contain <strong> bundles of errors</strong>, and the <strong> number of lost customers adds up</strong>.</p>

<p>As an aside, this screen also uses radio buttons incorrectly. In theory, all five choices are mutually exclusive, which does call for radio buttons. But in the user's mental model of the workflow, there are actually <em> two issues </em> here: (a) new vs. old customers, and (b) how to provide the measurements for your situation. You should use a single set of radio buttons only when users will choose between options for a single issue.</p>

<p>So, in the case above, a better design would first ask users to decide the new/existing customer question, and then reveal the relevant radio buttons for the option they choose.</p>

<h2>2. Inconsistency</h2>

<p>Non-standard GUI controls are a special case of the general problem of inconsistent design.</p>

<p>Confusion results when applications use different words or commands for the same thing, or when they use the same word for multiple concepts in different parts of the application. Similarly, users are confused when things move around, violating <strong> display inertia</strong>.</p>

<p>Using the <strong> same name for the same thing in the same place </strong> makes things easy.</p>

<p>Remember the double-D rule: <strong> differences are difficult</strong>.</p>

<p>Another example from our current study: Expedia pops up a two-month calendar view when users specify the departure or return date for a trip. The composite screenshot below was taken in February and shows what happens when you want to book a trip that starts on March 10 and ends on March 15.</p>

<p style="text-align:center"><img alt="Two screenshots of date-selection widget (calendar) at Expedia.com" height="219" src="http://media.nngroup.com/media/editor/alertbox/expedia-inconsistent-calendar.gif" width="798"/></p>

<p>In the second pop-up, the month of March has moved to the left, leaving room for April to appear on the right. This may seem like a convenient shortcut, since there's no way the user would want a February return date when traveling out in March.</p>

<p>In reality, however, the user is looking for March 15 in the same spot where it appeared in the first pop-up calendar: in the right-most column.</p>

<p>In our testing, the inconsistent placement of the months in the second pop-up caused confusion and delays, but users ultimately figured it out. We tested only a few users with this site, but if you observe this kind of <strong> almost-miss error </strong> in user testing, it's usually a sign that a few users will make the mistake for real during actual use.</p>

<p>Booking the wrong return date can have disastrous consequences — customers could arrive at the airport without a ticket for their expected flight. If a site has <a class="old" href="http://www.useit.com/alertbox/20031208.php" title="Alertbox: Confirmation Email, Automated Customer Service Email, and Transactional Messages"> good confirmation emails</a>, users might discover the problem before departure, but even that will cause aggravation and expensive customer support calls to resolve the situation.</p>

<p>Even if people eventually use the calendar correctly, it <strong> takes more time to ponder the inconsistent design </strong> than the time users save by not having to click the next-month button for April departures.</p>

<p>The shortcut that moves the months around saves time only for very frequent users who learn how to efficiently operate this part of the UI. So, an application for professional travel agents should probably use Expedia's calendar design. A site targeting average consumers should not.</p>

<h2>3. No Perceived Affordance</h2>

<p>"Affordance" means what you can do to an object. For example, a checkbox affords turning on and off, and a slider affords moving up or down. " <em> Perceived </em> affordances" are actions you understand just by <em> looking </em> at the object, before you start using it (or feeling it, if it's a physical device rather than an on-screen UI element). All of this is discussed in Don Norman's book <a class="old" href="http://www.amazon.com/dp/0465067107?tag=useitcomusablein" title="Amazon.com: info about this book"> <cite> The Design of Everyday Things</cite></a>.</p>

<p>Perceived affordances are especially important in UI design, because all screen pixels afford clicking — even though nothing usually happens if you click. There are so many visible things on a computer screen that users don't have time for a <strong> mine sweeping </strong> game, clicking around hoping to find something actionable. (Exception: <a class="old" href="../childrens-websites-usability-issues/index.php" title="Alertbox: Kids' Corner - Website Usability for Children"> small children</a> sometimes like to explore screens by clicking around.)</p>

<p><strong>Drag-and-drop </strong> designs are often the worst offenders when it's not apparent that something can be dragged or where something can be dropped. (Or what will happen if you do drag or drop.) In contrast, simple checkboxes and command buttons usually make it painfully obvious what you can click.</p>

<p>Common <strong> symptoms </strong> of the lack of perceived affordances are:</p>

<ul>
	<li>Users say, "What do I do here?"</li>
	<li>Users don't go near a feature that would help them.</li>
	<li>A profusion of screen text tries to overcome these two problems. (Even worse are verbose, multi-stage instructions that disappear after you perform the first of several actions.)</li>
</ul>

<p>When I tested some of the first Macintosh applications in the mid-1980s, users were often stumped by the empty screen that appeared when they launched, say, MacWrite. <em> What do I do here </em> , indeed. The first step was supposed to be to create a new document, but that command was not shown anywhere in the otherwise highly visible Macintosh UI unless you happened to pull down the <em> File </em> menu. Later application releases opened up with a blank document on the screen, complete with an inviting, blinking insertion point that provided the perceived affordance for "start typing."</p>

<h3>3.a. Tiny Click Targets</h3>

<p>An associated problem here is click targets that are so small that users miss and click outside the active area. Even if they originally perceived the associated affordance correctly, users often change their mind and start believing that something isn't actionable because they think they clicked it and nothing happened.</p>

<p>(Small click zones are a particular problem for <a class="old" href="../usability-for-senior-citizens/index.php" title="Alertbox: Usability for Senior Citizens"> old users</a> and users with motor skill <a class="old" href="../beyond-accessibility-treating-users-with-disabilities-as-people/index.php" title="Alertbox: Beyond Accessibility - Treating Users with Disabilities as People"> disabilities</a>.)</p>

<h2>4. No Feedback</h2>

<p>One of the most basic guidelines for improving a dialog's usability is to provide feedback:</p>

<ul>
	<li>Show users the system's current state.</li>
	<li>Tell users how their commands have been interpreted.</li>
	<li>Tell users what's happening.</li>
</ul>

<p>Sites that keep quiet leave users guessing. Often, they guess wrong.</p>

<p>(For an example of the problems with poor feedback, see the screenshot of VW's car configurator toward the bottom of my recent <a class="old" href="../user-skills-improving-only-slightly/index.php" title="Alertbox: User Skills Improving, But Only Slightly"> article reporting on our current round of testing</a>: Because users couldn't tell which tire was selected, they had trouble designing their preferred car.)</p>

<h3>4.a. Out to Lunch Without a Progress Indicator</h3>

<p>A variant on lack of feedback is when a system fails to notify users that it's taking a long time to complete an action. Users often think that the application is broken, or they start clicking on new actions.</p>

<p>If you can't meet the recommended <a class="old" href="http://www.useit.com/papers/responsetime.php" title="Jakob Nielsen: Response Times - The Three Important Limits"> response time limits</a>, say so, and keep users informed about what's going on:</p>

<ul>
	<li>If a command takes more than <strong> 1 second</strong>, show the <strong> "busy" cursor</strong>. This tells users to hold their horses and not click on anything else until the normal cursor returns.</li>
	<li>If a command takes more than <strong> 10 seconds</strong>, put up an explicit <strong> progress bar</strong>, preferably as a percent-done indicator (unless you truly can't predict how much work is left until the operation is done).</li>
</ul>

<h2>5. Bad Error Messages</h2>

<p>Error messages are a special form of feedback: they tell users that something has gone wrong. We've known the <a class="old" href="../error-message-guidelines/index.php" title="Alertbox: Error Message Guidelines"> guidelines for error messages</a> for almost 30 years, and yet many applications still violate them.</p>

<p>The most common guideline violation is when an error message simply says something is wrong, without <strong> explaining why and how </strong> the user can fix the problem. Such messages leave users stranded.</p>

<p>Informative error messages not only help users fix their current problems, they can also serve as a <strong> teachable moment</strong>. Typically, users won't invest time in reading and learning about features, but they will spend the time to understand an error situation if you explain it clearly, because they want to overcome the error.</p>

<p>On the Web, there's a second common problem with error messages: people overlook them on most Web pages because they're buried in masses of junk. Obviously, having simpler pages is one way to alleviate this problem, but it's also necessary to <strong> make error messages more prominent </strong> in Web-based UIs.</p>

<h2>6. Asking for the Same Info Twice</h2>

<p>Users shouldn't have to enter the same information more than once. After all, computers are pretty good at remembering data. The only reason users have to repeat themselves is because programmers get lazy and don't transfer the answers from one part of the app to another.</p>

<h2>7. No Default Values</h2>

<p>Defaults help users in many ways. Most importantly, defaults can:</p>

<ul>
	<li><strong>speed up </strong> the interaction by freeing users from having to specify a value if the default is acceptable;</li>
	<li><strong>teach, by example</strong>, the type of answer that is appropriate for the question; and</li>
	<li><strong>direct novice users </strong> toward a safe or common outcome, by letting them accept the default if they don't know what else to do.</li>
</ul>

<p>Because I used Liste Rouge Paris as a bad example under Mistake #1a, I thought I'd play nice and use them as a good example here. The tailor offers 15 different collar styles (among many other options) for people ordering custom-designed shirts. Luckily, they also provide good defaults for each of the many choices. In testing, this proved helpful to our first-time user, because the defaults steered him toward the most common or appropriate options when he didn't have a particular preference.</p>

<p> </p>

<p style="text-align: center;"><img alt="Partial screenshot of customization screen in the shirt design application on www.listerouge-paris.com" height="158" src="http://media.nngroup.com/media/editor/alertbox/listerouge-default-collar.jpg" width="434"/><br/>
<em>Dialog to specify your shirt's collar on www.listerouge-paris.com (3 of 15 styles shown). </em></p>

<h2>8. Dumping Users into the App</h2>

<p>Most Web-based applications are <a class="old" href="../ephemeral-web-based-applications/index.php" title="Alertbox: Ephemeral Web-Based Applications"> ephemeral applications</a> that users encounter as a by-product of their surfing. Even if users deliberately seek out a new app, they often approach it without a <strong> conceptual model </strong> of how it works. People don't know the workflow or the steps, they don't know the expected outcome, and they don't know the basic concepts that they'll be manipulating.</p>

<p>For traditional applications, this is less of a problem. Even if someone has never used PowerPoint, they've probably seen a slide presentation. Thus, a new PowerPoint user will typically have at least a bare-bones understanding of the application before double-clicking the icon for the first time.</p>

<p>For mission-critical applications, you can often assume that most users have tried the app many times before. You can also often assume that new users will get some training before seeing the UI on their own. At the minimum, they'll usually have nearby colleagues who can give them a few pointers on the basics. And a good boss will give new hires some background info as to <em> why </em> they're being asked to use the application and <em> what </em> they're supposed to accomplish with it.</p>

<p>Sadly, none of these aides to understanding apply for most Web-based applications. They don't even apply for many ephemeral <a class="new" href="../../reports/intranet-usability-guidelines/index.php" title="Nielsen Norman Group report: Intranet Usability Guidelines, vol. 10 - Killer Apps"> intranet applications</a>.</p>

<p>Usability suffers when users are dumped directly into an application's guts without any set-up to give them an idea of what's going to happen. Unfortunately, most <a class="old" href="../how-users-read-on-the-web/index.php" title="Alertbox: How Users Read on the Web"> users won't read</a> a lot of upfront instructions, so you might have to offer them in a short bulleted list or through a single image that lets them grok the application's main point in one view.</p>

<p>As an example, our test user who was trying to order a custom-tailored shirt was highly confused when the first screen in Hamilton Shirts' "Create Your Shirt" process displayed a fully designed shirt with an "Add to Bag" button. This screen mixed two metaphors: a configurator and an e-commerce product screen.</p>

<p style="text-align:center"><img alt="Screenshot of the upper part of the screen for the first step of Hamilton's shirt-design application" height="382" src="http://media.nngroup.com/media/editor/alertbox/hamilton-create-your-shirt-step1.jpg" width="500"/></p>

<p>This is a case where a default value isn't helpful: people who want to design their own shirt are unlikely to want to buy a pre-designed shirt on the first screen.</p>

<p>(This screen also suffers from Mistake #1: non-standard GUI controls. In addition to its non-standard drop-down selection menus in a tabbed dialog that doesn't <a class="old" href="../tabs-used-right/index.php" title="Alertbox: Tabs, Used Right"> look enough like tabs</a>, the screen has a non-standard way of paging through additional fabric swatches. Users are less likely to understand how to select fabrics when the controls are presented in this manner.)</p>

<p>Our test user never understood the process of designing his own shirt on this site and ultimately took his business elsewhere.</p>

<h2>9. Not Indicating How Info Will Be Used</h2>

<p>The worst instance of forcing users through a workflow without making the outcome clear is worth singling out as a separate mistake: Asking users to enter information without telling them what you'll use it for.</p>

<p>A classic example is the "nickname" field in the registration process for a bulletin board application. Many users don't realize the nickname will be used to identify them in their postings for the rest of eternity — so they often enter something inappropriate.</p>

<p>As another example, we once tested an e-commerce site that smacked users with a demand for their ZIP code before they could view product pages. This was a big turn-off and many users left the site due to privacy concerns. People hate snoopy sites. An alternative design worked much better: It explained that the site needed to know the user's location so it could state shipping charges for the very heavy products in question.</p>

<h2>10. System-Centric Features</h2>

<p>Too many applications expose their dirty laundry, offering features that reflect the system's internal view of the data rather than users' understanding of the problem space.</p>

<p>In our current study, one user wanted to reallocate her retirement savings among various investments offered by her company's plan (for example, to invest more in bonds and less in stocks). She thought she did this correctly, but in fact she had changed only the allocation of <em> future additions </em> to her retirement account. Her existing investments remained unchanged.</p>

<p>As far as the mutual funds company is concerned, new investments and current investments are treated differently. Reallocating future additions means changing the funds they'll buy when the employer transfers money into the account. Reallocating current investments means selling some of the holdings in existing mutual funds and using the proceeds to buy into other funds.</p>

<p>The key insights here?</p>

<ul>
	<li>Our test user didn't have this distinction between new and old money; she simply wanted her retirement savings allocated according to her revised investment strategy.</li>
	<li>Even users who understand the distinction between new and old money might prefer to treat their retirement savings as a single unit rather than make separate decisions (and issue separate commands) for the new and old money.</li>
</ul>

<p>It would probably be better to offer a prominent feature for changing the entire account's allocation, and use <a class="old" href="../progressive-disclosure/index.php" title="Alertbox: Progressive Disclosure"> progressive disclosure</a> to reveal expert settings for users who want to make the more detailed distinction between the two classes of money.</p>

<h2>Bonus Mistake: <em> Reset </em> Button on Web Forms</h2>

<p>This mistake relates to Web forms, but because so many applications are rich in forms, I'll mention it here: It's almost always <a class="old" href="../reset-and-cancel-buttons/index.php" title="Alertbox: Reset and Cancel Buttons"> wrong to have a <em> Reset </em> button on a Web form</a>.</p>

<p>The reset button clears the user's entire input and returns the form to its pristine state. Users would want that only if they're repeatedly completing the same form with completely different data, which almost never happens on websites. (Call center operators are a different matter.)</p>

<p>Making it easy for users to <strong> destroy their work in a single click </strong> violates one of the most basic usability principles, which is to respect and protect the user's work at almost any cost. (That's why you need <strong> confirmation dialogs </strong> for the most destructive actions.)</p>
</section>
  

  
  <section>
    
        
    
  </section>
  
  
  
  
  

  


  <section>
    <br />
    <p class="share"><b>Share <span class="hide-for-small-down">this article</span>:</b> <a href="https://twitter.com/intent/tweet?url=http://www.nngroup.com/articles/top-10-application-design-mistakes/&amp;text=Top%2010%20Application-Design%20Mistakes&amp;via=nngroup">Twitter</a> | <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=http://www.nngroup.com/articles/top-10-application-design-mistakes/&amp;title=Top%2010%20Application-Design%20Mistakes&amp;source=Nielsen%20Norman%20Group">LinkedIn</a> | <a href="https://plus.google.com/share?url=http://www.nngroup.com/articles/top-10-application-design-mistakes/">Google+</a> | <a href="mailto:?subject=NN/g Article: Top 10 Application-Design Mistakes&amp;body=http://www.nngroup.com/articles/top-10-application-design-mistakes/">Email</a></p>
    <hr />
  </section>

  <footer class="l-subsection">
    
    
    <div class="learn-more">
      <h2>Learn More</h2>
      <p><a href="../subscribe/index.php">Subscribe to the weekly newsletter</a> to get notified about future articles.</p>
      </div>
    
    

    <div class="row show-for-small-down related-content">
      
      
      

      
      
        <div class="column small-12 topic-links">All articles about:
          <ul>
            
            <li><a href="../../topic/applications/index.php">Application Design</a></li>
            
            <li><a href="../../topic/design-patterns/index.php">Design Patterns</a></li>
            
          </ul>
        </div>
      
      

      
      
        <div class="column small-12">
          <h3>UX Conference Training Courses</h3>
           <ul class="no-bullet">
    
        <li><a href="../../courses/emerging-patterns-web-design/index.php">Emerging Patterns for Web Design</a></li>
    
        <li><a href="../../courses/web-page-design/index.php">Web Page UX Design</a></li>
    
        <li><a href="../../courses/application-ux/index.php">Application Design for Web and Desktop</a></li>
    
        <li><a href="../../courses/emotional-design/index.php">Emotional Design</a></li>
    
</ul>
        </div>
      
      

      
      
        <div class="small-12 column">
          <h3>Research Reports</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../reports/best-applications-2/index.php">Application Design Showcase: 2012</a></li>
              
            
              
                <li><a href="../../reports/enterprise-mobile-showcase/index.php">Mobile Intranets and Enterprise Apps</a></li>
              
            
              
                <li><a href="../../reports/website-tools-and-applications-flash/index.php">Website Tools and Applications with Flash</a></li>
              
            
              
                <li><a href="../../reports/customization-features/index.php">Customization Features Done Correctly for the Right Reasons</a></li>
              
            
              
                <li><a href="../../reports/best-applications-1/index.php">Application Design Showcase: 2008</a></li>
              
            
          </ul>
        </div>
      
      

      
      
        <div class="small-12 column">
          <h3>Online Seminars</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../online-seminars/ux-big-data/index.php">UX for Big Data</a></li>
              
            
          </ul>
        </div>
      
      
    </div>

    <div class="hide-for-small-down row related-content">
      <div class="medium-6 columns">
        
        
          <div>
            <h3>Articles</h3>
            <ul class="no-bullet">
              
                
                <li><a href="../tabs-used-right/index.php">Tabs, Used Right</a></li>
                
              
                
                <li><a href="../indicators-validations-notifications/index.php">Indicators, Validations, and Notifications: Pick the Correct Communication Option</a></li>
                
              
                
                <li><a href="../passwords-memory/index.php">Help People Create Passwords That They Can Actually Remember</a></li>
                
              
                
                <li><a href="../mobile-instructional-overlay/index.php">Instructional Overlays and Coach Marks for Mobile Apps</a></li>
                
              
                
                <li><a href="../cards-component/index.php">Cards: UI-Component Definition</a></li>
                
              
            </ul>
          </div>
        
        

        
          
          <div>
            <h3>Research Reports</h3>
            <ul class="no-bullet">
              
                
                  <li><a href="../../reports/best-applications-2/index.php">Application Design Showcase: 2012</a></li>
                
              
                
                  <li><a href="../../reports/enterprise-mobile-showcase/index.php">Mobile Intranets and Enterprise Apps</a></li>
                
              
                
                  <li><a href="../../reports/website-tools-and-applications-flash/index.php">Website Tools and Applications with Flash</a></li>
                
              
                
                  <li><a href="../../reports/customization-features/index.php">Customization Features Done Correctly for the Right Reasons</a></li>
                
              
                
                  <li><a href="../../reports/best-applications-1/index.php">Application Design Showcase: 2008</a></li>
                
              
            </ul>
          </div>
          
        

      </div>
      <div class="medium-6 columns">
        
          
          <div>
            <h3>UX Conference Training Courses</h3>
             <ul class="no-bullet">
    
        <li><a href="../../courses/emerging-patterns-web-design/index.php">Emerging Patterns for Web Design</a></li>
    
        <li><a href="../../courses/web-page-design/index.php">Web Page UX Design</a></li>
    
        <li><a href="../../courses/application-ux/index.php">Application Design for Web and Desktop</a></li>
    
        <li><a href="../../courses/emotional-design/index.php">Emotional Design</a></li>
    
</ul>
          </div>
          
        
        
          
          <div>
            <h3>Online Seminars</h3>
            <ul class="no-bullet">
            
                <li><a href="../../online-seminars/ux-big-data/index.php">UX for Big Data</a></li>
            
            </ul>
          </div>
          
        
      </div>
    </div>
  </footer>
</article>

        </div>

      

    
  </div>
  <div class="row">
    
      <div class="column small-12 show-for-small-down l-navrail announcements">
        
          
        
      </div>
    
    
  </div>

    </div>
    




<footer id="page-footer">
    <div class="row hide-for-print">
        <div class="column small-12 medium-6">
            <section class="footer-popular">
                
                <h1>Popular Topics</h1>
                <div style="float: left;  width: 15em; display: block;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/applications/index.php">Application Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/e-commerce/index.php">E-Commerce Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/eyetracking/index.php">Eyetracking </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/information-architecture/index.php">Information Architecture </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/intranets/index.php">Intranet Design </a></li>
	<li><a href="../../topic/interaction-design/index.php">Interaction Design </a></li>
</ul>
</div>

<div style="float: left;  width: 15em; display: block; list-style: none;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/mobile-and-tablet-design/index.php">Mobile and Tablet Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/credibility-persuasion/index.php">Persuasive Design</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/research-methods/index.php">Research Methods</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/user-testing/index.php">User Testing </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/web-usability/index.php">Web Usability </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/writing-web/index.php">Web Writing &amp; Content Strategy </a></li>
</ul>
</div>

<p>&nbsp;</p>

                
            </section>
        </div>

        <div class="column small-12 medium-6">
            <div class="row">
                <div class="column small-6">
                    <section class="footer-about">
                        <h1>Follow Us</h1>
                        <ul>
	<li><a href="../subscribe/index.php">Subscribe to our weekly UX newsletter</a>,&nbsp;Jakob Nielsen&#39;s Alertbox</li>
	<li>Visit Don Norman&#39;s website, <a href="http://www.jnd.org/"> www.jnd.org </a></li>
	<li>Visit Bruce Tognazzini&#39;s website, <a href="http://www.asktog.com/"> www.asktog.com </a></li>
	<li style="padding-top: 6px;"><a class="twitter-follow-button" data-show-count="false" data-size="large" href="https://twitter.com/nngroup"><img alt="Follow @nngroup on Twitter" height="20" src="https://media.nngroup.com/media/editor/2016/12/30/twitter-follow-button.png" style="width: 117px; height: 20px;" width="117" /></a></li>
	<li style="padding-top: 6px;"><img alt="" height="14" src="https://media.nngroup.com/media/editor/2016/07/26/feed-icon-14x14.png" style="vertical-align:text-top;width:14px;margin-right:.25em;" width="14" /><a href="../../feed/rss/index.php">Subscribe to our RSS feed</a></li>
</ul>

                    </section>
                </div>

                <div class="column small-6">
                    <section class="footer-about right-column">
                        <h1>About
</h1>
                        <ul class="no-bullet">
	<li><a href="../../about/contact/index.php">Contact information</a></li>
	<li><a href="../../about/index.php">About Us</a></li>
	<li><a href="../../about/why-nng/index.php">Why NN/g? </a></li>
	<li><a href="../../return-policy/index.php">Return Policy</a></li>
</ul>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="copyright column small-12">
            <p><a href="../../copyright-and-reprint-info/index.php">Copyright</a> &copy; 1998-2017 Nielsen Norman Group, All Rights Reserved.</p>
        </div>
    </div>
</footer>


    


        <script src="https://media.nngroup.com/static/vendor/fastclick/js/fastclick.min.js"></script>
        <script src="https://media.nngroup.com/static/vendor/foundation/js/foundation.min.js"></script>
        <script src="http://media.nngroup.com/static/js/app.js"></script>
        <script src="http://media.nngroup.com/static/js/scroll-to-fixed.js"></script><script>var externalLinks = document.querySelectorAll('a[href^="http"]');for (var i = externalLinks.length-1; i >= 0; i--) {externalLinks[i].addEventListener("click", function(event) { alert("External links are disabled for this test."); event.preventDefault(); }, false);}var mailLinks = document.querySelectorAll('a[href^="mailto"]');for (var i = mailLinks.length-1; i >= 0; i--) {mailLinks[i].addEventListener("click", function(event) { alert("Email links are disabled for this test."); event.preventDefault(); }, false);}var externalForms = document.querySelectorAll('form');for (var i = externalForms.length-1; i >= 0; i--) {externalForms[i].addEventListener("submit", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false); externalForms[i].addEventListener("click", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false);}</script>

        <script>
            
            $(function() {
                $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
                $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
            });
        </script>

        
<script src="https://media.nngroup.com/static/js/articles.js"></script>


        
        <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1072699960;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072699960/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
    </body>

<!-- Mirrored from www.nngroup.com/articles/top-10-application-design-mistakes/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:13:01 GMT -->
</html>
