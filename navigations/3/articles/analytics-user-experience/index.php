<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->





    
<!-- Mirrored from www.nngroup.com/articles/analytics-user-experience/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:10:33 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1026.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"licenseKey":"1a30ef9931","queueTime":0,"beacon":"bam.nr-data.net","errorBeacon":"bam.nr-data.net","applicationID":"2048992","transactionName":"NFxSMRBUX0EDUkIPDA0WdhAMVkVbDV8ZBxEXUFMJB0YfRAtUQRVZIktEDAFZVHYHRVcPD01eVRE=","applicationTime":394,"agent":""}</script>
        <title>3 Uses for Analytics in User Experience Practice</title><meta property="og:title" content="3 Uses for Analytics in User Experience Practice" />
  
        
        <meta name="description" content="In order to make the most of analytics data, UX professionals need to integrate this data where it can add value to qualitative processes instead of distract resources.">
        <meta property="og:description" content="In order to make the most of analytics data, UX professionals need to integrate this data where it can add value to qualitative processes instead of distract resources." />
        
  
        
	
        
        <meta name="keywords" content="analytics, statistics, data analysis, traffic analysis, quantitative data, Google Analytics, metrics, UX metrics, conversions, triangulation, usability triangulation, traffic, web traffic, site traffic, traffic analysis, traffic statistics, statistics, UX statistics">
        

        

        <meta name="robots" content="noarchive">
        <meta name="viewport" content="width=device-width">

        <link rel="search" type="application/opensearchdescription+xml" title="NN/g Search" href="../../opensearch.xml" />
        <link rel="shortcut icon" href="http://media.nngroup.com/static/img/favicon.ico" />

        <link rel="stylesheet" href="https://media.nngroup.com/static/vendor/font-awesome/css/font-awesome.min.css">

        
        <link rel="stylesheet" href="https://media.nngroup.com/static/css/7232a960f1df.css" type="text/css" />
        <link rel="stylesheet" href="http://fast.fonts.com/t/1.css?apiType=css&amp;projectid=c5846260-9def-46ec-9407-0f1349bd2d64" type="text/css">

        <script src="https://media.nngroup.com/static/vendor/modernizr/js/modernizr.min.js"></script>

        <script type="text/javascript" src="https://media.nngroup.com/static/js/3283be5def74.js"></script>
        

        

        
        
        <!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=229034,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('http://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->


<!-- Google Analytics Tracking Code for http://nngroup.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23132681-1', 'nngroup.com');
  ga('require', 'linkid', 'https://www.nngroup.com/articles/analytics-user-experience/linkid.js');
  ga('require', 'displayfeatures');

// Optimizely Universal Analytics Integration
 window.optimizely = window.optimizely || [];
 window.optimizely.push("activateUniversalAnalytics");

  ga('send', 'pageview');

</script>
<script>
/**
* Function to track a click on an outbound link in Google Analytics.
*/
function trackOutboundLink(label, url) {
    if(window.ga && ga.loaded) {
        ga('send', 'event', 'outbound', 'click', label, {'hitCallback': function(){document.location = url;}
        });
    } else {
        document.location = url;
    }
}
/**
* Function to track a click on a link in the article announcement area.
*/
$('#articleAnnouncement a').on('click', function(){
  ga('send', 'event', 'link', 'click', 'articleAnnouncement');
});
</script>

<script type="text/javascript">
 setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2421.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



<!-- Hotjar Tracking Code for http://nngroup.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:121605,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

        <script src="https://media.nngroup.com/static/js/analytics/track_outbound_link.js"></script>
        
        
        
    </head>
    <body class="location-articles articles-detail">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
    <header id="page-header">
        <div id="cart" class="shopping-cart mobile-cart show-for-small-down"></div>
        <div class="row">
            <div id="masthead" class="masthead small-12 medium-7 column">

                <!-- The button to toggles menu on small screen -->

                <h1><a href="../../index.php"><img src="http://media.nngroup.com/static/img/logo-wordmark.png" width="74" height="30"> Nielsen <span class="norman">Norman</span> Group</a></h1>
                <p class="tagline show-for-medium-up">Evidence-Based User Experience Research, Training, and Consulting</p>

                <!-- The button to toggles form search on small screen -->


                <div style="clear: right;width:  100%;height: 1px;"></div><?php define('ROOT_PATH',$_SERVER["DOCUMENT_ROOT"] . '/apps/navigation_styles/navigations/3'); ?><?php include(ROOT_PATH)."/navigation.php"; ?>

                <!-- Drop down menu on small screen -->
                <div id="hideNav" class="show-for-small-down">
                  
                  <ul class="menu-content">
                    <li><a href="../../index.php">Home</a></li>
                    <li class="has-submenu">
                      <a>Training</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../training/index.php">Overview</a></li>
                        
<li id="nav-ux-conference"><a href="../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../online-seminars/index.php"><span>Online Seminars</span></a></li>

                      </ul>
                    </li>
                    <li class="has-submenu">
                      <a>Consulting</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
  <li><a href="../../consulting/index.php">Overview</a></li>
  
  <li><a href="../../consulting/benchmarking/index.php">Benchmarking</a></li>
  
  <li><a href="../../consulting/expert-design-review/index.php">Expert Design Review</a></li>
  
  <li><a href="../../consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
  
  <li><a href="../../consulting/usability-testing/index.php">Usability Testing </a></li>
  
  <li><a href="../../consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
  
  <li><a href="../../in-house-training/index.php">In-House Training</a></li>
</ul>

                    </li>

                    <li><a href="../../reports/index.php">Reports</a></li>
                    <li><a href="../index.php">Articles</a></li>
                    <li class="has-submenu"><a>About NN/G</a><i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../about/index.php">Overview</a></li>
                        <li><a href="../../people/index.php">People</a></li>
                        <li><a href="../../about/why-nng/index.php">Why NN/g?</a></li>
                        <li><a href="../../about/contact/index.php">Contact</a></li>
                        <li><a href="../../news/index.php">News</a></li>
                        <li><a href="../../about/history/index.php">History</a></li>
                        <li><a href="../../books/index.php">Books</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                </div>

                <hr class="header-divider show-for-small-only"/>


            </div>
       </div>

        <div id="hideNav">            <nav class="primary-nav small-12 column">
                <ul>
                    <li><a class="navitem" id="nav-home" href="../../index.php">Home</a></li>
                    <li><a class="navitem" id="nav-training" href="../../training/index.php">Training</a></li>
                    <li><a class="navitem" id="nav-consulting" href="../../consulting/index.php">Consulting</a></li>
                    <li><a class="navitem" id="nav-reports" href="../../reports/index.php">Reports</a></li>
                    <li><a class="navitem" id="nav-articles" href="../index.php">Articles</a></li>
                    <li><a class="navitem" id="nav-about" href="../../about/index.php">About NN/G</a></li>
                </ul>
            </nav>
            <nav class="secondary-nav small-12 medium-12 large-12 columns">
                
                
            </nav>
        </div>
    </header>
    <div id="mainContent" class="l-content">
    
  

  

  <div class="row">
    
      
      <section class="small-12 medium-3 columns collapsable-content l-navrail articles-left-column">
        <h1 class="show-for-small-down collapse-link mobile-browse-header">Browse by Topic and Author</h1>
          <div class="row collapsable">

            
            <div class="small-6 medium-12 columns">
              <section id="article-popular-topics" style="display: none;">
                
                <h1>Topics</h1>
                <ul id="article-popular-topics">
                    
                    <li>
                        
                        <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/intranets/all/index.php">Intranets</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/user-testing/all/index.php">User Testing</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                        
                    </li>
                    
                    <li><a href="#" id="article-see-all-topics"><strong>See all topics&hellip;</strong></a></li>
                </ul>
                
              </section>
              <section id="article-all-topics" style="display: none;">
                
                <h1>
                  <span class="show-for-medium-up">All Article Topics</span>
                  <span class="show-for-small-down">All Topics</span>
                  (<a href="#" id="article-hide-all-topics">hide</a>)
                </h1>
                <ul>
                  
                  <li>
                    
                    <a href="../../topic/accessibility/all/index.php">Accessibility</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/agile/all/index.php">Agile</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/analytics-and-metrics/all/index.php">Analytics &amp; Metrics</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/applications/all/index.php">Application Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/b2b-websites/all/index.php">B2B Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/behavior-patterns/all/index.php">Behavior Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/branding/all/index.php">Branding</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/content-strategy/all/index.php">Content Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/corporate-websites/all/index.php">Corporate Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/design-patterns/all/index.php">Design Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-design-process/all/index.php">Design Process</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/email/all/index.php">Email</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/eyetracking/all/index.php">Eyetracking</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/heuristic-evaluation/all/index.php">Heuristic Evaluation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/human-computer-interaction/all/index.php">Human Computer Interaction</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-ideation/all/index.php">Ideation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/information-architecture/all/index.php">Information Architecture</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/interaction-design/all/index.php">Interaction Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/international-users/all/index.php">International Users</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/intranets/all/index.php">Intranets</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-management/all/index.php">Management</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/navigation/all/index.php">Navigation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/non-profit-websites/all/index.php">Non-Profit Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/personas/all/index.php">Personas</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/persuasive-design/all/index.php">Persuasive Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/prototyping/all/index.php">Prototyping</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/psychology-and-ux/all/index.php">Psychology and UX</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/research-methods/all/index.php">Research Methods</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/search/all/index.php">Search</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/social-media/all/index.php">Social Media</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/strategy/all/index.php">Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/user-testing/all/index.php">User Testing</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-teams/all/index.php">UX Teams</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/visual-design/all/index.php">Visual Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/writing-web/all/index.php">Writing for the Web</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/young-users/all/index.php">Young Users</a>
                    
                  </li>
                  
                </ul>
                
              </section>
            </div>
            <script>
              /* Determine which topics list will be displayed: the full or the
               * partial.  We are inlining this close to the list itself to
               * prevent rendering delays on the client
               */
              var topic_list = $.cookie("article-topics");

              var popular_topics = $('#article-popular-topics');
              var all_topics = $('#article-all-topics');

              if(topic_list == 1) {
                  popular_topics.hide();
                  all_topics.show();
              } else {
                  popular_topics.show();
                  all_topics.hide();
              }
            </script>
            

            
            <div class="small-6 medium-12 columns">
              
              <section id="article-initial-authors">
                  <h1>Author</h1>
                  <ul>
                    
                    <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                    
                    <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                    
                    <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                    
                    <li><a href="#" onClick="$('#article-initial-authors').hide(); $('#article-all-authors').show();"><strong>See all authors&hellip;</strong></a></li>
                  </ul>
              </section>
              <section id="article-all-authors" style="display: none;">
                <h1>
                  All Authors
                  (<a href="#" id="article-hide-all-authors">hide</a>)
                </h1>
                <ul>
                  
                  <li><a href="../author/raluca-budiu/index.php">Raluca Budiu</a></li>
                  
                  <li><a href="../author/patty-caya/index.php">Patty Caya</a></li>
                  
                  <li><a href="../author/susan-farrell/index.php">Susan Farrell</a></li>
                  
                  <li><a href="../author/therese-fessenden/index.php">Therese Fessenden</a></li>
                  
                  <li><a href="../author/kim-flaherty/index.php">Kim Flaherty</a></li>
                  
                  <li><a href="../author/sarah-gibbons/index.php">Sarah Gibbons</a></li>
                  
                  <li><a href="../author/aurora-harley/index.php">Aurora Harley</a></li>
                  
                  <li><a href="../author/kate-kaplan/index.php">Kate Kaplan</a></li>
                  
                  <li><a href="../author/page-laubheimer/index.php">Page Laubheimer</a></li>
                  
                  <li><a href="../author/angela-li/index.php">Angie Li</a></li>
                  
                  <li><a href="../author/hoa-loranger/index.php">Hoa Loranger</a></li>
                  
                  <li><a href="../author/kate-meyer/index.php">Kate Meyer</a></li>
                  
                  <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                  
                  <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                  
                  <li><a href="../author/kara-pernice/index.php">Kara Pernice</a></li>
                  
                  <li><a href="../author/christian-rohrer/index.php">Christian Rohrer</a></li>
                  
                  <li><a href="../author/amy-schade/index.php">Amy Schade</a></li>
                  
                  <li><a href="../author/katie-sherwin/index.php">Katie Sherwin</a></li>
                  
                  <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                  
                  <li><a href="../author/kathryn-whitenton/index.php">Kathryn Whitenton</a></li>
                  
                </ul>
              </section>
              
            </div>
            
          </div>

          
          <section class="hide-for-small-down">
            
              
              <h1>Recent Articles</h1>
              
              <ul>
                  
                  <li><a href="../seamless-cross-channel/index.php">Seamlessness in the Omnichannel User Experience</a></li>
                  
                  <li><a href="../brand-intention-interpretation/index.php">Experience Design: Bridging Brand Intention and Brand Interpretation</a></li>
                  
                  <li><a href="../eyetracking-tasks-efficient-scanning/index.php">Scanning Patterns on the Web Are Optimized for the Current Task</a></li>
                  
                  <li><a href="../flat-design-best-practices/index.php">Flat-Design Best Practices</a></li>
                  
                  <li><a href="../ux-user-stories/index.php">Accounting for UX Work with User Stories in Agile Projects</a></li>
                  
                  <li><a href="../index.php"><strong>See all articles&hellip;</strong></a></li>
              </ul>
              
            
          </section>
          

          
          
          <section class="hide-for-small-down">
            <h1>Popular Articles</h1>
            
            <ul>
                
                <li><a href="../usability-101-introduction-to-usability/index.php">Usability 101: Introduction to Usability</a></li>
                
                <li><a href="../top-10-mistakes-web-design/index.php">Top 10 Mistakes in Web Design</a></li>
                
                <li><a href="../how-users-read-on-the-web/index.php">How Users Read on the Web</a></li>
                
                <li><a href="../f-shaped-pattern-reading-web-content/index.php">F-Shaped Pattern For Reading Web Content</a></li>
                
                <li><a href="../ten-usability-heuristics/index.php">10 Usability Heuristics for User Interface Design</a></li>
                
                <li><a href="../intranet-design-2016/index.php">10 Best Intranets of 2016</a></li>
                
                <li><a href="../which-ux-research-methods/index.php">When to Use Which User-Experience Research Methods</a></li>
                
                <li><a href="../response-times-3-important-limits/index.php">Response Times: The 3 Important Limits</a></li>
                
                <li><a href="../why-you-only-need-to-test-with-5-users/index.php">Why You Only Need to Test with 5 Users</a></li>
                
                <li><a href="../page-fold-manifesto/index.php">The Fold Manifesto: Why the Page Fold Still Matters</a></li>
                
            </ul>
          </section>
          
          

          
            <section class="hide-for-small-down">
              
            </section>
          

      </section>
      

      
        <div class="small-12 medium-9 columns content-body">
          




<article class="l-subsection alertbox-article-content article-detail-bl">
  
  <header>
    <div class="l-subsection">
      <h1>Three Uses for Analytics in User-Experience Practice</h1>
      <div class="author-meta">by
        
          
            
            <a href="../author/jen-cardello/index.php">Jennifer Cardello</a>
            
          
        on  November 17, 2013
        
      </div>

      

      
      <div class="topic-links">
        Topics:
        
          <ul>

  <li><a href="../../topic/analytics-and-metrics/index.php">Analytics &amp; Metrics</a></li>

  <li><a href="../../topic/research-methods/index.php">Research Methods</a></li>

  <li><a href="../../topic/strategy/index.php">Strategy</a></li>

</ul>
        
      </div>
      
    </div>
  </header>
  

  
  
  <section>
    <p class="lede"><strong>Summary:</strong> In order to make the most of analytics data, UX professionals need to integrate this data where it can add value to qualitative processes instead of distract resources.</p>
    <br />
  </section>
  
  

  
  <section id="article-body"><p>Analytics has traditionally been used to inform marketing strategy and tactics, but we now see <strong>more usability and user-experience professionals relying on this quantitative-data source</strong> to aid in research and design.</p>

<p>The biggest issue with analytics is that it can very quickly become a distracting black hole of “interesting” <strong>data without any actionable insight</strong>.</p>

<p>Probably the worst thing you can do is teach someone how to use an analytics system and hope she will get you some interesting findings. Even a “free” analytics service will cost you a fortune if it redirects resources from more productive uses. Many beginners get stumped over one of three hurdles:</p>

<ul>
	<li><strong>Scope of metrics: </strong>So many things that can be measured, but which are meaningful?</li>
	<li><strong>Difference between metrics:</strong> Which metrics best answer specific questions?</li>
	<li><strong>Interface complexity:</strong> How do you get the analytics system to tell you what you want to find out?</li>
</ul>

<p>Because of this last point, many people end up jumping in the deep end and focusing on the tool instead of the work that it is intended to support. With that in mind, we recommend that UX professionals back up a step and <strong>think about how analytics data can supplement current methods</strong> and processes.</p>

<p>After interviewing a variety of UX teams regarding their use of analytics and other web data, we discovered some interesting high-value UX uses for analytics. We’ll cover 3 in this article (and more in our course on <a href="../../courses/analytics-and-user-experience/index.php">Analytics and User Experience</a>):</p>

<ol>
	<li><strong>Issue indication:</strong> Notifying the team of potential problems reaching goals</li>
	<li><strong>Investigation:</strong> Identifying potential causes of issues</li>
	<li><strong>Triangulation:</strong> Adding data to supplement qualitative research</li>
</ol>

<h2>Use #1: Issue Indication</h2>

<p>Some UX teams collaborate with optimization specialists while designing the site or launching new features to develop and implement a measurement plan.  UX teams receive reports daily or weekly in order to monitor the site’s ability to meet stated goals. They can use the web metrics to diagnose specific issues or rely on them as clues to guide further investigations.</p>

<p>A <strong>measurement plan</strong> consists of:</p>

<ul>
	<li><strong>Goals/macro conversions: </strong>These are the big-picture actions that users need to complete on the site in order for it to be successful. Examples include the number of purchase completions and the number of lead submissions.</li>
	<li><strong>Desirable actions/<a href="../micro-conversions/index.php" title="Define Micro Conversions to Measure Incremental UX Improvements, article">micro conversions</a>:</strong> These are smaller actions that, when combined, support the meeting of the goal—such as progressing along a lead-generation funnel. Examples include visiting a specific page, clicking a particular link, or entering data in a form.</li>
	<li><strong>Web metrics:</strong> These are web-analytics data that indicate whether these desirable actions occur; they help UX teams identify potential issues.</li>
</ul>

<table border="1" cellpadding="0" cellspacing="0" class="MsoTableGrid" style="margin-left:.4in;border-collapse:collapse;mso-table-layout-alt:fixed;
 border:none;mso-border-alt:solid black .5pt;mso-border-themecolor:text1;
 mso-yfti-tbllook:1184;mso-padding-alt:0in 5.4pt 0in 5.4pt" width="617">
	<tbody>
		<tr style="mso-yfti-irow:0;mso-yfti-firstrow:yes">
			<td colspan="4" style="width:462.6pt;border:solid black 1.0pt;
  mso-border-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:
  text1;background:#17365D;mso-background-themecolor:text2;mso-background-themeshade:
  191;padding:0in 5.4pt 0in 5.4pt" valign="top" width="617">
			<h2 class="TableHeading" style="margin-left: 200px;"><span style="font-size:14.0pt;color:white;mso-themecolor:
  background1">Measurement Plan Example<o:p></o:p></span></h2>
			</td>
		</tr>
		<tr style="mso-yfti-irow:1">
			<td style="width:84.6pt;border:solid black 1.0pt;
  mso-border-themecolor:text1;border-top:none;mso-border-top-alt:solid black .5pt;
  mso-border-top-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:
  text1;background:#95B3D7;mso-background-themecolor:accent1;mso-background-themetint:
  153;padding:0in 5.4pt 0in 5.4pt" valign="top" width="113">
			<p class="TableAxisHeading"><strong><span style="mso-bidi-font-size:10.0pt;color:windowtext">Goal</span></strong><span style="mso-bidi-font-size:10.0pt;color:windowtext"><o:p></o:p></span></p>
			</td>
			<td style="width:76.5pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;mso-border-bottom-themecolor:text1;
  border-right:solid black 1.0pt;mso-border-right-themecolor:text1;mso-border-top-alt:
  solid black .5pt;mso-border-top-themecolor:text1;mso-border-left-alt:solid black .5pt;
  mso-border-left-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:
  text1;background:#95B3D7;mso-background-themecolor:accent1;mso-background-themetint:
  153;padding:0in 5.4pt 0in 5.4pt" valign="top" width="102">
			<p class="TableAxisHeading"><strong><span style="mso-bidi-font-size:10.0pt;color:windowtext">Desirable Action by Site Visitors</span></strong><span style="mso-bidi-font-size:10.0pt;color:windowtext"><o:p></o:p></span></p>
			</td>
			<td style="width:1.25in;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;mso-border-bottom-themecolor:text1;
  border-right:solid black 1.0pt;mso-border-right-themecolor:text1;mso-border-top-alt:
  solid black .5pt;mso-border-top-themecolor:text1;mso-border-left-alt:solid black .5pt;
  mso-border-left-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:
  text1;background:#95B3D7;mso-background-themecolor:accent1;mso-background-themetint:
  153;padding:0in 5.4pt 0in 5.4pt" valign="top" width="120">
			<p class="TableAxisHeading"><strong><span style="mso-bidi-font-size:10.0pt;color:windowtext">Web Metric</span></strong><span style="mso-bidi-font-size:10.0pt;color:windowtext"><o:p></o:p></span></p>
			</td>
			<td style="width:211.5pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;mso-border-bottom-themecolor:text1;
  border-right:solid black 1.0pt;mso-border-right-themecolor:text1;mso-border-top-alt:
  solid black .5pt;mso-border-top-themecolor:text1;mso-border-left-alt:solid black .5pt;
  mso-border-left-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:
  text1;background:#95B3D7;mso-background-themecolor:accent1;mso-background-themetint:
  153;padding:0in 5.4pt 0in 5.4pt" valign="top" width="282">
			<p class="TableAxisHeading"><strong><span style="mso-bidi-font-size:10.0pt;color:windowtext">Description</span></strong><span style="mso-bidi-font-size:10.0pt;color:windowtext"><o:p></o:p></span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:2">
			<td rowspan="3" style="width:84.6pt;border:solid black 1.0pt;
  mso-border-themecolor:text1;border-top:none;mso-border-top-alt:solid black .5pt;
  mso-border-top-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:
  text1;padding:0in 5.4pt 0in 5.4pt" valign="top" width="113">
			<p class="MsoNormal"><b style="mso-bidi-font-weight:normal"><span style='mso-bidi-font-size:10.0pt;mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman"'>50 consulting leads per month<o:p></o:p></span></b></p>
			</td>
			<td style="width:76.5pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;mso-border-bottom-themecolor:text1;
  border-right:solid black 1.0pt;mso-border-right-themecolor:text1;mso-border-top-alt:
  solid black .5pt;mso-border-top-themecolor:text1;mso-border-left-alt:solid black .5pt;
  mso-border-left-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:
  text1;padding:0in 5.4pt 0in 5.4pt" valign="top" width="102">
			<p class="MsoNormal"><span style='mso-bidi-font-size:10.0pt;mso-fareast-font-family:
  "Times New Roman";mso-bidi-font-family:"Times New Roman"'>Visit consulting service section<o:p></o:p></span></p>
			</td>
			<td style="width:1.25in;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;mso-border-bottom-themecolor:text1;
  border-right:solid black 1.0pt;mso-border-right-themecolor:text1;mso-border-top-alt:
  solid black .5pt;mso-border-top-themecolor:text1;mso-border-left-alt:solid black .5pt;
  mso-border-left-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:
  text1;padding:0in 5.4pt 0in 5.4pt" valign="top" width="120">
			<p class="MsoNormal"><span style='mso-bidi-font-size:10.0pt;mso-fareast-font-family:
  "Times New Roman";mso-bidi-font-family:"Times New Roman"'>Unique page views<o:p></o:p></span></p>
			</td>
			<td style="width:211.5pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;mso-border-bottom-themecolor:text1;
  border-right:solid black 1.0pt;mso-border-right-themecolor:text1;mso-border-top-alt:
  solid black .5pt;mso-border-top-themecolor:text1;mso-border-left-alt:solid black .5pt;
  mso-border-left-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:
  text1;padding:0in 5.4pt 0in 5.4pt" valign="top" width="282">
			<p class="MsoNormal"><span style='mso-bidi-font-size:10.0pt;mso-fareast-font-family:
  "Times New Roman";mso-bidi-font-family:"Times New Roman"'>“Unique page views” indicates how many site visitors visited this specific page. “Unique” means that the page is not counted multiple times if the same user visits it multiple times.<o:p></o:p></span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:3">
			<td style="width:76.5pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;mso-border-bottom-themecolor:text1;
  border-right:solid black 1.0pt;mso-border-right-themecolor:text1;mso-border-top-alt:
  solid black .5pt;mso-border-top-themecolor:text1;mso-border-left-alt:solid black .5pt;
  mso-border-left-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:
  text1;padding:0in 5.4pt 0in 5.4pt" valign="top" width="102">
			<p class="MsoNormal"><span style='mso-bidi-font-size:10.0pt;mso-fareast-font-family:
  "Times New Roman";mso-bidi-font-family:"Times New Roman"'>Read about consulting services<o:p></o:p></span></p>
			</td>
			<td style="width:1.25in;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;mso-border-bottom-themecolor:text1;
  border-right:solid black 1.0pt;mso-border-right-themecolor:text1;mso-border-top-alt:
  solid black .5pt;mso-border-top-themecolor:text1;mso-border-left-alt:solid black .5pt;
  mso-border-left-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:
  text1;padding:0in 5.4pt 0in 5.4pt" valign="top" width="120">
			<p class="MsoNormal"><span style='mso-bidi-font-size:10.0pt;mso-fareast-font-family:
  "Times New Roman";mso-bidi-font-family:"Times New Roman"'>Average time on page<o:p></o:p></span></p>
			</td>
			<td style="width:211.5pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;mso-border-bottom-themecolor:text1;
  border-right:solid black 1.0pt;mso-border-right-themecolor:text1;mso-border-top-alt:
  solid black .5pt;mso-border-top-themecolor:text1;mso-border-left-alt:solid black .5pt;
  mso-border-left-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:
  text1;padding:0in 5.4pt 0in 5.4pt" valign="top" width="282">
			<p class="MsoNormal"><span style='mso-bidi-font-size:10.0pt;mso-fareast-font-family:
  "Times New Roman";mso-bidi-font-family:"Times New Roman"'>“Time on page” indicates the average amount of time spent on the page. More or less time is not necessarily a good thing. So, it’s important to always look at this data in context and compare it to similar time periods. <o:p></o:p></span></p>
			</td>
		</tr>
		<tr style="mso-yfti-irow:4;mso-yfti-lastrow:yes">
			<td style="width:76.5pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;mso-border-bottom-themecolor:text1;
  border-right:solid black 1.0pt;mso-border-right-themecolor:text1;mso-border-top-alt:
  solid black .5pt;mso-border-top-themecolor:text1;mso-border-left-alt:solid black .5pt;
  mso-border-left-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:
  text1;padding:0in 5.4pt 0in 5.4pt" valign="top" width="102">
			<p class="MsoNormal"><span style='mso-bidi-font-size:10.0pt;mso-fareast-font-family:
  "Times New Roman";mso-bidi-font-family:"Times New Roman"'>Download whitepapers, enter data into form fields, submit the lead form<o:p></o:p></span></p>
			</td>
			<td style="width:1.25in;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;mso-border-bottom-themecolor:text1;
  border-right:solid black 1.0pt;mso-border-right-themecolor:text1;mso-border-top-alt:
  solid black .5pt;mso-border-top-themecolor:text1;mso-border-left-alt:solid black .5pt;
  mso-border-left-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:
  text1;padding:0in 5.4pt 0in 5.4pt" valign="top" width="120">
			<p class="MsoNormal"><span style='mso-bidi-font-size:10.0pt;mso-fareast-font-family:
  "Times New Roman";mso-bidi-font-family:"Times New Roman"'>Events<o:p></o:p></span></p>
			</td>
			<td style="width:211.5pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;mso-border-bottom-themecolor:text1;
  border-right:solid black 1.0pt;mso-border-right-themecolor:text1;mso-border-top-alt:
  solid black .5pt;mso-border-top-themecolor:text1;mso-border-left-alt:solid black .5pt;
  mso-border-left-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:
  text1;padding:0in 5.4pt 0in 5.4pt" valign="top" width="282">
			<p class="MsoNormal"><span style='mso-bidi-font-size:10.0pt;mso-fareast-font-family:
  "Times New Roman";mso-bidi-font-family:"Times New Roman"'>"Events" refers to any user action on the site that you are tracking. In Google Analytics (and many other systems) you can define what event instances should be recorded. Examples of events include downloading a PDF, clicking a particular button, entering text in a field, and watching a video.<o:p></o:p></span></p>
			</td>
		</tr>
	</tbody>
</table>

<h2>Use #2: Investigation</h2>

<p>In this mode, UX teams—either on their own or with the assistance of optimization specialists—develop hypothesis for macro-<a href="../conversion-rates/index.php" title="Defining Conversion Rate as used in UX, usability, e-commerce, and web analytics, article">conversion</a> issues and use analytics to prove/disprove the hypotheses. There are several problem categories that guide the investigation: traffic, technical, content, navigation, visual design. Most examples we provide below are from <a href="http://www.google.com/analytics/">Google Analytics</a>' free offering.</p>

<h3>1) Traffic Issues</h3>

<dl>
	<dt><strong>Example investigation: </strong>Determine if there is one traffic source (Google, Bing, Yahoo, direct, email campaigns, etc.) that is responsible for a decrease in page visitors.</dt>
	<dt><strong>Useful analytics report:</strong> <em>Pages</em> (filtered by the page URI and using <em>Source</em> as the secondary dimension)</dt>
	<dd>In Google Analytics, you can generate page-specific reports that display where the traffic to the page originated (search, email, direct, etc.).</dd>
</dl>

<div><img alt="" src="http://media.nngroup.com/media/editor/2013/11/14/Pages_report_using_filter_and_dimension.png" style="width: 710px; height: 565px;"/>
<p><em>Example Pages report (in Google Analytics) filtered by URI and using Source as Secondary dimension.</em></p>

<h3>2) Technical Issues</h3>

<dl>
	<dt><strong>Example investigation:</strong> Determine if a page element is not loading properly.</dt>
	<dt><strong>Useful analytics report: </strong><em>Event Pages</em></dt>
	<dd>The <em>Event Pages</em> report lists all the pages where events are tracked. You can select the specific page being investigated to get metrics on that specific-page’s events.</dd>
</dl>

<div><img alt="" src="http://media.nngroup.com/media/editor/2013/11/14/event pages report.png" style="width: 710px; height: 489px;"/>
<p><em>Example Event Pages report in Google Analytics.</em></p>
</div>

<h3>3) Content and 4) Visual-Design Issues</h3>

<dl>
	<dt><strong>Example investigations:</strong></dt>
	<dd>
	<ul>
		<li>Determine if new wording may not effectively communicate the benefits of or the process for taking a specific action.</li>
		<li>Determine if imagery, typography, colors, and/or layout are distracting from calls to action (CTAs).</li>
	</ul>
	</dd>
	<dt><strong>Useful analytics report:</strong> <em>In-page Analytics</em></dt>
	<dd><em>In-Page Analytics</em> indicates what links users select.</dd>
	<dt><strong>Note:</strong> You need to set up <a href="https://support.google.com/analytics/answer/2558867?hl=en&amp;utm_id=ad">Enhanced Link Attribution</a> in the Google Analytics admin to see separate percentages for links on a page that all have the same destination. We also reocmmended supplementing In-Page Analytics data with clicktracking services like <a href="http://www.clicktale.com/">Clicktale</a> and/or <a href="http://www.crazyegg.com/">CrazyEgg</a>.</dt>
</dl>

<div><img alt="" src="http://media.nngroup.com/media/editor/2013/11/14/in-page_analytics_report_1.png" style="width: 710px; height: 694px;"/>
<p><em>Example In-Page Analytics indicating what links users are selecting.</em></p>
</div>

<div><img alt="" src="http://media.nngroup.com/media/editor/2013/11/14/crazy_egg.png" style="width: 710px; height: 575px;"/>
<p><em>An example of a CrazyEgg Heatmap display; clicks can also be expressed in numbers via the Overlay view.</em></p>
</div>

<h3>5) Navigation Issues</h3>

<dl>
</dl>

<dl>
	<dt><strong>Example investigation:</strong> Determine if specific links/buttons are not being clicked.</dt>
	<dt><strong>Useful Analytics Report:</strong> <em>Pages</em> (filtered by the page URI and selecting the <em>Navigation Summary</em> tab)</dt>
	<dd><em>Navigation Summary</em> is a tab you can select from any <em>Pages</em> report (illustrated below). It details from which website pages people came before visiting the page of interest and where they went after visiting that page.</dd>
</dl>

<div><img alt="" src="http://media.nngroup.com/media/editor/2013/11/14/navigation tab.png" style="width: 710px; height: 674px;"/>
<p><em>Example Pages report with Navigation Summary selected.</em></p>
</div>

<h2>Use #3: Triangulation</h2>

<p>In this mode, the UX team uses analytics to verify findings derived from qualitative research (e.g., usability testing) and gather additional clues to help in defining a solution. If the original <a href="../how-many-test-users/index.php">usability test was run with about 5 users</a> — as we often recommend — then there is always the risk that estimates like success rates will be wrong. But such a quick test has the advantage of rapidly pinpointing a potential trouble spot, which can then be instrumented for targeted collection of a few thousand analytics data points that support much more accurate estimates.</p>

<h3>Examples of usability-test findings to verify with analytics data:</h3>

<p><strong>Finding:</strong> Study participants don’t know where to find information about a topic because the word used on the site is different than what they use.</p>

<dl>
	<dt><strong>Additional questions to answer: </strong>Are people searching for those terms that participants mentioned in the study?</dt>
	<dt><strong>Useful analytics report:</strong> <em>Search Terms</em></dt>
	<dd>The <em>Search Terms</em> report lists the terms users enter into the website’s own search box (not web-wide search). You can download the search-terms lists corresponding to any time period and conduct more extensive analysis. Terminology that users typed into your own search box is a prime candidate for use to <a href="../user-centric-language/index.php" title="User-centric vs. Maker-centric Language: 3 Essential Guidelines, article">turn your content into user-centered language</a>.</dd>
</dl>

<div><img alt="" src="http://media.nngroup.com/media/editor/2013/11/14/search_terms.png" style="width: 710px; height: 687px;"/>
<p><em>Example Search Terms report in Google Analytics.</em></p>
</div>

<p><strong>Finding:</strong> A feature is not used or a page is not accessed because study participants didn’t notice the link.</p>

<dl>
	<dt><strong>Additional question to answer: </strong>Where are users going instead?</dt>
	<dt><strong>Useful analytics reports: </strong><em>Pages</em> (filtered by the page URI and selecting the <em>Navigation Summary</em> tab) and <em>In-Page Analytics</em> (see examples above in the Investigations section)</dt>
</dl>

<p><strong>Finding:</strong> A form is not being completed because people don’t feel comfortable providing required information.</p>

<dl>
	<dt><strong>Additional question to answer: </strong>On which fields are people abandoning the form?</dt>
	<dt><strong>Useful analytics report:</strong> <em>Event Pages</em> (see example above in the Investigations section)</dt>
</dl>

<h2>Conclusion</h2>

<p>Quantitative data is increasingly becoming a key ingredient in usability and user-experience work. With this change comes the need for UX professionals to become familiar with the language and tools and determine which metrics and features are useful in UX practice.</p>

<p>Adding analytics to UX work enables us to:</p>

<ul>
	<li>Take early action to prevent unnecessary conversion decreases</li>
	<li>Quickly prove/disprove causation/correlation theories</li>
	<li>Better persuade the more data-oriented stakeholders in our organizations</li>
</ul>

<p>Learning analytics systems can be daunting because they are complex and have been purpose-built for marketing activities, not UX. The learning process can be improved by starting with existing UX processes and determining where metrics can add value in those processes.</p>

<p>Beyond what is covered here, there are many more analytics metrics that can be incorporated into UX research and design processes. We will cover these in our new course, <a href="../../courses/analytics-and-user-experience/index.php">Analytics and User Experience</a>.</p>
</div>
</section>
  

  
  <section>
    
        
    
  </section>
  
  
  
  
  

  


  <section>
    <br />
    <p class="share"><b>Share <span class="hide-for-small-down">this article</span>:</b> <a href="https://twitter.com/intent/tweet?url=http://www.nngroup.com/articles/analytics-user-experience/&amp;text=Three%20Uses%20for%20Analytics%20in%20User-Experience%20Practice&amp;via=nngroup">Twitter</a> | <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=http://www.nngroup.com/articles/analytics-user-experience/&amp;title=Three%20Uses%20for%20Analytics%20in%20User-Experience%20Practice&amp;source=Nielsen%20Norman%20Group">LinkedIn</a> | <a href="https://plus.google.com/share?url=http://www.nngroup.com/articles/analytics-user-experience/">Google+</a> | <a href="mailto:?subject=NN/g Article: Three Uses for Analytics in User-Experience Practice&amp;body=http://www.nngroup.com/articles/analytics-user-experience/">Email</a></p>
    <hr />
  </section>

  <footer class="l-subsection">
    
    
    <div class="learn-more">
      <h2>Learn More</h2>
      <p><a href="../subscribe/index.php">Subscribe to the weekly newsletter</a> to get notified about future articles.</p>
      </div>
    
    

    <div class="row show-for-small-down related-content">
      
      
      

      
      
        <div class="column small-12 topic-links">All articles about:
          <ul>
            
            <li><a href="../../topic/analytics-and-metrics/index.php">Analytics &amp; Metrics</a></li>
            
            <li><a href="../../topic/research-methods/index.php">Research Methods</a></li>
            
            <li><a href="../../topic/strategy/index.php">Strategy</a></li>
            
          </ul>
        </div>
      
      

      
      
        <div class="column small-12">
          <h3>UX Conference Training Courses</h3>
           <ul class="no-bullet">
    
        <li><a href="../../courses/ux-basic-training/index.php">UX Basic Training</a></li>
    
        <li><a href="../../courses/journey-mapping/index.php">Journey Mapping to Understand Customer Needs</a></li>
    
        <li><a href="../../courses/facilitating-ux-workshops/index.php">Facilitating UX Workshops</a></li>
    
        <li><a href="../../courses/measuring-ux/index.php">Measuring User Experience</a></li>
    
        <li><a href="../../courses/analytics-and-user-experience/index.php">Analytics and User Experience</a></li>
    
</ul>
        </div>
      
      

      
      
        <div class="small-12 column">
          <h3>Research Reports</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../reports/usability-return-on-investment-roi/index.php">Return on Investment (ROI) for Usability</a></li>
              
            
              
                <li><a href="../../reports/user-experience-careers/index.php">User Experience Careers</a></li>
              
            
          </ul>
        </div>
      
      

      
      
        <div class="small-12 column">
          <h3>Online Seminars</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../online-seminars/ux-real-world/index.php">UX in the Real World</a></li>
              
            
              
                <li><a href="../../online-seminars/how-change-your-corporate-culture/index.php">How to Change Your Corporate Culture to be User First</a></li>
              
            
          </ul>
        </div>
      
      
    </div>

    <div class="hide-for-small-down row related-content">
      <div class="medium-6 columns">
        
        
          <div>
            <h3>Articles</h3>
            <ul class="no-bullet">
              
                
                <li><a href="../return-visits-not-bounce/index.php">Optimize for Return Visits, not Bounce Rate</a></li>
                
              
                
                <li><a href="../frequency-recency/index.php">Frequency &amp; Recency of Site Visits: 2 Metrics for User Engagement</a></li>
                
              
                
                <li><a href="../pogo-sticking/index.php">No More Pogo Sticking: Protect Users from Wasted Clicks</a></li>
                
              
                
                <li><a href="../analytics-persona-segment/index.php">Segment Analytics Data Using Personas</a></li>
                
              
                
                <li><a href="../satisfaction-vs-performance-metrics/index.php">User Satisfaction vs. Performance Metrics</a></li>
                
              
            </ul>
          </div>
        
        

        
          
          <div>
            <h3>Research Reports</h3>
            <ul class="no-bullet">
              
                
                  <li><a href="../../reports/usability-return-on-investment-roi/index.php">Return on Investment (ROI) for Usability</a></li>
                
              
                
                  <li><a href="../../reports/user-experience-careers/index.php">User Experience Careers</a></li>
                
              
            </ul>
          </div>
          
        

      </div>
      <div class="medium-6 columns">
        
          
          <div>
            <h3>UX Conference Training Courses</h3>
             <ul class="no-bullet">
    
        <li><a href="../../courses/ux-basic-training/index.php">UX Basic Training</a></li>
    
        <li><a href="../../courses/journey-mapping/index.php">Journey Mapping to Understand Customer Needs</a></li>
    
        <li><a href="../../courses/facilitating-ux-workshops/index.php">Facilitating UX Workshops</a></li>
    
        <li><a href="../../courses/measuring-ux/index.php">Measuring User Experience</a></li>
    
        <li><a href="../../courses/analytics-and-user-experience/index.php">Analytics and User Experience</a></li>
    
</ul>
          </div>
          
        
        
          
          <div>
            <h3>Online Seminars</h3>
            <ul class="no-bullet">
            
                <li><a href="../../online-seminars/ux-real-world/index.php">UX in the Real World</a></li>
            
                <li><a href="../../online-seminars/how-change-your-corporate-culture/index.php">How to Change Your Corporate Culture to be User First</a></li>
            
            </ul>
          </div>
          
        
      </div>
    </div>
  </footer>
</article>

        </div>

      

    
  </div>
  <div class="row">
    
      <div class="column small-12 show-for-small-down l-navrail announcements">
        
          
        
      </div>
    
    
  </div>

    </div>
    




<footer id="page-footer">
    <div class="row hide-for-print">
        <div class="column small-12 medium-6">
            <section class="footer-popular">
                
                <h1>Popular Topics</h1>
                <div style="float: left;  width: 15em; display: block;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/applications/index.php">Application Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/e-commerce/index.php">E-Commerce Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/eyetracking/index.php">Eyetracking </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/information-architecture/index.php">Information Architecture </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/intranets/index.php">Intranet Design </a></li>
	<li><a href="../../topic/interaction-design/index.php">Interaction Design </a></li>
</ul>
</div>

<div style="float: left;  width: 15em; display: block; list-style: none;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/mobile-and-tablet-design/index.php">Mobile and Tablet Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/credibility-persuasion/index.php">Persuasive Design</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/research-methods/index.php">Research Methods</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/user-testing/index.php">User Testing </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/web-usability/index.php">Web Usability </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/writing-web/index.php">Web Writing &amp; Content Strategy </a></li>
</ul>
</div>

<p>&nbsp;</p>

                
            </section>
        </div>

        <div class="column small-12 medium-6">
            <div class="row">
                <div class="column small-6">
                    <section class="footer-about">
                        <h1>Follow Us</h1>
                        <ul>
	<li><a href="../subscribe/index.php">Subscribe to our weekly UX newsletter</a>,&nbsp;Jakob Nielsen&#39;s Alertbox</li>
	<li>Visit Don Norman&#39;s website, <a href="http://www.jnd.org/"> www.jnd.org </a></li>
	<li>Visit Bruce Tognazzini&#39;s website, <a href="http://www.asktog.com/"> www.asktog.com </a></li>
	<li style="padding-top: 6px;"><a class="twitter-follow-button" data-show-count="false" data-size="large" href="https://twitter.com/nngroup"><img alt="Follow @nngroup on Twitter" height="20" src="https://media.nngroup.com/media/editor/2016/12/30/twitter-follow-button.png" style="width: 117px; height: 20px;" width="117" /></a></li>
	<li style="padding-top: 6px;"><img alt="" height="14" src="https://media.nngroup.com/media/editor/2016/07/26/feed-icon-14x14.png" style="vertical-align:text-top;width:14px;margin-right:.25em;" width="14" /><a href="../../feed/rss/index.php">Subscribe to our RSS feed</a></li>
</ul>

                    </section>
                </div>

                <div class="column small-6">
                    <section class="footer-about right-column">
                        <h1>About
</h1>
                        <ul class="no-bullet">
	<li><a href="../../about/contact/index.php">Contact information</a></li>
	<li><a href="../../about/index.php">About Us</a></li>
	<li><a href="../../about/why-nng/index.php">Why NN/g? </a></li>
	<li><a href="../../return-policy/index.php">Return Policy</a></li>
</ul>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="copyright column small-12">
            <p><a href="../../copyright-and-reprint-info/index.php">Copyright</a> &copy; 1998-2017 Nielsen Norman Group, All Rights Reserved.</p>
        </div>
    </div>
</footer>


    


        <script src="https://media.nngroup.com/static/vendor/fastclick/js/fastclick.min.js"></script>
        <script src="https://media.nngroup.com/static/vendor/foundation/js/foundation.min.js"></script>
        <script src="http://media.nngroup.com/static/js/app.js"></script>
        <script src="http://media.nngroup.com/static/js/scroll-to-fixed.js"></script><script>var externalLinks = document.querySelectorAll('a[href^="http"]');for (var i = externalLinks.length-1; i >= 0; i--) {externalLinks[i].addEventListener("click", function(event) { alert("External links are disabled for this test."); event.preventDefault(); }, false);}var mailLinks = document.querySelectorAll('a[href^="mailto"]');for (var i = mailLinks.length-1; i >= 0; i--) {mailLinks[i].addEventListener("click", function(event) { alert("Email links are disabled for this test."); event.preventDefault(); }, false);}var externalForms = document.querySelectorAll('form');for (var i = externalForms.length-1; i >= 0; i--) {externalForms[i].addEventListener("submit", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false); externalForms[i].addEventListener("click", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false);}</script>

        <script>
            
            $(function() {
                $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
                $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
            });
        </script>

        
<script src="https://media.nngroup.com/static/js/articles.js"></script>


        
        <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1072699960;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072699960/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
    </body>

<!-- Mirrored from www.nngroup.com/articles/analytics-user-experience/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:10:33 GMT -->
</html>
