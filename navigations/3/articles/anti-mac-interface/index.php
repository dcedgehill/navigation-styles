<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->





    
<!-- Mirrored from www.nngroup.com/articles/anti-mac-interface/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:14:00 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1026.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"beacon":"bam.nr-data.net","queueTime":3,"applicationTime":577,"agent":"","licenseKey":"1a30ef9931","errorBeacon":"bam.nr-data.net","applicationID":"2048992","transactionName":"NFxSMRBUX0EDUkIPDA0WdhAMVkVbDV8ZBxEXUFMJB0YfRAtUQRVZIktEDAFZVHYHRVcPD01eVRE="}</script>
        <title>The Anti-Mac User Interface (Don Gentner and Jakob Nielsen)</title><meta property="og:title" content="The Anti-Mac User Interface (Don Gentner and Jakob Nielsen)" />
  
        
        <meta name="description" content="We reverse all of the core design principles behind the Macintosh human interface guidelines to arrive at the characteristics of the Internet desktop.">
        <meta property="og:description" content="We reverse all of the core design principles behind the Macintosh human interface guidelines to arrive at the characteristics of the Internet desktop." />
        
  
        
	
        
        <meta name="keywords" content="Macintosh Human Interface guidelines, Mac, Anti-Mac, Anti-Macintosh, design guidelines, WIMP, windows, icons, menus, pointing device, metaphor, direct manipulation, see-and-point, consistency, WYSIWYG, user control, feedback, dialog, modes, modelessness, Magic Cap, language, expert users">
        

        

        <meta name="robots" content="noarchive">
        <meta name="viewport" content="width=device-width">

        <link rel="search" type="application/opensearchdescription+xml" title="NN/g Search" href="../../opensearch.xml" />
        <link rel="shortcut icon" href="http://media.nngroup.com/static/img/favicon.ico" />

        <link rel="stylesheet" href="https://media.nngroup.com/static/vendor/font-awesome/css/font-awesome.min.css">

        
        <link rel="stylesheet" href="https://media.nngroup.com/static/css/7232a960f1df.css" type="text/css" />
        <link rel="stylesheet" href="http://fast.fonts.com/t/1.css?apiType=css&amp;projectid=c5846260-9def-46ec-9407-0f1349bd2d64" type="text/css">

        <script src="https://media.nngroup.com/static/vendor/modernizr/js/modernizr.min.js"></script>

        <script type="text/javascript" src="https://media.nngroup.com/static/js/3283be5def74.js"></script>
        

        

        
        
        <!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=229034,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('http://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->


<!-- Google Analytics Tracking Code for http://nngroup.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23132681-1', 'nngroup.com');
  ga('require', 'linkid', 'https://www.nngroup.com/articles/anti-mac-interface/linkid.js');
  ga('require', 'displayfeatures');

// Optimizely Universal Analytics Integration
 window.optimizely = window.optimizely || [];
 window.optimizely.push("activateUniversalAnalytics");

  ga('send', 'pageview');

</script>
<script>
/**
* Function to track a click on an outbound link in Google Analytics.
*/
function trackOutboundLink(label, url) {
    if(window.ga && ga.loaded) {
        ga('send', 'event', 'outbound', 'click', label, {'hitCallback': function(){document.location = url;}
        });
    } else {
        document.location = url;
    }
}
/**
* Function to track a click on a link in the article announcement area.
*/
$('#articleAnnouncement a').on('click', function(){
  ga('send', 'event', 'link', 'click', 'articleAnnouncement');
});
</script>

<script type="text/javascript">
 setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2421.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



<!-- Hotjar Tracking Code for http://nngroup.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:121605,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

        <script src="https://media.nngroup.com/static/js/analytics/track_outbound_link.js"></script>
        
        
        
    </head>
    <body class="location-articles articles-detail">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
    <header id="page-header">
        <div id="cart" class="shopping-cart mobile-cart show-for-small-down"></div>
        <div class="row">
            <div id="masthead" class="masthead small-12 medium-7 column">

                <!-- The button to toggles menu on small screen -->

                <h1><a href="../../index.php"><img src="http://media.nngroup.com/static/img/logo-wordmark.png" width="74" height="30"> Nielsen <span class="norman">Norman</span> Group</a></h1>
                <p class="tagline show-for-medium-up">Evidence-Based User Experience Research, Training, and Consulting</p>

                <!-- The button to toggles form search on small screen -->


                <div style="clear: right;width:  100%;height: 1px;"></div><?php define('ROOT_PATH',$_SERVER["DOCUMENT_ROOT"] . '/apps/navigation_styles/navigations/3'); ?><?php include(ROOT_PATH)."/navigation.php"; ?>

                <!-- Drop down menu on small screen -->
                <div id="hideNav" class="show-for-small-down">
                  
                  <ul class="menu-content">
                    <li><a href="../../index.php">Home</a></li>
                    <li class="has-submenu">
                      <a>Training</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../training/index.php">Overview</a></li>
                        
<li id="nav-ux-conference"><a href="../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../online-seminars/index.php"><span>Online Seminars</span></a></li>

                      </ul>
                    </li>
                    <li class="has-submenu">
                      <a>Consulting</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
  <li><a href="../../consulting/index.php">Overview</a></li>
  
  <li><a href="../../consulting/benchmarking/index.php">Benchmarking</a></li>
  
  <li><a href="../../consulting/expert-design-review/index.php">Expert Design Review</a></li>
  
  <li><a href="../../consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
  
  <li><a href="../../consulting/usability-testing/index.php">Usability Testing </a></li>
  
  <li><a href="../../consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
  
  <li><a href="../../in-house-training/index.php">In-House Training</a></li>
</ul>

                    </li>

                    <li><a href="../../reports/index.php">Reports</a></li>
                    <li><a href="../index.php">Articles</a></li>
                    <li class="has-submenu"><a>About NN/G</a><i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../about/index.php">Overview</a></li>
                        <li><a href="../../people/index.php">People</a></li>
                        <li><a href="../../about/why-nng/index.php">Why NN/g?</a></li>
                        <li><a href="../../about/contact/index.php">Contact</a></li>
                        <li><a href="../../news/index.php">News</a></li>
                        <li><a href="../../about/history/index.php">History</a></li>
                        <li><a href="../../books/index.php">Books</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                </div>

                <hr class="header-divider show-for-small-only"/>


            </div>
       </div>

        <div id="hideNav">            <nav class="primary-nav small-12 column">
                <ul>
                    <li><a class="navitem" id="nav-home" href="../../index.php">Home</a></li>
                    <li><a class="navitem" id="nav-training" href="../../training/index.php">Training</a></li>
                    <li><a class="navitem" id="nav-consulting" href="../../consulting/index.php">Consulting</a></li>
                    <li><a class="navitem" id="nav-reports" href="../../reports/index.php">Reports</a></li>
                    <li><a class="navitem" id="nav-articles" href="../index.php">Articles</a></li>
                    <li><a class="navitem" id="nav-about" href="../../about/index.php">About NN/G</a></li>
                </ul>
            </nav>
            <nav class="secondary-nav small-12 medium-12 large-12 columns">
                
                
            </nav>
        </div>
    </header>
    <div id="mainContent" class="l-content">
    
  

  

  <div class="row">
    
      
      <section class="small-12 medium-3 columns collapsable-content l-navrail articles-left-column">
        <h1 class="show-for-small-down collapse-link mobile-browse-header">Browse by Topic and Author</h1>
          <div class="row collapsable">

            
            <div class="small-6 medium-12 columns">
              <section id="article-popular-topics" style="display: none;">
                
                <h1>Topics</h1>
                <ul id="article-popular-topics">
                    
                    <li>
                        
                        <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/intranets/all/index.php">Intranets</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/user-testing/all/index.php">User Testing</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                        
                    </li>
                    
                    <li><a href="#" id="article-see-all-topics"><strong>See all topics&hellip;</strong></a></li>
                </ul>
                
              </section>
              <section id="article-all-topics" style="display: none;">
                
                <h1>
                  <span class="show-for-medium-up">All Article Topics</span>
                  <span class="show-for-small-down">All Topics</span>
                  (<a href="#" id="article-hide-all-topics">hide</a>)
                </h1>
                <ul>
                  
                  <li>
                    
                    <a href="../../topic/accessibility/all/index.php">Accessibility</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/agile/all/index.php">Agile</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/analytics-and-metrics/all/index.php">Analytics &amp; Metrics</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/applications/all/index.php">Application Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/b2b-websites/all/index.php">B2B Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/behavior-patterns/all/index.php">Behavior Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/branding/all/index.php">Branding</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/content-strategy/all/index.php">Content Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/corporate-websites/all/index.php">Corporate Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/design-patterns/all/index.php">Design Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-design-process/all/index.php">Design Process</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/email/all/index.php">Email</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/eyetracking/all/index.php">Eyetracking</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/heuristic-evaluation/all/index.php">Heuristic Evaluation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/human-computer-interaction/all/index.php">Human Computer Interaction</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-ideation/all/index.php">Ideation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/information-architecture/all/index.php">Information Architecture</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/interaction-design/all/index.php">Interaction Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/international-users/all/index.php">International Users</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/intranets/all/index.php">Intranets</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-management/all/index.php">Management</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/navigation/all/index.php">Navigation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/non-profit-websites/all/index.php">Non-Profit Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/personas/all/index.php">Personas</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/persuasive-design/all/index.php">Persuasive Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/prototyping/all/index.php">Prototyping</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/psychology-and-ux/all/index.php">Psychology and UX</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/research-methods/all/index.php">Research Methods</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/search/all/index.php">Search</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/social-media/all/index.php">Social Media</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/strategy/all/index.php">Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/user-testing/all/index.php">User Testing</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-teams/all/index.php">UX Teams</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/visual-design/all/index.php">Visual Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/writing-web/all/index.php">Writing for the Web</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/young-users/all/index.php">Young Users</a>
                    
                  </li>
                  
                </ul>
                
              </section>
            </div>
            <script>
              /* Determine which topics list will be displayed: the full or the
               * partial.  We are inlining this close to the list itself to
               * prevent rendering delays on the client
               */
              var topic_list = $.cookie("article-topics");

              var popular_topics = $('#article-popular-topics');
              var all_topics = $('#article-all-topics');

              if(topic_list == 1) {
                  popular_topics.hide();
                  all_topics.show();
              } else {
                  popular_topics.show();
                  all_topics.hide();
              }
            </script>
            

            
            <div class="small-6 medium-12 columns">
              
              <section id="article-initial-authors">
                  <h1>Author</h1>
                  <ul>
                    
                    <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                    
                    <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                    
                    <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                    
                    <li><a href="#" onClick="$('#article-initial-authors').hide(); $('#article-all-authors').show();"><strong>See all authors&hellip;</strong></a></li>
                  </ul>
              </section>
              <section id="article-all-authors" style="display: none;">
                <h1>
                  All Authors
                  (<a href="#" id="article-hide-all-authors">hide</a>)
                </h1>
                <ul>
                  
                  <li><a href="../author/raluca-budiu/index.php">Raluca Budiu</a></li>
                  
                  <li><a href="../author/patty-caya/index.php">Patty Caya</a></li>
                  
                  <li><a href="../author/susan-farrell/index.php">Susan Farrell</a></li>
                  
                  <li><a href="../author/therese-fessenden/index.php">Therese Fessenden</a></li>
                  
                  <li><a href="../author/kim-flaherty/index.php">Kim Flaherty</a></li>
                  
                  <li><a href="../author/sarah-gibbons/index.php">Sarah Gibbons</a></li>
                  
                  <li><a href="../author/aurora-harley/index.php">Aurora Harley</a></li>
                  
                  <li><a href="../author/kate-kaplan/index.php">Kate Kaplan</a></li>
                  
                  <li><a href="../author/page-laubheimer/index.php">Page Laubheimer</a></li>
                  
                  <li><a href="../author/angela-li/index.php">Angie Li</a></li>
                  
                  <li><a href="../author/hoa-loranger/index.php">Hoa Loranger</a></li>
                  
                  <li><a href="../author/kate-meyer/index.php">Kate Meyer</a></li>
                  
                  <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                  
                  <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                  
                  <li><a href="../author/kara-pernice/index.php">Kara Pernice</a></li>
                  
                  <li><a href="../author/christian-rohrer/index.php">Christian Rohrer</a></li>
                  
                  <li><a href="../author/amy-schade/index.php">Amy Schade</a></li>
                  
                  <li><a href="../author/katie-sherwin/index.php">Katie Sherwin</a></li>
                  
                  <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                  
                  <li><a href="../author/kathryn-whitenton/index.php">Kathryn Whitenton</a></li>
                  
                </ul>
              </section>
              
            </div>
            
          </div>

          
          <section class="hide-for-small-down">
            
              
              <h1>Recent Articles</h1>
              
              <ul>
                  
                  <li><a href="../seamless-cross-channel/index.php">Seamlessness in the Omnichannel User Experience</a></li>
                  
                  <li><a href="../brand-intention-interpretation/index.php">Experience Design: Bridging Brand Intention and Brand Interpretation</a></li>
                  
                  <li><a href="../eyetracking-tasks-efficient-scanning/index.php">Scanning Patterns on the Web Are Optimized for the Current Task</a></li>
                  
                  <li><a href="../flat-design-best-practices/index.php">Flat-Design Best Practices</a></li>
                  
                  <li><a href="../ux-user-stories/index.php">Accounting for UX Work with User Stories in Agile Projects</a></li>
                  
                  <li><a href="../index.php"><strong>See all articles&hellip;</strong></a></li>
              </ul>
              
            
          </section>
          

          
          
          <section class="hide-for-small-down">
            <h1>Popular Articles</h1>
            
            <ul>
                
                <li><a href="../usability-101-introduction-to-usability/index.php">Usability 101: Introduction to Usability</a></li>
                
                <li><a href="../top-10-mistakes-web-design/index.php">Top 10 Mistakes in Web Design</a></li>
                
                <li><a href="../how-users-read-on-the-web/index.php">How Users Read on the Web</a></li>
                
                <li><a href="../f-shaped-pattern-reading-web-content/index.php">F-Shaped Pattern For Reading Web Content</a></li>
                
                <li><a href="../ten-usability-heuristics/index.php">10 Usability Heuristics for User Interface Design</a></li>
                
                <li><a href="../intranet-design-2016/index.php">10 Best Intranets of 2016</a></li>
                
                <li><a href="../which-ux-research-methods/index.php">When to Use Which User-Experience Research Methods</a></li>
                
                <li><a href="../response-times-3-important-limits/index.php">Response Times: The 3 Important Limits</a></li>
                
                <li><a href="../why-you-only-need-to-test-with-5-users/index.php">Why You Only Need to Test with 5 Users</a></li>
                
                <li><a href="../page-fold-manifesto/index.php">The Fold Manifesto: Why the Page Fold Still Matters</a></li>
                
            </ul>
          </section>
          
          

          
            <section class="hide-for-small-down">
              
            </section>
          

      </section>
      

      
        <div class="small-12 medium-9 columns content-body">
          




<article class="l-subsection alertbox-article-content article-detail-bl">
  
  <header>
    <div class="l-subsection">
      <h1>The Anti-Mac Interface</h1>
      <div class="author-meta">by
        
          
            
            <a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a>
            
          
        on  August 1, 1996
        
      </div>

      

      
      <div class="topic-links">
        Topics:
        
          <ul>

  <li><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction</a></li>

  <li><a href="../../topic/applications/index.php">Application Design</a></li>

  <li><a href="../../topic/ux-ideation/index.php">Ideation</a></li>

</ul>
        
      </div>
      
    </div>
  </header>
  

  
  
  <section>
    <p class="lede"><strong>Summary:</strong> We reverse all of the core design principles behind the Macintosh human interface guidelines to arrive at the characteristics of the Internet desktop.</p>
    <br />
  </section>
  
  

  
  <section id="article-body"><p><em>Originally published as: Gentner, D., and Nielsen, J.: The Anti-Mac interface, <cite> Communications of the ACM </cite> <strong> 39</strong>, 8 (August 1996), 70-82. </em></p>

<p>(See also: my <a href="../macintosh-25-years/index.php"> appreciation of the Macintosh</a> on its 25th anniversary.)</p>

<p>By exploring alternative interfaces that transcend the principles behind conventional graphical interfaces, a human-computer interface emerges that is based on language, a richer representation of objects, expert users, and shared control.</p>

<p>At recent user interface conferences, several speakers have lamented that the human interface is stuck. We seem to have settled on the WIMP (windows, icons, menus, pointer) model, and there is very little real innovation in interface design anymore.</p>

<p>Physicists and mathematicians often stretch their imaginations by considering what the world would be like if some of their basic assumptions and principles were violated (for example, see [1]). This has led to new concepts such as non-Euclidean geometry, positrons, antimatter, and antigravity. At the least, violating basic assumptions is a useful mental exercise, but a surprising number of the resulting concepts have provided useful descriptions of the real world.</p>

<p>In this article, we explore the types of interfaces that could result if we violate each of the Macintosh human interface design principles. We focus on the Macintosh interface because it is a prime example of the current interface paradigm, and Apple Computer has published an explicit list of Macintosh human interface design principles [2]. These principles have not significantly changed since the introduction of the Macintosh; style guides for other popular graphical interfaces, such as Motif, OPEN LOOK, and Windows [16, 18, 22], list a very similar set of principles as the basis for their interfaces.</p>

<p>We should state at the outset that we are devoted fans of the Macintosh human interface and frequent users of Macintosh computers. Our purpose is not to argue that the Macintosh human interface guidelines are bad principles, but rather to explore alternative approaches to computer interfaces. The Anti-Mac interface is not intended to be hostile to the Macintosh, only different. In fact, human interface designers at Apple and elsewhere have already incorporated some of the Anti-Mac features into the Macintosh desktop and applications. The Macintosh was designed to be "the computer for the rest of us" and succeeded well enough that it became, as Alan Kay once said, "the first personal computer good enough to be criticized." This article should be taken in the same spirit.</p>

<p>The Macintosh was designed under a number of constraints, including:</p>

<ul>
	<li>It needed to sell to "naive users," that is, users without any previous computer experience.</li>
	<li>It was targeted at a narrow range of applications (mostly office work, though entertainment and multimedia applications have been added later in ways that sometimes break slightly with the standard interface).</li>
	<li>It controlled relatively weak computational resources (originally a non-networked computer with 128KB RAM, a 400KB storage device, and a dot-matrix printer).</li>
	<li>It was supported by highly impoverished communication channels between the user and the computer (initially a small black-and-white screen with poor audio output, no audio input, and no other sensors than the keyboard and a one-button mouse).</li>
	<li>It was a standalone machine that at most was connected to a printer.</li>
</ul>

<p>These constraints have all been relaxed somewhat during the 12 years since the introduction of the Macintosh, but we will explore what might happen if they were to be eliminated completely.</p>

<h2>The Macintosh Human Interface Design Principles</h2>

<p>According to the Macintosh guidelines [2], the design of human interfaces for Macintosh system software and applications is based on a number of fundamental principles of human-computer interaction. These principles have led to excellent graphical interfaces, but we wonder: How do these principles limit the computer-human interface? What types of interfaces would result from violating these principles?</p>

<p>We will address these two questions for each of the Macintosh human interface design principles. (The Macintosh design principles and their corresponding Anti-Mac principles are summarized in Table 1.)</p>

<p> </p>

<table style="border: 1px solid rgb(204, 204, 204); text-align: left; margin-right: auto; margin-left: auto; border-collapse: collapse;">
	<caption align="bottom"><strong>Table 1. </strong> <em> The Mac and Anti-Mac design principles. </em></caption>
	<tbody>
		<tr>
			<th style="border-width: 1px; border-style: solid; padding: 0.5ex;">Mac</th>
			<th style="border-width: 1px; border-style: solid; padding: 0.5ex;">Anti-Mac</th>
		</tr>
		<tr>
			<td style="border-width: 1px; border-style: solid; padding: 0.5ex;">Metaphors</td>
			<td style="border-width: 1px; border-style: solid; padding: 0.5ex;">Reality</td>
		</tr>
		<tr>
			<td style="border-width: 1px; border-style: solid; padding: 0.5ex;">Direct Manipulation</td>
			<td style="border-width: 1px; border-style: solid; padding: 0.5ex;">Delegation</td>
		</tr>
		<tr>
			<td style="border-width: 1px; border-style: solid; padding: 0.5ex;">See and Point</td>
			<td style="border-width: 1px; border-style: solid; padding: 0.5ex;">Describe and Command</td>
		</tr>
		<tr>
			<td style="border-width: 1px; border-style: solid; padding: 0.5ex;">Consistency</td>
			<td style="border-width: 1px; border-style: solid; padding: 0.5ex;">Diversity</td>
		</tr>
		<tr>
			<td style="border-width: 1px; border-style: solid; padding: 0.5ex;">WYSIWYG</td>
			<td style="border-width: 1px; border-style: solid; padding: 0.5ex;">Represent Meaning</td>
		</tr>
		<tr>
			<td style="border-width: 1px; border-style: solid; padding: 0.5ex;">User Control</td>
			<td style="border-width: 1px; border-style: solid; padding: 0.5ex;">Shared Control</td>
		</tr>
		<tr>
			<td style="border-width: 1px; border-style: solid; padding: 0.5ex;">Feedback and Dialog</td>
			<td style="border-width: 1px; border-style: solid; padding: 0.5ex;">System Handles Details</td>
		</tr>
		<tr>
			<td style="border-width: 1px; border-style: solid; padding: 0.5ex;">Forgiveness</td>
			<td style="border-width: 1px; border-style: solid; padding: 0.5ex;">Model User Actions</td>
		</tr>
		<tr>
			<td style="border-width: 1px; border-style: solid; padding: 0.5ex;">Aesthetic Integrity</td>
			<td style="border-width: 1px; border-style: solid; padding: 0.5ex;">Graphic Variety</td>
		</tr>
		<tr>
			<td style="border-width: 1px; border-style: solid; padding: 0.5ex;">Modelessness</td>
			<td style="border-width: 1px; border-style: solid; padding: 0.5ex;">Richer Cues</td>
		</tr>
	</tbody>
</table>

<h3>Metaphors</h3>

<p>The first Macintosh principle states that the interface should be based on metaphors with the familiar noncomputer world around us. In the Macintosh interface, computer files are represented as documents in paper folders that are placed on a desktop. Files are deleted by dragging them to the trash can. Many recent interfaces have tried to overcome the limitations of the desktop metaphor by extending it to some other room or building metaphor (e.g., Bob or Magic Cap, Figure 1) or to a village metaphor (e.g., eWorld). These 3D designs try to emulate virtual reality on a flat screen but often seem to introduce a level of clunky indirectness in achieving common user goals. They are navigationally cumbersome, asking users to go to the "other end of town" to pick up their email from the Post Office, and interactionally cumbersome, overloading users with additional windows and other interface elements necessitated by the metaphor but not by the user's task.</p>

<p style="width: 480px; margin-right: auto; margin-left: auto;"><img alt="The 'desktop' on a Magic Cap device (note link in upper right to move out into the hallway, as shown in the next screenshot)." src="http://media.nngroup.com/media/editor/2012/12/11/anti-mac-fig1.gif" style="width: 480px; height: 318px;"/><br/>
<img alt="The 'hallway' on a Magic Cap device." src="http://media.nngroup.com/media/editor/2012/12/11/anti-mac-fig1a.gif" style="width: 480px; height: 318px;"/> <strong>Figure 1.</strong> <em>The Magic Cap interface is based on literal metaphors for desktops, buildings, and villages.</em></p>

<p>Although the use of metaphor may ease learning for the computer novice, it can also cripple the interface with irrelevant limitations and blind the designer to new paradigms more appropriate for a computer-based application. The designers of the Phelps farm tractor in 1901 based their interface on a metaphor with the interface for the familiar horse: farmers used reins to control the tractor. The tractor was steered by pulling on the appropriate rein, both reins were loosened to go forward and pulled back to stop, and pulling back harder on the reins caused the tractor to back up [5]. It's clear in hindsight that this was a dead end, and automobiles have developed their own user interfaces without metaphors based on earlier technologies. Nonetheless, people today are designing information-retrieval interfaces based on metaphors with books, even though young folks spend more time flipping television channels and playing video games than they do turning the pages of books.</p>

<p>The three classic problems with metaphors [9] are:</p>

<ul>
	<li>The target domain has features not in the source domain (e.g., telling the user that "a word processor is like a typewriter" would not lead the user to look for the replace command).</li>
	<li>The source domain has features not in the target domain (a typewriter has the ability to mark up any form you receive in the mail, but a user trying to do that on current computer systems will normally fail).</li>
	<li>Some features exist in both domains but work very differently (the treatment of white space representing space characters, tabs, and line feeds is very different on typewriters and in word processors). Therefore, users may have trouble seeing beyond the metaphor to use the system in the ways it was intended. It is possible to design interfaces to map very closely to metaphors (e.g., Bob Mack's NOSE-no-surprise editor-really did act like a typewriter in all respects), but those interfaces will often be very low-powered and suited mainly for walk-up-and-use situations.</li>
</ul>

<p>As an example of the limitations of the desktop metaphor, consider the trash can on the Macintosh desktop. It is a fine metaphor for the wastebasket in an office. In both cases we dispose of objects by putting them in the trash can and we're able to retrieve documents we had thrown out until the trash is emptied. However, the limits of the single-trash-can metaphor has led to a system that fails to meet the user's needs and causes confusion by masking the realities of the implementation. In the underlying implementation, there are separate trash containers for each volume, such as a hard disk or a floppy disk, but to avoid the confusion of multiple trash cans in the interface, the contents of the trash containers for all mounted volumes are combined and shown as a single desktop trash can. Because there is only one trash can in the metaphor, if the user empties the trash to create room on a floppy disk, the trash contents on both the floppy and the hard disk are deleted, even though there was no need to delete files from the hard disk's trash can. The desktop metaphor has enforced a limitation on the interface that does not serve the user's real needs. An alternative approach would be to simply cross out files and have them disappear (perhaps to a temporary limbo before being permanently discarded). This approach avoids the problems caused by the trash can metaphor, even though it does not have an analogy in the real world.</p>

<p>Metaphors not only constrain and mislead users, they can also limit designers' ability to invent more powerful interface mechanisms. For example, we are as guilty as anybody for using the tired book metaphor: When we recently designed the user interface to 300MB worth of Sun's online documentation, we used a book metaphor to unify the icons, the vocabulary, and the hierarchical structure of the user's navigation. Our excuse was that the interface will display information originally written as separate printed manuals and that it would have been confusing to use a free-form hyperspace model to represent this text. There is no doubt, however, that the book metaphor prevented us from introducing desirable features like the ability to reorder the chapters according to their relevance scores after a search.</p>

<p>The desktop metaphor assumes we save training time by taking advantage of the time that users have already invested in learning to operate the traditional office with its paper documents and filing cabinets. But the next generation of users will make their learning investments with computers, and it is counterproductive to give them interfaces based on awkward imitations of obsolete technologies. Instead, we need to develop new interface paradigms based on the structure of computer systems and the tasks users really have to perform, rather than paradigms that enshrine outmoded technology. The way to advance the interface is not to develop ever-more-faithful imitations of the desktop, but instead to escape the limitations of the desktop especially as computers themselves become ubiquitous [21] and are used away from the desk.</p>

<h3>Direct Manipulation</h3>

<p>Using direct manipulation, users interact directly with objects in the interface [17]. The archetypal example is to move a file from one directory to another by opening the original folder and using the mouse pointer to drag the file icon to the destination folder. This procedure works well for simple actions with a small number of objects, but as the number of actions or objects increases, direct manipulation quickly becomes repetitive drudgery. The dark side of a direct manipulation interface is that you have to directly manipulate everything. Instead of an executive who gives high-level instructions, the user is reduced to an assembly line worker who must carry out the same task over and over.</p>

<p>Direct manipulation also means that users must always operate at the atomic level. They cannot group a related series of basic actions into one high-level action or use conditionals. Suppose we have a group of images and want to convert all of the PICT files into icons (see Figure 2).</p>

<p style="width: 375px; margin-right: auto; margin-left: auto;"><img alt="Picture of 11 Macintosh icons, each representing an image file" src="http://media.nngroup.com/media/editor/2012/12/11/anti-mac-fig2.gif" style="width: 375px; height: 399px;"/> <strong> Figure 2. </strong> <em> We wish to convert all the PICT files into icons. This task involves several steps and would be very difficult to accomplish with direct manipulation. A simple scripting language provides a natural specification of the task. </em></p>

<p>If the conversion requires several steps, this will be a very tedious process with direct manipulation, but a simple scripting language provides a natural means for specifying this task. Direct manipulation also limits the precision of our actions to the precision achieved with eye-hand-mouse coordination. Language and mathematics can be more precise ("Place the bottom of the triangle level with the middle of the circle") and more dynamic ("Maintain the height of this histogram bar at 37% of that of the bar to its left"). Finally, direct manipulation requires the user to be involved in every action, but sometimes the user may not know what to do. For example, as applications become more complex and involve many files spread throughout the computer, installation and removal of applications exceeds the understanding of most users. Rather than directly draging files to the proper places, most users would prefer pressing a single button on an installation program and have the computer move the files to the appropriate places. Indeed, install and uninstall programs have become an essential adjunct of complex software packages.</p>

<h3>See-and-Point</h3>

<p>The see-and-point principle states that users interact with the computer by pointing at the objects they can see on the screen. It's as if we have thrown away a million years of evolution, lost our facility with expressive language, and been reduced to pointing at objects in the immediate environment. Mouse buttons and modifier keys give us a vocabulary equivalent to a few different grunts. We have lost all the power of language, and can no longer talk about objects that are not immediately visible (all files more than one week old), objects that don't exist yet (future messages from my boss), or unknown objects (any guides to restaurants in Boston).</p>

<p>If we want to order food in a country where we don't know the language at all, we're forced to go into the kitchen and use a see-and-point interface. With a little understanding of the language, we can point at menus to select our dinner from the dining room. But language allows us to discuss exactly what we would like to eat with the waiter or chef. Similarly, computer interfaces must evolve to let us utilize more of the power of language. Adding language to the interface allows us to use a rich vocabulary and gives us basic linguistic structures such as conditionals. Language lets us refer to objects that are not immediately visible. For example, we could say something like "Notify me if there is a new message from Emily." Note we are not advocating an interface is based solely on language. Neither does the interface have to understand full natural language. Real expressive power comes from the combination of language, examples, and pointing.</p>

<h3>Consistency</h3>

<p>Consistency is one of those principles that sounds like a great idea when you first come across it, but it is very difficult to apply the principle in any real situation where there is a wide array of conflicting things with which you can be consistent [8].</p>

<p>The basic advantage of consistency is the hope that learning will be reduced if objects with a similar function always look and behave the same. People will recognize applications more easily if they all have similar icons. Yet in the real world, people have no difficulty switching between ballpoint pens and fibertip pens even though they look somewhat different and have different controls. They are similar enough that they are both recognized as pens, whereas their varying appearances provide pleasure and clues to their slightly different functionality. It is the rich and fine-grained representation of objects in the real world that allows pens or books to have a wide variety of appearances and still be easily recognizable. As representations of objects in the computer interface become richer and more fine-grained, the need for complete consistency will drop.</p>

<p>Note that consistency is not symmetrical. Whereas we argue that objects with similar functions need not have consistent appearances or controls, it is still important objects with similar appearances have similar behavior and functions. Except as a joke, a pen that looks like a tennis shoe will not be very useful.</p>

<h3>WYSIWYG</h3>

<p>What You See Is What You Get (WYSIWYG) means your document, as it appears on the screen, accurately reflects what it will look like when it is printed. This is certainly a big improvement over earlier computer-based text formatting systems such as troff, in which there was no obvious relation between the document's appearance on the screen and what was produced by the printer, and it took many trials to get a new document to print properly.</p>

<p>The problem with WYSIWYG is that it is usually equivalent to WYSIATI (What You See Is All There Is). A document has a rich semantic structure that is often poorly captured by its appearance on a screen or printed page. For example, a word may be printed in italic font for emphasis, as part of a book title, or as part of a quotation, but the specific meaning is lost if it is represented only by the fact that the characters are italicized. A WYSIWYG document shows only the final printed representation; it does not capture the user's intentions.</p>

<p>Other text representations, such as Standard Generalized Markup Language (SGML), preserve the semantic meaning inherent in the text and have rules for converting the text and semantics into their appearance on the printed page. For example, a string of text may be labeled as a book title in SGML. In one situation that text might be printed in an italic font; in another case it might be printed in a bold font, and in a third case it might be accessed by a bibliographic retrieval program.</p>

<p>WYSIWYG assumes there is only one useful representation of the information: that of the final printed report. Although we are not arguing against a print preview function, even when the goal is to produce a printed document, it may be useful to have a different representation when preparing the document. For example, we may want to see formatting symbols or margin outlines, or it may be useful to see index terms assembled in the margin while we are composing.</p>

<p>In some sense, WYSIWYG is equivalent to the horselike tractor we discussed earlier in connection with metaphors. WYSIWYG assumes people want paper-style reports: Information might be produced (and even consumed) on screen, but it should still be structured the same way it was on paper. These days, however, who has time to read all the documents we get? In reality, people rarely read information from beginning to end the way they would have read a report in the good old days where even busy managers got at most a single report per day. Instead, electronic information should be modularized and presented in ways that encourage people to read as little as possible by popping information of interest to the specific user to the top, while enabling each user to link to backup information as needed. Object-oriented authoring and reading would allow the same piece of information to be presented at multiple levels and possibly from different perspectives.</p>

<p>We should also mention that not all users can see. For <a href="../../topic/accessibility/index.php" title="Nielsen Norman Group report: Usability for users with disabilities"> blind users</a>, information that is encoded with SGML-like rich intentional attributes can be represented in alternative formats (e.g., sound) in a much more usable fashion than is possible with simple screen readers that are limited to reading aloud from the pixels on the screen without knowing what they mean.</p>

<h3>User Control</h3>

<p>It has become almost a religious crusade among WIMP advocates that the user should be in control—the user, not the computer, should initiate and control actions. The negative side of user control is that the user has to be in control. There are many situations where we do not want to be in control: flying an airliner, for example, or watching our toast in the morning to see that it doesn't burn. Many activities in life are either so difficult or so boring and repetitive that we would like to delegate them to other people or to machines. Similarly, on the computer, there are many activities that we either do not want to control or do not know how to control. This is prime territory for agents and daemons, computer processes that tirelessly stand guard, take care of routine tasks, or have the knowledge that we lack to handle complex tasks.</p>

<p>Most computer programmers gave up complete control some time ago when they stopped writing in machine language and let assemblers, compilers, and interpreters worry about all the little details. And these days, few users still specify the exact routing for their email messages. We've learned that it's not worth the trouble and that computers can often do a better job than we can. Computers and people differ widely in their expertise. For example, people are notoriously bad at vigilance tasks, so it makes sense to let the computer take control of periodically saving our word processor documents, backing up our hard drives, and reminding us of our meetings.</p>

<p>Even if it were desirable, full control is becoming impossible with networked computers. User control assumes you are the only actor in the system, but these days, millions of people are on the Internet and can change the system behind your back. Indeed, it is one of the benefits of the Internet that new resources appear without any work on your part. It would be possible to conceptualize the Internet as a groupware application with 40 million users, but in reality, it is better thought of as a system without any centralized control in which things just happen without other users' asking you what you would like.</p>

<h3>Feedback and Dialog</h3>

<p>This principle states that the computer interface should provide the user with clear and immediate feedback on any actions initiated by the user. It is closely connected with the previous principle of user control: If the user is required to be in control of all the details of an action, then the user's needs detailed feedback. But if a sequence of activities can be delegated to an agent or encapsulated in a script, then there is no longer a need for detailed and continuous feedback. The user doesn't have to be bothered unless the system encounters a problem that it cannot handle.</p>

<p>A supervisor interacts very intensely with a new employee and solicits detailed progress reports, but as the employee gains experience and the supervisor gains confidence, the need for this detailed feedback and dialog decreases. The supervisor can assign complex tasks and be confident that "no news is good news." Rather than always providing the user with feedback on activities, the computer should be more flexible in the amount of feedback it provides. Initially, the computer could provide detailed feedback to familiarize the user with its operations and instill confidence; then the feedback could be scaled back over time and restricted to unusual circumstances or times when the user requests more feedback.</p>

<h3>Forgiveness</h3>

<p>The forgiveness principle states that user actions should generally be reversible and that users should be warned if they try to do something that will cause irreversible data loss. But even forgiveness can have a negative side. Consider as an example the common case where a user wants to copy a file to a floppy that does not have enough free space. The Macintosh gives an error message explaining there is not enough room and we must throw away 125K. But when we throw away some files and again attempt to copy the file, we get another error message stating there is not enough room unless we empty the trash, and asking if we want to empty the trash now. In this case forgiveness becomes a nuisance. The underlying problem here is that the computer has a very meager understanding of the interaction history. A stateless interface is doomed to present the inappropriate message because it cannot relate the user's action to its own prior recommendation to delete files. The computer needs to build a deeper model of our intentions and history.</p>

<h3>Perceived Stability</h3>

<p>Perceived stability means that elements in the computer interface should not be changed without the user's involvement. For example, icons on the desktop and windows should reappear as they were when the user last closed them. But it is a sure sign of a lack of confidence if a person's last words before leaving the room are, "Don't move until I get back." In a computer interface, the principle of perceived stability implies the computer will most likely make things worse on its own and the user will be unable to function in a changing environment. We're still children in the computer age, and children like stability. They want to hear the same bedtime story or watch the same video again and again. But as we grow more capable and are better able to cope with a changing world, we become more comfortable with changes and even seek novelty for its own sake.</p>

<p>One of the most compelling aspects of computer games and some computer-based learning environments is the lack of stability that derives from dividing control between the user and the computer or even among users on networked computers [7]. This should not be surprising, because the world we live and act and play in is an arena of mixed control with initiatives from individuals, their associates, and the larger environment.</p>

<p>There are many things computers and other people can do for us if we decide they don't have to maintain perceived stability. Today, electronic messages and news articles appear unbeckoned on our computer desktops. Applications have existed for some time that can automatically scan and sort incoming mail [12]. Programs such as Magnet, a Macintosh application that searches for files meeting a specified criterion and moves them into the user's folders, act as simple agents to build an improved environment for the computer user.</p>

<p>The World-Wide Web is a prime illustration of the advantages of shared control in user interfaces. The Web changes constantly, and every time users connect to a home page, they might be presented with a completely new interface. For example, AT&amp;T changes its home page daily [3], and in designing Sun's home page we decided we needed to change it drastically every month to keep the users' interest [14]: Stability can be boring! Denying the principle of perceived stability can often make the interface simpler and more intuitive. We can be easily overwhelmed by all the capabilities of a large application, but if the application discreetly rearranges the interface from time to time to offer us only the features of current interest to us, we can get the feeling that everything we need is easy to find and readily at hand.</p>

<h3>Aesthetic Integrity</h3>

<p>The principle of aesthetic integrity states that the graphic design of the interface should be simple, clean, and consistent. Screens should be visually pleasant and easy to understand. Part of the need for aesthetic integrity derives from the limited expressiveness of current computers. If computers could communicate with a richer language, it would not be so important that everything have a "single look." With networking, a person's computer world extends beyond the bounds of his or her desktop machine. Just as a city designed by a single architect with a consistent visual appearance would be difficult to navigate and somewhat boring to visit, a variety of visual designs would make our computer world more interesting, more memorable, and more comprehensible.</p>

<p>As an extreme example, the old user interface to the Internet had great aesthetic integrity, with one "ls" listing in ftp looking pretty much like any other, and with all needed information (modification date, size, and file type as indicated by the extension) shown in a severely minimalist manner. Even so, the wild and woolly extremes of Web home-page design have taken over. Users seem to prefer more illustrative indications of location and content in cyberspace. Note we do not argue for complete anarchy: With Darrell Sano we recently <a href="../1994-design-sunweb-sun-microsystems-intranet/index.php">designed SunWeb</a> (Sun's internal Web pages) to have a unified, though flexible, look [15]. We did want SunWeb to be recognizably different from, say, Silicon Surf (SGI's Web pages), but we did not want the Help buttons to appear in 20 different places.</p>

<p>As our computer world expands (especially over the network) to encompass millions of objects, these objects should not all look the same. Totally uniform interfaces will be drab and boring and will increase the risk of users' getting lost in hyperspace. Richer visual designs will feel more exciting. From a functionality perspective, richness will increase usability by making it easier for users to deal with a multiplicity of objects and to derive an understanding of location and navigation in cyberspace.</p>

<h3>Modelessness</h3>

<p>Modelessness means the computer interface should not have distinct modes that restrict the user's actions depending on the mode he or she is in. Users should be able to perform any task at any time. Although modelessness seems to be an object of veneration among some Macintosh interface designers, even the section on modelessness in the Macintosh Human Interface Guidelines [2] is primarily devoted to explaining how to use modes successfully. The basic problem presented by modelessness is that the user cannot cope with everything at once. Users need the interface to narrow their attention and choices so they can find the information and actions they need at any particular time. Real life is highly moded [11]: What you can do in the swimming pool is different from what you can do in the kitchen, and people can easily distinguish between the two because of the richness of the experience and the ease with which we can move between environments.</p>

<h2>The Anti-Mac Interface</h2>

<p>Although this article starts as an exploration of alternatives to the individual Macintosh human interface principles, it has not quite ended up that way. Just as the Macintosh design principles are interrelated and give a resulting coherence to the Macintosh interface, violation of those principles also points to a coherent interface design that we call the Anti-Mac interface.</p>

<p>The basic principles of the Anti-Mac interface are:</p>

<ul>
	<li>The central role of language</li>
	<li>A richer internal representation of objects</li>
	<li>A more expressive interface</li>
	<li>Expert users</li>
	<li>Shared control</li>
</ul>

<h3>The Central Role of Language</h3>

<p>Over the past million years, humans have evolved language as our major communication mode. Language lets us refer to things not immediately present, reason about potential actions, and use conditionals and other concepts not available with a see-and-point interface. Another important property of language missing in graphical interfaces is the ability to encapsulate complex groups of objects or actions and refer to them with a single name. An interface that can better exploit human language will be both more natural and more powerful. Finally, natural languages can cope with ambiguity and fuzzy categories. Adding the ability to deal with imprecise language to the computer interface will increase the computer's flexibility and its fit with people's normal expressive modes. As Susan Brennan has shown [4], natural language, in addition to being natural for people, has several advantages as a medium for human-computer interaction, including the role of negotiated understanding, use of shared context, and easy integration with pointing and other input/output channels.</p>

<p>We are not proposing, however, what AI researchers would call a "natural language interface." It seems a computer that can hold a normal conversation with the user will remain in the realm of science fiction for some time yet, and we are interested in computer-human interfaces for the near future. Instead, we have in mind something more like the interfaces of text-based adventure games, with their understanding of synonyms, relatively simple syntax, and tolerance for error in the input-—a pidgin language for computers. The critical research question is, "How can we capture many of the advantages of natural language input without having to solve the 'AI-Complete' problem of natural language understanding?" Command line interfaces have some of the advantages of language, such as the large number of commands always available to the user and the rich syntactic structures that can be used to form complex commands. But command line interfaces have two major problems. First, although the user can type anything, the computer can understand only a limited number of commands and there is no easy way for the user to discover which commands will be understood. Second, the command line interface is very rigid and cannot tolerate synonyms, misspellings, or imperfect grammar. We believe that both these deficiencies can be dealt with through a process of negotiation.</p>

<p>Think of the way a new library user might interact with a reference librarian. A librarian who had a command line interface would understand only a limited number of grammatically perfect queries, and the novice user would have to consult an obscure reference manual to learn which queries to write out. A reference librarian with a WIMP interface would have a set of menus on his or her desktop; the user would search the menus and point to the appropriate query. Neither interface seems very helpful. Instead, real reference librarians talk with the user for a while to negotiate the actual query. Similarly, we envision a computer interface that utilizes a thesaurus, spelling correction, displays of what is possible, and knowledge of the user and the task to take part in a negotiation of the user's command. We could imagine, for example, a dialog in which the user makes a free-form request, the computer responds with a list of possible tasks that seem to match the request, and both engage in a dialog to focus on the request the user actually intended.</p>

<h3>A Richer Internal Representation of Objects</h3>

<p>Current interfaces have access to very little information about the objects he user deals with. For example, the only information known about a file may be its name, size, and modification date; the type of data it contains; and the application that created it. In order to deal more effectively with these objects, the computer needs a much richer representation of them. For a document, this representation could include its authors, topic matter, keywords, and importance; whether there are other copies; what other documents it is related to; and so forth. The list of attributes is similar to what would be needed by a good secretary who was expected to handle the documents intelligently. It is not necessary for the secretary to fully understand the document's contents, but he or she must have a general sense of what the document is about and of its significance. If a computer interface is to handle documents intelligently, it must have the same sorts of information.</p>

<p>Much of this information does not require natural language understanding. We already have techniques in full-text search systems that could be used to automatically extract this information from the document. As we move to text systems with tags based on meaning rather than a WYSIWYG system, much of this information will be available in the document itself. Further, if we allow the computer more control over the interface, it could monitor what we do with the objects and change their representations accordingly [10]. For example, if two objects were almost always used together, the computer could create a hypertext link between them.</p>

<h3>A More Expressive Interface</h3>

<p>The richer internal representation of objects will allow more intelligent interpretation of user commands, but it will also be reflected in a more expressive interface with a richer external representation. The Macintosh interface was originally designed for a 9-inch display that contained less than 200,000 black-and-white pixels. It is remarkable how well it has translated to today's 21-inch displays that contain 2,000,000 color pixels---an increase of a factor of 240 in information capability. This trend will continue until displays approach the size of a desk and the practical resolution of the human eye (an additional factor of 340) and the interface should take advantage of this change to increase the expressiveness of the objects it displays.</p>

<p style="width: 400px; margin-right: auto; margin-left: auto;"><img alt="Photo of a bookshelf with various books" src="http://media.nngroup.com/media/editor/2012/12/11/anti-mac-fig3.jpg" style="width: 234px; height: 400px;"/><br/>
<strong>Figure 3. </strong> <em> The variety and rich visual affordances of a bookcase make it easy to recognize objects of interest. </em></p>

<p>Notice in Figure 3 how the books on a bookshelf have a wide variety of appearances and yet are all recognizable as books. This variety adds visual interest and helps us quickly locate a particular book. Yet in our computer interfaces, all documents of a given application usually appear identical. This practice is starting to change—some graphics applications represent documents with thumbnail images, but that is only a beginning. Improved displays and sound can give us a computer world that is as comfortable to navigate as the real world.</p>

<p>In addition to richer output, richer input devices will allow the user more flexibility in manipulating objects, and monitoring devices like active badges [20] will allow the computer to accommodate the user's needs without explicit commands.</p>

<h3>Expert Users</h3>

<p>The GUIs of contemporary applications are generally well designed for ease of learning, but there often is a trade-off between ease of learning on one hand, and ease of use, power, and flexibility on the other hand. Although you could imagine a society where language was easy to learn because people communicated by pointing to words and icons on large menus they carried about, humans have instead chosen to invest many years in mastering a rich and complex language.</p>

<p>Today's children will spend a large fraction of their lives communicating with computers. We should think about the trade-offs between ease of learning and power in computer-human interfaces. If there were a compensating return in increased power, it would not be unreasonable to expect a person to spend several years learning to communicate with computers, just as we now expect children to spend 20 years mastering their native language.</p>

<p>It is sometimes claimed that humans have limited cognitive abilities and therefore will not be able to handle increasingly complex computers. Even though the human brain may stay about the same for the foreseeable future, it is demonstrably not true that a fixed human brain implies fixed abilities to use specific systems. As an example, consider the ability to survive in a jungle without modern weapons. Most readers of this article would probably fare rather poorly if they were dropped in a jungle and told to catch dinner with their bare hands. At the same time, anybody born and raised in the same jungle would probably perform the dinner-catching task superbly. The difference in performance between the two groups is due to the benefits of having grown up in an environment and fully internalized its signals and rules. As the example shows, two people with the same brain capacity can have very different capabilities when it comes to using a given system. We predict that people who have grown up with computers will be much more capable as computer users than the current generation of users. Thus, they will be able to use (and will in fact, demand) expressive interfaces with advanced means of expressing their wants.</p>

<h3>Shared Control</h3>

<p>But the entire burden should not be on the individual user. Computer-based agents have gotten attention from computer scientists and human interface designers in recent years, but their capabilities have not yet progressed beyond the most simple-minded tasks. Before people will be willing to delegate complex tasks to agents, we need agents skilled both in specific task areas and in communicating with users. The recent rapid rise in use of the Web illustrates the advantage of sharing control of your computer world with other people. By relinquishing control over a portion of the world, you can utilize the products of other people's efforts, knowledge, and creativity.</p>

<p>A computer environment in which both humans and computer-based agents have active roles implies a mixed locus of control [13]. The user's environment will no longer be completely stable, and the user will no longer be totally in control. For general sanity, users still need to determine the balance of control in the different parts of their environment, they need the means to find out what their agents have been up to, and they need mechanisms for navigating in the wider networked world; but the Anti-Mac interface trades some stability for the richer possibilities of a shared world. In the real world, we don't want anyone rearranging the mess on our desks, but we don't mind if someone puts a Post-It note on our computer screen, empties the wastebasket overnight, or refills the newspaper stand with the latest edition.</p>

<h3>Mutually Reinforcing Design Principles</h3>

<p>During the relatively short history of computing, several changes have taken place in the ways computers are used and in the people who form the main user community. These changes in usage and users lead to important changes in user interfaces. Highly usable designs reflect a set of principles that are mutually reinforcing and that match the needs of the main users and their tasks.</p>

<p>For example, the original text-based Unix user interface was very appropriate when it was designed. The command-line interface was suited for a user community dominated by programmers who had the expertise, interest, and time to understand how the many different system features could be combined for optimal use through mechanisms like piping and shell scripts. Early Unix users manipulated relatively few data objects (their own code, text files, and memos) that were mostly plain Ascii text. The Unix design principles of modular commands, with many parameters and options, worked well for the chosen users and their tasks. As evidenced by several attempts to build graphical user interfaces on top of Unix, it is not possible to modify the design by simply changing one or two aspects (e.g., by representing files as icons), since the full set of design attributes were chosen to work together. The successful modifications of Unix have been those that combine several new design principles to achieve a new mix of mutually reinforcing design elements (e.g., combining icons with drag-and-drop and multiple workspaces to structure the user's tasks).</p>

<p>Similarly, the Macintosh was optimized for a certain user community with a certain main type of data: knowledge workers without any computer science background who wanted to manipulate graphic documents, but not overwhelmingly many of them. The various Macintosh user interface principles work well together and help the chosen user category achieve its main goals. Since the principles are mutually reinforcing, it may not be easy to break just a few as changing users and tasks lead to new requirements. For example, as argued earlier, it becomes impossible to make all objects visible in the interface as system usage changes to deal with millions of information objects. But having invisible objects not only breaks the design principle of see-and-point, it makes direct manipulation impossible, risks weakening the chosen metaphors, and breaks perceived stability if some objects are made visible anyway at certain times.</p>

<p>The Anti-Mac principles outlined here are optimized for the category of users and data that we believe will be dominant in the future: people with extensive computer experience who want to manipulate huge numbers of complex information objects while being connected to a network shared by immense numbers of other users and computers. These new user interface principles also reinforce each other, just as the Mac principles did. The richer internal representation of objects naturally leads to a more expressive external representation and to increased possibilities for using language to refer to objects in sophisticated ways. Expert users will be more capable of expressing themselves to the computer using a language-based interface and will feel more comfortable with shared control of the interface because they will be capable of understanding what is happening, and the expressive interface will lead to better explanations.</p>

<h2>Conclusions</h2>

<p>First, to avoid misunderstandings, let us reiterate that we are not criticizing the Macintosh interface because we think it is bad, but because we view it as a starting point for considering the differing needs future user interfaces must meet. Table 2 compares some of the characteristics of the original Macintosh and Anti-Mac user interfaces.</p>

<p> </p>

<table style="border: 1px solid rgb(204, 204, 204); text-align: left; margin-right: auto; margin-left: auto; border-collapse: collapse;">
	<caption align="bottom"><strong>Table 2. </strong> <em> A comparison of the Mac and Anti-Mac interfaces. </em></caption>
	<tbody>
		<tr>
			<th style="border-width: 1px; border-style: solid; padding: 0.5ex;">Mac</th>
			<th style="border-width: 1px; border-style: solid; padding: 0.5ex;">Anti-Mac</th>
		</tr>
		<tr>
			<td style="border-width: 1px; border-style: solid; padding: 0.5ex;">Users are "the rest of us" (have no previous computer experience)</td>
			<td style="border-width: 1px; border-style: solid; padding: 0.5ex;">Users are "The Post-Nintendo Generation" (grown up with computers)</td>
		</tr>
		<tr>
			<td style="border-width: 1px; border-style: solid; padding: 0.5ex;">Office automation "productivity" applications</td>
			<td style="border-width: 1px; border-style: solid; padding: 0.5ex;">Work, play, groupware, embedded, and ubiquitous</td>
		</tr>
		<tr>
			<td style="border-width: 1px; border-style: solid; padding: 0.5ex;">Weak computer (128K RAM, 68000 CPU)</td>
			<td style="border-width: 1px; border-style: solid; padding: 0.5ex;">Humongous computer (multi-gigabyte RAM, Cray-on-a-chip RISC processors)</td>
		</tr>
		<tr>
			<td style="border-width: 1px; border-style: solid; padding: 0.5ex;">Impoverished communication bandwidth (small screen, keyboard/mouse input)</td>
			<td style="border-width: 1px; border-style: solid; padding: 0.5ex;">Rich communication (computer can see you, knows where you are, large high-res screen, new I/O devices)</td>
		</tr>
		<tr>
			<td style="border-width: 1px; border-style: solid; padding: 0.5ex;">Stand-alone system that is stable unless the user decides to make a change</td>
			<td style="border-width: 1px; border-style: solid; padding: 0.5ex;">Connected system subjected to constant change</td>
		</tr>
		<tr>
			<td style="border-width: 1px; border-style: solid; padding: 0.5ex;">Manipulation of icons</td>
			<td style="border-width: 1px; border-style: solid; padding: 0.5ex;">Language</td>
		</tr>
		<tr>
			<td style="border-width: 1px; border-style: solid; padding: 0.5ex;">Weak object-orientation (small number of large objects with very few attributes)</td>
			<td style="border-width: 1px; border-style: solid; padding: 0.5ex;">Strong object-orientation (large number of small objects with rich attribute sets)</td>
		</tr>
		<tr>
			<td style="border-width: 1px; border-style: solid; padding: 0.5ex;">"Finder" (visible file system) is unifying home base, and files are the basic interaction object</td>
			<td style="border-width: 1px; border-style: solid; padding: 0.5ex;">Personal information retrieval as unifying principle with atomic information units as basic interaction object</td>
		</tr>
		<tr>
			<td style="border-width: 1px; border-style: solid; padding: 0.5ex;">Surf your hard-drive</td>
			<td style="border-width: 1px; border-style: solid; padding: 0.5ex;">Information comes to you</td>
		</tr>
		<tr>
			<td style="border-width: 1px; border-style: solid; padding: 0.5ex;">"The Power to Be Your Best" (<em>ed. </em> Apple's slogan at the time)</td>
			<td style="border-width: 1px; border-style: solid; padding: 0.5ex;">You won't always have to work that hard</td>
		</tr>
	</tbody>
</table>

<p><a href="http://www.asktog.com/starfire/">Bruce Tognazzini's <cite> Starfire </cite> film</a> [19] was an attempt to show how a high-end workstation might look in the year 2004. The interface visualized in the film has several similarities to our Anti-Mac design: The system understands objects well enough to integrate them seamlessly (it can construct a 3D texture map of a person from a 2D video image); the screen is very large, with fairly expressive interface elements; the computer has some independent initiative (when the heroine searches for an article reprint, the agent automatically decides to search further and display subsequent articles with links to the original article); and the computer monitors the user (it does not attempt speech recognition while she is talking with a colleague).</p>

<p>Despite its Anti-Mac aspects, <cite> Starfire </cite> is still a very recognizable computer with many similarities to current user interfaces and research systems. Indeed, there are some Anti-Mac features in evidence in current commercial products. For example, products like On Location and SearchIt show autonomy in taking the initiative to index the user's file system when needed and allow users to retrieve email objects by content and other rich attributes even though each email message may be part of a large text file when viewed through the standard system interface.</p>

<p>Any realistic person who reads the preceding outline of the Anti-Mac interface will realize it is mostly impractical with the current state of computers and software. Language understanding is still in its infancy; most of the population are having their first encounters with computers; and most users know better than to trust computer-based agents with a free rein on their systems. Today's standard WIMP interfaces are fairly well suited to current hardware and software capabilities. But hardware designers are not slowing down, and the challenge for application and interface designers is to take advantage of the coming computing power to move the computer-human interface to a new plateau. Interface designers need to start work on these issues now so that solutions will be available when the next generation of computers arrives, and also to help guide the directions of hardware and software development.</p>

<p>To realize the full benefits from the Anti-Mac approach, we argue, it will not be sufficient to retrofit its features one at a time to systems that basically follow current user interface architectures. We believe an integrated design that builds a new user interface from the bottom up will be more coherent and will have a better potential for increasing user productivity by at least an order of magnitude. A full Anti-Mac system will likely have to be based on deep object structures in an architecture that supports network-distributed objects and detailed attributes that are sufficiently standardized to be shared among multiple functionality modules. Realistically speaking, such a complete redesign will take some time to appear. Even if the full system were not to appear for several years to come, it is necessary for detailed research to begin now to develop the needed features and to collect usability data on how the Anti-Mac characteristics should be shaped to truly meet users' needs.</p>

<h3>Acknowledgments</h3>

<p>We would like to thank Bruce Tognazzini, Ellen Isaacs, Robin Jeffries, Jonathan Grudin, Tom Erickson, and the members of the human interface group at SunSoft for valuable comments on this article.</p>

<p> </p>

<h2>References</h2>

<ol>
	<li>Alfven, H. <em> Worlds-Antiworlds</em>. W. H. Freeman, San Francisco, 1966.</li>
	<li>Apple Computer. <em> Macintosh Human Interface Guidelines</em>. Addison-Wesley, Reading, Mass., 1992.</li>
	<li>AT&amp;T. Home page at URL <a href="http://www.att.com/"> http://www.att.com </a></li>
	<li>Brennan, S. E. Conversation as direct manipulation: An iconoclastic view. In <em> The Art of Human-Computer Interface Design</em>, B. Laurel, Ed. Addison-Wesley, Reading, Mass, 1990, pp. 393-404.</li>
	<li>Clymer, F. <em> Treasury of Early American Automobiles</em>. McGraw-Hill, New York, 1950.</li>
	<li>Cypher, A. Eager: Programming repetitive tasks by example. In <em> Proceedings of the ACM CHI'91 Conference Human Factors in Computing Systems </em> (New Orleans, April 28-May 2) 1991, pp. 33-39.</li>
	<li>Gentner, D. R. Interfaces for learning: motivation and locus of control. In <em> Cognitive Modelling and Interactive Environments in Language </em> Learning, F. L. Engel, D. G. Bouwhuis, T. Boesser, and G. d'Ydewalle, Eds. NATO ASI Series, vol. F 87, Springer-Verlag, Berlin, 1992.</li>
	<li>Grudin, J. The case against user interface consistency. <em> Commun. ACM </em> <strong> 32</strong>, 10 (1989), 1164-1173.</li>
	<li>Halasz, F., and Moran, T. P. Analogy considered harmful. In <em> Proceedings of the ACM Conference on Human Factors in Computer Systems </em> (Gaithersburg, Md., March 15-17) 1982, pp. 383-386.</li>
	<li>Hill, W.C., Hollan, J.D., Wroblewski, D., and McCandless, T. Edit wear and read wear. In Proceedings of the ACM CHI'92 Conference on Human Factors in Computing Systems., (Monterey, Calif., May 3-7) 1992, pp. 3-9.</li>
	<li>Johnson, J. Modes in non-computer devices. <em> Int. J. Man-Mach. Stud</em>. <strong> 32 </strong> (1990), 423-438.</li>
	<li>Malone, T. W., Grant, K. R., Turbak, R. A., Brobst, S. A., and Cohen, M. D. Intelligent information-sharing systems. <em> Commun. ACM </em> <strong> 30</strong>, 5 (1987), 484-497.</li>
	<li>Nielsen, J. <a href="../noncommand/index.php"> Noncommand user interfaces</a>. <em> Commun. ACM </em> <strong> 36</strong>, 4 (1993), 83-99.</li>
	<li>Nielsen, J. <a href="../1995-design-sun-microsystems-website/index.php"> Interface design for Sun's WWW site</a>. (June 1995).</li>
	<li>Nielsen, J., and Sano, D. <a href="../1994-design-sunweb-sun-microsystems-intranet/index.php"> SunWeb: User interface design for Sun Microsystems' internal web</a>. In <em> Proceedings of the Second International WWW Conference '94: Mosaic and the Web</em>. (Chicago, Oct. 17-20).</li>
	<li>Open Software Foundation. <em> OSF/Motif Style Guide </em> . Prentice-Hall, Englewood Cliffs, N.J., 1993.</li>
	<li>Shneiderman, B. Direct manipulation: A step beyond programming languages. <em> IEEE Comput</em>. <strong> 16</strong>, 8 (1983), 57-69.</li>
	<li>Sun Microsystems, Inc. <em> OPEN LOOK: Graphical User Interface Application Style Guidelines</em>. Addison-Wesley, Reading, Mass, 1990.</li>
	<li>Tognazzini, B. The <cite> Starfire </cite> video prototype project: A case history. In <em>Proceedings of the ACM CHI'94 Conference on Human Factors in Computing Systems </em> (Boston, April 24-28), pp. 99-105. <a href="http://www.asktog.com/starfire/"> Film clips available</a>.</li>
	<li>Want, R., Hopper, A., Falco, V., and Gibbons, J. The active badge location system. <em> ACM Trans. Inf. Syst</em>. <strong> 10</strong>, 1 (1992), 91-102.</li>
	<li>Weiser, M. The computer for the 21st century. Sci. Am. 265, 3 (1991), 94-104.</li>
	<li><em>The Windows Interface: An Application Design Guide</em>. Microsoft Press, Redmond, Wash., 1992.</li>
</ol>
</section>
  

  
  <section>
    
        
    
  </section>
  
  
  
  
  

  


  <section>
    <br />
    <p class="share"><b>Share <span class="hide-for-small-down">this article</span>:</b> <a href="https://twitter.com/intent/tweet?url=http://www.nngroup.com/articles/anti-mac-interface/&amp;text=The%20Anti-Mac%20Interface&amp;via=nngroup">Twitter</a> | <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=http://www.nngroup.com/articles/anti-mac-interface/&amp;title=The%20Anti-Mac%20Interface&amp;source=Nielsen%20Norman%20Group">LinkedIn</a> | <a href="https://plus.google.com/share?url=http://www.nngroup.com/articles/anti-mac-interface/">Google+</a> | <a href="mailto:?subject=NN/g Article: The Anti-Mac Interface&amp;body=http://www.nngroup.com/articles/anti-mac-interface/">Email</a></p>
    <hr />
  </section>

  <footer class="l-subsection">
    
    
    <div class="learn-more">
      <h2>Learn More</h2>
      <p><a href="../subscribe/index.php">Subscribe to the weekly newsletter</a> to get notified about future articles.</p>
      </div>
    
    

    <div class="row show-for-small-down related-content">
      
      
      

      
      
        <div class="column small-12 topic-links">All articles about:
          <ul>
            
            <li><a href="../../topic/applications/index.php">Application Design</a></li>
            
            <li><a href="../../topic/direct-manipulation/index.php">direct manipulation</a></li>
            
            <li><a href="../../topic/gui/index.php">graphical user interfaces</a></li>
            
            <li><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction</a></li>
            
            <li><a href="../../topic/ux-ideation/index.php">Ideation</a></li>
            
          </ul>
        </div>
      
      

      
      
        <div class="column small-12">
          <h3>UX Conference Training Courses</h3>
           <ul class="no-bullet">
    
        <li><a href="../../courses/ux-basic-training/index.php">UX Basic Training</a></li>
    
        <li><a href="../../courses/hci/index.php">User Interface Principles Every Designer Must Know</a></li>
    
        <li><a href="../../courses/ideation/index.php">Effective Ideation Techniques for UX Design</a></li>
    
        <li><a href="../../courses/generating-big-ideas/index.php">Generating Big Ideas with Design Thinking</a></li>
    
        <li><a href="../../courses/interaction-design-3-day-course/index.php">Interaction Design: 3-Day Course</a></li>
    
</ul>
        </div>
      
      

      
      
        <div class="small-12 column">
          <h3>Research Reports</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../reports/best-applications-2/index.php">Application Design Showcase: 2012</a></li>
              
            
              
                <li><a href="../../reports/enterprise-mobile-showcase/index.php">Mobile Intranets and Enterprise Apps</a></li>
              
            
              
                <li><a href="../../reports/website-tools-and-applications-flash/index.php">Website Tools and Applications with Flash</a></li>
              
            
              
                <li><a href="../../reports/customization-features/index.php">Customization Features Done Correctly for the Right Reasons</a></li>
              
            
              
                <li><a href="../../reports/best-applications-1/index.php">Application Design Showcase: 2008</a></li>
              
            
          </ul>
        </div>
      
      

      
      
        <div class="small-12 column">
          <h3>Online Seminars</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../online-seminars/ux-big-data/index.php">UX for Big Data</a></li>
              
            
          </ul>
        </div>
      
      
    </div>

    <div class="hide-for-small-down row related-content">
      <div class="medium-6 columns">
        
        
          <div>
            <h3>Articles</h3>
            <ul class="no-bullet">
              
                
                <li><a href="../direct-manipulation/index.php">Direct Manipulation: Definition</a></li>
                
              
                
                <li><a href="../expandable-menus/index.php">Expandable Menus: Pull-Down, Square, or Pie? </a></li>
                
              
                
                <li><a href="../ux-quiz-15/index.php">User-Experience Quiz: 2015 UX Year in Review</a></li>
                
              
                
                <li><a href="../closeness-of-actions-and-objects-gui/index.php">Closeness of Actions and Objects in GUI Design</a></li>
                
              
                
                <li><a href="../rip-wysiwyg/index.php">R.I.P. WYSIWYG</a></li>
                
              
            </ul>
          </div>
        
        

        
          
          <div>
            <h3>Research Reports</h3>
            <ul class="no-bullet">
              
                
                  <li><a href="../../reports/best-applications-2/index.php">Application Design Showcase: 2012</a></li>
                
              
                
                  <li><a href="../../reports/enterprise-mobile-showcase/index.php">Mobile Intranets and Enterprise Apps</a></li>
                
              
                
                  <li><a href="../../reports/website-tools-and-applications-flash/index.php">Website Tools and Applications with Flash</a></li>
                
              
                
                  <li><a href="../../reports/customization-features/index.php">Customization Features Done Correctly for the Right Reasons</a></li>
                
              
                
                  <li><a href="../../reports/best-applications-1/index.php">Application Design Showcase: 2008</a></li>
                
              
            </ul>
          </div>
          
        

      </div>
      <div class="medium-6 columns">
        
          
          <div>
            <h3>UX Conference Training Courses</h3>
             <ul class="no-bullet">
    
        <li><a href="../../courses/ux-basic-training/index.php">UX Basic Training</a></li>
    
        <li><a href="../../courses/hci/index.php">User Interface Principles Every Designer Must Know</a></li>
    
        <li><a href="../../courses/ideation/index.php">Effective Ideation Techniques for UX Design</a></li>
    
        <li><a href="../../courses/generating-big-ideas/index.php">Generating Big Ideas with Design Thinking</a></li>
    
        <li><a href="../../courses/interaction-design-3-day-course/index.php">Interaction Design: 3-Day Course</a></li>
    
</ul>
          </div>
          
        
        
          
          <div>
            <h3>Online Seminars</h3>
            <ul class="no-bullet">
            
                <li><a href="../../online-seminars/ux-big-data/index.php">UX for Big Data</a></li>
            
            </ul>
          </div>
          
        
      </div>
    </div>
  </footer>
</article>

        </div>

      

    
  </div>
  <div class="row">
    
      <div class="column small-12 show-for-small-down l-navrail announcements">
        
          
        
      </div>
    
    
  </div>

    </div>
    




<footer id="page-footer">
    <div class="row hide-for-print">
        <div class="column small-12 medium-6">
            <section class="footer-popular">
                
                <h1>Popular Topics</h1>
                <div style="float: left;  width: 15em; display: block;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/applications/index.php">Application Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/e-commerce/index.php">E-Commerce Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/eyetracking/index.php">Eyetracking </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/information-architecture/index.php">Information Architecture </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/intranets/index.php">Intranet Design </a></li>
	<li><a href="../../topic/interaction-design/index.php">Interaction Design </a></li>
</ul>
</div>

<div style="float: left;  width: 15em; display: block; list-style: none;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/mobile-and-tablet-design/index.php">Mobile and Tablet Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/credibility-persuasion/index.php">Persuasive Design</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/research-methods/index.php">Research Methods</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/user-testing/index.php">User Testing </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/web-usability/index.php">Web Usability </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/writing-web/index.php">Web Writing &amp; Content Strategy </a></li>
</ul>
</div>

<p>&nbsp;</p>

                
            </section>
        </div>

        <div class="column small-12 medium-6">
            <div class="row">
                <div class="column small-6">
                    <section class="footer-about">
                        <h1>Follow Us</h1>
                        <ul>
	<li><a href="../subscribe/index.php">Subscribe to our weekly UX newsletter</a>,&nbsp;Jakob Nielsen&#39;s Alertbox</li>
	<li>Visit Don Norman&#39;s website, <a href="http://www.jnd.org/"> www.jnd.org </a></li>
	<li>Visit Bruce Tognazzini&#39;s website, <a href="http://www.asktog.com/"> www.asktog.com </a></li>
	<li style="padding-top: 6px;"><a class="twitter-follow-button" data-show-count="false" data-size="large" href="https://twitter.com/nngroup"><img alt="Follow @nngroup on Twitter" height="20" src="https://media.nngroup.com/media/editor/2016/12/30/twitter-follow-button.png" style="width: 117px; height: 20px;" width="117" /></a></li>
	<li style="padding-top: 6px;"><img alt="" height="14" src="https://media.nngroup.com/media/editor/2016/07/26/feed-icon-14x14.png" style="vertical-align:text-top;width:14px;margin-right:.25em;" width="14" /><a href="../../feed/rss/index.php">Subscribe to our RSS feed</a></li>
</ul>

                    </section>
                </div>

                <div class="column small-6">
                    <section class="footer-about right-column">
                        <h1>About
</h1>
                        <ul class="no-bullet">
	<li><a href="../../about/contact/index.php">Contact information</a></li>
	<li><a href="../../about/index.php">About Us</a></li>
	<li><a href="../../about/why-nng/index.php">Why NN/g? </a></li>
	<li><a href="../../return-policy/index.php">Return Policy</a></li>
</ul>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="copyright column small-12">
            <p><a href="../../copyright-and-reprint-info/index.php">Copyright</a> &copy; 1998-2017 Nielsen Norman Group, All Rights Reserved.</p>
        </div>
    </div>
</footer>


    


        <script src="https://media.nngroup.com/static/vendor/fastclick/js/fastclick.min.js"></script>
        <script src="https://media.nngroup.com/static/vendor/foundation/js/foundation.min.js"></script>
        <script src="http://media.nngroup.com/static/js/app.js"></script>
        <script src="http://media.nngroup.com/static/js/scroll-to-fixed.js"></script><script>var externalLinks = document.querySelectorAll('a[href^="http"]');for (var i = externalLinks.length-1; i >= 0; i--) {externalLinks[i].addEventListener("click", function(event) { alert("External links are disabled for this test."); event.preventDefault(); }, false);}var mailLinks = document.querySelectorAll('a[href^="mailto"]');for (var i = mailLinks.length-1; i >= 0; i--) {mailLinks[i].addEventListener("click", function(event) { alert("Email links are disabled for this test."); event.preventDefault(); }, false);}var externalForms = document.querySelectorAll('form');for (var i = externalForms.length-1; i >= 0; i--) {externalForms[i].addEventListener("submit", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false); externalForms[i].addEventListener("click", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false);}</script>

        <script>
            
            $(function() {
                $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
                $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
            });
        </script>

        
<script src="https://media.nngroup.com/static/js/articles.js"></script>


        
        <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1072699960;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072699960/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
    </body>

<!-- Mirrored from www.nngroup.com/articles/anti-mac-interface/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:14:00 GMT -->
</html>
