<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->





    
<!-- Mirrored from www.nngroup.com/articles/3d-touch/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:08:06 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1026.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"licenseKey":"1a30ef9931","queueTime":0,"beacon":"bam.nr-data.net","errorBeacon":"bam.nr-data.net","applicationID":"2048992","transactionName":"NFxSMRBUX0EDUkIPDA0WdhAMVkVbDV8ZBxEXUFMJB0YfRAtUQRVZIktEDAFZVHYHRVcPD01eVRE=","applicationTime":601,"agent":""}</script>
        <title>3D Touch on iPhone 6S: Embrace the Force</title><meta property="og:title" content="3D Touch on iPhone 6S: Embrace the Force" />
  
        
        <meta name="description" content="3D Touch poses some physical challenges for users. Designers should use it as an enhancement, not a requirement for a good user experience.">
        <meta property="og:description" content="3D Touch poses some physical challenges for users. Designers should use it as an enhancement, not a requirement for a good user experience." />
        
  
        
	
        
        <meta name="keywords" content="3D Touch, iPhone, iOS, iOS 9, mobile UX, mobile design, mobile usability, touch gestures, peek and pop, quick actions, force touch, feedback, gestural user interfaces, gestures">
        

        

        <meta name="robots" content="noarchive">
        <meta name="viewport" content="width=device-width">

        <link rel="search" type="application/opensearchdescription+xml" title="NN/g Search" href="../../opensearch.xml" />
        <link rel="shortcut icon" href="http://media.nngroup.com/static/img/favicon.ico" />

        <link rel="stylesheet" href="https://media.nngroup.com/static/vendor/font-awesome/css/font-awesome.min.css">

        
        <link rel="stylesheet" href="https://media.nngroup.com/static/css/7232a960f1df.css" type="text/css" />
        <link rel="stylesheet" href="http://fast.fonts.com/t/1.css?apiType=css&amp;projectid=c5846260-9def-46ec-9407-0f1349bd2d64" type="text/css">

        <script src="https://media.nngroup.com/static/vendor/modernizr/js/modernizr.min.js"></script>

        <script type="text/javascript" src="https://media.nngroup.com/static/js/3283be5def74.js"></script>
        

        

        
        
        <!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=229034,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('http://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->


<!-- Google Analytics Tracking Code for http://nngroup.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23132681-1', 'nngroup.com');
  ga('require', 'linkid', 'https://www.nngroup.com/articles/3d-touch/linkid.js');
  ga('require', 'displayfeatures');

// Optimizely Universal Analytics Integration
 window.optimizely = window.optimizely || [];
 window.optimizely.push("activateUniversalAnalytics");

  ga('send', 'pageview');

</script>
<script>
/**
* Function to track a click on an outbound link in Google Analytics.
*/
function trackOutboundLink(label, url) {
    if(window.ga && ga.loaded) {
        ga('send', 'event', 'outbound', 'click', label, {'hitCallback': function(){document.location = url;}
        });
    } else {
        document.location = url;
    }
}
/**
* Function to track a click on a link in the article announcement area.
*/
$('#articleAnnouncement a').on('click', function(){
  ga('send', 'event', 'link', 'click', 'articleAnnouncement');
});
</script>

<script type="text/javascript">
 setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2421.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



<!-- Hotjar Tracking Code for http://nngroup.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:121605,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

        <script src="https://media.nngroup.com/static/js/analytics/track_outbound_link.js"></script>
        
        
        
    </head>
    <body class="location-articles articles-detail">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
    <header id="page-header">
        <div id="cart" class="shopping-cart mobile-cart show-for-small-down"></div>
        <div class="row">
            <div id="masthead" class="masthead small-12 medium-7 column">

                <!-- The button to toggles menu on small screen -->

                <h1><a href="../../index.php"><img src="http://media.nngroup.com/static/img/logo-wordmark.png" width="74" height="30"> Nielsen <span class="norman">Norman</span> Group</a></h1>
                <p class="tagline show-for-medium-up">Evidence-Based User Experience Research, Training, and Consulting</p>

                <!-- The button to toggles form search on small screen -->


                <div style="clear: right;width:  100%;height: 1px;"></div><?php define('ROOT_PATH',$_SERVER["DOCUMENT_ROOT"] . '/apps/navigation_styles/navigations/3'); ?><?php include(ROOT_PATH)."/navigation.php"; ?>

                <!-- Drop down menu on small screen -->
                <div id="hideNav" class="show-for-small-down">
                  
                  <ul class="menu-content">
                    <li><a href="../../index.php">Home</a></li>
                    <li class="has-submenu">
                      <a>Training</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../training/index.php">Overview</a></li>
                        
<li id="nav-ux-conference"><a href="../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../online-seminars/index.php"><span>Online Seminars</span></a></li>

                      </ul>
                    </li>
                    <li class="has-submenu">
                      <a>Consulting</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
  <li><a href="../../consulting/index.php">Overview</a></li>
  
  <li><a href="../../consulting/benchmarking/index.php">Benchmarking</a></li>
  
  <li><a href="../../consulting/expert-design-review/index.php">Expert Design Review</a></li>
  
  <li><a href="../../consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
  
  <li><a href="../../consulting/usability-testing/index.php">Usability Testing </a></li>
  
  <li><a href="../../consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
  
  <li><a href="../../in-house-training/index.php">In-House Training</a></li>
</ul>

                    </li>

                    <li><a href="../../reports/index.php">Reports</a></li>
                    <li><a href="../index.php">Articles</a></li>
                    <li class="has-submenu"><a>About NN/G</a><i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../about/index.php">Overview</a></li>
                        <li><a href="../../people/index.php">People</a></li>
                        <li><a href="../../about/why-nng/index.php">Why NN/g?</a></li>
                        <li><a href="../../about/contact/index.php">Contact</a></li>
                        <li><a href="../../news/index.php">News</a></li>
                        <li><a href="../../about/history/index.php">History</a></li>
                        <li><a href="../../books/index.php">Books</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                </div>

                <hr class="header-divider show-for-small-only"/>


            </div>
       </div>

        <div id="hideNav">            <nav class="primary-nav small-12 column">
                <ul>
                    <li><a class="navitem" id="nav-home" href="../../index.php">Home</a></li>
                    <li><a class="navitem" id="nav-training" href="../../training/index.php">Training</a></li>
                    <li><a class="navitem" id="nav-consulting" href="../../consulting/index.php">Consulting</a></li>
                    <li><a class="navitem" id="nav-reports" href="../../reports/index.php">Reports</a></li>
                    <li><a class="navitem" id="nav-articles" href="../index.php">Articles</a></li>
                    <li><a class="navitem" id="nav-about" href="../../about/index.php">About NN/G</a></li>
                </ul>
            </nav>
            <nav class="secondary-nav small-12 medium-12 large-12 columns">
                
                
            </nav>
        </div>
    </header>
    <div id="mainContent" class="l-content">
    
  

  

  <div class="row">
    
      
      <section class="small-12 medium-3 columns collapsable-content l-navrail articles-left-column">
        <h1 class="show-for-small-down collapse-link mobile-browse-header">Browse by Topic and Author</h1>
          <div class="row collapsable">

            
            <div class="small-6 medium-12 columns">
              <section id="article-popular-topics" style="display: none;">
                
                <h1>Topics</h1>
                <ul id="article-popular-topics">
                    
                    <li>
                        
                        <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/intranets/all/index.php">Intranets</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/user-testing/all/index.php">User Testing</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                        
                    </li>
                    
                    <li><a href="#" id="article-see-all-topics"><strong>See all topics&hellip;</strong></a></li>
                </ul>
                
              </section>
              <section id="article-all-topics" style="display: none;">
                
                <h1>
                  <span class="show-for-medium-up">All Article Topics</span>
                  <span class="show-for-small-down">All Topics</span>
                  (<a href="#" id="article-hide-all-topics">hide</a>)
                </h1>
                <ul>
                  
                  <li>
                    
                    <a href="../../topic/accessibility/all/index.php">Accessibility</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/agile/all/index.php">Agile</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/analytics-and-metrics/all/index.php">Analytics &amp; Metrics</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/applications/all/index.php">Application Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/b2b-websites/all/index.php">B2B Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/behavior-patterns/all/index.php">Behavior Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/branding/all/index.php">Branding</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/content-strategy/all/index.php">Content Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/corporate-websites/all/index.php">Corporate Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/design-patterns/all/index.php">Design Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-design-process/all/index.php">Design Process</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/email/all/index.php">Email</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/eyetracking/all/index.php">Eyetracking</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/heuristic-evaluation/all/index.php">Heuristic Evaluation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/human-computer-interaction/all/index.php">Human Computer Interaction</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-ideation/all/index.php">Ideation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/information-architecture/all/index.php">Information Architecture</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/interaction-design/all/index.php">Interaction Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/international-users/all/index.php">International Users</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/intranets/all/index.php">Intranets</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-management/all/index.php">Management</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/navigation/all/index.php">Navigation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/non-profit-websites/all/index.php">Non-Profit Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/personas/all/index.php">Personas</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/persuasive-design/all/index.php">Persuasive Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/prototyping/all/index.php">Prototyping</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/psychology-and-ux/all/index.php">Psychology and UX</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/research-methods/all/index.php">Research Methods</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/search/all/index.php">Search</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/social-media/all/index.php">Social Media</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/strategy/all/index.php">Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/user-testing/all/index.php">User Testing</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-teams/all/index.php">UX Teams</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/visual-design/all/index.php">Visual Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/writing-web/all/index.php">Writing for the Web</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/young-users/all/index.php">Young Users</a>
                    
                  </li>
                  
                </ul>
                
              </section>
            </div>
            <script>
              /* Determine which topics list will be displayed: the full or the
               * partial.  We are inlining this close to the list itself to
               * prevent rendering delays on the client
               */
              var topic_list = $.cookie("article-topics");

              var popular_topics = $('#article-popular-topics');
              var all_topics = $('#article-all-topics');

              if(topic_list == 1) {
                  popular_topics.hide();
                  all_topics.show();
              } else {
                  popular_topics.show();
                  all_topics.hide();
              }
            </script>
            

            
            <div class="small-6 medium-12 columns">
              
              <section id="article-initial-authors">
                  <h1>Author</h1>
                  <ul>
                    
                    <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                    
                    <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                    
                    <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                    
                    <li><a href="#" onClick="$('#article-initial-authors').hide(); $('#article-all-authors').show();"><strong>See all authors&hellip;</strong></a></li>
                  </ul>
              </section>
              <section id="article-all-authors" style="display: none;">
                <h1>
                  All Authors
                  (<a href="#" id="article-hide-all-authors">hide</a>)
                </h1>
                <ul>
                  
                  <li><a href="../author/raluca-budiu/index.php">Raluca Budiu</a></li>
                  
                  <li><a href="../author/patty-caya/index.php">Patty Caya</a></li>
                  
                  <li><a href="../author/susan-farrell/index.php">Susan Farrell</a></li>
                  
                  <li><a href="../author/therese-fessenden/index.php">Therese Fessenden</a></li>
                  
                  <li><a href="../author/kim-flaherty/index.php">Kim Flaherty</a></li>
                  
                  <li><a href="../author/sarah-gibbons/index.php">Sarah Gibbons</a></li>
                  
                  <li><a href="../author/aurora-harley/index.php">Aurora Harley</a></li>
                  
                  <li><a href="../author/kate-kaplan/index.php">Kate Kaplan</a></li>
                  
                  <li><a href="../author/page-laubheimer/index.php">Page Laubheimer</a></li>
                  
                  <li><a href="../author/angela-li/index.php">Angie Li</a></li>
                  
                  <li><a href="../author/hoa-loranger/index.php">Hoa Loranger</a></li>
                  
                  <li><a href="../author/kate-meyer/index.php">Kate Meyer</a></li>
                  
                  <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                  
                  <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                  
                  <li><a href="../author/kara-pernice/index.php">Kara Pernice</a></li>
                  
                  <li><a href="../author/christian-rohrer/index.php">Christian Rohrer</a></li>
                  
                  <li><a href="../author/amy-schade/index.php">Amy Schade</a></li>
                  
                  <li><a href="../author/katie-sherwin/index.php">Katie Sherwin</a></li>
                  
                  <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                  
                  <li><a href="../author/kathryn-whitenton/index.php">Kathryn Whitenton</a></li>
                  
                </ul>
              </section>
              
            </div>
            
          </div>

          
          <section class="hide-for-small-down">
            
              
              <h1>Recent Articles</h1>
              
              <ul>
                  
                  <li><a href="../seamless-cross-channel/index.php">Seamlessness in the Omnichannel User Experience</a></li>
                  
                  <li><a href="../brand-intention-interpretation/index.php">Experience Design: Bridging Brand Intention and Brand Interpretation</a></li>
                  
                  <li><a href="../eyetracking-tasks-efficient-scanning/index.php">Scanning Patterns on the Web Are Optimized for the Current Task</a></li>
                  
                  <li><a href="../flat-design-best-practices/index.php">Flat-Design Best Practices</a></li>
                  
                  <li><a href="../ux-user-stories/index.php">Accounting for UX Work with User Stories in Agile Projects</a></li>
                  
                  <li><a href="../index.php"><strong>See all articles&hellip;</strong></a></li>
              </ul>
              
            
          </section>
          

          
          
          <section class="hide-for-small-down">
            <h1>Popular Articles</h1>
            
            <ul>
                
                <li><a href="../usability-101-introduction-to-usability/index.php">Usability 101: Introduction to Usability</a></li>
                
                <li><a href="../top-10-mistakes-web-design/index.php">Top 10 Mistakes in Web Design</a></li>
                
                <li><a href="../how-users-read-on-the-web/index.php">How Users Read on the Web</a></li>
                
                <li><a href="../f-shaped-pattern-reading-web-content/index.php">F-Shaped Pattern For Reading Web Content</a></li>
                
                <li><a href="../ten-usability-heuristics/index.php">10 Usability Heuristics for User Interface Design</a></li>
                
                <li><a href="../intranet-design-2016/index.php">10 Best Intranets of 2016</a></li>
                
                <li><a href="../which-ux-research-methods/index.php">When to Use Which User-Experience Research Methods</a></li>
                
                <li><a href="../response-times-3-important-limits/index.php">Response Times: The 3 Important Limits</a></li>
                
                <li><a href="../why-you-only-need-to-test-with-5-users/index.php">Why You Only Need to Test with 5 Users</a></li>
                
                <li><a href="../page-fold-manifesto/index.php">The Fold Manifesto: Why the Page Fold Still Matters</a></li>
                
            </ul>
          </section>
          
          

          
            <section class="hide-for-small-down">
              
            </section>
          

      </section>
      

      
        <div class="small-12 medium-9 columns content-body">
          




<article class="l-subsection alertbox-article-content article-detail-bl">
  
  <header>
    <div class="l-subsection">
      <h1>3D Touch on iPhone 6S: Embrace the Force</h1>
      <div class="author-meta">by
        
          
            
            <a href="../author/raluca-budiu/index.php">Raluca Budiu</a>
            
          
        on  November 1, 2015
        
      </div>

      

      
      <div class="topic-links">
        Topics:
        
          <ul>

  <li><a href="../../topic/mobile-and-tablet-design/index.php">Mobile &amp; Tablet</a></li>

</ul>
        
      </div>
      
    </div>
  </header>
  

  
  
  <section>
    <p class="lede"><strong>Summary:</strong> The new iOS gesture poses some physical challenges for users. Designers should take advantage of it to enhance the user experience by making pages previewable and supporting quick access to frequently used features.</p>
    <br />
  </section>
  
  

  
  <section id="article-body"><p>With the new iPhone 6S, Apple introduced a new type of touch gesture: the 3D Touch. Unlike the other gestures, which usually are detected by taking into account the amount of time the finger is on the screen and the trajectory of that finger, the 3D Touch takes into account the amount of force being deployed. (The name “3D” is probably an analogy with pressing “deep” into the screen as opposed to just touching the surface, though the user is actually still pressing down on a rigid unmoving piece of glass. Thus, the press is not literally three-dimensional.)</p>

<p><a href="../smartwatch/index.php">Apple Watch</a> users may be familiar with 3D Touch’s predecessor: the Force Touch. Unlike Force Touch, which is associated with the entire screen, 3D Touch is connected with a particular UI element. In other words, on the watch it doesn’t matter where you press: if you press with the right amount of force anywhere on the screen you will get the Force Touch menu for that screen (if such a menu exists). On the phone, you will get different results depending on <em>where </em>you press. Thus, the phone operation is similar to right-clicking the mouse in Windows (which gives you a context-sensitive result), whereas the watch operation is similar to pressing the <em>Windows</em> key on a PC keyboard (which produces the same menu no matter where your mouse is pointing).</p>

<figure class="caption"><img alt="Activity Monitor screenshot and menu showing two controls" border="0" height="245" src="https://media.nngroup.com/media/editor/2015/10/29/fig1-watch.png" width="400"/>
<figcaption><em>Activity Monitor for Apple Watch: Applying Force Touch anywhere on the screen shown on the left triggers the menu displayed in the right screenshot.</em></figcaption>
</figure>

<figure class="caption"><img alt="Quick actions menus on Mail and Safari" border="0" height="527" src="https://media.nngroup.com/media/editor/2015/10/29/fig2-quickactions.png" width="600"/>
<figcaption><em>Unlike Force Touch, the 3D Touch depends on where the screen is touched. On the iPhone 6S’s homescreen, the 3D Touch applied to the Mail app triggers the list of quick actions associated with this app (left). In the right screenshot you can see the quick actions for Safari.</em></figcaption>
</figure>

<p>3D Touch can produce two kinds of views: a <strong>quick-actions</strong> view, like in the example above or in the Maps example below, or a <strong>peek-and-pop</strong> mode, in which users see a preview of an item such as an email message or a link on a website.</p>

<figure class="caption"><img alt="Map showing Kaiser Permanente location in Santa Clara and menu displaying quick actions " border="0" height="520" src="https://media.nngroup.com/media/editor/2015/10/29/fig3-maps.png" width="600"/>
<figcaption><em>Quick actions: In Maps, 3D Touch on a spot on the map shows a list of quick actions associated with that location.</em></figcaption>
</figure>

<figure class="caption"><img alt="NNgroup.com search result page: list of articles and preview of one article" height="522" src="https://media.nngroup.com/media/editor/2015/10/29/peek.png" width="600"/>
<figcaption><em>Peek and pop: In Safari 3D Touch on a link shows a preview of that webpage.</em></figcaption>
</figure>

<p>We often question <a href="../overloaded-vs-generic-commands/index.php">overloaded commands</a>: the idea of having the same operation produce two different results, depending on the context. However, in this case, the two different outcomes of 3D Touch are unlikely to cause usability problems. For most users, there’s not much difference between the two outcomes: both simply show more details related to the target they’ve pressed. Interaction designers will appreciate the difference between the two view options, but users don’t really need to know or care about this distinction. (As long as pressing the same UI element doesn’t produce different views depending on some hidden mode or state that users would usually not remember. That would be confusing.)</p>

<p>However, as discussed further below, one confusing aspect of having two different outcomes for 3D Touch is that the interaction with each of these views is quite different.</p>

<h2>The Appeal of Gestures on a Small Screen</h2>

<p>Gestures have always fascinated small-touchscreen designers, for good reason: they represent a unique opportunity of loading more functionality into an app, without taking up any visible real estate. They are an elegant solution to the <a href="../content-chrome-ratio/index.php">tension</a> between content and <a href="../browser-and-gui-chrome/index.php">chrome</a> that makes <a href="../mobile-ux/index.php">designing for mobile devices</a> such a challenge.</p>

<p>However, here we are, 8 years since touchphones became mainstream, and gestures are still far from being widely used by apps. Yes, certain <a href="../generic-commands/index.php">generic-command</a> gestures such as the horizontal swipe have become more standard, yet most of the gestures available on either iOS or Android are still only modestly used in apps. And for good reason.</p>

<p>First and foremost, gestures have <strong>no natural </strong><a href="http://jnd.org/dn.mss/signifiers_not_affordances.php"><strong>signifiers</strong></a>: most of the time, people have no idea what gestures they are supposed to use with an interface. Designers have to work hard to create signifiers for the gestures that they employ. Second, gestures are not easily <strong>memorable</strong>. Especially when there are many gestures to be learned, people often get confused and forget which gesture they are supposed to use for which action. And last but not least, gestures can sometimes be <strong>hard to produce</strong> reliably. This last difficulty is especially a problem when two very similar gestures (such as flick and swipe, or swipe and long swipe) are assigned to two different actions, and is one of the main issues with the 3D Touch, as we discuss below.</p>

<h2>Long Press vs 3D Touch</h2>

<p>In theory, the difference between long press and 3D Touch is quite clear: the first requires the finger to touch the screen for a <strong>longer period </strong>of time, while the second requires the finger to press <strong>harder</strong>. Moreover, the 3D Touch has haptic feedback: when the user uses the correct gesture, the phone vibrates shortly in recognition. In practice, however, it’s quite hard to distinguish between the two gestures and to perform each reliably. Even after using my new iPhone for a month, half the time my 3D Touch gestures end up being interpreted as long presses.</p>

<p>There are a few consequences of this similarity:</p>

<ol>
	<li>Whenever the long press is available, the action triggered by it is inadvertently taken instead of that corresponding to the 3D Touch. For instance, on the homescreen, instead of getting the quick actions associated with the Messages app, I may find myself in the homescreen-edit mode and accidentally delete apps.</li>
</ol>

<figure class="caption"><img alt="iPhone Homescreen" border="0" height="498" src="https://media.nngroup.com/media/editor/2015/10/29/delete-screen.png" width="280"/>
<figcaption><em>An incorrectly performed 3D Touch on the homescreen is mistaken for a long press and displays the homescreen-edit mode.</em></figcaption>
</figure>

<ol>
	<li value="2">Users are unsure if the gesture was performed correctly. 3D Touch provides feedback <em>only</em> if the gesture has been associated with that design element that you’re pressing. If you happen to try 3D Touch in an app that does not use the gesture, you won’t get any feedback, and thus you won’t know if you didn’t actually perform the gesture correctly or if, in fact, the interface does not support 3D Touch. (Or, if the phone is slow for some reason, which is one of the ways in which engineering flaws degrade the user experience.) You may try again repeatedly, hoping that the app will respond, or you may give up.<br/>
	If most apps will not in fact support 3D Touch, the lack of consistent results may make users rely less and less on the gesture. This is what happened with the <em>Menu </em>and <em>Search </em>physical buttons on the initial Android phones: because apps used them inconsistently, users ended up ignoring them most of the time. Google’s official Nexus phones no longer have these physical buttons, and although Samsung phones still carry the physical <em>Menu</em> button, most Android apps do not use that button for essential functionality (and justly so).</li>
</ol>

<h2>The Mechanics of 3D Touch: Maintaining Screen Contact</h2>

<p>One of the uses of 3D Touch is the so-called <strong>peek and pop</strong>, that enables users to take a preview a content element such as an email in their inbox or a link on a website. For instance, when users perform a 3D Touch on an email message they will see a lightbox with the beginning of the message. To keep the preview visible, users must continue touching the screen, although this contact is just a regular touch — it does not need the same amount of force as when the peek was initiated. Should they lift the finger from the screen, the lightbox will disappear. Should they slide the finger up or laterally, more actions will become available. Should they do another 3D Touch in the preview mode, the message will <strong>pop </strong>up full screen.</p>

<p>Maintaining continuous contact with the screen during the peek is problematic for three reasons:</p>

<ol>
	<li>The finger blocks the content. The point of the preview is to actually <em>see the content</em>, yet, because the finger must stay on the screen, it blocks a good part of what’s actually displayed.</li>
</ol>

<figure class="caption"><img alt="Phone held in hand in preview mode" border="0" height="472" src="https://media.nngroup.com/media/editor/2015/10/29/finger2.jpg" width="350"/>
<figcaption><em>Mail for iPhone: When peeking at an email message, a significant portion of the screen is obscured by the finger that must maintain the touch.</em></figcaption>
</figure>

<ol>
	<li value="2">Maintaining the touch on the screen is somewhat strenuous. If you add to that the fact that users are supposed to do other gestures (such as slide the finger horizontally or vertically, or even press harder) to further act upon the item, while still sustaining contact with the screen, you can see how it’s easy to make an error and accidentally dismiss the item while trying to perform one of these actions.</li>
</ol>

<figure class="caption"><img alt="Preview of email message slid to the right or left" border="0" height="522" src="https://media.nngroup.com/media/editor/2015/10/29/email-left-right.png" width="600"/>
<figcaption><em>Sliding a peek to the right (left screenshot) or left (right screenshot) will expose “controls” that allow users to mark the item as read, or to delete it, respectively. It can be tiring to perform these actions without lifting the finger off the screen. Note also that, because </em>Read<em> and </em>Trash<em> are styled like buttons, some users may think that they should press them. In fact, these are not real “controls” — they are just tips that inform users of the action that will be taken if they either continue the swipe or lift the finger off the screen. (This is an interesting and playful example where a false signifier —the button styling — actually encourages the desired effect: as soon as the user lifts the finger to reach for the target, the action is automatically completed and the preview disappears.)</em></figcaption>
</figure>

<p><span style="line-height: 1.6;">If users do slide up the peek, they can finally lift their finger and read the list of actions available for that item.</span></p>

<figure class="caption"><img alt="Peek mode after sliding up" height="498" src="https://media.nngroup.com/media/editor/2015/10/29/mail-actions.png" width="280"/>
<figcaption><em>Sliding up in the peek mode presents users with a set of actions that can be performed. Once users have reached this view, they can lift up the finger from the screen without losing the preview mode.</em></figcaption>
</figure>

<ol>
	<li value="3">Last but not least, having to maintain screen contact in peek mode is inconsistent with how 3D Touch is implemented for quick actions. In quick-actions mode, users can lift the finger off the screen without losing the menu generated by the 3D Touch. However, in peek and pop, the contact must be sustained to avoid dismissing the preview.</li>
</ol>

<p>While these two different types of behavior may seem logical if you know of the two different modes available with 3D Touch (quick actions and peek and pop), for users this distinction is not evident, because both are generated by the same gesture, even though they look different. There is no reason for one to require a different interaction than the other.</p>

<h2>Enhancement, Not Requirement</h2>

<p>Although the actual implementation of the 3D Touch is somewhat problematic, the approach taken to the functionality assigned to this feature is the correct one: 3D Touch should be an <strong>enhancement</strong> to the user experience, <strong>not a requirement</strong> to achieving a user task. Indeed, so far, all the functionality provided by 3D Touch, whether in quick actions or peek-and-pop mode, is redundant: users who don’t have the latest iPhone or have trouble with the 3D Touch can still do their tasks without using it and achieve the same kinds of actions, albeit in a more roundabout way. This redundancy is the right solution to the problems that gestures pose: lack of affordance and memorability, as well as difficulty in performing them.</p>

<h2>Is 3D Touch Worth the Effort?</h2>

<p>Is 3D Touch here to stay or is it just Apple’s marketing gimmick to encourage users to upgrade to a new phone? It’s too early to have an answer to that. First, with a little over a month since the iPhone 6S came out, we don’t still understand very well how fast people will adapt to its idiosyncrasies. Will its perceived benefits be worth the hassle of not getting it right? Second, in further iOS updates, Apple may easily improve one of the problems with 3D Touch — namely, maintaining screen contact in peek-and-pop view. Third, and very important, part of the answer lies in how the app designers are going to respond to it: are they going to enthusiastically adopt it or ignore it?</p>

<p>Is this a feature worth having? Yes, as an enhancement. There is a lot of potential for improving the user experience and supporting behaviors that mobile and desktop users are engaging in already. Two of them come to mind: <strong>microsessions</strong> and <strong>avoiding pogo sticking</strong>.</p>

<p>Microsessions are phone sessions that are 15 seconds or shorter. Recent research by Denzil Ferreira and colleagues shows that 40% of app launches are microsessions, namely short interactions in which users are able to quickly satisfy their goals. A common microsession activity is checking for updates in an app (such as Email or Facebook); the quick actions offer an opportunity for rapid access to such frequent tasks or content. Peek-and-pop views should also make many microsessions more efficient for users.</p>

<p><a href="../pogo-sticking/index.php">Pogo sticking</a> refers to alternating between inspecting a collection of items (such as a list of products) and looking at each item individually (a product in the list). It is usually an inefficient behavior because it makes users jump back and forth between pages, losing not only time for loading the page but also the time needed for recovering context. Our recent research with Millennials shows that pogo sticking is so annoying that, on desktop, users have developed a special behavior called <a href="../multi-tab-page-parking/index.php">page parking</a> to avoid it. On mobile phones, page parking is a lot more difficult. But the peek-and-pop view can actually prevent pogo sticking: it can helps users quickly inspect items that have the potential of being interesting, without navigating all the way to those items and thus without the need to issue an explicit <em>Back</em> command. Wisely selected actions in the peek-and-pop view may actually allow users to collect items of interest for further inspection.</p>

<h2>What Should Designers Do?</h2>

<p><strong>App designers: Embrace the force!</strong></p>

<p>The silly Star Wars word play notwithstanding, the only chance that we have to actually make 3D Touch a success is to embrace it in apps and make it available for users. If people try the gesture and get rewarded with a result most of the time, chances are that it will be eventually learned in spite of lacking signifiers and the difficulties associated with the mechanics. But if only the occasional app takes advantage of the 3D Touch, it may have the same fate as the physical <em>Menu </em>and <em>Search </em>buttons on Android: too many disappointments, and people will give up trying. Luckily, we’re still at the beginning: the vast majority of iPhone users do not own a 3D Touch device yet. But hurry up, so that, when they do, people will be able to learn to use the gesture easily.</p>

<p><strong>Web designers: Is your page previewable?</strong></p>

<p>Think of the preview as a <a href="https://en.wikipedia.org/wiki/Speed_dating">speed date</a>: you have a few seconds to make a good impression under conditions that may not be optimal. Make sure that your users are compelled by what they see in the peek and pop and ready to go on to the full-page view.</p>

<ul>
	<li>The preview mode (as well as iOS 9’s split view and slide-over view for the iPad) means that you cannot guarantee how users are going to see your site: some may see it full screen and may be able to easily scroll down, while others may look at the front page only in a section of the screen. So, <strong>whatever you put at the top of the page should be compelling enough</strong> to pass the preview test and make users click over. That’s another reason why the <a href="../page-fold-manifesto/index.php">page fold matters</a> (if you weren’t already convinced).</li>
	<li>Previewed pages are squeezed into the smaller preview window, so as a result, the text will appear about 7% smaller than in a full-size window, plus users won’t be able to zoom in to increase the font size. Tiny fonts will get tinier, and the previewed page may become illegible, so <strong>start with a font that’s big enough to stand a decrease without impacting legibility.</strong></li>
</ul>

<figure class="caption"><img alt="Page with text " border="0" height="523" src="https://media.nngroup.com/media/editor/2015/10/29/font-size.png" width="600"/>
<figcaption><em>The preview mode shows the same site full screen (left) and in preview mode (right). The site text is small to start with, but in the preview mode is even smaller to the point of becoming illegible.</em></figcaption>
</figure>

<ul>
	<li><strong>Stay away from Interstitials</strong>: They prevent users from seeing a preview of the actual content. There are many reasons for not using interstitials that ask users to choose whether they want to see the app or the webpage, but previews are another one: if people see irrelevant info as the preview, they’ll assume that your site is irrelevant.</li>
</ul>

<figure class="caption"><img alt="Yelp screen asking whether to open app or site" border="0" height="498" src="https://media.nngroup.com/media/editor/2015/10/29/yelp-interstitial.png" width="280"/>
<figcaption><em>This Yelp interstitial prevents users from seeing the content of the page and is useless in preview mode.</em></figcaption>
</figure>

<ul>
	<li><strong>Don’t ask for permissions before loading</strong> the page. If the site requires permission to use current location before loading the page, you’re in trouble: users in peek-and-pop mode cannot give you permission without leaving the preview mode, nor can they see what your site has in store for them. They may simply not bother and skip your site in favor of a different one.</li>
</ul>

<figure class="caption"><img alt="Alert asking for current location" border="0" height="498" src="https://media.nngroup.com/media/editor/2015/10/29/usatod.png" width="280"/>
<figcaption><em>When users try to preview the USA Today link in the Bing search results, the preview window briefly appears only to be replaced by an alert asking users for permission to use the current location.</em></figcaption>
</figure>

<h2>Conclusion</h2>

<p>The 3D Touch is a new gesture that presents some challenges for users due to its similarity to the long press. Designers should use this gesture as an opportunity to enhance the user experience of power users, by providing quick access to frequently accessed pages and to by making their pages previewable.</p>

<h2>Reference</h2>

<p>D. Ferreira, J. Goncalves, V. Kostakos, L. Barkhuus, A. K. Dey. 2014. Contextual experience sampling of mobile application micro-usage. In MobileHCI '14. <a href="http://doi.acm.org/10.1145/2628363.2628367">http</a><a href="http://doi.acm.org/10.1145/2628363.2628367">://doi.acm.org/10.1145/2628363.2628367</a></p>
</section>
  

  
  <section>
    
        
    
  </section>
  
  
  
  
  

  


  <section>
    <br />
    <p class="share"><b>Share <span class="hide-for-small-down">this article</span>:</b> <a href="https://twitter.com/intent/tweet?url=http://www.nngroup.com/articles/3d-touch/&amp;text=3D%20Touch%20on%20iPhone%206S:%20Embrace%20the%20Force&amp;via=nngroup">Twitter</a> | <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=http://www.nngroup.com/articles/3d-touch/&amp;title=3D%20Touch%20on%20iPhone%206S:%20Embrace%20the%20Force&amp;source=Nielsen%20Norman%20Group">LinkedIn</a> | <a href="https://plus.google.com/share?url=http://www.nngroup.com/articles/3d-touch/">Google+</a> | <a href="mailto:?subject=NN/g Article: 3D Touch on iPhone 6S: Embrace the Force&amp;body=http://www.nngroup.com/articles/3d-touch/">Email</a></p>
    <hr />
  </section>

  <footer class="l-subsection">
    
    
    <div class="learn-more">
      <h2>Learn More</h2>
      <p><a href="../subscribe/index.php">Subscribe to the weekly newsletter</a> to get notified about future articles.</p>
      </div>
    
    

    <div class="row show-for-small-down related-content">
      
      
      

      
      
        <div class="column small-12 topic-links">All articles about:
          <ul>
            
            <li><a href="../../topic/gestures/index.php">gestures</a></li>
            
            <li><a href="../../topic/ios/index.php">ios</a></li>
            
            <li><a href="../../topic/mobile-and-tablet-design/index.php">Mobile &amp; Tablet</a></li>
            
          </ul>
        </div>
      
      

      
      
        <div class="column small-12">
          <h3>UX Conference Training Courses</h3>
           <ul class="no-bullet">
    
        <li><a href="../../courses/cross-channel-user-experience/index.php">Omnichannel Journeys and Customer Experience</a></li>
    
        <li><a href="../../courses/usability-mobile-websites-apps/index.php">Mobile User Experience</a></li>
    
        <li><a href="../../courses/scaling-responsive-design/index.php">Scaling User Interfaces</a></li>
    
        <li>Visual Design for Mobile and Tablet: <a href="../../courses/vis-mob-1/index.php">Day 1</a> and <a href="../../courses/vis-mob-2/index.php">Day 2</a></li>
    
</ul>
        </div>
      
      

      
      
        <div class="small-12 column">
          <h3>Research Reports</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../reports/tablets/index.php">Tablet Website and Application UX</a></li>
              
            
              
                <li><a href="../../reports/mobile-website-and-application-usability/index.php">User Experience for Mobile Applications and Websites</a></li>
              
            
              
                <li><a href="../../reports/enterprise-mobile-showcase/index.php">Mobile Intranets and Enterprise Apps</a></li>
              
            
              
                <li><a href="../../reports/wap-usability/index.php">WAP Usability</a></li>
              
            
              
                <li><a href="../../reports/ipad-app-and-website-usability/index.php">iPad App and Website Usability</a></li>
              
            
          </ul>
        </div>
      
      

      
      
        <div class="small-12 column">
          <h3>Online Seminars</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../online-seminars/every-word-count/index.php">Making Every Word Count</a></li>
              
            
              
                <li><a href="../../online-seminars/mobile-user-testing/index.php">Mobile User Testing</a></li>
              
            
          </ul>
        </div>
      
      
    </div>

    <div class="hide-for-small-down row related-content">
      <div class="medium-6 columns">
        
        
          <div>
            <h3>Articles</h3>
            <ul class="no-bullet">
              
                
                <li><a href="../smartwatch/index.php">The Apple Watch: User-Experience Appraisal</a></li>
                
              
                
                <li><a href="../ios-9-back-to-app-button/index.php">iOS 9 App Switching and the Back-to-App Button</a></li>
                
              
                
                <li><a href="../4-ios-rules-break/index.php">4 iOS Rules to Break </a></li>
                
              
                
                <li><a href="../wechat-qr-shake/index.php">Scan and Shake: A Lesson in Technology Adoption from China’s WeChat</a></li>
                
              
                
                <li><a href="../wechat-integrated-ux/index.php">WeChat: China’s Integrated Internet User Experience</a></li>
                
              
            </ul>
          </div>
        
        

        
          
          <div>
            <h3>Research Reports</h3>
            <ul class="no-bullet">
              
                
                  <li><a href="../../reports/tablets/index.php">Tablet Website and Application UX</a></li>
                
              
                
                  <li><a href="../../reports/mobile-website-and-application-usability/index.php">User Experience for Mobile Applications and Websites</a></li>
                
              
                
                  <li><a href="../../reports/enterprise-mobile-showcase/index.php">Mobile Intranets and Enterprise Apps</a></li>
                
              
                
                  <li><a href="../../reports/wap-usability/index.php">WAP Usability</a></li>
                
              
                
                  <li><a href="../../reports/ipad-app-and-website-usability/index.php">iPad App and Website Usability</a></li>
                
              
            </ul>
          </div>
          
        

      </div>
      <div class="medium-6 columns">
        
          
          <div>
            <h3>UX Conference Training Courses</h3>
             <ul class="no-bullet">
    
        <li><a href="../../courses/cross-channel-user-experience/index.php">Omnichannel Journeys and Customer Experience</a></li>
    
        <li><a href="../../courses/usability-mobile-websites-apps/index.php">Mobile User Experience</a></li>
    
        <li><a href="../../courses/scaling-responsive-design/index.php">Scaling User Interfaces</a></li>
    
        <li>Visual Design for Mobile and Tablet: <a href="../../courses/vis-mob-1/index.php">Day 1</a> and <a href="../../courses/vis-mob-2/index.php">Day 2</a></li>
    
</ul>
          </div>
          
        
        
          
          <div>
            <h3>Online Seminars</h3>
            <ul class="no-bullet">
            
                <li><a href="../../online-seminars/every-word-count/index.php">Making Every Word Count</a></li>
            
                <li><a href="../../online-seminars/mobile-user-testing/index.php">Mobile User Testing</a></li>
            
            </ul>
          </div>
          
        
      </div>
    </div>
  </footer>
</article>

        </div>

      

    
  </div>
  <div class="row">
    
      <div class="column small-12 show-for-small-down l-navrail announcements">
        
          
        
      </div>
    
    
  </div>

    </div>
    




<footer id="page-footer">
    <div class="row hide-for-print">
        <div class="column small-12 medium-6">
            <section class="footer-popular">
                
                <h1>Popular Topics</h1>
                <div style="float: left;  width: 15em; display: block;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/applications/index.php">Application Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/e-commerce/index.php">E-Commerce Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/eyetracking/index.php">Eyetracking </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/information-architecture/index.php">Information Architecture </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/intranets/index.php">Intranet Design </a></li>
	<li><a href="../../topic/interaction-design/index.php">Interaction Design </a></li>
</ul>
</div>

<div style="float: left;  width: 15em; display: block; list-style: none;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/mobile-and-tablet-design/index.php">Mobile and Tablet Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/credibility-persuasion/index.php">Persuasive Design</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/research-methods/index.php">Research Methods</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/user-testing/index.php">User Testing </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/web-usability/index.php">Web Usability </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/writing-web/index.php">Web Writing &amp; Content Strategy </a></li>
</ul>
</div>

<p>&nbsp;</p>

                
            </section>
        </div>

        <div class="column small-12 medium-6">
            <div class="row">
                <div class="column small-6">
                    <section class="footer-about">
                        <h1>Follow Us</h1>
                        <ul>
	<li><a href="../subscribe/index.php">Subscribe to our weekly UX newsletter</a>,&nbsp;Jakob Nielsen&#39;s Alertbox</li>
	<li>Visit Don Norman&#39;s website, <a href="http://www.jnd.org/"> www.jnd.org </a></li>
	<li>Visit Bruce Tognazzini&#39;s website, <a href="http://www.asktog.com/"> www.asktog.com </a></li>
	<li style="padding-top: 6px;"><a class="twitter-follow-button" data-show-count="false" data-size="large" href="https://twitter.com/nngroup"><img alt="Follow @nngroup on Twitter" height="20" src="https://media.nngroup.com/media/editor/2016/12/30/twitter-follow-button.png" style="width: 117px; height: 20px;" width="117" /></a></li>
	<li style="padding-top: 6px;"><img alt="" height="14" src="https://media.nngroup.com/media/editor/2016/07/26/feed-icon-14x14.png" style="vertical-align:text-top;width:14px;margin-right:.25em;" width="14" /><a href="../../feed/rss/index.php">Subscribe to our RSS feed</a></li>
</ul>

                    </section>
                </div>

                <div class="column small-6">
                    <section class="footer-about right-column">
                        <h1>About
</h1>
                        <ul class="no-bullet">
	<li><a href="../../about/contact/index.php">Contact information</a></li>
	<li><a href="../../about/index.php">About Us</a></li>
	<li><a href="../../about/why-nng/index.php">Why NN/g? </a></li>
	<li><a href="../../return-policy/index.php">Return Policy</a></li>
</ul>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="copyright column small-12">
            <p><a href="../../copyright-and-reprint-info/index.php">Copyright</a> &copy; 1998-2017 Nielsen Norman Group, All Rights Reserved.</p>
        </div>
    </div>
</footer>


    


        <script src="https://media.nngroup.com/static/vendor/fastclick/js/fastclick.min.js"></script>
        <script src="https://media.nngroup.com/static/vendor/foundation/js/foundation.min.js"></script>
        <script src="http://media.nngroup.com/static/js/app.js"></script>
        <script src="http://media.nngroup.com/static/js/scroll-to-fixed.js"></script><script>var externalLinks = document.querySelectorAll('a[href^="http"]');for (var i = externalLinks.length-1; i >= 0; i--) {externalLinks[i].addEventListener("click", function(event) { alert("External links are disabled for this test."); event.preventDefault(); }, false);}var mailLinks = document.querySelectorAll('a[href^="mailto"]');for (var i = mailLinks.length-1; i >= 0; i--) {mailLinks[i].addEventListener("click", function(event) { alert("Email links are disabled for this test."); event.preventDefault(); }, false);}var externalForms = document.querySelectorAll('form');for (var i = externalForms.length-1; i >= 0; i--) {externalForms[i].addEventListener("submit", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false); externalForms[i].addEventListener("click", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false);}</script>

        <script>
            
            $(function() {
                $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
                $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
            });
        </script>

        
<script src="https://media.nngroup.com/static/js/articles.js"></script>


        
        <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1072699960;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072699960/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
    </body>

<!-- Mirrored from www.nngroup.com/articles/3d-touch/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:08:07 GMT -->
</html>
