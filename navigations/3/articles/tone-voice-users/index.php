<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->





    
<!-- Mirrored from www.nngroup.com/articles/tone-voice-users/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:04:15 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1026.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"licenseKey":"1a30ef9931","errorBeacon":"bam.nr-data.net","applicationID":"2048992","transactionName":"NFxSMRBUX0EDUkIPDA0WdhAMVkVbDV8ZBxEXUFMJB0YfRAtUQRVZIktEDAFZVHYHRVcPD01eVRE=","beacon":"bam.nr-data.net","queueTime":4,"applicationTime":504,"agent":""}</script>
        <title>The Impact of Tone of Voice on Users&#39; Brand Perception (Nielsen Norman Group UX research)</title><meta property="og:title" content="The Impact of Tone of Voice on Users&#39; Brand Perception (Nielsen Norman Group UX research)" />
  
        
        <meta name="description" content="Tone of voice in web content impacts user perceptions of brand friendliness, trustworthiness, and desirability. Casual, conversational, and enthusiastic tones performed best.">
        <meta property="og:description" content="Tone of voice in web content impacts user perceptions of brand friendliness, trustworthiness, and desirability. Casual, conversational, and enthusiastic tones performed best." />
        
  
        
	
        
        <meta name="keywords" content="tone of voice, tone words, content, users, content strategy, user experience">
        

        

        <meta name="robots" content="noarchive">
        <meta name="viewport" content="width=device-width">

        <link rel="search" type="application/opensearchdescription+xml" title="NN/g Search" href="../../opensearch.xml" />
        <link rel="shortcut icon" href="http://media.nngroup.com/static/img/favicon.ico" />

        <link rel="stylesheet" href="https://media.nngroup.com/static/vendor/font-awesome/css/font-awesome.min.css">

        
        <link rel="stylesheet" href="https://media.nngroup.com/static/css/7232a960f1df.css" type="text/css" />
        <link rel="stylesheet" href="http://fast.fonts.com/t/1.css?apiType=css&amp;projectid=c5846260-9def-46ec-9407-0f1349bd2d64" type="text/css">

        <script src="https://media.nngroup.com/static/vendor/modernizr/js/modernizr.min.js"></script>

        <script type="text/javascript" src="https://media.nngroup.com/static/js/3283be5def74.js"></script>
        

        

        
        
        <!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=229034,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('http://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->


<!-- Google Analytics Tracking Code for http://nngroup.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23132681-1', 'nngroup.com');
  ga('require', 'linkid', 'https://www.nngroup.com/articles/tone-voice-users/linkid.js');
  ga('require', 'displayfeatures');

// Optimizely Universal Analytics Integration
 window.optimizely = window.optimizely || [];
 window.optimizely.push("activateUniversalAnalytics");

  ga('send', 'pageview');

</script>
<script>
/**
* Function to track a click on an outbound link in Google Analytics.
*/
function trackOutboundLink(label, url) {
    if(window.ga && ga.loaded) {
        ga('send', 'event', 'outbound', 'click', label, {'hitCallback': function(){document.location = url;}
        });
    } else {
        document.location = url;
    }
}
/**
* Function to track a click on a link in the article announcement area.
*/
$('#articleAnnouncement a').on('click', function(){
  ga('send', 'event', 'link', 'click', 'articleAnnouncement');
});
</script>

<script type="text/javascript">
 setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2421.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



<!-- Hotjar Tracking Code for http://nngroup.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:121605,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

        <script src="https://media.nngroup.com/static/js/analytics/track_outbound_link.js"></script>
        
        
        
    </head>
    <body class="location-articles articles-detail">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
    <header id="page-header">
        <div id="cart" class="shopping-cart mobile-cart show-for-small-down"></div>
        <div class="row">
            <div id="masthead" class="masthead small-12 medium-7 column">

                <!-- The button to toggles menu on small screen -->

                <h1><a href="../../index.php"><img src="http://media.nngroup.com/static/img/logo-wordmark.png" width="74" height="30"> Nielsen <span class="norman">Norman</span> Group</a></h1>
                <p class="tagline show-for-medium-up">Evidence-Based User Experience Research, Training, and Consulting</p>

                <!-- The button to toggles form search on small screen -->


                <div style="clear: right;width:  100%;height: 1px;"></div><?php define('ROOT_PATH',$_SERVER["DOCUMENT_ROOT"] . '/apps/navigation_styles/navigations/3'); ?><?php include(ROOT_PATH)."/navigation.php"; ?>

                <!-- Drop down menu on small screen -->
                <div id="hideNav" class="show-for-small-down">
                  
                  <ul class="menu-content">
                    <li><a href="../../index.php">Home</a></li>
                    <li class="has-submenu">
                      <a>Training</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../training/index.php">Overview</a></li>
                        
<li id="nav-ux-conference"><a href="../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../online-seminars/index.php"><span>Online Seminars</span></a></li>

                      </ul>
                    </li>
                    <li class="has-submenu">
                      <a>Consulting</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
  <li><a href="../../consulting/index.php">Overview</a></li>
  
  <li><a href="../../consulting/benchmarking/index.php">Benchmarking</a></li>
  
  <li><a href="../../consulting/expert-design-review/index.php">Expert Design Review</a></li>
  
  <li><a href="../../consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
  
  <li><a href="../../consulting/usability-testing/index.php">Usability Testing </a></li>
  
  <li><a href="../../consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
  
  <li><a href="../../in-house-training/index.php">In-House Training</a></li>
</ul>

                    </li>

                    <li><a href="../../reports/index.php">Reports</a></li>
                    <li><a href="../index.php">Articles</a></li>
                    <li class="has-submenu"><a>About NN/G</a><i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../about/index.php">Overview</a></li>
                        <li><a href="../../people/index.php">People</a></li>
                        <li><a href="../../about/why-nng/index.php">Why NN/g?</a></li>
                        <li><a href="../../about/contact/index.php">Contact</a></li>
                        <li><a href="../../news/index.php">News</a></li>
                        <li><a href="../../about/history/index.php">History</a></li>
                        <li><a href="../../books/index.php">Books</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                </div>

                <hr class="header-divider show-for-small-only"/>


            </div>
       </div>

        <div id="hideNav">            <nav class="primary-nav small-12 column">
                <ul>
                    <li><a class="navitem" id="nav-home" href="../../index.php">Home</a></li>
                    <li><a class="navitem" id="nav-training" href="../../training/index.php">Training</a></li>
                    <li><a class="navitem" id="nav-consulting" href="../../consulting/index.php">Consulting</a></li>
                    <li><a class="navitem" id="nav-reports" href="../../reports/index.php">Reports</a></li>
                    <li><a class="navitem" id="nav-articles" href="../index.php">Articles</a></li>
                    <li><a class="navitem" id="nav-about" href="../../about/index.php">About NN/G</a></li>
                </ul>
            </nav>
            <nav class="secondary-nav small-12 medium-12 large-12 columns">
                
                
            </nav>
        </div>
    </header>
    <div id="mainContent" class="l-content">
    
  

  

  <div class="row">
    
      
      <section class="small-12 medium-3 columns collapsable-content l-navrail articles-left-column">
        <h1 class="show-for-small-down collapse-link mobile-browse-header">Browse by Topic and Author</h1>
          <div class="row collapsable">

            
            <div class="small-6 medium-12 columns">
              <section id="article-popular-topics" style="display: none;">
                
                <h1>Topics</h1>
                <ul id="article-popular-topics">
                    
                    <li>
                        
                        <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/intranets/all/index.php">Intranets</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/user-testing/all/index.php">User Testing</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                        
                    </li>
                    
                    <li><a href="#" id="article-see-all-topics"><strong>See all topics&hellip;</strong></a></li>
                </ul>
                
              </section>
              <section id="article-all-topics" style="display: none;">
                
                <h1>
                  <span class="show-for-medium-up">All Article Topics</span>
                  <span class="show-for-small-down">All Topics</span>
                  (<a href="#" id="article-hide-all-topics">hide</a>)
                </h1>
                <ul>
                  
                  <li>
                    
                    <a href="../../topic/accessibility/all/index.php">Accessibility</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/agile/all/index.php">Agile</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/analytics-and-metrics/all/index.php">Analytics &amp; Metrics</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/applications/all/index.php">Application Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/b2b-websites/all/index.php">B2B Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/behavior-patterns/all/index.php">Behavior Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/branding/all/index.php">Branding</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/content-strategy/all/index.php">Content Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/corporate-websites/all/index.php">Corporate Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/design-patterns/all/index.php">Design Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-design-process/all/index.php">Design Process</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/email/all/index.php">Email</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/eyetracking/all/index.php">Eyetracking</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/heuristic-evaluation/all/index.php">Heuristic Evaluation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/human-computer-interaction/all/index.php">Human Computer Interaction</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-ideation/all/index.php">Ideation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/information-architecture/all/index.php">Information Architecture</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/interaction-design/all/index.php">Interaction Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/international-users/all/index.php">International Users</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/intranets/all/index.php">Intranets</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-management/all/index.php">Management</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/navigation/all/index.php">Navigation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/non-profit-websites/all/index.php">Non-Profit Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/personas/all/index.php">Personas</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/persuasive-design/all/index.php">Persuasive Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/prototyping/all/index.php">Prototyping</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/psychology-and-ux/all/index.php">Psychology and UX</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/research-methods/all/index.php">Research Methods</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/search/all/index.php">Search</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/social-media/all/index.php">Social Media</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/strategy/all/index.php">Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/user-testing/all/index.php">User Testing</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-teams/all/index.php">UX Teams</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/visual-design/all/index.php">Visual Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/writing-web/all/index.php">Writing for the Web</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/young-users/all/index.php">Young Users</a>
                    
                  </li>
                  
                </ul>
                
              </section>
            </div>
            <script>
              /* Determine which topics list will be displayed: the full or the
               * partial.  We are inlining this close to the list itself to
               * prevent rendering delays on the client
               */
              var topic_list = $.cookie("article-topics");

              var popular_topics = $('#article-popular-topics');
              var all_topics = $('#article-all-topics');

              if(topic_list == 1) {
                  popular_topics.hide();
                  all_topics.show();
              } else {
                  popular_topics.show();
                  all_topics.hide();
              }
            </script>
            

            
            <div class="small-6 medium-12 columns">
              
              <section id="article-initial-authors">
                  <h1>Author</h1>
                  <ul>
                    
                    <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                    
                    <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                    
                    <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                    
                    <li><a href="#" onClick="$('#article-initial-authors').hide(); $('#article-all-authors').show();"><strong>See all authors&hellip;</strong></a></li>
                  </ul>
              </section>
              <section id="article-all-authors" style="display: none;">
                <h1>
                  All Authors
                  (<a href="#" id="article-hide-all-authors">hide</a>)
                </h1>
                <ul>
                  
                  <li><a href="../author/raluca-budiu/index.php">Raluca Budiu</a></li>
                  
                  <li><a href="../author/patty-caya/index.php">Patty Caya</a></li>
                  
                  <li><a href="../author/susan-farrell/index.php">Susan Farrell</a></li>
                  
                  <li><a href="../author/therese-fessenden/index.php">Therese Fessenden</a></li>
                  
                  <li><a href="../author/kim-flaherty/index.php">Kim Flaherty</a></li>
                  
                  <li><a href="../author/sarah-gibbons/index.php">Sarah Gibbons</a></li>
                  
                  <li><a href="../author/aurora-harley/index.php">Aurora Harley</a></li>
                  
                  <li><a href="../author/kate-kaplan/index.php">Kate Kaplan</a></li>
                  
                  <li><a href="../author/page-laubheimer/index.php">Page Laubheimer</a></li>
                  
                  <li><a href="../author/angela-li/index.php">Angie Li</a></li>
                  
                  <li><a href="../author/hoa-loranger/index.php">Hoa Loranger</a></li>
                  
                  <li><a href="../author/kate-meyer/index.php">Kate Meyer</a></li>
                  
                  <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                  
                  <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                  
                  <li><a href="../author/kara-pernice/index.php">Kara Pernice</a></li>
                  
                  <li><a href="../author/christian-rohrer/index.php">Christian Rohrer</a></li>
                  
                  <li><a href="../author/amy-schade/index.php">Amy Schade</a></li>
                  
                  <li><a href="../author/katie-sherwin/index.php">Katie Sherwin</a></li>
                  
                  <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                  
                  <li><a href="../author/kathryn-whitenton/index.php">Kathryn Whitenton</a></li>
                  
                </ul>
              </section>
              
            </div>
            
          </div>

          
          <section class="hide-for-small-down">
            
              
              <h1>Recent Articles</h1>
              
              <ul>
                  
                  <li><a href="../seamless-cross-channel/index.php">Seamlessness in the Omnichannel User Experience</a></li>
                  
                  <li><a href="../brand-intention-interpretation/index.php">Experience Design: Bridging Brand Intention and Brand Interpretation</a></li>
                  
                  <li><a href="../eyetracking-tasks-efficient-scanning/index.php">Scanning Patterns on the Web Are Optimized for the Current Task</a></li>
                  
                  <li><a href="../flat-design-best-practices/index.php">Flat-Design Best Practices</a></li>
                  
                  <li><a href="../ux-user-stories/index.php">Accounting for UX Work with User Stories in Agile Projects</a></li>
                  
                  <li><a href="../index.php"><strong>See all articles&hellip;</strong></a></li>
              </ul>
              
            
          </section>
          

          
          
          <section class="hide-for-small-down">
            <h1>Popular Articles</h1>
            
            <ul>
                
                <li><a href="../usability-101-introduction-to-usability/index.php">Usability 101: Introduction to Usability</a></li>
                
                <li><a href="../top-10-mistakes-web-design/index.php">Top 10 Mistakes in Web Design</a></li>
                
                <li><a href="../how-users-read-on-the-web/index.php">How Users Read on the Web</a></li>
                
                <li><a href="../f-shaped-pattern-reading-web-content/index.php">F-Shaped Pattern For Reading Web Content</a></li>
                
                <li><a href="../ten-usability-heuristics/index.php">10 Usability Heuristics for User Interface Design</a></li>
                
                <li><a href="../intranet-design-2016/index.php">10 Best Intranets of 2016</a></li>
                
                <li><a href="../which-ux-research-methods/index.php">When to Use Which User-Experience Research Methods</a></li>
                
                <li><a href="../response-times-3-important-limits/index.php">Response Times: The 3 Important Limits</a></li>
                
                <li><a href="../why-you-only-need-to-test-with-5-users/index.php">Why You Only Need to Test with 5 Users</a></li>
                
                <li><a href="../page-fold-manifesto/index.php">The Fold Manifesto: Why the Page Fold Still Matters</a></li>
                
            </ul>
          </section>
          
          

          
            <section class="hide-for-small-down">
              
            </section>
          

      </section>
      

      
        <div class="small-12 medium-9 columns content-body">
          




<article class="l-subsection alertbox-article-content article-detail-bl">
  
  <header>
    <div class="l-subsection">
      <h1>The Impact of Tone of Voice on Users&#39; Brand Perception</h1>
      <div class="author-meta">by
        
          
            
            <a href="../author/kate-meyer/index.php">Kate Meyer</a>
            
          
        on  August 7, 2016
        
      </div>

      

      
      <div class="topic-links">
        Topics:
        
          <ul>

  <li><a href="../../topic/branding/index.php">Branding</a></li>

  <li><a href="../../topic/content-strategy/index.php">Content Strategy</a></li>

  <li><a href="../../topic/writing-web/index.php">Writing for the Web</a></li>

</ul>
        
      </div>
      
    </div>
  </header>
  

  
  
  <section>
    <p class="lede"><strong>Summary:</strong> A two-part experiment found that different tones of voice on a website have measurable impacts on users’ perceptions of a brand’s friendliness, trustworthiness, and desirability. Casual, conversational, and enthusiastic tones performed best.</p>
    <br />
  </section>
  
  

  
  <section id="article-body"><p>As web content professionals, we know that <em>how</em> we communicate with our users is just as important as <em>what</em> we’re communicating. But often, it’s difficult to predict and demonstrate exactly how our tone of voice might affect our users.</p>

<p>In a two-part study, we tested pairs of nearly identical website content. In each pair, the only aspect that we varied was the tone of voice used. We found that there are indeed <strong>measurable effects of tone of voice on users</strong>, specifically on users’ impressions of an organization’s friendliness, trustworthiness, and desirability. We also found that a user’s impression of an organization’s trustworthiness is a strong predictor of their willingness to recommend that <a href="../brand-experience-ux/index.php">brand</a>.</p>

<h2>The Tone of Voice Samples</h2>

<p>We began by identifying <a href="../tone-of-voice-dimensions/index.php">4 core dimensions of tone of voice</a>. Each dimension can be thought of as a 3-point scale, with a neutral midpoint. They can be used to create comparable tone profiles for content. Tones could fall at either extreme of each dimension, or somewhere in between:</p>

<ul>
	<li>Funny vs. serious</li>
	<li>Formal vs. casual</li>
	<li>Respectful vs. irreverent</li>
	<li>Enthusiastic vs. matter-of-fact</li>
</ul>

<div style="text-align:center">
<figure class="caption" style="display:inline-block"><img alt="" height="356" src="https://media.nngroup.com/media/editor/2016/07/11/tone-profile-delta.png" width="400"/>
<figcaption><em>This example of tone profile illustrates how a site’s tone of voice can be expressed as a function of the 4 tone dimensions. We can say that this site’s tone of voice is serious and casual. (When a dimension is not noted, it means that the site rates as neutral on that dimension.)</em></figcaption>
</figure>
</div>

<p>We used those 4 dimensions to design 4 pairs of tone-of-voice samples, with 8 <a href="../tone-voice-samples/index.php">samples</a> in total.</p>

<p>Each pair of samples was nearly identical — the same presentation, topic, context, and details. The only things we varied between the pairs was the tone and the name of the fake organization. The pairs were:</p>

<ol>
	<li>Two landing pages for fake <strong>insurance companies,</strong> highlighting the features of their auto-insurance services and encouraging users to get a quote online</li>
	<li>Two patient-care pages for fake<strong> hospitals</strong>, explaining the importance of their high-quality care</li>
	<li>Two landing pages for fake <strong>banks</strong>, listing the benefits of their personal checking accounts</li>
	<li>Two homepages for fake <strong>home</strong>-<strong>security systems</strong>, providing an overview of their available services</li>
</ol>

<p>Each tone sample was written as realistically as possible. The language used came from real-life websites, but was adapted to remove any extraneous differences that could become confounding variables. For example, both of the bank pages had to have identical APY rates, because if one had a better rate than the other to prevent participants from responding more favorably to that detail, rather than the tone.</p>

<p>We designed the samples to <a href="../tone-voice-samples/index.php">create a variety of tone profiles</a>, described in the following table.</p>

<table border="1" cellpadding="0" cellspacing="0">
	<tbody>
		<tr>
			<td style="width:107px;">
			<p><strong>Pair</strong></p>
			</td>
			<td style="width:65px;"> </td>
			<td style="width:131px;">
			<p><strong>Tone profile</strong></p>
			</td>
			<td style="width:320px;">
			<p><strong>Copy example</strong></p>
			</td>
		</tr>
		<tr>
			<td rowspan="2" style="width:107px;">
			<p><strong>Auto insurance</strong></p>
			</td>
			<td style="width:65px;">
			<p><strong>Alpha</strong></p>
			</td>
			<td style="width:131px;">
			<p>Funny,<br/>
			casual,<br/>
			irreverent, enthusiastic</p>
			</td>
			<td style="width:320px;">
			<p>“You’ll find that our ALPHA car insurance customers are a happy bunch. With our world-class service and speedy claims process, who can blame them?”</p>
			</td>
		</tr>
		<tr>
			<td style="width:65px;">
			<p><strong>Beta</strong></p>
			</td>
			<td style="width:131px;">
			<p>Serious,<br/>
			matter-of-fact</p>
			</td>
			<td style="width:320px;">
			<p>“Your lifestyle, driving habits, and policy management preferences are just a few of the many factors considered when determining which discounts are available to you.”</p>
			</td>
		</tr>
		<tr>
			<td rowspan="2" style="width:107px;">
			<p><strong>Checking account</strong></p>
			</td>
			<td style="width:65px;">
			<p><strong>Delta</strong></p>
			</td>
			<td style="width:131px;">
			<p>Serious, casual</p>
			</td>
			<td style="width:320px;">
			<p>“Everything you need and want from a bank: online and mobile banking services, a debit MasterCard, and standard checks. And those are just a few of the features included at no charge.”</p>
			</td>
		</tr>
		<tr>
			<td style="width:65px;">
			<p><strong>Gamma</strong></p>
			</td>
			<td style="width:131px;">
			<p>Serious, formal, matter-of-fact</p>
			</td>
			<td style="width:320px;">
			<p>“Open an account today to get multiple interest-bearing accounts and complimentary and discounted services.”</p>
			</td>
		</tr>
		<tr>
			<td rowspan="2" style="width:107px;">
			<p><strong>Security system</strong></p>
			</td>
			<td style="width:65px;">
			<p><strong>Theta</strong></p>
			</td>
			<td style="width:131px;">
			<p>Funny, casual, irreverent, enthusiastic</p>
			</td>
			<td style="width:320px;">
			<p>“A Theta Security sign in your front yard tells criminals, ‘Don’t even think about it.’”</p>
			</td>
		</tr>
		<tr>
			<td style="width:65px;">
			<p><strong>Kappa</strong></p>
			</td>
			<td style="width:131px;">
			<p>Serious, respectful, matter-of-fact</p>
			</td>
			<td style="width:320px;">
			<p>“Make home feel safe again. We’re passionate about protecting your world.”</p>
			</td>
		</tr>
		<tr>
			<td rowspan="2" style="width:107px;">
			<p><strong>Hospital</strong></p>
			</td>
			<td style="width:65px;">
			<p><strong>Epsilon</strong></p>
			</td>
			<td style="width:131px;">
			<p>Serious, formal, respectful, matter-of-fact</p>
			</td>
			<td style="width:320px;">
			<p>“For over four decades, Epsilon Health has met the health care needs of the local community. Juxtaposed with clinical excellence and cutting-edge research, Epsilon takes pride in innovative treatments.”</p>
			</td>
		</tr>
		<tr>
			<td style="width:65px;">
			<p><strong>Zeta</strong></p>
			</td>
			<td style="width:131px;">
			<p>Serious, casual, respectful, enthusiastic</p>
			</td>
			<td style="width:320px;">
			<p>“At Zeta Healthcare, we don’t just treat diseases, we treat individuals. We put our patients’ priorities at the center of our care.”</p>
			</td>
		</tr>
	</tbody>
</table>

<p><em>Table 1: We designed <a href="../tone-voice-samples/index.php">four pairs of tone-of-voice samples</a>. Each pair was nearly identical except for the tone-of-voice profile.</em></p>

<p>The tone pairs were developed based on real-life content from actual web pages. We examined the tones of those real-life pages, and then combined excerpts from the real copy to create the fictional tone samples.</p>

<p>We used a manipulation check to verify that the variations in tone between the two samples in a pair were perceptible and aligned with our definitions. We surveyed 50 Americans aged 18 or older. They answered 2 Likert-rating-scales questions for each tone sample, indicating their agreement with the following two statements:</p>

<ul>
	<li>“The website is friendly.”</li>
	<li>“The website is formal.”</li>
</ul>

<p>The results are discussed in more detail in our article on the <a href="../tone-of-voice-dimensions/index.php">dimensions of tone of voice</a> and strongly confirmed that the tonal qualities of each pair were perceived as different.</p>

<h2>Methodology</h2>

<p>Our study had <strong>two primary phases</strong>.</p>

<ol>
	<li>Qualitative in-person user testing</li>
	<li>Quantitative online survey, using <a href="http://userzoom.com/">UserZoom</a> and <a href="https://www.viget.com/articles/how-to-run-quick-and-cheap-usability-tests-using-mechanical-turk" title="Blog post from Viget.com">Amazon’s Mechanical Turk</a></li>
</ol>

<p>We should pause here and acknowledge the very real <a href="../testing-content-websites/index.php">challenges of evaluating writing</a> through user research. Many users are prone to make comments related to aspects of visual design or information architecture, but aren’t inclined to think about the writing. To eliminate potential distractions, we presented the tone samples in two stripped-down formats: simple wireframes for the qualitative phase, and text excerpts for the quantitative phase.</p>

<figure class="caption"><img alt="" height="921" src="https://media.nngroup.com/media/editor/2016/07/11/hospital-zeta.jpg" width="1440"/>
<figcaption><em>The simple wireframe for Zeta, one of the fake hospitals. These wireframes were used during the qualitative research.</em></figcaption>
</figure>

<figure class="caption"><img alt="" height="428" src="https://media.nngroup.com/media/editor/2016/07/18/tone-delta-text-2.png" width="661"/>
<figcaption><em>The text-only version of Delta, one of the fake banks. These samples were presented during the quantitative research, with only basic formatting (bold text, hyperlink styling, variations in font size, etc.)</em></figcaption>
</figure>

<p>During the <strong>qualitative testing</strong>, we gave users scenarios (e.g. “Imagine your friend is preparing for minor surgery. You’ve offered to help your friend decide which hospital to visit for the surgery.”) and showed them the wireframes. We asked them to think aloud and share their thoughts on the wireframes.</p>

<p>In the <strong>quantitative survey</strong> of 100 American users over the age of 18, we asked the respondents to rate each sample on three 5-point Likert scales. In each scale, we asked the respondents to indicate their impressions of the organization itself, not their impressions of the writing. We were interested in measuring respondents’ perceptions of the <a href="../brand-experience-ux/index.php">brand’s</a>:</p>

<ul>
	<li>Friendliness</li>
	<li>Trustworthiness</li>
	<li>Desirability (whether they would recommend the product or service to a friend)</li>
</ul>

<p>We selected these three factors as the most important aspects of users’ impressions that we expected to be influenced by the tone of voice used in the writing. Of these three, desirability is the most important: a user’s indication of willingness to recommend a brand is a strong predictor of the success of that brand (which is partially why the Net Promoter Score, NPS, is so popular). Desirability is derived subjectively by each customer from those product and service qualities that we do have the ability to design, such as the tone-of-voice manipulations in this study.</p>

<h2>Key Takeaways</h2>

<h3>Tone of voice has measurable qualities and measurable impact on users.</h3>

<p>There will always be subtle differences in individual interpretations of tone. What sounds “witty” to one person will sound “corny” to another. But our findings suggest that:</p>

<ul>
	<li>there are <strong>quantifiable qualities of tone</strong> (like friendliness and formality);</li>
	<li>those aspects have <strong>measurable impacts on user’s impressions of brand personality</strong> (like the friendliness and trustworthiness of the brand); and</li>
	<li>those impressions <strong>significantly influence users’ willingness to recommend a brand</strong>.</li>
</ul>

<p>Measurements of desirability (like NPS) are critical to brand success, and can be very difficult to improve — but <strong>these results show that the tone of your content can influence that desirability</strong>.</p>

<figure class="caption"><img alt="" height="832" src="https://media.nngroup.com/media/editor/2016/07/11/differences-pairs-2.png" width="1101"/>
<figcaption><em>This chart summarizes the average <strong>differences </strong>between each sample pair for the three measured brand impressions: friendliness, trustworthiness, and desirability. So, for example, the first two bars show that Alpha Insurance was rated significantly friendlier than Beta Insurance but that it was significantly less trustworthy. The third bar shows that there was no significant difference in users' willingness to recommend the two insurance companies.<br/>
The asterisks by the bars indicate which differences are significant at p &lt; 0.05.</em></figcaption>
</figure>

<h3>Trust and credibility are essential.</h3>

<p>A multiple regression analysis of our survey data with friendliness and trustworthiness as the independent variables showed that both factors were statistically significant predictors of desirability (willingness to recommend). However, trustworthiness is a much stronger predictor of desirability, across all the industries.</p>

<p>On average, <strong>52% of the variability in the desirability scores is explained by trustworthiness</strong>. Friendliness only explains an extra 8% of the variability in the desirability scores when we add it to the regression analysis. This confirms what we would expect — that users’ <a href="../trustworthy-design/index.php" title="Trustworthiness in Web Design: 4 Credibility Factors">perceptions of trustworthiness are critical</a> to their decisions whether or not to interact with an organization (make a purchase, use a service, etc.) But it is surprising that trustworthiness may be so much more important than friendliness. <strong>Tone of voice is a powerful tool for influencing that perception of trustworthiness</strong>, and the right tone to evoke trust will differ based on your users and their concerns. Make sure you convey a sense of trust in all aspects of your site.</p>

<h3>A playful tone won’t work well for everyone.</h3>

<p>Participants in both phases of the study agreed that Alpha (the auto-insurance sample whose tone profile was playful and informal – see Table 1) was friendlier than the more serious sample, Beta. (In the online survey, Alpha was rated as friendlier by a difference of 0.3 points ± 0.2 on a 5-point scale.) Despite this, respondents were no more likely to recommend Alpha, and actually rated Beta as more trustworthy than Alpha by 0.3 points.</p>

<p>A playful tone for a serious industry has the potential of creating pleasant surprise and helping a company stand out from its competitors. However, in this situation,<strong> the friendliness and irreverence actually undermined users’ perceptions of trustworthiness and professionalism.</strong></p>

<p>People don’t expect car insurance to be fun, and this particular humorous tone came on too strong for the topic. “The speech is too chummy,” one participant pointed out. “The friendliness kind of takes away some of the credibility.”</p>

<p>If you’re frequently hearing feedback from your stakeholders like, “Make it exciting!” or “Add a joke,” bear this example in mind. <strong>An exciting, chummy, or funny tone of voice won’t be ideal for all organizations.</strong></p>

<h3>Even traditionally dry industries like finance can benefit from a little conversational language.</h3>

<p>Serious doesn’t necessarily have to be cold and formal. In the quantitative phase of the study, the more casual bank, Delta, was perceived as friendlier (0.7 points ± 0.2 on a 5-point scale) and more trustworthy (0.3 ± 0.1) than its serious counterpart Gamma. Respondents were more likely to recommend Delta to a friend (0.4 ± 0.2).</p>

<p>Participants in the qualitative phase described Delta as “approachable” and “straightforward,” and described Gamma as “dull” and “intimidating.”</p>

<h3>Don’t let a humorous tone get in the way of actually communicating with your users.</h3>

<p>In the online survey, respondents rated the humorous tone of the home-security site Theta as friendlier (0.5 points ± 0.2 on a 5-point scale) and more trustworthy (0.4 ± 0.1) than Kappa’s. Respondents were also more likely to recommend Theta to a friend (0.4 ± 0.2).</p>

<p>One participant in the qualitative phase enjoyed the humor in Theta, and found Kappa to be too heavy. “’Make Home Feel Safe Again,’” she read. “Like it wasn’t safe before?” But three other participants really <em>disliked</em> Theta. One participant said she found the humorous headlines to be “corny,” and another said, “They need to get more to the point.”</p>

<p>This tone-sample pair illustrates the fact that <strong>humor can be a powerful way of differentiating</strong> you from your competitors — as long as your users actually find it funny. <strong>Humor is extremely risky</strong>, because when it fails, you annoy and alienate your users.</p>

<p>In particular, <strong>don’t let your attempts at humor get in the way of actually communicating</strong> to users the information they need to know. Theta used headlines like “Where there’s smoke, there’s us” (a verbatim headline from a real home-security website). Funny or clever headlines often fail to serve the actual purpose of headlines — letting users know what information is located below the headline.</p>

<h3>Consider the emotional needs of your users when choosing a tone.</h3>

<p>In the qualitative study, the more casual and enthusiastic hospital, Zeta, was perceived as friendlier than Epsilon (a difference of 0.5 points ± 0.2 points on a 5-point scale), and marginally more trustworthy than Epsilon (<em>p</em>=0.05). In the qualitative phase, users unanimously preferred Zeta over Epsilon.</p>

<p>In this scenario, Zeta’s friendliness without an attempt at humor seemed to reassure users during a potentially stressful situation. One participant in the qualitative study compared the difference between Zeta and Epsilon to the difference between a doctor and a nurse. She described Epsilon as “businesslike,” and said that in her experience “nurses have more of a bedside manner.”</p>

<p><strong>Remember to consider the concerns of your readers and their emotional state when choosing a tone. </strong>If your users are nervous patients preparing for surgery, do you really think they’d prefer a formal tone? If your users are arriving frustrated at an error page, do you really think they want that error message to be a joke?</p>

<h3>The best tone for your content will depend on your users, your message, and your brand.</h3>

<p>Across all of the tone samples tested, <strong>we saw that casual, conversational, and moderately enthusiastic tones performed best</strong>, though they do not necessarily need to be combined. As we saw with Delta and Gamma, a conversational but serious tone can be successful for a bank.</p>

<p>Choosing a tone of voice is a tricky game of balancing your brand’s personality and priorities. There’s no one solution for every situation. As we saw with Alpha and Beta, it’s possible to choose a tone that makes your brand seem friendly, but still doesn’t make your potential customers more likely to choose you.</p>

<p>The best way to know what will work for you is to <strong>evaluate your tone of voice with your users.</strong></p>

<h2>Tips for Evaluating Your Tone of Voice</h2>

<p>Your users’ context is hugely important, and it’s not always easy to predict. You might think people respond to a grandiose and professional tone for a hospital, because it inspires confidence. But many participants found that a light, conversational tone conveyed empathy, which was meaningful to them. Remember, <strong>you are not your user</strong><em>.</em> Your predictions of how your users will react to your tone may be off. As much as possible, test with representative users to help you evaluate your expectations.</p>

<p><strong>Interpretation of tone is intensely personal and individual.</strong> You'll always have a few people that hate or love a tone unexpectedly, but you have to listen to the comments to understand overall themes across many participants.</p>

<p>Since there is substantial nuance in the tone-of-voice interpretations (and users’ associated preferences), we recommend that you <strong>don’t rely solely on quantitative metrics</strong> like the ones we've discussed here.</p>

<p>Quantitative data is useful for measuring whether users can perceive your intended tone, and comparing such numbers between alternate proposed pieces of writing. Qualitative research is much better for understanding why users like certain forms of communication, as well as getting deeper insights into how they interpret your content. For these insights, be sure to conduct qualitative user studies where customers will <em>say something</em> about your content, not just score it.</p>

<p>If you find that your users are frequently commenting on the visual design, interaction design, or features of your UI when you want them to comment on tone, you have to be patient. <strong>You shouldn’t just direct your users to talk about the tone of voice</strong>, because you’ll risk biasing them.</p>

<p>To encourage user comments about the tone of voice:</p>

<ul>
	<li>Try eliminating distractions by creating wireframes or simple text excerpts of your content (as we did for this experiment).</li>
	<li>Ask your users prompting but vague questions, such as “If this website were a person, who would it be? Why?,” to get them thinking about the personality of the writing.</li>
	<li>Try a modified <a href="../microsoft-desirability-toolkit/index.php">product-reaction test</a>, by asking your users to select descriptions from <a href="../tone-voice-words/index.php">a list of tone words.</a></li>
</ul>

<h2>Conclusion</h2>

<p>Tone of voice is a complicated and nuanced aspect of your digital copy. However, as this study shows, the impacts of tone on brand perception are significant enough to merit your attention. Tone of voice <em>can and should</em> be tested, just like other pieces of the user experience.</p>

<p>(More on tone of voice in our <a href="../../courses/topic/content-strategy/index.php">digital content strategy</a> courses. More on trustworthiness in the <a href="../../courses/credibility-and-persuasive-web-design/index.php">Persuasive Web Design</a> course.)</p>
</section>
  

  
  <section>
    
        
    
  </section>
  
  
  
  
  

  


  <section>
    <br />
    <p class="share"><b>Share <span class="hide-for-small-down">this article</span>:</b> <a href="https://twitter.com/intent/tweet?url=http://www.nngroup.com/articles/tone-voice-users/&amp;text=The%20Impact%20of%20Tone%20of%20Voice%20on%20Users'%20Brand%20Perception&amp;via=nngroup">Twitter</a> | <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=http://www.nngroup.com/articles/tone-voice-users/&amp;title=The%20Impact%20of%20Tone%20of%20Voice%20on%20Users'%20Brand%20Perception&amp;source=Nielsen%20Norman%20Group">LinkedIn</a> | <a href="https://plus.google.com/share?url=http://www.nngroup.com/articles/tone-voice-users/">Google+</a> | <a href="mailto:?subject=NN/g Article: The Impact of Tone of Voice on Users&#39; Brand Perception&amp;body=http://www.nngroup.com/articles/tone-voice-users/">Email</a></p>
    <hr />
  </section>

  <footer class="l-subsection">
    
    
    <div class="learn-more">
      <h2>Learn More</h2>
      <p><a href="../subscribe/index.php">Subscribe to the weekly newsletter</a> to get notified about future articles.</p>
      </div>
    
    

    <div class="row show-for-small-down related-content">
      
      
      

      
      
        <div class="column small-12 topic-links">All articles about:
          <ul>
            
            <li><a href="../../topic/branding/index.php">Branding</a></li>
            
            <li><a href="../../topic/content-strategy/index.php">Content Strategy</a></li>
            
            <li><a href="../../topic/desirability/index.php">desirability</a></li>
            
            <li><a href="../../topic/tone-voice/index.php">tone of voice</a></li>
            
            <li><a href="../../topic/writing-web/index.php">Writing for the Web</a></li>
            
          </ul>
        </div>
      
      

      
      
        <div class="column small-12">
          <h3>UX Conference Training Courses</h3>
           <ul class="no-bullet">
    
        <li><a href="../../courses/cross-channel-user-experience/index.php">Omnichannel Journeys and Customer Experience</a></li>
    
        <li><a href="../../courses/stakeholder-ux-approval/index.php">Engaging Stakeholders to Build Buy-In</a></li>
    
        <li><a href="../../courses/credibility-and-persuasive-web-design/index.php">Persuasive Web Design</a></li>
    
        <li><a href="../../courses/content-strategy/index.php">Content Strategy</a></li>
    
        <li><a href="../../courses/translating-brand/index.php">Translating Brand into User Interactions</a></li>
    
</ul>
        </div>
      
      

      
      
        <div class="small-12 column">
          <h3>Research Reports</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../reports/how-people-read-web-eyetracking-evidence/index.php">How People Read on the Web: The Eyetracking Evidence</a></li>
              
            
              
                <li><a href="../../reports/strategic-design-faqs/index.php">Strategic Design for Frequently Asked Questions</a></li>
              
            
              
                <li><a href="../../reports/social-media-user-experience/index.php">Social Media User Experience</a></li>
              
            
              
                <li><a href="../../reports/pr-websites/index.php">PR on Websites</a></li>
              
            
          </ul>
        </div>
      
      

      
      
        <div class="small-12 column">
          <h3>Online Seminars</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../online-seminars/improving-intranet-content/index.php">Improving Intranet Content</a></li>
              
            
              
                <li><a href="../../online-seminars/every-word-count/index.php">Making Every Word Count</a></li>
              
            
          </ul>
        </div>
      
      
    </div>

    <div class="hide-for-small-down row related-content">
      <div class="medium-6 columns">
        
        
          <div>
            <h3>Articles</h3>
            <ul class="no-bullet">
              
                
                <li><a href="../tone-of-voice-dimensions/index.php">The Four Dimensions of Tone of Voice</a></li>
                
              
                
                <li><a href="../interaction-branding/index.php">The Impact of Interaction Design on Brand Perception</a></li>
                
              
                
                <li><a href="../centered-logos/index.php">Centered Logos Hurt Website Navigation</a></li>
                
              
                
                <li><a href="../mobile-content/index.php">Reading Content on Mobile Devices</a></li>
                
              
                
                <li><a href="../top-10-enduring/index.php">Top 10 Enduring Web-Design Mistakes</a></li>
                
              
            </ul>
          </div>
        
        

        
          
          <div>
            <h3>Research Reports</h3>
            <ul class="no-bullet">
              
                
                  <li><a href="../../reports/how-people-read-web-eyetracking-evidence/index.php">How People Read on the Web: The Eyetracking Evidence</a></li>
                
              
                
                  <li><a href="../../reports/strategic-design-faqs/index.php">Strategic Design for Frequently Asked Questions</a></li>
                
              
                
                  <li><a href="../../reports/social-media-user-experience/index.php">Social Media User Experience</a></li>
                
              
                
                  <li><a href="../../reports/pr-websites/index.php">PR on Websites</a></li>
                
              
            </ul>
          </div>
          
        

      </div>
      <div class="medium-6 columns">
        
          
          <div>
            <h3>UX Conference Training Courses</h3>
             <ul class="no-bullet">
    
        <li><a href="../../courses/cross-channel-user-experience/index.php">Omnichannel Journeys and Customer Experience</a></li>
    
        <li><a href="../../courses/stakeholder-ux-approval/index.php">Engaging Stakeholders to Build Buy-In</a></li>
    
        <li><a href="../../courses/credibility-and-persuasive-web-design/index.php">Persuasive Web Design</a></li>
    
        <li><a href="../../courses/content-strategy/index.php">Content Strategy</a></li>
    
        <li><a href="../../courses/translating-brand/index.php">Translating Brand into User Interactions</a></li>
    
</ul>
          </div>
          
        
        
          
          <div>
            <h3>Online Seminars</h3>
            <ul class="no-bullet">
            
                <li><a href="../../online-seminars/improving-intranet-content/index.php">Improving Intranet Content</a></li>
            
                <li><a href="../../online-seminars/every-word-count/index.php">Making Every Word Count</a></li>
            
            </ul>
          </div>
          
        
      </div>
    </div>
  </footer>
</article>

        </div>

      

    
  </div>
  <div class="row">
    
      <div class="column small-12 show-for-small-down l-navrail announcements">
        
          
        
      </div>
    
    
  </div>

    </div>
    




<footer id="page-footer">
    <div class="row hide-for-print">
        <div class="column small-12 medium-6">
            <section class="footer-popular">
                
                <h1>Popular Topics</h1>
                <div style="float: left;  width: 15em; display: block;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/applications/index.php">Application Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/e-commerce/index.php">E-Commerce Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/eyetracking/index.php">Eyetracking </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/information-architecture/index.php">Information Architecture </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/intranets/index.php">Intranet Design </a></li>
	<li><a href="../../topic/interaction-design/index.php">Interaction Design </a></li>
</ul>
</div>

<div style="float: left;  width: 15em; display: block; list-style: none;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/mobile-and-tablet-design/index.php">Mobile and Tablet Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/credibility-persuasion/index.php">Persuasive Design</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/research-methods/index.php">Research Methods</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/user-testing/index.php">User Testing </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/web-usability/index.php">Web Usability </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/writing-web/index.php">Web Writing &amp; Content Strategy </a></li>
</ul>
</div>

<p>&nbsp;</p>

                
            </section>
        </div>

        <div class="column small-12 medium-6">
            <div class="row">
                <div class="column small-6">
                    <section class="footer-about">
                        <h1>Follow Us</h1>
                        <ul>
	<li><a href="../subscribe/index.php">Subscribe to our weekly UX newsletter</a>,&nbsp;Jakob Nielsen&#39;s Alertbox</li>
	<li>Visit Don Norman&#39;s website, <a href="http://www.jnd.org/"> www.jnd.org </a></li>
	<li>Visit Bruce Tognazzini&#39;s website, <a href="http://www.asktog.com/"> www.asktog.com </a></li>
	<li style="padding-top: 6px;"><a class="twitter-follow-button" data-show-count="false" data-size="large" href="https://twitter.com/nngroup"><img alt="Follow @nngroup on Twitter" height="20" src="https://media.nngroup.com/media/editor/2016/12/30/twitter-follow-button.png" style="width: 117px; height: 20px;" width="117" /></a></li>
	<li style="padding-top: 6px;"><img alt="" height="14" src="https://media.nngroup.com/media/editor/2016/07/26/feed-icon-14x14.png" style="vertical-align:text-top;width:14px;margin-right:.25em;" width="14" /><a href="../../feed/rss/index.php">Subscribe to our RSS feed</a></li>
</ul>

                    </section>
                </div>

                <div class="column small-6">
                    <section class="footer-about right-column">
                        <h1>About
</h1>
                        <ul class="no-bullet">
	<li><a href="../../about/contact/index.php">Contact information</a></li>
	<li><a href="../../about/index.php">About Us</a></li>
	<li><a href="../../about/why-nng/index.php">Why NN/g? </a></li>
	<li><a href="../../return-policy/index.php">Return Policy</a></li>
</ul>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="copyright column small-12">
            <p><a href="../../copyright-and-reprint-info/index.php">Copyright</a> &copy; 1998-2017 Nielsen Norman Group, All Rights Reserved.</p>
        </div>
    </div>
</footer>


    


        <script src="https://media.nngroup.com/static/vendor/fastclick/js/fastclick.min.js"></script>
        <script src="https://media.nngroup.com/static/vendor/foundation/js/foundation.min.js"></script>
        <script src="http://media.nngroup.com/static/js/app.js"></script>
        <script src="http://media.nngroup.com/static/js/scroll-to-fixed.js"></script><script>var externalLinks = document.querySelectorAll('a[href^="http"]');for (var i = externalLinks.length-1; i >= 0; i--) {externalLinks[i].addEventListener("click", function(event) { alert("External links are disabled for this test."); event.preventDefault(); }, false);}var mailLinks = document.querySelectorAll('a[href^="mailto"]');for (var i = mailLinks.length-1; i >= 0; i--) {mailLinks[i].addEventListener("click", function(event) { alert("Email links are disabled for this test."); event.preventDefault(); }, false);}var externalForms = document.querySelectorAll('form');for (var i = externalForms.length-1; i >= 0; i--) {externalForms[i].addEventListener("submit", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false); externalForms[i].addEventListener("click", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false);}</script>

        <script>
            
            $(function() {
                $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
                $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
            });
        </script>

        
<script src="https://media.nngroup.com/static/js/articles.js"></script>


        
        <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1072699960;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072699960/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
    </body>

<!-- Mirrored from www.nngroup.com/articles/tone-voice-users/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:04:16 GMT -->
</html>
