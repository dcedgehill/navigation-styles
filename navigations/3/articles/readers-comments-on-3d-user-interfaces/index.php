<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->





    
<!-- Mirrored from www.nngroup.com/articles/readers-comments-on-3d-user-interfaces/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:16:06 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1026.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"licenseKey":"1a30ef9931","queueTime":0,"beacon":"bam.nr-data.net","errorBeacon":"bam.nr-data.net","applicationID":"2048992","transactionName":"NFxSMRBUX0EDUkIPDA0WdhAMVkVbDV8ZBxEXUFMJB0YfRAtUQRVZIktEDAFZVHYHRVcPD01eVRE=","applicationTime":587,"agent":""}</script>
        <title>Readers&#39; Comments on 3D User Interfaces</title><meta property="og:title" content="Readers&#39; Comments on 3D User Interfaces" />
  
        
        <meta name="description" content="2D GUIs also had problems which were overcome, so readers think there is hope for 3D yet.">
        <meta property="og:description" content="2D GUIs also had problems which were overcome, so readers think there is hope for 3D yet." />
        
  
        
	
        
        <meta name="keywords" content="DOOM, Castle Wolfenstein 3D, 2D, Bob Jacobson, evolution and interface design, immersive designs, augmented reality, virtual reality, Fakespace, Worldesign, auditory interfaces, gestures">
        

        

        <meta name="robots" content="noarchive">
        <meta name="viewport" content="width=device-width">

        <link rel="search" type="application/opensearchdescription+xml" title="NN/g Search" href="../../opensearch.xml" />
        <link rel="shortcut icon" href="http://media.nngroup.com/static/img/favicon.ico" />

        <link rel="stylesheet" href="https://media.nngroup.com/static/vendor/font-awesome/css/font-awesome.min.css">

        
        <link rel="stylesheet" href="https://media.nngroup.com/static/css/7232a960f1df.css" type="text/css" />
        <link rel="stylesheet" href="http://fast.fonts.com/t/1.css?apiType=css&amp;projectid=c5846260-9def-46ec-9407-0f1349bd2d64" type="text/css">

        <script src="https://media.nngroup.com/static/vendor/modernizr/js/modernizr.min.js"></script>

        <script type="text/javascript" src="https://media.nngroup.com/static/js/3283be5def74.js"></script>
        

        

        
        
        <!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=229034,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('http://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->


<!-- Google Analytics Tracking Code for http://nngroup.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23132681-1', 'nngroup.com');
  ga('require', 'linkid', 'https://www.nngroup.com/articles/readers-comments-on-3d-user-interfaces/linkid.js');
  ga('require', 'displayfeatures');

// Optimizely Universal Analytics Integration
 window.optimizely = window.optimizely || [];
 window.optimizely.push("activateUniversalAnalytics");

  ga('send', 'pageview');

</script>
<script>
/**
* Function to track a click on an outbound link in Google Analytics.
*/
function trackOutboundLink(label, url) {
    if(window.ga && ga.loaded) {
        ga('send', 'event', 'outbound', 'click', label, {'hitCallback': function(){document.location = url;}
        });
    } else {
        document.location = url;
    }
}
/**
* Function to track a click on a link in the article announcement area.
*/
$('#articleAnnouncement a').on('click', function(){
  ga('send', 'event', 'link', 'click', 'articleAnnouncement');
});
</script>

<script type="text/javascript">
 setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2421.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



<!-- Hotjar Tracking Code for http://nngroup.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:121605,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

        <script src="https://media.nngroup.com/static/js/analytics/track_outbound_link.js"></script>
        
        
        
    </head>
    <body class="location-articles articles-detail">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
    <header id="page-header">
        <div id="cart" class="shopping-cart mobile-cart show-for-small-down"></div>
        <div class="row">
            <div id="masthead" class="masthead small-12 medium-7 column">

                <!-- The button to toggles menu on small screen -->

                <h1><a href="../../index.php"><img src="http://media.nngroup.com/static/img/logo-wordmark.png" width="74" height="30"> Nielsen <span class="norman">Norman</span> Group</a></h1>
                <p class="tagline show-for-medium-up">Evidence-Based User Experience Research, Training, and Consulting</p>

                <!-- The button to toggles form search on small screen -->


                <div style="clear: right;width:  100%;height: 1px;"></div><?php define('ROOT_PATH',$_SERVER["DOCUMENT_ROOT"] . '/apps/navigation_styles/navigations/3'); ?><?php include(ROOT_PATH)."/navigation.php"; ?>

                <!-- Drop down menu on small screen -->
                <div id="hideNav" class="show-for-small-down">
                  
                  <ul class="menu-content">
                    <li><a href="../../index.php">Home</a></li>
                    <li class="has-submenu">
                      <a>Training</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../training/index.php">Overview</a></li>
                        
<li id="nav-ux-conference"><a href="../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../online-seminars/index.php"><span>Online Seminars</span></a></li>

                      </ul>
                    </li>
                    <li class="has-submenu">
                      <a>Consulting</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
  <li><a href="../../consulting/index.php">Overview</a></li>
  
  <li><a href="../../consulting/benchmarking/index.php">Benchmarking</a></li>
  
  <li><a href="../../consulting/expert-design-review/index.php">Expert Design Review</a></li>
  
  <li><a href="../../consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
  
  <li><a href="../../consulting/usability-testing/index.php">Usability Testing </a></li>
  
  <li><a href="../../consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
  
  <li><a href="../../in-house-training/index.php">In-House Training</a></li>
</ul>

                    </li>

                    <li><a href="../../reports/index.php">Reports</a></li>
                    <li><a href="../index.php">Articles</a></li>
                    <li class="has-submenu"><a>About NN/G</a><i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../about/index.php">Overview</a></li>
                        <li><a href="../../people/index.php">People</a></li>
                        <li><a href="../../about/why-nng/index.php">Why NN/g?</a></li>
                        <li><a href="../../about/contact/index.php">Contact</a></li>
                        <li><a href="../../news/index.php">News</a></li>
                        <li><a href="../../about/history/index.php">History</a></li>
                        <li><a href="../../books/index.php">Books</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                </div>

                <hr class="header-divider show-for-small-only"/>


            </div>
       </div>

        <div id="hideNav">            <nav class="primary-nav small-12 column">
                <ul>
                    <li><a class="navitem" id="nav-home" href="../../index.php">Home</a></li>
                    <li><a class="navitem" id="nav-training" href="../../training/index.php">Training</a></li>
                    <li><a class="navitem" id="nav-consulting" href="../../consulting/index.php">Consulting</a></li>
                    <li><a class="navitem" id="nav-reports" href="../../reports/index.php">Reports</a></li>
                    <li><a class="navitem" id="nav-articles" href="../index.php">Articles</a></li>
                    <li><a class="navitem" id="nav-about" href="../../about/index.php">About NN/G</a></li>
                </ul>
            </nav>
            <nav class="secondary-nav small-12 medium-12 large-12 columns">
                
                
            </nav>
        </div>
    </header>
    <div id="mainContent" class="l-content">
    
  

  

  <div class="row">
    
      
      <section class="small-12 medium-3 columns collapsable-content l-navrail articles-left-column">
        <h1 class="show-for-small-down collapse-link mobile-browse-header">Browse by Topic and Author</h1>
          <div class="row collapsable">

            
            <div class="small-6 medium-12 columns">
              <section id="article-popular-topics" style="display: none;">
                
                <h1>Topics</h1>
                <ul id="article-popular-topics">
                    
                    <li>
                        
                        <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/intranets/all/index.php">Intranets</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/user-testing/all/index.php">User Testing</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                        
                    </li>
                    
                    <li><a href="#" id="article-see-all-topics"><strong>See all topics&hellip;</strong></a></li>
                </ul>
                
              </section>
              <section id="article-all-topics" style="display: none;">
                
                <h1>
                  <span class="show-for-medium-up">All Article Topics</span>
                  <span class="show-for-small-down">All Topics</span>
                  (<a href="#" id="article-hide-all-topics">hide</a>)
                </h1>
                <ul>
                  
                  <li>
                    
                    <a href="../../topic/accessibility/all/index.php">Accessibility</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/agile/all/index.php">Agile</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/analytics-and-metrics/all/index.php">Analytics &amp; Metrics</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/applications/all/index.php">Application Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/b2b-websites/all/index.php">B2B Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/behavior-patterns/all/index.php">Behavior Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/branding/all/index.php">Branding</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/content-strategy/all/index.php">Content Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/corporate-websites/all/index.php">Corporate Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/design-patterns/all/index.php">Design Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-design-process/all/index.php">Design Process</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/email/all/index.php">Email</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/eyetracking/all/index.php">Eyetracking</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/heuristic-evaluation/all/index.php">Heuristic Evaluation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/human-computer-interaction/all/index.php">Human Computer Interaction</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-ideation/all/index.php">Ideation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/information-architecture/all/index.php">Information Architecture</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/interaction-design/all/index.php">Interaction Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/international-users/all/index.php">International Users</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/intranets/all/index.php">Intranets</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-management/all/index.php">Management</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/navigation/all/index.php">Navigation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/non-profit-websites/all/index.php">Non-Profit Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/personas/all/index.php">Personas</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/persuasive-design/all/index.php">Persuasive Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/prototyping/all/index.php">Prototyping</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/psychology-and-ux/all/index.php">Psychology and UX</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/research-methods/all/index.php">Research Methods</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/search/all/index.php">Search</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/social-media/all/index.php">Social Media</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/strategy/all/index.php">Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/user-testing/all/index.php">User Testing</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-teams/all/index.php">UX Teams</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/visual-design/all/index.php">Visual Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/writing-web/all/index.php">Writing for the Web</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/young-users/all/index.php">Young Users</a>
                    
                  </li>
                  
                </ul>
                
              </section>
            </div>
            <script>
              /* Determine which topics list will be displayed: the full or the
               * partial.  We are inlining this close to the list itself to
               * prevent rendering delays on the client
               */
              var topic_list = $.cookie("article-topics");

              var popular_topics = $('#article-popular-topics');
              var all_topics = $('#article-all-topics');

              if(topic_list == 1) {
                  popular_topics.hide();
                  all_topics.show();
              } else {
                  popular_topics.show();
                  all_topics.hide();
              }
            </script>
            

            
            <div class="small-6 medium-12 columns">
              
              <section id="article-initial-authors">
                  <h1>Author</h1>
                  <ul>
                    
                    <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                    
                    <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                    
                    <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                    
                    <li><a href="#" onClick="$('#article-initial-authors').hide(); $('#article-all-authors').show();"><strong>See all authors&hellip;</strong></a></li>
                  </ul>
              </section>
              <section id="article-all-authors" style="display: none;">
                <h1>
                  All Authors
                  (<a href="#" id="article-hide-all-authors">hide</a>)
                </h1>
                <ul>
                  
                  <li><a href="../author/raluca-budiu/index.php">Raluca Budiu</a></li>
                  
                  <li><a href="../author/patty-caya/index.php">Patty Caya</a></li>
                  
                  <li><a href="../author/susan-farrell/index.php">Susan Farrell</a></li>
                  
                  <li><a href="../author/therese-fessenden/index.php">Therese Fessenden</a></li>
                  
                  <li><a href="../author/kim-flaherty/index.php">Kim Flaherty</a></li>
                  
                  <li><a href="../author/sarah-gibbons/index.php">Sarah Gibbons</a></li>
                  
                  <li><a href="../author/aurora-harley/index.php">Aurora Harley</a></li>
                  
                  <li><a href="../author/kate-kaplan/index.php">Kate Kaplan</a></li>
                  
                  <li><a href="../author/page-laubheimer/index.php">Page Laubheimer</a></li>
                  
                  <li><a href="../author/angela-li/index.php">Angie Li</a></li>
                  
                  <li><a href="../author/hoa-loranger/index.php">Hoa Loranger</a></li>
                  
                  <li><a href="../author/kate-meyer/index.php">Kate Meyer</a></li>
                  
                  <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                  
                  <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                  
                  <li><a href="../author/kara-pernice/index.php">Kara Pernice</a></li>
                  
                  <li><a href="../author/christian-rohrer/index.php">Christian Rohrer</a></li>
                  
                  <li><a href="../author/amy-schade/index.php">Amy Schade</a></li>
                  
                  <li><a href="../author/katie-sherwin/index.php">Katie Sherwin</a></li>
                  
                  <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                  
                  <li><a href="../author/kathryn-whitenton/index.php">Kathryn Whitenton</a></li>
                  
                </ul>
              </section>
              
            </div>
            
          </div>

          
          <section class="hide-for-small-down">
            
              
              <h1>Recent Articles</h1>
              
              <ul>
                  
                  <li><a href="../seamless-cross-channel/index.php">Seamlessness in the Omnichannel User Experience</a></li>
                  
                  <li><a href="../brand-intention-interpretation/index.php">Experience Design: Bridging Brand Intention and Brand Interpretation</a></li>
                  
                  <li><a href="../eyetracking-tasks-efficient-scanning/index.php">Scanning Patterns on the Web Are Optimized for the Current Task</a></li>
                  
                  <li><a href="../flat-design-best-practices/index.php">Flat-Design Best Practices</a></li>
                  
                  <li><a href="../ux-user-stories/index.php">Accounting for UX Work with User Stories in Agile Projects</a></li>
                  
                  <li><a href="../index.php"><strong>See all articles&hellip;</strong></a></li>
              </ul>
              
            
          </section>
          

          
          
          <section class="hide-for-small-down">
            <h1>Popular Articles</h1>
            
            <ul>
                
                <li><a href="../usability-101-introduction-to-usability/index.php">Usability 101: Introduction to Usability</a></li>
                
                <li><a href="../top-10-mistakes-web-design/index.php">Top 10 Mistakes in Web Design</a></li>
                
                <li><a href="../how-users-read-on-the-web/index.php">How Users Read on the Web</a></li>
                
                <li><a href="../f-shaped-pattern-reading-web-content/index.php">F-Shaped Pattern For Reading Web Content</a></li>
                
                <li><a href="../ten-usability-heuristics/index.php">10 Usability Heuristics for User Interface Design</a></li>
                
                <li><a href="../intranet-design-2016/index.php">10 Best Intranets of 2016</a></li>
                
                <li><a href="../which-ux-research-methods/index.php">When to Use Which User-Experience Research Methods</a></li>
                
                <li><a href="../response-times-3-important-limits/index.php">Response Times: The 3 Important Limits</a></li>
                
                <li><a href="../why-you-only-need-to-test-with-5-users/index.php">Why You Only Need to Test with 5 Users</a></li>
                
                <li><a href="../page-fold-manifesto/index.php">The Fold Manifesto: Why the Page Fold Still Matters</a></li>
                
            </ul>
          </section>
          
          

          
            <section class="hide-for-small-down">
              
            </section>
          

      </section>
      

      
        <div class="small-12 medium-9 columns content-body">
          




<article class="l-subsection alertbox-article-content article-detail-bl">
  
  <header>
    <div class="l-subsection">
      <h1>Readers&#39; Comments on 3D User Interfaces</h1>
      <div class="author-meta">by
        
          
            
            <a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a>
            
          
        on  November 15, 1998
        
      </div>

      

      
      <div class="topic-links">
        Topics:
        
          <ul>

</ul>
        
      </div>
      
    </div>
  </header>
  

  
  
  

  
  <section id="article-body"><p>
 <em>
  Sidebar to
  <a href="../../people/jakob-nielsen/index.php" title="Author biography">
   Jakob Nielsen
  </a>
  &#39;s column on why
  <a href="../2d-is-better-than-3d/index.php">
   2D is better than 3D
  </a>
  .
 </em>
</p>
<p>
 I have received some interesting user comments on my November Alertbox on the problems with 3D interfaces for abstract information spaces.
</p>
<h2>
 2D Had Its Own Problems
</h2>
<p>
 <em>
  <a class="out" href="http://www.pobox.com/~kragen" title="Personal home page">
   Kragen Sitaker
  </a>
  &lt;
  <a href="mailto:kragen@pobox.com">
   kragen@pobox.com
  </a>
  &gt; of Arden writes:
 </em>
</p>
<blockquote>
 I agree with you that people sometimes try to force their clients to use 3D stuff because it&#39;s cool, not because their clients find it useful. Much of the enabling technology for good 3D interfaces is not there yet. And sometimes people try to force inappropriate metaphors on their users --- although that didn&#39;t begin with 3D stuff, certainly!
 <p>
  I have beefs with a few of the things you said, though:
 </p>
 <ul>
  <li>
   eyes on the sides of our heads: our eyes are in front of our heads so our stereoscopic vision covers a large proportion of our visual field, making it possible to determine depth. Monkeys that swing through the trees have the same anatomy in this department.
  </li>
  <li>
   a lot of your arguments incline me to believe that the problem is not that 3D is inherently worse than 2D, but that we don&#39;t have good ways of using 3D interfaces yet.
   <p>
    Most of the same arguments would have applied to graphical user interfaces in 1970 or so:
   </p>
   <ul>
    <li>
     just compare the number of people who write memos for a living to the number of people who draw pictures for a living;
    </li>
    <li>
     evolution optimized humans for abstraction, not concrete interfaces;
    </li>
    <li>
     it&#39;s hard to control a GUI with a keyboard, and mice are expensive;
    </li>
    <li>
     users need to pay attention to the GUI, and it distracts them from their task (actually, my wife prefers full-screen text-mode in text-mode apps for just this reason);
    </li>
    <li>
     poor terminal resolution means you can&#39;t see much if you have dialog boxes (although I don&#39;t think anyone tried to put a GUI on a glass TTY until the 1980s);
    </li>
    <li>
     most abstract information doesn&#39;t render well graphically, and is better represented as text;
    </li>
    <li>
     it requires big, bulky, unstable software;
    </li>
    <li>
     (well, the confusion argument doesn&#39;t apply)
    </li>
   </ul>
   (In fact, a remarkable number of these
   <em>
    still
   </em>
   apply. Yet people often prefer GUIs.)
  </li>
  <li>
   On
   <cite>
    Doom
   </cite>
   : you stumbled into a historical cow-patty here, I&#39;m afraid.
   <cite>
    Doom
   </cite>
   &#39;s predecessor,
   <cite>
    Wolfenstein 3D
   </cite>
   , was a relatively straightforward three-dimensionalization of a popular 2D game,
   <cite>
    Castle Wolfenstein
   </cite>
   . The interface was essentially the same. I&#39;ve spent a few hours playing each of these games, and I didn&#39;t find
   <cite>
    Wolf3D
   </cite>
   any harder to use --- although there
   <em>
    is
   </em>
   the behind-you factor, as you mentioned.
  </li>
  <li>
   On what kind of data wants 3D presentation: linegraph data (i.e. nodes connected by edges) is often hard to grasp in 2D, and sometimes easier to grasp in 3D. There&#39;s quite a bit of data whose most natural representation is a linegraph.
  </li>
 </ul>
 <p>
  I have high hopes for 3D interfaces for abstract information navigation in the future.
 </p>
</blockquote>
<p>
 <em>
  Jakob&#39;s reply:
 </em>
 I like your list of similar arguments against 2D GUIs: in fact, you have probably listed the reasons IBM didn&#39;t use Doug Engelbart&#39;s inventions on their mainframe terminals in the 1970s.
</p>
<p>
 For anybody doing a Web design
 <em>
  right now
 </em>
 it does not matter much whether the arguments against 3D are temporary (as were most of the arguments against GUIs) or fundamental. In either case, they surely apply today and serve as a warning against using 3D except in the cases where the third dimension adds substantial value to the user&#39;s task.
</p>
<p>
 I tend to believe that many of the arguments against 3D interfaces
 <em>
  are
 </em>
 fundamental in nature and will remain true in the future. But I would more than happy to be proven wrong since I find the current generation of Macintosh-clone designs supremely inadequate to support the
 <a class="old" href="../the-internet-desktop/index.php" title="Alertbox March 1996: requirements for an Internet Desktop">
  Internet Desktop
 </a>
 vision. We need something better, but we need this improvement so soon that we cannot wait for 3D to potentially become usable. Thus, I recommend more focus on 2D designs that integrate better semantics and richer attributes.
</p>
<h2>
 Introspective vs. Extraspective 3D
</h2>
<p>
 <em>
  Benjamin P. Samuel from Drexel University writes:
 </em>
</p>
<blockquote>
 I may be nitpicking, but only the most primitive GUIs (usually terminal based GUIs) are strictly 2D. The moment you introduce a stacking order you introduce a third dimension. I bring this up because I want to distinguish between two types of 3D manipulation.
 <p>
  There&#39;s the
  <strong>
   introspective
  </strong>
  (I hope I&#39;m using a real word) type where you&#39;re &quot;swinging through trees.&quot; In other words, you&#39;re
  <strong>
   controlling your body and point of view
  </strong>
  . This requires some kind of immersive experience because the mind wants the whole body to take over. If you&#39;ve ever watched people playing 1st person games like DOOM or Marathon, you&#39;ll notice they sometimes flinch when a missile flies close by their head.
 </p>
 <p>
  The other is the
  <strong>
   extraspective
  </strong>
  type where you&#39;re
  <strong>
   removed from the objects
  </strong>
  . It&#39;s pseduo-3D because objects are treated as flat and largely static. This is the basis for the traditional GUI, essentially you sit at a desk and objects are laid out. Certain objects lie on top of each other, some are contained within. This requires an entirely different type of response. Someone playing a first-person game doesn&#39;t get bothered by blinking things, whereas if I&#39;m in my office and the message-waiting indicator is blinking on my telephone, I notice it instantly. The same is true for the accursed &lt;BLINK&gt; tag.
 </p>
 <p>
  That said, I think you&#39;re right that from an interface standpoint, the introspective form of 3D is not useful for navigating. However, your examples of when to use 3D do show that an introspective view is useful for illustrating a task such as a surgeon or engineer&#39;s.
 </p>
 <p>
  My point, then, is that one shouldn&#39;t pretend one is working exclusively in 2D. Rather, when the designer needs to make the transition between the two types as seamless (natural) as possible. Any object can be found lying around on the ground, we need to be able to pick it up and take it with us.
 </p>
</blockquote>
<h2>
 3D Is Improving
</h2>
<p>
 <em>
  Bob Jacobson of Bluefire! Consulting (bluefire
  <code>
   @
  </code>
  well.com) writes:
 </em>
</p>
<blockquote>
 I&#39;m as stern a critic of poorly-done 3D interface as you are, but your last column flushes the baby down the drain with the bathwater. Condemning today&#39;s 3D hackjobs is a lot like criticizing computer graphics, when desktop publishing was in vogue, for generating prolific banalaity. Every period of innovation begins with widespread experimentation; a few individuals learn from invention and advance the state-of-the-art. We&#39;re getting there.
 <p>
  But here are my nitpicks with your conclusions.
 </p>
 <ol>
  <li>
   &quot;3D&quot; environment is commonly equated with a virtual world, though it is not a virtual world. (You perpetuate this usage.) 3D is only one element of a virtual world. Every virtual world has one aspect, an individual&#39;s internalized mental-map; and may have a second, a 3D computer-map created by workstations and projectors. When they converge, these create a sense of &quot;presence.&quot; An immersive sound field, for example, can effectively synergize with a 3D visual presentation to generate a profound 360-degree experience.
  </li>
  <li>
   Experiencing computer-generated virtual worlds does not require &quot;weird headgear.&quot; It&#39;s been a long time since anyone in the profession argued for it. More popular are immersion environments. Immersion desktops aren&#39;t far off and even in the current situation, more designers are learning to write for them. See
   <a class="out" href="http://www.fakespace.com/" title="Company website">
    Fakespace
   </a>
   for some hints as to what lies ahead.
   <br />
   (Mark Bolas, CEO of Fakespace, is one of the most talented designers working in 3D. His early [1988] wireframe evocation of an elevator remains the most effective graphic I&#39;ve ever experienced...and my most acute memory of movement through space prior to flying over the &quot;Giza Plateau, 2000 BC&quot; in a virtual world created by Worldesign Inc., my former employer.)
  </li>
  <li>
   Almost all useful data have spatial dimensions. Even very abstract information, to have meaning for human beings, must be spatially referenced (where is this?) and often, geospatially referenced (where on earth is this?). Multidimensional displays (employing GIS/GPS) are the best way to display these relationships and far more effective than 2D maps. Most adults cannot read 2D maps. Perhaps they at least can cognitively traverse 3D maps.
  </li>
 </ol>
 Finally, the computer-generated virtual world absolutely registers better with the internalized virtual world (a la Peter Senge). Experiments by architect Daniel Henry conducted at the HIT Lab in Seattle have demonstrated this fact. So, even if we can&#39;t achieve perfection today, it&#39;s a goal worth striving toward -- not surrendering to the inadequacies of 2D.
 <p>
  Thanks, as always, Jakob, for a provocative and well-thought-through column.
 </p>
</blockquote>
<p>
 <em>
  Jakob&#39;s reply:
 </em>
 I agree completely with the utility of
 <strong>
  augmented reality
 </strong>
 . One of my favorite future interfaces is to say
 <em>
  &quot;Computer, where are my car keys?&quot;
 </em>
 and have the system reply by shining a spotlight on the keys. Immersing interaction in the physical environment is a sure way to connect closely with people&#39;s basic existence and everyday needs. But we need even more special equipment to do this.
</p>
<p>
 It&#39;s a good point that today&#39;s bad 3D designs may not be indicative of the higher-quality designs to come once we learn when and how to best use 3D in user interfaces. On the other hand, I have been to countless SIGGRAPH conferences and rarely seen any 3D designs that would offer measurable improvements in usability beyond the &quot;coolness&quot; factor (except for the examples mentioned in my column). There is certainly hope for good 3D interfaces to come and we need to get more examples of 3D designs that have been through usability engineering and iterative design to refine the interaction to the point where it becomes truly useful.
</p>
<h2>
 Artists Simplify the World Into 2D
</h2>
<p>
 <em>
  George Olsen, Design Director/Web Architect at 2-Lane Media, writes:
 </em>
</p>
<blockquote>
 I too am skeptical of 3D interfaces, having sat through five years of virtual worlds demos, but I agree with Kragen Sitaker it&#39;s possible there are better interfaces down the road (and obviously not applicable in the here and now). For example, to take your example of Doom, a real-time 3D POV did provide a different experience to this sort of gaming -- and arguably one that&#39;s well-suited for the intended goals of Doom. At the time, I was part of a group discussing the potential ideas for interactive entertainment (sort of like when Eisenstein and friends discussed what they&#39;d do if they ever had enough filmstock to make movies) and I remember that this sort of POV added dimensions to the experience that hadn&#39;t been anticipated.
 <p>
  However, I do think there are fundamental aspects to 3D that limit its usage. For centuries humans have
  <strong>
   rendered 3D natural environments into simplified 2D versions
  </strong>
  . Why, not because they lacked 3D modelers (aka sculptors in the analog world). Instead it was to simplify the world around them to make it more understandable. Unfortunately, too many examples
  <strong>
   3D interfaces today seem to want to make the map into the territory
  </strong>
  -- on a 1:1 scale....
 </p>
 <p>
  There are definitely cases where this is appropriate as you&#39;ve pointed out, but the overriding reason for 3D is that it&#39;s better than reality -- not that it resembles reality.
 </p>
 <p>
  Incidentally, coming from a graphic design background, it&#39;s disappointing to see so few HCI people talking to designers, who sometimes do more than make things pretty. After all, we do have at least
  <strong>
   five centuries of beta testing experience
  </strong>
  .
 </p>
</blockquote>
<p>
 <em>
  Jakob&#39;s reply:
 </em>
 Your analogy with painting versus sculpture is right on target: 3D may be a closer match to reality, but 2D usually offers more useful simplifications. And we
 <em>
  need
 </em>
 simplicity in user interfaces: a simpler representation can often scale up better to handle larger amounts of complexity.
</p>
<p>
 I also agree that interaction experts and design experts need to talk more. One of my personal favorites is Scott McCloud&#39;s book
 <cite>
  <a class="out" href="http://www.amazon.com/exec/obidos/ISBN=006097625X/useitcomusableinA/" title="Book page at Amazon.com">
   Understanding Comics
  </a>
 </cite>
 which is particularly helpful because comics have so much in common with user interfaces. But we need an entire series of
 <cite>
  Understanding
  <code>
   X
  </code>
 </cite>
 for all values of
 <code>
  X=artform
 </code>
 (speaking like a true engineer here :-)
</p>
<h2>
 Does Evolution Predetermine Interface Styles?
</h2>
<p>
 <em>
  <a class="out" href="http://www.eramp.com/" title="Consulting company homepage">
   Bill Shackleton
  </a>
  writes:
 </em>
</p>
<blockquote>
 I must respectfully disagree with you when you say: &quot;Evolution optimized homo sapiens for wandering the savannah - moving around a plane - and not swinging through the trees.&quot; Yes we have been optimized somewhat for wandering the plains and therefore stand up straight and use our hind legs for transportation, but we also have opposing thumbs (for gripping tree branches), binocular vision to help us judge the distance to the branch we are leaping for, and colour vision to help us distinguish the fruit in the trees from the rest of the foliage.
 <p>
  Although I do not wish to live back in the trees or on the savannah, I must say that I get a little morose at the thought of human beings cubicled - Dilbert like - 30 stories above some congested core of steel, plastic, and concrete - using that wonderfully evolved opposing thumb for smacking down on a space bar every 5 or 8 letters or so. Due to another gift from evolution - extreme adaptability - I happen to be a fairly quick typist. I think that I could - and probably would prefer to eventually adapt to a more 3D technological space.
 </p>
 <p>
  To take an example, imagine a person who was blind navigating a 3D aural/information space!
 </p>
</blockquote>
<p>
 <em>
  Jakob&#39;s reply:
 </em>
 I tend to think that stereo vision is somewhat over-rated: try to close one eye and note how little changes. I believe much more in your closing remark about a 3D space for blind users: spatial location and gestural interaction become important once we abandon the screen (either because the user can&#39;t see or because we want a UI for cases where it&#39;s better not to be tied to a monitor). Reach for your data!
</p>
<p>
 <em>
  <a class="out" href="http://cs.anu.edu.au/people/Hugh.Fisher/" title="Personal home 
page at The Australian National University">
   Hugh Fisher
  </a>
  from The Australian National University writes:
 </em>
</p>
<blockquote>
 First, we&#39;re fairly well optimised for swinging around in trees, which is why we can rotate our arms through 360 degrees vertically and hang from things. Baboons and other primates which have spent much more time on the savannah do not have nearly as much reach and flexibility. Swinging around in the trees is also where we get our superb binocular color vision, which only evolves for specialist carnivores like lions and arboreal primates like our ancestors.
 <p>
  More to the point, so what? I&#39;m not writing this email from the savannah or the treetops, but from a desk. We didn&#39;t evolve in an environment with computers, so are unlikely to be preadapted for any particular user interface style. (Does Windows 95 outsell the Mac because the designers tapped into some genetic racial memory?)
 </p>
 <p>
  And second, unless the prices in the US are considerably different than they are here in Australia, I believe the number of people driving cars rather than helicopters is due to the two orders of magnitude price difference rather than any innate evolutionary tendency. Since the accident rate per kilometre/passenger for commercial aviation is much lower than it is for cars, one could even argue that we are in fact better at navigating in 3D than 2D.
 </p>
 <p>
  I suspect you&#39;re going to get a flood of similar emails, arguing about that particular paragraph rather than the article as a whole. I suggest you delete that paragraph, so people will be more inclined to think about the rest of it.
 </p>
</blockquote>
<p>
 <em>
  Jakob&#39;s reply:
 </em>
 You are probably right that humans are decently capable of swinging through trees. A better analogy may have been birds or fish: true 3D creatures that navigate a space with full up-down navigational freedom.
</p>
<p>
 I do maintain that we are more capable of moving around a flat surface and that we spend most of our time doing just that. It is true that people are adaptable and can learn to fly helicopers and airplanes, but the reason for the lower accident rate is more likely to be the fact that pilots are extremely highly trained, have been selected from a small group of elite &quot;right stuff&quot; candidates, and are continually supported by air traffic control. If the same selectivity and support environment applied to audomobile drivers, you can be sure that we would have very few road casualties. But imagine not being able to turn into your driveway without clearing it with the tower first.
</p>
<p>
 Returning to the main point: Evolution does favor certain interface styles over others. For example, you can&#39;t use a mouse for fine art because the arm muscles do not allow as detailed movements as the fingers. That&#39;s why all artists use a drawing tablet. Similarly, fingers only move so fast, so there are limits to the amount of text people can type, meaning that interface styles become infeasible if they require too much typing. (Obviously, some people are
 <em>
  really
 </em>
 fast typists, and such users have been observed to use different interaction techniques than normal users: for example, they often prefer to delete an entire screen and retype everything rather than having to spend time trying to find out which element on the screen contains an error.)
</p>
</section>
  

  
  <section>
    
        
    
  </section>
  
  
  
  
  

  


  <section>
    <br />
    <p class="share"><b>Share <span class="hide-for-small-down">this article</span>:</b> <a href="https://twitter.com/intent/tweet?url=http://www.nngroup.com/articles/readers-comments-on-3d-user-interfaces/&amp;text=Readers'%20Comments%20on%203D%20User%20Interfaces&amp;via=nngroup">Twitter</a> | <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=http://www.nngroup.com/articles/readers-comments-on-3d-user-interfaces/&amp;title=Readers'%20Comments%20on%203D%20User%20Interfaces&amp;source=Nielsen%20Norman%20Group">LinkedIn</a> | <a href="https://plus.google.com/share?url=http://www.nngroup.com/articles/readers-comments-on-3d-user-interfaces/">Google+</a> | <a href="mailto:?subject=NN/g Article: Readers&#39; Comments on 3D User Interfaces&amp;body=http://www.nngroup.com/articles/readers-comments-on-3d-user-interfaces/">Email</a></p>
    <hr />
  </section>

  <footer class="l-subsection">
    
    
    

    <div class="row show-for-small-down related-content">
      
      
      

      
      
      

      
      
      

      
      
      

      
      
      
    </div>

    <div class="hide-for-small-down row related-content">
      <div class="medium-6 columns">
        
        
        

        

      </div>
      <div class="medium-6 columns">
        
        
      </div>
    </div>
  </footer>
</article>

        </div>

      

    
  </div>
  <div class="row">
    
      <div class="column small-12 show-for-small-down l-navrail announcements">
        
          
        
      </div>
    
    
  </div>

    </div>
    




<footer id="page-footer">
    <div class="row hide-for-print">
        <div class="column small-12 medium-6">
            <section class="footer-popular">
                
                <h1>Popular Topics</h1>
                <div style="float: left;  width: 15em; display: block;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/applications/index.php">Application Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/e-commerce/index.php">E-Commerce Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/eyetracking/index.php">Eyetracking </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/information-architecture/index.php">Information Architecture </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/intranets/index.php">Intranet Design </a></li>
	<li><a href="../../topic/interaction-design/index.php">Interaction Design </a></li>
</ul>
</div>

<div style="float: left;  width: 15em; display: block; list-style: none;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/mobile-and-tablet-design/index.php">Mobile and Tablet Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/credibility-persuasion/index.php">Persuasive Design</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/research-methods/index.php">Research Methods</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/user-testing/index.php">User Testing </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/web-usability/index.php">Web Usability </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/writing-web/index.php">Web Writing &amp; Content Strategy </a></li>
</ul>
</div>

<p>&nbsp;</p>

                
            </section>
        </div>

        <div class="column small-12 medium-6">
            <div class="row">
                <div class="column small-6">
                    <section class="footer-about">
                        <h1>Follow Us</h1>
                        <ul>
	<li><a href="../subscribe/index.php">Subscribe to our weekly UX newsletter</a>,&nbsp;Jakob Nielsen&#39;s Alertbox</li>
	<li>Visit Don Norman&#39;s website, <a href="http://www.jnd.org/"> www.jnd.org </a></li>
	<li>Visit Bruce Tognazzini&#39;s website, <a href="http://www.asktog.com/"> www.asktog.com </a></li>
	<li style="padding-top: 6px;"><a class="twitter-follow-button" data-show-count="false" data-size="large" href="https://twitter.com/nngroup"><img alt="Follow @nngroup on Twitter" height="20" src="https://media.nngroup.com/media/editor/2016/12/30/twitter-follow-button.png" style="width: 117px; height: 20px;" width="117" /></a></li>
	<li style="padding-top: 6px;"><img alt="" height="14" src="https://media.nngroup.com/media/editor/2016/07/26/feed-icon-14x14.png" style="vertical-align:text-top;width:14px;margin-right:.25em;" width="14" /><a href="../../feed/rss/index.php">Subscribe to our RSS feed</a></li>
</ul>

                    </section>
                </div>

                <div class="column small-6">
                    <section class="footer-about right-column">
                        <h1>About
</h1>
                        <ul class="no-bullet">
	<li><a href="../../about/contact/index.php">Contact information</a></li>
	<li><a href="../../about/index.php">About Us</a></li>
	<li><a href="../../about/why-nng/index.php">Why NN/g? </a></li>
	<li><a href="../../return-policy/index.php">Return Policy</a></li>
</ul>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="copyright column small-12">
            <p><a href="../../copyright-and-reprint-info/index.php">Copyright</a> &copy; 1998-2017 Nielsen Norman Group, All Rights Reserved.</p>
        </div>
    </div>
</footer>


    


        <script src="https://media.nngroup.com/static/vendor/fastclick/js/fastclick.min.js"></script>
        <script src="https://media.nngroup.com/static/vendor/foundation/js/foundation.min.js"></script>
        <script src="http://media.nngroup.com/static/js/app.js"></script>
        <script src="http://media.nngroup.com/static/js/scroll-to-fixed.js"></script><script>var externalLinks = document.querySelectorAll('a[href^="http"]');for (var i = externalLinks.length-1; i >= 0; i--) {externalLinks[i].addEventListener("click", function(event) { alert("External links are disabled for this test."); event.preventDefault(); }, false);}var mailLinks = document.querySelectorAll('a[href^="mailto"]');for (var i = mailLinks.length-1; i >= 0; i--) {mailLinks[i].addEventListener("click", function(event) { alert("Email links are disabled for this test."); event.preventDefault(); }, false);}var externalForms = document.querySelectorAll('form');for (var i = externalForms.length-1; i >= 0; i--) {externalForms[i].addEventListener("submit", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false); externalForms[i].addEventListener("click", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false);}</script>

        <script>
            
            $(function() {
                $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
                $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
            });
        </script>

        
<script src="https://media.nngroup.com/static/js/articles.js"></script>


        
        <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1072699960;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072699960/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
    </body>

<!-- Mirrored from www.nngroup.com/articles/readers-comments-on-3d-user-interfaces/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:16:06 GMT -->
</html>
