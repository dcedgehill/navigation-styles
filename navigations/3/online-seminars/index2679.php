<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->





    
<!-- Mirrored from www.nngroup.com/online-seminars/?page=1 by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:10:35 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1026.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"licenseKey":"1a30ef9931","errorBeacon":"bam.nr-data.net","applicationID":"2048992","transactionName":"NFxSMRBUX0EDUkIPDA0WdhAMVkVbDV8ZCQ0PUF4APUZUXwtfVxQQTU9ZABVGC30MXV8IBjBcXQwMVENBKl5bA00EXEQ=","beacon":"bam.nr-data.net","queueTime":2,"applicationTime":180,"agent":""}</script>
        <title>Online Seminars: 1-hour UX Learning | Nielsen Norman Group</title><meta property="og:title" content="Online Seminars: 1-hour UX Learning | Nielsen Norman Group" />
  
        
        <meta name="description" content="Experts share insights on specialized UX topics in a 1-hour online format. Learn best practices, gain practical tips and find solutions to your user experience challenges on your own at your desk or in a conference room with your team. ">
        <meta property="og:description" content="Experts share insights on specialized UX topics in a 1-hour online format. Learn best practices, gain practical tips and find solutions to your user experience challenges on your own at your desk or in a conference room with your team. " />
        
  
        
	
        
        <meta name="keywords" content="online learning, webinar, online seminars, UX, usability">
        

        

        <meta name="robots" content="noarchive">
        <meta name="viewport" content="width=device-width">

        <link rel="search" type="application/opensearchdescription+xml" title="NN/g Search" href="../opensearch.xml" />
        <link rel="shortcut icon" href="http://media.nngroup.com/static/img/favicon.ico" />

        <link rel="stylesheet" href="https://media.nngroup.com/static/vendor/font-awesome/css/font-awesome.min.css">

        
        <link rel="stylesheet" href="https://media.nngroup.com/static/css/7232a960f1df.css" type="text/css" />
        <link rel="stylesheet" href="http://fast.fonts.com/t/1.css?apiType=css&amp;projectid=c5846260-9def-46ec-9407-0f1349bd2d64" type="text/css">

        <script src="https://media.nngroup.com/static/vendor/modernizr/js/modernizr.min.js"></script>

        <script type="text/javascript" src="https://media.nngroup.com/static/js/3283be5def74.js"></script>
        

        

        
        
        <!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=229034,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('http://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->


<!-- Google Analytics Tracking Code for http://nngroup.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23132681-1', 'nngroup.com');
  ga('require', 'linkid', 'https://www.nngroup.com/online-seminars/linkid.js');
  ga('require', 'displayfeatures');

// Optimizely Universal Analytics Integration
 window.optimizely = window.optimizely || [];
 window.optimizely.push("activateUniversalAnalytics");

  ga('send', 'pageview');

</script>
<script>
/**
* Function to track a click on an outbound link in Google Analytics.
*/
function trackOutboundLink(label, url) {
    if(window.ga && ga.loaded) {
        ga('send', 'event', 'outbound', 'click', label, {'hitCallback': function(){document.location = url;}
        });
    } else {
        document.location = url;
    }
}
/**
* Function to track a click on a link in the article announcement area.
*/
$('#articleAnnouncement a').on('click', function(){
  ga('send', 'event', 'link', 'click', 'articleAnnouncement');
});
</script>

<script type="text/javascript">
 setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2421.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



<!-- Hotjar Tracking Code for http://nngroup.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:121605,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

        <script src="https://media.nngroup.com/static/js/analytics/track_outbound_link.js"></script>
        
        
        
    </head>
    <body class="location-training location-online-seminars online-seminars">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
    <header id="page-header">
        <div id="cart" class="shopping-cart mobile-cart show-for-small-down"></div>
        <div class="row">
            <div id="masthead" class="masthead small-12 medium-7 column">

                <!-- The button to toggles menu on small screen -->

                <h1><a href="../index.php"><img src="http://media.nngroup.com/static/img/logo-wordmark.png" width="74" height="30"> Nielsen <span class="norman">Norman</span> Group</a></h1>
                <p class="tagline show-for-medium-up">Evidence-Based User Experience Research, Training, and Consulting</p>

                <!-- The button to toggles form search on small screen -->


                <div style="clear: right;width:  100%;height: 1px;"></div><?php define('ROOT_PATH',$_SERVER["DOCUMENT_ROOT"] . '/apps/navigation_styles/navigations/3'); ?><?php include(ROOT_PATH)."/navigation.php"; ?>

                <!-- Drop down menu on small screen -->
                <div id="hideNav" class="show-for-small-down">
                  
                  <ul class="menu-content">
                    <li><a href="../index.php">Home</a></li>
                    <li class="has-submenu">
                      <a>Training</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../training/index.php">Overview</a></li>
                        
<li id="nav-ux-conference"><a href="../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="index.php"><span>Online Seminars</span></a></li>

                      </ul>
                    </li>
                    <li class="has-submenu">
                      <a>Consulting</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
  <li><a href="../consulting/index.php">Overview</a></li>
  
  <li><a href="../consulting/benchmarking/index.php">Benchmarking</a></li>
  
  <li><a href="../consulting/expert-design-review/index.php">Expert Design Review</a></li>
  
  <li><a href="../consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
  
  <li><a href="../consulting/usability-testing/index.php">Usability Testing </a></li>
  
  <li><a href="../consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
  
  <li><a href="../in-house-training/index.php">In-House Training</a></li>
</ul>

                    </li>

                    <li><a href="../reports/index.php">Reports</a></li>
                    <li><a href="../articles/index.php">Articles</a></li>
                    <li class="has-submenu"><a>About NN/G</a><i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../about/index.php">Overview</a></li>
                        <li><a href="../people/index.php">People</a></li>
                        <li><a href="../about/why-nng/index.php">Why NN/g?</a></li>
                        <li><a href="../about/contact/index.php">Contact</a></li>
                        <li><a href="../news/index.php">News</a></li>
                        <li><a href="../about/history/index.php">History</a></li>
                        <li><a href="../books/index.php">Books</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                </div>

                <hr class="header-divider show-for-small-only"/>


            </div>
       </div>

        <div id="hideNav">            <nav class="primary-nav small-12 column">
                <ul>
                    <li><a class="navitem" id="nav-home" href="../index.php">Home</a></li>
                    <li><a class="navitem" id="nav-training" href="../training/index.php">Training</a></li>
                    <li><a class="navitem" id="nav-consulting" href="../consulting/index.php">Consulting</a></li>
                    <li><a class="navitem" id="nav-reports" href="../reports/index.php">Reports</a></li>
                    <li><a class="navitem" id="nav-articles" href="../articles/index.php">Articles</a></li>
                    <li><a class="navitem" id="nav-about" href="../about/index.php">About NN/G</a></li>
                </ul>
            </nav>
            <nav class="secondary-nav small-12 medium-12 large-12 columns">
                
  <ul class="inline-list">
  
<li id="nav-ux-conference"><a href="../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="index.php"><span>Online Seminars</span></a></li>

</ul>


            </nav>
        </div>
    </header>
    <div id="mainContent" class="l-content">
    


<div class="row">
    <section class="l-section-intro eight columns">
		<header>
			<h1>Online Seminars</h1>
		</header>
    <p><p>Experts share UX insights in a 1-hour online format. Learn best practices, gain practical tips and find solutions to your UX challenges on your own at your desk or in a conference room with your team.&nbsp;</p>

<ul style="margin-left: 40px;">
	<li>Ask a question of the speaker in live events</li>
	<li>Live events are available as recorded events within 3 business days of the live event</li>
	<li>Recorded seminars are available when and where you need them, at your convenience</li>
	<li>Attendees of both live and recorded events receive access to the recording to watch it again</li>
</ul>

<p><a href="../articles/subscribe/index.php">Sign up for our weekly newsletter</a> to stay updated about future online seminars.&nbsp;</p>

<p>More information:&nbsp;<strong><a href="../online-seminars-faq/index.php">Online Seminars Frequently Asked Questions</a></strong></p>
</p>
    </section>
</div>

<div class="row list-view">
  
  <section class="small-12 columns l-subsection seminar-group">
    
      
        
        <h1>Upcoming Live Seminars</h1>
        <p>Ask questions in live events.</p>
        <hr />
      
      

<article class="row course-listing l-subsection online-seminar">
  <div class="small-12 medium-10 medium-push-2 columns l-subsection detailColumn">
    <header>
      <h2><a href="improving-intranet-content/index.php">Improving Intranet Content</a></h2>
      
        <strong>Featuring Intranet Design Annual Winners</strong>
      
    </header>
    <section>
      <p>
        
        Live event Tuesday, April 4, 2017
        <br />
        
        05:00 PM ET / 02:00 PM PT<br />
        
        
        $99
      </p>
    </section>

    <p>Creating strong intranet content is dependent on audits and assessments of current content, coordination with and support of content authors and managers, and ongoing maintenance and governance. Team members from 2 Intranet Design Annual winning organizations, Enbridge Inc. and Tourism New Zealand, will share their experiences, tips and lessons learned.&nbsp;</p>

  </div>

  <div class="hide-for-small-down medium-2 medium-pull-10 columns l-subsection speakerColumn">
    

<section class="course-instructor">

  
    
      
        
          
          <span class="instructor-name">Kerry Corcoran</span>

          
            
              <em class="instructor-title">Senior Digital Communications Advisor, Enbridge Inc.</em>
            
          
        
          
          <span class="instructor-name">Irina Winsley</span>

          
            
              <em class="instructor-title">Information Manager, Tourism New Zealand</em>
            
          
        
      
    
  

</section>

  </div>

</article>

    
      
      

<article class="row course-listing l-subsection online-seminar">
  <div class="small-12 medium-10 medium-push-2 columns l-subsection detailColumn">
    <header>
      <h2><a href="fundamental-flaw-flat-design/index.php">The Fundamental Flaw in Flat Design</a></h2>
      
    </header>
    <section>
      <p>
        
        Live event Wednesday, April 26, 2017
        <br />
        
        01:00 PM ET / 10:00 AM PT<br />
        
        
        $99
      </p>
    </section>

    <p>Flat design has a common, nasty side effect: It can make users feel uncertain and powerless. If you&rsquo;re considering adopting a minimalist or flat design aesthetic, learn how to keep the clean visual appeal but ditch the negative interaction consequences.</p>

  </div>

  <div class="hide-for-small-down medium-2 medium-pull-10 columns l-subsection speakerColumn">
    

<section class="course-instructor">

  
    
      
        
          
            <img class="hide-for-small-down" src="https://media.nngroup.com/media/people/photos/IMG_2009_copy-retouched-square-closer-800px.jpg.140x140_q95_autocrop_crop_upscale.jpg">
          
          <span class="instructor-name">Kate Meyer</span>

          
            
              <em class="instructor-title">User Experience Specialist, Nielsen Norman Group</em>
            
          
        
      
    
  

</section>

  </div>

</article>

    
      
      

<article class="row course-listing l-subsection online-seminar">
  <div class="small-12 medium-10 medium-push-2 columns l-subsection detailColumn">
    <header>
      <h2><a href="ux-big-data/index.php">UX for Big Data</a></h2>
      
        <strong>Creating Interfaces for Analyzing Complex Data </strong>
      
    </header>
    <section>
      <p>
        
        Live event Wednesday, May 10, 2017
        <br />
        
        02:00 PM ET / 11:00 AM PT<br />
        
        
        $99
      </p>
    </section>

    <p>Creating user interfaces around complex data? In this online seminar, we&rsquo;ll take a hands-on, step-by-step approach to designing a great user experience around big data, using a sample data set and visualization tools. We will walk through steps to create interfaces that support end users who need to choose what data to look at, how to interpret what they&rsquo;re seeing, and what to do about it.&nbsp;</p>

  </div>

  <div class="hide-for-small-down medium-2 medium-pull-10 columns l-subsection speakerColumn">
    

<section class="course-instructor">

  
    
      
        
          
          <span class="instructor-name">Valle Hansen</span>

          
            
              <em class="instructor-title">Sr. User Researcher, Expero Inc.</em>
            
          
        
          
          <span class="instructor-name">Chris LaCava</span>

          
            
              <em class="instructor-title">UX Design Lead, Expero Inc.</em>
            
          
        
      
    
  

</section>

  </div>

</article>

    
      
        
        
        </section>
        <section class="small-12 columns l-subsection seminar-group">
        
        <h1>Recorded Seminars</h1>
        <p>Training when and where you need it.</p>
        <hr />
      
      

<article class="row course-listing l-subsection online-seminar">
  <div class="small-12 medium-10 medium-push-2 columns l-subsection detailColumn">
    <header>
      <h2><a href="millennials-online/index.php">Millennials Online</a></h2>
      
        <strong>Facts and Fictions</strong>
      
    </header>
    <section>
      <p>
        
        Recorded March 2017<br/>
        
        $99
      </p>
    </section>

    <p>Trying to appeal to Millennial users? Many UX professionals make the mistake of basing their design decisions on popular misconceptions about this young user group. This research-based seminar separates the hard facts about the Millennial generation from the myths, equipping you to make data-driven decisions to appeal to your specific Millennial audience.&nbsp;</p>

  </div>

  <div class="hide-for-small-down medium-2 medium-pull-10 columns l-subsection speakerColumn">
    

<section class="course-instructor">

  
    
      
        
          
            <img class="hide-for-small-down" src="https://media.nngroup.com/media/people/photos/IMG_2009_copy-retouched-square-closer-800px.jpg.140x140_q95_autocrop_crop_upscale.jpg">
          
          <span class="instructor-name">Kate Meyer</span>

          
            
              <em class="instructor-title">User Experience Specialist, Nielsen Norman Group</em>
            
          
        
      
    
  

</section>

  </div>

</article>

    
      
      

<article class="row course-listing l-subsection online-seminar">
  <div class="small-12 medium-10 medium-push-2 columns l-subsection detailColumn">
    <header>
      <h2><a href="design-thinking-agile/index.php">Design Thinking in Agile</a></h2>
      
        <strong>Google Design Sprint vs. IBM Design Loop</strong>
      
    </header>
    <section>
      <p>
        
        Recorded February 2017<br/>
        
        $99
      </p>
    </section>

    <p>What happens when Design Thinking meets Agile? The smart people at Google Labs came up with Design Sprints and the smart people at IBM Design came up with Design Loop. Two different ways of applying Design Thinking to Agile, both with the goal of solving and testing design problems while keeping users&#39; needs in mind.</p>

  </div>

  <div class="hide-for-small-down medium-2 medium-pull-10 columns l-subsection speakerColumn">
    

<section class="course-instructor">

  
    
      
        
          
            <img class="hide-for-small-down" src="https://media.nngroup.com/media/people/photos/svt_suite_tie.jpeg.140x140_q95_autocrop_crop_upscale.jpg">
          
          <span class="instructor-name">Sean Van Tyne</span>

          
            
              <em class="instructor-title"> Principal, Van Tyne Group</em>
            
          
        
      
    
  

</section>

  </div>

</article>

    
      
      

<article class="row course-listing l-subsection online-seminar">
  <div class="small-12 medium-10 medium-push-2 columns l-subsection detailColumn">
    <header>
      <h2><a href="intranet-users-find/index.php">Helping Intranet Users Find What They Need</a></h2>
      
        <strong>Featuring 2017 Intranet Design Annual Winners</strong>
      
    </header>
    <section>
      <p>
        
        Recorded January 2017<br/>
        
        $99
      </p>
    </section>

    <p>With the depth and breadth of information available on intranets, how can we help users find the right information quickly and easily? Team members from IBM Corporation and Goodwill Industries International, 2 of our 2017 Intranet Design Annual winning organizations, will share their experiences, insights, and what they&#39;ve learned about organizing information for their audiences&#39; varying needs on their award-winning sites.&nbsp;</p>

  </div>

  <div class="hide-for-small-down medium-2 medium-pull-10 columns l-subsection speakerColumn">
    

<section class="course-instructor">

  
    
      
        
          
          <span class="instructor-name">Jason Blackwell, Ph.D.</span>

          
        
          
          <span class="instructor-name">Arlene McCrehan</span>

          
        
          
          <span class="instructor-name">Ed McFadden</span>

          
        
          
          <span class="instructor-name">Chirag Patel</span>

          
        
          
          <span class="instructor-name">Beth Perell</span>

          
        
      
    
  

</section>

  </div>

</article>

    
      
      

<article class="row course-listing l-subsection online-seminar">
  <div class="small-12 medium-10 medium-push-2 columns l-subsection detailColumn">
    <header>
      <h2><a href="optimizing-seo/index.php">Optimizing Your Site in a Constantly Changing SEO World</a></h2>
      
    </header>
    <section>
      <p>
        
        Recorded November 2016<br/>
        
        $99
      </p>
    </section>

    <p>Google&rsquo;s algorithm now factors in &lsquo;real-time&rsquo; considerations when determining what information to rank organically. Therefore it is as important as ever to understand the latest ranking factors and how to stay up-to-date on SEO best practices.</p>

<p>Get an update on recent algorithm changes and how to execute an SEO program that is prepared to deal with industry changes.</p>

  </div>

  <div class="hide-for-small-down medium-2 medium-pull-10 columns l-subsection speakerColumn">
    

<section class="course-instructor">

  
    
      
        
          
            <img class="hide-for-small-down" src="https://media.nngroup.com/media/people/photos/Bob_Cavilla.png.140x140_q95_autocrop_crop_upscale.png">
          
          <span class="instructor-name">Robert Cavilla</span>

          
            
              <em class="instructor-title">Vice President, BusinessOnline</em>
            
          
        
      
    
  

</section>

  </div>

</article>

    
      
      

<article class="row course-listing l-subsection online-seminar">
  <div class="small-12 medium-10 medium-push-2 columns l-subsection detailColumn">
    <header>
      <h2><a href="beyond-screens/index.php">Beyond Screens</a></h2>
      
        <strong>Designing for Augmented Reality and Emerging Technologies</strong>
      
    </header>
    <section>
      <p>
        
        Recorded November 2016<br/>
        
        $99
      </p>
    </section>

    <p><span style="line-height: 1.6;">Learn about the unique challenges designers face when creating user interfaces and user experiences for AR. This online seminar will walk you through the design and development process for AR technologies. You will discover successful design approaches and learn from the failures of typical design assumptions while discussing effective UI/UX tips you can use rightaway.</span></p>

  </div>

  <div class="hide-for-small-down medium-2 medium-pull-10 columns l-subsection speakerColumn">
    

<section class="course-instructor">

  
    
      
        
          
          <span class="instructor-name">Matt Forcum</span>

          
            
              <em class="instructor-title">UI/UX Designer, Float</em>
            
          
        
          
          <span class="instructor-name">Heather Ford</span>

          
            
              <em class="instructor-title">UI/UX Designer, Float</em>
            
          
        
      
    
  

</section>

  </div>

</article>

    
      
      

<article class="row course-listing l-subsection online-seminar">
  <div class="small-12 medium-10 medium-push-2 columns l-subsection detailColumn">
    <header>
      <h2><a href="ux-real-world/index.php">UX in the Real World</a></h2>
      
        <strong>Making the Case for User-Centered Design to Clients and Organizations</strong>
      
    </header>
    <section>
      <p>
        
        Recorded October 2016<br/>
        
        $99
      </p>
    </section>

    <p>Usually people talk about their successes. But in this session we&#39;ll share some of our favorite failures in order to explain how to identify and overcome real-world obstacles. Because designing for the user is important, and the challenges we face are real. Whether you&rsquo;re a one-person show working at a startup or advocating from within a larger corporate team, we&rsquo;ll provide relevant case studies and strategies for gaining and maintaining stakeholder buy-in, leveraging limited resources, and cultivating a user-centered design process that benefits everybody.</p>

  </div>

  <div class="hide-for-small-down medium-2 medium-pull-10 columns l-subsection speakerColumn">
    

<section class="course-instructor">

  
    
      
        
          
          <span class="instructor-name">Kristine Malden</span>

          
            
              <em class="instructor-title">Director of User Experience, Estee Lauder Companies Online</em>
            
          
        
          
          <span class="instructor-name">Hana Schank</span>

          
            
              <em class="instructor-title">Founder, CollectiveUX</em>
            
          
        
      
    
  

</section>

  </div>

</article>

    
      
      

<article class="row course-listing l-subsection online-seminar">
  <div class="small-12 medium-10 medium-push-2 columns l-subsection detailColumn">
    <header>
      <h2><a href="every-word-count/index.php">Making Every Word Count</a></h2>
      
        <strong>Writing Well for Responsive Design</strong>
      
    </header>
    <section>
      <p>
        
        Recorded September 2016<br/>
        
        $99
      </p>
    </section>

    <p>Every use of your website is a conversation that the site visitor starts. Today, more than ever, those conversations are on a tablet or smartphone. And your site visitor is busy and impatient.<br />
<br />
Learn how to write clear, compelling, conversational copy (and microcopy) for those busy people on their small screens: Practical guidelines. Great examples.&nbsp;</p>

  </div>

  <div class="hide-for-small-down medium-2 medium-pull-10 columns l-subsection speakerColumn">
    

<section class="course-instructor">

  
    
      
        
          
            <img class="hide-for-small-down" src="https://media.nngroup.com/media/people/photos/Ginny_2014_face_right.jpg.140x140_q95_autocrop_crop_upscale.jpg">
          
          <span class="instructor-name">Janice (Ginny) Redish</span>

          
            
              <em class="instructor-title">President, Redish &amp; Associates, Inc.</em>
            
          
        
      
    
  

</section>

  </div>

</article>

    
    <div class="article-pager">
      

    
        <span><span class="show-for-medium-up">Previous &laquo; </span>
        <span class="show-for-small-down">Prev &laquo; </span></span>
    

    
        
            <span class="currentpage">&nbsp;1&nbsp;</span>
        
    
        
            <a href="index4658.php?page=2">&nbsp;2&nbsp;</a>
        
    

    <a href="index4658.php?page=2"> &raquo; Next</a>


    </div>
  </section>
</div>

    </div>
    




<footer id="page-footer">
    <div class="row hide-for-print">
        <div class="column small-12 medium-6">
            <section class="footer-popular">
                
                <h1>Popular Topics</h1>
                <div style="float: left;  width: 15em; display: block;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../topic/applications/index.php">Application Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../topic/e-commerce/index.php">E-Commerce Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../topic/eyetracking/index.php">Eyetracking </a></li>
	<li style="padding-bottom: .5em;"><a href="../topic/human-computer-interaction/index.php">Human Computer Interaction </a></li>
	<li style="padding-bottom: .5em;"><a href="../topic/information-architecture/index.php">Information Architecture </a></li>
	<li style="padding-bottom: .5em;"><a href="../topic/intranets/index.php">Intranet Design </a></li>
	<li><a href="../topic/interaction-design/index.php">Interaction Design </a></li>
</ul>
</div>

<div style="float: left;  width: 15em; display: block; list-style: none;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../topic/mobile-and-tablet-design/index.php">Mobile and Tablet Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../topic/credibility-persuasion/index.php">Persuasive Design</a></li>
	<li style="padding-bottom: .5em;"><a href="../topic/research-methods/index.php">Research Methods</a></li>
	<li style="padding-bottom: .5em;"><a href="../topic/user-testing/index.php">User Testing </a></li>
	<li style="padding-bottom: .5em;"><a href="../topic/web-usability/index.php">Web Usability </a></li>
	<li style="padding-bottom: .5em;"><a href="../topic/writing-web/index.php">Web Writing &amp; Content Strategy </a></li>
</ul>
</div>

<p>&nbsp;</p>

                
            </section>
        </div>

        <div class="column small-12 medium-6">
            <div class="row">
                <div class="column small-6">
                    <section class="footer-about">
                        <h1>Follow Us</h1>
                        <ul>
	<li><a href="../articles/subscribe/index.php">Subscribe to our weekly UX newsletter</a>,&nbsp;Jakob Nielsen&#39;s Alertbox</li>
	<li>Visit Don Norman&#39;s website, <a href="http://www.jnd.org/"> www.jnd.org </a></li>
	<li>Visit Bruce Tognazzini&#39;s website, <a href="http://www.asktog.com/"> www.asktog.com </a></li>
	<li style="padding-top: 6px;"><a class="twitter-follow-button" data-show-count="false" data-size="large" href="https://twitter.com/nngroup"><img alt="Follow @nngroup on Twitter" height="20" src="https://media.nngroup.com/media/editor/2016/12/30/twitter-follow-button.png" style="width: 117px; height: 20px;" width="117" /></a></li>
	<li style="padding-top: 6px;"><img alt="" height="14" src="https://media.nngroup.com/media/editor/2016/07/26/feed-icon-14x14.png" style="vertical-align:text-top;width:14px;margin-right:.25em;" width="14" /><a href="../feed/rss/index.php">Subscribe to our RSS feed</a></li>
</ul>

                    </section>
                </div>

                <div class="column small-6">
                    <section class="footer-about right-column">
                        <h1>About
</h1>
                        <ul class="no-bullet">
	<li><a href="../about/contact/index.php">Contact information</a></li>
	<li><a href="../about/index.php">About Us</a></li>
	<li><a href="../about/why-nng/index.php">Why NN/g? </a></li>
	<li><a href="../return-policy/index.php">Return Policy</a></li>
</ul>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="copyright column small-12">
            <p><a href="../copyright-and-reprint-info/index.php">Copyright</a> &copy; 1998-2017 Nielsen Norman Group, All Rights Reserved.</p>
        </div>
    </div>
</footer>


    


        <script src="https://media.nngroup.com/static/vendor/fastclick/js/fastclick.min.js"></script>
        <script src="https://media.nngroup.com/static/vendor/foundation/js/foundation.min.js"></script>
        <script src="http://media.nngroup.com/static/js/app.js"></script>
        <script src="http://media.nngroup.com/static/js/scroll-to-fixed.js"></script><script>var externalLinks = document.querySelectorAll('a[href^="http"]');for (var i = externalLinks.length-1; i >= 0; i--) {externalLinks[i].addEventListener("click", function(event) { alert("External links are disabled for this test."); event.preventDefault(); }, false);}var mailLinks = document.querySelectorAll('a[href^="mailto"]');for (var i = mailLinks.length-1; i >= 0; i--) {mailLinks[i].addEventListener("click", function(event) { alert("Email links are disabled for this test."); event.preventDefault(); }, false);}var externalForms = document.querySelectorAll('form');for (var i = externalForms.length-1; i >= 0; i--) {externalForms[i].addEventListener("submit", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false); externalForms[i].addEventListener("click", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false);}</script>

        <script>
            
            $(function() {
                $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
                $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
            });
        </script>

        


        
        <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1072699960;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072699960/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
    </body>

<!-- Mirrored from www.nngroup.com/online-seminars/?page=1 by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:10:35 GMT -->
</html>
