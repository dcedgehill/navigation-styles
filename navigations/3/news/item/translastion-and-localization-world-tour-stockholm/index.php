<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->





    
<!-- Mirrored from www.nngroup.com/news/item/translastion-and-localization-world-tour-stockholm/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:13:19 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1026.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"licenseKey":"1a30ef9931","errorBeacon":"bam.nr-data.net","applicationID":"2048992","transactionName":"NFxSMRBUX0EDUkIPDA0WdhAMVkVbDV8ZCAYUSh4TC1BGQVh/UxEQJ1xEBAtZH1UHRQ==","beacon":"bam.nr-data.net","queueTime":8,"applicationTime":92,"agent":""}</script>
        <title>World Tour Stockholm | Nielsen Norman Group</title><meta property="og:title" content="World Tour Stockholm | Nielsen Norman Group" />
  
        
        <meta name="description" content="December 2000, Interviews with participants at the Stockholm User Experience World Tour.">
        <meta property="og:description" content="December 2000, Interviews with participants at the Stockholm User Experience World Tour." />
        
  
        
	
        
        <meta name="keywords" content="Usability, design">
        

        

        <meta name="robots" content="noarchive">
        <meta name="viewport" content="width=device-width">

        <link rel="search" type="application/opensearchdescription+xml" title="NN/g Search" href="../../../opensearch.xml" />
        <link rel="shortcut icon" href="http://media.nngroup.com/static/img/favicon.ico" />

        <link rel="stylesheet" href="https://media.nngroup.com/static/vendor/font-awesome/css/font-awesome.min.css">

        
        <link rel="stylesheet" href="https://media.nngroup.com/static/css/7232a960f1df.css" type="text/css" />
        <link rel="stylesheet" href="http://fast.fonts.com/t/1.css?apiType=css&amp;projectid=c5846260-9def-46ec-9407-0f1349bd2d64" type="text/css">

        <script src="https://media.nngroup.com/static/vendor/modernizr/js/modernizr.min.js"></script>

        <script type="text/javascript" src="https://media.nngroup.com/static/js/3283be5def74.js"></script>
        

        

        
        
        <!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=229034,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('http://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->


<!-- Google Analytics Tracking Code for http://nngroup.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23132681-1', 'nngroup.com');
  ga('require', 'linkid', 'https://www.nngroup.com/news/item/translastion-and-localization-world-tour-stockholm/linkid.js');
  ga('require', 'displayfeatures');

// Optimizely Universal Analytics Integration
 window.optimizely = window.optimizely || [];
 window.optimizely.push("activateUniversalAnalytics");

  ga('send', 'pageview');

</script>
<script>
/**
* Function to track a click on an outbound link in Google Analytics.
*/
function trackOutboundLink(label, url) {
    if(window.ga && ga.loaded) {
        ga('send', 'event', 'outbound', 'click', label, {'hitCallback': function(){document.location = url;}
        });
    } else {
        document.location = url;
    }
}
/**
* Function to track a click on a link in the article announcement area.
*/
$('#articleAnnouncement a').on('click', function(){
  ga('send', 'event', 'link', 'click', 'articleAnnouncement');
});
</script>

<script type="text/javascript">
 setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2421.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



<!-- Hotjar Tracking Code for http://nngroup.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:121605,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

        <script src="https://media.nngroup.com/static/js/analytics/track_outbound_link.js"></script>
        
        
        
    </head>
    <body class="location-about location-news">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
    <header id="page-header">
        <div id="cart" class="shopping-cart mobile-cart show-for-small-down"></div>
        <div class="row">
            <div id="masthead" class="masthead small-12 medium-7 column">

                <!-- The button to toggles menu on small screen -->

                <h1><a href="../../../index.php"><img src="http://media.nngroup.com/static/img/logo-wordmark.png" width="74" height="30"> Nielsen <span class="norman">Norman</span> Group</a></h1>
                <p class="tagline show-for-medium-up">Evidence-Based User Experience Research, Training, and Consulting</p>

                <!-- The button to toggles form search on small screen -->


                <div style="clear: right;width:  100%;height: 1px;"></div><?php define('ROOT_PATH',$_SERVER["DOCUMENT_ROOT"] . '/apps/navigation_styles/navigations/3'); ?><?php include(ROOT_PATH)."/navigation.php"; ?>

                <!-- Drop down menu on small screen -->
                <div id="hideNav" class="show-for-small-down">
                  
                  <ul class="menu-content">
                    <li><a href="../../../index.php">Home</a></li>
                    <li class="has-submenu">
                      <a>Training</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../../training/index.php">Overview</a></li>
                        
<li id="nav-ux-conference"><a href="../../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../../online-seminars/index.php"><span>Online Seminars</span></a></li>

                      </ul>
                    </li>
                    <li class="has-submenu">
                      <a>Consulting</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
  <li><a href="../../../consulting/index.php">Overview</a></li>
  
  <li><a href="../../../consulting/benchmarking/index.php">Benchmarking</a></li>
  
  <li><a href="../../../consulting/expert-design-review/index.php">Expert Design Review</a></li>
  
  <li><a href="../../../consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
  
  <li><a href="../../../consulting/usability-testing/index.php">Usability Testing </a></li>
  
  <li><a href="../../../consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
  
  <li><a href="../../../in-house-training/index.php">In-House Training</a></li>
</ul>

                    </li>

                    <li><a href="../../../reports/index.php">Reports</a></li>
                    <li><a href="../../../articles/index.php">Articles</a></li>
                    <li class="has-submenu"><a>About NN/G</a><i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../../about/index.php">Overview</a></li>
                        <li><a href="../../../people/index.php">People</a></li>
                        <li><a href="../../../about/why-nng/index.php">Why NN/g?</a></li>
                        <li><a href="../../../about/contact/index.php">Contact</a></li>
                        <li><a href="../../index.php">News</a></li>
                        <li><a href="../../../about/history/index.php">History</a></li>
                        <li><a href="../../../books/index.php">Books</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                </div>

                <hr class="header-divider show-for-small-only"/>


            </div>
       </div>

        <div id="hideNav">            <nav class="primary-nav small-12 column">
                <ul>
                    <li><a class="navitem" id="nav-home" href="../../../index.php">Home</a></li>
                    <li><a class="navitem" id="nav-training" href="../../../training/index.php">Training</a></li>
                    <li><a class="navitem" id="nav-consulting" href="../../../consulting/index.php">Consulting</a></li>
                    <li><a class="navitem" id="nav-reports" href="../../../reports/index.php">Reports</a></li>
                    <li><a class="navitem" id="nav-articles" href="../../../articles/index.php">Articles</a></li>
                    <li><a class="navitem" id="nav-about" href="../../../about/index.php">About NN/G</a></li>
                </ul>
            </nav>
            <nav class="secondary-nav small-12 medium-12 large-12 columns">
                
<ul class="inline-list">
    <li id="nav-people"><a href="../../../people/index.php"><span>People</span></a></li>
    <li id="nav-whynng"><a href="../../../about/why-nng/index.php"><span>Why NN/g?</span></a></li>
    <li id="nav-contact"><a href="../../../about/contact/index.php"><span>Contact</span></a></li>
    <li id="nav-news"><a href="../../index.php"><span>News</span></a></li>
    <li id="nav-history"><a href="../../../about/history/index.php"><span>History</span></a></li>
    <li id="nav-books"><a href="../../../books/index.php"><span>Books</span></a></li>
</ul>

            </nav>
        </div>
    </header>
    <div id="mainContent" class="l-content">
    


<div class="row">
    <div class="small-12 medium-4 large-4 columns offset-by-eight">
    </div>
</div>
<div class="row">
    <div class="small-12 medium-3 large-3 columns l-navrail news-rail hide-for-small-only">
        <h1><a href="../../index.php">All News Types</a></h1>
        <ul class="no-bullet">
            
                
                    <li><a href="../../type/announcements/index.php">Announcements (52)</a></li>
                
            
                
                    <li><a href="../../type/press-mentions/index.php">Interviews &amp; Press (225)</a></li>
                
            
        </ul>
    </div>
    <div class="small-12 medium-9 large-9 columns">
        <article class="news_detail">
            <div class="l-section-intro">
                <header>
                    <h1>NN/g News</h1>
                    <h2>Translation and Localization are Still Tough Problems</h2>
                    <p>December 7, 2000</p>
                </header>
            </div>
            <div class="l-subsection">
                <p>
 <em>
  By
  <a href="../../../people/kara-pernice/index.php">
   Kara Pernice
  </a>
 </em>
</p>
<p>
 <strong>
  STOCKHOLM, Sweden, December 7, 2000.&nbsp;
 </strong>
 Usability issues associated with having international users is an important topic for usability professionals in the United States. When we talked with attendees at the World Tour in Stockholm, we found that many of them are also struggling with these issues.
</p>
<table align="left" border="0" cellpadding="0" cellspacing="0">
 <tbody>
  <tr>
   <td>
    <img alt="Photo of Åsa Bäckström, Kreatel " src="http://media.nngroup.com.s3.amazonaws.com/media/editor/2012/11/30/st-aasabaeckstroem.jpg" style="border-width: 0px; border-style: solid; width: 75px; height: 100px;" />
   </td>
  </tr>
  <tr>
   <td>
    <em>
     &Aring;sa B&auml;ckstr&ouml;m,
     <br />
     <small>
      Kreatel
     </small>
    </em>
   </td>
  </tr>
 </tbody>
</table>
<p>
 &Aring;sa B&auml;ckstr&ouml;m, an interaction designer at Kreatel, a telephone and broadband services company in Sweden, is facing international challenges. &quot;We have
 <strong>
  customers all around Europe and Asia
 </strong>
 ,&quot; she said. &quot;The older products are mostly hardware and a small user manual, so designing [for international customers] is not really a problem. But when it comes to the broadband service options, you have the remote control, the portal, the keyboard and the box itself. One tiny little detail is the text you put on top of the buttons on the remote and keyboard. You want to make them as international as possible.&quot;
</p>
<p>
 B&auml;ckstr&ouml;m said that designing international products that have with several components can impact schedules significantly. &quot;The challenges we have are both software and hardware,&quot; she said. &quot;The lead-time you have when working with hardware is very, very different from working with software. With software, you can duplicate it as easily as pressing two keys on a keyboard. Hardware you have to be really prepared and so much in the process early. A lot of the hardware is produced in Japan.&quot;
</p>
<p>
 To internationalize the hardware products, they have chosen to use a combination of acronyms and icons. She said, &quot;Some of the icons we use are very commonly-used icons, like the mute symbol, a loudspeaker that is crossed out. Or volume is like a triangle with a plus. I think those are the only international icons we use. It&#39;s really, really difficult. We use acronyms, so for example, we use www to surf the Web, and a TV button to watch TV.&quot;
</p>
<p>
 While they do try to make one version of the hardware products, they cannot do this with the software. &quot;The portal itself can&#39;t be international. It has to be translated,&quot; she said. &quot;Maybe there are also changes for colors, for example what Brenda Laurel said about using yellow and red in different counties.&quot;
</p>
<p>
 Fiona Munn, who works on design management for British Airways in London, discussed the unique challenges they face in designing for international customers. &quot;That is one of the major challenges,&quot; she said. &quot;It is a global website...We have Web sites in each country, some
 <strong>
  91 countries online, that are part of the whole British Airways brand
 </strong>
 . We have a big global presence. That&#39;s one of the big issues, how to communicate to those markets...people traveling from those countries, making sure they get information.
</p>
<p>
 &quot;The practical issue for us is translation,&quot; she said. &quot;There are local language sites which are translated.&quot; However, there is some information that is not translated, she said. &quot;The core parts of our site, the transactional system, schedule tables, are all in English. Those are areas that have been highlighted that we need to look into... We&#39;re investigating the most proper way of doing it. We appreciate the technology is not there to do automatic translation.&quot;
</p>
<p>
 Geir B. Hansen, a designer and developer at Funcom.com, a computer games company in Norway, is also facing translation challenges. &quot;We are looking at rebuilding our entire Web site,&quot; he said. &quot;We are currently at four languages, something like 400 pages in each language. We&#39;re going to do French and German too, so it will be six.&quot; They found several challenges while working with so many pages and languages. &quot;We put quite an effort into making it maintainable,&quot; he said. &quot;We&#39;ve had quite a lot of trouble with text being translated as graphics. Web pages put headlines out in graphics rather than plain text. We had trouble with that because it takes quite a lot of work to translate.&quot;
</p>
<p>
 After encountering these translation issues, they created some specific methods for making the site easier to manage. Hansen said, &quot;We&#39;ve made it a policy to keep text as text, and to separate text from presentation so the translators can keep up.&quot; In addition to creating policies, they have also created software to help them with this work. &quot;We made an in-house translation tool for audio and text for an adventure game we&#39;re making ...took something like a week to develop the tool. Saved, I expect, hundreds of thousands of dollars in translation costs.&quot;
</p>
<p>
 In addition to the translation and localization issues, Hansen says his company faces other international issues. &quot;You have ratings in the U.S. and we do too. We created this family-friendly game. PG is for kids with parents and guidance. We got one of those ratings [in the U.S.] for a very family-oriented and non-violent game because of what the American people see as bad language and sexual content. This is different from European. We wanted a G rating because it&#39;s a family-oriented game.
 <strong>
  Violent games in the U.S. get lower ratings than ours because ours has swearing. In the U.S., swearing seems worse than violence
 </strong>
 .&quot; Hansen said that he&#39;s not entirely sure why the game got a PG rating. &quot;The
 <em>
  F
 </em>
 -word is just the beginning. It fits in the story. It seems natural to us. It would be perfectly normal on TV here. It is a totally non-violent game.&quot; He said this is a relatively serious problem for their company because sales in America are important. &quot;Compared to population, we&#39;ve sold a lot in Norway. All games sell the most in the U.S.&quot;
</p>
<p>
 &nbsp;
</p>

            </div>
        </article>
    </div>
</div>


    </div>
    




<footer id="page-footer">
    <div class="row hide-for-print">
        <div class="column small-12 medium-6">
            <section class="footer-popular">
                
                <h1>Popular Topics</h1>
                <div style="float: left;  width: 15em; display: block;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../../topic/applications/index.php">Application Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../../topic/e-commerce/index.php">E-Commerce Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../../topic/eyetracking/index.php">Eyetracking </a></li>
	<li style="padding-bottom: .5em;"><a href="../../../topic/human-computer-interaction/index.php">Human Computer Interaction </a></li>
	<li style="padding-bottom: .5em;"><a href="../../../topic/information-architecture/index.php">Information Architecture </a></li>
	<li style="padding-bottom: .5em;"><a href="../../../topic/intranets/index.php">Intranet Design </a></li>
	<li><a href="../../../topic/interaction-design/index.php">Interaction Design </a></li>
</ul>
</div>

<div style="float: left;  width: 15em; display: block; list-style: none;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../../topic/mobile-and-tablet-design/index.php">Mobile and Tablet Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../../topic/credibility-persuasion/index.php">Persuasive Design</a></li>
	<li style="padding-bottom: .5em;"><a href="../../../topic/research-methods/index.php">Research Methods</a></li>
	<li style="padding-bottom: .5em;"><a href="../../../topic/user-testing/index.php">User Testing </a></li>
	<li style="padding-bottom: .5em;"><a href="../../../topic/web-usability/index.php">Web Usability </a></li>
	<li style="padding-bottom: .5em;"><a href="../../../topic/writing-web/index.php">Web Writing &amp; Content Strategy </a></li>
</ul>
</div>

<p>&nbsp;</p>

                
            </section>
        </div>

        <div class="column small-12 medium-6">
            <div class="row">
                <div class="column small-6">
                    <section class="footer-about">
                        <h1>Follow Us</h1>
                        <ul>
	<li><a href="../../../articles/subscribe/index.php">Subscribe to our weekly UX newsletter</a>,&nbsp;Jakob Nielsen&#39;s Alertbox</li>
	<li>Visit Don Norman&#39;s website, <a href="http://www.jnd.org/"> www.jnd.org </a></li>
	<li>Visit Bruce Tognazzini&#39;s website, <a href="http://www.asktog.com/"> www.asktog.com </a></li>
	<li style="padding-top: 6px;"><a class="twitter-follow-button" data-show-count="false" data-size="large" href="https://twitter.com/nngroup"><img alt="Follow @nngroup on Twitter" height="20" src="https://media.nngroup.com/media/editor/2016/12/30/twitter-follow-button.png" style="width: 117px; height: 20px;" width="117" /></a></li>
	<li style="padding-top: 6px;"><img alt="" height="14" src="https://media.nngroup.com/media/editor/2016/07/26/feed-icon-14x14.png" style="vertical-align:text-top;width:14px;margin-right:.25em;" width="14" /><a href="../../../feed/rss/index.php">Subscribe to our RSS feed</a></li>
</ul>

                    </section>
                </div>

                <div class="column small-6">
                    <section class="footer-about right-column">
                        <h1>About
</h1>
                        <ul class="no-bullet">
	<li><a href="../../../about/contact/index.php">Contact information</a></li>
	<li><a href="../../../about/index.php">About Us</a></li>
	<li><a href="../../../about/why-nng/index.php">Why NN/g? </a></li>
	<li><a href="../../../return-policy/index.php">Return Policy</a></li>
</ul>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="copyright column small-12">
            <p><a href="../../../copyright-and-reprint-info/index.php">Copyright</a> &copy; 1998-2017 Nielsen Norman Group, All Rights Reserved.</p>
        </div>
    </div>
</footer>


    


        <script src="https://media.nngroup.com/static/vendor/fastclick/js/fastclick.min.js"></script>
        <script src="https://media.nngroup.com/static/vendor/foundation/js/foundation.min.js"></script>
        <script src="http://media.nngroup.com/static/js/app.js"></script>
        <script src="http://media.nngroup.com/static/js/scroll-to-fixed.js"></script><script>var externalLinks = document.querySelectorAll('a[href^="http"]');for (var i = externalLinks.length-1; i >= 0; i--) {externalLinks[i].addEventListener("click", function(event) { alert("External links are disabled for this test."); event.preventDefault(); }, false);}var mailLinks = document.querySelectorAll('a[href^="mailto"]');for (var i = mailLinks.length-1; i >= 0; i--) {mailLinks[i].addEventListener("click", function(event) { alert("Email links are disabled for this test."); event.preventDefault(); }, false);}var externalForms = document.querySelectorAll('form');for (var i = externalForms.length-1; i >= 0; i--) {externalForms[i].addEventListener("submit", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false); externalForms[i].addEventListener("click", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false);}</script>

        <script>
            
            $(function() {
                $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
                $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
            });
        </script>

        


        
        <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1072699960;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072699960/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
    </body>

<!-- Mirrored from www.nngroup.com/news/item/translastion-and-localization-world-tour-stockholm/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:13:19 GMT -->
</html>
