<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->





    
<!-- Mirrored from www.nngroup.com/news/item/feed-magazine-interview-keynote-speakers-ux/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:15:50 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1026.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"licenseKey":"1a30ef9931","queueTime":5,"beacon":"bam.nr-data.net","errorBeacon":"bam.nr-data.net","applicationID":"2048992","transactionName":"NFxSMRBUX0EDUkIPDA0WdhAMVkVbDV8ZCAYUSh4TC1BGQVh/UxEQJ1xEBAtZH1UHRQ==","applicationTime":84,"agent":""}</script>
        <title>Feed Magazine World Tour Interview | Nielsen Norman Group</title><meta property="og:title" content="Feed Magazine World Tour Interview | Nielsen Norman Group" />
  
        
        <meta name="description" content="November 2000, FEED Magazine&#39;s interview with User Experience World Tour keynote speakers">
        <meta property="og:description" content="November 2000, FEED Magazine&#39;s interview with User Experience World Tour keynote speakers" />
        
  
        
	
        
        <meta name="keywords" content="Usability, design">
        

        

        <meta name="robots" content="noarchive">
        <meta name="viewport" content="width=device-width">

        <link rel="search" type="application/opensearchdescription+xml" title="NN/g Search" href="../../../opensearch.xml" />
        <link rel="shortcut icon" href="http://media.nngroup.com/static/img/favicon.ico" />

        <link rel="stylesheet" href="https://media.nngroup.com/static/vendor/font-awesome/css/font-awesome.min.css">

        
        <link rel="stylesheet" href="https://media.nngroup.com/static/css/7232a960f1df.css" type="text/css" />
        <link rel="stylesheet" href="http://fast.fonts.com/t/1.css?apiType=css&amp;projectid=c5846260-9def-46ec-9407-0f1349bd2d64" type="text/css">

        <script src="https://media.nngroup.com/static/vendor/modernizr/js/modernizr.min.js"></script>

        <script type="text/javascript" src="https://media.nngroup.com/static/js/3283be5def74.js"></script>
        

        

        
        
        <!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=229034,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('http://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->


<!-- Google Analytics Tracking Code for http://nngroup.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23132681-1', 'nngroup.com');
  ga('require', 'linkid', 'https://www.nngroup.com/news/item/feed-magazine-interview-keynote-speakers-ux/linkid.js');
  ga('require', 'displayfeatures');

// Optimizely Universal Analytics Integration
 window.optimizely = window.optimizely || [];
 window.optimizely.push("activateUniversalAnalytics");

  ga('send', 'pageview');

</script>
<script>
/**
* Function to track a click on an outbound link in Google Analytics.
*/
function trackOutboundLink(label, url) {
    if(window.ga && ga.loaded) {
        ga('send', 'event', 'outbound', 'click', label, {'hitCallback': function(){document.location = url;}
        });
    } else {
        document.location = url;
    }
}
/**
* Function to track a click on a link in the article announcement area.
*/
$('#articleAnnouncement a').on('click', function(){
  ga('send', 'event', 'link', 'click', 'articleAnnouncement');
});
</script>

<script type="text/javascript">
 setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2421.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



<!-- Hotjar Tracking Code for http://nngroup.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:121605,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

        <script src="https://media.nngroup.com/static/js/analytics/track_outbound_link.js"></script>
        
        
        
    </head>
    <body class="location-about location-news">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
    <header id="page-header">
        <div id="cart" class="shopping-cart mobile-cart show-for-small-down"></div>
        <div class="row">
            <div id="masthead" class="masthead small-12 medium-7 column">

                <!-- The button to toggles menu on small screen -->

                <h1><a href="../../../index.php"><img src="http://media.nngroup.com/static/img/logo-wordmark.png" width="74" height="30"> Nielsen <span class="norman">Norman</span> Group</a></h1>
                <p class="tagline show-for-medium-up">Evidence-Based User Experience Research, Training, and Consulting</p>

                <!-- The button to toggles form search on small screen -->


                <div style="clear: right;width:  100%;height: 1px;"></div><?php define('ROOT_PATH',$_SERVER["DOCUMENT_ROOT"] . '/apps/navigation_styles/navigations/3'); ?><?php include(ROOT_PATH)."/navigation.php"; ?>

                <!-- Drop down menu on small screen -->
                <div id="hideNav" class="show-for-small-down">
                  
                  <ul class="menu-content">
                    <li><a href="../../../index.php">Home</a></li>
                    <li class="has-submenu">
                      <a>Training</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../../training/index.php">Overview</a></li>
                        
<li id="nav-ux-conference"><a href="../../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../../online-seminars/index.php"><span>Online Seminars</span></a></li>

                      </ul>
                    </li>
                    <li class="has-submenu">
                      <a>Consulting</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
  <li><a href="../../../consulting/index.php">Overview</a></li>
  
  <li><a href="../../../consulting/benchmarking/index.php">Benchmarking</a></li>
  
  <li><a href="../../../consulting/expert-design-review/index.php">Expert Design Review</a></li>
  
  <li><a href="../../../consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
  
  <li><a href="../../../consulting/usability-testing/index.php">Usability Testing </a></li>
  
  <li><a href="../../../consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
  
  <li><a href="../../../in-house-training/index.php">In-House Training</a></li>
</ul>

                    </li>

                    <li><a href="../../../reports/index.php">Reports</a></li>
                    <li><a href="../../../articles/index.php">Articles</a></li>
                    <li class="has-submenu"><a>About NN/G</a><i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../../about/index.php">Overview</a></li>
                        <li><a href="../../../people/index.php">People</a></li>
                        <li><a href="../../../about/why-nng/index.php">Why NN/g?</a></li>
                        <li><a href="../../../about/contact/index.php">Contact</a></li>
                        <li><a href="../../index.php">News</a></li>
                        <li><a href="../../../about/history/index.php">History</a></li>
                        <li><a href="../../../books/index.php">Books</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                </div>

                <hr class="header-divider show-for-small-only"/>


            </div>
       </div>

        <div id="hideNav">            <nav class="primary-nav small-12 column">
                <ul>
                    <li><a class="navitem" id="nav-home" href="../../../index.php">Home</a></li>
                    <li><a class="navitem" id="nav-training" href="../../../training/index.php">Training</a></li>
                    <li><a class="navitem" id="nav-consulting" href="../../../consulting/index.php">Consulting</a></li>
                    <li><a class="navitem" id="nav-reports" href="../../../reports/index.php">Reports</a></li>
                    <li><a class="navitem" id="nav-articles" href="../../../articles/index.php">Articles</a></li>
                    <li><a class="navitem" id="nav-about" href="../../../about/index.php">About NN/G</a></li>
                </ul>
            </nav>
            <nav class="secondary-nav small-12 medium-12 large-12 columns">
                
<ul class="inline-list">
    <li id="nav-people"><a href="../../../people/index.php"><span>People</span></a></li>
    <li id="nav-whynng"><a href="../../../about/why-nng/index.php"><span>Why NN/g?</span></a></li>
    <li id="nav-contact"><a href="../../../about/contact/index.php"><span>Contact</span></a></li>
    <li id="nav-news"><a href="../../index.php"><span>News</span></a></li>
    <li id="nav-history"><a href="../../../about/history/index.php"><span>History</span></a></li>
    <li id="nav-books"><a href="../../../books/index.php"><span>Books</span></a></li>
</ul>

            </nav>
        </div>
    </header>
    <div id="mainContent" class="l-content">
    


<div class="row">
    <div class="small-12 medium-4 large-4 columns offset-by-eight">
    </div>
</div>
<div class="row">
    <div class="small-12 medium-3 large-3 columns l-navrail news-rail hide-for-small-only">
        <h1><a href="../../index.php">All News Types</a></h1>
        <ul class="no-bullet">
            
                
                    <li><a href="../../type/announcements/index.php">Announcements (52)</a></li>
                
            
                
                    <li><a href="../../type/press-mentions/index.php">Interviews &amp; Press (225)</a></li>
                
            
        </ul>
    </div>
    <div class="small-12 medium-9 large-9 columns">
        <article class="news_detail">
            <div class="l-section-intro">
                <header>
                    <h1>NN/g News</h1>
                    <h2>FEED Magazine interview with keynote speakers, User Experience World Tour, November 2000</h2>
                    <p>November 17, 2000</p>
                </header>
            </div>
            <div class="l-subsection">
                <h1>
	FEED | Dialog: The Butterfly Effect</h1>
<p>
	<em>By Steven Johnson</em></p>
<p>
	The study of information and interaction design has for years been thought of by people outside the field as a somewhat nerdy, esoteric pursuit that has little direct bearing on their lives. And two weeks ago, pretty much anyone hearing the phrase &quot;butterfly ballot&quot; could be excused for assuming that a lepidopterists&#39; club somewhere was electing a new president.</p>
<p>
	But as the ongoing ballot debacle in Florida continues to slouch awkwardly toward a conclusion, it&#39;s clear that &quot;butterfly ballot&quot; might well carry the same weight with future historians that &quot;Watergate,&quot; &quot;Tammany Hall&quot; and other pithy political catchphrases carry today.</p>
<p>
	It&#39;s not surprising, then, that the field of information design is suddenly newsworthy, if not downright fascinating, for millions of people who never gave it a second thought. But what, exactly, <i>is</i> information and interactive design? Why -- aside from the infrequent cataclysmic meltdowns precipitated by crappy design -- should anyone care one way or another? And who can explain the nuts and bolts of what really went wrong in the Florida election in the first place?</p>
<p>
	The usability experts that FEED&#39;s Steven Johnson recently talked to are happy to take up the task. Jakob Nielsen, Bruce Tognazzini, Brenda Laurel, and Don Norman are recognized as four of the finest information designers in the world, and are currently on a &quot;world tour,&quot; dubbed The Main Event, giving talks and conducting seminars in the U.S., Europe, Japan, and Down Under. In a wide-ranging discussion on topics from the butterfly ballot, to computer interfaces, to the best and worst aspects of the Web, the foursome -- joined by <i>HotWired Style</i> author Jeffrey Veen -- brought some much-needed methodology and meaning to the madness.</p>
<p>
	<font color="#000000" face="Helvetica,Arial,Monaco,Courier"><b>A NOTE ON THE PARTICIPANTS:</b></font></p>
<blockquote>
	<ul>
		<li>
			<font color="#336633" face="Helvetica,Arial,Monaco,Courier"><b>JAKOB NIELSEN</b></font> is a principal of Nielsen Norman Group. Dr. Nielsen was previously a Sun Microsystems Distinguished Engineer and has also worked at the IBM User Interface Institute, the Technical University of Denmark, and Bell Communications Research. His bi-weekly <a href="../../../articles/index.php" target="new"><i>Alertbox</i></a> column about Web usability has been published on the Web since 1995 and currently gets about six million page views per year. Nielsen&#39;s most recent book, <a href="../../../books/designing-web-usability/index.php" target="new"><i>Designing Web Usability: The Practice of Simplicity,</i></a> has been translated into ten languages and has about a quarter million copies in print. Dr. Nielsen is the founder of the &quot;discount usability engineering&quot; approach to design, using fast and cheap methods to build on user data. He holds fifty U.S. patents, mostly on ways of making the Internet easier to use.
			<p>
				&nbsp;</p>
		</li>
		<li>
			<font color="#990000" face="Helvetica,Arial,Monaco,Courier"><b>BRUCE TOGNAZZINI</b></font> is a recognized leader in human/computer interaction design. As Chief Designer at Healtheon/WebMD, he helped establish WebMD as the premiere healthcare Web site. Before that, he was Distinguished Engineer for Strategic Technology at Sun where he led the Starfire project that predicted the rise of the World Wide Web. During his fourteen years at Apple Computer, he founded the Apple Human Interface Group and acted as Apple&#39;s Human Interface Evangelist. He has recently rejoined his long-time colleagues as a principal of Nielsen Norman Group. A sought-after public speaker and consultant, he has published two books, <a href="http://www.amazon.com/exec/obidos/ISBN%3D0201608421/useitcomusableinA/002-1979496-6564807" target="new"><i>Tog on Interface</i></a> and <a href="http://www.amazon.com/exec/obidos/ISBN%3D0201489171/useitcomusableinA/002-1979496-6564807" target="new"><i>Tog on Software Design,</i></a> both from Addison Wesley and is currently publishing the free Web &#39;zine, <a href="http://www.asktog.com/" target="new"><i>AskTog.</i></a>
			<p>
				&nbsp;</p>
		</li>
		<li>
			<font color="#996633" face="Helvetica,Arial,Monaco,Courier"><b>BRENDA LAUREL</b></font> is a designer, researcher, writer, and performer, and a principal of Nielsen Norman Group. For twenty-five years, her work has focused on interactive story, experience design, and cultural aspects of technology, working with companies including Atari, Activision, and Apple. A pioneer in the design and theory of virtual reality, she co-founded Telepresence Research in 1990. She co-designed and produced the seminal virtual environment, &quot;Placeholder,&quot; at the Banff Centre for the Arts in 1993. After a four-year stint studying gender and technology at Interval Research, Dr. Laurel co-founded Purple Moon in 1996 to create interactive media for girls (acquired by Mattel in 1999). Among her many publications are <a href="http://www.amazon.com/exec/obidos/ISBN%3D0201517973/useitcomusableinA/002-1979496-6564807" target="new"><i>The Art of Human-Computer Interface Design</i></a> and <a href="http://www.amazon.com/exec/obidos/ISBN%3D0201550601/useitcomusableinA/002-1979496-6564807" target="new"><i>Computers as Theatre.</i></a>
			<p>
				&nbsp;</p>
		</li>
		<li>
			<font color="#006699" face="Helvetica,Arial,Monaco,Courier"><b>DON NORMAN</b></font> is a principal of Nielsen Norman Group and President of Learning Systems, a division of UNext.com, a distance education company. He is also cofounder of the Nielsen Norman Group, an executive consulting firm that helps companies produce human-centered products and services. In this role, he serves on the advisory boards of numerous companies. Dr. Norman is Professor Emeritus of both Cognitive Science and Psychology at the University of California, San Diego, former Vice President of the advanced technology group, Apple Computer and an executive at Hewlett-Packard. Dr. Norman is the author of <a href="http://www.amazon.com/exec/obidos/ISBN=0385267746/useitcomusableinA/" target="new"><i>The Psychology of Everyday Things,</i></a> <a href="http://www.amazon.com/exec/obidos/ISBN=0201626950/useitcomusableinA/" target="new"><i>Things That Make Us Smart,</i></a> and most recently, <a href="http://www.amazon.com/exec/obidos/ISBN=0262640414/useitcomusableinA/" target="new"><i>The Invisible Computer,</i></a> a book that Business Week has called &quot;the bible of &#39;post PC&#39; thinking.&quot;</li>
	</ul>
</blockquote>
<p>
	<font color="#333333" face="helvetica,arial, monaco,courier"><b>FEED:</b></font> So let&#39;s start with the now world-famous butterfly ballot. What&#39;s the problem with this thing?</p>
<p>
	<font color="#006699" face="Helvetica,Arial,Monaco,Courier"><b>DON NORMAN:</b></font> There are several problems with the butterfly ballot. One is it clearly wasn&#39;t user-tested. But in fact, it&#39;s not easy to user-test, because the error rate is like one percent. And so you&#39;re going to have to make sure you test 500 people in order to make sure you&#39;ve really tested it. Second, you&#39;ve got to be careful how you test it. The election people said: Well, we showed it to all parties and they all said it was okay. But you can&#39;t just show the ballot, you have to show it in context, inside the voting booth.</p>
<p>
	And finally, you have to look in context at how a person uses it, which is what Tog [Bruce Tognazzini] has argued. It&#39;s not like a menu -- I go and I read it and I study it and I think about it and try to evaluate which candidate I want. Instead, I go in already knowing which candidate I want. You look for Bush, and it&#39;s the first name on the ballot and you punch the first hole. You go in for Gore, and it&#39;s the second name on the ballot so you punch the second hole. You go in for Buchanan; you look on the left and it&#39;s not there, so you look at the right -- oh, there it is, you see Buchanan and you punch the hole. But if I&#39;m a Gore voter, then oops, the second hole which I punched for Gore is actually Buchanan. And I never even saw the name on the right.</p>
<p>
	<font color="#996633" face="Helvetica,Arial,Monaco,Courier"><b>BRENDA LAUREL:</b></font> Because you weren&#39;t looking for it.</p>
<p>
	<font color="#006699" face="Helvetica,Arial,Monaco,Courier"><b>NORMAN:</b></font> Because I wasn&#39;t looking for it. And so it had to be user-tested, and it wasn&#39;t. You can&#39;t do one of Jakob&#39;s quick tests in this case, because it&#39;s a subtle error. Most people did get it right. Maybe one percent or less got it wrong, but that&#39;s still thousands of people. But here&#39;s the thing that I chide our audience on. How come nobody here was asked to help? How come no one would ever think of asking. <i>USA Today</i> asked three people to redesign the ballot to eliminate the errors. And they showed three redesigns. You think any of them was a usability specialist? No. They were all graphic artists or typographers -- they were all people with actually appropriate skills, but it never even occurred to <i>USA Today</i> to ask a usability person. And I believe it&#39;s because usability people are not designers -- that until we become designers, nobody cares about us. What we are good at is pointing out flaws after somebody else has designed it, which makes us not very interesting and not very relevant. And I think that we have to become designers to make the point, to have the power to have people think about us.</p>
<p>
	<font color="#333333" face="helvetica,arial, monaco,courier"><b>FEED:</b></font> Well, that segues nicely to the whole mission of this tour. What do you think the overall state is now of usability design?</p>
<p>
	<font color="#336633" face="Helvetica,Arial,Monaco,Courier"><b>JAKOB NIELSEN:</b></font> Well, I think there are two different ways of looking at it. On the one hand, there&#39;s been a huge explosion, really, taking us back to kind of the early days when we were starting early in this area and there were not a lot of other people there. It was a very oppressed field, mainly only in Silicon Valley and a few computer companies. And now, if you just look at the huge audience here, the worldwide audience, you know -- people that really are doing it, there&#39;s a huge growth. That&#39;s a very positive spin, that the Web has really built a lot more attention for us because it&#39;s just like life or death for Web sites, whether they do it right. On the other hand, the growth is minuscule relative to the growth of the Internet. So in other words, the vast majority of Web sites have never had a professional usability person look at them. They don&#39;t have anybody in their project teams other than -- if they&#39;re lucky -- graphic designers. But, you know, not people with interaction design skills, not people with usability testing skills, no expertise. They&#39;ve never run a single test. And if they do any customer research, it&#39;s focus groups, which are often misleading as opposed to helpful for interactions. So there&#39;s a lot of people who think they get customer data, but they actually get bogus or misleading data.</p>
<p>
	<font color="#996633" face="Helvetica,Arial,Monaco,Courier"><b>LAUREL:</b></font> And there&#39;s a tremendous amount of pressure to get it right because, as Jakob always points out, if a person gets frustrated, they&#39;re gone, you know, they&#39;ve made no upfront investment. But, although right now we can do a pretty good job of measuring the effectiveness of advertising, we can&#39;t do a very good job of measuring the effectiveness of usability. So even though sites may be failing because they&#39;re not usable, it&#39;s non-trivial to be able to assign usability as the cause. And there are all these other factors that people blame. So I think part of our mission is to get better at it and helping people understand the techniques they can use to measure usability, so that you can lay the blame where it belongs and sort of decouple it from advertising.</p>
<p>
	<font color="#333333" face="helvetica,arial, monaco,courier"><b>FEED:</b></font> What are the key usability problems that you see on the Web today?</p>
<p>
	<font color="#990000" face="Helvetica,Arial,Monaco,Courier"><b>BRUCE TOGNAZZINI:</b></font> Fundamentally, what Apple did with the Mac interface and what Windows copied so faithfully, was that there was a built-in user interface. It was easier to do things right than to do them wrong. There were guidelines, there were financial motivations, there was this whole thrust to do it the right way. So even if you weren&#39;t a user interface designer, you were probably -- unless you were really idiosyncratic and just wanted to go off on your own -- going to end up with some semblance of a standard interface. The Web is just this blank sheet of paper. There&#39;s no standard interface, it is infinitely more difficult to do it right than to do it wrong. It&#39;s exactly backwards in that way, so it&#39;s encouraging bad design.</p>
<p>
	<font color="#336633" face="Helvetica,Arial,Monaco,Courier"><b>NIELSEN:</b></font> And the vocabulary is too limited, but also, people reinvent the wheel or they invent square wheels [laughter] -- like weird Flash scroll bars and all. And it&#39;s not to say that the Mac scroll bar is the perfect scroll bar and you can never improve on it. It&#39;s just that they did spend a lot of time on getting all the little details done, whereas now, every single Flash designer thinks that they&#39;re an interaction designer, as well, and they make up their own widgets.</p>
<p>
	And of course most of the time, they&#39;re going to be bad, as opposed to good, because they don&#39;t put enough effort in it. But I also think that there is much too little emphasis on content on the Web, which is really what people are online for -- to get solutions and answers and buy something, so on and so forth. And yet most of the attention in the design field is really not on the content, not on writing the one line that will describe what this site does. Many, many times, that&#39;s an afterthought, how to actually describe what the site is doing. They don&#39;t have copywriters in the design teams. The actual product pages, for example, are described in pure fluff where you cannot decode what the product actually does. And that gets almost no attention -- the content design. While the graphic design does because it&#39;s kind of more striking, I guess.</p>
<p>
	<font color="#996633" face="Helvetica,Arial,Monaco,Courier"><b>LAUREL:</b></font> One of the reasons why content is poorly designed is because it doesn&#39;t play a major role in the economic model of most sites. So even though content is why I&#39;m going there, the way the company is getting its revenue is from advertising or transaction fees or something. So, again, if you can&#39;t make the case that the content is the reason why the user&#39;s coming, and you can&#39;t reflect that as some kind of direct economy, then it gets neglected and, in fact, the content creator and the guy who designs content is the first one who gets shot in the head when the business model starts to fail. So there&#39;s that aspect of it, too.</p>
<p>
	<font color="#333333" face="helvetica,arial, monaco,courier"><b>FEED:</b></font> What is the Web doing right? What&#39;s the part of the Web from the user&#39;s experience that you&#39;re most encouraged by?</p>
<p>
	<font color="#006699" face="Helvetica,Arial,Monaco,Courier"><b>NORMAN:</b></font> Encouraged?</p>
<p>
	<font color="#333333" face="helvetica,arial, monaco,courier"><b>FEED:</b></font> Is there anything?</p>
<p>
	<font color="#996633" face="Helvetica,Arial,Monaco,Courier"><b>LAUREL:</b></font> I&#39;m most encouraged by the diversity of stuff that&#39;s out there and the increasing competence of search engines to find it. Since word of mouth is such a strong way to market content, the only piece that remains to be filled in the whole puzzle to ensure that we have a medium that provides a lot of diversity, is an economic model that lets content creators actually earn a living doing it. And I think that&#39;s right around the corner.</p>
<p>
	<font color="#336633" face="Helvetica,Arial,Monaco,Courier"><b>NIELSEN:</b></font> I think that the strength of the Web really is the small, targeted Web sites that provide very specific services, and having systems like Google around or reputation management services like E-pinions is going to be helping that. I think there&#39;s been a period where only big Web sites work because people could only feel comfortable there. That is, in fact, a usability issue. If it&#39;s too difficult to go elsewhere, people will not go elsewhere. Once it becomes easy to go elsewhere, people will do that.</p>
<p>
	<font color="#006699" face="Helvetica,Arial,Monaco,Courier"><b>NORMAN:</b></font> It seems silly to say this, but the Web has transformed the world. It seems trite at this point, but I basically don&#39;t use any other reference material now. I search there and most of the time I find it. And do I complain about this site or that site? Yeah, but let&#39;s not overlook the fact that I find what I&#39;m looking for.</p>
<p>
	<font color="#336633" face="Helvetica,Arial,Monaco,Courier"><b>NIELSEN:</b></font> That&#39;s true. One of my radical suggestions for big companies is to say, no more print collateral. You know, acknowledge that from now on, whatever customers get at trade shows, goes directly into the circular filing cabinet. And when they want to know about your product, they look it up on the Web site. And yet, the budgets allocated to producing glossy brochures versus maintaining the Web site are completely out of whack, and one of the reasons you get so many bad corporate Web sites is that they have underfunded their online content -- and still, when they do a new product, they first make up all the glossy materials that nobody will read. And then they repurpose it onto their Web site. And sure enough, the Web site fails them in customer task support because of that.</p>
<p>
	<font color="#990000" face="Helvetica,Arial,Monaco,Courier"><b>TOGNAZZINI:</b></font> And so far as your question about what&#39;s good about it, which was the initial question... [Laughter]</p>
<p>
	<font color="#996633" face="Helvetica,Arial,Monaco,Courier"><b>LAUREL:</b></font> Jakob, you&#39;re being negative again. [Laughter]</p>
<p>
	<font color="#990000" face="Helvetica,Arial,Monaco,Courier"><b>TOGNAZZINI:</b></font> I think it&#39;s been a renaissance that I haven&#39;t seen since about &#39;77 in micros --</p>
<p>
	<font color="#996633" face="Helvetica,Arial,Monaco,Courier"><b>LAUREL:</b></font> Could we have a four-digit year, please? [Laughter]</p>
<p>
	<font color="#990000" face="Helvetica,Arial,Monaco,Courier"><b>TOGNAZZINI:</b></font> Right, 1977. The revolution that the Apple II and the Commodore Pet brought about is that, for the first time ever, almost, normal human beings could actually use a computer, any computer. It had a very simple BASIC language on it and most people could figure that out. They might have to take a short course, but they were on their way. And then they could do things with it.</p>
<p>
	<font color="#996633" face="Helvetica,Arial,Monaco,Courier"><b>LAUREL:</b></font> The Atari 400 belongs on that list.</p>
<p>
	<font color="#990000" face="Helvetica,Arial,Monaco,Courier"><b>TOGNAZZINI:</b></font> And the Atari 400. There were a bunch of them in there and then the TRS 80. Of course the Apple II was first. It got to the West Coast Computer Fair 23 hours before the Pet. [Laughter] This is why Apple got to say it was the first. [Laughter] That renaissance occurred, and then it got really hard. Macintosh is extremely difficult to program, even if you get one of those so-called simple BASIC languages. There&#39;s so much underpinnings and basement activities and overheads, it&#39;s just impractical. HTML opened the door again. And everybody came in, and all of a sudden there was this flowering, and that&#39;s why the graphic designers are doing this, because they can.</p>
<p>
	<font color="#006699" face="Helvetica,Arial,Monaco,Courier"><b>NORMAN:</b></font> HyperCard should be in there too.</p>
<p>
	<font color="#990000" face="Helvetica,Arial,Monaco,Courier"><b>TOGNAZZINI:</b></font> Yes.</p>
<p>
	<font color="#333333" face="helvetica,arial, monaco,courier"><b>FEED:</b></font> I lost my whole sophomore year in college to HyperCard.</p>
<p>
	<font color="#996633" face="Helvetica,Arial,Monaco,Courier"><b>LAUREL:</b></font> Yeah, you found yourself programming really stupid stuff, because it was so much fun.</p>
<p>
	<font color="#006699" face="Helvetica,Arial,Monaco,Courier"><b>NORMAN:</b></font> It&#39;s giving people the power to be in control. Their older machines all came in BASIC, the MacIntosh did not. HyperCard again gave power to the people. HTML again gave power to them.</p>
<p>
	<font color="#333333" face="helvetica,arial, monaco,courier"><b>FEED:</b></font> How about XML?</p>
<p>
	<font color="#336633" face="Helvetica,Arial,Monaco,Courier"><b>NIELSEN:</b></font> I think the benefit of XML is that it frees us from the proprietary binary formats. I think it will encourage more of the ASP model, which, again, I was critical of before. But in general, it&#39;s a positive trend and will allow small companies to actually produce very powerful Web sites with a lot of features. You know, programming is a matter of what are your primitives in the programming language. In BASIC, you know, it&#39;s &quot;add A to B&quot; or whatever. But it could also be &quot;add search engine here&quot; or, you know, &quot;add request marketing program here&quot; or, &quot;add affiliate program here,&quot; and things like that. All of which is barely possible today.</p>
<p>
	<font color="#006699" face="Helvetica,Arial,Monaco,Courier"><b>NORMAN:</b></font> XML, I think, is the future. But it&#39;s interesting, because it&#39;s a professional language. And HTML is an amateur&#39;s language. And HTML, remember, was really not made for what it&#39;s being used for. It was made so physicists could share documents. And then the graphic artists complained that they had lost control, and so we started adding little elements. I mean, appropriately. And that&#39;s not a complaint. In fact, they had, and that&#39;s why we all learned the tables -- we were regaining control -- and the purists said, no, tables are for tabular data. And we said, well, how do I make this on the right-hand side of the screen instead of the left? And XML fixes that.</p>
<p>
	But here&#39;s an experience we just had on our Web site -- we have lots of HTML coders, and they&#39;re pretty good. Now it turns out, though, that their HTML code is incredibly sloppy. They&#39;ve grown up being sloppy. And HTML and browsers have encouraged it. Because you can always say, &quot;start a paragraph&quot; -- you never have to say, &quot;end a paragraph.&quot; Right? Well, guess what? XML breaks that -- XML says, you&#39;re professionals, guys. If you start a paragraph, you&#39;ve got to end it. And you start something and you have to be strict about your use of code, and so what happened is, we had our courses working just fine and then we changed the delivery medium, which suddenly was strict about its interpretation of XML. And, whoops, nothing worked anymore. And I think all these amateurs who do their own Web sites have been killed by this. And I don&#39;t know if this is a good thing or a bad thing. It&#39;s good that we&#39;re starting to get some discipline in the language, but it means that it&#39;s going to be harder to teach yourself.</p>
<p>
	<font color="#003399" face="helvetica,arial,monaco,courier"><b>JEFFREY VEEN:</b></font> Well, one of the reasons HTML is so successful is because it&#39;s so simple and forgiving, right? The browser would do whatever it could do, never threw an error, right? The browser would say: I&#39;ll try to figure out no matter what you throw at me. And that&#39;s why it was so easy. You could make a Web page in two minutes, look at this, it&#39;s simple. And as you said, XML sort of breaks that. Most of the people who came from SGML or from serious publishing said, &quot;XML <i>fixes</i> that.&quot; [Laughter] And now we have parts of documents and they have structured code, finally. So I wonder if there will be sort of a split, right, where HTML sort of lives in Geocities land, and you can do whatever you want. But for those of us that want to do high-level professional publishing or application design or whatever, you have XML.</p>
<p>
	<font color="#006699" face="Helvetica,Arial,Monaco,Courier"><b>NORMAN:</b></font> By the way, it&#39;s interesting -- it fixes this or it breaks it -- it&#39;s really interesting because from the computer scientist point of view, it fixes it. And from the hacker&#39;s point of view, it breaks it. I don&#39;t know which side I&#39;m on.</p>
<p>
	<font color="#003399" face="helvetica,arial,monaco,courier"><b>VEEN:</b></font> I&#39;m totally on both sides.</p>
<p>
	<font color="#006699" face="Helvetica,Arial,Monaco,Courier"><b>NORMAN:</b></font> I&#39;m totally on both sides.</p>
<p>
	<font color="#336633" face="Helvetica,Arial,Monaco,Courier"><b>NIELSEN:</b></font> We want tools that free people from actually hand-writing HTML. The tools really do dictate the design to a great extent, but there are, you know, a lot of hidden aspects of Web sites, and if you only look at the WYSIWYG [What You See Is What You Get] editors, you&#39;re going to miss completely. There&#39;s no such thing as WYSIWYG within in the Web. Because the way it looks to your user it not going to be the way it looks to you. But secondly, a lot of the other information like page titles -- how it shows up in the search engines -- all of that is something that people very, very easily miss. And even on high-end professional Web sites, when you go into the search engines, you will find millions of documents that are called &quot;untitled document&quot; or you will find fifty pages with the same title so you can&#39;t differentiate. There are things like that. And one of the reasons is that they pay too much attention to how it should look when I look at the page, as opposed to the way the page will be <i>used,</i> which is a multiplicity of uses.</p>
<p>
	Actually, you know, I&#39;ve talked to a lot of my different clients and one of the things I asked them is, do you have a good production system that you like to use for content management and Web back-end. And I really haven&#39;t heard anybody say that they liked what they were using. So that&#39;s such a horrible area right now.</p>
<p>
	The second is, what is the usability applications of the tools? So many of the tools, for example, generate URLs that are a mile long and do not work for things like bookmarks or emailing -- for the social navigation of the Internet, which is: I email you where to go. The URLs generated by many of these tools break when you send them via email.</p>
<p>
	<font color="#006699" face="Helvetica,Arial,Monaco,Courier"><b>NORMAN:</b></font> That&#39;s not so much the fault of the tools, I think, as it is the problem of the Internet. It wasn&#39;t designed for this, okay? We basically want to pass arguments from one place to another. And that wasn&#39;t how the designers of the Web thought. And so we discovered that the URL, once the browser has successfully gotten it, ignores all the stuff on the right. So hey, we could use that to pass arguments. And that&#39;s why you get these unintelligible URLs.</p>
<p>
	It&#39;s just like in HyperCard, remember? You didn&#39;t have a way of storing variables. And so you discovered you could make these invisible cards. [Laughter]</p>
<p>
	<font color="#336633" face="Helvetica,Arial,Monaco,Courier"><b>NIELSEN:</b></font> At least it didn&#39;t impact the user.</p>
<p>
	<font color="#003399" face="helvetica,arial,monaco,courier"><b>VEEN:</b></font> Well, take a look at when Tim Berners-Lee talks about his first iteration of the Web project, in which the browser was also an editor, right? He never conceived of anything different: push a button and you can edit the document or view it. And he never thought people would ever look at or type URLs. They were just this hidden thing inside the browser and it was just all links.</p>
<p>
	<font color="#006699" face="Helvetica,Arial,Monaco,Courier"><b>NORMAN:</b></font> Because it was a list of papers -- I want to read this paper in physics, I click on the link, and so on.</p>
<p>
	<font color="#336633" face="Helvetica,Arial,Monaco,Courier"><b>NIELSEN:</b></font> But when you email a lot, the social navigation of the Web is really, really powerful. So you email people, and say go and look at this Web page, but then the frames will often break it, content management systems will often break it, even email itself will break it, because it puts a return in the middle of the URL. So that&#39;s one of the reasons you&#39;ve got to alter all these things so that they&#39;re less than 76 characters long. And why does it have to be less than 80 characters? Because that was an IBM punch card, which was designed to fit into the filing boxes, which were themselves the size of the old dollar bills, for which they had spare filing cabinets back in 1890.</p>
<p>
	<font color="#333333" face="helvetica,arial, monaco,courier"><b>FEED:</b></font> We&#39;re back to the punch cards. [Laughter]</p>
<p>
	<font color="#990000" face="Helvetica,Arial,Monaco,Courier"><b>TOGNAZZINI:</b></font> It&#39;s like the story I told today about Morse and all-caps. Morse chose to have all-caps for Morse code. He didn&#39;t want to do mixed case, because that would require extra keys or key presses. But he chose all caps because he had to be able to transmit the word &quot;God.&quot; And that continued all the way through the early micros, which were all caps, and they thought it was because it was more readable. But it&#39;s, in fact, less readable -- significantly less readable.</p>
<p>
	<font color="#336633" face="Helvetica,Arial,Monaco,Courier"><b>NIELSEN:</b></font> That is one of the reasons to try to get some of these design decisions right from the beginning. We often come across this notion that, oh, this will be fixed in the redesign. But quite often, once you build something, you&#39;re stuck with it for hundreds of years, like the 80-character limit.</p>
<p>
	<font color="#006699" face="Helvetica,Arial,Monaco,Courier"><b>NORMAN:</b></font> But that design decision was right. I mean, I agree with what you said in flavor, but just thinking about Morse -- to spell &quot;God&quot; in lower case in those days was blasphemy.</p>
<p>
	<font color="#990000" face="Helvetica,Arial,Monaco,Courier"><b>TOGNAZZINI:</b></font> Right. Particularly considering the first thing he was going to transmit was, &quot;What hath God wrought?&quot; Lower-case would have been more like &quot;God hath wrought this rail we&#39;re going to run you out of town on.&quot; [Laughter]</p>

            </div>
        </article>
    </div>
</div>


    </div>
    




<footer id="page-footer">
    <div class="row hide-for-print">
        <div class="column small-12 medium-6">
            <section class="footer-popular">
                
                <h1>Popular Topics</h1>
                <div style="float: left;  width: 15em; display: block;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../../topic/applications/index.php">Application Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../../topic/e-commerce/index.php">E-Commerce Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../../topic/eyetracking/index.php">Eyetracking </a></li>
	<li style="padding-bottom: .5em;"><a href="../../../topic/human-computer-interaction/index.php">Human Computer Interaction </a></li>
	<li style="padding-bottom: .5em;"><a href="../../../topic/information-architecture/index.php">Information Architecture </a></li>
	<li style="padding-bottom: .5em;"><a href="../../../topic/intranets/index.php">Intranet Design </a></li>
	<li><a href="../../../topic/interaction-design/index.php">Interaction Design </a></li>
</ul>
</div>

<div style="float: left;  width: 15em; display: block; list-style: none;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../../topic/mobile-and-tablet-design/index.php">Mobile and Tablet Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../../topic/credibility-persuasion/index.php">Persuasive Design</a></li>
	<li style="padding-bottom: .5em;"><a href="../../../topic/research-methods/index.php">Research Methods</a></li>
	<li style="padding-bottom: .5em;"><a href="../../../topic/user-testing/index.php">User Testing </a></li>
	<li style="padding-bottom: .5em;"><a href="../../../topic/web-usability/index.php">Web Usability </a></li>
	<li style="padding-bottom: .5em;"><a href="../../../topic/writing-web/index.php">Web Writing &amp; Content Strategy </a></li>
</ul>
</div>

<p>&nbsp;</p>

                
            </section>
        </div>

        <div class="column small-12 medium-6">
            <div class="row">
                <div class="column small-6">
                    <section class="footer-about">
                        <h1>Follow Us</h1>
                        <ul>
	<li><a href="../../../articles/subscribe/index.php">Subscribe to our weekly UX newsletter</a>,&nbsp;Jakob Nielsen&#39;s Alertbox</li>
	<li>Visit Don Norman&#39;s website, <a href="http://www.jnd.org/"> www.jnd.org </a></li>
	<li>Visit Bruce Tognazzini&#39;s website, <a href="http://www.asktog.com/"> www.asktog.com </a></li>
	<li style="padding-top: 6px;"><a class="twitter-follow-button" data-show-count="false" data-size="large" href="https://twitter.com/nngroup"><img alt="Follow @nngroup on Twitter" height="20" src="https://media.nngroup.com/media/editor/2016/12/30/twitter-follow-button.png" style="width: 117px; height: 20px;" width="117" /></a></li>
	<li style="padding-top: 6px;"><img alt="" height="14" src="https://media.nngroup.com/media/editor/2016/07/26/feed-icon-14x14.png" style="vertical-align:text-top;width:14px;margin-right:.25em;" width="14" /><a href="../../../feed/rss/index.php">Subscribe to our RSS feed</a></li>
</ul>

                    </section>
                </div>

                <div class="column small-6">
                    <section class="footer-about right-column">
                        <h1>About
</h1>
                        <ul class="no-bullet">
	<li><a href="../../../about/contact/index.php">Contact information</a></li>
	<li><a href="../../../about/index.php">About Us</a></li>
	<li><a href="../../../about/why-nng/index.php">Why NN/g? </a></li>
	<li><a href="../../../return-policy/index.php">Return Policy</a></li>
</ul>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="copyright column small-12">
            <p><a href="../../../copyright-and-reprint-info/index.php">Copyright</a> &copy; 1998-2017 Nielsen Norman Group, All Rights Reserved.</p>
        </div>
    </div>
</footer>


    


        <script src="https://media.nngroup.com/static/vendor/fastclick/js/fastclick.min.js"></script>
        <script src="https://media.nngroup.com/static/vendor/foundation/js/foundation.min.js"></script>
        <script src="http://media.nngroup.com/static/js/app.js"></script>
        <script src="http://media.nngroup.com/static/js/scroll-to-fixed.js"></script><script>var externalLinks = document.querySelectorAll('a[href^="http"]');for (var i = externalLinks.length-1; i >= 0; i--) {externalLinks[i].addEventListener("click", function(event) { alert("External links are disabled for this test."); event.preventDefault(); }, false);}var mailLinks = document.querySelectorAll('a[href^="mailto"]');for (var i = mailLinks.length-1; i >= 0; i--) {mailLinks[i].addEventListener("click", function(event) { alert("Email links are disabled for this test."); event.preventDefault(); }, false);}var externalForms = document.querySelectorAll('form');for (var i = externalForms.length-1; i >= 0; i--) {externalForms[i].addEventListener("submit", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false); externalForms[i].addEventListener("click", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false);}</script>

        <script>
            
            $(function() {
                $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
                $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
            });
        </script>

        


        
        <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1072699960;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072699960/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
    </body>

<!-- Mirrored from www.nngroup.com/news/item/feed-magazine-interview-keynote-speakers-ux/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:15:50 GMT -->
</html>
