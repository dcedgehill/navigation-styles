<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->





    
<!-- Mirrored from www.nngroup.com/books-prioritizing-web-usability-toc-spanish/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:13:19 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1026.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"beacon":"bam.nr-data.net","queueTime":12,"applicationTime":121,"agent":"","licenseKey":"1a30ef9931","errorBeacon":"bam.nr-data.net","applicationID":"2048992","transactionName":"NFxSMRBUX0EDUkIPDA0WdhAMVkVbDV8ZFgIEXFIMFkYfRAtUQRVZM1hXADRcVEVMVlMS"}</script>
        <title>Prioritizing Web Usability book: Spanish Translation</title><meta property="og:title" content="Prioritizing Web Usability book: Spanish Translation" />
  
        
        <meta name="description" content="Spanish Translation of Prioritizing Web Usability, book by Nielsen Norman Group">
        <meta property="og:description" content="Spanish Translation of Prioritizing Web Usability, book by Nielsen Norman Group" />
        
  
        
	
        

        

        <meta name="robots" content="noarchive">
        <meta name="viewport" content="width=device-width">

        <link rel="search" type="application/opensearchdescription+xml" title="NN/g Search" href="../opensearch.xml" />
        <link rel="shortcut icon" href="http://media.nngroup.com/static/img/favicon.ico" />

        <link rel="stylesheet" href="https://media.nngroup.com/static/vendor/font-awesome/css/font-awesome.min.css">

        
        <link rel="stylesheet" href="https://media.nngroup.com/static/css/7232a960f1df.css" type="text/css" />
        <link rel="stylesheet" href="http://fast.fonts.com/t/1.css?apiType=css&amp;projectid=c5846260-9def-46ec-9407-0f1349bd2d64" type="text/css">

        <script src="https://media.nngroup.com/static/vendor/modernizr/js/modernizr.min.js"></script>

        <script type="text/javascript" src="https://media.nngroup.com/static/js/3283be5def74.js"></script>
        

        

        
        
        <!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=229034,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('http://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->


<!-- Google Analytics Tracking Code for http://nngroup.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23132681-1', 'nngroup.com');
  ga('require', 'linkid', 'https://www.nngroup.com/books-prioritizing-web-usability-toc-spanish/linkid.js');
  ga('require', 'displayfeatures');

// Optimizely Universal Analytics Integration
 window.optimizely = window.optimizely || [];
 window.optimizely.push("activateUniversalAnalytics");

  ga('send', 'pageview');

</script>
<script>
/**
* Function to track a click on an outbound link in Google Analytics.
*/
function trackOutboundLink(label, url) {
    if(window.ga && ga.loaded) {
        ga('send', 'event', 'outbound', 'click', label, {'hitCallback': function(){document.location = url;}
        });
    } else {
        document.location = url;
    }
}
/**
* Function to track a click on a link in the article announcement area.
*/
$('#articleAnnouncement a').on('click', function(){
  ga('send', 'event', 'link', 'click', 'articleAnnouncement');
});
</script>

<script type="text/javascript">
 setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2421.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



<!-- Hotjar Tracking Code for http://nngroup.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:121605,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

        <script src="https://media.nngroup.com/static/js/analytics/track_outbound_link.js"></script>
        
        
        
    </head>
    <body class="location-about">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
    <header id="page-header">
        <div id="cart" class="shopping-cart mobile-cart show-for-small-down"></div>
        <div class="row">
            <div id="masthead" class="masthead small-12 medium-7 column">

                <!-- The button to toggles menu on small screen -->

                <h1><a href="../index.php"><img src="http://media.nngroup.com/static/img/logo-wordmark.png" width="74" height="30"> Nielsen <span class="norman">Norman</span> Group</a></h1>
                <p class="tagline show-for-medium-up">Evidence-Based User Experience Research, Training, and Consulting</p>

                <!-- The button to toggles form search on small screen -->


                <div style="clear: right;width:  100%;height: 1px;"></div><?php define('ROOT_PATH',$_SERVER["DOCUMENT_ROOT"] . '/apps/navigation_styles/navigations/3'); ?><?php include(ROOT_PATH)."/navigation.php"; ?>

                <!-- Drop down menu on small screen -->
                <div id="hideNav" class="show-for-small-down">
                  
                  <ul class="menu-content">
                    <li><a href="../index.php">Home</a></li>
                    <li class="has-submenu">
                      <a>Training</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../training/index.php">Overview</a></li>
                        
<li id="nav-ux-conference"><a href="../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../online-seminars/index.php"><span>Online Seminars</span></a></li>

                      </ul>
                    </li>
                    <li class="has-submenu">
                      <a>Consulting</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
  <li><a href="../consulting/index.php">Overview</a></li>
  
  <li><a href="../consulting/benchmarking/index.php">Benchmarking</a></li>
  
  <li><a href="../consulting/expert-design-review/index.php">Expert Design Review</a></li>
  
  <li><a href="../consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
  
  <li><a href="../consulting/usability-testing/index.php">Usability Testing </a></li>
  
  <li><a href="../consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
  
  <li><a href="../in-house-training/index.php">In-House Training</a></li>
</ul>

                    </li>

                    <li><a href="../reports/index.php">Reports</a></li>
                    <li><a href="../articles/index.php">Articles</a></li>
                    <li class="has-submenu"><a>About NN/G</a><i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../about/index.php">Overview</a></li>
                        <li><a href="../people/index.php">People</a></li>
                        <li><a href="../about/why-nng/index.php">Why NN/g?</a></li>
                        <li><a href="../about/contact/index.php">Contact</a></li>
                        <li><a href="../news/index.php">News</a></li>
                        <li><a href="../about/history/index.php">History</a></li>
                        <li><a href="../books/index.php">Books</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                </div>

                <hr class="header-divider show-for-small-only"/>


            </div>
       </div>

        <div id="hideNav">            <nav class="primary-nav small-12 column">
                <ul>
                    <li><a class="navitem" id="nav-home" href="../index.php">Home</a></li>
                    <li><a class="navitem" id="nav-training" href="../training/index.php">Training</a></li>
                    <li><a class="navitem" id="nav-consulting" href="../consulting/index.php">Consulting</a></li>
                    <li><a class="navitem" id="nav-reports" href="../reports/index.php">Reports</a></li>
                    <li><a class="navitem" id="nav-articles" href="../articles/index.php">Articles</a></li>
                    <li><a class="navitem" id="nav-about" href="../about/index.php">About NN/G</a></li>
                </ul>
            </nav>
            <nav class="secondary-nav small-12 medium-12 large-12 columns">
                
                
            </nav>
        </div>
    </header>
    <div id="mainContent" class="l-content">
    
<div class="row">
    <div class="small-12 column l-subsection">
        <h1>
 <cite>
  Usabilidad, Prioridad en el dise&ntilde;o Web
 </cite>
 : Spanish Translation of
 <cite>
  Prioritizing Web Usability
 </cite>
</h1>
<div style="float: right; width: 200px; margin: 0 0 1ex 1.5ex">
 <a href="http://www.agapea.com/Usabilidad-Prioridad-en-el-diseno-Web-n629275i.htm" title="Book page at Agapea">
  <img alt="Usabilidad: Prioridad en el diseño Web - Spanish Book Cover" src="http://media.nngroup.com.s3.amazonaws.com/media/editor/2012/12/31/prioritizing_web_usability_spanish-cover-small.jpg" style="width: 200px; height: 258px;" />
  <cite>
   Usabilidad: Prioridad en el dise&ntilde;o Web
  </cite>
 </a>
 <br />
 ISBN 84-415-2092-9
</div>
<p>
 Spanish table of contents for
 <a href="../books/prioritizing-web-usability/index.php">
  Prioritizing Web Usability
 </a>
 , by Jakob Nielsen and Hoa Loranger:
</p>
<p>
 <span lang="ES">
  Agradecimientos
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sobre los autores
  <br />
  <br />
  PREFACIO
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Usabilidad: Entonces y ahora
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&iquest;Qui&eacute;n deber&iacute;a leer este libro?
  <br />
  <br />
  CAP&Iacute;TULO 1. INTRODUCCI&Oacute;N: NADA QUE ESCONDER
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;D&oacute;nde conseguimos nuestros datos
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;C&oacute;mo realizamos el estudio para el libro
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&iquest;Qu&eacute; ocurre si el sitio Web ha cambiado?
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;D&iacute;gamelo otra vez: &iquest;Por qu&eacute; necesito hacer una prueba de usuario?
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Las excepciones
  <br />
  <br />
  CAP&Iacute;TULO 2. LA EXPERIENCIA DEL USUARIO WEB
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&iquest;Se usa bien la Web?
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tasas de &eacute;xito en la Web global
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&Eacute;xito seg&uacute;n el nivel de experiencia
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Satisfacci&oacute;n del usuario con los sitios Web
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;C&oacute;mo se utilizan los sitios
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La p&aacute;gina principal: Tanto que decir en tan poco tiempo
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Comportamiento en las p&aacute;ginas interiores
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Predominio de b&uacute;squeda
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;C&oacute;mo se usa la p&aacute;gina de resultados del motor de b&uacute;squeda
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Utilizar el precio de las palabras claves para estimar las mejoras de usabilidad
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Desplazamientos
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Cumplir las convenciones de dise&ntilde;o y las directrices de usabilidad
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Rastreo de informaci&oacute;n
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;El rastro de la informaci&oacute;n: Predecir el &eacute;xito de una ruta
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Selecci&oacute;n de la dieta: Qu&eacute; sitios visitar
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Abandonar el coto: Cu&aacute;ndo cazar en otro sitio
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Comportamiento de navegaci&oacute;n de los depredadores de informaci&oacute;n
  <br />
  <br />
  CAP&Iacute;TULO 3. REVISANDO LOS PRIMEROS HALLAZGOS DE USABILIDAD WEB
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ocho problemas que no han cambiado
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Enlaces que no cambian de color cuando se visitan
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Inhabilitaci&oacute;n del bot&oacute;n Atr&aacute;s
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Abrir nuevas ventanas de navegaci&oacute;n
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ventanas emergentes
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Elementos de dise&ntilde;o que parecen publicidad
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Violaci&oacute;n de las convenciones globales de la Web
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Contenido et&eacute;reo y promoci&oacute;n vana
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Contenido denso y texto extenso
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Cambio tecnol&oacute;gico: Su impacto en la usabilidad
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tiempo lento de descarga
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Marcos
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Flash
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Listados de b&uacute;squeda de baja relevancia
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Multimedia y v&iacute;deos largos
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Dise&ntilde;os congelados
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Incompatibilidad de plataformas m&uacute;ltiples
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Adaptaci&oacute;n: C&oacute;mo los usuarios han influido en la usabilidad
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Posibilidad incierta de hacer clic
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Enlaces que no son azules
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Desplazamiento
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Inscripci&oacute;n
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;URL complejos
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Men&uacute;s desplegables y en cascada
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Limitaciones: C&oacute;mo los dise&ntilde;adores han aliviado los problemas de usabilidad
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Plug-In y tecnolog&iacute;a Bleeding-Edge
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Interfaz de Usuario en 3D
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Dise&ntilde;o relleno
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;P&aacute;ginas splash
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Gr&aacute;ficos en movimiento y desplazamiento del texto
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Artilugios de interfaz gr&aacute;fica de usuario (GUI) de dise&ntilde;o propio
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No mostrar qui&eacute;n est&aacute; detr&aacute;s de la informaci&oacute;n
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Palabras inventadas
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Contenido obsoleto
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Inconsistencia dentro de un sitio Web
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Peticiones prematuras de datos personales
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sitios m&uacute;ltiples
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;P&aacute;ginas hu&eacute;rfanas
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Evaluaci&oacute;n del destino de los primeros hallazgos
  <br />
  <br />
  CAP&Iacute;TULO 4. PRIORIZANDO SUS PROBLEMAS DE USABILIDAD
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Lo que hace que un problema sea serio
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La escala de la infelicidad
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Por qu&eacute; fracasan los usuarios
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&iquest;Es suficiente centrarse en los problemas m&aacute;s graves
  <br />
  <br />
  CAP&Iacute;TULO 5. B&Uacute;SQUEDA
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;El estado de la b&uacute;squeda
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;C&oacute;mo deber&iacute;a funcionar un buscador
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Interfaz de b&uacute;squeda
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Longitud de la consulta y ancho del cuadro de b&uacute;squeda
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;B&uacute;squeda avanzada
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;P&aacute;ginas de resultados del motor de b&uacute;squeda
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Las mejores apuestas
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Clasificaci&oacute;n de la SERP
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No se encontraron resultados
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Se encontr&oacute; un resultado
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Optimizaci&oacute;n del motor de b&uacute;squeda
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SEO ling&uuml;&iacute;stica
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SEO Arquitect&oacute;nico
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Reputaci&oacute;n SEO
  <br />
  <br />
  CAP&Iacute;TULO 6. NAVEGACI&Oacute;N Y ARQUITECTURA DE LA INFORMACI&Oacute;N
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&iquest;Ya he llegado?
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ajustar la estructura del sitio con las expectativas del usuario
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Navegaci&oacute;n: Sea consistente
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Navegaci&oacute;n: Cuidado con el factor modernidad
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Reducir el desorden y evitar la redundancia
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Enlaces y nombres de etiquetas: Sea espec&iacute;fico
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Men&uacute;s desplegables verticales: Lo breve es bello
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Men&uacute;s multi-nivel: Menos es m&aacute;s
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&iquest;Puedo hacer clic aqu&iacute;?
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Acceso directo desde la p&aacute;gina de inicio
  <br />
  <br />
  CAP&Iacute;TULO 7. TIPOGRAF&Iacute;A: LEGIBILIDAD
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Cuerpo del texto: La regla de los diez puntos
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La edad no es el problema
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Planificando las diferencias de hardware
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Resoluciones de pantalla t&iacute;picas
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Especificaciones relativas
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Dise&ntilde;os para usuarios con problemas de visi&oacute;n
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Escoger las fuentes
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Cuando tenga dudas, utilice Verdana
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Mezclar fuentes y colores
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Contraste entre texto y fondo
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Daltonismo com&uacute;n
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Im&aacute;genes de texto
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Texto en movimiento
  <br />
  <br />
  CAP&Iacute;TULO 8. ESCRIBIR PARA LA WEB
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;C&oacute;mo una mala redacci&oacute;n causa el fracaso de los sitios Web
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Comprender c&oacute;mo leen los usuarios
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Escribir para sus lectores
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Utilice lenguaje sencillo
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Modere la exageraci&oacute;n del marketing
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Resuma los puntos clave y vaya al grano
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Formatear el texto para conseguir legibilidad
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Palabras clave resaltadas
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Utilice t&iacute;tulos y encabezados concisos y descriptivos
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Utilice listas de boliches y numeradas
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Redacte p&aacute;rrafos cortos
  <br />
  <br />
  CAP&Iacute;TULO 9. OFRECER BUENA INFORMACI&Oacute;N DE PRODUCTO
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ens&eacute;&ntilde;eme el dinero
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sin excusas
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Muestre los costes adicionales
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;G&aacute;nese la confianza del cliente
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Describa el producto
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Muestre im&aacute;genes e ilustraciones de producto
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;P&aacute;ginas de producto en capas
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Demuestre buena fe
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;D&eacute; soporte a la venta comparada
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Refinado y clasificaci&oacute;n
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Respalde las ventas con contenido de calidad
  <br />
  <br />
  CAP&Iacute;TULO 10. PRESENTACI&Oacute;N DE ELEMENTOS DE P&Aacute;GINA
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&iquest;Dise&ntilde;os con desplazamiento de texto?
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Guiar al usuario, paso a paso
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Mantenga cercanos los an&aacute;logos
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Formularios de formato descuidado
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Cumpla con las expectativas de sus usuarios
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Utilizar el espacio en blanco
  <br />
  <br />
  CAP&Iacute;TULO 11. EQUILIBRIO ENTRE TECNOLOG&Iacute;A Y NECESIDADES DEL USUARIO
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Utilice multimedia cuando beneficie a su audiencia
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Venciendo las barreras multimedia
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ad&aacute;ptese a los usuarios menos modernizados
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Dise&ntilde;e teniendo en cuenta la velocidad de conexi&oacute;n de su audiencia
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ofrezca un indicador de estado de descarga sencillo y preciso
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No sobrestime el conocimiento tecnol&oacute;gico de sus usuarios
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Detectar el ancho de banda del usuario
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Utilice las convenciones conocidas para interfaces
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Evite los excesos multimedia
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Baje el volumen
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Crear v&iacute;deos para la Web
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La pr&aacute;ctica de la sencillez
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Hacia un dise&ntilde;o m&aacute;s elegante
  <br />
  <br />
  CAP&Iacute;TULO 12. PENSAMIENTOS FINALES: UN DISE&Ntilde;O QUE FUNCIONA
  <br />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ponga a prueba sus hip&oacute;tesis
  <br />
  <br />
  &Iacute;NDICE ALFAB&Eacute;TICO
 </span>
</p>

    </div>
</div>

    </div>
    




<footer id="page-footer">
    <div class="row hide-for-print">
        <div class="column small-12 medium-6">
            <section class="footer-popular">
                
                <h1>Popular Topics</h1>
                <div style="float: left;  width: 15em; display: block;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../topic/applications/index.php">Application Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../topic/e-commerce/index.php">E-Commerce Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../topic/eyetracking/index.php">Eyetracking </a></li>
	<li style="padding-bottom: .5em;"><a href="../topic/human-computer-interaction/index.php">Human Computer Interaction </a></li>
	<li style="padding-bottom: .5em;"><a href="../topic/information-architecture/index.php">Information Architecture </a></li>
	<li style="padding-bottom: .5em;"><a href="../topic/intranets/index.php">Intranet Design </a></li>
	<li><a href="../topic/interaction-design/index.php">Interaction Design </a></li>
</ul>
</div>

<div style="float: left;  width: 15em; display: block; list-style: none;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../topic/mobile-and-tablet-design/index.php">Mobile and Tablet Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../topic/credibility-persuasion/index.php">Persuasive Design</a></li>
	<li style="padding-bottom: .5em;"><a href="../topic/research-methods/index.php">Research Methods</a></li>
	<li style="padding-bottom: .5em;"><a href="../topic/user-testing/index.php">User Testing </a></li>
	<li style="padding-bottom: .5em;"><a href="../topic/web-usability/index.php">Web Usability </a></li>
	<li style="padding-bottom: .5em;"><a href="../topic/writing-web/index.php">Web Writing &amp; Content Strategy </a></li>
</ul>
</div>

<p>&nbsp;</p>

                
            </section>
        </div>

        <div class="column small-12 medium-6">
            <div class="row">
                <div class="column small-6">
                    <section class="footer-about">
                        <h1>Follow Us</h1>
                        <ul>
	<li><a href="../articles/subscribe/index.php">Subscribe to our weekly UX newsletter</a>,&nbsp;Jakob Nielsen&#39;s Alertbox</li>
	<li>Visit Don Norman&#39;s website, <a href="http://www.jnd.org/"> www.jnd.org </a></li>
	<li>Visit Bruce Tognazzini&#39;s website, <a href="http://www.asktog.com/"> www.asktog.com </a></li>
	<li style="padding-top: 6px;"><a class="twitter-follow-button" data-show-count="false" data-size="large" href="https://twitter.com/nngroup"><img alt="Follow @nngroup on Twitter" height="20" src="https://media.nngroup.com/media/editor/2016/12/30/twitter-follow-button.png" style="width: 117px; height: 20px;" width="117" /></a></li>
	<li style="padding-top: 6px;"><img alt="" height="14" src="https://media.nngroup.com/media/editor/2016/07/26/feed-icon-14x14.png" style="vertical-align:text-top;width:14px;margin-right:.25em;" width="14" /><a href="../feed/rss/index.php">Subscribe to our RSS feed</a></li>
</ul>

                    </section>
                </div>

                <div class="column small-6">
                    <section class="footer-about right-column">
                        <h1>About
</h1>
                        <ul class="no-bullet">
	<li><a href="../about/contact/index.php">Contact information</a></li>
	<li><a href="../about/index.php">About Us</a></li>
	<li><a href="../about/why-nng/index.php">Why NN/g? </a></li>
	<li><a href="../return-policy/index.php">Return Policy</a></li>
</ul>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="copyright column small-12">
            <p><a href="../copyright-and-reprint-info/index.php">Copyright</a> &copy; 1998-2017 Nielsen Norman Group, All Rights Reserved.</p>
        </div>
    </div>
</footer>


    


        <script src="https://media.nngroup.com/static/vendor/fastclick/js/fastclick.min.js"></script>
        <script src="https://media.nngroup.com/static/vendor/foundation/js/foundation.min.js"></script>
        <script src="http://media.nngroup.com/static/js/app.js"></script>
        <script src="http://media.nngroup.com/static/js/scroll-to-fixed.js"></script><script>var externalLinks = document.querySelectorAll('a[href^="http"]');for (var i = externalLinks.length-1; i >= 0; i--) {externalLinks[i].addEventListener("click", function(event) { alert("External links are disabled for this test."); event.preventDefault(); }, false);}var mailLinks = document.querySelectorAll('a[href^="mailto"]');for (var i = mailLinks.length-1; i >= 0; i--) {mailLinks[i].addEventListener("click", function(event) { alert("Email links are disabled for this test."); event.preventDefault(); }, false);}var externalForms = document.querySelectorAll('form');for (var i = externalForms.length-1; i >= 0; i--) {externalForms[i].addEventListener("submit", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false); externalForms[i].addEventListener("click", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false);}</script>

        <script>
            
            $(function() {
                $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
                $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
            });
        </script>

        


        
        <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1072699960;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072699960/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
    </body>

<!-- Mirrored from www.nngroup.com/books-prioritizing-web-usability-toc-spanish/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:13:19 GMT -->
</html>
