<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->





    
<!-- Mirrored from www.nngroup.com/books/homepage-usability/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:02:37 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1026.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"licenseKey":"1a30ef9931","errorBeacon":"bam.nr-data.net","applicationID":"2048992","transactionName":"NFxSMRBUX0EDUkIPDA0WdhAMVkVbDV8ZBAwMUkNLFFxURRELdAkMCH1VEQNcXRwFVEI=","beacon":"bam.nr-data.net","queueTime":3,"applicationTime":411,"agent":""}</script>
        <title>Homepage Usability: Book by Jakob Nielsen and Marie Tahir</title><meta property="og:title" content="Homepage Usability: Book by Jakob Nielsen and Marie Tahir" />
  
        
        <meta name="description" content="Jakob Nielsen&#39;s book contains 113 design guidelines for homepages and detailed reviews of 50 major sites. Links to buy the book online.">
        <meta property="og:description" content="Jakob Nielsen&#39;s book contains 113 design guidelines for homepages and detailed reviews of 50 major sites. Links to buy the book online." />
        
  
        
	
        

        

        <meta name="robots" content="noarchive">
        <meta name="viewport" content="width=device-width">

        <link rel="search" type="application/opensearchdescription+xml" title="NN/g Search" href="../../opensearch.xml" />
        <link rel="shortcut icon" href="http://media.nngroup.com/static/img/favicon.ico" />

        <link rel="stylesheet" href="https://media.nngroup.com/static/vendor/font-awesome/css/font-awesome.min.css">

        
        <link rel="stylesheet" href="https://media.nngroup.com/static/css/7232a960f1df.css" type="text/css" />
        <link rel="stylesheet" href="http://fast.fonts.com/t/1.css?apiType=css&amp;projectid=c5846260-9def-46ec-9407-0f1349bd2d64" type="text/css">

        <script src="https://media.nngroup.com/static/vendor/modernizr/js/modernizr.min.js"></script>

        <script type="text/javascript" src="https://media.nngroup.com/static/js/3283be5def74.js"></script>
        

        

        
        
        <!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=229034,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('http://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->


<!-- Google Analytics Tracking Code for http://nngroup.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23132681-1', 'nngroup.com');
  ga('require', 'linkid', 'https://www.nngroup.com/books/homepage-usability/linkid.js');
  ga('require', 'displayfeatures');

// Optimizely Universal Analytics Integration
 window.optimizely = window.optimizely || [];
 window.optimizely.push("activateUniversalAnalytics");

  ga('send', 'pageview');

</script>
<script>
/**
* Function to track a click on an outbound link in Google Analytics.
*/
function trackOutboundLink(label, url) {
    if(window.ga && ga.loaded) {
        ga('send', 'event', 'outbound', 'click', label, {'hitCallback': function(){document.location = url;}
        });
    } else {
        document.location = url;
    }
}
/**
* Function to track a click on a link in the article announcement area.
*/
$('#articleAnnouncement a').on('click', function(){
  ga('send', 'event', 'link', 'click', 'articleAnnouncement');
});
</script>

<script type="text/javascript">
 setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2421.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



<!-- Hotjar Tracking Code for http://nngroup.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:121605,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

        <script src="https://media.nngroup.com/static/js/analytics/track_outbound_link.js"></script>
        
        
        
    </head>
    <body class="location-about location-books book-detail">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
    <header id="page-header">
        <div id="cart" class="shopping-cart mobile-cart show-for-small-down"></div>
        <div class="row">
            <div id="masthead" class="masthead small-12 medium-7 column">

                <!-- The button to toggles menu on small screen -->

                <h1><a href="../../index.php"><img src="http://media.nngroup.com/static/img/logo-wordmark.png" width="74" height="30"> Nielsen <span class="norman">Norman</span> Group</a></h1>
                <p class="tagline show-for-medium-up">Evidence-Based User Experience Research, Training, and Consulting</p>

                <!-- The button to toggles form search on small screen -->


                <div style="clear: right;width:  100%;height: 1px;"></div><?php define('ROOT_PATH',$_SERVER["DOCUMENT_ROOT"] . '/apps/navigation_styles/navigations/3'); ?><?php include(ROOT_PATH)."/navigation.php"; ?>

                <!-- Drop down menu on small screen -->
                <div id="hideNav" class="show-for-small-down">
                  
                  <ul class="menu-content">
                    <li><a href="../../index.php">Home</a></li>
                    <li class="has-submenu">
                      <a>Training</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../training/index.php">Overview</a></li>
                        
<li id="nav-ux-conference"><a href="../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../online-seminars/index.php"><span>Online Seminars</span></a></li>

                      </ul>
                    </li>
                    <li class="has-submenu">
                      <a>Consulting</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
  <li><a href="../../consulting/index.php">Overview</a></li>
  
  <li><a href="../../consulting/benchmarking/index.php">Benchmarking</a></li>
  
  <li><a href="../../consulting/expert-design-review/index.php">Expert Design Review</a></li>
  
  <li><a href="../../consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
  
  <li><a href="../../consulting/usability-testing/index.php">Usability Testing </a></li>
  
  <li><a href="../../consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
  
  <li><a href="../../in-house-training/index.php">In-House Training</a></li>
</ul>

                    </li>

                    <li><a href="../../reports/index.php">Reports</a></li>
                    <li><a href="../../articles/index.php">Articles</a></li>
                    <li class="has-submenu"><a>About NN/G</a><i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../about/index.php">Overview</a></li>
                        <li><a href="../../people/index.php">People</a></li>
                        <li><a href="../../about/why-nng/index.php">Why NN/g?</a></li>
                        <li><a href="../../about/contact/index.php">Contact</a></li>
                        <li><a href="../../news/index.php">News</a></li>
                        <li><a href="../../about/history/index.php">History</a></li>
                        <li><a href="../index.php">Books</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                </div>

                <hr class="header-divider show-for-small-only"/>


            </div>
       </div>

        <div id="hideNav">            <nav class="primary-nav small-12 column">
                <ul>
                    <li><a class="navitem" id="nav-home" href="../../index.php">Home</a></li>
                    <li><a class="navitem" id="nav-training" href="../../training/index.php">Training</a></li>
                    <li><a class="navitem" id="nav-consulting" href="../../consulting/index.php">Consulting</a></li>
                    <li><a class="navitem" id="nav-reports" href="../../reports/index.php">Reports</a></li>
                    <li><a class="navitem" id="nav-articles" href="../../articles/index.php">Articles</a></li>
                    <li><a class="navitem" id="nav-about" href="../../about/index.php">About NN/G</a></li>
                </ul>
            </nav>
            <nav class="secondary-nav small-12 medium-12 large-12 columns">
                
<ul class="inline-list">
    <li id="nav-people"><a href="../../people/index.php"><span>People</span></a></li>
    <li id="nav-whynng"><a href="../../about/why-nng/index.php"><span>Why NN/g?</span></a></li>
    <li id="nav-contact"><a href="../../about/contact/index.php"><span>Contact</span></a></li>
    <li id="nav-news"><a href="../../news/index.php"><span>News</span></a></li>
    <li id="nav-history"><a href="../../about/history/index.php"><span>History</span></a></li>
    <li id="nav-books"><a href="../index.php"><span>Books</span></a></li>
</ul>

            </nav>
        </div>
    </header>
    <div id="mainContent" class="l-content">
    
<div class="row">
  <div class="medium-2 columns hide-for-small-down">
    <img class="book-cover" src="https://media.nngroup.com/media/publications/books/homepage.jpg.300x400_q95_autocrop_crop_upscale.jpg">
  </div>
  <div class="small-12 medium-6 columns l-subsection">
    <h1>Homepage Usability: 50 Websites Deconstructed</h1>

    <img class="show-for-small-down book-cover" src="https://media.nngroup.com/media/publications/books/homepage.jpg.300x400_q95_autocrop_crop_upscale.jpg">
    <h2>
<a href="../../people/jakob-nielsen/index.php">Jakob Nielsen</a>
            

        
             and 
            
                Marie Tahir
, 2001</h2>

    

    <p>Homepages are the most valuable real estate in the world. Millions of dollars are funneled through a space that&#39;s not even a square foot in size. The homepage is also your company&#39;s face to the world. Potential customers look at your company&#39;s online presence before doing any business with you. Complexity or confusion make people go away. Of course, all other aspects of bad web design should be fixed as well, but if the homepage doesn&#39;t communicate what users can do and why they should care about the website, you might as well not have a website at all. That&#39;s why homepage usability is so important and that&#39;s why we wrote a book specifically about this one topic.</p>

<p>We followed on our own principles when naming the book. Most people should be able to figure out what a book called Homepage Usability is about. Two words, and you know the topic. (New Riders Publishing.)</p>

<h2>Study Guide</h2>

<p><a href="../../books-homepage-usability-study-guide/index.php">Four exercises </a> &nbsp;to accompany the book for use by corporate reading groups or individual home study.</p>

<h2>Press</h2>

<p>Winner of the Independent Publisher Book Award for best book of the year in the &quot;computer/Internet&quot; category. Award presented at BookExpo America.</p>

<p><a href="http://www.netmechanic.com/news/vol5/review_no21.htm">NetMechanic </a> : &quot;You&#39;ll probably have it [the book&#39;s checklist] taped next to your monitor soon after finishing the book ... excellent resource and well worth the cost.&quot;</p>

<p><a href="http://www.ablestable.com/products/books/reference/reviews/nielsen-tahir.htm">AbleStable </a> : &quot;***** rating ... &quot;a truly wonderful book ... great resource for anyone wanting to learn about the strengths and weaknesses of homepage web design.&quot;</p>

<p><a href="http://www.pcmag.com/article2/0,2817,1165443,00.asp">PC Magazine </a> : &quot;a must-read for Web site designers.&quot;</p>

<p><a href="http://blogcritics.org/books/article/homepage-useability-50-websites-deconstructed-by/">Blogcritics </a> : &quot;Well worth the money [...] It&#39;s so much fun just to read.&quot;</p>

<p><a href="http://www.mantex.co.uk/2009/07/12/homepage-usability/">Mantex UK </a> : &quot;any Web designer [...] will learn a lot.&quot;</p>

<p><a href="http://dannyreviews.com/h/Homepage_Usability.php">Danny Yee&#39;s Book Reviews </a> : &quot;invaluable advice on web design - I learned a lot from it, as I think even seasoned web designers will.&quot;</p>

<p><a href="http://content.stuttgarter-zeitung.de/stz/page/detail.php/228123">Stuttgarter (German) </a></p>

<p><a href="http://www1.folha.uol.com.br/folha/informatica/ult124u9779.shtml">Folha de S. Paulo Review (Portuguese) </a></p>

<h2>Have Us Review Your Homepage</h2>

<p>Nielsen Norman Group offers a consulting service with an <a href="../../consulting/design-reviews/index.php"> independent usability review </a> of your homepage.</p>

<ul>
	<li>Get a report that spares no punches while giving you directions for how to improve the design of the most important page on your website.</li>
	<li>Get your score and find out how you stack up against the other sites we have reviewed.</li>
</ul>

<h2>Table of Contents</h2>

<ol>
	<li>Preface</li>
	<li>113 Design Guidelines for Homepages</li>
	<li>Determining Homepage Content</li>
	<li>Vertical Industry Segments</li>
	<li>Communicating the Site&#39;s Purpose</li>
	<li>Communicating Information About Your Company</li>
	<li>Content Writing</li>
	<li>Revealing Content Through Examples</li>
	<li>Archives and Accessing Past Content</li>
	<li>Links</li>
	<li>Navigation</li>
	<li>Search</li>
	<li>Tools and Task Shortcuts</li>
	<li>Graphics and Animation</li>
	<li>Graphic Design</li>
	<li>UI Widgets</li>
	<li>Window Titles</li>
	<li>URLs</li>
	<li>News and Press Releases</li>
	<li>Popup Windows and Staging Pages</li>
	<li>Advertising</li>
	<li>Welcomes</li>
	<li>Communicating Technical Problems and Handling Emergencies</li>
	<li>Credits</li>
	<li>Page Reload and Refresh</li>
	<li>Customization</li>
	<li>Gathering Customer Data</li>
	<li>Fostering Community</li>
	<li>Dates and Times</li>
	<li>Stock Quotes and Displaying Numbers</li>
	<li>Homepage Design Statistics</li>
	<li>Percentages showing how frequently each of the possible alternatives are used for main homepage design choices.</li>
	<li>Download Time</li>
	<li>Basic Page Layout</li>
	<li>Page Width</li>
	<li>Liquid Versus Frozen Layout</li>
	<li>Page Length</li>
	<li>Frames</li>
	<li>Fundamental Page Design Elements</li>
	<li>Logo</li>
	<li>Search</li>
	<li>Navigation</li>
	<li>Footer Navigation</li>
	<li>Site Map</li>
	<li>Routing Pages</li>
	<li>Splash Pages</li>
	<li>Frequent Features</li>
	<li>Sign In</li>
	<li>About Us</li>
	<li>Contact Info</li>
	<li>Privacy Policy</li>
	<li>Job Openings</li>
	<li>Help</li>
	<li>Graphics and Multimedia</li>
	<li>Pictures</li>
	<li>ALT Text</li>
	<li>Music</li>
	<li>Animation</li>
	<li>Advertising</li>
	<li>Typography</li>
	<li>Body Text and Background Colors</li>
	<li>Link Formatting</li>
	<li>Recommended Homepage Design</li>
	<li>Fifty Homepage Reviews</li>
	<li>Listing of Window Titles and Taglines</li>
	<li>Screen Real Estate Allocation</li>
	<li>Gallery of Logos</li>
	<li>Gallery of Search Features</li>
	<li>Gallery of People Photos</li>
	<li>Gallery of Shopping Carts</li>
	<li>Index</li>
</ol>

<h2>Translations</h2>

<table cellpadding="3" cellspacing="3" width="100%">
	<tbody>
		<tr>
			<td width="50%"><a href="http://product.dangdang.com/main/product.aspx?product_id=9053452"><img alt="Book cover of the Chinese (simplified)  translation of Homepage Usability" src="http://media.nngroup.com.s3.amazonaws.com/media/editor/2013/01/09/homepage_china_small.jpg" style="width: 175px; height: 177px;" /> </a></td>
			<td width="50%"><a href="#"> <img alt="Book cover of Traditional Chinese translation of Homepage Usability" src="http://media.nngroup.com.s3.amazonaws.com/media/editor/2013/01/09/homepage_cover_taiwan_small.jpg" style="width: 175px; height: 175px;" /> </a></td>
		</tr>
		<tr>
			<td width="50%">
			<p><a href="http://product.dangdang.com/main/product.aspx?product_id=9053452">Chinese (simplified) at Dangdang.com&nbsp; </a></p>

			<p>专业主页设计技术：50佳站点赏析</p>

			<p>ISBN 986-7944-16-X</p>
			</td>
			<td width="50%">
			<p>Chinese (traditional)</p>

			<p>ISBN 986-7944-16-X</p>
			</td>
		</tr>
		<tr>
			<td width="50%"><a href="http://www.zonerpress.cz/inshop/scripts/shop.aspx?action=DoViewProductDetail&amp;ProductID=31"><img alt="Book cover of Czech translation of Homepage usability" src="http://media.nngroup.com.s3.amazonaws.com/media/editor/2013/01/09/homepage_czech.png" style="width: 175px; height: 175px;" /> </a></td>
			<td width="50%"><a href="http://www.amazon.fr/exec/obidos/ASIN/2212111177"><img alt="Book cover of French translation of Homepage Usability" src="http://media.nngroup.com.s3.amazonaws.com/media/editor/2013/01/09/homepage_french.jpg" style="width: 175px; height: 175px;" /> </a></td>
		</tr>
		<tr>
			<td width="50%">
			<p><a href="http://www.amazon.fr/dp/2744023159/">C</a><a href="http://www.zonerpress.cz/inshop/scripts/shop.aspx?action=DoViewProductDetail&amp;ProductID=31">zech at Zonerpress.cz </a></p>

			<p>Použitelnost domovsk&yacute;ch str&aacute;nek<br />
			ISBN 80-86815-18-8</p>

			<p>&nbsp;</p>
			</td>
			<td width="50%">
			<p><a href="http://www.amazon.fr/exec/obidos/ASIN/2212111177">French at Amazon.fr </a></p>

			<p>L&#39;art de la page d&#39;accueil: 50 sites web pass&eacute;s au crible<br />
			ISBN 2-212-11117-7</p>
			</td>
		</tr>
		<tr>
			<td><a href="http://www.amazon.de/exec/obidos/ASIN/3827268478/"><img alt="Book cover of German Translation of Homepage usability" src="http://media.nngroup.com.s3.amazonaws.com/media/editor/2013/01/09/homepage_german.jpg" style="width: 175px; height: 175px;" /> </a></td>
			<td><a href="http://www.apogeonline.com/libri/88-7303-848-4/scheda"><img alt="Book cover of Italian Translation of Homepage Usability" src="http://media.nngroup.com.s3.amazonaws.com/media/editor/2013/01/09/homepage_italian.gif" style="width: 150px; height: 187px;" /> </a></td>
		</tr>
		<tr>
			<td>
			<p><a href="http://www.amazon.de/exec/obidos/ASIN/3827268478/">German at Amazon.de </a></p>

			<p>Homepage Usability. 50 enttarnte Webseiten<br />
			ISBN 3-8272-6847-8</p>

			<p><a href="http://content.stuttgarter-zeitung.de/stz/page/detail.php/228123">Stuttgatter-Zeitgung review </a></p>
			</td>
			<td>
			<p><a href="http://www.apogeonline.com/libri/88-7303-848-4/scheda">Italian at Apogeeonline.com </a></p>

			<p>Homepage usability: 50 siti web analizzati<br />
			ISBN 88-7303-848-4</p>

			<p>&nbsp;</p>
			</td>
		</tr>
		<tr>
			<td><a href="http://www.amazon.co.jp/exec/obidos/ASIN/4844356402/"><img alt="Book cover of Japanese translation of Homepage Usability" src="http://media.nngroup.com.s3.amazonaws.com/media/editor/2013/01/09/homepage_japan_cover_small.jpg" style="width: 175px; height: 265px;" /> </a></td>
			<td><a href="http://www.adlibris.com/no/product.aspx?isbn=8241205295"><img alt="Book cover of Finnish (Norwegian) translation of Homepage Usability" src="http://media.nngroup.com.s3.amazonaws.com/media/editor/2013/01/09/homepage_norwegian.jpg" style="width: 175px; height: 170px;" /> </a></td>
		</tr>
		<tr>
			<td>
			<p><a href="http://www.itcpub.co.kr/book/book_view1.php?h_code=h_9&amp;book_num=902">J</a><a href="http://www.amazon.co.jp/exec/obidos/ASIN/4844356402/">apanese at Amazon.co.jp </a></p>

			<p>ホームページ・ユーザビリティ ~顧客をつかむ勝ち組サイト32の決定的法則<br />
			ISBN 4-8443-5640-2</p>
			</td>
			<td>
			<p><a href="http://helion.pl/ksiazki/optymalizacja-funkcjonalnosci-serwisow-internetowych-jakob-nielsen-hoa-loranger,ofuser.htm">F</a><a href="http://www.adlibris.com/no/product.aspx?isbn=8241205295">innish at adlibris.com </a></p>

			<p>Funksjonelle hjemmesider: 50 hjemmesider bit-for-bit<br />
			ISBN 82-412-0529-5</p>
			</td>
		</tr>
		<tr>
			<td><a href="http://helion.pl/ksiazki/funkcjonalnosc-stron-www-50-witryn-bez-sekretow-jakob-nielsen-marie-tahir,funw50.htm"><img alt="Book cover of Polish translation of Homepage Usability" src="http://media.nngroup.com.s3.amazonaws.com/media/editor/2013/01/09/homepage_polish_.jpg" style="width: 175px; height: 250px;" /> </a></td>
			<td><img alt="Book cover of Portuguese translation of Homepage Usability" src="http://media.nngroup.com.s3.amazonaws.com/media/editor/2013/01/09/homepage_portuguese.jpg" style="width: 175px; height: 252px;" /></td>
		</tr>
		<tr>
			<td>
			<p><a href="http://helion.pl/ksiazki/funkcjonalnosc-stron-www-50-witryn-bez-sekretow-jakob-nielsen-marie-tahir,funw50.htm">Polish at helion.pl </a></p>

			<p>Funkcjonalność stron www. 50 witryn bez sekret&oacute;w<br />
			ISBN 85-352-0945-X</p>
			</td>
			<td>
			<p>Portuguese</p>

			<p>Homepage: Usabilidade - 50 websites desconstru&iacute;dos<br />
			ISBN 83-246-0126-0</p>
			</td>
		</tr>
		<tr>
			<td><a href="http://www.williamspublishing.com/Books/5-8459-0315-7.php"><img alt="Book cover for Russian Translation of Homepage Usability" src="http://media.nngroup.com.s3.amazonaws.com/media/editor/2013/01/09/homepage_russian_small.jpg" style="width: 175px; height: 247px;" /> </a></td>
			<td><a href="http://www.casadellibro.com/libro-usabilidad-de-paginas-de-inicio-analisis-de-50-sitios-web-nddsc/9788420532028/828152"><img alt="Book cover of Spanish translation of Homepage Usability" src="http://media.nngroup.com.s3.amazonaws.com/media/editor/2013/01/09/homepage_spanish.jpg" style="width: 143px; height: 217px;" /> </a></td>
		</tr>
		<tr>
			<td>
			<p><a href="http://www.williamspublishing.com/Books/5-8459-0315-7.php">Russian at Williamspublishing.com </a></p>

			<p>Дизайн Web-страниц (веб дизайн). Анализ удобства и простоты использования 50 сайтов<br />
			ISBN 5-8459-0315-7</p>
			</td>
			<td>
			<p><a href="http://www.casadellibro.com/libro-usabilidad-de-paginas-de-inicio-analisis-de-50-sitios-web-nddsc/9788420532028/828152">Spanish at casdellibro.com </a></p>

			<p>Usabilidad de P&aacute;ginas de Inicio: An&aacute;lisis de 50 sitios Web<br />
			ISBN 84-205-3202-9</p>
			</td>
		</tr>
		<tr>
			<td><a href="http://www.adlibris.com/se/product.aspx?isbn=9789163119729&amp;"><img alt="Book cover of Swedish translation of Homepage Usability" src="http://media.nngroup.com.s3.amazonaws.com/media/editor/2013/01/09/homepage_swedish.jpg" style="width: 175px; height: 175px;" /> </a></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>
			<p><a href="http://www.williamspublishing.com/Books/5-8459-0315-7.php">S</a><a href="http://www.adlibris.com/se/product.aspx?isbn=9789163119729&amp;">wedish at bokhavet.com </a></p>

			<p>Anv&auml;ndbara Hemsidor - Analys av 50 webbplatser<br />
			ISBN 91-63-11972-2</p>
			</td>
			<td>
			<p>&nbsp;</p>
			</td>
		</tr>
	</tbody>
</table>

<h2>Errata</h2>

<p>This list of corrections does not include minor typos but only errors of substance.</p>

<dl>
	<dt><strong>Page 165 </strong></dt>
	<dd>The pie chart has too small a wedge for &quot;content of interest&quot; (should be 19%) and too big a wedge for &quot;unused&quot; (should be 31%).</dd>
	<dt><strong>Page 186-187 </strong></dt>
	<dd>Comment 17 on page 187 refers to the menu item &quot;Specialized Transportation&quot; in the right column of the screenshot on page 186. The #17 red bubble has erroneously been placed in the first column instead.</dd>
</dl>


    <div class="show-for-small-down purchase-from">
      <hr>
      
  <h2>Purchase from</h2>
  
    <p><a href="http://www.amazon.com/exec/obidos/ASIN/073571102X/ref=nosim/useitcomusablein">Amazon.com</a></p>
  
  
    
    
      <ul class="no-bullet">
        <li><strong>UK/Europe</strong></li>
        
          <li><a href="http://www.amazon.co.uk/exec/obidos/ASIN/073571102X/useit-21/ref=nosim">Buy from Amazon.co.uk</a></li>
        
      </ul>
    
      <ul class="no-bullet">
        <li><strong>Other</strong></li>
        
          <li><a href="http://www.kurslitteratur.se/ISBN/9163119722.aspx">Användbara Hemsidor - Analys av 50 webbplatser </a></li>
        
          <li><a href="http://helion.pl/ksiazki/funw50.htm">Funkcjonalność stron www. 50 witryn bez sekretów </a></li>
        
          <li><a href="http://www.adlibris.com/no/product.aspx?isbn=8241205295">Funksjonelle hjemmesider: 50 hjemmesider bit-for-bit </a></li>
        
          <li><a href="http://www.amazon.de/exec/obidos/ASIN/3827268478/">Homepage Usability. 50 enttarnte Webseiten </a></li>
        
          <li><a href="http://www.apogeonline.com/libri/00848/scheda">Homepage usability: 50 siti web analizzati </a></li>
        
          <li><a href="http://www.amazon.fr/exec/obidos/ASIN/2212111177">L&#39;art de la page d&#39;accueil: 50 sites web passés au crible </a></li>
        
          <li><a href="http://www.zonerpress.cz/kniha-pouzitelnost-domovskych-stranek.php">Použitelnost domovských stránek </a></li>
        
          <li><a href="http://www.casadellibro.com/libro-usabilidad-de-paginas-de-inicio-analisis-de-50-sitios-web-nddsc/9788420532028/828152">Usabilidad de Páginas de Inicio: Análisis de 50 sitios Web </a></li>
        
          <li><a href="http://www.williamspublishing.com/Books/5-8459-0315-7.php">Дизайн Web-страниц (веб дизайн). Анализ удобства и простоты использования 50 сайтов</a></li>
        
          <li><a href="http://product.dangdang.com/product.aspx?product_id=9053452">专业主页设计技术：50佳站点赏析</a></li>
        
          <li><a href="http://www.amazon.co.jp/exec/obidos/ASIN/4844356402/">ホームページ・ユーザビリティ ~顧客をつかむ勝ち組サイト32の決定的法則 [単行本]</a></li>
        
      </ul>
    
  


    </div>
    <div class="related">
      


  <hr>



  <h3>Related Reports</h3>
  <ul class="no-bullet">
    
      <li><a href="../../reports/b2b-websites-usability/index.php">B2B Website Usability for Converting Users into Leads and Customers</a></li>
    
      <li><a href="../../reports/strategic-design-faqs/index.php">Strategic Design for Frequently Asked Questions</a></li>
    
  </ul>



  <h3>Related Courses</h3>
   <ul class="no-bullet">
    
        <li><a href="../../courses/ux-basic-training/index.php">UX Basic Training</a></li>
    
        <li><a href="../../courses/human-mind/index.php">The Human Mind and Usability</a></li>
    
        <li><a href="../../courses/emerging-patterns-web-design/index.php">Emerging Patterns for Web Design</a></li>
    
        <li><a href="../../courses/web-page-design/index.php">Web Page UX Design</a></li>
    
</ul>



  <h3>Articles</h3>
  <ul class="no-bullet">
    
      <li><a href="../../articles/menu-design/index.php">Menu Design: Checklist of 15 UX Guidelines to Help Users </a></li>
    
      <li><a href="../../articles/flat-design/index.php">Flat Design: Its Origins, Its Problems, and Why Flat 2.0 Is Better for Users</a></li>
    
      <li><a href="../../articles/page-fold-manifesto/index.php">The Fold Manifesto: Why the Page Fold Still Matters</a></li>
    
      <li><a href="../../articles/scaling-user-interfaces/index.php">Scaling User Interfaces: An Information-Processing Approach to Multi-Device Design</a></li>
    
      <li><a href="../../articles/top-10-mistakes-web-design/index.php">Top 10 Mistakes in Web Design</a></li>
    
  </ul>


    </div>
  </div>

  <div class="small-12 medium-4 columns hide-for-small-down l-subsection">
    
  <h2>Purchase from</h2>
  
    <p><a href="http://www.amazon.com/exec/obidos/ASIN/073571102X/ref=nosim/useitcomusablein">Amazon.com</a></p>
  
  
    
    
      <ul class="no-bullet">
        <li><strong>UK/Europe</strong></li>
        
          <li><a href="http://www.amazon.co.uk/exec/obidos/ASIN/073571102X/useit-21/ref=nosim">Buy from Amazon.co.uk</a></li>
        
      </ul>
    
      <ul class="no-bullet">
        <li><strong>Other</strong></li>
        
          <li><a href="http://www.kurslitteratur.se/ISBN/9163119722.aspx">Användbara Hemsidor - Analys av 50 webbplatser </a></li>
        
          <li><a href="http://helion.pl/ksiazki/funw50.htm">Funkcjonalność stron www. 50 witryn bez sekretów </a></li>
        
          <li><a href="http://www.adlibris.com/no/product.aspx?isbn=8241205295">Funksjonelle hjemmesider: 50 hjemmesider bit-for-bit </a></li>
        
          <li><a href="http://www.amazon.de/exec/obidos/ASIN/3827268478/">Homepage Usability. 50 enttarnte Webseiten </a></li>
        
          <li><a href="http://www.apogeonline.com/libri/00848/scheda">Homepage usability: 50 siti web analizzati </a></li>
        
          <li><a href="http://www.amazon.fr/exec/obidos/ASIN/2212111177">L&#39;art de la page d&#39;accueil: 50 sites web passés au crible </a></li>
        
          <li><a href="http://www.zonerpress.cz/kniha-pouzitelnost-domovskych-stranek.php">Použitelnost domovských stránek </a></li>
        
          <li><a href="http://www.casadellibro.com/libro-usabilidad-de-paginas-de-inicio-analisis-de-50-sitios-web-nddsc/9788420532028/828152">Usabilidad de Páginas de Inicio: Análisis de 50 sitios Web </a></li>
        
          <li><a href="http://www.williamspublishing.com/Books/5-8459-0315-7.php">Дизайн Web-страниц (веб дизайн). Анализ удобства и простоты использования 50 сайтов</a></li>
        
          <li><a href="http://product.dangdang.com/product.aspx?product_id=9053452">专业主页设计技术：50佳站点赏析</a></li>
        
          <li><a href="http://www.amazon.co.jp/exec/obidos/ASIN/4844356402/">ホームページ・ユーザビリティ ~顧客をつかむ勝ち組サイト32の決定的法則 [単行本]</a></li>
        
      </ul>
    
  


  </div>

</div>

    </div>
    




<footer id="page-footer">
    <div class="row hide-for-print">
        <div class="column small-12 medium-6">
            <section class="footer-popular">
                
                <h1>Popular Topics</h1>
                <div style="float: left;  width: 15em; display: block;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/applications/index.php">Application Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/e-commerce/index.php">E-Commerce Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/eyetracking/index.php">Eyetracking </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/information-architecture/index.php">Information Architecture </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/intranets/index.php">Intranet Design </a></li>
	<li><a href="../../topic/interaction-design/index.php">Interaction Design </a></li>
</ul>
</div>

<div style="float: left;  width: 15em; display: block; list-style: none;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/mobile-and-tablet-design/index.php">Mobile and Tablet Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/credibility-persuasion/index.php">Persuasive Design</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/research-methods/index.php">Research Methods</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/user-testing/index.php">User Testing </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/web-usability/index.php">Web Usability </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/writing-web/index.php">Web Writing &amp; Content Strategy </a></li>
</ul>
</div>

<p>&nbsp;</p>

                
            </section>
        </div>

        <div class="column small-12 medium-6">
            <div class="row">
                <div class="column small-6">
                    <section class="footer-about">
                        <h1>Follow Us</h1>
                        <ul>
	<li><a href="../../articles/subscribe/index.php">Subscribe to our weekly UX newsletter</a>,&nbsp;Jakob Nielsen&#39;s Alertbox</li>
	<li>Visit Don Norman&#39;s website, <a href="http://www.jnd.org/"> www.jnd.org </a></li>
	<li>Visit Bruce Tognazzini&#39;s website, <a href="http://www.asktog.com/"> www.asktog.com </a></li>
	<li style="padding-top: 6px;"><a class="twitter-follow-button" data-show-count="false" data-size="large" href="https://twitter.com/nngroup"><img alt="Follow @nngroup on Twitter" height="20" src="https://media.nngroup.com/media/editor/2016/12/30/twitter-follow-button.png" style="width: 117px; height: 20px;" width="117" /></a></li>
	<li style="padding-top: 6px;"><img alt="" height="14" src="https://media.nngroup.com/media/editor/2016/07/26/feed-icon-14x14.png" style="vertical-align:text-top;width:14px;margin-right:.25em;" width="14" /><a href="../../feed/rss/index.php">Subscribe to our RSS feed</a></li>
</ul>

                    </section>
                </div>

                <div class="column small-6">
                    <section class="footer-about right-column">
                        <h1>About
</h1>
                        <ul class="no-bullet">
	<li><a href="../../about/contact/index.php">Contact information</a></li>
	<li><a href="../../about/index.php">About Us</a></li>
	<li><a href="../../about/why-nng/index.php">Why NN/g? </a></li>
	<li><a href="../../return-policy/index.php">Return Policy</a></li>
</ul>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="copyright column small-12">
            <p><a href="../../copyright-and-reprint-info/index.php">Copyright</a> &copy; 1998-2017 Nielsen Norman Group, All Rights Reserved.</p>
        </div>
    </div>
</footer>


    


        <script src="https://media.nngroup.com/static/vendor/fastclick/js/fastclick.min.js"></script>
        <script src="https://media.nngroup.com/static/vendor/foundation/js/foundation.min.js"></script>
        <script src="http://media.nngroup.com/static/js/app.js"></script>
        <script src="http://media.nngroup.com/static/js/scroll-to-fixed.js"></script><script>var externalLinks = document.querySelectorAll('a[href^="http"]');for (var i = externalLinks.length-1; i >= 0; i--) {externalLinks[i].addEventListener("click", function(event) { alert("External links are disabled for this test."); event.preventDefault(); }, false);}var mailLinks = document.querySelectorAll('a[href^="mailto"]');for (var i = mailLinks.length-1; i >= 0; i--) {mailLinks[i].addEventListener("click", function(event) { alert("Email links are disabled for this test."); event.preventDefault(); }, false);}var externalForms = document.querySelectorAll('form');for (var i = externalForms.length-1; i >= 0; i--) {externalForms[i].addEventListener("submit", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false); externalForms[i].addEventListener("click", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false);}</script>

        <script>
            
            $(function() {
                $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
                $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
            });
        </script>

        


        
        <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1072699960;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072699960/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
    </body>

<!-- Mirrored from www.nngroup.com/books/homepage-usability/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:02:38 GMT -->
</html>
