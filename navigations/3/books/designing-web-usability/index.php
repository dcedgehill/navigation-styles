<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->





    
<!-- Mirrored from www.nngroup.com/books/designing-web-usability/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:02:39 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1026.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"beacon":"bam.nr-data.net","queueTime":8,"applicationTime":352,"agent":"","licenseKey":"1a30ef9931","errorBeacon":"bam.nr-data.net","applicationID":"2048992","transactionName":"NFxSMRBUX0EDUkIPDA0WdhAMVkVbDV8ZBAwMUkNLFFxURRELdAkMCH1VEQNcXRwFVEI="}</script>
        <title>Designing Web Usability: a Book by Jakob Nielsen</title><meta property="og:title" content="Designing Web Usability: a Book by Jakob Nielsen" />
  
        
        <meta name="description" content="Book page for Jakob Nielsen&#39;s classic book (1/4 million copies sold) Designing Web Usability: The Practice of Simplicity, available in 22 languages.">
        <meta property="og:description" content="Book page for Jakob Nielsen&#39;s classic book (1/4 million copies sold) Designing Web Usability: The Practice of Simplicity, available in 22 languages." />
        
  
        
	
        

        

        <meta name="robots" content="noarchive">
        <meta name="viewport" content="width=device-width">

        <link rel="search" type="application/opensearchdescription+xml" title="NN/g Search" href="../../opensearch.xml" />
        <link rel="shortcut icon" href="http://media.nngroup.com/static/img/favicon.ico" />

        <link rel="stylesheet" href="https://media.nngroup.com/static/vendor/font-awesome/css/font-awesome.min.css">

        
        <link rel="stylesheet" href="https://media.nngroup.com/static/css/7232a960f1df.css" type="text/css" />
        <link rel="stylesheet" href="http://fast.fonts.com/t/1.css?apiType=css&amp;projectid=c5846260-9def-46ec-9407-0f1349bd2d64" type="text/css">

        <script src="https://media.nngroup.com/static/vendor/modernizr/js/modernizr.min.js"></script>

        <script type="text/javascript" src="https://media.nngroup.com/static/js/3283be5def74.js"></script>
        

        

        
        
        <!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=229034,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('http://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->


<!-- Google Analytics Tracking Code for http://nngroup.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23132681-1', 'nngroup.com');
  ga('require', 'linkid', 'https://www.nngroup.com/books/designing-web-usability/linkid.js');
  ga('require', 'displayfeatures');

// Optimizely Universal Analytics Integration
 window.optimizely = window.optimizely || [];
 window.optimizely.push("activateUniversalAnalytics");

  ga('send', 'pageview');

</script>
<script>
/**
* Function to track a click on an outbound link in Google Analytics.
*/
function trackOutboundLink(label, url) {
    if(window.ga && ga.loaded) {
        ga('send', 'event', 'outbound', 'click', label, {'hitCallback': function(){document.location = url;}
        });
    } else {
        document.location = url;
    }
}
/**
* Function to track a click on a link in the article announcement area.
*/
$('#articleAnnouncement a').on('click', function(){
  ga('send', 'event', 'link', 'click', 'articleAnnouncement');
});
</script>

<script type="text/javascript">
 setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2421.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



<!-- Hotjar Tracking Code for http://nngroup.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:121605,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

        <script src="https://media.nngroup.com/static/js/analytics/track_outbound_link.js"></script>
        
        
        
    </head>
    <body class="location-about location-books book-detail">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
    <header id="page-header">
        <div id="cart" class="shopping-cart mobile-cart show-for-small-down"></div>
        <div class="row">
            <div id="masthead" class="masthead small-12 medium-7 column">

                <!-- The button to toggles menu on small screen -->

                <h1><a href="../../index.php"><img src="http://media.nngroup.com/static/img/logo-wordmark.png" width="74" height="30"> Nielsen <span class="norman">Norman</span> Group</a></h1>
                <p class="tagline show-for-medium-up">Evidence-Based User Experience Research, Training, and Consulting</p>

                <!-- The button to toggles form search on small screen -->


                <div style="clear: right;width:  100%;height: 1px;"></div><?php define('ROOT_PATH',$_SERVER["DOCUMENT_ROOT"] . '/apps/navigation_styles/navigations/3'); ?><?php include(ROOT_PATH)."/navigation.php"; ?>

                <!-- Drop down menu on small screen -->
                <div id="hideNav" class="show-for-small-down">
                  
                  <ul class="menu-content">
                    <li><a href="../../index.php">Home</a></li>
                    <li class="has-submenu">
                      <a>Training</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../training/index.php">Overview</a></li>
                        
<li id="nav-ux-conference"><a href="../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../online-seminars/index.php"><span>Online Seminars</span></a></li>

                      </ul>
                    </li>
                    <li class="has-submenu">
                      <a>Consulting</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
  <li><a href="../../consulting/index.php">Overview</a></li>
  
  <li><a href="../../consulting/benchmarking/index.php">Benchmarking</a></li>
  
  <li><a href="../../consulting/expert-design-review/index.php">Expert Design Review</a></li>
  
  <li><a href="../../consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
  
  <li><a href="../../consulting/usability-testing/index.php">Usability Testing </a></li>
  
  <li><a href="../../consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
  
  <li><a href="../../in-house-training/index.php">In-House Training</a></li>
</ul>

                    </li>

                    <li><a href="../../reports/index.php">Reports</a></li>
                    <li><a href="../../articles/index.php">Articles</a></li>
                    <li class="has-submenu"><a>About NN/G</a><i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../about/index.php">Overview</a></li>
                        <li><a href="../../people/index.php">People</a></li>
                        <li><a href="../../about/why-nng/index.php">Why NN/g?</a></li>
                        <li><a href="../../about/contact/index.php">Contact</a></li>
                        <li><a href="../../news/index.php">News</a></li>
                        <li><a href="../../about/history/index.php">History</a></li>
                        <li><a href="../index.php">Books</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                </div>

                <hr class="header-divider show-for-small-only"/>


            </div>
       </div>

        <div id="hideNav">            <nav class="primary-nav small-12 column">
                <ul>
                    <li><a class="navitem" id="nav-home" href="../../index.php">Home</a></li>
                    <li><a class="navitem" id="nav-training" href="../../training/index.php">Training</a></li>
                    <li><a class="navitem" id="nav-consulting" href="../../consulting/index.php">Consulting</a></li>
                    <li><a class="navitem" id="nav-reports" href="../../reports/index.php">Reports</a></li>
                    <li><a class="navitem" id="nav-articles" href="../../articles/index.php">Articles</a></li>
                    <li><a class="navitem" id="nav-about" href="../../about/index.php">About NN/G</a></li>
                </ul>
            </nav>
            <nav class="secondary-nav small-12 medium-12 large-12 columns">
                
<ul class="inline-list">
    <li id="nav-people"><a href="../../people/index.php"><span>People</span></a></li>
    <li id="nav-whynng"><a href="../../about/why-nng/index.php"><span>Why NN/g?</span></a></li>
    <li id="nav-contact"><a href="../../about/contact/index.php"><span>Contact</span></a></li>
    <li id="nav-news"><a href="../../news/index.php"><span>News</span></a></li>
    <li id="nav-history"><a href="../../about/history/index.php"><span>History</span></a></li>
    <li id="nav-books"><a href="../index.php"><span>Books</span></a></li>
</ul>

            </nav>
        </div>
    </header>
    <div id="mainContent" class="l-content">
    
<div class="row">
  <div class="medium-2 columns hide-for-small-down">
    <img class="book-cover" src="https://media.nngroup.com/media/publications/books/designing.gif.300x400_q95_autocrop_crop_upscale.jpg">
  </div>
  <div class="small-12 medium-6 columns l-subsection">
    <h1>Designing Web Usability: The Practice of Simplicity</h1>

    <img class="show-for-small-down book-cover" src="https://media.nngroup.com/media/publications/books/designing.gif.300x400_q95_autocrop_crop_upscale.jpg">
    <h2>
<a href="../../people/jakob-nielsen/index.php">Jakob Nielsen</a>
, 1999</h2>

    

    <p>
	Over 250,000 Internet professionals around the world have turned to this landmark, definitive guide to usability. From content and page design to designing for ease of navigation and users with disabilities, Designing Web Usability delivers complete direction on how to connect with any web user, in any situation. 432 pages, full color illustrations. (New Riders Publishing.)</p>
<p>
	Please buy through these links: Amazon pays me a referral fee that doubles the share of the purchase price that goes to the author, giving me time off from other projects to write new books</p>
<h2>
	Reviews and Articles About the Book</h2>
<ul>
	<li>
		Educational Technology Blog: 10 Years of Web Usability (2010): &quot;those chapters which focus on technique rather than technology are still relevant and many have become accepted practice.&quot;</li>
	<li>
		<a href="http://boschatzberg.com/2009/08/09/book-report-designing-web-usability/">boschatzberg.com </a> (2009): &quot;&quot;For the most part, the guidelines in this book still apply today, even though the book was published in 1999. [...]This book is a must-read for anyone working on the web.&quot;</li>
	<li>
		<a href="http://stevenclark.com.au/2008/02/08/designing-web-usability/">stevenclark.com.au </a> (2008): &quot;Well worth a read and still relevant and correct.&quot;</li>
	<li>
		<a href="http://cogitas.wordpress.com/2008/01/26/review-jakob-nielsens-designing-web-usability/">Weinberg&#39;s research blog </a> (2008): &quot;I&#39;m glad I read this book. [...] Nielsen has a way of saying things very simply and clearly.&quot;</li>
	<li>
		<a href="http://www.flashmagazine.com/html/699.htm">FlashMagazine </a> (2002): &quot;Surprisingly, <strong> as time goes by, &quot;Designing Web Usability&quot; becomes more useful </strong> [...] you will return to this book many times.&quot;</li>
	<li>
		Builder.com (July 2001): &quot;an undeniable standard ... this book is necessary for those who want to understand the Web and its users&quot;</li>
	<li>
		Rough Notes Magazine (May 2001): Designing Successful Web Sites: Put usability first; practice simplicity: &quot;Scores of books purport to offer insight into successful Web site design. At least one actually does. ... It is recommended reading for agency principals and Web managers alike.&quot;</li>
	<li>
		Free Pint Bookshelf (March 2001): &quot;It is a testament to the usefulness of this book that it has lived beside my terminal at work for the last few weeks as I put the theory into practice.&quot;</li>
	<li>
		Business Week: Jakob Nielsen&#39;s Gospel of Good Web Design (March 2, 2000): &quot;should [...] be read by any executive with responsibility for managing online operations&quot;</li>
	<li>
		NewMedia (September 2000): Who Says Design Should be Simple?: &quot;No one with whom I&#39;ve spoken about Designing Web Usability has come away unaffected&quot; [...] &quot;the most important book of the year&quot;</li>
	<li>
		<a href="http://dannyreviews.com/h/Web_Usability.php">Danny Yee&#39;s Book Reviews </a> (2000): &quot;The most important book on web publishing yet to appear.&quot;</li>
	<li>
		Linux ISO: &quot; <strong> I couldn&#39;t put it </strong> [the book] <strong> down! </strong> The author, Jakob Nielsen, is entertaining, direct, and obviously not one to join the latest trend of the day.&quot;</li>
	<li>
		Professional WebMaster Magazine (July/August 2000): &quot;explains lots of factors that taken individually fall into the &#39;obvious&#39; category and when juxtaposed in an intelligent fashion lead to a far <strong> deeper understanding </strong> of the subject than you would have been able to arrive at on your own&quot;</li>
	<li>
		Builder.com (July 2001): &quot;an <strong> undeniable standard </strong> ... this book is necessary for those who want to understand the Web and its users&quot;</li>
	<li>
		Rough Notes Magazine (May 2001): Designing Successful Web Sites: Put usability first; practice simplicity: &quot;Scores of books purport to offer insight into successful Web site design. At least one actually does. ... It is recommended reading for agency principals and Web managers alike.&quot;</li>
	<li>
		Internet World UK (March 2000): &quot;if the Web design company you are employing <strong> hasn&#39;t got a copy of this book on its shelves, you&#39;ll know to go somewhere else </strong> &quot;</li>
	<li>
		<cite>Free Pint Bookshelf </cite> (March 2001): &quot;It is a testament to the usefulness of this book that it has <strong> lived beside my terminal </strong> at work for the last few weeks as I put the theory into practice.&quot;</li>
	<li>
		<cite>Telecomwire </cite> (October 2000): &quot;a <strong> must for any business </strong> who wants to create a strong online presence&quot;</li>
	<li>
		New Architect (August 2000): <a href="http://www.ddj.com/dept/architect/184413704"> Keeping Sites Simple </a> : &quot;Nielsen has done a <strong> great service to the entire Web community </strong> by showing why simplicity in design is important, and by clearly explaining how to achieve it&quot;</li>
	<li>
		<a href="http://www.dlib.org/dlib/june00/06bookreview.php"><cite>Digital Libraries Magazine </cite> </a> (June 2000): &quot;When I visit a hard-to-use site, it often seems clear that Nielsen&#39;s advice would have made the site easier to use. It&#39;s not that the web is inherently hard to use or that I can&#39;t figure out a particular site; the site&#39;s designers are to blame.&quot;</li>
	<li>
		<a href="http://www.slashdot.org/books/00/04/03/1210217.shtml"><cite>Slashdot </cite> </a> (April 27, 2000): &quot; <strong> well researched </strong> , sensible, and right on target [...] impressively concise and comprehensive&quot;</li>
	<li>
		Sydney Morning Herald, Australia, Science rules in Web design (April 3, 2000): &quot;relentlessly sensible&quot;</li>
	<li>
		Ex Libris: <a href="http://marylaine.com/exlibris/xlib47.php"> E-Zine for Librarians and Other Information Junkies </a> (March 10, 2000): &quot;I can&#39;t think of a more valuable resource for both amateur and experienced webmasters&quot;</li>
	<li>
		Chicago Tribune, reading list about current technology (March 6, 2000): &quot;Jakob Nielsen <strong> knows more about what makes Web sites work than anyone else on the planet </strong> &quot;</li>
	<li>
		WebReference Newsletter, December 1999: <a href="http://www.webreference.com/new/nielsen.php"> We interview the Web&#39;s <strong> usability czar </strong> about his new book and the Web&#39;s future </a></li>
	<li>
		<a href="http://www.amazon.com/exec/obidos/redirect?tag=useitcomusablein&amp;path=tg/feature/-/21147">Author interview on Amazon.com </a> , January 2000</li>
	<li>
		Le Journal du Net (French August 2000):&nbsp; <a href="http://www.journaldunet.com/solutions/0008/000822design.shtml"> Conception de sites Web : l&#39;art de la simplicit&eacute; </a></li>
	<li>
		PC World (Danish December 2001):&nbsp; <a href="http://www.pcworld.dk/art/3017/gode-raad-fra-nielsen"> Gode r&aring;d fra Nielsen </a></li>
</ul>
<h2>
	Advance Reader Comments on <cite> Designing Web Usability: The Practice of Simplicity </cite></h2>
<p style="margin-left: 40px;">
	&quot;Eighty gajillion things have been written about the Web. Do we need one more? Answer: Yes! If it&#39;s from Jakob Nielsen. <cite> Designing Web Usability: The Practice of Simplicity </cite> is a masterpiece. Jakob knows his stuff like, literally, no one else! This book is a pleasure to read. And invaluable. May simplicity rule!&quot; --Tom Peters,&nbsp;Management expert,&nbsp;Bestselling author ( <cite> In Search of Excellence </cite> , <cite> The Circle of Innovation </cite> , et al)</p>
<p style="margin-left: 40px;">
	&quot;Jakob is hands down the most perceptive and articulate person working in this field today, and I was really looking forward to getting my hands on this. Needless to say, I wasn&#39;t disappointed. <cite> Designing Web Usability </cite> should go a long, long way in making the Web a far more appealing, effective and user-friendly medium. For the good of us all, let&#39;s hope every Web designer in cyberspace reads it!&quot;&nbsp;Sam Vincent Meddis,&nbsp;Technology Editor,&nbsp;USATODAY.com</p>
<p style="margin-left: 40px;">
	&quot;With <cite> Designing Web Usability: The Practice of Simplicity </cite> , Jakob Nielsen proves what many of us thought after reading <cite> Usability Engineering </cite> -- his books are indispensable tools for Web site architects and designers. With <cite> Designing Web Usability </cite> , Nielsen offers a comprehensive overview of the practical problems to be solved in web sight design. He goes at the &quot;what&quot; and the &quot;how.&quot; He explains in clear prose and describes using current examples of what is right and what is wrong with Web sites seen by millions.&nbsp;If you follow Nielsen&#39;s instructions carefully you will be rewarded with faster Web projects and satisfied Web customers. There is no reason to make the same mistakes others have made, and no reason to make your users frustrated by your Web site&#39;s design. <cite> Designing Web Usability </cite> goes a long way toward solving today&#39;s problems today.&quot; &nbsp;Henry Lichstein,&nbsp;VP, Citibank</p>
<p style="margin-left: 40px;">
	&quot;Through the use of both good and bad examples with an easy-to-read style, Jakob does a great job exploring Web site design and navigation from a management perspective. The fully e-commerce-enabled site of the future needs to pay attention to the non-monetary forms of currency (time, attention, trust &amp; convenience). This book can help show you the way.&quot;&nbsp;Mitchell Levy,&nbsp;President, ECnow.com,&nbsp;Executive Producer, ECMgt.com</p>
<p style="margin-left: 40px;">
	&quot;The web design tips in this book are practical - it is based on research rather than opinion and presented in an accessible way. Most every web design question I&#39;ve heard lately is addressed in this book. The web sites used as examples help ground the guidelines in reality - a corporate site, intranet, e-commerce, or application site could all benefit from this wisdom. This is a great way to avoid the mistakes that other sites have made without replicating them oneself!&quot; &nbsp;Stacey Ashlund,&nbsp;Usability Manager,&nbsp;Infoseek (part of the GO Network)</p>
<p style="margin-left: 40px;">
	&quot;This text is a must-read for anyone involved with site design.&quot;&nbsp;N.J. Stoyanoff, Ph.D.,&nbsp;Director of E-Business,&nbsp;Ogilvy &amp; Mather</p>
<p style="margin-left: 40px;">
	&quot;Jakob Nielsen&#39;s timing is perfect. The explosion of the Internet has produced a myriad of ways to design and present Web-based content. By focusing on the user experience, these guidelines map the way to happier customers , brand loyalty, and success on the Web for businesses of every kind.&quot;&nbsp;Raymond G. Nasr,&nbsp;Director, Office of Corporate Affairs,&nbsp;Novell, Inc.</p>
<h2>
	Table of Contents</h2>
<p style="margin-left: 40px;">
	Preface</p>
<p style="margin-left: 40px;">
	<a href="../../books-designing-web-usability-preface/index.php">Preface to the 7th printing of the book </a></p>
<p style="margin-left: 40px;">
	<strong>1. Introduction: Why Web Usability? </strong></p>
<ul style="margin-left: 40px;">
	<li>
		Art Versus Engineering
		<ul>
			<li>
				About the Examples</li>
		</ul>
	</li>
	<li>
		A Call for Action</li>
	<li>
		What This Book Is Not
		<ul>
			<li>
				Bad Usability Equals No Customers</li>
		</ul>
	</li>
	<li>
		Why Everybody Designs Websites Incorrectly</li>
</ul>
<p style="margin-left: 40px;">
	<strong>2. Page Design </strong></p>
<ul>
	<li style="margin-left: 40px;">
		Screen Real Estate
		<ul>
			<li style="margin-left: 40px;">
				Data Ink and Chart Junk</li>
		</ul>
	</li>
	<li style="margin-left: 40px;">
		Cross-Platform Design
		<ul>
			<li style="margin-left: 40px;">
				Where Are Users Coming From?</li>
			<li style="margin-left: 40px;">
				The Car as a Web Browser</li>
			<li style="margin-left: 40px;">
				Color Depth Getting Deeper</li>
			<li style="margin-left: 40px;">
				Get a Big Screen</li>
			<li style="margin-left: 40px;">
				Resolution-Independent Design</li>
			<li style="margin-left: 40px;">
				Using Non-Standard Content</li>
			<li style="margin-left: 40px;">
				Installation Inertia</li>
			<li style="margin-left: 40px;">
				Helpful Super-Users</li>
			<li style="margin-left: 40px;">
				When Is It Safe to Upgrade?</li>
			<li style="margin-left: 40px;">
				Collect Browsers</li>
		</ul>
	</li>
	<li style="margin-left: 40px;">
		Separating Meaning and Presentation
		<ul>
			<li style="margin-left: 40px;">
				Platform Transition</li>
			<li style="margin-left: 40px;">
				Data Lives Forever</li>
		</ul>
	</li>
	<li style="margin-left: 40px;">
		Response Times
		<ul>
			<li style="margin-left: 40px;">
				Predictable Response Times</li>
			<li style="margin-left: 40px;">
				Server Response Time</li>
			<li style="margin-left: 40px;">
				The Best Sites Are Fast</li>
			<li style="margin-left: 40px;">
				Speedy Downloads, Speedy Connections</li>
			<li style="margin-left: 40px;">
				Users Like Fast Pages</li>
			<li style="margin-left: 40px;">
				You Need Your Own T1 Line</li>
			<li style="margin-left: 40px;">
				Understanding Page Size</li>
			<li style="margin-left: 40px;">
				Faster URLs</li>
			<li style="margin-left: 40px;">
				Glimpsing the First Screenful</li>
			<li style="margin-left: 40px;">
				Taking Advantage of HTTP Keep-Alive</li>
		</ul>
	</li>
	<li style="margin-left: 40px;">
		Linking
		<ul>
			<li style="margin-left: 40px;">
				Link Descriptions</li>
			<li style="margin-left: 40px;">
				Link Titles</li>
			<li style="margin-left: 40px;">
				Guidelines for Link Titles</li>
			<li style="margin-left: 40px;">
				Use Link Titles Without Waiting</li>
			<li style="margin-left: 40px;">
				Coloring Your Links</li>
			<li style="margin-left: 40px;">
				The Physiology of Blue</li>
			<li style="margin-left: 40px;">
				Link Expectations</li>
			<li style="margin-left: 40px;">
				Peoplelinks</li>
			<li style="margin-left: 40px;">
				Outbound Links</li>
			<li style="margin-left: 40px;">
				Incoming Links</li>
			<li style="margin-left: 40px;">
				Linking to Subscriptions and Registrations</li>
			<li style="margin-left: 40px;">
				Advertising Links</li>
		</ul>
	</li>
	<li style="margin-left: 40px;">
		Style Sheets
		<ul>
			<li style="margin-left: 40px;">
				Standardizing Design Through Style Sheets</li>
			<li style="margin-left: 40px;">
				WYSIWYG</li>
			<li style="margin-left: 40px;">
				Style Sheet Examples for Intranets</li>
			<li style="margin-left: 40px;">
				Making Sure Style Sheets Work</li>
		</ul>
	</li>
	<li style="margin-left: 40px;">
		Frames
		<ul>
			<li style="margin-left: 40px;">
				&lt;NOFRAMES&gt;</li>
			<li style="margin-left: 40px;">
				Frames in Netscape 2.0</li>
			<li style="margin-left: 40px;">
				Is It Ever OK to Use Frames?</li>
			<li style="margin-left: 40px;">
				Borderless Frames</li>
			<li style="margin-left: 40px;">
				Frames as Copyright Violation</li>
		</ul>
	</li>
	<li style="margin-left: 40px;">
		Credibility</li>
	<li style="margin-left: 40px;">
		Printing</li>
	<li style="margin-left: 40px;">
		Conclusion</li>
</ul>
<p style="margin-left: 40px;">
	<strong>3. Content Design </strong></p>
<ul style="margin-left: 40px;">
	<li>
		Writing for the Web
		<ul>
			<li>
				The Value of an Editor</li>
			<li>
				Keep Your Texts Short</li>
			<li>
				Copy Editing</li>
			<li>
				Web Attitude</li>
			<li>
				Scannability</li>
			<li>
				Why Users Scan</li>
			<li>
				Plain Language</li>
			<li>
				Page Chunking</li>
			<li>
				Limit Use of Within-Page Links</li>
		</ul>
	</li>
	<li>
		Page Titles</li>
	<li>
		Writing Headlines</li>
	<li>
		Legibility</li>
	<li>
		Online Documentation
		<ul>
			<li>
				Page Screenshots</li>
		</ul>
	</li>
	<li>
		Multimedia
		<ul>
			<li>
				Waiting for Software to Evolve</li>
			<li>
				Auto-Installing Plug-Ins</li>
		</ul>
	</li>
	<li>
		Response Time
		<ul>
			<li>
				Client-Side Multimedia</li>
		</ul>
	</li>
	<li>
		Images and Photographs
		<ul>
			<li>
				Image Reduction</li>
		</ul>
	</li>
	<li>
		Animation
		<ul>
			<li>
				Showing Continuity in Transitions</li>
			<li>
				Indicating Dimensionality in Transitions</li>
			<li>
				Illustrating Change Over Time</li>
			<li>
				Multiplexing the Display</li>
			<li>
				Enriching Graphical Representations</li>
			<li>
				Visualizing Three-Dimensional Structures</li>
			<li>
				Attracting Attention</li>
			<li>
				Animation Backfires</li>
		</ul>
	</li>
	<li>
		Video
		<ul>
			<li>
				Streaming Video Versus Downloadable Video</li>
		</ul>
	</li>
	<li>
		Audio</li>
	<li>
		Enabling Users with Disabilities to Use Multimedia Content</li>
	<li>
		Three-Dimensional Graphics
		<ul>
			<li>
				Bad Use of 3D</li>
			<li>
				When to Use 3D</li>
		</ul>
	</li>
	<li>
		Conclusion
		<ul>
			<li>
				The Attention Economy</li>
		</ul>
	</li>
</ul>
<p style="margin-left: 40px;">
	<strong>4. Site Design </strong></p>
<ul style="margin-left: 40px;">
	<li>
		The Home Page</li>
	<li>
		How Wide Should the Page Be?
		<ul>
			<li>
				Home Page Width</li>
		</ul>
	</li>
	<li>
		Splash Screens Must Die</li>
	<li>
		The Home Page Versus Interior Pages
		<ul>
			<li>
				Deep Linking</li>
			<li>
				Affiliates Programs</li>
		</ul>
	</li>
	<li>
		Metaphor
		<ul>
			<li>
				Shopping Carts as Interface Standard</li>
			<li>
				Alternative Terminology</li>
		</ul>
	</li>
	<li>
		Navigation
		<ul>
			<li>
				Navigation Support in Browsers</li>
			<li>
				Where Am I?</li>
			<li>
				Where Have I Been?</li>
			<li>
				Where Can I Go?</li>
			<li>
				Site Structure</li>
			<li>
				The Vice-Presidential Button</li>
			<li>
				Importance of User-Centered Structure</li>
			<li>
				Breadth Versus Depth</li>
		</ul>
	</li>
	<li>
		The User Controls Navigation
		<ul>
			<li>
				Design Creationism Versus Design Darwinism</li>
			<li>
				Help Users Manage Large Amounts of Information</li>
			<li>
				Future Navigation</li>
			<li>
				Reducing Navigational Clutter</li>
			<li>
				Avoid 3D for Navigation</li>
		</ul>
	</li>
	<li>
		Subsites</li>
	<li>
		Search Capabilities
		<ul>
			<li>
				Don&#39;t Search the Web</li>
			<li>
				Micro-Navigation</li>
			<li>
				Global Search</li>
			<li>
				Advanced Search</li>
			<li>
				The Search Results Page</li>
			<li>
				Page Descriptions and Keywords</li>
			<li>
				Use a Wide Search Box</li>
			<li>
				See What People Search For</li>
			<li>
				Search Destination Design</li>
			<li>
				Integrating Sites and Search Engines</li>
		</ul>
	</li>
	<li>
		URL Design
		<ul>
			<li>
				Compound Domain Names</li>
			<li>
				Fully Specify URLs in HTML Code</li>
			<li>
				URL Guessing</li>
			<li>
				Beware of the Os and 0s</li>
			<li>
				Archival URLs</li>
			<li>
				Y2K URL</li>
			<li>
				Advertising a URL</li>
			<li>
				Supporting Old URLs</li>
		</ul>
	</li>
	<li>
		User-Contributed Content</li>
	<li>
		Applet Navigation
		<ul>
			<li>
				Double-Click</li>
			<li>
				Slow Operations</li>
		</ul>
	</li>
	<li>
		Conclusion</li>
</ul>
<p style="margin-left: 40px;">
	<strong>5. Intranet Design </strong></p>
<ul style="margin-left: 40px;">
	<li>
		Differentiating Intranet Design from Internet Design</li>
	<li>
		Extranet Design</li>
	<li>
		Improving the Bottom Line Through Employee Productivity
		<ul>
			<li>
				Average Versus Marginal Costs</li>
		</ul>
	</li>
	<li>
		Intranet Portals:</li>
	<li>
		The Corporate Information Infrastructure
		<ul>
			<li>
				Get Rid of Email</li>
			<li>
				Intranet Maintenance</li>
			<li>
				The Big Three Infrastructure Components: Directory, Search, and News</li>
		</ul>
	</li>
	<li>
		Intranet Design Standards
		<ul>
			<li>
				Guidelines for Standards</li>
			<li>
				Outsourcing Your Intranet Design</li>
		</ul>
	</li>
	<li>
		Managing Employees&#39; Web Access
		<ul>
			<li>
				Hardware Standards</li>
			<li>
				Browser Defaults</li>
			<li>
				Search Engine Defaults</li>
		</ul>
	</li>
	<li>
		Intranet User Testing
		<ul>
			<li>
				Field Studies</li>
			<li>
				Don&#39;t Videotape in the Field</li>
		</ul>
	</li>
	<li>
		Conclusion</li>
</ul>
<p style="margin-left: 40px;">
	<strong>6. Accessibility for Users with Disabilities </strong></p>
<ul style="margin-left: 40px;">
	<li>
		Web Accessibility Initiative
		<ul>
			<li>
				Disabilities Associated with Aging</li>
			<li>
				Assistive Technology</li>
		</ul>
	</li>
	<li>
		Visual Disabilities
		<ul>
			<li>
				ALT Attributes</li>
		</ul>
	</li>
	<li>
		Auditory Disabilities</li>
	<li>
		Speech Disabilities</li>
	<li>
		Motor Disabilities</li>
	<li>
		Cognitive Disabilities
		<ul>
			<li>
				Search Without Spelling</li>
		</ul>
	</li>
	<li>
		Conclusion: Pragmatic Accessibility</li>
</ul>
<p style="margin-left: 40px;">
	<strong>7. International Use: Serving a Global Audience </strong></p>
<ul style="margin-left: 40px;">
	<li>
		Internationalization Versus Localization</li>
	<li>
		Designing for Internationalization</li>
	<li>
		International Inspection
		<ul>
			<li>
				Should Domains End in .com?</li>
		</ul>
	</li>
	<li>
		Translated and Multilingual Sites
		<ul>
			<li>
				Language Choice</li>
			<li>
				Make Translations Bookmarkable</li>
			<li>
				Multilingual Search</li>
		</ul>
	</li>
	<li>
		Regional Differences</li>
	<li>
		International User Testing
		<ul>
			<li>
				Overcoming the Language Gap</li>
			<li>
				How Many Countries Should You Test?</li>
			<li>
				Thanking Your Participants</li>
		</ul>
	</li>
	<li>
		Methods of Testing
		<ul>
			<li>
				Travel Yourself</li>
			<li>
				Add a Few Days to Your Stay</li>
			<li>
				Remote User Testing</li>
			<li>
				Usability Labs for International Testing</li>
		</ul>
	</li>
	<li>
		Self-Administered Tests</li>
	<li style="margin-left: 40px;">
		Conclusion</li>
</ul>
<p style="margin-left: 40px;">
	<strong>8. Future Predictions: The Only Web Constant Is Change </strong></p>
<ul style="margin-left: 40px;">
	<li>
		The Internet Is Hard</li>
	<li>
		Long-Term Trends
		<ul>
			<li>
				The Anti-Mac User Interface</li>
		</ul>
	</li>
	<li>
		Information Appliances
		<ul>
			<li>
				Drawing a Computer</li>
			<li>
				The Invisible Computer</li>
			<li>
				WebTV</li>
			<li>
				Designing for WebTV</li>
		</ul>
	</li>
	<li>
		Death of Web Browsers</li>
	<li>
		Slowly Increasing Bandwidth</li>
	<li>
		Metaphors for the Web
		<ul>
			<li>
				Different Media, Different Strengths</li>
			<li>
				The Telephone</li>
			<li>
				Telephone Usability Problems</li>
			<li>
				Contact Tokens</li>
			<li>
				The Television</li>
		</ul>
	</li>
	<li>
		Restructuring Media Space: Good-Bye, Newspapers
		<ul>
			<li>
				Media Distinctions Caused by Technology</li>
		</ul>
	</li>
	<li>
		Conclusion</li>
</ul>
<p style="margin-left: 40px;">
	<strong>9. Conclusion: Simplicity in Web Design </strong></p>
<p style="margin-left: 40px;">
	Recommended Readings</p>
<p>
	&nbsp;</p>
<h2>
	History of the Book Title</h2>
<p>
	For a long time, the working title for this book was Designing Excellent Websites: Secrets of an Information Architect, but we finally decided that this title was too convoluted (as well as illogical: once you print 250,000 copies of something, it&#39;s not exactly a &quot;secret&quot;). For the final title, simplicity rules as that is the core message of the book.</p>
<h2>
	&nbsp;</h2>
<h2>
	Also Translated Into</h2>
<ul>
	<li>
		Bulgarian</li>
	<li>
		Czech</li>
	<li>
		Finnish</li>
	<li>
		Greek</li>
	<li>
		Hebrew</li>
	<li>
		Hungarian</li>
	<li>
		Korean</li>
	<li>
		Norwegian</li>
	<li>
		Portuguese</li>
	<li>
		Russian</li>
	<li>
		Serbian</li>
	<li>
		Swedish</li>
</ul>
<h2>
	Translations</h2>
<table cellpadding="3" cellspacing="3" width="100%">
	<tbody>
		<tr>
			<td width="50%">
				<img alt="Book cover for the Bulgarian translation of Designing Web Usability" src="http://media.nngroup.com.s3.amazonaws.com/media/editor/2013/01/09/designing_bulgarian_small.gif" style="width: 175px; height: 237px;" /></td>
			<td width="50%">
				<a href="http://product.dangdang.com/main/product.aspx?product_id=9053856"><img alt="Book cover for the Chinese translation of Designing Web Usability" src="http://media.nngroup.com.s3.amazonaws.com/media/editor/2013/01/09/designing_cover_china_small.jpg" style="width: 175px; height: 243px;" /> </a></td>
		</tr>
		<tr>
			<td width="50%">
				<p>
					Bulgarian<br />
					ISBN 954-685-189-2</p>
				<p>
					&nbsp;</p>
			</td>
			<td width="50%">
				<p>
					<a href="http://product.dangdang.com/main/product.aspx?product_id=9053856">Chinese at dangdang.com </a></p>
				<p>
					Web 可用性设计<br />
					ISBN 7-115-08726-1</p>
			</td>
		</tr>
		<tr>
			<td width="50%">
				<img alt="Book cover for the Czech translation of Designing Web Usability" src="http://media.nngroup.com.s3.amazonaws.com/media/editor/2013/01/09/designing-czechl.gif" style="width: 175px; height: 244px;" /></td>
			<td width="50%">
				<a href="http://www.adlibris.com/dk/product.aspx?isbn=8778434793"><img alt="Book cover for the Danish translation of Designing Web Usability" src="http://media.nngroup.com.s3.amazonaws.com/media/editor/2013/01/09/designing_danish.gif" style="width: 175px; height: 252px;" /> </a></td>
		</tr>
		<tr>
			<td width="50%">
				<p>
					Czech<br />
					<em>web.design </em><br />
					ISBN 8086497275</p>
				<p>
					&nbsp;</p>
			</td>
			<td width="50%">
				<p>
					<a href="http://www.adlibris.com/dk/product.aspx?isbn=8778434793">Danish at adlibris.com </a></p>
				<p>
					Godt webdesign<br />
					ISBN 87-7843-479-3</p>
			</td>
		</tr>
		<tr>
			<td>
				<a href="http://www.bol.com/nl/p/functioneel-webdesign/666881379/"><img alt="Book cover of the Dutch translation of Designing Web Usability" src="http://media.nngroup.com.s3.amazonaws.com/media/editor/2013/01/09/designing_dutch.jpg" style="width: 175px; height: 253px;" /> </a></td>
			<td>
				<img alt="Book cover of the Finnish translation of Designing Web Usability" src="http://media.nngroup.com.s3.amazonaws.com/media/editor/2013/01/09/designing_finnish.jpg" style="width: 175px; height: 237px;" /></td>
		</tr>
		<tr>
			<td>
				<p>
					<a href="http://www.bol.com/nl/p/functioneel-webdesign/666881379/">Dutch at bol.com </a></p>
				<p>
					Functioneel webdesign:&nbsp;De kracht van eenvoud<br />
					ISBN 90-430-0383-2</p>
				<p>
					&nbsp;</p>
			</td>
			<td>
				<p>
					Finnish</p>
				<p>
					WWW-Suunnittelu - K&auml;ytett&auml;vyys<br />
					ISBN 951-826-203-9</p>
			</td>
		</tr>
		<tr>
			<td>
				<a href="http://www.amazon.fr/exec/obidos/ASIN/2744018651"><img alt="Book cover of the French translation of Designing Web Usability" src="http://media.nngroup.com.s3.amazonaws.com/media/editor/2013/01/09/designing_french.gif" style="width: 175px; height: 246px;" /> </a></td>
			<td>
				<a href="http://www.amazon.de/exec/obidos/ASIN/3827262062"><img alt="Book cover of the German translation of Designing Web Usability" src="http://media.nngroup.com.s3.amazonaws.com/media/editor/2013/01/09/designing_german.gif" style="width: 175px; height: 218px;" /> </a></td>
		</tr>
		<tr>
			<td>
				<p>
					<a href="http://www.amazon.fr/exec/obidos/ASIN/2744018651">French at amazon.fr </a></p>
				<p>
					Conception de sites Web:<br />
					L&#39;art de la simplicit&eacute;<br />
					ISBN 2-7440-0887-7 (1st edition)<br />
					ISBN 2-7440-1865-1 (2nd edition)</p>
			</td>
			<td>
				<p>
					<a href="http://www.amazon.de/exec/obidos/ASIN/3827262062">German and amazon.de </a></p>
				<p>
					Erfolg des Einfachen<br />
					ISBN 3-8272-5779-4 (1st edition)<br />
					ISBN 3-8272-6206-4 (2nd edition)<br />
					ISBN 3-8272-6846-X (3rd edition)<br />
					The second edition is a new translation and uses the &quot;standard&quot; blue-green cover. I preferred the German title used for the first edition, so I am still showing a scan of the first edition here. Except for the title, it&#39;s obviously recommended to buy the third edition and get the benefits of the improved translation.</p>
			</td>
		</tr>
		<tr>
			<td>
				<img alt="Book cover of the Hebrew translation of Designing Web Usability" src="http://media.nngroup.com.s3.amazonaws.com/media/editor/2013/01/09/designing_hebrew.gif" style="width: 175px; height: 246px;" /></td>
			<td>
				<img alt="Book cover for Hungarian translation of Designing Web Usability" src="http://media.nngroup.com.s3.amazonaws.com/media/editor/2013/01/09/designing_hungarian.jpg" style="width: 175px; height: 243px;" /></td>
		</tr>
		<tr>
			<td>
				<p>
					Hebrew<br />
					ISBN 965-361-274-3</p>
				<p>
					&nbsp;</p>
			</td>
			<td>
				<p>
					Hungarian<br />
					Web-design<br />
					ISBN 963-9326-26-7</p>
			</td>
		</tr>
		<tr>
			<td>
				<a href="http://www.apogeonline.com/libri/88-7303-686-4/scheda"><img alt="Book cover for Italian translation of Designing Web Usability" src="http://media.nngroup.com.s3.amazonaws.com/media/editor/2013/01/09/designing_italian.gif" style="width: 175px; height: 262px;" /> </a></td>
			<td>
				<a href="http://www.amazon.co.jp/exec/obidos/ASIN/4844355627"><img alt="Book cover of the Japanese translation of Designing Web Usability" src="http://media.nngroup.com.s3.amazonaws.com/media/editor/2013/01/09/designing_japan.gif" style="width: 175px; height: 258px;" /> </a></td>
		</tr>
		<tr>
			<td>
				<p>
					<a href="http://www.apogeonline.com/libri/88-7303-686-4/scheda">Italian at Apogeeonline.com </a><br />
					Web usability: La pratica della semplicit&agrave;<br />
					ISBN 88-7303-686-4</p>
			</td>
			<td>
				<p>
					<a href="http://www.amazon.co.jp/exec/obidos/ASIN/4844355627">Japanese at Amazon.co.jp </a><br />
					ウェブ・ユーザビリティ―顧客を逃がさないサイトづくりの秘訣 [単行本</p>
				<p>
					ISBN 4-8443-5562-7</p>
			</td>
		</tr>
		<tr>
			<td>
				<img alt="Book cover of the Korean translation of Designing Web Usability" src="http://media.nngroup.com.s3.amazonaws.com/media/editor/2013/01/09/designing_korean.gif" style="width: 175px; height: 230px;" /></td>
			<td>
				<img alt="Book cover of the Norwegian translation of Designing Web Usability" src="http://media.nngroup.com.s3.amazonaws.com/media/editor/2013/01/09/designing_norwegian_.gif" style="width: 175px; height: 239px;" /></td>
		</tr>
		<tr>
			<td>
				<p>
					Korean<br />
					ISBN 89-7059-161-3</p>
			</td>
			<td>
				<p>
					Norwegian<br />
					Funksjonell webdesign<br />
					ISBN 82-412-0528-7</p>
			</td>
		</tr>
		<tr>
			<td>
				<a href="http://helion.pl/ksiazki/projektowanie-funkcjonalnych-serwisow-internetowych-jakob-nielsen,pfunki.htm"><img alt="Book cover of the Polish translation of Designing Web Usability" src="http://media.nngroup.com.s3.amazonaws.com/media/editor/2013/01/09/designing_polish.gif" style="width: 175px; height: 261px;" /> </a></td>
			<td>
				<img alt="Book cover of the Portuguese translation of Designing Web Usability" src="http://media.nngroup.com.s3.amazonaws.com/media/editor/2013/01/09/designing_brazil.gif" style="width: 175px; height: 244px;" /></td>
		</tr>
		<tr>
			<td>
				<p>
					<a href="http://helion.pl/ksiazki/projektowanie-funkcjonalnych-serwisow-internetowych-jakob-nielsen,pfunki.htm">Polish at Helion.pl </a><br />
					Projektowanie funkcjonalnych serwis&oacute;w internetowych: Prostota funkcjonalność ergonomia<br />
					ISBN 83-7197-928-2</p>
			</td>
			<td>
				<p>
					Portuguese (Brazil)<br />
					Projetando Websites: A Pr&aacute;tica da Simplicidade<br />
					ISBN 85-352-0656-6</p>
			</td>
		</tr>
		<tr>
			<td>
				<img alt="Book cover of the Russian translation of Designing Web Usability" src="http://media.nngroup.com.s3.amazonaws.com/media/editor/2013/01/09/designing_russia.gif" style="width: 175px; height: 227px;" /></td>
			<td>
				<img alt="Book cover of the Serbian translation of Designing Web Usability" src="http://media.nngroup.com.s3.amazonaws.com/media/editor/2013/01/09/designing_serbian.gif" style="width: 175px; height: 254px;" /></td>
		</tr>
		<tr>
			<td>
				<p>
					Russian<br />
					ISBN 5-93286-004-9</p>
			</td>
			<td>
				<p>
					Serbian<br />
					Dizajn funkcionalnih Web strana: U potrazi za jednostravnim<br />
					ISBN 86-7991-139-9</p>
			</td>
		</tr>
		<tr>
			<td>
				<a href="http://www.amazon.com/exec/obidos/ISBN=8420530085/ref=nosim/useitcomusableinA/"><img alt="Book cover of the Spanish translation of Designing Web Usability" src="http://media.nngroup.com.s3.amazonaws.com/media/editor/2013/01/09/designing_spanish.gif" style="width: 175px; height: 245px;" /> </a></td>
			<td>
				<a href="http://www.prisjakt.nu/bok.php?p=43588"><img alt="Book cover of the Swedish translation of Designing Web Usability" src="http://media.nngroup.com.s3.amazonaws.com/media/editor/2013/01/09/designing_swedish.gif" style="width: 175px; height: 244px;" /> </a></td>
		</tr>
		<tr>
			<td>
				<p>
					<a href="http://www.amazon.com/exec/obidos/ISBN=8420530085/ref=nosim/useitcomusableinA/">Spanish at Amazon.com </a><br />
					Usabilidad: Dise&ntilde;o de sitios Web<br />
					ISBN 84-205-3008-5</p>
			</td>
			<td>
				<p>
					<a href="http://www.prisjakt.nu/bok.php?p=43588">Swedish at Prisjakt.nu </a><br />
					Anv&auml;ndbar Webbdesign<br />
					ISBN 91-47-03612-5</p>
			</td>
		</tr>
	</tbody>
</table>
<h2>
	Errata</h2>
<p>
	You can tell <strong> what printing you have </strong> as follows:</p>
<ol>
	<li>
		Locate the copyright page (page <em> ii </em> , but it&#39;s not numbered: this is on the back of the title page which shows the book name in color and just across from the &quot;Contents at a Glance&quot; page)</li>
	<li>
		On the middle of the page is a line of numbers looking like this:<br />
		<code>03 02 01 00 &nbsp; &nbsp; &nbsp; 7 6 5 4 3 2 </code></li>
	<li>
		The <strong> rightmost number </strong> on this line indicates the printing (the above example would be for the second printing).</li>
</ol>
<h3>
	Addendum to 3rd, 4th, and 5th Printings</h3>
<p>
	Page 330, figure caption:<br />
	There are also problems with the spelling and accents for German and Spanish; the use of Simplified Chinese characters will cause difficulties for users in Hong Kong and Taiwan. This latter issue is particularly important: when we run usability studies that involve Chinese users, we always try to include both Simplified and Traditional regions (assuming a written interface; auditory interfaces obviously have to include Mandarin, Cantonese, etc.).</p>
<h3>
	Errors in 2nd and 1st Printing (Fixed in 3rd Printing)</h3>
<ul>
	<li>
		Page 34, figure caption: slope of recent transitions is 1% per <em> week </em> , not per year.</li>
	<li>
		Pages 40-41: Figure captions are reversed. Page 40 shows Internet Explorer 3.01 and page 41 shows Netscape 4.01.</li>
	<li>
		Page 209: Figure caption refers to a different example than the one used in the book. Correct caption: &quot;In this example, I was interested in HTML tools for web design, and a click on that option would give me a list of the tools. But the display provides additional breadth by also showing me the other types of web design tools discussed on the site. Stepping up one level takes us further from the user&#39;s current interest, so less breadth is provided. The third column indicates that more information is available about other Internet-related issues, but these topics are not described in as much detail as the Web design tools. Going one step farther up, the second column lists other computer-oriented topics, but this level is described at a very coarse level of granularity.&quot;</li>
</ul>
<p>
	Also, some smaller typos were found and corrected.</p>
<h3>
	Errors in 1st Printing (Fixed in 2nd Printing)</h3>
<ul>
	<li>
		Page 35: The word &quot;gizmo&quot; is missing from the second paragraph.</li>
	<li>
		Page 395: The names of Louis Rosenfeld and Peter Morville are misspelled. My apologies to Louis and Peter! This is twice as embarrassing because these guys are my good friends.</li>
</ul>
<p>
	Also, many smaller typos were found and corrected.</p>


    <div class="show-for-small-down purchase-from">
      <hr>
      
  <h2>Purchase from</h2>
  
    <p><a href="http://www.amazon.com/dp/156205810X?tag=useitcomusablein">Amazon.com</a></p>
  
  
    
    
      <ul class="no-bullet">
        <li><strong>UK/Europe</strong></li>
        
          <li><a href="http://www.amazon.co.uk/dp/156205810X?tag=useit-21">Buy from Amazon.co.uk</a></li>
        
      </ul>
    
      <ul class="no-bullet">
        <li><strong>Other</strong></li>
        
          <li><a href="http://product.dangdang.com/product.aspx?product_id=9053856">CHINESE: Web 可用性设计</a></li>
        
          <li><a href="http://www.saxo.com/item/3817537">DANISH: Godt webdesign </a></li>
        
          <li><a href="http://books.google.com/books/about/Functioneel_webdesign.php?id=-QL1uoz7BloC">DUTCH: Functioneel webdesign: De kracht van eenvoud </a></li>
        
          <li><a href="http://www.amazon.fr/exec/obidos/ASIN/2744018651">FRENCH: Conception de sites Web : L&#39;art de la simplicité</a></li>
        
          <li><a href="http://www.amazon.de/exec/obidos/ASIN/3827262062">GERMAN: Erfolg des Einfachen</a></li>
        
          <li><a href="http://www.apogeonline.com/catalogo/686.php">ITALIAN: Web usability: La pratica della semplicità </a></li>
        
          <li><a href="http://www.amazon.co.jp/exec/obidos/ASIN/4844355627">JAPANESE: ウェブ・ユーザビリティ―顧客を逃がさないサイトづくりの秘訣</a></li>
        
          <li><a href="http://helion.pl/ksiazki/pfunki.htm">POLISH: Projektowanie funkcjonalnych serwisów internetowych: Prostota funkcjonalność ergonomia </a></li>
        
          <li><a href="http://www.amazon.com/exec/obidos/ISBN=8420530085/ref=nosim/useitcomusableinA/">SPANISH: Usabilidad: Diseño de sitios Web </a></li>
        
      </ul>
    
  


    </div>
    <div class="related">
      


  <hr>



  <h3>Related Reports</h3>
  <ul class="no-bullet">
    
      <li><a href="../../reports/how-people-read-web-eyetracking-evidence/index.php">How People Read on the Web: The Eyetracking Evidence</a></li>
    
      <li><a href="../../reports/b2b-websites-usability/index.php">B2B Website Usability for Converting Users into Leads and Customers</a></li>
    
      <li><a href="../../reports/strategic-design-faqs/index.php">Strategic Design for Frequently Asked Questions</a></li>
    
      <li><a href="../../reports/social-media-user-experience/index.php">Social Media User Experience</a></li>
    
      <li><a href="../../reports/pr-websites/index.php">PR on Websites</a></li>
    
  </ul>



  <h3>Related Courses</h3>
   <ul class="no-bullet">
    
        <li><a href="../../courses/ux-basic-training/index.php">UX Basic Training</a></li>
    
        <li><a href="../../courses/human-mind/index.php">The Human Mind and Usability</a></li>
    
        <li><a href="../../courses/emerging-patterns-web-design/index.php">Emerging Patterns for Web Design</a></li>
    
        <li><a href="../../courses/web-page-design/index.php">Web Page UX Design</a></li>
    
        <li><a href="../../courses/content-strategy/index.php">Content Strategy</a></li>
    
</ul>



  <h3>Articles</h3>
  <ul class="no-bullet">
    
      <li><a href="../../articles/menu-design/index.php">Menu Design: Checklist of 15 UX Guidelines to Help Users </a></li>
    
      <li><a href="../../articles/flat-design/index.php">Flat Design: Its Origins, Its Problems, and Why Flat 2.0 Is Better for Users</a></li>
    
      <li><a href="../../articles/page-fold-manifesto/index.php">The Fold Manifesto: Why the Page Fold Still Matters</a></li>
    
      <li><a href="../../articles/scaling-user-interfaces/index.php">Scaling User Interfaces: An Information-Processing Approach to Multi-Device Design</a></li>
    
      <li><a href="../../articles/top-10-mistakes-web-design/index.php">Top 10 Mistakes in Web Design</a></li>
    
  </ul>


    </div>
  </div>

  <div class="small-12 medium-4 columns hide-for-small-down l-subsection">
    
  <h2>Purchase from</h2>
  
    <p><a href="http://www.amazon.com/dp/156205810X?tag=useitcomusablein">Amazon.com</a></p>
  
  
    
    
      <ul class="no-bullet">
        <li><strong>UK/Europe</strong></li>
        
          <li><a href="http://www.amazon.co.uk/dp/156205810X?tag=useit-21">Buy from Amazon.co.uk</a></li>
        
      </ul>
    
      <ul class="no-bullet">
        <li><strong>Other</strong></li>
        
          <li><a href="http://product.dangdang.com/product.aspx?product_id=9053856">CHINESE: Web 可用性设计</a></li>
        
          <li><a href="http://www.saxo.com/item/3817537">DANISH: Godt webdesign </a></li>
        
          <li><a href="http://books.google.com/books/about/Functioneel_webdesign.php?id=-QL1uoz7BloC">DUTCH: Functioneel webdesign: De kracht van eenvoud </a></li>
        
          <li><a href="http://www.amazon.fr/exec/obidos/ASIN/2744018651">FRENCH: Conception de sites Web : L&#39;art de la simplicité</a></li>
        
          <li><a href="http://www.amazon.de/exec/obidos/ASIN/3827262062">GERMAN: Erfolg des Einfachen</a></li>
        
          <li><a href="http://www.apogeonline.com/catalogo/686.php">ITALIAN: Web usability: La pratica della semplicità </a></li>
        
          <li><a href="http://www.amazon.co.jp/exec/obidos/ASIN/4844355627">JAPANESE: ウェブ・ユーザビリティ―顧客を逃がさないサイトづくりの秘訣</a></li>
        
          <li><a href="http://helion.pl/ksiazki/pfunki.htm">POLISH: Projektowanie funkcjonalnych serwisów internetowych: Prostota funkcjonalność ergonomia </a></li>
        
          <li><a href="http://www.amazon.com/exec/obidos/ISBN=8420530085/ref=nosim/useitcomusableinA/">SPANISH: Usabilidad: Diseño de sitios Web </a></li>
        
      </ul>
    
  


  </div>

</div>

    </div>
    




<footer id="page-footer">
    <div class="row hide-for-print">
        <div class="column small-12 medium-6">
            <section class="footer-popular">
                
                <h1>Popular Topics</h1>
                <div style="float: left;  width: 15em; display: block;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/applications/index.php">Application Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/e-commerce/index.php">E-Commerce Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/eyetracking/index.php">Eyetracking </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/information-architecture/index.php">Information Architecture </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/intranets/index.php">Intranet Design </a></li>
	<li><a href="../../topic/interaction-design/index.php">Interaction Design </a></li>
</ul>
</div>

<div style="float: left;  width: 15em; display: block; list-style: none;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/mobile-and-tablet-design/index.php">Mobile and Tablet Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/credibility-persuasion/index.php">Persuasive Design</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/research-methods/index.php">Research Methods</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/user-testing/index.php">User Testing </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/web-usability/index.php">Web Usability </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/writing-web/index.php">Web Writing &amp; Content Strategy </a></li>
</ul>
</div>

<p>&nbsp;</p>

                
            </section>
        </div>

        <div class="column small-12 medium-6">
            <div class="row">
                <div class="column small-6">
                    <section class="footer-about">
                        <h1>Follow Us</h1>
                        <ul>
	<li><a href="../../articles/subscribe/index.php">Subscribe to our weekly UX newsletter</a>,&nbsp;Jakob Nielsen&#39;s Alertbox</li>
	<li>Visit Don Norman&#39;s website, <a href="http://www.jnd.org/"> www.jnd.org </a></li>
	<li>Visit Bruce Tognazzini&#39;s website, <a href="http://www.asktog.com/"> www.asktog.com </a></li>
	<li style="padding-top: 6px;"><a class="twitter-follow-button" data-show-count="false" data-size="large" href="https://twitter.com/nngroup"><img alt="Follow @nngroup on Twitter" height="20" src="https://media.nngroup.com/media/editor/2016/12/30/twitter-follow-button.png" style="width: 117px; height: 20px;" width="117" /></a></li>
	<li style="padding-top: 6px;"><img alt="" height="14" src="https://media.nngroup.com/media/editor/2016/07/26/feed-icon-14x14.png" style="vertical-align:text-top;width:14px;margin-right:.25em;" width="14" /><a href="../../feed/rss/index.php">Subscribe to our RSS feed</a></li>
</ul>

                    </section>
                </div>

                <div class="column small-6">
                    <section class="footer-about right-column">
                        <h1>About
</h1>
                        <ul class="no-bullet">
	<li><a href="../../about/contact/index.php">Contact information</a></li>
	<li><a href="../../about/index.php">About Us</a></li>
	<li><a href="../../about/why-nng/index.php">Why NN/g? </a></li>
	<li><a href="../../return-policy/index.php">Return Policy</a></li>
</ul>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="copyright column small-12">
            <p><a href="../../copyright-and-reprint-info/index.php">Copyright</a> &copy; 1998-2017 Nielsen Norman Group, All Rights Reserved.</p>
        </div>
    </div>
</footer>


    


        <script src="https://media.nngroup.com/static/vendor/fastclick/js/fastclick.min.js"></script>
        <script src="https://media.nngroup.com/static/vendor/foundation/js/foundation.min.js"></script>
        <script src="http://media.nngroup.com/static/js/app.js"></script>
        <script src="http://media.nngroup.com/static/js/scroll-to-fixed.js"></script><script>var externalLinks = document.querySelectorAll('a[href^="http"]');for (var i = externalLinks.length-1; i >= 0; i--) {externalLinks[i].addEventListener("click", function(event) { alert("External links are disabled for this test."); event.preventDefault(); }, false);}var mailLinks = document.querySelectorAll('a[href^="mailto"]');for (var i = mailLinks.length-1; i >= 0; i--) {mailLinks[i].addEventListener("click", function(event) { alert("Email links are disabled for this test."); event.preventDefault(); }, false);}var externalForms = document.querySelectorAll('form');for (var i = externalForms.length-1; i >= 0; i--) {externalForms[i].addEventListener("submit", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false); externalForms[i].addEventListener("click", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false);}</script>

        <script>
            
            $(function() {
                $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
                $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
            });
        </script>

        


        
        <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1072699960;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072699960/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
    </body>

<!-- Mirrored from www.nngroup.com/books/designing-web-usability/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:02:39 GMT -->
</html>
