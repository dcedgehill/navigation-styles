<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->





    
<!-- Mirrored from www.nngroup.com/topic/design-patterns/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:11:57 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1026.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"licenseKey":"1a30ef9931","errorBeacon":"bam.nr-data.net","applicationID":"2048992","transactionName":"NFxSMRBUX0EDUkIPDA0WdhAMVkVbDV8ZBxEXUFMJB0YfRAtUQRVZIktEDAFZVGYNQV8FLwpKREsFUEU=","beacon":"bam.nr-data.net","queueTime":0,"applicationTime":534,"agent":""}</script>
        <title>Design Patterns Articles, Reports, and Training Courses | NN/g</title><meta property="og:title" content="Design Patterns Articles, Reports, and Training Courses | NN/g" />
  
        
        <meta name="description" content="Design patterns help define and describe successful user interface elements so that they can be re-used and applied in new designs. Patterns allow designers to build great interfaces without re-inventing the wheel. These articles, reports and training courses include guidelines and tips on creating and using design patterns effectively.  ">
        <meta property="og:description" content="Design patterns help define and describe successful user interface elements so that they can be re-used and applied in new designs. Patterns allow designers to build great interfaces without re-inventing the wheel. These articles, reports and training courses include guidelines and tips on creating and using design patterns effectively.  " />
        
  
        
	
        
        <meta name="keywords" content="design patterns, pattern library, ui patterns">
        

        

        <meta name="robots" content="noarchive">
        <meta name="viewport" content="width=device-width">

        <link rel="search" type="application/opensearchdescription+xml" title="NN/g Search" href="../../opensearch.xml" />
        <link rel="shortcut icon" href="http://media.nngroup.com/static/img/favicon.ico" />

        <link rel="stylesheet" href="https://media.nngroup.com/static/vendor/font-awesome/css/font-awesome.min.css">

        
        <link rel="stylesheet" href="https://media.nngroup.com/static/css/7232a960f1df.css" type="text/css" />
        <link rel="stylesheet" href="http://fast.fonts.com/t/1.css?apiType=css&amp;projectid=c5846260-9def-46ec-9407-0f1349bd2d64" type="text/css">

        <script src="https://media.nngroup.com/static/vendor/modernizr/js/modernizr.min.js"></script>

        <script type="text/javascript" src="https://media.nngroup.com/static/js/3283be5def74.js"></script>
        

        
<link rel="canonical" href="index.php" />


        
        
        <!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=229034,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('http://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->


<!-- Google Analytics Tracking Code for http://nngroup.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23132681-1', 'nngroup.com');
  ga('require', 'linkid', 'https://www.nngroup.com/topic/design-patterns/linkid.js');
  ga('require', 'displayfeatures');

// Optimizely Universal Analytics Integration
 window.optimizely = window.optimizely || [];
 window.optimizely.push("activateUniversalAnalytics");

  ga('send', 'pageview');

</script>
<script>
/**
* Function to track a click on an outbound link in Google Analytics.
*/
function trackOutboundLink(label, url) {
    if(window.ga && ga.loaded) {
        ga('send', 'event', 'outbound', 'click', label, {'hitCallback': function(){document.location = url;}
        });
    } else {
        document.location = url;
    }
}
/**
* Function to track a click on a link in the article announcement area.
*/
$('#articleAnnouncement a').on('click', function(){
  ga('send', 'event', 'link', 'click', 'articleAnnouncement');
});
</script>

<script type="text/javascript">
 setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2421.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



<!-- Hotjar Tracking Code for http://nngroup.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:121605,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

        <script src="https://media.nngroup.com/static/js/analytics/track_outbound_link.js"></script>
        
        
        
    </head>
    <body class="location-articles articles-topic">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
    <header id="page-header">
        <div id="cart" class="shopping-cart mobile-cart show-for-small-down"></div>
        <div class="row">
            <div id="masthead" class="masthead small-12 medium-7 column">

                <!-- The button to toggles menu on small screen -->

                <h1><a href="../../index.php"><img src="http://media.nngroup.com/static/img/logo-wordmark.png" width="74" height="30"> Nielsen <span class="norman">Norman</span> Group</a></h1>
                <p class="tagline show-for-medium-up">Evidence-Based User Experience Research, Training, and Consulting</p>

                <!-- The button to toggles form search on small screen -->


                <div style="clear: right;width:  100%;height: 1px;"></div><?php define('ROOT_PATH',$_SERVER["DOCUMENT_ROOT"] . '/apps/navigation_styles/navigations/3'); ?><?php include(ROOT_PATH)."/navigation.php"; ?>

                <!-- Drop down menu on small screen -->
                <div id="hideNav" class="show-for-small-down">
                  
                  <ul class="menu-content">
                    <li><a href="../../index.php">Home</a></li>
                    <li class="has-submenu">
                      <a>Training</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../training/index.php">Overview</a></li>
                        
<li id="nav-ux-conference"><a href="../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../online-seminars/index.php"><span>Online Seminars</span></a></li>

                      </ul>
                    </li>
                    <li class="has-submenu">
                      <a>Consulting</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
  <li><a href="../../consulting/index.php">Overview</a></li>
  
  <li><a href="../../consulting/benchmarking/index.php">Benchmarking</a></li>
  
  <li><a href="../../consulting/expert-design-review/index.php">Expert Design Review</a></li>
  
  <li><a href="../../consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
  
  <li><a href="../../consulting/usability-testing/index.php">Usability Testing </a></li>
  
  <li><a href="../../consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
  
  <li><a href="../../in-house-training/index.php">In-House Training</a></li>
</ul>

                    </li>

                    <li><a href="../../reports/index.php">Reports</a></li>
                    <li><a href="../../articles/index.php">Articles</a></li>
                    <li class="has-submenu"><a>About NN/G</a><i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../about/index.php">Overview</a></li>
                        <li><a href="../../people/index.php">People</a></li>
                        <li><a href="../../about/why-nng/index.php">Why NN/g?</a></li>
                        <li><a href="../../about/contact/index.php">Contact</a></li>
                        <li><a href="../../news/index.php">News</a></li>
                        <li><a href="../../about/history/index.php">History</a></li>
                        <li><a href="../../books/index.php">Books</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                </div>

                <hr class="header-divider show-for-small-only"/>


            </div>
       </div>

        <div id="hideNav">            <nav class="primary-nav small-12 column">
                <ul>
                    <li><a class="navitem" id="nav-home" href="../../index.php">Home</a></li>
                    <li><a class="navitem" id="nav-training" href="../../training/index.php">Training</a></li>
                    <li><a class="navitem" id="nav-consulting" href="../../consulting/index.php">Consulting</a></li>
                    <li><a class="navitem" id="nav-reports" href="../../reports/index.php">Reports</a></li>
                    <li><a class="navitem" id="nav-articles" href="../../articles/index.php">Articles</a></li>
                    <li><a class="navitem" id="nav-about" href="../../about/index.php">About NN/G</a></li>
                </ul>
            </nav>
            <nav class="secondary-nav small-12 medium-12 large-12 columns">
                
                
            </nav>
        </div>
    </header>
    <div id="mainContent" class="l-content">
    
  

  
  <div class="row">
    <div class="small-12 medium-8 columns l-section-intro">
      <header>
        <h1>Articles</h1>
      </header>
    </div>
  </div>
  

  <div class="row">
    
  
      <section class="small-12 medium-3 columns collapsable-content l-navrail articles-left-column">
        <h1 class="show-for-small-down collapse-link mobile-browse-header">Browse by Topic and Author</h1>
          <div class="row collapsable">

            
            <div class="small-6 medium-12 columns">
              <section id="article-popular-topics" style="display: none;">
                
                <h1>Topics</h1>
                <ul id="article-popular-topics">
                    
                    <li>
                        
                        <a href="../e-commerce/all/index.php">E-commerce</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../intranets/all/index.php">Intranets</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../user-testing/all/index.php">User Testing</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../web-usability/all/index.php">Web Usability</a>
                        
                    </li>
                    
                    <li><a href="#" id="article-see-all-topics"><strong>See all topics&hellip;</strong></a></li>
                </ul>
                
              </section>
              <section id="article-all-topics" style="display: none;">
                
                <h1>
                  <span class="show-for-medium-up">All Article Topics</span>
                  <span class="show-for-small-down">All Topics</span>
                  (<a href="#" id="article-hide-all-topics">hide</a>)
                </h1>
                <ul>
                  
                  <li>
                    
                    <a href="../accessibility/all/index.php">Accessibility</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../agile/all/index.php">Agile</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../analytics-and-metrics/all/index.php">Analytics &amp; Metrics</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../applications/all/index.php">Application Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../b2b-websites/all/index.php">B2B Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../behavior-patterns/all/index.php">Behavior Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../branding/all/index.php">Branding</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../content-strategy/all/index.php">Content Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../corporate-websites/all/index.php">Corporate Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="all/index.php">Design Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../ux-design-process/all/index.php">Design Process</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../e-commerce/all/index.php">E-commerce</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../email/all/index.php">Email</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../eyetracking/all/index.php">Eyetracking</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../heuristic-evaluation/all/index.php">Heuristic Evaluation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../human-computer-interaction/all/index.php">Human Computer Interaction</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../ux-ideation/all/index.php">Ideation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../information-architecture/all/index.php">Information Architecture</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../interaction-design/all/index.php">Interaction Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../international-users/all/index.php">International Users</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../intranets/all/index.php">Intranets</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../ux-management/all/index.php">Management</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../navigation/all/index.php">Navigation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../non-profit-websites/all/index.php">Non-Profit Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../personas/all/index.php">Personas</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../persuasive-design/all/index.php">Persuasive Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../prototyping/all/index.php">Prototyping</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../psychology-and-ux/all/index.php">Psychology and UX</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../research-methods/all/index.php">Research Methods</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../search/all/index.php">Search</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../social-media/all/index.php">Social Media</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../strategy/all/index.php">Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../user-testing/all/index.php">User Testing</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../ux-teams/all/index.php">UX Teams</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../visual-design/all/index.php">Visual Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../web-usability/all/index.php">Web Usability</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../writing-web/all/index.php">Writing for the Web</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../young-users/all/index.php">Young Users</a>
                    
                  </li>
                  
                </ul>
                
              </section>
            </div>
            <script>
              /* Determine which topics list will be displayed: the full or the
               * partial.  We are inlining this close to the list itself to
               * prevent rendering delays on the client
               */
              var topic_list = $.cookie("article-topics");

              var popular_topics = $('#article-popular-topics');
              var all_topics = $('#article-all-topics');

              if(topic_list == 1) {
                  popular_topics.hide();
                  all_topics.show();
              } else {
                  popular_topics.show();
                  all_topics.hide();
              }
            </script>
            

            
            <div class="small-6 medium-12 columns">
              
              <section id="article-initial-authors">
                  <h1>Author</h1>
                  <ul>
                    
                    <li><a href="../../articles/author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                    
                    <li><a href="../../articles/author/don-norman/index.php">Don Norman</a></li>
                    
                    <li><a href="../../articles/author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                    
                    <li><a href="#" onClick="$('#article-initial-authors').hide(); $('#article-all-authors').show();"><strong>See all authors&hellip;</strong></a></li>
                  </ul>
              </section>
              <section id="article-all-authors" style="display: none;">
                <h1>
                  All Authors
                  (<a href="#" id="article-hide-all-authors">hide</a>)
                </h1>
                <ul>
                  
                  <li><a href="../../articles/author/raluca-budiu/index.php">Raluca Budiu</a></li>
                  
                  <li><a href="../../articles/author/patty-caya/index.php">Patty Caya</a></li>
                  
                  <li><a href="../../articles/author/susan-farrell/index.php">Susan Farrell</a></li>
                  
                  <li><a href="../../articles/author/therese-fessenden/index.php">Therese Fessenden</a></li>
                  
                  <li><a href="../../articles/author/kim-flaherty/index.php">Kim Flaherty</a></li>
                  
                  <li><a href="../../articles/author/sarah-gibbons/index.php">Sarah Gibbons</a></li>
                  
                  <li><a href="../../articles/author/aurora-harley/index.php">Aurora Harley</a></li>
                  
                  <li><a href="../../articles/author/kate-kaplan/index.php">Kate Kaplan</a></li>
                  
                  <li><a href="../../articles/author/page-laubheimer/index.php">Page Laubheimer</a></li>
                  
                  <li><a href="../../articles/author/angela-li/index.php">Angie Li</a></li>
                  
                  <li><a href="../../articles/author/hoa-loranger/index.php">Hoa Loranger</a></li>
                  
                  <li><a href="../../articles/author/kate-meyer/index.php">Kate Meyer</a></li>
                  
                  <li><a href="../../articles/author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                  
                  <li><a href="../../articles/author/don-norman/index.php">Don Norman</a></li>
                  
                  <li><a href="../../articles/author/kara-pernice/index.php">Kara Pernice</a></li>
                  
                  <li><a href="../../articles/author/christian-rohrer/index.php">Christian Rohrer</a></li>
                  
                  <li><a href="../../articles/author/amy-schade/index.php">Amy Schade</a></li>
                  
                  <li><a href="../../articles/author/katie-sherwin/index.php">Katie Sherwin</a></li>
                  
                  <li><a href="../../articles/author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                  
                  <li><a href="../../articles/author/kathryn-whitenton/index.php">Kathryn Whitenton</a></li>
                  
                </ul>
              </section>
              
            </div>
            
          </div>

          
          <section class="hide-for-small-down">
            
              
              <h1>Recent Articles</h1>
              
              <ul>
                  
                  <li><a href="../../articles/seamless-cross-channel/index.php">Seamlessness in the Omnichannel User Experience</a></li>
                  
                  <li><a href="../../articles/brand-intention-interpretation/index.php">Experience Design: Bridging Brand Intention and Brand Interpretation</a></li>
                  
                  <li><a href="../../articles/eyetracking-tasks-efficient-scanning/index.php">Scanning Patterns on the Web Are Optimized for the Current Task</a></li>
                  
                  <li><a href="../../articles/flat-design-best-practices/index.php">Flat-Design Best Practices</a></li>
                  
                  <li><a href="../../articles/ux-user-stories/index.php">Accounting for UX Work with User Stories in Agile Projects</a></li>
                  
                  <li><a href="../../articles/index.php"><strong>See all articles&hellip;</strong></a></li>
              </ul>
              
            
          </section>
          

          
          
          <section class="hide-for-small-down">
            <h1>Popular Articles</h1>
            
            <ul>
                
                <li><a href="../../articles/usability-101-introduction-to-usability/index.php">Usability 101: Introduction to Usability</a></li>
                
                <li><a href="../../articles/top-10-mistakes-web-design/index.php">Top 10 Mistakes in Web Design</a></li>
                
                <li><a href="../../articles/how-users-read-on-the-web/index.php">How Users Read on the Web</a></li>
                
                <li><a href="../../articles/f-shaped-pattern-reading-web-content/index.php">F-Shaped Pattern For Reading Web Content</a></li>
                
                <li><a href="../../articles/ten-usability-heuristics/index.php">10 Usability Heuristics for User Interface Design</a></li>
                
                <li><a href="../../articles/intranet-design-2016/index.php">10 Best Intranets of 2016</a></li>
                
                <li><a href="../../articles/which-ux-research-methods/index.php">When to Use Which User-Experience Research Methods</a></li>
                
                <li><a href="../../articles/response-times-3-important-limits/index.php">Response Times: The 3 Important Limits</a></li>
                
                <li><a href="../../articles/why-you-only-need-to-test-with-5-users/index.php">Why You Only Need to Test with 5 Users</a></li>
                
                <li><a href="../../articles/page-fold-manifesto/index.php">The Fold Manifesto: Why the Page Fold Still Matters</a></li>
                
            </ul>
          </section>
          
          

          
            <section class="hide-for-small-down">
              
            </section>
          

      </section>
      

  
    <div class="small-12 medium-7 columns content-body">
      
  <h2 class="topic">Topic: <strong>Design Patterns</strong></h2>
  
      
          
    <article class="alertbox-article-listitem">
        <h1>
            
                <a href="../../articles/date-input/index.php">Date-Input Form Fields: UX Design Guidelines</a>
            
        </h1>
        <h2>January 22, 2017</h2>
        <p>Date-entry fields must be unambiguous and support task completion by using the right design pattern. Small design changes can prevent big user errors.</p>
    </article>

    <article class="alertbox-article-listitem">
        <h1>
            
                <a href="../../articles/zen-mode/index.php">Why Zen Mode Isn’t the Answer to Everything</a>
            
        </h1>
        <h2>December 18, 2016</h2>
        <p>Interfaces that temporarily hide the UI elements to emphasize content often increase the interaction cost, cognitive load, and the number of attention switches.
</p>
    </article>

    <article class="alertbox-article-listitem">
        <h1>
            
                <a href="../../articles/top-intranet-design-mistakes/index.php">The Top Enduring Intranet-Design Mistakes: 7 Deadly Sins</a>
            
        </h1>
        <h2>November 27, 2016</h2>
        <p>Intranet tools and programs come and go, but bad UIs linger for years, impact employee productivity and morale, and ultimately increase organizational costs.</p>
    </article>

    <article class="alertbox-article-listitem">
        <h1>
            
                <a href="../../articles/cards-component/index.php">Cards: UI-Component Definition</a>
            
        </h1>
        <h2>November 6, 2016</h2>
        <p>A “card” is a UI design pattern that groups related information in a flexible-size container visually resembling a playing card. 
</p>
    </article>

    <article class="alertbox-article-listitem">
        <h1>
            
                <a href="../../articles/tabs-used-right/index.php">Tabs, Used Right</a>
            
        </h1>
        <h2>July 9, 2016</h2>
        <p>12 design guidelines for tab controls to distinguish tabs from site navigation and address click uncertainty. </p>
    </article>

    <article class="alertbox-article-listitem">
        <h1>
            
                <a href="../../articles/hamburger-menus/index.php">Hamburger Menus and Hidden Navigation Hurt UX Metrics</a>
            
        </h1>
        <h2>June 26, 2016</h2>
        <p>Discoverability is cut almost in half by hiding a website’s main navigation. Also, task time is longer and perceived task difficulty increases.</p>
    </article>

    <article class="alertbox-article-listitem">
        <h1>
            
                <a href="../../articles/needy-design-patterns/index.php">Needy Design Patterns: Please-Don’t-Go Popups &amp; Get-Back-to-Me Tabs</a>
            
        </h1>
        <h2>May 15, 2016</h2>
        <p>These two overly demanding website design patterns aimed at driving engagement are in conflict with how people utilize browser tabs.</p>
    </article>

    <article class="alertbox-article-listitem">
        <h1>
            
                <a href="../../articles/mobile-navigation-patterns/index.php">Basic Patterns for Mobile Navigation: A Primer</a>
            
        </h1>
        <h2>November 15, 2015</h2>
        <p>Mobile navigation must be discoverable, accessible, and take little screen space. Exposing the navigation and hiding it in a hamburger both have pros and cons.</p>
    </article>

    <article class="alertbox-article-listitem">
        <h1>
            
                <a href="../../articles/common-ux-deliverables/index.php">Which UX Deliverables Are Most Commonly Created and Shared?</a>
            
        </h1>
        <h2>October 18, 2015</h2>
        <p>Static wireframes are the most popular UX deliverable, but 11 different deliverable formats were used by at least half the professionals we surveyed.
</p>
    </article>

    <article class="alertbox-article-listitem">
        <h1>
            
                <a href="../../articles/gui-slider-controls/index.php">Slider Design: Rules of Thumb</a>
            
        </h1>
        <h2>September 13, 2015</h2>
        <p>Selecting a precise value using a slider is a difficult task requiring good motor skills, even if the slider is well designed. If picking an exact value is important to the goal of the interface, choose an alternate UI element.</p>
    </article>

    <article class="alertbox-article-listitem">
        <h1>
            
                <a href="../../articles/4-ios-rules-break/index.php">4 iOS Rules to Break </a>
            
        </h1>
        <h2>July 19, 2015</h2>
        <p>Page control (dots), Submit at top, and the Plus (+) and Move icons are 4 common iOS patterns that cause usability problems in testing.</p>
    </article>

    <article class="alertbox-article-listitem">
        <h1>
            
                <a href="../../articles/animation-usability/index.php">Animation for Attention and Comprehension</a>
            
        </h1>
        <h2>September 21, 2014</h2>
        <p>Moving elements are a powerful tool to attract users’ attention. When designing an animation consider its goal, its frequency of occurrence, and its mechanics.</p>
    </article>

    <article class="alertbox-article-listitem">
        <h1>
            
                <a href="../../articles/breaking-web-conventions/index.php">Breaking Web Design Conventions = Breaking the User Experience</a>
            
        </h1>
        <h2>July 20, 2014</h2>
        <p>Bucknell University caused a stir with its unconventional responsive redesign, but at a high cost to usability, as shown in tests with students and parents.</p>
    </article>

    <article class="alertbox-article-listitem">
        <h1>
            
                <a href="../../articles/accordions-complex-content/index.php">Accordions Are Not Always the Answer for Complex Content on Desktops</a>
            
        </h1>
        <h2>May 18, 2014</h2>
        <p>Longer pages can benefit users. Accordions shorten pages and reduce scrolling, but they increase the interaction cost by requiring people to decide on topic headings.</p>
    </article>

    <article class="alertbox-article-listitem">
        <h1>
            
                <a href="../../articles/horizontal-scrolling/index.php">Beware Horizontal Scrolling and Mimicking Swipe on Desktop  </a>
            
        </h1>
        <h2>April 27, 2014</h2>
        <p>Even as more sites mimic swiping gestures and incorporate horizontal scrolling in desktop designs, users remain reluctant to move sideways through content. </p>
    </article>

    <article class="alertbox-article-listitem">
        <h1>
            
                <a href="../../articles/magnifying-glass-icon/index.php">The Magnifying-Glass Icon in Search Design: Pros and Cons</a>
            
        </h1>
        <h2>February 23, 2014</h2>
        <p>Users recognize a magnifying-glass icon as meaning ‘search’ even without a textual label. The downside is that icon-only search is harder for users to find. </p>
    </article>

    <article class="alertbox-article-listitem">
        <h1>
            
                <a href="../../articles/mobile-instructional-overlay/index.php">Instructional Overlays and Coach Marks for Mobile Apps</a>
            
        </h1>
        <h2>February 16, 2014</h2>
        <p>Instructions in mobile applications must be designed for optimal scannability, as users tend to dismiss them quickly and do not read thoroughly.</p>
    </article>

    <article class="alertbox-article-listitem">
        <h1>
            
                <a href="../../articles/killing-global-navigation-one-trend-avoid/index.php">Killing Off the Global Navigation: One Trend to Avoid</a>
            
        </h1>
        <h2>February 9, 2014</h2>
        <p>For desktop sites, demoting your main content categories into a drop-down menu makes it harder for users to discover your offerings.</p>
    </article>

    <article class="alertbox-article-listitem">
        <h1>
            
                <a href="../../articles/infinite-scrolling/index.php">Infinite Scrolling Is Not for Every Website</a>
            
        </h1>
        <h2>February 2, 2014</h2>
        <p>Endless scrolling saves people from having to attend to the mechanics of pagination in browsing tasks, but is not a good choice for websites that support goal-oriented finding tasks.</p>
    </article>

    <article class="alertbox-article-listitem">
        <h1>
            
                <a href="../../articles/fight-right-rail-blindness/index.php">Fight Against “Right-Rail Blindness”</a>
            
        </h1>
        <h2>October 12, 2013</h2>
        <p>Users have trained themselves to divert their attention away from areas that look like advertising. When designed well, sidebars can effectively increase content discoverability.
</p>
    </article>


      
      <div class="article-pager">
          

    
        <span><span class="show-for-medium-up">Previous &laquo; </span>
        <span class="show-for-small-down">Prev &laquo; </span></span>
    

    
        
            <span class="currentpage">&nbsp;1&nbsp;</span>
        
    
        
            <a href="index4658.php?page=2">&nbsp;2&nbsp;</a>
        
    

    <a href="index4658.php?page=2"> &raquo; Next</a>


      </div>
  

    </div>
  

  
    <div class="small-12 medium-2 columns l-navrail">
    
    
    <div class="right-aside learn-more">
        <h2 class="show-for-small-down">Learn More</h2>
        

        
            <h3>UX Conference Training Courses</h3>
             <ul class="no-bullet">
    
        <li><a href="../../courses/emerging-patterns-web-design/index.php">Emerging Patterns for Web Design</a></li>
    
        <li><a href="../../courses/web-page-design/index.php">Web Page UX Design</a></li>
    
</ul>
        

        

        
    </div>
    

    </div>
  

  </div>
  <div class="row">
    
      <div class="column small-12 show-for-small-down l-navrail announcements">
        
          
        
      </div>
    
    
      <div class="small-12 columns show-for-small-down">
        <a class="button expand subscribe-bt" href="../../articles/subscribe/index.php">Subscribe to receive our weekly articles</a>

      </div>
    
  </div>

    </div>
    




<footer id="page-footer">
    <div class="row hide-for-print">
        <div class="column small-12 medium-6">
            <section class="footer-popular">
                
                <h1>Popular Topics</h1>
                <div style="float: left;  width: 15em; display: block;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../applications/index.php">Application Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../e-commerce/index.php">E-Commerce Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../eyetracking/index.php">Eyetracking </a></li>
	<li style="padding-bottom: .5em;"><a href="../human-computer-interaction/index.php">Human Computer Interaction </a></li>
	<li style="padding-bottom: .5em;"><a href="../information-architecture/index.php">Information Architecture </a></li>
	<li style="padding-bottom: .5em;"><a href="../intranets/index.php">Intranet Design </a></li>
	<li><a href="../interaction-design/index.php">Interaction Design </a></li>
</ul>
</div>

<div style="float: left;  width: 15em; display: block; list-style: none;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../mobile-and-tablet-design/index.php">Mobile and Tablet Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../credibility-persuasion/index.php">Persuasive Design</a></li>
	<li style="padding-bottom: .5em;"><a href="../research-methods/index.php">Research Methods</a></li>
	<li style="padding-bottom: .5em;"><a href="../user-testing/index.php">User Testing </a></li>
	<li style="padding-bottom: .5em;"><a href="../web-usability/index.php">Web Usability </a></li>
	<li style="padding-bottom: .5em;"><a href="../writing-web/index.php">Web Writing &amp; Content Strategy </a></li>
</ul>
</div>

<p>&nbsp;</p>

                
            </section>
        </div>

        <div class="column small-12 medium-6">
            <div class="row">
                <div class="column small-6">
                    <section class="footer-about">
                        <h1>Follow Us</h1>
                        <ul>
	<li><a href="../../articles/subscribe/index.php">Subscribe to our weekly UX newsletter</a>,&nbsp;Jakob Nielsen&#39;s Alertbox</li>
	<li>Visit Don Norman&#39;s website, <a href="http://www.jnd.org/"> www.jnd.org </a></li>
	<li>Visit Bruce Tognazzini&#39;s website, <a href="http://www.asktog.com/"> www.asktog.com </a></li>
	<li style="padding-top: 6px;"><a class="twitter-follow-button" data-show-count="false" data-size="large" href="https://twitter.com/nngroup"><img alt="Follow @nngroup on Twitter" height="20" src="https://media.nngroup.com/media/editor/2016/12/30/twitter-follow-button.png" style="width: 117px; height: 20px;" width="117" /></a></li>
	<li style="padding-top: 6px;"><img alt="" height="14" src="https://media.nngroup.com/media/editor/2016/07/26/feed-icon-14x14.png" style="vertical-align:text-top;width:14px;margin-right:.25em;" width="14" /><a href="../../feed/rss/index.php">Subscribe to our RSS feed</a></li>
</ul>

                    </section>
                </div>

                <div class="column small-6">
                    <section class="footer-about right-column">
                        <h1>About
</h1>
                        <ul class="no-bullet">
	<li><a href="../../about/contact/index.php">Contact information</a></li>
	<li><a href="../../about/index.php">About Us</a></li>
	<li><a href="../../about/why-nng/index.php">Why NN/g? </a></li>
	<li><a href="../../return-policy/index.php">Return Policy</a></li>
</ul>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="copyright column small-12">
            <p><a href="../../copyright-and-reprint-info/index.php">Copyright</a> &copy; 1998-2017 Nielsen Norman Group, All Rights Reserved.</p>
        </div>
    </div>
</footer>


    


        <script src="https://media.nngroup.com/static/vendor/fastclick/js/fastclick.min.js"></script>
        <script src="https://media.nngroup.com/static/vendor/foundation/js/foundation.min.js"></script>
        <script src="http://media.nngroup.com/static/js/app.js"></script>
        <script src="http://media.nngroup.com/static/js/scroll-to-fixed.js"></script><script>var externalLinks = document.querySelectorAll('a[href^="http"]');for (var i = externalLinks.length-1; i >= 0; i--) {externalLinks[i].addEventListener("click", function(event) { alert("External links are disabled for this test."); event.preventDefault(); }, false);}var mailLinks = document.querySelectorAll('a[href^="mailto"]');for (var i = mailLinks.length-1; i >= 0; i--) {mailLinks[i].addEventListener("click", function(event) { alert("Email links are disabled for this test."); event.preventDefault(); }, false);}var externalForms = document.querySelectorAll('form');for (var i = externalForms.length-1; i >= 0; i--) {externalForms[i].addEventListener("submit", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false); externalForms[i].addEventListener("click", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false);}</script>

        <script>
            
            $(function() {
                $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
                $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
            });
        </script>

        
<script src="https://media.nngroup.com/static/js/articles.js"></script>


        
        <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1072699960;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072699960/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
    </body>

<!-- Mirrored from www.nngroup.com/topic/design-patterns/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:11:57 GMT -->
</html>
