<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->





    
<!-- Mirrored from www.nngroup.com/topic/human-computer-interaction/?page=4 by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:07:18 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1026.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"beacon":"bam.nr-data.net","queueTime":6,"applicationTime":912,"agent":"","licenseKey":"1a30ef9931","errorBeacon":"bam.nr-data.net","applicationID":"2048992","transactionName":"NFxSMRBUX0EDUkIPDA0WdhAMVkVbDV8ZBxEXUFMJB0YfRAtUQRVZIktEDAFZVGYNQV8FLwpKREsFUEU="}</script>
        <title>Human Computer Interaction (HCI): Articles and Training Courses | NN/g</title><meta property="og:title" content="Human Computer Interaction (HCI): Articles and Training Courses | NN/g" />
  
        
  
        
	
        

        

        <meta name="robots" content="noarchive">
        <meta name="viewport" content="width=device-width">

        <link rel="search" type="application/opensearchdescription+xml" title="NN/g Search" href="../../opensearch.xml" />
        <link rel="shortcut icon" href="http://media.nngroup.com/static/img/favicon.ico" />

        <link rel="stylesheet" href="https://media.nngroup.com/static/vendor/font-awesome/css/font-awesome.min.css">

        
        <link rel="stylesheet" href="https://media.nngroup.com/static/css/7232a960f1df.css" type="text/css" />
        <link rel="stylesheet" href="http://fast.fonts.com/t/1.css?apiType=css&amp;projectid=c5846260-9def-46ec-9407-0f1349bd2d64" type="text/css">

        <script src="https://media.nngroup.com/static/vendor/modernizr/js/modernizr.min.js"></script>

        <script type="text/javascript" src="https://media.nngroup.com/static/js/3283be5def74.js"></script>
        

        
<link rel="canonical" href="index.php" />


        
        
        <!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=229034,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('http://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->


<!-- Google Analytics Tracking Code for http://nngroup.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23132681-1', 'nngroup.com');
  ga('require', 'linkid', 'https://www.nngroup.com/topic/human-computer-interaction/linkid.js');
  ga('require', 'displayfeatures');

// Optimizely Universal Analytics Integration
 window.optimizely = window.optimizely || [];
 window.optimizely.push("activateUniversalAnalytics");

  ga('send', 'pageview');

</script>
<script>
/**
* Function to track a click on an outbound link in Google Analytics.
*/
function trackOutboundLink(label, url) {
    if(window.ga && ga.loaded) {
        ga('send', 'event', 'outbound', 'click', label, {'hitCallback': function(){document.location = url;}
        });
    } else {
        document.location = url;
    }
}
/**
* Function to track a click on a link in the article announcement area.
*/
$('#articleAnnouncement a').on('click', function(){
  ga('send', 'event', 'link', 'click', 'articleAnnouncement');
});
</script>

<script type="text/javascript">
 setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2421.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



<!-- Hotjar Tracking Code for http://nngroup.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:121605,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

        <script src="https://media.nngroup.com/static/js/analytics/track_outbound_link.js"></script>
        
        
        
    </head>
    <body class="location-articles articles-topic">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
    <header id="page-header">
        <div id="cart" class="shopping-cart mobile-cart show-for-small-down"></div>
        <div class="row">
            <div id="masthead" class="masthead small-12 medium-7 column">

                <!-- The button to toggles menu on small screen -->

                <h1><a href="../../index.php"><img src="http://media.nngroup.com/static/img/logo-wordmark.png" width="74" height="30"> Nielsen <span class="norman">Norman</span> Group</a></h1>
                <p class="tagline show-for-medium-up">Evidence-Based User Experience Research, Training, and Consulting</p>

                <!-- The button to toggles form search on small screen -->


                <div style="clear: right;width:  100%;height: 1px;"></div><?php define('ROOT_PATH',$_SERVER["DOCUMENT_ROOT"] . '/apps/navigation_styles/navigations/3'); ?><?php include(ROOT_PATH)."/navigation.php"; ?>

                <!-- Drop down menu on small screen -->
                <div id="hideNav" class="show-for-small-down">
                  
                  <ul class="menu-content">
                    <li><a href="../../index.php">Home</a></li>
                    <li class="has-submenu">
                      <a>Training</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../training/index.php">Overview</a></li>
                        
<li id="nav-ux-conference"><a href="../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../online-seminars/index.php"><span>Online Seminars</span></a></li>

                      </ul>
                    </li>
                    <li class="has-submenu">
                      <a>Consulting</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
  <li><a href="../../consulting/index.php">Overview</a></li>
  
  <li><a href="../../consulting/benchmarking/index.php">Benchmarking</a></li>
  
  <li><a href="../../consulting/expert-design-review/index.php">Expert Design Review</a></li>
  
  <li><a href="../../consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
  
  <li><a href="../../consulting/usability-testing/index.php">Usability Testing </a></li>
  
  <li><a href="../../consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
  
  <li><a href="../../in-house-training/index.php">In-House Training</a></li>
</ul>

                    </li>

                    <li><a href="../../reports/index.php">Reports</a></li>
                    <li><a href="../../articles/index.php">Articles</a></li>
                    <li class="has-submenu"><a>About NN/G</a><i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../about/index.php">Overview</a></li>
                        <li><a href="../../people/index.php">People</a></li>
                        <li><a href="../../about/why-nng/index.php">Why NN/g?</a></li>
                        <li><a href="../../about/contact/index.php">Contact</a></li>
                        <li><a href="../../news/index.php">News</a></li>
                        <li><a href="../../about/history/index.php">History</a></li>
                        <li><a href="../../books/index.php">Books</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                </div>

                <hr class="header-divider show-for-small-only"/>


            </div>
       </div>

        <div id="hideNav">            <nav class="primary-nav small-12 column">
                <ul>
                    <li><a class="navitem" id="nav-home" href="../../index.php">Home</a></li>
                    <li><a class="navitem" id="nav-training" href="../../training/index.php">Training</a></li>
                    <li><a class="navitem" id="nav-consulting" href="../../consulting/index.php">Consulting</a></li>
                    <li><a class="navitem" id="nav-reports" href="../../reports/index.php">Reports</a></li>
                    <li><a class="navitem" id="nav-articles" href="../../articles/index.php">Articles</a></li>
                    <li><a class="navitem" id="nav-about" href="../../about/index.php">About NN/G</a></li>
                </ul>
            </nav>
            <nav class="secondary-nav small-12 medium-12 large-12 columns">
                
                
            </nav>
        </div>
    </header>
    <div id="mainContent" class="l-content">
    
  

  
  <div class="row">
    <div class="small-12 medium-8 columns l-section-intro">
      <header>
        <h1>Articles</h1>
      </header>
    </div>
  </div>
  

  <div class="row">
    
  
      <section class="small-12 medium-3 columns collapsable-content l-navrail articles-left-column">
        <h1 class="show-for-small-down collapse-link mobile-browse-header">Browse by Topic and Author</h1>
          <div class="row collapsable">

            
            <div class="small-6 medium-12 columns">
              <section id="article-popular-topics" style="display: none;">
                
                <h1>Topics</h1>
                <ul id="article-popular-topics">
                    
                    <li>
                        
                        <a href="../e-commerce/all/index.php">E-commerce</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../intranets/all/index.php">Intranets</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../user-testing/all/index.php">User Testing</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../web-usability/all/index.php">Web Usability</a>
                        
                    </li>
                    
                    <li><a href="#" id="article-see-all-topics"><strong>See all topics&hellip;</strong></a></li>
                </ul>
                
              </section>
              <section id="article-all-topics" style="display: none;">
                
                <h1>
                  <span class="show-for-medium-up">All Article Topics</span>
                  <span class="show-for-small-down">All Topics</span>
                  (<a href="#" id="article-hide-all-topics">hide</a>)
                </h1>
                <ul>
                  
                  <li>
                    
                    <a href="../accessibility/all/index.php">Accessibility</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../agile/all/index.php">Agile</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../analytics-and-metrics/all/index.php">Analytics &amp; Metrics</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../applications/all/index.php">Application Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../b2b-websites/all/index.php">B2B Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../behavior-patterns/all/index.php">Behavior Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../branding/all/index.php">Branding</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../content-strategy/all/index.php">Content Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../corporate-websites/all/index.php">Corporate Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../design-patterns/all/index.php">Design Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../ux-design-process/all/index.php">Design Process</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../e-commerce/all/index.php">E-commerce</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../email/all/index.php">Email</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../eyetracking/all/index.php">Eyetracking</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../heuristic-evaluation/all/index.php">Heuristic Evaluation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="all/index.php">Human Computer Interaction</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../ux-ideation/all/index.php">Ideation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../information-architecture/all/index.php">Information Architecture</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../interaction-design/all/index.php">Interaction Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../international-users/all/index.php">International Users</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../intranets/all/index.php">Intranets</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../ux-management/all/index.php">Management</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../navigation/all/index.php">Navigation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../non-profit-websites/all/index.php">Non-Profit Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../personas/all/index.php">Personas</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../persuasive-design/all/index.php">Persuasive Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../prototyping/all/index.php">Prototyping</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../psychology-and-ux/all/index.php">Psychology and UX</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../research-methods/all/index.php">Research Methods</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../search/all/index.php">Search</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../social-media/all/index.php">Social Media</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../strategy/all/index.php">Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../user-testing/all/index.php">User Testing</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../ux-teams/all/index.php">UX Teams</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../visual-design/all/index.php">Visual Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../web-usability/all/index.php">Web Usability</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../writing-web/all/index.php">Writing for the Web</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../young-users/all/index.php">Young Users</a>
                    
                  </li>
                  
                </ul>
                
              </section>
            </div>
            <script>
              /* Determine which topics list will be displayed: the full or the
               * partial.  We are inlining this close to the list itself to
               * prevent rendering delays on the client
               */
              var topic_list = $.cookie("article-topics");

              var popular_topics = $('#article-popular-topics');
              var all_topics = $('#article-all-topics');

              if(topic_list == 1) {
                  popular_topics.hide();
                  all_topics.show();
              } else {
                  popular_topics.show();
                  all_topics.hide();
              }
            </script>
            

            
            <div class="small-6 medium-12 columns">
              
              <section id="article-initial-authors">
                  <h1>Author</h1>
                  <ul>
                    
                    <li><a href="../../articles/author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                    
                    <li><a href="../../articles/author/don-norman/index.php">Don Norman</a></li>
                    
                    <li><a href="../../articles/author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                    
                    <li><a href="#" onClick="$('#article-initial-authors').hide(); $('#article-all-authors').show();"><strong>See all authors&hellip;</strong></a></li>
                  </ul>
              </section>
              <section id="article-all-authors" style="display: none;">
                <h1>
                  All Authors
                  (<a href="#" id="article-hide-all-authors">hide</a>)
                </h1>
                <ul>
                  
                  <li><a href="../../articles/author/raluca-budiu/index.php">Raluca Budiu</a></li>
                  
                  <li><a href="../../articles/author/patty-caya/index.php">Patty Caya</a></li>
                  
                  <li><a href="../../articles/author/susan-farrell/index.php">Susan Farrell</a></li>
                  
                  <li><a href="../../articles/author/therese-fessenden/index.php">Therese Fessenden</a></li>
                  
                  <li><a href="../../articles/author/kim-flaherty/index.php">Kim Flaherty</a></li>
                  
                  <li><a href="../../articles/author/sarah-gibbons/index.php">Sarah Gibbons</a></li>
                  
                  <li><a href="../../articles/author/aurora-harley/index.php">Aurora Harley</a></li>
                  
                  <li><a href="../../articles/author/kate-kaplan/index.php">Kate Kaplan</a></li>
                  
                  <li><a href="../../articles/author/page-laubheimer/index.php">Page Laubheimer</a></li>
                  
                  <li><a href="../../articles/author/angela-li/index.php">Angie Li</a></li>
                  
                  <li><a href="../../articles/author/hoa-loranger/index.php">Hoa Loranger</a></li>
                  
                  <li><a href="../../articles/author/kate-meyer/index.php">Kate Meyer</a></li>
                  
                  <li><a href="../../articles/author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                  
                  <li><a href="../../articles/author/don-norman/index.php">Don Norman</a></li>
                  
                  <li><a href="../../articles/author/kara-pernice/index.php">Kara Pernice</a></li>
                  
                  <li><a href="../../articles/author/christian-rohrer/index.php">Christian Rohrer</a></li>
                  
                  <li><a href="../../articles/author/amy-schade/index.php">Amy Schade</a></li>
                  
                  <li><a href="../../articles/author/katie-sherwin/index.php">Katie Sherwin</a></li>
                  
                  <li><a href="../../articles/author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                  
                  <li><a href="../../articles/author/kathryn-whitenton/index.php">Kathryn Whitenton</a></li>
                  
                </ul>
              </section>
              
            </div>
            
          </div>

          
          <section class="hide-for-small-down">
            
              
              <h1>Recent Articles</h1>
              
              <ul>
                  
                  <li><a href="../../articles/seamless-cross-channel/index.php">Seamlessness in the Omnichannel User Experience</a></li>
                  
                  <li><a href="../../articles/brand-intention-interpretation/index.php">Experience Design: Bridging Brand Intention and Brand Interpretation</a></li>
                  
                  <li><a href="../../articles/eyetracking-tasks-efficient-scanning/index.php">Scanning Patterns on the Web Are Optimized for the Current Task</a></li>
                  
                  <li><a href="../../articles/flat-design-best-practices/index.php">Flat-Design Best Practices</a></li>
                  
                  <li><a href="../../articles/ux-user-stories/index.php">Accounting for UX Work with User Stories in Agile Projects</a></li>
                  
                  <li><a href="../../articles/index.php"><strong>See all articles&hellip;</strong></a></li>
              </ul>
              
            
          </section>
          

          
          
          <section class="hide-for-small-down">
            <h1>Popular Articles</h1>
            
            <ul>
                
                <li><a href="../../articles/usability-101-introduction-to-usability/index.php">Usability 101: Introduction to Usability</a></li>
                
                <li><a href="../../articles/top-10-mistakes-web-design/index.php">Top 10 Mistakes in Web Design</a></li>
                
                <li><a href="../../articles/how-users-read-on-the-web/index.php">How Users Read on the Web</a></li>
                
                <li><a href="../../articles/f-shaped-pattern-reading-web-content/index.php">F-Shaped Pattern For Reading Web Content</a></li>
                
                <li><a href="../../articles/ten-usability-heuristics/index.php">10 Usability Heuristics for User Interface Design</a></li>
                
                <li><a href="../../articles/intranet-design-2016/index.php">10 Best Intranets of 2016</a></li>
                
                <li><a href="../../articles/which-ux-research-methods/index.php">When to Use Which User-Experience Research Methods</a></li>
                
                <li><a href="../../articles/response-times-3-important-limits/index.php">Response Times: The 3 Important Limits</a></li>
                
                <li><a href="../../articles/why-you-only-need-to-test-with-5-users/index.php">Why You Only Need to Test with 5 Users</a></li>
                
                <li><a href="../../articles/page-fold-manifesto/index.php">The Fold Manifesto: Why the Page Fold Still Matters</a></li>
                
            </ul>
          </section>
          
          

          
            <section class="hide-for-small-down">
              
            </section>
          

      </section>
      

  
    <div class="small-12 medium-7 columns content-body">
      
  <h2 class="topic">Topic: <strong>Human Computer Interaction</strong></h2>
  
      <ul class="tabs" data-tab>
        <li class="active"><a href="#recent">Most Recent</a></li>
        <li><a href="#popular">Most Popular</a></li>
      </ul>

      <ul class="tabs-content">
        <li id="recent" class="content active">
          
            
    <article class="alertbox-article-listitem">
        <h1>
            
                <a href="../../articles/the-need-for-speed/index.php">The Need for Speed</a>
            
        </h1>
        <h2>March 1, 1997</h2>
        <p>All usability studies show that fast response times are essential for Web usability: let&#39;s believe the data for once! Advice for speeding up sites despite the fact that bandwidth is going down, not up.</p>
    </article>

    <article class="alertbox-article-listitem">
        <h1>
            
                <a href="../../articles/tv-meets-the-web/index.php">TV Meets the Web</a>
            
        </h1>
        <h2>February 15, 1997</h2>
        <p>Comparing the nature of the Web as a medium when accessed through television sets and when accessed through computers, concluding that the level of user engagement is a main differentiator</p>
    </article>

    <article class="alertbox-article-listitem">
        <h1>
            
                <a href="../../articles/webtv-usability-review/index.php">WebTV Usability Review</a>
            
        </h1>
        <h2>February 1, 1997</h2>
        <p>Analysis of the usability of WebTV, including user interface guidelines for designing cross-platform Web pages that are considerate of WebTV users</p>
    </article>

    <article class="alertbox-article-listitem">
        <h1>
            
                <a href="../../articles/anti-mac-interface/index.php">The Anti-Mac Interface</a>
            
        </h1>
        <h2>August 1, 1996</h2>
        <p>We reverse all of the core design principles behind the Macintosh human interface guidelines to arrive at the characteristics of the Internet desktop.</p>
    </article>

    <article class="alertbox-article-listitem">
        <h1>
            
                <a href="../../articles/file-death/index.php">The Death of File Systems</a>
            
        </h1>
        <h2>March 1, 1996</h2>
        <p>The file system has been a trusted part of most computers for many years, and will likely continue as such in operating systems for many more. However, several emerging trends in user interfaces indicate that the basic file-system model is inadequate to fully satisfy the needs of new users, despite the flexibility of the underlying code and data structures.
Originally published as: Nielsen, J. (1996). The impending demise of file systems. IEEE Software 13, 2 (March).</p>
    </article>

    <article class="alertbox-article-listitem">
        <h1>
            
                <a href="../../articles/the-internet-desktop/index.php">The Internet Desktop</a>
            
        </h1>
        <h2>March 1, 1996</h2>
        <p></p>
    </article>

    <article class="alertbox-article-listitem">
        <h1>
            
                <a href="../../articles/guidelines-for-multimedia-on-the-web/index.php">Guidelines for Multimedia on the Web</a>
            
        </h1>
        <h2>December 1, 1995</h2>
        <p>Multimedia is gaining popularity on the Web with several technologies to support use of animation, video, and audio to supplement the traditional media of text and images. These new media provide more design options but also require design discipline. Unconstrained use of multimedia results in user interfaces that confuse users and make it harder for them to understand the information. Not every webpage needs to bombard the user with the equivalent of Times Square in impressions and movement.</p>
    </article>

    <article class="alertbox-article-listitem">
        <h1>
            
                <a href="../../articles/how-much-bandwidth-is-enough-a-tbps/index.php">How Much Bandwidth is Enough? A Tbps!</a>
            
        </h1>
        <h2>November 1, 1995</h2>
        <p>In the long term we will need about a million times more bandwidth than a T1, as shown by the following list of requirements for the perfect user interface.</p>
    </article>

    <article class="alertbox-article-listitem">
        <h1>
            
                <a href="../../articles/history-has-a-lesson-for-hotjava/index.php">History has a Lesson for HotJava</a>
            
        </h1>
        <h2>June 1, 1995</h2>
        <p>Many users of the World Wide Web are unaware of the rich history of the hypertext field, and those who do not understand history are often doomed to repeat it. Case in point, the release of the Java language and the HotJava browser will probably mimic the events that followed the introduction of Hypercard in 1987.</p>
    </article>

    <article class="alertbox-article-listitem">
        <h1>
            
                <a href="../../articles/coping-information-overload/index.php">Coping with Information Overload</a>
            
        </h1>
        <h2>February 1, 1995</h2>
        <p>Chapter 8 from Jakob Nielsen&#39;s book, Multimedia and Hypertext: The Internet and Beyond, explores a variety of information retrieval strategies for dealing with the ever-increasing volume of information on the internet. </p>
    </article>

    <article class="alertbox-article-listitem">
        <h1>
            
                <a href="../../articles/hypertext-history/index.php">The History of Hypertext</a>
            
        </h1>
        <h2>February 1, 1995</h2>
        <p>Chapter 3 from Jakob Nielsen&#39;s book, Multimedia and Hypertext, describes the major milestones for hypertext, the internet, and the world wide web, including Vannevar Bush&#39;s Memex and Doug Engelbart&#39;s landmark demo of the online system (NLS.)</p>
    </article>

    <article class="alertbox-article-listitem">
        <h1>
            
                <a href="../../articles/summary-of-usability-inspection-methods/index.php">Summary of Usability Inspection Methods</a>
            
        </h1>
        <h2>January 1, 1995</h2>
        <p>Usability inspection is the generic name for a set of methods that are all based on having evaluators inspect a user interface. Typically, usability inspection is aimed at finding usability problems in the design, though some methods also address issues like the severity of the usability problems and the overall usability of an entire system.</p>
    </article>

    <article class="alertbox-article-listitem">
        <h1>
            
                <a href="../../articles/ten-usability-heuristics/index.php">10 Usability Heuristics for User Interface Design</a>
            
        </h1>
        <h2>January 1, 1995</h2>
        <p>Jakob Nielsen&#39;s 10 general principles for interaction design. They are called &quot;heuristics&quot; because they are broad rules of thumb and not specific usability guidelines.</p>
    </article>

    <article class="alertbox-article-listitem">
        <h1>
            
                <a href="../../articles/trip-report-chi-themes/index.php">Annual Themes of the CHI Conferences</a>
            
        </h1>
        <h2>December 31, 1994</h2>
        <p>Jakob Nielsen&#39;s trip report overview of the Computer Human Interaction (CHI) Conferences from 1983 to 1994.</p>
    </article>

    <article class="alertbox-article-listitem">
        <h1>
            
                <a href="../../articles/trip-report-chi-94/index.php">CHI&#39;94 Trip Report</a>
            
        </h1>
        <h2>June 1, 1994</h2>
        <p>Jakob Nielsen&#39;s trip report for the 1994 Computer-Human Interaction conference (CHI &#39;94).
CHI&#39;94 (Boston, MA, April 24-28), IEEE Software 11, 4 (July), 110-112.</p>
    </article>

    <article class="alertbox-article-listitem">
        <h1>
            
                <a href="../../articles/usability-labs/index.php">Usability Laboratories: A 1994 Survey</a>
            
        </h1>
        <h2>February 1, 1994</h2>
        <p>A summary of statistics for the thirteen usability laboratories in 1994, an introduction to the main uses of usability laboratories in usability engineering, and survey of some of the issues related to practical use of user testing and CAUSE tools for computer-aided usability engineering.
Nielsen, J. (1994). Usability laboratories. Behaviour &amp; Information Technology 13, 1&amp;2, 3-8.</p>
    </article>

    <article class="alertbox-article-listitem">
        <h1>
            
                <a href="../../articles/goal-composition/index.php">Goal Composition: Extending Task Analysis to Predict Things People May Want to Do</a>
            
        </h1>
        <h2>January 1, 1994</h2>
        <p>This essay describes a technique for extending a task analysis based on the principle of goal composition. Basically, goal composition starts by considering each primary goal that the user may have when using the system. A list of possible additional features is then generated by combining each of these goals with a set of general meta-goals that extend the primary goals. </p>
    </article>

    <article class="alertbox-article-listitem">
        <h1>
            
                <a href="../../articles/trip-report-upa-93/index.php">UPA&#39;93: Usability Professionals Association Annual Meeting</a>
            
        </h1>
        <h2>August 1, 1993</h2>
        <p>Jakob Nielsen&#39;s trip report for the 1993 Usability Professionals Association (UPA) Annual Meeting. 
Usability Professionals Association Annual Meeting (Redmond, WA, July 21-23, 1993), SIGCHI Bulletin 26, 2 (April 1994).</p>
    </article>

    <article class="alertbox-article-listitem">
        <h1>
            
                <a href="../../articles/noncommand/index.php">Noncommand User Interfaces</a>
            
        </h1>
        <h2>April 2, 1993</h2>
        <p>Several new user interface technologies and interaction principles seem to define a new generation of user interfaces that will move off the flat screen and into the physical world to some extent. Many of these next-generation interfaces will not have the user control the computer through commands, but will have the computer adapt the dialogue to the user&#39;s needs based on its inferences from observing the user. This article defines twelve dimensions across which future user interfaces may differ from the canonical window systems of today: User focus, the computer&#39;s role, interface control, syntax, object visibility, interaction stream, bandwidth, tracking feedback, interface locus, user programming, and software packaging. 

Nielsen, J. (1993). Noncommand user interfaces. Communications of the ACM 36, 4 (April), 83-99.</p>
    </article>

    <article class="alertbox-article-listitem">
        <h1>
            
                <a href="../../articles/handwriting-retrieval/index.php">Information Retrieval of Imperfectly Recognized Handwriting</a>
            
        </h1>
        <h2>January 1, 1993</h2>
        <p>A user test of handwritten input on a pen machine achieved a 1.6% recognition error rate at the character level, corresponding to 8.8% errors on the word level. Input speed was 10 words per minute. In spite of the recognition errors, information retrieval of the handwritten notes was almost as good as retrieval of perfect text.</p>
    </article>


          
          <div class="article-pager">
            

    <a href="index9ba9.php?page=3">
        <span><span class="show-for-medium-up">Previous &laquo; </span>
        <span class="show-for-small-down">Prev &laquo; </span></span>
    

    
        
            <a href="index2679.php?page=1">&nbsp;1&nbsp;</a>
        
    
        
            <a href="index4658.php?page=2">&nbsp;2&nbsp;</a>
        
    
        
            <a href="index9ba9.php?page=3">&nbsp;3&nbsp;</a>
        
    
        
            <span class="currentpage">&nbsp;4&nbsp;</span>
        
    
        
            <a href="indexaf4d.php?page=5">&nbsp;5&nbsp;</a>
        
    

    <a href="indexaf4d.php?page=5"> &raquo; Next</a>


          </div>
        </li>
        <li id="popular" class="content">
          
            
    <article class="alertbox-article-listitem">
        <h1>
            
                <a href="../../articles/too-fast-ux/index.php">When the UI Is Too Fast</a>
            
        </h1>
        <h2>July 8, 2013</h2>
        <p>Users might overlook things that change too fast—and even when they do notice, changeable screen elements are harder to understand in a limited timeframe.</p>
    </article>

    <article class="alertbox-article-listitem">
        <h1>
            
                <a href="../../articles/form-design-white-space/index.php">Form Design Quick Fix: Group Form Elements Effectively Using White Space</a>
            
        </h1>
        <h2>November 3, 2013</h2>
        <p>Improve the layout of your online forms by placing form labels near the associated text field and by grouping similar fields.  </p>
    </article>

    <article class="alertbox-article-listitem">
        <h1>
            
                <a href="../../articles/interaction-cost-definition/index.php">Interaction Cost</a>
            
        </h1>
        <h2>August 31, 2013</h2>
        <p>The interaction cost is the sum of efforts — mental and physical — that the users must deploy in
interacting with a site in order to reach their goals.</p>
    </article>

    <article class="alertbox-article-listitem">
        <h1>
            
                <a href="../../articles/minimize-cognitive-load/index.php">Minimize Cognitive Load to Maximize Usability</a>
            
        </h1>
        <h2>December 22, 2013</h2>
        <p>The total cognitive load, or amount of mental processing power needed to use your site, affects how easily users find content and complete tasks.</p>
    </article>

    <article class="alertbox-article-listitem">
        <h1>
            
                <a href="../../articles/productivity-and-screen-size/index.php">Productivity and Screen Size</a>
            
        </h1>
        <h2>October 23, 2006</h2>
        <p>A study of the benefits of big monitors fails on two accounts: it didn&#39;t test realistic tasks, and it didn&#39;t test realistic use. Productivity is a key argument for workplace usability, but you must measure it carefully.</p>
    </article>

    <article class="alertbox-article-listitem">
        <h1>
            
                <a href="../../articles/security-and-human-factors/index.php">Security &amp; Human Factors</a>
            
        </h1>
        <h2>November 26, 2000</h2>
        <p>A big lie of computer security is that security improves as password complexity increases. In reality, users simply write down difficult passwords, leaving the system vulnerable. Security is better increased by designing for how people actually behave.</p>
    </article>

    <article class="alertbox-article-listitem">
        <h1>
            
                <a href="../../articles/usability-101-introduction-to-usability/index.php">Usability 101: Introduction to Usability</a>
            
        </h1>
        <h2>January 4, 2012</h2>
        <p>What is usability? How, when, and where to improve it? Why should you care? Overview answers basic questions + how to run fast user tests.</p>
    </article>

    <article class="alertbox-article-listitem">
        <h1>
            
                <a href="../../articles/goal-composition/index.php">Goal Composition: Extending Task Analysis to Predict Things People May Want to Do</a>
            
        </h1>
        <h2>January 1, 1994</h2>
        <p>This essay describes a technique for extending a task analysis based on the principle of goal composition. Basically, goal composition starts by considering each primary goal that the user may have when using the system. A list of possible additional features is then generated by combining each of these goals with a set of general meta-goals that extend the primary goals. </p>
    </article>


          
        </li>
      </ul>
  

    </div>
  

  
    <div class="small-12 medium-2 columns l-navrail">
    
    
    <div class="right-aside learn-more">
        <h2 class="show-for-small-down">Learn More</h2>
        

        
            <h3>UX Conference Training Courses</h3>
             <ul class="no-bullet">
    
        <li><a href="../../courses/ux-basic-training/index.php">UX Basic Training</a></li>
    
        <li><a href="../../courses/hci/index.php">User Interface Principles Every Designer Must Know</a></li>
    
        <li><a href="../../courses/interaction-design-3-day-course/index.php">Interaction Design: 3-Day Course</a></li>
    
</ul>
        

        

        
          <h3>Books</h3>
          <ul>
            
              <li><a href="../../books/design-of-future-things/index.php">The Design of Future Things</a></li>
            
              <li><a href="../../books/advances-in-human-computer-interaction/index.php">Advances in Human/Computer Interaction, Volume 5</a></li>
            
              <li><a href="../../books/usability-inspection-methods/index.php">Usability Inspection Methods</a></li>
            
              <li><a href="../../books/coordinating-user-interfaces-for-consistency/index.php">Coordinating User Interfaces for Consistency</a></li>
            
          </ul>
        
    </div>
    

    </div>
  

  </div>
  <div class="row">
    
      <div class="column small-12 show-for-small-down l-navrail announcements">
        
          
        
      </div>
    
    
      <div class="small-12 columns show-for-small-down">
        <a class="button expand subscribe-bt" href="../../articles/subscribe/index.php">Subscribe to receive our weekly articles</a>

      </div>
    
  </div>

    </div>
    




<footer id="page-footer">
    <div class="row hide-for-print">
        <div class="column small-12 medium-6">
            <section class="footer-popular">
                
                <h1>Popular Topics</h1>
                <div style="float: left;  width: 15em; display: block;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../applications/index.php">Application Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../e-commerce/index.php">E-Commerce Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../eyetracking/index.php">Eyetracking </a></li>
	<li style="padding-bottom: .5em;"><a href="index.php">Human Computer Interaction </a></li>
	<li style="padding-bottom: .5em;"><a href="../information-architecture/index.php">Information Architecture </a></li>
	<li style="padding-bottom: .5em;"><a href="../intranets/index.php">Intranet Design </a></li>
	<li><a href="../interaction-design/index.php">Interaction Design </a></li>
</ul>
</div>

<div style="float: left;  width: 15em; display: block; list-style: none;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../mobile-and-tablet-design/index.php">Mobile and Tablet Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../credibility-persuasion/index.php">Persuasive Design</a></li>
	<li style="padding-bottom: .5em;"><a href="../research-methods/index.php">Research Methods</a></li>
	<li style="padding-bottom: .5em;"><a href="../user-testing/index.php">User Testing </a></li>
	<li style="padding-bottom: .5em;"><a href="../web-usability/index.php">Web Usability </a></li>
	<li style="padding-bottom: .5em;"><a href="../writing-web/index.php">Web Writing &amp; Content Strategy </a></li>
</ul>
</div>

<p>&nbsp;</p>

                
            </section>
        </div>

        <div class="column small-12 medium-6">
            <div class="row">
                <div class="column small-6">
                    <section class="footer-about">
                        <h1>Follow Us</h1>
                        <ul>
	<li><a href="../../articles/subscribe/index.php">Subscribe to our weekly UX newsletter</a>,&nbsp;Jakob Nielsen&#39;s Alertbox</li>
	<li>Visit Don Norman&#39;s website, <a href="http://www.jnd.org/"> www.jnd.org </a></li>
	<li>Visit Bruce Tognazzini&#39;s website, <a href="http://www.asktog.com/"> www.asktog.com </a></li>
	<li style="padding-top: 6px;"><a class="twitter-follow-button" data-show-count="false" data-size="large" href="https://twitter.com/nngroup"><img alt="Follow @nngroup on Twitter" height="20" src="https://media.nngroup.com/media/editor/2016/12/30/twitter-follow-button.png" style="width: 117px; height: 20px;" width="117" /></a></li>
	<li style="padding-top: 6px;"><img alt="" height="14" src="https://media.nngroup.com/media/editor/2016/07/26/feed-icon-14x14.png" style="vertical-align:text-top;width:14px;margin-right:.25em;" width="14" /><a href="../../feed/rss/index.php">Subscribe to our RSS feed</a></li>
</ul>

                    </section>
                </div>

                <div class="column small-6">
                    <section class="footer-about right-column">
                        <h1>About
</h1>
                        <ul class="no-bullet">
	<li><a href="../../about/contact/index.php">Contact information</a></li>
	<li><a href="../../about/index.php">About Us</a></li>
	<li><a href="../../about/why-nng/index.php">Why NN/g? </a></li>
	<li><a href="../../return-policy/index.php">Return Policy</a></li>
</ul>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="copyright column small-12">
            <p><a href="../../copyright-and-reprint-info/index.php">Copyright</a> &copy; 1998-2017 Nielsen Norman Group, All Rights Reserved.</p>
        </div>
    </div>
</footer>


    


        <script src="https://media.nngroup.com/static/vendor/fastclick/js/fastclick.min.js"></script>
        <script src="https://media.nngroup.com/static/vendor/foundation/js/foundation.min.js"></script>
        <script src="http://media.nngroup.com/static/js/app.js"></script>
        <script src="http://media.nngroup.com/static/js/scroll-to-fixed.js"></script><script>var externalLinks = document.querySelectorAll('a[href^="http"]');for (var i = externalLinks.length-1; i >= 0; i--) {externalLinks[i].addEventListener("click", function(event) { alert("External links are disabled for this test."); event.preventDefault(); }, false);}var mailLinks = document.querySelectorAll('a[href^="mailto"]');for (var i = mailLinks.length-1; i >= 0; i--) {mailLinks[i].addEventListener("click", function(event) { alert("Email links are disabled for this test."); event.preventDefault(); }, false);}var externalForms = document.querySelectorAll('form');for (var i = externalForms.length-1; i >= 0; i--) {externalForms[i].addEventListener("submit", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false); externalForms[i].addEventListener("click", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false);}</script>

        <script>
            
            $(function() {
                $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
                $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
            });
        </script>

        
<script src="https://media.nngroup.com/static/js/articles.js"></script>


        
        <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1072699960;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072699960/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
    </body>

<!-- Mirrored from www.nngroup.com/topic/human-computer-interaction/?page=4 by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:07:18 GMT -->
</html>
