<!doctype html>
<html id="doc" class="no-js">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="/apps/navigation_styles/app/app.js"></script>
    <script>
        window.onclick = function() { addCoordinates() };
        window.addEventListener("touchstart", function() { addCoordinates() }, false);
    </script>
    <link rel="stylesheet" href="/apps/navigation_styles/navigations/3/assets/lightslider.css"/>
    <link href="/apps/navigation_styles/navigations/3/assets/style.css" rel="stylesheet"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="/apps/navigation_styles/navigations/3/assets/lightslider.js"></script>
    <script src="/apps/navigation_styles/navigations/3/assets/scripts.js"></script>
</head>
<body>
<div id="navPosition">
    <div class="item">
        <ul id="content-slider" class="content-slider">
            <li>
                <a href="/apps/navigation_styles/navigations/3/index.php"><h3>Home</h3></a>
            </li>
            <li>
                <a href="/apps/navigation_styles/navigations/3/reports/index.php"><h3>Reports</h3></a>
            </li>
            <li>
                <a href="/apps/navigation_styles/navigations/3/articles/index.php"><h3>Articles</h3></a>
            </li>
            <li>
                <a href="/apps/navigation_styles/navigations/3/training/index.php"><h3>Training</h3></a>
            </li>
            <li>
                <a href="/apps/navigation_styles/navigations/3/consulting/index.php"><h3>Consulting</h3></a>
            </li>
            <li>
                <a href="/apps/navigation_styles/navigations/3/about/index.php"><h3>About NN/G</h3></a>
            </li>
        </ul>
    </div>
</div>
</body>
</html>