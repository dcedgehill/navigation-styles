<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->





    
<!-- Mirrored from www.nngroup.com/courses/application-ux/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 14:58:57 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1026.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"licenseKey":"1a30ef9931","queueTime":4,"beacon":"bam.nr-data.net","errorBeacon":"bam.nr-data.net","applicationID":"2048992","transactionName":"NFxSMRBUX0EDUkIPDA0WdhAMVkVbDV8ZBQwWS0MAERtHWwdGRVwgDExCFgdxVEYDWFpIBAZN","applicationTime":402,"agent":""}</script>
        <title>Application Design | Nielsen Norman Group UX Training Course</title><meta property="og:title" content="Application Design | Nielsen Norman Group UX Training Course" />
  
        
        <meta name="description" content="Principles of web and desktop application design, with an analysis of GUI screen components, workflows, and varying user types.">
        <meta property="og:description" content="Principles of web and desktop application design, with an analysis of GUI screen components, workflows, and varying user types." />
        
  
        
	
        

        

        <meta name="robots" content="noarchive">
        <meta name="viewport" content="width=device-width">

        <link rel="search" type="application/opensearchdescription+xml" title="NN/g Search" href="../../opensearch.xml" />
        <link rel="shortcut icon" href="http://media.nngroup.com/static/img/favicon.ico" />

        <link rel="stylesheet" href="https://media.nngroup.com/static/vendor/font-awesome/css/font-awesome.min.css">

        
        <link rel="stylesheet" href="https://media.nngroup.com/static/css/7232a960f1df.css" type="text/css" />
        <link rel="stylesheet" href="http://fast.fonts.com/t/1.css?apiType=css&amp;projectid=c5846260-9def-46ec-9407-0f1349bd2d64" type="text/css">

        <script src="https://media.nngroup.com/static/vendor/modernizr/js/modernizr.min.js"></script>

        <script type="text/javascript" src="https://media.nngroup.com/static/js/3283be5def74.js"></script>
        

        

        
        
        <!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=229034,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('http://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->


<!-- Google Analytics Tracking Code for http://nngroup.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23132681-1', 'nngroup.com');
  ga('require', 'linkid', 'https://www.nngroup.com/courses/application-ux/linkid.js');
  ga('require', 'displayfeatures');

// Optimizely Universal Analytics Integration
 window.optimizely = window.optimizely || [];
 window.optimizely.push("activateUniversalAnalytics");

  ga('send', 'pageview');

</script>
<script>
/**
* Function to track a click on an outbound link in Google Analytics.
*/
function trackOutboundLink(label, url) {
    if(window.ga && ga.loaded) {
        ga('send', 'event', 'outbound', 'click', label, {'hitCallback': function(){document.location = url;}
        });
    } else {
        document.location = url;
    }
}
/**
* Function to track a click on a link in the article announcement area.
*/
$('#articleAnnouncement a').on('click', function(){
  ga('send', 'event', 'link', 'click', 'articleAnnouncement');
});
</script>

<script type="text/javascript">
 setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2421.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



<!-- Hotjar Tracking Code for http://nngroup.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:121605,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

        <script src="https://media.nngroup.com/static/js/analytics/track_outbound_link.js"></script>
        
        
        
    </head>
    <body class="location-training location-course-detail course-detail">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
    <header id="page-header">
        <div id="cart" class="shopping-cart mobile-cart show-for-small-down"></div>
        <div class="row">
            <div id="masthead" class="masthead small-12 medium-7 column">

                <!-- The button to toggles menu on small screen -->

                <h1><a href="../../index.php"><img src="http://media.nngroup.com/static/img/logo-wordmark.png" width="74" height="30"> Nielsen <span class="norman">Norman</span> Group</a></h1>
                <p class="tagline show-for-medium-up">Evidence-Based User Experience Research, Training, and Consulting</p>

                <!-- The button to toggles form search on small screen -->


                <div style="clear: right;width:  100%;height: 1px;"></div><?php define('ROOT_PATH',$_SERVER["DOCUMENT_ROOT"] . '/apps/navigation_styles/navigations/3'); ?><?php include(ROOT_PATH)."/navigation.php"; ?>

                <!-- Drop down menu on small screen -->
                <div id="hideNav" class="show-for-small-down">
                  
                  <ul class="menu-content">
                    <li><a href="../../index.php">Home</a></li>
                    <li class="has-submenu">
                      <a>Training</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../training/index.php">Overview</a></li>
                        
<li id="nav-ux-conference"><a href="../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../online-seminars/index.php"><span>Online Seminars</span></a></li>

                      </ul>
                    </li>
                    <li class="has-submenu">
                      <a>Consulting</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
  <li><a href="../../consulting/index.php">Overview</a></li>
  
  <li><a href="../../consulting/benchmarking/index.php">Benchmarking</a></li>
  
  <li><a href="../../consulting/expert-design-review/index.php">Expert Design Review</a></li>
  
  <li><a href="../../consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
  
  <li><a href="../../consulting/usability-testing/index.php">Usability Testing </a></li>
  
  <li><a href="../../consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
  
  <li><a href="../../in-house-training/index.php">In-House Training</a></li>
</ul>

                    </li>

                    <li><a href="../../reports/index.php">Reports</a></li>
                    <li><a href="../../articles/index.php">Articles</a></li>
                    <li class="has-submenu"><a>About NN/G</a><i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../about/index.php">Overview</a></li>
                        <li><a href="../../people/index.php">People</a></li>
                        <li><a href="../../about/why-nng/index.php">Why NN/g?</a></li>
                        <li><a href="../../about/contact/index.php">Contact</a></li>
                        <li><a href="../../news/index.php">News</a></li>
                        <li><a href="../../about/history/index.php">History</a></li>
                        <li><a href="../../books/index.php">Books</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                </div>

                <hr class="header-divider show-for-small-only"/>


            </div>
       </div>

        <div id="hideNav">            <nav class="primary-nav small-12 column">
                <ul>
                    <li><a class="navitem" id="nav-home" href="../../index.php">Home</a></li>
                    <li><a class="navitem" id="nav-training" href="../../training/index.php">Training</a></li>
                    <li><a class="navitem" id="nav-consulting" href="../../consulting/index.php">Consulting</a></li>
                    <li><a class="navitem" id="nav-reports" href="../../reports/index.php">Reports</a></li>
                    <li><a class="navitem" id="nav-articles" href="../../articles/index.php">Articles</a></li>
                    <li><a class="navitem" id="nav-about" href="../../about/index.php">About NN/G</a></li>
                </ul>
            </nav>
            <nav class="secondary-nav small-12 medium-12 large-12 columns">
                
  <ul class="inline-list">
  
<li id="nav-ux-conference"><a href="../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../online-seminars/index.php"><span>Online Seminars</span></a></li>

</ul>


            </nav>
        </div>
    </header>
    <div id="mainContent" class="l-content">
    

<article class="course-detail">
    <div class="row">
        
        <p class="small-12 column">
            
            Full day training course
            
        </p>
    </div>
    <div class="row">
        <div class="l-section-intro small-12 medium-8 end column">
            <header>
                <h1>Application Design for Web and Desktop</h1>
                <h2></h2>
            </header>
             <div class="usercontent">
                
            </div>
           
        </div>
    </div>

    <div class="row">
        <div class="l-course-content small-12 medium-8 column">
            <div class="l-usercontent mainCourseContent">
                <p>Designing applications for desktop and laptop computers poses special challenges: facilitating intricate tasks and workflows, enabling users to comprehend and manage complex data, and accommodating a rich variety of user roles, needs, and processes. Effective design and implementation of applications can have profound, positive implications on productivity, efficiency, accuracy, and satisfaction in a huge range of environments: creative studios, healthcare, home entertainment and productivity, manufacturing, call centers, supply-chain management, and many more.</p>

<p>In this seminar, we explore interaction-design patterns found in a range of desktop applications, from web-based configurators, to creative and productivity software, all the way to complex native desktop apps built for expert professionals with specialized domain experience. &nbsp;We review design considerations for cloud-based webapps and Software as a Service (SaaS), as well as native apps built for Windows and macOS. In addition, we demonstrate key workflow concepts for different task types, and we present guidelines for specific application types. We&rsquo;ll cover how to understand users&rsquo; needs, goals, and tasks, and how to enable users to not only use your app immediately, but also learn advanced functionality over time.</p>

<blockquote>&quot;This course ties together all of the principles learned throughout the rest of Usability Week and applies them directly to application design. What I learned today will be a fantastic resource for my ongoing application work!&quot;</blockquote>

<p>Meredith Englund<br />
Intronis</p>

<p><strong>Please note:&nbsp;</strong>This course doesn&rsquo;t cover the screen design or implementation of mobile-specific or tablet-specific apps. If you&rsquo;re designing native apps for mobile devices such as the iPhone, Android, or iPad, consider our&nbsp;<a href="../usability-mobile-websites-apps/index.php">Mobile User Experience</a> course. If you&rsquo;re designing applications to be accessed from both desktop and mobile devices, then consider taking both this course (on the general application user experience independent of platforms) and the course on&nbsp;<a href="../scaling-responsive-design/index.php">Scaling User Interfaces</a>&nbsp;(on how to adapt the design across screen sizes using techniques such as responsive design and other multi-platform approaches).</p>

            </div>
            
                <div class="l-usercontent mainCourseContent collapsable-content">

                    <h1 class="collapse-link show-for-small-down">Topics Covered</h1>
                    <h2 class="show-for-medium-up">Topics Covered</h2>

                    <div class="collapsable">
                        <ul>
	<li>Strategy and design considerations for apps
	<ul>
		<li>Ease of use&nbsp;</li>
		<li>Learnability and learning curves</li>
		<li>Usefulness vs. usability and how they impact user attitudes</li>
		<li>Web-based apps vs. native desktop apps</li>
	</ul>
	</li>
	<li>User needs and tasks
	<ul>
		<li>Research methods for understanding user behaviors and goals
		<ul>
			<li>Ethnographic studies</li>
			<li>Contextual inquiry and task analysis</li>
		</ul>
		</li>
		<li>Expert vs. novice users</li>
		<li>App users vs. website users</li>
	</ul>
	</li>
	<li>App structure: how to present it to users
	<ul>
		<li>Task presentation</li>
		<li>Modules&nbsp;</li>
		<li>Control panels</li>
		<li>Menus &amp; direct manipulation</li>
	</ul>
	</li>
	<li>Workflows
	<ul>
		<li>Wizards</li>
		<li>Guiding users through complex tasks or complex information</li>
		<li>Modes and views
		<ul>
			<li>Different modes for different tasks</li>
			<li>Reusing the same UI elements in different modes</li>
		</ul>
		</li>
		<li>Screen splitting and dealing with multiple sources of information</li>
		<li>Repetitive tasks</li>
		<li>Minimizing user effort
		<ul>
			<li>Autosuggestion &amp; autocomplete</li>
			<li>Predicting users&rsquo; next steps</li>
		</ul>
		</li>
	</ul>
	</li>
	<li>Presenting and interacting with complex data
	<ul>
		<li>Information-visualization techniques
		<ul>
			<li>Focus + context</li>
			<li>Tree maps&nbsp;</li>
		</ul>
		</li>
		<li>Managing large datasets
		<ul>
			<li>Filtering data and accessing data subsets</li>
		</ul>
		</li>
		<li>Dashboards&nbsp;</li>
	</ul>
	</li>
	<li>Input, data entry, and selection
	<ul>
		<li>Forms vs. other methods of data input</li>
	</ul>
	</li>
	<li>Customization, personalization, and settings
	<ul>
		<li>Role-based customization or personalization</li>
		<li>Administration and privileges</li>
	</ul>
	</li>
	<li>Output and exporting data</li>
	<li>Errors and feedback</li>
	<li>Help
	<ul>
		<li>Inline and contextual help and tips</li>
		<li>Documentation and tutorials&nbsp;</li>
		<li>Catering to both novice and expert users</li>
		<li>Progressive disclosure</li>
	</ul>
	</li>
</ul>

                    </div>

                </div>
            

            
            <div class="l-usercontent mainCourseContent collapsable-content">

                <h1 class="collapse-link show-for-small-down">Format</h1>
                <h2 class="show-for-medium-up">Format</h2>

                <div class="collapsable">
                    <p>The basis of the course is a lecture format with a few group exercises to reinforce the learned principles and guidelines.</p>

                </div>

            </div>
            

            

            

            

            

            

            
              



<div class="row certification-info">
  <section class="l-subsection collapsable-content small-12 medium-8 columns">
    <h1 class="collapse-link show-for-small-down">UX Certification Credit</h1>
    <h2 class="hide-for-small-down">UX Certification Credit</h2>
    <div class="collapsable">
      <p>
      Attending this course and passing the exam earns <strong>1 UX Certification credit</strong>, which also counts towards the optional <a href="../../ux-certification/interaction-design-specialty/index.php">Interaction Design Specialty</a>.
      </p>

      

      <p>
      Learn more about NN/g's <a href="../../ux-certification/index.php">UX Certification</a> Program.
      </p>
    </div>
  </section>
  <div class="hide-for-small-down medium-4 columns badge">
    <img alt="UX Certification Badge from Nielsen Norman Group" src="https://media.nngroup.com/nng-uxc-badge.png">
  </div>
</div>

            

            
<div class="l-usercontent mainCourseContent collapsable-content participant-comments">
  <h1 class="collapse-link show-for-small-down">Participant Comments</h1>
  <h2 class="show-for-medium-up">Participant Comments</h2>
  <div class="collapse-content collapsable">
    <blockquote>&quot;The instructor covered a lot of topics, the level of information is huge (coffee is available to keep you focused). It is great being able to approach such experts, discuss, share ideas and ask for opinions!&quot;</blockquote>

<p>Klervi Coccolini, LogicNew</p>

<blockquote>&quot;Great overview of application design best practices. I wanted to keep learning!&quot;</blockquote>

<p>Aaron Gillihan, Medimmune</p>

<blockquote>&quot;Excellent instructor. I really enjoyed the pace &amp; delivery.&quot;</blockquote>

<p>Deniz Koteen, Medimmune</p>

    <ul id="commentAccordian" class="accordion" data-accordion>
      <li class="accordion-navigation">
        <a href="#cc" class="closed" id="commentCollapseLink">More Participant Comments</a>
        <div id="cc" class="content more-comments">
          <blockquote>&quot;Great speaker with the right tempo. Open to new ideas and input. Lot of interation &amp; interesting exercises.&quot;</blockquote>

<p>Harry van Mierloo, Fox-IT</p>

<blockquote>&quot;Great course conducted by an engaging instructor. Excellent opportunity to learn and re-imagine existing knowledge.&quot;</blockquote>

<p>Paul Medland, Green 4 Solutions</p>

<blockquote>&quot;Excellent course that&#39;s perfect for those of us who may not work on the sexiest of applications, but we want them to work!&quot;</blockquote>

<p>Lynne Venart, The Art Monkey</p>

<blockquote>&quot;Excellent! Being in government, I was especially interested in the speaker&#39;s previous work with TurboTax forms and data collection tricky. Thanks!&quot;</blockquote>

<p>Hayley Moore</p>

        </div>
      </li>
    </ul>
  </div>
</div>
<script>
  $('#commentAccordian').on('toggled', function (event, accordion) {
    $link = $('#commentCollapseLink');
    $link.toggleClass('closed');
    if($link.hasClass('closed')) {
      $link.text('More Participant Comments');
    } else {
      $link.text('Hide Participant Comments');
    }
  });
</script>



            <div class="l-usercontent">
                <!-- Ethnio Activation Code -->
            </div>
            <div class="usercontent">
                
            </div>
            
            <section class="course-instructors l-subsection collapsable-content">
                <h1 class="show-for-medium-up">Instructors</h1>
                <h1 class="show-for-small-down collapse-link">Instructors</h1>
                <div class="collapsable">
                    
                    <article class="course-instructor ">
                        <h2><a href="../../people/page-laubheimer/index.php">Page Laubheimer</a></h2>
                        <div class="row">
                            <div class="small-12 medium-3 column">
                                <a href="../../people/page-laubheimer/index.php"><img src="https://media.nngroup.com/media/people/photos/IMG_2366-copy-400x400.jpg.200x200_q95_autocrop_crop_upscale.jpg"></a>
                            </div>
                            <div class="small-12 medium-9 column">
                                <div class="usercontent"><p>Page Laubheimer is a User Experience Specialist with Nielsen Norman Group. He helps organizations focus on delivering outstanding user experience in order to achieve their strategic goals. He combines his expertise in website usability with experience managing a team of designers and developers to successfully implement UX best practices across a range of platforms. Read more about <a href="../../people/page-laubheimer/index.php">Page</a>.</p>
</div>
                            </div>
                        </div>
                    </article>
                    
                    <article class="course-instructor ">
                        <h2><a href="../../people/raluca-budiu/index.php">Raluca Budiu</a></h2>
                        <div class="row">
                            <div class="small-12 medium-3 column">
                                <a href="../../people/raluca-budiu/index.php"><img src="https://media.nngroup.com/media/people/photos/Raluca_Budiu-headshot.jpg.200x200_q95_autocrop_crop_upscale.jpg"></a>
                            </div>
                            <div class="small-12 medium-9 column">
                                <div class="usercontent"><p>Raluca Budiu is Director of Research at Nielsen Norman Group, where she consults for clients from a variety of industries and presents tutorials on mobile usability, designing interfaces for multiple devices, quantitative usability methods, cognitive psychology for designers, and principles of human-computer interaction. She also serves as editor for the articles published on NNgroup.com. Raluca coauthored the NN/g reports on tablet usability, mobile usability, iPad usability, and the usability of children&#39;s websites, as well as the book Mobile Usability. She served as a judge for GSMA Global Mobile Awards 2012-2016 and Tabby Awards 2013.<span style="line-height: 1.6;">&nbsp;She holds a Ph.D. from Carnegie Mellon University.&nbsp;</span><a href="../../people/raluca-budiu/index.php" style="line-height: 1.6;">Read more about Raluca</a><span style="line-height: 1.6;">.</span></p>
</div>
                            </div>
                        </div>
                    </article>
                    
                </div>
            </section>
        </div>

        <div class="l-event-sidebar small-12 medium-4 l-subsection column sidebar">
          <div class="collapsable-content">
            <h1 class="show-for-small-down collapse-link">Course Dates</h1>
            <div class="l-subsection course-scheduled collapsable">
              <div  style="display: table">
                <h2 class="show-for-medium-up">Course Dates:</h2>
                <ul class="no-bullet ">
                  
                  
                    
                    <li><a href="../../training/san-francisco/index.php">San Francisco</a> May 04, 2017</li>
                    
                    
                    
                    <li><a href="../../training/washington-dc/index.php">Washington DC</a> May 17, 2017</li>
                    
                    
                  <li><a href="../../in-house-training/index.php">In-House Training Options</a></li>
                </ul>
              </div>
            </div>
          </div>

          <div class="show-for-small-down">
            



<div class="collapsable-content">
  <section class="l-subsection related-images">
    <h2 class="hide-for-small-down">Scenes From Past Trainings</h2>
    <h1 class="show-for-small-down collapse-link">Scenes From Past Trainings</h1>
    <div class="collapsable">
    <p>At the UX Conference, you get a full day of in-depth training with expert instructors. Learn more about <a href="../../training/why-attend/index.php">why you should attend</a>.</p>

    
      
        
          
            <img srcset="https://media.nngroup.com/media/files/images/IMG_1090_copy_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale@2x.jpg 2x" src="https://media.nngroup.com/media/files/images/IMG_1090_copy_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale.jpg" alt="">
          
        
      
    
      
        
          
            <img srcset="https://media.nngroup.com/media/files/images/IMG_8444_copy_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale@2x.jpg 2x" src="https://media.nngroup.com/media/files/images/IMG_8444_copy_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale.jpg" alt="">
          
        
      
    
      
        
          
            <img srcset="https://media.nngroup.com/media/files/images/IMG_8530_copy_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale@2x.jpg 2x" src="https://media.nngroup.com/media/files/images/IMG_8530_copy_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale.jpg" alt="">
          
        
      
    
      
        
          
            <img srcset="https://media.nngroup.com/media/files/images/IMG_4730_copy_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale@2x.jpg 2x" src="https://media.nngroup.com/media/files/images/IMG_4730_copy_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale.jpg" alt="">
          
        
      
    
    </div>
  </section>
</div>


          </div>

            <hr class="show-for-small-down"/>
            <div class="collapsable-content">
                <div class="l-subsection course-categories">
                    <h2 class="show-for-medium-up">Browse other courses by category:</h2>
                    <h1 class="hide-for-medium-up collapse-link">Browse Other Courses by Category</h1>
                    <div class="collapsable">
                        <ul class="no-bullet">
                            
                            <li><a href="../topic/agile/index.php">Agile</a></li>
                            
                            <li><a href="../topic/analytics-and-metrics/index.php">Analytics &amp; Metrics</a></li>
                            
                            <li><a href="../topic/applications/index.php">Application Design</a></li>
                            
                            <li><a href="../topic/content-strategy/index.php">Content Strategy</a></li>
                            
                            <li><a href="../topic/design-patterns/index.php">Design Patterns</a></li>
                            
                            <li><a href="../topic/ux-design-process/index.php">Design Process</a></li>
                            
                            <li><a href="../topic/e-commerce/index.php">E-commerce</a></li>
                            
                            <li><a href="../topic/human-computer-interaction/index.php">Human Computer Interaction</a></li>
                            
                            <li><a href="../topic/ux-ideation/index.php">Ideation</a></li>
                            
                            <li><a href="../topic/information-architecture/index.php">Information Architecture</a></li>
                            
                            <li><a href="../topic/interaction-design/index.php">Interaction Design</a></li>
                            
                            <li><a href="../topic/ux-management/index.php">Management</a></li>
                            
                            <li><a href="../topic/mobile-and-tablet-design/index.php">Mobile &amp; Tablet</a></li>
                            
                            <li><a href="../topic/navigation/index.php">Navigation</a></li>
                            
                            <li><a href="../topic/persuasive-design/index.php">Persuasive Design</a></li>
                            
                            <li><a href="../topic/prototyping/index.php">Prototyping</a></li>
                            
                            <li><a href="../topic/psychology-and-ux/index.php">Psychology and UX</a></li>
                            
                            <li><a href="../topic/research-methods/index.php">Research Methods</a></li>
                            
                            <li><a href="../topic/social-media/index.php">Social Media</a></li>
                            
                            <li><a href="../topic/strategy/index.php">Strategy</a></li>
                            
                            <li><a href="../topic/user-testing/index.php">User Testing</a></li>
                            
                            <li><a href="../topic/ux-teams/index.php">UX Teams</a></li>
                            
                            <li><a href="../topic/visual-design/index.php">Visual Design</a></li>
                            
                            <li><a href="../topic/web-usability/index.php">Web Usability</a></li>
                            
                            <li><a href="../topic/writing-web/index.php">Writing for the Web</a></li>
                            
                        </ul>
                        <h2>Recommended Courses for:</h2>
                        <ul class="no-bullet">
                            
                            <li><a href="../topic/managers/index.php">For Managers</a></li>
                            
                            <li><a href="../topic/new-user-experience/index.php">New to User Experience</a></li>
                            
                            <li><a href="../topic/refresher-ux-pros/index.php">Refresher for UX Pros</a></li>
                            
                            <li><a href="../topic/transitioning-another-discipline/index.php">Transitioning From Another Discipline</a></li>
                            
                        </ul>
                    </div>
                </div>
                <div class="l-navrail">
                    



                </div>
                <div class=" hide-for-small-down l-subsection">
                    
                    

                    
                </div>

                <div class="hide-for-small-down">
                    



<div class="collapsable-content">
  <section class="l-subsection related-images">
    <h2 class="hide-for-small-down">Scenes From Past Trainings</h2>
    <h1 class="show-for-small-down collapse-link">Scenes From Past Trainings</h1>
    <div class="collapsable">
    <p>At the UX Conference, you get a full day of in-depth training with expert instructors. Learn more about <a href="../../training/why-attend/index.php">why you should attend</a>.</p>

    
      
        
          
            <img srcset="https://media.nngroup.com/media/files/images/IMG_1090_copy_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale@2x.jpg 2x" src="https://media.nngroup.com/media/files/images/IMG_1090_copy_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale.jpg" alt="">
          
        
      
    
      
        
          
            <img srcset="https://media.nngroup.com/media/files/images/IMG_8444_copy_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale@2x.jpg 2x" src="https://media.nngroup.com/media/files/images/IMG_8444_copy_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale.jpg" alt="">
          
        
      
    
      
        
          
            <img srcset="https://media.nngroup.com/media/files/images/IMG_8530_copy_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale@2x.jpg 2x" src="https://media.nngroup.com/media/files/images/IMG_8530_copy_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale.jpg" alt="">
          
        
      
    
      
        
          
            <img srcset="https://media.nngroup.com/media/files/images/IMG_4730_copy_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale@2x.jpg 2x" src="https://media.nngroup.com/media/files/images/IMG_4730_copy_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale.jpg" alt="">
          
        
      
    
    </div>
  </section>
</div>


                </div>
            </div>
        </div>
    </div>

    <div class="row hide-for-small-down related-content">
      
      
        <section class="l-subsection related-courses medium-6 column">
          <h1>Related Courses</h1>
           <ul class="no-bullet">
    
        <li><a href="../ux-deliverables/index.php">UX Deliverables</a></li>
    
        <li><a href="../lean-ux-and-agile/index.php">Lean UX and Agile</a></li>
    
        <li><a href="../ideation/index.php">Effective Ideation Techniques for UX Design</a></li>
    
        <li><a href="../generating-big-ideas/index.php">Generating Big Ideas with Design Thinking</a></li>
    
        <li><a href="../cross-channel-user-experience/index.php">Omnichannel Journeys and Customer Experience</a></li>
    
</ul>
        </section>
      
      

      
      
        <section class="l-subsection related-articles medium-6 column">
        <h1>Related Articles</h1>
        <ul class="no-bullet">
          
            
            <li><a href="../../articles/tabs-used-right/index.php">Tabs, Used Right</a></li>
            
          
            
            <li><a href="../../articles/indicators-validations-notifications/index.php">Indicators, Validations, and Notifications: Pick the Correct Communication Option</a></li>
            
          
            
            <li><a href="../../articles/passwords-memory/index.php">Help People Create Passwords That They Can Actually Remember</a></li>
            
          
            
            <li><a href="../../articles/progress-indicators/index.php">Progress Indicators Make a Slow System Less Insufferable</a></li>
            
          
            
            <li><a href="../../articles/radio-buttons-default-selection/index.php">Radio Buttons: Select One by Default or Leave All Unselected?</a></li>
            
          
        </ul>
        </section>
      
      

    </div>
</article>

    </div>
    




<footer id="page-footer">
    <div class="row hide-for-print">
        <div class="column small-12 medium-6">
            <section class="footer-popular">
                
                <h1>Popular Topics</h1>
                <div style="float: left;  width: 15em; display: block;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/applications/index.php">Application Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/e-commerce/index.php">E-Commerce Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/eyetracking/index.php">Eyetracking </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/information-architecture/index.php">Information Architecture </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/intranets/index.php">Intranet Design </a></li>
	<li><a href="../../topic/interaction-design/index.php">Interaction Design </a></li>
</ul>
</div>

<div style="float: left;  width: 15em; display: block; list-style: none;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/mobile-and-tablet-design/index.php">Mobile and Tablet Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/credibility-persuasion/index.php">Persuasive Design</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/research-methods/index.php">Research Methods</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/user-testing/index.php">User Testing </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/web-usability/index.php">Web Usability </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/writing-web/index.php">Web Writing &amp; Content Strategy </a></li>
</ul>
</div>

<p>&nbsp;</p>

                
            </section>
        </div>

        <div class="column small-12 medium-6">
            <div class="row">
                <div class="column small-6">
                    <section class="footer-about">
                        <h1>Follow Us</h1>
                        <ul>
	<li><a href="../../articles/subscribe/index.php">Subscribe to our weekly UX newsletter</a>,&nbsp;Jakob Nielsen&#39;s Alertbox</li>
	<li>Visit Don Norman&#39;s website, <a href="http://www.jnd.org/"> www.jnd.org </a></li>
	<li>Visit Bruce Tognazzini&#39;s website, <a href="http://www.asktog.com/"> www.asktog.com </a></li>
	<li style="padding-top: 6px;"><a class="twitter-follow-button" data-show-count="false" data-size="large" href="https://twitter.com/nngroup"><img alt="Follow @nngroup on Twitter" height="20" src="https://media.nngroup.com/media/editor/2016/12/30/twitter-follow-button.png" style="width: 117px; height: 20px;" width="117" /></a></li>
	<li style="padding-top: 6px;"><img alt="" height="14" src="https://media.nngroup.com/media/editor/2016/07/26/feed-icon-14x14.png" style="vertical-align:text-top;width:14px;margin-right:.25em;" width="14" /><a href="../../feed/rss/index.php">Subscribe to our RSS feed</a></li>
</ul>

                    </section>
                </div>

                <div class="column small-6">
                    <section class="footer-about right-column">
                        <h1>About
</h1>
                        <ul class="no-bullet">
	<li><a href="../../about/contact/index.php">Contact information</a></li>
	<li><a href="../../about/index.php">About Us</a></li>
	<li><a href="../../about/why-nng/index.php">Why NN/g? </a></li>
	<li><a href="../../return-policy/index.php">Return Policy</a></li>
</ul>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="copyright column small-12">
            <p><a href="../../copyright-and-reprint-info/index.php">Copyright</a> &copy; 1998-2017 Nielsen Norman Group, All Rights Reserved.</p>
        </div>
    </div>
</footer>


    


        <script src="https://media.nngroup.com/static/vendor/fastclick/js/fastclick.min.js"></script>
        <script src="https://media.nngroup.com/static/vendor/foundation/js/foundation.min.js"></script>
        <script src="http://media.nngroup.com/static/js/app.js"></script>
        <script src="http://media.nngroup.com/static/js/scroll-to-fixed.js"></script><script>var externalLinks = document.querySelectorAll('a[href^="http"]');for (var i = externalLinks.length-1; i >= 0; i--) {externalLinks[i].addEventListener("click", function(event) { alert("External links are disabled for this test."); event.preventDefault(); }, false);}var mailLinks = document.querySelectorAll('a[href^="mailto"]');for (var i = mailLinks.length-1; i >= 0; i--) {mailLinks[i].addEventListener("click", function(event) { alert("Email links are disabled for this test."); event.preventDefault(); }, false);}var externalForms = document.querySelectorAll('form');for (var i = externalForms.length-1; i >= 0; i--) {externalForms[i].addEventListener("submit", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false); externalForms[i].addEventListener("click", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false);}</script>

        <script>
            
            $(function() {
                $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
                $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
            });
        </script>

        


        
        <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1072699960;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072699960/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
    </body>

<!-- Mirrored from www.nngroup.com/courses/application-ux/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 14:58:57 GMT -->
</html>
