<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->





    
<!-- Mirrored from www.nngroup.com/ux-certification/featured-people/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 14:58:37 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1026.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"licenseKey":"1a30ef9931","errorBeacon":"bam.nr-data.net","applicationID":"2048992","transactionName":"NFxSMRBUX0EDUkIPDA0WdhAMVkVbDV8ZBQYRTVkDC1ZQRgteWBYRDF9ZCQcbR1sHRkVcIAZLRAwEXFJTFlhZCDMCXlVLBVBF","beacon":"bam.nr-data.net","queueTime":0,"applicationTime":122,"agent":""}</script>
        <title>Featured UX Certified Professionals: Case Studies of the UX Certification experience and benefits | Nielsen Norman Group</title><meta property="og:title" content="Featured UX Certified Professionals: Case Studies of the UX Certification experience and benefits | Nielsen Norman Group" />
  
        
        <meta name="description" content="Hear from practitioners who have participated in the UX Certification program in their own words, describing the UX Certification experience. With backgrounds ranging from graphic design to programming to  business management, learn about the skills they&#39;ve acquired from NN/g UX Training. ">
        <meta property="og:description" content="Hear from practitioners who have participated in the UX Certification program in their own words, describing the UX Certification experience. With backgrounds ranging from graphic design to programming to  business management, learn about the skills they&#39;ve acquired from NN/g UX Training. " />
        
  
        
	
        
        <meta name="keywords" content="case studies, profiles, featured professionals, UX Certification, UX Master Certification, benefits of UX Certification">
        

        

        <meta name="robots" content="noarchive">
        <meta name="viewport" content="width=device-width">

        <link rel="search" type="application/opensearchdescription+xml" title="NN/g Search" href="../../opensearch.xml" />
        <link rel="shortcut icon" href="http://media.nngroup.com/static/img/favicon.ico" />

        <link rel="stylesheet" href="https://media.nngroup.com/static/vendor/font-awesome/css/font-awesome.min.css">

        
        <link rel="stylesheet" href="https://media.nngroup.com/static/css/7232a960f1df.css" type="text/css" />
        <link rel="stylesheet" href="http://fast.fonts.com/t/1.css?apiType=css&amp;projectid=c5846260-9def-46ec-9407-0f1349bd2d64" type="text/css">

        <script src="https://media.nngroup.com/static/vendor/modernizr/js/modernizr.min.js"></script>

        <script type="text/javascript" src="https://media.nngroup.com/static/js/3283be5def74.js"></script>
        

        

        
        
        <!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=229034,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('http://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->


<!-- Google Analytics Tracking Code for http://nngroup.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23132681-1', 'nngroup.com');
  ga('require', 'linkid', 'https://www.nngroup.com/ux-certification/featured-people/linkid.js');
  ga('require', 'displayfeatures');

// Optimizely Universal Analytics Integration
 window.optimizely = window.optimizely || [];
 window.optimizely.push("activateUniversalAnalytics");

  ga('send', 'pageview');

</script>
<script>
/**
* Function to track a click on an outbound link in Google Analytics.
*/
function trackOutboundLink(label, url) {
    if(window.ga && ga.loaded) {
        ga('send', 'event', 'outbound', 'click', label, {'hitCallback': function(){document.location = url;}
        });
    } else {
        document.location = url;
    }
}
/**
* Function to track a click on a link in the article announcement area.
*/
$('#articleAnnouncement a').on('click', function(){
  ga('send', 'event', 'link', 'click', 'articleAnnouncement');
});
</script>

<script type="text/javascript">
 setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2421.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



<!-- Hotjar Tracking Code for http://nngroup.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:121605,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

        <script src="https://media.nngroup.com/static/js/analytics/track_outbound_link.js"></script>
        
        
        
    </head>
    <body class="location-training location-ux-certification ux-certification-page location-featured-people">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
    <header id="page-header">
        <div id="cart" class="shopping-cart mobile-cart show-for-small-down"></div>
        <div class="row">
            <div id="masthead" class="masthead small-12 medium-7 column">

                <!-- The button to toggles menu on small screen -->

                <h1><a href="../../index.php"><img src="http://media.nngroup.com/static/img/logo-wordmark.png" width="74" height="30"> Nielsen <span class="norman">Norman</span> Group</a></h1>
                <p class="tagline show-for-medium-up">Evidence-Based User Experience Research, Training, and Consulting</p>

                <!-- The button to toggles form search on small screen -->


                <div style="clear: right;width:  100%;height: 1px;"></div><?php define('ROOT_PATH',$_SERVER["DOCUMENT_ROOT"] . '/apps/navigation_styles/navigations/1'); ?><?php include(ROOT_PATH)."/navigation.php"; ?>

                <!-- Drop down menu on small screen -->
                <div id="hideNav" class="show-for-small-down">
                  
                  <ul class="menu-content">
                    <li><a href="../../index.php">Home</a></li>
                    <li class="has-submenu">
                      <a>Training</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../training/index.php">Overview</a></li>
                        
<li id="nav-ux-conference"><a href="../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../online-seminars/index.php"><span>Online Seminars</span></a></li>

                      </ul>
                    </li>
                    <li class="has-submenu">
                      <a>Consulting</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
  <li><a href="../../consulting/index.php">Overview</a></li>
  
  <li><a href="../../consulting/benchmarking/index.php">Benchmarking</a></li>
  
  <li><a href="../../consulting/expert-design-review/index.php">Expert Design Review</a></li>
  
  <li><a href="../../consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
  
  <li><a href="../../consulting/usability-testing/index.php">Usability Testing </a></li>
  
  <li><a href="../../consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
  
  <li><a href="../../in-house-training/index.php">In-House Training</a></li>
</ul>

                    </li>

                    <li><a href="../../reports/index.php">Reports</a></li>
                    <li><a href="../../articles/index.php">Articles</a></li>
                    <li class="has-submenu"><a>About NN/G</a><i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../about/index.php">Overview</a></li>
                        <li><a href="../../people/index.php">People</a></li>
                        <li><a href="../../about/why-nng/index.php">Why NN/g?</a></li>
                        <li><a href="../../about/contact/index.php">Contact</a></li>
                        <li><a href="../../news/index.php">News</a></li>
                        <li><a href="../../about/history/index.php">History</a></li>
                        <li><a href="../../books/index.php">Books</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                </div>

                <hr class="header-divider show-for-small-only"/>


            </div>
       </div>

        <div id="hideNav">            <nav class="primary-nav small-12 column">
                <ul>
                    <li><a class="navitem" id="nav-home" href="../../index.php">Home</a></li>
                    <li><a class="navitem" id="nav-training" href="../../training/index.php">Training</a></li>
                    <li><a class="navitem" id="nav-consulting" href="../../consulting/index.php">Consulting</a></li>
                    <li><a class="navitem" id="nav-reports" href="../../reports/index.php">Reports</a></li>
                    <li><a class="navitem" id="nav-articles" href="../../articles/index.php">Articles</a></li>
                    <li><a class="navitem" id="nav-about" href="../../about/index.php">About NN/G</a></li>
                </ul>
            </nav>
            <nav class="secondary-nav small-12 medium-12 large-12 columns">
                
  <ul class="inline-list">
  
<li id="nav-ux-conference"><a href="../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../online-seminars/index.php"><span>Online Seminars</span></a></li>

</ul>


            </nav>
        </div>
    </header>
    <div class="l-content">
    
  <div class="row">
    <section class="small-12 medium-3 columns collapsable-content">
      
  <h1 class="show-for-small-down collapse-link">More UX Certification Information</h1>
  <div class="collapsable">
  <ul class="page-side-nav root">
      <li role="menuitem">
        <a href="../index.php">About UX Certification</a>
      </li>
      
      <li role="menuitem" class="">
        <a href="../process/index.php">Process</a>
        
          
        
      </li>
      
      <li role="menuitem" class="">
        <a href="../benefits/index.php">Benefits</a>
        
          
        
      </li>
      
      <li role="menuitem" class="">
        <a href="../exams/index.php">Exams</a>
        
          
        
      </li>
      
      <li role="menuitem" class="">
        <a href="../specialties/index.php">Specialties</a>
        
          
            <ul class="page-side-nav">
            
              <li class="">
                <a href="../interaction-design-specialty/index.php">Interaction Design</a>
              </li>
            
              <li class="">
                <a href="../mobile-design-specialty/index.php">Mobile Design</a>
              </li>
            
              <li class="">
                <a href="../ux-management-specialty/index.php">UX Management</a>
              </li>
            
              <li class="">
                <a href="../ux-research-specialty/index.php">UX Research</a>
              </li>
            
              <li class="">
                <a href="../web-design-specialty/index.php">Web Design</a>
              </li>
            
            </ul>
          
        
      </li>
      
      <li role="menuitem" class="">
        <a href="../ux-master-certification/index.php">UX Master Certification</a>
        
          
        
      </li>
      
      <li role="menuitem" class="">
        <a href="../pricing/index.php">Pricing</a>
        
          
        
      </li>
      
      <li role="menuitem" class="">
        <a href="../scheduling/index.php">Scheduling</a>
        
          
        
      </li>
      
      <li role="menuitem" class="active">
        <a href="index.php">Featured UX Certified People</a>
        
          
        
      </li>
      
      <li role="menuitem" class=""><a href="../verify/index.php">List of UX Certified People</a></li>
    </ul>
  </div>

    </section>
    <article class="small-12 medium-9 columns">
      
        
        
        
          
          
          <section class="pagebit l-subsection"><h1>Featured UX Certified Professionals</h1>

<p>We at NN/g are very proud of the <a href="../verify/index.php">people who participate</a> in the&nbsp;<a href="../index.php">UX Certification Program</a>.</p>

<p>Read some of their stories here. (Would you like to be interviewed for a case study? Email <a href="mailto:certification@nngroup.com?subject=Case%20Study">certification@nngroup.com</a>.)</p>

<p>&nbsp;</p>

<h1>Enhancing 19 Years of UX Design With UX Certification</h1>

<div>
<div style="float: left; padding-right: 20px; max-width: 50%;">
<p><img alt="Photo of Jaime Wolf" height="200" src="https://media.nngroup.com/media/editor/2016/11/16/jamiewolf_img_2541-copy-square.jpg" width="200" /></p>
</div>

<div>
<p><strong>Jaime Wolf, UX Certified</strong><br />
Sr. User Experience Designer at Dolby<br />
San Jose, California, U.S.A.<br />
Years in the UX field: 10+&nbsp;<br />
Specialty: <a href="../interaction-design-specialty/index.php">Interaction Design</a> &amp; <a href="../ux-research-specialty/index.php">UX Research</a></p>

<h3>UX Certification Benefits</h3>

<p>&ldquo;I learned things that I could immediately put into place when I got back to work.&rdquo;</p>

<div style="clear:both;">
<h3>Organization Benefits</h3>

<p>&ldquo;I&rsquo;ve added skills and I&#39;ve learned more techniques, for instance, in the area of <a href="../../courses/usability-testing/index.php">usability testing</a> or the <a href="../../courses/human-mind/index.php">human mind</a> usability. I learned a lot of psychological information that&rsquo;s a little bit harder to notice in real life, but it&#39;s useful and valuable. I had been conducting usability testing for a while, and then I took the class here. Not everything was new, but it certainly helped me become better.&rdquo;</p>

<h3>Motivation</h3>

<p>&ldquo;I was transitioning from caring for only aesthetics to improving and creating a user-friendly experience. I&#39;ve evolved in my career, so I&#39;ve been passionate about really having user data to inform my science.&nbsp;I had already known and respected NN/g and was already taking classes. There was this bonus of applying what I&rsquo;ve learned toward UXC. It made it convenient.&rdquo;</p>

<h3>Career Path</h3>

<p>Jaime made her mark in the world of audio by being the first to do UX work at both of her audio-industry employers: first Polycom, then Dolby. Her biggest UX accomplishment to date was helping to design a groundbreaking, award-winning conference phone, the Polycom&reg; RealPresence TrioTM. She chose the NN/g UXMC Program instead of a traditional master&rsquo;s degree.&nbsp;</p>
</div>
</div>
</div>

<p>&nbsp;</p>

<h1>Streamlining Research and Design Methods</h1>

<div>
<div style="float: left; padding-right: 20px; max-width: 50%;">
<h2><img alt="Photo of Marc Kollmann" height="200" src="https://media.nngroup.com/media/editor/2016/11/16/marc-kollmann_img_2677-copy-square.jpg" width="200" /></h2>
</div>

<div>
<p><strong>Marc Kollmann, UX Master Certified</strong><br />
Sr. UX/UI Designer at Kollmann Creative<br />
Amsterdam, The Netherlands, E.U.<br />
Years in the UX field: 10+<br />
Specialty: <a href="../interaction-design-specialty/index.php">Interaction Design</a> &amp;&nbsp;<a href="../ux-management-specialty/index.php">UX Management</a></p>

<h3>UX Certification Benefits</h3>

<p>&ldquo;I learned a lot of things attending, and also from course materials; techniques that I can use, especially on <a href="../../courses/usability-testing/index.php">usability testing</a>. I really connected on a higher level. I even got some feedback from my fellow attendees about programming, which I&#39;m using right now.&rdquo;</p>

<h3>Organization Benefits</h3>

<p>&ldquo;I went guerilla, like more <a href="../../courses/one-person-ux-team/index.php">guerilla testing</a>. We went to the offices where our users are with testing software I got from some of the other NN/g attendees. There was a whole thing that happened the Amsterdam conference. My co-worker said, &lsquo;Wow! You really came back with a bunch of stuff,&rsquo; which we&rsquo;re now using.&rdquo;</p>

<h3>Motivation</h3>

<p>&ldquo;I was following NN/g for a while because I was looking for education or a refresher course. Jakob Nielsen is quite a name in the industry; he&#39;s lived up to his name and to that of the NN/g organization. I have followed them. I said to my colleague, &rsquo;You know, I really want to do the whole conference thing.&rsquo; He did it first in London, and when it was came to Amsterdam, I was there.&rdquo;</p>

<h3>Career Path</h3>

<p>&ldquo;Transitioning from only caring for aesthetics to improving or creating a positive user experience for users and in all facets of a company.&rdquo;</p>
</div>
</div>

<p>&nbsp;</p>

<h1>Melding Content Strategy With UX Strategy</h1>

<div>
<div style="float: left; padding-right: 20px; max-width: 30%;">
<p><img alt="Photo of Lisanne Wirth" height="200" src="https://media.nngroup.com/media/editor/2016/11/16/lisannewirth_img_2848-copy-square.jpg" width="200" /></p>
</div>

<div>
<p><strong>Lisanne Wirth, UX Certified</strong><br />
UX/CX Content Strategist &amp; Owner of Contentment Communications<br />
Folsom, California, USA<br />
Years in the UX field: 10+<br />
Specialty: <a href="../ux-management-specialty/index.php">UX Management</a></p>

<h3>UX Certification Benefits</h3>

<p>&ldquo;It helped me solidify my UX <a href="../../courses/writing/index.php">content</a> expertise early on. I created the first content strategy for VerizonWireless.com Support, which covered everything from development to governance. With NN/g&rsquo;s expertise backing me, it became easier to gain alignment with executives on new initiatives I proposed. I was promoted and my team size tripled.&rdquo;</p>

<h3>Organization Benefits</h3>

<p>&ldquo;At Verizon, I saw the entire Internet organization transform because of UX training. The organization had a common understanding and vocabulary about how to truly put customers first online. We became smarter about testing enhancements to ensure we used resources for maximum impact. The content strategy I developed was eventually benchmarked for an enterprise-wide content alliance.&rdquo;</p>

<h3>Motivation</h3>

<p>&ldquo;The customization options for choosing a specialty is what sets NN/g&rsquo;s training apart from other certification programs. They have robust programs covering UX, content strategy, and mobile. The courses are relevant, timely, and forward-thinking.&quot;</p>

<h3>Career Path</h3>

<p>Lisanne started taking NN/g courses in 2010, and it accelerated her career at Verizon Wireless, where she spent 17 years. Later, she founded UXContentStrategy.com in order to solve new challenges. She returned to NN/g to complete the UXC Program.</p>
</div>

<p>&nbsp;</p>

<h1>Communicating UX Methodologies and Processes</h1>

<div style="float: left; padding-right: 20px; max-width: 50%;">
<p><img alt="Photo of Jim Bull" height="200" src="https://media.nngroup.com/media/editor/2016/11/16/jim-bull_img_2580-copy-square.jpg" width="200" /></p>
</div>

<div>
<p><strong>Jim Bull, UX Certified</strong><br />
Senior User Experience Architect at Phoenix Contact<br />
Harrisburg, Pennsylvania, USA<br />
Years in the UX field: 10+<br />
Specialty: <a href="../interaction-design-specialty/index.php">Interaction Design</a> &amp; <a href="../ux-management-specialty/index.php">UX Management</a></p>

<h3>UX Certification Benefits</h3>

<p>&ldquo;I basically attribute everything I know about user experience to NN/g. I&#39;m grateful for all the great insight on the website, and now these courses. They are pioneers of their craft and I wholeheartedly like to follow the methodologies. I believe in it, and I believe in the cognitive science behind UX.&rdquo;</p>

<h3>Organization Benefits</h3>

<p>&ldquo;My boss sees value in it because of all the projects and deliverables that I&#39;m getting out, and giving to the right people to consume it. And the developers are starting to have UX thinking.&rdquo;</p>

<h3>Motivation</h3>

<p>&ldquo;I wanted to add credibility to my role. One of the hardest things that I&#39;ve ever had to do is describe UX, describe the methodologies and the processes that you go through. It&rsquo;s a career progression and it gives you the influence that says you put the time in. It&#39;s just like any other degree. The instructors are fantastic, all of them. You know they&rsquo;re definitely the best in the field. The experience has always been good. I&#39;ll come back every year.&rdquo;</p>

<h3>Career Path</h3>

<p>&ldquo;As a Multimedia Director, I hated designing user interfaces because people didn&rsquo;t &lsquo;get&rsquo; the trendy designs that I was proud of. Then I discovered UX.&nbsp;It&#39;s really been a fun career path.&rdquo;</p>
</div>

<p>&nbsp;</p>

<h1>Gaining Credibility in Government Work with UX Frameworks and Techniques<strong>&nbsp;</strong></h1>

<div style="float: left; padding-right: 20px; max-width: 50%;">
<p><img alt="Photo of Michelle Larsen" height="200" src="https://media.nngroup.com/media/editor/2016/11/16/michelle-larsen_img_2176-copy-square.jpg" width="200" /></p>
</div>

<div>
<p><strong>Michelle A. Larsen, UX Master Certified</strong><br />
Sr. Analyst, Systems Development &amp; QA at Aquas, Inc.<br />
Washington, D.C., USA<br />
Years in the UX field: 3<br />
Specialty: <a href="../interaction-design-specialty/index.php">Interaction Design</a>, <a href="../ux-management-specialty/index.php">UX Management</a>, <a href="../ux-research-specialty/index.php">UX Research</a></p>

<h3>UX Master Certification Benefits</h3>

<p>&ldquo;The immediate benefit is that I feel a lot more confident about what I&rsquo;m doing, what I&rsquo;m saying to people when I espouse the value, the benefits of user-centric thinking.&rdquo;</p>

<h3>Organization Benefits</h3>

<p>&ldquo;In the world of government contracting, procuring contracts whether state or federal, it bodes well if you hold multiple certifications as a way to prove experience. It&rsquo;s definitely an attractive thing for me that I can put acronyms after my name. And just having the reinforcement gives confidence in what I can provide, what kind of knowledge and skills I can provide at a project level.&rdquo;</p>

<h3>Motivation</h3>

<p>&ldquo;A lot of the job for someone who is going to be a UX practitioner is not just the acquisition of hard skills, but knowing what the framework is and then knowing when to apply the right technique. Each NN/g instructor brings their own interpretation of, &lsquo;These are my practices, this is my framework, and this is what I can use.&rsquo; I decided after reading about the specializations and different levels, that if I planned it out well, I could get the Master&rsquo;s Certification with specialties.&rdquo;</p>

<h3>Career Path</h3>

<p>&ldquo;Artist/ designer/ organizer/ standards leader/ use-centered design leader. I used to make beauty for myself, now I do it for others.&rdquo; She kick-started her organization&rsquo;s UX.&nbsp;</p>

<p>&nbsp;</p>

<h1>Mastering Group Dynamics in UX Design Processes</h1>

<div style="float: left; padding-right: 20px; max-width: 50%;">
<p><img alt="Photo of Tom Verhage" height="200" src="https://media.nngroup.com/media/editor/2016/11/16/tom-verhage_img_2104-copy-square.jpg" width="200" /></p>
</div>

<p><strong>Tom Verhage, UX Certified</strong><br />
UX Designer at Ordina<br />
The Netherlands, E.U.<br />
Years in the UX field: 5<br />
Specialty: <a href="../ux-management-specialty/index.php">UX Management</a>, <a href="../ux-research-specialty/index.php">UX Research</a></p>

<h3>UX Certification Benefits</h3>

<p>&ldquo;Soft skills, like the way I behave in a group and change my usual way of doing something in favor of getting something done. I think I&rsquo;ve learned a lot of that here. You know you&rsquo;re right. But that doesn&rsquo;t mean everyone knows you&rsquo;re right. So, you have to change your way of acting and responding, and eventually you get done what you want to get done. Being better at soft skills makes me more appreciated by coworkers, which results in more trust.&rdquo;</p>

<h3>Organization Benefits</h3>

<p>&ldquo;I went to the NN/g Conference in London, and I came back all enthusiastic and fired up. It was like, &lsquo;Yeah, we&rsquo;re going to do this!&rsquo; And then 2 of my colleagues went. One of them actually went to the San Francisco conference too.&rdquo; Tom&rsquo;s manager can charge more for contracts because of Tom&rsquo;s advanced UX training from NN/g. The UXC Program is a product differentiator for Tom.</p>

<h3>Motivation&nbsp;</h3>

<p>&ldquo;It was really fun because it was such an international crowd. It was the whole of Europe.&rdquo;</p>

<h3>Career Path</h3>

<p>&ldquo;First functional design, then visual design, now UX design. I have always done development and consultancy as well.&rdquo; From the Netherlands to London to San Francisco, Tom Verhage has made international work part of his job description thanks to a strong ROI on the UXC Program.&nbsp;</p>

<p>&nbsp;</p>

<h1>Using UX to Drive Projects</h1>

<div style="float: left; padding-right: 20px; max-width: 50%;">
<h2><img alt="Photo of Mike Brodeur" height="200" src="https://media.nngroup.com/media/editor/2016/11/16/mike-brodeur_cropped.png" width="180" /></h2>
</div>

<div>
<p><strong>Mike Brodeur, UX Master Certified</strong><br />
Senior Web Software Developer at Rogers Corporation<br />
Rogers, Connecticut, USA<br />
Specialty: <a href="../web-design-specialty/index.php">Web Design</a></p>

<h3>UX Master Certification Benefits</h3>

<p>&ldquo;Achieving a UX Master Certification has enabled me to provide better qualitative research, enabling my colleagues to understand the reasoning behind user&rsquo;s decisions and thought processes. UXMC also allowed my suggestions and research to carry more weight with my colleagues.&rdquo;</p>

<h3>Organization Benefits</h3>

<p>&ldquo;Our newest website redesign project has been a watershed moment for our company in the realm of UX.&nbsp; For the first time, I have been able to be drive our preparations more thoroughly by creating and implementing useful <a href="../../courses/personas/index.php">personas</a> and testing our <a href="../../courses/information-architecture/index.php">information architecture</a> rather than simply jumping into a graphical design we think might work. And also for the first time, I have been able to get UX testing built directly into the project plan. Really understanding UX concepts and applying the results of <a href="../../courses/usability-testing/index.php">usability studies</a> helps me produce more usable tools and websites for my customers, both internal and external.&rdquo;</p>

<h3>Motivation</h3>

<p>&ldquo;I attended my first NN/g conference in 2008, initially thinking that would help my design and development layouts. However, it was during this week that my introduction to the world of UX would begin and I would start to learn how it would affect everything that I, as a web developer, would produce. It was during this initial weeklong experience that I knew UX was an area of proficiency I would need to be successful. I subsequently attended additional NN/g conferences, and after learning they would be offering a UX Certification Program, it became my goal to achieve this standard.&nbsp;</p>

<h3>Career Path</h3>

<p>Making usability a priority has become my priority throughout every part of the development process. Whether through an online community or simply reading articles from NN/g, UX has become more than just a training class I attend once a year. It has become an important skill I am passionate about. It&nbsp;helps me be successful as both a web developer and a project leader.</p>
</div>

<p>&nbsp;</p>

<h1>Supporting International Work with Strong UX Foundations</h1>

<div style="float: left; padding-right: 20px; max-width: 50%;">
<h2><img alt="Photo of Gastón Milano" height="200" src="https://media.nngroup.com/media/editor/2016/11/16/gaston-milano_img_2689-copy-square.jpg" width="200" /></h2>
</div>

<div>
<p><strong>Gast&oacute;n Milano, UX Certified</strong><br />
Chief Technology Officer at GeneXus<br />
Uruguay<br />
Years in the UX field: 10+<br />
Specialty: <a href="../interaction-design-specialty/index.php">Interaction Design</a></p>

<h3>UX Certification Benefits</h3>

<p>&ldquo;I took it because it was a challenge for me. I was reading and reading, and thought, &lsquo;What do you really know about UX? Do you really understand the thing you are working on or not?&rsquo; Sometimes, when you meet somebody or you go to some countries like Japan or China, in many cases a certificate is very important to prove what you know.&rdquo;</p>

<h3>Organization Benefits</h3>

<p>&ldquo;As software engineers, we are more logical. In many cases we are designing for other people. Only with the user can we can we find the final answer. And NN/g has a lot of these reports, real cases, with real people.&rdquo;</p>

<h3>Motivation</h3>

<p>&ldquo;I truly believe in the research studies that NN/g does. For 20 years, I&rsquo;ve been reading their newsletter articles. I like their approach, not following the first new design that&rsquo;s out there. Engineers want proof that something is going to work or not, and why. I needed to have a foundation of principles in user experience, people who truly know the topic they are talking about. And I felt that it was the best way network with people who have the same interests.&rdquo;</p>

<h3>Career Path</h3>

<p>&ldquo;I have been working all my career to create software that makes software.&rdquo; Gast&oacute;n has worked for the same employer to improve software development over two decades. His training from NN/g has helped him to confirm his UX expertise and to develop blueprints for an innovative path. He wants his company&rsquo;s software development process to be like an artist&rsquo;s flow when creating a masterpiece, instead of the more typical assembly line.&nbsp;</p>

<p>&nbsp;</p>

<h1>UX to Grow Sales and Gather Leads</h1>

<div style="float: left; padding-right: 20px; max-width: 50%;">
<h2><img alt="Photo of Frank Astorga" height="200" src="https://media.nngroup.com/media/editor/2016/11/16/frank-astorga_img_2372-copy-square.jpg" width="200" /></h2>
</div>

<div>
<p><strong>Frank Astorga, UX Master Certified</strong><br />
Merchandising Coordinator at Coppel SA de CV<br />
Culiac&aacute;n Area, Mexico<br />
Years in the UX field: 5<br />
Specialty: <a href="../ux-research-specialty/index.php">UX Research</a></p>

<h3>UX Master Certification Benefits</h3>

<p>&ldquo;You come and you talk to people and you can improve with the networking you get here. It&rsquo;s important not only to improve academically, but personally as well.&rdquo;</p>

<h3>Organization Benefits</h3>

<p>&ldquo;These courses are really, really useful. The examples and the information is down to earth. And you can go to the company where you work and you apply the knowledge. We increased the leads from 30 to 300 per week. So UX may help you to get more leads, get more sales, and get more happy people.&rdquo;</p>

<h3>Motivation</h3>

<p>&ldquo;The knowledge you get here in the courses is useful, and not only for a specific culture because you can apply it according to the needs, the scope, and the environment where you are working. The actual learning is a good, and the quality never ends. You can continue to learn. Even if after got a UX Certificate, I went for the Master&rsquo;s, and I&rsquo;ll continue to get more Specialties. There&rsquo;s always something to learn.&rdquo;</p>

<h3>Career Path</h3>

<p>&ldquo;From quality analyst to manager of a global retail store thanks to NN/g.&rdquo;</p>

<p>&nbsp;</p>

<h1>Harmonizing UX with Lean and Agile Methods</h1>

<div style="float: left; padding-right: 20px; max-width: 50%;">
<p><img alt="Photo of Colin Miller" height="200" src="https://media.nngroup.com/media/editor/2016/11/16/colinmiller_img_2694-copy-square.jpg" width="200" /></p>
</div>

<div>
<p><strong>Colin Miller, UX Certified</strong><br />
Sr. User Experience Designer at a&nbsp;global enterprise software provider<br />
San Francisco, California, USA<br />
Years in the UX field: 5<br />
Specialty: <a href="../ux-management-specialty/index.php">UX Management</a></p>

<h3>UX Certification Benefits</h3>

<p>&ldquo;The first thing was this real broadening of my understanding of the field. It wasn&rsquo;t just doing visual design or interaction design, but integrating both together. The other thing was using <a href="../../courses/lean-ux-and-agile/index.php">Lean processes</a> to validate the features and then using <a href="../../courses/usability-testing/index.php">usability testing</a> to research the interaction design.&rdquo;</p>

<h3>Organization Benefits</h3>

<p>&ldquo;We can&rsquo;t say, &lsquo;We need a better login page because it&rsquo;s better UX.&rsquo; Instead, we say, &lsquo;We need a better UX on the login page because we&rsquo;ll have less abandonment there, and that means our user acquisition will go up, then our stock price goes up.&rsquo; Now, the business person says, &lsquo;I get it.&rsquo; You&rsquo;re talking to them in terms that have meaning to them.&rdquo;</p>

<h3>Motivation&nbsp;</h3>

<p>&ldquo;The Management Specialty is aligned with my aspirations, opportunities at my current job, and what&rsquo;s happening with the industry in general. I see a lack of leadership in the UX area, which I think has to do with the maturity of UX. A lot of companies didn&rsquo;t have any designers five years ago.&rdquo;</p>

<h3>Career Path&nbsp;</h3>

<p>&ldquo;I have been in leadership, creative, and design roles from film to 3D projection mapping to UX.&rdquo; Colin combines traditional artistic techniques with digital technology for movies and UX design. He&rsquo;s worked on films like <em>The Nightmare Before Christmas</em>, <em>Coraline</em>, and <em>The Avengers</em>. His UX passion stems from his vision of a better world and championing user feedback during the design process. The UXC Program provides a theoretical framework for Colin as a UX leader.&nbsp;</p>

<p>&nbsp;</p>

<h1>Evangelizing UX with Science and Data&nbsp;</h1>

<div style="float: left; padding-right: 20px; max-width: 50%;">
<h2><img alt="Photo of Aaron Olsen" height="200" src="https://media.nngroup.com/media/editor/2016/11/16/aaron-olsen_img_2843-copy-square.jpg" width="200" /></h2>
</div>

<div>
<p><strong>Aaron Olsen, UX Certified</strong><br />
Director of Web Development at Brigham Young University<br />
Provo, Utah, USA<br />
Years in the UX field: 10+</p>

<h3>UX Certification Benefits</h3>

<p>&ldquo;The training has helped in discussions that I have with different stakeholders, to add some weight to what I&rsquo;m saying. It&#39;s come up over the last year at least 20 times in meetings. It&#39;s not just me who brings it up, others reference it, asking &lsquo;Hey, what did you learn there about this?&rsquo;&rdquo;</p>

<h3>Organization Benefits</h3>

<p>&ldquo;I love those concepts of showing the process and presenting UX as a science, giving the team some insight into that, and success stories. That&#39;s what I&#39;m doing with my group. We identified 4 UX successes that we did, and we put together 30 to 60-page reports.&rdquo;</p>

<h3>Motivation</h3>

<p>&ldquo;This whole UX thing is something that I have to work hard to sell. There&#39;s a lot to the science. It&#39;s complicated. I&#39;ve got to educate them on the value, and the UX Certificate is a must-have for me.&nbsp;I wanted to solidify UX in my mind so that I could present to the world that that UX is what I was, that&#39;s what I do. Going to the conference, I got exactly that.&quot;</p>

<h3>Career Path</h3>

<p>&ldquo;Front-end website developer with an MBA and 20 years&rsquo; experience coding and managing web development projects.&rdquo;</p>
</div>
</div>
</div>
</div>
</div>
</div>

<h2>&nbsp;</h2>

<h2>&nbsp;</h2>

<p>&nbsp;</p>

<h2>Learn more</h2>

<p>Get a comprehensive overview of the <a href="../benefits/index.php">benefits</a> of UX Certification for both individuals and organizations.</p>
</section>
        
      
    </article>
  </div>

    </div>
    




<footer id="page-footer">
    <div class="row hide-for-print">
        <div class="column small-12 medium-6">
            <section class="footer-popular">
                
                <h1>Popular Topics</h1>
                <div style="float: left;  width: 15em; display: block;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/applications/index.php">Application Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/e-commerce/index.php">E-Commerce Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/eyetracking/index.php">Eyetracking </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/information-architecture/index.php">Information Architecture </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/intranets/index.php">Intranet Design </a></li>
	<li><a href="../../topic/interaction-design/index.php">Interaction Design </a></li>
</ul>
</div>

<div style="float: left;  width: 15em; display: block; list-style: none;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/mobile-and-tablet-design/index.php">Mobile and Tablet Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/credibility-persuasion/index.php">Persuasive Design</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/research-methods/index.php">Research Methods</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/user-testing/index.php">User Testing </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/web-usability/index.php">Web Usability </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/writing-web/index.php">Web Writing &amp; Content Strategy </a></li>
</ul>
</div>

<p>&nbsp;</p>

                
            </section>
        </div>

        <div class="column small-12 medium-6">
            <div class="row">
                <div class="column small-6">
                    <section class="footer-about">
                        <h1>Follow Us</h1>
                        <ul>
	<li><a href="../../articles/subscribe/index.php">Subscribe to our weekly UX newsletter</a>,&nbsp;Jakob Nielsen&#39;s Alertbox</li>
	<li>Visit Don Norman&#39;s website, <a href="http://www.jnd.org/"> www.jnd.org </a></li>
	<li>Visit Bruce Tognazzini&#39;s website, <a href="http://www.asktog.com/"> www.asktog.com </a></li>
	<li style="padding-top: 6px;"><a class="twitter-follow-button" data-show-count="false" data-size="large" href="https://twitter.com/nngroup"><img alt="Follow @nngroup on Twitter" height="20" src="https://media.nngroup.com/media/editor/2016/12/30/twitter-follow-button.png" style="width: 117px; height: 20px;" width="117" /></a></li>
	<li style="padding-top: 6px;"><img alt="" height="14" src="https://media.nngroup.com/media/editor/2016/07/26/feed-icon-14x14.png" style="vertical-align:text-top;width:14px;margin-right:.25em;" width="14" /><a href="../../feed/rss/index.php">Subscribe to our RSS feed</a></li>
</ul>

                    </section>
                </div>

                <div class="column small-6">
                    <section class="footer-about right-column">
                        <h1>About
</h1>
                        <ul class="no-bullet">
	<li><a href="../../about/contact/index.php">Contact information</a></li>
	<li><a href="../../about/index.php">About Us</a></li>
	<li><a href="../../about/why-nng/index.php">Why NN/g? </a></li>
	<li><a href="../../return-policy/index.php">Return Policy</a></li>
</ul>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="copyright column small-12">
            <p><a href="../../copyright-and-reprint-info/index.php">Copyright</a> &copy; 1998-2017 Nielsen Norman Group, All Rights Reserved.</p>
        </div>
    </div>
</footer>


    


        <script src="https://media.nngroup.com/static/vendor/fastclick/js/fastclick.min.js"></script>
        <script src="https://media.nngroup.com/static/vendor/foundation/js/foundation.min.js"></script>
        <script src="http://media.nngroup.com/static/js/app.js"></script>
        <script src="http://media.nngroup.com/static/js/scroll-to-fixed.js"></script><script>var externalLinks = document.querySelectorAll('a[href^="http"]');for (var i = externalLinks.length-1; i >= 0; i--) {externalLinks[i].addEventListener("click", function(event) { alert("External links are disabled for this test."); event.preventDefault(); }, false);}var mailLinks = document.querySelectorAll('a[href^="mailto"]');for (var i = mailLinks.length-1; i >= 0; i--) {mailLinks[i].addEventListener("click", function(event) { alert("Email links are disabled for this test."); event.preventDefault(); }, false);}var externalForms = document.querySelectorAll('form');for (var i = externalForms.length-1; i >= 0; i--) {externalForms[i].addEventListener("submit", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false); externalForms[i].addEventListener("click", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false);}</script>

        <script>
            
            $(function() {
                $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
                $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
            });
        </script>

        


        
        <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1072699960;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072699960/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
    </body>

<!-- Mirrored from www.nngroup.com/ux-certification/featured-people/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 14:58:37 GMT -->
</html>
