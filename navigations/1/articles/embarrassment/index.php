<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->





    
<!-- Mirrored from www.nngroup.com/articles/embarrassment/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:07:08 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1026.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"beacon":"bam.nr-data.net","queueTime":6,"applicationTime":1158,"agent":"","licenseKey":"1a30ef9931","errorBeacon":"bam.nr-data.net","applicationID":"2048992","transactionName":"NFxSMRBUX0EDUkIPDA0WdhAMVkVbDV8ZBxEXUFMJB0YfRAtUQRVZIktEDAFZVHYHRVcPD01eVRE="}</script>
        <title>Computer-Assisted Embarrassment | Nielsen Norman Group</title><meta property="og:title" content="Computer-Assisted Embarrassment | Nielsen Norman Group" />
  
        
        <meta name="description" content="Computer systems shouldn&#39;t make us feel bad. But they often do. Contextual usability methods can help discover social defects in user experience.">
        <meta property="og:description" content="Computer systems shouldn&#39;t make us feel bad. But they often do. Contextual usability methods can help discover social defects in user experience." />
        
  
        
	
        
        <meta name="keywords" content="embarrassment, harm, robots, social defects, context collapse">
        

        

        <meta name="robots" content="noarchive">
        <meta name="viewport" content="width=device-width">

        <link rel="search" type="application/opensearchdescription+xml" title="NN/g Search" href="../../opensearch.xml" />
        <link rel="shortcut icon" href="http://media.nngroup.com/static/img/favicon.ico" />

        <link rel="stylesheet" href="https://media.nngroup.com/static/vendor/font-awesome/css/font-awesome.min.css">

        
        <link rel="stylesheet" href="https://media.nngroup.com/static/css/7232a960f1df.css" type="text/css" />
        <link rel="stylesheet" href="http://fast.fonts.com/t/1.css?apiType=css&amp;projectid=c5846260-9def-46ec-9407-0f1349bd2d64" type="text/css">

        <script src="https://media.nngroup.com/static/vendor/modernizr/js/modernizr.min.js"></script>

        <script type="text/javascript" src="https://media.nngroup.com/static/js/3283be5def74.js"></script>
        

        

        
        
        <!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=229034,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('http://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->


<!-- Google Analytics Tracking Code for http://nngroup.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23132681-1', 'nngroup.com');
  ga('require', 'linkid', 'https://www.nngroup.com/articles/embarrassment/linkid.js');
  ga('require', 'displayfeatures');

// Optimizely Universal Analytics Integration
 window.optimizely = window.optimizely || [];
 window.optimizely.push("activateUniversalAnalytics");

  ga('send', 'pageview');

</script>
<script>
/**
* Function to track a click on an outbound link in Google Analytics.
*/
function trackOutboundLink(label, url) {
    if(window.ga && ga.loaded) {
        ga('send', 'event', 'outbound', 'click', label, {'hitCallback': function(){document.location = url;}
        });
    } else {
        document.location = url;
    }
}
/**
* Function to track a click on a link in the article announcement area.
*/
$('#articleAnnouncement a').on('click', function(){
  ga('send', 'event', 'link', 'click', 'articleAnnouncement');
});
</script>

<script type="text/javascript">
 setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2421.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



<!-- Hotjar Tracking Code for http://nngroup.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:121605,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

        <script src="https://media.nngroup.com/static/js/analytics/track_outbound_link.js"></script>
        
        
        
    </head>
    <body class="location-articles articles-detail">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
    <header id="page-header">
        <div id="cart" class="shopping-cart mobile-cart show-for-small-down"></div>
        <div class="row">
            <div id="masthead" class="masthead small-12 medium-7 column">

                <!-- The button to toggles menu on small screen -->

                <h1><a href="../../index.php"><img src="http://media.nngroup.com/static/img/logo-wordmark.png" width="74" height="30"> Nielsen <span class="norman">Norman</span> Group</a></h1>
                <p class="tagline show-for-medium-up">Evidence-Based User Experience Research, Training, and Consulting</p>

                <!-- The button to toggles form search on small screen -->


                <div style="clear: right;width:  100%;height: 1px;"></div><?php define('ROOT_PATH',$_SERVER["DOCUMENT_ROOT"] . '/apps/navigation_styles/navigations/1'); ?><?php include(ROOT_PATH)."/navigation.php"; ?>

                <!-- Drop down menu on small screen -->
                <div id="hideNav" class="show-for-small-down">
                  
                  <ul class="menu-content">
                    <li><a href="../../index.php">Home</a></li>
                    <li class="has-submenu">
                      <a>Training</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../training/index.php">Overview</a></li>
                        
<li id="nav-ux-conference"><a href="../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../online-seminars/index.php"><span>Online Seminars</span></a></li>

                      </ul>
                    </li>
                    <li class="has-submenu">
                      <a>Consulting</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
  <li><a href="../../consulting/index.php">Overview</a></li>
  
  <li><a href="../../consulting/benchmarking/index.php">Benchmarking</a></li>
  
  <li><a href="../../consulting/expert-design-review/index.php">Expert Design Review</a></li>
  
  <li><a href="../../consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
  
  <li><a href="../../consulting/usability-testing/index.php">Usability Testing </a></li>
  
  <li><a href="../../consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
  
  <li><a href="../../in-house-training/index.php">In-House Training</a></li>
</ul>

                    </li>

                    <li><a href="../../reports/index.php">Reports</a></li>
                    <li><a href="../index.php">Articles</a></li>
                    <li class="has-submenu"><a>About NN/G</a><i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../about/index.php">Overview</a></li>
                        <li><a href="../../people/index.php">People</a></li>
                        <li><a href="../../about/why-nng/index.php">Why NN/g?</a></li>
                        <li><a href="../../about/contact/index.php">Contact</a></li>
                        <li><a href="../../news/index.php">News</a></li>
                        <li><a href="../../about/history/index.php">History</a></li>
                        <li><a href="../../books/index.php">Books</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                </div>

                <hr class="header-divider show-for-small-only"/>


            </div>
       </div>

        <div id="hideNav">            <nav class="primary-nav small-12 column">
                <ul>
                    <li><a class="navitem" id="nav-home" href="../../index.php">Home</a></li>
                    <li><a class="navitem" id="nav-training" href="../../training/index.php">Training</a></li>
                    <li><a class="navitem" id="nav-consulting" href="../../consulting/index.php">Consulting</a></li>
                    <li><a class="navitem" id="nav-reports" href="../../reports/index.php">Reports</a></li>
                    <li><a class="navitem" id="nav-articles" href="../index.php">Articles</a></li>
                    <li><a class="navitem" id="nav-about" href="../../about/index.php">About NN/G</a></li>
                </ul>
            </nav>
            <nav class="secondary-nav small-12 medium-12 large-12 columns">
                
                
            </nav>
        </div>
    </header>
    <div class="l-content">
    
  

  

  <div class="row">
    
      
      <section class="small-12 medium-3 columns collapsable-content l-navrail articles-left-column">
        <h1 class="show-for-small-down collapse-link mobile-browse-header">Browse by Topic and Author</h1>
          <div class="row collapsable">

            
            <div class="small-6 medium-12 columns">
              <section id="article-popular-topics" style="display: none;">
                
                <h1>Topics</h1>
                <ul id="article-popular-topics">
                    
                    <li>
                        
                        <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/intranets/all/index.php">Intranets</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/user-testing/all/index.php">User Testing</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                        
                    </li>
                    
                    <li><a href="#" id="article-see-all-topics"><strong>See all topics&hellip;</strong></a></li>
                </ul>
                
              </section>
              <section id="article-all-topics" style="display: none;">
                
                <h1>
                  <span class="show-for-medium-up">All Article Topics</span>
                  <span class="show-for-small-down">All Topics</span>
                  (<a href="#" id="article-hide-all-topics">hide</a>)
                </h1>
                <ul>
                  
                  <li>
                    
                    <a href="../../topic/accessibility/all/index.php">Accessibility</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/agile/all/index.php">Agile</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/analytics-and-metrics/all/index.php">Analytics &amp; Metrics</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/applications/all/index.php">Application Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/b2b-websites/all/index.php">B2B Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/behavior-patterns/all/index.php">Behavior Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/branding/all/index.php">Branding</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/content-strategy/all/index.php">Content Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/corporate-websites/all/index.php">Corporate Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/design-patterns/all/index.php">Design Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-design-process/all/index.php">Design Process</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/email/all/index.php">Email</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/eyetracking/all/index.php">Eyetracking</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/heuristic-evaluation/all/index.php">Heuristic Evaluation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/human-computer-interaction/all/index.php">Human Computer Interaction</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-ideation/all/index.php">Ideation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/information-architecture/all/index.php">Information Architecture</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/interaction-design/all/index.php">Interaction Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/international-users/all/index.php">International Users</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/intranets/all/index.php">Intranets</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-management/all/index.php">Management</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/navigation/all/index.php">Navigation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/non-profit-websites/all/index.php">Non-Profit Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/personas/all/index.php">Personas</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/persuasive-design/all/index.php">Persuasive Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/prototyping/all/index.php">Prototyping</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/psychology-and-ux/all/index.php">Psychology and UX</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/research-methods/all/index.php">Research Methods</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/search/all/index.php">Search</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/social-media/all/index.php">Social Media</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/strategy/all/index.php">Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/user-testing/all/index.php">User Testing</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-teams/all/index.php">UX Teams</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/visual-design/all/index.php">Visual Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/writing-web/all/index.php">Writing for the Web</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/young-users/all/index.php">Young Users</a>
                    
                  </li>
                  
                </ul>
                
              </section>
            </div>
            <script>
              /* Determine which topics list will be displayed: the full or the
               * partial.  We are inlining this close to the list itself to
               * prevent rendering delays on the client
               */
              var topic_list = $.cookie("article-topics");

              var popular_topics = $('#article-popular-topics');
              var all_topics = $('#article-all-topics');

              if(topic_list == 1) {
                  popular_topics.hide();
                  all_topics.show();
              } else {
                  popular_topics.show();
                  all_topics.hide();
              }
            </script>
            

            
            <div class="small-6 medium-12 columns">
              
              <section id="article-initial-authors">
                  <h1>Author</h1>
                  <ul>
                    
                    <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                    
                    <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                    
                    <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                    
                    <li><a href="#" onClick="$('#article-initial-authors').hide(); $('#article-all-authors').show();"><strong>See all authors&hellip;</strong></a></li>
                  </ul>
              </section>
              <section id="article-all-authors" style="display: none;">
                <h1>
                  All Authors
                  (<a href="#" id="article-hide-all-authors">hide</a>)
                </h1>
                <ul>
                  
                  <li><a href="../author/raluca-budiu/index.php">Raluca Budiu</a></li>
                  
                  <li><a href="../author/patty-caya/index.php">Patty Caya</a></li>
                  
                  <li><a href="../author/susan-farrell/index.php">Susan Farrell</a></li>
                  
                  <li><a href="../author/therese-fessenden/index.php">Therese Fessenden</a></li>
                  
                  <li><a href="../author/kim-flaherty/index.php">Kim Flaherty</a></li>
                  
                  <li><a href="../author/sarah-gibbons/index.php">Sarah Gibbons</a></li>
                  
                  <li><a href="../author/aurora-harley/index.php">Aurora Harley</a></li>
                  
                  <li><a href="../author/kate-kaplan/index.php">Kate Kaplan</a></li>
                  
                  <li><a href="../author/page-laubheimer/index.php">Page Laubheimer</a></li>
                  
                  <li><a href="../author/angela-li/index.php">Angie Li</a></li>
                  
                  <li><a href="../author/hoa-loranger/index.php">Hoa Loranger</a></li>
                  
                  <li><a href="../author/kate-meyer/index.php">Kate Meyer</a></li>
                  
                  <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                  
                  <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                  
                  <li><a href="../author/kara-pernice/index.php">Kara Pernice</a></li>
                  
                  <li><a href="../author/christian-rohrer/index.php">Christian Rohrer</a></li>
                  
                  <li><a href="../author/amy-schade/index.php">Amy Schade</a></li>
                  
                  <li><a href="../author/katie-sherwin/index.php">Katie Sherwin</a></li>
                  
                  <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                  
                  <li><a href="../author/kathryn-whitenton/index.php">Kathryn Whitenton</a></li>
                  
                </ul>
              </section>
              
            </div>
            
          </div>

          
          <section class="hide-for-small-down">
            
              
              <h1>Recent Articles</h1>
              
              <ul>
                  
                  <li><a href="../seamless-cross-channel/index.php">Seamlessness in the Omnichannel User Experience</a></li>
                  
                  <li><a href="../brand-intention-interpretation/index.php">Experience Design: Bridging Brand Intention and Brand Interpretation</a></li>
                  
                  <li><a href="../eyetracking-tasks-efficient-scanning/index.php">Scanning Patterns on the Web Are Optimized for the Current Task</a></li>
                  
                  <li><a href="../flat-design-best-practices/index.php">Flat-Design Best Practices</a></li>
                  
                  <li><a href="../ux-user-stories/index.php">Accounting for UX Work with User Stories in Agile Projects</a></li>
                  
                  <li><a href="../index.php"><strong>See all articles&hellip;</strong></a></li>
              </ul>
              
            
          </section>
          

          
          
          <section class="hide-for-small-down">
            <h1>Popular Articles</h1>
            
            <ul>
                
                <li><a href="../usability-101-introduction-to-usability/index.php">Usability 101: Introduction to Usability</a></li>
                
                <li><a href="../top-10-mistakes-web-design/index.php">Top 10 Mistakes in Web Design</a></li>
                
                <li><a href="../how-users-read-on-the-web/index.php">How Users Read on the Web</a></li>
                
                <li><a href="../f-shaped-pattern-reading-web-content/index.php">F-Shaped Pattern For Reading Web Content</a></li>
                
                <li><a href="../ten-usability-heuristics/index.php">10 Usability Heuristics for User Interface Design</a></li>
                
                <li><a href="../intranet-design-2016/index.php">10 Best Intranets of 2016</a></li>
                
                <li><a href="../which-ux-research-methods/index.php">When to Use Which User-Experience Research Methods</a></li>
                
                <li><a href="../response-times-3-important-limits/index.php">Response Times: The 3 Important Limits</a></li>
                
                <li><a href="../why-you-only-need-to-test-with-5-users/index.php">Why You Only Need to Test with 5 Users</a></li>
                
                <li><a href="../page-fold-manifesto/index.php">The Fold Manifesto: Why the Page Fold Still Matters</a></li>
                
            </ul>
          </section>
          
          

          
            <section class="hide-for-small-down">
              
            </section>
          

      </section>
      

      
        <div class="small-12 medium-9 columns content-body">
          




<article class="l-subsection alertbox-article-content article-detail-bl">
  
  <header>
    <div class="l-subsection">
      <h1>Computer-Assisted Embarrassment</h1>
      <div class="author-meta">by
        
          
            
            <a href="../author/susan-farrell/index.php">Susan Farrell</a>
            
          
        on  June 12, 2016
        
      </div>

      

      
      <div class="topic-links">
        Topics:
        
          <ul>

  <li><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction</a></li>

  <li><a href="../../topic/behavior-patterns/index.php">Behavior Patterns</a></li>

</ul>
        
      </div>
      
    </div>
  </header>
  

  
  
  <section>
    <p class="lede"><strong>Summary:</strong> Computer systems shouldn&#39;t make us feel bad. But they often do. Contextual usability methods can help discover social defects in user experience.</p>
    <br />
  </section>
  
  

  
  <section id="article-body"><p>Computer systems should <strong>not harm humans</strong> in any way — and that includes not embarrassing people or making them feel bad. Back in 1942, Isaac Asimov made this do-no-harm principle his <a href="https://en.wikipedia.org/wiki/Three_Laws_of_Robotics">First Law of Robotics</a>, but we still haven’t achieved this fundamental state of not being harmed by our computers. Despite amazing achievements in digital technologies, many systems never arrive at user delight, and many companies still release usability disasters.</p>

<p>It’s not enough to have systems that function according to specifications. And it’s no longer enough to have systems that are usable. A plethora of smart devices have invaded our world and inserted themselves in almost every context of our existence. Their flaws and faulty interactions are no longer only theirs — they reflect badly on their users and embarrass them in front of others.</p>

<p>In other words, by making smart devices ubiquitous, we’ve exposed ourselves to <strong>computer-assisted embarrassment</strong>. We must expand our usability methods to cover not only the isolated user in one context of use, but also the social user, who interacts with the system in the presence of others, and the communities of users that use systems together.</p>

<p>In the early days of computing, people were content to simply <em>run</em> programs on their computers. Usability was closely tied to functionality and understandability. As digital systems matured and became more capable, concerns for ease of use grew into the field of user-experience design, which aims to improve people’s experiences before, during, and after computer use. Attention to task completion and user satisfaction expanded into caring about many other aspects of human-product-company interactions, such as measures of quality, people’s perceptions of value, and their emotional responses to technology.</p>

<figure class="caption"><img alt="A command-line interface shows white text prompt on black background: 'Not ready reading drive A; Abort, Retry, Fail?'" height="200" src="https://media.nngroup.com/media/editor/2016/06/04/dos_abort_retry_fail.png" width="350"/>
<figcaption><em>The infamous MS-DOS error message, “Abort, Retry, Fail?” once represented the quality and limitations of our conversations with computers.</em></figcaption>
</figure>

<p>Computers and smart devices have become social actors. We talk to them, they reply, and they act on our behalf, representing us in communications and commerce. Digital assistants lurk in living rooms, hanging on every word, but they often misinterpret people. As systems get smarter, we’ll expect them to be more helpful and better behaved. We expect our software to be truthful, <a href="../commitment-levels/index.php">trustworthy</a>, and polite to us.</p>

<p>User-experience design must also be concerned with <em>social</em> user experience: the situated experiences with technologies in the context of groups of people. We need to understand how the behaviors of what we design fit or do not fit with families, work environments, school cultures, and play.</p>

<p>Dinner, open-plan offices, sporting events, and church services all require different behaviors from both our digital systems and us. Some systems fit more politely into our contexts, for example by offering settings for <em>Meeting</em> mode or <em>Do Not Disturb</em> mode that can override normal settings in predictable ways. These modes can be quite helpful if we understand their consequences, know which mode we’re in, and remember when to switch.</p>

<p>Some systems go further: for example, they predict what people want by using sensors to detect motion and using information from clocks, maps, and calendars to guess intentions or contexts of use. Some systems even learn from people’s actions.</p>

<p>Some have default settings that cause embarrassment, however, such as systems that <a href="http://techcrunch.com/2011/07/03/sexual-activity-tracked-by-fitbit-shows-up-in-google-search-results/">publicly report your most private activity as exercise</a> or automatically turn off the phone ringer just when you’re expecting a late-night call.</p>

<h2>Social Embarrassment</h2>

<p>There are countless examples of computer-assisted embarrassment. Here are just a few of the typical classes of problems. You could probably add many of your own excruciating moments to this list:</p>

<ul>
	<li><strong>Communication disasters</strong>: Messaging systems often make it easy to include unintended recipients or messages, or to send from an unintended address. Group SMS messages often end in embarrassment for everyone when messages thought private are distributed to the whole group.</li>
	<li><strong>Privacy leaks</strong>: Systems often expose, share, or don’t really delete private information (such as photos, names, and locations), sometimes without you becoming aware of the problem until much later. Wireless microphones and cameras that you think are off or muted may actually be transmitting private moments live.</li>
	<li><strong>Terrible timing</strong>: For those not using 24-hour clocks, alarms and appointments may default to 2 am instead of 2 pm. Calendars may invite people to 1-minute meetings. Unwanted sound effects can spoil quiet moments, such as volume-change sounds that intrude awkwardly into recordings and meetings. Personal messages tend to pop up during formal presentations.</li>
	<li><strong>Aggressive helpfulness</strong>: Autocorrect or other interface features may unexpectedly embarrass you in front of colleagues. <a href="http://boingboing.net/2016/05/15/algorithmic-cruelty-when-gmai.php">Algorithms that add your enemies</a> to your contacts or speed-dial lists may cause distress or even danger.</li>
	<li><strong>One-size-fits-all alerts</strong>: Apps and services often spam your social-media contacts with automated messages. Notifications may make noise on all your devices. Public alerts from 800 miles away can wake up everyone in your household in the middle of the night. Relentless, intrusive upgrade notifications sometimes won’t take “No thanks” for an answer, even when some technical or policy issue prevents you from ever saying yes.</li>
	<li><strong>Systems designed for only one gender, ethnic group, or set of abilities</strong>: Image search that <a href="http://www.cnet.com/news/google-apologizes-for-algorithm-mistakenly-calling-black-people-gorillas/">does not recognize black people,</a> voice-recognition systems that don’t work for some dialects, and devices that don’t work unless you can use both hands are just a few of the ways design has gone terribly wrong because teams design systems for people just like themselves. The first law of interface design is: <strong>You are not your user.</strong> These systems are an embarrassment to their creators, but, more important, they also cause harm and embarrassment to the people who have been left out of the design specifications.</li>
</ul>

<figure class="caption"><img alt="Three tweets from https://twitter.com/thingclash show 1. A house sitter who can’t turn on the bathroom lights without the homeowner’s detailed instructions, an app install, and a stepstool; 2. A person whose e-passport functionality relies on changing gate-technology providers; and 3. Wearables that track emotions and sexual response." height="724" src="https://media.nngroup.com/media/editor/2016/06/12/thingclash2016.png" width="600"/>
<figcaption><em><a href="https://twitter.com/thingclash">@Thingclash</a> on Twitter collects “impacts and implications of colliding technologies, systems, cultures and values around the Internet of Things.” Bathroom lights, digital passports, and wearables that track intimate personal moments all have the potential to cause embarrassment and worse.</em></figcaption>
</figure>

<figure class="caption"><img alt="Screenshot of a Salon.com article on how one William Johnson was embarrassed by an erroneous LinkedIn news update sent to his social network. May 12 2016." height="1045" src="https://media.nngroup.com/media/editor/2016/06/06/slate2016-05-linkedin_called_me_a_white_supremacist05.jpg" width="600"/>
<figcaption><em><a href="http://www.slate.com/articles/technology/technology/2016/05/linkedin_called_me_a_white_supremacist.php">LinkedIn combined a Jewish socialist’s profile photo with news of an infamous white supremacist’s political activity</a> in an automated email and sent it to the wrong guy’s social network — simply because both people had the same name.</em></figcaption>
</figure>

<p>Algorithms that are not specific or accurate enough can be a big source of embarrassment. For example, <a href="http://www.slate.com/articles/technology/technology/2016/05/linkedin_called_me_a_white_supremacist.php">LinkedIn combined a Jewish socialist’s profile photo with news of a white supremacist’s political activity</a> in an automated email announcement and sent it to the wrong guy’s social network — simply because both people had the same name. According to a Slate article about this incident, LinkedIn knew that it had a sloppy piece of software, but used it anyway, relying on readers to submit errors after the fact. The Will Johnson who wrote the Slate article wonders how many people may erroneously be declared dead using similar methods. In a professional network, extreme embarrassment can feel fatal even it only kills your employment prospects.</p>

<p><strong>Mapping out unintended and potentially harmful user consequences (and how to avoid and mitigate them) should be part of every system’s design plan. Start by asking yourselves, “What could possibly go wrong?”</strong></p>

<h2>Awkward Phone Moments</h2>

<p>Mobile phones have introduced a number of new ways for technology to embarrass us. Here are just a few examples.</p>

<p>Some phones make accidental calls, from inside your pocket or bag or when you just want to look at contact information, and they sometimes don’t hang up when you’re finished with a call.</p>

<p>When you are talking on the phone, nearby people might think you are talking to them. If that happens, you’ll have to apologize to both the person with whom you intended to talk and to those with whom you didn’t, and you’ll likely cause embarrassment for everyone involved. </p>

<p>Sometimes mobile boarding passes disappear before you can use them at the airport. Should you look for them in your email attachments, in an app, in a browser download area, or are they images in your camera app? Keeping track of where the boarding pass belongs is an <a href="../recognition-and-recall/index.php">additional memory burden</a> and increases people’s <a href="../minimize-cognitive-load/index.php">cognitive load</a>. Sometimes you have to go through the online check-in process again while at the gate, just to display the barcode in order to board the plane. No pressure. We’ll all wait patiently while you do that.</p>

<p><img alt="Screenshot from Twitter, ‘Thanks @Delta app for having my boarding pass disappear so I had to start over in the TSA line. Great way to start the day.’" height="159" src="https://media.nngroup.com/media/editor/2016/06/04/a_twitter2.png" width="350"/></p>

<p><img alt="Screenshot from Twitter, ‘Thanks for making my boarding pass disappear from your computer right before I boarded my flight @SouthwestAir Just what I needed.’" height="159" src="https://media.nngroup.com/media/editor/2016/06/04/b_twitter2.png" width="350"/></p>

<p><img alt="Screenshot from Twitter, ‘@AmericanAir The boarding pass should not disappear from the emailed URL. Do you understand my complaint and concern here?’" height="176" src="https://media.nngroup.com/media/editor/2016/06/12/c_twitter4d.png" width="350"/></p>

<p><em>Airline customer-service reps on Twitter get complaints from angry travelers with disappearing mobile-phone boarding passes.</em></p>

<p>Fortunately, airlines and travelers are learning to compensate for these issues, by providing better instructions, using images, and going back to print.</p>

<p>Phone banking or mobile voting, anyone? How about phone passports and phone driver licenses? The consequences of failure are getting more and more severe all the time.</p>

<h2>Examples of Good Contextual Usability</h2>

<p>Some systems already exhibit contextual-UX smarts that can help prevent embarrassment:</p>

<ul>
	<li>Gmail warns you if you mention an email attachment in a message but don’t attach one.</li>
	<li>Some Android phones can tell you if you’re going to arrive at a closed business.</li>
	<li>Some automobiles make it difficult to lock yourself out or to accidentally leave car doors unlocked.</li>
	<li>Some messaging systems make it simple for you to add appointments to your calendar as soon as you agree to do something.</li>
</ul>

<h2>Social Defects Cost Money, Too</h2>

<p>Increasingly, people gauge ease of use against the best designs they’ve seen: a mediocre experience seems half empty rather than half full. When a product defect causes problems, a customer may go from being a word-of-mouth advocate to a writer of long-lasting, negative reviews.</p>

<p>Whenever computer systems aren’t well behaved, they reflect poorly on those who designed them. Many of the unexpected things that can happen through interactions with our not-smart-enough systems can be deeply embarrassing or otherwise make us feel bad. Those emotional regrets may last a long time because of how people tend to chain bad experiences together in memory. Each humiliation reminds us of other times we felt that way. It’s enough to make you prefer paper and pen.</p>

<p>Companies should pay a lot more attention to the <a href="../return-on-investment-for-usability/index.php">return on investment</a> for fixing problems that users find. It’s easier to <a href="../intranet-portals-the-corporate-information-infrastructure/index.php">measure usability improvements for time-wasting intranets</a>, but poor designs also cost customers time, money, and often their dignity.</p>

<p>Don’t underestimate the costs of humiliating customers. No one is angrier than loyal customers let down by the companies and products they once loved. Such feelings of betrayal are expensive, whether you can measure them or not.</p>

<h2>How Problems Can Occur</h2>

<p>Andrew Hinton, in his book, <i>Understanding Context</i>, explains that <em><strong>context collapse</strong></em> causes a lot of these issues: activities that are fine in one context can be completely out of place in another, causing social awkwardness or worse. Permutations of digital and physical contexts create clashes in the social norms and in the expectations we have for other people, organizations, our digital systems, and ourselves.</p>

<p>It’s possible that some of these social problems appear because designers and researchers did not test systems in the right context, or they tested with the wrong people. But social defects are also difficult to find with the <a href="../which-ux-research-methods/index.php">UX methods</a> that teams normally use. Many problems likely happen because <strong>interfaces are tested in the lab</strong>, with <strong>one user at a time</strong>, and with <strong>no understanding or no realistic simulation of the actual context, scope, or scale of use</strong>. A system may be quite usable in isolation, in prototype, before it’s installed, with small amounts of data, when seated at a desk, or by particular types of users in just the right lighting; but when it’s in use at home, by groups of people, in public, or while doing normal life activities, these socially embarrassing issues appear.</p>

<figure class="caption"><img alt="Screen capture of a meteorologist gesturing at a giant Windows upgrade dialog that covers her radar map on live television. http://www.kcci.com/news/windows-10-notice-appears-during-live-tv-weathercast/39241288 April 2016" height="831" src="https://media.nngroup.com/media/editor/2016/06/06/windows_10_interrupts_a_live_tv_broadcast_with_an_unwanted_upgradebetanewscom2016060606b.jpg" width="550"/>
<figcaption><em>News Meteorologist Metinka Slater was giving a live update on thunderstorms when “Upgrade now” to Windows 10 appeared on top of her radar map. Slater quickly switched to another video source. Beta News: Television station KCCI 8, Des Moines, Iowa, USA.</em></figcaption>
</figure>

<h2>Fixing Problems After Release Can Be Difficult</h2>

<p>If you’re relying on finding and fixing embarrassing UX problems after a system has been released, think again. It’s often <strong>too difficult for users to report problems</strong>. Some companies also find it hard to track and react quickly to desperate user complaints posted in the wild.</p>

<p>Many systems <strong>cannot be changed after they are released</strong>. This ominous situation is often found in technologies we rely on and use frequently, such as phones, cars, computerized devices (internet routers, garage-door openers, entertainment systems, and other appliances) and, increasingly, in internet-of-things (IoT) ecosystems.</p>

<p>Rebooting your car is becoming common, but upgrading its software can be a complicated process. Watching your smart home become incompetent, nonfunctional, and untrustworthy is the kind of living-in-the-future that nobody wants. <strong>Designing for ease of use <em>in context and over time</em> is absolutely essential</strong>.</p>

<h2>How to Find and Fix Contextual Problems in UX Designs</h2>

<ul>
	<li><strong>Learn from well-designed systems</strong> that make people feel smarter.</li>
	<li>Do <strong>field studies</strong> to observe groups of people interacting with the system, and repeat studies over time. When <a href="../interviewing-users/index.php">interviewing</a> and surveying users, <strong>ask about their emotional reactions, including annoyances and potentially embarrassing incidents</strong>.</li>
	<li>Ask people to self-report their interactions and any accidents or surprises while using the system daily, over time — for example, by using <a href="../diary-studies/index.php">diary studies</a> and other <a href="http://www.usabilitybok.org/longitudinal-study">longitudinal study methods</a>.</li>
	<li><strong>Consider system usability in wildly different contexts</strong>, such as in countries (and companies) where the political environment is completely different than in yours.</li>
	<li>Regardless of the intended use, think about a wide range of <strong>other use and abuse possibilities</strong>.</li>
	<li>Consider how a <strong>malicious user or an untrustworthy system operator</strong> might use the system to harm others.</li>
	<li>Consider the <strong>technology ecosystems</strong> and other products that should work well with yours. What’s most likely to break or change over time?</li>
	<li><a href="../the-most-important-usability-activity/index.php">Test early and often</a> with a <strong>diverse range of people</strong> — <strong>alone and in groups</strong>.</li>
	<li><strong>Make updating easy to do</strong>. Ensure that your systems can be repaired easily over time, to mitigate unforeseen problems and keep people safer.</li>
	<li>Plan how to <strong>keep personal information secure over time</strong>. Some things don’t belong in the cloud. Often data can’t be truly anonymized, secured, or deleted, so be very careful which information is shared by default or automatically collected from the user. Don’t collect what you can’t protect.</li>
	<li><strong>Give users control of their personal information </strong>and sharing choices by making them aware of both their options and their risks.</li>
	<li>Consider whether contextual and social defects could create <strong>privacy, security, legal, and safety issues</strong>.</li>
	<li><strong>Make it easy for anyone to report usability bugs</strong>. Look at user-reported problems for clues about expectation mismatches and unintended effects.</li>
	<li><strong>Tell stories about usability problems</strong> that lay the blame where it belongs, on the system and its policies.</li>
	<li><strong>Don’t suffer in silence</strong>. Complain about systems that make you and others uncomfortable or embarrassed, whether or not you are on that design team. Companies tend to fix systems that many people are unhappy with.</li>
</ul>

<h2>Social Defects in Systems Matter</h2>

<p>Some pundits expect robots to replace half of human jobs and self-driving vehicles to take over the highways soon. In any case, the systems of the future will be more social, more powerful, and more able to cause harm. As human dependency on smart systems increases, it’s very likely that software quality and access defects will also become business liability issues, no matter what the unread disclaimer on the package says.</p>

<p>Given the unexpected longevity of software made decades ago, it’s easy to predict that some of the systems developed today might be around for a long time. <strong>If we want people to live happily with the systems we design, we need to prevent usability problems, including social defects, and plan to repair and update systems long after deployment.</strong></p>

<h2>References</h2>

<p style="margin-left:.25in;">Asimov, Isaac. <a href="https://en.wikipedia.org/wiki/Robot_series_(Asimov)"><em>Robot science fiction series</em></a>, 38 short stories and 5 novels. Various publishers worldwide, 1939–1986.</p>

<p style="margin-left:.25in;">Hinton, Andrew. <a href="http://www.contextbook.com/"><em>Understanding Context</em></a>. ISBN 1449323170. O'Reilly Media, USA, December 2014.</p>
</section>
  

  
  <section>
    
        
    
  </section>
  
  
  
  
  

  


  <section>
    <br />
    <p class="share"><b>Share <span class="hide-for-small-down">this article</span>:</b> <a href="https://twitter.com/intent/tweet?url=http://www.nngroup.com/articles/embarrassment/&amp;text=Computer-Assisted%20Embarrassment&amp;via=nngroup">Twitter</a> | <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=http://www.nngroup.com/articles/embarrassment/&amp;title=Computer-Assisted%20Embarrassment&amp;source=Nielsen%20Norman%20Group">LinkedIn</a> | <a href="https://plus.google.com/share?url=http://www.nngroup.com/articles/embarrassment/">Google+</a> | <a href="mailto:?subject=NN/g Article: Computer-Assisted Embarrassment&amp;body=http://www.nngroup.com/articles/embarrassment/">Email</a></p>
    <hr />
  </section>

  <footer class="l-subsection">
    
    
    <div class="learn-more">
      <h2>Learn More</h2>
      <p><a href="../subscribe/index.php">Subscribe to the weekly newsletter</a> to get notified about future articles.</p>
      </div>
    
    

    <div class="row show-for-small-down related-content">
      
      
      

      
      
        <div class="column small-12 topic-links">All articles about:
          <ul>
            
            <li><a href="../../topic/behavior-patterns/index.php">Behavior Patterns</a></li>
            
            <li><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction</a></li>
            
          </ul>
        </div>
      
      

      
      
        <div class="column small-12">
          <h3>UX Conference Training Courses</h3>
           <ul class="no-bullet">
    
        <li><a href="../../courses/ux-basic-training/index.php">UX Basic Training</a></li>
    
        <li><a href="../../courses/hci/index.php">User Interface Principles Every Designer Must Know</a></li>
    
        <li><a href="../../courses/web-page-design/index.php">Web Page UX Design</a></li>
    
        <li><a href="../../courses/interaction-design-3-day-course/index.php">Interaction Design: 3-Day Course</a></li>
    
        <li><a href="../../courses/emotional-design/index.php">Emotional Design</a></li>
    
</ul>
        </div>
      
      

      
      
      

      
      
      
    </div>

    <div class="hide-for-small-down row related-content">
      <div class="medium-6 columns">
        
        
          <div>
            <h3>Articles</h3>
            <ul class="no-bullet">
              
                
                <li><a href="../enhancement/index.php">The Role of Enhancement in Web Design</a></li>
                
              
                
                <li><a href="../lazy-users/index.php">Why Designers Think Users Are Lazy: 3 Human Behaviors</a></li>
                
              
                
                <li><a href="http://www.jnd.org/dn.mss/apples_products_are.php">Apple&#39;s products are getting harder to use because they ignore principles of design</a></li>
                
              
                
                <li><a href="../efficiency-vs-expectations/index.php">Don’t Prioritize Efficiency Over Expectations</a></li>
                
              
                
                <li><a href="../direct-vs-sequential-access/index.php">Direct Access vs. Sequential Access: Definition</a></li>
                
              
            </ul>
          </div>
        
        

        

      </div>
      <div class="medium-6 columns">
        
          
          <div>
            <h3>UX Conference Training Courses</h3>
             <ul class="no-bullet">
    
        <li><a href="../../courses/ux-basic-training/index.php">UX Basic Training</a></li>
    
        <li><a href="../../courses/hci/index.php">User Interface Principles Every Designer Must Know</a></li>
    
        <li><a href="../../courses/web-page-design/index.php">Web Page UX Design</a></li>
    
        <li><a href="../../courses/interaction-design-3-day-course/index.php">Interaction Design: 3-Day Course</a></li>
    
        <li><a href="../../courses/emotional-design/index.php">Emotional Design</a></li>
    
</ul>
          </div>
          
        
        
      </div>
    </div>
  </footer>
</article>

        </div>

      

    
  </div>
  <div class="row">
    
      <div class="column small-12 show-for-small-down l-navrail announcements">
        
          
        
      </div>
    
    
  </div>

    </div>
    




<footer id="page-footer">
    <div class="row hide-for-print">
        <div class="column small-12 medium-6">
            <section class="footer-popular">
                
                <h1>Popular Topics</h1>
                <div style="float: left;  width: 15em; display: block;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/applications/index.php">Application Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/e-commerce/index.php">E-Commerce Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/eyetracking/index.php">Eyetracking </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/information-architecture/index.php">Information Architecture </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/intranets/index.php">Intranet Design </a></li>
	<li><a href="../../topic/interaction-design/index.php">Interaction Design </a></li>
</ul>
</div>

<div style="float: left;  width: 15em; display: block; list-style: none;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/mobile-and-tablet-design/index.php">Mobile and Tablet Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/credibility-persuasion/index.php">Persuasive Design</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/research-methods/index.php">Research Methods</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/user-testing/index.php">User Testing </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/web-usability/index.php">Web Usability </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/writing-web/index.php">Web Writing &amp; Content Strategy </a></li>
</ul>
</div>

<p>&nbsp;</p>

                
            </section>
        </div>

        <div class="column small-12 medium-6">
            <div class="row">
                <div class="column small-6">
                    <section class="footer-about">
                        <h1>Follow Us</h1>
                        <ul>
	<li><a href="../subscribe/index.php">Subscribe to our weekly UX newsletter</a>,&nbsp;Jakob Nielsen&#39;s Alertbox</li>
	<li>Visit Don Norman&#39;s website, <a href="http://www.jnd.org/"> www.jnd.org </a></li>
	<li>Visit Bruce Tognazzini&#39;s website, <a href="http://www.asktog.com/"> www.asktog.com </a></li>
	<li style="padding-top: 6px;"><a class="twitter-follow-button" data-show-count="false" data-size="large" href="https://twitter.com/nngroup"><img alt="Follow @nngroup on Twitter" height="20" src="https://media.nngroup.com/media/editor/2016/12/30/twitter-follow-button.png" style="width: 117px; height: 20px;" width="117" /></a></li>
	<li style="padding-top: 6px;"><img alt="" height="14" src="https://media.nngroup.com/media/editor/2016/07/26/feed-icon-14x14.png" style="vertical-align:text-top;width:14px;margin-right:.25em;" width="14" /><a href="../../feed/rss/index.php">Subscribe to our RSS feed</a></li>
</ul>

                    </section>
                </div>

                <div class="column small-6">
                    <section class="footer-about right-column">
                        <h1>About
</h1>
                        <ul class="no-bullet">
	<li><a href="../../about/contact/index.php">Contact information</a></li>
	<li><a href="../../about/index.php">About Us</a></li>
	<li><a href="../../about/why-nng/index.php">Why NN/g? </a></li>
	<li><a href="../../return-policy/index.php">Return Policy</a></li>
</ul>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="copyright column small-12">
            <p><a href="../../copyright-and-reprint-info/index.php">Copyright</a> &copy; 1998-2017 Nielsen Norman Group, All Rights Reserved.</p>
        </div>
    </div>
</footer>


    


        <script src="https://media.nngroup.com/static/vendor/fastclick/js/fastclick.min.js"></script>
        <script src="https://media.nngroup.com/static/vendor/foundation/js/foundation.min.js"></script>
        <script src="http://media.nngroup.com/static/js/app.js"></script>
        <script src="http://media.nngroup.com/static/js/scroll-to-fixed.js"></script><script>var externalLinks = document.querySelectorAll('a[href^="http"]');for (var i = externalLinks.length-1; i >= 0; i--) {externalLinks[i].addEventListener("click", function(event) { alert("External links are disabled for this test."); event.preventDefault(); }, false);}var mailLinks = document.querySelectorAll('a[href^="mailto"]');for (var i = mailLinks.length-1; i >= 0; i--) {mailLinks[i].addEventListener("click", function(event) { alert("Email links are disabled for this test."); event.preventDefault(); }, false);}var externalForms = document.querySelectorAll('form');for (var i = externalForms.length-1; i >= 0; i--) {externalForms[i].addEventListener("submit", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false); externalForms[i].addEventListener("click", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false);}</script>

        <script>
            
            $(function() {
                $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
                $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
            });
        </script>

        
<script src="https://media.nngroup.com/static/js/articles.js"></script>


        
        <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1072699960;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072699960/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
    </body>

<!-- Mirrored from www.nngroup.com/articles/embarrassment/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:07:08 GMT -->
</html>
