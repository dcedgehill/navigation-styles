<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->





    
<!-- Mirrored from www.nngroup.com/articles/guerrilla-hci/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:10:41 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1026.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"beacon":"bam.nr-data.net","queueTime":0,"applicationTime":536,"agent":"","licenseKey":"1a30ef9931","errorBeacon":"bam.nr-data.net","applicationID":"2048992","transactionName":"NFxSMRBUX0EDUkIPDA0WdhAMVkVbDV8ZBxEXUFMJB0YfRAtUQRVZIktEDAFZVHYHRVcPD01eVRE="}</script>
        <title>Guerrilla HCI: Article by Jakob Nielsen</title><meta property="og:title" content="Guerrilla HCI: Article by Jakob Nielsen" />
  
        
        <meta name="description" content="Using Discount Usability Engineering to Penetrate the Intimidation Barrier,&#39; paper by Jakob Nielsen on simpler and cheaper ways to a better UI;with examples of fast usability projects.">
        <meta property="og:description" content="Using Discount Usability Engineering to Penetrate the Intimidation Barrier,&#39; paper by Jakob Nielsen on simpler and cheaper ways to a better UI;with examples of fast usability projects." />
        
  
        
	
        
        <meta name="keywords" content="discount usability engineering, system development, cost-benefit analysis, organizational maturity, usability methodology">
        

        

        <meta name="robots" content="noarchive">
        <meta name="viewport" content="width=device-width">

        <link rel="search" type="application/opensearchdescription+xml" title="NN/g Search" href="../../opensearch.xml" />
        <link rel="shortcut icon" href="http://media.nngroup.com/static/img/favicon.ico" />

        <link rel="stylesheet" href="https://media.nngroup.com/static/vendor/font-awesome/css/font-awesome.min.css">

        
        <link rel="stylesheet" href="https://media.nngroup.com/static/css/7232a960f1df.css" type="text/css" />
        <link rel="stylesheet" href="http://fast.fonts.com/t/1.css?apiType=css&amp;projectid=c5846260-9def-46ec-9407-0f1349bd2d64" type="text/css">

        <script src="https://media.nngroup.com/static/vendor/modernizr/js/modernizr.min.js"></script>

        <script type="text/javascript" src="https://media.nngroup.com/static/js/3283be5def74.js"></script>
        

        

        
        
        <!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=229034,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('http://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->


<!-- Google Analytics Tracking Code for http://nngroup.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23132681-1', 'nngroup.com');
  ga('require', 'linkid', 'https://www.nngroup.com/articles/guerrilla-hci/linkid.js');
  ga('require', 'displayfeatures');

// Optimizely Universal Analytics Integration
 window.optimizely = window.optimizely || [];
 window.optimizely.push("activateUniversalAnalytics");

  ga('send', 'pageview');

</script>
<script>
/**
* Function to track a click on an outbound link in Google Analytics.
*/
function trackOutboundLink(label, url) {
    if(window.ga && ga.loaded) {
        ga('send', 'event', 'outbound', 'click', label, {'hitCallback': function(){document.location = url;}
        });
    } else {
        document.location = url;
    }
}
/**
* Function to track a click on a link in the article announcement area.
*/
$('#articleAnnouncement a').on('click', function(){
  ga('send', 'event', 'link', 'click', 'articleAnnouncement');
});
</script>

<script type="text/javascript">
 setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2421.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



<!-- Hotjar Tracking Code for http://nngroup.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:121605,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

        <script src="https://media.nngroup.com/static/js/analytics/track_outbound_link.js"></script>
        
        
        
    </head>
    <body class="location-articles articles-detail">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
    <header id="page-header">
        <div id="cart" class="shopping-cart mobile-cart show-for-small-down"></div>
        <div class="row">
            <div id="masthead" class="masthead small-12 medium-7 column">

                <!-- The button to toggles menu on small screen -->

                <h1><a href="../../index.php"><img src="http://media.nngroup.com/static/img/logo-wordmark.png" width="74" height="30"> Nielsen <span class="norman">Norman</span> Group</a></h1>
                <p class="tagline show-for-medium-up">Evidence-Based User Experience Research, Training, and Consulting</p>

                <!-- The button to toggles form search on small screen -->


                <div style="clear: right;width:  100%;height: 1px;"></div><?php define('ROOT_PATH',$_SERVER["DOCUMENT_ROOT"] . '/apps/navigation_styles/navigations/1'); ?><?php include(ROOT_PATH)."/navigation.php"; ?>

                <!-- Drop down menu on small screen -->
                <div id="hideNav" class="show-for-small-down">
                  
                  <ul class="menu-content">
                    <li><a href="../../index.php">Home</a></li>
                    <li class="has-submenu">
                      <a>Training</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../training/index.php">Overview</a></li>
                        
<li id="nav-ux-conference"><a href="../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../online-seminars/index.php"><span>Online Seminars</span></a></li>

                      </ul>
                    </li>
                    <li class="has-submenu">
                      <a>Consulting</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
  <li><a href="../../consulting/index.php">Overview</a></li>
  
  <li><a href="../../consulting/benchmarking/index.php">Benchmarking</a></li>
  
  <li><a href="../../consulting/expert-design-review/index.php">Expert Design Review</a></li>
  
  <li><a href="../../consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
  
  <li><a href="../../consulting/usability-testing/index.php">Usability Testing </a></li>
  
  <li><a href="../../consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
  
  <li><a href="../../in-house-training/index.php">In-House Training</a></li>
</ul>

                    </li>

                    <li><a href="../../reports/index.php">Reports</a></li>
                    <li><a href="../index.php">Articles</a></li>
                    <li class="has-submenu"><a>About NN/G</a><i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../about/index.php">Overview</a></li>
                        <li><a href="../../people/index.php">People</a></li>
                        <li><a href="../../about/why-nng/index.php">Why NN/g?</a></li>
                        <li><a href="../../about/contact/index.php">Contact</a></li>
                        <li><a href="../../news/index.php">News</a></li>
                        <li><a href="../../about/history/index.php">History</a></li>
                        <li><a href="../../books/index.php">Books</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                </div>

                <hr class="header-divider show-for-small-only"/>


            </div>
       </div>

        <div id="hideNav">            <nav class="primary-nav small-12 column">
                <ul>
                    <li><a class="navitem" id="nav-home" href="../../index.php">Home</a></li>
                    <li><a class="navitem" id="nav-training" href="../../training/index.php">Training</a></li>
                    <li><a class="navitem" id="nav-consulting" href="../../consulting/index.php">Consulting</a></li>
                    <li><a class="navitem" id="nav-reports" href="../../reports/index.php">Reports</a></li>
                    <li><a class="navitem" id="nav-articles" href="../index.php">Articles</a></li>
                    <li><a class="navitem" id="nav-about" href="../../about/index.php">About NN/G</a></li>
                </ul>
            </nav>
            <nav class="secondary-nav small-12 medium-12 large-12 columns">
                
                
            </nav>
        </div>
    </header>
    <div class="l-content">
    
  

  

  <div class="row">
    
      
      <section class="small-12 medium-3 columns collapsable-content l-navrail articles-left-column">
        <h1 class="show-for-small-down collapse-link mobile-browse-header">Browse by Topic and Author</h1>
          <div class="row collapsable">

            
            <div class="small-6 medium-12 columns">
              <section id="article-popular-topics" style="display: none;">
                
                <h1>Topics</h1>
                <ul id="article-popular-topics">
                    
                    <li>
                        
                        <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/intranets/all/index.php">Intranets</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/user-testing/all/index.php">User Testing</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                        
                    </li>
                    
                    <li><a href="#" id="article-see-all-topics"><strong>See all topics&hellip;</strong></a></li>
                </ul>
                
              </section>
              <section id="article-all-topics" style="display: none;">
                
                <h1>
                  <span class="show-for-medium-up">All Article Topics</span>
                  <span class="show-for-small-down">All Topics</span>
                  (<a href="#" id="article-hide-all-topics">hide</a>)
                </h1>
                <ul>
                  
                  <li>
                    
                    <a href="../../topic/accessibility/all/index.php">Accessibility</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/agile/all/index.php">Agile</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/analytics-and-metrics/all/index.php">Analytics &amp; Metrics</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/applications/all/index.php">Application Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/b2b-websites/all/index.php">B2B Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/behavior-patterns/all/index.php">Behavior Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/branding/all/index.php">Branding</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/content-strategy/all/index.php">Content Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/corporate-websites/all/index.php">Corporate Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/design-patterns/all/index.php">Design Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-design-process/all/index.php">Design Process</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/email/all/index.php">Email</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/eyetracking/all/index.php">Eyetracking</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/heuristic-evaluation/all/index.php">Heuristic Evaluation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/human-computer-interaction/all/index.php">Human Computer Interaction</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-ideation/all/index.php">Ideation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/information-architecture/all/index.php">Information Architecture</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/interaction-design/all/index.php">Interaction Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/international-users/all/index.php">International Users</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/intranets/all/index.php">Intranets</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-management/all/index.php">Management</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/navigation/all/index.php">Navigation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/non-profit-websites/all/index.php">Non-Profit Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/personas/all/index.php">Personas</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/persuasive-design/all/index.php">Persuasive Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/prototyping/all/index.php">Prototyping</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/psychology-and-ux/all/index.php">Psychology and UX</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/research-methods/all/index.php">Research Methods</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/search/all/index.php">Search</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/social-media/all/index.php">Social Media</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/strategy/all/index.php">Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/user-testing/all/index.php">User Testing</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-teams/all/index.php">UX Teams</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/visual-design/all/index.php">Visual Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/writing-web/all/index.php">Writing for the Web</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/young-users/all/index.php">Young Users</a>
                    
                  </li>
                  
                </ul>
                
              </section>
            </div>
            <script>
              /* Determine which topics list will be displayed: the full or the
               * partial.  We are inlining this close to the list itself to
               * prevent rendering delays on the client
               */
              var topic_list = $.cookie("article-topics");

              var popular_topics = $('#article-popular-topics');
              var all_topics = $('#article-all-topics');

              if(topic_list == 1) {
                  popular_topics.hide();
                  all_topics.show();
              } else {
                  popular_topics.show();
                  all_topics.hide();
              }
            </script>
            

            
            <div class="small-6 medium-12 columns">
              
              <section id="article-initial-authors">
                  <h1>Author</h1>
                  <ul>
                    
                    <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                    
                    <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                    
                    <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                    
                    <li><a href="#" onClick="$('#article-initial-authors').hide(); $('#article-all-authors').show();"><strong>See all authors&hellip;</strong></a></li>
                  </ul>
              </section>
              <section id="article-all-authors" style="display: none;">
                <h1>
                  All Authors
                  (<a href="#" id="article-hide-all-authors">hide</a>)
                </h1>
                <ul>
                  
                  <li><a href="../author/raluca-budiu/index.php">Raluca Budiu</a></li>
                  
                  <li><a href="../author/patty-caya/index.php">Patty Caya</a></li>
                  
                  <li><a href="../author/susan-farrell/index.php">Susan Farrell</a></li>
                  
                  <li><a href="../author/therese-fessenden/index.php">Therese Fessenden</a></li>
                  
                  <li><a href="../author/kim-flaherty/index.php">Kim Flaherty</a></li>
                  
                  <li><a href="../author/sarah-gibbons/index.php">Sarah Gibbons</a></li>
                  
                  <li><a href="../author/aurora-harley/index.php">Aurora Harley</a></li>
                  
                  <li><a href="../author/kate-kaplan/index.php">Kate Kaplan</a></li>
                  
                  <li><a href="../author/page-laubheimer/index.php">Page Laubheimer</a></li>
                  
                  <li><a href="../author/angela-li/index.php">Angie Li</a></li>
                  
                  <li><a href="../author/hoa-loranger/index.php">Hoa Loranger</a></li>
                  
                  <li><a href="../author/kate-meyer/index.php">Kate Meyer</a></li>
                  
                  <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                  
                  <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                  
                  <li><a href="../author/kara-pernice/index.php">Kara Pernice</a></li>
                  
                  <li><a href="../author/christian-rohrer/index.php">Christian Rohrer</a></li>
                  
                  <li><a href="../author/amy-schade/index.php">Amy Schade</a></li>
                  
                  <li><a href="../author/katie-sherwin/index.php">Katie Sherwin</a></li>
                  
                  <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                  
                  <li><a href="../author/kathryn-whitenton/index.php">Kathryn Whitenton</a></li>
                  
                </ul>
              </section>
              
            </div>
            
          </div>

          
          <section class="hide-for-small-down">
            
              
              <h1>Recent Articles</h1>
              
              <ul>
                  
                  <li><a href="../seamless-cross-channel/index.php">Seamlessness in the Omnichannel User Experience</a></li>
                  
                  <li><a href="../brand-intention-interpretation/index.php">Experience Design: Bridging Brand Intention and Brand Interpretation</a></li>
                  
                  <li><a href="../eyetracking-tasks-efficient-scanning/index.php">Scanning Patterns on the Web Are Optimized for the Current Task</a></li>
                  
                  <li><a href="../flat-design-best-practices/index.php">Flat-Design Best Practices</a></li>
                  
                  <li><a href="../ux-user-stories/index.php">Accounting for UX Work with User Stories in Agile Projects</a></li>
                  
                  <li><a href="../index.php"><strong>See all articles&hellip;</strong></a></li>
              </ul>
              
            
          </section>
          

          
          
          <section class="hide-for-small-down">
            <h1>Popular Articles</h1>
            
            <ul>
                
                <li><a href="../usability-101-introduction-to-usability/index.php">Usability 101: Introduction to Usability</a></li>
                
                <li><a href="../top-10-mistakes-web-design/index.php">Top 10 Mistakes in Web Design</a></li>
                
                <li><a href="../how-users-read-on-the-web/index.php">How Users Read on the Web</a></li>
                
                <li><a href="../f-shaped-pattern-reading-web-content/index.php">F-Shaped Pattern For Reading Web Content</a></li>
                
                <li><a href="../ten-usability-heuristics/index.php">10 Usability Heuristics for User Interface Design</a></li>
                
                <li><a href="../intranet-design-2016/index.php">10 Best Intranets of 2016</a></li>
                
                <li><a href="../which-ux-research-methods/index.php">When to Use Which User-Experience Research Methods</a></li>
                
                <li><a href="../response-times-3-important-limits/index.php">Response Times: The 3 Important Limits</a></li>
                
                <li><a href="../why-you-only-need-to-test-with-5-users/index.php">Why You Only Need to Test with 5 Users</a></li>
                
                <li><a href="../page-fold-manifesto/index.php">The Fold Manifesto: Why the Page Fold Still Matters</a></li>
                
            </ul>
          </section>
          
          

          
            <section class="hide-for-small-down">
              
            </section>
          

      </section>
      

      
        <div class="small-12 medium-9 columns content-body">
          




<article class="l-subsection alertbox-article-content article-detail-bl">
  
  <header>
    <div class="l-subsection">
      <h1>Guerrilla HCI: Using Discount Usability Engineering to Penetrate the Intimidation Barrier</h1>
      <div class="author-meta">by
        
          
            
            <a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a>
            
          
        on  January 1, 1994
        
      </div>

      

      
      <div class="topic-links">
        Topics:
        
          <ul>

  <li><a href="../../topic/prototyping/index.php">Prototyping</a></li>

  <li><a href="../../topic/strategy/index.php">Strategy</a></li>

  <li><a href="../../topic/user-testing/index.php">User Testing</a></li>

</ul>
        
      </div>
      
    </div>
  </header>
  

  
  
  

  
  <section id="article-body"><p>One of the oldest jokes in computer science goes as follows:</p>

<blockquote><strong>Q: </strong> <em> How many programmers does it take to change a light bulb? </em><br/>
<strong>A: </strong> <em> None; it is a hardware problem! </em></blockquote>

<p>When asking how many usability specialists it takes to change a light bulb, the answer might well be four: Two to conduct a field study and task analysis to determine whether people really need light, one to observe the user who actually screws in the light bulb, and one to control the video camera filming the event. It is certainly true that one should study user needs before implementing supposed solutions to those problems. Even so, the perception that anybody touching usability will come down with a bad case of budget overruns is keeping many software projects from achieving the level of usability their users deserve.</p>

<h2>1 The Intimidation Barrier</h2>

<p>It is well known that people rarely use the recommended usability engineering methods [Nielsen 1993; Whiteside et al. 1988] on software development projects in real life. This includes even such basic usability engineering techniques as early focus on the user, empirical measurement, and iterative design which are used by very few companies. Gould and Lewis [1985] found that only 16% of developers mentioned all three principles when asked what one should do when developing and evaluating a new computer system for end users. Twenty-six percent of developers did not mention a single of these extremely basic principles. A more recent study found that only 21% of Danish software developers knew about the thinking aloud method and that only 6% actually used it [Milsted et al. 1989]. More advanced usability methods were not used at all.</p>

<p>One important reason usability engineering is not used in practice is the cost of using the techniques. Or rather, the reason is the perceived cost of using these techniques, as this chapter will show that many usability techniques can be used quite cheaply. It should be no surprise, however, that practitioners view usability methods as expensive considering, for example, that a paper in the widely read and very respected journal Communications of the ACM estimated that the "costs required to add human factors elements to the development of software" was $128,330 [Mantei and Teorey 1988]. This sum is several times the total budget for usability in most smaller companies, and one interface evangelist has actually found it necessary to warn such small companies against believing the CACM estimate [Tognazzini 1990]. Otherwise, the result could easily be that a project manager would discard any attempt at usability engineering in the belief that the project's budget could not bear the cost. Table 1 shows the result of adjusting a usability budget according to the discount usability engineering method discussed below. The numbers in Table 1 are for a medium scale software project (about 32,000 lines of code). For small projects, even cheaper methods can be used, while really large projects might consider additional funds to usability and the full-blown traditional methodology, though even large projects can benefit considerably from using discount usability engineering.</p>

<table border="" cols="2">
	<caption align="bottom"><strong>Table 1 </strong><br/>
	Cost savings in a medium scale software project by using the discount usability engineering method instead of the more thorough usability methods sometimes recommended.</caption>
	<tbody>
		<tr>
			<td>Original usability cost estimate by [Mantei and Teorey 1988]</td>
			<td align="right">$128,330</td>
		</tr>
		<tr>
			<td>Scenario developed as paper mockup instead of on videotape</td>
			<td align="right">- $2,160</td>
		</tr>
		<tr>
			<td>Prototyping done with free hypertext package</td>
			<td align="right">- $16,000</td>
		</tr>
		<tr>
			<td>All user testing done with 3 subjects instead of 5</td>
			<td align="right">- $11,520</td>
		</tr>
		<tr>
			<td>Thinking aloud studies analyzed by taking notes instead of by video taping</td>
			<td align="right">- $5,520</td>
		</tr>
		<tr>
			<td>Special video laboratory not needed</td>
			<td align="right">- $17,600</td>
		</tr>
		<tr>
			<td>Only 2 focus groups instead of 3 for market research</td>
			<td align="right">- $2,000</td>
		</tr>
		<tr>
			<td>Only 1 focus group instead of 3 for accept analysis</td>
			<td align="right">- $4,000</td>
		</tr>
		<tr>
			<td>Questionnaires only used in feedback phase, not after prototype testing</td>
			<td align="right">- $7,200</td>
		</tr>
		<tr>
			<td>Usability expert brought in for heuristic evaluation</td>
			<td align="right">+ $3,000</td>
		</tr>
		<tr class="summaryrow">
			<td>Cost for "discount usability engineering" project</td>
			<td align="right">$65,330</td>
		</tr>
	</tbody>
</table>

<p>British studies [Bellotti 1988] indicate that many developers don't use usability engineering because HCI (human-computer interaction) methods are seen as too time consuming and expensive and because the techniques are often intimidating in their complexity. The "discount usability engineering" approach is intended to address these two issues. Further reasons given by Bellotti were that there were sometimes no perceived need for HCI and a lack of awareness about appropriate techniques. These two other problems must be addressed by education [Perlman 1988, 1990; Nielsen and Molich 1989] and propaganda [Nielsen 1990a], but even for that purpose, simpler usability methods should help. Also, time itself is on the side of increasing the perceived need for HCI since the software market seems to be shifting away from the "features war" of earlier years [Telles 1990]. Now, most software products have more features than users will ever need or learn, and Telles [1990] states that the "interface has become an important element in garnering good reviews" of software in the trade press.</p>

<p>As an example of "intimidating complexity," consider the paper by Karwowski et al. [1989] on extending the GOMS model [Card et al. 1983] with fuzzy logic. Note that I am not complaining that doing so is bad research. On the contrary, I find it very exciting to develop methods to extend models like GOMS to deal better with real-world circumstances like uncertainty and user errors. Unfortunately, the fuzzy logic GOMS and similar work can easily lead to intimidation when software people without in-depth knowledge of the HCI field read the papers. These readers may well believe that such methods represent "the way" to do usability engineering even though usability specialists would know that the research represents exploratory probes to extend the field and should only serve as, say, the fifth or so method one would use on a project. There are many simpler methods one should use first [Nielsen 1992a, 1993].</p>

<p>I certainly can be guilty of intimidating behavior too. For example, together with Marco Bergman, I recently completed a research project on iterative design where we employed a total of 99 subjects to test various versions of a user interface at a total estimated cost of $62,786. People reading papers reporting on this and similar studies might be excused if they think that iterative design and user testing are expensive and overly elaborate procedures. In fact, of course, it is possible to use considerably fewer subjects and get by with much cheaper methods, and we took care to say so explicitly in our paper. A basic problem is that with a few exceptions, published descriptions of usability work normally describe cases where considerable extra efforts were expended on deriving publication-quality results, even though most development needs can be met in much simpler ways.</p>

<p>As one example, consider the issue of statistical significance. I recently had a meeting to discuss usability engineering with the head of computer science for one of the world's most famous laboratories, and when discussing the needed number of subjects for various tests, he immediately referred to the need for test results to be statistically significant to be worth collecting. Certainly, for much research, you need to have a high degree of confidence that your claimed findings are not just due to chance. For the development of usable interfaces, however, one can often be satisfied by less rigorous tests.</p>

<p>Statistical significance is basically an indication of the probability that one is not making the wrong conclusion (e.g., a claim that a certain result is significant at the p&lt;.05 level indicates that there is a 5% probability that it is false). Consider the problem of choosing between two alternative interface designs [Landauer 1988]. If no information is available, you might as well choose by tossing a coin, and you will have a 50% probability of choosing the best interface. If a small amount of user testing has been done, you may find that interface A is better than interface B at the 20% level of significance. Even though 20% is considered "not significant," your tests have actually improved your chance of choosing the best interface from 50/50 to 4-to-1, meaning that you would be foolish not to take the data into account when choosing. Furthermore, even though there remains a 20% probability that interface A is not better than interface B, it is very unlikely that it would be much worse than interface B. Most of the 20% accounts for cases where the two interfaces are equal or where B is slightly better than A, meaning that it would almost never be a really bad decision to choose interface A. In other words, even tests that are not statistically significant are well worth doing since they will improve the quality of decisions substantially.</p>

<h2>2 The Discount Usability Engineering Approach</h2>

<p>Usability specialists will often propose using the best possible methodology. Indeed, this is what they have been trained to do in most universities. Unfortunately, it seems that "le mieux est l'ennemi du bien" (the best is the enemy of the good) [Voltaire 1764] to the extent that insisting on using only the best methods may result in having no methods used at all. Therefore, I will focus on achieving "the good" with respect to having some usability engineering work performed, even though the methods needed to achieve this result are definitely not "the best" method and will not give perfect results.</p>

<p>It will be easy for the knowledgable reader to put down the methods proposed here with various well-known counter-examples showing important usability aspects that will be missed under certain circumstances. Some of these counter-examples are no doubt true and I do agree that better results can be achieved by applying more careful methodologies. But remember that such more careful methods are also more expensive -- often in terms of money, and always in terms of required expertise (leading to the intimidation factor discussed above). Therefore, the simpler methods stand a much better chance of actually being used in practical design situations and they should therefore be viewed as a way of serving the user community.</p>

<p>The "discount usability engineering" [Nielsen 1989b, 1990a, 1993] method is based on the use of the following three techniques:</p>

<ul>
	<li>Scenarios</li>
	<li>Simplified thinking aloud</li>
	<li>Heuristic evaluation</li>
</ul>

<p>Additionally, the basic principle of early focus on users should of course be followed. It can be achieved in various ways, including simple visits to customer locations.</p>

<h3>2.1 Scenarios</h3>

<p>Scenarios are a special kind of prototyping as shown in Figure 1. The entire idea behind prototyping is to cut down on the complexity of implementation by eliminating parts of the full system. Horizontal prototypes reduce the level of functionality and result in a user interface surface layer, while vertical prototypes reduce the number of features and implement the full functionality of those chosen (i.e. we get a part of the system to play with).</p>

<table width="100%">
	<caption align="bottom"><strong>Figure 1 </strong><br/>
	The concept of a <em> scenario </em> compared to vertical and horizontal prototypes as ways to make rapid prototyping simpler.</caption>
	<tbody>
		<tr>
			<td align="center"><img alt="Scenarios are prototypes that have been limited both horizontally and vertically" src="http://media.nngroup.com/media/editor/2012/12/10/guerrilla_scenario_fig.gif" style="width: 423px; height: 272px;"/></td>
		</tr>
	</tbody>
</table>

<p>Scenarios take prototyping to the extreme by reducing both the level of functionality and the number of features. By reducing the part of interface being considered to the minimum, a scenario can be very cheap to design and implement, but it is only able to simulate the user interface as long as a test user follows a previously planned path.</p>

<p>Since the scenario is small, we can afford to change it frequently, and if we use cheap, small thinking aloud studies, we can also afford to test each of the versions. Therefore scenarios are a way of getting quick and frequent feedback from users.</p>

<p>Scenarios can be implemented as <a class="new" href="../../reports/paper-prototyping-training-video/index.php" title="Nielsen Norman Group: Paper Prototyping, a How-To Video (32 minute film)"> paper mock-ups</a> [Nielsen 1990b] or in simple prototyping environments [Nielsen 1989a] that may be easier to learn than more advanced programming environments [Nielsen et al. 1991]. This is an additional savings compared to more complex prototypes requiring the use of advanced software tools.</p>

<h3>2.2 Simplified Thinking Aloud</h3>

<p>Traditionally, thinking aloud studies are conducted with psychologists or user interface experts as experimenters who videotape the subjects and perform detailed protocol analysis. This kind of method certainly may seem intimidating for ordinary developers. However, it is possible to run user tests without sophisticated labs, simply by bringing in some real users, giving them some typical test tasks, and asking them to think out loud while they perform the tasks. Those developers who have used the thinking aloud method are happy about it [Jørgensen 1989, Monk et al. 1993], and my studies [Nielsen 1992b] show that computer scientists are indeed able to apply the thinking aloud method effectively to evaluate user interfaces with a minimum of training, and that even fairly methodologically primitive experiments will succeed in finding many usability problems.</p>

<p>I have long claimed that one learns the most from the first few test users, based on several case studies. In earlier papers, I have usually recommended using between three and five test users per test as a way of simplifying user testing while gaining almost the same benefits as one would get from more elaborate tests with large numbers of subjects. Recently, Tom Landauer and I developed a mathematical model of the number of usability problems [Nielsen and Landauer 1993], and when plugging in typical budget figures from different kinds of user testing, we derived curves like the ones shown in Figure 2 for the ratio between the benefits of user testing and the cost of the test for medium-sized development projects. The curves basically show that the benefits from user testing are much larger than the costs, no matter how many subjects are used. The maximum benefit-cost ratio is achieved when using between three and five subjects, confirming my earlier experience.</p>

<table width="100%">
	<caption align="bottom"><strong>Figure 2 </strong><br/>
	Cost-benefit trade-off curve for a "typical" project, varying the number of test users, using the model and average parameters described by Nielsen and Landauer [1993]. The curve shows the ratio of benefits to costs, that is, how many times the benefits are larger than the costs. For example, a benefit-to-cost ratio of 50 might correspond to costs of $10,000 and benefits of $500,000.</caption>
	<tbody>
		<tr>
			<td align="center"><img alt="Cost-Benefit Curve" src="http://media.nngroup.com/media/editor/2012/12/10/guerrilla_benefit_cost.gif" style="width: 474px; height: 315px;"/></td>
		</tr>
	</tbody>
</table>

<p>Besides reducing the number of subjects, another major difference between simplified and traditional thinking aloud is that data analysis can be done on the basis of the notes taken by the experimenter instead of by videotapes. Recording, watching, and analyzing the videotapes is expensive and takes a lot of time which is better spent on running more subjects and on testing more iterations of redesigned user interfaces. Video taping should only be done in those cases (such as research studies) where absolute certainty is needed. In discount usability engineering we don't aim at perfection anyway, we just want to find most of the usability problems, and a survey of 11 software engineers [Perlman 1988] found that they rated simple tests of prototypes as almost twice as useful as video protocols.</p>

<h3>2.3 Heuristic Evaluation</h3>

<p>Current user interface standards and collections of usability guidelines typically have on the order of one thousand rules to follow and are therefore seen as intimidating by developers. For the discount method I advocate cutting the complexity by two orders of magnitudes and instead rely on a small set of heuristics such as the <a href="../ten-usability-heuristics/index.php"> ten basic usability principles</a> (listed on a separate page).</p>

<p>These principles can be presented in a single lecture and can be used to explain a very large proportion of the problems one observes in user interface designs. Unfortunately it does require some experience with the principles to apply them sufficiently thoroughly [Nielsen 1992c], so it might be necessary to spend some money on getting outside usability consultants to help with a heuristic evaluation. On the other hand, even non-experts can find many usability problems by heuristic evaluation and many of the remaining problems would be revealed by the simplified thinking aloud test. It can also be recommended to let several different people <a href="../../topic/heuristic-evaluation/index.php"> perform a heuristic evaluation</a> as different people locate different usability problems [Nielsen and Molich 1990]. This is another reason why even discount usability engineers might consider setting aside a part of their budget for outside usability consultants.</p>

<h2>3 Validating Discount Usability Engineering</h2>

<p>In one case, I used the discount usability engineering method to redesign a set of account statements [Nielsen 1989b]. I tested eight different versions (the original design plus seven redesigns) before I was satisfied. Even so, the entire project required only about 90 hours, including designing seven versions of twelve different kinds of statements (not all the forms were changed in each iteration, however) and testing them in simplified thinking aloud experiments. Most versions were tested with just a single user. To validate the redesign, a further experiment was done using traditional statistical measurement methods. It should be stressed that this validation was a research exercise and not part of the discount usability engineering method itself: The usability engineering work ended with the development of the improved account statements, but as a check of the usability engineering methods used, it was decided to conduct a usability measurement of one of the new designs compared with the original design.</p>

<h3>3.1 Experiment 1: Double Blind Test Taking Usability Measurements</h3>

<p>The validation was done using a double blind test: 38 experimenters each ran four subjects (for a total of 152 subjects) in a between-subjects design. Neither the experimenters nor the subjects knew which was the original account statement and which was the new. The results which are reported in Table 3 show clear and highly statistically significant improvements in measurement values for the new statement with respect to the understandability of the information in the statement as measured by the average number of correct answers to four questions concerning the contents of the statement. The value had indeed been the usability parameter which had been monitored as a goal during the iterative design. Two other usability parameters which had not been considered goals in the iterative design process (efficiency of use and subjective satisfaction) were also measured in the final test, and the two versions of the statement got practically identical scores on those.</p>

<table border="1" cols="4" width="90%">
	<caption align="bottom"><strong>Table 3 </strong><br/>
	Result of Experiment 1: a double blind test (N=152) comparing the original and the revised version of a bank account statement. The values measured are: How many of the subjects could correctly answer each of four questions about the contents of the statement (and the combined average for those four questions), the average time needed by subjects to review the statement and answer the questions, and the subjects' average subjective rating (scale: 1 [bad] to 5 [good]).<br/>
	The rightmost column indicates whether the difference between the two account statements is statistically significant according to a <em> t </em> -test.</caption>
	<tbody>
		<tr>
			<td> </td>
			<th>Original design</th>
			<th>Revised design</th>
			<th>Significance of Difference</th>
		</tr>
		<tr>
			<td>"Size of deposit"</td>
			<td align="center">79%</td>
			<td align="center">95%</td>
			<td align="center"><em>p </em> &lt;.01</td>
		</tr>
		<tr>
			<td>"Commission"</td>
			<td align="center">34%</td>
			<td align="center">53%</td>
			<td align="center"><em>p </em> &lt;.05</td>
		</tr>
		<tr>
			<td>"Interest rates"</td>
			<td align="center">20%</td>
			<td align="center">58%</td>
			<td align="center"><em>p </em> &lt;.01</td>
		</tr>
		<tr>
			<td>"Credit limit"</td>
			<td align="center">93%</td>
			<td align="center">99%</td>
			<td align="center"><em>p </em> &lt;.05</td>
		</tr>
		<tr>
			<td>Average correct</td>
			<td align="center">56%</td>
			<td align="center">76%</td>
			<td align="center"><em>p </em> &lt;.01</td>
		</tr>
		<tr>
			<td>Task time (sec.)</td>
			<td align="center">315</td>
			<td align="center">303</td>
			<td align="center">n.s. ( <em> p </em> =.58)</td>
		</tr>
		<tr>
			<td>Subjective satisfaction [1-5 scale]</td>
			<td align="center">2.8</td>
			<td align="center">3.0</td>
			<td align="center">n.s. ( <em> p </em> =.14)</td>
		</tr>
	</tbody>
</table>

<p>This study supports the use of discount usability engineering techniques and shows that they can indeed cause measurable improvements in usability. However, the results also indicate that one should be cautious in setting the goals for usability engineering work. Those usability parameters that have no goals set for improvement risk being left behind as the attention of the usability engineer is concentrated on the official goals. In this study, no negative effects in the form of actual degradation in measured usability parameters were observed but one can not always count on being so lucky.</p>

<h3>3.2 Experiment 2: Recommendations from People without Usability Expertise</h3>

<p>Two groups of evaluators were shown the two versions of the account statement (without being told which one was the revised version) and asked which one they would recommend management to use. All the evaluators were computer science students who had signed up for a user interface design course but who had not yet been taught anything in the course. This meant that they did not know the <a href="../ten-usability-heuristics/index.php" title="List of basic usability principles for heuristic evaluation"> usability heuristics</a> which they might otherwise have used to evaluate the two versions.</p>

<p>Group A consisted of the experimenters from Experiment 1 (reported above) who had run two short experiments with each version of the account statement, while the evaluators in Group B had to make their recommendation on the basis of their own personal evaluation of the two versions. The results are reported in Table 4 and show a significant difference in the recommendations: Evaluators in Group A preferred the revised version 4 to 1 while evaluators in Group B were split equally between the two versions. This latter result is probably a reflection of the fact that the two versions are almost equally subjectively satisfying according to the measurement results reported in Table 3.</p>

<table border="1" width="90%">
	<caption align="bottom"><strong>Table 4 </strong><br/>
	Result of Experiment 2: asking two group of evaluators to recommend one of the two versions of an account statement. In Group A, each person had first run an empirical test with four subjects, whereas the evaluators in Group B had no basis for their recommendation except their own subjective evaluation.<br/>
	The difference between the two groups is statistically different at the <em> p </em> &lt;.05 level.</caption>
	<tbody>
		<tr align="center">
			<td> </td>
			<th>Group A</th>
			<th>Group B</th>
		</tr>
		<tr align="center">
			<td> </td>
			<td>N=38</td>
			<td>N=21</td>
		</tr>
		<tr>
			<td>Recommends original</td>
			<td align="center">16%</td>
			<td align="center">48%</td>
		</tr>
		<tr>
			<td>Recommends revised</td>
			<td align="center">68%</td>
			<td align="center">48%</td>
		</tr>
		<tr>
			<td>No recommendation</td>
			<td align="center">16%</td>
			<td align="center">5%</td>
		</tr>
	</tbody>
</table>

<p>If we accept the statistical measurement results in Table 3 as defining the revised version as the "best," we see that Group A was dramatically better at making the correct recommendation than Group B was. This was in spite of the fact that each of the individuals in Group A had knowledge only of the experimental results from two subjects for each of the designs (the aggregate statistics were not calculated until after the recommendations had been made, so each evaluator knew only the results from the four subjects run by that individual).</p>

<p>So we can conclude that running even a small, cheap empirical study can help non-human factors people significantly in their evaluation of user interfaces. If we count the evaluators who did not make a recommendation as having a 50/50 chance of picking the right interface, this experiment shows that running just two subjects for each version in a small test improved the probability for recommending the best of two versions from 50% to 76%.</p>

<h2>4 Cost-Benefit Analysis of Heuristic Evaluation: A Case Study</h2>

<p>A cost-benefit analysis of heuristic evaluation includes two main elements: First estimating the costs in terms of time spent performing the evaluation, and second estimating the benefits in terms of increased usability (less the development costs for the redesign). Since these estimates involve some uncertainties, they will be converted into dollar amounts by using round numbers. Any given company will of course have slightly different conversion factors, depending on its exact financial circumstances.</p>

<p>The following case study regards a prototype user interface for a system for internal telephone company use which will be called the Integrating System in this chapter. The Integrating System is fairly complicated and understanding its details requires extensive knowledge of telephone company concepts, procedures, and databases. Since a detailed explanation is not necessary to understand the generally applicable lessons from the study, the Integrating System will only be outlined here.</p>

<p>Briefly, the Integrating System provides a graphical user interface to access information from several systems running on various remote computers in a uniform manner despite the differences between the backend systems. The Integrating System can be used to resolve certain problems when data inconsistencies require manual intervention by a technician because the computer systems cannot determine which information is correct. The traditional method for resolving these problems involves having the technician compare information across several of these databases by accessing them through a number of traditional alphanumeric terminal sessions. The databases reside on different computers and have different data formats and user interface designs, so this traditional method is somewhat awkward and requires the technicians to learn a large number of inconsistent user interfaces.</p>

<p>Performing this task involves a large amount of highly domain-specific knowledge about the way the telephone system is constructed and the structure of the different databases. Technicians need to know where to look for what data and how the different kinds of data are related. Also, the individual data items themselves are extremely obscure for people without detailed domain knowledge.</p>

<p>As a result of the heuristic evaluation of this interface with 11 evaluators (described in further detail in [Nielsen 1994b]), 44 usability problems were found. Forty of these problems are denoted "core" usability problems and were found in the part of the interface that was subjected to intensive evaluation, whereas the remaining four problems were discovered in parts of the interface that we had not planned to study as part of the heuristic evaluation.</p>

<h3>4.1 Time Expenditure</h3>

<p>As usual in usability engineering, the cost estimates are the easiest to get right. Table 5 accounts for the total time spent on the heuristic evaluation project in terms of person-hours. No attempt has been made to distinguish between different categories of professional staff. Practically all the person-hours listed in Table 5 were spent by usability specialists. The only exception is a small number of hours spent by development specialists in getting the prototype ready for the evaluation and in attending the debriefing session.</p>

<table border="1" cols="2">
	<caption align="bottom"><strong>Table 5 </strong><br/>
	Estimate of the total number of person-hours spent on the heuristic evaluation study described in this article. The estimate of "time to prepare the prototype" does not include the time needed for the initial task analysis, user interface design, or implementation of the prototype since these activities had already been undertaken independently of the heuristic evaluation.</caption>
	<tbody>
		<tr>
			<td>Assessing appropriate ways to use heuristic evaluation, 4 people @ 2 hours</td>
			<td align="right">8</td>
		</tr>
		<tr>
			<td>Having outside evaluation expert learn about the domain and scenario</td>
			<td align="right">8</td>
		</tr>
		<tr>
			<td>Finding and scheduling evaluators, 1.8 hours + 0.2 hours per evaluator</td>
			<td align="right">4</td>
		</tr>
		<tr>
			<td>Preparing the briefing</td>
			<td align="right">3</td>
		</tr>
		<tr>
			<td>Preparing scenario for the evaluators</td>
			<td align="right">2</td>
		</tr>
		<tr>
			<td>Briefing, 1 system expert, 1 evaluation expert, 11 evaluators @ 1.5 hours</td>
			<td align="right">19.5</td>
		</tr>
		<tr>
			<td>Preparing the prototype (software and its hardware platform) for the evaluation</td>
			<td align="right">5</td>
		</tr>
		<tr>
			<td>Actual evaluation, 11 evaluators @ 1 hour</td>
			<td align="right">11</td>
		</tr>
		<tr>
			<td>Observing the evaluation sessions, 2 observers @ 11 hours</td>
			<td align="right">22</td>
		</tr>
		<tr>
			<td>Debriefing, 3 evaluators, 3 developers, 1 evaluation expert @ 1 hour</td>
			<td align="right">7</td>
		</tr>
		<tr>
			<td>Writing list of usability problems based on notes from evaluation sessions</td>
			<td align="right">2</td>
		</tr>
		<tr>
			<td>Writing problem descriptions for use in severity-rating questionnaire</td>
			<td align="right">6</td>
		</tr>
		<tr>
			<td>Severity rating, 11 evaluators @ 0.5 hours</td>
			<td align="right">5.5</td>
		</tr>
		<tr>
			<td>Analyzing severity ratings</td>
			<td align="right">2</td>
		</tr>
		<tr class="summaryrow">
			<td>Total</td>
			<td align="right">105</td>
		</tr>
	</tbody>
</table>

<p>Note that the time given for the preparation of the scenario covers only the effort of writing up the scenario in a form that would be usable by the evaluators during the evaluation. Considerable additional effort was needed to specify the scenario in the first place, but that effort was part of the general task analysis and design activities performed before the evaluation. Scenario-based design is a well-known method for user interface design [Carroll and Rosson 1990, Clarke 1991], so one will often be able to draw upon interaction scenarios that have been developed in previous stages of the usability lifecycle. Even so, we were probably lucky that the scenario developed for the present system could be used for the evaluation with such a small amount of additional effort.</p>

<p>The evaluation sessions were videotaped, and approximately eight hours were spent on mundane tasks like getting videotapes, learning to operate the video equipment in the specific usability laboratory used for the evaluation sessions, setting up and closing down the video equipment on each of the two days of the study, rewinding tapes, etc. This videotaping was not part of the heuristic evaluation as such, and the tapes were not reviewed for the purpose of arriving at the list of usability problems. The observers' notes were sufficient for that purpose. The videotapes were used to some extent in this research analysis of the study where an additional eight hours were spent reviewing details of some evaluation sessions, but since this use was not part of the practical application of the heuristic evaluation method, the time spent on the videotapes has not been included in Table 5.</p>

<p>It follows from Table 5 that the total number of person-hours spent on the evaluation can be determined by the formula</p>

<table align="center">
	<tbody>
		<tr>
			<td align="center">Equation 1:<br/>
			time( <em> i </em> ) = 47.8 + 5.2 <em> i </em></td>
		</tr>
	</tbody>
</table>

<p>where <em> i </em> is the number of evaluators. This formula is not exact for large values of <em> i </em> , since some of the effort devoted to room scheduling and to the analysis of the severity ratings is partly dependent on the number of evaluators and would change with large <em> i </em> s.</p>

<p>The cost estimate in (Equation 1) is probably larger than necessary for future heuristic evaluations. Major reductions in both the fixed and variable costs could be achieved by reducing the team of two observers to a single observer. This observer should be the person who is familiar with the application such that the observer can answer questions from the evaluators during the evaluation. Also, even though the observer should have a certain level of usability knowledge in order to understand the comments made by the evaluators, the observer need not be a highly skilled expert specializing in usability. A major difference between heuristic evaluation and traditional user testing is that an observer of a heuristic evaluation session is mostly freed from having to interpret user actions since the evaluators are assuming the task of explicitly identifying the usability problems. In contrast, the experimenter in a traditional user test would need a higher level of usability expertise in order to translate the subject's actions and difficulties into interface-related usability problems.</p>

<p>This single change would result in the following, revised formula</p>

<table align="center">
	<tbody>
		<tr>
			<td align="center">Equation 2:<br/>
			time( <em> i </em> ) = 37.3 + 4.2 <em> i </em></td>
		</tr>
	</tbody>
</table>

<p>Transforming the time estimates in (Equation 1) or (Equation 2) to money estimates can be done fairly simply by multiplying the number of hours by an estimate of the loaded hourly cost of professional staff. Note that the salary and benefits costs of the professional staff are not sufficient, since additional costs are incurred in form of the computer equipment and laboratory space used for the test. To use round numbers, an estimated hourly loaded cost for professional staff of $100 translates into a total cost for the heuristic evaluation of $10,500 for the 105 hours that were actually spent.</p>

<h3>4.2 Benefit Estimation</h3>

<p>The only way to get an exact measure of the benefits of the heuristic evaluation would be to fully implement two versions of the user interface; one without any changes and one with the changes implied by the evaluation results. These two versions should then be used by a large number of real users to perform real tasks for sufficiently long time that the steady-state level of expert performance had been reached in both cases [Gray et al. 1992]. This process would provide exact measures for the differences in learning time and expert performance. Unfortunately, the version of the interface that was evaluated only exists in a prototype form with which one cannot do any real work, and it would be unrealistic to expect significant development resources to be invested in transforming this prototype to a final product with an identical user interface now that a large number of usability problems have been documented.</p>

<p>Alternatively, one could build a detailed economic work-study model of the different steps involved in the users' workday in order to assess the frequency and duration of each sub-task. One could then further use formal models of user interaction times to estimate the duration of performing each step with each of a set of alternative user interface designs [Gray et al. 1992]. Such an approach would provide fairly detailed estimates but would not necessarily be accurate because of unknown durations of the operations in the model. It would also be very time-consuming to carry out.</p>

<p>It is thus necessary to rely on estimates of the benefits rather than hard measurement data. To get such estimates, the 11 evaluators were asked to estimate the improvements in usability from fixing all the 44 usability problems identified by the heuristic evaluation. Usability improvements were estimated with respect to two usability parameters:</p>

<ul>
	<li>Reduction of learning time: How much less time would the users need to spend learning to use the system? Learning time considered as a usability parameter represents a one-time loss of productive time for each new user to learn the system, so any savings would be realized only once.</li>
	<li>Speedup in expert performance: Once the users have reached a steady state of expert performance, how much faster would they be able to perform their work when using a system with all the usability problems fixed than when using a system with all the problems still in place? Expert performance considered as a usability parameter represents a continued advantage for the use of the improved interface, so any savings would be realized throughout the lifetime of the system.</li>
</ul>

<p>Other usability parameters of interest include frequency of user errors and the users' subjective satisfaction, but these parameters were not estimated. Since several of the usability problems we found were related to error-prone circumstances, it is likely that the number of user errors would go down.</p>

<p>Ten of the 11 evaluators provided learning time estimates and all 11 provided expert speedup estimates. Histograms of the distribution of these estimates are shown in Figure 3. Nielsen and Phillips [1993] found that estimates of changes in user performance made by usability specialists were highly variable, as also seen in the figure here, but that mean values of at least three independent estimates were reasonably close to the values measured by controlled experiments.</p>

<table width="100%">
	<caption align="bottom"><strong>Figure 3 </strong><br/>
	Histograms showing the distribution of the evaluators' estimates of savings in learning time (top) and expert performance speedup (bottom) for an interface fixing all the usability problems found in the heuristic evaluation. One evaluator did not provide a learning time estimate.</caption>
	<tbody>
		<tr>
			<td align="center"><img alt="Histograms of estimated improvements in learning time and expert performance" src="http://media.nngroup.com/media/editor/2012/12/10/guerrilla_improvement_est.gif" style="width: 580px; height: 330px;"/></td>
		</tr>
	</tbody>
</table>

<p>Given that the benefit estimates are based purely on subjective judgments of experts rather than on empirical evidence, it would seem prudent to be conservative in translating the evaluators' estimates into projected monetary savings. The mean values are 0.8 days for learning time reduction and 18% for expert speedup when all evaluators are considered, and 0.5 days and 16%, respectively, when excluding the perhaps overly optimistic outliers at 2 days and 40%. In order to be conservative, we will choose 0.5 days as our learning time reduction estimate and 10% as our expert speedup estimate.</p>

<p>The 10% expert speedup obviously only applies to time spent using the interface. Studies of the users indicate that they will spend about 1/3 of their time doing other tasks, 1/3 of their time performing the task without operating the user interface, and 1/3 of their time actually operating the interface. The 10% expert speedup thus corresponds to 3.3% of total work time.</p>

<p>Translating these estimates into overall savings can be done under the following assumptions: We assume that 2,000 people will be using the system. This is somewhat conservative given that about 3,000 people currently perform this job. Having 2,000 people each save 0.5 days in learning to use the system corresponds to a total of 1,000 user-days saved as a one-time saving. Furthermore, having 2,000 users perform their work 3.3% faster after having reached expert performance corresponds to 67 user-years saved each year the system is in use. Again to be conservative, we will only consider the savings for the first year, even though computer systems of the magnitude we are talking about here are normally used for more than one year. Sixty-seven user-years correspond roughly to 13,000 user-days saved. The total number of user-days saved the first year is thus about 14,000.</p>

<p>To value the total savings in monetary terms, we will assume that the cost of one user-day is $100, and to be conservative, we will assume that only half of the usability problems can actually be fixed, so that only half of the potential savings are actually realized. Furthermore, we need to take into account the fact that the savings in user time are not realized until the system is introduced and thus have a smaller net present value than their absolute value. Again to use round numbers, we will discount the value of the saved learning time by 20% and the value of the expert speedup in the first year by 30%. Learning time can be discounted by a smaller percentage as this saving is realized on day one after the introduction of the system. Using these conservative assumptions, we find one-year savings of $540,000.</p>

<p>Of course, the savings are not realized just by wishing for half of the usability problems to be fixed, so we have to reduce the savings estimate with an estimate of the cost of the additional software engineering effort needed to redesign the interface rather than just implementing the interface from the existing prototype. Assuming that the amount of software engineering time needed for this additional work is 400 hours, and again assuming that the loaded cost of a professional is $100 per hour, we find that the savings estimate needs to be reduced by $40,000. This expense is incurred here and now and thus cannot be discounted. Our final estimate of the net present value of improving the user interface is thus $500,000.</p>

<p>Still being conservative, we have not taken into account the value of the saved software engineering costs from not having to modify the system after its release. Assuming that the original user interface were to be fully implemented and released, is it very likely that the users would demand substantial changes in the second release, and it is well known that making software engineering changes to a released system is much more expensive than making changes at a prototype stage of the software lifecycle.</p>

<p>The $500,000 benefit of improving the interface should be compared with the cost of the heuristic evaluation project, estimated at $10,500. We thus see that the benefit/cost ratio is 48. This number involves significant uncertainties, but is big enough that we do not hesitate to conclude that the heuristic evaluation paid off.</p>

<p>As a final comment on the cost-benefit analysis we should note that the "benefits" do not translate to an actual cash flow. Instead, they represent the avoidance of the penalty represented by the extra time the users would have had to spend if the prototype interface had been implemented and released without further changes. It is an interesting and an important management problem to find ways to properly represent such savings in the funding of software development.</p>

<h3>4.3 Cost-Benefit Analysis of User Testing</h3>

<p>After the heuristic evaluation exercise, additional user testing was performed on the same interface, running four test users. A major reason for using so many more heuristic evaluators than test users was that the users of this particular application were highly specialized technicians who were difficult to get into the lab, whereas it was reasonably easy to get a large number of usability specialists to participate in the heuristic evaluation session. Four new usability problems were found by the user testing which also confirmed 17 of the problems that had already been found by heuristic evaluation.</p>

<p>One can discuss whether the 23 core problems that were not observed in the user test are in fact "problems" given that they could not be seen to bother the real users. As argued elsewhere [Nielsen 1992b], such problems can indeed be very real, but their impact may just have too short a duration to be observable in a standard user test. Problems that have the effect of slowing users down for 0.1 second or so simply cannot be observed unless data from a very large number of users is subjected to statistical analysis, but they can be very real and costly problems nevertheless. Also, some problems may occur too infrequently to have been observed with the small number of users tested here.</p>

<p>The main cost of the user test activity was having two professionals spend 7 hours each on the running of the test and the briefing and debriefing of the test users. No time was needed for the traditionally time-consuming activity of defining the test tasks since the same scenario was used as that developed for the previous usability work. Additionally, half an hour was spent finding and scheduling the users for the test and two hours were spent on implementing a small training interface on which the users could learn to use a mouse and standard graphical interaction techniques like pull-down windows. These activities sum to a total of 16.5 person-hours of professional staff, or a cost of $1,650.</p>

<p>Furthermore, the four users and their manager spent essentially a full day on the test when their travel time is taken into account. Again assuming that the cost of one user-day is $100, and furthermore assuming that the cost of one manager-day is $200, the total cost of user involvement is $600. Adding the cost of the professionals and the users gives a total estimate of $2,250 as the cost of the user testing.</p>

<p>The $2,250 spent on user testing could potentially have been spent on additional heuristic evaluation efforts instead. According to Equation 1, this sum corresponds to using 4.3 additional evaluators. Nielsen and Landauer [1993] showed that the finding of usability problems by <em> i </em> evaluators can be modelled by the prediction formula</p>

<table align="center">
	<tbody>
		<tr>
			<td align="center">Equation 3:<br/>
			ProblemsFound( <em> i </em> ) = N(1 - (1- <em> l </em> ) <sup> <em> i </em> </sup> )</td>
		</tr>
	</tbody>
</table>

<p>For the core usability problems in the present study, the best-fit values for the parameters in this equation are N=40 and <em> l </em> =0.26. Increasing the number of heuristic evaluators, <em> i </em> , from 11 to 15.3 can thus be expected to result in the finding of about 1.1 additional usability problems. This estimate shows that the available additional resources do indeed seem to have been spent better on running a user test, finding four problems, than on potentially extending the heuristic evaluation further.</p>

<p>We have no systematic method to estimate the benefits of having found the four additional problems that were discovered by user testing. However, one easy way to arrive at a rough estimate is to assume that the average severity of the four new problems is the same as the average severity of the 17 problems that had already been found by heuristic evaluation. As part of the heuristic evaluation study, severity was measured on a rating scale, with each usability problem being assigned a severity score from zero to four, with higher scores denoting more serious problems. The sum of the severity scores for the original 44 usability problems was 98.41, and the sum of the severity scores for the 17 problems that were seen both in the user test and in the heuristic evaluation was 41.56. We can thus estimate the relative severity of the additional four problems as compared to the original problems as 4/17 xá41.56/98.41 = 0.099.</p>

<p>Knowing about the additional problems found by user testing would thus add 9.9% to the total potential for improving the interface. Furthermore, we might assume that the proportion of the new problems that can be fixed, the impact of fixing them, and the cost of fixing them are all the same as the estimates for the problems found by heuristic evaluation. Under these assumptions, the benefit of having found the additional four usability problems can be valued at $500,000 x 0.099 = $49,500.</p>

<p>Using these estimates, the benefit/cost ratio of adding the user test after the heuristic evaluation is 22. Of course, the benefits of user testing would have been larger if we had credited it with finding the problems that were observed during the user test but had already been found by the heuristic evaluation. We should note, though, that the cost of planning the user test would have been higher if the heuristic evaluation had not been performed and had confirmed the value of the usage scenario. Also, there is no guarantee that all the observed problems would in fact have been found if there had been no prior heuristic evaluation. Now, we knew what to look for, but we might not have noticed as many problems if the user test had been our first usability evaluation activity for this interface.</p>

<p>If the user test were to be credited with all 17 duplicate problems as well as the four new ones, taking the higher-than-average severity of the seventeen problems into account, the benefit of the user test would be valued at $260,500. Of course, this amount would be the benefit from the user test only if no prior heuristic evaluation had been performed. Therefore, it would seem reasonable to charge this hypothetical analysis of the user test with some of the costs that were in fact spent preparing for the heuristic evaluation. Specifically, referring to Table 5, we will add the costs of assessing the appropriate way to use the method, having the outside evaluation expert learn about the domain and scenario, preparing the scenario, and preparing the software, as well as half the time spent writing the problem descriptions (since about half as many problems were found). These activities sum to 24 hours, or an additional cost of $2,400, for a total estimated cost of running the user test without prior heuristic evaluation of $4,650. This translates into a benefit/cost ratio of 56.</p>

<p>To provide a fair comparison, it should be noted that the benefit/cost ratio of performing the heuristic evaluation with only four evaluators would have been 53. This number is larger than the benefit/cost ratio for the full evaluation since more previously unfound usability problems are identified by the first evaluators than by the last, as shown by (EQ 3). Furthermore, the heuristic evaluation provided severity estimates that can be used to prioritize the fixing of the usability problems in the further development process, and the availability of this data probably adds to the actual value of the method as measured by delivered usability. If the time spent on the debriefing and severity ratings is deducted from the time spent on the heuristic evaluation, the benefit/cost ratio for the full eleven evaluators becomes 59 and the ratio for four evaluators becomes 71.</p>

<p>Thus, within the uncertainty of these estimates, it appears that user testing and heuristic evaluation have comparable cost-benefit ratios, and that doing some of each may have additional value.</p>

<h2>5 The Evolution of Usability Engineering in Organizations</h2>

<p>Two of the fundamental slogans of discount usability engineering are that "any data is data" and "anything is better than nothing" when it comes to usability. Therefore, I often advocate an approach to usability that focuses on getting started to use a minimum of usability methods. Even so, there are many projects that would benefit from employing more than the minimum amount of discount usability methods. I used the term "guerrilla HCI" in the title of this chapter because I believe that simplified usability methods can be a way for a company to gradually build up its reliance on systematic usability methods, starting with the bare minimum and gradually progressing to a more refined lifecycle approach.</p>

<p>Based on observing multiple companies and projects over the years, I have arrived at the following series of steps in the increased use of usability engineering in software development.</p>

<ol>
	<li><strong>Usability does not matter. </strong> The main focus is to wring every last bit of performance from the iron. This is the attitude leading to the world-famous error message, "beep."</li>
	<li>Usability is important, but <strong> good interfaces can surely be designed by the regular development staff </strong> as part of their general system design. This attitude is symbolized by the famous statement made by King Frederik VI of Denmark on February 26, 1835: "We alone know what serves the true welfare and benefit of the State and People." At this stage, no attempt is made at user testing or at acquiring staff with usability expertise.</li>
	<li>The desire to have the <strong> interface blessed by the magic wand </strong> of a usability engineer. Developers recognize that they may not know everything about usability, so they call in a usability specialist to look over their design and comment on it. The involvement of the usability specialist is often too late to do much good in the project, and the usability specialist often has to provide advice on the interface without the benefit of access to real users.</li>
	<li><strong>GUI panic strikes </strong> , causing a sudden desire to learn about user interface issues. Currently, many companies are in this stage as they are moving from character-based user interfaces to graphical user interfaces and realize the need to bring in usability specialists to advise on graphical user interfaces from the start. Some usability specialists resent this attitude and maintain that it is more important to provide an appropriate interface for the task than to blindly go with a graphical interface without prior task analysis. Even so, GUI panic is an opportunity for usability specialists to get involved in interface design at an earlier stage than the traditional last-minute blessing of a design that cannot be changed much. <span class="updatecomment"> (Update added 1999: these days, this stage is often characterized by <strong> Web Panic Strikes </strong> . It's the same phenomenon and should be treated the same way.) </span></li>
	<li><strong>Discount usability engineering <em> sporadically </em> used. </strong> Typically, some projects use a few discount usability methods (like user testing or heuristic evaluation), though the methods are often used too late in the development lifecycle to do maximum good. Projects that do use usability methods often differ from others in having managers who have experienced the benefit of usability methods on earlier projects. Thus, usability acts as a kind of virus, infecting progressively more projects as more people experience its benefits.</li>
	<li><strong>Discount usability engineering <em> systematically </em> used. </strong> At some point in time, most projects involve some simple usability methods, and some projects even use usability methods in the early stages of system development. Scenarios and cheap prototyping techniques seem to be very effective weapons for guerrilla HCI in this stage.</li>
	<li><strong>Usability group and/or usability lab founded. </strong> Many companies decide to expand to a deluxe usability approach after having experienced the benefits of discount usability engineering. Currently, the building of <a href="../usability-labs/index.php" title="'Usability Laboratories: A 1994 Survey', paper by Jakob Nielsen"> usability laboratories</a> [Nielsen 1994a] is quite popular as is the formation of dedicated groups of usability specialists.</li>
	<li><strong>Usability permeates lifecycle. </strong> The final stage is rarely reached since even companies with usability groups and usability labs normally do not have enough usability resources to employ all the methods one could wish for at all the stages of the development lifecycle. However, there are some, often important, projects that have usability plans defined as part of their early project planning and where usability methods are used throughout the development lifecycle.</li>
</ol>

<p>This model is fairly similar to the series of organizational acceptance stages outlined by Ehrlich and Rohn [1994] but was developed independently. Stage 1-2 in the above list correspond to Ehrlich and Rohn's skepticism stage, stage 3-4 correspond to their curiosity stage, stage 5-6 correspond to their acceptance stage, and stage 7-8 correspond to their partnership stage.</p>

<p>Many teachers of usability engineering have described the almost religious effect it seems to have the first time students try running a user test and see with their own eyes the difficulties perfectly normal people can have using supposedly "easy" software. Unfortunately, organizations are more difficult to convert, so they mostly have to be conquered from within by the use of guerrilla methods like discount usability engineering that gradually show more and more people that usability methods work and improve products. It is too optimistic to assume that one can move a development organization from stage 1 or 2 in the above model to stage 7 or 8 in a single, sweeping change. In reality, almost all usability methods are extremely cheap to use compared to the benefits they provide in form of better and easier to use products, but often we have to start with the cheapest possible methods to overcome the intimidation barrier gradually.</p>

<p> </p>

<h3>Acknowledgments</h3>

<p>The author would like to thank Raldolph Bias, Tom Landauer, and Janice Rohn for helpful comments on an earlier version of the manuscript.</p>

<h2>References</h2>

<ul class="referencelist">
	<li>Apple Computer (1987). Human Interface Guidelines: The Apple Desktop Interface. Addison Wesley, Reading, MA.</li>
	<li>Apple Computer (1992). Macintosh Human Interface Guidelines. Addison Wesley, Reading, MA.</li>
	<li>Bellotti, V. (1988). Implications of current design practice for the use of HCI techniques. In Jones, D.M. and Winder, R. (Eds.), People and Computers IV, Cambridge University Press, Cambridge, U.K., 13-34.</li>
	<li>Boehm, B. W. (1981). Software Engineering Economics. Prentice-Hall, Englewood Cliffs, NJ.</li>
	<li>Card, S. K., Moran, T. P., and Newell, A. (1983). The Psychology of Human-Computer Interaction, Lawrence Erlbaum Associates, Hillsdale, NJ.</li>
	<li>Carroll, J. M., and Rosson, M. B. (1990). Human-computer interaction scenarios as a design representation. Proc. HICSS-23: Hawaii International Conference on System Science, IEEE Computer Society Press, 555-561.</li>
	<li>Clarke, L. (1991). The use of scenarios by user interface designers. In Diaper, D., and Hammond, N. (Eds.), People and Computers VI, Cambridge University Press, Cambridge, U.K. 103-115.</li>
	<li>Ehrlich, K., and Rohn, J. (1994). Cost-justification of usability engineering: A vendor's perspective. In Bias, R.G., and Mayhew, D.J. (Eds.), Cost-Justifying Usability. Academic Press, Boston, MA.</li>
	<li>Gould, J. D., and Lewis, C. H. (1985). Designing for usability: Key principles and what designers think. Communications of the ACM 28, 3 (March), 300-311.</li>
	<li>Gray, W. D., John, B. E., and Atwood, M. E. (1992). The precis of project Grace, or, an overview of a validation of GOMS. Proc. ACM CHI'92 (Monterey, CA, 3-7 May 1992), 307-312.</li>
	<li>Jørgensen, A.H. (1989). Using the thinking-aloud method in system development. In Salvendy, G. and Smith, M.J. (Eds.), Designing and Using Human-Computer Interfaces and Knowledge Based Systems. Elsevier Science Publishers, Amsterdam, 743-750.</li>
	<li>Karwowski, W., Kosiba, E., Benabdallah, S., and Salvendy, G. (1989). Fuzzy data and communication in human-computer interaction: For bad or for good. In Salvendy, G. and Smith, M.J. (Eds.), Designing and Using Human-Computer Interfaces and Knowledge Based Systems. Elsevier Science Publishers, Amsterdam, 402-409.</li>
	<li>Landauer, T. K. (1988). Research methods in human-computer interaction. In Helander, M. (Ed.), Handbook of Human-Computer Interaction. North-Holland, Amsterdam, The Netherlands. 543-568.</li>
	<li>Mantei, M. M., and Teorey, T.J. (1988). Cost/benefit analysis for incorporating human factors in the software lifecycle, Communications of the ACM 31, 4 (April), 428-439.</li>
	<li>Milsted, U., Varnild, A., and Jørgensen, A.H. (1989). Hvordan sikres kvaliteten af brugergr¾nsefladen i systemudviklingen ("Assuring the quality of user interfaces in system development," in Danish), Proceedings NordDATA'89 Joint Scandinavian Computer Conference (Copenhagen, Denmark, 19-22 June), 479-484.</li>
	<li>Molich, R., and Nielsen, J. (1990). Improving a human-computer dialogue, Communications of the ACM 33, 3 (March), 338-348.</li>
	<li>Monk, A., Wright, P., Haber, J., and Davenport, L. (1993). Improving Your Human-Computer Interface: A Practical Technique. Prentice Hall International, Hemel Hempstead, U.K.</li>
	<li>Nielsen, J. (1989a). Prototyping user interfaces using an object-oriented hypertext programming system, Proc. NordDATA'89 Joint Scandinavian Computer Conference (Copenhagen, Denmark, 19-22 June), 485-490.</li>
	<li>Nielsen, J. (1989b). Usability engineering at a discount. In Salvendy, G., and Smith, M.J. (Eds.), Designing and Using Human-Computer Interfaces and Knowledge Based Systems, Elsevier Science Publishers, Amsterdam. 394-401.</li>
	<li>Nielsen, J. (1990a). Big paybacks from 'discount' usability engineering, IEEE Software 7, 3 (May), 107-108.</li>
	<li>Nielsen, J. (1990b). Paper versus computer implementations as mockup scenarios for heuristic evaluation, Proc. INTERACT'90 3rd IFIP Conf. Human-Computer Interaction (Cambridge, U.K., 27-31 August), 315-320.</li>
	<li>Nielsen, J. (1992a). The usability engineering life cycle. IEEE Computer 25, 3 (March), 12-22.</li>
	<li>Nielsen, J. (1992b). Evaluating the thinking aloud technique for use by computer scientists. In Hartson, H. R. and Hix, D. (Eds.), Advances in Human-Computer Interaction Vol. 3, Ablex, Norwood, NJ. 75-88.</li>
	<li>Nielsen, J. (1992c). Finding usability problems through heuristic evaluation. Proc. ACM CHI'92 (Monterey, CA, 3-7 May), 373-380.</li>
	<li>Nielsen, J. (1993). <a href="../../books/usability-engineering/index.php"> Usability Engineering</a>. Academic Press, Boston, MA.</li>
	<li>Nielsen, J. (1994a). <a href="../usability-labs/index.php"> Usability laboratories</a>. Behaviour &amp; Information Technology 13, 1.</li>
	<li>Nielsen, J. (1994b). Heuristic evaluation. In Nielsen, J., and Mack, R.L. (Eds.), <a href="../../books/usability-inspection-methods/index.php"> Usability Inspection Methods</a>. John Wiley &amp; Sons, New York, NY.</li>
	<li>Nielsen, J., and Landauer, T. K. (1993). A mathematical model of the finding of usability problems. Proc. ACM INTERCHI'93 Conf. (Amsterdam, the Netherlands, 24-29 April), 206-213.</li>
	<li>Nielsen, J., and Levy, J. (1994). Measuring usability - preference vs. performance. <cite> Communications of the ACM </cite> <strong> 37 </strong> , 4 (April), 66-75.</li>
	<li>Nielsen, J., and Molich, R. (1989). Teaching user interface design based on usability engineering, ACM SIGCHI Bulletin 21, 1 (July), 45-48</li>
	<li>Nielsen, J., and Molich, R. (1990). Heuristic evaluation of user interfaces, Proc. ACM CHI'90 (Seattle, WA, 1-5 April), 249-256.</li>
	<li>Nielsen, J., and Phillips, V. L. (1993). Estimating the relative usability of two interfaces: Heuristic, formal, and empirical methods compared. Proc. ACM INTERCHI'93 Conf. (Amsterdam, the Netherlands, 24-29 April), 214-221.</li>
	<li>Nielsen, J., Frehr, I., and Nymand, H. O. (1991). The learnability of HyperCard as an object-oriented programming system, Behaviour and Information Technology 10, 2 (March-April), 111-120.</li>
	<li>Perlman, G. (1988). Teaching user interface development to software engineers, Proc. Human Factors Society 32nd Annual Meeting, 391-394.</li>
	<li>Perlman, G. (1990). Teaching user-interface development, IEEE Software 7, 6 (November), 85-86.</li>
	<li>Telles, M. (1990). Updating an older interface, Proc. ACM CHI'90 (Seattle, WA, 1-5 April), 243-247.</li>
	<li>Thovtrup, H., and Nielsen, J. (1991). Assessing the usability of a user interface standard, Proc. ACM CHI'91 (New Orleans, LA, 28 April-2 May), 335-341.</li>
	<li>Tognazzini, B. (1990). User testing on the cheap, Apple Direct 2, 6 (March), 21-27. Reprinted as Chapter 14 in <a href="http://www.amazon.com/exec/obidos/ISBN=0201608421/useitcomusableinA/"> <cite> TOG on Interface</cite></a>, Addison-Wesley, Reading, MA, 1992.</li>
	<li>Voltaire, F. M. A. (1764). Dictionnaire Philosophique.</li>
	<li>Whiteside, J., Bennett, J., and Holtzblatt, K. (1988). Usability engineering: Our experience and evolution. In Helander, M. (Ed.), Handbook of Human-Computer Interaction, North-Holland, Amsterdam, 791-817.</li>
</ul>
</section>
  

  
  <section>
    
        
    
  </section>
  
  
  
  
  

  


  <section>
    <br />
    <p class="share"><b>Share <span class="hide-for-small-down">this article</span>:</b> <a href="https://twitter.com/intent/tweet?url=http://www.nngroup.com/articles/guerrilla-hci/&amp;text=Guerrilla%20HCI:%20Using%20Discount%20Usability%20Engineering%20to%20Penetrate%20the%20Intimidation%20Barrier&amp;via=nngroup">Twitter</a> | <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=http://www.nngroup.com/articles/guerrilla-hci/&amp;title=Guerrilla%20HCI:%20Using%20Discount%20Usability%20Engineering%20to%20Penetrate%20the%20Intimidation%20Barrier&amp;source=Nielsen%20Norman%20Group">LinkedIn</a> | <a href="https://plus.google.com/share?url=http://www.nngroup.com/articles/guerrilla-hci/">Google+</a> | <a href="mailto:?subject=NN/g Article: Guerrilla HCI: Using Discount Usability Engineering to Penetrate the Intimidation Barrier&amp;body=http://www.nngroup.com/articles/guerrilla-hci/">Email</a></p>
    <hr />
  </section>

  <footer class="l-subsection">
    
    
    <div class="learn-more">
      <h2>Learn More</h2>
      <p><a href="../subscribe/index.php">Subscribe to the weekly newsletter</a> to get notified about future articles.</p>
      </div>
    
    

    <div class="row show-for-small-down related-content">
      
      
      

      
      
        <div class="column small-12 topic-links">All articles about:
          <ul>
            
            <li><a href="../../topic/discount-methods/index.php">discount methods</a></li>
            
            <li><a href="../../topic/prototyping/index.php">Prototyping</a></li>
            
            <li><a href="../../topic/strategy/index.php">Strategy</a></li>
            
            <li><a href="../../topic/user-testing/index.php">User Testing</a></li>
            
          </ul>
        </div>
      
      

      
      
        <div class="column small-12">
          <h3>UX Conference Training Courses</h3>
           <ul class="no-bullet">
    
        <li><a href="../../courses/ux-basic-training/index.php">UX Basic Training</a></li>
    
        <li><a href="../../courses/journey-mapping/index.php">Journey Mapping to Understand Customer Needs</a></li>
    
        <li><a href="../../courses/facilitating-ux-workshops/index.php">Facilitating UX Workshops</a></li>
    
        <li><a href="../../courses/analytics-and-user-experience/index.php">Analytics and User Experience</a></li>
    
        <li><a href="../../courses/ux-deliverables/index.php">UX Deliverables</a></li>
    
</ul>
        </div>
      
      

      
      
        <div class="small-12 column">
          <h3>Research Reports</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../reports/paper-prototyping-training-video/index.php">Paper Prototyping Training Video</a></li>
              
            
              
                <li><a href="../../reports/usability-return-on-investment-roi/index.php">Return on Investment (ROI) for Usability</a></li>
              
            
              
                <li><a href="../../reports/how-to-conduct-eyetracking-studies/index.php">How to Conduct Eyetracking Studies</a></li>
              
            
              
                <li><a href="../../reports/how-to-recruit-participants-usability-studies/index.php">How to Recruit Participants for Usability Studies</a></li>
              
            
              
                <li><a href="../../reports/how-to-conduct-usability-studies-accessibility/index.php">How to Conduct Usability Studies for Accessibility</a></li>
              
            
          </ul>
        </div>
      
      

      
      
        <div class="small-12 column">
          <h3>Online Seminars</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../online-seminars/ux-real-world/index.php">UX in the Real World</a></li>
              
            
              
                <li><a href="../../online-seminars/how-change-your-corporate-culture/index.php">How to Change Your Corporate Culture to be User First</a></li>
              
            
              
                <li><a href="../../online-seminars/mobile-user-testing/index.php">Mobile User Testing</a></li>
              
            
          </ul>
        </div>
      
      
    </div>

    <div class="hide-for-small-down row related-content">
      <div class="medium-6 columns">
        
        
          <div>
            <h3>Articles</h3>
            <ul class="no-bullet">
              
                
                <li><a href="../web-discount-usability/index.php">Discount Usability for the Web</a></li>
                
              
                
                <li><a href="../ux-stories/index.php">UX Stories Communicate Designs </a></li>
                
              
                
                <li><a href="../discount-usability-20-years/index.php">Discount Usability: 20 Years</a></li>
                
              
                
                <li><a href="../usability-for-200/index.php">Usability for $200</a></li>
                
              
                
                <li><a href="../testing-decreased-support/index.php">How Iterative Testing Decreased Support Calls By 70% on Mozilla&#39;s Support Website</a></li>
                
              
            </ul>
          </div>
        
        

        
          
          <div>
            <h3>Research Reports</h3>
            <ul class="no-bullet">
              
                
                  <li><a href="../../reports/paper-prototyping-training-video/index.php">Paper Prototyping Training Video</a></li>
                
              
                
                  <li><a href="../../reports/usability-return-on-investment-roi/index.php">Return on Investment (ROI) for Usability</a></li>
                
              
                
                  <li><a href="../../reports/how-to-conduct-eyetracking-studies/index.php">How to Conduct Eyetracking Studies</a></li>
                
              
                
                  <li><a href="../../reports/how-to-recruit-participants-usability-studies/index.php">How to Recruit Participants for Usability Studies</a></li>
                
              
                
                  <li><a href="../../reports/how-to-conduct-usability-studies-accessibility/index.php">How to Conduct Usability Studies for Accessibility</a></li>
                
              
            </ul>
          </div>
          
        

      </div>
      <div class="medium-6 columns">
        
          
          <div>
            <h3>UX Conference Training Courses</h3>
             <ul class="no-bullet">
    
        <li><a href="../../courses/ux-basic-training/index.php">UX Basic Training</a></li>
    
        <li><a href="../../courses/journey-mapping/index.php">Journey Mapping to Understand Customer Needs</a></li>
    
        <li><a href="../../courses/facilitating-ux-workshops/index.php">Facilitating UX Workshops</a></li>
    
        <li><a href="../../courses/analytics-and-user-experience/index.php">Analytics and User Experience</a></li>
    
        <li><a href="../../courses/ux-deliverables/index.php">UX Deliverables</a></li>
    
</ul>
          </div>
          
        
        
          
          <div>
            <h3>Online Seminars</h3>
            <ul class="no-bullet">
            
                <li><a href="../../online-seminars/ux-real-world/index.php">UX in the Real World</a></li>
            
                <li><a href="../../online-seminars/how-change-your-corporate-culture/index.php">How to Change Your Corporate Culture to be User First</a></li>
            
                <li><a href="../../online-seminars/mobile-user-testing/index.php">Mobile User Testing</a></li>
            
            </ul>
          </div>
          
        
      </div>
    </div>
  </footer>
</article>

        </div>

      

    
  </div>
  <div class="row">
    
      <div class="column small-12 show-for-small-down l-navrail announcements">
        
          
        
      </div>
    
    
  </div>

    </div>
    




<footer id="page-footer">
    <div class="row hide-for-print">
        <div class="column small-12 medium-6">
            <section class="footer-popular">
                
                <h1>Popular Topics</h1>
                <div style="float: left;  width: 15em; display: block;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/applications/index.php">Application Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/e-commerce/index.php">E-Commerce Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/eyetracking/index.php">Eyetracking </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/information-architecture/index.php">Information Architecture </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/intranets/index.php">Intranet Design </a></li>
	<li><a href="../../topic/interaction-design/index.php">Interaction Design </a></li>
</ul>
</div>

<div style="float: left;  width: 15em; display: block; list-style: none;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/mobile-and-tablet-design/index.php">Mobile and Tablet Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/credibility-persuasion/index.php">Persuasive Design</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/research-methods/index.php">Research Methods</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/user-testing/index.php">User Testing </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/web-usability/index.php">Web Usability </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/writing-web/index.php">Web Writing &amp; Content Strategy </a></li>
</ul>
</div>

<p>&nbsp;</p>

                
            </section>
        </div>

        <div class="column small-12 medium-6">
            <div class="row">
                <div class="column small-6">
                    <section class="footer-about">
                        <h1>Follow Us</h1>
                        <ul>
	<li><a href="../subscribe/index.php">Subscribe to our weekly UX newsletter</a>,&nbsp;Jakob Nielsen&#39;s Alertbox</li>
	<li>Visit Don Norman&#39;s website, <a href="http://www.jnd.org/"> www.jnd.org </a></li>
	<li>Visit Bruce Tognazzini&#39;s website, <a href="http://www.asktog.com/"> www.asktog.com </a></li>
	<li style="padding-top: 6px;"><a class="twitter-follow-button" data-show-count="false" data-size="large" href="https://twitter.com/nngroup"><img alt="Follow @nngroup on Twitter" height="20" src="https://media.nngroup.com/media/editor/2016/12/30/twitter-follow-button.png" style="width: 117px; height: 20px;" width="117" /></a></li>
	<li style="padding-top: 6px;"><img alt="" height="14" src="https://media.nngroup.com/media/editor/2016/07/26/feed-icon-14x14.png" style="vertical-align:text-top;width:14px;margin-right:.25em;" width="14" /><a href="../../feed/rss/index.php">Subscribe to our RSS feed</a></li>
</ul>

                    </section>
                </div>

                <div class="column small-6">
                    <section class="footer-about right-column">
                        <h1>About
</h1>
                        <ul class="no-bullet">
	<li><a href="../../about/contact/index.php">Contact information</a></li>
	<li><a href="../../about/index.php">About Us</a></li>
	<li><a href="../../about/why-nng/index.php">Why NN/g? </a></li>
	<li><a href="../../return-policy/index.php">Return Policy</a></li>
</ul>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="copyright column small-12">
            <p><a href="../../copyright-and-reprint-info/index.php">Copyright</a> &copy; 1998-2017 Nielsen Norman Group, All Rights Reserved.</p>
        </div>
    </div>
</footer>


    


        <script src="https://media.nngroup.com/static/vendor/fastclick/js/fastclick.min.js"></script>
        <script src="https://media.nngroup.com/static/vendor/foundation/js/foundation.min.js"></script>
        <script src="http://media.nngroup.com/static/js/app.js"></script>
        <script src="http://media.nngroup.com/static/js/scroll-to-fixed.js"></script><script>var externalLinks = document.querySelectorAll('a[href^="http"]');for (var i = externalLinks.length-1; i >= 0; i--) {externalLinks[i].addEventListener("click", function(event) { alert("External links are disabled for this test."); event.preventDefault(); }, false);}var mailLinks = document.querySelectorAll('a[href^="mailto"]');for (var i = mailLinks.length-1; i >= 0; i--) {mailLinks[i].addEventListener("click", function(event) { alert("Email links are disabled for this test."); event.preventDefault(); }, false);}var externalForms = document.querySelectorAll('form');for (var i = externalForms.length-1; i >= 0; i--) {externalForms[i].addEventListener("submit", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false); externalForms[i].addEventListener("click", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false);}</script>

        <script>
            
            $(function() {
                $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
                $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
            });
        </script>

        
<script src="https://media.nngroup.com/static/js/articles.js"></script>


        
        <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1072699960;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072699960/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
    </body>

<!-- Mirrored from www.nngroup.com/articles/guerrilla-hci/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:10:41 GMT -->
</html>
