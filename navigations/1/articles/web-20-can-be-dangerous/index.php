<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->





    
<!-- Mirrored from www.nngroup.com/articles/web-20-can-be-dangerous/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:15:05 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1026.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"licenseKey":"1a30ef9931","queueTime":3,"beacon":"bam.nr-data.net","errorBeacon":"bam.nr-data.net","applicationID":"2048992","transactionName":"NFxSMRBUX0EDUkIPDA0WdhAMVkVbDV8ZBxEXUFMJB0YfRAtUQRVZIktEDAFZVHYHRVcPD01eVRE=","applicationTime":580,"agent":""}</script>
        <title>Web 2.0 Can Be Dangerous...</title><meta property="og:title" content="Web 2.0 Can Be Dangerous..." />
  
        
        <meta name="description" content="AJAX, rich Internet UIs, mashups, communities, and user-generated content often add more complexity than they&#39;re worth. They divert design resources and prove that what&#39;s hyped is rarely what&#39;s most profitable.">
        <meta property="og:description" content="AJAX, rich Internet UIs, mashups, communities, and user-generated content often add more complexity than they&#39;re worth. They divert design resources and prove that what&#39;s hyped is rarely what&#39;s most profitable." />
        
  
        
	
        
        <meta name="keywords" content="Web2, Web 2.0, RIA, rich Internet applications, AJAX, response time, page updates, within-page updates, shopping carts, communities, user-generated content, intranets, intranet communities, mashups, mash-up, co-branding, maps, advertising, Internet ads, press coverage, hype, drag-and-drop, efficiency, simplicity, repeated actions, application design, social loafing">
        

        

        <meta name="robots" content="noarchive">
        <meta name="viewport" content="width=device-width">

        <link rel="search" type="application/opensearchdescription+xml" title="NN/g Search" href="../../opensearch.xml" />
        <link rel="shortcut icon" href="http://media.nngroup.com/static/img/favicon.ico" />

        <link rel="stylesheet" href="https://media.nngroup.com/static/vendor/font-awesome/css/font-awesome.min.css">

        
        <link rel="stylesheet" href="https://media.nngroup.com/static/css/7232a960f1df.css" type="text/css" />
        <link rel="stylesheet" href="http://fast.fonts.com/t/1.css?apiType=css&amp;projectid=c5846260-9def-46ec-9407-0f1349bd2d64" type="text/css">

        <script src="https://media.nngroup.com/static/vendor/modernizr/js/modernizr.min.js"></script>

        <script type="text/javascript" src="https://media.nngroup.com/static/js/3283be5def74.js"></script>
        

        

        
        
        <!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=229034,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('http://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->


<!-- Google Analytics Tracking Code for http://nngroup.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23132681-1', 'nngroup.com');
  ga('require', 'linkid', 'https://www.nngroup.com/articles/web-20-can-be-dangerous/linkid.js');
  ga('require', 'displayfeatures');

// Optimizely Universal Analytics Integration
 window.optimizely = window.optimizely || [];
 window.optimizely.push("activateUniversalAnalytics");

  ga('send', 'pageview');

</script>
<script>
/**
* Function to track a click on an outbound link in Google Analytics.
*/
function trackOutboundLink(label, url) {
    if(window.ga && ga.loaded) {
        ga('send', 'event', 'outbound', 'click', label, {'hitCallback': function(){document.location = url;}
        });
    } else {
        document.location = url;
    }
}
/**
* Function to track a click on a link in the article announcement area.
*/
$('#articleAnnouncement a').on('click', function(){
  ga('send', 'event', 'link', 'click', 'articleAnnouncement');
});
</script>

<script type="text/javascript">
 setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2421.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



<!-- Hotjar Tracking Code for http://nngroup.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:121605,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

        <script src="https://media.nngroup.com/static/js/analytics/track_outbound_link.js"></script>
        
        
        
    </head>
    <body class="location-articles articles-detail">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
    <header id="page-header">
        <div id="cart" class="shopping-cart mobile-cart show-for-small-down"></div>
        <div class="row">
            <div id="masthead" class="masthead small-12 medium-7 column">

                <!-- The button to toggles menu on small screen -->

                <h1><a href="../../index.php"><img src="http://media.nngroup.com/static/img/logo-wordmark.png" width="74" height="30"> Nielsen <span class="norman">Norman</span> Group</a></h1>
                <p class="tagline show-for-medium-up">Evidence-Based User Experience Research, Training, and Consulting</p>

                <!-- The button to toggles form search on small screen -->


                <div style="clear: right;width:  100%;height: 1px;"></div><?php define('ROOT_PATH',$_SERVER["DOCUMENT_ROOT"] . '/apps/navigation_styles/navigations/1'); ?><?php include(ROOT_PATH)."/navigation.php"; ?>

                <!-- Drop down menu on small screen -->
                <div id="hideNav" class="show-for-small-down">
                  
                  <ul class="menu-content">
                    <li><a href="../../index.php">Home</a></li>
                    <li class="has-submenu">
                      <a>Training</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../training/index.php">Overview</a></li>
                        
<li id="nav-ux-conference"><a href="../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../online-seminars/index.php"><span>Online Seminars</span></a></li>

                      </ul>
                    </li>
                    <li class="has-submenu">
                      <a>Consulting</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
  <li><a href="../../consulting/index.php">Overview</a></li>
  
  <li><a href="../../consulting/benchmarking/index.php">Benchmarking</a></li>
  
  <li><a href="../../consulting/expert-design-review/index.php">Expert Design Review</a></li>
  
  <li><a href="../../consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
  
  <li><a href="../../consulting/usability-testing/index.php">Usability Testing </a></li>
  
  <li><a href="../../consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
  
  <li><a href="../../in-house-training/index.php">In-House Training</a></li>
</ul>

                    </li>

                    <li><a href="../../reports/index.php">Reports</a></li>
                    <li><a href="../index.php">Articles</a></li>
                    <li class="has-submenu"><a>About NN/G</a><i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../about/index.php">Overview</a></li>
                        <li><a href="../../people/index.php">People</a></li>
                        <li><a href="../../about/why-nng/index.php">Why NN/g?</a></li>
                        <li><a href="../../about/contact/index.php">Contact</a></li>
                        <li><a href="../../news/index.php">News</a></li>
                        <li><a href="../../about/history/index.php">History</a></li>
                        <li><a href="../../books/index.php">Books</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                </div>

                <hr class="header-divider show-for-small-only"/>


            </div>
       </div>

        <div id="hideNav">            <nav class="primary-nav small-12 column">
                <ul>
                    <li><a class="navitem" id="nav-home" href="../../index.php">Home</a></li>
                    <li><a class="navitem" id="nav-training" href="../../training/index.php">Training</a></li>
                    <li><a class="navitem" id="nav-consulting" href="../../consulting/index.php">Consulting</a></li>
                    <li><a class="navitem" id="nav-reports" href="../../reports/index.php">Reports</a></li>
                    <li><a class="navitem" id="nav-articles" href="../index.php">Articles</a></li>
                    <li><a class="navitem" id="nav-about" href="../../about/index.php">About NN/G</a></li>
                </ul>
            </nav>
            <nav class="secondary-nav small-12 medium-12 large-12 columns">
                
                
            </nav>
        </div>
    </header>
    <div class="l-content">
    
  

  

  <div class="row">
    
      
      <section class="small-12 medium-3 columns collapsable-content l-navrail articles-left-column">
        <h1 class="show-for-small-down collapse-link mobile-browse-header">Browse by Topic and Author</h1>
          <div class="row collapsable">

            
            <div class="small-6 medium-12 columns">
              <section id="article-popular-topics" style="display: none;">
                
                <h1>Topics</h1>
                <ul id="article-popular-topics">
                    
                    <li>
                        
                        <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/intranets/all/index.php">Intranets</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/user-testing/all/index.php">User Testing</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                        
                    </li>
                    
                    <li><a href="#" id="article-see-all-topics"><strong>See all topics&hellip;</strong></a></li>
                </ul>
                
              </section>
              <section id="article-all-topics" style="display: none;">
                
                <h1>
                  <span class="show-for-medium-up">All Article Topics</span>
                  <span class="show-for-small-down">All Topics</span>
                  (<a href="#" id="article-hide-all-topics">hide</a>)
                </h1>
                <ul>
                  
                  <li>
                    
                    <a href="../../topic/accessibility/all/index.php">Accessibility</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/agile/all/index.php">Agile</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/analytics-and-metrics/all/index.php">Analytics &amp; Metrics</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/applications/all/index.php">Application Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/b2b-websites/all/index.php">B2B Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/behavior-patterns/all/index.php">Behavior Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/branding/all/index.php">Branding</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/content-strategy/all/index.php">Content Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/corporate-websites/all/index.php">Corporate Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/design-patterns/all/index.php">Design Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-design-process/all/index.php">Design Process</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/email/all/index.php">Email</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/eyetracking/all/index.php">Eyetracking</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/heuristic-evaluation/all/index.php">Heuristic Evaluation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/human-computer-interaction/all/index.php">Human Computer Interaction</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-ideation/all/index.php">Ideation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/information-architecture/all/index.php">Information Architecture</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/interaction-design/all/index.php">Interaction Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/international-users/all/index.php">International Users</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/intranets/all/index.php">Intranets</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-management/all/index.php">Management</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/navigation/all/index.php">Navigation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/non-profit-websites/all/index.php">Non-Profit Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/personas/all/index.php">Personas</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/persuasive-design/all/index.php">Persuasive Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/prototyping/all/index.php">Prototyping</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/psychology-and-ux/all/index.php">Psychology and UX</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/research-methods/all/index.php">Research Methods</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/search/all/index.php">Search</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/social-media/all/index.php">Social Media</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/strategy/all/index.php">Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/user-testing/all/index.php">User Testing</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-teams/all/index.php">UX Teams</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/visual-design/all/index.php">Visual Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/writing-web/all/index.php">Writing for the Web</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/young-users/all/index.php">Young Users</a>
                    
                  </li>
                  
                </ul>
                
              </section>
            </div>
            <script>
              /* Determine which topics list will be displayed: the full or the
               * partial.  We are inlining this close to the list itself to
               * prevent rendering delays on the client
               */
              var topic_list = $.cookie("article-topics");

              var popular_topics = $('#article-popular-topics');
              var all_topics = $('#article-all-topics');

              if(topic_list == 1) {
                  popular_topics.hide();
                  all_topics.show();
              } else {
                  popular_topics.show();
                  all_topics.hide();
              }
            </script>
            

            
            <div class="small-6 medium-12 columns">
              
              <section id="article-initial-authors">
                  <h1>Author</h1>
                  <ul>
                    
                    <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                    
                    <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                    
                    <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                    
                    <li><a href="#" onClick="$('#article-initial-authors').hide(); $('#article-all-authors').show();"><strong>See all authors&hellip;</strong></a></li>
                  </ul>
              </section>
              <section id="article-all-authors" style="display: none;">
                <h1>
                  All Authors
                  (<a href="#" id="article-hide-all-authors">hide</a>)
                </h1>
                <ul>
                  
                  <li><a href="../author/raluca-budiu/index.php">Raluca Budiu</a></li>
                  
                  <li><a href="../author/patty-caya/index.php">Patty Caya</a></li>
                  
                  <li><a href="../author/susan-farrell/index.php">Susan Farrell</a></li>
                  
                  <li><a href="../author/therese-fessenden/index.php">Therese Fessenden</a></li>
                  
                  <li><a href="../author/kim-flaherty/index.php">Kim Flaherty</a></li>
                  
                  <li><a href="../author/sarah-gibbons/index.php">Sarah Gibbons</a></li>
                  
                  <li><a href="../author/aurora-harley/index.php">Aurora Harley</a></li>
                  
                  <li><a href="../author/kate-kaplan/index.php">Kate Kaplan</a></li>
                  
                  <li><a href="../author/page-laubheimer/index.php">Page Laubheimer</a></li>
                  
                  <li><a href="../author/angela-li/index.php">Angie Li</a></li>
                  
                  <li><a href="../author/hoa-loranger/index.php">Hoa Loranger</a></li>
                  
                  <li><a href="../author/kate-meyer/index.php">Kate Meyer</a></li>
                  
                  <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                  
                  <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                  
                  <li><a href="../author/kara-pernice/index.php">Kara Pernice</a></li>
                  
                  <li><a href="../author/christian-rohrer/index.php">Christian Rohrer</a></li>
                  
                  <li><a href="../author/amy-schade/index.php">Amy Schade</a></li>
                  
                  <li><a href="../author/katie-sherwin/index.php">Katie Sherwin</a></li>
                  
                  <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                  
                  <li><a href="../author/kathryn-whitenton/index.php">Kathryn Whitenton</a></li>
                  
                </ul>
              </section>
              
            </div>
            
          </div>

          
          <section class="hide-for-small-down">
            
              
              <h1>Recent Articles</h1>
              
              <ul>
                  
                  <li><a href="../seamless-cross-channel/index.php">Seamlessness in the Omnichannel User Experience</a></li>
                  
                  <li><a href="../brand-intention-interpretation/index.php">Experience Design: Bridging Brand Intention and Brand Interpretation</a></li>
                  
                  <li><a href="../eyetracking-tasks-efficient-scanning/index.php">Scanning Patterns on the Web Are Optimized for the Current Task</a></li>
                  
                  <li><a href="../flat-design-best-practices/index.php">Flat-Design Best Practices</a></li>
                  
                  <li><a href="../ux-user-stories/index.php">Accounting for UX Work with User Stories in Agile Projects</a></li>
                  
                  <li><a href="../index.php"><strong>See all articles&hellip;</strong></a></li>
              </ul>
              
            
          </section>
          

          
          
          <section class="hide-for-small-down">
            <h1>Popular Articles</h1>
            
            <ul>
                
                <li><a href="../usability-101-introduction-to-usability/index.php">Usability 101: Introduction to Usability</a></li>
                
                <li><a href="../top-10-mistakes-web-design/index.php">Top 10 Mistakes in Web Design</a></li>
                
                <li><a href="../how-users-read-on-the-web/index.php">How Users Read on the Web</a></li>
                
                <li><a href="../f-shaped-pattern-reading-web-content/index.php">F-Shaped Pattern For Reading Web Content</a></li>
                
                <li><a href="../ten-usability-heuristics/index.php">10 Usability Heuristics for User Interface Design</a></li>
                
                <li><a href="../intranet-design-2016/index.php">10 Best Intranets of 2016</a></li>
                
                <li><a href="../which-ux-research-methods/index.php">When to Use Which User-Experience Research Methods</a></li>
                
                <li><a href="../response-times-3-important-limits/index.php">Response Times: The 3 Important Limits</a></li>
                
                <li><a href="../why-you-only-need-to-test-with-5-users/index.php">Why You Only Need to Test with 5 Users</a></li>
                
                <li><a href="../page-fold-manifesto/index.php">The Fold Manifesto: Why the Page Fold Still Matters</a></li>
                
            </ul>
          </section>
          
          

          
            <section class="hide-for-small-down">
              
            </section>
          

      </section>
      

      
        <div class="small-12 medium-9 columns content-body">
          




<article class="l-subsection alertbox-article-content article-detail-bl">
  
  <header>
    <div class="l-subsection">
      <h1>Web 2.0 Can Be Dangerous...</h1>
      <div class="author-meta">by
        
          
            
            <a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a>
            
          
        on  December 17, 2007
        
      </div>

      

      
      <div class="topic-links">
        Topics:
        
          <ul>

  <li><a href="../../topic/strategy/index.php">Strategy</a></li>

  <li><a href="../../topic/web-usability/index.php">Web Usability</a></li>

</ul>
        
      </div>
      
    </div>
  </header>
  

  
  
  <section>
    <p class="lede"><strong>Summary:</strong> AJAX, rich Internet UIs, mashups, communities, and user-generated content often add more complexity than they&#39;re worth. They also divert design resources and prove (once again) that what&#39;s hyped is rarely what&#39;s most profitable.</p>
    <br />
  </section>
  
  

  
  <section id="article-body"><p>
 ... dangerous for your profits, that is. If you focus on over-hyped technology developments, you risk diverting resources from the
 <a class="old" href="../10-high-profit-redesign-priorities/index.php" title="Alertbox: 10 High-Profit Redesign Priorities">
  high-ROI design issues
 </a>
 that really matter to your users &mdash; and to your profits.
</p>
<p>
 Unlike some older technologies (notably,
 <a class="old" href="../flash-99-percent-bad/index.php" title="Alertbox: Flash, 99% Bad">
  Flash
 </a>
 and
 <a class="old" href="../pdf-unfit-for-human-consumption/index.php" title="Alertbox: PDF, Unfit for Human Consumption">
  PDF
 </a>
 ),
 <strong>
  Web 2.0 ideas are not
  <em>
   inherently
  </em>
  bad
 </strong>
 for users. They
 <em>
  can
 </em>
 be highly effective; we sometimes see examples of usability-enhancing Web 2.0 designs in our studies. But it&#39;s more common to find Web 2.0 ideas that either
 <strong>
  hurt
 </strong>
 users or simply
 <strong>
  don&#39;t matter
 </strong>
 to users&#39; core needs. While the latter case might seem innocent, irrelevant website &quot;enhancements&quot; diminish profits because they indicate a failure to focus on those simpler design issues that actually increase sales and leads.
</p>
<p>
 While there&#39;s no single definition of the much-abused &quot;Web 2.0&quot; term, I&#39;ll look at four trends that are often considered its defining elements:
</p>
<ul>
 <li>
  &quot;Rich&quot; Internet Applications (RIA)
 </li>
 <li>
  Community features, social networks, and user-generated content
 </li>
 <li>
  Mashups (using other sites&#39; services as a development platform)
 </li>
 <li>
  Advertising as the main or only business model
 </li>
</ul>
<h2>
 AJAX and &quot;Rich&quot; Internet UI: Too Much Complexity
</h2>
<p>
 There&#39;s no doubt that the pageview model of interaction provides a scaled-back UI. But this also means that it&#39;s a
 <em>
  simple
 </em>
 UI. When all they can do is click a link to get a new page,
 <strong>
  users know how to operate the UI
 </strong>
 . People are in control of their own user experience and thus focus on your content.
</p>
<p>
 &quot;Rich&quot; Internet UIs highlight the more flexible GUI design options that we&#39;ve enjoyed in personal computing since 1984. Such interfaces can work well, especially for
 <a class="new" href="../../courses/application-ux/index.php" title="Nielsen Norman Group: 2-day training tutorial on Application Usability">
  actual applications
 </a>
 that offer true functionality and thus require a full GUI. But if you&#39;re just designing a
 <strong>
  website
 </strong>
 , the more advanced UIs often
 <strong>
  confuse users
 </strong>
 more than they help. Why? Because users
 <a class="old" href="../feature-richness-and-user-engagement/index.php" title="Alertbox: Feature Richness and User Engagement">
  engage less with websites
 </a>
 than with apps. (And many applications are
 <a class="old" href="../ephemeral-web-based-applications/index.php" title="Alertbox: Ephemeral Web-Based Applications">
  ephemeral apps
 </a>
 that also have low user engagement.)
</p>
<p>
 Take the most famous example of rich UI: AJAX, which lets designers update part of a page, rather than taking users to an entirely new page. Because less data download is required, these smaller updates are typically faster, decreasing response times.
</p>
<p>
 Only a fool would deny the importance of
 <a class="old" href="http://www.useit.com/papers/responsetime.php" title="Overview of the 3 response time limits">
  response time
 </a>
 and download speeds for the Web user experience. After all, we&#39;ve known since 1968 that speedy interfaces feel better and support flow.
</p>
<p>
 So yes, faster is better. But only if users continue to understand what&#39;s happening. We recently tested about 100 e-commerce sites and found many problems with
 <strong>
  AJAX shopping carts
 </strong>
 . In particular, users often
 <strong>
  overlooked modest changes
 </strong>
 , such as when they added something to the cart and it updated only a small area in a corner of the screen.
</p>
<p>
 It&#39;s deadly for e-commerce sites when users can&#39;t operate the shopping cart, so it&#39;s usually best to stick to
 <a class="old" href="../../reports/ecommerce-ux-shopping-carts-checkout-registration/index.php" title="Nielsen Norman Group report: Usability guidelines for shopping carts and checkout on e-commerce sites">
  simple shopping-cart designs
 </a>
 that everybody understands.
</p>
<p>
 To get the required response times, spend your money on bigger servers and better hosting providers. And
 <strong>
  stick fewer gadgets on each page
 </strong>
 : these days, slow response times are often caused by too many complex, dynamic design elements that eat up server time.
</p>
<p>
 As illustrated in a sidebar, an
 <a class="new" href="../good-ajax-an-example/index.php" title="Alertbox sidebar: Good AJAX -- An Example">
  AJAX feature can work
 </a>
 well on a website. And our testing did find one usable AJAX shopping cart. As always, the real question is not technology, but usability. If you use technology right, it can help sales. Still, the
 <strong>
  risk is typically too high
 </strong>
 with new technology because best practices haven&#39;t jelled yet. You
 <strong>
  can&#39;t just emulate designs you see around the Web
 </strong>
 &mdash; they&#39;re likely to be bad because they were hacked together by geeks drunk on the newest and coolest tech. And, sadly, &quot;newest and coolest&quot; usually translates into &quot;untried and unusable&quot; &mdash; and thus money-losing.
</p>
<h2>
 Community and User-Generated Content: Too Few Users
</h2>
<p>
 User-generated content can be a great supplement to your own content. The most famous example is Amazon&#39;s book reviews, which date from 1996 (not exactly &quot;2.0.&quot;). Communities, which were the main recommendation in the 1997 book
 <a class="out" href="http://www.amazon.com/dp/0875847595?tag=useitcomusablein" title="Amazon.com: John Hagel III and Arthur G. Armstrong's book 'Net Gain: Expanding Markets Through Virtual Communities'">
  <cite>
   Net.Gain
  </cite>
 </a>
 , are also an old idea.
</p>
<p>
 <a class="old" href="../social-networking-on-intranets/index.php" title="Alertbox: Social Networking on Intranets">
  Community features are particularly useful on intranets
 </a>
 , and many of the
 <a class="old" href="../intranet-design/index.php" title="Alertbox: Year's 10 Best Intranets">
  Intranet Design Annual
 </a>
 winners offer them. The reasons communities work better on intranets also explains why they&#39;re often less useful on the open Internet:
</p>
<ul>
 <li>
  A company&#39;s employees are an actual community with a crucial
  <strong>
   shared interest
  </strong>
  : succeeding in business.
 </li>
 <li>
  Employees are pre-vetted: they&#39;ve been hired and thus presumably have a
  <strong>
   minimum quality level
  </strong>
  . In contrast, on the Web, most people are bozos and not worth listening to.
 </li>
 <li>
  Although some intranet communities &mdash; such as those around internal classified ads &mdash; are aimed at lightening up the workplace, most intranet communities are
  <strong>
   tightly focused
  </strong>
  on company projects. Discussions stay on topic rather than wandering all over the map.
 </li>
 <li>
  Intranet users are
  <strong>
   accountable
  </strong>
  for their postings and care about their reputation among colleagues and bosses. As a result, postings aim to be productive instead of destructive or flaming.
 </li>
 <li>
  Small groups of people who know each other are less susceptible to
  <strong>
   social loafing
  </strong>
  , so more users contribute to intranet community features. In contrast, Internet communities suffer from
  <a class="old" href="../participation-inequality/index.php" title="Alertbox: Participation Inequality, Encouraging More Users to Contribute">
   participation inequality
  </a>
  , where most users never contribute and the most active 1% of people dominate the discussions.
 </li>
</ul>
<p>
 Realistically, most
 <strong>
  business tasks are too boring
 </strong>
 to support community features. The fact that the city Sanitation Department will pick up Christmas trees sometime after December 25 isn&#39;t likely to inspire a longing to discuss shared experiences on the department&#39;s site. Users will visit the site to find the pick-up dates and rules. Nonetheless, the Christmas tree pick-up page is an example of how
 <a class="old" href="../government-non-profits-usability-roi/index.php" title="Alertbox: Do Government Agencies and Non-Profits Get Return on Investment From Usability?">
  government websites can offer taxpayers great ROI
 </a>
 : if done right, this one page will save the city from answering endless phone calls &mdash; each costing $10 or more. Often, such boring, workhorse stuff is where the money is.
</p>
<h2>
 Mashups: Co-Branded Confusion
</h2>
<p>
 One of the defining ideas of &quot;Web as platform&quot; is that it lets developers merge the features of different sites into a single service. If you&#39;re a business, doing this is dangerous for two reasons:
</p>
<ul>
 <li>
  <strong>
   Co-branding confuses
  </strong>
  users, who find it much easier to understand the simpler model of one site = one company. Users are confused when other companies sell on Amazon. Similarly, our studies of
  <a class="old" href="../investor-relations-ir/index.php" title="Alertbox: Investor Relations (IR) Website Design">
   investor-relations sites
  </a>
  found that individual investors were confused when a corporate site&#39;s IR area linked to a third-party site for quarterly reports and the like.
 </li>
 <li>
  Having part of your site effectively
  <strong>
   under another company&#39;s control
  </strong>
  means that you&#39;re at that company&#39;s mercy if they decide to change the terms of service. For example, the outside provider might decide to throw in advertisements from your competitors. Not material you want to promote to your hot prospects.
 </li>
</ul>
<p>
 Finally, a &quot;mashed&quot; service will never have usability as good as one that&#39;s
 <strong>
  designed specifically for your needs
 </strong>
 . In
 <a class="old" href="../store-finders-and-locators/index.php" title="Alertbox: Helping Users Find Physical Locations">
  testing store finders and other locators
 </a>
 , we found that the most usable maps were custom drawn to highlight a specific store and included surrounding landmarks, recommended parking facilities, and information on how to best access the location using public transportation. A general mapping service doesn&#39;t know your customers&#39; needs and thus can&#39;t draw the map that will bring in the most customers.
</p>
<p>
 Of course, if you&#39;re a small company, borrowing features from an outside service can help you add functionality to your site. But if you&#39;re big, the profits from an optimal user experience usually beat the cost of its creation.
</p>
<h2>
 Advertising-Funded Business Models: Bubble 2.0
</h2>
<p>
 The number of companies that chase the same advertising dollars as their only business model is a sure sign that we&#39;re at the peak of Bubble 2.0. It would be much more sustainable if companies aimed to create services that users valued enough to pay for.
</p>
<p>
 Right now, considerable advertising money is sloshing through the Web because most
 <strong>
  marketing managers remain clueless
 </strong>
 about how it works. They think that because search advertisements generate lots of business, other Web ads must work just as well.
 <em>
  What a fallacy
 </em>
 &mdash; brought on by ignorance of the basic Web user experience. People go to search engines when they&#39;re explicitly looking for a place to do business. This is why
 <a class="old" href="../search-engines-as-leeches-on-the-web/index.php" title="Alertbox: Search Engines as Leeches on the Web">
  search engines profit
 </a>
 from sucking up the work of content sites (where users exhibit strong
 <a class="old" href="../banner-blindness-old-and-new-findings/index.php" title="Alertbox: Banner Blindness, Old and New Findings">
  banner blindness
 </a>
 ).
</p>
<p>
 Marketing managers won&#39;t remain clueless forever. Sooner or later they&#39;ll discover that Web advertising offers almost no ROI. Only two forms of Web ads actually work:
 <a class="old" href="../designing-web-ads-using-click-through-data/index.php" title="Alertbox: Designing Web Ads Using Click-Through Data">
  search ads
 </a>
 and classified ads (such as eBay and real estate listings). A third type of Internet advertising that
 <em>
  might
 </em>
 work are video ads, because video is a linear media form (in contrast to nonlinear website navigation). At this point, we don&#39;t have enough user research about Internet video to say for sure.
</p>
<h2>
 Hyped Websites: Unrepresentative for Business Sites
</h2>
<p>
 In 1997, I said that
 <a class="old" href="../the-fallacy-of-atypical-web-examples/index.php" title="Alertbox: The Fallacy of Atypical Web Examples">
  atypical examples are poor indicators
 </a>
 of what you should do with your site. In
 <a class="old" href="../hyped-web-stories-are-irrelevant/index.php" title="Alertbox: Hyped Web Stories Are Irrelevant">
  2006, I said this again, using examples
 </a>
 that were widely hyped that year. Sadly, many Internet managers
 <strong>
  continue to make this mistake
 </strong>
 and ask their team to emulate approaches highlighted in the news stories they read. So, I&#39;ll say it for the third time:
</p>
<ul>
 <li>
  If you&#39;re a mainstream business site (including government and non-profit sites),
  <strong>
   your user experience needs are very different
  </strong>
  than those of the few hot sites that attract all the attention.
 </li>
</ul>
<p>
 As an example, a smaller e-commerce site should
 <a class="old" href="../amazon-no-e-commerce-role-model/index.php" title="Alertbox: Amazon, No Longer the Role Model for E-Commerce Design">
  not emulate Amazon.com&#39;s design
 </a>
 . While they do many things right, there are also many ways in which Amazon deviates from the
 <a class="old" href="../../reports/ecommerce/index.php" title="Nielsen Norman Group report: E-commerce usability guidelines">
  mainstream guidelines for e-commerce user experience
 </a>
 . They can (and probably
 <em>
  should
 </em>
 ) deviate because of their unique position (which you don&#39;t have).
</p>
<p>
 By definition, any
 <strong>
  website that gets extensive press coverage is unrepresentative
 </strong>
 for the vast majority of sites. The media covers only &quot;exciting&quot; stories &mdash; not everyday business.
</p>
<p>
 The most-hyped site right now,
 <strong>
  Facebook, is the &quot;Iron Chef&quot; of the Internet
 </strong>
 . The
 <a class="out" href="http://www.tv.com/iron-chef/show/16226/summary.php" title="TV.com: overview of the 'Iron Chef' television series from Fuji TV and the Food Network">
  Iron Chef
 </a>
 competition makes for great TV, but has nothing to do with running a restaurant as a successful business. After all, chefs aren&#39;t typically assigned a &quot;mystery ingredient&quot; shortly before dinnertime that they must feature throughout a multi-course meal. Broccoli ice cream? Not if you want to make money and bring the diners back.
</p>
<p>
 Like Iron Chef, Facebook has much drama that makes for good press coverage, but most of its features are worthless for a
 <a class="old" href="../b2b-usability/index.php" title="Alertbox: Business-to-Business Usability">
  B2B site
 </a>
 that, say, is trying to sell forklift trucks to 50-year-old warehouse managers. Instead of adding Facebook-like features that let users &quot;bite&quot; other users and turn them into zombies, the B2B site would get more sales by offering clear prices, good product photos, detailed specs, convincing whitepapers, an easily navigable
 <a class="old" href="../fixing-information-architecture/index.php" title="Alertbox: 6 Ways to Fix a Confused Information Architecture">
  information architecture
 </a>
 , and an
 <a class="old" href="../e-mail-newsletters-usability/index.php" title="Alertbox: Email Newsletters, Surviving Inbox Congestion">
  email newsletter
 </a>
 .
</p>
<h2>
 Adapt a Few Web 2.0 Features, but Focus on Core Services
</h2>
<p>
 As I&#39;ve noted, there are many reasons not to get caught up in the Web 2.0 hype. Still, Web 2.0 has some good ideas that can benefit mainstream websites. The problem is that different sites need
 <em>
  different
 </em>
 subsets of the Web 2.0 features.
</p>
<p>
 As an extremely rough guideline, here&#39;s the percentage of Web 2.0 infusion that might benefit different types of user experience:
</p>
<ul>
 <li>
  <strong>
   Informational/Marketing
  </strong>
  website (whether corporate, government, or non-profit): 10%
 </li>
 <li>
  <strong>
   E-commerce
  </strong>
  site: 20%
 </li>
 <li>
  <strong>
   Media
  </strong>
  site: 30%
 </li>
 <li>
  <strong>
   Intranets:
  </strong>
  40%
 </li>
 <li>
  <strong>
   Applications:
  </strong>
  50%
 </li>
</ul>
<p>
 Applications score so high because users perform actions repeatedly and thus truly benefit from rich UI possibilities. In contrast, mainstream websites have very
 <strong>
  few repeated actions
 </strong>
 that justify the added complexity of a full GUI&#39;s shortcuts.
</p>
<p>
 For website usability, the problem is not whether a specific operation takes 1 second or 10 seconds; people typically perform each operation only once or twice. The problem for websites is the 5&ndash;10 minutes users lose when they do something wrong because the site is too complicated. (After such an experience, they usually leave &mdash; and you lose the business.)
 <strong>
  Simplicity is more important than efficiency
 </strong>
 for done-once actions.
</p>
<p>
 Take
 <strong>
  drag-and-drop
 </strong>
 : when used right, it can expedite application interactions. But on a corporate site, the lack of perceived affordances can cause users to overlook important options. (And, of course, when used
 <em>
  wrong
 </em>
 , fancy interaction techniques also doom applications, which is why it&#39;s important to follow
 <a class="new" href="../../courses/application-ux/index.php" title="Nielsen Norman Group: 2-day training tutorial on Application Usability">
  application usability guidelines
 </a>
 if that&#39;s your game. The fact that a technique is &quot;2.0&quot; is more likely to harm than help your users if implemented with poor usability.)
</p>
<p>
 The bottom line? While a modest 2.0 infusion can be beneficial, advanced features are rarely the most important contributor to good user experience or profitable websites. If you get caught up in the hype, you divert attention and resources from the simpler things that really matter. This
 <strong>
  opportunity cost
 </strong>
 is the real reason to take it easy on Web 2.0.
</p>
<p>
 Before throwing spending money at &quot;2.0&quot; features, make sure that you have
 <em>
  all
 </em>
 the &quot;1.0&quot; requirements working to perfection. Of the 149,784,002 sites on the Web, maybe a handful can make this claim. Most sites don&#39;t even use the
 <a class="old" href="../web-writing-use-search-keywords/index.php" title="Alertbox: Use Old Words When Writing for Findability">
  customers&#39; terminology in headlines and page titles
 </a>
 &mdash; if you want one quick action item to improve site profitability through better SEO ranking, more clickthroughs, and better understanding of your services, rewriting the first two words of your
 <a class="old" href="../microcontent-how-to-write-headlines-page-titles-and-subject-lines/index.php" title="Alertbox: Microcontent, How to Write Headlines, Page Titles, and Subject Lines">
  microcontent
 </a>
 will beat any technology any day.
</p>
</section>
  

  
  <section>
    
        
    
  </section>
  
  
  
  
  

  


  <section>
    <br />
    <p class="share"><b>Share <span class="hide-for-small-down">this article</span>:</b> <a href="https://twitter.com/intent/tweet?url=http://www.nngroup.com/articles/web-20-can-be-dangerous/&amp;text=Web%202.0%20Can%20Be%20Dangerous...&amp;via=nngroup">Twitter</a> | <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=http://www.nngroup.com/articles/web-20-can-be-dangerous/&amp;title=Web%202.0%20Can%20Be%20Dangerous...&amp;source=Nielsen%20Norman%20Group">LinkedIn</a> | <a href="https://plus.google.com/share?url=http://www.nngroup.com/articles/web-20-can-be-dangerous/">Google+</a> | <a href="mailto:?subject=NN/g Article: Web 2.0 Can Be Dangerous...&amp;body=http://www.nngroup.com/articles/web-20-can-be-dangerous/">Email</a></p>
    <hr />
  </section>

  <footer class="l-subsection">
    
    
    <div class="learn-more">
      <h2>Learn More</h2>
      <p><a href="../subscribe/index.php">Subscribe to the weekly newsletter</a> to get notified about future articles.</p>
      </div>
    
    

    <div class="row show-for-small-down related-content">
      
      
      

      
      
        <div class="column small-12 topic-links">All articles about:
          <ul>
            
            <li><a href="../../topic/strategy/index.php">Strategy</a></li>
            
            <li><a href="../../topic/web-usability/index.php">Web Usability</a></li>
            
          </ul>
        </div>
      
      

      
      
        <div class="column small-12">
          <h3>UX Conference Training Courses</h3>
           <ul class="no-bullet">
    
        <li><a href="../../courses/ux-basic-training/index.php">UX Basic Training</a></li>
    
        <li><a href="../../courses/journey-mapping/index.php">Journey Mapping to Understand Customer Needs</a></li>
    
        <li><a href="../../courses/facilitating-ux-workshops/index.php">Facilitating UX Workshops</a></li>
    
        <li><a href="../../courses/human-mind/index.php">The Human Mind and Usability</a></li>
    
        <li><a href="../../courses/analytics-and-user-experience/index.php">Analytics and User Experience</a></li>
    
</ul>
        </div>
      
      

      
      
        <div class="small-12 column">
          <h3>Research Reports</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../reports/usability-return-on-investment-roi/index.php">Return on Investment (ROI) for Usability</a></li>
              
            
              
                <li><a href="../../reports/b2b-websites-usability/index.php">B2B Website Usability for Converting Users into Leads and Customers</a></li>
              
            
              
                <li><a href="../../reports/strategic-design-faqs/index.php">Strategic Design for Frequently Asked Questions</a></li>
              
            
          </ul>
        </div>
      
      

      
      
        <div class="small-12 column">
          <h3>Online Seminars</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../online-seminars/fundamental-flaw-flat-design/index.php">The Fundamental Flaw in Flat Design</a></li>
              
            
              
                <li><a href="../../online-seminars/millennials-online/index.php">Millennials Online</a></li>
              
            
              
                <li><a href="../../online-seminars/ux-real-world/index.php">UX in the Real World</a></li>
              
            
              
                <li><a href="../../online-seminars/how-change-your-corporate-culture/index.php">How to Change Your Corporate Culture to be User First</a></li>
              
            
              
                <li><a href="../../online-seminars/university-websites/index.php">University Website UX Essentials</a></li>
              
            
          </ul>
        </div>
      
      
    </div>

    <div class="hide-for-small-down row related-content">
      <div class="medium-6 columns">
        
        
          <div>
            <h3>Articles</h3>
            <ul class="no-bullet">
              
                
                <li><a href="../simplicity-vs-choice/index.php">Simplicity Wins over Abundance of Choice</a></li>
                
              
                
                <li><a href="../outcomes-vs-features/index.php">Minimize Design Risk by Focusing on Outcomes not Features</a></li>
                
              
                
                <li><a href="../radical-incremental-redesign/index.php">Radical Redesign or Incremental Change?</a></li>
                
              
                
                <li><a href="../four-bad-designs/index.php">Four Bad Designs</a></li>
                
              
                
                <li><a href="../design-priorities/index.php">Growing a Business Website: Fix the Basics First</a></li>
                
              
            </ul>
          </div>
        
        

        
          
          <div>
            <h3>Research Reports</h3>
            <ul class="no-bullet">
              
                
                  <li><a href="../../reports/usability-return-on-investment-roi/index.php">Return on Investment (ROI) for Usability</a></li>
                
              
                
                  <li><a href="../../reports/b2b-websites-usability/index.php">B2B Website Usability for Converting Users into Leads and Customers</a></li>
                
              
                
                  <li><a href="../../reports/strategic-design-faqs/index.php">Strategic Design for Frequently Asked Questions</a></li>
                
              
            </ul>
          </div>
          
        

      </div>
      <div class="medium-6 columns">
        
          
          <div>
            <h3>UX Conference Training Courses</h3>
             <ul class="no-bullet">
    
        <li><a href="../../courses/ux-basic-training/index.php">UX Basic Training</a></li>
    
        <li><a href="../../courses/journey-mapping/index.php">Journey Mapping to Understand Customer Needs</a></li>
    
        <li><a href="../../courses/facilitating-ux-workshops/index.php">Facilitating UX Workshops</a></li>
    
        <li><a href="../../courses/human-mind/index.php">The Human Mind and Usability</a></li>
    
        <li><a href="../../courses/analytics-and-user-experience/index.php">Analytics and User Experience</a></li>
    
</ul>
          </div>
          
        
        
          
          <div>
            <h3>Online Seminars</h3>
            <ul class="no-bullet">
            
                <li><a href="../../online-seminars/fundamental-flaw-flat-design/index.php">The Fundamental Flaw in Flat Design</a></li>
            
                <li><a href="../../online-seminars/millennials-online/index.php">Millennials Online</a></li>
            
                <li><a href="../../online-seminars/ux-real-world/index.php">UX in the Real World</a></li>
            
                <li><a href="../../online-seminars/how-change-your-corporate-culture/index.php">How to Change Your Corporate Culture to be User First</a></li>
            
                <li><a href="../../online-seminars/university-websites/index.php">University Website UX Essentials</a></li>
            
            </ul>
          </div>
          
        
      </div>
    </div>
  </footer>
</article>

        </div>

      

    
  </div>
  <div class="row">
    
      <div class="column small-12 show-for-small-down l-navrail announcements">
        
          
        
      </div>
    
    
  </div>

    </div>
    




<footer id="page-footer">
    <div class="row hide-for-print">
        <div class="column small-12 medium-6">
            <section class="footer-popular">
                
                <h1>Popular Topics</h1>
                <div style="float: left;  width: 15em; display: block;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/applications/index.php">Application Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/e-commerce/index.php">E-Commerce Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/eyetracking/index.php">Eyetracking </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/information-architecture/index.php">Information Architecture </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/intranets/index.php">Intranet Design </a></li>
	<li><a href="../../topic/interaction-design/index.php">Interaction Design </a></li>
</ul>
</div>

<div style="float: left;  width: 15em; display: block; list-style: none;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/mobile-and-tablet-design/index.php">Mobile and Tablet Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/credibility-persuasion/index.php">Persuasive Design</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/research-methods/index.php">Research Methods</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/user-testing/index.php">User Testing </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/web-usability/index.php">Web Usability </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/writing-web/index.php">Web Writing &amp; Content Strategy </a></li>
</ul>
</div>

<p>&nbsp;</p>

                
            </section>
        </div>

        <div class="column small-12 medium-6">
            <div class="row">
                <div class="column small-6">
                    <section class="footer-about">
                        <h1>Follow Us</h1>
                        <ul>
	<li><a href="../subscribe/index.php">Subscribe to our weekly UX newsletter</a>,&nbsp;Jakob Nielsen&#39;s Alertbox</li>
	<li>Visit Don Norman&#39;s website, <a href="http://www.jnd.org/"> www.jnd.org </a></li>
	<li>Visit Bruce Tognazzini&#39;s website, <a href="http://www.asktog.com/"> www.asktog.com </a></li>
	<li style="padding-top: 6px;"><a class="twitter-follow-button" data-show-count="false" data-size="large" href="https://twitter.com/nngroup"><img alt="Follow @nngroup on Twitter" height="20" src="https://media.nngroup.com/media/editor/2016/12/30/twitter-follow-button.png" style="width: 117px; height: 20px;" width="117" /></a></li>
	<li style="padding-top: 6px;"><img alt="" height="14" src="https://media.nngroup.com/media/editor/2016/07/26/feed-icon-14x14.png" style="vertical-align:text-top;width:14px;margin-right:.25em;" width="14" /><a href="../../feed/rss/index.php">Subscribe to our RSS feed</a></li>
</ul>

                    </section>
                </div>

                <div class="column small-6">
                    <section class="footer-about right-column">
                        <h1>About
</h1>
                        <ul class="no-bullet">
	<li><a href="../../about/contact/index.php">Contact information</a></li>
	<li><a href="../../about/index.php">About Us</a></li>
	<li><a href="../../about/why-nng/index.php">Why NN/g? </a></li>
	<li><a href="../../return-policy/index.php">Return Policy</a></li>
</ul>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="copyright column small-12">
            <p><a href="../../copyright-and-reprint-info/index.php">Copyright</a> &copy; 1998-2017 Nielsen Norman Group, All Rights Reserved.</p>
        </div>
    </div>
</footer>


    


        <script src="https://media.nngroup.com/static/vendor/fastclick/js/fastclick.min.js"></script>
        <script src="https://media.nngroup.com/static/vendor/foundation/js/foundation.min.js"></script>
        <script src="http://media.nngroup.com/static/js/app.js"></script>
        <script src="http://media.nngroup.com/static/js/scroll-to-fixed.js"></script><script>var externalLinks = document.querySelectorAll('a[href^="http"]');for (var i = externalLinks.length-1; i >= 0; i--) {externalLinks[i].addEventListener("click", function(event) { alert("External links are disabled for this test."); event.preventDefault(); }, false);}var mailLinks = document.querySelectorAll('a[href^="mailto"]');for (var i = mailLinks.length-1; i >= 0; i--) {mailLinks[i].addEventListener("click", function(event) { alert("Email links are disabled for this test."); event.preventDefault(); }, false);}var externalForms = document.querySelectorAll('form');for (var i = externalForms.length-1; i >= 0; i--) {externalForms[i].addEventListener("submit", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false); externalForms[i].addEventListener("click", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false);}</script>

        <script>
            
            $(function() {
                $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
                $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
            });
        </script>

        
<script src="https://media.nngroup.com/static/js/articles.js"></script>


        
        <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1072699960;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072699960/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
    </body>

<!-- Mirrored from www.nngroup.com/articles/web-20-can-be-dangerous/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:15:12 GMT -->
</html>
