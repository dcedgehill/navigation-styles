<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->





    
<!-- Mirrored from www.nngroup.com/articles/readers-comments-on-the-end-of-legacy-media/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:16:02 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1026.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"licenseKey":"1a30ef9931","queueTime":2,"beacon":"bam.nr-data.net","errorBeacon":"bam.nr-data.net","applicationID":"2048992","transactionName":"NFxSMRBUX0EDUkIPDA0WdhAMVkVbDV8ZBxEXUFMJB0YfRAtUQRVZIktEDAFZVHYHRVcPD01eVRE=","applicationTime":404,"agent":""}</script>
        <title>Readers&#39; Comments on the End of Legacy Media</title><meta property="og:title" content="Readers&#39; Comments on the End of Legacy Media" />
  
        
        <meta name="description" content="Radio and linear media for storytelling will survive but will be integrated into the Internet and accessed over tablet computers, car computers, and maybe wearables.">
        <meta property="og:description" content="Radio and linear media for storytelling will survive but will be integrated into the Internet and accessed over tablet computers, car computers, and maybe wearables." />
        
  
        
	
        
        <meta name="keywords" content="tablets, tablet computers, wearable computer, FaxView, radio, auditory content, screen size, storytelling, stories, linear media">
        

        

        <meta name="robots" content="noarchive">
        <meta name="viewport" content="width=device-width">

        <link rel="search" type="application/opensearchdescription+xml" title="NN/g Search" href="../../opensearch.xml" />
        <link rel="shortcut icon" href="http://media.nngroup.com/static/img/favicon.ico" />

        <link rel="stylesheet" href="https://media.nngroup.com/static/vendor/font-awesome/css/font-awesome.min.css">

        
        <link rel="stylesheet" href="https://media.nngroup.com/static/css/7232a960f1df.css" type="text/css" />
        <link rel="stylesheet" href="http://fast.fonts.com/t/1.css?apiType=css&amp;projectid=c5846260-9def-46ec-9407-0f1349bd2d64" type="text/css">

        <script src="https://media.nngroup.com/static/vendor/modernizr/js/modernizr.min.js"></script>

        <script type="text/javascript" src="https://media.nngroup.com/static/js/3283be5def74.js"></script>
        

        

        
        
        <!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=229034,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('http://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->


<!-- Google Analytics Tracking Code for http://nngroup.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23132681-1', 'nngroup.com');
  ga('require', 'linkid', 'https://www.nngroup.com/articles/readers-comments-on-the-end-of-legacy-media/linkid.js');
  ga('require', 'displayfeatures');

// Optimizely Universal Analytics Integration
 window.optimizely = window.optimizely || [];
 window.optimizely.push("activateUniversalAnalytics");

  ga('send', 'pageview');

</script>
<script>
/**
* Function to track a click on an outbound link in Google Analytics.
*/
function trackOutboundLink(label, url) {
    if(window.ga && ga.loaded) {
        ga('send', 'event', 'outbound', 'click', label, {'hitCallback': function(){document.location = url;}
        });
    } else {
        document.location = url;
    }
}
/**
* Function to track a click on a link in the article announcement area.
*/
$('#articleAnnouncement a').on('click', function(){
  ga('send', 'event', 'link', 'click', 'articleAnnouncement');
});
</script>

<script type="text/javascript">
 setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2421.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



<!-- Hotjar Tracking Code for http://nngroup.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:121605,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

        <script src="https://media.nngroup.com/static/js/analytics/track_outbound_link.js"></script>
        
        
        
    </head>
    <body class="location-articles articles-detail">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
    <header id="page-header">
        <div id="cart" class="shopping-cart mobile-cart show-for-small-down"></div>
        <div class="row">
            <div id="masthead" class="masthead small-12 medium-7 column">

                <!-- The button to toggles menu on small screen -->

                <h1><a href="../../index.php"><img src="http://media.nngroup.com/static/img/logo-wordmark.png" width="74" height="30"> Nielsen <span class="norman">Norman</span> Group</a></h1>
                <p class="tagline show-for-medium-up">Evidence-Based User Experience Research, Training, and Consulting</p>

                <!-- The button to toggles form search on small screen -->


                <div style="clear: right;width:  100%;height: 1px;"></div><?php define('ROOT_PATH',$_SERVER["DOCUMENT_ROOT"] . '/apps/navigation_styles/navigations/1'); ?><?php include(ROOT_PATH)."/navigation.php"; ?>

                <!-- Drop down menu on small screen -->
                <div id="hideNav" class="show-for-small-down">
                  
                  <ul class="menu-content">
                    <li><a href="../../index.php">Home</a></li>
                    <li class="has-submenu">
                      <a>Training</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../training/index.php">Overview</a></li>
                        
<li id="nav-ux-conference"><a href="../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../online-seminars/index.php"><span>Online Seminars</span></a></li>

                      </ul>
                    </li>
                    <li class="has-submenu">
                      <a>Consulting</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
  <li><a href="../../consulting/index.php">Overview</a></li>
  
  <li><a href="../../consulting/benchmarking/index.php">Benchmarking</a></li>
  
  <li><a href="../../consulting/expert-design-review/index.php">Expert Design Review</a></li>
  
  <li><a href="../../consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
  
  <li><a href="../../consulting/usability-testing/index.php">Usability Testing </a></li>
  
  <li><a href="../../consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
  
  <li><a href="../../in-house-training/index.php">In-House Training</a></li>
</ul>

                    </li>

                    <li><a href="../../reports/index.php">Reports</a></li>
                    <li><a href="../index.php">Articles</a></li>
                    <li class="has-submenu"><a>About NN/G</a><i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../about/index.php">Overview</a></li>
                        <li><a href="../../people/index.php">People</a></li>
                        <li><a href="../../about/why-nng/index.php">Why NN/g?</a></li>
                        <li><a href="../../about/contact/index.php">Contact</a></li>
                        <li><a href="../../news/index.php">News</a></li>
                        <li><a href="../../about/history/index.php">History</a></li>
                        <li><a href="../../books/index.php">Books</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                </div>

                <hr class="header-divider show-for-small-only"/>


            </div>
       </div>

        <div id="hideNav">            <nav class="primary-nav small-12 column">
                <ul>
                    <li><a class="navitem" id="nav-home" href="../../index.php">Home</a></li>
                    <li><a class="navitem" id="nav-training" href="../../training/index.php">Training</a></li>
                    <li><a class="navitem" id="nav-consulting" href="../../consulting/index.php">Consulting</a></li>
                    <li><a class="navitem" id="nav-reports" href="../../reports/index.php">Reports</a></li>
                    <li><a class="navitem" id="nav-articles" href="../index.php">Articles</a></li>
                    <li><a class="navitem" id="nav-about" href="../../about/index.php">About NN/G</a></li>
                </ul>
            </nav>
            <nav class="secondary-nav small-12 medium-12 large-12 columns">
                
                
            </nav>
        </div>
    </header>
    <div class="l-content">
    
  

  

  <div class="row">
    
      
      <section class="small-12 medium-3 columns collapsable-content l-navrail articles-left-column">
        <h1 class="show-for-small-down collapse-link mobile-browse-header">Browse by Topic and Author</h1>
          <div class="row collapsable">

            
            <div class="small-6 medium-12 columns">
              <section id="article-popular-topics" style="display: none;">
                
                <h1>Topics</h1>
                <ul id="article-popular-topics">
                    
                    <li>
                        
                        <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/intranets/all/index.php">Intranets</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/user-testing/all/index.php">User Testing</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                        
                    </li>
                    
                    <li><a href="#" id="article-see-all-topics"><strong>See all topics&hellip;</strong></a></li>
                </ul>
                
              </section>
              <section id="article-all-topics" style="display: none;">
                
                <h1>
                  <span class="show-for-medium-up">All Article Topics</span>
                  <span class="show-for-small-down">All Topics</span>
                  (<a href="#" id="article-hide-all-topics">hide</a>)
                </h1>
                <ul>
                  
                  <li>
                    
                    <a href="../../topic/accessibility/all/index.php">Accessibility</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/agile/all/index.php">Agile</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/analytics-and-metrics/all/index.php">Analytics &amp; Metrics</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/applications/all/index.php">Application Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/b2b-websites/all/index.php">B2B Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/behavior-patterns/all/index.php">Behavior Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/branding/all/index.php">Branding</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/content-strategy/all/index.php">Content Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/corporate-websites/all/index.php">Corporate Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/design-patterns/all/index.php">Design Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-design-process/all/index.php">Design Process</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/email/all/index.php">Email</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/eyetracking/all/index.php">Eyetracking</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/heuristic-evaluation/all/index.php">Heuristic Evaluation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/human-computer-interaction/all/index.php">Human Computer Interaction</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-ideation/all/index.php">Ideation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/information-architecture/all/index.php">Information Architecture</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/interaction-design/all/index.php">Interaction Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/international-users/all/index.php">International Users</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/intranets/all/index.php">Intranets</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-management/all/index.php">Management</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/navigation/all/index.php">Navigation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/non-profit-websites/all/index.php">Non-Profit Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/personas/all/index.php">Personas</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/persuasive-design/all/index.php">Persuasive Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/prototyping/all/index.php">Prototyping</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/psychology-and-ux/all/index.php">Psychology and UX</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/research-methods/all/index.php">Research Methods</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/search/all/index.php">Search</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/social-media/all/index.php">Social Media</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/strategy/all/index.php">Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/user-testing/all/index.php">User Testing</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-teams/all/index.php">UX Teams</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/visual-design/all/index.php">Visual Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/writing-web/all/index.php">Writing for the Web</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/young-users/all/index.php">Young Users</a>
                    
                  </li>
                  
                </ul>
                
              </section>
            </div>
            <script>
              /* Determine which topics list will be displayed: the full or the
               * partial.  We are inlining this close to the list itself to
               * prevent rendering delays on the client
               */
              var topic_list = $.cookie("article-topics");

              var popular_topics = $('#article-popular-topics');
              var all_topics = $('#article-all-topics');

              if(topic_list == 1) {
                  popular_topics.hide();
                  all_topics.show();
              } else {
                  popular_topics.show();
                  all_topics.hide();
              }
            </script>
            

            
            <div class="small-6 medium-12 columns">
              
              <section id="article-initial-authors">
                  <h1>Author</h1>
                  <ul>
                    
                    <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                    
                    <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                    
                    <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                    
                    <li><a href="#" onClick="$('#article-initial-authors').hide(); $('#article-all-authors').show();"><strong>See all authors&hellip;</strong></a></li>
                  </ul>
              </section>
              <section id="article-all-authors" style="display: none;">
                <h1>
                  All Authors
                  (<a href="#" id="article-hide-all-authors">hide</a>)
                </h1>
                <ul>
                  
                  <li><a href="../author/raluca-budiu/index.php">Raluca Budiu</a></li>
                  
                  <li><a href="../author/patty-caya/index.php">Patty Caya</a></li>
                  
                  <li><a href="../author/susan-farrell/index.php">Susan Farrell</a></li>
                  
                  <li><a href="../author/therese-fessenden/index.php">Therese Fessenden</a></li>
                  
                  <li><a href="../author/kim-flaherty/index.php">Kim Flaherty</a></li>
                  
                  <li><a href="../author/sarah-gibbons/index.php">Sarah Gibbons</a></li>
                  
                  <li><a href="../author/aurora-harley/index.php">Aurora Harley</a></li>
                  
                  <li><a href="../author/kate-kaplan/index.php">Kate Kaplan</a></li>
                  
                  <li><a href="../author/page-laubheimer/index.php">Page Laubheimer</a></li>
                  
                  <li><a href="../author/angela-li/index.php">Angie Li</a></li>
                  
                  <li><a href="../author/hoa-loranger/index.php">Hoa Loranger</a></li>
                  
                  <li><a href="../author/kate-meyer/index.php">Kate Meyer</a></li>
                  
                  <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                  
                  <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                  
                  <li><a href="../author/kara-pernice/index.php">Kara Pernice</a></li>
                  
                  <li><a href="../author/christian-rohrer/index.php">Christian Rohrer</a></li>
                  
                  <li><a href="../author/amy-schade/index.php">Amy Schade</a></li>
                  
                  <li><a href="../author/katie-sherwin/index.php">Katie Sherwin</a></li>
                  
                  <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                  
                  <li><a href="../author/kathryn-whitenton/index.php">Kathryn Whitenton</a></li>
                  
                </ul>
              </section>
              
            </div>
            
          </div>

          
          <section class="hide-for-small-down">
            
              
              <h1>Recent Articles</h1>
              
              <ul>
                  
                  <li><a href="../seamless-cross-channel/index.php">Seamlessness in the Omnichannel User Experience</a></li>
                  
                  <li><a href="../brand-intention-interpretation/index.php">Experience Design: Bridging Brand Intention and Brand Interpretation</a></li>
                  
                  <li><a href="../eyetracking-tasks-efficient-scanning/index.php">Scanning Patterns on the Web Are Optimized for the Current Task</a></li>
                  
                  <li><a href="../flat-design-best-practices/index.php">Flat-Design Best Practices</a></li>
                  
                  <li><a href="../ux-user-stories/index.php">Accounting for UX Work with User Stories in Agile Projects</a></li>
                  
                  <li><a href="../index.php"><strong>See all articles&hellip;</strong></a></li>
              </ul>
              
            
          </section>
          

          
          
          <section class="hide-for-small-down">
            <h1>Popular Articles</h1>
            
            <ul>
                
                <li><a href="../usability-101-introduction-to-usability/index.php">Usability 101: Introduction to Usability</a></li>
                
                <li><a href="../top-10-mistakes-web-design/index.php">Top 10 Mistakes in Web Design</a></li>
                
                <li><a href="../how-users-read-on-the-web/index.php">How Users Read on the Web</a></li>
                
                <li><a href="../f-shaped-pattern-reading-web-content/index.php">F-Shaped Pattern For Reading Web Content</a></li>
                
                <li><a href="../ten-usability-heuristics/index.php">10 Usability Heuristics for User Interface Design</a></li>
                
                <li><a href="../intranet-design-2016/index.php">10 Best Intranets of 2016</a></li>
                
                <li><a href="../which-ux-research-methods/index.php">When to Use Which User-Experience Research Methods</a></li>
                
                <li><a href="../response-times-3-important-limits/index.php">Response Times: The 3 Important Limits</a></li>
                
                <li><a href="../why-you-only-need-to-test-with-5-users/index.php">Why You Only Need to Test with 5 Users</a></li>
                
                <li><a href="../page-fold-manifesto/index.php">The Fold Manifesto: Why the Page Fold Still Matters</a></li>
                
            </ul>
          </section>
          
          

          
            <section class="hide-for-small-down">
              
            </section>
          

      </section>
      

      
        <div class="small-12 medium-9 columns content-body">
          




<article class="l-subsection alertbox-article-content article-detail-bl">
  
  <header>
    <div class="l-subsection">
      <h1>Readers&#39; Comments on the End of Legacy Media</h1>
      <div class="author-meta">by
        
          
            
            <a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a>
            
          
        on  August 23, 1998
        
      </div>

      

      
      <div class="topic-links">
        Topics:
        
          <ul>

</ul>
        
      </div>
      
    </div>
  </header>
  

  
  
  

  
  <section id="article-body"><p>
 <em>
  Sidebar to
  <a href="../../people/jakob-nielsen/index.php" title="Author biography">
   Jakob Nielsen
  </a>
  &#39;s column on the
  <a href="../the-end-of-legacy-media-newspapers-magazines-books-tv-networks/index.php">
   end of legacy media
  </a>
  .
 </em>
</p>
<p>
 I have received some interesting user comments on my August Alertbox on the integration of media formats.
</p>
<h2>
 Tablet Computers Needed
</h2>
<p>
 <em>
  <a class="out" href="http://way.nu/" title="Personal home page">
   Jonathan Peterson
  </a>
  writes:
 </em>
</p>
<blockquote>
 I wasn&#39;t clear on exactly
 <em>
  what
 </em>
 you thought would be available in 5-10 years... Sure, my monitor and most applications will likely support anti-aliased (at worst) or 300dpi text (much better). And more people probably will use a computing device for news and magazine content (though I&#39;ve been using the Web as my main source of news since we launched CNN Interactive, supplemented by NPR radio during my commute, a bit of TV and US News and World report.).
 <p>
  But until I have the resolution you are talking about available in a form factor that is no bigger and heavier than a trade paperback, and no slower to boot than a magazine, I&#39;m not going to quit on books and magazines for occasional reading. Almost all my paper reading is in bed, on the couch (while playing with my 11 month old son, thus only in short snippets) and in my car at lunch. Or are you proposing that a 300dpi, wireless, ubiquitous computer-book-TV hybrid will be available in 5-10 years?
 </p>
 <p>
  Some sort of broadband, on-demand device will replace broadcast TV, but I&#39;m willing to be that some sort of live broadcast model will stay alive. Much of the success of TV&#39;s hit comes from people standing around the water-cooler talking about last night&#39;s
  <cite>
   Seinfeld
  </cite>
  episode. I wonder if there is some human need for shared experience which will continue to generate a demand for &quot;broadcast&quot;?
 </p>
</blockquote>
<p>
 <em>
  Jakob&#39;s reply:
 </em>
 I wanted to focus on the structure of the media landscape, which is why I glossed over the hardware issues. I do believe we will use tablet computers in various sizes from palm-sized to newspaper-sized. A book-sized tablet may be slightly heavier than a paperback, but no heavier than a hardcover. The tablets will have high-resolution screens: current high-resolution monitors are already flat-panel displays, so they would lend themselves well to tablets once they become cheaper (and possibly also less electricity-hungry). I also think the tablets will have a fairly high bandwidth wireless network as long as they are used indoors. For outdoors use, they will fall back on some form of cellular telephony network which will likely have lower bandwidth and be more expensive per packet. Thus, within my 10-years horizon, tablets will probably only deliver the full multimedia experience indoors.
</p>
<p>
 I agree that live transmissions will continue. There is something special about knowing that something is
 <em>
  live
 </em>
 , especially when watching sporting events (even though this is rather illogical, since a game ought to be equally exciting whether or not it is live, as long as you don&#39;t know the final score). But I don&#39;t think shows like
 <cite>
  Seinfeld
 </cite>
 will be shown &quot;live&quot; at a certain hour. Rather, a new episode will be made available in the database at a given time on a regular schedule. True fanatics may want to download it that exact second, but many other fans will view it some other time that evening. Others may wait and not download the show until the next day - and then only if it generated a positive buzz at the water-cooler.
</p>
<p>
 <em>
  A reader who prefers to remain anonymous writes:
 </em>
</p>
<blockquote>
 Ah, but some of us
 <em>
  like
 </em>
 newspapers -- a pencil and a crossword folded over, the comics, something to read in bed or in the park.
 <p>
  Remember all those predictions about the death of the movie theater when videos first came out? Did you ever read that Ray Bradbury story (it&#39;s part of
  <a href="http://www.amazon.com/exec/obidos/ISBN%3D0553278223/useitcomusableinA/" title="Amazon.com's book page - buy at 20% discount">
   The Martian Chronicles
  </a>
  ) about the guy who invented grass that never needed mowing?
 </p>
</blockquote>
<p>
 <em>
  Jakob&#39;s reply:
 </em>
 When read on a high-resolution tablet, you should be able to get all these benefits, including annotation and ability to do crosswords (with online crosswords, you can even get hints). Despite what I said above regarding lower bandwidth outdoors, you might still be able to enjoy the online newspaper in the park:
</p>
<ul>
 <li>
  Most of the content would not require video or other high-bandwidth media types, so you could still get the text and images. If you wanted to watch a certain video, it could either be delivered in poorer quality or it might be shipped to your home system for you to watch when you got back indoors.
 </li>
 <li>
  The tablet could also store a few gigabytes of video data that it had downloaded overnight. In 10 years, a typical high-end user will have a 6 Mbps line into their house, and such a line can download about 17 gigabytes while you sleep. Not a whole lot of video, but still enough to serve as supplementary video clips for the daily newspaper. Of course, this will only work if the network transmission costs are very low and if the micropayment system for these video clips are based on whether you watch them and not whether you download them. Pay-per-view instead of pay-per-download requires some form of trusted device and probably some new (but reasonably simple) encoding like the IBM Cryptolobes.
 </li>
</ul>
<p>
 <em>
  Ronni Bennett from
  <a class="out" href="http://www.cbs.com/" title="Company homepage">
   CBS New Media
  </a>
  writes:
 </em>
</p>
<blockquote>
 The coming integration seems obvious to me, but I agree that many corporate media types will likely lose the race before they realize they are in one.
 <p>
  But, I wonder if you mean to imply that print, (and television as entertainment) will disappear. High resolution monitors or not, sitting in front of a screen to read
  <cite>
   Gone With the Wind
  </cite>
  does not seem to me to be an appealing or likely prospect. People like to read in bed, at the beach, lying on the sofa, on the subway. One of the advantages of print media is its portability.
 </p>
 <p>
  Another reason for a book or long magazine piece in print is the ease with which you can flip back a page or two, or even to a previous chapter or article to check something. With scrolling or clicking backwards, there are rarely the visual cues to help a reader find a previous passage that there are in print - a large first letter at the beginning of a chapter; page numbers; or in a magazine, remembering that the paragraph you want to check was in the lower part of a verso page.
 </p>
 <p>
  I have been thinking lately that once online connections are fast and ubiquitous, and when large, flatscreen television screens are cheap enough to replace the boxy sets we use now, people will have both in their homes. They will use their computer monitors to do all the things we do now - check the headlines, watch a short video, download a backgrounder on terrorism, send email, look up something, etc.
 </p>
 <p>
  And while they are doing that, they may have a television show playing in the lower corner of the screen as we can do here at CBS while working on something else.
 </p>
 <p>
  But when it is time to watch a movie, people want a huge screen. Then they want to lie back and let the movie wash over them from their large, flatscreen TV as we do in movie theaters - losing ourselves in the experience.
 </p>
 <p>
  I do think the computer and the large TV screen will be interchangeable in that you can, if you wish, watch the movie in the corner of the computer screen while you&#39;re answering email; and you will also be able to answer mom&#39;s email immediately while you&#39;re watching
  <cite>
   Casablanca
  </cite>
  on the flatscreen.
 </p>
 <p>
  Different size screens serve different purposes. I don&#39;t mind the size of the screen on my PalmPilot when I&#39;m checking my schedule or looking for a phone number, but I sure don&#39;t want to read a magazine piece on it.
 </p>
 <p>
  So I&#39;m not convinced that in ten years &quot;all computer users will prefer using the Web over reading printed pages.&quot;
 </p>
</blockquote>
<p>
 <em>
  Jakob&#39;s reply:
 </em>
 I think you are right that there will be different sizes of screen. Watching a movie is a rather passive experience where the user wants to recline and watch in the company of others. Thus, a big, remotely mounted screen seems better than a magazine-sized, hand-held screen. But the film may still come in over the Internet and be chosen from a Web-based service that is searched and navigated on the magazine-sized screen. The two screens would presumably have some way to communicate such that the film would start playing on the large canvas.
</p>
<p>
 You are more than right in lamenting the difficulty in navigating information on a screen when compared with the ease of flipping pages in print. I would hope that user interface research would give us much better mechanisms over the next ten years. We have reached the end of the line with respect to making more and more copies of the 1984 Macintosh design. Just one idea that might address your comment about remembering the location of a paragraph on the page: if the computer had eye-tracking, it would remember which things you had actually read in an article. This could be used to facilitate reorientation: when you return to an article, it shows you where you left off. Or if you do a search for something you
 <em>
  know
 </em>
 you have seen, it could scope the search - not simply to
 <em>
  articles
 </em>
 you had already seen but to
 <em>
  paragraphs
 </em>
 within these articles that you had actually read.
</p>
<h3>
 Wearable Computers as Alternatives to Tablets
</h3>
<p>
 <em>
  Another reader who prefers to remain anonymous writes:
 </em>
</p>
<blockquote>
 Display quality and technology seems to play an important role in several of your columns. I would like to call your attention to a development that may alter your perceptions on this:
 <p>
  <strong>
   Wearable displays
  </strong>
  ... or virtual displays as some prefer. My own awareness of this comes from my interest in the emerging field of wearable computers. The biggest blips on the screen at the moment are:
 </p>
 <ul>
  <li>
   Motorola who is launching an OEM product in collaboration with
   <a class="out" href="http://www.kopin.com/html/cyberdisplay.php" title="CyberDisplay product page">
    Kopin
   </a>
   who are making the actual miniature LCD screens.
  </li>
  <li>
   Reflection Technology who are using a diode based technology.
  </li>
 </ul>
 Both these technologies have the potential to produce high resolution displays at prices well below regular monitors. More important, they both seem to have a greater potential for refinement and increasing resolution than both regular LCD and CRT technology.
</blockquote>
<p>
 <em>
  Jakob&#39;s reply:
 </em>
 Wearable computers are definitely an important component of the trend toward computers that go beyond a box sitting on a desk. I tend to be more enamored of tablets since wearables often seem too intrusive and geeky. Fine for mechanics who need to look up specs or procedures while crawling around in the guts of a 747, but not something you want in a business meeting or at home. Just look at people trying to read a fax through one of these devices (if the models hadn&#39;t been in a photoshoot, they would have been squinting) [the FaxView product must have died since the site and the photo are now off the Web as of Feb. 2000]. Wouldn&#39;t you rather read the fax on a super-high-res version of a PalmPilot-style tablet where you could annotate it with the pen? But I admit that the manufacturing physics may make it cheaper to make squintable displays than tablet displays with the same resolution. Also, wearables have huge benefits in applications where it is important for the user to
 <em>
  always
 </em>
 have access to something. Even the smallest non-wearable computer sometimes gets left at home.
</p>
<h2>
 Multicast vs. Broadcast vs. Narrowcast
</h2>
<p>
 <em>
  Kragen Sitaker writes:
 </em>
</p>
<blockquote>
 On the death of TV: you&#39;ve said a couple of times that there&#39;s absolutely no reason for Star Trek to come on at 9:00 of you&#39;re ready for it at 8:43, that this is just an artifact of old technology.
 <p>
  I think it&#39;s actually an artifact of limited bandwidth, which will be a problem into the foreseeable future. If I&#39;m watching Star Trek starting at 9:00, and my neighbor is watching it starting at 8:59, the broadcaster has to send out every packet of the show twice, a minute apart. If there are ten million people watching Star Trek (and there are already), all starting at different times, that means each packet has to be sent out ten million times.
 </p>
 <p>
  If bandwidth costs money, which it will for a while, it will be seven orders of magnitude less expensive to simply multi cast the show at a specified time. Many publishers of bandwidth-intensive things like video may find this a compelling difference, and choose multicast.
 </p>
 <p>
  The reason for the difference in cost is that, with multi cast, the production of multiple copies of the video is done as close to the ultimate destination as possible, so only one copy ever goes over a particular link, regardless of how many people want to receive it. Usenet also works this way, or used to, and didn&#39;t suffer the time constraints that IP multicast does, as it was a store-and-forward system, that you fetched articles from asynchronously.
 </p>
 <p>
  Perhaps Usenet-like technologies will provide a middle way that can achieve your vision.
 </p>
</blockquote>
<p>
 <em>
  Jakob&#39;s reply:
 </em>
 I agree that bandwidth costs money. In fact, I believe that flat-rate charging will be replaced with usage charges, though hopefully at a very low level. A one-hour video episode will be several gigabytes in high-quality video which might cost 10 cents for the transmission alone (plus, of course, another 10 cents micropayment to the content producers). If we look far enough into the future, bandwidth costs will be small enough to make such a scenario feasible. In the short term, the solution might be to disallow full flexibility in when to start the show. For example, a fresh copy of each episode might be multicast every minute. Thus, a user might have to wait as much as 59 seconds (30 seconds on average) before packets of the desired episode arrived. Slow response times might be acceptable when they only occur once in starting a one-hour show. Alternatively, the first few minutes of the show could be transmitted individually while transforming the video to play either slightly faster (to catch up with the previous minute&#39;s multicast) or slightly slower (to wait for the next minute&#39;s multicast to catch up). Thus, I agree that multicasting is a potential solution to the bandwidth problem, especially if used in a user-accommodating manner.
</p>
<h2>
 Advertising Needs Linear Media
</h2>
<p>
 <em>
  Dave Trautman from the
  <a class="out" href="http://www.atl.ualberta.ca/" title="University homepage">
   University of Alberta
  </a>
  writes:
 </em>
</p>
<blockquote>
 I read with interest your points about the end of linear - separated - unconnected media and on the whole I think you are on to something. But I think you forgot about advertising when considering both why these sources continue to be separate and why they will not soon become interactive.
 <p>
  Newspaper articles are sometimes referred to as the stuff which fills the space between the ads. My local newspapers are constantly fined for exceeding (daily) their maximum allotment of space for advertising as a percentage of page space. Limits on TV are also in place controlling just how many minutes of commercial time may be included in each hour of program content.
 </p>
 <p>
  Recent news seems to indicate the first try for banner advertising has resulted in an adjustment of behavior by the browsing public, heck I think even you pointed this out somewhere.
 </p>
 <p>
  Our problem is the revenue stream. Media companies have a lock on the revenue stream. They offer advertising organizations (and their clients) predictable audience behaviors and reliable eyeballs data. Until our dynamic media offer the same classification of audience and response we have an impasse.
 </p>
 <p>
  You do mention the death of &quot;companies&quot; and not of media and this is probably true to some extent but I have seen many a company morph into another enterprise with nary a hint of its former self in evidence. Perhaps we need to think about this further and to examine the role advertising has played in making these separate media outlets distinct.
 </p>
 <p>
  Without getting too involved in a reply I just thought I&#39;d point out where (in my view) streaming technology has failed to address the biggest challenge to gathering an audience which is to provide streaming cash in the other direction. Without meters and coin slots and retail outlets these other print and visual media cannot be distributed. Making them web savvy and hyperlinked doesn&#39;t provide for the cost of creation and distribution nor does it identify a significantly NEW stream of revenue to the &#39;publisher&#39; of it. Until the other ponds dry up each will remain separate and until the shipping of bits becomes a metered environment (which I do not yet endorse) there&#39;s not much hope for a revenue source from that end of the pipe.
 </p>
 <p>
  When the currency of everyday life transforms from bank notes and credit cards into a government underwritten exchange of information (gathered, processed, personal or manufactured) only then (I think) will we see the transformation you visualized. Unfortunately there are plenty of historical precedents for the slow and glacial forces of change in commerce and society.
 </p>
</blockquote>
<p>
 <em>
  Jakob&#39;s reply:
 </em>
 You are probably right, but since I
 <a class="old" href="../why-advertising-doesnt-work-on-the-web/index.php" title="Alertbox Sept. 1997: Why advertising doesn't work on the Web">
  don&#39;t believe in advertising on the Web
 </a>
 to begin with, I do not view this as a major limitation of non-linear media. I do share your concern that
 <a class="old" href="../the-case-for-micropayments/index.php" title="Alertbox Jan. 1998: Micropayments">
  micropayments
 </a>
 or other non-advertising payment systems will be slow in coming. One reason is that too many Internet strategists are in denial and cling to the hope that Web advertising can be made to work so that they can continue to think the way they are used to. I am sorry: Web advertising doesn&#39;t work because of the
 <em>
  fundamental
 </em>
 nature of the Web, so it&#39;s not a simple matter of fixing a few features of banners to make them better. The Web would be a better place if people would stop fighting its fundamental nature and instead embrace it for what it is good at. Then let&#39;s get the browser vendors to include a single micropayment system as a standard, and the Web can advance to the next level.
</p>
<h2>
 Legacy Media Do Live Forever
</h2>
<p>
 <em>
  Kym Kittell from Los Alamos National Laboratory writes:
 </em>
</p>
<blockquote>
 How do you explain the
 <strong>
  continued popularity of radio
 </strong>
 (including news radio) given that TV has been around for so many years? Do you believe that TV (as we know it) and/or radio will go away completely or will people continue to turn to these media in different circumstances (when you want to listen, but can&#39;t devote your entire attention to sitting at the computer)?
</blockquote>
<p>
 <em>
  Jakob&#39;s reply:
 </em>
 Several other readers made similar points about radio. As Peter Zollman once said, &quot;with the possible exception of the town crier, a new medium has never put an old medium out of business.&quot;
</p>
<p>
 Radio is different from the other media I have discussed because it is completely non-visual. Thus, it is ideally suited for eyes-busy tasks such as driving. I think it will survive and I actually predict the emergence of more auditory media, including some non-linear ones (but since it is much harder to navigate non-visual information spaces, they tend to be less advanced). The one way radio will die is that the same kind of content will be accessed through the Internet and not through the airwaves. A person driving a car might have the car computer connect to a Web server that would know his or her preferences in music, news, and talkshows and compose a personalized playlist. Thus radio may not exist in the form of stations, though the medium will survive in the form of streaming auditory content.
</p>
<p>
 In general, I don&#39;t think the old media will be &quot;out out of business&quot; in the sense of vanishing. We will still have auditory content, text content in various degrees of depth and writing styles, moving-images content, photo content, cartoon content, and so on. I simply predict that these content styles will be integrated in new ways and accessed in a non-linear fashion over the Internet instead of being bought in their current (separate) packaging.
</p>
<p>
 <em>
  John Brandon from
  <a class='out"' href="http://www.bestbuy.com/" title="Company website">
   Best Buy Corporation
  </a>
  writes:
 </em>
</p>
<blockquote>
 As my wife is fond of pointing out to me, nothing is going to replace a good book (or for that matter linear technology). I think the point you failed to make was that linear video will be available
 <em>
  from
 </em>
 the interactive media and will be a part of it. Short clips should be integrated into the user experience, but there&#39;s no telling when people will want the linear story.
</blockquote>
<p>
 <em>
  Jakob&#39;s reply:
 </em>
 You are right. Linear media will remain for many applications, especially fiction (since storytelling is mostly linear). Thus, we will probably retain novels, films, and sports transmissions as basically linear, though they may include branching points for added information or side stories. I do believe in your point that the main use of non-linear media relative to these forms of expression will be in advance: when you are deciding what novel to read or what film to watch.
</p>
</section>
  

  
  <section>
    
        
    
  </section>
  
  
  
  
  

  


  <section>
    <br />
    <p class="share"><b>Share <span class="hide-for-small-down">this article</span>:</b> <a href="https://twitter.com/intent/tweet?url=http://www.nngroup.com/articles/readers-comments-on-the-end-of-legacy-media/&amp;text=Readers'%20Comments%20on%20the%20End%20of%20Legacy%20Media&amp;via=nngroup">Twitter</a> | <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=http://www.nngroup.com/articles/readers-comments-on-the-end-of-legacy-media/&amp;title=Readers'%20Comments%20on%20the%20End%20of%20Legacy%20Media&amp;source=Nielsen%20Norman%20Group">LinkedIn</a> | <a href="https://plus.google.com/share?url=http://www.nngroup.com/articles/readers-comments-on-the-end-of-legacy-media/">Google+</a> | <a href="mailto:?subject=NN/g Article: Readers&#39; Comments on the End of Legacy Media&amp;body=http://www.nngroup.com/articles/readers-comments-on-the-end-of-legacy-media/">Email</a></p>
    <hr />
  </section>

  <footer class="l-subsection">
    
    
    

    <div class="row show-for-small-down related-content">
      
      
      

      
      
      

      
      
      

      
      
      

      
      
      
    </div>

    <div class="hide-for-small-down row related-content">
      <div class="medium-6 columns">
        
        
        

        

      </div>
      <div class="medium-6 columns">
        
        
      </div>
    </div>
  </footer>
</article>

        </div>

      

    
  </div>
  <div class="row">
    
      <div class="column small-12 show-for-small-down l-navrail announcements">
        
          
        
      </div>
    
    
  </div>

    </div>
    




<footer id="page-footer">
    <div class="row hide-for-print">
        <div class="column small-12 medium-6">
            <section class="footer-popular">
                
                <h1>Popular Topics</h1>
                <div style="float: left;  width: 15em; display: block;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/applications/index.php">Application Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/e-commerce/index.php">E-Commerce Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/eyetracking/index.php">Eyetracking </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/information-architecture/index.php">Information Architecture </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/intranets/index.php">Intranet Design </a></li>
	<li><a href="../../topic/interaction-design/index.php">Interaction Design </a></li>
</ul>
</div>

<div style="float: left;  width: 15em; display: block; list-style: none;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/mobile-and-tablet-design/index.php">Mobile and Tablet Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/credibility-persuasion/index.php">Persuasive Design</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/research-methods/index.php">Research Methods</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/user-testing/index.php">User Testing </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/web-usability/index.php">Web Usability </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/writing-web/index.php">Web Writing &amp; Content Strategy </a></li>
</ul>
</div>

<p>&nbsp;</p>

                
            </section>
        </div>

        <div class="column small-12 medium-6">
            <div class="row">
                <div class="column small-6">
                    <section class="footer-about">
                        <h1>Follow Us</h1>
                        <ul>
	<li><a href="../subscribe/index.php">Subscribe to our weekly UX newsletter</a>,&nbsp;Jakob Nielsen&#39;s Alertbox</li>
	<li>Visit Don Norman&#39;s website, <a href="http://www.jnd.org/"> www.jnd.org </a></li>
	<li>Visit Bruce Tognazzini&#39;s website, <a href="http://www.asktog.com/"> www.asktog.com </a></li>
	<li style="padding-top: 6px;"><a class="twitter-follow-button" data-show-count="false" data-size="large" href="https://twitter.com/nngroup"><img alt="Follow @nngroup on Twitter" height="20" src="https://media.nngroup.com/media/editor/2016/12/30/twitter-follow-button.png" style="width: 117px; height: 20px;" width="117" /></a></li>
	<li style="padding-top: 6px;"><img alt="" height="14" src="https://media.nngroup.com/media/editor/2016/07/26/feed-icon-14x14.png" style="vertical-align:text-top;width:14px;margin-right:.25em;" width="14" /><a href="../../feed/rss/index.php">Subscribe to our RSS feed</a></li>
</ul>

                    </section>
                </div>

                <div class="column small-6">
                    <section class="footer-about right-column">
                        <h1>About
</h1>
                        <ul class="no-bullet">
	<li><a href="../../about/contact/index.php">Contact information</a></li>
	<li><a href="../../about/index.php">About Us</a></li>
	<li><a href="../../about/why-nng/index.php">Why NN/g? </a></li>
	<li><a href="../../return-policy/index.php">Return Policy</a></li>
</ul>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="copyright column small-12">
            <p><a href="../../copyright-and-reprint-info/index.php">Copyright</a> &copy; 1998-2017 Nielsen Norman Group, All Rights Reserved.</p>
        </div>
    </div>
</footer>


    


        <script src="https://media.nngroup.com/static/vendor/fastclick/js/fastclick.min.js"></script>
        <script src="https://media.nngroup.com/static/vendor/foundation/js/foundation.min.js"></script>
        <script src="http://media.nngroup.com/static/js/app.js"></script>
        <script src="http://media.nngroup.com/static/js/scroll-to-fixed.js"></script><script>var externalLinks = document.querySelectorAll('a[href^="http"]');for (var i = externalLinks.length-1; i >= 0; i--) {externalLinks[i].addEventListener("click", function(event) { alert("External links are disabled for this test."); event.preventDefault(); }, false);}var mailLinks = document.querySelectorAll('a[href^="mailto"]');for (var i = mailLinks.length-1; i >= 0; i--) {mailLinks[i].addEventListener("click", function(event) { alert("Email links are disabled for this test."); event.preventDefault(); }, false);}var externalForms = document.querySelectorAll('form');for (var i = externalForms.length-1; i >= 0; i--) {externalForms[i].addEventListener("submit", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false); externalForms[i].addEventListener("click", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false);}</script>

        <script>
            
            $(function() {
                $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
                $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
            });
        </script>

        
<script src="https://media.nngroup.com/static/js/articles.js"></script>


        
        <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1072699960;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072699960/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
    </body>

<!-- Mirrored from www.nngroup.com/articles/readers-comments-on-the-end-of-legacy-media/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:16:02 GMT -->
</html>
