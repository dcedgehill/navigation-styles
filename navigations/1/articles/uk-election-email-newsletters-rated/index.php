<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->





    
<!-- Mirrored from www.nngroup.com/articles/uk-election-email-newsletters-rated/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:14:07 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1026.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"licenseKey":"1a30ef9931","errorBeacon":"bam.nr-data.net","applicationID":"2048992","transactionName":"NFxSMRBUX0EDUkIPDA0WdhAMVkVbDV8ZBxEXUFMJB0YfRAtUQRVZIktEDAFZVHYHRVcPD01eVRE=","beacon":"bam.nr-data.net","queueTime":1,"applicationTime":455,"agent":""}</script>
        <title>UK Election Email Newsletters Rated</title><meta property="og:title" content="UK Election Email Newsletters Rated" />
  
        
        <meta name="description" content="The main British parties&#39; email newsletters have higher usability scores than we found for US political newsletters in our last evaluation.">
        <meta property="og:description" content="The main British parties&#39; email newsletters have higher usability scores than we found for US political newsletters in our last evaluation." />
        
  
        
	
        
        <meta name="keywords" content="email usability, e-mail design, email newsletters, newsletters, newsletter design, newsletter case studies, newsletter case study, UK, United Kingdom, Conservative Party, Conservatives, Labour Party, Liberal Democrats, Lib Dems, LibDems, registration, signup, privacy policy, tabs, publication frequency, scannability, newsletter layout, subject line, from field, headlines, confirmation pages, confirmation email, consistency, distractions, social media, elections, campaigns, privacy policy, readability, reading levels, text complexity, copywriting, copy writing, enewsletter, ezine, ezines, email marketing">
        

        

        <meta name="robots" content="noarchive">
        <meta name="viewport" content="width=device-width">

        <link rel="search" type="application/opensearchdescription+xml" title="NN/g Search" href="../../opensearch.xml" />
        <link rel="shortcut icon" href="http://media.nngroup.com/static/img/favicon.ico" />

        <link rel="stylesheet" href="https://media.nngroup.com/static/vendor/font-awesome/css/font-awesome.min.css">

        
        <link rel="stylesheet" href="https://media.nngroup.com/static/css/7232a960f1df.css" type="text/css" />
        <link rel="stylesheet" href="http://fast.fonts.com/t/1.css?apiType=css&amp;projectid=c5846260-9def-46ec-9407-0f1349bd2d64" type="text/css">

        <script src="https://media.nngroup.com/static/vendor/modernizr/js/modernizr.min.js"></script>

        <script type="text/javascript" src="https://media.nngroup.com/static/js/3283be5def74.js"></script>
        

        

        
        
        <!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=229034,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('http://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->


<!-- Google Analytics Tracking Code for http://nngroup.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23132681-1', 'nngroup.com');
  ga('require', 'linkid', 'https://www.nngroup.com/articles/uk-election-email-newsletters-rated/linkid.js');
  ga('require', 'displayfeatures');

// Optimizely Universal Analytics Integration
 window.optimizely = window.optimizely || [];
 window.optimizely.push("activateUniversalAnalytics");

  ga('send', 'pageview');

</script>
<script>
/**
* Function to track a click on an outbound link in Google Analytics.
*/
function trackOutboundLink(label, url) {
    if(window.ga && ga.loaded) {
        ga('send', 'event', 'outbound', 'click', label, {'hitCallback': function(){document.location = url;}
        });
    } else {
        document.location = url;
    }
}
/**
* Function to track a click on a link in the article announcement area.
*/
$('#articleAnnouncement a').on('click', function(){
  ga('send', 'event', 'link', 'click', 'articleAnnouncement');
});
</script>

<script type="text/javascript">
 setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2421.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



<!-- Hotjar Tracking Code for http://nngroup.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:121605,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

        <script src="https://media.nngroup.com/static/js/analytics/track_outbound_link.js"></script>
        
        
        
    </head>
    <body class="location-articles articles-detail">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
    <header id="page-header">
        <div id="cart" class="shopping-cart mobile-cart show-for-small-down"></div>
        <div class="row">
            <div id="masthead" class="masthead small-12 medium-7 column">

                <!-- The button to toggles menu on small screen -->

                <h1><a href="../../index.php"><img src="http://media.nngroup.com/static/img/logo-wordmark.png" width="74" height="30"> Nielsen <span class="norman">Norman</span> Group</a></h1>
                <p class="tagline show-for-medium-up">Evidence-Based User Experience Research, Training, and Consulting</p>

                <!-- The button to toggles form search on small screen -->


                <div style="clear: right;width:  100%;height: 1px;"></div><?php define('ROOT_PATH',$_SERVER["DOCUMENT_ROOT"] . '/apps/navigation_styles/navigations/1'); ?><?php include(ROOT_PATH)."/navigation.php"; ?>

                <!-- Drop down menu on small screen -->
                <div id="hideNav" class="show-for-small-down">
                  
                  <ul class="menu-content">
                    <li><a href="../../index.php">Home</a></li>
                    <li class="has-submenu">
                      <a>Training</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../training/index.php">Overview</a></li>
                        
<li id="nav-ux-conference"><a href="../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../online-seminars/index.php"><span>Online Seminars</span></a></li>

                      </ul>
                    </li>
                    <li class="has-submenu">
                      <a>Consulting</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
  <li><a href="../../consulting/index.php">Overview</a></li>
  
  <li><a href="../../consulting/benchmarking/index.php">Benchmarking</a></li>
  
  <li><a href="../../consulting/expert-design-review/index.php">Expert Design Review</a></li>
  
  <li><a href="../../consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
  
  <li><a href="../../consulting/usability-testing/index.php">Usability Testing </a></li>
  
  <li><a href="../../consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
  
  <li><a href="../../in-house-training/index.php">In-House Training</a></li>
</ul>

                    </li>

                    <li><a href="../../reports/index.php">Reports</a></li>
                    <li><a href="../index.php">Articles</a></li>
                    <li class="has-submenu"><a>About NN/G</a><i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../about/index.php">Overview</a></li>
                        <li><a href="../../people/index.php">People</a></li>
                        <li><a href="../../about/why-nng/index.php">Why NN/g?</a></li>
                        <li><a href="../../about/contact/index.php">Contact</a></li>
                        <li><a href="../../news/index.php">News</a></li>
                        <li><a href="../../about/history/index.php">History</a></li>
                        <li><a href="../../books/index.php">Books</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                </div>

                <hr class="header-divider show-for-small-only"/>


            </div>
       </div>

        <div id="hideNav">            <nav class="primary-nav small-12 column">
                <ul>
                    <li><a class="navitem" id="nav-home" href="../../index.php">Home</a></li>
                    <li><a class="navitem" id="nav-training" href="../../training/index.php">Training</a></li>
                    <li><a class="navitem" id="nav-consulting" href="../../consulting/index.php">Consulting</a></li>
                    <li><a class="navitem" id="nav-reports" href="../../reports/index.php">Reports</a></li>
                    <li><a class="navitem" id="nav-articles" href="../index.php">Articles</a></li>
                    <li><a class="navitem" id="nav-about" href="../../about/index.php">About NN/G</a></li>
                </ul>
            </nav>
            <nav class="secondary-nav small-12 medium-12 large-12 columns">
                
                
            </nav>
        </div>
    </header>
    <div class="l-content">
    
  

  

  <div class="row">
    
      
      <section class="small-12 medium-3 columns collapsable-content l-navrail articles-left-column">
        <h1 class="show-for-small-down collapse-link mobile-browse-header">Browse by Topic and Author</h1>
          <div class="row collapsable">

            
            <div class="small-6 medium-12 columns">
              <section id="article-popular-topics" style="display: none;">
                
                <h1>Topics</h1>
                <ul id="article-popular-topics">
                    
                    <li>
                        
                        <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/intranets/all/index.php">Intranets</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/user-testing/all/index.php">User Testing</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                        
                    </li>
                    
                    <li><a href="#" id="article-see-all-topics"><strong>See all topics&hellip;</strong></a></li>
                </ul>
                
              </section>
              <section id="article-all-topics" style="display: none;">
                
                <h1>
                  <span class="show-for-medium-up">All Article Topics</span>
                  <span class="show-for-small-down">All Topics</span>
                  (<a href="#" id="article-hide-all-topics">hide</a>)
                </h1>
                <ul>
                  
                  <li>
                    
                    <a href="../../topic/accessibility/all/index.php">Accessibility</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/agile/all/index.php">Agile</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/analytics-and-metrics/all/index.php">Analytics &amp; Metrics</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/applications/all/index.php">Application Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/b2b-websites/all/index.php">B2B Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/behavior-patterns/all/index.php">Behavior Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/branding/all/index.php">Branding</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/content-strategy/all/index.php">Content Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/corporate-websites/all/index.php">Corporate Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/design-patterns/all/index.php">Design Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-design-process/all/index.php">Design Process</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/email/all/index.php">Email</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/eyetracking/all/index.php">Eyetracking</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/heuristic-evaluation/all/index.php">Heuristic Evaluation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/human-computer-interaction/all/index.php">Human Computer Interaction</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-ideation/all/index.php">Ideation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/information-architecture/all/index.php">Information Architecture</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/interaction-design/all/index.php">Interaction Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/international-users/all/index.php">International Users</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/intranets/all/index.php">Intranets</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-management/all/index.php">Management</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/navigation/all/index.php">Navigation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/non-profit-websites/all/index.php">Non-Profit Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/personas/all/index.php">Personas</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/persuasive-design/all/index.php">Persuasive Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/prototyping/all/index.php">Prototyping</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/psychology-and-ux/all/index.php">Psychology and UX</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/research-methods/all/index.php">Research Methods</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/search/all/index.php">Search</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/social-media/all/index.php">Social Media</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/strategy/all/index.php">Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/user-testing/all/index.php">User Testing</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-teams/all/index.php">UX Teams</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/visual-design/all/index.php">Visual Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/writing-web/all/index.php">Writing for the Web</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/young-users/all/index.php">Young Users</a>
                    
                  </li>
                  
                </ul>
                
              </section>
            </div>
            <script>
              /* Determine which topics list will be displayed: the full or the
               * partial.  We are inlining this close to the list itself to
               * prevent rendering delays on the client
               */
              var topic_list = $.cookie("article-topics");

              var popular_topics = $('#article-popular-topics');
              var all_topics = $('#article-all-topics');

              if(topic_list == 1) {
                  popular_topics.hide();
                  all_topics.show();
              } else {
                  popular_topics.show();
                  all_topics.hide();
              }
            </script>
            

            
            <div class="small-6 medium-12 columns">
              
              <section id="article-initial-authors">
                  <h1>Author</h1>
                  <ul>
                    
                    <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                    
                    <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                    
                    <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                    
                    <li><a href="#" onClick="$('#article-initial-authors').hide(); $('#article-all-authors').show();"><strong>See all authors&hellip;</strong></a></li>
                  </ul>
              </section>
              <section id="article-all-authors" style="display: none;">
                <h1>
                  All Authors
                  (<a href="#" id="article-hide-all-authors">hide</a>)
                </h1>
                <ul>
                  
                  <li><a href="../author/raluca-budiu/index.php">Raluca Budiu</a></li>
                  
                  <li><a href="../author/patty-caya/index.php">Patty Caya</a></li>
                  
                  <li><a href="../author/susan-farrell/index.php">Susan Farrell</a></li>
                  
                  <li><a href="../author/therese-fessenden/index.php">Therese Fessenden</a></li>
                  
                  <li><a href="../author/kim-flaherty/index.php">Kim Flaherty</a></li>
                  
                  <li><a href="../author/sarah-gibbons/index.php">Sarah Gibbons</a></li>
                  
                  <li><a href="../author/aurora-harley/index.php">Aurora Harley</a></li>
                  
                  <li><a href="../author/kate-kaplan/index.php">Kate Kaplan</a></li>
                  
                  <li><a href="../author/page-laubheimer/index.php">Page Laubheimer</a></li>
                  
                  <li><a href="../author/angela-li/index.php">Angie Li</a></li>
                  
                  <li><a href="../author/hoa-loranger/index.php">Hoa Loranger</a></li>
                  
                  <li><a href="../author/kate-meyer/index.php">Kate Meyer</a></li>
                  
                  <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                  
                  <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                  
                  <li><a href="../author/kara-pernice/index.php">Kara Pernice</a></li>
                  
                  <li><a href="../author/christian-rohrer/index.php">Christian Rohrer</a></li>
                  
                  <li><a href="../author/amy-schade/index.php">Amy Schade</a></li>
                  
                  <li><a href="../author/katie-sherwin/index.php">Katie Sherwin</a></li>
                  
                  <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                  
                  <li><a href="../author/kathryn-whitenton/index.php">Kathryn Whitenton</a></li>
                  
                </ul>
              </section>
              
            </div>
            
          </div>

          
          <section class="hide-for-small-down">
            
              
              <h1>Recent Articles</h1>
              
              <ul>
                  
                  <li><a href="../seamless-cross-channel/index.php">Seamlessness in the Omnichannel User Experience</a></li>
                  
                  <li><a href="../brand-intention-interpretation/index.php">Experience Design: Bridging Brand Intention and Brand Interpretation</a></li>
                  
                  <li><a href="../eyetracking-tasks-efficient-scanning/index.php">Scanning Patterns on the Web Are Optimized for the Current Task</a></li>
                  
                  <li><a href="../flat-design-best-practices/index.php">Flat-Design Best Practices</a></li>
                  
                  <li><a href="../ux-user-stories/index.php">Accounting for UX Work with User Stories in Agile Projects</a></li>
                  
                  <li><a href="../index.php"><strong>See all articles&hellip;</strong></a></li>
              </ul>
              
            
          </section>
          

          
          
          <section class="hide-for-small-down">
            <h1>Popular Articles</h1>
            
            <ul>
                
                <li><a href="../usability-101-introduction-to-usability/index.php">Usability 101: Introduction to Usability</a></li>
                
                <li><a href="../top-10-mistakes-web-design/index.php">Top 10 Mistakes in Web Design</a></li>
                
                <li><a href="../how-users-read-on-the-web/index.php">How Users Read on the Web</a></li>
                
                <li><a href="../f-shaped-pattern-reading-web-content/index.php">F-Shaped Pattern For Reading Web Content</a></li>
                
                <li><a href="../ten-usability-heuristics/index.php">10 Usability Heuristics for User Interface Design</a></li>
                
                <li><a href="../intranet-design-2016/index.php">10 Best Intranets of 2016</a></li>
                
                <li><a href="../which-ux-research-methods/index.php">When to Use Which User-Experience Research Methods</a></li>
                
                <li><a href="../response-times-3-important-limits/index.php">Response Times: The 3 Important Limits</a></li>
                
                <li><a href="../why-you-only-need-to-test-with-5-users/index.php">Why You Only Need to Test with 5 Users</a></li>
                
                <li><a href="../page-fold-manifesto/index.php">The Fold Manifesto: Why the Page Fold Still Matters</a></li>
                
            </ul>
          </section>
          
          

          
            <section class="hide-for-small-down">
              
            </section>
          

      </section>
      

      
        <div class="small-12 medium-9 columns content-body">
          




<article class="l-subsection alertbox-article-content article-detail-bl">
  
  <header>
    <div class="l-subsection">
      <h1>UK Election Email Newsletters Rated</h1>
      <div class="author-meta">by
        
          
            
            <a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a>
            
          
        on  April 26, 2010
        
      </div>

      

      
      <div class="topic-links">
        Topics:
        
          <ul>

  <li><a href="../../topic/email/index.php">Email</a></li>

</ul>
        
      </div>
      
    </div>
  </header>
  

  
  
  <section>
    <p class="lede"><strong>Summary:</strong> The main British parties&#39; email newsletters have higher usability scores than we found for US political newsletters in our last evaluation.</p>
    <br />
  </section>
  
  

  
  <section id="article-body"><p>
	Since our <a class="old" href="../email-newsletters-pick-up-where-websites-leave-off/index.php" title="Alertbox: Email Newsletters Pick Up Where Websites Leave Off"> earliest user research into email newsletters</a> 8 years ago, we've known that newsletters are a superb mechanism for growing a <strong> relationship </strong> with customers. Indeed, when we recently asked users why they were visiting particular websites, the most common response was, <em> "I was reminded to do so because I received an email newsletter from the site." </em></p>
<p>
	Beyond being good for business, newsletters are an effective means for political candidates to stay in touch with their supporters during an election campaign. (Twitter and Facebook campaigning might get more press coverage, but representatives from all 3 parties <a class="out" href="http://www.economist.com/world/britain/displaystory.cfm?story_id=15719160" title=" New media and the election: Thus far and no farther. The potential - and limits - of the internet in political campaigning, The Economist March 18, 2010"> told <cite> The Economist </cite> </a> that the old-fashioned newsletter is actually the more effective campaigning tool.)</p>
<p>
	I decided to check the state of newsletter design for the current UK elections. To do so, I evaluated newsletters from all 3 main parties: the Conservatives, Labour, and the Liberal Democrats.</p>
<p>
	I evaluated the subscription process on April 8, 2010, the unsubscribe process on April 21, 2010, and the newsletter content over the two-week interim (April 8–21).</p>
<p>
	I didn't evaluate the websites themselves, only the parties' use of off-Web Internet communication. Still, I can't help but point out that the Lib Dems' site violates a key <a class="new" href="../../courses/ia-2/index.php" title="Nielsen Norman Group: 'Information Architecture 2: Navigation Design' detailed outline for training tutorial"> navigation design guideline</a>  by using <a class="old" href="../tabs-used-right/index.php" title="Alertbox: Tabs, Used Right"> tabs that don't look like tabs</a>:</p>
<p style="text-align: center;">
	<img alt="Box from the Lib Dem homepage" height="154" src="http://media.nngroup.com/media/editor/alertbox/libdem-bad-tabs-homepage.png" width="469"/></p>
<p>
	In this design, "Media Centre" looks much more like a subtitle or an explanation for "Latest news" than like a clickable GUI item in its own right.</p>
<h2>
	Score Card</h2>
<p>
	Following are the newsletters' average compliance ratings in the 4 guideline categories:</p>
<table align="center" style="border-style: solid; border-color: #cccccc; border-width: 1px; border-collapse: collapse; ">
	<tbody>
		<tr>
			<th style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left; background-color:  #003366; color: #ffffff; width: 40%;">
				 </th>
			<th style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: center; background-color:  #003366; color: #ffffff; width: 20%;">
				Conservatives</th>
			<th style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: center; background-color:  #003366; color: #ffffff; width: 20%;">
				Labour</th>
			<th style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: center; background-color:  #003366; color: #ffffff; width: 20%;">
				Liberal Democrats</th>
		</tr>
		<tr>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left; vertical-align: middle;">
				Subscription interface</td>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: center; vertical-align: middle;">
				55%</td>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: center; vertical-align: middle;">
				42%</td>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: center; vertical-align: middle;">
				47%</td>
		</tr>
		<tr>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left; vertical-align: middle;">
				Newsletter content and presentation</td>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: center; vertical-align: middle;">
				68%</td>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: center; vertical-align: middle;">
				63%</td>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: center; vertical-align: middle;">
				66%</td>
		</tr>
		<tr>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left; vertical-align: middle;">
				Subscription maintenance and unsubscribing</td>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: center; vertical-align: middle;">
				64%</td>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: center; vertical-align: middle;">
				63%</td>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: center; vertical-align: middle;">
				73%</td>
		</tr>
		<tr>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left; vertical-align: middle;">
				Differentiating newsletter from junk mail</td>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: center; vertical-align: middle;">
				50%</td>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: center; vertical-align: middle;">
				50%</td>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: center; vertical-align: middle;">
				100%</td>
		</tr>
		<tr>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left; vertical-align: middle;">
				<strong>All guidelines </strong></td>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: center; vertical-align: middle;">
				<strong>63% </strong></td>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: center; vertical-align: middle;">
				<strong>56% </strong></td>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: center; vertical-align: middle;">
				<strong>62% </strong></td>
		</tr>
	</tbody>
</table>
<p>
	(The overall usability rating is determined by averaging all <a class="new" href="../../reports/email-newsletter-design/index.php" title="Nielsen Norman Group report: Email Newsletter Usability - 149 Design Guidelines for Newsletter Subscription, Content, and Account Maintenance Based on Usability Studies"> 149 guidelines for newsletter usability</a>. Thus, it's not simply an average of the 4 category scores, because there are different numbers of guidelines in each category.)</p>
<h2>
	Signup Process</h2>
<p>
	Labour's signup page violates many usability guidelines:</p>
<p style="text-align: center;">
	<img alt="Labour Party signup page for email newsletter subscribers" height="651" src="http://media.nngroup.com/media/editor/alertbox/labour-signup-page.png" width="600"/></p>
<p>
	When you're trying to build a mailing list, the worst mistake is to violate the guideline to <strong> remove distractions from the signup </strong> page. You <em> almost </em> captured the user's email — but they decided to go off and gawk at some of your Facebook fans instead.</p>
<p>
	Also, while consistency is usually good, it's actually bad to include the standard navigation's newsletter subscriptions element on the page that users land on when they click its "Sign Up" button. Doing so violates a standard <a class="new" href="../../courses/apps-1/index.php" title="Nielsen Norman Group course"> application design guideline</a> to avoid "no-op" commands that simply return users to the same state they're already in.</p>
<p>
	(Similarly, even though all other website pages should link to the homepage, the homepage itself shouldn't have a homepage link. Homepages that link to themselves were mistake #10 on my list of <a class="old" href="../most-violated-homepage-guidelines/index.php" title="Alertbox: The Ten Most Violated Homepage Design Guidelines"> top homepage mistakes</a>. Sadly, Labour and the Lib Dems both make this classic mistake — 7 years after I warned against it — but at least the Conservatives get it right.)</p>
<p>
	To its credit, Labour makes a half-hearted attempt to explain its <strong> privacy policy </strong> right on the signup page. Of course, this is one of the weakest privacy policies in history, simply saying that they "may use the data you have supplied." But the other two parties don't even bother to mention how they'll use your private data. (And no, it's not enough to have a full privacy policy elsewhere on the site. Users need to be told the policy on the spot when you ask for sensitive info.)</p>
<p>
	As for the immediate post-signup experience, the Lib Dems have one of the worst <strong> confirmation pages </strong> I have ever seen:</p>
<p style="text-align: center;">
	<img alt="Liberal Democratic Party confirmation page for new newsletter subscribers" height="438" src="http://media.nngroup.com/media/editor/alertbox/libdem-confirmation-page.png" width="600"/></p>
<p>
	The header is "Sign up for Email News" — but that's what I just did! The header is followed by a slogan and some <a class="old" href="../blah-blah-text-keep-cut-or-kill/index.php" title="Alertbox: Blah-Blah Text - Keep, Cut, or Kill?"> blah-blah text</a>. Only at the very bottom do we get a modest line confirming that "your details have been submitted." Even here, they don't say that I've been <em> subscribed </em> or inform me of when to expect the first newsletter. Maybe they want to approve new subscribers before spending the 0.1 penny it'll cost to send those emails for the duration of the campaign.</p>
<p>
	Besides violating all the guidelines for newsletter confirmations, this page also violates several guidelines for <a class="old" href="../../topic/writing-web/index.php" title="Articles about content usability"> writing for the Web</a>, including elementary mistakes like punctuation inconsistencies. Also, anyone who appreciates <a class="old" href="../../courses/topic/visual-design/index.php" title="Nielsen Norman Group course: Visual Web Design 1: Graphic Design Fundamentals and Concepts"> online typography</a> is unlikely to be fond of the sudden switch to a serif typeface for that final line, even though it does make it stand out a bit on an all-sans-serif site.</p>
<p>
	Although it has the worst confirmation <em> page</em>, the Lib Dems won the confirmation <em> email </em> race, being the first party to send me a welcome message (beating Labour by 4 minutes). The Conservative party never bothered to acknowledge my subscription, even though its weekly newsletter would benefit more than daily ones from a good welcome message. After all, people might easily go 6–7 days without hearing a thing; at that point, some users will have forgotten they subscribed and consider the newsletter spam.</p>
<p>
	The Lib Dems' welcome message also has by far the best <em> From </em> field and <em> Subject </em> line, as this comparison shows:</p>
<ul>
	<li>
		<em>From: </em> Liberal Democrats — <em> Subject: </em> Thank you for signing up for Liberal Democrat email news</li>
	<li>
		<em>From: </em> labourparty@email-new.labour.org.uk — <em> Subject: </em> Thank you for signing up</li>
</ul>
<p>
	Given that users are drowning in spam, it's important that your subject lines be as precise and descriptive as possible to encourage users to open your messages. For the same reason, it's also better to have a human-readable <em> From </em> field than to rely solely on a geeky-looking (and less readable) email address. <em> Liberal Democrats </em> is better than <em> labourparty </em> — at least typographically.</p>
<h2>
	Publication Frequency</h2>
<p>
	Following are the number of emails (not counting <a class="old" href="../transactional-and-confirmation-email/index.php" title="Alertbox: Transactional Email and Confirmation Messages"> confirmation messages</a>) that I received from each party during my 14-day evaluation period:</p>
<ul>
	<li>
		Labour: 3</li>
	<li>
		Conservatives: 4</li>
	<li>
		Liberal Democrats: 9</li>
</ul>
<p>
	The Conservative homepage asks users to sign up for "David's weekly email," so you'd think messages would be sent weekly. Not so. I received 4 Tory emails over 2 weeks. I don't think this constitutes overloading supporters during an election campaign, but it's certainly not weekly.</p>
<p>
	In contrast, I got only 3 Labour newsletters during the same period. This is definitely too little communication; people sign up for a party's newsletter to be kept informed about the election.</p>
<p>
	The Lib Dem party sent 9 emails, which borders on <a class="old" href="../information-pollution/index.php" title="Alertbox"> information pollution</a>. This was especially true on the day it sent 3 emails within an hour, rather than consolidating the barrage into a single message.</p>
<p>
	In general, 2–3 emails per week is appropriate during a campaign's early phases, with the frequency increasing to daily updates during the final hectic days before the election, when subscriber interest intensifies.</p>
<p>
	American politicians make the mistake of courting <strong> supporter fatigue </strong> by spamming subscribers with far too many emails. British politicians make the opposite mistake: they <strong> communicate too little </strong> during a period when supporters <em> want </em> frequent updates.</p>
<p>
	All 3 parties in my analysis are making the (safe) choice to carefully limit how much email they send to keep people from unsubscribing. This is generally good, but I think they're perhaps overly concerned with the less-committed segments of their audience. This will disappoint the more-committed supporters, but perhaps the parties assume that their Twitter feeds serve that group.</p>
<p>
	Even though the parties comply with more <a class="old" href="../../courses/topic/social-ux/index.php" title=" Nielsen Norman Group course: Integrating Social Features on Mainstream Websites - User-Generated Content, Social Media, Collaboration, and More"> social networking usability guidelines</a> than newsletter guidelines, Twitter can't replace a good email newsletter. For one, the parties tend to tweet more or less hourly, which is overkill for anybody except political wonks. As a result, they have only 14,000 to 27,000 followers, which is nothing in a country with 61 million people.</p>
<p>
	The recommended solution to this dilemma is to <strong> offer 2 newsletters </strong> with different publication frequencies, but none of the parties do this.</p>
<h2>
	Receiving Newsletters</h2>
<p>
	The first newsletter I received from the Conservatives was sent with a <em> From </em> field reading "George Osborne," even though the website advertised the newsletter as "David's weekly email." (David Cameron is the Conservative candidate for Prime Minister; Osborne is the party's candidate for Chancellor.)</p>
<p>
	The general guideline is for newsletters to be sent from either a recognized institutional name or from a celebrity in his/her own name. In this case, "David's email" ought to be from David, not George. It's fine for political newsletters to feature multiple party leaders, but if they do so, they shouldn't be promoted in the name of a single celebrity.</p>
<p style="text-align: center;">
	<img alt="Email newsletters from all 3 major parties in the British election" height="602" src="http://media.nngroup.com/media/editor/alertbox/campaign_newsletters_uk.jpg" width="700"/><br/>
	<em>Left-to-right: Sample newsletters from the Conservatives, Labour, and the Liberal Democrats. </em></p>
<p>
	Of the 3 newsletters, 2 feature prominent links to the parties' Facebook pages and Twitter feeds. This is recommended. When we did <a class="old" href="../writing-social-media-facebook-twitter/index.php" title="Alertbox: Streams, Walls, and Feeds - Distributing Content Through Social Networks and RSS"> user testing of social network publishing</a>, we found that users had great difficulty locating the official pages for companies and organizations. Of major online services, only the Apple App Store has more <strong> atrocious findability</strong>.</p>
<p>
	The Lib Dems newsletter is the most scannable and the only one truly designed for today's time-pressed readers. The Tory newsletter does follow <a class="old" href="../how-users-read-on-the-web/index.php" title="Alertbox: How Users Read on the Web"> writing guidelines</a>, like the use of highlighted keywords, and also uses stills from (linked) video clips to break up the text, drawing users' eyes down the screen. In contrast to these rival offerings, Labour's newsletter is one big wall of gray, undifferentiated text.</p>
<p>
	To target a broad consumer audience, our recommendation is to <a class="old" href="../writing-for-lower-literacy-users/index.php" title="Alertbox: Low-Literacy Users"> write at an 8th-grade reading level</a>. (Meaning that the text should have a complexity level suitable for a well-schooled 15-year-old.) Only the Lib Dems actually follow this recommendation, as the following readability statistics for sample newsletters show:</p>
<ul>
	<li>
		Conservatives: 10th grade</li>
	<li>
		Labour: 9th grade</li>
	<li>
		Liberal Democrats: 8th grade</li>
</ul>
<p>
	Does it matter that content from the Conservatives and Labour is too complicated for 43% of the population? Probably not much; people who follow politics so closely that they'll subscribe to a party newsletter probably belong to the elite 57% of the population who can read the text easily enough. At least none of these newsletters were written at the university level — a style characteristic of many government and big-company websites (and even my own articles, which tend to be written at a reading level equivalent to undergraduate university texts).</p>
<p>
	<strong>Subject lines </strong> are uniformly bad. Here's a sample. Try to guess (a) which party wrote which headline, and (b) which (if any) of the following emails you'd open:</p>
<ul>
	<li>
		<em>One simple word </em></li>
	<li>
		<em>Answer time </em></li>
	<li>
		<em>Can you help make it a fair fight? </em></li>
	<li>
		<em>Jakob, my take on week two of the campaign </em></li>
	<li>
		<em>State of the Race memo 3 </em></li>
</ul>
<p>
	At least the last two (from the Tories and Labour, respectively) have some small <a class="old" href="../information-foraging/index.php" title="Alertbox: Information Foraging - Why Google Makes People Leave Your Site Faster"> information scent</a> to indicate that they're about the election. Still, it would have been better to add some actual content to summarize the analysis and enhance the "open-me" attractiveness of these subject lines.</p>
<p>
	Following are two of the better subject lines, both from the Lib Dems:</p>
<ul>
	<li>
		<em>Spread the word — The tax cut you can believe in </em></li>
	<li>
		<em>Help elect a Lib Dem MP </em></li>
</ul>
<p>
	The first of these contains specific content about a topic of interest (taxes). Sadly, however, it defers the information-carrying words to the headline's second part, leading instead with off-putting blather. (I'll decide for myself whether to spread the word.)</p>
<p>
	The second subject line was on the only personalized email I received during the entire 2-week study and referred to a specific, closely fought race of interest to the user profile provided during the sign-up process. (In contrast, it's not true personalization to just mail-merge a user's name into a headline; that's presumptuous, not personal.)</p>
<h2>
	Who Will Win?</h2>
<p>
	Last time I scored the usability of <a class="old" href="../bush-vs-kerry-email-newsletters-rated/index.php" title="Alertbox: Bush vs. Kerry - Email Newsletters Rated"> campaign newsletters was in 2004</a>, when George W. Bush won with a usability score of 58% compared to John Kerry's 57%. In addition to winning my review, Bush also won the election.</p>
<p>
	(You can't directly compare those 2004 usability scores with the scores in this article, because I relied on an <a class="old" href="../targeted-email-newsletters/index.php" title="Alertbox: Targeted Email Newsletters Show Continued Strength"> earlier version of our newsletter research</a> to conduct the 2004 review. We now know much more about email usability, so a good rating is more difficult to achieve. Considering that high marks are harder to get now, it's a nice sign of usability progress that the winning score has increased from 58% in 2004 to 63% in 2010.)</p>
<p>
	In 1996, I evaluated <a class="out" href="http://www.nytimes.com/library/cyber/week/0920candidates.php" title="New York Times: A Web Design Expert Reviews the Candidates' Sites"> Bill Clinton vs. Bob Dole for usability</a>. Even though my initial score was the same for both candidates, Clinton followed all of my recommendations within 2 weeks, whereas Dole never bothered to do what I told him. Thus, for most of the campaign, Clinton's site had superior usability. And he won that election.</p>
<p>
	Given this track record of usability scores predicting election outcomes, will the Conservatives win the UK election on May 6? Not necessarily, because much can happen from April to May. Also, the Internet probably influences only 1–2% of an election's outcome. In a close race (such as Bush vs. Kerry), good usability might move the needle enough to determine the winner.</p>
<p>
	If the race is tight in May, then maybe the Conservatives will win because of their superior usability. But if Gordon Brown pulls a last-minute rabbit out of his hat or if David Cameron goofs in the last television debate, the outcome could easily be reversed, because such events can move 5% or more of the electorate. Old media rules in the end. But that's no reason to ignore email, especially since it's a much cheaper medium.</p>
</section>
  

  
  <section>
    
        
    
  </section>
  
  
  
  
  

  


  <section>
    <br />
    <p class="share"><b>Share <span class="hide-for-small-down">this article</span>:</b> <a href="https://twitter.com/intent/tweet?url=http://www.nngroup.com/articles/uk-election-email-newsletters-rated/&amp;text=UK%20Election%20Email%20Newsletters%20Rated&amp;via=nngroup">Twitter</a> | <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=http://www.nngroup.com/articles/uk-election-email-newsletters-rated/&amp;title=UK%20Election%20Email%20Newsletters%20Rated&amp;source=Nielsen%20Norman%20Group">LinkedIn</a> | <a href="https://plus.google.com/share?url=http://www.nngroup.com/articles/uk-election-email-newsletters-rated/">Google+</a> | <a href="mailto:?subject=NN/g Article: UK Election Email Newsletters Rated&amp;body=http://www.nngroup.com/articles/uk-election-email-newsletters-rated/">Email</a></p>
    <hr />
  </section>

  <footer class="l-subsection">
    
    
    <div class="learn-more">
      <h2>Learn More</h2>
      <p><a href="../subscribe/index.php">Subscribe to the weekly newsletter</a> to get notified about future articles.</p>
      </div>
    
    

    <div class="row show-for-small-down related-content">
      
      
      

      
      
        <div class="column small-12 topic-links">All articles about:
          <ul>
            
            <li><a href="../../topic/email/index.php">Email</a></li>
            
          </ul>
        </div>
      
      

      
      
        <div class="column small-12">
          <h3>UX Conference Training Courses</h3>
           <ul class="no-bullet">
    
        <li><a href="../../courses/cross-channel-user-experience/index.php">Omnichannel Journeys and Customer Experience</a></li>
    
</ul>
        </div>
      
      

      
      
        <div class="small-12 column">
          <h3>Research Reports</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../reports/ecommerce-transactional-email-confirmation-message/index.php">Vol. 12: Transactional Email and Confirmation Messages</a></li>
              
            
              
                <li><a href="../../reports/email-newsletter-design/index.php">Email Newsletter Design  to Increase Conversion and Loyalty  </a></li>
              
            
          </ul>
        </div>
      
      

      
      
      
    </div>

    <div class="hide-for-small-down row related-content">
      <div class="medium-6 columns">
        
        
          <div>
            <h3>Articles</h3>
            <ul class="no-bullet">
              
                
                <li><a href="../email-subject-lines/index.php">Email Subject Lines: 5 Tips to Attract Readers</a></li>
                
              
                
                <li><a href="../mobile-email-newsletters/index.php">Mobile Email Newsletters</a></li>
                
              
                
                <li><a href="../e-mail-newsletters-usability/index.php">E-Mail Newsletters: Increasing Usability</a></li>
                
              
                
                <li><a href="../email-press-releases-journalists/index.php">E-Mailing Press Releases to Journalists</a></li>
                
              
                
                <li><a href="../transactional-and-confirmation-email/index.php">Transactional Email and Confirmation Messages</a></li>
                
              
            </ul>
          </div>
        
        

        
          
          <div>
            <h3>Research Reports</h3>
            <ul class="no-bullet">
              
                
                  <li><a href="../../reports/ecommerce-transactional-email-confirmation-message/index.php">Vol. 12: Transactional Email and Confirmation Messages</a></li>
                
              
                
                  <li><a href="../../reports/email-newsletter-design/index.php">Email Newsletter Design  to Increase Conversion and Loyalty  </a></li>
                
              
            </ul>
          </div>
          
        

      </div>
      <div class="medium-6 columns">
        
          
          <div>
            <h3>UX Conference Training Courses</h3>
             <ul class="no-bullet">
    
        <li><a href="../../courses/cross-channel-user-experience/index.php">Omnichannel Journeys and Customer Experience</a></li>
    
</ul>
          </div>
          
        
        
      </div>
    </div>
  </footer>
</article>

        </div>

      

    
  </div>
  <div class="row">
    
      <div class="column small-12 show-for-small-down l-navrail announcements">
        
          
        
      </div>
    
    
  </div>

    </div>
    




<footer id="page-footer">
    <div class="row hide-for-print">
        <div class="column small-12 medium-6">
            <section class="footer-popular">
                
                <h1>Popular Topics</h1>
                <div style="float: left;  width: 15em; display: block;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/applications/index.php">Application Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/e-commerce/index.php">E-Commerce Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/eyetracking/index.php">Eyetracking </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/information-architecture/index.php">Information Architecture </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/intranets/index.php">Intranet Design </a></li>
	<li><a href="../../topic/interaction-design/index.php">Interaction Design </a></li>
</ul>
</div>

<div style="float: left;  width: 15em; display: block; list-style: none;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/mobile-and-tablet-design/index.php">Mobile and Tablet Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/credibility-persuasion/index.php">Persuasive Design</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/research-methods/index.php">Research Methods</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/user-testing/index.php">User Testing </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/web-usability/index.php">Web Usability </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/writing-web/index.php">Web Writing &amp; Content Strategy </a></li>
</ul>
</div>

<p>&nbsp;</p>

                
            </section>
        </div>

        <div class="column small-12 medium-6">
            <div class="row">
                <div class="column small-6">
                    <section class="footer-about">
                        <h1>Follow Us</h1>
                        <ul>
	<li><a href="../subscribe/index.php">Subscribe to our weekly UX newsletter</a>,&nbsp;Jakob Nielsen&#39;s Alertbox</li>
	<li>Visit Don Norman&#39;s website, <a href="http://www.jnd.org/"> www.jnd.org </a></li>
	<li>Visit Bruce Tognazzini&#39;s website, <a href="http://www.asktog.com/"> www.asktog.com </a></li>
	<li style="padding-top: 6px;"><a class="twitter-follow-button" data-show-count="false" data-size="large" href="https://twitter.com/nngroup"><img alt="Follow @nngroup on Twitter" height="20" src="https://media.nngroup.com/media/editor/2016/12/30/twitter-follow-button.png" style="width: 117px; height: 20px;" width="117" /></a></li>
	<li style="padding-top: 6px;"><img alt="" height="14" src="https://media.nngroup.com/media/editor/2016/07/26/feed-icon-14x14.png" style="vertical-align:text-top;width:14px;margin-right:.25em;" width="14" /><a href="../../feed/rss/index.php">Subscribe to our RSS feed</a></li>
</ul>

                    </section>
                </div>

                <div class="column small-6">
                    <section class="footer-about right-column">
                        <h1>About
</h1>
                        <ul class="no-bullet">
	<li><a href="../../about/contact/index.php">Contact information</a></li>
	<li><a href="../../about/index.php">About Us</a></li>
	<li><a href="../../about/why-nng/index.php">Why NN/g? </a></li>
	<li><a href="../../return-policy/index.php">Return Policy</a></li>
</ul>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="copyright column small-12">
            <p><a href="../../copyright-and-reprint-info/index.php">Copyright</a> &copy; 1998-2017 Nielsen Norman Group, All Rights Reserved.</p>
        </div>
    </div>
</footer>


    


        <script src="https://media.nngroup.com/static/vendor/fastclick/js/fastclick.min.js"></script>
        <script src="https://media.nngroup.com/static/vendor/foundation/js/foundation.min.js"></script>
        <script src="http://media.nngroup.com/static/js/app.js"></script>
        <script src="http://media.nngroup.com/static/js/scroll-to-fixed.js"></script><script>var externalLinks = document.querySelectorAll('a[href^="http"]');for (var i = externalLinks.length-1; i >= 0; i--) {externalLinks[i].addEventListener("click", function(event) { alert("External links are disabled for this test."); event.preventDefault(); }, false);}var mailLinks = document.querySelectorAll('a[href^="mailto"]');for (var i = mailLinks.length-1; i >= 0; i--) {mailLinks[i].addEventListener("click", function(event) { alert("Email links are disabled for this test."); event.preventDefault(); }, false);}var externalForms = document.querySelectorAll('form');for (var i = externalForms.length-1; i >= 0; i--) {externalForms[i].addEventListener("submit", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false); externalForms[i].addEventListener("click", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false);}</script>

        <script>
            
            $(function() {
                $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
                $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
            });
        </script>

        
<script src="https://media.nngroup.com/static/js/articles.js"></script>


        
        <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1072699960;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072699960/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
    </body>

<!-- Mirrored from www.nngroup.com/articles/uk-election-email-newsletters-rated/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:14:07 GMT -->
</html>
