<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->





    
<!-- Mirrored from www.nngroup.com/articles/wechat-integrated-ux/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:07:59 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1026.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"beacon":"bam.nr-data.net","queueTime":0,"applicationTime":1128,"agent":"","licenseKey":"1a30ef9931","errorBeacon":"bam.nr-data.net","applicationID":"2048992","transactionName":"NFxSMRBUX0EDUkIPDA0WdhAMVkVbDV8ZBxEXUFMJB0YfRAtUQRVZIktEDAFZVHYHRVcPD01eVRE="}</script>
        <title>WeChat: China’s Integrated Internet User Experience</title><meta property="og:title" content="WeChat: China’s Integrated Internet User Experience" />
  
        
        <meta name="description" content="User research finds that tightly integrated services with simple and unified design make people use WeChat; mainly through traditional GUI interactions.">
        <meta property="og:description" content="User research finds that tightly integrated services with simple and unified design make people use WeChat; mainly through traditional GUI interactions." />
        
  
        
	
        
        <meta name="keywords" content="WeChat, Tencent, China, Chinese, Beijing, integration, chat, IM, Moments, payments, payment, conversational user interfaces, natural language, GUI, menus, consistency">
        

        

        <meta name="robots" content="noarchive">
        <meta name="viewport" content="width=device-width">

        <link rel="search" type="application/opensearchdescription+xml" title="NN/g Search" href="../../opensearch.xml" />
        <link rel="shortcut icon" href="http://media.nngroup.com/static/img/favicon.ico" />

        <link rel="stylesheet" href="https://media.nngroup.com/static/vendor/font-awesome/css/font-awesome.min.css">

        
        <link rel="stylesheet" href="https://media.nngroup.com/static/css/7232a960f1df.css" type="text/css" />
        <link rel="stylesheet" href="http://fast.fonts.com/t/1.css?apiType=css&amp;projectid=c5846260-9def-46ec-9407-0f1349bd2d64" type="text/css">

        <script src="https://media.nngroup.com/static/vendor/modernizr/js/modernizr.min.js"></script>

        <script type="text/javascript" src="https://media.nngroup.com/static/js/3283be5def74.js"></script>
        

        

        
        
        <!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=229034,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('http://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->


<!-- Google Analytics Tracking Code for http://nngroup.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23132681-1', 'nngroup.com');
  ga('require', 'linkid', 'https://www.nngroup.com/articles/wechat-integrated-ux/linkid.js');
  ga('require', 'displayfeatures');

// Optimizely Universal Analytics Integration
 window.optimizely = window.optimizely || [];
 window.optimizely.push("activateUniversalAnalytics");

  ga('send', 'pageview');

</script>
<script>
/**
* Function to track a click on an outbound link in Google Analytics.
*/
function trackOutboundLink(label, url) {
    if(window.ga && ga.loaded) {
        ga('send', 'event', 'outbound', 'click', label, {'hitCallback': function(){document.location = url;}
        });
    } else {
        document.location = url;
    }
}
/**
* Function to track a click on a link in the article announcement area.
*/
$('#articleAnnouncement a').on('click', function(){
  ga('send', 'event', 'link', 'click', 'articleAnnouncement');
});
</script>

<script type="text/javascript">
 setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2421.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



<!-- Hotjar Tracking Code for http://nngroup.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:121605,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

        <script src="https://media.nngroup.com/static/js/analytics/track_outbound_link.js"></script>
        
        
        
    </head>
    <body class="location-articles articles-detail">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
    <header id="page-header">
        <div id="cart" class="shopping-cart mobile-cart show-for-small-down"></div>
        <div class="row">
            <div id="masthead" class="masthead small-12 medium-7 column">

                <!-- The button to toggles menu on small screen -->

                <h1><a href="../../index.php"><img src="http://media.nngroup.com/static/img/logo-wordmark.png" width="74" height="30"> Nielsen <span class="norman">Norman</span> Group</a></h1>
                <p class="tagline show-for-medium-up">Evidence-Based User Experience Research, Training, and Consulting</p>

                <!-- The button to toggles form search on small screen -->


                <div style="clear: right;width:  100%;height: 1px;"></div><?php define('ROOT_PATH',$_SERVER["DOCUMENT_ROOT"] . '/apps/navigation_styles/navigations/1'); ?><?php include(ROOT_PATH)."/navigation.php"; ?>

                <!-- Drop down menu on small screen -->
                <div id="hideNav" class="show-for-small-down">
                  
                  <ul class="menu-content">
                    <li><a href="../../index.php">Home</a></li>
                    <li class="has-submenu">
                      <a>Training</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../training/index.php">Overview</a></li>
                        
<li id="nav-ux-conference"><a href="../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../online-seminars/index.php"><span>Online Seminars</span></a></li>

                      </ul>
                    </li>
                    <li class="has-submenu">
                      <a>Consulting</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
  <li><a href="../../consulting/index.php">Overview</a></li>
  
  <li><a href="../../consulting/benchmarking/index.php">Benchmarking</a></li>
  
  <li><a href="../../consulting/expert-design-review/index.php">Expert Design Review</a></li>
  
  <li><a href="../../consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
  
  <li><a href="../../consulting/usability-testing/index.php">Usability Testing </a></li>
  
  <li><a href="../../consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
  
  <li><a href="../../in-house-training/index.php">In-House Training</a></li>
</ul>

                    </li>

                    <li><a href="../../reports/index.php">Reports</a></li>
                    <li><a href="../index.php">Articles</a></li>
                    <li class="has-submenu"><a>About NN/G</a><i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../about/index.php">Overview</a></li>
                        <li><a href="../../people/index.php">People</a></li>
                        <li><a href="../../about/why-nng/index.php">Why NN/g?</a></li>
                        <li><a href="../../about/contact/index.php">Contact</a></li>
                        <li><a href="../../news/index.php">News</a></li>
                        <li><a href="../../about/history/index.php">History</a></li>
                        <li><a href="../../books/index.php">Books</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                </div>

                <hr class="header-divider show-for-small-only"/>


            </div>
       </div>

        <div id="hideNav">            <nav class="primary-nav small-12 column">
                <ul>
                    <li><a class="navitem" id="nav-home" href="../../index.php">Home</a></li>
                    <li><a class="navitem" id="nav-training" href="../../training/index.php">Training</a></li>
                    <li><a class="navitem" id="nav-consulting" href="../../consulting/index.php">Consulting</a></li>
                    <li><a class="navitem" id="nav-reports" href="../../reports/index.php">Reports</a></li>
                    <li><a class="navitem" id="nav-articles" href="../index.php">Articles</a></li>
                    <li><a class="navitem" id="nav-about" href="../../about/index.php">About NN/G</a></li>
                </ul>
            </nav>
            <nav class="secondary-nav small-12 medium-12 large-12 columns">
                
                
            </nav>
        </div>
    </header>
    <div class="l-content">
    
  

  

  <div class="row">
    
      
      <section class="small-12 medium-3 columns collapsable-content l-navrail articles-left-column">
        <h1 class="show-for-small-down collapse-link mobile-browse-header">Browse by Topic and Author</h1>
          <div class="row collapsable">

            
            <div class="small-6 medium-12 columns">
              <section id="article-popular-topics" style="display: none;">
                
                <h1>Topics</h1>
                <ul id="article-popular-topics">
                    
                    <li>
                        
                        <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/intranets/all/index.php">Intranets</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/user-testing/all/index.php">User Testing</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                        
                    </li>
                    
                    <li><a href="#" id="article-see-all-topics"><strong>See all topics&hellip;</strong></a></li>
                </ul>
                
              </section>
              <section id="article-all-topics" style="display: none;">
                
                <h1>
                  <span class="show-for-medium-up">All Article Topics</span>
                  <span class="show-for-small-down">All Topics</span>
                  (<a href="#" id="article-hide-all-topics">hide</a>)
                </h1>
                <ul>
                  
                  <li>
                    
                    <a href="../../topic/accessibility/all/index.php">Accessibility</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/agile/all/index.php">Agile</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/analytics-and-metrics/all/index.php">Analytics &amp; Metrics</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/applications/all/index.php">Application Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/b2b-websites/all/index.php">B2B Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/behavior-patterns/all/index.php">Behavior Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/branding/all/index.php">Branding</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/content-strategy/all/index.php">Content Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/corporate-websites/all/index.php">Corporate Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/design-patterns/all/index.php">Design Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-design-process/all/index.php">Design Process</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/email/all/index.php">Email</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/eyetracking/all/index.php">Eyetracking</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/heuristic-evaluation/all/index.php">Heuristic Evaluation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/human-computer-interaction/all/index.php">Human Computer Interaction</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-ideation/all/index.php">Ideation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/information-architecture/all/index.php">Information Architecture</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/interaction-design/all/index.php">Interaction Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/international-users/all/index.php">International Users</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/intranets/all/index.php">Intranets</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-management/all/index.php">Management</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/navigation/all/index.php">Navigation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/non-profit-websites/all/index.php">Non-Profit Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/personas/all/index.php">Personas</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/persuasive-design/all/index.php">Persuasive Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/prototyping/all/index.php">Prototyping</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/psychology-and-ux/all/index.php">Psychology and UX</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/research-methods/all/index.php">Research Methods</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/search/all/index.php">Search</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/social-media/all/index.php">Social Media</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/strategy/all/index.php">Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/user-testing/all/index.php">User Testing</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-teams/all/index.php">UX Teams</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/visual-design/all/index.php">Visual Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/writing-web/all/index.php">Writing for the Web</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/young-users/all/index.php">Young Users</a>
                    
                  </li>
                  
                </ul>
                
              </section>
            </div>
            <script>
              /* Determine which topics list will be displayed: the full or the
               * partial.  We are inlining this close to the list itself to
               * prevent rendering delays on the client
               */
              var topic_list = $.cookie("article-topics");

              var popular_topics = $('#article-popular-topics');
              var all_topics = $('#article-all-topics');

              if(topic_list == 1) {
                  popular_topics.hide();
                  all_topics.show();
              } else {
                  popular_topics.show();
                  all_topics.hide();
              }
            </script>
            

            
            <div class="small-6 medium-12 columns">
              
              <section id="article-initial-authors">
                  <h1>Author</h1>
                  <ul>
                    
                    <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                    
                    <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                    
                    <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                    
                    <li><a href="#" onClick="$('#article-initial-authors').hide(); $('#article-all-authors').show();"><strong>See all authors&hellip;</strong></a></li>
                  </ul>
              </section>
              <section id="article-all-authors" style="display: none;">
                <h1>
                  All Authors
                  (<a href="#" id="article-hide-all-authors">hide</a>)
                </h1>
                <ul>
                  
                  <li><a href="../author/raluca-budiu/index.php">Raluca Budiu</a></li>
                  
                  <li><a href="../author/patty-caya/index.php">Patty Caya</a></li>
                  
                  <li><a href="../author/susan-farrell/index.php">Susan Farrell</a></li>
                  
                  <li><a href="../author/therese-fessenden/index.php">Therese Fessenden</a></li>
                  
                  <li><a href="../author/kim-flaherty/index.php">Kim Flaherty</a></li>
                  
                  <li><a href="../author/sarah-gibbons/index.php">Sarah Gibbons</a></li>
                  
                  <li><a href="../author/aurora-harley/index.php">Aurora Harley</a></li>
                  
                  <li><a href="../author/kate-kaplan/index.php">Kate Kaplan</a></li>
                  
                  <li><a href="../author/page-laubheimer/index.php">Page Laubheimer</a></li>
                  
                  <li><a href="../author/angela-li/index.php">Angie Li</a></li>
                  
                  <li><a href="../author/hoa-loranger/index.php">Hoa Loranger</a></li>
                  
                  <li><a href="../author/kate-meyer/index.php">Kate Meyer</a></li>
                  
                  <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                  
                  <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                  
                  <li><a href="../author/kara-pernice/index.php">Kara Pernice</a></li>
                  
                  <li><a href="../author/christian-rohrer/index.php">Christian Rohrer</a></li>
                  
                  <li><a href="../author/amy-schade/index.php">Amy Schade</a></li>
                  
                  <li><a href="../author/katie-sherwin/index.php">Katie Sherwin</a></li>
                  
                  <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                  
                  <li><a href="../author/kathryn-whitenton/index.php">Kathryn Whitenton</a></li>
                  
                </ul>
              </section>
              
            </div>
            
          </div>

          
          <section class="hide-for-small-down">
            
              
              <h1>Recent Articles</h1>
              
              <ul>
                  
                  <li><a href="../seamless-cross-channel/index.php">Seamlessness in the Omnichannel User Experience</a></li>
                  
                  <li><a href="../brand-intention-interpretation/index.php">Experience Design: Bridging Brand Intention and Brand Interpretation</a></li>
                  
                  <li><a href="../eyetracking-tasks-efficient-scanning/index.php">Scanning Patterns on the Web Are Optimized for the Current Task</a></li>
                  
                  <li><a href="../flat-design-best-practices/index.php">Flat-Design Best Practices</a></li>
                  
                  <li><a href="../ux-user-stories/index.php">Accounting for UX Work with User Stories in Agile Projects</a></li>
                  
                  <li><a href="../index.php"><strong>See all articles&hellip;</strong></a></li>
              </ul>
              
            
          </section>
          

          
          
          <section class="hide-for-small-down">
            <h1>Popular Articles</h1>
            
            <ul>
                
                <li><a href="../usability-101-introduction-to-usability/index.php">Usability 101: Introduction to Usability</a></li>
                
                <li><a href="../top-10-mistakes-web-design/index.php">Top 10 Mistakes in Web Design</a></li>
                
                <li><a href="../how-users-read-on-the-web/index.php">How Users Read on the Web</a></li>
                
                <li><a href="../f-shaped-pattern-reading-web-content/index.php">F-Shaped Pattern For Reading Web Content</a></li>
                
                <li><a href="../ten-usability-heuristics/index.php">10 Usability Heuristics for User Interface Design</a></li>
                
                <li><a href="../intranet-design-2016/index.php">10 Best Intranets of 2016</a></li>
                
                <li><a href="../which-ux-research-methods/index.php">When to Use Which User-Experience Research Methods</a></li>
                
                <li><a href="../response-times-3-important-limits/index.php">Response Times: The 3 Important Limits</a></li>
                
                <li><a href="../why-you-only-need-to-test-with-5-users/index.php">Why You Only Need to Test with 5 Users</a></li>
                
                <li><a href="../page-fold-manifesto/index.php">The Fold Manifesto: Why the Page Fold Still Matters</a></li>
                
            </ul>
          </section>
          
          

          
            <section class="hide-for-small-down">
              
            </section>
          

      </section>
      

      
        <div class="small-12 medium-9 columns content-body">
          




<article class="l-subsection alertbox-article-content article-detail-bl">
  
  <header>
    <div class="l-subsection">
      <h1>WeChat: China’s Integrated Internet User Experience</h1>
      <div class="author-meta">by
        
          
            
            <a href="../author/yuno-cheng/index.php">Yunnuo Cheng</a>
            
            and
            
            <a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a>
            
          
        on  August 21, 2016
        
      </div>

      

      
      <div class="topic-links">
        Topics:
        
          <ul>

  <li><a href="../../topic/international-users/index.php">International Users</a></li>

  <li><a href="../../topic/mobile-and-tablet-design/index.php">Mobile &amp; Tablet</a></li>

  <li><a href="../../topic/web-usability/index.php">Web Usability</a></li>

  <li><a href="../../topic/social-media/index.php">Social Media</a></li>

</ul>
        
      </div>
      
    </div>
  </header>
  

  
  
  <section>
    <p class="lede"><strong>Summary:</strong> UX research finds that tightly integrated services with a wide-ranging set of convenient features, accessed through a simple and unified design, are the reason Chinese users use WeChat so much. People mainly use traditional GUI interactions, not a “conversational user interface,” despite the hype.</p>
    <br />
  </section>
  
  

  
  <section id="article-body"><p>The big Internet companies of the Western world have proclaimed “conversational user interfaces” and chatbots to be the Next Big Thing, with major initiatives led by Amazon, Apple, Facebook, Google, and Microsoft. Even Taco Bell has a “TacoBot,” a service that allows users to order and customize their tacos using a natural-language UI.</p>

<p>Much of this hype stems from angst generated by the success of the Chinese WeChat service, which had <strong>700 million users</strong> as of April 2016. WeChat has been touted as the poster child for conversational user interfaces. In this article, we report on user research we did in China with WeChat users. The study aimed to uncover practices in which WeChat users engaged, as well as why and in which cases its users preferred to use WeChat instead of regular mobile websites and apps.</p>

<h2>WeChat: All You Want in One Place</h2>

<p>As the name implies, WeChat started as a mobile instant-messaging (IM) app, launched by Tencent in 2011. However, WeChat quickly added other features; today, it is no longer primarily an IM application, but it also includes:</p>

<ul>
	<li>A <strong>payment</strong> <strong>service</strong>. 200 million users have connected their bank accounts to WeChat. This capability allows person-to-person money transfer, including 8 billion “red packets” (so called because gifts of lucky money were traditionally given in red envelopes) sent during the 2016 Chinese New Year celebrations, as well as easy payments to either online or real-world businesses.</li>
	<li>A platform for <strong>companies’ online presence</strong>, with 10 million “official accounts.” <strong>Official accounts</strong> are WeChat-supported mini-websites that are easy to establish and especially convenient for small businesses, compared with the high-tech skills needed to build and maintain an interactive website.</li>
	<li><strong>An ecommerce platform</strong>, which is particularly convenient because it’s integrated with the payment service.</li>
	<li><strong>Social networking.</strong> The “<strong>Moments</strong>” feature is somewhat like a Facebook wall where friends can post messages in different formats (e.g., text, audio, pictures, stickers, videos). Furthermore, <strong>message dialogues </strong>allow users to interchange text, audio, pictures, stickers, short videos that are taken within WeChat, links, contact cards, and documents.</li>
	<li>Other <strong>social services</strong>, such as “people nearby.”</li>
	<li><strong>Games.</strong></li>
	<li>A broad range of <strong>3<sup>rd</sup> party services</strong>. Examples include ordering a taxi, booking train and airline tickets, buying movie tickets, and paying utility bills.</li>
	<li><strong>Integration with the physical world. </strong><a href="../wechat-qr-shake/index.php">QR scanning</a> is widely used to quickly access WeChat official accounts, and exchange information (for example, contact information). There are even plush toys that connect to WeChat.</li>
</ul>

<figure class="caption"><img alt="Little girl receiving a message from her parent on her Mon Mon toy, as indicated by the toy's heart glowing" height="330" src="https://media.nngroup.com/media/editor/2016/08/19/mon-mon-wechat-plush-toy.jpg" width="600"/>
<figcaption><em>The Mon Mon plush toy for little children to interchange voice messages with their parents’ WeChat accounts. (Still from advertisement created by the vendor, the Chinese toy company Dan Dan Man.)</em></figcaption>
</figure>

<p>In total, <strong>more than</strong> <strong>a third of the time spent by Chinese users on mobile is on WeChat</strong>. No wonder Western companies want to get in on the action.</p>

<p>However, the key UX advantage of WeChat is not that it grew out of a chat service; it’s the <strong>integrated user experience</strong>. Each individual service is fine, but not necessarily better than those offered by other companies. In fact, our user testing of WeChat revealed many usability problems in various areas. What’s superior is how these services play together and reinforce each other. Most importantly, <strong>these benefits are not the result of a superior, simple conversational UI</strong>; instead, they are often provided through a simplified graphical user interface (GUI).</p>

<p>WeChat is similar with a megaportal providing access to a huge number of services. It’s almost a parallel, alternate web, whose explosive evolution seems to be rooted in two different aspects:</p>

<ol>
	<li>A huge initial adoption of the IM service and, later, of the payment ability</li>
	<li>The scarcity of mobile-optimized sites and services on the Chinese web</li>
</ol>

<p>The first aspect led to widespread adoption and provided a convenient substitute for credit cards (a less common method of payment in China); the second provided one (and often the only) convenient way of online access to a business or a service.</p>

<p>We asked our Chinese research participants to draw their <a href="../mental-models/index.php">mental models</a> of WeChat, and as shown in the following examples, many of them considered the smooth payment service and the official accounts more central than that the chat service.</p>

<figure class="caption"><img alt="" height="589" src="https://media.nngroup.com/media/editor/2016/08/19/wechat-mental-model-1.png" width="800"/><br/>
<img alt="Mental model drawing 2" height="589" src="https://media.nngroup.com/media/editor/2016/08/19/wechat-mental-model-2.png" width="800"/>
<figcaption><em>Mental models of WeChat, as drawn by two of our study participants.</em></figcaption>
</figure>

<h2>User Research</h2>

<p>We conducted two rounds of user research in China:</p>

<ul>
	<li>A <a href="../diary-studies/index.php"><strong>diary study</strong></a>, in which 12 participants logged their WeChat use for 7 days:

	<ul style="list-style-type:circle;">
		<li>Users sent us a WeChat message each time they used a special WeChat functionality.</li>
		<li>Users also completed two questionnaires at the end of each day: one about their use of basic WeChat features and one about their use of special WeChat features. Basic features were defined as messages and chat with friends, checking <em>Moments</em> (the Facebook-wall equivalent), and reading articles published by official accounts. All other activities were considered special features; these included interacting with official accounts, posting to Moments, using payments, scanning QR codes, and shopping.</li>
	</ul>
	</li>
	<li>A <strong>usability test</strong>, using the <a href="../thinking-aloud-the-1-usability-tool/index.php">thinking-aloud method</a>, where 14 participants used WeChat and other mobile apps to perform <a href="../task-scenarios-usability-testing/index.php">representative tasks</a> during 90-minute sessions. At the end of these sessions, most users sketched their mental models of WeChat.</li>
</ul>

<p>About half of the users were in Beijing (one of China’s advanced “tier-1” cities, with 21 million inhabitants) and the other half were in Tangshan (a smaller and more traditional industrial “tier-3” city, with 7 million inhabitants). The specific <a href="../location-irrelevant-for-user-testing/index.php">location doesn’t usually matter for usability testing</a>, but, in this case, market research suggests that WeChat penetration varies in different parts of China: 93% market penetration in tier-1 cities vs. 43% in tier-3 cities. Local services (e.g., offline businesses using WeChat official accounts and payment tools) also differ in these two types of cities.</p>

<p>About half the study participants owned iPhones and the other half owned Android phones.</p>

<p>Here are some examples of usability-testing tasks (translated into English):</p>

<ul>
	<li><em>You are planning a trip to Beidaihe next Tuesday. Use WeChat to find what train tickets are available.</em></li>
	<li><em>Use WeChat to check how crowded it is now at Sanlitun.</em></li>
	<li><em>Your friend recommended “Jacky’s Crawfish.” Order some crawfish, and have it delivered to our testing location. Please stop before just before completing the purchase.</em></li>
</ul>

<h2>WeChat Payments Rule</h2>

<p>In the diary study, fully 32% of all recorded WeChat activities were payments, confirming this service’s central role of in WeChat. On average, during the 7-day diary, each participant made 6 payments — about one per day. The majority of the payments were directed towards offline businesses or physical persons, and actual ecommerce payments were in the minority.</p>

<figure class="caption"><img alt="" height="310" src="https://media.nngroup.com/media/editor/2016/08/19/wechat-payment-recipients.png" width="305"/>
<figcaption><em>Recipients of WeChat payments made by our study participants.</em></figcaption>
</figure>

<p>The main reason for the popularity of the WeChat payment service was its smooth user experience and its ability to cut across channels and across the business/social divide. As one study participant said, “When I am paying with WeChat, I only need to take my phone out — no bank card, no signatures, no cash, no change — so convenient!”</p>

<p>The next two major uses of WeChat were <em>Moments</em> (the Facebook equivalent) with 16% of use and official accounts (miniwebsites hosted by WeChat) with 10% of use. The many other WeChat features accounted for the remaining interactions and thus summed to 42% percent of the overall use.</p>

<h2>Integration and Consistency of User Experience</h2>

<p>What makes WeChat superior is its <strong>integration</strong>: many features that all work together and build on each other. Once users subscribe to an official account, they can receive content pushed by that account. They can also access the WeChat page of that account and find more relevant information about the company or read associated pieces of content. They can purchase products or send money to the company. Thus, subscribing to an account opens up a world of interaction possibilities with that company, all strongly tied into WeChat.</p>

<figure class="caption"><img alt="" height="444" src="https://media.nngroup.com/media/editor/2016/08/19/palace-museum-wechat-navigation.png" width="811"/><br/>
<img alt="" height="334" src="https://media.nngroup.com/media/editor/2016/08/19/palace-museum-wechat-page.jpg" width="188"/>
<figcaption><em>Palace Museum account: The user can select one of the tabs at the bottom of the screen to interact with the Palace Museum (top left). Each tab opens up a submenu (top middle). A list of articles about the Palace Museum accessed through one of the menu options (top right). A WeChat “webpage” for the Palace Museum can be accessed through another one of the menus (bottom). (Note, however, that the Palace Museum’s webpage accessed through a regular mobile browser is actually not mobile optimized.)</em></figcaption>
</figure>

<p>These WeChat services are often not available through other mobile channels — for example, the Palace Museum does not have a mobile-optimized website available through a mobile browser. Thus, many users will prefer to interact with a company through the consistent and relatively predictable WeChat interface instead of risking to go to a different channel and have an inferior user experience.</p>

<figure class="caption"><img alt="" height="335" src="https://media.nngroup.com/media/editor/2016/08/19/palace-museum-mobile-website.jpg" width="188"/>
<figcaption><em>The Palace Museum website as accessed through a mobile browser is not mobile optimized.</em></figcaption>
</figure>

<p>For companies, WeChat is an inexpensive way to ensure a presence on mobile; that presence has the advantage of offering a <strong>standardized, consistent user experience</strong>. Users are likely to succeed when engaging with an official account because most of them look the same — they are the equivalent of <a href="../end-of-web-design/index.php">websites designed according to the same design pattern</a>. The interaction is thus easy to learn and is made of the same WeChat supported building blocks that people are already familiar with from countless previous interactions with other companies.</p>

<h2>WeChat vs. Traditional Websites</h2>

<p>Although the mobile web is perhaps not as strong in China at this point (with many sites not having mobile versions), it is most likely that WeChat and regular websites will continue to coexist because they each have advantages and disadvantages. Our users didn’t expect the WeChat official accounts to cover all their needs, but they saw them as an easy way to access commonly used features.</p>

<p>Our users said that they preferred using companies’ official accounts on WeChat for several reasons:</p>

<ul>
	<li><strong>Ease of access</strong>. WeChat is the primary QR-scanning application, freeing users from typing which has high <a href="../interaction-cost-definition/index.php">interaction cost</a> on mobile phones and is particularly unpleasant for older users.</li>
	<li><strong>Ease of interaction.</strong> As discussed above, it is easier to interact with an official account — you know where to click and how to use it because they all share the same basic interaction style.</li>
	<li><strong>Access to small businesses.</strong> Many such business (e.g., a fruit shop, handcraft, interest-based organizations) are unlikely to invest in a real website.</li>
	<li><strong>Special discounts.</strong> Followers of a WeChat account often receive promotions or exclusive campaigns.</li>
	<li><strong>Up-to-date information.</strong> There’s obviously no fundamental reason why a company’s website could not be kept as current as an official account, but users’ perception was that websites were more static, whereas WeChat accounts were updated more frequently.</li>
</ul>

<p>While WeChat official accounts have their benefits, users continued to use traditional websites in parallel with WeChat, for several reasons:</p>

<ul>
	<li><strong>More complex functionality</strong>, beyond the simple WeChat-supported UI</li>
	<li><strong>Spam</strong>: Too many articles pushed by WeChat official accounts can easily become annoying, even for followers of those accounts. Some abuse users’ trust and flood them with ads.</li>
	<li><strong>Closed system</strong>: WeChat excludes access to frequently used services such as Taobao (a major ecommerce site) and important music providers.</li>
	<li><strong>Impaired findability:</strong> Often different accounts will have similar names, and some of them may belong to a legit company and others may be fake and trying to impersonate that company. To make things worse, the same company may have different WeChat accounts, and the accounts themselves may be of different types (service accounts vs. subscription accounts) with different capabilities (service accounts will support payments, while subscription accounts won’t). Finding the right account can thus be challenging for users.</li>
	<li><strong>Trust issues:</strong> Some articles on WeChat spread fake information.</li>
</ul>

<figure class="caption"><img alt="" height="347" src="https://media.nngroup.com/media/editor/2016/08/19/search-palace-museum.jpg" width="196"/> <img alt="" height="348" src="https://media.nngroup.com/media/editor/2016/08/19/search-xuxian.jpg" width="196"/>
<figcaption><em>WeChat search results for Palace Museum (left) and for Xuxian, the fruit business (right). Checkmarks indicate that the account is verified by WeChat. But these verification checkmarks do not help identify the desired account, because many verified results may actually be returned. Some of these results will belong to the same company but will have different functionalities, and some will belong to a fake company that may take advantage of a better-known brand’s name. Users can differentiate companies by clicking on their introduction page or checking their messaging history, but these operations are time consuming.</em></figcaption>
</figure>

<p>Some of these problems are similar to those faced by users of traditional search engines, whereas others are unique to WeChat, especially the peculiar distinction between subscription and service accounts, which breaks the integrated model of the service and seems to be a legacy of WeChat’s history.</p>

<h2>WeChat Usability Problems</h2>

<p>Our user testing revealed plenty of usability problems in WeChat and the various official accounts we tested. This is no surprise, since the perfect user interface hasn’t been built yet, but should come as a warning to those technology enthusiasts who believe that simply moving to a new platform or embracing a new interaction style will result in great user experience. On the contrary: any design will still need polishing and adaptation to users’ real needs, as opposed to companies’ internal hopes for what their customers will do. We’ll just give two examples here.</p>

<p><strong>Team Maker</strong> allows users to define group events. The setup screen requires users to enter a name for the new event, but 10 of 14 study participants overlooked this field and went straight for the second field (denoted by a clock icon) to enter the time of the event.</p>

<figure class="caption"><img alt="" height="354" src="https://media.nngroup.com/media/editor/2016/08/19/team-maker-screenshot.jpg" width="200"/>
<figcaption><em>Team Maker’s official account: The top field had the instructions “Event Title (Required),” but it was overlooked by most test users.</em></figcaption>
</figure>

<p>Several design issues combined to cause this problem:</p>

<ul>
	<li><a href="../banner-blindness-old-and-new-findings/index.php">Banner blindness</a> possibly made users overlook the top field because of the graphic.</li>
	<li>The prompt was shown as a <a href="../form-design-placeholders/index.php">placeholder text within the field</a>, which is less noticeable than a separate field label.</li>
	<li>That field was not aligned with the other fields in the form, and thus looked as if it was not part of a form, but rather a static title.</li>
	<li>The activity title prompt was shown in <a href="../low-contrast/index.php">low-contrast text</a>, especially compared with the brighter text used for the time field.</li>
	<li>Users may have had a selfish-action bias, preferring fields related to their own needs (the time of the group meeting) over fields related to system needs (the title).</li>
</ul>

<p><strong>City Heatmap</strong> is a WeChat feature that indicates how crowded a given location was. This feature is available under <em>Wallet → Public Services → City Heatmap. </em>Our users had great difficulty finding it<em>. </em>This navigational path was simply not expected, and suffered from a convoluted <a href="../../topic/information-architecture/index.php">information architecture</a> and <a href="../category-names-suck/index.php">problematic naming</a> with poor <a href="../information-scent/index.php">information scent</a>.</p>

<p>Such issues are not special to WeChat and we’ve seen them many times in usability studies. <strong>New platforms have old usability issues</strong>, because the main determinant of usability is the <a href="../../courses/human-mind/index.php">human mind</a> and people’s needs, not technology.</p>

<h2>Text-Based Interaction Gets Limited Use</h2>

<p>WeChat supports two different types of interaction with an account:</p>

<ol>
	<li>Text-based: Users can text a number or a keyword to a service account and receive an IM reply from that account. (The reply can be automatic or can be generated by a customer-service representative in charge with responding to WeChat queries).</li>
	<li>Menu/link based: Users can select a link embedded in a text message or they can use the menu buttons available on official-account interfaces and in the chat window.</li>
</ol>

<p>Sometimes all these interaction methods were available for an account, but in other situations only the more basic text interface was supported.</p>

<figure class="caption"><img alt="" height="347" src="https://media.nngroup.com/media/editor/2016/08/19/palace-museum-change-menu.jpg" width="196"/>
<figcaption><em>Palace Museum’s official account: users can interact with this account either by texting back one of the numbers 0–6, or by tapping the GUI menu button in the bottom left corner of the screen and selecting some of the available options. On some pages (but not in this case), the numeric options inside the text message are themselves tappable links. (In fact, many of our test participants tried to tap the options inside the message and were disappointed when that action did not produce any result.)</em></figcaption>
</figure>

<p>In our diary study and in the usability-testing sessions, we noticed only <strong>limited use of the text interface.</strong> In particular, some users texted back a number in response to a first message received after subscribing to a service account. They also occasionally texted back a keyword hoping to get back matching search results from that company. However, most users much preferred a menu-based interaction over the more effortful text-based interaction. Whenever the bottom menu was available for a service account, users relied on it, as a more expedient way to interact with the company. They also attempted to tap on the numerical options displayed in the chat message as an even faster way to make a choice.</p>

<p>Our initial interest in WeChat was to better understand conversational text interfaces. The appeal of such natural-language interfaces is that they supposedly allow users to simply express their goal and then sit back, while letting the site do all the work for them. No clicks or taps are involved once the initial query has been formulated, so in a sense, such interfaces have the potential of getting closer to the holy grail of usability ­— zero interaction cost. (Such interfaces do assume that users will be able to formulate a goal — an assumption that does not always hold, because <a href="../search-not-enough/index.php">users do not always know the search space</a> well enough.)</p>

<p>We did not find evidence for sophisticated natural-language understanding in WeChat. (Those human-staffed official accounts that reply to individual user queries are definitely not scalable in the long run and were not the norm in our study.) Instead of a true conversational text interface, we discovered a system that warrants the interest of an evolutionary web scientist for the way in which it mimics the evolution of the mobile web — a world in which historic, simple interaction such as the numeric-menu selection and keyword-input coexist with more sophisticated menu-based interfaces or GUIs. These latter methods are newer and often preferred by WeChat users for their lower interaction cost, yet, at this point, they have not yet completely erased the chat-box interface.</p>

<h2>Conclusion: WeChat Integration Builds Convenience</h2>

<p>If conversational interfaces are not a strength of WeChat, what is the secret behind its success? When we asked our users for their top likes about WeChat, the most frequently mentioned attribute was <strong>convenience</strong>.</p>

<p>WeChat shines in several ways that build convenience:</p>

<ul>
	<li>A <strong>wide variety </strong>of services and features</li>
	<li><strong>Integration </strong>of these features among themselves and, especially in the case of WeChat payment and QR codes, with the physical world</li>
	<li><strong>Simple, consistent interaction </strong>that stays the same across different official accounts — in marked contrast to the diversity of user interface techniques that plague websites</li>
</ul>

<p>Integration and simplicity are especially important because WeChat is mainly a mobile service. User interfaces are more difficult to use on <a href="../mobile-ux/index.php">mobile devices because of their screen-size limitations</a> and error-prone touch input. A simple interaction style that provides a seamless <a href="../customer-journeys-omnichannel/index.php">omnichannel customer journey</a> is essential for mobile designs.</p>

<p>Different usability aspects of WeChat were cited as major positives (second most common after convenience) <em>and</em> as major negatives by our study participants. No UI is completely good or completely bad in terms of usability, and while this interface had many good parts, there were others that definitely needed improvement.</p>

<figure class="caption"><img alt="" height="393" src="https://media.nngroup.com/media/editor/2016/08/19/wechat-good-bad.png" width="800"/>
<figcaption><em>Our study participants’ likes and dislikes about WeChat.</em></figcaption>
</figure>

<p>The interplay between usability and convenience points to one of the major contrasts in our field — <strong>that between usability and user experience (UX)</strong>. While usability applies to well-designed user interfaces (UIs), usability is not enough for a good UX. Users need <strong>useful features </strong>that address their <a href="../outcomes-vs-features/index.php">real needs instead of their imaginary ones</a>. When a system is convenient, it goes beyond usability and succeeds in being useful and simplifying a previously complicated task.</p>

<p>WeChat does all of this, and that’s why it’s successful. Not because it grew out of a chat site or originally used a conversational user interface.</p>
</section>
  

  
  <section>
    
        
    
  </section>
  
  
  
  
  

  


  <section>
    <br />
    <p class="share"><b>Share <span class="hide-for-small-down">this article</span>:</b> <a href="https://twitter.com/intent/tweet?url=http://www.nngroup.com/articles/wechat-integrated-ux/&amp;text=WeChat:%20China%e2%80%99s%20Integrated%20Internet%20User%20Experience&amp;via=nngroup">Twitter</a> | <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=http://www.nngroup.com/articles/wechat-integrated-ux/&amp;title=WeChat:%20China%e2%80%99s%20Integrated%20Internet%20User%20Experience&amp;source=Nielsen%20Norman%20Group">LinkedIn</a> | <a href="https://plus.google.com/share?url=http://www.nngroup.com/articles/wechat-integrated-ux/">Google+</a> | <a href="mailto:?subject=NN/g Article: WeChat: China’s Integrated Internet User Experience&amp;body=http://www.nngroup.com/articles/wechat-integrated-ux/">Email</a></p>
    <hr />
  </section>

  <footer class="l-subsection">
    
    
    <div class="learn-more">
      <h2>Learn More</h2>
      <p><a href="../subscribe/index.php">Subscribe to the weekly newsletter</a> to get notified about future articles.</p>
      </div>
    
    

    <div class="row show-for-small-down related-content">
      
      
      

      
      
        <div class="column small-12 topic-links">All articles about:
          <ul>
            
            <li><a href="../../topic/international-users/index.php">International Users</a></li>
            
            <li><a href="../../topic/mobile-and-tablet-design/index.php">Mobile &amp; Tablet</a></li>
            
            <li><a href="../../topic/technology/index.php">new technologies</a></li>
            
            <li><a href="../../topic/social-media/index.php">Social Media</a></li>
            
            <li><a href="../../topic/web-usability/index.php">Web Usability</a></li>
            
          </ul>
        </div>
      
      

      
      
        <div class="column small-12">
          <h3>UX Conference Training Courses</h3>
           <ul class="no-bullet">
    
        <li><a href="../../courses/ux-basic-training/index.php">UX Basic Training</a></li>
    
        <li><a href="../../courses/human-mind/index.php">The Human Mind and Usability</a></li>
    
        <li><a href="../../courses/cross-channel-user-experience/index.php">Omnichannel Journeys and Customer Experience</a></li>
    
        <li><a href="../../courses/emerging-patterns-web-design/index.php">Emerging Patterns for Web Design</a></li>
    
        <li><a href="../../courses/usability-mobile-websites-apps/index.php">Mobile User Experience</a></li>
    
</ul>
        </div>
      
      

      
      
        <div class="small-12 column">
          <h3>Research Reports</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../reports/tablets/index.php">Tablet Website and Application UX</a></li>
              
            
              
                <li><a href="../../reports/mobile-website-and-application-usability/index.php">User Experience for Mobile Applications and Websites</a></li>
              
            
              
                <li><a href="../../reports/b2b-websites-usability/index.php">B2B Website Usability for Converting Users into Leads and Customers</a></li>
              
            
              
                <li><a href="../../reports/strategic-design-faqs/index.php">Strategic Design for Frequently Asked Questions</a></li>
              
            
              
                <li><a href="../../reports/enterprise-mobile-showcase/index.php">Mobile Intranets and Enterprise Apps</a></li>
              
            
          </ul>
        </div>
      
      

      
      
        <div class="small-12 column">
          <h3>Online Seminars</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../online-seminars/fundamental-flaw-flat-design/index.php">The Fundamental Flaw in Flat Design</a></li>
              
            
              
                <li><a href="../../online-seminars/millennials-online/index.php">Millennials Online</a></li>
              
            
              
                <li><a href="../../online-seminars/beyond-screens/index.php">Beyond Screens</a></li>
              
            
              
                <li><a href="../../online-seminars/every-word-count/index.php">Making Every Word Count</a></li>
              
            
              
                <li><a href="../../online-seminars/university-websites/index.php">University Website UX Essentials</a></li>
              
            
          </ul>
        </div>
      
      
    </div>

    <div class="hide-for-small-down row related-content">
      <div class="medium-6 columns">
        
        
          <div>
            <h3>Articles</h3>
            <ul class="no-bullet">
              
                
                <li><a href="../mobile-behavior-india/index.php">Mobile User Behavior in India</a></li>
                
              
                
                <li><a href="../voice-interaction-ux/index.php">Voice Interaction UX: Brave New World...Same Old Story</a></li>
                
              
                
                <li><a href="../very-large-touchscreen-ux-design/index.php">Very Large Touchscreens: UX Design Differs From Mobile Screens</a></li>
                
              
                
                <li><a href="../legacy-tablet-ux/index.php">Tablet UX Research From the Pioneer Days</a></li>
                
              
                
                <li><a href="../cultural-nuances/index.php">Cultural Nuances Impact User Experience: Why We Test with International Audiences</a></li>
                
              
            </ul>
          </div>
        
        

        
          
          <div>
            <h3>Research Reports</h3>
            <ul class="no-bullet">
              
                
                  <li><a href="../../reports/tablets/index.php">Tablet Website and Application UX</a></li>
                
              
                
                  <li><a href="../../reports/mobile-website-and-application-usability/index.php">User Experience for Mobile Applications and Websites</a></li>
                
              
                
                  <li><a href="../../reports/b2b-websites-usability/index.php">B2B Website Usability for Converting Users into Leads and Customers</a></li>
                
              
                
                  <li><a href="../../reports/strategic-design-faqs/index.php">Strategic Design for Frequently Asked Questions</a></li>
                
              
                
                  <li><a href="../../reports/enterprise-mobile-showcase/index.php">Mobile Intranets and Enterprise Apps</a></li>
                
              
            </ul>
          </div>
          
        

      </div>
      <div class="medium-6 columns">
        
          
          <div>
            <h3>UX Conference Training Courses</h3>
             <ul class="no-bullet">
    
        <li><a href="../../courses/ux-basic-training/index.php">UX Basic Training</a></li>
    
        <li><a href="../../courses/human-mind/index.php">The Human Mind and Usability</a></li>
    
        <li><a href="../../courses/cross-channel-user-experience/index.php">Omnichannel Journeys and Customer Experience</a></li>
    
        <li><a href="../../courses/emerging-patterns-web-design/index.php">Emerging Patterns for Web Design</a></li>
    
        <li><a href="../../courses/usability-mobile-websites-apps/index.php">Mobile User Experience</a></li>
    
</ul>
          </div>
          
        
        
          
          <div>
            <h3>Online Seminars</h3>
            <ul class="no-bullet">
            
                <li><a href="../../online-seminars/fundamental-flaw-flat-design/index.php">The Fundamental Flaw in Flat Design</a></li>
            
                <li><a href="../../online-seminars/millennials-online/index.php">Millennials Online</a></li>
            
                <li><a href="../../online-seminars/beyond-screens/index.php">Beyond Screens</a></li>
            
                <li><a href="../../online-seminars/every-word-count/index.php">Making Every Word Count</a></li>
            
                <li><a href="../../online-seminars/university-websites/index.php">University Website UX Essentials</a></li>
            
            </ul>
          </div>
          
        
      </div>
    </div>
  </footer>
</article>

        </div>

      

    
  </div>
  <div class="row">
    
      <div class="column small-12 show-for-small-down l-navrail announcements">
        
          
        
      </div>
    
    
  </div>

    </div>
    




<footer id="page-footer">
    <div class="row hide-for-print">
        <div class="column small-12 medium-6">
            <section class="footer-popular">
                
                <h1>Popular Topics</h1>
                <div style="float: left;  width: 15em; display: block;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/applications/index.php">Application Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/e-commerce/index.php">E-Commerce Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/eyetracking/index.php">Eyetracking </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/information-architecture/index.php">Information Architecture </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/intranets/index.php">Intranet Design </a></li>
	<li><a href="../../topic/interaction-design/index.php">Interaction Design </a></li>
</ul>
</div>

<div style="float: left;  width: 15em; display: block; list-style: none;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/mobile-and-tablet-design/index.php">Mobile and Tablet Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/credibility-persuasion/index.php">Persuasive Design</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/research-methods/index.php">Research Methods</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/user-testing/index.php">User Testing </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/web-usability/index.php">Web Usability </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/writing-web/index.php">Web Writing &amp; Content Strategy </a></li>
</ul>
</div>

<p>&nbsp;</p>

                
            </section>
        </div>

        <div class="column small-12 medium-6">
            <div class="row">
                <div class="column small-6">
                    <section class="footer-about">
                        <h1>Follow Us</h1>
                        <ul>
	<li><a href="../subscribe/index.php">Subscribe to our weekly UX newsletter</a>,&nbsp;Jakob Nielsen&#39;s Alertbox</li>
	<li>Visit Don Norman&#39;s website, <a href="http://www.jnd.org/"> www.jnd.org </a></li>
	<li>Visit Bruce Tognazzini&#39;s website, <a href="http://www.asktog.com/"> www.asktog.com </a></li>
	<li style="padding-top: 6px;"><a class="twitter-follow-button" data-show-count="false" data-size="large" href="https://twitter.com/nngroup"><img alt="Follow @nngroup on Twitter" height="20" src="https://media.nngroup.com/media/editor/2016/12/30/twitter-follow-button.png" style="width: 117px; height: 20px;" width="117" /></a></li>
	<li style="padding-top: 6px;"><img alt="" height="14" src="https://media.nngroup.com/media/editor/2016/07/26/feed-icon-14x14.png" style="vertical-align:text-top;width:14px;margin-right:.25em;" width="14" /><a href="../../feed/rss/index.php">Subscribe to our RSS feed</a></li>
</ul>

                    </section>
                </div>

                <div class="column small-6">
                    <section class="footer-about right-column">
                        <h1>About
</h1>
                        <ul class="no-bullet">
	<li><a href="../../about/contact/index.php">Contact information</a></li>
	<li><a href="../../about/index.php">About Us</a></li>
	<li><a href="../../about/why-nng/index.php">Why NN/g? </a></li>
	<li><a href="../../return-policy/index.php">Return Policy</a></li>
</ul>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="copyright column small-12">
            <p><a href="../../copyright-and-reprint-info/index.php">Copyright</a> &copy; 1998-2017 Nielsen Norman Group, All Rights Reserved.</p>
        </div>
    </div>
</footer>


    


        <script src="https://media.nngroup.com/static/vendor/fastclick/js/fastclick.min.js"></script>
        <script src="https://media.nngroup.com/static/vendor/foundation/js/foundation.min.js"></script>
        <script src="http://media.nngroup.com/static/js/app.js"></script>
        <script src="http://media.nngroup.com/static/js/scroll-to-fixed.js"></script><script>var externalLinks = document.querySelectorAll('a[href^="http"]');for (var i = externalLinks.length-1; i >= 0; i--) {externalLinks[i].addEventListener("click", function(event) { alert("External links are disabled for this test."); event.preventDefault(); }, false);}var mailLinks = document.querySelectorAll('a[href^="mailto"]');for (var i = mailLinks.length-1; i >= 0; i--) {mailLinks[i].addEventListener("click", function(event) { alert("Email links are disabled for this test."); event.preventDefault(); }, false);}var externalForms = document.querySelectorAll('form');for (var i = externalForms.length-1; i >= 0; i--) {externalForms[i].addEventListener("submit", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false); externalForms[i].addEventListener("click", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false);}</script>

        <script>
            
            $(function() {
                $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
                $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
            });
        </script>

        
<script src="https://media.nngroup.com/static/js/articles.js"></script>


        
        <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1072699960;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072699960/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
    </body>

<!-- Mirrored from www.nngroup.com/articles/wechat-integrated-ux/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:07:59 GMT -->
</html>
