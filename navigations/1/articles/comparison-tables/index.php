<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->





    
<!-- Mirrored from www.nngroup.com/articles/comparison-tables/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 14:55:00 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1026.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"licenseKey":"1a30ef9931","queueTime":0,"beacon":"bam.nr-data.net","errorBeacon":"bam.nr-data.net","applicationID":"2048992","transactionName":"NFxSMRBUX0EDUkIPDA0WdhAMVkVbDV8ZBxEXUFMJB0YfRAtUQRVZIktEDAFZVHYHRVcPD01eVRE=","applicationTime":515,"agent":""}</script>
        <title>Comparison Tables for Products, Services, and Features</title><meta property="og:title" content="Comparison Tables for Products, Services, and Features" />
  
        
        <meta name="description" content="Use this versatile GUI tool to support users when they need to make a decision that involves considering multiple attributes of a small number of items.">
        <meta property="og:description" content="Use this versatile GUI tool to support users when they need to make a decision that involves considering multiple attributes of a small number of items." />
        
  
        
	
        
        <meta name="keywords" content="comparison table, product comparison table, decision making, noncompensatory decision making">
        

        

        <meta name="robots" content="noarchive">
        <meta name="viewport" content="width=device-width">

        <link rel="search" type="application/opensearchdescription+xml" title="NN/g Search" href="../../opensearch.xml" />
        <link rel="shortcut icon" href="http://media.nngroup.com/static/img/favicon.ico" />

        <link rel="stylesheet" href="https://media.nngroup.com/static/vendor/font-awesome/css/font-awesome.min.css">

        
        <link rel="stylesheet" href="https://media.nngroup.com/static/css/7232a960f1df.css" type="text/css" />
        <link rel="stylesheet" href="http://fast.fonts.com/t/1.css?apiType=css&amp;projectid=c5846260-9def-46ec-9407-0f1349bd2d64" type="text/css">

        <script src="https://media.nngroup.com/static/vendor/modernizr/js/modernizr.min.js"></script>

        <script type="text/javascript" src="https://media.nngroup.com/static/js/3283be5def74.js"></script>
        

        

        
        
        <!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=229034,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('http://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->


<!-- Google Analytics Tracking Code for http://nngroup.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23132681-1', 'nngroup.com');
  ga('require', 'linkid', 'https://www.nngroup.com/articles/comparison-tables/linkid.js');
  ga('require', 'displayfeatures');

// Optimizely Universal Analytics Integration
 window.optimizely = window.optimizely || [];
 window.optimizely.push("activateUniversalAnalytics");

  ga('send', 'pageview');

</script>
<script>
/**
* Function to track a click on an outbound link in Google Analytics.
*/
function trackOutboundLink(label, url) {
    if(window.ga && ga.loaded) {
        ga('send', 'event', 'outbound', 'click', label, {'hitCallback': function(){document.location = url;}
        });
    } else {
        document.location = url;
    }
}
/**
* Function to track a click on a link in the article announcement area.
*/
$('#articleAnnouncement a').on('click', function(){
  ga('send', 'event', 'link', 'click', 'articleAnnouncement');
});
</script>

<script type="text/javascript">
 setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2421.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



<!-- Hotjar Tracking Code for http://nngroup.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:121605,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

        <script src="https://media.nngroup.com/static/js/analytics/track_outbound_link.js"></script>
        
        
        
    </head>
    <body class="location-articles articles-detail">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
    <header id="page-header">
        <div id="cart" class="shopping-cart mobile-cart show-for-small-down"></div>
        <div class="row">
            <div id="masthead" class="masthead small-12 medium-7 column">

                <!-- The button to toggles menu on small screen -->

                <h1><a href="../../index.php"><img src="http://media.nngroup.com/static/img/logo-wordmark.png" width="74" height="30"> Nielsen <span class="norman">Norman</span> Group</a></h1>
                <p class="tagline show-for-medium-up">Evidence-Based User Experience Research, Training, and Consulting</p>

                <!-- The button to toggles form search on small screen -->


                <div style="clear: right;width:  100%;height: 1px;"></div><?php define('ROOT_PATH',$_SERVER["DOCUMENT_ROOT"] . '/apps/navigation_styles/navigations/1'); ?><?php include(ROOT_PATH)."/navigation.php"; ?>

                <!-- Drop down menu on small screen -->
                <div id="hideNav" class="show-for-small-down">
                  
                  <ul class="menu-content">
                    <li><a href="../../index.php">Home</a></li>
                    <li class="has-submenu">
                      <a>Training</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../training/index.php">Overview</a></li>
                        
<li id="nav-ux-conference"><a href="../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../online-seminars/index.php"><span>Online Seminars</span></a></li>

                      </ul>
                    </li>
                    <li class="has-submenu">
                      <a>Consulting</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
  <li><a href="../../consulting/index.php">Overview</a></li>
  
  <li><a href="../../consulting/benchmarking/index.php">Benchmarking</a></li>
  
  <li><a href="../../consulting/expert-design-review/index.php">Expert Design Review</a></li>
  
  <li><a href="../../consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
  
  <li><a href="../../consulting/usability-testing/index.php">Usability Testing </a></li>
  
  <li><a href="../../consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
  
  <li><a href="../../in-house-training/index.php">In-House Training</a></li>
</ul>

                    </li>

                    <li><a href="../../reports/index.php">Reports</a></li>
                    <li><a href="../index.php">Articles</a></li>
                    <li class="has-submenu"><a>About NN/G</a><i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../about/index.php">Overview</a></li>
                        <li><a href="../../people/index.php">People</a></li>
                        <li><a href="../../about/why-nng/index.php">Why NN/g?</a></li>
                        <li><a href="../../about/contact/index.php">Contact</a></li>
                        <li><a href="../../news/index.php">News</a></li>
                        <li><a href="../../about/history/index.php">History</a></li>
                        <li><a href="../../books/index.php">Books</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                </div>

                <hr class="header-divider show-for-small-only"/>


            </div>
       </div>

        <div id="hideNav">            <nav class="primary-nav small-12 column">
                <ul>
                    <li><a class="navitem" id="nav-home" href="../../index.php">Home</a></li>
                    <li><a class="navitem" id="nav-training" href="../../training/index.php">Training</a></li>
                    <li><a class="navitem" id="nav-consulting" href="../../consulting/index.php">Consulting</a></li>
                    <li><a class="navitem" id="nav-reports" href="../../reports/index.php">Reports</a></li>
                    <li><a class="navitem" id="nav-articles" href="../index.php">Articles</a></li>
                    <li><a class="navitem" id="nav-about" href="../../about/index.php">About NN/G</a></li>
                </ul>
            </nav>
            <nav class="secondary-nav small-12 medium-12 large-12 columns">
                
                
            </nav>
        </div>
    </header>
    <div class="l-content">
    
  

  

  <div class="row">
    
      
      <section class="small-12 medium-3 columns collapsable-content l-navrail articles-left-column">
        <h1 class="show-for-small-down collapse-link mobile-browse-header">Browse by Topic and Author</h1>
          <div class="row collapsable">

            
            <div class="small-6 medium-12 columns">
              <section id="article-popular-topics" style="display: none;">
                
                <h1>Topics</h1>
                <ul id="article-popular-topics">
                    
                    <li>
                        
                        <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/intranets/all/index.php">Intranets</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/user-testing/all/index.php">User Testing</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                        
                    </li>
                    
                    <li><a href="#" id="article-see-all-topics"><strong>See all topics&hellip;</strong></a></li>
                </ul>
                
              </section>
              <section id="article-all-topics" style="display: none;">
                
                <h1>
                  <span class="show-for-medium-up">All Article Topics</span>
                  <span class="show-for-small-down">All Topics</span>
                  (<a href="#" id="article-hide-all-topics">hide</a>)
                </h1>
                <ul>
                  
                  <li>
                    
                    <a href="../../topic/accessibility/all/index.php">Accessibility</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/agile/all/index.php">Agile</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/analytics-and-metrics/all/index.php">Analytics &amp; Metrics</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/applications/all/index.php">Application Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/b2b-websites/all/index.php">B2B Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/behavior-patterns/all/index.php">Behavior Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/branding/all/index.php">Branding</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/content-strategy/all/index.php">Content Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/corporate-websites/all/index.php">Corporate Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/design-patterns/all/index.php">Design Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-design-process/all/index.php">Design Process</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/email/all/index.php">Email</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/eyetracking/all/index.php">Eyetracking</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/heuristic-evaluation/all/index.php">Heuristic Evaluation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/human-computer-interaction/all/index.php">Human Computer Interaction</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-ideation/all/index.php">Ideation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/information-architecture/all/index.php">Information Architecture</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/interaction-design/all/index.php">Interaction Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/international-users/all/index.php">International Users</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/intranets/all/index.php">Intranets</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-management/all/index.php">Management</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/navigation/all/index.php">Navigation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/non-profit-websites/all/index.php">Non-Profit Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/personas/all/index.php">Personas</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/persuasive-design/all/index.php">Persuasive Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/prototyping/all/index.php">Prototyping</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/psychology-and-ux/all/index.php">Psychology and UX</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/research-methods/all/index.php">Research Methods</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/search/all/index.php">Search</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/social-media/all/index.php">Social Media</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/strategy/all/index.php">Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/user-testing/all/index.php">User Testing</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-teams/all/index.php">UX Teams</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/visual-design/all/index.php">Visual Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/writing-web/all/index.php">Writing for the Web</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/young-users/all/index.php">Young Users</a>
                    
                  </li>
                  
                </ul>
                
              </section>
            </div>
            <script>
              /* Determine which topics list will be displayed: the full or the
               * partial.  We are inlining this close to the list itself to
               * prevent rendering delays on the client
               */
              var topic_list = $.cookie("article-topics");

              var popular_topics = $('#article-popular-topics');
              var all_topics = $('#article-all-topics');

              if(topic_list == 1) {
                  popular_topics.hide();
                  all_topics.show();
              } else {
                  popular_topics.show();
                  all_topics.hide();
              }
            </script>
            

            
            <div class="small-6 medium-12 columns">
              
              <section id="article-initial-authors">
                  <h1>Author</h1>
                  <ul>
                    
                    <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                    
                    <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                    
                    <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                    
                    <li><a href="#" onClick="$('#article-initial-authors').hide(); $('#article-all-authors').show();"><strong>See all authors&hellip;</strong></a></li>
                  </ul>
              </section>
              <section id="article-all-authors" style="display: none;">
                <h1>
                  All Authors
                  (<a href="#" id="article-hide-all-authors">hide</a>)
                </h1>
                <ul>
                  
                  <li><a href="../author/raluca-budiu/index.php">Raluca Budiu</a></li>
                  
                  <li><a href="../author/patty-caya/index.php">Patty Caya</a></li>
                  
                  <li><a href="../author/susan-farrell/index.php">Susan Farrell</a></li>
                  
                  <li><a href="../author/therese-fessenden/index.php">Therese Fessenden</a></li>
                  
                  <li><a href="../author/kim-flaherty/index.php">Kim Flaherty</a></li>
                  
                  <li><a href="../author/sarah-gibbons/index.php">Sarah Gibbons</a></li>
                  
                  <li><a href="../author/aurora-harley/index.php">Aurora Harley</a></li>
                  
                  <li><a href="../author/kate-kaplan/index.php">Kate Kaplan</a></li>
                  
                  <li><a href="../author/page-laubheimer/index.php">Page Laubheimer</a></li>
                  
                  <li><a href="../author/angela-li/index.php">Angie Li</a></li>
                  
                  <li><a href="../author/hoa-loranger/index.php">Hoa Loranger</a></li>
                  
                  <li><a href="../author/kate-meyer/index.php">Kate Meyer</a></li>
                  
                  <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                  
                  <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                  
                  <li><a href="../author/kara-pernice/index.php">Kara Pernice</a></li>
                  
                  <li><a href="../author/christian-rohrer/index.php">Christian Rohrer</a></li>
                  
                  <li><a href="../author/amy-schade/index.php">Amy Schade</a></li>
                  
                  <li><a href="../author/katie-sherwin/index.php">Katie Sherwin</a></li>
                  
                  <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                  
                  <li><a href="../author/kathryn-whitenton/index.php">Kathryn Whitenton</a></li>
                  
                </ul>
              </section>
              
            </div>
            
          </div>

          
          <section class="hide-for-small-down">
            
              
              <h1>Recent Articles</h1>
              
              <ul>
                  
                  <li><a href="../seamless-cross-channel/index.php">Seamlessness in the Omnichannel User Experience</a></li>
                  
                  <li><a href="../brand-intention-interpretation/index.php">Experience Design: Bridging Brand Intention and Brand Interpretation</a></li>
                  
                  <li><a href="../eyetracking-tasks-efficient-scanning/index.php">Scanning Patterns on the Web Are Optimized for the Current Task</a></li>
                  
                  <li><a href="../flat-design-best-practices/index.php">Flat-Design Best Practices</a></li>
                  
                  <li><a href="../ux-user-stories/index.php">Accounting for UX Work with User Stories in Agile Projects</a></li>
                  
                  <li><a href="../index.php"><strong>See all articles&hellip;</strong></a></li>
              </ul>
              
            
          </section>
          

          
          
          <section class="hide-for-small-down">
            <h1>Popular Articles</h1>
            
            <ul>
                
                <li><a href="../usability-101-introduction-to-usability/index.php">Usability 101: Introduction to Usability</a></li>
                
                <li><a href="../top-10-mistakes-web-design/index.php">Top 10 Mistakes in Web Design</a></li>
                
                <li><a href="../how-users-read-on-the-web/index.php">How Users Read on the Web</a></li>
                
                <li><a href="../f-shaped-pattern-reading-web-content/index.php">F-Shaped Pattern For Reading Web Content</a></li>
                
                <li><a href="../ten-usability-heuristics/index.php">10 Usability Heuristics for User Interface Design</a></li>
                
                <li><a href="../intranet-design-2016/index.php">10 Best Intranets of 2016</a></li>
                
                <li><a href="../which-ux-research-methods/index.php">When to Use Which User-Experience Research Methods</a></li>
                
                <li><a href="../response-times-3-important-limits/index.php">Response Times: The 3 Important Limits</a></li>
                
                <li><a href="../why-you-only-need-to-test-with-5-users/index.php">Why You Only Need to Test with 5 Users</a></li>
                
                <li><a href="../page-fold-manifesto/index.php">The Fold Manifesto: Why the Page Fold Still Matters</a></li>
                
            </ul>
          </section>
          
          

          
            <section class="hide-for-small-down">
              
            </section>
          

      </section>
      

      
        <div class="small-12 medium-9 columns content-body">
          




<article class="l-subsection alertbox-article-content article-detail-bl">
  
  <header>
    <div class="l-subsection">
      <h1>Comparison Tables for Products, Services, and Features</h1>
      <div class="author-meta">by
        
          
            
            <a href="../author/kate-meyer/index.php">Kate Meyer</a>
            
          
        on  March 5, 2017
        
      </div>

      

      
      <div class="topic-links">
        Topics:
        
          <ul>

  <li><a href="../../topic/psychology-and-ux/index.php">Psychology and UX</a></li>

  <li><a href="../../topic/e-commerce/index.php">E-commerce</a></li>

  <li><a href="../../topic/web-usability/index.php">Web Usability</a></li>

</ul>
        
      </div>
      
    </div>
  </header>
  

  
  
  <section>
    <p class="lede"><strong>Summary:</strong> Use this versatile GUI tool to support users when they need to make a decision that involves considering multiple attributes of a small number of offerings. Consistency in content, scannability, and a simple layout are some of the most important qualities of successful comparison tables.</p>
    <br />
  </section>
  
  

  
  <section id="article-body"><p>Comparison is <a href="../the-3cs-of-critical-web-use-collect-compare-choose/index.php">one of the most critical activities</a> users perform on the web. In many cases, it’s a necessary step before your site visitors will perform a desired action, like buying your product, signing up for membership, contacting you, or requesting a quote.</p>

<p>The first step to enabling comparison is providing consistent information for all comparable products or services. However, when that information is distributed across detail pages, the <a href="../interaction-cost-definition/index.php">interaction cost</a> and the <a href="../minimize-cognitive-load/index.php">cognitive load</a> both increase: users are forced to remember information, take notes, flip between tabs, or open multiple browser windows.</p>

<p>This is where the comparison table comes in — a well-known, yet often neglected or misused pattern. The basic comparison table uses columns for the products or services, and rows for the attributes. It allows for quick and easy comparison between each offering’s features and characteristics.</p>

<div style="text-align:center">
<figure class="caption" style="display:inline-block"><img alt="" height="717" src="https://media.nngroup.com/media/editor/2017/02/24/keurig.png" width="700"/>
<figcaption><em>keurig.com: A typical product-comparison table</em></figcaption>
</figure>
</div>

<h2>When You Need a Comparison Table</h2>

<p>Comparison tables are often misunderstood as tools for ecommerce only. It’s true that the most common instances of comparison tables are for mid-range to expensive consumer goods, especially electronics (think microwaves, fitness trackers, vacuum cleaners, or cars). But <strong>comparison tables are equally well-suited to services, membership levels, pricing packages, software features, </strong><a href="../explicit-differences/index.php"><strong>tuition rates</strong></a>, <strong>or locations</strong>. They can be used to <strong>compare similar items from the same organization</strong>, or to <strong>compare one organization’s products against those of a competitor</strong>. The comparison table is a much more versatile tool than it gets credit for.</p>

<p>To understand when you should use a comparison table, you have to first consider how people make decisions.</p>

<p>When people have to choose among many alternatives, it’s hard to compare the pros and cons of each individual alternative, and as a result they engage in <strong>noncompensatory decision making. </strong>To narrow down the number of alternatives to a manageable one, they usually use one hard criterion that outweighs any other considerations. For example, a user looking to buy a new car might filter out all the cars that are more expensive than that $20,000, even though some of them may be surpassing the budget by a very small amount. This nonnegociable filter helps the user restrict the set of results to a reasonable size.</p>

<p>When people have to select among a small set of alternatives (usually under 5–7), they usually engage in <strong>compensatory</strong> <strong>decision</strong> <strong>making</strong>: they look at the individual merits of each and compare their advantages and disadvantages according to a number of criteria. People might accept a negative attribute as a tradeoff for a positive one. For example, a user researching a new laptop might be willing to consider a heavier computer if it has better battery life and computing power.</p>

<p><a href="../filters-vs-facets/index.php">Filters and facets</a> support noncompensatory decision making. In contrast, compensatory decision making is best served by comparison tables. They allow users to easily see and compare multiple important attributes at a glance.</p>

<p>There are certainly cases when a comparison table is unnecessary. In addition to the noncompensatory decision situation discussed above, you probably don’t need a comparison table if:</p>

<ul>
	<li>Similar items are not necessarily mutually exclusive. For example, unlike a microwave or a laptop, consumers are unlikely to purchase only one shirt, so Zara doesn’t need to put together a comparison table to let users see several shirts side by side.</li>
	<li>The product or service is simple, and users wouldn’t be interested in analyzing the characteristics. For example, a coffee mug doesn’t have many meaningful, comparable attributes. Someone shopping for a coffee mug online might be interested in the style (and possibly size) but probably won’t want to compare many attributes of several coffee mugs.</li>
	<li>The product or service is cheap or easily replaceable (e.g., a pen, paper towels) and users are likely to engage in <a href="../satisficing/index.php">satisficing</a> behavior and not spend much time analyzing alternatives.</li>
	<li>The product or service is unique and hard to compare with others. For example, it’d be hard to come up with a set of attributes to compare works of art.</li>
</ul>

<p>Outside those exceptions, a comparison table’s applications are limited only by your imagination. Pretty much any time your users want to choose among similar offerings, especially if multiple factors contribute to the decision, it’s an opportunity for a comparison table.</p>

<h2>Types of Comparison Tables</h2>

<p>Some comparison tables are <strong>static</strong> — they contain preselected products; others are <strong>dynamic</strong> and allow users to decide which items they want to compare. The type you need will largely depend on how big your product universe is: static tables are used for small (under 5 items) product universes and dynamic tables are appropriate when your product universe contains many items.</p>

<h3>Static</h3>

<p>If you have a small number of products your users will need to compare, you may want to create prebuilt, <strong>static comparison tables</strong>. For example, Apple currently only sells 5 different models of the Apple Watch, so it provides a ready-made comparison table for users trying to decide.</p>

<div style="text-align:center">
<figure class="caption" style="display:inline-block"><img alt="" height="714" src="https://media.nngroup.com/media/editor/2017/02/24/apple-watches-compare-full-2.png" width="700"/>
<figcaption><em>apple.com: With only five models of the Apple Watch available, Apple can provide a comparison table that includes them all.</em></figcaption>
</figure>
</div>

<p>A good rule of thumb is to take this approach if you have 5 offerings or fewer, but we’ll get into limiting the number of compared items later in this article. Static comparison tables usually work well for membership levels or pricing packages.</p>

<p>With this kind of comparison table implementation, you’ll be able to control how the information is displayed on the page. For example, you can change the copy to fit the space constraints of one cell. Note, however, that this implementation is not as easily scalable as dynamic comparison tables: You’ll have to update the comparison table if you offer a new product or service.</p>

<div style="text-align:center">
<figure class="caption" style="display:inline-block"><img alt="" height="238" src="https://media.nngroup.com/media/editor/2017/02/24/apple-watches-nav.png" width="700"/>
<figcaption><em>apple.com: Ensure users can find a static comparison table by including a link to the table in your information architecture alongside your product menu. Apple includes a link to its static comparison table in the Watch secondary navigation menu, alongside the links to the various Apple Watch models.</em></figcaption>
</figure>
</div>

<h3>Dynamic</h3>

<p>Dynamic tables allow users to select which items they want to see in the comparison table. They are appropriate for situations where your product universe is larger than 5 items.</p>

<p>While dynamic comparison tables scale well as your set of offerings increases or changes, they are usually implemented using a <a href="../layout-vs-content/index.php">flexible layout</a> and their appearance cannot be as closely controlled as that of static tables.</p>

<p><strong>Selection of items in a dynamic comparison table.</strong> Two popular ways of allowing users to select which items to add to the table include:</p>

<ol>
	<li><strong><em>Compare </em>buttons or checkboxes</strong> directly on the listing pages. With this method, users can select the items they’re interested in, and then move to a dynamically created comparison page.</li>
</ol>

<div style="text-align:center">
<figure class="caption" style="display:inline-block"><img alt="" height="258" src="https://media.nngroup.com/media/editor/2017/02/24/homedepot-lowes-2.png" width="700"/>
<figcaption><em>Checkmarks (left, homedepot.com) and buttons (right, lowes.com) are the two most common ways of allowing users to select items for an interactive comparison table.</em></figcaption>
</figure>
</div>

<p style="margin-left:.5in;">Product listing pages tend to be overcrowded, so one major challenge for this implementation is making the <em>Compare </em>buttons discoverable without giving them too much space on the page. The safest option is to include <em>Compare</em> buttons as secondary action buttons, either at the top and bottom of the page, or beneath all product descriptions.</p>

<div style="text-align:center">
<figure class="caption" style="display:inline-block"><img alt="" height="398" src="https://media.nngroup.com/media/editor/2017/02/24/delta-faucets-onhover-2.png" width="700"/>
<figcaption><em>delta.com: Delta’s interactive comparison tool is only revealed when users hover over products. Don’t hide the comparison tool behind hover effects.</em></figcaption>
</figure>
</div>

<ol>
	<li value="2"><strong>Direct manipulation of the items of interest</strong>. With this method, users can click or drag the products that they want to add to the comparison table. Usually this action is possible either because, like in Garmin’s example below, a special <em>Compare </em>mode disables the regular meaning of clicking on a product (which normally takes users to the product-detail page) or because, like in Fitbit’s example, the site has a special dedicated <em>Compare </em>page.</li>
</ol>

<div style="text-align:center">
<figure class="caption" style="display:inline-block"><img alt="" height="368" src="https://media.nngroup.com/media/editor/2017/02/24/garmin-marineradar-select-2.png" width="700"/>
<figcaption><em>garmin.com: Garmin features a COMPARE button at the top of its product-listing page. When that button is pressed, clicking on a product adds it to the comparison table instead of leading to that product’s detail page. Unfortunately, this grey ghost button fades into the UI and is not easily noticeable.</em></figcaption>
</figure>
</div>

<div style="text-align:center">
<figure class="caption" style="display:inline-block"><img alt="" height="780" src="https://media.nngroup.com/media/editor/2017/02/24/fitbit-compare-2.png" width="700"/>
<figcaption><em>fitbit.com: FitBit offers 7 models of fitness trackers. A dedicated comparison page lets users choose the products to compare. Note that this solution is impractical when the product universe is big, and may be unnecessary if you already have listing pages.</em></figcaption>
</figure>
</div>

<p>One advantage of this direct-manipulation approach to selection is the opportunity to separate the comparison activity from the product-listing page, keeping that view visually cleaner.</p>

<p>A separate comparison page (like Fitbit’s) is a good option when you don’t have enough products or offerings to justify a traditional listing page in the first place.</p>

<h2>Best Practices for Comparison Tables</h2>

<p>Regardless of which type of comparison table you choose, follow these <strong>best practices</strong> to support your users’ decision making.</p>

<h3>Use Comparison Tables for Up to 5 Items</h3>

<p>Comparison tables support compensatory decision making, in which people engage only when they have relatively few alternatives to consider. When more than 5 items need to be compared, add other mechanisms such as filters to help users narrow down the larger set of possibilities to 5 or fewer.</p>

<p>For static comparison tables, err on the side of simplicity. If you can’t keep a static table down to at most 5 comparable options, reconsider if it should be a static comparison table at all. Depending on the complexity of the information you’re presenting, even 5 options may be too many.</p>

<p>For dynamic tables, an extra consideration is whether the layout will scale gracefully up to 5 items if users have the freedom to select what those 5 items will be. Most dynamic comparison tables accept 3–4 items only. Consider how much text will need to be included for the attributes, and how that will impact layout and readability.</p>

<p>Also consider the size of the user’s device or browser. You may need to reduce the number of items to two for presentation on mobile. On the other hand, don’t force users to compare only two items at a time if you have the space to show more.</p>

<div style="text-align:center">
<figure class="caption" style="display:inline-block"><img alt="" height="356" src="https://media.nngroup.com/media/editor/2017/02/24/apple-mac-compare-big-2.png" width="700"/>
<figcaption><em>Apple’s dynamic comparison table for its Mac computers unnecessarily restricts users to comparing two items, even at very large screen sizes (shown here on a 3008x1692 resolution monitor.)</em></figcaption>
</figure>
</div>

<p>Whatever your limit, make sure you clearly communicate it to your users to avoid confusion and errors. And don’t forget to let users remove items from the comparison, as they narrow down their selections.</p>

<h3>Be Consistent</h3>

<p>The biggest problem with most comparison tables isn’t a design problem, it’s a content problem. When attribute information is missing, incomplete, or inconsistent across similar offerings, otherwise handy comparison tables quickly become useless. This is especially problematic for dynamic comparison tables, when you’re dealing with many offerings with slightly different metadata available.</p>

<div style="text-align:center">
<figure class="caption" style="display:inline-block"><img alt="" height="336" src="https://media.nngroup.com/media/editor/2017/02/24/bestbuy-cameras-productlisting-comparison-missing_info-2.png" width="700"/>
<figcaption><em>bestbuy.com: This comparison table displays the attributes for 4 different mirrorless cameras sold by Best Buy (two Sony cameras, one Canon camera, and one Panasonic camera). The metadata for the Panasonic camera is much richer than for the others. It can be challenging to keep the metadata consistent and up to date, especially when companies sell many products from many different manufacturers. However, if the information isn’t complete for all compared products, it might as well not be in a comparison table at all.</em></figcaption>
</figure>
</div>

<div style="text-align:center">
<figure class="caption" style="display:inline-block"><img alt="" height="283" src="https://media.nngroup.com/media/editor/2017/02/24/smugmug-plans-detailed-3.png" width="700"/>
<figcaption><em>smugmug.com: In this comparison table detailing the features of SmugMug’s four membership levels, each column has a different value for the attribute Ads and spam: Zip, Zero, Zilch, and Nada. Comparison-table values aren’t a good place to showcase your site’s personality — it’d be better to leave these values equivalent and not distract users while they scan for differences.</em></figcaption>
</figure>
</div>

<h3>Support Scannability</h3>

<p>Comparing cons and pros of different products is a cognitively demanding process. You want to make sure your users can focus on the essentials. An effective way to do this is to make the table easy to scan for differences, similarities, and keywords.</p>

<p>Stick to the standard table layout: options as columns, attributes as rows, with row labels on the left and column labels above. Use consistent text alignment in each column.</p>

<div style="text-align:center">
<figure class="caption" style="display:inline-block"><img alt="" height="651" src="https://media.nngroup.com/media/editor/2017/02/24/williams-sonoma-2.png" width="700"/>
<figcaption><em>williams-sonoma.com: Williams Sonoma does a good job categorizing pans for this static comparison table. Unfortunately, the horizontal row headers are nonstandard and impede scannability. The narrow paragraphs under DISTINGUISHING FEATURE are not scannable or easy to read.</em></figcaption>
</figure>
</div>

<p>When using text within comparison tables, keep it short. Whenever possible, avoid full sentences.</p>

<p>Color coding can help as well — either lightly shading the backgrounds of each column, or coloring the text of the cells. Just make sure you’re maintaining enough contrast and not sacrificing readability.</p>

<div style="text-align:center">
<figure class="caption" style="display:inline-block"><img alt="" height="780" src="https://media.nngroup.com/media/editor/2017/02/24/fitbit-compare-2.png" width="700"/>
<figcaption><em>FitBit.com uses a different color for each column’s checkmarks to help users clearly differentiate between each product’s features. This color-coding approach wouldn’t work quite as well for more complex attribute values that can’t be expressed in checkmarks.</em></figcaption>
</figure>
</div>

<div style="text-align:center">
<figure class="caption" style="display:inline-block"><img alt="" height="510" src="https://media.nngroup.com/media/editor/2017/02/24/techsmith-2.png" width="700"/>
<figcaption><em>techsmith.com uses color-coded columns to show the different features available in its software offerings, as well as which platforms they can be used on.</em></figcaption>
</figure>
</div>

<p>It’s also important to clearly indicate rows so users can easily tell which attribute a cell refers to, especially when using symbols like checkmarks that can’t stand alone. Row borders, row shading, or extra spacing can help keep the rows distinct and separate.</p>

<h3>Sticky Column Headers</h3>

<p>Especially when dealing with long lists of attributes that occupy several screenfuls, keep column headers fixed as users scroll. Human short-term memory is limited, and users will easily forget which column is for which product.</p>

<div style="text-align:center">
<figure class="caption" style="display:inline-block"><img alt="" height="386" src="https://media.nngroup.com/media/editor/2017/02/24/disney-passes-2.png" width="700"/>
<figcaption><em>disneyworld.disney.go.com: Disney offers a dynamic comparison table to help its customers compare different annual passes. Unfortunately, it doesn’t have fixed column headers, so users are forced to remember column names, or scroll back up the page to check which column is for “Disney Platinum Plus Pass” and which column is for “Disney Platinum Pass.”</em></figcaption>
</figure>
</div>

<h3>Meaningful Attributes</h3>

<p>Your comparison table should include attributes that your users will actually care about. Don’t just throw every piece of metadata you have into the table, because it will make the job harder for users.</p>

<p>As much as possible, try to define unfamiliar terms in context, and connect vague attributes to something concrete. An example of this is on an Amazon page for an Anker portable power bank.</p>

<div style="text-align:center">
<figure class="caption" style="display:inline-block"><img alt="" height="401" src="https://media.nngroup.com/media/editor/2017/02/24/amazon-powerbanks-2.png" width="700"/>
<figcaption><em>amazon.com: The manufacturer’s product-comparison table translates technical attributes into language that is understandable by the average consumer.</em></figcaption>
</figure>
</div>

<p>Anker’s comparison table has attributes that are useful in the context of comparing portable power banks: battery capacity in mAh, charging speed, and weight. What <em>really</em> makes this a good comparison table is the fact that it translates those attributes into things that would be meaningful for the average consumer. The battery capacity is “3350 mAh,” which is approximately 1.2 “iPhone 6 charges.” The size is “3.5 x 0.9 x 0.9 inches” — about the size of a tube of lipstick. The weight is “2.7 oz,” which is similar to an egg’s weight. The average consumer may have trouble imagining what 2.7 oz feels like, but can probably imagine the weight of an egg.</p>

<p>Another effective way to make obscure attributes meaningful is to include links to more information or in-context tooltips with definitions or clarifying information.</p>

<div style="text-align:center">
<figure class="caption" style="display:inline-block"><img alt="" height="448" src="https://media.nngroup.com/media/editor/2017/02/24/shopify-comparison-tooltip-2.png" width="700"/>
<figcaption><em>shopify.com: The hover-activated tooltip clarifies the meaning of the attributes without adding extra text to the page.</em></figcaption>
</figure>
</div>

<h3>Give Users Control</h3>

<p>Even if you do a good job making the entries scannable or the headers sticky, it can be hard for users to compare products with many attributes, especially when these attributes span several screenfuls. In those situations, users may have to scroll back and forth between different rows as they compare the pros and cons of different products.</p>

<p>In order to make the task manageable, consider allowing users to select which attributes they want displayed in the table. Collapsible rows are an easy implementation for this feature. Additionally, let users hide the rows for which all offerings are similar, and only show the differences.</p>

<div style="text-align:center">
<figure class="caption" style="display:inline-block"><img alt="" height="454" src="https://media.nngroup.com/media/editor/2017/02/24/samsung-compare-fixed-2.png" width="700"/>
<figcaption><em>samsung.com: Samsung offers substantial customization options in its dynamic product comparison table. Users can collapse or expand categories of attributes, and can select specific attributes to show only the rows they’re interested in. However, this table suffers from imperfectly aligned columns, which impede scannability. This implementation also goes too far into customization, and seems to be a victim of feature bloat. It’s unclear why a user would need to see Only Similarities when deciding among products.</em></figcaption>
</figure>
</div>

<div style="text-align:center">
<figure class="caption" style="display:inline-block"><img alt="" height="433" src="https://media.nngroup.com/media/editor/2017/02/24/crutchfield-2.png" width="700"/>
<figcaption><em>crutchfield.com: Crutchfield allows users to remove attributes that are the same for all compared options by clicking Hide similar features. If all three speakers have the same warranty length, that’s probably not a useful piece of information for deciding among them.</em></figcaption>
</figure>
</div>

<p>For simple static comparison tables, consider presenting a simplified table with those attributes you expect will be most important to users, but also allow access to a more detailed table.</p>

<div style="text-align:center">
<figure class="caption" style="display:inline-block"><img alt="" height="528" src="https://media.nngroup.com/media/editor/2017/02/24/smugmug-plans-2.png" width="700"/>
<figcaption><em>smugmug.com: For its 4 plan options, SmugMug presents a very simplified “at a glance” comparison table.</em></figcaption>
</figure>
</div>

<div style="text-align:center">
<figure class="caption" style="display:inline-block"><img alt="" height="814" src="https://media.nngroup.com/media/editor/2017/02/24/smugmug-plans-detailed-2.png" width="700"/>
<figcaption><em>smugmug.com: The much more detailed full comparison table is available on a different page.</em></figcaption>
</figure>
</div>

<h3>Simplify the Comparison for Mobile</h3>

<p>Some websites just remove their comparison functionality altogether for small devices. If at all possible, you should try to support some level of comparison on mobile, but it’s unlikely that you’ll be able to show more than 2 items at a time in a comparison table. Remember that on smaller screens, fewer rows will be visible at a time and more strain will be placed on the user’s short-term memory. Therefore, following the previous recommendations (especially making entries scannable and giving users control to choose what they want to display) will be even more important on mobile.</p>

<p>If a true comparison table on a mobile devices is impractical for your offerings, you might alternatively consider converting the table to tabs or lists for small screens. Just remember that these formats do not support compensatory decision making as well, because users have to remember the attributes for each products in order to weigh pros and cons.</p>

<div style="text-align:center">
<figure class="caption" style="display:inline-block"><img alt="" height="615" src="https://media.nngroup.com/media/editor/2017/02/24/img_2072-2.png" width="350"/>
<figcaption><em>shopify.com: One strategy for making comparison tables work on mobile is converting the columns to tabs. This means users can’t do true side-by-side comparison like they can on a full table, but it at least they will be able to switch among products more easily than if they had to scroll or switch mobile-browser tabs.</em></figcaption>
</figure>
</div>

<h2>The Golden Rule of Comparison Tables</h2>

<p>To sum up many of the guidelines we’ve listed here: <strong>Above all else, do the work for the consumer.</strong> Don’t slow them down with nonstandard or overly long tables with repeated information; don’t ask them to hold things in their memory; and don’t make them Google terms they aren’t familiar with.</p>

<p>Think of a comparison table as a tool to help each user find the option that suits them, rather than a way to upsell them. It may be tempting to manipulate users toward buying the most expensive option, but an honest presentation will be more profitable for your company in the long term.</p>

<p>Helping users determine the solution that’s best for their needs helps you in several ways:</p>

<ul>
	<li>Don’t try to manipulate your users, and you’ll avoid losing their trust. Many users will detect manipulation which will drastically reduces the <a href="../trustworthy-design/index.php">credibility</a> (and thus <a href="../../courses/credibility-and-persuasive-web-design/index.php">persuasiveness</a>) of your site.</li>
	<li>When people buy what’s best for them, they’re more likely to be satisfied customers, and promote your brand to others (also improving your <a href="../nps-ux/index.php">NPS</a>).</li>
	<li>Beyond valuing the next click, you should value the next year: the more somebody values their current interaction with your website, the more likely they are to turn into <a href="../usability-roi-declining-but-still-strong/index.php">loyal users who’ll return</a>.</li>
</ul>

<p><strong>Make your users’ decisions as easy as possible</strong>: you’ll be happy, and they’ll be happy.</p>

<p> </p>

<p>For more tips on supporting your users’ cognitive processes, like decision-making, check out our full-day training seminar on <a href="../../courses/human-mind/index.php">The Human Mind and Usability.</a></p>
</section>
  

  
  <section>
    
        
    
  </section>
  
  
  
  
  

  


  <section>
    <br />
    <p class="share"><b>Share <span class="hide-for-small-down">this article</span>:</b> <a href="https://twitter.com/intent/tweet?url=http://www.nngroup.com/articles/comparison-tables/&amp;text=Comparison%20Tables%20for%20Products,%20Services,%20and%20Features&amp;via=nngroup">Twitter</a> | <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=http://www.nngroup.com/articles/comparison-tables/&amp;title=Comparison%20Tables%20for%20Products,%20Services,%20and%20Features&amp;source=Nielsen%20Norman%20Group">LinkedIn</a> | <a href="https://plus.google.com/share?url=http://www.nngroup.com/articles/comparison-tables/">Google+</a> | <a href="mailto:?subject=NN/g Article: Comparison Tables for Products, Services, and Features&amp;body=http://www.nngroup.com/articles/comparison-tables/">Email</a></p>
    <hr />
  </section>

  <footer class="l-subsection">
    
    
    <div class="learn-more">
      <h2>Learn More</h2>
      <p><a href="../subscribe/index.php">Subscribe to the weekly newsletter</a> to get notified about future articles.</p>
      </div>
    
    

    <div class="row show-for-small-down related-content">
      
      
      

      
      
        <div class="column small-12 topic-links">All articles about:
          <ul>
            
            <li><a href="../../topic/comparison/index.php">comparison</a></li>
            
            <li><a href="../../topic/e-commerce/index.php">E-commerce</a></li>
            
            <li><a href="../../topic/psychology-and-ux/index.php">Psychology and UX</a></li>
            
            <li><a href="../../topic/ui-elements/index.php">ui elements</a></li>
            
            <li><a href="../../topic/web-usability/index.php">Web Usability</a></li>
            
          </ul>
        </div>
      
      

      
      
        <div class="column small-12">
          <h3>UX Conference Training Courses</h3>
           <ul class="no-bullet">
    
        <li><a href="../../courses/ux-basic-training/index.php">UX Basic Training</a></li>
    
        <li><a href="../../courses/human-mind/index.php">The Human Mind and Usability</a></li>
    
        <li><a href="../../courses/hci/index.php">User Interface Principles Every Designer Must Know</a></li>
    
        <li><a href="../../courses/cross-channel-user-experience/index.php">Omnichannel Journeys and Customer Experience</a></li>
    
        <li><a href="../../courses/emerging-patterns-web-design/index.php">Emerging Patterns for Web Design</a></li>
    
</ul>
        </div>
      
      

      
      
        <div class="small-12 column">
          <h3>Research Reports</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../reports/ecommerce-user-experience/index.php">E-Commerce User Experience</a></li>
              
            
              
                <li><a href="../../reports/b2b-websites-usability/index.php">B2B Website Usability for Converting Users into Leads and Customers</a></li>
              
            
              
                <li><a href="../../reports/strategic-design-faqs/index.php">Strategic Design for Frequently Asked Questions</a></li>
              
            
              
                <li><a href="../../reports/celebrating-holidays-and-current-events-web/index.php">Celebrating Holidays and Current Events on the Web</a></li>
              
            
          </ul>
        </div>
      
      

      
      
        <div class="small-12 column">
          <h3>Online Seminars</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../online-seminars/fundamental-flaw-flat-design/index.php">The Fundamental Flaw in Flat Design</a></li>
              
            
              
                <li><a href="../../online-seminars/millennials-online/index.php">Millennials Online</a></li>
              
            
              
                <li><a href="../../online-seminars/university-websites/index.php">University Website UX Essentials</a></li>
              
            
          </ul>
        </div>
      
      
    </div>

    <div class="hide-for-small-down row related-content">
      <div class="medium-6 columns">
        
        
          <div>
            <h3>Articles</h3>
            <ul class="no-bullet">
              
                
                <li><a href="../computer-skill-levels/index.php">The Distribution of Users’ Computer Skills: Worse Than You Think</a></li>
                
              
                
                <li><a href="../power-law-learning/index.php">The Power Law of Learning: Consistency vs. Innovation in User Interfaces </a></li>
                
              
                
                <li><a href="../negativity-bias-ux/index.php">The Negativity Bias in User Experience</a></li>
                
              
                
                <li><a href="../prospect-theory/index.php">Prospect Theory and Loss Aversion: How Users Make Decisions</a></li>
                
              
                
                <li><a href="../fresh-start-effect/index.php">Fresh Start Effect: How to Motivate Users with New Beginnings</a></li>
                
              
            </ul>
          </div>
        
        

        
          
          <div>
            <h3>Research Reports</h3>
            <ul class="no-bullet">
              
                
                  <li><a href="../../reports/ecommerce-user-experience/index.php">E-Commerce User Experience</a></li>
                
              
                
                  <li><a href="../../reports/b2b-websites-usability/index.php">B2B Website Usability for Converting Users into Leads and Customers</a></li>
                
              
                
                  <li><a href="../../reports/strategic-design-faqs/index.php">Strategic Design for Frequently Asked Questions</a></li>
                
              
                
                  <li><a href="../../reports/celebrating-holidays-and-current-events-web/index.php">Celebrating Holidays and Current Events on the Web</a></li>
                
              
            </ul>
          </div>
          
        

      </div>
      <div class="medium-6 columns">
        
          
          <div>
            <h3>UX Conference Training Courses</h3>
             <ul class="no-bullet">
    
        <li><a href="../../courses/ux-basic-training/index.php">UX Basic Training</a></li>
    
        <li><a href="../../courses/human-mind/index.php">The Human Mind and Usability</a></li>
    
        <li><a href="../../courses/hci/index.php">User Interface Principles Every Designer Must Know</a></li>
    
        <li><a href="../../courses/cross-channel-user-experience/index.php">Omnichannel Journeys and Customer Experience</a></li>
    
        <li><a href="../../courses/emerging-patterns-web-design/index.php">Emerging Patterns for Web Design</a></li>
    
</ul>
          </div>
          
        
        
          
          <div>
            <h3>Online Seminars</h3>
            <ul class="no-bullet">
            
                <li><a href="../../online-seminars/fundamental-flaw-flat-design/index.php">The Fundamental Flaw in Flat Design</a></li>
            
                <li><a href="../../online-seminars/millennials-online/index.php">Millennials Online</a></li>
            
                <li><a href="../../online-seminars/university-websites/index.php">University Website UX Essentials</a></li>
            
            </ul>
          </div>
          
        
      </div>
    </div>
  </footer>
</article>

        </div>

      

    
  </div>
  <div class="row">
    
      <div class="column small-12 show-for-small-down l-navrail announcements">
        
          
        
      </div>
    
    
  </div>

    </div>
    




<footer id="page-footer">
    <div class="row hide-for-print">
        <div class="column small-12 medium-6">
            <section class="footer-popular">
                
                <h1>Popular Topics</h1>
                <div style="float: left;  width: 15em; display: block;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/applications/index.php">Application Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/e-commerce/index.php">E-Commerce Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/eyetracking/index.php">Eyetracking </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/information-architecture/index.php">Information Architecture </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/intranets/index.php">Intranet Design </a></li>
	<li><a href="../../topic/interaction-design/index.php">Interaction Design </a></li>
</ul>
</div>

<div style="float: left;  width: 15em; display: block; list-style: none;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/mobile-and-tablet-design/index.php">Mobile and Tablet Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/credibility-persuasion/index.php">Persuasive Design</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/research-methods/index.php">Research Methods</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/user-testing/index.php">User Testing </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/web-usability/index.php">Web Usability </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/writing-web/index.php">Web Writing &amp; Content Strategy </a></li>
</ul>
</div>

<p>&nbsp;</p>

                
            </section>
        </div>

        <div class="column small-12 medium-6">
            <div class="row">
                <div class="column small-6">
                    <section class="footer-about">
                        <h1>Follow Us</h1>
                        <ul>
	<li><a href="../subscribe/index.php">Subscribe to our weekly UX newsletter</a>,&nbsp;Jakob Nielsen&#39;s Alertbox</li>
	<li>Visit Don Norman&#39;s website, <a href="http://www.jnd.org/"> www.jnd.org </a></li>
	<li>Visit Bruce Tognazzini&#39;s website, <a href="http://www.asktog.com/"> www.asktog.com </a></li>
	<li style="padding-top: 6px;"><a class="twitter-follow-button" data-show-count="false" data-size="large" href="https://twitter.com/nngroup"><img alt="Follow @nngroup on Twitter" height="20" src="https://media.nngroup.com/media/editor/2016/12/30/twitter-follow-button.png" style="width: 117px; height: 20px;" width="117" /></a></li>
	<li style="padding-top: 6px;"><img alt="" height="14" src="https://media.nngroup.com/media/editor/2016/07/26/feed-icon-14x14.png" style="vertical-align:text-top;width:14px;margin-right:.25em;" width="14" /><a href="../../feed/rss/index.php">Subscribe to our RSS feed</a></li>
</ul>

                    </section>
                </div>

                <div class="column small-6">
                    <section class="footer-about right-column">
                        <h1>About
</h1>
                        <ul class="no-bullet">
	<li><a href="../../about/contact/index.php">Contact information</a></li>
	<li><a href="../../about/index.php">About Us</a></li>
	<li><a href="../../about/why-nng/index.php">Why NN/g? </a></li>
	<li><a href="../../return-policy/index.php">Return Policy</a></li>
</ul>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="copyright column small-12">
            <p><a href="../../copyright-and-reprint-info/index.php">Copyright</a> &copy; 1998-2017 Nielsen Norman Group, All Rights Reserved.</p>
        </div>
    </div>
</footer>


    


        <script src="https://media.nngroup.com/static/vendor/fastclick/js/fastclick.min.js"></script>
        <script src="https://media.nngroup.com/static/vendor/foundation/js/foundation.min.js"></script>
        <script src="http://media.nngroup.com/static/js/app.js"></script>
        <script src="http://media.nngroup.com/static/js/scroll-to-fixed.js"></script><script>var externalLinks = document.querySelectorAll('a[href^="http"]');for (var i = externalLinks.length-1; i >= 0; i--) {externalLinks[i].addEventListener("click", function(event) { alert("External links are disabled for this test."); event.preventDefault(); }, false);}var mailLinks = document.querySelectorAll('a[href^="mailto"]');for (var i = mailLinks.length-1; i >= 0; i--) {mailLinks[i].addEventListener("click", function(event) { alert("Email links are disabled for this test."); event.preventDefault(); }, false);}var externalForms = document.querySelectorAll('form');for (var i = externalForms.length-1; i >= 0; i--) {externalForms[i].addEventListener("submit", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false); externalForms[i].addEventListener("click", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false);}</script>

        <script>
            
            $(function() {
                $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
                $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
            });
        </script>

        
<script src="https://media.nngroup.com/static/js/articles.js"></script>


        
        <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1072699960;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072699960/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
    </body>

<!-- Mirrored from www.nngroup.com/articles/comparison-tables/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 14:55:01 GMT -->
</html>
