<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->





    
<!-- Mirrored from www.nngroup.com/articles/b2b-vs-b2c/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:13:04 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1026.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"licenseKey":"1a30ef9931","errorBeacon":"bam.nr-data.net","applicationID":"2048992","transactionName":"NFxSMRBUX0EDUkIPDA0WdhAMVkVbDV8ZBxEXUFMJB0YfRAtUQRVZIktEDAFZVHYHRVcPD01eVRE=","beacon":"bam.nr-data.net","queueTime":3,"applicationTime":441,"agent":""}</script>
        <title>B2B vs. B2C Websites: Key UX Differences </title><meta property="og:title" content="B2B vs. B2C Websites: Key UX Differences " />
  
        
        <meta name="description" content="B2B sites must help both end users and decision makers. Include price, specifications, compatibility information, and be relevant to all targeted verticals. ">
        <meta property="og:description" content="B2B sites must help both end users and decision makers. Include price, specifications, compatibility information, and be relevant to all targeted verticals. " />
        
  
        
	
        
        <meta name="keywords" content="B2B B2C Usability User Experience UX design veticals segmentation content strategy buying cycle integrations compatibility standards users choosers complex pricing">
        

        

        <meta name="robots" content="noarchive">
        <meta name="viewport" content="width=device-width">

        <link rel="search" type="application/opensearchdescription+xml" title="NN/g Search" href="../../opensearch.xml" />
        <link rel="shortcut icon" href="http://media.nngroup.com/static/img/favicon.ico" />

        <link rel="stylesheet" href="https://media.nngroup.com/static/vendor/font-awesome/css/font-awesome.min.css">

        
        <link rel="stylesheet" href="https://media.nngroup.com/static/css/7232a960f1df.css" type="text/css" />
        <link rel="stylesheet" href="http://fast.fonts.com/t/1.css?apiType=css&amp;projectid=c5846260-9def-46ec-9407-0f1349bd2d64" type="text/css">

        <script src="https://media.nngroup.com/static/vendor/modernizr/js/modernizr.min.js"></script>

        <script type="text/javascript" src="https://media.nngroup.com/static/js/3283be5def74.js"></script>
        

        

        
        
        <!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=229034,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('http://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->


<!-- Google Analytics Tracking Code for http://nngroup.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23132681-1', 'nngroup.com');
  ga('require', 'linkid', 'https://www.nngroup.com/articles/b2b-vs-b2c/linkid.js');
  ga('require', 'displayfeatures');

// Optimizely Universal Analytics Integration
 window.optimizely = window.optimizely || [];
 window.optimizely.push("activateUniversalAnalytics");

  ga('send', 'pageview');

</script>
<script>
/**
* Function to track a click on an outbound link in Google Analytics.
*/
function trackOutboundLink(label, url) {
    if(window.ga && ga.loaded) {
        ga('send', 'event', 'outbound', 'click', label, {'hitCallback': function(){document.location = url;}
        });
    } else {
        document.location = url;
    }
}
/**
* Function to track a click on a link in the article announcement area.
*/
$('#articleAnnouncement a').on('click', function(){
  ga('send', 'event', 'link', 'click', 'articleAnnouncement');
});
</script>

<script type="text/javascript">
 setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2421.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



<!-- Hotjar Tracking Code for http://nngroup.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:121605,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

        <script src="https://media.nngroup.com/static/js/analytics/track_outbound_link.js"></script>
        
        
        
    </head>
    <body class="location-articles articles-detail">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
    <header id="page-header">
        <div id="cart" class="shopping-cart mobile-cart show-for-small-down"></div>
        <div class="row">
            <div id="masthead" class="masthead small-12 medium-7 column">

                <!-- The button to toggles menu on small screen -->

                <h1><a href="../../index.php"><img src="http://media.nngroup.com/static/img/logo-wordmark.png" width="74" height="30"> Nielsen <span class="norman">Norman</span> Group</a></h1>
                <p class="tagline show-for-medium-up">Evidence-Based User Experience Research, Training, and Consulting</p>

                <!-- The button to toggles form search on small screen -->


                <div style="clear: right;width:  100%;height: 1px;"></div><?php define('ROOT_PATH',$_SERVER["DOCUMENT_ROOT"] . '/apps/navigation_styles/navigations/1'); ?><?php include(ROOT_PATH)."/navigation.php"; ?>

                <!-- Drop down menu on small screen -->
                <div id="hideNav" class="show-for-small-down">
                  
                  <ul class="menu-content">
                    <li><a href="../../index.php">Home</a></li>
                    <li class="has-submenu">
                      <a>Training</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../training/index.php">Overview</a></li>
                        
<li id="nav-ux-conference"><a href="../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../online-seminars/index.php"><span>Online Seminars</span></a></li>

                      </ul>
                    </li>
                    <li class="has-submenu">
                      <a>Consulting</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
  <li><a href="../../consulting/index.php">Overview</a></li>
  
  <li><a href="../../consulting/benchmarking/index.php">Benchmarking</a></li>
  
  <li><a href="../../consulting/expert-design-review/index.php">Expert Design Review</a></li>
  
  <li><a href="../../consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
  
  <li><a href="../../consulting/usability-testing/index.php">Usability Testing </a></li>
  
  <li><a href="../../consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
  
  <li><a href="../../in-house-training/index.php">In-House Training</a></li>
</ul>

                    </li>

                    <li><a href="../../reports/index.php">Reports</a></li>
                    <li><a href="../index.php">Articles</a></li>
                    <li class="has-submenu"><a>About NN/G</a><i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../about/index.php">Overview</a></li>
                        <li><a href="../../people/index.php">People</a></li>
                        <li><a href="../../about/why-nng/index.php">Why NN/g?</a></li>
                        <li><a href="../../about/contact/index.php">Contact</a></li>
                        <li><a href="../../news/index.php">News</a></li>
                        <li><a href="../../about/history/index.php">History</a></li>
                        <li><a href="../../books/index.php">Books</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                </div>

                <hr class="header-divider show-for-small-only"/>


            </div>
       </div>

        <div id="hideNav">            <nav class="primary-nav small-12 column">
                <ul>
                    <li><a class="navitem" id="nav-home" href="../../index.php">Home</a></li>
                    <li><a class="navitem" id="nav-training" href="../../training/index.php">Training</a></li>
                    <li><a class="navitem" id="nav-consulting" href="../../consulting/index.php">Consulting</a></li>
                    <li><a class="navitem" id="nav-reports" href="../../reports/index.php">Reports</a></li>
                    <li><a class="navitem" id="nav-articles" href="../index.php">Articles</a></li>
                    <li><a class="navitem" id="nav-about" href="../../about/index.php">About NN/G</a></li>
                </ul>
            </nav>
            <nav class="secondary-nav small-12 medium-12 large-12 columns">
                
                
            </nav>
        </div>
    </header>
    <div class="l-content">
    
  

  

  <div class="row">
    
      
      <section class="small-12 medium-3 columns collapsable-content l-navrail articles-left-column">
        <h1 class="show-for-small-down collapse-link mobile-browse-header">Browse by Topic and Author</h1>
          <div class="row collapsable">

            
            <div class="small-6 medium-12 columns">
              <section id="article-popular-topics" style="display: none;">
                
                <h1>Topics</h1>
                <ul id="article-popular-topics">
                    
                    <li>
                        
                        <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/intranets/all/index.php">Intranets</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/user-testing/all/index.php">User Testing</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                        
                    </li>
                    
                    <li><a href="#" id="article-see-all-topics"><strong>See all topics&hellip;</strong></a></li>
                </ul>
                
              </section>
              <section id="article-all-topics" style="display: none;">
                
                <h1>
                  <span class="show-for-medium-up">All Article Topics</span>
                  <span class="show-for-small-down">All Topics</span>
                  (<a href="#" id="article-hide-all-topics">hide</a>)
                </h1>
                <ul>
                  
                  <li>
                    
                    <a href="../../topic/accessibility/all/index.php">Accessibility</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/agile/all/index.php">Agile</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/analytics-and-metrics/all/index.php">Analytics &amp; Metrics</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/applications/all/index.php">Application Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/b2b-websites/all/index.php">B2B Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/behavior-patterns/all/index.php">Behavior Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/branding/all/index.php">Branding</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/content-strategy/all/index.php">Content Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/corporate-websites/all/index.php">Corporate Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/design-patterns/all/index.php">Design Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-design-process/all/index.php">Design Process</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/email/all/index.php">Email</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/eyetracking/all/index.php">Eyetracking</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/heuristic-evaluation/all/index.php">Heuristic Evaluation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/human-computer-interaction/all/index.php">Human Computer Interaction</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-ideation/all/index.php">Ideation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/information-architecture/all/index.php">Information Architecture</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/interaction-design/all/index.php">Interaction Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/international-users/all/index.php">International Users</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/intranets/all/index.php">Intranets</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-management/all/index.php">Management</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/navigation/all/index.php">Navigation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/non-profit-websites/all/index.php">Non-Profit Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/personas/all/index.php">Personas</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/persuasive-design/all/index.php">Persuasive Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/prototyping/all/index.php">Prototyping</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/psychology-and-ux/all/index.php">Psychology and UX</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/research-methods/all/index.php">Research Methods</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/search/all/index.php">Search</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/social-media/all/index.php">Social Media</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/strategy/all/index.php">Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/user-testing/all/index.php">User Testing</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-teams/all/index.php">UX Teams</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/visual-design/all/index.php">Visual Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/writing-web/all/index.php">Writing for the Web</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/young-users/all/index.php">Young Users</a>
                    
                  </li>
                  
                </ul>
                
              </section>
            </div>
            <script>
              /* Determine which topics list will be displayed: the full or the
               * partial.  We are inlining this close to the list itself to
               * prevent rendering delays on the client
               */
              var topic_list = $.cookie("article-topics");

              var popular_topics = $('#article-popular-topics');
              var all_topics = $('#article-all-topics');

              if(topic_list == 1) {
                  popular_topics.hide();
                  all_topics.show();
              } else {
                  popular_topics.show();
                  all_topics.hide();
              }
            </script>
            

            
            <div class="small-6 medium-12 columns">
              
              <section id="article-initial-authors">
                  <h1>Author</h1>
                  <ul>
                    
                    <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                    
                    <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                    
                    <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                    
                    <li><a href="#" onClick="$('#article-initial-authors').hide(); $('#article-all-authors').show();"><strong>See all authors&hellip;</strong></a></li>
                  </ul>
              </section>
              <section id="article-all-authors" style="display: none;">
                <h1>
                  All Authors
                  (<a href="#" id="article-hide-all-authors">hide</a>)
                </h1>
                <ul>
                  
                  <li><a href="../author/raluca-budiu/index.php">Raluca Budiu</a></li>
                  
                  <li><a href="../author/patty-caya/index.php">Patty Caya</a></li>
                  
                  <li><a href="../author/susan-farrell/index.php">Susan Farrell</a></li>
                  
                  <li><a href="../author/therese-fessenden/index.php">Therese Fessenden</a></li>
                  
                  <li><a href="../author/kim-flaherty/index.php">Kim Flaherty</a></li>
                  
                  <li><a href="../author/sarah-gibbons/index.php">Sarah Gibbons</a></li>
                  
                  <li><a href="../author/aurora-harley/index.php">Aurora Harley</a></li>
                  
                  <li><a href="../author/kate-kaplan/index.php">Kate Kaplan</a></li>
                  
                  <li><a href="../author/page-laubheimer/index.php">Page Laubheimer</a></li>
                  
                  <li><a href="../author/angela-li/index.php">Angie Li</a></li>
                  
                  <li><a href="../author/hoa-loranger/index.php">Hoa Loranger</a></li>
                  
                  <li><a href="../author/kate-meyer/index.php">Kate Meyer</a></li>
                  
                  <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                  
                  <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                  
                  <li><a href="../author/kara-pernice/index.php">Kara Pernice</a></li>
                  
                  <li><a href="../author/christian-rohrer/index.php">Christian Rohrer</a></li>
                  
                  <li><a href="../author/amy-schade/index.php">Amy Schade</a></li>
                  
                  <li><a href="../author/katie-sherwin/index.php">Katie Sherwin</a></li>
                  
                  <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                  
                  <li><a href="../author/kathryn-whitenton/index.php">Kathryn Whitenton</a></li>
                  
                </ul>
              </section>
              
            </div>
            
          </div>

          
          <section class="hide-for-small-down">
            
              
              <h1>Recent Articles</h1>
              
              <ul>
                  
                  <li><a href="../seamless-cross-channel/index.php">Seamlessness in the Omnichannel User Experience</a></li>
                  
                  <li><a href="../brand-intention-interpretation/index.php">Experience Design: Bridging Brand Intention and Brand Interpretation</a></li>
                  
                  <li><a href="../eyetracking-tasks-efficient-scanning/index.php">Scanning Patterns on the Web Are Optimized for the Current Task</a></li>
                  
                  <li><a href="../flat-design-best-practices/index.php">Flat-Design Best Practices</a></li>
                  
                  <li><a href="../ux-user-stories/index.php">Accounting for UX Work with User Stories in Agile Projects</a></li>
                  
                  <li><a href="../index.php"><strong>See all articles&hellip;</strong></a></li>
              </ul>
              
            
          </section>
          

          
          
          <section class="hide-for-small-down">
            <h1>Popular Articles</h1>
            
            <ul>
                
                <li><a href="../usability-101-introduction-to-usability/index.php">Usability 101: Introduction to Usability</a></li>
                
                <li><a href="../top-10-mistakes-web-design/index.php">Top 10 Mistakes in Web Design</a></li>
                
                <li><a href="../how-users-read-on-the-web/index.php">How Users Read on the Web</a></li>
                
                <li><a href="../f-shaped-pattern-reading-web-content/index.php">F-Shaped Pattern For Reading Web Content</a></li>
                
                <li><a href="../ten-usability-heuristics/index.php">10 Usability Heuristics for User Interface Design</a></li>
                
                <li><a href="../intranet-design-2016/index.php">10 Best Intranets of 2016</a></li>
                
                <li><a href="../which-ux-research-methods/index.php">When to Use Which User-Experience Research Methods</a></li>
                
                <li><a href="../response-times-3-important-limits/index.php">Response Times: The 3 Important Limits</a></li>
                
                <li><a href="../why-you-only-need-to-test-with-5-users/index.php">Why You Only Need to Test with 5 Users</a></li>
                
                <li><a href="../page-fold-manifesto/index.php">The Fold Manifesto: Why the Page Fold Still Matters</a></li>
                
            </ul>
          </section>
          
          

          
            <section class="hide-for-small-down">
              
            </section>
          

      </section>
      

      
        <div class="small-12 medium-9 columns content-body">
          




<article class="l-subsection alertbox-article-content article-detail-bl">
  
  <header>
    <div class="l-subsection">
      <h1>B2B vs. B2C Websites: Key UX Differences </h1>
      <div class="author-meta">by
        
          
            
            <a href="../author/page-laubheimer/index.php">Page Laubheimer</a>
            
          
        on  May 29, 2016
        
      </div>

      

      
      <div class="topic-links">
        Topics:
        
          <ul>

  <li><a href="../../topic/b2b-websites/index.php">B2B Websites</a></li>

</ul>
        
      </div>
      
    </div>
  </header>
  

  
  
  <section>
    <p class="lede"><strong>Summary:</strong> Business-to-Business websites need to include content that is tailored to both end users and decision makers during long purchase cycles, clearly indicates integration details, has representative prices even for complex pricing scenarios, and speaks to all relevant verticals without alienating any of them.
</p>
    <br />
  </section>
  
  

  
  <section id="article-body"><p>Business-to-business (B2B) sites have much in common with business-to-consumer (B2C) ecommerce sites: they need to <a href="../category-names-suck/index.php">establish a clear information architecture</a>, include <a href="../cringeworthy-words/index.php">compelling content</a>, offer <a href="../product-descriptions/index.php">details about products and services that users care about</a>, and have simple, understandable <a href="http://asktog.com/atc/principles-of-interaction-design/">interaction design</a>. In essence, nearly all of the standard user-experience principles that have been discovered for B2C apply to B2B as well. When testing B2B sites, we often hear business customers lament the usability gap between B2B sites and the better-designed consumer sites they use after hours.</p>

<p>However, the B2B audience often has needs that are very different from those of consumers. As a result, B2B purchases have longer decision-making phases, often significantly higher price tags, and less of a need to market based on raw product desirability.</p>

<p>Our research with B2B sites across a broad range of verticals has identified 5 major differences in the user-experience requirements for B2B sites and B2C sites.</p>

<h2>Difference 1: Content Must Support Long Purchase Decisions</h2>

<p>B2B purchases are rarely impulsive; most commonly, they are the result of a long, complex decision-making process because they involve relatively high priced items that are expected to last for quite a while. They typically involve multiple people, across a corporate hierarchy and functional job roles. Often, customers will begin researching a purchase weeks, months, or even years before the decision is finalized. In addition, customers will share options with multiple people on their team, for research, justification, and approval purposes.</p>

<p>This purchase process frequently begins with one customer researching a problem faced by her company and ways to solve it. Initially, the customer may have little awareness of market segment, industry, or products, and may simply look for <em>a solution</em> to that particular problem. Once the user acquires an initial level of understanding, she will start researching competitors more carefully in search for the <em>best solution. </em></p>

<p>To support B2B customers at all stages of the purchase process, you need to:</p>

<ol>
	<li>Provide content such as articles, blog posts, webinars, technical white papers, buying guides, or case studies that help early-stage researchers understand the types of problems you solve, how typical solutions work, and how your products or services are a key part of those solutions.</li>
	<li>Ensure that your content provides criteria for why you have solved a particular problem <em>well</em>, so that your site visitors use those criteria as a rubric for evaluating your competitors. This type of content also helps to showcase your expertise. (Since the time of Julius Caesar, military commanders have known the advantage of choosing the battleground to suit their own army’s strengths. You should do the same. In <a href="../../courses/credibility-and-persuasive-web-design/index.php">persuasive design</a>, this concept is called <em>framing</em>.)</li>
	<li>Acknowledge your competitor's products, and show how your solutions are better, through comparison tables (or additional text content).</li>
	<li>Provide tools that enable your customers to share your content, products, and the contents of their shopping carts with other members of their team for review, discussion, and approval.</li>
</ol>

<figure class="caption"><img alt="Quadgraphics case study" border="0" height="512" src="https://media.nngroup.com/media/editor/2016/05/06/quadgraphics.png" width="720"/>
<figcaption><em>Quadgraphics.com uses case studies to showcase how its expertise and products solve specific, familiar business challenges. The site structures the content into Challenge, Solution, and Results, paying attention to the details most important for their users. The bulleted format used for case studies makes this content a useful rubric for customers to check against their competitors.</em></figcaption>
</figure>

<h2>Difference 2: Integration, Compatibility, and Regulatory Information Needs to Be Clear</h2>

<p>Many typical B2B purchases are not standalone products, but are purchased as part of a larger system that requires careful consideration of compatibility. While some B2C products (especially software or computer hardware) also have specific system requirements, compatibility is a much more prevalent consideration for B2B, as B2B customers need to ensure that new products, software, and services will fit into the existing systems and workflows used in their company. Not being able to find compatibility and integration details on a website is a major pain point for B2B customers.</p>

<p>Here is a small sample of the types of B2B purchases that need full compatibility or integration information:</p>

<ul>
	<li>Software</li>
	<li>Printers, scanners, networking equipment, and other computer hardware and devices</li>
	<li>Manufacturing hardware, including robotics and controller systems</li>
	<li>HVAC systems, HEPA filters, and cleanroom supplies</li>
	<li>Engineering test equipment and automated tools</li>
	<li>Projectors, video walls, sound systems, speakerphones, and other visualization, presentation, and conference-room equipment</li>
	<li>Chemical/biotech-storage equipment, lab equipment, and test equipment</li>
	<li>Security systems</li>
	<li>Loading dock equipment, such as automated-door controls</li>
	<li>Retail and point-of-sale terminals and kiosks</li>
</ul>

<p>When providing compatibility information, be sure to include:</p>

<ul>
	<li><strong>Product integrations</strong>: Name the standard products that you integrate with.</li>
	<li><strong>Versions</strong>: Indicate the versions of those products that you integrate with (for example, if your app requires a specific edition of Salesforce, clearly specify this).</li>
	<li><strong>Feature parity</strong>: Explicitly name the companion platform or software with which your product works best. For instance, if your product is available on both iOS and Android, but the iOS version has additional features, make that clear. Sometimes, your customers decide not only on your product, but also on other companion products at the same time, so make it clear if you have a preferred partner.</li>
	<li><strong>Technical support and integration help</strong>: Tell your users what types of support you provide during integration or transition to a new system. Such support can be a major value add, and can build confidence that you will continue to support customers long after the purchase is completed.</li>
	<li><strong>Standards</strong>: If you use industry standards for data exchange or physical connections, indicate the specific standard you comply with, and link to detailed information on that standard. (The link should point to the organization responsible for that standard, like IEEE, ISO, or ANSI.)</li>
	<li><strong>Physical connections for hardware</strong>: Provide clear engineering drawings of any hardware connections, including dimensions, requirements, or proximity of other connections (such as how close ports on a server are). Specify any third-part products or typical modifications that users must make to your product to connect with other standard items. Include temperature requirements, space required for effective heat radiation, power requirements, or other environmental details, as they can have implications for installation choices in machine rooms, server closets, and such.</li>
	<li><strong>Cloud-based integration and API-support information for software</strong></li>
</ul>

<p>Also remember that many industries also have strict regulations or other standards that they must adhere to. Examples include environmental requirements (e.g., RoHS), health- or financial-privacy requirements (e.g., HIPPA), import/export requirements (e.g., EAR/ECCN), or even quality-control standards (e.g., ISO 9001). For these customers, their choice of products or services will be affected by their need to comply with these standards, so provide detailed compliance information.</p>

<figure class="caption"><img alt="Amazon Web Services ISO 9001 certification" border="0" height="802" src="https://media.nngroup.com/media/editor/2016/05/06/aws-iso-9001.png" width="977"/>
<figcaption><em>Amazon's Web Services site provides a comprehensive page indicating its services’ compliance with ISO 9001 standards, details on the standard itself, the circumstances in which that standard matters, and how its services were certified.</em></figcaption>
</figure>

<h2>Difference 3: Content Should Speak to both "Choosers" and "Users"</h2>

<p>B2B complex purchasing processes commonly involve multiple people, often at different levels of the company hierarchy. While an engineering lead may initiate the purchase of new oscilloscopes, a midlevel manager may need to approve it, an executive may have to confirm that approval, and a procurement-team member may actually purchase it. Many B2B sites go wrong by tailoring their content to decision makers, who may never actually use a product or service, and might only refer to the website as part of a final review (rather than initial research).</p>

<p><a href="../../reports/b2b-websites-usability/index.php">Our studies revealed </a>that there was typically a lot of dialogue and discussion between decision makers (known as "choosers") and key staff that actually use the products ("users") during the purchase process. Frequently, a potential “user” would become the main researcher, and would later present options to decision makers. Once this "user" decided on a favorite product, he often became a proxy or surrogate for that product, seeking <a href="../b2b-convincing-decision-makers/index.php">ways to justify it to his boss</a> (the "chooser").</p>

<p>Your content must speak to both users and choosers. "Users" are often concerned with specifications and details, with the experience of using a product, and with the available support options after purchase. They also are interested in ways to justify it to their supervisors. Focus your content on answering questions about the user experience, and provide an “advocacy kit” (like brochures and PowerPoint templates) to help end users be your champions during the purchasing process.</p>

<p>For service-based businesses, the end users will want to read the bio pages of those team members they might interact with; your team pages should include not only executives and leadership, but also actual account reps (and others) with whom your customers will work during the engagement.</p>

<p>"Choosers" are typically concerned with cost, reliability, integration efforts, support contracts, and evidence that the purchase is going to provide the best possible return on their investment. ROI information, details on typical product lifespan, and information on competitive advantages speak effectively to decision makers.</p>

<h2>Difference 4: Complex Pricing Requires Realistic Scenarios</h2>

<p>Most B2C products have straightforward prices that stay the same from one customer to the next, (unless there are major sales occurring or promotion codes are offered). With B2B, things aren't always so simple. The product or service may be heavily customized for each customer's needs, there can be significant quantity discounts, or customers may have relationships with account reps that allow them to negotiate favorable discounts.</p>

<p>As a result, pricing is not nearly as simple for B2B as it is for B2C, and in some cases, it can be very challenging to display prices. However, <a href="../show-price/index.php">B2B customers are just as price conscious </a>as B2C customers; while price may not be the only point under consideration, it certainly is a major factor. Whenever possible, show the exact price.</p>

<p>If showing exact pricing is not possible, <a href="../show-prices-for-common-scenarios/index.php">show <em>representative</em> sample pricing</a>. Present prices in common scenarios, or provide simple price calculators. Especially in early-stage research, B2B customers will need a price range so they can begin getting budget approval; precise pricing can be confirmed later on.</p>

<figure class="caption"><img alt="Chargify pricing chart" border="0" height="563" src="https://media.nngroup.com/media/editor/2016/05/06/chargify.png" width="700"/>
<figcaption><em>Chargify shows a simple pricing chart, with only a few commonplace scenarios corresponding to different organization sizes. For large-enterprises that will require a custom contract, the site gives a ballpark price ("as low as $0.06 per customer"), and has a clear call-to-action button to have a discussion with a sales rep for a formal quote ("</em>Let's Chat<em>").</em></figcaption>
</figure>

<p>Showing the price can have another positive effect for your business: the price information acts as a filter, and your sales team will have to spend less time following up with leads that cannot afford your products or services. However, they may keep your product in mind as the "next step up" for when they will be able to afford you.</p>

<p>At the other end of the scale, if you’re very cheap but still able to serve enterprise customers, you’ll need to explain why a cheap solution can scale as required.</p>

<h2>Difference 5: Speak to Different Customer Segments and Scales of Businesses Without Alienating Audiences</h2>

<p>B2B companies often need to support customers from different-size businesses, from small mom-and-pop shops to huge, multinational enterprises. This often means that some of the available offerings may not be suitable for all customer segments. In addition, a B2B site has to establish credibility with each audience, indicating how its solutions fit the problems in that industry and scale. For example, a B2B site selling security systems needs to be able to communicate whether those systems are appropriate for securing a small-business storefront during off hours, or a bank branch (and indeed the bank’s entire network of branches). These two customers are both B2B, but have very different needs, so it is critical that the site selling to them is able to help those users find the products and solutions that fit their needs.</p>

<p>A typical approach to this problem is to build the site's information architecture around these customer segments, using terms like <em>For Small Business</em>, or <em>For National Accounts.</em> However, <a href="../audience-based-navigation/index.php">audience-based navigation can cause a myriad of problems</a> if these segments aren't clearly labeled (or if there is any overlap between them). For example, a huge enterprise customer might still have a small team with a specialized (and localized) need closely matches that of small-business customer. Whenever using visitor segments in navigation, ensure that they are clearly defined (i.e. define the number of employees for a small business vs. a large business), and that they are mutually exclusive.</p>

<figure class="caption"><img alt="Sharp B2B navigation with verticals" border="0" height="461" src="https://media.nngroup.com/media/editor/2016/05/06/sharpb2b.png" width="700"/>
<figcaption><em>Sharp's business site features a navigation based on customer-industry segments. Since several of these verticals overlap to some degree (such as </em>Corporate<em> and </em>Legal<em>), Sharp also provides navigation options to find products by type.</em></figcaption>
</figure>

<p>Another option is to include <a href="../filters-vs-facets/index.php">filters or facets</a> that help users find products and services based on their needs, rather than arbitrary company size or market segments. These mechanisms allow users to identify products satisfying a range of criteria, beyond the simplistic company sizes. If you design your B2B product pages to speak to multiple segments, be cautious with large images that show products in use; if these images are too specific, they can alienate customers from other industries.</p>

<h2>Summary</h2>

<p>While B2B sites have a lot in common with B2C websites, B2B audiences have distinct needs from consumer audiences. When designing a B2B site make sure that you support a complex buying cycle with content that speaks to your audience throughout the decision-making process. Clearly identify how your products integrate with other common industry solutions and include content that speaks to both the end users and the purchasing decision makers. Communicate your price (or realistic sample prices), and help users find products that fit the needs unique to companies of their size.</p>

<p>To learn more about designing for B2B websites, read our brand new <a href="../../reports/b2b-websites-usability/index.php">3<sup>rd</sup> edition B2B Website UX Report</a>, complete with 183 design guidelines and 249 screenshots and illustrations of specific designs that worked or failed with the users in our research.</p>

<h2>Video</h2>

<p>Watch Page Laubheimer discuss the differences between B2B and B2C (2 min. video):</p>

<div class="flex-video"><iframe allowfullscreen="" frameborder="0" height="424" src="https://www.youtube.com/embed/VaBqftkCeaM?rel=0" width="720"></iframe></div>
</section>
  

  
  <section>
    
        
    
  </section>
  
  
  
  
  

  


  <section>
    <br />
    <p class="share"><b>Share <span class="hide-for-small-down">this article</span>:</b> <a href="https://twitter.com/intent/tweet?url=http://www.nngroup.com/articles/b2b-vs-b2c/&amp;text=B2B%20vs.%20B2C%20Websites:%20Key%20UX%20Differences%20&amp;via=nngroup">Twitter</a> | <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=http://www.nngroup.com/articles/b2b-vs-b2c/&amp;title=B2B%20vs.%20B2C%20Websites:%20Key%20UX%20Differences%20&amp;source=Nielsen%20Norman%20Group">LinkedIn</a> | <a href="https://plus.google.com/share?url=http://www.nngroup.com/articles/b2b-vs-b2c/">Google+</a> | <a href="mailto:?subject=NN/g Article: B2B vs. B2C Websites: Key UX Differences &amp;body=http://www.nngroup.com/articles/b2b-vs-b2c/">Email</a></p>
    <hr />
  </section>

  <footer class="l-subsection">
    
    
    <div class="learn-more">
      <h2>Learn More</h2>
      <p><a href="../subscribe/index.php">Subscribe to the weekly newsletter</a> to get notified about future articles.</p>
      </div>
    
    

    <div class="row show-for-small-down related-content">
      
      
      

      
      
        <div class="column small-12 topic-links">All articles about:
          <ul>
            
            <li><a href="../../topic/b2b-websites/index.php">B2B Websites</a></li>
            
          </ul>
        </div>
      
      

      
      
      

      
      
        <div class="small-12 column">
          <h3>Research Reports</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../reports/b2b-websites-usability/index.php">B2B Website Usability for Converting Users into Leads and Customers</a></li>
              
            
          </ul>
        </div>
      
      

      
      
      
    </div>

    <div class="hide-for-small-down row related-content">
      <div class="medium-6 columns">
        
        
          <div>
            <h3>Articles</h3>
            <ul class="no-bullet">
              
                
                <li><a href="../international-b2b/index.php">International B2B Audiences: Top 5 Ways to Improve Your Site for Global Users</a></li>
                
              
                
                <li><a href="../b2b-specs/index.php">B2B Product Specifications: Guidelines for Creating Useful Product-Specs Pages</a></li>
                
              
                
                <li><a href="../content-behind-forms/index.php">When to Hide Content Behind Forms and When to Give Content Away </a></li>
                
              
                
                <li><a href="../specialized-words-specialized-audience/index.php">Use Specialized Language for Specialized Audiences</a></li>
                
              
                
                <li><a href="../format-based-navigation/index.php">Avoid Format-Based Primary Navigation</a></li>
                
              
            </ul>
          </div>
        
        

        
          
          <div>
            <h3>Research Reports</h3>
            <ul class="no-bullet">
              
                
                  <li><a href="../../reports/b2b-websites-usability/index.php">B2B Website Usability for Converting Users into Leads and Customers</a></li>
                
              
            </ul>
          </div>
          
        

      </div>
      <div class="medium-6 columns">
        
        
      </div>
    </div>
  </footer>
</article>

        </div>

      

    
  </div>
  <div class="row">
    
      <div class="column small-12 show-for-small-down l-navrail announcements">
        
          
        
      </div>
    
    
  </div>

    </div>
    




<footer id="page-footer">
    <div class="row hide-for-print">
        <div class="column small-12 medium-6">
            <section class="footer-popular">
                
                <h1>Popular Topics</h1>
                <div style="float: left;  width: 15em; display: block;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/applications/index.php">Application Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/e-commerce/index.php">E-Commerce Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/eyetracking/index.php">Eyetracking </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/information-architecture/index.php">Information Architecture </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/intranets/index.php">Intranet Design </a></li>
	<li><a href="../../topic/interaction-design/index.php">Interaction Design </a></li>
</ul>
</div>

<div style="float: left;  width: 15em; display: block; list-style: none;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/mobile-and-tablet-design/index.php">Mobile and Tablet Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/credibility-persuasion/index.php">Persuasive Design</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/research-methods/index.php">Research Methods</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/user-testing/index.php">User Testing </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/web-usability/index.php">Web Usability </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/writing-web/index.php">Web Writing &amp; Content Strategy </a></li>
</ul>
</div>

<p>&nbsp;</p>

                
            </section>
        </div>

        <div class="column small-12 medium-6">
            <div class="row">
                <div class="column small-6">
                    <section class="footer-about">
                        <h1>Follow Us</h1>
                        <ul>
	<li><a href="../subscribe/index.php">Subscribe to our weekly UX newsletter</a>,&nbsp;Jakob Nielsen&#39;s Alertbox</li>
	<li>Visit Don Norman&#39;s website, <a href="http://www.jnd.org/"> www.jnd.org </a></li>
	<li>Visit Bruce Tognazzini&#39;s website, <a href="http://www.asktog.com/"> www.asktog.com </a></li>
	<li style="padding-top: 6px;"><a class="twitter-follow-button" data-show-count="false" data-size="large" href="https://twitter.com/nngroup"><img alt="Follow @nngroup on Twitter" height="20" src="https://media.nngroup.com/media/editor/2016/12/30/twitter-follow-button.png" style="width: 117px; height: 20px;" width="117" /></a></li>
	<li style="padding-top: 6px;"><img alt="" height="14" src="https://media.nngroup.com/media/editor/2016/07/26/feed-icon-14x14.png" style="vertical-align:text-top;width:14px;margin-right:.25em;" width="14" /><a href="../../feed/rss/index.php">Subscribe to our RSS feed</a></li>
</ul>

                    </section>
                </div>

                <div class="column small-6">
                    <section class="footer-about right-column">
                        <h1>About
</h1>
                        <ul class="no-bullet">
	<li><a href="../../about/contact/index.php">Contact information</a></li>
	<li><a href="../../about/index.php">About Us</a></li>
	<li><a href="../../about/why-nng/index.php">Why NN/g? </a></li>
	<li><a href="../../return-policy/index.php">Return Policy</a></li>
</ul>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="copyright column small-12">
            <p><a href="../../copyright-and-reprint-info/index.php">Copyright</a> &copy; 1998-2017 Nielsen Norman Group, All Rights Reserved.</p>
        </div>
    </div>
</footer>


    


        <script src="https://media.nngroup.com/static/vendor/fastclick/js/fastclick.min.js"></script>
        <script src="https://media.nngroup.com/static/vendor/foundation/js/foundation.min.js"></script>
        <script src="http://media.nngroup.com/static/js/app.js"></script>
        <script src="http://media.nngroup.com/static/js/scroll-to-fixed.js"></script><script>var externalLinks = document.querySelectorAll('a[href^="http"]');for (var i = externalLinks.length-1; i >= 0; i--) {externalLinks[i].addEventListener("click", function(event) { alert("External links are disabled for this test."); event.preventDefault(); }, false);}var mailLinks = document.querySelectorAll('a[href^="mailto"]');for (var i = mailLinks.length-1; i >= 0; i--) {mailLinks[i].addEventListener("click", function(event) { alert("Email links are disabled for this test."); event.preventDefault(); }, false);}var externalForms = document.querySelectorAll('form');for (var i = externalForms.length-1; i >= 0; i--) {externalForms[i].addEventListener("submit", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false); externalForms[i].addEventListener("click", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false);}</script>

        <script>
            
            $(function() {
                $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
                $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
            });
        </script>

        
<script src="https://media.nngroup.com/static/js/articles.js"></script>


        
        <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1072699960;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072699960/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
    </body>

<!-- Mirrored from www.nngroup.com/articles/b2b-vs-b2c/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:13:04 GMT -->
</html>
