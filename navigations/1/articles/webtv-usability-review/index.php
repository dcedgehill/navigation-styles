<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->





    
<!-- Mirrored from www.nngroup.com/articles/webtv-usability-review/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:14:35 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1026.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"beacon":"bam.nr-data.net","queueTime":0,"applicationTime":676,"agent":"","licenseKey":"1a30ef9931","errorBeacon":"bam.nr-data.net","applicationID":"2048992","transactionName":"NFxSMRBUX0EDUkIPDA0WdhAMVkVbDV8ZBxEXUFMJB0YfRAtUQRVZIktEDAFZVHYHRVcPD01eVRE="}</script>
        <title>WebTV Usability Review</title><meta property="og:title" content="WebTV Usability Review" />
  
        
        <meta name="description" content="Analysis of the usability of WebTV, including user interface guidelines for designing cross-platform Web pages that are considerate of WebTV users">
        <meta property="og:description" content="Analysis of the usability of WebTV, including user interface guidelines for designing cross-platform Web pages that are considerate of WebTV users" />
        
  
        
	
        
        <meta name="keywords" content="WebTV, TV, television, usability, screen size, monitor, remote control, scrolling, user interface design guidelines, outlines, page navigation, cursor keys">
        

        

        <meta name="robots" content="noarchive">
        <meta name="viewport" content="width=device-width">

        <link rel="search" type="application/opensearchdescription+xml" title="NN/g Search" href="../../opensearch.xml" />
        <link rel="shortcut icon" href="http://media.nngroup.com/static/img/favicon.ico" />

        <link rel="stylesheet" href="https://media.nngroup.com/static/vendor/font-awesome/css/font-awesome.min.css">

        
        <link rel="stylesheet" href="https://media.nngroup.com/static/css/7232a960f1df.css" type="text/css" />
        <link rel="stylesheet" href="http://fast.fonts.com/t/1.css?apiType=css&amp;projectid=c5846260-9def-46ec-9407-0f1349bd2d64" type="text/css">

        <script src="https://media.nngroup.com/static/vendor/modernizr/js/modernizr.min.js"></script>

        <script type="text/javascript" src="https://media.nngroup.com/static/js/3283be5def74.js"></script>
        

        

        
        
        <!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=229034,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('http://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->


<!-- Google Analytics Tracking Code for http://nngroup.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23132681-1', 'nngroup.com');
  ga('require', 'linkid', 'https://www.nngroup.com/articles/webtv-usability-review/linkid.js');
  ga('require', 'displayfeatures');

// Optimizely Universal Analytics Integration
 window.optimizely = window.optimizely || [];
 window.optimizely.push("activateUniversalAnalytics");

  ga('send', 'pageview');

</script>
<script>
/**
* Function to track a click on an outbound link in Google Analytics.
*/
function trackOutboundLink(label, url) {
    if(window.ga && ga.loaded) {
        ga('send', 'event', 'outbound', 'click', label, {'hitCallback': function(){document.location = url;}
        });
    } else {
        document.location = url;
    }
}
/**
* Function to track a click on a link in the article announcement area.
*/
$('#articleAnnouncement a').on('click', function(){
  ga('send', 'event', 'link', 'click', 'articleAnnouncement');
});
</script>

<script type="text/javascript">
 setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2421.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



<!-- Hotjar Tracking Code for http://nngroup.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:121605,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

        <script src="https://media.nngroup.com/static/js/analytics/track_outbound_link.js"></script>
        
        
        
    </head>
    <body class="location-articles articles-detail">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
    <header id="page-header">
        <div id="cart" class="shopping-cart mobile-cart show-for-small-down"></div>
        <div class="row">
            <div id="masthead" class="masthead small-12 medium-7 column">

                <!-- The button to toggles menu on small screen -->

                <h1><a href="../../index.php"><img src="http://media.nngroup.com/static/img/logo-wordmark.png" width="74" height="30"> Nielsen <span class="norman">Norman</span> Group</a></h1>
                <p class="tagline show-for-medium-up">Evidence-Based User Experience Research, Training, and Consulting</p>

                <!-- The button to toggles form search on small screen -->


                <div style="clear: right;width:  100%;height: 1px;"></div><?php define('ROOT_PATH',$_SERVER["DOCUMENT_ROOT"] . '/apps/navigation_styles/navigations/1'); ?><?php include(ROOT_PATH)."/navigation.php"; ?>

                <!-- Drop down menu on small screen -->
                <div id="hideNav" class="show-for-small-down">
                  
                  <ul class="menu-content">
                    <li><a href="../../index.php">Home</a></li>
                    <li class="has-submenu">
                      <a>Training</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../training/index.php">Overview</a></li>
                        
<li id="nav-ux-conference"><a href="../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../online-seminars/index.php"><span>Online Seminars</span></a></li>

                      </ul>
                    </li>
                    <li class="has-submenu">
                      <a>Consulting</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
  <li><a href="../../consulting/index.php">Overview</a></li>
  
  <li><a href="../../consulting/benchmarking/index.php">Benchmarking</a></li>
  
  <li><a href="../../consulting/expert-design-review/index.php">Expert Design Review</a></li>
  
  <li><a href="../../consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
  
  <li><a href="../../consulting/usability-testing/index.php">Usability Testing </a></li>
  
  <li><a href="../../consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
  
  <li><a href="../../in-house-training/index.php">In-House Training</a></li>
</ul>

                    </li>

                    <li><a href="../../reports/index.php">Reports</a></li>
                    <li><a href="../index.php">Articles</a></li>
                    <li class="has-submenu"><a>About NN/G</a><i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../about/index.php">Overview</a></li>
                        <li><a href="../../people/index.php">People</a></li>
                        <li><a href="../../about/why-nng/index.php">Why NN/g?</a></li>
                        <li><a href="../../about/contact/index.php">Contact</a></li>
                        <li><a href="../../news/index.php">News</a></li>
                        <li><a href="../../about/history/index.php">History</a></li>
                        <li><a href="../../books/index.php">Books</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                </div>

                <hr class="header-divider show-for-small-only"/>


            </div>
       </div>

        <div id="hideNav">            <nav class="primary-nav small-12 column">
                <ul>
                    <li><a class="navitem" id="nav-home" href="../../index.php">Home</a></li>
                    <li><a class="navitem" id="nav-training" href="../../training/index.php">Training</a></li>
                    <li><a class="navitem" id="nav-consulting" href="../../consulting/index.php">Consulting</a></li>
                    <li><a class="navitem" id="nav-reports" href="../../reports/index.php">Reports</a></li>
                    <li><a class="navitem" id="nav-articles" href="../index.php">Articles</a></li>
                    <li><a class="navitem" id="nav-about" href="../../about/index.php">About NN/G</a></li>
                </ul>
            </nav>
            <nav class="secondary-nav small-12 medium-12 large-12 columns">
                
                
            </nav>
        </div>
    </header>
    <div class="l-content">
    
  

  

  <div class="row">
    
      
      <section class="small-12 medium-3 columns collapsable-content l-navrail articles-left-column">
        <h1 class="show-for-small-down collapse-link mobile-browse-header">Browse by Topic and Author</h1>
          <div class="row collapsable">

            
            <div class="small-6 medium-12 columns">
              <section id="article-popular-topics" style="display: none;">
                
                <h1>Topics</h1>
                <ul id="article-popular-topics">
                    
                    <li>
                        
                        <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/intranets/all/index.php">Intranets</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/user-testing/all/index.php">User Testing</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                        
                    </li>
                    
                    <li><a href="#" id="article-see-all-topics"><strong>See all topics&hellip;</strong></a></li>
                </ul>
                
              </section>
              <section id="article-all-topics" style="display: none;">
                
                <h1>
                  <span class="show-for-medium-up">All Article Topics</span>
                  <span class="show-for-small-down">All Topics</span>
                  (<a href="#" id="article-hide-all-topics">hide</a>)
                </h1>
                <ul>
                  
                  <li>
                    
                    <a href="../../topic/accessibility/all/index.php">Accessibility</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/agile/all/index.php">Agile</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/analytics-and-metrics/all/index.php">Analytics &amp; Metrics</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/applications/all/index.php">Application Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/b2b-websites/all/index.php">B2B Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/behavior-patterns/all/index.php">Behavior Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/branding/all/index.php">Branding</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/content-strategy/all/index.php">Content Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/corporate-websites/all/index.php">Corporate Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/design-patterns/all/index.php">Design Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-design-process/all/index.php">Design Process</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/email/all/index.php">Email</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/eyetracking/all/index.php">Eyetracking</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/heuristic-evaluation/all/index.php">Heuristic Evaluation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/human-computer-interaction/all/index.php">Human Computer Interaction</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-ideation/all/index.php">Ideation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/information-architecture/all/index.php">Information Architecture</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/interaction-design/all/index.php">Interaction Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/international-users/all/index.php">International Users</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/intranets/all/index.php">Intranets</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-management/all/index.php">Management</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/navigation/all/index.php">Navigation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/non-profit-websites/all/index.php">Non-Profit Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/personas/all/index.php">Personas</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/persuasive-design/all/index.php">Persuasive Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/prototyping/all/index.php">Prototyping</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/psychology-and-ux/all/index.php">Psychology and UX</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/research-methods/all/index.php">Research Methods</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/search/all/index.php">Search</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/social-media/all/index.php">Social Media</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/strategy/all/index.php">Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/user-testing/all/index.php">User Testing</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-teams/all/index.php">UX Teams</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/visual-design/all/index.php">Visual Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/writing-web/all/index.php">Writing for the Web</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/young-users/all/index.php">Young Users</a>
                    
                  </li>
                  
                </ul>
                
              </section>
            </div>
            <script>
              /* Determine which topics list will be displayed: the full or the
               * partial.  We are inlining this close to the list itself to
               * prevent rendering delays on the client
               */
              var topic_list = $.cookie("article-topics");

              var popular_topics = $('#article-popular-topics');
              var all_topics = $('#article-all-topics');

              if(topic_list == 1) {
                  popular_topics.hide();
                  all_topics.show();
              } else {
                  popular_topics.show();
                  all_topics.hide();
              }
            </script>
            

            
            <div class="small-6 medium-12 columns">
              
              <section id="article-initial-authors">
                  <h1>Author</h1>
                  <ul>
                    
                    <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                    
                    <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                    
                    <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                    
                    <li><a href="#" onClick="$('#article-initial-authors').hide(); $('#article-all-authors').show();"><strong>See all authors&hellip;</strong></a></li>
                  </ul>
              </section>
              <section id="article-all-authors" style="display: none;">
                <h1>
                  All Authors
                  (<a href="#" id="article-hide-all-authors">hide</a>)
                </h1>
                <ul>
                  
                  <li><a href="../author/raluca-budiu/index.php">Raluca Budiu</a></li>
                  
                  <li><a href="../author/patty-caya/index.php">Patty Caya</a></li>
                  
                  <li><a href="../author/susan-farrell/index.php">Susan Farrell</a></li>
                  
                  <li><a href="../author/therese-fessenden/index.php">Therese Fessenden</a></li>
                  
                  <li><a href="../author/kim-flaherty/index.php">Kim Flaherty</a></li>
                  
                  <li><a href="../author/sarah-gibbons/index.php">Sarah Gibbons</a></li>
                  
                  <li><a href="../author/aurora-harley/index.php">Aurora Harley</a></li>
                  
                  <li><a href="../author/kate-kaplan/index.php">Kate Kaplan</a></li>
                  
                  <li><a href="../author/page-laubheimer/index.php">Page Laubheimer</a></li>
                  
                  <li><a href="../author/angela-li/index.php">Angie Li</a></li>
                  
                  <li><a href="../author/hoa-loranger/index.php">Hoa Loranger</a></li>
                  
                  <li><a href="../author/kate-meyer/index.php">Kate Meyer</a></li>
                  
                  <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                  
                  <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                  
                  <li><a href="../author/kara-pernice/index.php">Kara Pernice</a></li>
                  
                  <li><a href="../author/christian-rohrer/index.php">Christian Rohrer</a></li>
                  
                  <li><a href="../author/amy-schade/index.php">Amy Schade</a></li>
                  
                  <li><a href="../author/katie-sherwin/index.php">Katie Sherwin</a></li>
                  
                  <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                  
                  <li><a href="../author/kathryn-whitenton/index.php">Kathryn Whitenton</a></li>
                  
                </ul>
              </section>
              
            </div>
            
          </div>

          
          <section class="hide-for-small-down">
            
              
              <h1>Recent Articles</h1>
              
              <ul>
                  
                  <li><a href="../seamless-cross-channel/index.php">Seamlessness in the Omnichannel User Experience</a></li>
                  
                  <li><a href="../brand-intention-interpretation/index.php">Experience Design: Bridging Brand Intention and Brand Interpretation</a></li>
                  
                  <li><a href="../eyetracking-tasks-efficient-scanning/index.php">Scanning Patterns on the Web Are Optimized for the Current Task</a></li>
                  
                  <li><a href="../flat-design-best-practices/index.php">Flat-Design Best Practices</a></li>
                  
                  <li><a href="../ux-user-stories/index.php">Accounting for UX Work with User Stories in Agile Projects</a></li>
                  
                  <li><a href="../index.php"><strong>See all articles&hellip;</strong></a></li>
              </ul>
              
            
          </section>
          

          
          
          <section class="hide-for-small-down">
            <h1>Popular Articles</h1>
            
            <ul>
                
                <li><a href="../usability-101-introduction-to-usability/index.php">Usability 101: Introduction to Usability</a></li>
                
                <li><a href="../top-10-mistakes-web-design/index.php">Top 10 Mistakes in Web Design</a></li>
                
                <li><a href="../how-users-read-on-the-web/index.php">How Users Read on the Web</a></li>
                
                <li><a href="../f-shaped-pattern-reading-web-content/index.php">F-Shaped Pattern For Reading Web Content</a></li>
                
                <li><a href="../ten-usability-heuristics/index.php">10 Usability Heuristics for User Interface Design</a></li>
                
                <li><a href="../intranet-design-2016/index.php">10 Best Intranets of 2016</a></li>
                
                <li><a href="../which-ux-research-methods/index.php">When to Use Which User-Experience Research Methods</a></li>
                
                <li><a href="../response-times-3-important-limits/index.php">Response Times: The 3 Important Limits</a></li>
                
                <li><a href="../why-you-only-need-to-test-with-5-users/index.php">Why You Only Need to Test with 5 Users</a></li>
                
                <li><a href="../page-fold-manifesto/index.php">The Fold Manifesto: Why the Page Fold Still Matters</a></li>
                
            </ul>
          </section>
          
          

          
            <section class="hide-for-small-down">
              
            </section>
          

      </section>
      

      
        <div class="small-12 medium-9 columns content-body">
          




<article class="l-subsection alertbox-article-content article-detail-bl">
  
  <header>
    <div class="l-subsection">
      <h1>WebTV Usability Review</h1>
      <div class="author-meta">by
        
          
            
            <a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a>
            
          
        on  February 1, 1997
        
      </div>

      

      
      <div class="topic-links">
        Topics:
        
          <ul>

  <li><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction</a></li>

</ul>
        
      </div>
      
    </div>
  </header>
  

  
  
  

  
  <section id="article-body"><p>
	<img align="right" alt="WebTV remote control with specialized buttons for Web browsing, including 'back' and 'home'" height="629" src="http://media.nngroup.com/media/editor/alertbox/webtv_remote.jpg" width="128"/> WebTV achieves a very high level of usability given its design constraints. Unfortunately, the constraints are so severe that even this great design ultimately fails to provide an optimal Web user experience.</p>
<p>
	WebTV's usability engineers have done a good job at making it very easy to install and as easy as possible to use, and WebTV's imaging engineers have done an incredible job at high-quality character rendering in an NTSC video signal. In fact, the screenshots in this column do not look as good as WebTV does when displayed on a good television set: you have to see it to believe that it's possible to achieve WebTV's level of text readability on a television screen.</p>
<h2>
	Remote Control</h2>
<p>
	The remote control has great industrial design: the main buttons can be used while the user keeps his or her gaze directed at the TV screen. The four cursor key buttons surround the "action" button and can easily be used to move the area cursor around the screen to follow hyperlinks. Also, the scroll up/down buttons and the "back" button are easy to find without looking at the remote control. It actually feels very natural to sit in a reclined posture and move through online content with the WebTV remote as the only input device.</p>
<p>
	Unfortunately, the need for a simple remote control ultimately dooms WebTV as a highly usable Web experience. No matter how well designed, it is simply too awkward to use cursor keys to move around the screen instead of using a mouse or other direct pointing device. My best description of the experience of browsing the Web with cursor keys is that it feels very much like using DOS or one of the old IBM 3270 terminals. Instead of saying " <em> this </em> is where I want to point", the user has to say "OK, to get over here, I first do UP, UP, and then LEFT, LEFT". In other words, instead of being a single action, pointing turns into a <strong> sequence of actions </strong> that have to be planned and monitored with a much larger degree of cognitive load than when using a mouse.</p>
<p>
	Normally, the cursor keys jump the area cursor to the next selectable area of the screen, but when an imagemap is selected, they double as directional continuous-movement controls for a pointer. Using imagemaps is particularly cumbersome since doing so requires the user to first move the area cursor to the graphic, then select the graphic with the action button, then manipulate the <code> x-y </code> position of the pointer within the image with the cursor keys, and finally push the action key a second time. Compare this sequence of steps with simply moving a mouse to the desired spot and clicking the mouse button in essentially a single step.</p>
<p>
	When viewing three-column layouts it is often hard to position the area cursor in the middle column, because the left or right buttons jump the cursor to the selectable area closest to the current <code> y </code> position. Thus, the cursor often jumps directly from column 1 to column 3 or from column 3 to column 1.</p>
<p>
	The remote control has a set of scroll-buttons in addition to the cursor-movement buttons. Since the cursor-buttons also scroll the screen when used to move below the last selectable item or above the first selectable item, and the scroll-buttons cause new items to be selected (the first item on the new screenful is automatically selected), users often confuse the two sets of buttons. Even after many hours of WebTV usage, I found myself sometimes pressing the wrong button. Since the button-design for the remote control is an obvious candidate for intensive usability testing, I am sure that WebTV considered this problem and has good reasons for having both sets of buttons on the remote, but I would certainly consider simplifying within-page navigation to a single set of buttons.</p>
<h3>
	Keyboard</h3>
<p>
	The wireless keyboard is supposedly a $60 option, but the system is almost useless without it: email obviously requires a keyboard, and mail is still the true killer app of the Internet. There is an on-screen keyboard that can be operated by the remote control, but it is extremely painful to tap out even short URLs by moving cursor keys up-right-left-down. On-screen keyboards work reasonably well on devices like the Newton that have a direct pointing device, but with cursor keys: forget it! The on-screen keyboard can be configured to display the characters alphabetically (ABCDEF etc.) or as a traditional typewriter keyboard (QWERTY etc. - presumably other configurations will be available in international versions). The default setting is alphabetical layout which only works for people who have never used a typewriter; of course, there are probably many such users in the WebTV target market.</p>
<p>
	The wireless keyboard is very well designed as a true laptop device. You can sit in your couch and type while resting the keyboard in your lap. The form factor, weight, and typability are all well done, but the key layout is very difficult to learn. There are special function keys for the most common WebTV commands: the basic keys from the remote control (up-down, back, home, etc.) are replicated and keys are added for functions that otherwise need indirect access through the Options command (for example, GoTo and Find). Unfortunately, all these function keys are really tiny and arranged in a seemingly random fashion around the periphery of the keyboard. It is impossible to learn where one finds which keys, so the user must perform a visual scan of the function keys every time they are used. This visual scan is very difficult given that the keys are small (i.e., tiny labels) and that the keyboard is typically in the user's lap (i.e., far away from the eyes) and in poor lighting. Make a more navigable keyboard for Version 2, please.</p>
<p>
	Typing is only possible when the keyboard focus has been explicitly assigned to a type-in field. Unless the field is the first element on the screen, this means that the user has to manually use the cursor keys to move the focus to the type-in field before typing. These extra actions should be eliminated, especially given the awkward nature of using cursor keys instead of a mouse to move around on the screen. Unless the user has explicitly moved the keyboard focus to a specific field, typing should be directed to the first type-in field on the screen as soon as the user sends character input from the keyboard. This lack of a default keyboard focus is one of the relatively few areas I have found where the interaction is insufficiently polished. Quite often, shortcuts and appropriate defaults are designed into the system to reduce the number of user actions needed to achieve a given result. Doing so is a good idea in most user interfaces but is critical for a system with indirect or hard-to-use interaction techniques (e.g., voice recognition systems, telephone-operated interfaces, and WebTV).</p>
<h2>
	Learnability</h2>
<p>
	WebTV is the first major attempt at building an <a href="http://www.amazon.com/exec/obidos/ISBN=0262140659/useitcomusableinA/" title="Don Norman's book 'The Invisible Computer' about information appliances"> information appliance</a> and it has definitely shown the promise of non-general purpose computers when it comes to ease of installation. In fact, the only difficulty in installing WebTV comes when connecting it to the TV through the VCR. Getting the cables and all the VCR and TV settings just right to pass the WebTV signal through to the screen is often fairly complicated. Pundits often say that consumer electronics companies know how to build products for naive users, but the people who built my VCR could definitely learn something from WebTV when it comes to ease of use.</p>
<p>
	Interestingly, many of the usability problems I have observed in informal studies of a few WebTV users turned out to hurt experienced users the most. WebTV is using its own vocabulary, which is probably a better match with novice users' expectations but causes problems for experienced users. For example, when defining a new user account, the user is asked to type the user's "Internet name". One user I tested thought that he was supposed to give his email address, so he entered his current address. In fact, the system wants to create a new userid for the user on WebTV's system, and this would have been clearer for an experienced user if it had said so. For novice users (who are presumably WebTV's design center), I do agree that "Internet name" is a better term than "userid". A further problem for my experienced user was that he could not enter the userid he wanted. He has a quite distinct userid on other email systems and naturally wanted the same on WebTV, but it was taken already. The same was true for the first several variations he tried of his name. As this example shows, naming is starting to be a problem on mass services (of course, on AOL most people get names like <code> John537 </code> since they are even further along the curve) and we will need to invent better ways of creating acceptable names.</p>
<p>
	<img align="right" alt="WebTV connecting to Washington Post server" height="287" src="http://media.nngroup.com/media/editor/alertbox/webtv_connecting.jpg" width="380"/> Other terminology differences include using <strong> publisher </strong> instead of <strong> site </strong> (e.g., a status message says "Contacting Publisher" instead of "looking for site" when connecting to a new site) and real error messages (e.g., <em> The publisher couldn't find the requested page </em> instead of <strong> 404</strong>). This non-standard vocabulary no doubt tests better with novice users but will make it more difficult for WebTV users to communicate with users of other devices.</p>
<p>
	While a page is being downloaded, a <strong> single </strong> progress bar is displayed (strange that I have to praise WebTV for having a useful progress bar, but most other Web browsers can't seem to get this basic UI element right). To further increase usability, the user is informed of the name of the "publisher" as soon as possible: the text <em> Contacting Publisher </em> is replaced with the site name as extracted from the page &lt;TITLE&gt;. Unfortunately, WebTV can't display very many characters in the progress dialog (since a large screen font is necessary for readability on a TV), so it is common for the publisher name to be given as "Welcome to Washi..." instead of "Washington Post". WebTV ought to make their server chop off any leading "Welcome to" prefix from page titles. Actually, as mentioned in my discussion of <a href="../marginalia-of-web-design/index.php"> proper &lt;TITLE&gt; design</a>, it is bad form to use <em> Welcome to </em> titles in the first place!</p>
<p>
	<img align="left" alt="WebTV email icon when no new mail is waiting" height="75" src="http://media.nngroup.com/media/editor/alertbox/webtv_mailbox.jpg" width="60"/> The email icon several times made me think that I had new messages waiting when in fact there were none. My problem was that the flag is up on the mailbox even when there is no mail (new mail is indicated by the appearance of some envelopes in the mailbox); a raised mailbox flag was used by some early Unix mail programs as the indicator of new mail. New users may not have this problem, but it may be worth revising the mail icon in an upcoming release. Also, this very American mailbox will definitely need to be replaced with another icon for international versions of the system.</p>
<h3>
	Progressive Disclosure</h3>
<p>
	WebTV correctly follows the principle of progressive disclosure to move complex and less frequently used options out of the main user interface and into secondary screens. The problem from the experienced Web user's perspective is that the definition of "complex options" includes some features that are more or less second nature to the experienced user. For example, I observed one experienced user become very frustrated when she couldn't find the URL of a page she was viewing. After many failed attempts she gave up and assumed that WebTV didn't support URLs. Only after I broke a basic rule of user testing and assured her that the feature was indeed available did she persist and eventually found the URL under the "Info" command in the Options menu.</p>
<p>
	Similarly, experienced users have also had problems finding the way to type in a URL to go to a specific page. The underlying issue is that WebTV (correctly) decided that URLs have low usability and therefore wanted to hide them from their novice users. I completely agree with the desire to simplify the user interface and hide complex features. The only problem is that the Web is currently so hard to navigate that many experienced users have learned to rely on the URLs to help them understand what is going on. In particular, the lack of URL information increases the experienced user's feeling of being lost and of viewing the Web through a very small peephole. The ideal solution would not rely on URLs but would employ other means of visualizing the structure of the information space and the user's current location. In other words, we need some combination of local and regional sitemaps with path representation and a global overview of the Web (distorted to emphasize areas of interest to the user). Unfortunately, these navigational aids (which we will hopefully see in a year or two) require a larger screen area than could reasonably be dedicated on a TV monitor. Whether it will be possible to use a combination of overlays and transparency effects with some form of rapid screen multiplexing to present some of this information to TV users remains to be seen.</p>
<p>
	With respect to feature elimination, I would have removed the user preference to sort the email list with either newest or oldest messages first. WebTV's email interface only works for people who get a <em> very </em> small number of messages, so they should not need to sort them. One cannot accuse WebTV of feature creep, but maybe this one feature did creep in from traditional power-user design.</p>
<h2>
	Screen Size</h2>
<p>
	WebTV has a small screen that shows a very limited amount of information compared with a traditional computer screen. This is particularly true given the need for WebTV to use large fonts because of the poor display quality of NTSC televisions and the typical viewing distance between the TV set and the user's couch.</p>
<center>
	<p>
		<img alt="Screenshot of a Web page on a workstation-class computer screen with a red box indicating the portion viewable on a WebTV screen" height="294" src="http://media.nngroup.com/media/editor/alertbox/webtv_viewport.jpg" width="239"/>   <img alt="Screenshot of the same Web page displayed on WebTV" height="286" src="http://media.nngroup.com/media/editor/alertbox/webtv_alertbox1.jpg" width="380"/><br/>
		<em><a href="../relationships-on-the-web/index.php">A sample Web page</a> as displayed on a workstation-class computer monitor (reduced to 35 percent of normal size) and on WebTV (reduced to 67 percent of normal size).<br/>
		The red box on the computer display marks the part of the page that is visible on WebTV's screen. </em></p>
</center>
<p>
	Three problems stem from WebTV's small screen size:</p>
<ul>
	<li>
		Excessive scrolling is needed to move about the page, meaning that the user will often not see the entire page because of the extra effort. Scrolling is known to cause problems even on normal computer screens, and it is likely that WebTV users will have even worse problems since they will need to scroll more.</li>
	<li>
		Users often get lost within a single page: there is no way of knowing how far one has scrolled down the page or what other information is on the page. In particular, it is almost impossible to get an idea of the structure of a page or to predict what else one might see further down the page. These problems also exist in large-screen browsers, but because more content can be seen at any one time and because less effort is needed to scroll about the page, the usability consequences are much less severe.</li>
	<li>
		Once the user has scrolled down a page, it is a lot of work to get back to the top of the page. Essentially the only way to do so is to hold down the "scroll up" button until the system has scrolled all the way to the top, one screen at a time. There is a "scroll to top" command, but it is hidden away under the Find command and requires 6-8 button presses (depending on the prior command) on 4-5 different buttons (too many) to activate. It would be better if the system would recognize a "long" press on the "scroll up" button as a command to go all the way to the top in a single fast leap.</li>
</ul>
<h2>
	Navigation</h2>
<p>
	WebTV is the first Web user interface for which I have discovered a serious need for navigational aids within the page. See the sidebar for some of my ideas for <a href="../ideas-for-improved-within-page-navigation/index.php"> improved within-page navigation</a>.</p>
<p>
	Except for within-page navigation, WebTV is not much different from other Web browsers with respect to navigation. The basic problem is lack of navigation support for an understanding of larger structures, but it would not be fair to criticize WebTV for lacking features that nobody else has at this time. WebTV actually improves on the history lists in other browsers by using a "recent" screen that shows recently seen pages in miniature. Often, it is easy to pick out the relevant page from the recent screen and backtrack many steps in a single action.</p>
<p>
	A satisfying <strong> "thud" </strong> sound is played if the user tries to scroll past the bottom of the screen. In general, WebTV has instructive and pleasing sound effects as an integrated part of the user interface in great contrast with the impoverished audio in most computer user interfaces (except games).</p>
<h3>
	Bookmarks</h3>
<p>
	On the "Favorites" page, bookmarked sites are shown as labeled miniatures. This is a good way of showing a small number of bookmarks and allows the user to quickly discriminate the options. The presentation of the favorites would improve if sites starting to use WebTV's convention for specifying a logo image to replace the miniature with an appropriate logo (the LOGO attribute of the BODY tag). By the way, small logo glyphs might also improve the scannability of bookmarks and favorites menus in traditional, computer-based browsers. In principle, I would prefer to have logo-specifications be part of a site management system instead of being embedded within the BODY tags of every page: we need extensions to HTTP to allow browsers and proxies to retrieve site-level information (like logos and sitemaps).</p>
<p>
	<img align="right" alt="WebTV uses miniature homepage to visualize bookmarks" height="287" src="http://media.nngroup.com/media/editor/alertbox/webtv_favorites.jpg" width="380"/> Sometimes, bookmarking a page results in a black square instead of a miniature. This seems to happen when the server has not finished downloading all the graphics on the bookmarked page but feels like a bug to me: it would often be better to produce a miniature of the parts of the page that <em> did </em> get downloaded. A worse usability problem is that the labels show the full &lt;TITLE&gt; of the bookmarked page, even when the text is very long and takes up most of the screen in the WebTV layout. I would have preferred a solution that showed only the first two lines or so of the &lt;TITLE&gt; and then popped up the remaining text when the user places the area cursor over the bookmark.</p>
<h3>
	Find Within a Page</h3>
<p>
	WebTV provides a Find command to locate a text string within the current page. Since we know from studies of traditional browsers that users frequently confuse Search (whether site-wide or Web-wide) and Find, I am sure that Find will cause problems for WebTV users too. Even so, I agree with the decision to include this feature because of the reduced text readability and the problems with understanding the structure of any given page because of the small viewport. Since the user cannot easily go to the part of a page that covers a given topic of interest, the Find feature turns out to be a very handy within-page navigation aid.</p>
<p>
	If the user tries to Find a string that is not on the page, a reasonably good error message is displayed ("Could not find the word on this page"). When Find is subsequently activated on another page, WebTV correctly prepopulates the search field with the previous search string (good defaults always save users time but are especially critical for interaction widgets that are as cumbersome to operate as WebTV's). Interestingly, the Find area still displays the error message saying that it could not find the word. The first few times this happened, I assumed that WebTV was fast enough to have searched the new page for my Find string while it animated the appearance of the Find area (a reasonable assumption for the performance of a 112-MHz, 64-bit RISC processor searching a few kilobytes of text for an exact match). Because of this conclusion, I immediately closed the Find dialog without ever searching. As it turns out, WebTV had <em> not </em> searched the subsequent pages and the error message was a bug. My preference for a redesign would be for WebTV to make my erroneous assumption come true and search the page for the previous search string while the Find dialog is animated, since doing so would save users having to explicitly activating a search for a string that is not found. Again, manipulating interaction widgets with the remote control is much more awkward than using a mouse, so anything that minimizes user operations should be done.</p>
<h2>
	Branding</h2>
<p>
	<img align="right" alt="WebTV logo" height="56" src="http://media.nngroup.com/media/editor/alertbox/webtv-logo.gif" width="75"/> Despite Sony's best efforts at putting their logo on the remote control, there is no doubt that the user experience has a big fat WebTV brand on it. The "home page" icon on most navigation screens says WebTV, as do many parts of the user interface. Most important, most of the top-level content accessed by the user carries a WebTV logo.</p>
<p>
	My conclusion is that WebTV is one of the first cases where the <em> Content-is-King </em> -principle has been used to overrule the brand of the physical device in the users' hands and turn the content into the main brand. In contrast, if you are using a Sun workstation to browse Microsoft's website, you still feel like a Sun customer, and conversely if you are using a Mac to browse Sun's website, you feel like an Apple customer.</p>
<h2>
	Designing for WebTV</h2>
<p>
	My main recommendation is <strong> not </strong> to design explicitly for WebTV unless you are creating a <a href="../tv-meets-the-web/index.php"> service dedicated to television users</a>. The power of the Web is <a href="../trends-for-the-web-in-1997/index.php"> cross-platform design</a> and the ability to have your user interface reside on a server under your control from which it can be projected to any customer in the world no matter what local equipment they have. This property is destroyed if you start to design for specific hardware. What should happen is that WebTV get with it and includes stylesheet support in Version 2: once that happens, you can link all your pages to a special stylesheet that is optimized for television in addition to stylesheets for traditional computers, PDAs, etc.</p>
<p>
	If you are designing a dedicated service or if you want to set up your server to detect the user agent and provide different pages to different browsers, then WebTV's own styleguide is certainly a start. <cite> Web Review </cite> 's feature story on <a href="http://www.webreview.com/1997/01_17/designers/01_17_97_1.shtml"> designing for WebTV</a> is another source.</p>
<p>
	<img align="right" alt="The three-column layout from news.com looks bad on WebTV" height="285" src="http://media.nngroup.com/media/editor/alertbox/webtv_threecolumns.jpg" width="380"/> If you want to design for the Web as a cross-platform information system but want to be considerate of WebTV users, then follow these guidelines:</p>
<ul>
	<li>
		Do not use large images (more than 544 pixels wide or 376 pixels tall) unless they will still work after WebTV's automatic rescaling to the available viewport area</li>
	<li>
		Reserve use of imagemaps for the absolutely necessary cases (e.g., for selecting a location on a map) and make navigation bars and other sets of buttons into discrete images</li>
	<li>
		Do not include text in your images since the characters will be very difficult to read on the WebTV screen</li>
	<li>
		If you <em> need </em> to include text in an image, use 16 point bold Helvetica or larger (or an equally readable font) - the 16 point guideline assumes that the image is small enough that it will not be rescaled</li>
	<li>
		Do not use a multi-column layout. The figure shows how poorly the 3-column layout from <a href="http://www.news.com/"> news.com</a> works on WebTV (even though it is one of my favorite designs on a regular computer screen)</li>
	<li>
		If your design requires columns anyway, use at most two columns and make sure that they will work on a 544-pixels-wide screen</li>
	<li>
		If you have a page that will scroll over more than four WebTV screenfuls, then repeat any navigation bar at the bottom of the page. I dislike this advice since extra interaction options increase the burden on the "normal" user to understand the page, but if your log files show more than, say, 5 percent WebTV users, then remember that the current WebTV design makes it hard to scroll back to the top of the page.</li>
	<li>
		Split your information into a rich hypertext space with a larger number of smaller nodes (OK, I didn't follow this advice for this column which is painful to read on WebTV)</li>
	<li>
		If possible, make each unit small enough to fit on a single WebTV screen to eliminate scrolling (interestingly, this advice is a return to the <a href="../two-basic-hypertext-presentation-models/index.php"> "card shark" model of hypertext</a> as exemplified by HyperCard in 1987: ultimately we will need to evolve a new form of HTML to define pop-ups, overlays, partial fades, and the other interaction options that enhance card-based hypertext).</li>
	<li>
		<a href="../be-succinct-writing-for-the-web/index.php">Brevity</a>: write less text</li>
</ul>
<table border="1" class="updatecomment" frame="void" rules="rows" width="100%">
	<tbody>
		<tr>
			<td>
				<strong>Note added mid-February 1997: </strong> At the Milia'97 conference in France, I saw a demo of the French <a href="http://www.nytimes.com/library/cyber/euro/021197euro.php"> NetBox</a> system: very similar to WebTV but clearly less well designed. NetBox has a fairly geeky user interface that approximates the experience of using a traditional browser on a traditional desktop computer; simply not appropriate for a TV-based device and its new class of users.
				<p>
					In other Milia news, WebTV demoed a PAL version (European video standard) which will have slightly higher resolution than the American version. Interesting hints (but no firm announcements) were given of ways to bypass the high local access charges of European telephone companies. After the demo I got the opportunity to ask WebTV's Steve Perlman about their support of stylesheets and he assured the audience that stylesheets will be in an upcoming release.</p>
				<p>
					<strong>Note added January 1999: </strong> If you don't have a WebTV, you can now <a href="http://webtv.com/" title="WebTV Developer Support"> download a tool for your PC to view your site</a> the way it will look to WebTV users. The viewer even shows how the site looks in both PAL (Europe) and NTSC (U.S. and Japan).</p>
			</td>
		</tr>
	</tbody>
</table>
<p>
	 </p>
</section>
  

  
  <section>
    
        
    
  </section>
  
  
  
  
  

  


  <section>
    <br />
    <p class="share"><b>Share <span class="hide-for-small-down">this article</span>:</b> <a href="https://twitter.com/intent/tweet?url=http://www.nngroup.com/articles/webtv-usability-review/&amp;text=WebTV%20Usability%20Review&amp;via=nngroup">Twitter</a> | <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=http://www.nngroup.com/articles/webtv-usability-review/&amp;title=WebTV%20Usability%20Review&amp;source=Nielsen%20Norman%20Group">LinkedIn</a> | <a href="https://plus.google.com/share?url=http://www.nngroup.com/articles/webtv-usability-review/">Google+</a> | <a href="mailto:?subject=NN/g Article: WebTV Usability Review&amp;body=http://www.nngroup.com/articles/webtv-usability-review/">Email</a></p>
    <hr />
  </section>

  <footer class="l-subsection">
    
    
    <div class="learn-more">
      <h2>Learn More</h2>
      <p><a href="../subscribe/index.php">Subscribe to the weekly newsletter</a> to get notified about future articles.</p>
      </div>
    
    

    <div class="row show-for-small-down related-content">
      
      
      

      
      
        <div class="column small-12 topic-links">All articles about:
          <ul>
            
            <li><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction</a></li>
            
            <li><a href="../../topic/technology/index.php">new technologies</a></li>
            
          </ul>
        </div>
      
      

      
      
        <div class="column small-12">
          <h3>UX Conference Training Courses</h3>
           <ul class="no-bullet">
    
        <li><a href="../../courses/ux-basic-training/index.php">UX Basic Training</a></li>
    
        <li><a href="../../courses/hci/index.php">User Interface Principles Every Designer Must Know</a></li>
    
        <li><a href="../../courses/emerging-patterns-web-design/index.php">Emerging Patterns for Web Design</a></li>
    
        <li><a href="../../courses/interaction-design-3-day-course/index.php">Interaction Design: 3-Day Course</a></li>
    
</ul>
        </div>
      
      

      
      
      

      
      
        <div class="small-12 column">
          <h3>Online Seminars</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../online-seminars/beyond-screens/index.php">Beyond Screens</a></li>
              
            
          </ul>
        </div>
      
      
    </div>

    <div class="hide-for-small-down row related-content">
      <div class="medium-6 columns">
        
        
          <div>
            <h3>Articles</h3>
            <ul class="no-bullet">
              
                
                <li><a href="../embarrassment/index.php">Computer-Assisted Embarrassment</a></li>
                
              
                
                <li><a href="../enhancement/index.php">The Role of Enhancement in Web Design</a></li>
                
              
                
                <li><a href="../voice-interaction-ux/index.php">Voice Interaction UX: Brave New World...Same Old Story</a></li>
                
              
                
                <li><a href="../human-body-touch-input/index.php">The Human Body as Touchscreen Replacement</a></li>
                
              
                
                <li><a href="../kinect-gestural-ui-first-impressions/index.php">Kinect Gestural UI: First Impressions</a></li>
                
              
            </ul>
          </div>
        
        

        

      </div>
      <div class="medium-6 columns">
        
          
          <div>
            <h3>UX Conference Training Courses</h3>
             <ul class="no-bullet">
    
        <li><a href="../../courses/ux-basic-training/index.php">UX Basic Training</a></li>
    
        <li><a href="../../courses/hci/index.php">User Interface Principles Every Designer Must Know</a></li>
    
        <li><a href="../../courses/emerging-patterns-web-design/index.php">Emerging Patterns for Web Design</a></li>
    
        <li><a href="../../courses/interaction-design-3-day-course/index.php">Interaction Design: 3-Day Course</a></li>
    
</ul>
          </div>
          
        
        
          
          <div>
            <h3>Online Seminars</h3>
            <ul class="no-bullet">
            
                <li><a href="../../online-seminars/beyond-screens/index.php">Beyond Screens</a></li>
            
            </ul>
          </div>
          
        
      </div>
    </div>
  </footer>
</article>

        </div>

      

    
  </div>
  <div class="row">
    
      <div class="column small-12 show-for-small-down l-navrail announcements">
        
          
        
      </div>
    
    
  </div>

    </div>
    




<footer id="page-footer">
    <div class="row hide-for-print">
        <div class="column small-12 medium-6">
            <section class="footer-popular">
                
                <h1>Popular Topics</h1>
                <div style="float: left;  width: 15em; display: block;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/applications/index.php">Application Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/e-commerce/index.php">E-Commerce Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/eyetracking/index.php">Eyetracking </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/information-architecture/index.php">Information Architecture </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/intranets/index.php">Intranet Design </a></li>
	<li><a href="../../topic/interaction-design/index.php">Interaction Design </a></li>
</ul>
</div>

<div style="float: left;  width: 15em; display: block; list-style: none;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/mobile-and-tablet-design/index.php">Mobile and Tablet Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/credibility-persuasion/index.php">Persuasive Design</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/research-methods/index.php">Research Methods</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/user-testing/index.php">User Testing </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/web-usability/index.php">Web Usability </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/writing-web/index.php">Web Writing &amp; Content Strategy </a></li>
</ul>
</div>

<p>&nbsp;</p>

                
            </section>
        </div>

        <div class="column small-12 medium-6">
            <div class="row">
                <div class="column small-6">
                    <section class="footer-about">
                        <h1>Follow Us</h1>
                        <ul>
	<li><a href="../subscribe/index.php">Subscribe to our weekly UX newsletter</a>,&nbsp;Jakob Nielsen&#39;s Alertbox</li>
	<li>Visit Don Norman&#39;s website, <a href="http://www.jnd.org/"> www.jnd.org </a></li>
	<li>Visit Bruce Tognazzini&#39;s website, <a href="http://www.asktog.com/"> www.asktog.com </a></li>
	<li style="padding-top: 6px;"><a class="twitter-follow-button" data-show-count="false" data-size="large" href="https://twitter.com/nngroup"><img alt="Follow @nngroup on Twitter" height="20" src="https://media.nngroup.com/media/editor/2016/12/30/twitter-follow-button.png" style="width: 117px; height: 20px;" width="117" /></a></li>
	<li style="padding-top: 6px;"><img alt="" height="14" src="https://media.nngroup.com/media/editor/2016/07/26/feed-icon-14x14.png" style="vertical-align:text-top;width:14px;margin-right:.25em;" width="14" /><a href="../../feed/rss/index.php">Subscribe to our RSS feed</a></li>
</ul>

                    </section>
                </div>

                <div class="column small-6">
                    <section class="footer-about right-column">
                        <h1>About
</h1>
                        <ul class="no-bullet">
	<li><a href="../../about/contact/index.php">Contact information</a></li>
	<li><a href="../../about/index.php">About Us</a></li>
	<li><a href="../../about/why-nng/index.php">Why NN/g? </a></li>
	<li><a href="../../return-policy/index.php">Return Policy</a></li>
</ul>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="copyright column small-12">
            <p><a href="../../copyright-and-reprint-info/index.php">Copyright</a> &copy; 1998-2017 Nielsen Norman Group, All Rights Reserved.</p>
        </div>
    </div>
</footer>


    


        <script src="https://media.nngroup.com/static/vendor/fastclick/js/fastclick.min.js"></script>
        <script src="https://media.nngroup.com/static/vendor/foundation/js/foundation.min.js"></script>
        <script src="http://media.nngroup.com/static/js/app.js"></script>
        <script src="http://media.nngroup.com/static/js/scroll-to-fixed.js"></script><script>var externalLinks = document.querySelectorAll('a[href^="http"]');for (var i = externalLinks.length-1; i >= 0; i--) {externalLinks[i].addEventListener("click", function(event) { alert("External links are disabled for this test."); event.preventDefault(); }, false);}var mailLinks = document.querySelectorAll('a[href^="mailto"]');for (var i = mailLinks.length-1; i >= 0; i--) {mailLinks[i].addEventListener("click", function(event) { alert("Email links are disabled for this test."); event.preventDefault(); }, false);}var externalForms = document.querySelectorAll('form');for (var i = externalForms.length-1; i >= 0; i--) {externalForms[i].addEventListener("submit", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false); externalForms[i].addEventListener("click", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false);}</script>

        <script>
            
            $(function() {
                $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
                $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
            });
        </script>

        
<script src="https://media.nngroup.com/static/js/articles.js"></script>


        
        <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1072699960;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072699960/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
    </body>

<!-- Mirrored from www.nngroup.com/articles/webtv-usability-review/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:14:35 GMT -->
</html>
