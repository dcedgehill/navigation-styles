<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->





    
<!-- Mirrored from www.nngroup.com/articles/usability-of-websites-for-teenagers/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:13:13 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1026.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"licenseKey":"1a30ef9931","errorBeacon":"bam.nr-data.net","applicationID":"2048992","transactionName":"NFxSMRBUX0EDUkIPDA0WdhAMVkVbDV8ZBxEXUFMJB0YfRAtUQRVZIktEDAFZVHYHRVcPD01eVRE=","beacon":"bam.nr-data.net","queueTime":2,"applicationTime":448,"agent":""}</script>
        <title>Teenage Web UX: Designing Teen-Targeted Sites</title><meta property="og:title" content="Teenage Web UX: Designing Teen-Targeted Sites" />
  
        
        <meta name="description" content="Teens are (over)confident in their web abilities, but they perform worse than adults. Lower reading levels, impatience, and undeveloped research skills reduce teens&#39; task success and require simpler sites.">
        <meta property="og:description" content="Teens are (over)confident in their web abilities, but they perform worse than adults. Lower reading levels, impatience, and undeveloped research skills reduce teens&#39; task success and require simpler sites." />
        
  
        
	
        
        <meta name="keywords" content="teenagers, teenage usability, teens, age-specific web design, young users, research skills, skills, reading, research, impatience, patience">
        

        

        <meta name="robots" content="noarchive">
        <meta name="viewport" content="width=device-width">

        <link rel="search" type="application/opensearchdescription+xml" title="NN/g Search" href="../../opensearch.xml" />
        <link rel="shortcut icon" href="http://media.nngroup.com/static/img/favicon.ico" />

        <link rel="stylesheet" href="https://media.nngroup.com/static/vendor/font-awesome/css/font-awesome.min.css">

        
        <link rel="stylesheet" href="https://media.nngroup.com/static/css/7232a960f1df.css" type="text/css" />
        <link rel="stylesheet" href="http://fast.fonts.com/t/1.css?apiType=css&amp;projectid=c5846260-9def-46ec-9407-0f1349bd2d64" type="text/css">

        <script src="https://media.nngroup.com/static/vendor/modernizr/js/modernizr.min.js"></script>

        <script type="text/javascript" src="https://media.nngroup.com/static/js/3283be5def74.js"></script>
        

        

        
        
        <!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=229034,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('http://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->


<!-- Google Analytics Tracking Code for http://nngroup.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23132681-1', 'nngroup.com');
  ga('require', 'linkid', 'https://www.nngroup.com/articles/usability-of-websites-for-teenagers/linkid.js');
  ga('require', 'displayfeatures');

// Optimizely Universal Analytics Integration
 window.optimizely = window.optimizely || [];
 window.optimizely.push("activateUniversalAnalytics");

  ga('send', 'pageview');

</script>
<script>
/**
* Function to track a click on an outbound link in Google Analytics.
*/
function trackOutboundLink(label, url) {
    if(window.ga && ga.loaded) {
        ga('send', 'event', 'outbound', 'click', label, {'hitCallback': function(){document.location = url;}
        });
    } else {
        document.location = url;
    }
}
/**
* Function to track a click on a link in the article announcement area.
*/
$('#articleAnnouncement a').on('click', function(){
  ga('send', 'event', 'link', 'click', 'articleAnnouncement');
});
</script>

<script type="text/javascript">
 setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2421.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



<!-- Hotjar Tracking Code for http://nngroup.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:121605,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

        <script src="https://media.nngroup.com/static/js/analytics/track_outbound_link.js"></script>
        
        
        
    </head>
    <body class="location-articles articles-detail">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
    <header id="page-header">
        <div id="cart" class="shopping-cart mobile-cart show-for-small-down"></div>
        <div class="row">
            <div id="masthead" class="masthead small-12 medium-7 column">

                <!-- The button to toggles menu on small screen -->

                <h1><a href="../../index.php"><img src="http://media.nngroup.com/static/img/logo-wordmark.png" width="74" height="30"> Nielsen <span class="norman">Norman</span> Group</a></h1>
                <p class="tagline show-for-medium-up">Evidence-Based User Experience Research, Training, and Consulting</p>

                <!-- The button to toggles form search on small screen -->


                <div style="clear: right;width:  100%;height: 1px;"></div><?php define('ROOT_PATH',$_SERVER["DOCUMENT_ROOT"] . '/apps/navigation_styles/navigations/1'); ?><?php include(ROOT_PATH)."/navigation.php"; ?>

                <!-- Drop down menu on small screen -->
                <div id="hideNav" class="show-for-small-down">
                  
                  <ul class="menu-content">
                    <li><a href="../../index.php">Home</a></li>
                    <li class="has-submenu">
                      <a>Training</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../training/index.php">Overview</a></li>
                        
<li id="nav-ux-conference"><a href="../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../online-seminars/index.php"><span>Online Seminars</span></a></li>

                      </ul>
                    </li>
                    <li class="has-submenu">
                      <a>Consulting</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
  <li><a href="../../consulting/index.php">Overview</a></li>
  
  <li><a href="../../consulting/benchmarking/index.php">Benchmarking</a></li>
  
  <li><a href="../../consulting/expert-design-review/index.php">Expert Design Review</a></li>
  
  <li><a href="../../consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
  
  <li><a href="../../consulting/usability-testing/index.php">Usability Testing </a></li>
  
  <li><a href="../../consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
  
  <li><a href="../../in-house-training/index.php">In-House Training</a></li>
</ul>

                    </li>

                    <li><a href="../../reports/index.php">Reports</a></li>
                    <li><a href="../index.php">Articles</a></li>
                    <li class="has-submenu"><a>About NN/G</a><i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../about/index.php">Overview</a></li>
                        <li><a href="../../people/index.php">People</a></li>
                        <li><a href="../../about/why-nng/index.php">Why NN/g?</a></li>
                        <li><a href="../../about/contact/index.php">Contact</a></li>
                        <li><a href="../../news/index.php">News</a></li>
                        <li><a href="../../about/history/index.php">History</a></li>
                        <li><a href="../../books/index.php">Books</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                </div>

                <hr class="header-divider show-for-small-only"/>


            </div>
       </div>

        <div id="hideNav">            <nav class="primary-nav small-12 column">
                <ul>
                    <li><a class="navitem" id="nav-home" href="../../index.php">Home</a></li>
                    <li><a class="navitem" id="nav-training" href="../../training/index.php">Training</a></li>
                    <li><a class="navitem" id="nav-consulting" href="../../consulting/index.php">Consulting</a></li>
                    <li><a class="navitem" id="nav-reports" href="../../reports/index.php">Reports</a></li>
                    <li><a class="navitem" id="nav-articles" href="../index.php">Articles</a></li>
                    <li><a class="navitem" id="nav-about" href="../../about/index.php">About NN/G</a></li>
                </ul>
            </nav>
            <nav class="secondary-nav small-12 medium-12 large-12 columns">
                
                
            </nav>
        </div>
    </header>
    <div class="l-content">
    
  

  

  <div class="row">
    
      
      <section class="small-12 medium-3 columns collapsable-content l-navrail articles-left-column">
        <h1 class="show-for-small-down collapse-link mobile-browse-header">Browse by Topic and Author</h1>
          <div class="row collapsable">

            
            <div class="small-6 medium-12 columns">
              <section id="article-popular-topics" style="display: none;">
                
                <h1>Topics</h1>
                <ul id="article-popular-topics">
                    
                    <li>
                        
                        <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/intranets/all/index.php">Intranets</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/user-testing/all/index.php">User Testing</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                        
                    </li>
                    
                    <li><a href="#" id="article-see-all-topics"><strong>See all topics&hellip;</strong></a></li>
                </ul>
                
              </section>
              <section id="article-all-topics" style="display: none;">
                
                <h1>
                  <span class="show-for-medium-up">All Article Topics</span>
                  <span class="show-for-small-down">All Topics</span>
                  (<a href="#" id="article-hide-all-topics">hide</a>)
                </h1>
                <ul>
                  
                  <li>
                    
                    <a href="../../topic/accessibility/all/index.php">Accessibility</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/agile/all/index.php">Agile</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/analytics-and-metrics/all/index.php">Analytics &amp; Metrics</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/applications/all/index.php">Application Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/b2b-websites/all/index.php">B2B Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/behavior-patterns/all/index.php">Behavior Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/branding/all/index.php">Branding</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/content-strategy/all/index.php">Content Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/corporate-websites/all/index.php">Corporate Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/design-patterns/all/index.php">Design Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-design-process/all/index.php">Design Process</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/email/all/index.php">Email</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/eyetracking/all/index.php">Eyetracking</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/heuristic-evaluation/all/index.php">Heuristic Evaluation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/human-computer-interaction/all/index.php">Human Computer Interaction</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-ideation/all/index.php">Ideation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/information-architecture/all/index.php">Information Architecture</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/interaction-design/all/index.php">Interaction Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/international-users/all/index.php">International Users</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/intranets/all/index.php">Intranets</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-management/all/index.php">Management</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/navigation/all/index.php">Navigation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/non-profit-websites/all/index.php">Non-Profit Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/personas/all/index.php">Personas</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/persuasive-design/all/index.php">Persuasive Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/prototyping/all/index.php">Prototyping</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/psychology-and-ux/all/index.php">Psychology and UX</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/research-methods/all/index.php">Research Methods</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/search/all/index.php">Search</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/social-media/all/index.php">Social Media</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/strategy/all/index.php">Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/user-testing/all/index.php">User Testing</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-teams/all/index.php">UX Teams</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/visual-design/all/index.php">Visual Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/writing-web/all/index.php">Writing for the Web</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/young-users/all/index.php">Young Users</a>
                    
                  </li>
                  
                </ul>
                
              </section>
            </div>
            <script>
              /* Determine which topics list will be displayed: the full or the
               * partial.  We are inlining this close to the list itself to
               * prevent rendering delays on the client
               */
              var topic_list = $.cookie("article-topics");

              var popular_topics = $('#article-popular-topics');
              var all_topics = $('#article-all-topics');

              if(topic_list == 1) {
                  popular_topics.hide();
                  all_topics.show();
              } else {
                  popular_topics.show();
                  all_topics.hide();
              }
            </script>
            

            
            <div class="small-6 medium-12 columns">
              
              <section id="article-initial-authors">
                  <h1>Author</h1>
                  <ul>
                    
                    <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                    
                    <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                    
                    <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                    
                    <li><a href="#" onClick="$('#article-initial-authors').hide(); $('#article-all-authors').show();"><strong>See all authors&hellip;</strong></a></li>
                  </ul>
              </section>
              <section id="article-all-authors" style="display: none;">
                <h1>
                  All Authors
                  (<a href="#" id="article-hide-all-authors">hide</a>)
                </h1>
                <ul>
                  
                  <li><a href="../author/raluca-budiu/index.php">Raluca Budiu</a></li>
                  
                  <li><a href="../author/patty-caya/index.php">Patty Caya</a></li>
                  
                  <li><a href="../author/susan-farrell/index.php">Susan Farrell</a></li>
                  
                  <li><a href="../author/therese-fessenden/index.php">Therese Fessenden</a></li>
                  
                  <li><a href="../author/kim-flaherty/index.php">Kim Flaherty</a></li>
                  
                  <li><a href="../author/sarah-gibbons/index.php">Sarah Gibbons</a></li>
                  
                  <li><a href="../author/aurora-harley/index.php">Aurora Harley</a></li>
                  
                  <li><a href="../author/kate-kaplan/index.php">Kate Kaplan</a></li>
                  
                  <li><a href="../author/page-laubheimer/index.php">Page Laubheimer</a></li>
                  
                  <li><a href="../author/angela-li/index.php">Angie Li</a></li>
                  
                  <li><a href="../author/hoa-loranger/index.php">Hoa Loranger</a></li>
                  
                  <li><a href="../author/kate-meyer/index.php">Kate Meyer</a></li>
                  
                  <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                  
                  <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                  
                  <li><a href="../author/kara-pernice/index.php">Kara Pernice</a></li>
                  
                  <li><a href="../author/christian-rohrer/index.php">Christian Rohrer</a></li>
                  
                  <li><a href="../author/amy-schade/index.php">Amy Schade</a></li>
                  
                  <li><a href="../author/katie-sherwin/index.php">Katie Sherwin</a></li>
                  
                  <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                  
                  <li><a href="../author/kathryn-whitenton/index.php">Kathryn Whitenton</a></li>
                  
                </ul>
              </section>
              
            </div>
            
          </div>

          
          <section class="hide-for-small-down">
            
              
              <h1>Recent Articles</h1>
              
              <ul>
                  
                  <li><a href="../seamless-cross-channel/index.php">Seamlessness in the Omnichannel User Experience</a></li>
                  
                  <li><a href="../brand-intention-interpretation/index.php">Experience Design: Bridging Brand Intention and Brand Interpretation</a></li>
                  
                  <li><a href="../eyetracking-tasks-efficient-scanning/index.php">Scanning Patterns on the Web Are Optimized for the Current Task</a></li>
                  
                  <li><a href="../flat-design-best-practices/index.php">Flat-Design Best Practices</a></li>
                  
                  <li><a href="../ux-user-stories/index.php">Accounting for UX Work with User Stories in Agile Projects</a></li>
                  
                  <li><a href="../index.php"><strong>See all articles&hellip;</strong></a></li>
              </ul>
              
            
          </section>
          

          
          
          <section class="hide-for-small-down">
            <h1>Popular Articles</h1>
            
            <ul>
                
                <li><a href="../usability-101-introduction-to-usability/index.php">Usability 101: Introduction to Usability</a></li>
                
                <li><a href="../top-10-mistakes-web-design/index.php">Top 10 Mistakes in Web Design</a></li>
                
                <li><a href="../how-users-read-on-the-web/index.php">How Users Read on the Web</a></li>
                
                <li><a href="../f-shaped-pattern-reading-web-content/index.php">F-Shaped Pattern For Reading Web Content</a></li>
                
                <li><a href="../ten-usability-heuristics/index.php">10 Usability Heuristics for User Interface Design</a></li>
                
                <li><a href="../intranet-design-2016/index.php">10 Best Intranets of 2016</a></li>
                
                <li><a href="../which-ux-research-methods/index.php">When to Use Which User-Experience Research Methods</a></li>
                
                <li><a href="../response-times-3-important-limits/index.php">Response Times: The 3 Important Limits</a></li>
                
                <li><a href="../why-you-only-need-to-test-with-5-users/index.php">Why You Only Need to Test with 5 Users</a></li>
                
                <li><a href="../page-fold-manifesto/index.php">The Fold Manifesto: Why the Page Fold Still Matters</a></li>
                
            </ul>
          </section>
          
          

          
            <section class="hide-for-small-down">
              
            </section>
          

      </section>
      

      
        <div class="small-12 medium-9 columns content-body">
          




<article class="l-subsection alertbox-article-content article-detail-bl">
  
  <header>
    <div class="l-subsection">
      <h1>Teenage Usability: Designing Teen-Targeted Websites</h1>
      <div class="author-meta">by
        
          
            
            <a href="../author/hoa-loranger/index.php">Hoa Loranger</a>
            
            and
            
            <a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a>
            
          
        on  February 4, 2013
        
      </div>

      

      
      <div class="topic-links">
        Topics:
        
          <ul>

  <li><a href="../../topic/web-usability/index.php">Web Usability</a></li>

  <li><a href="../../topic/young-users/index.php">Young Users</a></li>

</ul>
        
      </div>
      
    </div>
  </header>
  

  
  
  <section>
    <p class="lede"><strong>Summary:</strong> Teens are (over)confident in their web abilities, but they perform worse than adults. Lower reading levels, impatience, and undeveloped research skills reduce teens’ task success and require simple, relatable sites.</p>
    <br />
  </section>
  
  

  
  <section id="article-body"><p>Teens are wired. Technology is so integrated with teenagers’ lives that creating useful and usable websites for them is more critical than ever. To succeed in a world where the next best thing is a click away and text message interruptions are the rule, not the exception, website creators must clearly understand what teens want and how to keep them on a site.</p>

<p>To understand the expectations of a generation that grew up with technology and the Internet, we conducted empirical usability studies with real teens to identify specific guidelines for how websites can be improved to match teenagers’ abilities and preferences.</p>

<p>Our research <strong> refutes many stereotypes</strong>, including that teens:</p>

<ul>
	<li>just want to be entertained online with graphics and multimedia,</li>
	<li>are supremely tech savvy,</li>
	<li>use smartphones for everything, and</li>
	<li>want everything to be social.</li>
</ul>

<p>Teens are not technowizards who surf the web with abandon. And they don’t like sites laden with glitzy, blinking graphics. Teens are often stereotyped as only wanting things that are bold and different. They’re also often viewed as being fearless about technology and constantly connected to some form of media. Although this might be partially true, it’s an oversimplification and letting this steer your design can lead to disastrous outcomes.</p>

<p>Teenagers use the Internet from many devices in various environments. For our research, we focused on web usability, mainly from desktop and laptop computers. We also looked at mobile website usability and how teenagers use mobile devices. Although teens spend endless time texting, Facebooking, etc., we didn’t focus on this because our goal was to derive design guidelines for mainstream websites, not to help build the next Facebook.</p>

<h2>The Research</h2>

<p>We derived <a href="../../reports/teenagers-on-the-web/index.php" title="Nielsen Norman Group report: Teenagers (Ages 13-17) on the Web"> 111 usability guidelines for engaging teens</a> and keeping them on your site. These recommendations are based on <strong> observational studies </strong> using multiple methodologies. A total of 84 users between the ages of 13 and 17 participated in two rounds of research: 38 teens in the original study (8 years ago), and 46 teens in our new study. We triangulated findings across three methods:</p>

<ul>
	<li><strong>Usability testing. </strong> We met with test participants one at a time and gave them tasks to perform, asking them to vocalize their thoughts as they attempted tasks. To keep the scenarios as authentic as possible, we matched the tasks with each teen’s actual interests and simulated real-world situations.</li>
	<li><strong>Field studies. </strong> We observed teenagers in their homes and at school. During these site visits, we didn't give users predefined tasks to perform, but simply watched as they used the web the way they normally would in these settings.</li>
	<li><strong>Interviews and focus groups. </strong> To gain further insight into their experiences and attitudes, we asked participants to offer stories and examples detailing how and when they use the web, and which sites they considered interesting and useful. We also solicited advice from teens on how to make websites appealing. Interviews were held before and after the other usability sessions, as well as during a focus group.</li>
</ul>

<p>We conducted these studies in the U.S. and Australia, in cities and towns ranging from affluent suburbs to disadvantaged urban areas. We tested a roughly equivalent number of boys and girls on a total of 152 websites that covered a broad range of genres, including:</p>

<ul>
	<li>School resources (California State University, BBC Schools, SparkNotes)</li>
	<li>Tourism/Arts &amp; Entertainment (ExploreChicago.com, Kidspace Children's Museum, Lonely Planet)</li>
	<li>Health (Australian Drug Foundation, KidsHealth, National Institute on Drug Abuse)</li>
	<li>Informational/Reference ( <em> Nature </em> , Food Network, <em> Scientific American</em>)</li>
	<li>News (CNN, Weather.com, BBC Teens, ChannelOne.com)</li>
	<li>Entertainment and Games (MTV, Playlist.com, Cool Math Games, Disney)</li>
	<li>E-commerce (American Eagle Outfitters, Apple, Volcom)</li>
	<li>Corporate sites (Pepsi-Cola, The Principal Financial Group, Procter &amp; Gamble, Samsung, Morton Salt)</li>
	<li>Government (Australian Government main portal, California’s Department of Motor Vehicles, the U.S. White House, NASA)</li>
	<li>Non-profits (Alzheimer’s Association, The Insite, World Food Programme, National Wildlife Federation)</li>
</ul>

<p>As these examples show, we tested both specialized sites that explicitly target teenagers and mainstream sites that include teens as part of a broader target audience.</p>

<h2>Teen Motivations for Using Websites</h2>

<p>Teenagers access the web for myriad activities, including entertainment. Generally, they have a specific <strong> goal</strong>, even if that goal is just to keep themselves occupied for 10 minutes.</p>

<p>Although their specific tasks might differ from adults, teens are similar to adults in major ways: both groups expect websites to be easy to use and to let them accomplish their tasks. Like adults, teens are goal-oriented and don’t surf the web aimlessly; website usability is thus as important for them as for any other user group.</p>

<p>Teens in our studies reported using the web for:</p>

<ul>
	<li>School assignments</li>
	<li>Hobbies or other special interests (including learning new skills or finding fun activities)</li>
	<li>Entertainment (including music and games)</li>
	<li>News (including sports, current events, and entertainment)</li>
	<li>Learning about new topics</li>
	<li>Talking to friends</li>
	<li>Shopping</li>
</ul>

<p>Even when teens don’t make actual purchases on websites, they do visit them to research products and build wish lists for the credit-card-carrying adults in their lives.</p>

<h2>Good and Bad News</h2>

<p><strong>The good news: </strong> Teens are becoming more successful at navigating websites and finding what they need. The success rate for teens has improved 16% during the 8 years between the old and new studies, for an <strong> improvement rate of 2 percentage points per year. </strong> This is slightly better than the improvement rate of <a href="../progress-in-usability-fast-or-slow/index.php" title="Alertbox: Progress in Usability: Fast or Slow?"> 1.7% per year for adults</a> using websites over the past decade.</p>

<p>Note: The <a href="../success-rate-the-simplest-usability-metric/index.php" title="Alertbox: Success Rate: The Simplest Usability Metric"> success rate </a> indicates the portion of tasks users were able to complete. Anything less than 100 percent represents a design failure or loss of business for a site.</p>

<p style="text-align: center;"><img alt="Teens success rate in usability studies 8 years ago vs now" src="http://media.nngroup.com/media/editor/2013/02/01/teenagers-usability-tast-success-rate.png" style="width: 330px; height: 250px;"/><br/>
<em>Good news: Success increased by 16 percent in 8 years. </em></p>

<p>Are teens getting better or are websites getting better? Probably a bit of both. We observed many of the same bad user habits among teens in our new study as we saw 8 years ago. Thus, the improved performance obviously stems in part from improvements in website design. That said, even though teens in our original study were heavy web users, teenagers today have even greater access to the internet and spend more time using it — and thus have more chances to hone their web surfing skills.</p>

<p><strong>The bad news: </strong> Teens are not as invincible as some people think. Although teens might feel confident online, they <em> do </em> make mistakes and often give up quickly. Fast-moving teens are also less cautious than adults and make snap judgments; these lead to lower success. Indeed, we measured a <strong> success rate of only 71% for teenage users </strong> compared to <a href="../progress-in-usability-fast-or-slow/index.php" title="Progress in Usability: Fast or Slow?"> 83% for adults</a>.</p>

<p>Teens perform worse than adults for three reasons:</p>

<ul>
	<li>Insufficient <strong> reading </strong> skills</li>
	<li>Less sophisticated <strong> research </strong> strategies</li>
	<li>Dramatically lower levels of <strong> patience </strong></li>
</ul>

<p>To improve your site’s usability among teens, you must consider all three factors.</p>

<p>Across different types of websites, teens had the <strong> most success on e-commerce </strong> websites, which often adhered to design standards and required little reading. Teens encountered the greatest challenges on large sites with dense content and poor navigation schemes. <strong> Government, non-profit, and school sites were the biggest culprits </strong> of poor usability.</p>

<p>Despite usability improvements, we observed users struggling with the same issues as in previous years — as well as new issues created by emerging features and design approaches. Thus, both traditional and new guidelines must be considered as technology and people continually evolve; our new report contains 110 guidelines total, compared with 61 in the first edition.</p>

<p>Many of the guidelines also apply to general audiences. For teens, however, these guidelines are even more important because the usability issues present bigger hurdles.</p>

<h2>Write Well</h2>

<p>Write for impatient users. Nothing deters younger audiences more than a cluttered screen full of text. Teens can quickly become bored, distracted, and frustrated.<br/>
Teenagers <strong> don’t like to read a lot on the web</strong>. They get enough of that at school. Also, the reading skills of many teenagers are not ideal, especially among younger teens. Sites that were easy to scan or that illustrated concepts visually were strongly preferred to sites with dense text.</p>

<p>Applying <a href="../../topic/writing-web/index.php" title="Articles on Writing for the Web"> proper web writing and formatting</a> techniques is crucial in communicating with teens. Display content in small, meaningful chunks with plenty of white space. Small chunks help students retain information and pick up where they left off after the inevitable interruptions of text messages and phone calls.</p>

<p>Help teens learn and stay focused by choosing your words wisely. Use words that teens understand. Write in short sentences and paragraphs. Teens generally have poorer reading and comprehension skills than adults. If your site targets a broad audience, aim to <a href="../writing-for-lower-literacy-users/index.php" title="Alertbox: Lower-Literacy Users: Writing for a Broad Consumer Audience"> write at a 6th-grade reading level</a> (or lower). Writing at this level will help audiences of all ages — young and old — quickly understand your content.</p>

<p>One surprising finding in this study: teenagers dislike tiny font sizes as much as adults do. We’ve often warned websites about using small text because of the negative implications for senior citizens (and even people in their late 40s, whose eyesight has begun to decline). We’ve always assumed that tiny text predominated because most Web designers are young and still have perfect vision, so we were surprised that small type often caused problems or provoked negative comments from our study’s teen users. Even though they’re sufficiently sharp-eyed, most teens move too quickly and are too easily distracted to attend to small text.</p>

<h2>Avoid Boring Content — and Entertainment Overload</h2>

<p>Teens complained about sites they found boring. Dull content is the kiss of death if your goal is to keep teens on your site. However, not everything needs to be interactive and fancy. Although teens have a strong appreciation for aesthetics, they <strong> detest sites that appear cluttered </strong> and contain pointless multimedia.</p>

<p>Beware of overusing interactive features just because you're designing for younger audiences. Multimedia can engage or enrage teens, depending on its usefulness. The best online experiences for teens are those that teach them something new or keep them focused on a goal.</p>

<p>What’s good? The following interactive features all worked well because they let teens do things rather than simply sit and read:</p>

<ul>
	<li>Online quizzes</li>
	<li>Forms for providing feedback or asking questions</li>
	<li>Online voting</li>
	<li>Games</li>
	<li>Features for sharing pictures or stories</li>
	<li>Message boards</li>
	<li>Forums for offering and receiving advice</li>
	<li>Features for creating a website or otherwise adding content</li>
</ul>

<p>These interactive features let teenagers make their mark on the Internet and express themselves in various ways — some small, some big.</p>

<p>The site type influences user expectations. For example, teens expect e-commerce and brand sites to look professional and informational sites to look simple and polished. For the latter sites, presenting interesting content in a clear manner is much more attractive than experimenting with new sophisticated features. Teens can learn and feel engaged without the nonessential enhancements.</p>

<h2>Make It Snappy</h2>

<p>A <a href="../website-response-times/index.php" title="Alertbox: Website Response Times"> slow-loading website is a deal-breaker</a>. Whatever you do, make sure your site loads quickly. Slow, sluggish sites are frustrating to anybody, but they’re especially offensive to young audiences who expect instant gratification.</p>

<p>Think twice before you develop that super-cool widget. If it’s slow or buggy, forget it. Teens won’t have the patience for it. Because teens often work on older, second-hand computers — and sometimes have slow Internet connections — fancy features might not work well. Teenagers like to do stuff on the Web. They dislike sites that are slow or that look fancy but behave clumsily.</p>

<h2>Don’t Talk Down to Teens</h2>

<p>Avoid anything that sounds condescending or babyish. The proper tone can make or break your site. Teens relate to content created by peers, so supplement your content with real stories, images, and examples from other teens.</p>

<p>Some websites in our study tried to serve both children and teens in a single area, usually titled something like <em> Kids</em>. This is a grave mistake; <strong> the word “kid” is a teen repellent</strong>. Teenagers are fiercely proud of their newly won status, and they don’t want overly childish content — one more reason to ease up on the heavy animations and garish color schemes that work for younger audiences. We recommend having separate sections for young children and teens, labeled Kids and <em> Teens</em>, respectively.</p>

<h2>Let Teens Control the Social Aspects</h2>

<p>Facilitate sharing but don’t force it. Teens rely on technology for social communication, but they don't want to be social all the time. They do want to control what they share and how they share it. Sites that force teens to register and then automatically make the profile public on the site violate trust. Parents and teachers teach teens to protect their privacy at a young age, and one of the things teens learn is to avoid nosey sites.</p>

<p>When offering sharing options, make sure to include email. Unlike college students, teens often prefer using email to share content as they’re more protective of their social accounts and cautious about who sees their activities.</p>

<h2>Design for Smaller Screens and Poor Ergonomics</h2>

<p>Many students access the web while sitting in awkward positions using portable devices with small screens, such as laptops, tablets, and mobile devices. The adoption of portable devices requires that you design a website in a way that doesn’t compromise usability. Thus, even though <a href="../computer-screens-getting-bigger/index.php" title="Alertbox: Computer Screens Getting Bigger"> screens are getting bigger for business users</a>, teens rarely get those top-end desktop computers.</p>

<p>Teens often work on laptops with track pads, making interactions that require precision — such as drop-down menus, drag-n-drop, and small buttons — difficult. Design elements such as rollover effects and small click zones are also problematic, if they’re usable at all. Small text sizes and dense text make reading difficult. Combine these elements with poor ergonomics and you have a prescription for fatigue and errors.</p>

<p>Media portrays teens as competent computer jockeys. In reality, teen overconfidence combined with developing cognitive abilities means they often give up quickly and blame the website’s design. They don’t blame themselves, they blame you.</p>

<h2>Age Group Differences</h2>

<p>The following table summarizes the main similarities and differences in web design approaches for young children, teenagers, college students, and adults. (The findings about children are from our <a href="../childrens-websites-usability-issues/index.php" title="Alertbox: Children's Websites: Usability Issues in Designing for Kids"> studies with 3–12-year-old users</a>; the findings about college students are from our <a href="../college-students-on-the-web/index.php" title="Alertbox: College Students on the Web"> study with 18–24-year-old users</a>.)</p>

<table align="center" style="border-width: 1px; border-style: solid; padding: 19px; border-collapse: collapse;">
	<colgroup>
		<col style="width: 16%;"></col>
		<col style="width: 14%;"></col>
		<col style="width: 14%;"></col>
		<col style="width: 14%;"></col>
		<col style="width: 14%;"></col>
		<col style="width: 14%;"></col>
		<col style="width: 14%;"></col>
	</colgroup>
	<tbody>
		<tr>
			<td> </td>
			<th style="border-width: 1px; border-style: solid; padding: 5px; text-align: center;">Hunting for things to click</th>
			<th style="border-width: 1px; border-style: solid; padding: 5px; text-align: center;">Tabbed browsing</th>
			<th style="border-width: 1px; border-style: solid; padding: 5px; text-align: center;">Scrolling</th>
			<th style="border-width: 1px; border-style: solid; padding: 5px; text-align: center;">Search</th>
			<th style="border-width: 1px; border-style: solid; padding: 5px; text-align: center;">Patience</th>
			<th style="border-width: 1px; border-style: solid; padding: 5px; text-align: center;">Animation<br/>
			and<br/>
			sound effects</th>
		</tr>
		<tr>
			<th style="border-width: 1px; border-style: solid; padding: 5px; text-align: left;">Kids<br/>
			(3–12)</th>
			<td style="border-width: 1px; border-style: solid; padding: 15px; text-align: center;"><img alt="yes" height="21" src="http://media.nngroup.com/media/editor/alertbox/happyface.gif" width="22"/></td>
			<td style="border-width: 1px; border-style: solid; padding: 15px; text-align: center;"><img alt="no" height="21" src="http://media.nngroup.com/media/editor/alertbox/sadface.gif" width="22"/></td>
			<td style="border-width: 1px; border-style: solid; padding: 15px; text-align: center;"><img alt="no" height="21" src="http://media.nngroup.com/media/editor/alertbox/sadface.gif" width="22"/></td>
			<td style="border-width: 1px; border-style: solid; padding: 15px; text-align: center;"><img alt="no" height="21" src="http://media.nngroup.com/media/editor/alertbox/sadface.gif" width="22"/></td>
			<td style="border-width: 1px; border-style: solid; padding: 15px; text-align: center;"><img alt="maybe" height="21" src="http://media.nngroup.com/media/editor/alertbox/neutralface.gif" width="22"/></td>
			<td style="border-width: 1px; border-style: solid; padding: 15px; text-align: center;"><img alt="yes" height="21" src="http://media.nngroup.com/media/editor/alertbox/happyface.gif" width="22"/></td>
		</tr>
		<tr>
			<th style="border-width: 1px; border-style: solid; padding: 5px; text-align: left;">Teens<br/>
			(13–17)</th>
			<td style="border-width: 1px; border-style: solid; padding: 15px; text-align: center;"><img alt="maybe" height="21" src="http://media.nngroup.com/media/editor/alertbox/neutralface.gif" width="22"/></td>
			<td style="border-width: 1px; border-style: solid; padding: 15px; text-align: center;"><img alt="maybe" height="21" src="http://media.nngroup.com/media/editor/alertbox/neutralface.gif" width="22"/></td>
			<td style="border-width: 1px; border-style: solid; padding: 15px; text-align: center;"><img alt="maybe" height="21" src="http://media.nngroup.com/media/editor/alertbox/neutralface.gif" width="22"/></td>
			<td style="border-width: 1px; border-style: solid; padding: 15px; text-align: center;"><img alt="no" height="21" src="http://media.nngroup.com/media/editor/alertbox/sadface.gif" width="22"/></td>
			<td style="border-width: 1px; border-style: solid; padding: 15px; text-align: center;"><img alt="no" height="21" src="http://media.nngroup.com/media/editor/alertbox/sadface.gif" width="22"/></td>
			<td style="border-width: 1px; border-style: solid; padding: 15px; text-align: center;"><img alt="maybe" height="21" src="http://media.nngroup.com/media/editor/alertbox/neutralface.gif" width="22"/></td>
		</tr>
		<tr>
			<th style="border-width: 1px; border-style: solid; padding: 5px; text-align: left;">College students<br/>
			(18–24)</th>
			<td style="border-width: 1px; border-style: solid; padding: 15px; text-align: center;"><img alt="no" height="21" src="http://media.nngroup.com/media/editor/alertbox/sadface.gif" width="22"/></td>
			<td style="border-width: 1px; border-style: solid; padding: 15px; text-align: center;"><img alt="yes" height="21" src="http://media.nngroup.com/media/editor/alertbox/happyface.gif" width="22"/></td>
			<td style="border-width: 1px; border-style: solid; padding: 15px; text-align: center;"><img alt="yes" height="21" src="http://media.nngroup.com/media/editor/alertbox/happyface.gif" width="22"/></td>
			<td style="border-width: 1px; border-style: solid; padding: 15px; text-align: center;"><img alt="yes" height="21" src="http://media.nngroup.com/media/editor/alertbox/happyface.gif" width="22"/></td>
			<td style="border-width: 1px; border-style: solid; padding: 15px; text-align: center;"><img alt="no" height="21" src="http://media.nngroup.com/media/editor/alertbox/sadface.gif" width="22"/></td>
			<td style="border-width: 1px; border-style: solid; padding: 15px; text-align: center;"><img alt="no" height="21" src="http://media.nngroup.com/media/editor/alertbox/sadface.gif" width="22"/></td>
		</tr>
		<tr>
			<th style="border-width: 1px; border-style: solid; padding: 5px; text-align: left;">Adults<br/>
			(25–64)</th>
			<td style="border-width: 1px; border-style: solid; padding: 15px; text-align: center;"><img alt="no" height="21" src="http://media.nngroup.com/media/editor/alertbox/sadface.gif" width="22"/></td>
			<td style="border-width: 1px; border-style: solid; padding: 15px; text-align: center;"><img alt="maybe" height="21" src="http://media.nngroup.com/media/editor/alertbox/neutralface.gif" width="22"/></td>
			<td style="border-width: 1px; border-style: solid; padding: 15px; text-align: center;"><img alt="yes" height="21" src="http://media.nngroup.com/media/editor/alertbox/happyface.gif" width="22"/></td>
			<td style="border-width: 1px; border-style: solid; padding: 15px; text-align: center;"><img alt="yes" height="21" src="http://media.nngroup.com/media/editor/alertbox/happyface.gif" width="22"/></td>
			<td style="border-width: 1px; border-style: solid; padding: 15px; text-align: center;"><img alt="yes" height="21" src="http://media.nngroup.com/media/editor/alertbox/happyface.gif" width="22"/></td>
			<td style="border-width: 1px; border-style: solid; padding: 15px; text-align: center;"><img alt="no" height="21" src="http://media.nngroup.com/media/editor/alertbox/sadface.gif" width="22"/></td>
		</tr>
	</tbody>
</table>

<div style="margin-left:auto; margin-right:auto; margin-top: 2ex; text-align: center;">
<table style="border-width: 1px; padding: 19px; border-collapse: collapse;">
	<tbody>
		<tr>
			<td style="padding: 8px;"><strong>Key: </strong></td>
			<td> </td>
		</tr>
		<tr>
			<td style="padding: 8px;"><img alt="yes" height="21" src="http://media.nngroup.com/media/editor/alertbox/happyface.gif" width="22"/></td>
			<td>Enjoyable, interesting, and appealing, or users can easily adjust to it.</td>
		</tr>
		<tr>
			<td style="padding: 8px;"><img alt="maybe" height="21" src="http://media.nngroup.com/media/editor/alertbox/neutralface.gif" width="22"/></td>
			<td>Users might appreciate it to some extent, but overuse can be problematic.</td>
		</tr>
		<tr>
			<td style="padding: 8px;"><img alt="no" height="21" src="http://media.nngroup.com/media/editor/alertbox/sadface.gif" width="22"/></td>
			<td>Users dislike it, don't do it, or find it difficult to operate.</td>
		</tr>
	</tbody>
</table>
</div>

<p>Clearly, there are many differences between age groups. The highest usability level for teens comes from designs that are targeted specifically at their needs and behaviors, which differ from those of adults and young children. As the table shows, this is true both for interaction design and for more obvious factors, such as topics and content style.</p>

<p>More information and complete <a href="../../reports/teenagers-on-the-web/index.php"> usability guidelines for designing websites for teens</a> in our full research report.</p>
</section>
  

  
  <section>
    
        
    
  </section>
  
  
  
  
  

  


  <section>
    <br />
    <p class="share"><b>Share <span class="hide-for-small-down">this article</span>:</b> <a href="https://twitter.com/intent/tweet?url=http://www.nngroup.com/articles/usability-of-websites-for-teenagers/&amp;text=Teenage%20Usability:%20Designing%20Teen-Targeted%20Websites&amp;via=nngroup">Twitter</a> | <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=http://www.nngroup.com/articles/usability-of-websites-for-teenagers/&amp;title=Teenage%20Usability:%20Designing%20Teen-Targeted%20Websites&amp;source=Nielsen%20Norman%20Group">LinkedIn</a> | <a href="https://plus.google.com/share?url=http://www.nngroup.com/articles/usability-of-websites-for-teenagers/">Google+</a> | <a href="mailto:?subject=NN/g Article: Teenage Usability: Designing Teen-Targeted Websites&amp;body=http://www.nngroup.com/articles/usability-of-websites-for-teenagers/">Email</a></p>
    <hr />
  </section>

  <footer class="l-subsection">
    
    
    <div class="learn-more">
      <h2>Learn More</h2>
      <p><a href="../subscribe/index.php">Subscribe to the weekly newsletter</a> to get notified about future articles.</p>
      </div>
    
    

    <div class="row show-for-small-down related-content">
      
      
      

      
      
        <div class="column small-12 topic-links">All articles about:
          <ul>
            
            <li><a href="../../topic/web-usability/index.php">Web Usability</a></li>
            
            <li><a href="../../topic/young-users/index.php">Young Users</a></li>
            
          </ul>
        </div>
      
      

      
      
        <div class="column small-12">
          <h3>UX Conference Training Courses</h3>
           <ul class="no-bullet">
    
        <li><a href="../../courses/ux-basic-training/index.php">UX Basic Training</a></li>
    
        <li><a href="../../courses/human-mind/index.php">The Human Mind and Usability</a></li>
    
        <li><a href="../../courses/emerging-patterns-web-design/index.php">Emerging Patterns for Web Design</a></li>
    
        <li><a href="../../courses/web-page-design/index.php">Web Page UX Design</a></li>
    
</ul>
        </div>
      
      

      
      
        <div class="small-12 column">
          <h3>Research Reports</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../reports/designing-for-young-adults/index.php">Designing for Young Adults (Ages 18-25)</a></li>
              
            
              
                <li><a href="../../reports/teenagers-on-the-web/index.php">Teenagers (Ages 13-17) on the Web</a></li>
              
            
              
                <li><a href="../../reports/children-on-the-web/index.php">Children (Ages 3-12) on the Web</a></li>
              
            
              
                <li><a href="../../reports/b2b-websites-usability/index.php">B2B Website Usability for Converting Users into Leads and Customers</a></li>
              
            
              
                <li><a href="../../reports/university/index.php">University Websites</a></li>
              
            
          </ul>
        </div>
      
      

      
      
        <div class="small-12 column">
          <h3>Online Seminars</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../online-seminars/fundamental-flaw-flat-design/index.php">The Fundamental Flaw in Flat Design</a></li>
              
            
              
                <li><a href="../../online-seminars/millennials-online/index.php">Millennials Online</a></li>
              
            
              
                <li><a href="../../online-seminars/university-websites/index.php">University Website UX Essentials</a></li>
              
            
          </ul>
        </div>
      
      
    </div>

    <div class="hide-for-small-down row related-content">
      <div class="medium-6 columns">
        
        
          <div>
            <h3>Articles</h3>
            <ul class="no-bullet">
              
                
                <li><a href="../ux-thanks/index.php">Give Thanks for Good UX</a></li>
                
              
                
                <li><a href="../university-sites/index.php">University Websites: Top 10 Design Guidelines</a></li>
                
              
                
                <li><a href="../top-10-enduring/index.php">Top 10 Enduring Web-Design Mistakes</a></li>
                
              
                
                <li><a href="../overuse-of-overlays/index.php">Overuse of Overlays: How to Avoid Misusing Lightboxes</a></li>
                
              
                
                <li><a href="../pop-up-adaptive-help/index.php">Pop-ups and Adaptive Help Get a Refresh</a></li>
                
              
            </ul>
          </div>
        
        

        
          
          <div>
            <h3>Research Reports</h3>
            <ul class="no-bullet">
              
                
                  <li><a href="../../reports/designing-for-young-adults/index.php">Designing for Young Adults (Ages 18-25)</a></li>
                
              
                
                  <li><a href="../../reports/teenagers-on-the-web/index.php">Teenagers (Ages 13-17) on the Web</a></li>
                
              
                
                  <li><a href="../../reports/children-on-the-web/index.php">Children (Ages 3-12) on the Web</a></li>
                
              
                
                  <li><a href="../../reports/b2b-websites-usability/index.php">B2B Website Usability for Converting Users into Leads and Customers</a></li>
                
              
                
                  <li><a href="../../reports/university/index.php">University Websites</a></li>
                
              
            </ul>
          </div>
          
        

      </div>
      <div class="medium-6 columns">
        
          
          <div>
            <h3>UX Conference Training Courses</h3>
             <ul class="no-bullet">
    
        <li><a href="../../courses/ux-basic-training/index.php">UX Basic Training</a></li>
    
        <li><a href="../../courses/human-mind/index.php">The Human Mind and Usability</a></li>
    
        <li><a href="../../courses/emerging-patterns-web-design/index.php">Emerging Patterns for Web Design</a></li>
    
        <li><a href="../../courses/web-page-design/index.php">Web Page UX Design</a></li>
    
</ul>
          </div>
          
        
        
          
          <div>
            <h3>Online Seminars</h3>
            <ul class="no-bullet">
            
                <li><a href="../../online-seminars/fundamental-flaw-flat-design/index.php">The Fundamental Flaw in Flat Design</a></li>
            
                <li><a href="../../online-seminars/millennials-online/index.php">Millennials Online</a></li>
            
                <li><a href="../../online-seminars/university-websites/index.php">University Website UX Essentials</a></li>
            
            </ul>
          </div>
          
        
      </div>
    </div>
  </footer>
</article>

        </div>

      

    
  </div>
  <div class="row">
    
      <div class="column small-12 show-for-small-down l-navrail announcements">
        
          
        
      </div>
    
    
  </div>

    </div>
    




<footer id="page-footer">
    <div class="row hide-for-print">
        <div class="column small-12 medium-6">
            <section class="footer-popular">
                
                <h1>Popular Topics</h1>
                <div style="float: left;  width: 15em; display: block;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/applications/index.php">Application Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/e-commerce/index.php">E-Commerce Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/eyetracking/index.php">Eyetracking </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/information-architecture/index.php">Information Architecture </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/intranets/index.php">Intranet Design </a></li>
	<li><a href="../../topic/interaction-design/index.php">Interaction Design </a></li>
</ul>
</div>

<div style="float: left;  width: 15em; display: block; list-style: none;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/mobile-and-tablet-design/index.php">Mobile and Tablet Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/credibility-persuasion/index.php">Persuasive Design</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/research-methods/index.php">Research Methods</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/user-testing/index.php">User Testing </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/web-usability/index.php">Web Usability </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/writing-web/index.php">Web Writing &amp; Content Strategy </a></li>
</ul>
</div>

<p>&nbsp;</p>

                
            </section>
        </div>

        <div class="column small-12 medium-6">
            <div class="row">
                <div class="column small-6">
                    <section class="footer-about">
                        <h1>Follow Us</h1>
                        <ul>
	<li><a href="../subscribe/index.php">Subscribe to our weekly UX newsletter</a>,&nbsp;Jakob Nielsen&#39;s Alertbox</li>
	<li>Visit Don Norman&#39;s website, <a href="http://www.jnd.org/"> www.jnd.org </a></li>
	<li>Visit Bruce Tognazzini&#39;s website, <a href="http://www.asktog.com/"> www.asktog.com </a></li>
	<li style="padding-top: 6px;"><a class="twitter-follow-button" data-show-count="false" data-size="large" href="https://twitter.com/nngroup"><img alt="Follow @nngroup on Twitter" height="20" src="https://media.nngroup.com/media/editor/2016/12/30/twitter-follow-button.png" style="width: 117px; height: 20px;" width="117" /></a></li>
	<li style="padding-top: 6px;"><img alt="" height="14" src="https://media.nngroup.com/media/editor/2016/07/26/feed-icon-14x14.png" style="vertical-align:text-top;width:14px;margin-right:.25em;" width="14" /><a href="../../feed/rss/index.php">Subscribe to our RSS feed</a></li>
</ul>

                    </section>
                </div>

                <div class="column small-6">
                    <section class="footer-about right-column">
                        <h1>About
</h1>
                        <ul class="no-bullet">
	<li><a href="../../about/contact/index.php">Contact information</a></li>
	<li><a href="../../about/index.php">About Us</a></li>
	<li><a href="../../about/why-nng/index.php">Why NN/g? </a></li>
	<li><a href="../../return-policy/index.php">Return Policy</a></li>
</ul>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="copyright column small-12">
            <p><a href="../../copyright-and-reprint-info/index.php">Copyright</a> &copy; 1998-2017 Nielsen Norman Group, All Rights Reserved.</p>
        </div>
    </div>
</footer>


    


        <script src="https://media.nngroup.com/static/vendor/fastclick/js/fastclick.min.js"></script>
        <script src="https://media.nngroup.com/static/vendor/foundation/js/foundation.min.js"></script>
        <script src="http://media.nngroup.com/static/js/app.js"></script>
        <script src="http://media.nngroup.com/static/js/scroll-to-fixed.js"></script><script>var externalLinks = document.querySelectorAll('a[href^="http"]');for (var i = externalLinks.length-1; i >= 0; i--) {externalLinks[i].addEventListener("click", function(event) { alert("External links are disabled for this test."); event.preventDefault(); }, false);}var mailLinks = document.querySelectorAll('a[href^="mailto"]');for (var i = mailLinks.length-1; i >= 0; i--) {mailLinks[i].addEventListener("click", function(event) { alert("Email links are disabled for this test."); event.preventDefault(); }, false);}var externalForms = document.querySelectorAll('form');for (var i = externalForms.length-1; i >= 0; i--) {externalForms[i].addEventListener("submit", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false); externalForms[i].addEventListener("click", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false);}</script>

        <script>
            
            $(function() {
                $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
                $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
            });
        </script>

        
<script src="https://media.nngroup.com/static/js/articles.js"></script>


        
        <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1072699960;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072699960/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
    </body>

<!-- Mirrored from www.nngroup.com/articles/usability-of-websites-for-teenagers/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:13:13 GMT -->
</html>
