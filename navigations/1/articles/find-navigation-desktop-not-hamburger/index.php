<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->





    
<!-- Mirrored from www.nngroup.com/articles/find-navigation-desktop-not-hamburger/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:13:06 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1026.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"beacon":"bam.nr-data.net","queueTime":26,"applicationTime":2394,"agent":"","licenseKey":"1a30ef9931","errorBeacon":"bam.nr-data.net","applicationID":"2048992","transactionName":"NFxSMRBUX0EDUkIPDA0WdhAMVkVbDV8ZBxEXUFMJB0YfRAtUQRVZIktEDAFZVHYHRVcPD01eVRE="}</script>
        <title>Beyond the Hamburger: What Makes Navigation Discoverable on Desktops</title><meta property="og:title" content="Beyond the Hamburger: What Makes Navigation Discoverable on Desktops" />
  
        
        <meta name="description" content="Hiding or displaying desktop navigation impacts use. But visual salience, information scent, context, and user expectations also affect discoverability.">
        <meta property="og:description" content="Hiding or displaying desktop navigation impacts use. But visual salience, information scent, context, and user expectations also affect discoverability." />
        
  
        
	
        
        <meta name="keywords" content="Navigation, hamburger, usability, quantitative test, qualitative test, link, menu, icon, down arrow icon, more, button, position, remote test, remote usability test, whatusersdo, discover, discoverability, find, findability, affordances, signifiers, progressive disclosure, interaction cost">
        

        

        <meta name="robots" content="noarchive">
        <meta name="viewport" content="width=device-width">

        <link rel="search" type="application/opensearchdescription+xml" title="NN/g Search" href="../../opensearch.xml" />
        <link rel="shortcut icon" href="http://media.nngroup.com/static/img/favicon.ico" />

        <link rel="stylesheet" href="https://media.nngroup.com/static/vendor/font-awesome/css/font-awesome.min.css">

        
        <link rel="stylesheet" href="https://media.nngroup.com/static/css/7232a960f1df.css" type="text/css" />
        <link rel="stylesheet" href="http://fast.fonts.com/t/1.css?apiType=css&amp;projectid=c5846260-9def-46ec-9407-0f1349bd2d64" type="text/css">

        <script src="https://media.nngroup.com/static/vendor/modernizr/js/modernizr.min.js"></script>

        <script type="text/javascript" src="https://media.nngroup.com/static/js/3283be5def74.js"></script>
        

        

        
        
        <!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=229034,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('http://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->


<!-- Google Analytics Tracking Code for http://nngroup.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23132681-1', 'nngroup.com');
  ga('require', 'linkid', 'https://www.nngroup.com/articles/find-navigation-desktop-not-hamburger/linkid.js');
  ga('require', 'displayfeatures');

// Optimizely Universal Analytics Integration
 window.optimizely = window.optimizely || [];
 window.optimizely.push("activateUniversalAnalytics");

  ga('send', 'pageview');

</script>
<script>
/**
* Function to track a click on an outbound link in Google Analytics.
*/
function trackOutboundLink(label, url) {
    if(window.ga && ga.loaded) {
        ga('send', 'event', 'outbound', 'click', label, {'hitCallback': function(){document.location = url;}
        });
    } else {
        document.location = url;
    }
}
/**
* Function to track a click on a link in the article announcement area.
*/
$('#articleAnnouncement a').on('click', function(){
  ga('send', 'event', 'link', 'click', 'articleAnnouncement');
});
</script>

<script type="text/javascript">
 setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2421.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



<!-- Hotjar Tracking Code for http://nngroup.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:121605,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

        <script src="https://media.nngroup.com/static/js/analytics/track_outbound_link.js"></script>
        
        
        
    </head>
    <body class="location-articles articles-detail">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
    <header id="page-header">
        <div id="cart" class="shopping-cart mobile-cart show-for-small-down"></div>
        <div class="row">
            <div id="masthead" class="masthead small-12 medium-7 column">

                <!-- The button to toggles menu on small screen -->

                <h1><a href="../../index.php"><img src="http://media.nngroup.com/static/img/logo-wordmark.png" width="74" height="30"> Nielsen <span class="norman">Norman</span> Group</a></h1>
                <p class="tagline show-for-medium-up">Evidence-Based User Experience Research, Training, and Consulting</p>

                <!-- The button to toggles form search on small screen -->


                <div style="clear: right;width:  100%;height: 1px;"></div><?php define('ROOT_PATH',$_SERVER["DOCUMENT_ROOT"] . '/apps/navigation_styles/navigations/1'); ?><?php include(ROOT_PATH)."/navigation.php"; ?>

                <!-- Drop down menu on small screen -->
                <div id="hideNav" class="show-for-small-down">
                  
                  <ul class="menu-content">
                    <li><a href="../../index.php">Home</a></li>
                    <li class="has-submenu">
                      <a>Training</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../training/index.php">Overview</a></li>
                        
<li id="nav-ux-conference"><a href="../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../online-seminars/index.php"><span>Online Seminars</span></a></li>

                      </ul>
                    </li>
                    <li class="has-submenu">
                      <a>Consulting</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
  <li><a href="../../consulting/index.php">Overview</a></li>
  
  <li><a href="../../consulting/benchmarking/index.php">Benchmarking</a></li>
  
  <li><a href="../../consulting/expert-design-review/index.php">Expert Design Review</a></li>
  
  <li><a href="../../consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
  
  <li><a href="../../consulting/usability-testing/index.php">Usability Testing </a></li>
  
  <li><a href="../../consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
  
  <li><a href="../../in-house-training/index.php">In-House Training</a></li>
</ul>

                    </li>

                    <li><a href="../../reports/index.php">Reports</a></li>
                    <li><a href="../index.php">Articles</a></li>
                    <li class="has-submenu"><a>About NN/G</a><i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../about/index.php">Overview</a></li>
                        <li><a href="../../people/index.php">People</a></li>
                        <li><a href="../../about/why-nng/index.php">Why NN/g?</a></li>
                        <li><a href="../../about/contact/index.php">Contact</a></li>
                        <li><a href="../../news/index.php">News</a></li>
                        <li><a href="../../about/history/index.php">History</a></li>
                        <li><a href="../../books/index.php">Books</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                </div>

                <hr class="header-divider show-for-small-only"/>


            </div>
       </div>

        <div id="hideNav">            <nav class="primary-nav small-12 column">
                <ul>
                    <li><a class="navitem" id="nav-home" href="../../index.php">Home</a></li>
                    <li><a class="navitem" id="nav-training" href="../../training/index.php">Training</a></li>
                    <li><a class="navitem" id="nav-consulting" href="../../consulting/index.php">Consulting</a></li>
                    <li><a class="navitem" id="nav-reports" href="../../reports/index.php">Reports</a></li>
                    <li><a class="navitem" id="nav-articles" href="../index.php">Articles</a></li>
                    <li><a class="navitem" id="nav-about" href="../../about/index.php">About NN/G</a></li>
                </ul>
            </nav>
            <nav class="secondary-nav small-12 medium-12 large-12 columns">
                
                
            </nav>
        </div>
    </header>
    <div class="l-content">
    
  

  

  <div class="row">
    
      
      <section class="small-12 medium-3 columns collapsable-content l-navrail articles-left-column">
        <h1 class="show-for-small-down collapse-link mobile-browse-header">Browse by Topic and Author</h1>
          <div class="row collapsable">

            
            <div class="small-6 medium-12 columns">
              <section id="article-popular-topics" style="display: none;">
                
                <h1>Topics</h1>
                <ul id="article-popular-topics">
                    
                    <li>
                        
                        <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/intranets/all/index.php">Intranets</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/user-testing/all/index.php">User Testing</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                        
                    </li>
                    
                    <li><a href="#" id="article-see-all-topics"><strong>See all topics&hellip;</strong></a></li>
                </ul>
                
              </section>
              <section id="article-all-topics" style="display: none;">
                
                <h1>
                  <span class="show-for-medium-up">All Article Topics</span>
                  <span class="show-for-small-down">All Topics</span>
                  (<a href="#" id="article-hide-all-topics">hide</a>)
                </h1>
                <ul>
                  
                  <li>
                    
                    <a href="../../topic/accessibility/all/index.php">Accessibility</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/agile/all/index.php">Agile</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/analytics-and-metrics/all/index.php">Analytics &amp; Metrics</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/applications/all/index.php">Application Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/b2b-websites/all/index.php">B2B Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/behavior-patterns/all/index.php">Behavior Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/branding/all/index.php">Branding</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/content-strategy/all/index.php">Content Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/corporate-websites/all/index.php">Corporate Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/design-patterns/all/index.php">Design Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-design-process/all/index.php">Design Process</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/email/all/index.php">Email</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/eyetracking/all/index.php">Eyetracking</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/heuristic-evaluation/all/index.php">Heuristic Evaluation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/human-computer-interaction/all/index.php">Human Computer Interaction</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-ideation/all/index.php">Ideation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/information-architecture/all/index.php">Information Architecture</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/interaction-design/all/index.php">Interaction Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/international-users/all/index.php">International Users</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/intranets/all/index.php">Intranets</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-management/all/index.php">Management</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/navigation/all/index.php">Navigation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/non-profit-websites/all/index.php">Non-Profit Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/personas/all/index.php">Personas</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/persuasive-design/all/index.php">Persuasive Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/prototyping/all/index.php">Prototyping</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/psychology-and-ux/all/index.php">Psychology and UX</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/research-methods/all/index.php">Research Methods</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/search/all/index.php">Search</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/social-media/all/index.php">Social Media</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/strategy/all/index.php">Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/user-testing/all/index.php">User Testing</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-teams/all/index.php">UX Teams</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/visual-design/all/index.php">Visual Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/writing-web/all/index.php">Writing for the Web</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/young-users/all/index.php">Young Users</a>
                    
                  </li>
                  
                </ul>
                
              </section>
            </div>
            <script>
              /* Determine which topics list will be displayed: the full or the
               * partial.  We are inlining this close to the list itself to
               * prevent rendering delays on the client
               */
              var topic_list = $.cookie("article-topics");

              var popular_topics = $('#article-popular-topics');
              var all_topics = $('#article-all-topics');

              if(topic_list == 1) {
                  popular_topics.hide();
                  all_topics.show();
              } else {
                  popular_topics.show();
                  all_topics.hide();
              }
            </script>
            

            
            <div class="small-6 medium-12 columns">
              
              <section id="article-initial-authors">
                  <h1>Author</h1>
                  <ul>
                    
                    <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                    
                    <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                    
                    <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                    
                    <li><a href="#" onClick="$('#article-initial-authors').hide(); $('#article-all-authors').show();"><strong>See all authors&hellip;</strong></a></li>
                  </ul>
              </section>
              <section id="article-all-authors" style="display: none;">
                <h1>
                  All Authors
                  (<a href="#" id="article-hide-all-authors">hide</a>)
                </h1>
                <ul>
                  
                  <li><a href="../author/raluca-budiu/index.php">Raluca Budiu</a></li>
                  
                  <li><a href="../author/patty-caya/index.php">Patty Caya</a></li>
                  
                  <li><a href="../author/susan-farrell/index.php">Susan Farrell</a></li>
                  
                  <li><a href="../author/therese-fessenden/index.php">Therese Fessenden</a></li>
                  
                  <li><a href="../author/kim-flaherty/index.php">Kim Flaherty</a></li>
                  
                  <li><a href="../author/sarah-gibbons/index.php">Sarah Gibbons</a></li>
                  
                  <li><a href="../author/aurora-harley/index.php">Aurora Harley</a></li>
                  
                  <li><a href="../author/kate-kaplan/index.php">Kate Kaplan</a></li>
                  
                  <li><a href="../author/page-laubheimer/index.php">Page Laubheimer</a></li>
                  
                  <li><a href="../author/angela-li/index.php">Angie Li</a></li>
                  
                  <li><a href="../author/hoa-loranger/index.php">Hoa Loranger</a></li>
                  
                  <li><a href="../author/kate-meyer/index.php">Kate Meyer</a></li>
                  
                  <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                  
                  <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                  
                  <li><a href="../author/kara-pernice/index.php">Kara Pernice</a></li>
                  
                  <li><a href="../author/christian-rohrer/index.php">Christian Rohrer</a></li>
                  
                  <li><a href="../author/amy-schade/index.php">Amy Schade</a></li>
                  
                  <li><a href="../author/katie-sherwin/index.php">Katie Sherwin</a></li>
                  
                  <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                  
                  <li><a href="../author/kathryn-whitenton/index.php">Kathryn Whitenton</a></li>
                  
                </ul>
              </section>
              
            </div>
            
          </div>

          
          <section class="hide-for-small-down">
            
              
              <h1>Recent Articles</h1>
              
              <ul>
                  
                  <li><a href="../seamless-cross-channel/index.php">Seamlessness in the Omnichannel User Experience</a></li>
                  
                  <li><a href="../brand-intention-interpretation/index.php">Experience Design: Bridging Brand Intention and Brand Interpretation</a></li>
                  
                  <li><a href="../eyetracking-tasks-efficient-scanning/index.php">Scanning Patterns on the Web Are Optimized for the Current Task</a></li>
                  
                  <li><a href="../flat-design-best-practices/index.php">Flat-Design Best Practices</a></li>
                  
                  <li><a href="../ux-user-stories/index.php">Accounting for UX Work with User Stories in Agile Projects</a></li>
                  
                  <li><a href="../index.php"><strong>See all articles&hellip;</strong></a></li>
              </ul>
              
            
          </section>
          

          
          
          <section class="hide-for-small-down">
            <h1>Popular Articles</h1>
            
            <ul>
                
                <li><a href="../usability-101-introduction-to-usability/index.php">Usability 101: Introduction to Usability</a></li>
                
                <li><a href="../top-10-mistakes-web-design/index.php">Top 10 Mistakes in Web Design</a></li>
                
                <li><a href="../how-users-read-on-the-web/index.php">How Users Read on the Web</a></li>
                
                <li><a href="../f-shaped-pattern-reading-web-content/index.php">F-Shaped Pattern For Reading Web Content</a></li>
                
                <li><a href="../ten-usability-heuristics/index.php">10 Usability Heuristics for User Interface Design</a></li>
                
                <li><a href="../intranet-design-2016/index.php">10 Best Intranets of 2016</a></li>
                
                <li><a href="../which-ux-research-methods/index.php">When to Use Which User-Experience Research Methods</a></li>
                
                <li><a href="../response-times-3-important-limits/index.php">Response Times: The 3 Important Limits</a></li>
                
                <li><a href="../why-you-only-need-to-test-with-5-users/index.php">Why You Only Need to Test with 5 Users</a></li>
                
                <li><a href="../page-fold-manifesto/index.php">The Fold Manifesto: Why the Page Fold Still Matters</a></li>
                
            </ul>
          </section>
          
          

          
            <section class="hide-for-small-down">
              
            </section>
          

      </section>
      

      
        <div class="small-12 medium-9 columns content-body">
          




<article class="l-subsection alertbox-article-content article-detail-bl">
  
  <header>
    <div class="l-subsection">
      <h1>Beyond the Hamburger: How to Make Navigation Discoverable on Desktops</h1>
      <div class="author-meta">by
        
          
            
            <a href="../author/kara-pernice/index.php">Kara Pernice</a>
            
            and
            
            <a href="../author/raluca-budiu/index.php">Raluca Budiu</a>
            
          
        on  July 24, 2016
        
      </div>

      

      
      <div class="topic-links">
        Topics:
        
          <ul>

  <li><a href="../../topic/navigation/index.php">Navigation</a></li>

</ul>
        
      </div>
      
    </div>
  </header>
  

  
  
  <section>
    <p class="lede"><strong>Summary:</strong> Hiding or displaying desktop navigation impacts use. But visual salience, information scent, context, clickability signifiers, and user expectations also play an important role in navigation discoverability on desktop.</p>
    <br />
  </section>
  
  

  
  <section id="article-body"><p>In a <a href="../hamburger-menus/index.php">previous article</a>, we reported a quantitative study that showed that <a href="../mobile-first-not-mobile-only/index.php">hiding navigation makes it less likely to be used and degrades the user experience</a> on a variety of metrics both on mobile and desktop. We attributed this effect to several factors:</p>

<ul>
	<li>Navigation options hidden under a menu have decreased <a href="../navigation-ia-tests/index.php">discoverability and findability</a>.</li>
	<li>The <a href="../interaction-cost-definition/index.php">interaction cost</a> to access these navigation options increases, since users must take extra steps to expose them before reading or selecting them</li>
	<li>Clicking on the navigation icon takes longer and is more<a href="../slips/index.php"> error-prone</a>, because these navigation-menu icons are often small and close to other clickable items.</li>
</ul>

<p>However, not all sites in our original study were equally impacted by the hidden navigation. In this article we discuss the <strong>site-design differences that affected desktop navigation usage</strong>. (See our companion article for an <a href="../find-navigation-mobile-even-hamburger/index.php">analysis of navigation on <strong>mobile </strong>sites</a>.)</p>

<h2>About the Study: Sites Tested</h2>

<p>NN/g partnered with international remote-usability-testing firm, <a href="http://whatusersdo.com/?utm_campaign=nng_hamburger_sidebar&amp;utm_medium=referral&amp;utm_source=ext_article">WhatUsersDo</a>, to test 3 different types of navigation: hidden, visible, and a combination of hidden and visible navigation. As described in the <strong><a href="../hidden-navigation-methodology/index.php">study methodology</a></strong>, 179 users participated in our study; they completed tasks on 6 different websites, on desktops and smartphones:</p>

<ul>
	<li>7digital (music, e-commerce)</li>
	<li>BBC (news)</li>
	<li>Bloomberg Business (business news)</li>
	<li>Business Insider UK (business news)</li>
	<li>Supermarket HQ (e-commerce)</li>
	<li>Slate (news)</li>
</ul>

<p>We collected 5 metrics:</p>

<ol>
	<li>Navigation use: whether participants used the navigation links at all</li>
	<li>Time to navigation: the time from the beginning of the task until the first use of the navigation in that task</li>
	<li>Task difficulty: participants’ assessment of task difficulty</li>
	<li>Content discoverability: whether participants were able to discover the content on the site</li>
	<li>Task time: the time it took people to complete the tasks</li>
</ol>

<p>The first 2 metrics are related to navigation usage, while the last three refer to the quality of the user experience. Since this article focuses on what makes navigation discoverable and findable, we refer mostly to the first 2 metrics. (Our other article discusses all <strong><a href="../hamburger-menus/index.php">hidden navigation study metrics</a></strong> in detail.)</p>

<h2>On Desktop, Salience Impacts Navigation Use</h2>

<p>The chart below shows desktop navigation usage on each of the 6 sites:</p>

<figure class="caption"><img alt="" height="436" src="https://media.nngroup.com/media/editor/2016/07/20/percent_nav_used_on_desktop.png" width="720"/>
<figcaption><em>Navigation usage on desktop sites: navigation was used least on Slate and Bloomberg, and most on SupermarketHQ.</em></figcaption>
</figure>

<p>As you can see, whereas the navigation usage was fairly similar for 7Digital, BBC, and Business Insider UK (the differences between these sites were not statistically significant), we did find that the other sites were different from this average group: navigation was used least on Slate and Bloomberg and most on Supermarket HQ. (The differences between these sites and the others were statistically significant, except for that between Bloomberg and Business Insider, which did not reach statistical significance.)</p>

<p>Let’s take a look at the navigation on each of these sites to better understand what was going on.</p>

<figure class="caption"><img alt="" height="660" src="https://media.nngroup.com/media/editor/2016/07/20/slate_desktop_large.png" width="720"/>
<figcaption><em>Slate desktop: Hamburger menu in upper right corner</em></figcaption>
</figure>

<p> </p>

<figure class="caption"><img alt="" height="210" src="https://media.nngroup.com/media/editor/2016/07/20/bloombergbusiness_desktop.png" width="720"/>
<figcaption><em>Bloomberg Business desktop: Links across the top (in low-contrast colors) plus a hidden navigation element in the form of a down arrow to the left of the links</em></figcaption>
</figure>

<p> </p>

<figure class="caption"><img alt="" height="236" src="https://media.nngroup.com/media/editor/2016/07/21/businessinsideruk_desktop.png" width="650"/>
<figcaption><em>Business Insider UK desktop: Hamburger menu in upper left corner</em></figcaption>
</figure>

<p> </p>

<figure class="caption"><img alt="" height="2171" src="https://media.nngroup.com/media/editor/2016/07/20/supermarkethq_desktop_scrolling-full-page.png" width="720"/>
<figcaption><em>Supermarket HQ: Browse button with down arrow in upper right, and visible icons lower on the page</em></figcaption>
</figure>

<p>The lowest performers hid at least some of the navigation under a menu: on Slate, it was completely hidden under a hamburger menu, whereas Bloomberg did have some visible navigation, albeit shown in low-contract colors that probably reduced <a href="../legibility-readability-comprehension/index.php">legibility</a> and visual salience.</p>

<p>Not only did Slate place the global navigation under a menu, but it also positioned this menu in the top right corner. Only 17% of people used it, as opposed to the 42% total average of desktop navigation used. And, it took an average of 33 seconds to find, compared to the desktop average of 25 seconds. Ironically, of all the sites in our study, users probably needed the menu most on this site. The mixed typefaces and crowded text made scanning the pages difficult. After some scrolling and reading, one user cursed then called the homepage “a real cluster”. Another user, who had also been scrolling and reading said, “I can’t find an article about the arts… Really weird typefaces. Slightly confusing and different all over.” After more scrolling she basically gave up on the task and said, “I can’t find an article on the arts. I am really not liking this. There is just too much. I don’t know where to start.” If the menu had been very simple and visible, this person probably would have used it.</p>

<p>There were several potential explanations for the poor navigation usage on this site:</p>

<ul>
	<li>The nonstandard position for desktop navigation (top right corner) was unexpected and nonconventional on the desktop.</li>
</ul>

<ul>
	<li>The navigation was placed at the top of a <a href="../fight-right-rail-blindness/index.php">right rail</a>. Traditionally, the right rail houses items such as related links or ads; people have learned to ignore this design element, especially if it resembles an ad (we’ve all heard of <a href="../banner-blindness-old-and-new-findings/index.php">banner blindness</a>).</li>
	<li>The maroon color was also employed as background for two other stories and thus it misled people into assuming that the Slate banner that housed the navigation menu was in fact such a story, too. As a result, people did not direct attention to it.</li>
	<li>The global-navigation menu was grouped with the <a href="../utility-navigation/index.php">utility-navigation</a> items. The <em>first</em> item people see in a set of grouped items can make them assume that all of the <em>rest</em> of the items are similar. Users who first noticed the person icon on the right may have assumed that the icons in this area were all related to logging in and may have stopped inspecting the area carefully.</li>
</ul>

<p>The bottom line: the global-navigation menu on the Slate homepage had a tiny footprint, low visual salience, and poor page placement. So people rarely used it.</p>

<p>The fact that Bloomberg performed so poorly (28% navigation usage vs. 42% overall navigation usage on the desktop) is more surprising: after all, Bloomberg did have a combination of hidden and visible navigation. The hidden navigation laid inside the arrow menu next to the logo, and the visible navigation was presented at the top of the screen, in the blue horizontal bar. We believe that there are several explanations for this result:</p>

<ul>
	<li>The homepage had section headers for various topics. Users who scrolled often found these topic headings and clicked these before they even looked for a menu.</li>
	<li>The arrow menu next to the logo was too small and hard to notice.</li>
	<li>The visible navigation options had low contrast with the blue background color and did not stand out sufficiently. (In fact, the white logo was the element most likely to stand out in the blue banner).</li>
	<li>The navigation options had <a href="../category-names-suck/index.php">vague, general navigation terms</a> (e.g., <em>Markets,</em> <em>Insights, Video</em>) that did not indicate specific news categories.</li>
</ul>

<p>Slate used inconspicuous, low-salience navigation that was easy to ignore, and as a result, our study participants did not use it. Bloomberg suffered some of the same traits, but the biggest reason people didn’t use the navigation probably had more to do with the topic links on the homepage, which people did see and use. One user, looking for an article about technology, scrolled the page and said, “I am looking left, and right, and to the middle and trying to find something on technology that I really like.” She scanned the page further and found the technology section and clicked it. After she chose and article she commented, “It wasn’t so easy to find. You have to make an extra effort to find it.”</p>

<p>Surprisingly, our top performer, SupermarketHQ, had some design commonalities with our bottom performer, Slate: they both placed the navigation in the top right corner under a menu. However, there were some important differences:</p>

<ul>
	<li>SupermarketHQ also listed visible navigation options at the bottom of the page.</li>
	<li>The hidden-navigation menu in the top right corner had a larger screen footprint than its counterpart on Slate.</li>
	<li>The navigation menu on SupermarketHQ was bigger and had good contrast with the green background.</li>
	<li>The hidden-navigation menu on SupermarketHQ had a clear label: <em>Browse.</em></li>
	<li>The rectangular shape of the menu (and the arrow next to it) helped the element look like a standard button and signaled clickability.</li>
</ul>

<p>Overall, we believe that navigation usage on SupermarketHQ was at least partially due to the salience of the global-navigation menu. That being said, it’s also possible that there are other explanations unrelated to the design of the navigation:</p>

<ul>
	<li>SupermarketHQ was an ecommerce site. People are used to browsing and engaging in undirected behaviors on news sites and more likely to have a defined goal on an ecommerce site. That can lead to increased navigation (or search) usage. However, if that were the only explanation, we’d expect an even higher navigation usage for the other ecommerce site in our study (7Digital), which had visible navigation. But in fact, the navigation was used more on SupermarketHQ (this difference was highly significant).</li>
	<li>SupermarketHQ had poor content on the homepage. In fact, the items featured on the homepage did not have any text descriptions (these appeared only on hover) and the vague information may have prompted participants to use the navigation. However, in spite of this poor content on the homepage, users were highly effective in completing the tasks on this site — the success rate was 95% — and did not take more time to complete the tasks or rate the tasks as more difficult.</li>
</ul>

<p>In general, we would recommend neither poor content nor putting the main navigation at the bottom of the page. In this case, the combination of 2 bad design choices turned out to save the site. A further saving grace was that the homepage photos were of high quality and intriguing, thus pulling people down the page even though the photos weren’t sufficiently communicative to facilitate navigation.</p>

<p>As for the visible list of navigation icons at the bottom of the page, these were less visible than the expandable menu at the top. People don’t usually expect navigation to be presented like this. And the text in the icons was small and not highly legible. However, these icons offered another shot at navigating the site if they users had scrolled too far down the page.</p>

<p>Most sites don’t want to dedicate such a significant amount of screen space on the page to navigation icons, and we agree that doing this is usually a misallocation of screen real estate. It’s better to make the main navigation visible and place it at the top of the page. However, to assist people who did not see the navigation at the top of the page and who scrolled down, we suggest repeating the navigation links in a fat footer over SupermarketHQ’s large, in-page icons. (Repeating navigation at the bottom of the page like SupermarketHQ did is pattern fairly common on mobile, where pages are long and people cannot easily scroll back to the top of the page. SupermarketHQ is in fact a responsive site, so it’s not surprising that it ported that pattern to the desktop.)</p>

<p>The other two sites where people used navigation significantly more than on Slate and Bloomberg (but less than on SupermarketHQ) were BBC and 7Digital: in both these cases, the navigation usage was over 43%. Both these designs had visible navigation placed in fairly standard position: at the top of the screen and on the left-hand side. It took people 18 seconds to use the navigation on BBC and 27 seconds on 7Digital (these numbers were not, however, statistically different from the other times to use the navigation on other sites).</p>

<figure class="caption"><img alt="" height="241" src="https://media.nngroup.com/media/editor/2016/07/20/bbc_desktop.png" width="720"/>
<figcaption><em>BBC desktop: Combination navigation including a visible navigation bar across the top of the page, and a More hidden-navigation item to the right of the navigation options</em></figcaption>
</figure>

<p> </p>

<figure class="caption"><img alt="" height="798" src="https://media.nngroup.com/media/editor/2016/07/20/7digital_desktop.png" width="720"/>
<figcaption><em>7Digital: Visible navigation options on the left side of page</em></figcaption>
</figure>

<p> </p>

<p>In addition to the visibility of the navigation, other traits that made the navigation easy to discover on BBC and 7Digital include:</p>

<ul>
	<li>The navigation appeared in a <a href="../killing-global-navigation-one-trend-avoid/index.php">standard, expected area</a>: either on at the top of the page or on the left-hand side. These designs are fairly common on the desktop and people have learned to use them proficiently.</li>
	<li>There was visual separation between the content area and the navigation —though a subtle horizontal grey line on BBC and through a different background color (gray vs. white) on 7Digital.</li>
	<li>On BBC, the lines between navigation options signal buttons and clickability.</li>
	<li>Also on BBC, having the search box on the same level with the navigation options may have slightly directed the eye towards these. Note that on the poorest performers —Slate and Bloomberg— navigation was also placed close to the search field. However, in both those cases the search field itself was not salient enough and did not attract the eye: Bloomberg used a low contrast text box with a placeholder that blended in with the rest of the elements in the blue banner, while Slate had a <a href="../magnifying-glass-icon/index.php">search icon</a> instead of a textbox. As a result, these sites did not get any benefit from placing search in the proximity of navigation.</li>
</ul>

<h2>Guidelines for Desktop Navigation Icons (Hidden Navigation)</h2>

<p>This article gave several examples and discussed the delicate minutia that can impact navigation usability for better or worse. Below we summarize some of the main guidelines for helping users discover and find navigation on desktop devices.</p>

<ol>
	<li>Use visible, exposed links for navigation instead of collapsing it under a hamburger, or any icon. Visible links are easier to discover, find, scan, and click.
	<ol style="list-style-type:lower-alpha;">
		<li>Position the links across the top or down the left side of pages.</li>
		<li>Make the navigation visually salient: use a background color that is different from the page background, <a href="../low-contrast/index.php">good contrast between the text and the background</a> in the navigation area. Potentially add rectangles around navigation options or other signifiers for clickability.</li>
		<li>Use clear, <a href="../wrong-information-scent-costs-sales/index.php">information-scent</a>-heavy labels for the navigation options.</li>
	</ol>
	</li>
	<li>If you choose to collapse the navigation under an icon, consider yourself warned: this will usually hurt your conversion rates because users will be less likely to navigate the site. To minimize damage, follow these guidelines:</li>
	<li>Don’t place navigation-menu icons in unexpected places, like within a right rail.</li>
	<li>Don’t group navigation icons with other unrelated design elements, such as the site logo or the utility navigation. On the big desktop screen, they could easily be considered one single unit.</li>
	<li>Place navigation at the top of pages, and if possible, near an exposed search field.</li>
	<li>Label the navigation menu with a word such as <em>Menu</em> or <em>Browse.</em></li>
	<li>Signal expandability by adding a small down arrow next to the menu icon or button (for added assurance).</li>
	<li>Signal clickability by making the menu icon stand out as a button (e.g., by using shadow, bevels, a rectangular shape, an outline, or a different color. If you adhere to a flat aesthetic, adhere to our <a href="../flat-design-long-exposure/index.php">guidelines for clickability in a flat world</a>.)</li>
</ol>

<p>Remember that previously-learned user behaviors, the total page layout, and even what seem to be minor visual elements can play a large role in making navigation discoverable. Our study teaches us that details matter: sites employing partially visible navigation can suffer from poor navigation discoverability (like Bloomberg) or from great navigation discoverability (like SupermarketHQ). Usability guidelines are always circumstantial: they depend on the context in which they are applied. When in doubt, you’ll get the best answer by running a usability test tailored to your design and to your users.</p>

<h3>Other Articles Related to the Study This Article was Based on</h3>

<p><a href="../find-navigation-mobile-even-hamburger/index.php">How to Make Navigation (Even Hamburger) Discoverable on Mobile</a></p>

<p><a href="../hamburger-menus/index.php">Hamburger Menus and Hidden Navigation Hurt UX Metrics</a></p>

<p><a href="../hidden-navigation-methodology/index.php">Hidden- and Visible-Navigation Study: Methodology</a></p>

<p><a href="../mobile-first-not-mobile-only/index.php">Mobile First Is NOT Mobile Only</a></p>

<p> </p>

<p>Learn more in our <a href="../../courses/navigation-design/index.php">full-day Navigation Design course</a>.</p>
</section>
  

  
  <section>
    
        
    
  </section>
  
  
  
  
    <section>
    <br>
    <p>
      <em>(An earlier version of this article was originally published July 24, 2016. The article was last updated and revised on July 24, 2016.)</em>
    </p>
    </section>
  
  

  


  <section>
    <br />
    <p class="share"><b>Share <span class="hide-for-small-down">this article</span>:</b> <a href="https://twitter.com/intent/tweet?url=http://www.nngroup.com/articles/find-navigation-desktop-not-hamburger/&amp;text=Beyond%20the%20Hamburger:%20How%20to%20Make%20Navigation%20Discoverable%20on%20Desktops&amp;via=nngroup">Twitter</a> | <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=http://www.nngroup.com/articles/find-navigation-desktop-not-hamburger/&amp;title=Beyond%20the%20Hamburger:%20How%20to%20Make%20Navigation%20Discoverable%20on%20Desktops&amp;source=Nielsen%20Norman%20Group">LinkedIn</a> | <a href="https://plus.google.com/share?url=http://www.nngroup.com/articles/find-navigation-desktop-not-hamburger/">Google+</a> | <a href="mailto:?subject=NN/g Article: Beyond the Hamburger: How to Make Navigation Discoverable on Desktops&amp;body=http://www.nngroup.com/articles/find-navigation-desktop-not-hamburger/">Email</a></p>
    <hr />
  </section>

  <footer class="l-subsection">
    
    
    <div class="learn-more">
      <h2>Learn More</h2>
      <p><a href="../subscribe/index.php">Subscribe to the weekly newsletter</a> to get notified about future articles.</p>
      </div>
    
    

    <div class="row show-for-small-down related-content">
      
      
      

      
      
        <div class="column small-12 topic-links">All articles about:
          <ul>
            
            <li><a href="../../topic/hamburger/index.php">hamburger</a></li>
            
            <li><a href="../../topic/navigation/index.php">Navigation</a></li>
            
          </ul>
        </div>
      
      

      
      
        <div class="column small-12">
          <h3>UX Conference Training Courses</h3>
           <ul class="no-bullet">
    
        <li><a href="../../courses/information-architecture/index.php">Information Architecture</a></li>
    
        <li><a href="../../courses/navigation-design/index.php">Navigation Design</a></li>
    
</ul>
        </div>
      
      

      
      
        <div class="small-12 column">
          <h3>Research Reports</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../reports/intranet-navigation-layout-and-text/index.php">Vol. 07: Navigation and Page Layout</a></li>
              
            
              
                <li><a href="../../reports/intranet-information-architecture-design-methods/index.php">Intranet Information Architecture Design Methods and Case Studies</a></li>
              
            
              
                <li><a href="../../reports/university/index.php">University Websites</a></li>
              
            
          </ul>
        </div>
      
      

      
      
        <div class="small-12 column">
          <h3>Online Seminars</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../online-seminars/intranet-users-find/index.php">Helping Intranet Users Find What They Need</a></li>
              
            
          </ul>
        </div>
      
      
    </div>

    <div class="hide-for-small-down row related-content">
      <div class="medium-6 columns">
        
        
          <div>
            <h3>Articles</h3>
            <ul class="no-bullet">
              
                
                <li><a href="../mobile-first-not-mobile-only/index.php">Mobile First Is NOT Mobile Only </a></li>
                
              
                
                <li><a href="../hamburger-menus/index.php">Hamburger Menus and Hidden Navigation Hurt UX Metrics</a></li>
                
              
                
                <li><a href="../support-mobile-navigation/index.php">Supporting Mobile Navigation in Spite of a Hamburger Menu</a></li>
                
              
                
                <li><a href="../top-10-enduring/index.php">Top 10 Enduring Web-Design Mistakes</a></li>
                
              
                
                <li><a href="../visual-indicators-differentiators/index.php">Visual Indicators to Differentiate Items in a List</a></li>
                
              
            </ul>
          </div>
        
        

        
          
          <div>
            <h3>Research Reports</h3>
            <ul class="no-bullet">
              
                
                  <li><a href="../../reports/intranet-navigation-layout-and-text/index.php">Vol. 07: Navigation and Page Layout</a></li>
                
              
                
                  <li><a href="../../reports/intranet-information-architecture-design-methods/index.php">Intranet Information Architecture Design Methods and Case Studies</a></li>
                
              
                
                  <li><a href="../../reports/university/index.php">University Websites</a></li>
                
              
            </ul>
          </div>
          
        

      </div>
      <div class="medium-6 columns">
        
          
          <div>
            <h3>UX Conference Training Courses</h3>
             <ul class="no-bullet">
    
        <li><a href="../../courses/information-architecture/index.php">Information Architecture</a></li>
    
        <li><a href="../../courses/navigation-design/index.php">Navigation Design</a></li>
    
</ul>
          </div>
          
        
        
          
          <div>
            <h3>Online Seminars</h3>
            <ul class="no-bullet">
            
                <li><a href="../../online-seminars/intranet-users-find/index.php">Helping Intranet Users Find What They Need</a></li>
            
            </ul>
          </div>
          
        
      </div>
    </div>
  </footer>
</article>

        </div>

      

    
  </div>
  <div class="row">
    
      <div class="column small-12 show-for-small-down l-navrail announcements">
        
          
        
      </div>
    
    
  </div>

    </div>
    




<footer id="page-footer">
    <div class="row hide-for-print">
        <div class="column small-12 medium-6">
            <section class="footer-popular">
                
                <h1>Popular Topics</h1>
                <div style="float: left;  width: 15em; display: block;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/applications/index.php">Application Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/e-commerce/index.php">E-Commerce Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/eyetracking/index.php">Eyetracking </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/information-architecture/index.php">Information Architecture </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/intranets/index.php">Intranet Design </a></li>
	<li><a href="../../topic/interaction-design/index.php">Interaction Design </a></li>
</ul>
</div>

<div style="float: left;  width: 15em; display: block; list-style: none;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/mobile-and-tablet-design/index.php">Mobile and Tablet Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/credibility-persuasion/index.php">Persuasive Design</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/research-methods/index.php">Research Methods</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/user-testing/index.php">User Testing </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/web-usability/index.php">Web Usability </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/writing-web/index.php">Web Writing &amp; Content Strategy </a></li>
</ul>
</div>

<p>&nbsp;</p>

                
            </section>
        </div>

        <div class="column small-12 medium-6">
            <div class="row">
                <div class="column small-6">
                    <section class="footer-about">
                        <h1>Follow Us</h1>
                        <ul>
	<li><a href="../subscribe/index.php">Subscribe to our weekly UX newsletter</a>,&nbsp;Jakob Nielsen&#39;s Alertbox</li>
	<li>Visit Don Norman&#39;s website, <a href="http://www.jnd.org/"> www.jnd.org </a></li>
	<li>Visit Bruce Tognazzini&#39;s website, <a href="http://www.asktog.com/"> www.asktog.com </a></li>
	<li style="padding-top: 6px;"><a class="twitter-follow-button" data-show-count="false" data-size="large" href="https://twitter.com/nngroup"><img alt="Follow @nngroup on Twitter" height="20" src="https://media.nngroup.com/media/editor/2016/12/30/twitter-follow-button.png" style="width: 117px; height: 20px;" width="117" /></a></li>
	<li style="padding-top: 6px;"><img alt="" height="14" src="https://media.nngroup.com/media/editor/2016/07/26/feed-icon-14x14.png" style="vertical-align:text-top;width:14px;margin-right:.25em;" width="14" /><a href="../../feed/rss/index.php">Subscribe to our RSS feed</a></li>
</ul>

                    </section>
                </div>

                <div class="column small-6">
                    <section class="footer-about right-column">
                        <h1>About
</h1>
                        <ul class="no-bullet">
	<li><a href="../../about/contact/index.php">Contact information</a></li>
	<li><a href="../../about/index.php">About Us</a></li>
	<li><a href="../../about/why-nng/index.php">Why NN/g? </a></li>
	<li><a href="../../return-policy/index.php">Return Policy</a></li>
</ul>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="copyright column small-12">
            <p><a href="../../copyright-and-reprint-info/index.php">Copyright</a> &copy; 1998-2017 Nielsen Norman Group, All Rights Reserved.</p>
        </div>
    </div>
</footer>


    


        <script src="https://media.nngroup.com/static/vendor/fastclick/js/fastclick.min.js"></script>
        <script src="https://media.nngroup.com/static/vendor/foundation/js/foundation.min.js"></script>
        <script src="http://media.nngroup.com/static/js/app.js"></script>
        <script src="http://media.nngroup.com/static/js/scroll-to-fixed.js"></script><script>var externalLinks = document.querySelectorAll('a[href^="http"]');for (var i = externalLinks.length-1; i >= 0; i--) {externalLinks[i].addEventListener("click", function(event) { alert("External links are disabled for this test."); event.preventDefault(); }, false);}var mailLinks = document.querySelectorAll('a[href^="mailto"]');for (var i = mailLinks.length-1; i >= 0; i--) {mailLinks[i].addEventListener("click", function(event) { alert("Email links are disabled for this test."); event.preventDefault(); }, false);}var externalForms = document.querySelectorAll('form');for (var i = externalForms.length-1; i >= 0; i--) {externalForms[i].addEventListener("submit", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false); externalForms[i].addEventListener("click", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false);}</script>

        <script>
            
            $(function() {
                $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
                $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
            });
        </script>

        
<script src="https://media.nngroup.com/static/js/articles.js"></script>


        
        <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1072699960;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072699960/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
    </body>

<!-- Mirrored from www.nngroup.com/articles/find-navigation-desktop-not-hamburger/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:13:06 GMT -->
</html>
