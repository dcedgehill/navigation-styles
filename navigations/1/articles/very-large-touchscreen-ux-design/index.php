<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->





    
<!-- Mirrored from www.nngroup.com/articles/very-large-touchscreen-ux-design/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:08:09 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1026.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"licenseKey":"1a30ef9931","queueTime":7,"beacon":"bam.nr-data.net","errorBeacon":"bam.nr-data.net","applicationID":"2048992","transactionName":"NFxSMRBUX0EDUkIPDA0WdhAMVkVbDV8ZBxEXUFMJB0YfRAtUQRVZIktEDAFZVHYHRVcPD01eVRE=","applicationTime":606,"agent":""}</script>
        <title>A Few Mobile UX Design Skills Help With Very Large Touchscreen UX Design</title><meta property="og:title" content="A Few Mobile UX Design Skills Help With Very Large Touchscreen UX Design" />
  
        
        <meta name="description" content="Users’ field of vision, arm motion, affordance, and privacy are a few of the design considerations for very large touchscreens (kiosks and other nonmobile use).">
        <meta property="og:description" content="Users’ field of vision, arm motion, affordance, and privacy are a few of the design considerations for very large touchscreens (kiosks and other nonmobile use)." />
        
  
        
	
        
        <meta name="keywords" content="Fitts&#39; Law, Fitts&#39;s law, Fitt&#39;s law, field of vision, arm motion, affordance, signifier, privacy, very large touchscreen, kiosk, table display, nonmobile, mobile, screen angle, 72-inch, gesture, Myo Gesture Control Armband, Myo, Gesture Control Armband,  Dorothy Shamonsky, ViewPoint">
        

        

        <meta name="robots" content="noarchive">
        <meta name="viewport" content="width=device-width">

        <link rel="search" type="application/opensearchdescription+xml" title="NN/g Search" href="../../opensearch.xml" />
        <link rel="shortcut icon" href="http://media.nngroup.com/static/img/favicon.ico" />

        <link rel="stylesheet" href="https://media.nngroup.com/static/vendor/font-awesome/css/font-awesome.min.css">

        
        <link rel="stylesheet" href="https://media.nngroup.com/static/css/7232a960f1df.css" type="text/css" />
        <link rel="stylesheet" href="http://fast.fonts.com/t/1.css?apiType=css&amp;projectid=c5846260-9def-46ec-9407-0f1349bd2d64" type="text/css">

        <script src="https://media.nngroup.com/static/vendor/modernizr/js/modernizr.min.js"></script>

        <script type="text/javascript" src="https://media.nngroup.com/static/js/3283be5def74.js"></script>
        

        

        
        
        <!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=229034,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('http://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->


<!-- Google Analytics Tracking Code for http://nngroup.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23132681-1', 'nngroup.com');
  ga('require', 'linkid', 'https://www.nngroup.com/articles/very-large-touchscreen-ux-design/linkid.js');
  ga('require', 'displayfeatures');

// Optimizely Universal Analytics Integration
 window.optimizely = window.optimizely || [];
 window.optimizely.push("activateUniversalAnalytics");

  ga('send', 'pageview');

</script>
<script>
/**
* Function to track a click on an outbound link in Google Analytics.
*/
function trackOutboundLink(label, url) {
    if(window.ga && ga.loaded) {
        ga('send', 'event', 'outbound', 'click', label, {'hitCallback': function(){document.location = url;}
        });
    } else {
        document.location = url;
    }
}
/**
* Function to track a click on a link in the article announcement area.
*/
$('#articleAnnouncement a').on('click', function(){
  ga('send', 'event', 'link', 'click', 'articleAnnouncement');
});
</script>

<script type="text/javascript">
 setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2421.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



<!-- Hotjar Tracking Code for http://nngroup.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:121605,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

        <script src="https://media.nngroup.com/static/js/analytics/track_outbound_link.js"></script>
        
        
        
    </head>
    <body class="location-articles articles-detail">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
    <header id="page-header">
        <div id="cart" class="shopping-cart mobile-cart show-for-small-down"></div>
        <div class="row">
            <div id="masthead" class="masthead small-12 medium-7 column">

                <!-- The button to toggles menu on small screen -->

                <h1><a href="../../index.php"><img src="http://media.nngroup.com/static/img/logo-wordmark.png" width="74" height="30"> Nielsen <span class="norman">Norman</span> Group</a></h1>
                <p class="tagline show-for-medium-up">Evidence-Based User Experience Research, Training, and Consulting</p>

                <!-- The button to toggles form search on small screen -->


                <div style="clear: right;width:  100%;height: 1px;"></div><?php define('ROOT_PATH',$_SERVER["DOCUMENT_ROOT"] . '/apps/navigation_styles/navigations/1'); ?><?php include(ROOT_PATH)."/navigation.php"; ?>

                <!-- Drop down menu on small screen -->
                <div id="hideNav" class="show-for-small-down">
                  
                  <ul class="menu-content">
                    <li><a href="../../index.php">Home</a></li>
                    <li class="has-submenu">
                      <a>Training</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../training/index.php">Overview</a></li>
                        
<li id="nav-ux-conference"><a href="../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../online-seminars/index.php"><span>Online Seminars</span></a></li>

                      </ul>
                    </li>
                    <li class="has-submenu">
                      <a>Consulting</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
  <li><a href="../../consulting/index.php">Overview</a></li>
  
  <li><a href="../../consulting/benchmarking/index.php">Benchmarking</a></li>
  
  <li><a href="../../consulting/expert-design-review/index.php">Expert Design Review</a></li>
  
  <li><a href="../../consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
  
  <li><a href="../../consulting/usability-testing/index.php">Usability Testing </a></li>
  
  <li><a href="../../consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
  
  <li><a href="../../in-house-training/index.php">In-House Training</a></li>
</ul>

                    </li>

                    <li><a href="../../reports/index.php">Reports</a></li>
                    <li><a href="../index.php">Articles</a></li>
                    <li class="has-submenu"><a>About NN/G</a><i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../about/index.php">Overview</a></li>
                        <li><a href="../../people/index.php">People</a></li>
                        <li><a href="../../about/why-nng/index.php">Why NN/g?</a></li>
                        <li><a href="../../about/contact/index.php">Contact</a></li>
                        <li><a href="../../news/index.php">News</a></li>
                        <li><a href="../../about/history/index.php">History</a></li>
                        <li><a href="../../books/index.php">Books</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                </div>

                <hr class="header-divider show-for-small-only"/>


            </div>
       </div>

        <div id="hideNav">            <nav class="primary-nav small-12 column">
                <ul>
                    <li><a class="navitem" id="nav-home" href="../../index.php">Home</a></li>
                    <li><a class="navitem" id="nav-training" href="../../training/index.php">Training</a></li>
                    <li><a class="navitem" id="nav-consulting" href="../../consulting/index.php">Consulting</a></li>
                    <li><a class="navitem" id="nav-reports" href="../../reports/index.php">Reports</a></li>
                    <li><a class="navitem" id="nav-articles" href="../index.php">Articles</a></li>
                    <li><a class="navitem" id="nav-about" href="../../about/index.php">About NN/G</a></li>
                </ul>
            </nav>
            <nav class="secondary-nav small-12 medium-12 large-12 columns">
                
                
            </nav>
        </div>
    </header>
    <div class="l-content">
    
  

  

  <div class="row">
    
      
      <section class="small-12 medium-3 columns collapsable-content l-navrail articles-left-column">
        <h1 class="show-for-small-down collapse-link mobile-browse-header">Browse by Topic and Author</h1>
          <div class="row collapsable">

            
            <div class="small-6 medium-12 columns">
              <section id="article-popular-topics" style="display: none;">
                
                <h1>Topics</h1>
                <ul id="article-popular-topics">
                    
                    <li>
                        
                        <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/intranets/all/index.php">Intranets</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/user-testing/all/index.php">User Testing</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                        
                    </li>
                    
                    <li><a href="#" id="article-see-all-topics"><strong>See all topics&hellip;</strong></a></li>
                </ul>
                
              </section>
              <section id="article-all-topics" style="display: none;">
                
                <h1>
                  <span class="show-for-medium-up">All Article Topics</span>
                  <span class="show-for-small-down">All Topics</span>
                  (<a href="#" id="article-hide-all-topics">hide</a>)
                </h1>
                <ul>
                  
                  <li>
                    
                    <a href="../../topic/accessibility/all/index.php">Accessibility</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/agile/all/index.php">Agile</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/analytics-and-metrics/all/index.php">Analytics &amp; Metrics</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/applications/all/index.php">Application Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/b2b-websites/all/index.php">B2B Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/behavior-patterns/all/index.php">Behavior Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/branding/all/index.php">Branding</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/content-strategy/all/index.php">Content Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/corporate-websites/all/index.php">Corporate Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/design-patterns/all/index.php">Design Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-design-process/all/index.php">Design Process</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/email/all/index.php">Email</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/eyetracking/all/index.php">Eyetracking</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/heuristic-evaluation/all/index.php">Heuristic Evaluation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/human-computer-interaction/all/index.php">Human Computer Interaction</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-ideation/all/index.php">Ideation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/information-architecture/all/index.php">Information Architecture</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/interaction-design/all/index.php">Interaction Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/international-users/all/index.php">International Users</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/intranets/all/index.php">Intranets</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-management/all/index.php">Management</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/navigation/all/index.php">Navigation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/non-profit-websites/all/index.php">Non-Profit Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/personas/all/index.php">Personas</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/persuasive-design/all/index.php">Persuasive Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/prototyping/all/index.php">Prototyping</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/psychology-and-ux/all/index.php">Psychology and UX</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/research-methods/all/index.php">Research Methods</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/search/all/index.php">Search</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/social-media/all/index.php">Social Media</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/strategy/all/index.php">Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/user-testing/all/index.php">User Testing</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-teams/all/index.php">UX Teams</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/visual-design/all/index.php">Visual Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/writing-web/all/index.php">Writing for the Web</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/young-users/all/index.php">Young Users</a>
                    
                  </li>
                  
                </ul>
                
              </section>
            </div>
            <script>
              /* Determine which topics list will be displayed: the full or the
               * partial.  We are inlining this close to the list itself to
               * prevent rendering delays on the client
               */
              var topic_list = $.cookie("article-topics");

              var popular_topics = $('#article-popular-topics');
              var all_topics = $('#article-all-topics');

              if(topic_list == 1) {
                  popular_topics.hide();
                  all_topics.show();
              } else {
                  popular_topics.show();
                  all_topics.hide();
              }
            </script>
            

            
            <div class="small-6 medium-12 columns">
              
              <section id="article-initial-authors">
                  <h1>Author</h1>
                  <ul>
                    
                    <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                    
                    <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                    
                    <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                    
                    <li><a href="#" onClick="$('#article-initial-authors').hide(); $('#article-all-authors').show();"><strong>See all authors&hellip;</strong></a></li>
                  </ul>
              </section>
              <section id="article-all-authors" style="display: none;">
                <h1>
                  All Authors
                  (<a href="#" id="article-hide-all-authors">hide</a>)
                </h1>
                <ul>
                  
                  <li><a href="../author/raluca-budiu/index.php">Raluca Budiu</a></li>
                  
                  <li><a href="../author/patty-caya/index.php">Patty Caya</a></li>
                  
                  <li><a href="../author/susan-farrell/index.php">Susan Farrell</a></li>
                  
                  <li><a href="../author/therese-fessenden/index.php">Therese Fessenden</a></li>
                  
                  <li><a href="../author/kim-flaherty/index.php">Kim Flaherty</a></li>
                  
                  <li><a href="../author/sarah-gibbons/index.php">Sarah Gibbons</a></li>
                  
                  <li><a href="../author/aurora-harley/index.php">Aurora Harley</a></li>
                  
                  <li><a href="../author/kate-kaplan/index.php">Kate Kaplan</a></li>
                  
                  <li><a href="../author/page-laubheimer/index.php">Page Laubheimer</a></li>
                  
                  <li><a href="../author/angela-li/index.php">Angie Li</a></li>
                  
                  <li><a href="../author/hoa-loranger/index.php">Hoa Loranger</a></li>
                  
                  <li><a href="../author/kate-meyer/index.php">Kate Meyer</a></li>
                  
                  <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                  
                  <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                  
                  <li><a href="../author/kara-pernice/index.php">Kara Pernice</a></li>
                  
                  <li><a href="../author/christian-rohrer/index.php">Christian Rohrer</a></li>
                  
                  <li><a href="../author/amy-schade/index.php">Amy Schade</a></li>
                  
                  <li><a href="../author/katie-sherwin/index.php">Katie Sherwin</a></li>
                  
                  <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                  
                  <li><a href="../author/kathryn-whitenton/index.php">Kathryn Whitenton</a></li>
                  
                </ul>
              </section>
              
            </div>
            
          </div>

          
          <section class="hide-for-small-down">
            
              
              <h1>Recent Articles</h1>
              
              <ul>
                  
                  <li><a href="../seamless-cross-channel/index.php">Seamlessness in the Omnichannel User Experience</a></li>
                  
                  <li><a href="../brand-intention-interpretation/index.php">Experience Design: Bridging Brand Intention and Brand Interpretation</a></li>
                  
                  <li><a href="../eyetracking-tasks-efficient-scanning/index.php">Scanning Patterns on the Web Are Optimized for the Current Task</a></li>
                  
                  <li><a href="../flat-design-best-practices/index.php">Flat-Design Best Practices</a></li>
                  
                  <li><a href="../ux-user-stories/index.php">Accounting for UX Work with User Stories in Agile Projects</a></li>
                  
                  <li><a href="../index.php"><strong>See all articles&hellip;</strong></a></li>
              </ul>
              
            
          </section>
          

          
          
          <section class="hide-for-small-down">
            <h1>Popular Articles</h1>
            
            <ul>
                
                <li><a href="../usability-101-introduction-to-usability/index.php">Usability 101: Introduction to Usability</a></li>
                
                <li><a href="../top-10-mistakes-web-design/index.php">Top 10 Mistakes in Web Design</a></li>
                
                <li><a href="../how-users-read-on-the-web/index.php">How Users Read on the Web</a></li>
                
                <li><a href="../f-shaped-pattern-reading-web-content/index.php">F-Shaped Pattern For Reading Web Content</a></li>
                
                <li><a href="../ten-usability-heuristics/index.php">10 Usability Heuristics for User Interface Design</a></li>
                
                <li><a href="../intranet-design-2016/index.php">10 Best Intranets of 2016</a></li>
                
                <li><a href="../which-ux-research-methods/index.php">When to Use Which User-Experience Research Methods</a></li>
                
                <li><a href="../response-times-3-important-limits/index.php">Response Times: The 3 Important Limits</a></li>
                
                <li><a href="../why-you-only-need-to-test-with-5-users/index.php">Why You Only Need to Test with 5 Users</a></li>
                
                <li><a href="../page-fold-manifesto/index.php">The Fold Manifesto: Why the Page Fold Still Matters</a></li>
                
            </ul>
          </section>
          
          

          
            <section class="hide-for-small-down">
              
            </section>
          

      </section>
      

      
        <div class="small-12 medium-9 columns content-body">
          




<article class="l-subsection alertbox-article-content article-detail-bl">
  
  <header>
    <div class="l-subsection">
      <h1>Very Large Touchscreens: UX Design Differs From Mobile Screens</h1>
      <div class="author-meta">by
        
          
            
            <a href="../author/kara-pernice/index.php">Kara Pernice</a>
            
          
        on  August 23, 2015
        
      </div>

      

      
      <div class="topic-links">
        Topics:
        
          <ul>

  <li><a href="../../topic/mobile-and-tablet-design/index.php">Mobile &amp; Tablet</a></li>

</ul>
        
      </div>
      
    </div>
  </header>
  

  
  
  <section>
    <p class="lede"><strong>Summary:</strong> Only a few mobile-design skills and design recommendations translate well to designing for very large touchscreens, as found in kiosks and other nonmobile use cases. Users’ field of vision, arm motion, affordance, and privacy are a few of the different considerations for such screens with up to 380 times the area of a smartphone.</p>
    <br />
  </section>
  
  

  
  <section id="article-body"><p>As smartphones continue to improve at rapid rates, tasks that used to be inconceivable on small touchscreens have become (often) quite simple and acceptable. We can read, buy, work, and collaborate… well sort of. Sharing information via a phone is easy, but sharing the small screen itself with the person by your side is not. If we need to look at the same item at the same time together, a larger screen or even a large print will easily win out over a 4-inch display. But maybe in the not-too-distant future tiny powerful projectors will come built in on all of our phones and <a href="../smartwatch/index.php">watches</a>, facilitating group viewing on any plain wall. In this scenario, the input device may still be the phone as a remote control. Or maybe <a href="../human-body-touch-input/index.php">we will be able to use the human body</a> or a gestural device, like the <a href="https://www.thalmic.com/en/myo/">Myo Gesture Control Armband</a>, for easier input, enabling us to “touch” the links on the wall and have the UI react.</p>

<figure class="caption"><img alt="Myo gesture control armband" height="250" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2015/07/27/myoblack-square.png" width="250"/>
<figcaption><em>Image of a black <a href="http://www.thalmic.com/en/myo/">Myo</a> Gesture Control Armband from https://store.myo.com/. Users would wear this device on their arms, “train” the UI on the computer, and apps would then recognize the users’ gestures.</em></figcaption>
</figure>

<p>But until this day arrives, let’s consider very large touchscreens of today, for which the screen is both the input and the <a href="../responsive-web-design-definition/index.php">output</a> device.</p>

<p>I had the pleasure of interviewing Dorothy Shamonsky who is Lead UX Designer for <a href="http://www.viewpointkiosks.com/">ViewPoint</a>, a software provider for touchscreen kiosks. Shamonsky works on the interface design for large touchscreens, including some as large as 72-inch.</p>

<figure class="caption"><img alt="Dorothy Shaonsky, Lead UX Designer for ViewPoint" height="333" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2015/07/27/dshamonsk_cropped.jpg" width="350"/>
<figcaption><em><strong>Dorothy Shamonsky</strong>, Lead UX Designer for <a href="http://www.viewpointkiosks.com/">ViewPoint</a>, works on the design of large touchscreen kiosks.</em></figcaption>
</figure>

<p>To get a sense of how big a 72-inch screen is, stretch your arms straight out by your sides. This is about the distance from your fingertips on one hand to those on your other hand. And while the screen is about 380 times larger than that of the average smart phone, some design principles for a phone touchscreen apply to the huge touchscreen. Shamonsky provided these and many more insights and design ideas for large, immovable touchscreens. These are described in this article.</p>

<h2>Design Recommendations for Both Small and Very Large Touchscreens</h2>

<p>Whether designing for a <a href="../scaling-user-interfaces/index.php">7- or 70-inch touchscreen</a>, there are <a href="../mobile-ux/index.php">many guidelines</a> that hold true. Some of the most important ones include the following:</p>

<ol>
	<li>Allow <a href="http://www.jnd.org/dn.mss/gestural_interfaces_a_step_backwards_in_usability_6.php">natural gestures</a>.</li>
	<li>Minimize the <a href="../interaction-cost-definition/index.php">interaction cost</a> of tapping, typing, and moving between screens.</li>
	<li>Offer user feedback via simple <a href="../animation-usability/index.php">animations</a>.</li>
	<li>Make it easy-to-decipher which elements are <a href="../clickable-elements/index.php">tappable</a>.</li>
	<li>Make targets easy to tap.</li>
	<li>Offer <a href="../low-contrast/index.php">legible text</a> and graphics.</li>
</ol>

<h2>What’s Different for Very Large Screens</h2>

<p>Beyond these commonalities, there are also some important differences between <a href="../mobile-usability-update/index.php">small</a> and very large touchscreens, both in terms of user behavior and design recommendations. Here we discuss a few of them, directed at both the <strong>macro</strong> level (how to design the screen so users know how they’re supposed to interact with it) and the <strong>micro</strong> level (how to design the specific UI elements so that they are noticeable).</p>

<h3>Affordance and Signifiers at the Macro Level</h3>

<p>Most people have learned to touch a smartphone screens, as well as screens that are commonly touch enabled at ATMs, gas pumps, ticket kiosks, or museums. But in some environments people do not know that a large screen is touch enabled and, due to this poorly signaled <a href="http://www.jnd.org/dn.mss/affordances_and.php">affordance</a>, they are reluctant to touch large displays when they are not sure whether they will <a href="../wrong-information-scent-costs-sales/index.php">enjoy any benefit for the effort</a>.</p>

<p>According to Shamonsky’s experiences, “Displays mounted on a wall or a stand tend to remind people of a TV and don’t imply that they are touch enabled. Instead users must rely on kiosk-specific cues such as <strong>location, angle of screen, and signage</strong> to figure out - that a large-screen interface is touch enabled.”</p>

<p>Shamonsky suggests a particular <a href="http://jnd.org/dn.mss/signifiers_not_affordances.php">signifier</a>: position the display at a 45-degree angle on the wall, with the top of the display leaning toward the wall and the bottom toward the user. “This tends to reassure users that the screen is touch enabled, especially in the absence of a keyboard or mouse. Tabletop displays also signal touch interactivity. But if you want to entertain and attract users from across a room, and encourage shared interaction, <strong>a wall-mounted screen is much more dramatic and preferable to a tabletop display</strong>.”</p>

<p>Large displays offer plenty of screen real estate for interface controls, so designers are not challenged to fit controls into the interface. However, <strong>since UI elements need to be larger to be seen and interacted with on a large screen, a big screen can fill up surprisingly quickly.</strong> Thus, a very large touchscreen design suffers the same threat of being over filled that smaller screens do. Designers should avoid clutter and should heed Edward Tufte’s recommendation to <a href="../content-chrome-ratio/index.php">mind the ratio of content versus other UI elements</a>. (<a href="http://thedoublethink.com/2009/08/tufte’s-principles-for-visualizing-quantitative-information/">Tufte’s Data-Ink Ratio</a> is usually applied to graphs but can also be telling about screen real estate and content value.) Similarly, our <a href="../../books/eyetracking-web-usability/index.php">eyetracking research</a> reveals that <a href="https://books.google.com/books?id=EeQhHqjgQosC&amp;pg=PA396&amp;dq=eyetracking+web+usability+page+clutter&amp;hl=en&amp;sa=X&amp;ved=0CDEQ6AEwAGoVChMIwoe6ja_ZxgIVgTY-Ch0hNgq6#v=onepage&amp;q=eyetracking%20web%20usability%20page%20clutter&amp;f=false">page density accounts for 8% of the variability in how people look at web pages</a>. In other words, good content, understandable UI elements, and <a href="../characteristics-minimalism/index.php">less cluttered pages</a> <a href="../../books/emotional-design/index.php">engage people</a> more than cluttered pages do.</p>

<h3>Recommended signifiers to indicating that a large screen is a touchscreen:</h3>

<ul style="margin-left:40px">
	<li>If the display is affixed to a wall, angle the screen 45 degrees.</li>
	<li>Choose a tabletop display (flat or slightly angled).</li>
	<li>Prompt to touch with words on the screen and other touchable-looking items on the screen.</li>
	<li>Offer signage near the screen prompting to touch, such as modest placard that says, “This is a touchscreen”.</li>
	<li>Implement a timeout event that, after a particular amount of idle time, starts an “attract mode” that encourages people to try the device.</li>
	<li>Make the UI engaging and interesting so there are often other people using it people learn how to use it by <a href="../social-proof-ux/index.php">seeing others</a> using the touchscreen.</li>
	<li>We could say not to clean the screen or use a material that retains fingerprints and smudges longer, but that would be gross. Still, fingerprints on the screen are signifiers.</li>
	<li>Measure the clutter amount in the screen design. Remove screen content with little or no value so the important actions and features are more visible.</li>
</ul>

<h3>Touch Targets and Signifiers at the Micro Level</h3>

<p>On <a href="../../courses/usability-mobile-websites-apps/index.php">smartphones, interface controls</a> are sometimes hidden in <a href="../../courses/vis-mob-1/index.php">small controls or hard-to-decipher icons</a>. But it is at least possible to take in the whole screen with one glance. With a very large screen, however, <strong>the large field of view makes it difficult to see and notice interface elements</strong>. Users must move their heads around, not just the angle of their eye gazes, and must engage their necks to see all the parts of the interface.</p>

<p>Shamonsky describes, “It seems counterintuitive that interface controls would be harder to find on a large screen, but that is my experience observing users. So the designer has a different challenge with large screens than with small screens, which is to make interface elements noticeable, without being obnoxious.”</p>

<p>People interacting with very large touchscreens are usually farther away from the screen than are when using a phone, so <strong>this affects what they can see</strong>. They are most likely to:</p>

<ul>
	<li>reach for the interface at extended arm’s length</li>
	<li>stand up</li>
	<li>have flexibility to move very close or step back to look at the screen.</li>
</ul>

<p>Shamonsky notes, “Stepping back does provide a better ability to see the entire display more easily, and there is a tendency for users to do that as they interact with the device.”</p>

<p>In addition to the vision considerations, designers need to consider reach and touch accuracy on a very large screen on which people can tap, swipe, flick, drag, pinch close, and pinch open. But unlike a phone that people can hold comfortably in their hands, a large screen is:</p>

<ul>
	<li>mounted firmly</li>
	<li>cannot be picked up by the user</li>
	<li>often cannot be tilted or swiveled at all</li>
	<li>may be flat against the wall</li>
</ul>

<figure class="caption"><img alt="person is using 2 fingers to swipe on the 72-inch ViewPoint touchscreen" height="374" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2015/07/27/viewpoint_kiosk-swipe_big.jpg" width="500"/>
<figcaption><em>A person is using 2 fingers to swipe on the 72-inch ViewPoint touchscreen. “As a designer, one of my goals was to take full advantage of the visceral appeal of interacting with touch on a very large display,” says Shamonsky.</em></figcaption>
</figure>

<p>When designing links and buttons for computer screens and phones we already consider <a href="http://www.asktog.com/columns/022DesignedToGiveFitts.php">Fitts’s Law</a>, which says that the time to acquire a target is <a href="../../courses/hci/index.php"><strong>a function of the distance to and size of</strong> the target</a>. With mobile-phone design, because the screen is so small, all points are almost at the same <a href="../../courses/vis-mob-2/index.php">distance from our fingertips</a>, so we mostly focus on <a href="../../courses/vis-mob-2/index.php">target size</a>.</p>

<p>But with very large touchscreens, the distance from the target becomes more relevant. In particular, designers must consider the human physical traits and capabilities such as:</p>

<ul>
	<li>arm reach</li>
	<li>arm motion</li>
	<li>hand touch with palm or multiple fingertips</li>
	<li>height of the person</li>
</ul>

<p>Of course, the target size still remains essential in ensuring accurate reach. Shamonsky shared, “Comfortable target sizes on large displays are more complicated to determine, although the large screen gives the designer much more space to err on the side of larger target sizes. A frequently used target may need to be larger so people don’t have to struggle and slow down their arm momentum to hit it accurately. A rarely used target works at a minimum size [because the nuisance associated with the use is not experienced often].”</p>

<p>Another factor to consider is user fatigue, observed Shamonsky. “Although an expansive screen is seductive and offers many advantages over a small screen, it can be tiring to use,” she says. “<strong>Large touchscreens engage the whole body</strong> since the large space usually requires the use of arms to reach interface controls. The physical effort involved in interacting with a very large screen is significant enough that it <strong>becomes noticeably tiring when the task goes beyond casual browsing</strong>. In an editorial in <em>Scientific American</em>, David Pogue alluded to the <a href="http://www.scientificamerican.com/article/why-touch-screens-will-not-take-over/">effect of extended use of touchscreen as ‘gorilla arm’</a>.”</p>

<p>When comparing a drag gesture on a very large screen to the same drag gesture on a small screen, interface elements feel harder to move on the large screen. Shamonsky observed that, “Tap is not an issue. But since <strong>swipe, flick, drag, and pinch and unpinch require continuous effort applied to a screen element, those gestures can feel more strenuous on a large display</strong>.”</p>

<p>Finally, the angle at which the screen is positioned can also alter how easily these gestures can be made, according to Shamonsky. “Especially when the screen is positioned at a 90-degree angle [hung so it is flat on the wall], it can negatively affect the user’s accuracy when dragging, pinching and unpinching, and typing on an onscreen keyboard. Allowing people to move the keyboard affords flexibility.”</p>

<p><strong>Recommendations for gestures and text input, and for signifiers indicating controls on a large touchscreen:</strong></p>

<ul>
	<li>Make large interface elements.</li>
	<li>Add animations and slight movement to elements to attract the user’s eye.</li>
</ul>

<ul>
	<li>Ensure that text, images, and buttons are legible and decipherable, especially when standing at arm’s length from the screen. Create larger targets that also account for arm motion, arm reach, and user height variables.</li>
	<li>Make the on-screen keyboard a moveable element so users may drag it and use it in an area on the screen that is most comfortable for them—independent on their height.</li>
	<li>Adjust the drag, acceleration, and deceleration on interface elements to make them feel lighter and easy to control.</li>
</ul>

<h3>Privacy and Screen Sharing</h3>

<p><strong>Sharing the screen occurs naturally</strong> when there are multiple people and a very large screen present. Shamonsky explains that sharing “begins with a user seeing a large display across a room and observing others interact with it. As the user approaches the device, she may even engage with the current users and comment to them about what she is observing.” Then, depending on the actual size of the device, there is likely enough screen space for 2 or 3 people to interact at the same time.</p>

<figure class="caption"><img alt="Two people collaborate using the ViewPoint kiosk at an auto dealership" height="281" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2015/07/27/viewpoint_kiosk2.jpg" width="500"/>
<figcaption><em>Two people collaborate using the ViewPoint kiosk at an auto dealership. The 72-inch touchscreen makes used-car inventory more visible and tangible, and, in the end, easier and more fun to find the right car.</em></figcaption>
</figure>

<p>The negative flipside to the easy screen sharing is that <strong>there is little or no privacy</strong> for users of large, public touch-screens. Shamonsky says that a screen angled at 45 degrees, “does offer a small amount of privacy from those standing farther back, but a vertical screen can only offer privacy of scale.”</p>

<p><strong>Recommendations for privacy</strong></p>

<ul>
	<li>Consider the type of information you are asking people to enter, and the environment in which they will use the touch screen. Based on this, decide whether the information should be asked for at all.</li>
	<li>If asking someone to enter something private, for example an email address, consider making the keyboard itself fairly large but displaying text that is typed as small as possible (so it is only legible for people standing close to the display).</li>
</ul>

<h3>Freedom and Novelty of a Large Screen</h3>

<p>“One of the most striking qualities of a large touchscreen,” explains Shamonsky, “is the size of the space in which information is presented, and the option to ‘play’ at interacting. It is inherently satisfying to have an expansive view of something. It is also appealing to have a large space in which to gesture with your whole arm, to move things around, and to share the space with others.”</p>

<p>Shamonsky also explained an unexpected attribute of large touchscreens: drawing out the entertainer in the user. “Obviously, others can observe what you are doing, so <strong>you become a performer of sorts</strong> with the application, which can be fun,” she says.</p>

<p><strong>Recommendations for supporting “performers”</strong></p>

<p>Ensure that the users are as comfortable as possible “performing”. You can do this by making them feel competent with designs that maximize ease of use. Adding in cool interface effects can always help a performer look impressive.</p>

<h2>Conclusion</h2>

<p>The table below summarizes some of the user behaviors with smart phones, <a href="../large-touchscreens/index.php">large touch screens</a> such as 24-in tablets, and very large touchscreens. (Besides their size, the main difference between large and very large touchescreens is that the large touchscreens can still be moved around by the user relatively easily.) Notice that large and very large touch screens present the most similar challenges.</p>

<table border="1" cellpadding="0" cellspacing="0" style="width:100%;">
	<tbody>
		<tr>
			<td style="width:25%;"> </td>
			<td style="width:25%;"><strong>Smart Phone</strong></td>
			<td style="width:25%;"><strong>Large Touch Screen</strong></td>
			<td style="width:25%;"><strong>Very Large Touch Screen</strong></td>
		</tr>
		<tr>
			<td><strong>Sample device</strong></td>
			<td>iPhone</td>
			<td>Small kiosk or <a href="../large-touchscreens/index.php">Nabi Big Tab</a></td>
			<td>Wall-mounted display</td>
		</tr>
		<tr>
			<td><strong>Typical size (diagonal)</strong></td>
			<td>4.7 inches = 12 cm</td>
			<td>24 inches = 61 cm</td>
			<td>72 inches = 183 cm</td>
		</tr>
		<tr>
			<td><strong>Easy to share the screen</strong></td>
			<td>No</td>
			<td>Yes</td>
			<td>Yes</td>
		</tr>
		<tr>
			<td><strong>Unintended touches common</strong></td>
			<td>Yes: Fat fingers. Avoid by designing larger targets.</td>
			<td>Yes: Unintended two-handed touches</td>
			<td>No</td>
		</tr>
		<tr>
			<td><strong>Extra physical effort to see the whole screen</strong></td>
			<td>No</td>
			<td>Yes, physical proximity makes it necessary to move head</td>
			<td>Yes, physical proximity makes it necessary to move head and sometimes step forward or backward</td>
		</tr>
		<tr>
			<td><strong>Extra physical effort to type (and tap)</strong></td>
			<td>No</td>
			<td>Yes, arm movement needed</td>
			<td>Yes, arm and neck movement needed, and sometimes stepping forward or backward</td>
		</tr>
		<tr>
			<td><strong>Privacy issues</strong></td>
			<td>No</td>
			<td>Yes</td>
			<td>Yes</td>
		</tr>
	</tbody>
</table>

<p>Shamonsky shares other findings based on her research experience with very large touch screens, “<strong>A large touchscreen can look beautiful and is enjoyable to interact with! At the same time, a large display will magnify a poor user experience.</strong> If you don’t like the way an interface looks at a small size, on a large screen it will be more offensive. Everything about the user experience is exaggerated at the large size—the beauty and the fun, as well as the effort and the frustration. Attempting to use touch on sites and apps that are were not designed for touch is, if nothing else, boring. Creating compelling touch interaction requires an understanding of the familiar gestures and how to use them appropriately. <strong>Use simple and clear visual and aural feedback to create a sense of tactile feedback. Tune into the joy of a good user experience. ”</strong></p>

<p><strong>For more information about designing for different screen sizes, consider our <a href="../../courses/scaling-responsive-design/index.php">Scaling User Interfaces course</a>.</strong></p>
</section>
  

  
  <section>
    
        
    
  </section>
  
  
  
  
  

  


  <section>
    <br />
    <p class="share"><b>Share <span class="hide-for-small-down">this article</span>:</b> <a href="https://twitter.com/intent/tweet?url=http://www.nngroup.com/articles/very-large-touchscreen-ux-design/&amp;text=Very%20Large%20Touchscreens:%20UX%20Design%20Differs%20From%20Mobile%20Screens&amp;via=nngroup">Twitter</a> | <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=http://www.nngroup.com/articles/very-large-touchscreen-ux-design/&amp;title=Very%20Large%20Touchscreens:%20UX%20Design%20Differs%20From%20Mobile%20Screens&amp;source=Nielsen%20Norman%20Group">LinkedIn</a> | <a href="https://plus.google.com/share?url=http://www.nngroup.com/articles/very-large-touchscreen-ux-design/">Google+</a> | <a href="mailto:?subject=NN/g Article: Very Large Touchscreens: UX Design Differs From Mobile Screens&amp;body=http://www.nngroup.com/articles/very-large-touchscreen-ux-design/">Email</a></p>
    <hr />
  </section>

  <footer class="l-subsection">
    
    
    <div class="learn-more">
      <h2>Learn More</h2>
      <p><a href="../subscribe/index.php">Subscribe to the weekly newsletter</a> to get notified about future articles.</p>
      </div>
    
    

    <div class="row show-for-small-down related-content">
      
      
      

      
      
        <div class="column small-12 topic-links">All articles about:
          <ul>
            
            <li><a href="../../topic/mobile-and-tablet-design/index.php">Mobile &amp; Tablet</a></li>
            
            <li><a href="../../topic/technology/index.php">new technologies</a></li>
            
          </ul>
        </div>
      
      

      
      
        <div class="column small-12">
          <h3>UX Conference Training Courses</h3>
           <ul class="no-bullet">
    
        <li><a href="../../courses/cross-channel-user-experience/index.php">Omnichannel Journeys and Customer Experience</a></li>
    
        <li><a href="../../courses/emerging-patterns-web-design/index.php">Emerging Patterns for Web Design</a></li>
    
        <li><a href="../../courses/usability-mobile-websites-apps/index.php">Mobile User Experience</a></li>
    
        <li><a href="../../courses/scaling-responsive-design/index.php">Scaling User Interfaces</a></li>
    
        <li>Visual Design for Mobile and Tablet: <a href="../../courses/vis-mob-1/index.php">Day 1</a> and <a href="../../courses/vis-mob-2/index.php">Day 2</a></li>
    
</ul>
        </div>
      
      

      
      
        <div class="small-12 column">
          <h3>Research Reports</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../reports/tablets/index.php">Tablet Website and Application UX</a></li>
              
            
              
                <li><a href="../../reports/mobile-website-and-application-usability/index.php">User Experience for Mobile Applications and Websites</a></li>
              
            
              
                <li><a href="../../reports/enterprise-mobile-showcase/index.php">Mobile Intranets and Enterprise Apps</a></li>
              
            
              
                <li><a href="../../reports/wap-usability/index.php">WAP Usability</a></li>
              
            
              
                <li><a href="../../reports/ipad-app-and-website-usability/index.php">iPad App and Website Usability</a></li>
              
            
          </ul>
        </div>
      
      

      
      
        <div class="small-12 column">
          <h3>Online Seminars</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../online-seminars/beyond-screens/index.php">Beyond Screens</a></li>
              
            
              
                <li><a href="../../online-seminars/every-word-count/index.php">Making Every Word Count</a></li>
              
            
              
                <li><a href="../../online-seminars/mobile-user-testing/index.php">Mobile User Testing</a></li>
              
            
          </ul>
        </div>
      
      
    </div>

    <div class="hide-for-small-down row related-content">
      <div class="medium-6 columns">
        
        
          <div>
            <h3>Articles</h3>
            <ul class="no-bullet">
              
                
                <li><a href="../legacy-tablet-ux/index.php">Tablet UX Research From the Pioneer Days</a></li>
                
              
                
                <li><a href="../wechat-qr-shake/index.php">Scan and Shake: A Lesson in Technology Adoption from China’s WeChat</a></li>
                
              
                
                <li><a href="../visual-indicators-differentiators/index.php">Visual Indicators to Differentiate Items in a List</a></li>
                
              
                
                <li><a href="../wechat-integrated-ux/index.php">WeChat: China’s Integrated Internet User Experience</a></li>
                
              
                
                <li><a href="../mobile-list-thumbnail/index.php">List Thumbnails on Mobile: When to Use Them and Where to Place Them</a></li>
                
              
            </ul>
          </div>
        
        

        
          
          <div>
            <h3>Research Reports</h3>
            <ul class="no-bullet">
              
                
                  <li><a href="../../reports/tablets/index.php">Tablet Website and Application UX</a></li>
                
              
                
                  <li><a href="../../reports/mobile-website-and-application-usability/index.php">User Experience for Mobile Applications and Websites</a></li>
                
              
                
                  <li><a href="../../reports/enterprise-mobile-showcase/index.php">Mobile Intranets and Enterprise Apps</a></li>
                
              
                
                  <li><a href="../../reports/wap-usability/index.php">WAP Usability</a></li>
                
              
                
                  <li><a href="../../reports/ipad-app-and-website-usability/index.php">iPad App and Website Usability</a></li>
                
              
            </ul>
          </div>
          
        

      </div>
      <div class="medium-6 columns">
        
          
          <div>
            <h3>UX Conference Training Courses</h3>
             <ul class="no-bullet">
    
        <li><a href="../../courses/cross-channel-user-experience/index.php">Omnichannel Journeys and Customer Experience</a></li>
    
        <li><a href="../../courses/emerging-patterns-web-design/index.php">Emerging Patterns for Web Design</a></li>
    
        <li><a href="../../courses/usability-mobile-websites-apps/index.php">Mobile User Experience</a></li>
    
        <li><a href="../../courses/scaling-responsive-design/index.php">Scaling User Interfaces</a></li>
    
        <li>Visual Design for Mobile and Tablet: <a href="../../courses/vis-mob-1/index.php">Day 1</a> and <a href="../../courses/vis-mob-2/index.php">Day 2</a></li>
    
</ul>
          </div>
          
        
        
          
          <div>
            <h3>Online Seminars</h3>
            <ul class="no-bullet">
            
                <li><a href="../../online-seminars/beyond-screens/index.php">Beyond Screens</a></li>
            
                <li><a href="../../online-seminars/every-word-count/index.php">Making Every Word Count</a></li>
            
                <li><a href="../../online-seminars/mobile-user-testing/index.php">Mobile User Testing</a></li>
            
            </ul>
          </div>
          
        
      </div>
    </div>
  </footer>
</article>

        </div>

      

    
  </div>
  <div class="row">
    
      <div class="column small-12 show-for-small-down l-navrail announcements">
        
          
        
      </div>
    
    
  </div>

    </div>
    




<footer id="page-footer">
    <div class="row hide-for-print">
        <div class="column small-12 medium-6">
            <section class="footer-popular">
                
                <h1>Popular Topics</h1>
                <div style="float: left;  width: 15em; display: block;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/applications/index.php">Application Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/e-commerce/index.php">E-Commerce Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/eyetracking/index.php">Eyetracking </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/information-architecture/index.php">Information Architecture </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/intranets/index.php">Intranet Design </a></li>
	<li><a href="../../topic/interaction-design/index.php">Interaction Design </a></li>
</ul>
</div>

<div style="float: left;  width: 15em; display: block; list-style: none;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/mobile-and-tablet-design/index.php">Mobile and Tablet Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/credibility-persuasion/index.php">Persuasive Design</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/research-methods/index.php">Research Methods</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/user-testing/index.php">User Testing </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/web-usability/index.php">Web Usability </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/writing-web/index.php">Web Writing &amp; Content Strategy </a></li>
</ul>
</div>

<p>&nbsp;</p>

                
            </section>
        </div>

        <div class="column small-12 medium-6">
            <div class="row">
                <div class="column small-6">
                    <section class="footer-about">
                        <h1>Follow Us</h1>
                        <ul>
	<li><a href="../subscribe/index.php">Subscribe to our weekly UX newsletter</a>,&nbsp;Jakob Nielsen&#39;s Alertbox</li>
	<li>Visit Don Norman&#39;s website, <a href="http://www.jnd.org/"> www.jnd.org </a></li>
	<li>Visit Bruce Tognazzini&#39;s website, <a href="http://www.asktog.com/"> www.asktog.com </a></li>
	<li style="padding-top: 6px;"><a class="twitter-follow-button" data-show-count="false" data-size="large" href="https://twitter.com/nngroup"><img alt="Follow @nngroup on Twitter" height="20" src="https://media.nngroup.com/media/editor/2016/12/30/twitter-follow-button.png" style="width: 117px; height: 20px;" width="117" /></a></li>
	<li style="padding-top: 6px;"><img alt="" height="14" src="https://media.nngroup.com/media/editor/2016/07/26/feed-icon-14x14.png" style="vertical-align:text-top;width:14px;margin-right:.25em;" width="14" /><a href="../../feed/rss/index.php">Subscribe to our RSS feed</a></li>
</ul>

                    </section>
                </div>

                <div class="column small-6">
                    <section class="footer-about right-column">
                        <h1>About
</h1>
                        <ul class="no-bullet">
	<li><a href="../../about/contact/index.php">Contact information</a></li>
	<li><a href="../../about/index.php">About Us</a></li>
	<li><a href="../../about/why-nng/index.php">Why NN/g? </a></li>
	<li><a href="../../return-policy/index.php">Return Policy</a></li>
</ul>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="copyright column small-12">
            <p><a href="../../copyright-and-reprint-info/index.php">Copyright</a> &copy; 1998-2017 Nielsen Norman Group, All Rights Reserved.</p>
        </div>
    </div>
</footer>


    


        <script src="https://media.nngroup.com/static/vendor/fastclick/js/fastclick.min.js"></script>
        <script src="https://media.nngroup.com/static/vendor/foundation/js/foundation.min.js"></script>
        <script src="http://media.nngroup.com/static/js/app.js"></script>
        <script src="http://media.nngroup.com/static/js/scroll-to-fixed.js"></script><script>var externalLinks = document.querySelectorAll('a[href^="http"]');for (var i = externalLinks.length-1; i >= 0; i--) {externalLinks[i].addEventListener("click", function(event) { alert("External links are disabled for this test."); event.preventDefault(); }, false);}var mailLinks = document.querySelectorAll('a[href^="mailto"]');for (var i = mailLinks.length-1; i >= 0; i--) {mailLinks[i].addEventListener("click", function(event) { alert("Email links are disabled for this test."); event.preventDefault(); }, false);}var externalForms = document.querySelectorAll('form');for (var i = externalForms.length-1; i >= 0; i--) {externalForms[i].addEventListener("submit", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false); externalForms[i].addEventListener("click", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false);}</script>

        <script>
            
            $(function() {
                $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
                $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
            });
        </script>

        
<script src="https://media.nngroup.com/static/js/articles.js"></script>


        
        <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1072699960;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072699960/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
    </body>

<!-- Mirrored from www.nngroup.com/articles/very-large-touchscreen-ux-design/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:08:09 GMT -->
</html>
