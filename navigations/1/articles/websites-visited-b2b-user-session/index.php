<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->





    
<!-- Mirrored from www.nngroup.com/articles/websites-visited-b2b-user-session/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:16:05 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1026.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"licenseKey":"1a30ef9931","queueTime":0,"beacon":"bam.nr-data.net","errorBeacon":"bam.nr-data.net","applicationID":"2048992","transactionName":"NFxSMRBUX0EDUkIPDA0WdhAMVkVbDV8ZBxEXUFMJB0YfRAtUQRVZIktEDAFZVHYHRVcPD01eVRE=","applicationTime":369,"agent":""}</script>
        <title>Websites Visited During a B2B User Session</title><meta property="og:title" content="Websites Visited During a B2B User Session" />
  
        
        <meta name="description" content="While researching a purchase, a user conducted 25 visits to 15 different sites. Discussion of why and how the user visited each site and shy she left it.">
        <meta property="og:description" content="While researching a purchase, a user conducted 25 visits to 15 different sites. Discussion of why and how the user visited each site and shy she left it." />
        
  
        
	
        
        <meta name="keywords" content="pop-ups, search, overlays, navigation, landing pages, reviews">
        

        

        <meta name="robots" content="noarchive">
        <meta name="viewport" content="width=device-width">

        <link rel="search" type="application/opensearchdescription+xml" title="NN/g Search" href="../../opensearch.xml" />
        <link rel="shortcut icon" href="http://media.nngroup.com/static/img/favicon.ico" />

        <link rel="stylesheet" href="https://media.nngroup.com/static/vendor/font-awesome/css/font-awesome.min.css">

        
        <link rel="stylesheet" href="https://media.nngroup.com/static/css/7232a960f1df.css" type="text/css" />
        <link rel="stylesheet" href="http://fast.fonts.com/t/1.css?apiType=css&amp;projectid=c5846260-9def-46ec-9407-0f1349bd2d64" type="text/css">

        <script src="https://media.nngroup.com/static/vendor/modernizr/js/modernizr.min.js"></script>

        <script type="text/javascript" src="https://media.nngroup.com/static/js/3283be5def74.js"></script>
        

        

        
        
        <!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=229034,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('http://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->


<!-- Google Analytics Tracking Code for http://nngroup.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23132681-1', 'nngroup.com');
  ga('require', 'linkid', 'https://www.nngroup.com/articles/websites-visited-b2b-user-session/linkid.js');
  ga('require', 'displayfeatures');

// Optimizely Universal Analytics Integration
 window.optimizely = window.optimizely || [];
 window.optimizely.push("activateUniversalAnalytics");

  ga('send', 'pageview');

</script>
<script>
/**
* Function to track a click on an outbound link in Google Analytics.
*/
function trackOutboundLink(label, url) {
    if(window.ga && ga.loaded) {
        ga('send', 'event', 'outbound', 'click', label, {'hitCallback': function(){document.location = url;}
        });
    } else {
        document.location = url;
    }
}
/**
* Function to track a click on a link in the article announcement area.
*/
$('#articleAnnouncement a').on('click', function(){
  ga('send', 'event', 'link', 'click', 'articleAnnouncement');
});
</script>

<script type="text/javascript">
 setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2421.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



<!-- Hotjar Tracking Code for http://nngroup.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:121605,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

        <script src="https://media.nngroup.com/static/js/analytics/track_outbound_link.js"></script>
        
        
        
    </head>
    <body class="location-articles articles-detail">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
    <header id="page-header">
        <div id="cart" class="shopping-cart mobile-cart show-for-small-down"></div>
        <div class="row">
            <div id="masthead" class="masthead small-12 medium-7 column">

                <!-- The button to toggles menu on small screen -->

                <h1><a href="../../index.php"><img src="http://media.nngroup.com/static/img/logo-wordmark.png" width="74" height="30"> Nielsen <span class="norman">Norman</span> Group</a></h1>
                <p class="tagline show-for-medium-up">Evidence-Based User Experience Research, Training, and Consulting</p>

                <!-- The button to toggles form search on small screen -->


                <div style="clear: right;width:  100%;height: 1px;"></div><?php define('ROOT_PATH',$_SERVER["DOCUMENT_ROOT"] . '/apps/navigation_styles/navigations/1'); ?><?php include(ROOT_PATH)."/navigation.php"; ?>

                <!-- Drop down menu on small screen -->
                <div id="hideNav" class="show-for-small-down">
                  
                  <ul class="menu-content">
                    <li><a href="../../index.php">Home</a></li>
                    <li class="has-submenu">
                      <a>Training</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../training/index.php">Overview</a></li>
                        
<li id="nav-ux-conference"><a href="../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../online-seminars/index.php"><span>Online Seminars</span></a></li>

                      </ul>
                    </li>
                    <li class="has-submenu">
                      <a>Consulting</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
  <li><a href="../../consulting/index.php">Overview</a></li>
  
  <li><a href="../../consulting/benchmarking/index.php">Benchmarking</a></li>
  
  <li><a href="../../consulting/expert-design-review/index.php">Expert Design Review</a></li>
  
  <li><a href="../../consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
  
  <li><a href="../../consulting/usability-testing/index.php">Usability Testing </a></li>
  
  <li><a href="../../consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
  
  <li><a href="../../in-house-training/index.php">In-House Training</a></li>
</ul>

                    </li>

                    <li><a href="../../reports/index.php">Reports</a></li>
                    <li><a href="../index.php">Articles</a></li>
                    <li class="has-submenu"><a>About NN/G</a><i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../about/index.php">Overview</a></li>
                        <li><a href="../../people/index.php">People</a></li>
                        <li><a href="../../about/why-nng/index.php">Why NN/g?</a></li>
                        <li><a href="../../about/contact/index.php">Contact</a></li>
                        <li><a href="../../news/index.php">News</a></li>
                        <li><a href="../../about/history/index.php">History</a></li>
                        <li><a href="../../books/index.php">Books</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                </div>

                <hr class="header-divider show-for-small-only"/>


            </div>
       </div>

        <div id="hideNav">            <nav class="primary-nav small-12 column">
                <ul>
                    <li><a class="navitem" id="nav-home" href="../../index.php">Home</a></li>
                    <li><a class="navitem" id="nav-training" href="../../training/index.php">Training</a></li>
                    <li><a class="navitem" id="nav-consulting" href="../../consulting/index.php">Consulting</a></li>
                    <li><a class="navitem" id="nav-reports" href="../../reports/index.php">Reports</a></li>
                    <li><a class="navitem" id="nav-articles" href="../index.php">Articles</a></li>
                    <li><a class="navitem" id="nav-about" href="../../about/index.php">About NN/G</a></li>
                </ul>
            </nav>
            <nav class="secondary-nav small-12 medium-12 large-12 columns">
                
                
            </nav>
        </div>
    </header>
    <div class="l-content">
    
  

  

  <div class="row">
    
      
      <section class="small-12 medium-3 columns collapsable-content l-navrail articles-left-column">
        <h1 class="show-for-small-down collapse-link mobile-browse-header">Browse by Topic and Author</h1>
          <div class="row collapsable">

            
            <div class="small-6 medium-12 columns">
              <section id="article-popular-topics" style="display: none;">
                
                <h1>Topics</h1>
                <ul id="article-popular-topics">
                    
                    <li>
                        
                        <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/intranets/all/index.php">Intranets</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/user-testing/all/index.php">User Testing</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                        
                    </li>
                    
                    <li><a href="#" id="article-see-all-topics"><strong>See all topics&hellip;</strong></a></li>
                </ul>
                
              </section>
              <section id="article-all-topics" style="display: none;">
                
                <h1>
                  <span class="show-for-medium-up">All Article Topics</span>
                  <span class="show-for-small-down">All Topics</span>
                  (<a href="#" id="article-hide-all-topics">hide</a>)
                </h1>
                <ul>
                  
                  <li>
                    
                    <a href="../../topic/accessibility/all/index.php">Accessibility</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/agile/all/index.php">Agile</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/analytics-and-metrics/all/index.php">Analytics &amp; Metrics</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/applications/all/index.php">Application Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/b2b-websites/all/index.php">B2B Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/behavior-patterns/all/index.php">Behavior Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/branding/all/index.php">Branding</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/content-strategy/all/index.php">Content Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/corporate-websites/all/index.php">Corporate Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/design-patterns/all/index.php">Design Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-design-process/all/index.php">Design Process</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/email/all/index.php">Email</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/eyetracking/all/index.php">Eyetracking</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/heuristic-evaluation/all/index.php">Heuristic Evaluation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/human-computer-interaction/all/index.php">Human Computer Interaction</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-ideation/all/index.php">Ideation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/information-architecture/all/index.php">Information Architecture</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/interaction-design/all/index.php">Interaction Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/international-users/all/index.php">International Users</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/intranets/all/index.php">Intranets</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-management/all/index.php">Management</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/navigation/all/index.php">Navigation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/non-profit-websites/all/index.php">Non-Profit Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/personas/all/index.php">Personas</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/persuasive-design/all/index.php">Persuasive Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/prototyping/all/index.php">Prototyping</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/psychology-and-ux/all/index.php">Psychology and UX</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/research-methods/all/index.php">Research Methods</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/search/all/index.php">Search</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/social-media/all/index.php">Social Media</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/strategy/all/index.php">Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/user-testing/all/index.php">User Testing</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-teams/all/index.php">UX Teams</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/visual-design/all/index.php">Visual Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/writing-web/all/index.php">Writing for the Web</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/young-users/all/index.php">Young Users</a>
                    
                  </li>
                  
                </ul>
                
              </section>
            </div>
            <script>
              /* Determine which topics list will be displayed: the full or the
               * partial.  We are inlining this close to the list itself to
               * prevent rendering delays on the client
               */
              var topic_list = $.cookie("article-topics");

              var popular_topics = $('#article-popular-topics');
              var all_topics = $('#article-all-topics');

              if(topic_list == 1) {
                  popular_topics.hide();
                  all_topics.show();
              } else {
                  popular_topics.show();
                  all_topics.hide();
              }
            </script>
            

            
            <div class="small-6 medium-12 columns">
              
              <section id="article-initial-authors">
                  <h1>Author</h1>
                  <ul>
                    
                    <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                    
                    <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                    
                    <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                    
                    <li><a href="#" onClick="$('#article-initial-authors').hide(); $('#article-all-authors').show();"><strong>See all authors&hellip;</strong></a></li>
                  </ul>
              </section>
              <section id="article-all-authors" style="display: none;">
                <h1>
                  All Authors
                  (<a href="#" id="article-hide-all-authors">hide</a>)
                </h1>
                <ul>
                  
                  <li><a href="../author/raluca-budiu/index.php">Raluca Budiu</a></li>
                  
                  <li><a href="../author/patty-caya/index.php">Patty Caya</a></li>
                  
                  <li><a href="../author/susan-farrell/index.php">Susan Farrell</a></li>
                  
                  <li><a href="../author/therese-fessenden/index.php">Therese Fessenden</a></li>
                  
                  <li><a href="../author/kim-flaherty/index.php">Kim Flaherty</a></li>
                  
                  <li><a href="../author/sarah-gibbons/index.php">Sarah Gibbons</a></li>
                  
                  <li><a href="../author/aurora-harley/index.php">Aurora Harley</a></li>
                  
                  <li><a href="../author/kate-kaplan/index.php">Kate Kaplan</a></li>
                  
                  <li><a href="../author/page-laubheimer/index.php">Page Laubheimer</a></li>
                  
                  <li><a href="../author/angela-li/index.php">Angie Li</a></li>
                  
                  <li><a href="../author/hoa-loranger/index.php">Hoa Loranger</a></li>
                  
                  <li><a href="../author/kate-meyer/index.php">Kate Meyer</a></li>
                  
                  <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                  
                  <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                  
                  <li><a href="../author/kara-pernice/index.php">Kara Pernice</a></li>
                  
                  <li><a href="../author/christian-rohrer/index.php">Christian Rohrer</a></li>
                  
                  <li><a href="../author/amy-schade/index.php">Amy Schade</a></li>
                  
                  <li><a href="../author/katie-sherwin/index.php">Katie Sherwin</a></li>
                  
                  <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                  
                  <li><a href="../author/kathryn-whitenton/index.php">Kathryn Whitenton</a></li>
                  
                </ul>
              </section>
              
            </div>
            
          </div>

          
          <section class="hide-for-small-down">
            
              
              <h1>Recent Articles</h1>
              
              <ul>
                  
                  <li><a href="../seamless-cross-channel/index.php">Seamlessness in the Omnichannel User Experience</a></li>
                  
                  <li><a href="../brand-intention-interpretation/index.php">Experience Design: Bridging Brand Intention and Brand Interpretation</a></li>
                  
                  <li><a href="../eyetracking-tasks-efficient-scanning/index.php">Scanning Patterns on the Web Are Optimized for the Current Task</a></li>
                  
                  <li><a href="../flat-design-best-practices/index.php">Flat-Design Best Practices</a></li>
                  
                  <li><a href="../ux-user-stories/index.php">Accounting for UX Work with User Stories in Agile Projects</a></li>
                  
                  <li><a href="../index.php"><strong>See all articles&hellip;</strong></a></li>
              </ul>
              
            
          </section>
          

          
          
          <section class="hide-for-small-down">
            <h1>Popular Articles</h1>
            
            <ul>
                
                <li><a href="../usability-101-introduction-to-usability/index.php">Usability 101: Introduction to Usability</a></li>
                
                <li><a href="../top-10-mistakes-web-design/index.php">Top 10 Mistakes in Web Design</a></li>
                
                <li><a href="../how-users-read-on-the-web/index.php">How Users Read on the Web</a></li>
                
                <li><a href="../f-shaped-pattern-reading-web-content/index.php">F-Shaped Pattern For Reading Web Content</a></li>
                
                <li><a href="../ten-usability-heuristics/index.php">10 Usability Heuristics for User Interface Design</a></li>
                
                <li><a href="../intranet-design-2016/index.php">10 Best Intranets of 2016</a></li>
                
                <li><a href="../which-ux-research-methods/index.php">When to Use Which User-Experience Research Methods</a></li>
                
                <li><a href="../response-times-3-important-limits/index.php">Response Times: The 3 Important Limits</a></li>
                
                <li><a href="../why-you-only-need-to-test-with-5-users/index.php">Why You Only Need to Test with 5 Users</a></li>
                
                <li><a href="../page-fold-manifesto/index.php">The Fold Manifesto: Why the Page Fold Still Matters</a></li>
                
            </ul>
          </section>
          
          

          
            <section class="hide-for-small-down">
              
            </section>
          

      </section>
      

      
        <div class="small-12 medium-9 columns content-body">
          




<article class="l-subsection alertbox-article-content article-detail-bl">
  
  <header>
    <div class="l-subsection">
      <h1>Websites Visited During a B2B User Session</h1>
      <div class="author-meta">by
        
          
            
            <a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a>
            
          
        on  February 6, 2006
        
      </div>

      

      
      <div class="topic-links">
        Topics:
        
          <ul>

</ul>
        
      </div>
      
    </div>
  </header>
  

  
  
  

  
  <section id="article-body"><p><em>Sidebar to <a href="../../people/jakob-nielsen/index.php" title="Author biography"> Jakob Nielsen</a>'s column <a class="new" href="../users-interleave-sites-and-genres/index.php" title="Alertbox Feb. 2006"> Users Interleave Sites and Genres</a>. </em></p>

<p>The following table is a play-by-play listing of the sites our user visited during a 44-minute session. Her goal was to <strong> research the purchase of a portable computer projector </strong> for use during presentations.</p>

<p>For each site visit, the table shows the total time and the number of pageviews.</p>

<p>The sites are color-coded according to their role:</p>

<ul>
	<li><span style="background-color: #00CFFF">Blue </span> : Search</li>
	<li><span style="background-color: #00FF00">Green </span> : E-commerce (retailer)</li>
	<li><span style="background-color: #FF9A00">Orange </span> : Product manufacturer (vendor)</li>
	<li><span style="background-color: #FFFF00">Yellow </span> : Content (mainly reviews)</li>
	<li><span style="background-color: #CCCACC">Gray </span> : Visited by mistake (the site was irrelevant to the task, and the user backed out right away)</li>
</ul>

<p> </p>

<table align="center" style="border-style: solid; border-width: 1px; border-collapse: collapse; padding: 19px;">
	<tbody>
		<tr>
			<td style="background-color: #00CFFF; border-style: solid; border-width: 1px; padding: 5px; text-align: left; vertical-align: top">1) <strong> Google </strong>

			<p>Search engine</p>

			<p>32 seconds<br/>
			2 pageviews</p>
			</td>
			<td style="border-style: solid; border-width: 1px; padding: 5px; text-align: left; vertical-align: top">Search: <strong> lcd projector portable </strong>
			<p>The user clicked the #3 organic link.</p>
			</td>
		</tr>
		<tr>
			<td style="background-color: #00FF00; border-style: solid; border-width: 1px; padding: 5px; text-align: left; vertical-align: top">2)  <strong> LCDprojectoronline.com </strong>
			<p>Retailer/e-commerce</p>

			<p>177 seconds<br/>
			2 pageviews</p>
			</td>
			<td style="border-style: solid; border-width: 1px; padding: 5px; text-align: left; vertical-align: top">On the landing page, the user looked over the list of projectors and noted the brands represented. (She used some of this info later, going directly to some of the vendors' sites.)
			<p>She clicked the first product in the list and spent significant time examining its specifications to get an idea of important parameters to consider when buying a projector. The visit to this e-commerce site was purely educational; she never considered buying this early in the process.</p>
			</td>
		</tr>
		<tr>
			<td style="background-color: #FFFF00; border-style: solid; border-width: 1px; padding: 5px; text-align: left; vertical-align: top">3) <strong> CNet </strong>
			<p>Content/reviews</p>

			<p>257 seconds<br/>
			9 pageviews</p>
			</td>
			<td style="border-style: solid; border-width: 1px; padding: 5px; text-align: left; vertical-align: top">Next, the user wanted to read product reviews, so she typed in the URL of a site she already knew of that featured reviews of computer products. On this site, she read reviews of several top-rated products and noted their brands, model names, prices, and specifications.</td>
		</tr>
		<tr>
			<td style="background-color: #00CFFF; border-style: solid; border-width: 1px; padding: 5px; text-align: left; vertical-align: top">4) <strong> Google </strong>
			<p>Search engine</p>

			<p>24 seconds<br/>
			2 pageviews</p>
			</td>
			<td style="border-style: solid; border-width: 1px; padding: 5px; text-align: left; vertical-align: top">Having seen that Dell projectors got good reviews, she next went to Google to find these products.
			<p>Search: <strong> dell projectors </strong></p>

			<p>She clicked the #1 sponsored result in the top blue area, which was Dell's own site.</p>
			</td>
		</tr>
		<tr>
			<td style="background-color: #FF9A00; border-style: solid; border-width: 1px; padding: 5px; text-align: left; vertical-align: top">5) <strong> Dell </strong>
			<p>Vendor</p>

			<p>423 seconds<br/>
			13 pageviews</p>
			</td>
			<td style="border-style: solid; border-width: 1px; padding: 5px; text-align: left; vertical-align: top">Despite having searched for projectors, the landing page was a general homepage, which is typically a usability mistake. In this case, however, the user navigated (with relative ease) to the accessories page, then on to the projectors page, and finally to the portable projectors page. She then looked at product pages and specifications for two promising projectors.</td>
		</tr>
		<tr>
			<td style="background-color: #00CFFF; border-style: solid; border-width: 1px; padding: 5px; text-align: left; vertical-align: top">6) <strong> Google </strong>
			<p>Search engine</p>

			<p>31 seconds<br/>
			2 pageviews</p>
			</td>
			<td style="border-style: solid; border-width: 1px; padding: 5px; text-align: left; vertical-align: top">Referring to her written notes from the CNet visit (step 3), the user next decided to find out more about a promising Mitsubishi projector. To do this, she returned to Google.
			<p>Search: <strong> mitsubishi XD50U </strong></p>

			<p>She noticed that the top sponsored link was a site that sold the product. However, at this stage, she decided to read more about the product and clicked the #4 organic link, which was a more detailed review at CNet.</p>
			</td>
		</tr>
		<tr>
			<td style="background-color: #FFFF00; border-style: solid; border-width: 1px; padding: 5px; text-align: left; vertical-align: top">7) <strong> CNet </strong>
			<p>Content/reviews</p>

			<p>204 seconds<br/>
			7 pageviews</p>
			</td>
			<td style="border-style: solid; border-width: 1px; padding: 5px; text-align: left; vertical-align: top">Here, she read about the Mitsubishi projector. She then wanted to read about a third-party product she'd found on Dell's site.
			<p>Search: <strong> epson powerlite 760c </strong></p>

			<p>On CNet's SERP, she clicked the "best bet" link that was featured at the top of the page. This led her to more information about the product, including its specs, but no real review.</p>
			</td>
		</tr>
		<tr>
			<td style="background-color: #00CFFF; border-style: solid; border-width: 1px; padding: 5px; text-align: left; vertical-align: top">8) <strong> Google </strong>
			<p>Search engine</p>

			<p>71 seconds<br/>
			3 pageviews</p>
			</td>
			<td style="border-style: solid; border-width: 1px; padding: 5px; text-align: left; vertical-align: top">Disappointed by the lack of reviews, the user went to Google, to <em> "plug it in to see how many people write awful things about it." </em>
			<p>Search: <strong> epson powerlite 760c </strong></p>

			<p>She didn't find any reviews on the first SERP, only promotions for sites selling the product. She modified her query and conduced another search.</p>

			<p>Search: <strong> epson powerlite 760c review </strong></p>

			<p>On the SERP, she clicked the #9 organic listing; all the rest looked like vendor or retailer sites.</p>
			</td>
		</tr>
		<tr>
			<td style="background-color: #00FF00; border-style: solid; border-width: 1px; padding: 5px; text-align: left; vertical-align: top">9) <strong> Smalldog.com </strong>
			<p>Retailer/e-commerce</p>

			<p>18 seconds<br/>
			1 pageview</p>
			</td>
			<td style="border-style: solid; border-width: 1px; padding: 5px; text-align: left; vertical-align: top">Even though the search listing looked like it might be informative, the site turned out to be a standard e-commerce retailer with no additional information about the projector. The user checked the price on the product page and determined that it was about the same as that offered by vendors listed on CNet. She bounced out of the site.</td>
		</tr>
		<tr>
			<td style="background-color: #00CFFF; border-style: solid; border-width: 1px; padding: 5px; text-align: left; vertical-align: top">10) <strong> Google </strong>
			<p>Search engine</p>

			<p>25 seconds<br/>
			2 pageviews</p>
			</td>
			<td style="border-style: solid; border-width: 1px; padding: 5px; text-align: left; vertical-align: top">After clicking the <em> Back </em> button to return to the Google SERP, the user went to the second SERP for the query she'd entered in step 8.
			<p>She clicked the #18 organic listing.</p>
			</td>
		</tr>
		<tr>
			<td style="background-color: #FFFF00; border-style: solid; border-width: 1px; padding: 5px; text-align: left; vertical-align: top">11) <strong> Projectorreviews.com </strong>
			<p>Content/reviews</p>

			<p>45 seconds<br/>
			2 pageviews</p>
			</td>
			<td style="border-style: solid; border-width: 1px; padding: 5px; text-align: left; vertical-align: top">On this site, she was disappointed to see that the "review" of the Epson was simply a listing of its specs. She clicked a navigation link for advice and info, which resulted in a list of articles that she didn't read. The user abandoned this site.</td>
		</tr>
		<tr>
			<td style="background-color: #00CFFF; border-style: solid; border-width: 1px; padding: 5px; text-align: left; vertical-align: top">12) <strong> Google </strong>
			<p>Search engine</p>

			<p>51 seconds<br/>
			1 pageview</p>
			</td>
			<td style="border-style: solid; border-width: 1px; padding: 5px; text-align: left; vertical-align: top">The user arrived at the search engine after clicking the <em> Back </em> button from the previous site. She didn't conduct any searches during this visit to Google, but pondered things she might search for in order to identify more review sites. She gave up on this and instead typed in the URL for a vendor she remembered from the list of brands in step 1.</td>
		</tr>
		<tr>
			<td style="background-color: #FF9A00; border-style: solid; border-width: 1px; padding: 5px; text-align: left; vertical-align: top">13) <strong> Proxima </strong>
			<p>Vendor</p>

			<p>149 seconds<br/>
			6 pageviews</p>
			</td>
			<td style="border-style: solid; border-width: 1px; padding: 5px; text-align: left; vertical-align: top">Going directly to this vendor site, she fairly easily navigated to a category page listing the projectors. She scanned it, picked out a promising projector (Proxima C180), and went to its product page and specifications. She was frustrated by the fact that no price was listed.</td>
		</tr>
		<tr>
			<td style="background-color: #00CFFF; border-style: solid; border-width: 1px; padding: 5px; text-align: left; vertical-align: top">14) <strong> Google </strong> (Froogle)
			<p>Search engine</p>

			<p>55 seconds<br/>
			2 pageviews</p>
			</td>
			<td style="border-style: solid; border-width: 1px; padding: 5px; text-align: left; vertical-align: top">To find the price of the projector she'd identified in the previous step, the user typed in the URL for Froogle (the shopping search feature on Google).
			<p>Search: <strong> proxima c180 </strong></p>

			<p>She scanned the SERP and wrote down the predominant price from the listings, without visiting any of the retailers that came up in the search.</p>
			</td>
		</tr>
		<tr>
			<td style="background-color: #CCCACC; border-style: solid; border-width: 1px; padding: 5px; text-align: left; vertical-align: top">15) <strong> Sharp.com </strong>
			<p>Site visited by mistake</p>

			<p>11 seconds<br/>
			1 pageview</p>
			</td>
			<td style="border-style: solid; border-width: 1px; padding: 5px; text-align: left; vertical-align: top">The user wanted the electronics vendor Sharp, because she knew from prior experience that they make projectors. To go there, she typed in the URL www.sharp.com. This gave her the site for a healthcare lender with the same name as the company she wanted. She abandoned this site.</td>
		</tr>
		<tr>
			<td style="background-color: #00CFFF; border-style: solid; border-width: 1px; padding: 5px; text-align: left; vertical-align: top">16) <strong> Google </strong>
			<p>Search engine</p>

			<p>19 seconds<br/>
			2 pageviews</p>
			</td>
			<td style="border-style: solid; border-width: 1px; padding: 5px; text-align: left; vertical-align: top">The user typed in the URL for Google.
			<p>Search: <strong> sharp projectors </strong></p>

			<p>On the SERP, she selected the #1 organic link.</p>
			</td>
		</tr>
		<tr>
			<td style="background-color: #FF9A00; border-style: solid; border-width: 1px; padding: 5px; text-align: left; vertical-align: top">17) <strong> SharpUSA.com </strong>
			<p>Vendor</p>

			<p>72 seconds<br/>
			4 pageviews</p>
			</td>
			<td style="border-style: solid; border-width: 1px; padding: 5px; text-align: left; vertical-align: top">The user easily navigated from the homepage to the category page for projectors to the subcategory page for mobile projectors. She selected the highest-end mobile projector, the XR-1X. After reviewing the product page for this product, she said, <em> "It only has 1200 ANSI lumens. Forget it. That's the end of that." </em> She abandoned the site.</td>
		</tr>
		<tr>
			<td style="background-color: #FF9A00; border-style: solid; border-width: 1px; padding: 5px; text-align: left; vertical-align: top">18) <strong> InFocus.com </strong>
			<p>Vendor</p>

			<p>170 seconds<br/>
			5 pageviews</p>
			</td>
			<td style="border-style: solid; border-width: 1px; padding: 5px; text-align: left; vertical-align: top">The user knew from prior experience that InFocus makes projectors. She went directly to this vendor's site by typing the URL (which worked, in contrast to Sharp).
			<p>She tried to navigate the site, but was very annoyed with an animated overlay advertisement: <em> "They have their own pop-up on their own website--why would you do that? Because it's blocking their own stuff. So they have a pop-up on their own website that I now have to close to find the products they are trying to sell me." </em> (Actually, it was a floating overlay ad, not a pop-up, but both are among the <a class="old" href="../most-hated-advertising-techniques/index.php" title="Alertbox: The Most Hated Advertising Techniques"> most-hated advertising techniques </a> , and there's no excuse for antagonizing users this way.)</p>

			<p>She navigated to a list of mobile projectors, but rejected them for having too few lumens.</p>
			</td>
		</tr>
		<tr>
			<td style="background-color: #CCCACC; border-style: solid; border-width: 1px; padding: 5px; text-align: left; vertical-align: top">19) <strong> Mitsubishi.com </strong>
			<p>Site visited by mistake</p>

			<p>29 seconds<br/>
			1 pageview</p>
			</td>
			<td style="border-style: solid; border-width: 1px; padding: 5px; text-align: left; vertical-align: top">At several earlier steps, the user had noted good Mitsubishi projectors; she now decided to visit the vendor's site. Trying to guess the URL, she typed in www.mitsubishi.com. This gave her a page, written in Japanese and English, that seemed to be a portal page to numerous Mitsubishi sites. She thought it was the wrong site and abandoned it.
			<p>(In other words, Mitsubishi really needs to work on the usability of its portal -- but so do most other conglomerates.)</p>
			</td>
		</tr>
		<tr>
			<td style="background-color: #00CFFF; border-style: solid; border-width: 1px; padding: 5px; text-align: left; vertical-align: top">20) <strong> Google </strong>
			<p>Search engine</p>

			<p>25 seconds<br/>
			2 pageviews</p>
			</td>
			<td style="border-style: solid; border-width: 1px; padding: 5px; text-align: left; vertical-align: top">To find the site, she went to Google.
			<p>Search: <strong> mitsubishi projectors </strong></p>

			<p>On the SERP, she selected the #1 organic listing, which was indeed the vendor's site (at the non-guessable domain name of mitsubishi-presentations.com).</p>
			</td>
		</tr>
		<tr>
			<td style="background-color: #FF9A00; border-style: solid; border-width: 1px; padding: 5px; text-align: left; vertical-align: top">21)  <strong> Mitsubishi-Presentations.com </strong>
			<p>Vendor</p>

			<p>269 seconds<br/>
			6 pageviews</p>
			</td>
			<td style="border-style: solid; border-width: 1px; padding: 5px; text-align: left; vertical-align: top">On Mitsubishi's site, the user easily found the category page for projectors, and noticed that they make many portable models. She clicked the most promising one and read its product page and specifications page. There was no price, so she clicked a <em> Buy online </em> link, which provided a list of retailers. She selected the first one, and clicked its <em> Buy now </em> button.</td>
		</tr>
		<tr>
			<td style="background-color: #00FF00; border-style: solid; border-width: 1px; padding: 5px; text-align: left; vertical-align: top">22) <strong> AVpartner.com </strong>
			<p>Retailer/e-commerce</p>

			<p>22 seconds<br/>
			1 pageview</p>
			</td>
			<td style="border-style: solid; border-width: 1px; padding: 5px; text-align: left; vertical-align: top">The <em> Buy now </em> link on Mitsubishi's product page goes directly to the etailer's correct product page (good).
			<p>The user noticed the price ($3,295) and said <em> "that's a lot." </em> She abandoned the site.</p>
			</td>
		</tr>
		<tr>
			<td style="background-color: #00CFFF; border-style: solid; border-width: 1px; padding: 5px; text-align: left; vertical-align: top">23) <strong> Google </strong> (Froogle)
			<p>Search engine</p>

			<p>129 seconds<br/>
			7 pageviews</p>
			</td>
			<td style="border-style: solid; border-width: 1px; padding: 5px; text-align: left; vertical-align: top">The user said, <em> "let's see if we can get it cheaper" </em> and typed in the URL for Google's shopping search, Froogle.
			<p>Search: <strong> mitsubishi XD480U </strong></p>

			<p>She noticed the offers on the first SERP, clicked to see a list of more prices, clicked <em> Back </em> to the first SERP, then continued to the second SERP. She finally concluded that it's possible to buy the projector for about $2,400, and that the total would be about $2,500 when adding costs.</p>

			<p>The user then typed in the URL for the main Google homepage, because she wanted to check the prices for replacement lamps.</p>

			<p>Search: <strong> lamp Mitsubishi XD480U </strong></p>

			<p>She clicked the #1 sponsored link in the top blue area.</p>
			</td>
		</tr>
		<tr>
			<td style="background-color: #00FF00; border-style: solid; border-width: 1px; padding: 5px; text-align: left; vertical-align: top">24)  <strong> Projectorlampcenter.com </strong>
			<p>Retailer/e-commerce</p>

			<p>157 seconds<br/>
			6 pageviews</p>
			</td>
			<td style="border-style: solid; border-width: 1px; padding: 5px; text-align: left; vertical-align: top">On this specialized lamp retailer's site, the user easily identified the product pages for the recommended replacement lamps for each of the three projectors on her shortlist (Mitsubishi XD480U, Proxima C180, and Epson PowerLite 760c).</td>
		</tr>
		<tr>
			<td style="background-color: #00FF00; border-style: solid; border-width: 1px; padding: 5px; text-align: left; vertical-align: top">25) <strong> Buy.com </strong>
			<p>Retailer/e-commerce</p>

			<p>202 seconds<br/>
			4 pageviews</p>
			</td>
			<td style="border-style: solid; border-width: 1px; padding: 5px; text-align: left; vertical-align: top">The user typed in the URL to go directly to this site, which she knew was a retailer that often carried cheap computer equipment.
			<p>Search: <strong> projector </strong></p>

			<p>The user complained that the SERP was just a bunch of projectors without any sorting options.</p>

			<p>She clicked the link for related searches, which resulted in a search for <strong> projectors </strong> (in plural).</p>

			<p>She also didn't like this SERP, saying <em> "I don't feel I am in the projector department." </em></p>

			<p>Search: <strong> mitsubishi XD480U </strong></p>

			<p>The user was taken directly to the product page and noticed that the price on this site was higher than what she had found previously. She abandoned the site.</p>
			</td>
		</tr>
	</tbody>
</table>

<p>As discussed in the <a class="new" href="../users-interleave-sites-and-genres/index.php" title="Alertbox Feb. 2006: Users Interleave Sites and Genres"> main article</a>, at this point the user had a shortlist of three projectors and a recommended purchase that she would take to her boss.</p>

<p>Note: SERP = Search Engine Results Page.</p>

<p>This session transcript is part of a bigger study of the <a class="new" href="../../reports/b2b-websites-usability/index.php" title="Nielsen Norman Group report: B2B Website Usability - 144 Design Guidelines for Converting Business Users Into Leads and Customers"> usability of B2B sites</a> which resulted in 141 UX guidelines.</p>
</section>
  

  
  <section>
    
        
    
  </section>
  
  
  
  
  

  


  <section>
    <br />
    <p class="share"><b>Share <span class="hide-for-small-down">this article</span>:</b> <a href="https://twitter.com/intent/tweet?url=http://www.nngroup.com/articles/websites-visited-b2b-user-session/&amp;text=Websites%20Visited%20During%20a%20B2B%20User%20Session&amp;via=nngroup">Twitter</a> | <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=http://www.nngroup.com/articles/websites-visited-b2b-user-session/&amp;title=Websites%20Visited%20During%20a%20B2B%20User%20Session&amp;source=Nielsen%20Norman%20Group">LinkedIn</a> | <a href="https://plus.google.com/share?url=http://www.nngroup.com/articles/websites-visited-b2b-user-session/">Google+</a> | <a href="mailto:?subject=NN/g Article: Websites Visited During a B2B User Session&amp;body=http://www.nngroup.com/articles/websites-visited-b2b-user-session/">Email</a></p>
    <hr />
  </section>

  <footer class="l-subsection">
    
    
    

    <div class="row show-for-small-down related-content">
      
      
      

      
      
      

      
      
      

      
      
      

      
      
      
    </div>

    <div class="hide-for-small-down row related-content">
      <div class="medium-6 columns">
        
        
        

        

      </div>
      <div class="medium-6 columns">
        
        
      </div>
    </div>
  </footer>
</article>

        </div>

      

    
  </div>
  <div class="row">
    
      <div class="column small-12 show-for-small-down l-navrail announcements">
        
          
        
      </div>
    
    
  </div>

    </div>
    




<footer id="page-footer">
    <div class="row hide-for-print">
        <div class="column small-12 medium-6">
            <section class="footer-popular">
                
                <h1>Popular Topics</h1>
                <div style="float: left;  width: 15em; display: block;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/applications/index.php">Application Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/e-commerce/index.php">E-Commerce Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/eyetracking/index.php">Eyetracking </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/information-architecture/index.php">Information Architecture </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/intranets/index.php">Intranet Design </a></li>
	<li><a href="../../topic/interaction-design/index.php">Interaction Design </a></li>
</ul>
</div>

<div style="float: left;  width: 15em; display: block; list-style: none;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/mobile-and-tablet-design/index.php">Mobile and Tablet Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/credibility-persuasion/index.php">Persuasive Design</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/research-methods/index.php">Research Methods</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/user-testing/index.php">User Testing </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/web-usability/index.php">Web Usability </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/writing-web/index.php">Web Writing &amp; Content Strategy </a></li>
</ul>
</div>

<p>&nbsp;</p>

                
            </section>
        </div>

        <div class="column small-12 medium-6">
            <div class="row">
                <div class="column small-6">
                    <section class="footer-about">
                        <h1>Follow Us</h1>
                        <ul>
	<li><a href="../subscribe/index.php">Subscribe to our weekly UX newsletter</a>,&nbsp;Jakob Nielsen&#39;s Alertbox</li>
	<li>Visit Don Norman&#39;s website, <a href="http://www.jnd.org/"> www.jnd.org </a></li>
	<li>Visit Bruce Tognazzini&#39;s website, <a href="http://www.asktog.com/"> www.asktog.com </a></li>
	<li style="padding-top: 6px;"><a class="twitter-follow-button" data-show-count="false" data-size="large" href="https://twitter.com/nngroup"><img alt="Follow @nngroup on Twitter" height="20" src="https://media.nngroup.com/media/editor/2016/12/30/twitter-follow-button.png" style="width: 117px; height: 20px;" width="117" /></a></li>
	<li style="padding-top: 6px;"><img alt="" height="14" src="https://media.nngroup.com/media/editor/2016/07/26/feed-icon-14x14.png" style="vertical-align:text-top;width:14px;margin-right:.25em;" width="14" /><a href="../../feed/rss/index.php">Subscribe to our RSS feed</a></li>
</ul>

                    </section>
                </div>

                <div class="column small-6">
                    <section class="footer-about right-column">
                        <h1>About
</h1>
                        <ul class="no-bullet">
	<li><a href="../../about/contact/index.php">Contact information</a></li>
	<li><a href="../../about/index.php">About Us</a></li>
	<li><a href="../../about/why-nng/index.php">Why NN/g? </a></li>
	<li><a href="../../return-policy/index.php">Return Policy</a></li>
</ul>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="copyright column small-12">
            <p><a href="../../copyright-and-reprint-info/index.php">Copyright</a> &copy; 1998-2017 Nielsen Norman Group, All Rights Reserved.</p>
        </div>
    </div>
</footer>


    


        <script src="https://media.nngroup.com/static/vendor/fastclick/js/fastclick.min.js"></script>
        <script src="https://media.nngroup.com/static/vendor/foundation/js/foundation.min.js"></script>
        <script src="http://media.nngroup.com/static/js/app.js"></script>
        <script src="http://media.nngroup.com/static/js/scroll-to-fixed.js"></script><script>var externalLinks = document.querySelectorAll('a[href^="http"]');for (var i = externalLinks.length-1; i >= 0; i--) {externalLinks[i].addEventListener("click", function(event) { alert("External links are disabled for this test."); event.preventDefault(); }, false);}var mailLinks = document.querySelectorAll('a[href^="mailto"]');for (var i = mailLinks.length-1; i >= 0; i--) {mailLinks[i].addEventListener("click", function(event) { alert("Email links are disabled for this test."); event.preventDefault(); }, false);}var externalForms = document.querySelectorAll('form');for (var i = externalForms.length-1; i >= 0; i--) {externalForms[i].addEventListener("submit", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false); externalForms[i].addEventListener("click", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false);}</script>

        <script>
            
            $(function() {
                $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
                $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
            });
        </script>

        
<script src="https://media.nngroup.com/static/js/articles.js"></script>


        
        <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1072699960;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072699960/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
    </body>

<!-- Mirrored from www.nngroup.com/articles/websites-visited-b2b-user-session/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:16:05 GMT -->
</html>
