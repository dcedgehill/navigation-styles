<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->





    
<!-- Mirrored from www.nngroup.com/articles/intranet-design-2013/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:13:13 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1026.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"beacon":"bam.nr-data.net","queueTime":2,"applicationTime":727,"agent":"","licenseKey":"1a30ef9931","errorBeacon":"bam.nr-data.net","applicationID":"2048992","transactionName":"NFxSMRBUX0EDUkIPDA0WdhAMVkVbDV8ZBxEXUFMJB0YfRAtUQRVZIktEDAFZVHYHRVcPD01eVRE="}</script>
        <title>10 Best Intranets of 2013 (Jakob Nielsen&#39;s Alertbox)</title><meta property="og:title" content="10 Best Intranets of 2013 (Jakob Nielsen&#39;s Alertbox)" />
  
        
        <meta name="description" content="Intranet team size has doubled, but companies do get something for this investment, including better-integrated social features and better information filtering. 70% of winners used SharePoint, but with extensive customization.">
        <meta property="og:description" content="Intranet team size has doubled, but companies do get something for this investment, including better-integrated social features and better information filtering. 70% of winners used SharePoint, but with extensive customization." />
        
  
        
	
        
        <meta name="keywords" content="intranet design, intranet usability, intranet design annual, SharePoint, mobile design, mobile intranets, mobile intranet access, team size, intranet staff, intranet staffing, intranet budgets, budgeting, staffing, management, content authors, social features on intranets, intranet social features, Enterprise 2.0">
        

        

        <meta name="robots" content="noarchive">
        <meta name="viewport" content="width=device-width">

        <link rel="search" type="application/opensearchdescription+xml" title="NN/g Search" href="../../opensearch.xml" />
        <link rel="shortcut icon" href="http://media.nngroup.com/static/img/favicon.ico" />

        <link rel="stylesheet" href="https://media.nngroup.com/static/vendor/font-awesome/css/font-awesome.min.css">

        
        <link rel="stylesheet" href="https://media.nngroup.com/static/css/7232a960f1df.css" type="text/css" />
        <link rel="stylesheet" href="http://fast.fonts.com/t/1.css?apiType=css&amp;projectid=c5846260-9def-46ec-9407-0f1349bd2d64" type="text/css">

        <script src="https://media.nngroup.com/static/vendor/modernizr/js/modernizr.min.js"></script>

        <script type="text/javascript" src="https://media.nngroup.com/static/js/3283be5def74.js"></script>
        

        

        
        
        <!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=229034,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('http://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->


<!-- Google Analytics Tracking Code for http://nngroup.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23132681-1', 'nngroup.com');
  ga('require', 'linkid', 'https://www.nngroup.com/articles/intranet-design-2013/linkid.js');
  ga('require', 'displayfeatures');

// Optimizely Universal Analytics Integration
 window.optimizely = window.optimizely || [];
 window.optimizely.push("activateUniversalAnalytics");

  ga('send', 'pageview');

</script>
<script>
/**
* Function to track a click on an outbound link in Google Analytics.
*/
function trackOutboundLink(label, url) {
    if(window.ga && ga.loaded) {
        ga('send', 'event', 'outbound', 'click', label, {'hitCallback': function(){document.location = url;}
        });
    } else {
        document.location = url;
    }
}
/**
* Function to track a click on a link in the article announcement area.
*/
$('#articleAnnouncement a').on('click', function(){
  ga('send', 'event', 'link', 'click', 'articleAnnouncement');
});
</script>

<script type="text/javascript">
 setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2421.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



<!-- Hotjar Tracking Code for http://nngroup.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:121605,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

        <script src="https://media.nngroup.com/static/js/analytics/track_outbound_link.js"></script>
        
        
        
    </head>
    <body class="location-articles articles-detail">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
    <header id="page-header">
        <div id="cart" class="shopping-cart mobile-cart show-for-small-down"></div>
        <div class="row">
            <div id="masthead" class="masthead small-12 medium-7 column">

                <!-- The button to toggles menu on small screen -->

                <h1><a href="../../index.php"><img src="http://media.nngroup.com/static/img/logo-wordmark.png" width="74" height="30"> Nielsen <span class="norman">Norman</span> Group</a></h1>
                <p class="tagline show-for-medium-up">Evidence-Based User Experience Research, Training, and Consulting</p>

                <!-- The button to toggles form search on small screen -->


                <div style="clear: right;width:  100%;height: 1px;"></div><?php define('ROOT_PATH',$_SERVER["DOCUMENT_ROOT"] . '/apps/navigation_styles/navigations/1'); ?><?php include(ROOT_PATH)."/navigation.php"; ?>

                <!-- Drop down menu on small screen -->
                <div id="hideNav" class="show-for-small-down">
                  
                  <ul class="menu-content">
                    <li><a href="../../index.php">Home</a></li>
                    <li class="has-submenu">
                      <a>Training</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../training/index.php">Overview</a></li>
                        
<li id="nav-ux-conference"><a href="../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../online-seminars/index.php"><span>Online Seminars</span></a></li>

                      </ul>
                    </li>
                    <li class="has-submenu">
                      <a>Consulting</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
  <li><a href="../../consulting/index.php">Overview</a></li>
  
  <li><a href="../../consulting/benchmarking/index.php">Benchmarking</a></li>
  
  <li><a href="../../consulting/expert-design-review/index.php">Expert Design Review</a></li>
  
  <li><a href="../../consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
  
  <li><a href="../../consulting/usability-testing/index.php">Usability Testing </a></li>
  
  <li><a href="../../consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
  
  <li><a href="../../in-house-training/index.php">In-House Training</a></li>
</ul>

                    </li>

                    <li><a href="../../reports/index.php">Reports</a></li>
                    <li><a href="../index.php">Articles</a></li>
                    <li class="has-submenu"><a>About NN/G</a><i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../about/index.php">Overview</a></li>
                        <li><a href="../../people/index.php">People</a></li>
                        <li><a href="../../about/why-nng/index.php">Why NN/g?</a></li>
                        <li><a href="../../about/contact/index.php">Contact</a></li>
                        <li><a href="../../news/index.php">News</a></li>
                        <li><a href="../../about/history/index.php">History</a></li>
                        <li><a href="../../books/index.php">Books</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                </div>

                <hr class="header-divider show-for-small-only"/>


            </div>
       </div>

        <div id="hideNav">            <nav class="primary-nav small-12 column">
                <ul>
                    <li><a class="navitem" id="nav-home" href="../../index.php">Home</a></li>
                    <li><a class="navitem" id="nav-training" href="../../training/index.php">Training</a></li>
                    <li><a class="navitem" id="nav-consulting" href="../../consulting/index.php">Consulting</a></li>
                    <li><a class="navitem" id="nav-reports" href="../../reports/index.php">Reports</a></li>
                    <li><a class="navitem" id="nav-articles" href="../index.php">Articles</a></li>
                    <li><a class="navitem" id="nav-about" href="../../about/index.php">About NN/G</a></li>
                </ul>
            </nav>
            <nav class="secondary-nav small-12 medium-12 large-12 columns">
                
                
            </nav>
        </div>
    </header>
    <div class="l-content">
    
  

  

  <div class="row">
    
      
      <section class="small-12 medium-3 columns collapsable-content l-navrail articles-left-column">
        <h1 class="show-for-small-down collapse-link mobile-browse-header">Browse by Topic and Author</h1>
          <div class="row collapsable">

            
            <div class="small-6 medium-12 columns">
              <section id="article-popular-topics" style="display: none;">
                
                <h1>Topics</h1>
                <ul id="article-popular-topics">
                    
                    <li>
                        
                        <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/intranets/all/index.php">Intranets</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/user-testing/all/index.php">User Testing</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                        
                    </li>
                    
                    <li><a href="#" id="article-see-all-topics"><strong>See all topics&hellip;</strong></a></li>
                </ul>
                
              </section>
              <section id="article-all-topics" style="display: none;">
                
                <h1>
                  <span class="show-for-medium-up">All Article Topics</span>
                  <span class="show-for-small-down">All Topics</span>
                  (<a href="#" id="article-hide-all-topics">hide</a>)
                </h1>
                <ul>
                  
                  <li>
                    
                    <a href="../../topic/accessibility/all/index.php">Accessibility</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/agile/all/index.php">Agile</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/analytics-and-metrics/all/index.php">Analytics &amp; Metrics</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/applications/all/index.php">Application Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/b2b-websites/all/index.php">B2B Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/behavior-patterns/all/index.php">Behavior Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/branding/all/index.php">Branding</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/content-strategy/all/index.php">Content Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/corporate-websites/all/index.php">Corporate Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/design-patterns/all/index.php">Design Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-design-process/all/index.php">Design Process</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/email/all/index.php">Email</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/eyetracking/all/index.php">Eyetracking</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/heuristic-evaluation/all/index.php">Heuristic Evaluation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/human-computer-interaction/all/index.php">Human Computer Interaction</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-ideation/all/index.php">Ideation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/information-architecture/all/index.php">Information Architecture</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/interaction-design/all/index.php">Interaction Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/international-users/all/index.php">International Users</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/intranets/all/index.php">Intranets</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-management/all/index.php">Management</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/navigation/all/index.php">Navigation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/non-profit-websites/all/index.php">Non-Profit Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/personas/all/index.php">Personas</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/persuasive-design/all/index.php">Persuasive Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/prototyping/all/index.php">Prototyping</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/psychology-and-ux/all/index.php">Psychology and UX</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/research-methods/all/index.php">Research Methods</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/search/all/index.php">Search</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/social-media/all/index.php">Social Media</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/strategy/all/index.php">Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/user-testing/all/index.php">User Testing</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-teams/all/index.php">UX Teams</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/visual-design/all/index.php">Visual Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/writing-web/all/index.php">Writing for the Web</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/young-users/all/index.php">Young Users</a>
                    
                  </li>
                  
                </ul>
                
              </section>
            </div>
            <script>
              /* Determine which topics list will be displayed: the full or the
               * partial.  We are inlining this close to the list itself to
               * prevent rendering delays on the client
               */
              var topic_list = $.cookie("article-topics");

              var popular_topics = $('#article-popular-topics');
              var all_topics = $('#article-all-topics');

              if(topic_list == 1) {
                  popular_topics.hide();
                  all_topics.show();
              } else {
                  popular_topics.show();
                  all_topics.hide();
              }
            </script>
            

            
            <div class="small-6 medium-12 columns">
              
              <section id="article-initial-authors">
                  <h1>Author</h1>
                  <ul>
                    
                    <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                    
                    <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                    
                    <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                    
                    <li><a href="#" onClick="$('#article-initial-authors').hide(); $('#article-all-authors').show();"><strong>See all authors&hellip;</strong></a></li>
                  </ul>
              </section>
              <section id="article-all-authors" style="display: none;">
                <h1>
                  All Authors
                  (<a href="#" id="article-hide-all-authors">hide</a>)
                </h1>
                <ul>
                  
                  <li><a href="../author/raluca-budiu/index.php">Raluca Budiu</a></li>
                  
                  <li><a href="../author/patty-caya/index.php">Patty Caya</a></li>
                  
                  <li><a href="../author/susan-farrell/index.php">Susan Farrell</a></li>
                  
                  <li><a href="../author/therese-fessenden/index.php">Therese Fessenden</a></li>
                  
                  <li><a href="../author/kim-flaherty/index.php">Kim Flaherty</a></li>
                  
                  <li><a href="../author/sarah-gibbons/index.php">Sarah Gibbons</a></li>
                  
                  <li><a href="../author/aurora-harley/index.php">Aurora Harley</a></li>
                  
                  <li><a href="../author/kate-kaplan/index.php">Kate Kaplan</a></li>
                  
                  <li><a href="../author/page-laubheimer/index.php">Page Laubheimer</a></li>
                  
                  <li><a href="../author/angela-li/index.php">Angie Li</a></li>
                  
                  <li><a href="../author/hoa-loranger/index.php">Hoa Loranger</a></li>
                  
                  <li><a href="../author/kate-meyer/index.php">Kate Meyer</a></li>
                  
                  <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                  
                  <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                  
                  <li><a href="../author/kara-pernice/index.php">Kara Pernice</a></li>
                  
                  <li><a href="../author/christian-rohrer/index.php">Christian Rohrer</a></li>
                  
                  <li><a href="../author/amy-schade/index.php">Amy Schade</a></li>
                  
                  <li><a href="../author/katie-sherwin/index.php">Katie Sherwin</a></li>
                  
                  <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                  
                  <li><a href="../author/kathryn-whitenton/index.php">Kathryn Whitenton</a></li>
                  
                </ul>
              </section>
              
            </div>
            
          </div>

          
          <section class="hide-for-small-down">
            
              
              <h1>Recent Articles</h1>
              
              <ul>
                  
                  <li><a href="../seamless-cross-channel/index.php">Seamlessness in the Omnichannel User Experience</a></li>
                  
                  <li><a href="../brand-intention-interpretation/index.php">Experience Design: Bridging Brand Intention and Brand Interpretation</a></li>
                  
                  <li><a href="../eyetracking-tasks-efficient-scanning/index.php">Scanning Patterns on the Web Are Optimized for the Current Task</a></li>
                  
                  <li><a href="../flat-design-best-practices/index.php">Flat-Design Best Practices</a></li>
                  
                  <li><a href="../ux-user-stories/index.php">Accounting for UX Work with User Stories in Agile Projects</a></li>
                  
                  <li><a href="../index.php"><strong>See all articles&hellip;</strong></a></li>
              </ul>
              
            
          </section>
          

          
          
          <section class="hide-for-small-down">
            <h1>Popular Articles</h1>
            
            <ul>
                
                <li><a href="../usability-101-introduction-to-usability/index.php">Usability 101: Introduction to Usability</a></li>
                
                <li><a href="../top-10-mistakes-web-design/index.php">Top 10 Mistakes in Web Design</a></li>
                
                <li><a href="../how-users-read-on-the-web/index.php">How Users Read on the Web</a></li>
                
                <li><a href="../f-shaped-pattern-reading-web-content/index.php">F-Shaped Pattern For Reading Web Content</a></li>
                
                <li><a href="../ten-usability-heuristics/index.php">10 Usability Heuristics for User Interface Design</a></li>
                
                <li><a href="../intranet-design-2016/index.php">10 Best Intranets of 2016</a></li>
                
                <li><a href="../which-ux-research-methods/index.php">When to Use Which User-Experience Research Methods</a></li>
                
                <li><a href="../response-times-3-important-limits/index.php">Response Times: The 3 Important Limits</a></li>
                
                <li><a href="../why-you-only-need-to-test-with-5-users/index.php">Why You Only Need to Test with 5 Users</a></li>
                
                <li><a href="../page-fold-manifesto/index.php">The Fold Manifesto: Why the Page Fold Still Matters</a></li>
                
            </ul>
          </section>
          
          

          
            <section class="hide-for-small-down">
              
            </section>
          

      </section>
      

      
        <div class="small-12 medium-9 columns content-body">
          




<article class="l-subsection alertbox-article-content article-detail-bl">
  
  <header>
    <div class="l-subsection">
      <h1>10 Best Intranets of 2013</h1>
      <div class="author-meta">by
        
          
            
            <a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a>
            
            and
            
            <a href="../author/kara-pernice/index.php">Kara Pernice</a>
            
          
        on  January 6, 2013
        
      </div>

      

      
      <div class="topic-links">
        Topics:
        
          <ul>

  <li><a href="../../topic/intranets/index.php">Intranets</a></li>

</ul>
        
      </div>
      
    </div>
  </header>
  

  
  
  <section>
    <p class="lede"><strong>Summary:</strong> Intranet team size has doubled, but companies do get something for this investment, including better-integrated social features and better information filtering. 70% of winners used SharePoint, but with extensive customization.</p>
    <br />
  </section>
  
  

  
  <section id="article-body"><p>
	The 10 best-designed intranets for 2013 are:</p>
<ul style="margin-left: 40px;">
	<li>
		<strong>Acorda Therapeutics</strong>, Inc., a biotechnology company (U.S.)</li>
	<li>
		<strong>American International Group</strong>, Inc., an insurance organization that provides life insurance and retirement services to commercial, institutional, and individual customers in more than 130 countries (U.S.)</li>
	<li>
		<strong>AT&amp;T</strong>, a telecommunications company (U.S.)</li>
	<li>
		<strong>Hager Group</strong>, a manufacturer in the electrical industry for residential and commercial buildings (Germany)</li>
	<li>
		<strong>Luzerner Kantonalbank </strong> AG, one of Switzerland’s largest cantonal banks, focuses on basic banking services, mortgages, corporate financing, and wealth management advice, and offers integrated wealth advisory services in the private banking segment (Switzerland)</li>
	<li>
		<strong>ONO</strong>, a telecommunications company (Spain)</li>
	<li>
		<strong>Saudi Commission for Tourism and Antiquities </strong> (SCTA), a governmental agency that specializes in and is responsible for supporting and controlling tourism places and activities and antiquities that represent the history of Saudi Arabia (Saudi Arabia)</li>
	<li>
		<strong>Swiss Mobiliar Insurance &amp; Pensions</strong>, an insurance company (Switzerland)</li>
	<li>
		<strong>WorkSafeBC</strong>, the Worker’s Compensation Board of British Columbia (Canada)</li>
	<li>
		<strong>XL Group </strong> plc, an insurance and reinsurance company servicing industrial, commercial, and professional firms throughout the world (Ireland)</li>
</ul>
<p>
	&gt; Read more <a href="../../news/item/2013-intranet-design-awards/index.php"> about the winning teams</a>.</p>
<p>
	Most of our winners are full-scale intranet applications designed to serve the entire organization and a variety of job roles. Two of this year’s winners have specialized intranets: AT&amp;T offers a knowledge management portal for customer-facing employees, and Luzerner Kantonalbank’s intranet caters to its customer advisors.</p>
<p>
	This is the second year in a row that the insurance business fostered 3 of our top-10 spots. The utilities and telecommunications industries also repeated <a href="../intranet-design-2012/index.php" title="Alertbox: 10 Best Intranets of 2012"> last year</a>’s tally with two winners each.</p>
<p>
	In summary, six industries are at the forefront of intranet design in this year’s winning set:</p>
<ul style="margin-left: 40px;">
	<li>
		Insurance (3)</li>
	<li>
		Utility (2)</li>
	<li>
		<a href="../../reports/best-government-agency-and-public-sector-intranets/index.php">Government</a> (2)</li>
	<li>
		Biotechnology (1)</li>
	<li>
		Financial (1)</li>
	<li>
		<a href="../../reports/best-industrial-product-manufacturing-intranets/index.php">Manufacturing</a> (1)</li>
</ul>
<p>
	<a href="../../reports/best-financial-sector-intranets/index.php" title="Nielsen Norman Group report:  Best Financial Sector Intranets">Financial organizations have the second greatest showing</a> overall, with 18% of winners since 2001—second only to <a href="../../reports/best-technology-sector-intranets/index.php" title="Nielsen Norman Group report: Best High-Tech Company Intranets"> technology firms</a> (22%).</p>
<p>
	Although the U.S. and European economies are still suffering, the intranet designs in these countries are flourishing. This year, winning companies come from seven different countries:</p>
<ul style="margin-left: 40px;">
	<li>
		United States (3)</li>
	<li>
		Switzerland (2)</li>
	<li>
		Canada (1)</li>
	<li>
		Germany (1)</li>
	<li>
		Ireland (1)</li>
	<li>
		Saudi Arabia (1)</li>
	<li>
		Spain (1)</li>
</ul>
<p>
	The Saudi Commission for Tourism and Antiquities (SCTA) is the first winner of our Design Annual from the Middle East. The rest of the winning countries are no strangers to our top-10 list. Since our first Design Annual, the U.S. has won 66 times; Canada, 9; Germany, 8; Switzerland, 7; Spain, 5; and Ireland, 3. The Swiss showing might be the most impressive, given the country’s size (7.9 million people, or 2.5% of the U.S.).</p>
<h2>
	Great Intranets Require Years to Create</h2>
<p>
	Creating a new intranet or intranet redesign has never been a small or quick endeavor. Since <a href="../the-10-best-intranet-designs-of-2001/index.php" title="Alertbox: 10 best intranets of 2001"> 2001</a>, we’ve tracked the length of time it takes to create a great intranet from inception to launch. On average, the process lasts for 42 months—or about <strong> 3.5 years</strong>. This year, the average was 27 months, or about 2.3 years to create the winning designs.</p>
<h2>
	Smaller Companies Now Leading the Way</h2>
<p>
	For the past four years, smaller organizations have been gobbling up most of the top-10 spots. This year, the average number of employees at organizations is 18,800, which is the smallest since we launched our contest 13 years ago (excluding 2004’s government-only focused Design Annual). Organization sizes this year range from 350 employees at Acorda Therapeutics to 127,000 at AT&amp;T. Seven sites support fewer than 5,000 employees, while an eighth supports just 6,000.</p>
<p>
	Very large companies do make some showing, but across all 13 Design Annuals, the overall average company size is about 57,000 employees—again emphasizing the smaller size of this new crop of winners. In recent years, the average number of employees at winning organizations was:</p>
<ul style="margin-left: 40px;">
	<li>
		<a href="../10-best-intranets-of-2009/index.php" title="Alertbox: 10 Best Intranets of 2009">2009</a>: 37,500</li>
	<li>
		<a href="../10-best-intranets-of-2010/index.php" title="Alertbox: 10 Best Intranets of 2010">2010</a>: 39,100 (excluding the mammoth outlier Walmart, with its 1.4 million store associates)</li>
	<li>
		<a href="../10-best-intranets-of-2011/index.php" title="Alertbox: 10 Best Intranets of 2011">2011</a>: 37,900</li>
	<li>
		<a href="../intranet-design-2012/index.php" title="Alertbox: 10 Best Intranets of 2012">2012</a>: 19,700</li>
	<li>
		2013: 18,800</li>
</ul>
<p>
	The numbers were essentially the same three years in a row, but dropped last year and held around that number this year.</p>
<p>
	Two years might not be quite enough data to declare a trend, but we’ll go out on a limb: the data seems to indicate that it’s <strong> getting easier to create a good intranet</strong>, in that it’s now within the reach of somewhat smaller organizations that have more limited resources.</p>
<h2>
	Team Sizes Are Still Growing</h2>
<p>
	Although organization size has been decreasing, the average intranet team size is increasing—this year to an impressive all-time Design Annual high of <strong> 27 people working on the intranet</strong>. AT&amp;T had the most team members we’ve seen—a notable 107—which brought this year’s average up. But, even if we exclude AT&amp;T, this year’s average is still the highest at 18 team members.</p>
<p>
	Intranet teams equal 0.14% of the organizations’ entire employee pool (i.e.,  <strong> 1.4 intranet staff members per thousand employees</strong>). This is double the previous high of 0.07% last year.</p>
<p style="text-align: center;">
	<img alt="Intranet Team Size as Percentage of Company Size (total number of employees)" src="http://media.nngroup.com/media/editor/2013/01/03/intranet-staff-growth.png" style="width: 488px; height: 246px;"/><br/>
	<em>Growth in intranet team size as percentage of the company's total number of employees, 2001-2013.<br/>
	(Excluding 2004's government-only intranet awards.) </em></p>
<p>
	As the chart shows, intranet team sizes were stable (and small) from 2001 through 2007 and then grew a bit between 2007 and 2011. And the last two years have seen drastically bigger intranet teams—at least compared with past years' tepid growth.</p>
<p>
	More resources are being allocated to intranet design and it shows. To make a great intranet, you need <a href="../../reports/intranet-management-and-teams/index.php" title="Nielsen Norman Group report: Managing the Intranet and Teams"> adequate people resources</a>. It’s not possible to piece together a first-rate intranet that informs and motivates employees and increases productivity and sharing unless you have the staff to design, develop, deploy, write, manage, and govern. It’s not fair to ask a tiny team to take on an endeavor as great as designing and managing an intranet, even if you are “simply deploying” an out-of box solution.</p>
<h2>
	Continued Involvement from Outside Consultants</h2>
<p>
	Winning intranet teams seek extra hands and guidance from outside consultants. In 2004, we began gathering information about each winning intranet team’s makeup; at that time, only four out of the 10 winning teams included external consultants. As in the previous two years, eight of this year’s 10 teams have included outside consultants. Many of those consultants served in development roles (with SharePoint, in particular), but they also worked on design, development, usability research, planning, project management, and scheduling.</p>
<p>
	External agencies or independent consultants provide targeted expertise—that is, skills that intranet teams don’t need permanently on hand—and add credibility to employee insights.</p>
<h2>
	Involving Content Authors Early In the Design Process</h2>
<p>
	The best intranet teams involve people from around the organization at the new intranet’s very inception, ensuring that employees from varied teams, offices, and cities describe their needs and way of working. One group that used to fall through the cracks was content writers, who were often brought in later in the design process. Winning teams this year met with content owners and writers very early on, so they could relentlessly cut unused content, edit older content, give feedback, and have adequate time to migrate, test, and optimize.</p>
<p>
	We know from our research on both writing for the web  and intranet content  that words are often the most impactful part of the total user experience, because users go to both websites and intranets to get information.</p>
<h2>
	Feature Trends</h2>
<p>
	Strong feature trends in intranets in years past include:</p>
<ul style="margin-left: 40px;">
	<li>
		<a href="../mega-menus-work-well/index.php" title="Alertbox: Mega Menus Work Well for Site Navigation">mega menus </a></li>
	<li>
		video channels</li>
	<li>
		beefed-up personal profile pages in the employee directory</li>
	<li>
		personalized homepages and sections</li>
</ul>
<p>
	These trends persist this year, along with a few new ones, including:</p>
<ul style="margin-left: 40px;">
	<li>
		Featuring the organization’s <strong> feeds from Twitter, Facebook, LinkedIn</strong>, and other social sites on the home or news pages. In the past, social features on intranets were often relegated to internal sharing, not to tapping into what’s happening outside the office walls. Acorda Therapeutics, Inc. and AIG both offer especially good examples here.</li>
	<li>
		<strong>Including filters </strong> in the UI to help users hone in on the right information set. Using facets to decrease and target content in search results was once too foreign or difficult for users to handle. But filters have become a better designed and more common website staple, and intranets are now following suit. XL Group, Luzerner Kantonalbank, AIG, and AT&amp;T have some good implementations.</li>
	<li>
		Offering <strong> actionable links </strong> in <a href="../../reports/intranet-searching/index.php" title="Nielsen Norman Group report:  Intranet Usability Guidelines - Searching the Intranet and the Employee Directory"> people search suggestions</a> that include the person’s most relevant information and let users invoke actions from the list itself, such as emailing, calling, and bookmarking the contact. Ono, Luzerner Kantonalbank, and Acorda display interesting features like this.</li>
</ul>
<h2>
	Mobile Optimization Is Paused</h2>
<p>
	Like <a href="../intranet-design-2012/index.php"> last year</a>, only one of this year’s winners offers a special mobile version of the intranet; growth has simply paused in the mobile intranet area. Our 2009 Design Annual had three winning intranet designs optimized for mobile, and in 2011 the mobile space looked even more promising, as the number doubled to six winners.</p>
<p>
	This year, AT&amp;T offers a <strong> mobile app optimized for iPads</strong>, built primarily to support the company’s frontline sellers and sales management team and to align with AT&amp;T’s retail mobilization roadmap. The company will support Android and Windows operating systems as its frontline sellers begin migrating to those tablet operating systems. A great challenge for this and other teams was determining whether to develop a mobile web site or native mobile apps for each operating system; at AT&amp;T, the team created a hybrid app, using HTML5 wherever possible but still offering the usability benefits of native controls where needed.</p>
<p>
	Sites such as Swiss Mobiliar and Acorda offer mobile intranet access to employees on the network, but neither site is optimized for mobile.</p>
<p>
	Common barriers to mobile design entry for intranets include:</p>
<ul style="margin-left: 40px;">
	<li>
		data security concerns,</li>
	<li>
		difficulty of choosing a platform,</li>
	<li>
		lack of resources to create and maintain the design; and</li>
	<li>
		uncertainty about whether to implement a full feature set with a good mobile user experience or an app for particular tasks.</li>
</ul>
<h2>
	Sharepoint Advice</h2>
<p>
	Among this year’s winners, <strong> 70% use Microsoft SharePoint </strong> in some way to create their wonderful intranets. We typically avoid making recommendations about specific intranet software and development tools because such recommendations are typically too narrow and short-lived for a broad design audience. However, given the prevalence of SharePoint use among this year’s winners—and the growth in SharePoint use in general—we’ll share here some of the winning team members’ tales of SharePoint-related gladness and anguish.<br/>
	Some of the strongest takeaways:</p>
<ul style="margin-left: 40px;">
	<li>
		<a href="../sharepoint-destroy-intranet-design/index.php" title="Alertbox: Does SharePoint Destroy Intranet Design?">Use SharePoint features to tackle usability challenges</a>. Several vexing intranet usability issues can be solved using SharePoint features:
		<ul>
			<li>
				Improve search capabilities using embedded indexing of all content. SharePoint enables indexing not only by document or page titles, but also by all content on every page. Organizations such as Hager Group used this to greatly improve search capabilities. The taxonomy model facilitates efficient metadata or full text searches with refinement options.</li>
			<li>
				Facilitate document creation and management through MS Office integration.</li>
			<li>
				Restrict and hide features, such as editing controls, from users who don’t need them.</li>
		</ul>
	</li>
	<li>
		Plan for <strong> design customization </strong> and development to make SharePoint work well for your organization. It’s a misconception that any out-of-box product will be great for an organization; such products typically require considerable design and development work. Indeed, you might need to hire external resources to help with development. Five of the seven sites that use SharePoint hired external SharePoint consultants to make the most of the system.</li>
	<li>
		<strong>Educate employees about team (collaboration) spaces</strong>, and develop rules about when to create new spaces. SharePoint makes it easy to create team spaces that let non-developers set up and maintain areas for people to communicate. These areas can become thriving, breathing ecosystems that aid teams—or dark silos where information is old, duplicated, buried, and difficult to find, especially for people unfamiliar with the space. If organizations aren’t careful, an old intranet problem—having content hidden among many different intranet sites—could easily rear its ugly head in a slightly altered form today. To avoid this: 1) ensure that all team space content is well indexed; 2) make a plan for integrating far-reaching content into the main intranet’s IA; and 3) encourage people to use an existing space when possible, rather than making a new one. It’s also important to designate “official” non-team-space content writers. Team spaces let people share all kinds of content, which is a very powerful model within organizations. However, with this model, general corporate content can fall through the cracks. To avoid this, establishing a communications team for the organization can be helpful.</li>
	<li>
		Understand that <strong> implementing your own branding </strong> can take time. Designers agree that using the organization’s branding is a SharePoint specialist’s job, as is customizing the default SharePoint UI to draw attention to particular areas.</li>
	<li>
		<strong>Keep permissions simple</strong>. Designers tend to make personalization very helpful for users, but complex on the backend due to very specific roles. In all systems, this can be a quagmire to plan and maintain, and SharePoint is no different. To avoid this, winners this year recommend that you protect confidential documents, but don’t put too much granularity in the permissions.</li>
	<li>
		Accompany <a href="../social-networking-on-intranets/index.php" title="Alertbox:  Social Features on Intranets: Case Studies of Enterprise 2.0"> Enterprise 2.0 features</a> with good planning and employee communication. SharePoint makes it easy to switch on powerful features such as commenting and rating, but employees need guidance on how to use these features on intranets. For example, management should formally sanction and use social features, and a document should live on the intranet describing expectations and rules surrounding their use. To assist content writers in encouraging reader responses, have them add a question or a call to action at the end of all articles to get readers thinking about the topic. Finally, persuade champions to start the conversations; many people are interested in commenting, but don’t want to be the first to do so.</li>
</ul>
<h2>
	Full Report</h2>
<p>
	<a href="../../reports/10-best-intranets-2013/index.php" title="Nielsen Norman Group report">373-page Intranet Design Annual</a> with 204 full-color screenshots of the 10 winners for 2013 is available for download.</p>
<p>
	(See also: <a href="../intranet-design/index.php">this year's Intranet Design Annual</a>.)</p>
</section>
  

  
  <section>
    
        
    
  </section>
  
  
  
  
  

  


  <section>
    <br />
    <p class="share"><b>Share <span class="hide-for-small-down">this article</span>:</b> <a href="https://twitter.com/intent/tweet?url=http://www.nngroup.com/articles/intranet-design-2013/&amp;text=10%20Best%20Intranets%20of%202013&amp;via=nngroup">Twitter</a> | <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=http://www.nngroup.com/articles/intranet-design-2013/&amp;title=10%20Best%20Intranets%20of%202013&amp;source=Nielsen%20Norman%20Group">LinkedIn</a> | <a href="https://plus.google.com/share?url=http://www.nngroup.com/articles/intranet-design-2013/">Google+</a> | <a href="mailto:?subject=NN/g Article: 10 Best Intranets of 2013&amp;body=http://www.nngroup.com/articles/intranet-design-2013/">Email</a></p>
    <hr />
  </section>

  <footer class="l-subsection">
    
    
    <div class="learn-more">
      <h2>Learn More</h2>
      <p><a href="../subscribe/index.php">Subscribe to the weekly newsletter</a> to get notified about future articles.</p>
      </div>
    
    

    <div class="row show-for-small-down related-content">
      
      
      

      
      
        <div class="column small-12 topic-links">All articles about:
          <ul>
            
            <li><a href="../../topic/intranets/index.php">Intranets</a></li>
            
          </ul>
        </div>
      
      

      
      
      

      
      
        <div class="small-12 column">
          <h3>Research Reports</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../reports/intranet-design-annual/index.php">2017 Intranet Design Annual</a></li>
              
            
              
                <li><a href="../../reports/intranet-usability-guidelines/index.php">Intranet Usability Guidelines: Findings from User Testing of 42 Intranets</a></li>
              
            
              
                <li><a href="../../reports/intranet-portals-experiences-real-life-projects/index.php">Intranet Portals: UX Design Experience from Real-Life Projects</a></li>
              
            
              
                <li><a href="../../reports/intranet-information-architecture-design-methods/index.php">Intranet Information Architecture Design Methods and Case Studies</a></li>
              
            
              
                <li><a href="../../reports/best-merged-intranets/index.php">Mergers and Acquisitions, and the Resulting Intranets 2007-2016</a></li>
              
            
          </ul>
        </div>
      
      

      
      
        <div class="small-12 column">
          <h3>Online Seminars</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../online-seminars/improving-intranet-content/index.php">Improving Intranet Content</a></li>
              
            
              
                <li><a href="../../online-seminars/intranet-users-find/index.php">Helping Intranet Users Find What They Need</a></li>
              
            
          </ul>
        </div>
      
      
    </div>

    <div class="hide-for-small-down row related-content">
      <div class="medium-6 columns">
        
        
          <div>
            <h3>Articles</h3>
            <ul class="no-bullet">
              
                
                <li><a href="../intranet-design/index.php">10 Best Intranets of 2017</a></li>
                
              
                
                <li><a href="../top-intranet-design-mistakes/index.php">The Top Enduring Intranet-Design Mistakes: 7 Deadly Sins</a></li>
                
              
                
                <li><a href="../top-intranet-trends/index.php">Top 10 Intranet Trends of 2016</a></li>
                
              
                
                <li><a href="../intranet-content-authors/index.php">3 Ways to Inspire Intranet Content Authors</a></li>
                
              
                
                <li><a href="../sharepoint-intranet-ux/index.php">Design a Brilliant SharePoint Intranet</a></li>
                
              
            </ul>
          </div>
        
        

        
          
          <div>
            <h3>Research Reports</h3>
            <ul class="no-bullet">
              
                
                  <li><a href="../../reports/intranet-design-annual/index.php">2017 Intranet Design Annual</a></li>
                
              
                
                  <li><a href="../../reports/intranet-usability-guidelines/index.php">Intranet Usability Guidelines: Findings from User Testing of 42 Intranets</a></li>
                
              
                
                  <li><a href="../../reports/intranet-portals-experiences-real-life-projects/index.php">Intranet Portals: UX Design Experience from Real-Life Projects</a></li>
                
              
                
                  <li><a href="../../reports/intranet-information-architecture-design-methods/index.php">Intranet Information Architecture Design Methods and Case Studies</a></li>
                
              
                
                  <li><a href="../../reports/best-merged-intranets/index.php">Mergers and Acquisitions, and the Resulting Intranets 2007-2016</a></li>
                
              
            </ul>
          </div>
          
        

      </div>
      <div class="medium-6 columns">
        
        
          
          <div>
            <h3>Online Seminars</h3>
            <ul class="no-bullet">
            
                <li><a href="../../online-seminars/improving-intranet-content/index.php">Improving Intranet Content</a></li>
            
                <li><a href="../../online-seminars/intranet-users-find/index.php">Helping Intranet Users Find What They Need</a></li>
            
            </ul>
          </div>
          
        
      </div>
    </div>
  </footer>
</article>

        </div>

      

    
  </div>
  <div class="row">
    
      <div class="column small-12 show-for-small-down l-navrail announcements">
        
          
        
      </div>
    
    
  </div>

    </div>
    




<footer id="page-footer">
    <div class="row hide-for-print">
        <div class="column small-12 medium-6">
            <section class="footer-popular">
                
                <h1>Popular Topics</h1>
                <div style="float: left;  width: 15em; display: block;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/applications/index.php">Application Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/e-commerce/index.php">E-Commerce Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/eyetracking/index.php">Eyetracking </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/information-architecture/index.php">Information Architecture </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/intranets/index.php">Intranet Design </a></li>
	<li><a href="../../topic/interaction-design/index.php">Interaction Design </a></li>
</ul>
</div>

<div style="float: left;  width: 15em; display: block; list-style: none;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/mobile-and-tablet-design/index.php">Mobile and Tablet Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/credibility-persuasion/index.php">Persuasive Design</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/research-methods/index.php">Research Methods</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/user-testing/index.php">User Testing </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/web-usability/index.php">Web Usability </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/writing-web/index.php">Web Writing &amp; Content Strategy </a></li>
</ul>
</div>

<p>&nbsp;</p>

                
            </section>
        </div>

        <div class="column small-12 medium-6">
            <div class="row">
                <div class="column small-6">
                    <section class="footer-about">
                        <h1>Follow Us</h1>
                        <ul>
	<li><a href="../subscribe/index.php">Subscribe to our weekly UX newsletter</a>,&nbsp;Jakob Nielsen&#39;s Alertbox</li>
	<li>Visit Don Norman&#39;s website, <a href="http://www.jnd.org/"> www.jnd.org </a></li>
	<li>Visit Bruce Tognazzini&#39;s website, <a href="http://www.asktog.com/"> www.asktog.com </a></li>
	<li style="padding-top: 6px;"><a class="twitter-follow-button" data-show-count="false" data-size="large" href="https://twitter.com/nngroup"><img alt="Follow @nngroup on Twitter" height="20" src="https://media.nngroup.com/media/editor/2016/12/30/twitter-follow-button.png" style="width: 117px; height: 20px;" width="117" /></a></li>
	<li style="padding-top: 6px;"><img alt="" height="14" src="https://media.nngroup.com/media/editor/2016/07/26/feed-icon-14x14.png" style="vertical-align:text-top;width:14px;margin-right:.25em;" width="14" /><a href="../../feed/rss/index.php">Subscribe to our RSS feed</a></li>
</ul>

                    </section>
                </div>

                <div class="column small-6">
                    <section class="footer-about right-column">
                        <h1>About
</h1>
                        <ul class="no-bullet">
	<li><a href="../../about/contact/index.php">Contact information</a></li>
	<li><a href="../../about/index.php">About Us</a></li>
	<li><a href="../../about/why-nng/index.php">Why NN/g? </a></li>
	<li><a href="../../return-policy/index.php">Return Policy</a></li>
</ul>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="copyright column small-12">
            <p><a href="../../copyright-and-reprint-info/index.php">Copyright</a> &copy; 1998-2017 Nielsen Norman Group, All Rights Reserved.</p>
        </div>
    </div>
</footer>


    


        <script src="https://media.nngroup.com/static/vendor/fastclick/js/fastclick.min.js"></script>
        <script src="https://media.nngroup.com/static/vendor/foundation/js/foundation.min.js"></script>
        <script src="http://media.nngroup.com/static/js/app.js"></script>
        <script src="http://media.nngroup.com/static/js/scroll-to-fixed.js"></script><script>var externalLinks = document.querySelectorAll('a[href^="http"]');for (var i = externalLinks.length-1; i >= 0; i--) {externalLinks[i].addEventListener("click", function(event) { alert("External links are disabled for this test."); event.preventDefault(); }, false);}var mailLinks = document.querySelectorAll('a[href^="mailto"]');for (var i = mailLinks.length-1; i >= 0; i--) {mailLinks[i].addEventListener("click", function(event) { alert("Email links are disabled for this test."); event.preventDefault(); }, false);}var externalForms = document.querySelectorAll('form');for (var i = externalForms.length-1; i >= 0; i--) {externalForms[i].addEventListener("submit", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false); externalForms[i].addEventListener("click", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false);}</script>

        <script>
            
            $(function() {
                $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
                $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
            });
        </script>

        
<script src="https://media.nngroup.com/static/js/articles.js"></script>


        
        <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1072699960;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072699960/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
    </body>

<!-- Mirrored from www.nngroup.com/articles/intranet-design-2013/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:13:13 GMT -->
</html>
