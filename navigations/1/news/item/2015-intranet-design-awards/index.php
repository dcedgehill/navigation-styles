<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->





    
<!-- Mirrored from www.nngroup.com/news/item/2015-intranet-design-awards/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:13:02 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8">
        <title>2015 Intranet Design Award Winners</title><meta property="og:title" content="2015 Intranet Design Award Winners" />
  
        
        <meta name="description" content="10 best intranets of 2015 announced.">
        <meta property="og:description" content="10 best intranets of 2015 announced." />
        
  
        
	
        

        

        <meta name="robots" content="noarchive">
        <meta name="viewport" content="width=device-width">

        <link rel="search" type="application/opensearchdescription+xml" title="NN/g Search" href="../../../opensearch.xml" />
        <link rel="shortcut icon" href="http://media.nngroup.com/static/img/favicon.ico" />

        <link rel="stylesheet" href="https://media.nngroup.com/static/vendor/font-awesome/css/font-awesome.min.css">

        
        <link rel="stylesheet" href="https://media.nngroup.com/static/css/7232a960f1df.css" type="text/css" />
        <link rel="stylesheet" href="http://fast.fonts.com/t/1.css?apiType=css&amp;projectid=c5846260-9def-46ec-9407-0f1349bd2d64" type="text/css">

        <script src="https://media.nngroup.com/static/vendor/modernizr/js/modernizr.min.js"></script>

        <script type="text/javascript" src="https://media.nngroup.com/static/js/3283be5def74.js"></script>
        

        

        
        
        <!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=229034,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('http://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->


<!-- Google Analytics Tracking Code for http://nngroup.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23132681-1', 'nngroup.com');
  ga('require', 'linkid', 'https://www.nngroup.com/news/item/2015-intranet-design-awards/linkid.js');
  ga('require', 'displayfeatures');

// Optimizely Universal Analytics Integration
 window.optimizely = window.optimizely || [];
 window.optimizely.push("activateUniversalAnalytics");

  ga('send', 'pageview');

</script>
<script>
/**
* Function to track a click on an outbound link in Google Analytics.
*/
function trackOutboundLink(label, url) {
    if(window.ga && ga.loaded) {
        ga('send', 'event', 'outbound', 'click', label, {'hitCallback': function(){document.location = url;}
        });
    } else {
        document.location = url;
    }
}
/**
* Function to track a click on a link in the article announcement area.
*/
$('#articleAnnouncement a').on('click', function(){
  ga('send', 'event', 'link', 'click', 'articleAnnouncement');
});
</script>

<script type="text/javascript">
 setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2421.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



<!-- Hotjar Tracking Code for http://nngroup.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:121605,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

        <script src="https://media.nngroup.com/static/js/analytics/track_outbound_link.js"></script>
        
        
        
    </head>
    <body class="location-about location-news">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
    <header id="page-header">
        <div id="cart" class="shopping-cart mobile-cart show-for-small-down"></div>
        <div class="row">
            <div id="masthead" class="masthead small-12 medium-7 column">

                <!-- The button to toggles menu on small screen -->

                <h1><a href="../../../index.php"><img src="http://media.nngroup.com/static/img/logo-wordmark.png" width="74" height="30"> Nielsen <span class="norman">Norman</span> Group</a></h1>
                <p class="tagline show-for-medium-up">Evidence-Based User Experience Research, Training, and Consulting</p>

                <!-- The button to toggles form search on small screen -->


                <div style="clear: right;width:  100%;height: 1px;"></div><?php define('ROOT_PATH',$_SERVER["DOCUMENT_ROOT"] . '/apps/navigation_styles/navigations/1'); ?><?php include(ROOT_PATH)."/navigation.php"; ?>

                <!-- Drop down menu on small screen -->
                <div id="hideNav" class="show-for-small-down">
                  
                  <ul class="menu-content">
                    <li><a href="../../../index.php">Home</a></li>
                    <li class="has-submenu">
                      <a>Training</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../../training/index.php">Overview</a></li>
                        
<li id="nav-ux-conference"><a href="../../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../../online-seminars/index.php"><span>Online Seminars</span></a></li>

                      </ul>
                    </li>
                    <li class="has-submenu">
                      <a>Consulting</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
  <li><a href="../../../consulting/index.php">Overview</a></li>
  
  <li><a href="../../../consulting/benchmarking/index.php">Benchmarking</a></li>
  
  <li><a href="../../../consulting/expert-design-review/index.php">Expert Design Review</a></li>
  
  <li><a href="../../../consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
  
  <li><a href="../../../consulting/usability-testing/index.php">Usability Testing </a></li>
  
  <li><a href="../../../consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
  
  <li><a href="../../../in-house-training/index.php">In-House Training</a></li>
</ul>

                    </li>

                    <li><a href="../../../reports/index.php">Reports</a></li>
                    <li><a href="../../../articles/index.php">Articles</a></li>
                    <li class="has-submenu"><a>About NN/G</a><i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../../about/index.php">Overview</a></li>
                        <li><a href="../../../people/index.php">People</a></li>
                        <li><a href="../../../about/why-nng/index.php">Why NN/g?</a></li>
                        <li><a href="../../../about/contact/index.php">Contact</a></li>
                        <li><a href="../../index.php">News</a></li>
                        <li><a href="../../../about/history/index.php">History</a></li>
                        <li><a href="../../../books/index.php">Books</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                </div>

                <hr class="header-divider show-for-small-only"/>


            </div>
       </div>

        <div id="hideNav">            <nav class="primary-nav small-12 column">
                <ul>
                    <li><a class="navitem" id="nav-home" href="../../../index.php">Home</a></li>
                    <li><a class="navitem" id="nav-training" href="../../../training/index.php">Training</a></li>
                    <li><a class="navitem" id="nav-consulting" href="../../../consulting/index.php">Consulting</a></li>
                    <li><a class="navitem" id="nav-reports" href="../../../reports/index.php">Reports</a></li>
                    <li><a class="navitem" id="nav-articles" href="../../../articles/index.php">Articles</a></li>
                    <li><a class="navitem" id="nav-about" href="../../../about/index.php">About NN/G</a></li>
                </ul>
            </nav>
            <nav class="secondary-nav small-12 medium-12 large-12 columns">
                
<ul class="inline-list">
    <li id="nav-people"><a href="../../../people/index.php"><span>People</span></a></li>
    <li id="nav-whynng"><a href="../../../about/why-nng/index.php"><span>Why NN/g?</span></a></li>
    <li id="nav-contact"><a href="../../../about/contact/index.php"><span>Contact</span></a></li>
    <li id="nav-news"><a href="../../index.php"><span>News</span></a></li>
    <li id="nav-history"><a href="../../../about/history/index.php"><span>History</span></a></li>
    <li id="nav-books"><a href="../../../books/index.php"><span>Books</span></a></li>
</ul>

            </nav>
        </div>
    </header>
    <div class="l-content">
    


<div class="row">
    <div class="small-12 medium-4 large-4 columns offset-by-eight">
    </div>
</div>
<div class="row">
    <div class="small-12 medium-3 large-3 columns l-navrail news-rail hide-for-small-only">
        <h1><a href="../../index.php">All News Types</a></h1>
        <ul class="no-bullet">
            
                
                    <li><a href="../../type/announcements/index.php">Announcements (52)</a></li>
                
            
                
                    <li><a href="../../type/press-mentions/index.php">Interviews &amp; Press (225)</a></li>
                
            
        </ul>
    </div>
    <div class="small-12 medium-9 large-9 columns">
        <article class="news_detail">
            <div class="l-section-intro">
                <header>
                    <h1>NN/g News</h1>
                    <h2>2015 Intranet Design Award Winners</h2>
                    <p>January 3, 2015</p>
                </header>
            </div>
            <div class="l-subsection">
                <h2>Congratulations to the 2015 Intranet Design Annual Award winners!</h2>

<h2>Accolade</h2>

<p>The Accolade team created an intranet solution that integrates the tools needed to do day-to-day work at the organization. With a focus on serving customers more effectively and efficiently and armed with data from user research, the team created a centralized site that combines customer relationship management, task management, and social tools into a cohesive experience, aiding employees and customers alike.</p>

<p><img alt="" height="480" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2014/12/02/01_accolade_team_photo-2.jpg" width="720" /> <strong>Accolade (in-house) team members</strong> (left to right): Hilda Boerma, Frank Smit, Hiltsje Rinsma, Michiel Booij, Bianca Bijlstra, Berend Hut, Martijn Weesjes, Koen Wemmenhove, Hermen Joostens, Irene Willems-Sloot, and Erik Dokter.</p>

<h2>Adobe</h2>

<p>Creating a unified intranet was no small feat for the Adobe intranet team. They tackled the mammoth undertaking by simultaneously re-platforming, re-architecting, and migrating content from disparate sites and systems. The resulting site uses streamlined navigation to move employees easily between areas of content while allowing them to connect and communicate.</p>

<p><img alt="" height="481" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2014/12/02/02_adobe_teamphoto_sm.jpg" width="720" /><br />
<strong>The Adobe team</strong> (left to right): Ray Brulotte, Andrea Brant, Rich Uyttebroek, Ryan Selewicz, and Gary Tuscany.</p>

<h2>ConocoPhillips</h2>

<p>This impressive intranet includes many productive features, such as regionalized information that is translated based on location and language, optimized search, a concise IA, and well-prioritized content. The Mark hits the design mark in every possible way.</p>

<p><img alt="" height="358" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2014/12/02/03-conoco_phillips_team_photo_0828.PNG" width="720" />&nbsp;<br />
<strong style="line-height: 1.6;">The ConocoPhillips intranet team</strong><span style="line-height: 1.6;">: (left to right, top row) Kanwal Khipple, Cathy Cram, Kunaal Kapoor, Jennifer Hohman, Ray Scippa, (left to right, bottom row) Glen Chambers, Helenita Frounfelkner, Jennifer Clifford, and Juan Larios.</span></p>

<h2>Klick Health</h2>

<p>Moving beyond a traditional intranet, Genome is how Klick Health employees get work done. Genome provides the solutions the data-driven company needs to encourage efficient work and effective communication, including a streamlined candidate-hiring tool, simple ways to recognize employees for work well done, and an innovative method to encourage philanthropic giving.</p>

<p><img alt="" height="720" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2014/12/02/04_klick_team_photo_sm.jpg" width="720" /><br />
<strong>Klick team members (left to right)</strong>: Aaron Goldstein, D&#39;Arcy Rittich, Benjamin Nadler, Leo Horie, Alexey Davydov, Rey Cris&oacute;stomo, Alex Chesser, Andrew Woronowicz, and Rob Brander.</p>

<h2>Saudi Food and Drug Authority</h2>

<p>&ldquo;Bawabaty&rdquo; ( بوابتي ) which means my portal in Arabic, is highly customizable; giving employees powerful tools they need to do their work, and the freedom to do it in their own way.</p>

<p><img alt="" height="667" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2014/12/02/05_sfda_00_team_photo.png" width="720" /><br />
<strong>SFDA project team members and management</strong> (top row, left to right): Falah AL-Mutairi, Faisal Alturaif, Bander Al-Johani, (bottom row, left to right) Mohammed AlMutairi, Abdulaziz Al-Fakhri, Fahad Alquait, Fahad Alanezi, and Abdulaziz Alsughyyer.</p>

<h2>Sprint</h2>

<p>The Sprint intranet includes a buffet of delicious interface components: from inventive wayfinding cues, and the ways and means for employees to socially promote products and services to customers, to a nearly flawless news carousel.</p>

<p><img alt="" height="623" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2014/12/02/06_sprint_team_group220croppednorancudo_sm.jpg" width="720" /><br />
<strong>Some members of the Sprint Business Implementation team</strong>: (back row, left to right) Melody Feekes, Susan Kreifel, Mark Kochanowski, Sarah Hebert, Sally Nellor, Terry Pulliam, Karen Schaeffer; (front row, left to right) Colleen Del Debbio, Karen Downs, Waqar Shah, Beth Zemcik, Hannah Benisch, Fazal Rehman, Beth Doeringer, Alicia Backlund, and Carla Hubbell.</p>

<h2>TAURON Polska Energia SA</h2>

<p>The Tauronet team takes the term &ldquo;role-based&rdquo; to a new level, integrating personalization with striking maturity and essential predictability.</p>

<p><img alt="" height="703" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2015/01/05/tauron_team_update5jan.png" width="720" /><br />
<strong>TAURON intranet team members</strong>: (top row, left to right) Paweł Gniadek, Łukasz Krause, Robert Głowacki, Grzegorz Drozd, Agnieszka Micał, Maciej Morasiewicz; (second row, left to right) Hubert Turaj (EDISONDA), Marcin Kręcioch (EDISONDA), Tomasz Szałaj (IT-Dev), Marcin Jędrusik, Andrzej Prucnal, Dorota Jarosz-Woźny; (third row, left to right) Katarzyna Bajor, Paulina Dynia, Maciej Mączka, Łukasz Łysak, Marceli Frączek, Maciej Rogalski; (bottom row, left to right) Marian Kuś, Marcin Sarna, Małgorzata Wiertel, Monika Meinhart-Burzyńska, and Jeremi Nagacz.&nbsp;</p>

<h2>The Foschini Group</h2>

<p>A fresh, bold design helped move The Foschini Group&rsquo;s intranet, aimed at head-office employees, from a repository for Excel files to a dynamic and engaging way to access information, connect with colleagues and get work done. The team used an Agile process to create a responsive SharePoint solution.</p>

<p><img alt="" height="594" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2014/12/02/08_tfg_intervate_02.JPG" width="720" /> <strong>Members of the TFG intranet team</strong> (back row, left to right): Marc Fletcher (Intervate), Craig Hall (TFG), Kevin Pederson (Intervate), Jean Snyman (TFG), Christo Greeff (Intervate), Marius Botha (TFG); (seated, left to right) Retha Carter (TFG), Kathryn Sakalis (TFG), and Roxanne Pearce (Intervate). Not present: Warren Soper (Intervate), Cindy Matungulu (TFG), and Mo Kola (TFG).</p>

<h2>UniCredit S.p.A.</h2>

<p>An intellectual design resulting from an exhaustive UX process and mind-blowing, cross-organizational work, the system&rsquo;s flexible framework with advanced content-targeting creates productive and happy employees.</p>

<p><img alt="" height="468" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2014/12/02/09_unicredit_team_picture.jpg" width="720" /><br />
<strong>OneGate Group Intranet Core Project Team</strong> (members from the Digital Internal Communications team and from Assist, standing, left to right): Alberto Angeli (Assist); Francesca Gabrielli (Assist), Luca di Bari (Digital Internal Communications) and Joachim Hiorth (Assist); (sitting, left to right) Letizia Chlapoutakis (Digital Internal Communications), Clara Corradi (Assist), Patrizio Regis (Head of Group Internal Communications), and Fabio Delton (Head of Digital Internal Communications). The wall in the background features photos of team members who are part of the larger project team.</p>

<h2>Verizon Communications</h2>

<p>Tasked with providing essential Human Resources information to an increasingly mobile user base of 230,000 employees, Verizon&rsquo;s intranet team reduced content by 50%, scaled back personalization rules by 60%, and created a well-considered and responsive HR portal honed to user tasks.</p>

<p><img alt="" height="355" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2014/12/02/10_verizon_team_photo_0923.png" width="720" /><br />
<strong>Verizon team members</strong> (left to right): Ruben Luque, Nathan Kemp, Anil Kumar, Chethan Makam, Venkata Avasarala, Shernell Saunders, Lolly Chessie, Rachel Knickmeyer, Alan Masters, Lillian Renner, Venkata Suburayalu, Fahimuddin Mohammed, Santhosh Sypureddi, Rajendra Prasad, Gregory Swindle. Not pictured: Gus Attar, Tom Bruser, and Abuthahir Kamalbatcha.</p>

<h2>Full Report</h2>

<p>The 360-page <a href="../../../reports/10-best-intranets-2015/index.php">Intranet Design Annual</a> with 149 full-color screenshots of the 10 winners for 2015 is available for download.</p>

            </div>
        </article>
    </div>
</div>


    </div>
    




<footer id="page-footer">
    <div class="row hide-for-print">
        <div class="column small-12 medium-6">
            <section class="footer-popular">
                
                <h1>Popular Topics</h1>
                <div style="float: left;  width: 15em; display: block;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../../topic/applications/index.php">Application Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../../topic/e-commerce/index.php">E-Commerce Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../../topic/eyetracking/index.php">Eyetracking </a></li>
	<li style="padding-bottom: .5em;"><a href="../../../topic/human-computer-interaction/index.php">Human Computer Interaction </a></li>
	<li style="padding-bottom: .5em;"><a href="../../../topic/information-architecture/index.php">Information Architecture </a></li>
	<li style="padding-bottom: .5em;"><a href="../../../topic/intranets/index.php">Intranet Design </a></li>
	<li><a href="../../../topic/interaction-design/index.php">Interaction Design </a></li>
</ul>
</div>

<div style="float: left;  width: 15em; display: block; list-style: none;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../../topic/mobile-and-tablet-design/index.php">Mobile and Tablet Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../../topic/credibility-persuasion/index.php">Persuasive Design</a></li>
	<li style="padding-bottom: .5em;"><a href="../../../topic/research-methods/index.php">Research Methods</a></li>
	<li style="padding-bottom: .5em;"><a href="../../../topic/user-testing/index.php">User Testing </a></li>
	<li style="padding-bottom: .5em;"><a href="../../../topic/web-usability/index.php">Web Usability </a></li>
	<li style="padding-bottom: .5em;"><a href="../../../topic/writing-web/index.php">Web Writing &amp; Content Strategy </a></li>
</ul>
</div>

<p>&nbsp;</p>

                
            </section>
        </div>

        <div class="column small-12 medium-6">
            <div class="row">
                <div class="column small-6">
                    <section class="footer-about">
                        <h1>Follow Us</h1>
                        <ul>
	<li><a href="../../../articles/subscribe/index.php">Subscribe to our weekly UX newsletter</a>,&nbsp;Jakob Nielsen&#39;s Alertbox</li>
	<li>Visit Don Norman&#39;s website, <a href="http://www.jnd.org/"> www.jnd.org </a></li>
	<li>Visit Bruce Tognazzini&#39;s website, <a href="http://www.asktog.com/"> www.asktog.com </a></li>
	<li style="padding-top: 6px;"><a class="twitter-follow-button" data-show-count="false" data-size="large" href="https://twitter.com/nngroup"><img alt="Follow @nngroup on Twitter" height="20" src="https://media.nngroup.com/media/editor/2016/12/30/twitter-follow-button.png" style="width: 117px; height: 20px;" width="117" /></a></li>
	<li style="padding-top: 6px;"><img alt="" height="14" src="https://media.nngroup.com/media/editor/2016/07/26/feed-icon-14x14.png" style="vertical-align:text-top;width:14px;margin-right:.25em;" width="14" /><a href="../../../feed/rss/index.php">Subscribe to our RSS feed</a></li>
</ul>

                    </section>
                </div>

                <div class="column small-6">
                    <section class="footer-about right-column">
                        <h1>About
</h1>
                        <ul class="no-bullet">
	<li><a href="../../../about/contact/index.php">Contact information</a></li>
	<li><a href="../../../about/index.php">About Us</a></li>
	<li><a href="../../../about/why-nng/index.php">Why NN/g? </a></li>
	<li><a href="../../../return-policy/index.php">Return Policy</a></li>
</ul>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="copyright column small-12">
            <p><a href="../../../copyright-and-reprint-info/index.php">Copyright</a> &copy; 1998-2017 Nielsen Norman Group, All Rights Reserved.</p>
        </div>
    </div>
</footer>


    


        <script src="https://media.nngroup.com/static/vendor/fastclick/js/fastclick.min.js"></script>
        <script src="https://media.nngroup.com/static/vendor/foundation/js/foundation.min.js"></script>
        <script src="http://media.nngroup.com/static/js/app.js"></script>
        <script src="http://media.nngroup.com/static/js/scroll-to-fixed.js"></script><script>var externalLinks = document.querySelectorAll('a[href^="http"]');for (var i = externalLinks.length-1; i >= 0; i--) {externalLinks[i].addEventListener("click", function(event) { alert("External links are disabled for this test."); event.preventDefault(); }, false);}var mailLinks = document.querySelectorAll('a[href^="mailto"]');for (var i = mailLinks.length-1; i >= 0; i--) {mailLinks[i].addEventListener("click", function(event) { alert("Email links are disabled for this test."); event.preventDefault(); }, false);}var externalForms = document.querySelectorAll('form');for (var i = externalForms.length-1; i >= 0; i--) {externalForms[i].addEventListener("submit", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false); externalForms[i].addEventListener("click", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false);}</script>

        <script>
            
            $(function() {
                $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
                $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
            });
        </script>

        


        
        <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1072699960;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072699960/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
    </body>

<!-- Mirrored from www.nngroup.com/news/item/2015-intranet-design-awards/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:13:02 GMT -->
</html>
