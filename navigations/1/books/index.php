<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->





    
<!-- Mirrored from www.nngroup.com/books/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 14:54:45 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1026.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"beacon":"bam.nr-data.net","queueTime":0,"applicationTime":10,"agent":"","licenseKey":"1a30ef9931","errorBeacon":"bam.nr-data.net","applicationID":"2048992","transactionName":"NFxSMRBUX0EDUkIPDA0WdhAMVkVbDV8ZBAwMUkNLFFxURRELdAkMCHVZFhY="}</script>
        <title>Books by Nielsen Norman Group Authors</title><meta property="og:title" content="Books by Nielsen Norman Group Authors" />
  
        
        <meta name="description" content="Classic books about usability and user interface design by Jakob Nielsen, Don Norman, and Bruce ">
        <meta property="og:description" content="Classic books about usability and user interface design by Jakob Nielsen, Don Norman, and Bruce " />
        
  
        
	
        
        <meta name="keywords" content="Jakob Nielsen, Donald A. Norman, Bruce Tognazzini, Tog, Don Norman, books, book, textbooks, textbook, author, authors, usability, HCI, user interface design, Web design">
        

        

        <meta name="robots" content="noarchive">
        <meta name="viewport" content="width=device-width">

        <link rel="search" type="application/opensearchdescription+xml" title="NN/g Search" href="../opensearch.xml" />
        <link rel="shortcut icon" href="http://media.nngroup.com/static/img/favicon.ico" />

        <link rel="stylesheet" href="https://media.nngroup.com/static/vendor/font-awesome/css/font-awesome.min.css">

        
        <link rel="stylesheet" href="https://media.nngroup.com/static/css/7232a960f1df.css" type="text/css" />
        <link rel="stylesheet" href="http://fast.fonts.com/t/1.css?apiType=css&amp;projectid=c5846260-9def-46ec-9407-0f1349bd2d64" type="text/css">

        <script src="https://media.nngroup.com/static/vendor/modernizr/js/modernizr.min.js"></script>

        <script type="text/javascript" src="https://media.nngroup.com/static/js/3283be5def74.js"></script>
        

        

        
        
        <!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=229034,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('http://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->


<!-- Google Analytics Tracking Code for http://nngroup.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23132681-1', 'nngroup.com');
  ga('require', 'linkid', 'https://www.nngroup.com/books/linkid.js');
  ga('require', 'displayfeatures');

// Optimizely Universal Analytics Integration
 window.optimizely = window.optimizely || [];
 window.optimizely.push("activateUniversalAnalytics");

  ga('send', 'pageview');

</script>
<script>
/**
* Function to track a click on an outbound link in Google Analytics.
*/
function trackOutboundLink(label, url) {
    if(window.ga && ga.loaded) {
        ga('send', 'event', 'outbound', 'click', label, {'hitCallback': function(){document.location = url;}
        });
    } else {
        document.location = url;
    }
}
/**
* Function to track a click on a link in the article announcement area.
*/
$('#articleAnnouncement a').on('click', function(){
  ga('send', 'event', 'link', 'click', 'articleAnnouncement');
});
</script>

<script type="text/javascript">
 setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2421.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



<!-- Hotjar Tracking Code for http://nngroup.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:121605,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

        <script src="https://media.nngroup.com/static/js/analytics/track_outbound_link.js"></script>
        
        
        
    </head>
    <body class="location-about location-books">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
    <header id="page-header">
        <div id="cart" class="shopping-cart mobile-cart show-for-small-down"></div>
        <div class="row">
            <div id="masthead" class="masthead small-12 medium-7 column">

                <!-- The button to toggles menu on small screen -->

                <h1><a href="../index.php"><img src="http://media.nngroup.com/static/img/logo-wordmark.png" width="74" height="30"> Nielsen <span class="norman">Norman</span> Group</a></h1>
                <p class="tagline show-for-medium-up">Evidence-Based User Experience Research, Training, and Consulting</p>

                <!-- The button to toggles form search on small screen -->


                <div style="clear: right;width:  100%;height: 1px;"></div><?php define('ROOT_PATH',$_SERVER["DOCUMENT_ROOT"] . '/apps/navigation_styles/navigations/1'); ?><?php include(ROOT_PATH)."/navigation.php"; ?>

                <!-- Drop down menu on small screen -->
                <div id="hideNav" class="show-for-small-down">
                  
                  <ul class="menu-content">
                    <li><a href="../index.php">Home</a></li>
                    <li class="has-submenu">
                      <a>Training</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../training/index.php">Overview</a></li>
                        
<li id="nav-ux-conference"><a href="../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../online-seminars/index.php"><span>Online Seminars</span></a></li>

                      </ul>
                    </li>
                    <li class="has-submenu">
                      <a>Consulting</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
  <li><a href="../consulting/index.php">Overview</a></li>
  
  <li><a href="../consulting/benchmarking/index.php">Benchmarking</a></li>
  
  <li><a href="../consulting/expert-design-review/index.php">Expert Design Review</a></li>
  
  <li><a href="../consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
  
  <li><a href="../consulting/usability-testing/index.php">Usability Testing </a></li>
  
  <li><a href="../consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
  
  <li><a href="../in-house-training/index.php">In-House Training</a></li>
</ul>

                    </li>

                    <li><a href="../reports/index.php">Reports</a></li>
                    <li><a href="../articles/index.php">Articles</a></li>
                    <li class="has-submenu"><a>About NN/G</a><i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../about/index.php">Overview</a></li>
                        <li><a href="../people/index.php">People</a></li>
                        <li><a href="../about/why-nng/index.php">Why NN/g?</a></li>
                        <li><a href="../about/contact/index.php">Contact</a></li>
                        <li><a href="../news/index.php">News</a></li>
                        <li><a href="../about/history/index.php">History</a></li>
                        <li><a href="index.php">Books</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                </div>

                <hr class="header-divider show-for-small-only"/>


            </div>
       </div>

        <div id="hideNav">            <nav class="primary-nav small-12 column">
                <ul>
                    <li><a class="navitem" id="nav-home" href="../index.php">Home</a></li>
                    <li><a class="navitem" id="nav-training" href="../training/index.php">Training</a></li>
                    <li><a class="navitem" id="nav-consulting" href="../consulting/index.php">Consulting</a></li>
                    <li><a class="navitem" id="nav-reports" href="../reports/index.php">Reports</a></li>
                    <li><a class="navitem" id="nav-articles" href="../articles/index.php">Articles</a></li>
                    <li><a class="navitem" id="nav-about" href="../about/index.php">About NN/G</a></li>
                </ul>
            </nav>
            <nav class="secondary-nav small-12 medium-12 large-12 columns">
                
<ul class="inline-list">
    <li id="nav-people"><a href="../people/index.php"><span>People</span></a></li>
    <li id="nav-whynng"><a href="../about/why-nng/index.php"><span>Why NN/g?</span></a></li>
    <li id="nav-contact"><a href="../about/contact/index.php"><span>Contact</span></a></li>
    <li id="nav-news"><a href="../news/index.php"><span>News</span></a></li>
    <li id="nav-history"><a href="../about/history/index.php"><span>History</span></a></li>
    <li id="nav-books"><a href="index.php"><span>Books</span></a></li>
</ul>

            </nav>
        </div>
    </header>
    <div class="l-content">
    

<div class="row">
    <div class="l-section-intro small-12 medium-12 large-12 columns">
        <header>
            <h1>Books</h1>
            <p></p>
        </header>
    </div>
</div>



<article class="row book">
    <div class="small-8 medium-12 large-12 columns booklist_header">
        <h1><a href="design-everyday-things-revised/index.php">The Design of Everyday Things: Revised and Expanded Edition</a></h1>
        <p>
<a href="../people/don-norman/index.php">Don Norman</a>
, 2013</p>
    </div>
    <div class="small-4 medium-3 large-3 columns"><a href="design-everyday-things-revised/index.php"><img class="bookimg" src="https://media.nngroup.com/media/publications/books/2013_design_of_everyday_things.jpg.225x300_q95_autocrop_crop_upscale.jpg"></a></div>
    <div class="show-for-medium-up small-12 medium-8 large-8 columns end">
        <p>Even the smartest among us can feel inept as we fail to figure out which light switch or oven burner to turn on, or whether to push, pull, or slide a door. The fault, argues this ingenious &mdash; even liberating &mdash; book, lies not in ourselves, but in product design that ignores the needs of users and the principles of cognitive psychology. The problems range from ambiguous and hidden controls to arbitrary relationships between controls and functions, coupled with a lack of feedback or other assistance and unreasonable demands on memorization.</p>

        <p><a href="design-everyday-things-revised/index.php">Read more details</a></p>
    </div>
</article>

<article class="row book">
    <div class="small-8 medium-12 large-12 columns booklist_header">
        <h1><a href="mobile-usability/index.php">Mobile Usability</a></h1>
        <p>
<a href="../people/jakob-nielsen/index.php">Jakob Nielsen</a>
            

        
             and 
            
                <a href="../people/raluca-budiu/index.php">Raluca Budiu</a>
, 2012</p>
    </div>
    <div class="small-4 medium-3 large-3 columns"><a href="mobile-usability/index.php"><img class="bookimg" src="https://media.nngroup.com/media/publications/books/mobile-usability-cover-english-small.png.225x300_q95_autocrop_crop_upscale.jpg"></a></div>
    <div class="show-for-medium-up small-12 medium-8 large-8 columns end">
        <p>
	How do we create a satisfactory user experience when limited to a small device? This book focuses on usability for mobile devices and covers such topics as developing a mobile strategy, designing for small screens, writing for mobile, usability comparisons, and looking toward the future. The book includes 228 full-color illustrations, mainly site/app screenshots with analysis of why they work or don&#39;t work for mobile users.</p>

        <p><a href="mobile-usability/index.php">Read more details</a></p>
    </div>
</article>

<article class="row book">
    <div class="small-8 medium-12 large-12 columns booklist_header">
        <h1><a href="living-with-complexity/index.php">Living With Complexity</a></h1>
        <p>
<a href="../people/don-norman/index.php">Don Norman</a>
, 2011</p>
    </div>
    <div class="small-4 medium-3 large-3 columns"><a href="living-with-complexity/index.php"><img class="bookimg" src="https://media.nngroup.com/media/publications/books/living_with_complexity.jpg.225x300_q95_autocrop_crop_upscale.jpg"></a></div>
    <div class="show-for-medium-up small-12 medium-8 large-8 columns end">
        <p>
	If only today&#39;s technology were simpler! It&#39;s the universal lament, but it&#39;s wrong. We don&#39;t want simplicity. Simple tools are not up to the task. The world is complex; our tools need to match that complexity. Simplicity turns out to be more complex than we thought. Don Norman writes that the complexity of our technology must mirror the complexity and richness of our lives. It&#39;s not complexity that&#39;s the problem, it&#39;s bad design. Bad design complicates things unnecessarily and confuses us. Good design can tame complexity.</p>
<p>
	&nbsp;</p>
<h2>
	&nbsp;</h2>

        <p><a href="living-with-complexity/index.php">Read more details</a></p>
    </div>
</article>

<article class="row book">
    <div class="small-8 medium-12 large-12 columns booklist_header">
        <h1><a href="eyetracking-web-usability/index.php">Eyetracking Web Usability</a></h1>
        <p>
<a href="../people/jakob-nielsen/index.php">Jakob Nielsen</a>
            

        
             and 
            
                <a href="../people/kara-pernice/index.php">Kara Pernice</a>
, 2009</p>
    </div>
    <div class="small-4 medium-3 large-3 columns"><a href="eyetracking-web-usability/index.php"><img class="bookimg" src="https://media.nngroup.com/media/publications/books/eyetracking_cover.jpg.225x300_q95_autocrop_crop_upscale.jpg"></a></div>
    <div class="show-for-medium-up small-12 medium-8 large-8 columns end">
        <p>
	Eyetracking Web Usability is based on one of the largest studies of eyetracking usability in existence. We analyzed 1.5 million instances where users looked at websites to understand how the human eyes interact with design. Richly illustrated with compelling eye gaze plots and heat maps. Includes advice for page layout, navigation menus, site elements, image selection, and advertising.</p>

        <p><a href="eyetracking-web-usability/index.php">Read more details</a></p>
    </div>
</article>

<article class="row book">
    <div class="small-8 medium-12 large-12 columns booklist_header">
        <h1><a href="design-of-future-things/index.php">The Design of Future Things</a></h1>
        <p>
<a href="../people/don-norman/index.php">Don Norman</a>
, 2007</p>
    </div>
    <div class="small-4 medium-3 large-3 columns"><a href="design-of-future-things/index.php"><img class="bookimg" src="https://media.nngroup.com/media/publications/books/the_design_of_future_things.jpg.225x300_q95_autocrop_crop_upscale.jpg"></a></div>
    <div class="show-for-medium-up small-12 medium-8 large-8 columns end">
        <p>
	A revealing examination of smart technology, from smooth-talking GPS units to cantankerous refrigerators. This book explores the links between design and human psychology, offering a consumer-oriented theory of natural human-machine interaction that can be put into practice by the engineers and industrial designers of tomorrow&#39;s thinking machines.</p>

        <p><a href="design-of-future-things/index.php">Read more details</a></p>
    </div>
</article>

<article class="row book">
    <div class="small-8 medium-12 large-12 columns booklist_header">
        <h1><a href="prioritizing-web-usability/index.php">Prioritizing Web Usability</a></h1>
        <p>
<a href="../people/hoa-loranger/index.php">Hoa Loranger</a>
            

        
             and 
            
                <a href="../people/jakob-nielsen/index.php">Jakob Nielsen</a>
, 2006</p>
    </div>
    <div class="small-4 medium-3 large-3 columns"><a href="prioritizing-web-usability/index.php"><img class="bookimg" src="https://media.nngroup.com/media/publications/books/prioritizing_web_usability_small.gif.225x300_q95_autocrop_crop_upscale.png"></a></div>
    <div class="show-for-medium-up small-12 medium-8 large-8 columns end">
        <p>After more than a decade of Web usability research, we literally have thousands of guidelines for making better websites. But what are the most important ones that all designers need to know? That&#39;s what <em>Prioritizing Web Usability</em> is about. A second goal of the book is to update the early Web usability guidelines Jakob Nielsen published in the 1990s. The book compares these old studies with more recent ones and explains which of the old guidelines should still be followed.&nbsp;406 pages, heavily illustrated, in full color.&nbsp;</p>

<p>&nbsp;</p>

        <p><a href="prioritizing-web-usability/index.php">Read more details</a></p>
    </div>
</article>

<article class="row book">
    <div class="small-8 medium-12 large-12 columns booklist_header">
        <h1><a href="emotional-design/index.php">Emotional Design: Why We Love (Or Hate) Everyday Things</a></h1>
        <p>
<a href="../people/don-norman/index.php">Don Norman</a>
, 2004</p>
    </div>
    <div class="small-4 medium-3 large-3 columns"><a href="emotional-design/index.php"><img class="bookimg" src="https://media.nngroup.com/media/publications/books/emotional_design.jpg.225x300_q95_autocrop_crop_upscale.jpg"></a></div>
    <div class="show-for-medium-up small-12 medium-8 large-8 columns end">
        <p>Did you ever wonder why cheap wine tastes better in fancy glasses? Why sales of Macintosh computers soared when Apple introduced the colorful iMac? New research on emotion and cognition has shown that attractive things really do work better, as Donald Norman amply demonstrates in this fascinating book, which has garnered acclaim everywhere from Scientific American to The New Yorker.Emotional Design articulates the profound influence of the feelings that objects evoke, from our willingness to spend thousands of dollars on Gucci bags and Rolex watches, to the impact of emotion on the everyday objects of tomorrow.Norman draws on a wealth of examples and the latest scientific insights to present a bold exploration of the objects in our everyday world. Emotional Design will appeal not only to designers and manufacturers but also to managers, psychologists, and general readers who love to think about their stuff.</p>

        <p><a href="emotional-design/index.php">Read more details</a></p>
    </div>
</article>

<article class="row book">
    <div class="small-8 medium-12 large-12 columns booklist_header">
        <h1><a href="the-design-of-everyday-things/index.php">The Design of Everyday Things</a></h1>
        <p>
<a href="../people/don-norman/index.php">Don Norman</a>
, 2002</p>
    </div>
    <div class="small-4 medium-3 large-3 columns"><a href="the-design-of-everyday-things/index.php"><img class="bookimg" src="https://media.nngroup.com/media/publications/books/the_design_of_everyday_things.jpg.225x300_q95_autocrop_crop_upscale.jpg"></a></div>
    <div class="show-for-medium-up small-12 medium-8 large-8 columns end">
        <p>Even the smartest among us can feel inept as we fail to figure out which light switch or oven burner to turn on, or whether to push, pull, or slide a door. The fault lies in product design that ignore the needs of users and the principles of cognitive psychology. A bestseller in the United States, this bible on the cognitive aspects of design contains examples of both good and bad design and simple rules that designers can use to improve the usability of objects as diverse as cars, computers, doors, and telephones.</p>

        <p><a href="the-design-of-everyday-things/index.php">Read more details</a></p>
    </div>
</article>

<article class="row book">
    <div class="small-8 medium-12 large-12 columns booklist_header">
        <h1><a href="homepage-usability/index.php">Homepage Usability: 50 Websites Deconstructed</a></h1>
        <p>
<a href="../people/jakob-nielsen/index.php">Jakob Nielsen</a>
            

        
             and 
            
                Marie Tahir
, 2001</p>
    </div>
    <div class="small-4 medium-3 large-3 columns"><a href="homepage-usability/index.php"><img class="bookimg" src="https://media.nngroup.com/media/publications/books/homepage.jpg.225x300_q95_autocrop_crop_upscale.jpg"></a></div>
    <div class="show-for-medium-up small-12 medium-8 large-8 columns end">
        <p>Homepages are the most valuable real estate in the world. Millions of dollars are funneled through a space that&#39;s not even a square foot in size. The homepage is also your company&#39;s face to the world. Potential customers look at your company&#39;s online presence before doing any business with you. Complexity or confusion make people go away. That&#39;s why homepage usability is so important and that&#39;s why we wrote a book specifically about this one topic.</p>

        <p><a href="homepage-usability/index.php">Read more details</a></p>
    </div>
</article>

<article class="row book">
    <div class="small-8 medium-12 large-12 columns booklist_header">
        <h1><a href="designing-web-usability/index.php">Designing Web Usability: The Practice of Simplicity</a></h1>
        <p>
<a href="../people/jakob-nielsen/index.php">Jakob Nielsen</a>
, 1999</p>
    </div>
    <div class="small-4 medium-3 large-3 columns"><a href="designing-web-usability/index.php"><img class="bookimg" src="https://media.nngroup.com/media/publications/books/designing.gif.225x300_q95_autocrop_crop_upscale.jpg"></a></div>
    <div class="show-for-medium-up small-12 medium-8 large-8 columns end">
        <p>
	Over 250,000 Internet professionals around the world have turned to this landmark, definitive guide to usability. From content and page design to designing for ease of navigation and users with disabilities, Designing Web Usability delivers complete direction on how to connect with any web user, in any situation. 432 pages, full color illustrations.</p>

        <p><a href="designing-web-usability/index.php">Read more details</a></p>
    </div>
</article>

<article class="row book">
    <div class="small-8 medium-12 large-12 columns booklist_header">
        <h1><a href="invisible-computer/index.php">The Invisible Computer</a></h1>
        <p>
<a href="../people/don-norman/index.php">Don Norman</a>
, 1998</p>
    </div>
    <div class="small-4 medium-3 large-3 columns"><a href="invisible-computer/index.php"><img class="bookimg" src="https://media.nngroup.com/media/publications/books/the_invisible_computer_transparent.jpg.225x300_q95_autocrop_crop_upscale.jpg"></a></div>
    <div class="show-for-medium-up small-12 medium-8 large-8 columns end">
        <p>
	The technology should be invisible, hidden from sight.In this book, Norman shows why the computer is so difficult to use and why this complexity is fundamental to its nature. The only answer, says Norman, is to start over again, to develop information appliances that fit people&#39;s needs and lives. To do this companies must change the way they develop products. They need to start with an understanding of people: user needs first, technology last--the opposite of how things are done now. This book shows how.</p>

        <p><a href="invisible-computer/index.php">Read more details</a></p>
    </div>
</article>

<article class="row book">
    <div class="small-8 medium-12 large-12 columns booklist_header">
        <h1><a href="international-user-interfaces/index.php">International User Interfaces </a></h1>
        <p>
<a href="../people/jakob-nielsen/index.php">Jakob Nielsen</a>
, 1996</p>
    </div>
    <div class="small-4 medium-3 large-3 columns"><a href="international-user-interfaces/index.php"><img class="bookimg" src="https://media.nngroup.com/media/publications/books/intluicover_resize.png.225x300_q95_autocrop_crop_upscale.png"></a></div>
    <div class="show-for-medium-up small-12 medium-8 large-8 columns end">
        <p>
	Book about the design of user interfaces to be used outside the country in which they were designed. Emphasis on usability methods and cultural differences as well as GUI and documentation design.</p>

        <p><a href="international-user-interfaces/index.php">Read more details</a></p>
    </div>
</article>

<article class="row book">
    <div class="small-8 medium-12 large-12 columns booklist_header">
        <h1><a href="tog-on-software-design/index.php">Tog on Software Design</a></h1>
        <p>
<a href="../people/bruce-tognazzini/index.php">Bruce Tognazzini</a>
, 1995</p>
    </div>
    <div class="small-4 medium-3 large-3 columns"><a href="tog-on-software-design/index.php"><img class="bookimg" src="https://media.nngroup.com/media/publications/books/tog_on_software.png.225x300_q95_autocrop_crop_upscale.jpg"></a></div>
    <div class="show-for-medium-up small-12 medium-8 large-8 columns end">
        <p>
	The computer industry is poised for its second great revolution, and within ten years, society will be in the midst of an equally great transformation. In Tog on Software Design, Bruce &quot;Tog&quot; Tognazzini, respected industry futurist, presents his vision of our technological future, detailing the steps computer professionals need to take now to deliver powerful new technologies in a form that will profit the industry and benefit society in general.</p>

        <p><a href="tog-on-software-design/index.php">Read more details</a></p>
    </div>
</article>

<article class="row book">
    <div class="small-8 medium-12 large-12 columns booklist_header">
        <h1><a href="advances-in-human-computer-interaction/index.php">Advances in Human/Computer Interaction, Volume 5</a></h1>
        <p>
<a href="../people/jakob-nielsen/index.php">Jakob Nielsen</a>
, 1995</p>
    </div>
    <div class="small-4 medium-3 large-3 columns"><a href="advances-in-human-computer-interaction/index.php"><img class="bookimg" src="https://media.nngroup.com/media/publications/books/ahci_cover_small.jpg.225x300_q95_autocrop_crop_upscale.jpg"></a></div>
    <div class="show-for-medium-up small-12 medium-8 large-8 columns end">
        <p>
	This volume includes essays by specialists in the field of HCI and addresses information overload, provides an overview of recent advances, and surveys interesting specific design or development projects.&nbsp;Edited by Jakob Nielsen,</p>

        <p><a href="advances-in-human-computer-interaction/index.php">Read more details</a></p>
    </div>
</article>

<article class="row book">
    <div class="small-8 medium-12 large-12 columns booklist_header">
        <h1><a href="multimedia-and-hypertext/index.php">Multimedia and Hypertext: The Internet and Beyond</a></h1>
        <p>
<a href="../people/jakob-nielsen/index.php">Jakob Nielsen</a>
, 1995</p>
    </div>
    <div class="small-4 medium-3 large-3 columns"><a href="multimedia-and-hypertext/index.php"><img class="bookimg" src="https://media.nngroup.com/media/publications/books/multimedia-hypertext-cover.png.225x300_q95_autocrop_crop_upscale.png"></a></div>
    <div class="show-for-medium-up small-12 medium-8 large-8 columns end">
        <p>
	The 2nd edition of the best-selling <cite>HyperText and HyperMedia</cite>, this book takes hypertext a step further&mdash;to the Internet.&nbsp;Based on a sound conceptual foundation in hypertext theory, the book includes&nbsp;richly illustrated examples of a wide variety of hypermedia systems, a range of strategies for overcoming information overload, and usability issues for hypertext.</p>

        <p><a href="multimedia-and-hypertext/index.php">Read more details</a></p>
    </div>
</article>

<article class="row book">
    <div class="small-8 medium-12 large-12 columns booklist_header">
        <h1><a href="usability-inspection-methods/index.php">Usability Inspection Methods</a></h1>
        <p>
<a href="../people/jakob-nielsen/index.php">Jakob Nielsen</a>
, 1994</p>
    </div>
    <div class="small-4 medium-3 large-3 columns"><a href="usability-inspection-methods/index.php"><img class="bookimg" src="https://media.nngroup.com/media/publications/books/usability-inspection-methods-resized.png.225x300_q95_autocrop_crop_upscale.png"></a></div>
    <div class="show-for-medium-up small-12 medium-8 large-8 columns end">
        <p>
	The first comprehensive, book-length work in the field of usability inspection methods. With the help of numerous real-life case studies, the authors give you: Step-by-step guidance on all important methods now in use, including the heuristic evaluation method, the pluralistic walkthrough method, the cognitive walkthrough method, and more.&nbsp;</p>

        <p><a href="usability-inspection-methods/index.php">Read more details</a></p>
    </div>
</article>

<article class="row book">
    <div class="small-8 medium-12 large-12 columns booklist_header">
        <h1><a href="things-that-make-us-smart/index.php">Things That Make us Smart: Defending Human Attributes in the Age of the Machine</a></h1>
        <p>
<a href="../people/don-norman/index.php">Don Norman</a>
, 1994</p>
    </div>
    <div class="small-4 medium-3 large-3 columns"><a href="things-that-make-us-smart/index.php"><img class="bookimg" src="https://media.nngroup.com/media/publications/books/things_that_make_us_smart_resized.png.225x300_q95_autocrop_crop_upscale.png"></a></div>
    <div class="show-for-medium-up small-12 medium-8 large-8 columns end">
        <p>
	Explore the complex interaction between human thought and the technology it creates. Humans have always worked with objects to extend our cognitive powers, from counting on our fingers to designing massive supercomputers. But advanced technology does more than merely assist with thought and memory&mdash;the machines we create begin to shape how we think and, at times, even what we value. Norman, in exploring this complex relationship between humans and machines, gives us the first steps towards demanding a person-centered redesign of the machines that surround our lives.</p>

        <p><a href="things-that-make-us-smart/index.php">Read more details</a></p>
    </div>
</article>

<article class="row book">
    <div class="small-8 medium-12 large-12 columns booklist_header">
        <h1><a href="usability-engineering/index.php">Usability Engineering</a></h1>
        <p>
<a href="../people/jakob-nielsen/index.php">Jakob Nielsen</a>
, 1993</p>
    </div>
    <div class="small-4 medium-3 large-3 columns"><a href="usability-engineering/index.php"><img class="bookimg" src="https://media.nngroup.com/media/publications/books/useengcover_resized.png.225x300_q95_autocrop_crop_upscale.png"></a></div>
    <div class="show-for-medium-up small-12 medium-8 large-8 columns end">
        <p>
	Detailing the methods of usability engineering, this book provides step-by-step information on which method to use at various stages during the development lifecycle, along with detailed information on how to run a usability test and the unique issues relating to international usability. This book emphasizes cost-effective methods that developers can implement immediately, and instructs readers about which methods to use when.</p>

        <p><a href="usability-engineering/index.php">Read more details</a></p>
    </div>
</article>

<article class="row book">
    <div class="small-8 medium-12 large-12 columns booklist_header">
        <h1><a href="turn-signals-are-the-facial-expressions-of-auto/index.php">Turn Signals Are the Facial Expressions of Automobiles</a></h1>
        <p>
<a href="../people/don-norman/index.php">Don Norman</a>
, 1993</p>
    </div>
    <div class="small-4 medium-3 large-3 columns"><a href="turn-signals-are-the-facial-expressions-of-auto/index.php"><img class="bookimg" src="https://media.nngroup.com/media/publications/books/turn_signals_resized2.jpg.225x300_q95_autocrop_crop_upscale.jpg"></a></div>
    <div class="show-for-medium-up small-12 medium-8 large-8 columns end">
        <p>
	From water faucets and airplane cockpits to the concept of &quot;real time&quot; and the future of memory, this wide-ranging tour through technology provides a new understanding of how the gadgets that surround us affect our lives. Donald Norman explores the plight of humans living in a world ruled by a technology that seems to exist for its own sake, oblivious to the needs of the people who create it. &quot;Turn Signals &quot;is an intelligent, whimsical, curmudgeonly look at our love/hate relationship with machines, as well as a persuasive call for the humanization of modern design.</p>

        <p><a href="turn-signals-are-the-facial-expressions-of-auto/index.php">Read more details</a></p>
    </div>
</article>

<article class="row book">
    <div class="small-8 medium-12 large-12 columns booklist_header">
        <h1><a href="tog-on-interface/index.php">Tog on Interface</a></h1>
        <p>
<a href="../people/bruce-tognazzini/index.php">Bruce Tognazzini</a>
, 1992</p>
    </div>
    <div class="small-4 medium-3 large-3 columns"><a href="tog-on-interface/index.php"><img class="bookimg" src="https://media.nngroup.com/media/publications/books/tog_on_interface_cover.png.225x300_q95_autocrop_crop_upscale.jpg"></a></div>
    <div class="show-for-medium-up small-12 medium-8 large-8 columns end">
        <p>
	From one of the foremost authorities on the design of user interfaces, this unique collection of ideas and opinions, while focusing on the Macintosh, neatly captures the underlying principles of all graphical user interfaces. Using ideas from such diverse sources as Information Theory, Carl Jung, and even professional beekeeping, the book provides a framework for achieving a deep understanding of user interface design.<br />
	With humor and thought-provoking insights, Bruce Tognazzini explores the central issues of human-computer interaction, including the challenges presented by multimedia applications, agents, virtual reality, and future technologies. Drawn from his long experience of working with developers, the book provides practical guidelines for developing successful applications that users will find simple, clear, and consistent.&nbsp;&quot;Tog on Interface&quot; is fascinating reading for all those concerned with the relationship between people and computers.</p>

        <p><a href="tog-on-interface/index.php">Read more details</a></p>
    </div>
</article>

<article class="row book">
    <div class="small-8 medium-12 large-12 columns booklist_header">
        <h1><a href="designing-user-interfaces-international-use/index.php">Designing User Interfaces for International Use</a></h1>
        <p>
<a href="../people/jakob-nielsen/index.php">Jakob Nielsen</a>
, 1990</p>
    </div>
    <div class="small-4 medium-3 large-3 columns"><a href="designing-user-interfaces-international-use/index.php"><img class="bookimg" src="https://media.nngroup.com/media/publications/books/designing-intl-use.jpg.225x300_q95_autocrop_crop_upscale.jpg"></a></div>
    <div class="show-for-medium-up small-12 medium-8 large-8 columns end">
        <p>
	Software sales and website use increasingly depend on international usability and not just their domestic usability. Seen from a user&#39;s perspective more than half of the world&#39;s users use interfaces which were originally designed in a foreign country. Usability for this large market of users will depend upon increased awareness of the issues involved in designing user interfaces for international use. As if it wasn&#39;t hard enough to design user interfaces for use across Europe, there are a further set of problems connected with user interfaces for Asia. Both of these issues are examined in depth.</p>

        <p><a href="designing-user-interfaces-international-use/index.php">Read more details</a></p>
    </div>
</article>

<article class="row book">
    <div class="small-8 medium-12 large-12 columns booklist_header">
        <h1><a href="hypertext-and-hypermedia/index.php">Hypertext and Hypermedia</a></h1>
        <p>
<a href="../people/jakob-nielsen/index.php">Jakob Nielsen</a>
, 1990</p>
    </div>
    <div class="small-4 medium-3 large-3 columns"><a href="hypertext-and-hypermedia/index.php"><img class="bookimg" src="https://media.nngroup.com/media/publications/books/hthmcover_small.jpg.225x300_q95_autocrop_crop_upscale.jpg"></a></div>
    <div class="show-for-medium-up small-12 medium-8 large-8 columns end">
        <p>
	Jakob Nielsen&#39;s original 1990 book about hypertext, republished in 1995 as &quot;Multimedia and Hypertext: The Internet and Beyond.&quot;</p>

        <p><a href="hypertext-and-hypermedia/index.php">Read more details</a></p>
    </div>
</article>

<article class="row book">
    <div class="small-8 medium-12 large-12 columns booklist_header">
        <h1><a href="coordinating-user-interfaces-for-consistency/index.php">Coordinating User Interfaces for Consistency</a></h1>
        <p>
<a href="../people/jakob-nielsen/index.php">Jakob Nielsen</a>
, 1989</p>
    </div>
    <div class="small-4 medium-3 large-3 columns"><a href="coordinating-user-interfaces-for-consistency/index.php"><img class="bookimg" src="https://media.nngroup.com/media/publications/books/constcover_new_resized.png.225x300_q95_autocrop_crop_upscale.png"></a></div>
    <div class="show-for-medium-up small-12 medium-8 large-8 columns end">
        <p>The core principles of interface consistency are as persistent and as valuable as when this book was first published. Contributed chapters include details on many methods for seeking and enforcing consistency, along with bottom-line analyses of its benefits and some warnings about its possible dangers. Most of what you&#39;ll learn applies equally to hardware and software development, and all of it holds real benefits for both your organization and your users.</p>

<p>&nbsp;</p>

        <p><a href="coordinating-user-interfaces-for-consistency/index.php">Read more details</a></p>
    </div>
</article>

<article class="row book">
    <div class="small-8 medium-12 large-12 columns booklist_header">
        <h1><a href="user-centered-system-design/index.php">User Centered System Design</a></h1>
        <p>
<a href="../people/don-norman/index.php">Don Norman</a>
, 1986</p>
    </div>
    <div class="small-4 medium-3 large-3 columns"><a href="user-centered-system-design/index.php"><img class="bookimg" src="https://media.nngroup.com/media/publications/books/user_centered_system_design_resized.png.225x300_q95_autocrop_crop_upscale.png"></a></div>
    <div class="show-for-medium-up small-12 medium-8 large-8 columns end">
        <p>
	This comprehensive volume is the product of an intensive collaborative effort among researchers across the United States, Europe and Japan. The result -- a change in the way we think of humans and computers. Edited by Don Norman and Stephen W. Draper.</p>

        <p><a href="user-centered-system-design/index.php">Read more details</a></p>
    </div>
</article>

<article class="row book">
    <div class="small-8 medium-12 large-12 columns booklist_header">
        <h1><a href="learning-and-memory/index.php">Learning and Memory: A Primer</a></h1>
        <p>
<a href="../people/don-norman/index.php">Don Norman</a>
, 1982</p>
    </div>
    <div class="small-4 medium-3 large-3 columns"><a href="learning-and-memory/index.php"><img class="bookimg" src="https://media.nngroup.com/media/publications/books/learning_and_memory.png.225x300_q95_autocrop_crop_upscale.png"></a></div>
    <div class="show-for-medium-up small-12 medium-8 large-8 columns end">
        <p>
	Don Norman&#39;s 1982 book explores human cognitive processes.&nbsp;</p>

        <p><a href="learning-and-memory/index.php">Read more details</a></p>
    </div>
</article>

<article class="row book">
    <div class="small-8 medium-12 large-12 columns booklist_header">
        <h1><a href="perspectives-on-cognitive-science/index.php">Perspectives on Cognitive Science</a></h1>
        <p>
<a href="../people/don-norman/index.php">Don Norman</a>
, 1981</p>
    </div>
    <div class="small-4 medium-3 large-3 columns"><a href="perspectives-on-cognitive-science/index.php"><img class="bookimg" src="#"></a></div>
    <div class="show-for-medium-up small-12 medium-8 large-8 columns end">
        <p>
	This edition of a series on cognitive science includes explorations of mental models, language and memory, physical symbol systems, and more. Edited by Don Norman.&nbsp;</p>

        <p><a href="perspectives-on-cognitive-science/index.php">Read more details</a></p>
    </div>
</article>

<article class="row book">
    <div class="small-8 medium-12 large-12 columns booklist_header">
        <h1><a href="explorations-in-cognition/index.php">Explorations in Cognition</a></h1>
        <p>
<a href="../people/don-norman/index.php">Don Norman</a>
, 1975</p>
    </div>
    <div class="small-4 medium-3 large-3 columns"><a href="explorations-in-cognition/index.php"><img class="bookimg" src="#"></a></div>
    <div class="show-for-medium-up small-12 medium-8 large-8 columns end">
        <p>
	This edition of a series of books in psychology includes discussions of linguistic analysis; the computer model; studies of language; and studies of visual perception and problem solving. By Donald A. Norman and David E. Rumelhart.</p>

        <p><a href="explorations-in-cognition/index.php">Read more details</a></p>
    </div>
</article>

<article class="row book">
    <div class="small-8 medium-12 large-12 columns booklist_header">
        <h1><a href="human-information-processing-introduction-psycholo/index.php">Human Information Processing: Introduction to Psychology</a></h1>
        <p>
<a href="../people/don-norman/index.php">Don Norman</a>
, 1972</p>
    </div>
    <div class="small-4 medium-3 large-3 columns"><a href="human-information-processing-introduction-psycholo/index.php"><img class="bookimg" src="https://media.nngroup.com/media/publications/books/human_info_processing_norman.jpg.225x300_q95_autocrop_crop_upscale.jpg"></a></div>
    <div class="show-for-medium-up small-12 medium-8 large-8 columns end">
        <p>
	This classic book explores basic principles of human psychology. By Peter H. Lindsay and Don Norman.&nbsp;</p>

        <p><a href="human-information-processing-introduction-psycholo/index.php">Read more details</a></p>
    </div>
</article>

<article class="row book">
    <div class="small-8 medium-12 large-12 columns booklist_header">
        <h1><a href="models-of-human-memory/index.php">Models of Human Memory</a></h1>
        <p>
<a href="../people/don-norman/index.php">Don Norman</a>
, 1970</p>
    </div>
    <div class="small-4 medium-3 large-3 columns"><a href="models-of-human-memory/index.php"><img class="bookimg" src="#"></a></div>
    <div class="show-for-medium-up small-12 medium-8 large-8 columns end">
        <p>
	Edited by By Donald A. Norman; contributors include Harley A. Bernbach.</p>

        <p><a href="models-of-human-memory/index.php">Read more details</a></p>
    </div>
</article>

<article class="row book">
    <div class="small-8 medium-12 large-12 columns booklist_header">
        <h1><a href="memory-and-attention/index.php">Memory and Attention: An Introduction to Human Information Processing</a></h1>
        <p>
<a href="../people/don-norman/index.php">Don Norman</a>
, 1969</p>
    </div>
    <div class="small-4 medium-3 large-3 columns"><a href="memory-and-attention/index.php"><img class="bookimg" src="https://media.nngroup.com/media/publications/books/memory_and_attention.jpg.225x300_q95_autocrop_crop_upscale.jpg"></a></div>
    <div class="show-for-medium-up small-12 medium-8 large-8 columns end">
        <p>
	Don Norman&#39;s 1969 book about human memory processes; translated into 5 languages.&nbsp;</p>

        <p><a href="memory-and-attention/index.php">Read more details</a></p>
    </div>
</article>


    </div>
    




<footer id="page-footer">
    <div class="row hide-for-print">
        <div class="column small-12 medium-6">
            <section class="footer-popular">
                
                <h1>Popular Topics</h1>
                <div style="float: left;  width: 15em; display: block;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../topic/applications/index.php">Application Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../topic/e-commerce/index.php">E-Commerce Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../topic/eyetracking/index.php">Eyetracking </a></li>
	<li style="padding-bottom: .5em;"><a href="../topic/human-computer-interaction/index.php">Human Computer Interaction </a></li>
	<li style="padding-bottom: .5em;"><a href="../topic/information-architecture/index.php">Information Architecture </a></li>
	<li style="padding-bottom: .5em;"><a href="../topic/intranets/index.php">Intranet Design </a></li>
	<li><a href="../topic/interaction-design/index.php">Interaction Design </a></li>
</ul>
</div>

<div style="float: left;  width: 15em; display: block; list-style: none;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../topic/mobile-and-tablet-design/index.php">Mobile and Tablet Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../topic/credibility-persuasion/index.php">Persuasive Design</a></li>
	<li style="padding-bottom: .5em;"><a href="../topic/research-methods/index.php">Research Methods</a></li>
	<li style="padding-bottom: .5em;"><a href="../topic/user-testing/index.php">User Testing </a></li>
	<li style="padding-bottom: .5em;"><a href="../topic/web-usability/index.php">Web Usability </a></li>
	<li style="padding-bottom: .5em;"><a href="../topic/writing-web/index.php">Web Writing &amp; Content Strategy </a></li>
</ul>
</div>

<p>&nbsp;</p>

                
            </section>
        </div>

        <div class="column small-12 medium-6">
            <div class="row">
                <div class="column small-6">
                    <section class="footer-about">
                        <h1>Follow Us</h1>
                        <ul>
	<li><a href="../articles/subscribe/index.php">Subscribe to our weekly UX newsletter</a>,&nbsp;Jakob Nielsen&#39;s Alertbox</li>
	<li>Visit Don Norman&#39;s website, <a href="http://www.jnd.org/"> www.jnd.org </a></li>
	<li>Visit Bruce Tognazzini&#39;s website, <a href="http://www.asktog.com/"> www.asktog.com </a></li>
	<li style="padding-top: 6px;"><a class="twitter-follow-button" data-show-count="false" data-size="large" href="https://twitter.com/nngroup"><img alt="Follow @nngroup on Twitter" height="20" src="https://media.nngroup.com/media/editor/2016/12/30/twitter-follow-button.png" style="width: 117px; height: 20px;" width="117" /></a></li>
	<li style="padding-top: 6px;"><img alt="" height="14" src="https://media.nngroup.com/media/editor/2016/07/26/feed-icon-14x14.png" style="vertical-align:text-top;width:14px;margin-right:.25em;" width="14" /><a href="../feed/rss/index.php">Subscribe to our RSS feed</a></li>
</ul>

                    </section>
                </div>

                <div class="column small-6">
                    <section class="footer-about right-column">
                        <h1>About
</h1>
                        <ul class="no-bullet">
	<li><a href="../about/contact/index.php">Contact information</a></li>
	<li><a href="../about/index.php">About Us</a></li>
	<li><a href="../about/why-nng/index.php">Why NN/g? </a></li>
	<li><a href="../return-policy/index.php">Return Policy</a></li>
</ul>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="copyright column small-12">
            <p><a href="../copyright-and-reprint-info/index.php">Copyright</a> &copy; 1998-2017 Nielsen Norman Group, All Rights Reserved.</p>
        </div>
    </div>
</footer>


    


        <script src="https://media.nngroup.com/static/vendor/fastclick/js/fastclick.min.js"></script>
        <script src="https://media.nngroup.com/static/vendor/foundation/js/foundation.min.js"></script>
        <script src="http://media.nngroup.com/static/js/app.js"></script>
        <script src="http://media.nngroup.com/static/js/scroll-to-fixed.js"></script><script>var externalLinks = document.querySelectorAll('a[href^="http"]');for (var i = externalLinks.length-1; i >= 0; i--) {externalLinks[i].addEventListener("click", function(event) { alert("External links are disabled for this test."); event.preventDefault(); }, false);}var mailLinks = document.querySelectorAll('a[href^="mailto"]');for (var i = mailLinks.length-1; i >= 0; i--) {mailLinks[i].addEventListener("click", function(event) { alert("Email links are disabled for this test."); event.preventDefault(); }, false);}var externalForms = document.querySelectorAll('form');for (var i = externalForms.length-1; i >= 0; i--) {externalForms[i].addEventListener("submit", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false); externalForms[i].addEventListener("click", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false);}</script>

        <script>
            
            $(function() {
                $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
                $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
            });
        </script>

        


        
        <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1072699960;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072699960/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
    </body>

<!-- Mirrored from www.nngroup.com/books/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 14:54:45 GMT -->
</html>
