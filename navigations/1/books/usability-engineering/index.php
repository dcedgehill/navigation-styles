<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->





    
<!-- Mirrored from www.nngroup.com/books/usability-engineering/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:02:45 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1026.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"licenseKey":"1a30ef9931","errorBeacon":"bam.nr-data.net","applicationID":"2048992","transactionName":"NFxSMRBUX0EDUkIPDA0WdhAMVkVbDV8ZBAwMUkNLFFxURRELdAkMCH1VEQNcXRwFVEI=","beacon":"bam.nr-data.net","queueTime":2,"applicationTime":305,"agent":""}</script>
        <title>Usability Engineering : Book by Jakob Nielsen</title><meta property="og:title" content="Usability Engineering : Book by Jakob Nielsen" />
  
        
        <meta name="description" content="Jakob Nielsen&#39;s textbook on applying systematic methods throughout the development lifecycle to increase ease-of-use for software, websites, and other user interfaces. Emphasis on cheap 
and fast methods.">
        <meta property="og:description" content="Jakob Nielsen&#39;s textbook on applying systematic methods throughout the development lifecycle to increase ease-of-use for software, websites, and other user interfaces. Emphasis on cheap 
and fast methods." />
        
  
        
	
        

        

        <meta name="robots" content="noarchive">
        <meta name="viewport" content="width=device-width">

        <link rel="search" type="application/opensearchdescription+xml" title="NN/g Search" href="../../opensearch.xml" />
        <link rel="shortcut icon" href="http://media.nngroup.com/static/img/favicon.ico" />

        <link rel="stylesheet" href="https://media.nngroup.com/static/vendor/font-awesome/css/font-awesome.min.css">

        
        <link rel="stylesheet" href="https://media.nngroup.com/static/css/7232a960f1df.css" type="text/css" />
        <link rel="stylesheet" href="http://fast.fonts.com/t/1.css?apiType=css&amp;projectid=c5846260-9def-46ec-9407-0f1349bd2d64" type="text/css">

        <script src="https://media.nngroup.com/static/vendor/modernizr/js/modernizr.min.js"></script>

        <script type="text/javascript" src="https://media.nngroup.com/static/js/3283be5def74.js"></script>
        

        

        
        
        <!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=229034,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('http://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->


<!-- Google Analytics Tracking Code for http://nngroup.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23132681-1', 'nngroup.com');
  ga('require', 'linkid', 'https://www.nngroup.com/books/usability-engineering/linkid.js');
  ga('require', 'displayfeatures');

// Optimizely Universal Analytics Integration
 window.optimizely = window.optimizely || [];
 window.optimizely.push("activateUniversalAnalytics");

  ga('send', 'pageview');

</script>
<script>
/**
* Function to track a click on an outbound link in Google Analytics.
*/
function trackOutboundLink(label, url) {
    if(window.ga && ga.loaded) {
        ga('send', 'event', 'outbound', 'click', label, {'hitCallback': function(){document.location = url;}
        });
    } else {
        document.location = url;
    }
}
/**
* Function to track a click on a link in the article announcement area.
*/
$('#articleAnnouncement a').on('click', function(){
  ga('send', 'event', 'link', 'click', 'articleAnnouncement');
});
</script>

<script type="text/javascript">
 setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2421.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



<!-- Hotjar Tracking Code for http://nngroup.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:121605,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

        <script src="https://media.nngroup.com/static/js/analytics/track_outbound_link.js"></script>
        
        
        
    </head>
    <body class="location-about location-books book-detail">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
    <header id="page-header">
        <div id="cart" class="shopping-cart mobile-cart show-for-small-down"></div>
        <div class="row">
            <div id="masthead" class="masthead small-12 medium-7 column">

                <!-- The button to toggles menu on small screen -->

                <h1><a href="../../index.php"><img src="http://media.nngroup.com/static/img/logo-wordmark.png" width="74" height="30"> Nielsen <span class="norman">Norman</span> Group</a></h1>
                <p class="tagline show-for-medium-up">Evidence-Based User Experience Research, Training, and Consulting</p>

                <!-- The button to toggles form search on small screen -->


                <div style="clear: right;width:  100%;height: 1px;"></div><?php define('ROOT_PATH',$_SERVER["DOCUMENT_ROOT"] . '/apps/navigation_styles/navigations/1'); ?><?php include(ROOT_PATH)."/navigation.php"; ?>

                <!-- Drop down menu on small screen -->
                <div id="hideNav" class="show-for-small-down">
                  
                  <ul class="menu-content">
                    <li><a href="../../index.php">Home</a></li>
                    <li class="has-submenu">
                      <a>Training</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../training/index.php">Overview</a></li>
                        
<li id="nav-ux-conference"><a href="../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../online-seminars/index.php"><span>Online Seminars</span></a></li>

                      </ul>
                    </li>
                    <li class="has-submenu">
                      <a>Consulting</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
  <li><a href="../../consulting/index.php">Overview</a></li>
  
  <li><a href="../../consulting/benchmarking/index.php">Benchmarking</a></li>
  
  <li><a href="../../consulting/expert-design-review/index.php">Expert Design Review</a></li>
  
  <li><a href="../../consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
  
  <li><a href="../../consulting/usability-testing/index.php">Usability Testing </a></li>
  
  <li><a href="../../consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
  
  <li><a href="../../in-house-training/index.php">In-House Training</a></li>
</ul>

                    </li>

                    <li><a href="../../reports/index.php">Reports</a></li>
                    <li><a href="../../articles/index.php">Articles</a></li>
                    <li class="has-submenu"><a>About NN/G</a><i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../about/index.php">Overview</a></li>
                        <li><a href="../../people/index.php">People</a></li>
                        <li><a href="../../about/why-nng/index.php">Why NN/g?</a></li>
                        <li><a href="../../about/contact/index.php">Contact</a></li>
                        <li><a href="../../news/index.php">News</a></li>
                        <li><a href="../../about/history/index.php">History</a></li>
                        <li><a href="../index.php">Books</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                </div>

                <hr class="header-divider show-for-small-only"/>


            </div>
       </div>

        <div id="hideNav">            <nav class="primary-nav small-12 column">
                <ul>
                    <li><a class="navitem" id="nav-home" href="../../index.php">Home</a></li>
                    <li><a class="navitem" id="nav-training" href="../../training/index.php">Training</a></li>
                    <li><a class="navitem" id="nav-consulting" href="../../consulting/index.php">Consulting</a></li>
                    <li><a class="navitem" id="nav-reports" href="../../reports/index.php">Reports</a></li>
                    <li><a class="navitem" id="nav-articles" href="../../articles/index.php">Articles</a></li>
                    <li><a class="navitem" id="nav-about" href="../../about/index.php">About NN/G</a></li>
                </ul>
            </nav>
            <nav class="secondary-nav small-12 medium-12 large-12 columns">
                
<ul class="inline-list">
    <li id="nav-people"><a href="../../people/index.php"><span>People</span></a></li>
    <li id="nav-whynng"><a href="../../about/why-nng/index.php"><span>Why NN/g?</span></a></li>
    <li id="nav-contact"><a href="../../about/contact/index.php"><span>Contact</span></a></li>
    <li id="nav-news"><a href="../../news/index.php"><span>News</span></a></li>
    <li id="nav-history"><a href="../../about/history/index.php"><span>History</span></a></li>
    <li id="nav-books"><a href="../index.php"><span>Books</span></a></li>
</ul>

            </nav>
        </div>
    </header>
    <div class="l-content">
    
<div class="row">
  <div class="medium-2 columns hide-for-small-down">
    <img class="book-cover" src="https://media.nngroup.com/media/publications/books/useengcover_resized.png.300x400_q95_autocrop_crop_upscale.png">
  </div>
  <div class="small-12 medium-6 columns l-subsection">
    <h1>Usability Engineering</h1>

    <img class="show-for-small-down book-cover" src="https://media.nngroup.com/media/publications/books/useengcover_resized.png.300x400_q95_autocrop_crop_upscale.png">
    <h2>
<a href="../../people/jakob-nielsen/index.php">Jakob Nielsen</a>
, 1993</h2>

    

    <p>
	Detailing the methods of usability engineering, this book provides the tools needed to avoid usability surprises and improve product quality. Step-by-step information on which method to use at various stages during the development lifecycle are included, along with detailed information on how to run a usability test and the unique issues relating to international usability. This book emphasizes cost-effective methods that developers can implement immediately, and instructs readers about which methods to use when, throughout the development lifecycle. Also includes strategies to avoid the four most frequently listed reasons for delay in software projects, detailed information on how to run a usability test, and an extensive bibliography allowing readers to find additional information.&nbsp;(Published by Morgan Kaufmann, San Francisco;&nbsp;ISBN 0-12-518406-9 slightly expanded paperback edition. Original hardcover edition published by AP Professional.)</p>
<p>
	Please buy through the purchase links provided here: Amazon pays me a referral fee that doubles the share of the purchase price that goes to the author, giving me time off from other projects to write new books.</p>
<p>
	<a href="http://www.newscientist.com/article/mg13918874.500-review-the-frustrated-user-always-knows-better-than-theengineer-.php">New Scientist review</a>: &quot;It is a book that does not moan about how bad things are but shows us how to change the world and does so admirably.&quot;</p>
<h2>
	Table of Contents</h2>
<h4>
	Preface</h4>
<ul>
	<li>
		Audience</li>
	<li>
		Teaching Usability Engineering</li>
	<li>
		Acknowledgments</li>
</ul>
<h4>
	1. Executive Summary</h4>
<p style="margin-left: 40px;">
	1.1 Cost Savings</p>
<p style="margin-left: 40px;">
	1.2 Usability Now!</p>
<p style="margin-left: 40px;">
	1.3 Usability Slogans</p>
<ul>
	<li style="margin-left: 40px;">
		Your Best Guess Is Not Good Enough</li>
	<li style="margin-left: 40px;">
		The User Is Always Right</li>
	<li style="margin-left: 40px;">
		The User Is Not Always Right</li>
	<li style="margin-left: 40px;">
		Users Are Not Designers</li>
	<li style="margin-left: 40px;">
		Designers Are Not Users</li>
	<li style="margin-left: 40px;">
		Vice Presidents Are Not Users</li>
	<li style="margin-left: 40px;">
		Less Is More</li>
	<li style="margin-left: 40px;">
		Details Matter</li>
	<li style="margin-left: 40px;">
		Help Doesn&#39;t</li>
	<li style="margin-left: 40px;">
		Usability Engineering Is Process</li>
</ul>
<p style="margin-left: 40px;">
	1.4 Discount Usability Engineering</p>
<ul>
	<li style="margin-left: 40px;">
		Scenarios</li>
	<li style="margin-left: 40px;">
		Simplified Thinking Aloud</li>
	<li style="margin-left: 40px;">
		Heuristic Evaluation</li>
</ul>
<p style="margin-left: 40px;">
	1.5 Recipe For Action</p>
<h4>
	Chapter 2: What Is Usability?</h4>
<p style="margin-left: 40px;">
	2.1 Usability and Other Considerations</p>
<p style="margin-left: 40px;">
	2.2 Definition of Usability</p>
<ul>
	<li style="margin-left: 40px;">
		Learnability</li>
	<li style="margin-left: 40px;">
		Efficiency of Use</li>
	<li style="margin-left: 40px;">
		Memorability</li>
	<li style="margin-left: 40px;">
		Few and Noncatastrophic Errors</li>
	<li style="margin-left: 40px;">
		Subjective Satisfaction</li>
</ul>
<p style="margin-left: 40px;">
	2.3 Example: Measuring the Usability of Icons</p>
<p style="margin-left: 40px;">
	2.4 Usability Trade-Offs</p>
<p style="margin-left: 40px;">
	2.5 Categories of Users and Individual User Differences</p>
<h4>
	Chapter 3: Generations of User Interfaces</h4>
<p style="margin-left: 40px;">
	3.1 Batch Systems</p>
<p style="margin-left: 40px;">
	3.2 Line-Oriented Interfaces</p>
<p style="margin-left: 40px;">
	3.3 Full-Screen Interfaces</p>
<ul>
	<li style="margin-left: 40px;">
		Menu Hierarchies</li>
</ul>
<p style="margin-left: 40px;">
	3.4 Graphical User Interfaces</p>
<p style="margin-left: 40px;">
	3.5 Next-Generation Interfaces</p>
<p style="margin-left: 40px;">
	3.6 Long-Term Trends in Usability</p>
<h4>
	Chapter 4: The Usability Engineering Lifecycle</h4>
<p style="margin-left: 40px;">
	4.1 Know the User</p>
<ul>
	<li style="margin-left: 40px;">
		Individual User Characteristics</li>
	<li style="margin-left: 40px;">
		Task Analysis</li>
	<li style="margin-left: 40px;">
		Functional Analysis</li>
	<li style="margin-left: 40px;">
		The Evolution of the User</li>
</ul>
<p style="margin-left: 40px;">
	4.2 Competitive Analysis</p>
<p style="margin-left: 40px;">
	4.3 Goal Setting</p>
<ul>
	<li style="margin-left: 40px;">
		Financial Impact Analysis</li>
</ul>
<p style="margin-left: 40px;">
	4.4 Parallel Design</p>
<p style="margin-left: 40px;">
	4.5 Participatory Design</p>
<p style="margin-left: 40px;">
	4.6 Coordinating the Total Interface</p>
<p style="margin-left: 40px;">
	4.7 Guidelines and Heuristic Evaluation</p>
<p style="margin-left: 40px;">
	4.8 Prototyping</p>
<ul>
	<li style="margin-left: 40px;">
		Scenarios</li>
</ul>
<p style="margin-left: 40px;">
	4.9 Interface Evaluation</p>
<ul>
	<li style="margin-left: 40px;">
		Severity Ratings</li>
</ul>
<p style="margin-left: 40px;">
	4.10 Iterative Design</p>
<ul>
	<li style="margin-left: 40px;">
		Capture the Design Rationale</li>
</ul>
<p style="margin-left: 40px;">
	4.11 Follow-Up Studies of Installed Systems</p>
<p style="margin-left: 40px;">
	4.12 Meta-Methods</p>
<p style="margin-left: 40px;">
	4.13 Prioritizing Usability Activities</p>
<p style="margin-left: 40px;">
	4.14 Be Prepared</p>
<h4>
	Chapter 5: Usability Heuristics</h4>
<p style="margin-left: 40px;">
	5.1 Simple and Natural Dialogue</p>
<ul>
	<li style="margin-left: 40px;">
		Graphic Design and Color</li>
	<li style="margin-left: 40px;">
		Less Is More</li>
</ul>
<p style="margin-left: 40px;">
	5.2 Speak the Users&#39; Language</p>
<ul>
	<li style="margin-left: 40px;">
		Mappings and Metaphors</li>
</ul>
<p style="margin-left: 40px;">
	5.3 Minimize User Memory Load</p>
<p style="margin-left: 40px;">
	5.4 Consistency</p>
<p style="margin-left: 40px;">
	5.5 Feedback</p>
<ul>
	<li style="margin-left: 40px;">
		<a href="../../articles/response-times-3-important-limits/index.php">Response Time </a></li>
	<li style="margin-left: 40px;">
		System Failure</li>
</ul>
<p style="margin-left: 40px;">
	5.6 Clearly Marked Exits</p>
<p style="margin-left: 40px;">
	5.7 Shortcuts</p>
<p style="margin-left: 40px;">
	5.8 Good Error Messages</p>
<ul>
	<li style="margin-left: 40px;">
		Multiple-Level Messages</li>
</ul>
<p style="margin-left: 40px;">
	5.9 Prevent Errors</p>
<ul>
	<li style="margin-left: 40px;">
		Avoid Modes</li>
</ul>
<p style="margin-left: 40px;">
	5.10 Help and Documentation</p>
<ul>
	<li style="margin-left: 40px;">
		A Model of Documentation Use</li>
	<li style="margin-left: 40px;">
		The Minimal Manual</li>
</ul>
<p style="margin-left: 40px;">
	5.11 Heuristic Evaluation</p>
<ul>
	<li style="margin-left: 40px;">
		Effect of Evaluator Expertise</li>
</ul>
<h4>
	Chapter 6: Usability Testing</h4>
<p style="margin-left: 40px;">
	Reliability</p>
<p style="margin-left: 40px;">
	Validity</p>
<p style="margin-left: 40px;">
	6.1 Test Goals and Test Plans</p>
<ul>
	<li style="margin-left: 40px;">
		Test Plans</li>
	<li style="margin-left: 40px;">
		Test Budget</li>
	<li style="margin-left: 40px;">
		Pilot Tests</li>
</ul>
<p style="margin-left: 40px;">
	6.2 Getting Test Users</p>
<ul>
	<li style="margin-left: 40px;">
		Novice versus Expert Users</li>
	<li style="margin-left: 40px;">
		Between-Subjects versus Within-Subjects Testing</li>
</ul>
<p style="margin-left: 40px;">
	6.3 Choosing Experimenters</p>
<p style="margin-left: 40px;">
	6.4 Ethical Aspects of Tests with Human Subjects</p>
<p style="margin-left: 40px;">
	6.5 Test Tasks</p>
<p style="margin-left: 40px;">
	6.6 Stages of a Test</p>
<ul>
	<li style="margin-left: 40px;">
		Preparation</li>
	<li style="margin-left: 40px;">
		Introduction</li>
	<li style="margin-left: 40px;">
		Running the Test</li>
	<li style="margin-left: 40px;">
		Debriefing</li>
</ul>
<p style="margin-left: 40px;">
	6.7 Performance Measurement</p>
<p style="margin-left: 40px;">
	6.8 Thinking Aloud</p>
<ul>
	<li style="margin-left: 40px;">
		Constructive Interaction</li>
	<li style="margin-left: 40px;">
		Retrospective Testing</li>
	<li style="margin-left: 40px;">
		Coaching Method</li>
</ul>
<p style="margin-left: 40px;">
	6.9 Usability Laboratories</p>
<ul>
	<li style="margin-left: 40px;">
		To Videotape or Not</li>
	<li style="margin-left: 40px;">
		Cameraless Videotaping</li>
	<li style="margin-left: 40px;">
		Portable Usability Laboratories</li>
	<li style="margin-left: 40px;">
		Usability Kiosks</li>
</ul>
<h4>
	Chapter 7: Usability Assessment Methods beyond Testing</h4>
<p style="margin-left: 40px;">
	7.1 Observation</p>
<p style="margin-left: 40px;">
	7.2 Questionnaires and Interviews</p>
<p style="margin-left: 40px;">
	7.3 Focus Groups</p>
<p style="margin-left: 40px;">
	7.4 Logging Actual Use</p>
<ul>
	<li style="margin-left: 40px;">
		Combining Logging with Follow-Up Interviews</li>
</ul>
<p style="margin-left: 40px;">
	7.5 User Feedback</p>
<p style="margin-left: 40px;">
	7.6 Choosing Usability Methods</p>
<ul>
	<li style="margin-left: 40px;">
		Combining Usability Methods</li>
</ul>
<h4>
	Chapter 8: Interface Standards</h4>
<ul>
	<li style="margin-left: 40px;">
		User Benefits from Consistency and Standards</li>
	<li style="margin-left: 40px;">
		Vendor Benefits from Consistency and Standards</li>
	<li style="margin-left: 40px;">
		The Dangers of Standards</li>
</ul>
<p style="margin-left: 40px;">
	8.1 National, International and Vendor Standards</p>
<p style="margin-left: 40px;">
	8.2 Producing Usable In-House Standards</p>
<h4>
	Chapter 9: International User Interfaces</h4>
<p style="margin-left: 40px;">
	9.1 International Graphical Interfaces</p>
<ul>
	<li style="margin-left: 40px;">
		Gestural Interfaces</li>
</ul>
<p style="margin-left: 40px;">
	9.2 International Usability Engineering</p>
<p style="margin-left: 40px;">
	9.3 Guidelines for Internationalization</p>
<ul>
	<li style="margin-left: 40px;">
		Characters</li>
	<li style="margin-left: 40px;">
		Numbers and Currency</li>
	<li style="margin-left: 40px;">
		Time and Measurement Units</li>
	<li style="margin-left: 40px;">
		Don&#39;t Despair</li>
</ul>
<p style="margin-left: 40px;">
	9.4 Resource Separation</p>
<p style="margin-left: 40px;">
	9.5 Multilocale Interfaces</p>
<h4>
	Chapter 10: Future Developments</h4>
<p style="margin-left: 40px;">
	10.1 Theoretical Solutions</p>
<p style="margin-left: 40px;">
	10.2 Technological Solutions</p>
<p style="margin-left: 40px;">
	10.3 CAUSE Tools: Computer-Aided Usability Engineering</p>
<p style="margin-left: 40px;">
	10.4 Technology Transfer</p>
<h4>
	Appendix A: Exercises</h4>
<p style="margin-left: 40px;">
	Hints</p>
<h4>
	Appendix B: Bibliography</h4>
<p style="margin-left: 40px;">
	B.1 Conference Proceedings</p>
<p style="margin-left: 40px;">
	B.2 Journals</p>
<p style="margin-left: 40px;">
	B.3 Introductions and Textbooks</p>
<p style="margin-left: 40px;">
	B.4 Handbook</p>
<p style="margin-left: 40px;">
	B.5 Reprint Collections</p>
<p style="margin-left: 40px;">
	B.6 Important Monographs and Collections of Original Papers</p>
<p style="margin-left: 40px;">
	B.7 Guidelines</p>
<ul>
	<li style="margin-left: 40px;">
		Style Guides</li>
</ul>
<p style="margin-left: 40px;">
	B.8 Videotapes</p>
<p style="margin-left: 40px;">
	B.9 Other Bibliographies</p>
<ul>
	<li style="margin-left: 40px;">
		Bibliographic Databases</li>
</ul>
<p style="margin-left: 40px;">
	B.10 References</p>
<h4>
	Author Index</h4>
<h4>
	Subject Index</h4>
<p>
	The original hardcover edition had ISBN 0-12-518405-0. The hardcover edition is now <em>out of print</em> and has been replaced by the updated paperback edition described on this page.</p>
<h2>
	Translations</h2>
<p>
	First Japanese edition, Toppan Publishing,&nbsp;ISBN 4-8101-9009-9</p>
<p>
	<img alt="" src="http://media.nngroup.com.s3.amazonaws.com/media/editor/2012/11/14/useengjapan_small.jpg" style="width: 200px; height: 297px;" /></p>
<p>
	&nbsp;</p>
<p>
	Second Japanese edition, Tokyo Denki University Press,&nbsp;ISBN 4-501-53200-9</p>
<p>
	<img alt="" src="http://media.nngroup.com.s3.amazonaws.com/media/editor/2012/11/14/useengjapan_small_2.jpg" style="width: 200px; height: 250px;" /></p>
<p>
	&nbsp;</p>
<p>
	Chinese edition,&nbsp;China Machine Press,&nbsp;ISBN 7-111-14792-8</p>
<p>
	<img alt="" src="http://media.nngroup.com.s3.amazonaws.com/media/editor/2012/11/14/useeng_chinese.jpg" style="width: 200px; height: 294px;" /></p>


    <div class="show-for-small-down purchase-from">
      <hr>
      
  <h2>Purchase from</h2>
  
    <p><a href="http://www.amazon.com/exec/obidos/ASIN/0125184069/ref=nosim/useitcomusablein">Amazon.com</a></p>
  
  
    
    
      <ul class="no-bullet">
        <li><strong>UK/Europe</strong></li>
        
          <li><a href="http://www.amazon.co.uk/exec/obidos/ASIN/0125184069/useit-21/ref=nosim">Buy from Amazon.co.uk</a></li>
        
      </ul>
    
  


    </div>
    <div class="related">
      


  <hr>



  <h3>Related Reports</h3>
  <ul class="no-bullet">
    
      <li><a href="../../reports/designing-for-young-adults/index.php">Designing for Young Adults (Ages 18-25)</a></li>
    
      <li><a href="../../reports/paper-prototyping-training-video/index.php">Paper Prototyping Training Video</a></li>
    
      <li><a href="../../reports/teenagers-on-the-web/index.php">Teenagers (Ages 13-17) on the Web</a></li>
    
      <li><a href="../../reports/children-on-the-web/index.php">Children (Ages 3-12) on the Web</a></li>
    
      <li><a href="../../reports/university/index.php">University Websites</a></li>
    
  </ul>



  <h3>Related Courses</h3>
   <ul class="no-bullet">
    
        <li><a href="../../courses/ux-basic-training/index.php">UX Basic Training</a></li>
    
        <li><a href="../../courses/journey-mapping/index.php">Journey Mapping to Understand Customer Needs</a></li>
    
        <li><a href="../../courses/measuring-ux/index.php">Measuring User Experience</a></li>
    
        <li><a href="../../courses/analytics-and-user-experience/index.php">Analytics and User Experience</a></li>
    
        <li><a href="../../courses/usability-testing/index.php">Usability Testing</a></li>
    
</ul>



  <h3>Articles</h3>
  <ul class="no-bullet">
    
      <li><a href="../../articles/how-many-test-users/index.php">How Many Test Users in a Usability Study?</a></li>
    
      <li><a href="../../articles/usability-of-websites-for-teenagers/index.php">Teenage Usability: Designing Teen-Targeted Websites</a></li>
    
      <li><a href="../../articles/usability-101-introduction-to-usability/index.php">Usability 101: Introduction to Usability</a></li>
    
      <li><a href="../../articles/ten-usability-heuristics/index.php">10 Usability Heuristics for User Interface Design</a></li>
    
      <li><a href="../../articles/how-to-conduct-a-heuristic-evaluation/index.php">How to Conduct a Heuristic Evaluation</a></li>
    
  </ul>


    </div>
  </div>

  <div class="small-12 medium-4 columns hide-for-small-down l-subsection">
    
  <h2>Purchase from</h2>
  
    <p><a href="http://www.amazon.com/exec/obidos/ASIN/0125184069/ref=nosim/useitcomusablein">Amazon.com</a></p>
  
  
    
    
      <ul class="no-bullet">
        <li><strong>UK/Europe</strong></li>
        
          <li><a href="http://www.amazon.co.uk/exec/obidos/ASIN/0125184069/useit-21/ref=nosim">Buy from Amazon.co.uk</a></li>
        
      </ul>
    
  


  </div>

</div>

    </div>
    




<footer id="page-footer">
    <div class="row hide-for-print">
        <div class="column small-12 medium-6">
            <section class="footer-popular">
                
                <h1>Popular Topics</h1>
                <div style="float: left;  width: 15em; display: block;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/applications/index.php">Application Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/e-commerce/index.php">E-Commerce Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/eyetracking/index.php">Eyetracking </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/information-architecture/index.php">Information Architecture </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/intranets/index.php">Intranet Design </a></li>
	<li><a href="../../topic/interaction-design/index.php">Interaction Design </a></li>
</ul>
</div>

<div style="float: left;  width: 15em; display: block; list-style: none;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/mobile-and-tablet-design/index.php">Mobile and Tablet Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/credibility-persuasion/index.php">Persuasive Design</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/research-methods/index.php">Research Methods</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/user-testing/index.php">User Testing </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/web-usability/index.php">Web Usability </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/writing-web/index.php">Web Writing &amp; Content Strategy </a></li>
</ul>
</div>

<p>&nbsp;</p>

                
            </section>
        </div>

        <div class="column small-12 medium-6">
            <div class="row">
                <div class="column small-6">
                    <section class="footer-about">
                        <h1>Follow Us</h1>
                        <ul>
	<li><a href="../../articles/subscribe/index.php">Subscribe to our weekly UX newsletter</a>,&nbsp;Jakob Nielsen&#39;s Alertbox</li>
	<li>Visit Don Norman&#39;s website, <a href="http://www.jnd.org/"> www.jnd.org </a></li>
	<li>Visit Bruce Tognazzini&#39;s website, <a href="http://www.asktog.com/"> www.asktog.com </a></li>
	<li style="padding-top: 6px;"><a class="twitter-follow-button" data-show-count="false" data-size="large" href="https://twitter.com/nngroup"><img alt="Follow @nngroup on Twitter" height="20" src="https://media.nngroup.com/media/editor/2016/12/30/twitter-follow-button.png" style="width: 117px; height: 20px;" width="117" /></a></li>
	<li style="padding-top: 6px;"><img alt="" height="14" src="https://media.nngroup.com/media/editor/2016/07/26/feed-icon-14x14.png" style="vertical-align:text-top;width:14px;margin-right:.25em;" width="14" /><a href="../../feed/rss/index.php">Subscribe to our RSS feed</a></li>
</ul>

                    </section>
                </div>

                <div class="column small-6">
                    <section class="footer-about right-column">
                        <h1>About
</h1>
                        <ul class="no-bullet">
	<li><a href="../../about/contact/index.php">Contact information</a></li>
	<li><a href="../../about/index.php">About Us</a></li>
	<li><a href="../../about/why-nng/index.php">Why NN/g? </a></li>
	<li><a href="../../return-policy/index.php">Return Policy</a></li>
</ul>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="copyright column small-12">
            <p><a href="../../copyright-and-reprint-info/index.php">Copyright</a> &copy; 1998-2017 Nielsen Norman Group, All Rights Reserved.</p>
        </div>
    </div>
</footer>


    


        <script src="https://media.nngroup.com/static/vendor/fastclick/js/fastclick.min.js"></script>
        <script src="https://media.nngroup.com/static/vendor/foundation/js/foundation.min.js"></script>
        <script src="http://media.nngroup.com/static/js/app.js"></script>
        <script src="http://media.nngroup.com/static/js/scroll-to-fixed.js"></script><script>var externalLinks = document.querySelectorAll('a[href^="http"]');for (var i = externalLinks.length-1; i >= 0; i--) {externalLinks[i].addEventListener("click", function(event) { alert("External links are disabled for this test."); event.preventDefault(); }, false);}var mailLinks = document.querySelectorAll('a[href^="mailto"]');for (var i = mailLinks.length-1; i >= 0; i--) {mailLinks[i].addEventListener("click", function(event) { alert("Email links are disabled for this test."); event.preventDefault(); }, false);}var externalForms = document.querySelectorAll('form');for (var i = externalForms.length-1; i >= 0; i--) {externalForms[i].addEventListener("submit", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false); externalForms[i].addEventListener("click", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false);}</script>

        <script>
            
            $(function() {
                $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
                $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
            });
        </script>

        


        
        <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1072699960;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072699960/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
    </body>

<!-- Mirrored from www.nngroup.com/books/usability-engineering/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:02:45 GMT -->
</html>
