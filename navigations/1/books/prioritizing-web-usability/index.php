<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->





    
<!-- Mirrored from www.nngroup.com/books/prioritizing-web-usability/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:02:35 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1026.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"beacon":"bam.nr-data.net","queueTime":0,"applicationTime":381,"agent":"","licenseKey":"1a30ef9931","errorBeacon":"bam.nr-data.net","applicationID":"2048992","transactionName":"NFxSMRBUX0EDUkIPDA0WdhAMVkVbDV8ZBAwMUkNLFFxURRELdAkMCH1VEQNcXRwFVEI="}</script>
        <title>Prioritizing Web Usability: Book by Jakob Nielsen and Hoa Loranger</title><meta property="og:title" content="Prioritizing Web Usability: Book by Jakob Nielsen and Hoa Loranger" />
  
        
        <meta name="description" content="Jakob Nielsen&#39;s classic book discusses the most important of the guidelines for Web usability. It also revisits guidelines from the 1990s in light of newer research.">
        <meta property="og:description" content="Jakob Nielsen&#39;s classic book discusses the most important of the guidelines for Web usability. It also revisits guidelines from the 1990s in light of newer research." />
        
  
        
	
        

        

        <meta name="robots" content="noarchive">
        <meta name="viewport" content="width=device-width">

        <link rel="search" type="application/opensearchdescription+xml" title="NN/g Search" href="../../opensearch.xml" />
        <link rel="shortcut icon" href="http://media.nngroup.com/static/img/favicon.ico" />

        <link rel="stylesheet" href="https://media.nngroup.com/static/vendor/font-awesome/css/font-awesome.min.css">

        
        <link rel="stylesheet" href="https://media.nngroup.com/static/css/7232a960f1df.css" type="text/css" />
        <link rel="stylesheet" href="http://fast.fonts.com/t/1.css?apiType=css&amp;projectid=c5846260-9def-46ec-9407-0f1349bd2d64" type="text/css">

        <script src="https://media.nngroup.com/static/vendor/modernizr/js/modernizr.min.js"></script>

        <script type="text/javascript" src="https://media.nngroup.com/static/js/3283be5def74.js"></script>
        

        

        
        
        <!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=229034,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('http://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->


<!-- Google Analytics Tracking Code for http://nngroup.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23132681-1', 'nngroup.com');
  ga('require', 'linkid', 'https://www.nngroup.com/books/prioritizing-web-usability/linkid.js');
  ga('require', 'displayfeatures');

// Optimizely Universal Analytics Integration
 window.optimizely = window.optimizely || [];
 window.optimizely.push("activateUniversalAnalytics");

  ga('send', 'pageview');

</script>
<script>
/**
* Function to track a click on an outbound link in Google Analytics.
*/
function trackOutboundLink(label, url) {
    if(window.ga && ga.loaded) {
        ga('send', 'event', 'outbound', 'click', label, {'hitCallback': function(){document.location = url;}
        });
    } else {
        document.location = url;
    }
}
/**
* Function to track a click on a link in the article announcement area.
*/
$('#articleAnnouncement a').on('click', function(){
  ga('send', 'event', 'link', 'click', 'articleAnnouncement');
});
</script>

<script type="text/javascript">
 setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2421.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



<!-- Hotjar Tracking Code for http://nngroup.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:121605,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

        <script src="https://media.nngroup.com/static/js/analytics/track_outbound_link.js"></script>
        
        
        
    </head>
    <body class="location-about location-books book-detail">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
    <header id="page-header">
        <div id="cart" class="shopping-cart mobile-cart show-for-small-down"></div>
        <div class="row">
            <div id="masthead" class="masthead small-12 medium-7 column">

                <!-- The button to toggles menu on small screen -->

                <h1><a href="../../index.php"><img src="http://media.nngroup.com/static/img/logo-wordmark.png" width="74" height="30"> Nielsen <span class="norman">Norman</span> Group</a></h1>
                <p class="tagline show-for-medium-up">Evidence-Based User Experience Research, Training, and Consulting</p>

                <!-- The button to toggles form search on small screen -->


                <div style="clear: right;width:  100%;height: 1px;"></div><?php define('ROOT_PATH',$_SERVER["DOCUMENT_ROOT"] . '/apps/navigation_styles/navigations/1'); ?><?php include(ROOT_PATH)."/navigation.php"; ?>

                <!-- Drop down menu on small screen -->
                <div id="hideNav" class="show-for-small-down">
                  
                  <ul class="menu-content">
                    <li><a href="../../index.php">Home</a></li>
                    <li class="has-submenu">
                      <a>Training</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../training/index.php">Overview</a></li>
                        
<li id="nav-ux-conference"><a href="../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../online-seminars/index.php"><span>Online Seminars</span></a></li>

                      </ul>
                    </li>
                    <li class="has-submenu">
                      <a>Consulting</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
  <li><a href="../../consulting/index.php">Overview</a></li>
  
  <li><a href="../../consulting/benchmarking/index.php">Benchmarking</a></li>
  
  <li><a href="../../consulting/expert-design-review/index.php">Expert Design Review</a></li>
  
  <li><a href="../../consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
  
  <li><a href="../../consulting/usability-testing/index.php">Usability Testing </a></li>
  
  <li><a href="../../consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
  
  <li><a href="../../in-house-training/index.php">In-House Training</a></li>
</ul>

                    </li>

                    <li><a href="../../reports/index.php">Reports</a></li>
                    <li><a href="../../articles/index.php">Articles</a></li>
                    <li class="has-submenu"><a>About NN/G</a><i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../about/index.php">Overview</a></li>
                        <li><a href="../../people/index.php">People</a></li>
                        <li><a href="../../about/why-nng/index.php">Why NN/g?</a></li>
                        <li><a href="../../about/contact/index.php">Contact</a></li>
                        <li><a href="../../news/index.php">News</a></li>
                        <li><a href="../../about/history/index.php">History</a></li>
                        <li><a href="../index.php">Books</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                </div>

                <hr class="header-divider show-for-small-only"/>


            </div>
       </div>

        <div id="hideNav">            <nav class="primary-nav small-12 column">
                <ul>
                    <li><a class="navitem" id="nav-home" href="../../index.php">Home</a></li>
                    <li><a class="navitem" id="nav-training" href="../../training/index.php">Training</a></li>
                    <li><a class="navitem" id="nav-consulting" href="../../consulting/index.php">Consulting</a></li>
                    <li><a class="navitem" id="nav-reports" href="../../reports/index.php">Reports</a></li>
                    <li><a class="navitem" id="nav-articles" href="../../articles/index.php">Articles</a></li>
                    <li><a class="navitem" id="nav-about" href="../../about/index.php">About NN/G</a></li>
                </ul>
            </nav>
            <nav class="secondary-nav small-12 medium-12 large-12 columns">
                
<ul class="inline-list">
    <li id="nav-people"><a href="../../people/index.php"><span>People</span></a></li>
    <li id="nav-whynng"><a href="../../about/why-nng/index.php"><span>Why NN/g?</span></a></li>
    <li id="nav-contact"><a href="../../about/contact/index.php"><span>Contact</span></a></li>
    <li id="nav-news"><a href="../../news/index.php"><span>News</span></a></li>
    <li id="nav-history"><a href="../../about/history/index.php"><span>History</span></a></li>
    <li id="nav-books"><a href="../index.php"><span>Books</span></a></li>
</ul>

            </nav>
        </div>
    </header>
    <div class="l-content">
    
<div class="row">
  <div class="medium-2 columns hide-for-small-down">
    <img class="book-cover" src="https://media.nngroup.com/media/publications/books/prioritizing_web_usability_small.gif.300x400_q95_autocrop_crop_upscale.png">
  </div>
  <div class="small-12 medium-6 columns l-subsection">
    <h1>Prioritizing Web Usability</h1>

    <img class="show-for-small-down book-cover" src="https://media.nngroup.com/media/publications/books/prioritizing_web_usability_small.gif.300x400_q95_autocrop_crop_upscale.png">
    <h2>
<a href="../../people/hoa-loranger/index.php">Hoa Loranger</a>
            

        
             and 
            
                <a href="../../people/jakob-nielsen/index.php">Jakob Nielsen</a>
, 2006</h2>

    

    <p>After more than a decade of Web usability research, we literally have thousands of guidelines for making better websites. But what are the most important ones that all designers need to know? That&#39;s what <em>Prioritizing Web Usability</em> is about. A second goal of the book is to update the early Web usability guidelines we published in the 1990s. All the guidelines found since 2000 continue to hold, but what about findings from the studies we conducted 1994 to 1999? The book compares these old studies with more recent ones and explains which of the old guidelines should still be followed.&nbsp;406 pages, heavily illustrated, in full color. (New Riders Press, Berkeley CA.)</p>

<p>ISBN-10: 0-321-35031-6<br />
ISBN-13: 978-0-321-35031-2</p>

<h2>Press</h2>

<p><a href="http://www.washingtonpost.com/wp-dyn/content/article/2006/05/31/AR2006053102190.php">Washington Post</a>: &quot;Drills down more deeply into the phenomenon of hyperactive Web browsing than I&#39;ve seen before.&quot;</p>

<p><a href="http://www.felgall.com/usability.htm">Felgall.com</a>: &quot;A must have book for anyone serious about web site usability.&quot;</p>

<p><a href="http://www.ecommerce-blog.org/archives/book-review-prioritizing-web-usability/">Ecommerce-blog.org</a>: &quot;Overall, this could be the only resource a savvy website owner needs to make their website very usable.&quot;</p>

<p><a href="http://www.456bereastreet.com/archive/200609/prioritizing_web_usability_book_review/">456 Berea Street</a>: &quot;Excellent book and a must-have for anybody involved in creating a website.&quot;</p>

<p><a href="http://mondaybynoon.com/20061120/prioritizing-web-usability-book-review/">Monday by Noon</a>: &quot;Jakob Nielsen and Hoa Loranger have really put together a resource that belongs on any designer&#39;s or developer&#39;s bookshelf.&quot;</p>

<p><a href="http://shapeshed.com/book_review_prioritizing_web_usability/">Shapeshed</a>: &quot;The book is brilliant as it backs up advice and assumptions with empirical data. [...] the next time a client asks why it is bad to have popup windows everywhere you can point to a study.&quot;</p>

<p><a href="http://www.cehwiedel.com/blogs/traces/?p=1181">Kicking Over My Traces</a>: &quot;If your business has a website, your web designer needs to read Nielsen&#39;s book.&quot;</p>

<p><a href="http://www.dlwaldron.com/BooksonWebdesign.php">D&#39;Lynne Waldron</a>: &quot;The advice in this book is worth millions of dollars to most corporations.&quot;</p>

<p><a href="http://churchcommunicationspro.com/2006/11/the-all-important-church-website-home-page-part-1/">Church Communications Pro</a>: &quot;An expensive book [...] but absolutely worth it!&quot;</p>

<p><a href="http://www.aneclecticmind.com/2006/08/04/prioritizing-web-usability/">Aneclecticmind.com</a>: &quot;Do you design Web sites? Get and read this book!&quot;</p>

<p><a href="http://www.fredcavazza.net/?2006/06/29/1202-note-de-lecture-sur-le-livre-prioritizing-web-usability-">FredCavazza.net</a>: &quot;Un bon livre qui sait intelligemment faire la part des choses entre la simplicit&eacute; d&#39;usage et les imp&eacute;ratifs d&#39;image de marque.&quot;</p>

<p><a href="http://www.jesusencinar.com/2006/06/nuevo_libro_de_.php">JesusEncinar.com</a>: &quot;Un libro estupendo de un experto en la material.&quot;</p>

<h2>Table of Contents</h2>

<p style="margin-left: 40px;">Preface</p>

<ul style="margin-left: 40px;">
	<li>What is Usability?</li>
	<li>Where to Find Detailed User Research</li>
	<li>Usability Then and Now</li>
	<li>Who Should Read This Book?</li>
</ul>

<ol>
	<li>Introduction: Nothing To Hide
	<ul style="">
		<li>Where We Got Our Data
		<ul>
			<li>How We Did the Book Study</li>
			<li>Sites Tested</li>
			<li>What if a Site Has Changed?</li>
			<li>User Testing in Three Days</li>
		</ul>
		</li>
		<li>Tell Me Again: Why Do I Need to Do User Testing?
		<ul>
			<li>The Exceptions</li>
		</ul>
		</li>
	</ul>
	</li>
	<li>The Web User Experience
	<ul style="">
		<li>How Well Do People Use the Web?
		<ul>
			<li>The Measure of Success</li>
			<li>Web-Wide Success Rates</li>
			<li>Success By Experience Level</li>
		</ul>
		</li>
		<li>User Satisfaction with Web Sites
		<ul>
			<li>Three Guidelines for Supporting Deep-Link Users</li>
		</ul>
		</li>
		<li>How People Use Sites
		<ul>
			<li>Four Goals in Thirty Seconds</li>
			<li>The Homepage: So Much To Say, So Little Time</li>
			<li>Interior Page Behavior</li>
			<li>Optimizing Interior Page Links</li>
			<li>The Rise of &quot;Answer Engines&quot;</li>
		</ul>
		</li>
		<li>Search Dominance
		<ul>
			<li>Four Ways to Grab Value From Search Engine Visitors</li>
			<li>Organic vs. Sponsored Links</li>
			<li>How People Use the Search Engine Results Page</li>
			<li>Number One Guideline for Search Engine Optimization</li>
			<li>Using Keyword Pricing to Estimate Usability Improvements</li>
			<li>How To Determine the Optimal Bid for a Search Keyword Ad</li>
			<li>How Much Is Improved Usability Worth?</li>
			<li>Three Reasons To Improve Your Site</li>
			<li>Design for Short Scrolling</li>
		</ul>
		</li>
		<li>Scrolling
		<ul>
			<li>Defining Standards and Conventions</li>
		</ul>
		</li>
		<li>Complying With Design Conventions and Usability Guidelines
		<ul>
			<li>Seven Reasons for Standard Design Elements</li>
		</ul>
		</li>
		<li>Information Foraging
		<ul>
			<li>Information Scent: Predicting a Path&#39;s Success</li>
			<li>Diet Selection: What Sites to Visit</li>
			<li>Three Ways to Enhance Information Scent</li>
			<li>Patch Abandonment: When to Hunt Elsewhere</li>
			<li>New Design Strategies for Attracting Information Foragers</li>
			<li>More information</li>
			<li>Informavore Navigation Behavior</li>
		</ul>
		</li>
	</ul>
	</li>
	<li>Revisiting Early Web Usability Findings
	<ul style="">
		<li>Problems That Haven&#39;t Changed
		<ul>
			<li>Links That Don&#39;t Change Color When Visited</li>
			<li>Why Designers Don&#39;t Believe Us</li>
			<li>Breaking the Back Button</li>
			<li>Fitts&#39; Law of Click Times</li>
			<li>Opening New Browser Windows</li>
			<li>The Curse of Maximization</li>
			<li>How Can You Use Windows if You Don&#39;t Understand Windows?</li>
			<li>Pop-Up Windows</li>
			<li>Most Hated Advertising Techniques</li>
			<li>Design Elements That Look Like Advertisements</li>
			<li>Avoid Influencing Users During Testing</li>
			<li>Violating Web-Wide Conventions</li>
			<li>Vaporous Content and Empty Hype</li>
			<li>Dense Content and Unscannable Text</li>
		</ul>
		</li>
		<li>Technological Change: Its Impact on Usability
		<ul>
			<li>1986 Air Force Guidelines Stand the Test of Time</li>
			<li>Don Norman&#39;s Three Levels of Emotional Design</li>
			<li>Slow Download Time</li>
			<li>Frames</li>
			<li>Flash: The Good, the Bad, and the Usable</li>
			<li>Low-Relevancy Search Listings</li>
			<li>Multimedia and Long Videos</li>
			<li>Teenagers: Masters of Technology?</li>
			<li>Frozen Layouts</li>
			<li>Sad Mac</li>
			<li>Cross-Platform Incompatibility</li>
			<li>Mobile Devices: A New Argument for Cross-Platform Design?</li>
		</ul>
		</li>
		<li>Adaptation: How Users Have Influenced Usability
		<ul>
			<li>Uncertain Clickability</li>
			<li>Links That Aren&#39;t Blue</li>
			<li>Scrolling</li>
			<li>Registration</li>
			<li>Complex URLs</li>
			<li>Pull-Down and Cascading Menus</li>
		</ul>
		</li>
		<li>Restraint: How Designers Have Alleviated Usability Problems
		<ul>
			<li>Plug-Ins and Bleeding-Edge Technology</li>
			<li>3D User Interface</li>
			<li>Bloated Design</li>
			<li>Splash Pages</li>
			<li>Moving Graphics and Scrolling Text</li>
			<li>Custom GUI Widgets</li>
			<li>&quot;About Us&quot; Features Don&#39;t Say Enough</li>
			<li>Not Disclosing Who&#39;s Behind Information</li>
			<li>Made-Up Words</li>
			<li>Outdated Content</li>
			<li>Inconsistency Within a Web Site</li>
			<li>Premature Requests for Personal Information</li>
			<li>Multiple Sites</li>
			<li>Orphan Pages</li>
		</ul>
		</li>
		<li>Assessing the Fate of the Early Findings</li>
	</ul>
	</li>
	<li>Prioritizing Your Usability Problems
	<ul style="">
		<li>How Severe Is the Problem?</li>
		<li>Scoring Severity</li>
		<li>What Makes Problems Severe
		<ul>
			<li>Hospital Usability: In Critical Condition</li>
		</ul>
		</li>
		<li>The Scale of Misery
		<ul>
			<li>The First Law of E-Commerce</li>
		</ul>
		</li>
		<li>Why Users Fail
		<ul>
			<li>Five Biggest Causes of User Failure</li>
		</ul>
		</li>
		<li>Is It Enough to Focus on the Worst Problems?</li>
	</ul>
	</li>
	<li>Search
	<ul style="">
		<li>How to Know if You Need Search</li>
		<li>The State of Search</li>
		<li>Three Simple Steps to Better Search</li>
		<li>The Three Things Users Expect from Search</li>
		<li>When is a Search Not Search?</li>
		<li>How Search Should Work</li>
		<li>Search Interface</li>
		<li>Don&#39;t Try to Be a Search Engine</li>
		<li>Search Box Go Wide</li>
		<li>Query Length and Search Box Width</li>
		<li>Advanced Search</li>
		<li>Target Practice</li>
		<li>Search Engine Results Pages</li>
		<li>SERP Dating Conventions</li>
		<li>Help Bad Spellers</li>
		<li>Best Bets</li>
		<li>Four Ways to Build Best Bets</li>
		<li>Maintaining Best Bets</li>
		<li>Sorting the SERP</li>
		<li>No Results Found</li>
		<li>One Result Found</li>
		<li>Search Engine Optimization</li>
		<li>Black-Hat SEO Tricks</li>
		<li>Naming Names</li>
		<li>The Beauty of Using Text-Only Ads</li>
		<li>Tracking the Value of Search Ads</li>
		<li>Linguistic SEO</li>
		<li>The Top Linguistic SEO Guideline</li>
		<li>Keyword Overuse Backfires</li>
		<li>Think Phrases, Not Keywords</li>
		<li>Architectural SEO</li>
		<li>Reputation SEO</li>
		<li>How Search Engines Determine a Site&#39;s Reputation</li>
	</ul>
	</li>
	<li>Navigation and Information Architecture
	<ul style="">
		<li>Am I There Yet?</li>
		<li>Match the Site Structure to User Expectations</li>
		<li>Navigation: Be Consistent</li>
		<li>Navigation: Beware the Coolness Factor</li>
		<li>Reduce Clutter and Avoid Redundancy</li>
		<li>Links and Label Names: Be Specific</li>
		<li>Vertical Dropdown Menus: Short Is Sweet</li>
		<li>Multilevel Menus: Less is More</li>
		<li>Can I Click on It?</li>
		<li>Affordances</li>
		<li>Direct Access From the Homepage</li>
	</ul>
	</li>
	<li>Typography: Readability &amp; Legibility
	<ul style="">
		<li>The Downside of Dummy Type</li>
		<li>Four Top Guidelines for Type</li>
		<li>Body Text: The Ten-Point Rule</li>
		<li>Age Is Not the Issue</li>
		<li>Avoid Anti-Aliasing</li>
		<li>When the Same Size Appears Smaller</li>
		<li>Planning for Differences in Hardware</li>
		<li>Accessibility Affects All of Us</li>
		<li>The Rule of Relative Size</li>
		<li>Relative Specifications</li>
		<li>Designing for Vision-Impaired Users</li>
		<li>Choosing Fonts</li>
		<li>When Will Screens Read as Well as Print?</li>
		<li>Mixing Fonts and Colors</li>
		<li>The Case Against Caps</li>
		<li>Text and Background Contrast</li>
		<li>Two Ways to Make Colors Pop</li>
		<li>Common Color Blindness</li>
		<li>Text Images</li>
		<li>Moving Text</li>
	</ul>
	</li>
	<li>Writing for the Web
	<ul style="">
		<li>How Poor Writing Makes Web Sites Fail</li>
		<li>Hire a Web Writer</li>
		<li>Understanding How Web Users Read</li>
		<li>Why Users Scan</li>
		<li>Know Your Audience</li>
		<li>Writing for Your Reader</li>
		<li>Three Guidelines for Better Web Writing</li>
		<li>Use Simple Language</li>
		<li>Meeting Low Literacy Needs</li>
		<li>Tone Down Marketing Hype</li>
		<li>When and Where to Toot Your Horn</li>
		<li>Writing Samples: Before and After</li>
		<li>The Two-Sentence Test</li>
		<li>Keeping It Short and Sweet</li>
		<li>Summarize Key Points and Pare Down</li>
		<li>Writing Descriptive Labels</li>
		<li>Making Usability Skyrocket</li>
		<li>Formatting Text for Readability</li>
		<li>Highlight Keywords</li>
		<li>Use Concise and Descriptive Titles and Headings</li>
		<li>Three Guidelines for Heading Hierarchy</li>
		<li>Use Bulleted and Numbered Lists</li>
		<li>Parallel Phrasing Is Important</li>
		<li>More Information</li>
		<li>Keep Paragraphs Short</li>
	</ul>
	</li>
	<li>Providing Good Product Information
	<ul style="">
		<li>Where To Display Prices</li>
		<li>Show Me the Money</li>
		<li>No Excuses</li>
		<li>Approximate Prices Are Better Than None</li>
		<li>Disclose Extra Fees</li>
		<li>Win Customer Confidence</li>
		<li>Describe the Product</li>
		<li>Test Driving an Auto Site</li>
		<li>Provide Pictures and Product Illustrations</li>
		<li>Five Big Illustration Errors</li>
		<li>Layer Product Pages</li>
		<li>Display Bona Fides</li>
		<li>Support Comparison Shopping</li>
		<li>Refine and Sort</li>
		<li>Support Sales with Quality Content</li>
		<li>Four Reasons for Informational Articles</li>
		<li>They Don&#39;t Have Products, Do They?</li>
	</ul>
	</li>
	<li>Presenting Page Elements
	<ul style="">
		<li>When the &quot;Three-Click Rule&quot; Wreaks Havoc</li>
		<li>Should You Design for Scrolling?</li>
		<li>Four Rules of Scrolling</li>
		<li>Beware of Magic Numbers</li>
		<li>Guiding Users, Step by Step</li>
		<li>Keep Like with Like</li>
		<li>Sloppy Formatting of Forms</li>
		<li>Look at Me!</li>
		<li>Satisfy Your Users&#39; Expectations</li>
		<li>Using White Space</li>
	</ul>
	</li>
	<li>Balancing Technology with People&#39;s Needs
	<ul style="">
		<li>Flashback to 2000</li>
		<li>Use Multimedia When It Benefits Your Audience</li>
		<li>Providing Alternative Accessibility</li>
		<li>Overcoming Barriers to Multimedia</li>
		<li>Accommodate Low-Tech Users</li>
		<li>Sites for Kids: Keep It Real</li>
		<li>Design for Your Audience&#39;s Connection Speed</li>
		<li>Provide a Simple and Accurate Loading-Status Indicator</li>
		<li>Watch Your Language</li>
		<li>Underestimate Your Users&#39; Technical Knowledge</li>
		<li>Detect Users&#39; Bandwidth</li>
		<li>Stick to Familiar Interface Conventions</li>
		<li>Pop-Ups Usually Strike Out</li>
		<li>Scroll Bars Should Be Standard</li>
		<li>Rich vs. Poor Media</li>
		<li>Avoid Multimedia Excesses</li>
		<li>How Do You Turn This Thing Off?</li>
		<li>Turn Down the Volume</li>
		<li>When to Take a Commercial Break</li>
		<li>Make Videos for the Web</li>
		<li>The Practice of Simplicity</li>
		<li>Improving a Site: Sooner or Later?</li>
		<li>Three Tips: Simplify. Simplify. Simplify.</li>
		<li>Toward a More Elegant Design</li>
	</ul>
	</li>
	<li>Final Thoughts: Design That Works</li>
	<li>Index</li>
</ol>

<h2>Translations</h2>

<table cellpadding="3" cellspacing="3" width="100%">
	<tbody>
		<tr>
			<td width="50%"><a href="http://product.dangdang.com/product.aspx?product_id=9345032"><img alt="" class="bookimg" src="http://media.nngroup.com.s3.amazonaws.com/media/editor/2012/12/31/prioritizing_web_usability_chinese-simplified-cover-280_.gif" style="width: 175px; height: 244px;" /> </a></td>
			<td width="50%"><a href="http://www.delightpress.com.tw/book.aspx?book_id=SKTM00005"><img alt="Cover of Traditional Chinese Translation of Prioritizing Web Usability" class="bookimg" src="http://media.nngroup.com.s3.amazonaws.com/media/editor/2012/12/31/prioritizing_web_usability_chinese-traditional-cover-280_.jpg" style="width: 175px; height: 244px;" /> </a></td>
		</tr>
		<tr>
			<td width="50%">
			<p><a href="http://product.dangdang.com/product.aspx?product_id=9345032">Chinese (simplified) at Dangjang.com </a></p>

			<p>网站优化:通过提高Web可用性构建用户满意的网站<br />
			ISBN 9787121039683</p>
			</td>
			<td width="50%">
			<p><a href="http://www.delightpress.com.tw/book.aspx?book_id=SKTM00005">Chinese (traditional) at Delight Press in Taiwan </a></p>

			<p>設計好網站的黃金準則<br />
			ISBN 978-986-6761-50-8</p>
			</td>
		</tr>
		<tr>
			<td width="50%"><a href="http://www.amazon.fr/dp/2744023159/"><img alt="Book cover of the French Translation of Prioritizing Web Usability" src="http://media.nngroup.com.s3.amazonaws.com/media/editor/2012/12/31/prioritizing_web_usability_french-cover-2nd-edition-280_.gif" style="width: 175px; height: 247px;" /> </a></td>
			<td width="50%"><a href="http://www.amazon.de/gp/product/3827327636/"><img alt="Book cover for the German Translation of Prioritizing Web Usability" src="http://media.nngroup.com.s3.amazonaws.com/media/editor/2012/12/31/prioritizing_web_usability_german-cover-280_.gif" style="width: 175px; height: 244px;" /> </a></td>
		</tr>
		<tr>
			<td width="50%">
			<p><a href="http://www.amazon.fr/dp/2744023159/">French at Amazon.fr </a></p>

			<p>Site Web: priorit&eacute; &agrave; la simplicit&eacute;<br />
			ISBN 978-2744021527 (first edition)<br />
			ISBN 978-2744023156 (second edition)</p>

			<p>&nbsp;</p>
			</td>
			<td width="50%">
			<p><a href="http://www.amazon.de/gp/product/3827327636/">German at Amazon.de </a></p>

			<p>Web Usability (Deutsch)<br />
			ISBN 3-8273-2448-3 (first edition)<br />
			ISBN 978-3827327635 (second edition)<br />
			<a href="../../books-prioritizing-web-usability-toc-german/index.php">Table of Contents </a></p>

			<p>&nbsp;</p>
			</td>
		</tr>
		<tr>
			<td><a href="http://www.apogeonline.com/libri/88-503-2539-8"><img alt="Book cover of the Italian Translation of Prioritizing Web Usability" src="http://media.nngroup.com.s3.amazonaws.com/media/editor/2012/12/31/prioritizing_web_usability_italian-cover-280_.gif" style="width: 175px; height: 248px;" /> </a></td>
			<td><a href="http://www.amazon.co.jp/gp/product/4844358928/"><img alt="Book cover for the Japanese Translation of Prioritizing Web Usability" src="http://media.nngroup.com.s3.amazonaws.com/media/editor/2012/12/31/prioritizing_web_usability_japanese-cover-280_.gif" style="width: 175px; height: 247px;" /> </a></td>
		</tr>
		<tr>
			<td>
			<p><a href="http://www.amazon.fr/dp/2744023159/">I </a> <a href="http://www.apogeonline.com/libri/88-503-2539-8"> talian at Apogeonline </a></p>

			<p>Web Usability 2.0: L&#39;usabilit&agrave; che conta<br />
			ISBN 88-503-2539-8</p>
			</td>
			<td>
			<p><a href="http://www.amazon.de/gp/product/3827327636/">J </a> <a href="http://www.amazon.co.jp/gp/product/4844358928/"> apanese at Amazon.co.jp </a></p>

			<p>新ウェブ・ユーザビリティ<br />
			ISBN 4-8443-5892-8</p>

			<p>&nbsp;</p>
			</td>
		</tr>
		<tr>
			<td><a href="http://www.itcpub.co.kr/book/book_view1.php?h_code=h_9&amp;book_num=902"><img alt="Book cover of the Korean Translation of Prioritizing Web Usability" src="http://media.nngroup.com.s3.amazonaws.com/media/editor/2012/12/31/prioritizing_web_usability_korean-cover-280_.gif" style="width: 175px; height: 244px;" /> </a></td>
			<td><a href="http://helion.pl/ksiazki/optymalizacja-funkcjonalnosci-serwisow-internetowych-jakob-nielsen-hoa-loranger,ofuser.htm"><img alt="Book cover for the Polish Translation of Prioritizing Web Usability" src="http://media.nngroup.com.s3.amazonaws.com/media/editor/2012/12/31/prioritizing_web_usability_polish-cover-280_.jpg" style="width: 175px; height: 270px;" /> </a></td>
		</tr>
		<tr>
			<td>
			<p><a href="http://www.itcpub.co.kr/book/book_view1.php?h_code=h_9&amp;book_num=902">Korean at ITC Publications </a></p>

			<p>웹 유저빌러티 우선순위<br />
			ISBN 89-90758-58-0</p>
			</td>
			<td>
			<p><a href="http://helion.pl/ksiazki/optymalizacja-funkcjonalnosci-serwisow-internetowych-jakob-nielsen-hoa-loranger,ofuser.htm">Polish at Helion.pl </a></p>

			<p>Optymalizacja funkcjonalności serwis&oacute;w internetowych<br />
			ISBN 83-246-0845-1</p>
			</td>
		</tr>
		<tr>
			<td><a href="http://www.submarino.com.br/produto/5661063/livro-projetando-websites-com-usabilidade"><img alt="Book cover of the Portuguese Translation of Prioritizing Web Usability" src="http://media.nngroup.com.s3.amazonaws.com/media/editor/2012/12/31/prioritizing_web_usability_portuguese-cover-280_.jpg" style="width: 160px; height: 280px;" /> </a></td>
			<td><a href="http://www.williamspublishing.com/Books/978-5-8459-1222-0.php"><img alt="Book cover for the Russian  Translation of Prioritizing Web Usability" src="http://media.nngroup.com.s3.amazonaws.com/media/editor/2012/12/31/prioritizing_web_usability_russian-cover-280_.jpg" style="width: 150px; height: 280px;" /> </a></td>
		</tr>
		<tr>
			<td>
			<p><a href="http://www.submarino.com.br/produto/5661063/livro-projetando-websites-com-usabilidade">Portuguese at Submarino.com </a></p>

			<p>Usabilidade na Web: Projetando Websites com Qualidade (new Portuguese title)<br />
			ISBN: 85-352-2190-5<br />
			ISBN-13: 978-85-352-2190-9<br />
			(The original Portuguese title for this book was Projetando Websites com Usabilidade.)</p>
			</td>
			<td>
			<p><a href="http://www.amazon.de/gp/product/3827327636/">R</a><a href="http://www.williamspublishing.com/Books/978-5-8459-1222-0.php">ussian at Dialektika-Williams </a></p>

			<p>Web-дизайн: удобство использования Web-сайтов<br />
			ISBN 978-5-8459-1222-0</p>
			</td>
		</tr>
		<tr>
			<td><a href="http://www.agapea.com/libros/Usabilidad-Prioridad-en-el-diseno-Web-9788441520929-i.htm"><img alt="Book cover of the Spanish Translation of Prioritizing Web Usability" src="http://media.nngroup.com.s3.amazonaws.com/media/editor/2012/12/31/prioritizing_web_usability_spanish-cover-280_.jpg" style="width: 175px; height: 244px;" /> </a></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>
			<p><a href="http://www.agapea.com/libros/Usabilidad-Prioridad-en-el-diseno-Web-9788441520929-i.htm">Spanish at Agapea.com </a></p>

			<p><a href="http://www.amazon.com/dp/8441520925?tag=useitcomusablein">Spanish at Amazon.com </a></p>

			<p>Usabilidad: Prioridad en el dise&ntilde;o Web<br />
			ISBN 84-415-2092-9</p>

			<p><a href="../../books-prioritizing-web-usability-toc-spanish/index.php">Table of Contents (in Spanish) </a></p>
			</td>
			<td>
			<p>&nbsp;</p>
			</td>
		</tr>
	</tbody>
</table>

<h2>Errata</h2>

<p>Errors in the first printing:</p>

<p>Page 69, sidebar box, last paragraph:<br />
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Change &quot;accidentally click outside the parent browser window&quot; to &quot;accidentally click the parent browser window&quot;<br />
Page 98, figure caption:<br />
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &quot;British or American flag&quot; should read &quot;British or French flag&quot;<br />
Page 99, top illustration:<br />
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; The bottom line of this illustration was cropped off during page layout. There was supposed to be one more link visible.<br />
Page 163, box titled &quot;Naming Names&quot;, 2nd paragraph:<br />
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; The correct name of the organization is First Division Association.<br />
Page 388, bottom-most figure caption:<br />
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Change &quot;This is the first of two&quot; to &quot;This is the second of two&quot;</p>

<p>We thank Jonathan Cooper, Tim Holyoake, Tom Styles, and Zhang Liang for spotting errors in the book and helping us make it better in subsequent printings.</p>


    <div class="show-for-small-down purchase-from">
      <hr>
      
  <h2>Purchase from</h2>
  
    <p><a href="http://www.amazon.com/exec/obidos/ASIN/0321350316/useitcomusablein/ref=nosim">Amazon.com</a></p>
  
  
    
    
      <ul class="no-bullet">
        <li><strong>UK/Europe</strong></li>
        
          <li><a href="http://www.amazon.co.uk/exec/obidos/ASIN/0321350316/useit-21/ref=nosim">Buy from Amazon.co.uk</a></li>
        
          <li><a href="http://www.amazon.de/gp/product/3827327636/">Web Usability (Deutsch) </a></li>
        
      </ul>
    
      <ul class="no-bullet">
        <li><strong>Other</strong></li>
        
          <li><a href="http://www.itcpub.co.kr/book/book_view1.php?h_code=h_9&amp;book_num=902">웹 유저빌러티 우선순위 </a></li>
        
          <li><a href="http://helion.pl/ksiazki/optymalizacja-funkcjonalnosci-serwisow-internetowych-jakob-nielsen-hoa-loranger,ofuser.htm">Optymalizacja funkcjonalności serwisów internetowych </a></li>
        
          <li><a href="http://www.amazon.fr/dp/2744023159/">Site Web: priorité à la simplicité </a></li>
        
          <li><a href="http://www.submarino.com.br/produto/5661063/livro-projetando-websites-com-usabilidade">Usabilidade na Web: Projetando Websites com Qualidade </a></li>
        
          <li><a href="http://www.amazon.com/dp/8441520925?tag=useitcomusablein">Usabilidad: Prioridad en el diseño Web </a></li>
        
          <li><a href="http://www.apogeonline.com/libri/88-503-2539-8">Web Usability 2.0: L&#39;usabilità che conta </a></li>
        
          <li><a href="http://www.williamspublishing.com/Books/978-5-8459-1222-0.php">Web-дизайн: удобство использования Web-сайтов </a></li>
        
          <li><a href="http://www.amazon.co.jp/gp/product/4844358928/">新ウェブ・ユーザビリティ </a></li>
        
          <li><a href="http://product.dangdang.com/product.aspx?product_id=9345032">网站优化:通过提高Web可用性构建用户满意的网站 </a></li>
        
          <li><a href="http://www.delightpress.com.tw/book.aspx?book_id=SKTM00005">設計好網站的黃金準則 </a></li>
        
      </ul>
    
  


    </div>
    <div class="related">
      


  <hr>



  <h3>Related Reports</h3>
  <ul class="no-bullet">
    
      <li><a href="../../reports/how-people-read-web-eyetracking-evidence/index.php">How People Read on the Web: The Eyetracking Evidence</a></li>
    
      <li><a href="../../reports/paper-prototyping-training-video/index.php">Paper Prototyping Training Video</a></li>
    
      <li><a href="../../reports/intranet-navigation-layout-and-text/index.php">Vol. 07: Navigation and Page Layout</a></li>
    
      <li><a href="../../reports/intranet-information-architecture-design-methods/index.php">Intranet Information Architecture Design Methods and Case Studies</a></li>
    
      <li><a href="../../reports/b2b-websites-usability/index.php">B2B Website Usability for Converting Users into Leads and Customers</a></li>
    
  </ul>



  <h3>Related Courses</h3>
   <ul class="no-bullet">
    
        <li><a href="../../courses/ux-basic-training/index.php">UX Basic Training</a></li>
    
        <li><a href="../../courses/human-mind/index.php">The Human Mind and Usability</a></li>
    
        <li><a href="../../courses/usability-testing/index.php">Usability Testing</a></li>
    
        <li><a href="../../courses/emerging-patterns-web-design/index.php">Emerging Patterns for Web Design</a></li>
    
        <li><a href="../../courses/web-page-design/index.php">Web Page UX Design</a></li>
    
</ul>



  <h3>Articles</h3>
  <ul class="no-bullet">
    
      <li><a href="../../articles/menu-design/index.php">Menu Design: Checklist of 15 UX Guidelines to Help Users </a></li>
    
      <li><a href="../../articles/flat-design/index.php">Flat Design: Its Origins, Its Problems, and Why Flat 2.0 Is Better for Users</a></li>
    
      <li><a href="../../articles/page-fold-manifesto/index.php">The Fold Manifesto: Why the Page Fold Still Matters</a></li>
    
      <li><a href="../../articles/scaling-user-interfaces/index.php">Scaling User Interfaces: An Information-Processing Approach to Multi-Device Design</a></li>
    
      <li><a href="../../articles/intranet-information-architecture-ia/index.php">Intranet Information Architecture (IA) Trends</a></li>
    
  </ul>


    </div>
  </div>

  <div class="small-12 medium-4 columns hide-for-small-down l-subsection">
    
  <h2>Purchase from</h2>
  
    <p><a href="http://www.amazon.com/exec/obidos/ASIN/0321350316/useitcomusablein/ref=nosim">Amazon.com</a></p>
  
  
    
    
      <ul class="no-bullet">
        <li><strong>UK/Europe</strong></li>
        
          <li><a href="http://www.amazon.co.uk/exec/obidos/ASIN/0321350316/useit-21/ref=nosim">Buy from Amazon.co.uk</a></li>
        
          <li><a href="http://www.amazon.de/gp/product/3827327636/">Web Usability (Deutsch) </a></li>
        
      </ul>
    
      <ul class="no-bullet">
        <li><strong>Other</strong></li>
        
          <li><a href="http://www.itcpub.co.kr/book/book_view1.php?h_code=h_9&amp;book_num=902">웹 유저빌러티 우선순위 </a></li>
        
          <li><a href="http://helion.pl/ksiazki/optymalizacja-funkcjonalnosci-serwisow-internetowych-jakob-nielsen-hoa-loranger,ofuser.htm">Optymalizacja funkcjonalności serwisów internetowych </a></li>
        
          <li><a href="http://www.amazon.fr/dp/2744023159/">Site Web: priorité à la simplicité </a></li>
        
          <li><a href="http://www.submarino.com.br/produto/5661063/livro-projetando-websites-com-usabilidade">Usabilidade na Web: Projetando Websites com Qualidade </a></li>
        
          <li><a href="http://www.amazon.com/dp/8441520925?tag=useitcomusablein">Usabilidad: Prioridad en el diseño Web </a></li>
        
          <li><a href="http://www.apogeonline.com/libri/88-503-2539-8">Web Usability 2.0: L&#39;usabilità che conta </a></li>
        
          <li><a href="http://www.williamspublishing.com/Books/978-5-8459-1222-0.php">Web-дизайн: удобство использования Web-сайтов </a></li>
        
          <li><a href="http://www.amazon.co.jp/gp/product/4844358928/">新ウェブ・ユーザビリティ </a></li>
        
          <li><a href="http://product.dangdang.com/product.aspx?product_id=9345032">网站优化:通过提高Web可用性构建用户满意的网站 </a></li>
        
          <li><a href="http://www.delightpress.com.tw/book.aspx?book_id=SKTM00005">設計好網站的黃金準則 </a></li>
        
      </ul>
    
  


  </div>

</div>

    </div>
    




<footer id="page-footer">
    <div class="row hide-for-print">
        <div class="column small-12 medium-6">
            <section class="footer-popular">
                
                <h1>Popular Topics</h1>
                <div style="float: left;  width: 15em; display: block;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/applications/index.php">Application Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/e-commerce/index.php">E-Commerce Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/eyetracking/index.php">Eyetracking </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/information-architecture/index.php">Information Architecture </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/intranets/index.php">Intranet Design </a></li>
	<li><a href="../../topic/interaction-design/index.php">Interaction Design </a></li>
</ul>
</div>

<div style="float: left;  width: 15em; display: block; list-style: none;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/mobile-and-tablet-design/index.php">Mobile and Tablet Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/credibility-persuasion/index.php">Persuasive Design</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/research-methods/index.php">Research Methods</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/user-testing/index.php">User Testing </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/web-usability/index.php">Web Usability </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/writing-web/index.php">Web Writing &amp; Content Strategy </a></li>
</ul>
</div>

<p>&nbsp;</p>

                
            </section>
        </div>

        <div class="column small-12 medium-6">
            <div class="row">
                <div class="column small-6">
                    <section class="footer-about">
                        <h1>Follow Us</h1>
                        <ul>
	<li><a href="../../articles/subscribe/index.php">Subscribe to our weekly UX newsletter</a>,&nbsp;Jakob Nielsen&#39;s Alertbox</li>
	<li>Visit Don Norman&#39;s website, <a href="http://www.jnd.org/"> www.jnd.org </a></li>
	<li>Visit Bruce Tognazzini&#39;s website, <a href="http://www.asktog.com/"> www.asktog.com </a></li>
	<li style="padding-top: 6px;"><a class="twitter-follow-button" data-show-count="false" data-size="large" href="https://twitter.com/nngroup"><img alt="Follow @nngroup on Twitter" height="20" src="https://media.nngroup.com/media/editor/2016/12/30/twitter-follow-button.png" style="width: 117px; height: 20px;" width="117" /></a></li>
	<li style="padding-top: 6px;"><img alt="" height="14" src="https://media.nngroup.com/media/editor/2016/07/26/feed-icon-14x14.png" style="vertical-align:text-top;width:14px;margin-right:.25em;" width="14" /><a href="../../feed/rss/index.php">Subscribe to our RSS feed</a></li>
</ul>

                    </section>
                </div>

                <div class="column small-6">
                    <section class="footer-about right-column">
                        <h1>About
</h1>
                        <ul class="no-bullet">
	<li><a href="../../about/contact/index.php">Contact information</a></li>
	<li><a href="../../about/index.php">About Us</a></li>
	<li><a href="../../about/why-nng/index.php">Why NN/g? </a></li>
	<li><a href="../../return-policy/index.php">Return Policy</a></li>
</ul>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="copyright column small-12">
            <p><a href="../../copyright-and-reprint-info/index.php">Copyright</a> &copy; 1998-2017 Nielsen Norman Group, All Rights Reserved.</p>
        </div>
    </div>
</footer>


    


        <script src="https://media.nngroup.com/static/vendor/fastclick/js/fastclick.min.js"></script>
        <script src="https://media.nngroup.com/static/vendor/foundation/js/foundation.min.js"></script>
        <script src="http://media.nngroup.com/static/js/app.js"></script>
        <script src="http://media.nngroup.com/static/js/scroll-to-fixed.js"></script><script>var externalLinks = document.querySelectorAll('a[href^="http"]');for (var i = externalLinks.length-1; i >= 0; i--) {externalLinks[i].addEventListener("click", function(event) { alert("External links are disabled for this test."); event.preventDefault(); }, false);}var mailLinks = document.querySelectorAll('a[href^="mailto"]');for (var i = mailLinks.length-1; i >= 0; i--) {mailLinks[i].addEventListener("click", function(event) { alert("Email links are disabled for this test."); event.preventDefault(); }, false);}var externalForms = document.querySelectorAll('form');for (var i = externalForms.length-1; i >= 0; i--) {externalForms[i].addEventListener("submit", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false); externalForms[i].addEventListener("click", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false);}</script>

        <script>
            
            $(function() {
                $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
                $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
            });
        </script>

        


        
        <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1072699960;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072699960/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
    </body>

<!-- Mirrored from www.nngroup.com/books/prioritizing-web-usability/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:02:35 GMT -->
</html>
