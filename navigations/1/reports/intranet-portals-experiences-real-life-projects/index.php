<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->





    
<!-- Mirrored from www.nngroup.com/reports/intranet-portals-experiences-real-life-projects/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:04:40 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1026.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"licenseKey":"1a30ef9931","errorBeacon":"bam.nr-data.net","applicationID":"2048992","transactionName":"NFxSMRBUX0EDUkIPDA0WdhAMVkVbDV8ZFAYTVkIRERtHWwdGRVwxBklfFxZxVEYDWFpIBAZN","beacon":"bam.nr-data.net","queueTime":0,"applicationTime":208,"agent":""}</script>
        <title>Intranet Portals UX Design | Nielsen Norman Group Research Report</title><meta property="og:title" content="Intranet Portals UX Design | Nielsen Norman Group Research Report" />
  
        
        <meta name="description" content="Case studies of 83 company portal projects and their user experience. Borrow from their strengths and avoid their weaknesses to improve the usability of your intranet portal.">
        <meta property="og:description" content="Case studies of 83 company portal projects and their user experience. Borrow from their strengths and avoid their weaknesses to improve the usability of your intranet portal." />
        
  
        
	
        
        <meta name="keywords" content="intranet portals, extranet portals, enterprise portal design, case study, reports, usability, design, designing corporate websites">
        

        

        <meta name="robots" content="noarchive">
        <meta name="viewport" content="width=device-width">

        <link rel="search" type="application/opensearchdescription+xml" title="NN/g Search" href="../../opensearch.xml" />
        <link rel="shortcut icon" href="http://media.nngroup.com/static/img/favicon.ico" />

        <link rel="stylesheet" href="https://media.nngroup.com/static/vendor/font-awesome/css/font-awesome.min.css">

        
        <link rel="stylesheet" href="https://media.nngroup.com/static/css/7232a960f1df.css" type="text/css" />
        <link rel="stylesheet" href="http://fast.fonts.com/t/1.css?apiType=css&amp;projectid=c5846260-9def-46ec-9407-0f1349bd2d64" type="text/css">

        <script src="https://media.nngroup.com/static/vendor/modernizr/js/modernizr.min.js"></script>

        <script type="text/javascript" src="https://media.nngroup.com/static/js/3283be5def74.js"></script>
        

        

        
        
        <!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=229034,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('http://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->


<!-- Google Analytics Tracking Code for http://nngroup.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23132681-1', 'nngroup.com');
  ga('require', 'linkid', 'https://www.nngroup.com/reports/intranet-portals-experiences-real-life-projects/linkid.js');
  ga('require', 'displayfeatures');

// Optimizely Universal Analytics Integration
 window.optimizely = window.optimizely || [];
 window.optimizely.push("activateUniversalAnalytics");

  ga('send', 'pageview');

</script>
<script>
/**
* Function to track a click on an outbound link in Google Analytics.
*/
function trackOutboundLink(label, url) {
    if(window.ga && ga.loaded) {
        ga('send', 'event', 'outbound', 'click', label, {'hitCallback': function(){document.location = url;}
        });
    } else {
        document.location = url;
    }
}
/**
* Function to track a click on a link in the article announcement area.
*/
$('#articleAnnouncement a').on('click', function(){
  ga('send', 'event', 'link', 'click', 'articleAnnouncement');
});
</script>

<script type="text/javascript">
 setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2421.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



<!-- Hotjar Tracking Code for http://nngroup.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:121605,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

        <script src="https://media.nngroup.com/static/js/analytics/track_outbound_link.js"></script>
        
        
        
    </head>
    <body class="location-reports report-detail">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
    <header id="page-header">
        <div id="cart" class="shopping-cart mobile-cart show-for-small-down"></div>
        <div class="row">
            <div id="masthead" class="masthead small-12 medium-7 column">

                <!-- The button to toggles menu on small screen -->

                <h1><a href="../../index.php"><img src="http://media.nngroup.com/static/img/logo-wordmark.png" width="74" height="30"> Nielsen <span class="norman">Norman</span> Group</a></h1>
                <p class="tagline show-for-medium-up">Evidence-Based User Experience Research, Training, and Consulting</p>

                <!-- The button to toggles form search on small screen -->


                <div style="clear: right;width:  100%;height: 1px;"></div><?php define('ROOT_PATH',$_SERVER["DOCUMENT_ROOT"] . '/apps/navigation_styles/navigations/1'); ?><?php include(ROOT_PATH)."/navigation.php"; ?>

                <!-- Drop down menu on small screen -->
                <div id="hideNav" class="show-for-small-down">
                  
                  <ul class="menu-content">
                    <li><a href="../../index.php">Home</a></li>
                    <li class="has-submenu">
                      <a>Training</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../training/index.php">Overview</a></li>
                        
<li id="nav-ux-conference"><a href="../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../online-seminars/index.php"><span>Online Seminars</span></a></li>

                      </ul>
                    </li>
                    <li class="has-submenu">
                      <a>Consulting</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
  <li><a href="../../consulting/index.php">Overview</a></li>
  
  <li><a href="../../consulting/benchmarking/index.php">Benchmarking</a></li>
  
  <li><a href="../../consulting/expert-design-review/index.php">Expert Design Review</a></li>
  
  <li><a href="../../consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
  
  <li><a href="../../consulting/usability-testing/index.php">Usability Testing </a></li>
  
  <li><a href="../../consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
  
  <li><a href="../../in-house-training/index.php">In-House Training</a></li>
</ul>

                    </li>

                    <li><a href="../index.php">Reports</a></li>
                    <li><a href="../../articles/index.php">Articles</a></li>
                    <li class="has-submenu"><a>About NN/G</a><i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../about/index.php">Overview</a></li>
                        <li><a href="../../people/index.php">People</a></li>
                        <li><a href="../../about/why-nng/index.php">Why NN/g?</a></li>
                        <li><a href="../../about/contact/index.php">Contact</a></li>
                        <li><a href="../../news/index.php">News</a></li>
                        <li><a href="../../about/history/index.php">History</a></li>
                        <li><a href="../../books/index.php">Books</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                </div>

                <hr class="header-divider show-for-small-only"/>


            </div>
       </div>

        <div id="hideNav">            <nav class="primary-nav small-12 column">
                <ul>
                    <li><a class="navitem" id="nav-home" href="../../index.php">Home</a></li>
                    <li><a class="navitem" id="nav-training" href="../../training/index.php">Training</a></li>
                    <li><a class="navitem" id="nav-consulting" href="../../consulting/index.php">Consulting</a></li>
                    <li><a class="navitem" id="nav-reports" href="../index.php">Reports</a></li>
                    <li><a class="navitem" id="nav-articles" href="../../articles/index.php">Articles</a></li>
                    <li><a class="navitem" id="nav-about" href="../../about/index.php">About NN/G</a></li>
                </ul>
            </nav>
            <nav class="secondary-nav small-12 medium-12 large-12 columns">
                
                
            </nav>
        </div>
    </header>
    <div class="l-content">
    


  


<div class="row">
    
    <section class="l-usercontent small-12 medium-9 medium-push-3 column report-sidebar">
        <div class="row">
            <div class="column report-body small-12 fix-report-width medium-8">
                <header>
                    <h1>Intranet Portals: UX Design Experience from Real-Life Projects</h1>
                    
                    
                    
                    5th Edition<br /><br />
                    

                    
                    <div class="topic-links">
                        Topics:
                        <ul>

  <li><a href="../topic/ux-design-process/index.php">Design Process</a></li>

  <li><a href="../topic/intranets/index.php">Intranets</a></li>

</ul>
                    </div>
                    
                </header>


                <div class="main-description">
                    
                    <img class="reportImage" src="https://media.nngroup.com/media/report_images/IntranetPortalsUXDesignExperience.jpg.150x235_q95_autocrop_crop_upscale.jpg" alt="Report PDF cover image" />
                    
                    <p>A good intranet portal provides easy access to all enterprise information, resources and tools. Intranet portals can also effectively consolidate applications, connect information, drive governance, change communication, and reduce fragmentation.</p>

<p>Comprehensive case studies show how the portal team can deliver what the organization and its users (your employees) need to be successful. Some of the most-praised features of intranet portals turn out not to be needed in most companies. For example, role-based personalization usually works better than individual personalization. And some compelling advice isn&rsquo;t about features at all, rather it&rsquo;s about process and governance.</p>

<p>This&nbsp;<strong>653-page&nbsp;</strong>report presents&nbsp;<strong>174 best practices </strong>based on <strong>83 case studies</strong>. Discussions and&nbsp;<strong>397 screenshot illustrations</strong>&nbsp;supplement our findings.</p>

<p>This report focuses on the design, user interface, use, usability, and adoption of an enterprise portal &mdash; that is, the user experience of intranets that look, feel and act like portals.</p>

<p><strong>Topics</strong></p>

<ul>
	<li>Portal characteristics</li>
	<li>Past and future portals</li>
	<li>Defining the portal</li>
	<li>Development best practices</li>
	<li>Governance models</li>
	<li>Department ownership and staffing</li>
	<li>Getting employees to use and like the portal</li>
	<li>Governance challenges</li>
	<li>Managing content</li>
	<li>The importance of the content management system (CMS)</li>
	<li>Centralized and decentralized ownership and authorship</li>
	<li>Templates, standards, and guidelines</li>
	<li>Communication and support</li>
	<li>User research, prototyping, card sorting, and focus groups</li>
	<li>The common portal homepage (or no homepage)</li>
	<li>Initial portal implementation strategy</li>
	<li>Sub-sites</li>
	<li>Department pages</li>
	<li>People pages</li>
	<li>Website design and structure</li>
	<li>Information architecture</li>
	<li>Moving from intranet IA to portal IA takes time</li>
	<li>Personalization and customization</li>
	<li>Application showcase</li>
	<li>Portal platforms</li>
	<li>Enterprise mobile</li>
	<li>Collaboration and social tools</li>
	<li>Security and single sign-on</li>
	<li>Search and filters</li>
	<li>Improving search</li>
	<li>Return on investment</li>
	<li>Methods and technologies used to realize the vision within an organization&#39;s framework</li>
</ul>

                </div>
                

                
                
                <div class="collapsable-content">
                    <p class="collapse-link collapsed force-desktop-collapsed">Full ToC and List of Participating Organizations</p>
                    <div class="collapsable force-desktop-collapsed collapsed">
                        <h2>Table of Contents</h2>

<ul>
	<li>Executive Summary</li>
	<li>Introduction to the 5th Edition</li>
	<li>Credits</li>
	<li>Defining the Portal</li>
	<li>Governance</li>
	<li>Managing Content</li>
	<li>Consulting the Users</li>
	<li>Site Design and Structure</li>
	<li>Personalization and Customization</li>
	<li>Homepage Gallery</li>
	<li>Applications</li>
	<li>The Portal Platform</li>
	<li>Enterprise Mobile</li>
	<li>Collaboration and Social Tools</li>
	<li>Security and Single Sign-On</li>
	<li>Search</li>
	<li>Return On Investment</li>
	<li>About the Authors</li>
	<li>Acknowledgments</li>
</ul>

<h2>Case Studies</h2>

<p>Studied for this edition of this report:</p>

<ul>
	<li>The Carle Foundation</li>
	<li>City of Olathe, Kansas</li>
	<li>Coca-Cola Enterprises Ltd.</li>
	<li>Consumer Financial Protection Bureau (CFPB)</li>
	<li>Department of Transport (Canada)</li>
	<li>FDC Solutions, Inc.</li>
	<li>Fraunhofer-Gesellschaft zur F&ouml;rderung der angewandten Forschung e.V.</li>
	<li>Fraunhofer Heinrich Hertz Institute</li>
	<li>Municipal Design and Survey Unitary Enterprise &ldquo;Minskinzhproekt&rdquo;</li>
	<li>National Archives and Records Administration (NARA)</li>
	<li>Northern Arizona University (NAU)</li>
	<li>Palm Beach County Board of County Commissioners</li>
	<li>Persistent Systems Limited</li>
	<li>Resource Data, Inc.</li>
	<li>Think Mutual Bank</li>
	<li>Yara International ASA</li>
</ul>

<p>Studied for previous editions of this report:</p>

<ul>
	<li>ABB</li>
	<li>Ahold</li>
	<li>Air France&ndash;KLM</li>
	<li>ANZ&nbsp;</li>
	<li>BEKK Consulting</li>
	<li>Boeing</li>
	<li>British Red Cross&nbsp;</li>
	<li>Build-A-Bear Workshop</li>
	<li>Burke Consortium</li>
	<li>Chevron Human Resources</li>
	<li>Cisco Systems&nbsp;</li>
	<li>City of Austin Fire Department</li>
	<li>City of New York</li>
	<li>Cognos</li>
	<li>Credit Suisse Financial Services</li>
	<li>The Defense Finance and Accounting Service (DFAS)</li>
	<li>Dell, Inc.</li>
	<li>Duke Energy&nbsp;</li>
	<li>Edens &amp; Avant</li>
	<li>EMBARQ</li>
	<li>Embry-Riddle Aeronautical University</li>
	<li>Erste Group Bank AG</li>
	<li>Eversheds</li>
	<li>EZ-Filing, Inc.&nbsp;</li>
	<li>IBM</li>
	<li>Idaho National Laboratory</li>
	<li>FIGG Engineering Group</li>
	<li>Fujitsu Siemens Computers</li>
	<li>Goodwin Procter, LLP&nbsp;</li>
	<li>Government of British Columbia Public Service Initiative&nbsp;</li>
	<li>HarperCollins</li>
	<li>HP Europe</li>
	<li>Huntington Bank&nbsp;</li>
	<li>Immaculata University&nbsp;</li>
	<li>Kadant Johnson Inc.&nbsp;</li>
	<li>KPMG UK</li>
	<li>Kaiser Permanente</li>
	<li>La Roche Ltd</li>
	<li>LM Glasfiber A/S</li>
	<li>Local PI and Riding and Sons Investigations&nbsp;</li>
	<li>Mars Symbioscience</li>
	<li>New Century Financial Corp.</li>
	<li>North Shore-LIJ Health System&nbsp;</li>
	<li>Northland Regional Council</li>
	<li>Ohio State University Medical Center (OSUMC)&nbsp;</li>
	<li>Pam Golding Properties</li>
	<li>PeaceHealth</li>
	<li>Pearson Australia Group&nbsp;</li>
	<li>Piper Jaffray &amp; Co.</li>
	<li>Point Loma Nazarene University</li>
	<li>Portland Public Schools</li>
	<li>RICS</li>
	<li>Saint Elizabeth Health Care&nbsp;</li>
	<li>SanDisk Corporation</li>
	<li>SAP AG</li>
	<li>South African Breweries (SAB)&nbsp;</li>
	<li>Sprint</li>
	<li>Strategic Logistical Alliance (SLA)</li>
	<li>Towers Perrin</li>
	<li>University of California, Irvine</li>
	<li>Vale Solu&ccedil;&otilde;es em Energia (VSE)&nbsp;</li>
	<li>Vattenfall</li>
	<li>Verizon</li>
	<li>Vertex, Inc.</li>
	<li>Wachovia</li>
	<li>Weber Associates</li>
	<li>Windana: Drug and Alcohol Recovery</li>
</ul>

                    </div>
                </div>
                
                <p><h2>Vendor-Independent Analysis</h2>

<p>Intranet portals are being pushed heavily by technology vendors, but the experience reported by the many portal managers we contacted is that technology only accounted for about a third of the problems they struggled with while implementing their portals. The rest were organizational issues and company politics.</p>

<p>This report presents a unique perspective on intranet portals: not that of a vendor trying to push a specific solution, but instead the user-experience point of view. What do portals mean to your employees and how can the portal team deliver what the organization needs? To find out, we investigated&nbsp;83 <em>real</em>&nbsp;portal projects, getting feedback from people who have managed the creation and updating of enterprise portals. <strong>Here</strong> <strong>they</strong> <strong>tell their stories about what works well.</strong></p>

<p>Some of the most-touted features of intranet portals turn out not to be needed in most companies: for example, role-based personalization usually works better than individual personalization. Similarly, one of the world&#39;s five-largest law firms discovered that its clients needed much simpler dealrooms than those promoted by most vendors of extranet portals.</p>

<p>We are vendor neutral. Some other companies that charge much higher prices for their reports receive large amounts of money from vendors. In contrast, we don&#39;t work with any portal vendors, nor do we edit out any of the harsh comments about them. We also don&#39;t have anything&nbsp;against&nbsp;the vendors, nor are we trying to sour anyone&rsquo;s perception of them just for the sake of doing so.</p>
</p>
            </div>

            <div class="small-12 medium-4 column report-sidebar">
                <section class="l-subsection">
                    
                    
                    <h1 class="purchase-title">Purchase</h1>
                    <p>Digital files for immediate download</p>
                    
                    
                    
                    <h3 class="cart-h3">Individual License</h3>
                    <div class="cart-block">
                        <span class="price">$348</span>
                        <a class="small pricing button" href="../../cart/add/intranetportalsindividual/index.php">Add to Cart</a>
                    </div>
                    
                    
                    
                    
                    <h3 class="cart-h3">Group License</h3>
                    <div class="cart-block">
                        <span class="price">$698</span>
                        <a class="small pricing button" href="../../cart/add/intranetportalsgroup/index.php">Add to Cart</a>
                    </div>
                    
                    

                    <!-- licensing block -->
                    
                    <div class="license-block">
                        <a class="lic-trigger">Which license should I purchase?</a>
                        <div id="lic-copy">
                            <h4>Individual License</h4>
                            <ul>
                                <li>Report or video will be used by only one person.</li>
                                <li>Cannot share the report or video with anyone else, or post it to any internal or external file server, website, or intranet.<br />
                                <a href="http://media.nngroup.com/static/license/individual_license.pdf" target="_blank">Individual License Terms</a></li>
                            </ul>

                            <h4>Group License</h4>
                            <ul>
                                <li>Report or video will be used by multiple people within your organization.</li>
                                <li>Can post the report or video on internal file servers or intranets and make it available to others within the organization.</li>
                                <li>Cannot make the report/video available to people not employed by your organization, the general public, or to post it to a publicly accessible website or file server.<br />
                                <a href="http://media.nngroup.com/static/license/group_license.pdf" target="_blank">Group License Terms</a></li>
                            </ul>
                        </div>
                    </div>
                    
                    <!-- end licensing block -->

                    
                </section>

                <div class="l-navrail">
                  


  
  


                </div>

                
            </div>

            <div class="column small-12 medium-8 related-report-training end">
                <hr>
                
                <section class="l-subsection collapsable-content">
                    <h1 class="hide-for-medium-up collapse-link">Related Reports and Training</h1>
                    <h1 class="hide-for-small-down">Related</h1>
                    <div class="collapsable related-content">
                        
                        <h2>Research Reports</h2>
                        <ul class="no-bullet">
                            
                            
                            <li><a href="../intranet-design-annual/index.php">2017 Intranet Design Annual</a></li>
                            
                            
                            
                            <li><a href="../intranet-usability-guidelines/index.php">Intranet Usability Guidelines: Findings from User Testing of 42 Intranets</a></li>
                            
                            
                            
                            <li><a href="../paper-prototyping-training-video/index.php">Paper Prototyping Training Video</a></li>
                            
                            
                            
                            <li><a href="../intranet-information-architecture-design-methods/index.php">Intranet Information Architecture Design Methods and Case Studies</a></li>
                            
                            
                            
                            <li><a href="../best-applications-2/index.php">Application Design Showcase: 2012</a></li>
                            
                            
                        </ul>
                        

                        
                        <h2>UX Conference Training Courses</h2>
                         <ul class="no-bullet">
    
        <li><a href="../../courses/journey-mapping/index.php">Journey Mapping to Understand Customer Needs</a></li>
    
        <li><a href="../../courses/facilitating-ux-workshops/index.php">Facilitating UX Workshops</a></li>
    
        <li><a href="../../courses/ux-deliverables/index.php">UX Deliverables</a></li>
    
        <li><a href="../../courses/personas/index.php">Personas: Turn User Data Into User-Centered Design</a></li>
    
        <li><a href="../../courses/lean-ux-and-agile/index.php">Lean UX and Agile</a></li>
    
</ul>
                        

                        
                        <h2>Online Seminars</h2>
                        <ul class="no-bullet">
                        
                            <li><a href="../../online-seminars/improving-intranet-content/index.php">Improving Intranet Content</a></li>
                        
                            <li><a href="../../online-seminars/design-thinking-agile/index.php">Design Thinking in Agile</a></li>
                        
                            <li><a href="../../online-seminars/intranet-users-find/index.php">Helping Intranet Users Find What They Need</a></li>
                        
                        </ul>
                        

                        
                        <h2>Articles</h2>
                        <ul class="no-bullet">
                            
                            <li><a href="../../articles/intranet-design/index.php">10 Best Intranets of 2017</a></li>
                            
                            <li><a href="../../articles/intranet-portals/index.php">Intranet Portals are the Hub of the Enterprise Universe</a></li>
                            
                            <li><a href="../../articles/intranet-information-architecture-ia/index.php">Intranet Information Architecture (IA) Trends</a></li>
                            
                            <li><a href="../../articles/intranet-social-features/index.php">Intranet Social Features</a></li>
                            
                            <li><a href="../../articles/narrative-biases/index.php">Narrative Biases: When Storytelling HURTS User Experience</a></li>
                            
                        </ul>
                        
                    </div>
                </section>
                
            </div>
        </div>
    </section>
    

    <section class="article-topics l-navrail medium-3 medium-pull-9 small-12 column">
        <div class="collapsable-content">
            <h1 class="hide-for-medium-up collapse-link">Browse by topic</h1>
            <h1 class="hide-for-small-down">Browse by topic</h1>
            <div class="collapsable">
                <ul class="no-bullet small-block-grid-2 medium-block-grid-1">
                    
                    <li><a href="../topic/accessibility/index.php">Accessibility</a></li>
                    
                    <li><a href="../topic/agile/index.php">Agile</a></li>
                    
                    <li><a href="../topic/applications/index.php">Application Design</a></li>
                    
                    <li><a href="../topic/b2b-websites/index.php">B2B Websites</a></li>
                    
                    <li><a href="../topic/content-strategy/index.php">Content Strategy</a></li>
                    
                    <li><a href="../topic/corporate-websites/index.php">Corporate Websites</a></li>
                    
                    <li><a href="../topic/ux-design-process/index.php">Design Process</a></li>
                    
                    <li><a href="../topic/e-commerce/index.php">E-commerce</a></li>
                    
                    <li><a href="../topic/email/index.php">Email</a></li>
                    
                    <li><a href="../topic/eyetracking/index.php">Eyetracking</a></li>
                    
                    <li><a href="../topic/information-architecture/index.php">Information Architecture</a></li>
                    
                    <li><a href="../topic/interaction-design/index.php">Interaction Design</a></li>
                    
                    <li><a href="../topic/international-users/index.php">International Users</a></li>
                    
                    <li><a href="../topic/intranets/index.php">Intranets</a></li>
                    
                    <li><a href="../topic/ux-management/index.php">Management</a></li>
                    
                    <li><a href="../topic/mobile-and-tablet-design/index.php">Mobile &amp; Tablet</a></li>
                    
                    <li><a href="../topic/navigation/index.php">Navigation</a></li>
                    
                    <li><a href="../topic/non-profit-websites/index.php">Non-Profit Websites</a></li>
                    
                    <li><a href="../topic/prototyping/index.php">Prototyping</a></li>
                    
                    <li><a href="../topic/research-methods/index.php">Research Methods</a></li>
                    
                    <li><a href="../topic/search/index.php">Search</a></li>
                    
                    <li><a href="../topic/social-media/index.php">Social Media</a></li>
                    
                    <li><a href="../topic/strategy/index.php">Strategy</a></li>
                    
                    <li><a href="../topic/user-testing/index.php">User Testing</a></li>
                    
                    <li><a href="../topic/web-usability/index.php">Web Usability</a></li>
                    
                    <li><a href="../topic/writing-web/index.php">Writing for the Web</a></li>
                    
                    <li><a href="../topic/young-users/index.php">Young Users</a></li>
                    
                    <li><a href="../free/index.php">Free Reports</a></li>
                </ul>
            </div>
        </div>

      
        
      
    </section>
</div>

<div id="pricingModal" class="reveal-modal medium l-subsection">
    <h1>Which License?</h1>

    <h2>Individual</h2>
    <p>
    Purchase an individual license if the report or video will only be used by one person.
    </p>

    <p>
    Customers who choose an individual license are not authorized to share the report or video with anyone else, or post it to any internal or external file server, website, or intranet.
    <a href="http://media.nngroup.com/static/license/individual_license.pdf" target="_blank">Individual License Terms</a>
    </p>

    <h2>Group</h2>

    <p>
    Purchase a group license if the report or video will be used by multiple people within your organization.
    </p>

    <p>
    Customers who choose a group license are authorized to post the report or video on internal file servers or intranets and make it available to others within the organization. The group license does not grant permission to make the report/video available to people not employed by your organization, the general public, or to post it to a publicly accessible website or file server. <a href="http://media.nngroup.com/static/license/group_license.pdf" target="_blank">Group License Terms</a>
    </p>
    <a class="close-reveal-modal">&#215;</a>
</div>

<div id="individual-added" data-reveal class="reveal-modal x-large l-subsection">
    <h2>You’ve just added the following to your cart:</h2>
    <h3>Intranet Portals: UX Design Experience from Real-Life Projects</h3>
    <p>Individual License</p>
    <p>$348</p>
    <div class="buttons">
        <button class="button small close">Continue Browsing</button>
        <a href="../../cart/index.php" class="button small">Checkout Now</a>
    </div>
    <a class="close-reveal-modal">&#215;</a>
</div>

<div id="group-added" data-reveal class="reveal-modal x-large l-subsection">
    <h2>You’ve just added the following to your cart:</h2>
    <h3>Intranet Portals: UX Design Experience from Real-Life Projects</h3>
    <p>Group License</p>
    <p>$698</p>
    <div class="buttons">
        <button class="button small close">Continue Browsing</button>
        <a href="../../cart/index.php" class="button small">Checkout Now</a>
    </div>
    <a class="close-reveal-modal">&#215;</a>
</div>

    </div>
    




<footer id="page-footer">
    <div class="row hide-for-print">
        <div class="column small-12 medium-6">
            <section class="footer-popular">
                
                <h1>Popular Topics</h1>
                <div style="float: left;  width: 15em; display: block;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/applications/index.php">Application Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/e-commerce/index.php">E-Commerce Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/eyetracking/index.php">Eyetracking </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/information-architecture/index.php">Information Architecture </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/intranets/index.php">Intranet Design </a></li>
	<li><a href="../../topic/interaction-design/index.php">Interaction Design </a></li>
</ul>
</div>

<div style="float: left;  width: 15em; display: block; list-style: none;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/mobile-and-tablet-design/index.php">Mobile and Tablet Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/credibility-persuasion/index.php">Persuasive Design</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/research-methods/index.php">Research Methods</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/user-testing/index.php">User Testing </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/web-usability/index.php">Web Usability </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/writing-web/index.php">Web Writing &amp; Content Strategy </a></li>
</ul>
</div>

<p>&nbsp;</p>

                
            </section>
        </div>

        <div class="column small-12 medium-6">
            <div class="row">
                <div class="column small-6">
                    <section class="footer-about">
                        <h1>Follow Us</h1>
                        <ul>
	<li><a href="../../articles/subscribe/index.php">Subscribe to our weekly UX newsletter</a>,&nbsp;Jakob Nielsen&#39;s Alertbox</li>
	<li>Visit Don Norman&#39;s website, <a href="http://www.jnd.org/"> www.jnd.org </a></li>
	<li>Visit Bruce Tognazzini&#39;s website, <a href="http://www.asktog.com/"> www.asktog.com </a></li>
	<li style="padding-top: 6px;"><a class="twitter-follow-button" data-show-count="false" data-size="large" href="https://twitter.com/nngroup"><img alt="Follow @nngroup on Twitter" height="20" src="https://media.nngroup.com/media/editor/2016/12/30/twitter-follow-button.png" style="width: 117px; height: 20px;" width="117" /></a></li>
	<li style="padding-top: 6px;"><img alt="" height="14" src="https://media.nngroup.com/media/editor/2016/07/26/feed-icon-14x14.png" style="vertical-align:text-top;width:14px;margin-right:.25em;" width="14" /><a href="../../feed/rss/index.php">Subscribe to our RSS feed</a></li>
</ul>

                    </section>
                </div>

                <div class="column small-6">
                    <section class="footer-about right-column">
                        <h1>About
</h1>
                        <ul class="no-bullet">
	<li><a href="../../about/contact/index.php">Contact information</a></li>
	<li><a href="../../about/index.php">About Us</a></li>
	<li><a href="../../about/why-nng/index.php">Why NN/g? </a></li>
	<li><a href="../../return-policy/index.php">Return Policy</a></li>
</ul>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="copyright column small-12">
            <p><a href="../../copyright-and-reprint-info/index.php">Copyright</a> &copy; 1998-2017 Nielsen Norman Group, All Rights Reserved.</p>
        </div>
    </div>
</footer>


    


        <script src="https://media.nngroup.com/static/vendor/fastclick/js/fastclick.min.js"></script>
        <script src="https://media.nngroup.com/static/vendor/foundation/js/foundation.min.js"></script>
        <script src="http://media.nngroup.com/static/js/app.js"></script>
        <script src="http://media.nngroup.com/static/js/scroll-to-fixed.js"></script><script>var externalLinks = document.querySelectorAll('a[href^="http"]');for (var i = externalLinks.length-1; i >= 0; i--) {externalLinks[i].addEventListener("click", function(event) { alert("External links are disabled for this test."); event.preventDefault(); }, false);}var mailLinks = document.querySelectorAll('a[href^="mailto"]');for (var i = mailLinks.length-1; i >= 0; i--) {mailLinks[i].addEventListener("click", function(event) { alert("Email links are disabled for this test."); event.preventDefault(); }, false);}var externalForms = document.querySelectorAll('form');for (var i = externalForms.length-1; i >= 0; i--) {externalForms[i].addEventListener("submit", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false); externalForms[i].addEventListener("click", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false);}</script>

        <script>
            
            $(function() {
                $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
                $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
            });
        </script>

        
    

    <script>
    $(function() {
        $('#individual-added .close').on("click", function() {
            $('#individual-added').foundation('reveal', 'close');
        });
        
        $('#individual-added').foundation('reveal', 'open');
        
        $('#group-added .close').on("click", function() {
            $('#group-added').foundation('reveal', 'close');
        });
        
        $('#group-added').foundation('reveal', 'open');
        
    });
    </script>


        
        <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1072699960;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072699960/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
    </body>

<!-- Mirrored from www.nngroup.com/reports/intranet-portals-experiences-real-life-projects/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:04:40 GMT -->
</html>
