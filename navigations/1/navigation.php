<!doctype html>
<html id="doc" class="no-js">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script>
        var doc = document.getElementById('doc');
        doc.removeAttribute('class', 'no-js');
        doc.setAttribute('class', 'js');
    </script>
    <script src="/apps/navigation_styles/app/app.js"></script>
    <script>
        window.onclick = function() { addCoordinates() };
        window.addEventListener("touchstart", function() { addCoordinates() }, false);
    </script>
    <link href="/apps/navigation_styles/navigations/1/assets/style.css" rel="stylesheet"/>
</head>
<body id="page" class="not-active">
<header id="header">
    <h1 id="site-title"><a href="">Logo</a></h1>
    <nav class="off-canvas-nav-links">
        <ul>
            <li class="menu-item"><a class='menu-button' href="#menu">Menu</a></li>
            <li class="sidebar-item"><a class='sidebar-button' id='sidebar-button' href="#sidebar" >Extra</a></li>
        </ul>
    </nav>
</header>
<nav id="menu" role="navigation">
    <ul id="nav">
        <li><a href="/apps/navigation_styles/navigations/1/index.php">Home</a></li>
        <li><a href="/apps/navigation_styles/navigations/1/reports/index.php">Reports</a></li>
        <li><a href="/apps/navigation_styles/navigations/1/articles/index.php">Articles</a></li>
        <li><span class="subnav-toggle">+ </span><a href="/apps/navigation_styles/navigations/1/training/index.php">Training</a>
            <ul>
                <li><a href="/apps/navigation_styles/navigations/1/ux-conference/index.php">UX Conference</a></li>
                <li><a href="/apps/navigation_styles/navigations/1/ux-certification/index.php">UX Certification</a></li>
                <li><a href="/apps/navigation_styles/navigations/1/in-house-training/index.php">In-House Training</a></li>
                <li><a href="/apps/navigation_styles/navigations/1/online-seminars/index.php">Online Seminars</a></li>
            </ul>
        </li>
        <li><span class="subnav-toggle">+ </span><a href="/apps/navigation_styles/navigations/1/consulting/index.php">Consulting</a>
            <ul>
                <li><a href="/apps/navigation_styles/navigations/1/consulting/benchmarking/index.php">Benchmarking</a></li>
                <li><a href="/apps/navigation_styles/navigations/1/consulting/expert-design-review/index.php">Expert Design Review</a></li>
                <li><a href="/apps/navigation_styles/navigations/1/consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
                <li><a href="/apps/navigation_styles/navigations/1/consulting/usability-testing/index.php">Usability Testing </a></li>
                <li><a href="/apps/navigation_styles/navigations/1/consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
                <li><a href="/apps/navigation_styles/navigations/1/in-house-training/index.php">In-House Training</a></li>
            </ul>
        </li>
        <li><span class="subnav-toggle">+ </span><a href="/apps/navigation_styles/navigations/1/about/index.php">About NN/G</a>
            <ul>
                <li><a href="/apps/navigation_styles/navigations/1/people/index.php">People</a></li>
                <li><a href="/apps/navigation_styles/navigations/1/about/why-nng/index.php">Why NN/g?</a></li>
                <li><a href="/apps/navigation_styles/navigations/1/about/contact/index.php">Contact</a></li>
                <li><a href="/apps/navigation_styles/navigations/1/news/index.php">News</a></li>
                <li><a href="/apps/navigation_styles/navigations/1/about/history/index.php">History</a></li>
                <li><a href="/apps/navigation_styles/navigations/1/books/index.php">Books</a></li>
            </ul>
        </li>
    </ul>
</nav>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="/apps/navigation_styles/navigations/1/assets/scripts.js"></script>
</body>
</html>