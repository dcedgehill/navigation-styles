var changeActive = function() {
    var page = document.getElementById("page");
    if (page.getAttribute("class") === "not-active") {
        page.setAttribute("class", "active-sidebar");
    } else if (page.getAttribute("class") === "active-sidebar") {
        page.setAttribute("class", "not-active");
    }
};
$(document).ready(function() {
    var sidebar_button = document.getElementById("sidebar-button");
    sidebar_button.onclick = function(event) {
        changeActive();
        event.preventDefault();
    };

    $('.subnav-toggle').click(function() {
        $(this).toggleClass('active-parent')
            .parent().find("ul").toggleClass("active-subnav");
    });
});

$(window).resize(function() {
    var page = document.getElementById("page");
    page.setAttribute("class", "not-active");
});