


<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from www.nngroup.com/training/vancouver/print/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:10:27 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
  <meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1026.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"licenseKey":"1a30ef9931","queueTime":7,"beacon":"bam.nr-data.net","errorBeacon":"bam.nr-data.net","applicationID":"2048992","transactionName":"NFxSMRBUX0EDUkIPDA0WdhAMVkVbDV8ZAxUGV0QWTENYVxVCDCMVBldEIQdBUFsOYUQPDRdYUgkHG1ZXFg==","applicationTime":261,"agent":""}</script>
  <meta name="robots" content="noindex, nofollow">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>NN/g UX Conference Vancouver</title>
  
    <link rel="stylesheet" href="https://media.nngroup.com/static/css/event_agenda.css" type="text/css">
  
  
</head>
<body>

  <header>
    <div class="logo">
      <img src="https://media.nngroup.com/static/img/logo-wordmark.svg">
    </div>
    <div class="heading">
      <h1>UX Conference Vancouver<br>August 13 &ndash; 18, 2017</h1>
    </div>
  </header>
  <table class="agenda">
    <caption>You may attend one course per day.  Each course runs from 9am - 5pm</caption>
    <thead>
    <tr>
    
      <th>Sunday<br><br>Aug 13, 2017</th>
    
      <th>Monday<br><br>Aug 14, 2017</th>
    
      <th>Tuesday<br><br>Aug 15, 2017</th>
    
      <th>Wednesday<br><br>Aug 16, 2017</th>
    
      <th>Thursday<br><br>Aug 17, 2017</th>
    
      <th>Friday<br><br>Aug 18, 2017</th>
    
    </tr>
    </thead>
    <tbody>
    
      <tr>
        
          
            
            <th>Sunday<br><br>Aug 13, 2017</th>
            
          
        
          
            
            <td>UX Basic Training</td>
            
          
        
          
            
            <td>Becoming a UX Strategist</td>
            
          
        
          
            
            <td>User Interface Principles Every Designer Must Know</td>
            
          
        
          
            
            <td>Usability Testing</td>
            
          
        
          
            
            <td>Generating Big Ideas with Design Thinking</td>
            
          
        
          
            
            <td>Analytics and User Experience</td>
            
          
        
      </tr>
    
      <tr>
        
          
            
            <th>Monday<br><br>Aug 14, 2017</th>
            
          
        
          
            
            <td>The UX VP/Director</td>
            
          
        
          
            
            <td>Emerging Patterns for Web Design</td>
            
          
        
          
            
            <td>Working Effectively in Cross-Functional Teams</td>
            
          
        
          
            
            <td>Persuasive Web Design</td>
            
          
        
          
            
            <td>Measuring User Experience</td>
            
          
        
          
            
            <td>Journey Mapping to Understand Customer Needs</td>
            
          
        
      </tr>
    
      <tr>
        
          
            
            <th>Tuesday<br><br>Aug 15, 2017</th>
            
          
        
          
            
            <td class="empty"></td>
            
          
        
          
            
            <td>Facilitating UX Workshops</td>
            
          
        
          
            
            <td>Mobile User Experience</td>
            
          
        
          
            
            <td>Visual Design for Mobile and Tablet: Day 1</td>
            
          
        
          
            
            <td>Visual Design for Mobile and Tablet: Day 2</td>
            
          
        
          
            
            <td>The Human Mind and Usability</td>
            
          
        
      </tr>
    
      <tr>
        
          
            
            <th>Wednesday<br><br>Aug 16, 2017</th>
            
          
        
          
            
            <td class="empty"></td>
            
          
        
          
            
            <td class="empty"></td>
            
          
        
          
            
            <td class="empty"></td>
            
          
        
          
            
            <td>Communicating Design</td>
            
          
        
          
            
            <td class="empty"></td>
            
          
        
          
            
            <td class="empty"></td>
            
          
        
      </tr>
    
    </tbody>
  </table>
  

  <section class="info location">
    <div>
    <h2>location</h2>
    <p>
      Pan Pacific Vancouver<br>
      Canada Place Vancouver<br>
      Suite 300-999<br>
      British Columbia V6C 3B5<br>
      +1 604 662-8111
    </p>
    </div>
  </section>
  <section class="info daily-schedule">
    <div>
    <h2>daily schedule</h2>
    <p>
      8 AM: Registration<br>
      9 AM: Classes begin<br>
      10:20 AM: Morning break<br>
      12:20 PM: Lunch<br>
      3 PM: Afternoon break<br>
      5 PM: Classes end
    </p>
    </div>
  </section>
  <section class="info organizer">
    <div>
    <h2>event questions</h2>
    <p>
      Nielsen Norman Group<br>
      events@nngroup.com<br>
      +1 (415) 685-4230 ext. 502<br>
      www.nngroup.com/training/
    </p>
    </div>
  </section>

</body>

<!-- Mirrored from www.nngroup.com/training/vancouver/print/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:10:27 GMT -->
</html>
