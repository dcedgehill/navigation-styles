<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->





    
<!-- Mirrored from www.nngroup.com/training/course/1647/ux-basic-training/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:04:49 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1026.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"licenseKey":"1a30ef9931","errorBeacon":"bam.nr-data.net","applicationID":"2048992","transactionName":"NFxSMRBUX0EDUkIPDA0WdhAMVkVbDV8ZAxUGV0QWTENYVxVCDCMVBldEJg1AQ0EHdVMSAgpVHgIHQQ==","beacon":"bam.nr-data.net","queueTime":1,"applicationTime":524,"agent":""}</script>
        <title>User Experience (UX) Basic Training | Nielsen Norman Group Full-Day Course</title><meta property="og:title" content="User Experience (UX) Basic Training | Nielsen Norman Group Full-Day Course" />
  
        
        <meta name="description" content="Full-day course taught at Nielsen Norman Group&#39;s UX Conferences. Learn user experience (UX) terminology and processes and ways to promote UX within your organization.">
        <meta property="og:description" content="Full-day course taught at Nielsen Norman Group&#39;s UX Conferences. Learn user experience (UX) terminology and processes and ways to promote UX within your organization." />
        
  
        
	
        

        

        <meta name="robots" content="noarchive">
        <meta name="viewport" content="width=device-width">

        <link rel="search" type="application/opensearchdescription+xml" title="NN/g Search" href="../../../../opensearch.xml" />
        <link rel="shortcut icon" href="http://media.nngroup.com/static/img/favicon.ico" />

        <link rel="stylesheet" href="https://media.nngroup.com/static/vendor/font-awesome/css/font-awesome.min.css">

        
        <link rel="stylesheet" href="https://media.nngroup.com/static/css/7232a960f1df.css" type="text/css" />
        <link rel="stylesheet" href="http://fast.fonts.com/t/1.css?apiType=css&amp;projectid=c5846260-9def-46ec-9407-0f1349bd2d64" type="text/css">

        <script src="https://media.nngroup.com/static/vendor/modernizr/js/modernizr.min.js"></script>

        <script type="text/javascript" src="https://media.nngroup.com/static/js/3283be5def74.js"></script>
        

        
    <link rel="canonical" href="../../../../courses/ux-basic-training/index.php" />


        
        
        <!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=229034,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('http://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->


<!-- Google Analytics Tracking Code for http://nngroup.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23132681-1', 'nngroup.com');
  ga('require', 'linkid', 'https://www.nngroup.com/training/course/1647/ux-basic-training/linkid.js');
  ga('require', 'displayfeatures');

// Optimizely Universal Analytics Integration
 window.optimizely = window.optimizely || [];
 window.optimizely.push("activateUniversalAnalytics");

  ga('send', 'pageview');

</script>
<script>
/**
* Function to track a click on an outbound link in Google Analytics.
*/
function trackOutboundLink(label, url) {
    if(window.ga && ga.loaded) {
        ga('send', 'event', 'outbound', 'click', label, {'hitCallback': function(){document.location = url;}
        });
    } else {
        document.location = url;
    }
}
/**
* Function to track a click on a link in the article announcement area.
*/
$('#articleAnnouncement a').on('click', function(){
  ga('send', 'event', 'link', 'click', 'articleAnnouncement');
});
</script>

<script type="text/javascript">
 setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2421.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



<!-- Hotjar Tracking Code for http://nngroup.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:121605,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

        <script src="https://media.nngroup.com/static/js/analytics/track_outbound_link.js"></script>
        
        
        
    </head>
    <body class="location-training location-event-course-detail course-detail">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
    <header id="page-header">
        <div id="cart" class="shopping-cart mobile-cart show-for-small-down"></div>
        <div class="row">
            <div id="masthead" class="masthead small-12 medium-7 column">

                <!-- The button to toggles menu on small screen -->

                <h1><a href="../../../../index.php"><img src="http://media.nngroup.com/static/img/logo-wordmark.png" width="74" height="30"> Nielsen <span class="norman">Norman</span> Group</a></h1>
                <p class="tagline show-for-medium-up">Evidence-Based User Experience Research, Training, and Consulting</p>

                <!-- The button to toggles form search on small screen -->


                <div style="clear: right;width:  100%;height: 1px;"></div><?php define('ROOT_PATH',$_SERVER["DOCUMENT_ROOT"] . '/apps/navigation_styles/navigations/1'); ?><?php include(ROOT_PATH)."/navigation.php"; ?>

                <!-- Drop down menu on small screen -->
                <div id="hideNav" class="show-for-small-down">
                  
                  <ul class="menu-content">
                    <li><a href="../../../../index.php">Home</a></li>
                    <li class="has-submenu">
                      <a>Training</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../../index.php">Overview</a></li>
                        
<li id="nav-ux-conference"><a href="../../../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../../../online-seminars/index.php"><span>Online Seminars</span></a></li>

                      </ul>
                    </li>
                    <li class="has-submenu">
                      <a>Consulting</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
  <li><a href="../../../../consulting/index.php">Overview</a></li>
  
  <li><a href="../../../../consulting/benchmarking/index.php">Benchmarking</a></li>
  
  <li><a href="../../../../consulting/expert-design-review/index.php">Expert Design Review</a></li>
  
  <li><a href="../../../../consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
  
  <li><a href="../../../../consulting/usability-testing/index.php">Usability Testing </a></li>
  
  <li><a href="../../../../consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
  
  <li><a href="../../../../in-house-training/index.php">In-House Training</a></li>
</ul>

                    </li>

                    <li><a href="../../../../reports/index.php">Reports</a></li>
                    <li><a href="../../../../articles/index.php">Articles</a></li>
                    <li class="has-submenu"><a>About NN/G</a><i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../../../about/index.php">Overview</a></li>
                        <li><a href="../../../../people/index.php">People</a></li>
                        <li><a href="../../../../about/why-nng/index.php">Why NN/g?</a></li>
                        <li><a href="../../../../about/contact/index.php">Contact</a></li>
                        <li><a href="../../../../news/index.php">News</a></li>
                        <li><a href="../../../../about/history/index.php">History</a></li>
                        <li><a href="../../../../books/index.php">Books</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                </div>

                <hr class="header-divider show-for-small-only"/>


            </div>
       </div>

        <div id="hideNav">            <nav class="primary-nav small-12 column">
                <ul>
                    <li><a class="navitem" id="nav-home" href="../../../../index.php">Home</a></li>
                    <li><a class="navitem" id="nav-training" href="../../../index.php">Training</a></li>
                    <li><a class="navitem" id="nav-consulting" href="../../../../consulting/index.php">Consulting</a></li>
                    <li><a class="navitem" id="nav-reports" href="../../../../reports/index.php">Reports</a></li>
                    <li><a class="navitem" id="nav-articles" href="../../../../articles/index.php">Articles</a></li>
                    <li><a class="navitem" id="nav-about" href="../../../../about/index.php">About NN/G</a></li>
                </ul>
            </nav>
            <nav class="secondary-nav small-12 medium-12 large-12 columns">
                
  <ul class="inline-list">
  
<li id="nav-ux-conference"><a href="../../../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../../../online-seminars/index.php"><span>Online Seminars</span></a></li>

</ul>


            </nav>
        </div>
    </header>
    <div class="l-content">
    

<article class="course-detail">
    <div class="row">
        <p class="small-12 column">
            
            Full day training course
            
            offered at <a href="../../../austin/index.php">The UX Conference Austin</a>
        </p>
    </div>
    <div class="row">
        <div class="l-section-intro small-12 medium-8 column">
            <header>
                <h1>UX Basic Training</h1>
                <h2>Be an effective UX professional: Know the lingo and sell the process</h2>
            </header>
        </div>

        <div class="small-12 column show-for-small-down l-subsection">
            
            <h2 class="course-dates">Course Date: June 19, 2017</h2>
            

            
            <a class="button course-register medium register-button" href="../../../register/92/index.php">
                Register
            </a>
            
            <div style="clear: both"></div>
            <hr style="border-color: #eee"/>
        </div>
    </div>


    
    <div class="row">
        <div class="l-course-content small-12 medium-8 column">
            <div class="l-usercontent mainCourseContent">
                <p>Focusing on user experience (UX) can differentiate a company from its competitors. In one day, we give you a thorough overview of the user experience field and its many components. You will learn the importance of a user-centered design process and the benefit of incorporating UX activities at every stage of a project.</p>

<p>This course is ideal for people who are beginning to work in user experience or have some knowledge of the field. It clarifies what UX professionals do, and need to do, to create good, usable designs.</p>

<blockquote>&quot;As a newbie in the UX field, the UX basic training course had both breadth and depth of the specialty making it a great place for me to start.&quot;</blockquote>

<p>Melanie Langmead<br />
CapTech Consulting</p>

            </div>
            
            <div class="l-usercontent mainCourseContent collapsable-content">

                <h1 class="collapse-link show-for-small-down">Topics Covered</h1>
                <h2 class="show-for-medium-up">Topics Covered</h2>

                <div class="collapsable">
                    <ul style="margin-left:40px">
	<li>Business value of UX design
	<ul>
		<li>Learn tips for promoting UX as a competitive advantage</li>
		<li>Key Performance Indicators (KPIs) and usability metrics</li>
		<li>Return on Investment (ROI)</li>
		<li>Learn to sell UX as a competitive advantage</li>
	</ul>
	</li>
	<li>Foundation of user experience
	<ul>
		<li>Defining key terms and understanding the relationships among them, such as: <em>user experience (UX), usability, utility, usefulness, user centered design (UCD), human factors, human-computer interaction (HCI), user interface (UI), graphical user interface (GUI), natural user interface (NUI), eyetracking gaze plot and heat maps, cross-channel design, responsive web design, emotional design, information architecture (IA), visual/graphic design, interaction design (IxD), search engine optimization (SEO), content strategy, accessibility,</em> and more</li>
		<li>Design products around people, instead of teaching people how to use products</li>
		<li>What it means to do user experience design</li>
		<li>Usability, usefulness, utility, and user experience</li>
		<li>How we measure usability</li>
		<li>Usability and user satisfaction</li>
	</ul>
	</li>
	<li>Understanding people in order to improve our design
	<ul>
		<li>Age and gender differences</li>
		<li>Cognitive aspects of user behavior</li>
		<li>Designing for the initial experience compared to supporting skilled performance</li>
		<li>Growth in user expertise over time and learning curves</li>
		<li>Supporting people with disabilities</li>
		<li>Create personas to focus the team on specific audience segments</li>
	</ul>
	</li>
	<li>What you can do to improve the UX
	<ul>
		<li>Form multidisciplinary project teams</li>
		<li>Know who you&#39;re designing for</li>
		<li>Follow design standards</li>
		<li>Test your design early and often</li>
	</ul>
	</li>
	<li>Know when to apply which research methods and how to use the data to improve design
	<ul>
		<li>Conducting studies in usability labs</li>
		<li>Testing your design remotely with people in their own home or office</li>
		<li>Eyetracking costs and benefits</li>
		<li>Field studies, site visits, and ethnography to uncover how your product is used &quot;in the wild&quot;</li>
		<li>Surveys and focus groups to gather preference data</li>
		<li>Customer satisfaction scores</li>
		<li>A/B and multivariate testing&nbsp;</li>
		<li>What to measure with site analytics</li>
		<li>Reading the value of site metrics&nbsp;</li>
		<li>Content strategy</li>
		<li>Determining navigation through card sorting or tree testing</li>
		<li>Qualitative vs. quantitative methods</li>
		<li>Outsourcing or doing it yourself</li>
	</ul>
	</li>
	<li>Starting designs off right
	<ul>
		<li>Focus on all levels of user interface from content to visual design</li>
		<li>Follow usability guidelines and best practices</li>
		<li>Pattern libraries</li>
		<li>Platform conventions</li>
	</ul>
	</li>
	<li>History, trends, and challenges for UX
	<ul>
		<li>Adaptive content and responsive web design</li>
		<li>Evaluating UX research, articles, and blogs</li>
		<li>User and system control</li>
	</ul>
	</li>
	<li>Integrating usability with the project lifecycle
	<ul>
		<li>Traditional development processes and UX</li>
		<li>Agile methods and UX</li>
		<li>Creating time for research and iterative design</li>
		<li>Involve developers early</li>
		<li>Durability of usability guidelines</li>
		<li>Iterative design and prototyping</li>
	</ul>
	</li>
	<li>Understand the purpose and roles of UX professionals throughout a project lifecycle
	<ul>
		<li>Who should conduct research: Designers or dedicated experts?</li>
		<li>How to evaluate consultant quality</li>
		<li>Building a UX team</li>
		<li>Fitting UX within your organization</li>
		<li>Being effective as the sole UX person in a company or group</li>
		<li>Transitioning into a UX role</li>
		<li>UX degrees and certifications</li>
	</ul>
	</li>
	<li>Stages of organizational UX maturity
	<ul>
		<li>Assess your organization&rsquo;s commitment to UX</li>
		<li>What to expect as your organization goes through the next step of UX maturity</li>
		<li>Choosing high-impact projects to drive personal and organizational growth</li>
	</ul>
	</li>
</ul>

                </div>

            </div>
            

            
            <div class="l-usercontent mainCourseContent collapsable-content">

                <h1 class="collapse-link show-for-small-down">Free Material With Course Attendance</h1>
                <h2 class="show-for-medium-up">Free Material With Course Attendance</h2>

                <div class="collapsable">
                    <p>Report: <a href="../../../../reports/how-to-conduct-usability-studies/index.php">How to Conduct Usability Studies</a></p>

<p>Get 230 tips for planning and conducting user research.</p>

<p>Report:&nbsp;<a href="../../../../reports/usability-return-on-investment-roi/index.php">Return on Investment (ROI) for Usability</a></p>

<p>This 212-page report features best practices for increasing key performance indicators and estimating usability budgets through 72 richly illustrated case studies.</p>

                </div>

            </div>
            

            
            <div class="l-usercontent mainCourseContent collapsable-content">

                <h1 class="collapse-link show-for-small-down">Format</h1>
                <h2 class="show-for-medium-up">Format</h2>

                <div class="collapsable">
                    <p>The basis of the course is a lecture format with a couple of group exercises to reinforce the learned principles and guidelines.</p>

<p>The course also includes:</p>

<ul style="margin-left:40px">
	<li>Findings from our own usability studies</li>
	<li>Videos from usability testing of people&#39;s behavior in response to a design</li>
	<li>Screenshots of designs that work and don&rsquo;t work, and why</li>
	<li>Opportunities to ask questions and get answers</li>
</ul>

                </div>

            </div>
            

            

            

            

            

            
                



<div class="row certification-info">
  <section class="l-subsection collapsable-content small-12 medium-8 columns">
    <h1 class="collapse-link show-for-small-down">UX Certification Credit</h1>
    <h2 class="hide-for-small-down">UX Certification Credit</h2>
    <div class="collapsable">
      <p>
      Attending this course and passing the exam earns <strong>1 UX Certification credit</strong>, which also counts towards the optional <a href="../../../../ux-certification/interaction-design-specialty/index.php">Interaction Design Specialty</a>.
      </p>

      

      <p>
      Learn more about NN/g's <a href="../../../../ux-certification/index.php">UX Certification</a> Program.
      </p>
    </div>
  </section>
  <div class="hide-for-small-down medium-4 columns badge">
    <img alt="UX Certification Badge from Nielsen Norman Group" src="https://media.nngroup.com/nng-uxc-badge.png">
  </div>
</div>

            

            
<div class="l-usercontent mainCourseContent collapsable-content participant-comments">
  <h1 class="collapse-link show-for-small-down">Participant Comments</h1>
  <h2 class="show-for-medium-up">Participant Comments</h2>
  <div class="collapse-content collapsable">
    <blockquote>&quot;Lots of take away after this session. Basically covered most of the basics of UX. Clear &amp; well presented. I do have a bit of bacground on UX &amp; this session does make everything clearer. It is easy to understand.&quot;</blockquote>

<p>Lay Lai Lee, Providend Ltd, Singapore</p>

<blockquote>&quot;I feel I have been provided with a tool box for improving UX on my company&#39;s product.&quot;</blockquote>

<p>Karina Methus, Netlife AS, Norway</p>

<blockquote>&quot;High quality content and instruction from the usability gurus themselves. Although most of the course is focused on Web Design and Usability, much of it is applicable to general product design.&quot;</blockquote>

<p>Elizabeth Knowles, Clinical Product Specialist, Electa</p>

<blockquote>&quot;A great intro into UX which lays a strong foundation for more advanced topics. A must for any project manager!&quot;</blockquote>

<p>Andrew Kucheriavy, Intechnic</p>

    <ul id="commentAccordian" class="accordion" data-accordion>
      <li class="accordion-navigation">
        <a href="#cc" class="closed" id="commentCollapseLink">More Participant Comments</a>
        <div id="cc" class="content more-comments">
          <blockquote>&quot;Nice crash course into the world of UX Design.&quot;</blockquote>

<p>Merlin Zuni, Havas Digital</p>

<blockquote>&quot;As a basis of understanding of UX and immense breadth and importance, I thought it was great. Having exercises break up the sessions was also really nice. Katie was a great presenter and kept it nice and light and energetic.&quot;</blockquote>

<p>Josh Awesome, EA</p>

<blockquote>&quot;Although called &#39;Basic Training&#39; this course is great even for the experienced UX designer to round-out and reinforce their knowledge in UX.&quot;</blockquote>

<p>Dave Dennis, Toronto, Ontario</p>

<blockquote>&quot;This course was a great refresher for things I&#39;ve learned and taught me new details of topics I had less knowledge of. I feel like it&#39;s a strong introduction and builds a good foundation of good design practices.&quot;</blockquote>

<p>Amanda Vidad, Relexion Health</p>

<blockquote>&quot;Instructor has an excellent command of the content. Very knowledgeable and excellent facilitator. Clearly an expert in her subject matter. Articulate.&quot;</blockquote>

<p>Amy Murray, WisdomeEdge</p>

<blockquote>&quot;As someone from a developer background, it was great to get more context and understanding on the motivations for various strategies.&quot;</blockquote>

<p>Jason Pittenger, Google</p>

<blockquote>&quot;This basic course was a real eye-opener on the depth and intricacies of UX. It is a great jumping off point for even more UX study.&quot;</blockquote>

<p>Dave Feroe, Dave Feroe Design</p>

<blockquote>&quot;Overview of UX methods but doesn&#39;t delve in to anything too deeply. The speaker was great. Nice, courteous, and on point. A great communicator. Don&#39;t know how she got through so much content. She knows her stuff!&quot;</blockquote>

<p>Nadia Malik, Johns Hopkins Applied Physics Lab</p>

<blockquote>&quot;I come [from] a consolidated retail company, but with very little UX experience. This course really gives you an idea of how to start and what to do.&quot;</blockquote>

<p>Catalina, Santiago, Chile</p>

<blockquote>&quot;This course was informational and valuable. I want to send every member on my team to give [them] overview knowledge.&quot;</blockquote>

<p>Matt Denny,&nbsp; Naval Air Warfare</p>

<blockquote>&quot;I feel really invigorated by the session. The delivery was well-paced and the level of detail was spot-on for a day long grounding in UX.&quot;</blockquote>

<p>Georgia Rakusen, What Users Do</p>

<blockquote>&quot;I think it was awesome! Great start and makes me very excited for the rest of the week.&quot;</blockquote>

<p>Jessi Winner, Floyds Knob</p>

<blockquote>&quot;It gave me a lot of structure for next steps to take in my organization.&quot;</blockquote>

<p>Ron Frank, Boston</p>

<blockquote>&quot;I have learned so much in the first day, it&#39;s crazy! And I have already done UCD testing before.&quot;</blockquote>

<p>Christine Spencer, Conceptual Mind Works, Inc.</p>

<blockquote>&quot;Great class. Gave me a good refresher of UX and really enjoyed the end to end lifecycle of UX.&quot;</blockquote>

<p>Jagannath Chakravarty, Octo Consulting Group</p>

<blockquote>&quot;It&#39;s a great overview of UX principles. I liked the correlations with real-world situations and collaborative critiquing of various UI.&quot;</blockquote>

<p>Don Hill, Tokio Marine</p>

<blockquote>&quot;Instructor was very clear and articulate and knowledgeable. I felt I was getting my &quot;money&#39;s worth&quot; during the course because I saw actionable tasks/plans I could implement.&quot;</blockquote>

<p>&nbsp;</p>

<blockquote>&quot;Excellent examples and use of visuals. Great speaker with lots of konwledge! Real life examples and actionable information.&quot;</blockquote>

<p>Cindy Wildish-Rasmussen, MCIS, Inc.</p>

<blockquote>&quot;Instructor was very polished and knowledgeable. She was able to keep the class engaged for 8 hours while covering a lot of content! Kudos! I will use the documentation and expect I will refer to it regularly!&quot;</blockquote>

<p>Melinda Bauman, University of Virginia Library</p>

<blockquote>&quot;This is a good (very good) intro to UX Design. A must.&quot;</blockquote>

<p>Moris Urar, GlueStudio, Sri Lanka</p>

<blockquote>&quot;I thoroughly enjoyed Kathryn&#39;s course. She provided the best summary I&#39;ve seen of UX practices. I would recommend this course to not only beginner UX&#39;ers, but also to the more advanced as a holistic refresher.&quot;</blockquote>

<p>Allie Etcoff, Zappos.com</p>

<blockquote>&quot;This was a great intro course for anyone starting out in the UX world. It was very informative and had just the right amount of depth to get you more interested in the field.&quot;</blockquote>

<p>Caroline Santos, Cedars-Sinai</p>

<blockquote>&quot;This is a great high level intro that anyone could take to gain a great understanding of what UX is and how it fits into a company project. For a seasoned UXer, it&rsquo;s a good reinforcement of basics.&quot;</blockquote>

<p>Devin Brown, Audience Partners</p>

<blockquote>&quot;I learned I have a lot to learn. Our department has not (yet) adopted UX; but see it as a new buzz word. However the fact that [management] are sending people to UX training gives me hope :)&quot;</blockquote>

<p>&nbsp;</p>

<blockquote>&quot;As a usability researcher, this course greatly reinforced what I have seen in the field as well as provided me with new insights into ways to convince our client that UX is the way to go! Kathryn was very knowledgable and able to answer some tough questions.&quot;</blockquote>

<p>James Villacci, Fulcrum IT Services</p>

<blockquote>&quot;Great course! A lot of information in a short amount of time. Crash-course.&quot;</blockquote>

<p>Yvon Schuurmans, Utrecht, The Netherlands</p>

<blockquote>&quot;It&#39;s a really great introduction to the whole UX world, thanks to the great speaker Kara.&quot;</blockquote>

<p>Sebastian Kolenko, NCM</p>

<blockquote>&quot;Really good, I&#39;m mainly self-taught so enforcing what I thought I know was a real help. I also learnt a lot that I didn&#39;t [know].&quot;</blockquote>

<p>Fraser Crick, Estee Lauder</p>

<blockquote>&quot;UX covers covers many aspects of digital marketing. Therefore a good background is useful to inform decisions. I will embed knowledge into future work. I feel more comfortable about UX now. Very good presentation.&quot;</blockquote>

<p>Poalo Margari, Caputo &amp; Partners</p>

<blockquote>&quot;As someone with a limited background in UX and all things design, this course made me feel comfortable speaking the lingo and understanding the concepts in a UX content, and I will never look at the web content the same way.&quot;</blockquote>

<p>Anastasio Adams, Deloitte Consulting</p>

<blockquote>&quot;This course was very valuable for what I currently do in my job, and I feel more confident with UX. I would highly recommend this course.&quot;</blockquote>

<p>Kayla Rossi, PRIDE Industries</p>

<blockquote>&quot;Great course! I think it&#39;s important to have a good foundation/understanding of usability principles and how that applies in the workplace. Kara&#39;s course was great for that!&quot;</blockquote>

<p>Ahmed Ghoneim, Ubisoft Toronto</p>

<blockquote>&quot;This course really dispelled a lot of the myths that I had about UX and made me aware of certain terminology that I did not know about.&quot;</blockquote>

<p>Christian Bortey, Prinova</p>

<blockquote>&quot;A good refersher for existing UXers, a thorough introduction for those new to the field. Nothing mind-blowing, but the case studies give participants a strong defense for the value and impact of the discipline.&quot;</blockquote>

<p>Brett Rowley, Rapid Miner</p>

<blockquote>&quot;It was at the perfect level for someone who has to PM projects and incorporate this work into the lifecycle.&quot;</blockquote>

<p>Aimee Decker, Ithaca, NY</p>

<blockquote>&quot;Everyone should attend this class even if they are a developer. It will change their way of thinking.&quot;</blockquote>

<p>&nbsp;</p>

<blockquote>&quot;It&#39;s a great course for people who need a holistic vision of what UX is and it&#39;s importance in every project. I strongly recommend it to people in management positions in order to help them to make better decisions for their business.&quot;</blockquote>

<p>Isadora Diaz, Rio de Janeiro, Brazil</p>

<blockquote>&quot;Kara and NNg did a great job articulating the role of UX in the current work climate and reinforcing the tools necessary to succeed.&quot;</blockquote>

<p>Reginald Harrison, Autotrader</p>

<blockquote>&quot;As someone who has extensive training in psychological research, some of the information was very basic, but in terms of learning about the field of UX and how those research methods should be used, the information was very valuable and will be useful moving forward with my career.&quot;</blockquote>

<p>Rebecca W,. Univ. of Tennessee</p>

<blockquote>&quot;Excellent course! It was a great introduction to UX. I will be telling all my colleagues about it!&quot;</blockquote>

<p>Mina Kalsi, Capgemini</p>

<blockquote>&quot;If you are unsure about a career in UX, this course is a great way to immerse yourself. Katie is passionate and energetic &mdash; she&#39;s able to make an extensive amount of info digestible and understandable. Thank you!&quot;</blockquote>

<p>Mina Kalsi, Capgemini</p>

<blockquote>&quot;An all inclusive overview of all aspects around UX. Kathryn&#39;s session displayed a large variety of topics and case studies, involving participants to exchange their working experiences.&quot;</blockquote>

<p>Sotiris Sotiropoulos, Zanshin Labs</p>

<blockquote>&quot;Articulate presenter, packed content, good leave-behinds. This course outlines fundamental practices for UX, useful for the development of those new to UX, as well as providing authority to those experienced UX practitioners.&quot;</blockquote>

<p>Henry Rahr, Corelock</p>

        </div>
      </li>
    </ul>
  </div>
</div>
<script>
  $('#commentAccordian').on('toggled', function (event, accordion) {
    $link = $('#commentCollapseLink');
    $link.toggleClass('closed');
    if($link.hasClass('closed')) {
      $link.text('More Participant Comments');
    } else {
      $link.text('Hide Participant Comments');
    }
  });
</script>



            <div class="l-usercontent"></div>

            
            
            <section class="course-instructors l-subsection collapsable-content">
                <h1 class="show-for-medium-up">Instructor</h1>
                <h1 class="show-for-small-down collapse-link">Instructor</h1>
                <div class="collapsable">
                    
                    <article class="course-instructor collapsable-content">
                        <h2><a href="../../../../people/kathryn-whitenton/index.php">Kathryn Whitenton</a></h2>
                        <div class="row">
                            <div class="medium-3 column hide-for-small-only">
                                <a href="../../../../people/kathryn-whitenton/index.php"><img src="https://media.nngroup.com/media/people/photos/Kathryn_1.jpg.200x200_q95_autocrop_crop_upscale.jpg"></a>
                            </div>
                            <div class="small-12 medium-9 column">
                                <a href="../../../../people/kathryn-whitenton/index.php"><img class="show-for-small-only" src="https://media.nngroup.com/media/people/photos/Kathryn_1.jpg.200x200_q95_autocrop_crop_upscale.jpg"></a>
                                <div class="usercontent"><p>Kathryn Whitenton is Nielsen Norman Group&#39;s Digital Strategy Manager. She works with clients to evaluate the user experience and information architecture of websites in a variety of industries including technology, telecommunications, and media, as well as corporate intranets. She has conducted usability research, eyetracking user research, and studies of users on mobile devices in the United States, Europe, Asia, and Australia. Her user studies have included general audiences as well as specific consumer types, business segments, children, and seniors. <a href="../../../../people/kathryn-whitenton/index.php">Find out more about Kathryn</a>.</p>
</div>
                            </div>
                        </div>
                    </article>
                    
                </div>
            </section>
            <div class="show-for-small-down">
                



<div class="collapsable-content">
  <section class="l-subsection related-images">
    <h2 class="hide-for-small-down">Scenes From Past Trainings</h2>
    <h1 class="show-for-small-down collapse-link">Scenes From Past Trainings</h1>
    <div class="collapsable">
    <p>At the UX Conference, you get a full day of in-depth training with expert instructors. Learn more about <a href="../../../why-attend/index.php">why you should attend</a>.</p>

    
      
        
          
            <img srcset="https://media.nngroup.com/media/files/images/IMG_4794_copy_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale@2x.jpg 2x" src="https://media.nngroup.com/media/files/images/IMG_4794_copy_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale.jpg" alt="">
          
        
      
    
      
        
          
            <img srcset="https://media.nngroup.com/media/files/images/IMG_9994_copy_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale@2x.jpg 2x" src="https://media.nngroup.com/media/files/images/IMG_9994_copy_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale.jpg" alt="">
          
        
      
    
      
        
          
            <img srcset="https://media.nngroup.com/media/files/images/IMG_2649_copy_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale@2x.jpg 2x" src="https://media.nngroup.com/media/files/images/IMG_2649_copy_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale.jpg" alt="">
          
        
      
    
      
        
          
            <img srcset="https://media.nngroup.com/media/files/images/IMG_2941_copy_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale@2x.jpg 2x" src="https://media.nngroup.com/media/files/images/IMG_2941_copy_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale.jpg" alt="Hands-on exercises">
          
        
      
    
    </div>
  </section>
</div>


            </div>
        </div>

        <div class="l-event-sidebar medium-4 small-12 column sidebar">
            <div class="show-for-medium-up">
                <div class="l-subsection">
                    
                    <h2>Course Date: June 19, 2017</h2>
                    
                </div>

                
                    
                        <p><a class="button medium register-button" href="../../../register/92/index.php">Register</a></p>
                    
                
            </div>

            
            <div class="show-for-small-down" style="margin: 2rem 0;">
                <a class="button expand register-button" href="../../../register/92/index.php">Register</a>
            </div>
            


            <section class="l-subsection l-uw-sidebar collapsable-content">
                <h2 class="show-for-medium-up">The UX Conference Austin Courses</h2>
                <h1 class="show-for-small-down collapse-link">UX Conference Austin Courses</h1>

                <div class="collapsable">
                    
                    <ul class="no-bullet">
                        
                        <li><strong>Monday, June 19</strong></li>
                        <ul>
                            
                            
                            
                            <li>
                                
                                <strong>UX Basic Training</strong>
                                
                            </li>
                            
                            
                            
                        </ul>
                        
                        <li><strong>Tuesday, June 20</strong></li>
                        <ul>
                            
                            
                            
                            <li>
                                
                                <a href="../../1650/facilitating-ux-workshops/index.php">Facilitating UX Workshops</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1648/usability-mobile-websites-apps/index.php">Mobile User Experience</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1649/hci/index.php">User Interface Principles Every Designer Must Know</a>
                                
                            </li>
                            
                            
                            
                        </ul>
                        
                        <li><strong>Wednesday, June 21</strong></li>
                        <ul>
                            
                            
                            
                            <li>
                                
                                <a href="../../1652/design-tradeoffs/index.php">Design Tradeoffs and UX Decision Frameworks</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1653/personas/index.php">Personas: Turn User Data Into User-Centered Design</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1651/scaling-responsive-design/index.php">Scaling User Interfaces</a>
                                
                            </li>
                            
                            
                            
                        </ul>
                        
                        <li><strong>Thursday, June 22</strong></li>
                        <ul>
                            
                            
                            
                            <li>
                                
                                <a href="../../1654/measuring-ux/index.php">Measuring User Experience</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1655/journey-mapping/index.php">Journey Mapping to Understand Customer Needs</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1656/web-page-design/index.php">Web Page UX Design</a>
                                
                            </li>
                            
                            
                            
                        </ul>
                        
                        <li><strong>Friday, June 23</strong></li>
                        <ul>
                            
                            
                            
                            <li>
                                
                                <a href="../../1657/ux-deliverables/index.php">UX Deliverables</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1658/cross-channel-user-experience/index.php">Omnichannel Journeys and Customer Experience</a>
                                
                            </li>
                            
                            
                            
                        </ul>
                        
                    </ul>
                </div>
            </section>

            <div class="hide-for-small-down">
                



<div class="collapsable-content">
  <section class="l-subsection related-images">
    <h2 class="hide-for-small-down">Scenes From Past Trainings</h2>
    <h1 class="show-for-small-down collapse-link">Scenes From Past Trainings</h1>
    <div class="collapsable">
    <p>At the UX Conference, you get a full day of in-depth training with expert instructors. Learn more about <a href="../../../why-attend/index.php">why you should attend</a>.</p>

    
      
        
          
            <img srcset="https://media.nngroup.com/media/files/images/IMG_4794_copy_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale@2x.jpg 2x" src="https://media.nngroup.com/media/files/images/IMG_4794_copy_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale.jpg" alt="">
          
        
      
    
      
        
          
            <img srcset="https://media.nngroup.com/media/files/images/IMG_9994_copy_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale@2x.jpg 2x" src="https://media.nngroup.com/media/files/images/IMG_9994_copy_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale.jpg" alt="">
          
        
      
    
      
        
          
            <img srcset="https://media.nngroup.com/media/files/images/IMG_2649_copy_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale@2x.jpg 2x" src="https://media.nngroup.com/media/files/images/IMG_2649_copy_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale.jpg" alt="">
          
        
      
    
      
        
          
            <img srcset="https://media.nngroup.com/media/files/images/IMG_2941_copy_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale@2x.jpg 2x" src="https://media.nngroup.com/media/files/images/IMG_2941_copy_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale.jpg" alt="Hands-on exercises">
          
        
      
    
    </div>
  </section>
</div>


            </div>

            <p class="other-cities"><a href="../../../../courses/ux-basic-training/index.php">Other cities</a> where UX Basic Training is offered.</p>

        </div>
    </div>
</article>

    </div>
    




<footer id="page-footer">
    <div class="row hide-for-print">
        <div class="column small-12 medium-6">
            <section class="footer-popular">
                
                <h1>Popular Topics</h1>
                <div style="float: left;  width: 15em; display: block;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../../../topic/applications/index.php">Application Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../../../topic/e-commerce/index.php">E-Commerce Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../../../topic/eyetracking/index.php">Eyetracking </a></li>
	<li style="padding-bottom: .5em;"><a href="../../../../topic/human-computer-interaction/index.php">Human Computer Interaction </a></li>
	<li style="padding-bottom: .5em;"><a href="../../../../topic/information-architecture/index.php">Information Architecture </a></li>
	<li style="padding-bottom: .5em;"><a href="../../../../topic/intranets/index.php">Intranet Design </a></li>
	<li><a href="../../../../topic/interaction-design/index.php">Interaction Design </a></li>
</ul>
</div>

<div style="float: left;  width: 15em; display: block; list-style: none;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../../../topic/mobile-and-tablet-design/index.php">Mobile and Tablet Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../../../topic/credibility-persuasion/index.php">Persuasive Design</a></li>
	<li style="padding-bottom: .5em;"><a href="../../../../topic/research-methods/index.php">Research Methods</a></li>
	<li style="padding-bottom: .5em;"><a href="../../../../topic/user-testing/index.php">User Testing </a></li>
	<li style="padding-bottom: .5em;"><a href="../../../../topic/web-usability/index.php">Web Usability </a></li>
	<li style="padding-bottom: .5em;"><a href="../../../../topic/writing-web/index.php">Web Writing &amp; Content Strategy </a></li>
</ul>
</div>

<p>&nbsp;</p>

                
            </section>
        </div>

        <div class="column small-12 medium-6">
            <div class="row">
                <div class="column small-6">
                    <section class="footer-about">
                        <h1>Follow Us</h1>
                        <ul>
	<li><a href="../../../../articles/subscribe/index.php">Subscribe to our weekly UX newsletter</a>,&nbsp;Jakob Nielsen&#39;s Alertbox</li>
	<li>Visit Don Norman&#39;s website, <a href="http://www.jnd.org/"> www.jnd.org </a></li>
	<li>Visit Bruce Tognazzini&#39;s website, <a href="http://www.asktog.com/"> www.asktog.com </a></li>
	<li style="padding-top: 6px;"><a class="twitter-follow-button" data-show-count="false" data-size="large" href="https://twitter.com/nngroup"><img alt="Follow @nngroup on Twitter" height="20" src="https://media.nngroup.com/media/editor/2016/12/30/twitter-follow-button.png" style="width: 117px; height: 20px;" width="117" /></a></li>
	<li style="padding-top: 6px;"><img alt="" height="14" src="https://media.nngroup.com/media/editor/2016/07/26/feed-icon-14x14.png" style="vertical-align:text-top;width:14px;margin-right:.25em;" width="14" /><a href="../../../../feed/rss/index.php">Subscribe to our RSS feed</a></li>
</ul>

                    </section>
                </div>

                <div class="column small-6">
                    <section class="footer-about right-column">
                        <h1>About
</h1>
                        <ul class="no-bullet">
	<li><a href="../../../../about/contact/index.php">Contact information</a></li>
	<li><a href="../../../../about/index.php">About Us</a></li>
	<li><a href="../../../../about/why-nng/index.php">Why NN/g? </a></li>
	<li><a href="../../../../return-policy/index.php">Return Policy</a></li>
</ul>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="copyright column small-12">
            <p><a href="../../../../copyright-and-reprint-info/index.php">Copyright</a> &copy; 1998-2017 Nielsen Norman Group, All Rights Reserved.</p>
        </div>
    </div>
</footer>


    


        <script src="https://media.nngroup.com/static/vendor/fastclick/js/fastclick.min.js"></script>
        <script src="https://media.nngroup.com/static/vendor/foundation/js/foundation.min.js"></script>
        <script src="http://media.nngroup.com/static/js/app.js"></script>
        <script src="http://media.nngroup.com/static/js/scroll-to-fixed.js"></script><script>var externalLinks = document.querySelectorAll('a[href^="http"]');for (var i = externalLinks.length-1; i >= 0; i--) {externalLinks[i].addEventListener("click", function(event) { alert("External links are disabled for this test."); event.preventDefault(); }, false);}var mailLinks = document.querySelectorAll('a[href^="mailto"]');for (var i = mailLinks.length-1; i >= 0; i--) {mailLinks[i].addEventListener("click", function(event) { alert("Email links are disabled for this test."); event.preventDefault(); }, false);}var externalForms = document.querySelectorAll('form');for (var i = externalForms.length-1; i >= 0; i--) {externalForms[i].addEventListener("submit", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false); externalForms[i].addEventListener("click", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false);}</script>

        <script>
            
            $(function() {
                $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
                $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
            });
        </script>

        
    

    <script>
        $(function() {
            var toggle = $("#collapse-toggle");
            toggle.on("click", function() {
                $(".collapsable").toggleClass("collapsed");
                toggle.toggleClass("collapsed");
                return(false);
            })
        });
    </script>


        
        <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1072699960;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072699960/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
    </body>

<!-- Mirrored from www.nngroup.com/training/course/1647/ux-basic-training/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:04:49 GMT -->
</html>
