<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->





    
<!-- Mirrored from www.nngroup.com/training/course/1438/usability-testing/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:16:00 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1026.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"licenseKey":"1a30ef9931","errorBeacon":"bam.nr-data.net","applicationID":"2048992","transactionName":"NFxSMRBUX0EDUkIPDA0WdhAMVkVbDV8ZAxUGV0QWTENYVxVCDCMVBldEJg1AQ0EHdVMSAgpVHgIHQQ==","beacon":"bam.nr-data.net","queueTime":3,"applicationTime":651,"agent":""}</script>
        <title>Usability Testing Full Day UX Training by Nielsen Norman Group</title><meta property="og:title" content="Usability Testing Full Day UX Training by Nielsen Norman Group" />
  
        
        <meta name="description" content="Full day course taught at Nielsen Norman Group’s UX Conferences. Learn practical user testing techniques for identifying usability issues quickly and cheaply. ">
        <meta property="og:description" content="Full day course taught at Nielsen Norman Group’s UX Conferences. Learn practical user testing techniques for identifying usability issues quickly and cheaply. " />
        
  
        
	
        

        

        <meta name="robots" content="noarchive">
        <meta name="viewport" content="width=device-width">

        <link rel="search" type="application/opensearchdescription+xml" title="NN/g Search" href="../../../../opensearch.xml" />
        <link rel="shortcut icon" href="http://media.nngroup.com/static/img/favicon.ico" />

        <link rel="stylesheet" href="https://media.nngroup.com/static/vendor/font-awesome/css/font-awesome.min.css">

        
        <link rel="stylesheet" href="https://media.nngroup.com/static/css/7232a960f1df.css" type="text/css" />
        <link rel="stylesheet" href="http://fast.fonts.com/t/1.css?apiType=css&amp;projectid=c5846260-9def-46ec-9407-0f1349bd2d64" type="text/css">

        <script src="https://media.nngroup.com/static/vendor/modernizr/js/modernizr.min.js"></script>

        <script type="text/javascript" src="https://media.nngroup.com/static/js/3283be5def74.js"></script>
        

        
    <link rel="canonical" href="../../../../courses/usability-testing/index.php" />


        
        
        <!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=229034,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('http://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->


<!-- Google Analytics Tracking Code for http://nngroup.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23132681-1', 'nngroup.com');
  ga('require', 'linkid', 'https://www.nngroup.com/training/course/1438/usability-testing/linkid.js');
  ga('require', 'displayfeatures');

// Optimizely Universal Analytics Integration
 window.optimizely = window.optimizely || [];
 window.optimizely.push("activateUniversalAnalytics");

  ga('send', 'pageview');

</script>
<script>
/**
* Function to track a click on an outbound link in Google Analytics.
*/
function trackOutboundLink(label, url) {
    if(window.ga && ga.loaded) {
        ga('send', 'event', 'outbound', 'click', label, {'hitCallback': function(){document.location = url;}
        });
    } else {
        document.location = url;
    }
}
/**
* Function to track a click on a link in the article announcement area.
*/
$('#articleAnnouncement a').on('click', function(){
  ga('send', 'event', 'link', 'click', 'articleAnnouncement');
});
</script>

<script type="text/javascript">
 setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2421.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



<!-- Hotjar Tracking Code for http://nngroup.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:121605,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

        <script src="https://media.nngroup.com/static/js/analytics/track_outbound_link.js"></script>
        
        
        
    </head>
    <body class="location-training location-event-course-detail course-detail">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
    <header id="page-header">
        <div id="cart" class="shopping-cart mobile-cart show-for-small-down"></div>
        <div class="row">
            <div id="masthead" class="masthead small-12 medium-7 column">

                <!-- The button to toggles menu on small screen -->

                <h1><a href="../../../../index.php"><img src="http://media.nngroup.com/static/img/logo-wordmark.png" width="74" height="30"> Nielsen <span class="norman">Norman</span> Group</a></h1>
                <p class="tagline show-for-medium-up">Evidence-Based User Experience Research, Training, and Consulting</p>

                <!-- The button to toggles form search on small screen -->


                <div style="clear: right;width:  100%;height: 1px;"></div><?php define('ROOT_PATH',$_SERVER["DOCUMENT_ROOT"] . '/apps/navigation_styles/navigations/1'); ?><?php include(ROOT_PATH)."/navigation.php"; ?>

                <!-- Drop down menu on small screen -->
                <div id="hideNav" class="show-for-small-down">
                  
                  <ul class="menu-content">
                    <li><a href="../../../../index.php">Home</a></li>
                    <li class="has-submenu">
                      <a>Training</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../../index.php">Overview</a></li>
                        
<li id="nav-ux-conference"><a href="../../../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../../../online-seminars/index.php"><span>Online Seminars</span></a></li>

                      </ul>
                    </li>
                    <li class="has-submenu">
                      <a>Consulting</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
  <li><a href="../../../../consulting/index.php">Overview</a></li>
  
  <li><a href="../../../../consulting/benchmarking/index.php">Benchmarking</a></li>
  
  <li><a href="../../../../consulting/expert-design-review/index.php">Expert Design Review</a></li>
  
  <li><a href="../../../../consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
  
  <li><a href="../../../../consulting/usability-testing/index.php">Usability Testing </a></li>
  
  <li><a href="../../../../consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
  
  <li><a href="../../../../in-house-training/index.php">In-House Training</a></li>
</ul>

                    </li>

                    <li><a href="../../../../reports/index.php">Reports</a></li>
                    <li><a href="../../../../articles/index.php">Articles</a></li>
                    <li class="has-submenu"><a>About NN/G</a><i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../../../about/index.php">Overview</a></li>
                        <li><a href="../../../../people/index.php">People</a></li>
                        <li><a href="../../../../about/why-nng/index.php">Why NN/g?</a></li>
                        <li><a href="../../../../about/contact/index.php">Contact</a></li>
                        <li><a href="../../../../news/index.php">News</a></li>
                        <li><a href="../../../../about/history/index.php">History</a></li>
                        <li><a href="../../../../books/index.php">Books</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                </div>

                <hr class="header-divider show-for-small-only"/>


            </div>
       </div>

        <div id="hideNav">            <nav class="primary-nav small-12 column">
                <ul>
                    <li><a class="navitem" id="nav-home" href="../../../../index.php">Home</a></li>
                    <li><a class="navitem" id="nav-training" href="../../../index.php">Training</a></li>
                    <li><a class="navitem" id="nav-consulting" href="../../../../consulting/index.php">Consulting</a></li>
                    <li><a class="navitem" id="nav-reports" href="../../../../reports/index.php">Reports</a></li>
                    <li><a class="navitem" id="nav-articles" href="../../../../articles/index.php">Articles</a></li>
                    <li><a class="navitem" id="nav-about" href="../../../../about/index.php">About NN/G</a></li>
                </ul>
            </nav>
            <nav class="secondary-nav small-12 medium-12 large-12 columns">
                
  <ul class="inline-list">
  
<li id="nav-ux-conference"><a href="../../../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../../../online-seminars/index.php"><span>Online Seminars</span></a></li>

</ul>


            </nav>
        </div>
    </header>
    <div class="l-content">
    

<article class="course-detail">
    <div class="row">
        <p class="small-12 column">
            
            Full day training course
            
            offered at <a href="../../../london/index.php">The UX Conference London</a>
        </p>
    </div>
    <div class="row">
        <div class="l-section-intro small-12 medium-8 column">
            <header>
                <h1>Usability Testing</h1>
                <h2>Do-it-yourself user tests: Plan, conduct, and analyze your own studies, using in-person, remote, or online methods</h2>
            </header>
        </div>

        <div class="small-12 column show-for-small-down l-subsection">
            
            <h2 class="course-dates">Course Date: November 08, 2016</h2>
            

            
            <div style="clear: both"></div>
            <hr style="border-color: #eee"/>
        </div>
    </div>


    
    <div class="row">
        <div class="l-course-content small-12 medium-8 column">
            <div class="l-usercontent mainCourseContent">
                <p>Watching users try to accomplish tasks on your site is the most effective and efficient way to uncover usability problems. This course will empower you to conduct quick and cheap usability tests throughout the project lifecycle, without having to set up an official lab. Learn the benefits of traditional user testing techniques and online testing tools.&nbsp;You will learn how to facilitate sessions, develop test plans, analyze data, and present the findings.</p>

<p>This course is ideal for people who are new to user testing or have some user testing experience, but no formal training.</p>

<blockquote>&quot;Nuts and bolts information about how to perform user testing will be extremely valuable in our redesign process.&quot;</blockquote>

<p>James Kreuziger<br />
University of California, Irvine</p>

            </div>
            
            <div class="l-usercontent mainCourseContent collapsable-content">

                <h1 class="collapse-link show-for-small-down">Topics Covered</h1>
                <h2 class="show-for-medium-up">Topics Covered</h2>

                <div class="collapsable">
                    <ul>
	<li>Step-by-step instruction on how to conduct your own usability&nbsp;studies</li>
	<li>Why watching users attempt activities is better than interviewing them about their behavior</li>
	<li>Differences between quantitative and qualitative studies, and when to conduct them</li>
	<li>Discount usability testing: Is 4-6&nbsp;participants enough?</li>
	<li>Benefit of spreading budget across multiple small studies through iterative design</li>
	<li>Measuring and tracking usability
	<ul>
		<li>Can new users quickly use the system?</li>
		<li>How fast can users accomplish tasks?</li>
		<li>Do people have re-learn how to use the system the second time they use it?</li>
		<li>Where and when do users encounter errors?</li>
	</ul>
	</li>
	<li>Subjective satisfaction through post-task questionnaire</li>
	<li>Determining scope of study, what to test and observe</li>
	<li>Why you&#39;re ready to test now; testing at every stage of the project lifecycle</li>
	<li>Targeting the right participant for your study
	<ul>
		<li>Identifying target users</li>
		<li>Developing&nbsp;recruitment screeners</li>
		<li>Recruiting agencies can be expensive, but efficient</li>
		<li>Payment and incentives</li>
	</ul>
	</li>
	<li>Recruiting participants anywhere
	<ul>
		<li>Finding your own</li>
		<li>Hiring market research firms</li>
		<li>Using panels</li>
		<li>Intercepting users from your site</li>
		<li>International testing</li>
	</ul>
	</li>
	<li>Usability labs and setup
	<ul>
		<li>Formal vs. informal labs</li>
		<li>Benefit of moderated in-person testing</li>
		<li>Testing mobile devices</li>
		<li>Writing a test plan</li>
		<li>Tools for screen-sharing and recording</li>
	</ul>
	</li>
	<li>Online and remote user testing
	<ul>
		<li>Get same-day user feedback on your design</li>
		<li><span style="font-size: 13px; line-height: 1.6;">Increase your whole team&#39;s understanding about user behavior with your design</span></li>
		<li><span style="font-size: 13px; line-height: 1.6;">Setting up un-moderated studies where the participants complete tasks on their own</span></li>
		<li><span style="font-size: 13px; line-height: 1.6;">Applying remote testing for Lean UX</span></li>
		<li><span style="font-size: 13px; line-height: 1.6;">Optimizing time for short (15 minutes or less) un-moderated sessions</span></li>
		<li><span style="font-size: 13px; line-height: 1.6;">Moderating sessions while on the phone with the participant</span></li>
		<li><span style="font-size: 13px; line-height: 1.6;">Using online tools for rapid prototyping and iterative design</span></li>
	</ul>
	</li>
	<li>Recording usability sessions
	<ul>
		<li>How to take notes while facilitating</li>
		<li>What observations to look for and record</li>
		<li>Tools and techniques for video recording and note taking</li>
	</ul>
	</li>
	<li>Writing tasks (scenarios) to avoid bias and get the feedback you need
	<ul>
		<li>Exploratory tasks are open-ended and more&nbsp;research oriented</li>
		<li>Directed tasks are specific&nbsp;and answer-oriented</li>
		<li>Presenting tasks at the right time and order</li>
		<li>Writing guidelines for tasks</li>
	</ul>
	</li>
	<li>Running sessions
	<ul>
		<li>Prepare participant when they arrive</li>
		<li>Techniques for getting participants to think aloud and provide valuable feedback</li>
		<li>Practice staying neutral and giving subtle acknowledgments</li>
		<li>Reading non-verbal cues</li>
		<li>Managing challenging situations</li>
		<li>What to capture when taking notes</li>
		<li>Note-taking tips and tools</li>
	</ul>
	</li>
	<li>Don&#39;t plan or test in isolation
	<ul>
		<li>Invite observers to watch sessions and take notes</li>
		<li>How to debrief observers</li>
	</ul>
	</li>
	<li>Analyzing findings
	<ul>
		<li>Techniques for quick turn-around times</li>
		<li>Use quantitative data to locate issues</li>
		<li>Determine severity of each finding</li>
		<li>Common analysis mistakes</li>
		<li>Providing design recommendations</li>
	</ul>
	</li>
	<li>Reporting usability findings
	<ul>
		<li>Items to include in reports</li>
		<li>Informal and formal reports</li>
	</ul>
	</li>
	<li>Presenting findings
	<ul>
		<li>Effective ways to communicate recommendations</li>
		<li>Persuasive illustrations and videos</li>
	</ul>
	</li>
</ul>

                </div>

            </div>
            

            
            <div class="l-usercontent mainCourseContent collapsable-content">

                <h1 class="collapse-link show-for-small-down">Free Material With Course Attendance</h1>
                <h2 class="show-for-medium-up">Free Material With Course Attendance</h2>

                <div class="collapsable">
                    <ul style="margin-left:40px">
	<li>Sample documents: Test script, test plan, recruitment screener, and usability report</li>
	<li>Criteria for evaluating the validity of user testing tasks</li>
	<li>Nielsen Norman Group reports on recruiting participants and conducting user testing
	<ul>
		<li>Report: <a href="../../../../reports/how-to-conduct-usability-studies/index.php">How to Conduct Usability Studies</a></li>
		<li>Report: <a href="../../../../reports/how-to-recruit-participants-usability-studies/index.php">How to Recruit Participants for Usability Studies</a></li>
	</ul>
	</li>
</ul>

                </div>

            </div>
            

            
            <div class="l-usercontent mainCourseContent collapsable-content">

                <h1 class="collapse-link show-for-small-down">Format</h1>
                <h2 class="show-for-medium-up">Format</h2>

                <div class="collapsable">
                    <p>You will practice planning, conducting, and analyzing&nbsp; your own usability study during this workshop and receive immediate expert feedback. Lecture segments introduce topics and techniques.</p>

<p>This course also includes:</p>

<ul style="margin-left:40px">
	<li>A live demonstration of usability testing</li>
	<li>Videos depicting what to do and what not to do</li>
	<li>Hands-on exercises practicing task writing, facilitation, note taking, and analyzing findings</li>
</ul>
<!--
<h2>Companion Course</h2>

<p><a href="http://www.nngroup.com/courses/online-and-remote-user-testing">Online and Remote User Testing</a> is a related course to Usability Testing. Each course can be taken independently or together.</p>

<p>In this course you will learn more detailed facilitation techniques and how to set up a quick and cheap usability lab. If you&rsquo;re specifically interested in conducting remote usability testing, we suggest taking the course Online and Remote User Testing to learn how to adapt the techniques.</p>
-->
                </div>

            </div>
            

            

            

            

            

            
                



<div class="row certification-info">
  <section class="l-subsection collapsable-content small-12 medium-8 columns">
    <h1 class="collapse-link show-for-small-down">UX Certification Credit</h1>
    <h2 class="hide-for-small-down">UX Certification Credit</h2>
    <div class="collapsable">
      <p>
      Attending this course and passing the exam earns <strong>1 UX Certification credit</strong>, which also counts towards the optional <a href="../../../../ux-certification/ux-research-specialty/index.php">UX Research Specialty</a>.
      </p>

      

      <p>
      Learn more about NN/g's <a href="../../../../ux-certification/index.php">UX Certification</a> Program.
      </p>
    </div>
  </section>
  <div class="hide-for-small-down medium-4 columns badge">
    <img alt="UX Certification Badge from Nielsen Norman Group" src="https://media.nngroup.com/nng-uxc-badge.png">
  </div>
</div>

            

            
<div class="l-usercontent mainCourseContent collapsable-content participant-comments">
  <h1 class="collapse-link show-for-small-down">Participant Comments</h1>
  <h2 class="show-for-medium-up">Participant Comments</h2>
  <div class="collapse-content collapsable">
    <blockquote>&quot;This class takes the scariness out of user testing. It emphasizes &quot;learning by doing&quot; which helps ingrain user testing knowledge really well. Great instructor too &mdash; great at keeping us awake with interactive examples and posing questions.&quot;</blockquote>

<p>Sydney Ratzlaff, Emergency Reporting</p>

<blockquote>&quot;Really great presentation. Finally learned how to ask the right questions.&quot;</blockquote>

<p>Melissa McKee, Caresync</p>

<blockquote>&quot;I learned so much from the day. Small changes I can make to our current usability testing which will make a big difference to our products.&quot;</blockquote>

<p>Paul O&#39;Leary, Asavie</p>

<blockquote>&quot;Anyone planning on doing usability tests, follow this course. I have been an usability specialist for the past two years and I&#39;ve learned more in one day! Thank you NNG group!&quot;</blockquote>

<p>Mohamad Marhaba, Michelin, France</p>

    <ul id="commentAccordian" class="accordion" data-accordion>
      <li class="accordion-navigation">
        <a href="#cc" class="closed" id="commentCollapseLink">More Participant Comments</a>
        <div id="cc" class="content more-comments">
          <blockquote>&quot;It&#39;s a great course for anyone willing to learn the foundations of usability testing. I liked the fact it&#39;s a constant flowing discussion between instructor and attendees.&quot;</blockquote>

<p>Andrew Briffa, Betsson Group</p>

<blockquote>&quot;It&#39;s a lot of information in a short amount of time &mdash; but very worth while.&quot;</blockquote>

<p>Richard Merrick, UI/UX Designer</p>

<blockquote>&quot;The exercises were very helpful to fully explain the ideas.&quot;</blockquote>

<p>Bill Dale, Harrisburg, PA</p>

<blockquote>&quot;Crash course in qualitative usability testing. Learn a lot in almost no time at all.&quot;</blockquote>

<p>Jett House, New York</p>

<blockquote>&quot;Great course. Highly knowledgeable and engaging facilitator. Will help me greatly as I take info back to my job. Made me realize that usability w/PWD has a long ways to go.&quot;</blockquote>

<p>Patrick Dunphy, CBC, Toronto</p>

<blockquote>&quot;I do usability testing and act as a moderator. This course provided some great information I plan to implement in my next user test.&quot;</blockquote>

<p>Derrick Pedranti, Cognosante</p>

<blockquote>&quot;Great throughout the whole day session. The afternoon session usually very tiring, but Hoa makes it very engaging and interesting. Good control in answering questions from audience.&quot;</blockquote>

<p>Jane Wong, Michell 1</p>

<blockquote>&quot;I really enjoyed this course! The level of detail and specific takeaways to implement has been really great.&quot;</blockquote>

<p>Lisa Murray, Zion &amp; Zion</p>

<blockquote>&quot;Conclusive content and Kara is very good and motivational speaker. I feel I&#39;m ready to go home and start running my own usability tests.&nbsp;&quot;</blockquote>

<p>Sofie Asplund, Knowledge Cube, Copenhagen</p>

<blockquote>&quot;Packed full of great information that helps you connect concepts and methods to your actions. This course was particularly useful for those who are not &#39;classically&#39; trained in usability testing.&quot;</blockquote>

<p>Judy Bayne, Autodesk</p>

<blockquote>&quot;Fantastic course&hellip; as the business solution owner, I&#39;m in charge of testing during development. This class gave me the complete playbook, including hacks, watch outs, and true rationale for a sound approach.&quot;</blockquote>

<p>&nbsp;</p>

<blockquote>&quot;Great insight into how usability test should be conducted. Very informative and very well presented.&quot;</blockquote>

<p>Bojan Gospavic, Stryker</p>

<blockquote>&quot;This is a great crash course in usability testing. As a researcher who has been doing usability testing of my own for a few years it stood as a good reminder of basic principles and introduced a few new tips and tricks.&quot;</blockquote>

<p>Paige Brannon, Career Builder</p>

<blockquote>&quot;I have taught myself how to do usability testing. This course helped to identify problems with my approach and reinforce the areas where I&#39;m getting it right.&quot;</blockquote>

<p>Brooke Fletcher, Shelter Insurance</p>

<blockquote>&quot;This was a great class. I appreciated the breadth of knowledge and best practices relayed. The instructor is an excellent speaker and engaged the class which greatly facilitated learning and made the class fun!&quot;</blockquote>

<p>Rey Echevarria, American Hospital Assoc.</p>

<blockquote>&quot;Great mix of theory/research and actual usable techniques for administering UX testing.&quot;</blockquote>

<p>Cindy Wildish Rasmussen, MCIS, Inc.</p>

<blockquote>&quot;A lot of great techniques on how to better personal skills of usability research, either observer, notetaker, or moderator.&quot;</blockquote>

<p>Daniel Hirst, NuSkin</p>

<blockquote>&quot;This is exactly what I was hoping for. Great information.&quot;</blockquote>

<p>John Nance, Amazon, Woot.com</p>

<blockquote>&quot;Course was great &mdash; I especially liked how Kara touched on solutions to conduct testing track insights and solution. This really elevated the course from &quot;instructional&quot; to &quot;actionable&quot;.&quot;</blockquote>

<p>Morgan Lacy, Shark Ninja</p>

<blockquote>&quot;The interactive level is perfect. The storytelling &amp; video helped illustrate best practices.&quot;</blockquote>

<p>Helen Fernety, Dillards</p>

<blockquote>&quot;One of my favorite courses of the entire week. Kara challenges everyone from novice to advanced usability testers to expand their horizons and not only learn from her but from your peers.&quot;</blockquote>

<p>Tiana, Washington, DC</p>

<blockquote>&quot;Kara was very helpful and accepting of questions about Usability Testing. I liked how interactive she made the sessions down to facilitating a session and taking notes. I learned a lot of new things to use in my practice.&quot;</blockquote>

<p>Daphne Adebayo, Career Builder</p>

<blockquote>&quot;Excellent content and instruction. Hoa is FANTASTIC, gave us &quot;real world&quot; scenarios.&quot;</blockquote>

<p>Jennifer Fletcher, Pacific Dental Services</p>

<blockquote>&quot;This course have given me tools for heading out to conduct UTST. I know common pitfalls and how to avoid them, Kara as a teacher is brilliant.&quot;</blockquote>

<p>Pieter H&ouml;rnell, Innofactor, Stockholm</p>

<blockquote>&quot;Excellent training with focus on real work situations, but backed up by science and years of experience.&quot;</blockquote>

<p>Harry van Mierloo, Fox-IT</p>

<blockquote>&quot;This course is really useful for those looking to introduce the concept of Usability Testing into their organization. Lots of actionable best practices are shared and you&#39;re likely to leave with at least one or two ideas to bring back to your company.&quot;</blockquote>

<p>Z. Smith, Domino&#39;s</p>

<blockquote>&quot;I love that every single question was answered with theory and an example from her own experience.&quot;</blockquote>

<p>Tom, Ordina, Netherlands</p>

<blockquote>&quot;Loranger&#39;s style is engaging, insightful and hands on. Her broad experience allows her to respond dynamically to any questions, and provide innovative ways to different scenarios. The session was fun til the end. Thanks!&quot;</blockquote>

<p>Daniel Penati, South Muir Health</p>

<blockquote>&quot;I learned a lot from this course. I felt like the instructor was very knowledgeable in the field. I didn&#39;t know how to conduct a usability test, and now I do! The only problem I encountered was the fact that we only ran through a usability exercise once. I did ok, but could have done better on the next test doing a real usability test.&quot;</blockquote>

<p>Marcus Davis, Northrop Grumman</p>

<blockquote>&quot;Enjoyed Kara&#39;s workshop very much. Very educational and easy to follow. Thank you!&quot;</blockquote>

<p>Zoe Fisher, San Francisco</p>

<blockquote>&quot;Really enjoyed the group exercises; it opened my mind to what others think and how they interpret.&quot;</blockquote>

<p>Saffron Davis, The Regional Municipality of York</p>

<blockquote>&quot;I liked the progression of the exercises and how we tried them together. I learned a lot from Kara&#39;s real world examples and really strengthened my knowledge. I&#39;ll be taking some of the tips and techniques I learned and using them right away.&quot;</blockquote>

<p>Michelle Larsen, Aquas, Inc.</p>

<blockquote>&quot;Great session! It was really well paced, and laid out very clearly how to set up and run tests, and how to report them.&quot;</blockquote>

<p>R Gilmour, t/ES, UK</p>

<blockquote>&quot;Good balance on tips, real-life stories, and class exercises.&quot;</blockquote>

<p>Brandon Welch, Medical University South Carolina</p>

<blockquote>&quot;Best course I attended so far. Kara is an incredible instructor. Accurate, enthusiastic, funny and very professional.&quot;</blockquote>

<p>Remy Florean, DUX Agency</p>

<blockquote>&quot;Great interaction with the class by presenting real examples from personal experience.&quot;</blockquote>

<p>Eshani Kumbhani, Spinach Studios, India</p>

<blockquote>&quot;Excellent course to solidify my skills in usability testing. Loved all the hints and tips Kara shared, but mostly I really appreciated to see how usability testing, being a fairly inexpensive technique, can help so much in a project.&quot;</blockquote>

<p>Norberto Henriques, WeDo Technologies, Portugal</p>

<blockquote>&quot;A solid session with lots of tips for in-action situations of usability testing. Kara&#39;s delivery on the subject was detailed and helpful. Real case studies (videos).&quot;</blockquote>

<p>Sotiris Sotriopoulos, Zanshin Labs, UK/Greece</p>

<blockquote>&quot;It&#39;s a great way to understand the process of testing a product and gain insights in how to handle specific situations when running a user test session.&quot;</blockquote>

<p>Sergi Vila, King, Spain</p>

<blockquote>&quot;Currently work on a UX team where we employ a lot of testing to gain understanding of our users. Found the class both informative and that it reinforced the practices we use in our organization. Also, learned some additional techniques we can employ.&quot;</blockquote>

<p>Oneka Williams, Home Depot</p>

        </div>
      </li>
    </ul>
  </div>
</div>
<script>
  $('#commentAccordian').on('toggled', function (event, accordion) {
    $link = $('#commentCollapseLink');
    $link.toggleClass('closed');
    if($link.hasClass('closed')) {
      $link.text('More Participant Comments');
    } else {
      $link.text('Hide Participant Comments');
    }
  });
</script>



            <div class="l-usercontent"></div>

            
            
            <section class="course-instructors l-subsection collapsable-content">
                <h1 class="show-for-medium-up">Instructor</h1>
                <h1 class="show-for-small-down collapse-link">Instructor</h1>
                <div class="collapsable">
                    
                    <article class="course-instructor collapsable-content">
                        <h2><a href="../../../../people/kara-pernice/index.php">Kara Pernice</a></h2>
                        <div class="row">
                            <div class="medium-3 column hide-for-small-only">
                                <a href="../../../../people/kara-pernice/index.php"><img src="https://media.nngroup.com/media/people/photos/IMG_0271_copy_retouched_square_900px.jpg.200x200_q95_autocrop_crop_upscale.jpg"></a>
                            </div>
                            <div class="small-12 medium-9 column">
                                <a href="../../../../people/kara-pernice/index.php"><img class="show-for-small-only" src="https://media.nngroup.com/media/people/photos/IMG_0271_copy_retouched_square_900px.jpg.200x200_q95_autocrop_crop_upscale.jpg"></a>
                                <div class="usercontent"><p>Kara Pernice is Senior Vice President at Nielsen Norman Group. Pernice uniquely combines her 20-plus years of business, research, and design knowledge and experience to help organizations increase their UX maturity and derive interfaces which are usable, useful, and surpass business goals.&nbsp;Pernice is accomplished at evaluating and managing design situations, and crafting with a team the most fitting design and research methods, and converting this analysis into outstanding design. Pernice has led teams running hundreds of intercontinental research studies, and is expert in many usability methods. <a href="../../../../people/kara-pernice/index.php">Read more about Kara</a>.</p>
</div>
                            </div>
                        </div>
                    </article>
                    
                </div>
            </section>
            <div class="show-for-small-down">
                



<div class="collapsable-content">
  <section class="l-subsection related-images">
    <h2 class="hide-for-small-down">Scenes From Past Trainings</h2>
    <h1 class="show-for-small-down collapse-link">Scenes From Past Trainings</h1>
    <div class="collapsable">
    <p>At the UX Conference, you get a full day of in-depth training with expert instructors. Learn more about <a href="../../../why-attend/index.php">why you should attend</a>.</p>

    
      
        
          
            <img srcset="https://media.nngroup.com/media/files/images/IMG_3991_copy_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale@2x.jpg 2x" src="https://media.nngroup.com/media/files/images/IMG_3991_copy_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale.jpg" alt="">
          
        
      
    
      
        
          
            <img srcset="https://media.nngroup.com/media/files/images/IMG_9994_copy_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale@2x.jpg 2x" src="https://media.nngroup.com/media/files/images/IMG_9994_copy_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale.jpg" alt="">
          
        
      
    
      
        
          
            <img srcset="https://media.nngroup.com/media/files/images/IMG_4114_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale@2x.jpg 2x" src="https://media.nngroup.com/media/files/images/IMG_4114_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale.jpg" alt="">
          
        
      
    
      
        
          
            <img srcset="https://media.nngroup.com/media/files/images/IMG_4730_copy_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale@2x.jpg 2x" src="https://media.nngroup.com/media/files/images/IMG_4730_copy_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale.jpg" alt="">
          
        
      
    
    </div>
  </section>
</div>


            </div>
        </div>

        <div class="l-event-sidebar medium-4 small-12 column sidebar">
            <div class="show-for-medium-up">
                <div class="l-subsection">
                    
                    <h2>Course Date: November 08, 2016</h2>
                    
                </div>

                
            </div>

            


            <section class="l-subsection l-uw-sidebar collapsable-content">
                <h2 class="show-for-medium-up">The UX Conference London Courses</h2>
                <h1 class="show-for-small-down collapse-link">UX Conference London Courses</h1>

                <div class="collapsable">
                    
                    <ul class="no-bullet">
                        
                        <li><strong>Saturday, November 5</strong></li>
                        <ul>
                            
                            
                            
                            <li>
                                
                                <a href="../../1423/ux-basic-training/index.php">UX Basic Training</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1424/ux-vp-director/index.php">The UX VP/Director</a>
                                
                            </li>
                            
                            
                            
                        </ul>
                        
                        <li><strong>Sunday, November 6</strong></li>
                        <ul>
                            
                            
                            
                            <li>
                                
                                <a href="../../1425/interaction-design-3-day-course/index.php">Interaction Design: 3-Day Course</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1450/journey-mapping/index.php">Journey Mapping to Understand Customer Needs</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1427/usability-mobile-websites-apps/index.php">Mobile User Experience</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1428/information-architecture/index.php">Information Architecture</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1426/cross-functional-team/index.php">Working Effectively in Cross-Functional Teams</a>
                                
                            </li>
                            
                            
                            
                        </ul>
                        
                        <li><strong>Monday, November 7</strong></li>
                        <ul>
                            
                            
                            
                            <li>
                                
                                <a href="../../1433/website-design-lessons-social-psychology/index.php">Website Design Lessons from Social Psychology</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1425/interaction-design-3-day-course/index.php">Interaction Design: 3-Day Course</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1430/leading-ux-teams/index.php">Leading Highly-Effective UX Teams </a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1431/personas/index.php">Personas: Turn User Data Into User-Centered Design</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1432/scaling-responsive-design/index.php">Scaling User Interfaces</a>
                                
                            </li>
                            
                            
                            
                        </ul>
                        
                        <li><strong>Tuesday, November 8</strong></li>
                        <ul>
                            
                            
                            
                            <li>
                                
                                <a href="../../1435/stakeholder-ux-approval/index.php">Engaging Stakeholders to Build Buy-In</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1436/ideation/index.php">Effective Ideation Techniques for UX Design</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1437/vis-mob-1/index.php">Visual Design for Mobile and Tablet: Day 1</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <strong>Usability Testing</strong>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1425/interaction-design-3-day-course/index.php">Interaction Design: 3-Day Course</a>
                                
                            </li>
                            
                            
                            
                        </ul>
                        
                        <li><strong>Wednesday, November 9</strong></li>
                        <ul>
                            
                            
                            
                            <li>
                                
                                <a href="../../1452/generating-big-ideas/index.php">Generating Big Ideas with Design Thinking</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1440/managing-user-experience-strategy/index.php">Managing User Experience Strategy</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1439/measuring-ux/index.php">Measuring User Experience</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1441/vis-mob-2/index.php">Visual Design for Mobile and Tablet: Day 2</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1442/content-strategy/index.php">Content Strategy</a>
                                
                            </li>
                            
                            
                            
                        </ul>
                        
                        <li><strong>Thursday, November 10</strong></li>
                        <ul>
                            
                            
                            
                            <li>
                                
                                <a href="../../1444/hci/index.php">User Interface Principles Every Designer Must Know</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1451/web-page-design/index.php">Web Page UX Design</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1443/analytics-and-user-experience/index.php">Analytics and User Experience</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1445/cross-channel-user-experience/index.php">Omnichannel Journeys and Customer Experience</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1453/facilitating-ux-workshops/index.php">Facilitating UX Workshops</a>
                                
                            </li>
                            
                            
                            
                        </ul>
                        
                        <li><strong>Friday, November 11</strong></li>
                        <ul>
                            
                            
                            
                            <li>
                                
                                <a href="../../1446/human-mind/index.php">The Human Mind and Usability</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1454/communicating-design/index.php">Communicating Design</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1449/designing-millennials/index.php">Designing for Millennials</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1448/wireframing-and-prototyping/index.php">Wireframing and Prototyping</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1447/ux-deliverables/index.php">UX Deliverables</a>
                                
                            </li>
                            
                            
                            
                        </ul>
                        
                    </ul>
                </div>
            </section>

            <div class="hide-for-small-down">
                



<div class="collapsable-content">
  <section class="l-subsection related-images">
    <h2 class="hide-for-small-down">Scenes From Past Trainings</h2>
    <h1 class="show-for-small-down collapse-link">Scenes From Past Trainings</h1>
    <div class="collapsable">
    <p>At the UX Conference, you get a full day of in-depth training with expert instructors. Learn more about <a href="../../../why-attend/index.php">why you should attend</a>.</p>

    
      
        
          
            <img srcset="https://media.nngroup.com/media/files/images/IMG_3991_copy_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale@2x.jpg 2x" src="https://media.nngroup.com/media/files/images/IMG_3991_copy_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale.jpg" alt="">
          
        
      
    
      
        
          
            <img srcset="https://media.nngroup.com/media/files/images/IMG_9994_copy_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale@2x.jpg 2x" src="https://media.nngroup.com/media/files/images/IMG_9994_copy_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale.jpg" alt="">
          
        
      
    
      
        
          
            <img srcset="https://media.nngroup.com/media/files/images/IMG_4114_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale@2x.jpg 2x" src="https://media.nngroup.com/media/files/images/IMG_4114_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale.jpg" alt="">
          
        
      
    
      
        
          
            <img srcset="https://media.nngroup.com/media/files/images/IMG_4730_copy_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale@2x.jpg 2x" src="https://media.nngroup.com/media/files/images/IMG_4730_copy_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale.jpg" alt="">
          
        
      
    
    </div>
  </section>
</div>


            </div>

            <p class="other-cities"><a href="../../../../courses/usability-testing/index.php">Other cities</a> where Usability Testing is offered.</p>

        </div>
    </div>
</article>

    </div>
    




<footer id="page-footer">
    <div class="row hide-for-print">
        <div class="column small-12 medium-6">
            <section class="footer-popular">
                
                <h1>Popular Topics</h1>
                <div style="float: left;  width: 15em; display: block;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../../../topic/applications/index.php">Application Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../../../topic/e-commerce/index.php">E-Commerce Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../../../topic/eyetracking/index.php">Eyetracking </a></li>
	<li style="padding-bottom: .5em;"><a href="../../../../topic/human-computer-interaction/index.php">Human Computer Interaction </a></li>
	<li style="padding-bottom: .5em;"><a href="../../../../topic/information-architecture/index.php">Information Architecture </a></li>
	<li style="padding-bottom: .5em;"><a href="../../../../topic/intranets/index.php">Intranet Design </a></li>
	<li><a href="../../../../topic/interaction-design/index.php">Interaction Design </a></li>
</ul>
</div>

<div style="float: left;  width: 15em; display: block; list-style: none;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../../../topic/mobile-and-tablet-design/index.php">Mobile and Tablet Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../../../topic/credibility-persuasion/index.php">Persuasive Design</a></li>
	<li style="padding-bottom: .5em;"><a href="../../../../topic/research-methods/index.php">Research Methods</a></li>
	<li style="padding-bottom: .5em;"><a href="../../../../topic/user-testing/index.php">User Testing </a></li>
	<li style="padding-bottom: .5em;"><a href="../../../../topic/web-usability/index.php">Web Usability </a></li>
	<li style="padding-bottom: .5em;"><a href="../../../../topic/writing-web/index.php">Web Writing &amp; Content Strategy </a></li>
</ul>
</div>

<p>&nbsp;</p>

                
            </section>
        </div>

        <div class="column small-12 medium-6">
            <div class="row">
                <div class="column small-6">
                    <section class="footer-about">
                        <h1>Follow Us</h1>
                        <ul>
	<li><a href="../../../../articles/subscribe/index.php">Subscribe to our weekly UX newsletter</a>,&nbsp;Jakob Nielsen&#39;s Alertbox</li>
	<li>Visit Don Norman&#39;s website, <a href="http://www.jnd.org/"> www.jnd.org </a></li>
	<li>Visit Bruce Tognazzini&#39;s website, <a href="http://www.asktog.com/"> www.asktog.com </a></li>
	<li style="padding-top: 6px;"><a class="twitter-follow-button" data-show-count="false" data-size="large" href="https://twitter.com/nngroup"><img alt="Follow @nngroup on Twitter" height="20" src="https://media.nngroup.com/media/editor/2016/12/30/twitter-follow-button.png" style="width: 117px; height: 20px;" width="117" /></a></li>
	<li style="padding-top: 6px;"><img alt="" height="14" src="https://media.nngroup.com/media/editor/2016/07/26/feed-icon-14x14.png" style="vertical-align:text-top;width:14px;margin-right:.25em;" width="14" /><a href="../../../../feed/rss/index.php">Subscribe to our RSS feed</a></li>
</ul>

                    </section>
                </div>

                <div class="column small-6">
                    <section class="footer-about right-column">
                        <h1>About
</h1>
                        <ul class="no-bullet">
	<li><a href="../../../../about/contact/index.php">Contact information</a></li>
	<li><a href="../../../../about/index.php">About Us</a></li>
	<li><a href="../../../../about/why-nng/index.php">Why NN/g? </a></li>
	<li><a href="../../../../return-policy/index.php">Return Policy</a></li>
</ul>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="copyright column small-12">
            <p><a href="../../../../copyright-and-reprint-info/index.php">Copyright</a> &copy; 1998-2017 Nielsen Norman Group, All Rights Reserved.</p>
        </div>
    </div>
</footer>


    


        <script src="https://media.nngroup.com/static/vendor/fastclick/js/fastclick.min.js"></script>
        <script src="https://media.nngroup.com/static/vendor/foundation/js/foundation.min.js"></script>
        <script src="http://media.nngroup.com/static/js/app.js"></script>
        <script src="http://media.nngroup.com/static/js/scroll-to-fixed.js"></script><script>var externalLinks = document.querySelectorAll('a[href^="http"]');for (var i = externalLinks.length-1; i >= 0; i--) {externalLinks[i].addEventListener("click", function(event) { alert("External links are disabled for this test."); event.preventDefault(); }, false);}var mailLinks = document.querySelectorAll('a[href^="mailto"]');for (var i = mailLinks.length-1; i >= 0; i--) {mailLinks[i].addEventListener("click", function(event) { alert("Email links are disabled for this test."); event.preventDefault(); }, false);}var externalForms = document.querySelectorAll('form');for (var i = externalForms.length-1; i >= 0; i--) {externalForms[i].addEventListener("submit", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false); externalForms[i].addEventListener("click", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false);}</script>

        <script>
            
            $(function() {
                $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
                $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
            });
        </script>

        
    

    <script>
        $(function() {
            var toggle = $("#collapse-toggle");
            toggle.on("click", function() {
                $(".collapsable").toggleClass("collapsed");
                toggle.toggleClass("collapsed");
                return(false);
            })
        });
    </script>


        
        <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1072699960;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072699960/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
    </body>

<!-- Mirrored from www.nngroup.com/training/course/1438/usability-testing/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:16:00 GMT -->
</html>
