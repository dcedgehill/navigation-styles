<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->





    
<!-- Mirrored from www.nngroup.com/news/item/2013-intranet-design-awards/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:13:15 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1026.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"beacon":"bam.nr-data.net","queueTime":4,"applicationTime":142,"agent":"","licenseKey":"1a30ef9931","errorBeacon":"bam.nr-data.net","applicationID":"2048992","transactionName":"NFxSMRBUX0EDUkIPDA0WdhAMVkVbDV8ZCAYUSh4TC1BGQVh/UxEQJ1xEBAtZH1UHRQ=="}</script>
        <title>2013 NN/g Intranet Design Award Winners</title><meta property="og:title" content="2013 NN/g Intranet Design Award Winners" />
  
        
        <meta name="description" content="2013 NN/g Intranet Design Award Winners">
        <meta property="og:description" content="2013 NN/g Intranet Design Award Winners" />
        
  
        
	
        

        

        <meta name="robots" content="noarchive">
        <meta name="viewport" content="width=device-width">

        <link rel="search" type="application/opensearchdescription+xml" title="NN/g Search" href="../../../opensearch.xml" />
        <link rel="shortcut icon" href="http://media.nngroup.com/static/img/favicon.ico" />

        <link rel="stylesheet" href="https://media.nngroup.com/static/vendor/font-awesome/css/font-awesome.min.css">

        
        <link rel="stylesheet" href="https://media.nngroup.com/static/css/7232a960f1df.css" type="text/css" />
        <link rel="stylesheet" href="http://fast.fonts.com/t/1.css?apiType=css&amp;projectid=c5846260-9def-46ec-9407-0f1349bd2d64" type="text/css">

        <script src="https://media.nngroup.com/static/vendor/modernizr/js/modernizr.min.js"></script>

        <script type="text/javascript" src="https://media.nngroup.com/static/js/3283be5def74.js"></script>
        

        

        
        
        <!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=229034,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('http://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->


<!-- Google Analytics Tracking Code for http://nngroup.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23132681-1', 'nngroup.com');
  ga('require', 'linkid', 'https://www.nngroup.com/news/item/2013-intranet-design-awards/linkid.js');
  ga('require', 'displayfeatures');

// Optimizely Universal Analytics Integration
 window.optimizely = window.optimizely || [];
 window.optimizely.push("activateUniversalAnalytics");

  ga('send', 'pageview');

</script>
<script>
/**
* Function to track a click on an outbound link in Google Analytics.
*/
function trackOutboundLink(label, url) {
    if(window.ga && ga.loaded) {
        ga('send', 'event', 'outbound', 'click', label, {'hitCallback': function(){document.location = url;}
        });
    } else {
        document.location = url;
    }
}
/**
* Function to track a click on a link in the article announcement area.
*/
$('#articleAnnouncement a').on('click', function(){
  ga('send', 'event', 'link', 'click', 'articleAnnouncement');
});
</script>

<script type="text/javascript">
 setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2421.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



<!-- Hotjar Tracking Code for http://nngroup.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:121605,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

        <script src="https://media.nngroup.com/static/js/analytics/track_outbound_link.js"></script>
        
        
        
    </head>
    <body class="location-about location-news">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
    <header id="page-header">
        <div id="cart" class="shopping-cart mobile-cart show-for-small-down"></div>
        <div class="row">
            <div id="masthead" class="masthead small-12 medium-7 column">

                <!-- The button to toggles menu on small screen -->

                <h1><a href="../../../index.php"><img src="http://media.nngroup.com/static/img/logo-wordmark.png" width="74" height="30"> Nielsen <span class="norman">Norman</span> Group</a></h1>
                <p class="tagline show-for-medium-up">Evidence-Based User Experience Research, Training, and Consulting</p>

                <!-- The button to toggles form search on small screen -->


                <div style="clear: right;width:  100%;height: 1px;"></div><?php define('ROOT_PATH',$_SERVER["DOCUMENT_ROOT"] . '/apps/navigation_styles/navigations/4'); ?><?php include(ROOT_PATH)."/navigation.php"; ?>

                <!-- Drop down menu on small screen -->
                <div id="hideNav" class="show-for-small-down">
                  
                  <ul class="menu-content">
                    <li><a href="../../../index.php">Home</a></li>
                    <li class="has-submenu">
                      <a>Training</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../../training/index.php">Overview</a></li>
                        
<li id="nav-ux-conference"><a href="../../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../../online-seminars/index.php"><span>Online Seminars</span></a></li>

                      </ul>
                    </li>
                    <li class="has-submenu">
                      <a>Consulting</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
  <li><a href="../../../consulting/index.php">Overview</a></li>
  
  <li><a href="../../../consulting/benchmarking/index.php">Benchmarking</a></li>
  
  <li><a href="../../../consulting/expert-design-review/index.php">Expert Design Review</a></li>
  
  <li><a href="../../../consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
  
  <li><a href="../../../consulting/usability-testing/index.php">Usability Testing </a></li>
  
  <li><a href="../../../consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
  
  <li><a href="../../../in-house-training/index.php">In-House Training</a></li>
</ul>

                    </li>

                    <li><a href="../../../reports/index.php">Reports</a></li>
                    <li><a href="../../../articles/index.php">Articles</a></li>
                    <li class="has-submenu"><a>About NN/G</a><i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../../about/index.php">Overview</a></li>
                        <li><a href="../../../people/index.php">People</a></li>
                        <li><a href="../../../about/why-nng/index.php">Why NN/g?</a></li>
                        <li><a href="../../../about/contact/index.php">Contact</a></li>
                        <li><a href="../../index.php">News</a></li>
                        <li><a href="../../../about/history/index.php">History</a></li>
                        <li><a href="../../../books/index.php">Books</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                </div>

                <hr class="header-divider show-for-small-only"/>


            </div>
       </div>

        <div id="hideNav">            <nav class="primary-nav small-12 column">
                <ul>
                    <li><a class="navitem" id="nav-home" href="../../../index.php">Home</a></li>
                    <li><a class="navitem" id="nav-training" href="../../../training/index.php">Training</a></li>
                    <li><a class="navitem" id="nav-consulting" href="../../../consulting/index.php">Consulting</a></li>
                    <li><a class="navitem" id="nav-reports" href="../../../reports/index.php">Reports</a></li>
                    <li><a class="navitem" id="nav-articles" href="../../../articles/index.php">Articles</a></li>
                    <li><a class="navitem" id="nav-about" href="../../../about/index.php">About NN/G</a></li>
                </ul>
            </nav>
            <nav class="secondary-nav small-12 medium-12 large-12 columns">
                
<ul class="inline-list">
    <li id="nav-people"><a href="../../../people/index.php"><span>People</span></a></li>
    <li id="nav-whynng"><a href="../../../about/why-nng/index.php"><span>Why NN/g?</span></a></li>
    <li id="nav-contact"><a href="../../../about/contact/index.php"><span>Contact</span></a></li>
    <li id="nav-news"><a href="../../index.php"><span>News</span></a></li>
    <li id="nav-history"><a href="../../../about/history/index.php"><span>History</span></a></li>
    <li id="nav-books"><a href="../../../books/index.php"><span>Books</span></a></li>
</ul>

            </nav>
        </div>
    </header>
    <div class="l-content">
    


<div class="row">
    <div class="small-12 medium-4 large-4 columns offset-by-eight">
    </div>
</div>
<div class="row">
    <div class="small-12 medium-3 large-3 columns l-navrail news-rail hide-for-small-only">
        <h1><a href="../../index.php">All News Types</a></h1>
        <ul class="no-bullet">
            
                
                    <li><a href="../../type/announcements/index.php">Announcements (52)</a></li>
                
            
                
                    <li><a href="../../type/press-mentions/index.php">Interviews &amp; Press (225)</a></li>
                
            
        </ul>
    </div>
    <div class="small-12 medium-9 large-9 columns">
        <article class="news_detail">
            <div class="l-section-intro">
                <header>
                    <h1>NN/g News</h1>
                    <h2>2013 Intranet Design Award Winners </h2>
                    <p>January 6, 2013</p>
                </header>
            </div>
            <div class="l-subsection">
                <h1>
	Congratulations to the 2013 Intranet Design Annual Award winners!</h1>
<p>
	This year&rsquo;s best intranet teams comprise 330 members.</p>
<h2>
	Acorda Therapeutics, Inc.</h2>
<p>
	Extensive prep work led digital agency Klick to design a streamlined site that meets Acorda&rsquo;s needs as a growing company of busy employees. Understanding the user was key to translating tools and content into a site that retains the company culture and helps employees get their work done.</p>
<p>
	<strong><img alt="" src="http://media.nngroup.com.s3.amazonaws.com/media/editor/2013/01/02/01_Acorda_00_Team_Photo_reduced_.jpg" style="width: 460px; height: 306px;" /> </strong></p>
<p>
	<strong>Acorda Therapeutics, Inc., Klick Health team </strong> (from left to right): Peter Cheung, Heeyol Lee, Rob Brander, Michael Hambor, Cynthia Dahl, Sivasankaran Uthayasankaran, Mark Hadfield, Michael Melnick, Leerom Segal, Birgit Cole, Glen Webster, Neville Thomas, Armando Narvaez, Ari Schaefer, Mark Donaldson, and Andrew Middlemas.<br />
	&nbsp;</p>
<h2>
	American International Group, Inc. (AIG)</h2>
<p>
	The AIG intranet boasts clear navigation and exhaustive news article offerings to help communicate the organization&rsquo;s depth and unify all regions, companies, divisions, and individuals under one parent company.</p>
<p>
	<strong><img alt="" src="http://media.nngroup.com.s3.amazonaws.com/media/editor/2013/01/02/02_AIG_99_team-photo_.png" style="width: 460px; height: 306px;" /> </strong></p>
<p>
	<strong>AIG intranet team </strong> (front row, left to right): Pinkal Gandhi, Marie Villanueva, Victor Aviles, Keerthy Raghuveer, and Jechul Wee; (second row, left to right): Huy Lim, Bryce Lee, Sean Ryan, Lawrence Yee, and Brian Kirby; (back row, left to right): Richie Block and Frank Williams.<br />
	&nbsp;</p>
<h2>
	AT&amp;T</h2>
<p>
	Helping frontline employees provide quick answers to customers, the AT&amp;T My CSP knowledge management portal streamlines essential communications focused on making every second count. Clean design, smart details, video messages, and mobile access all contribute to educating and informing employees in a remarkably efficient way.</p>
<h3>
	<strong><img alt="" src="http://media.nngroup.com.s3.amazonaws.com/media/editor/2013/01/02/03_AT%26T-My-CSP-Team-EXPORT_.png" style="width: 460px; height: 550px;" /> </strong></h3>
<h3>
	<strong>AT&amp;T intranet team </strong></h3>
<p>
	<strong>Left tablet </strong> (left to right): Bea Adesina, Nickey Allen-Clayton, Jill Anders, Daniel Bailey, Scott Beck, Doug Beighley, Linda Blackmore-Ingram, Adam Boudreaux, John Bridges, Kevin Burns, Jessica Carter, Joanne Chao, Steve Chesterton, Amy Chisam, Raechelle Chisolm, Peter Chyan, Tiffany Cook, Travis Cook, Dorian Crawford, Carey Erickson, Charlena Fisher, Jeff Frye, Cheri Gaylord, Angie Gottfried, Debbie Granada, Ernie Guerra, Keeley Hale, Mary Harrison, Kat Helton, and Byron Henry.</p>
<p>
	<strong>Right tablet </strong> (left to right): Tracey Herring, Shannon Hughes, Steve Hyska, Jason Johnson, Jamie Jones, Scott Kanbara, Teri Kneppar, Ken Langston, Linda Licon, Peggy Marceaux, Lisa Meitzler, Christian Miller, Debbie Mulitsch, Todd Newport, Gina Nicolas, Dan Northington, Rachel Parker-Stephen, Russell Richardson, Christopher Rivera, Curtis Rogers, Jill Romeo, Bobby Shively, Sandy Shiver, and Chris Shurley.</p>
<p>
	<strong>Horizontal phone </strong> (left to right): Kathryn Smith, Trent Smith, Michelle Spontak, Michelle Thomas, Amy Turner, and Lori VanNess.</p>
<p>
	<strong>Vertical phone </strong> (left to right): Anna Waddell, Sandy West, Eric Williams, Steve Williams, Michael Willis, and Shane Wright.</p>
<p>
	<strong>Not pictured: </strong> Kayla Berry, Kevin Eagan, Brant Evans, and Jessica Walton.<br />
	&nbsp;</p>
<h2>
	Hager Group</h2>
<p>
	Designers instituted clear and creative space-saving options on the homepage that engage employees from the moment they feast their eyes on them. And information about colleagues and various site areas is accessible, helping employees collaborate productively.</p>
<p>
	<strong><img alt="" src="http://media.nngroup.com.s3.amazonaws.com/media/editor/2013/01/02/04_Hager_group_00_team_picture_reduced_.jpg" style="width: 460px; height: 306px;" /> </strong></p>
<p>
	<strong>Hager Group intranet team </strong> (back row, left to right): Martin Mosler, Thomas Lhuillier, David Pauly, G&eacute;rard Aubry, Philipp Alsfasser, Sandra Hartmann, and Stefan Kranz; (front row, left to right): Isabel Kr&auml;mer, Matthieu Fenger, Ang&eacute;lique Krupa, Christine Grosshans, Laure Battiston Apostolo, Philippe Dennler, Anna Bielak, Jean Gersbach, Henri Hasenfratz, and Julie Roiatti; (kneeling, left to right): Carole Ritter, and Mathias Templin. Missing from photo: Friedrich Eva, Kirsten Born, Michael Nezet, and Claire Freudenberger.<br />
	&nbsp;</p>
<h2>
	Luzerner Kantonalbank AG</h2>
<p>
	The Luzerner Kantonalbank AG (LUKB) intranet harnesses and exploits the power of metadata and taxonomy to ensure that the right kind of content automatically makes it to the right places. Marrying this with a calm, consistent design throughout makes this intranet an unusually supportive tool.</p>
<p>
	<strong><img alt="" src="http://media.nngroup.com.s3.amazonaws.com/media/editor/2013/01/02/05_LUKB_team_photo_reduced_.jpg" style="width: 460px; height: 338px;" /> </strong></p>
<p>
	<strong>Luzerner Kantonalbank AG intranet team </strong> (standing, left to right): Urs Wanner, Bernd Langkau, Pius Peter, Lars Kadner, Markus Spiekermann, Roger Baumeler, and Roger M&uuml;ller; (seated, left to right): Samuel Peyer, Esther Schwarzentruber, Lars Kadner, Peter Mitura, and Rudolf K&uuml;nzli.<br />
	&nbsp;</p>
<h2>
	ONO</h2>
<p>
	ONO&rsquo;s intranet team worked to create a SharePoint site in disguise via a custom design. Beyond the visuals, integrating a social networking and collaboration tool throughout the site turned a dated design at a company using email to communicate into an engaging site that encouraged collaboration and streamlined workflows, reflecting a corporate cultural change.</p>
<p>
	<strong><img alt="" src="http://media.nngroup.com.s3.amazonaws.com/media/editor/2013/01/02/06_ONO_team photo_reduced_.jpg" style="width: 460px; height: 306px;" /> </strong></p>
<p>
	<strong>ONO intranet team </strong> (from left to right): Alfredo Navarrete, Alicia Delgado, Mois&eacute;s Rodr&iacute;guez, Carmen Mart&iacute;nez, Sara Garc&iacute;a, and Carlos Fern&aacute;ndez.<br />
	&nbsp;</p>
<h2>
	Saudi Commission for Tourism and Antiquities (SCTA)</h2>
<p>
	With diverse, thorough content and unusual, whimsical features, the intranet for the Saudi Commission for Tourism and Antiquities far exceeds its goal to involve, include, and attract all employees.</p>
<p>
	<strong><img alt="" src="http://media.nngroup.com.s3.amazonaws.com/media/editor/2013/01/02/07_Saudi_Commission_team_Photo_.png" /> </strong></p>
<p>
	<strong>SCTA intranet team </strong> (from left to right): Hossam Almujallai, Anas Alsolai, Majed Alshedi, Abdullah Aljehani, Massar Almassar, Ali Abdulwahed, Mohammed Almazrooa, and Waqas Chaudhry.<br />
	&nbsp;</p>
<h2>
	Swiss Mobiliar Insurance &amp; Pensions</h2>
<p>
	Setting out to create a joyful intranet experience, the team at Swiss Mobiliar focused on usability and user experience throughout the redesign. The result is a site that lets users find content and company expertise, connect with others, and enjoy a personalized site experience.</p>
<p>
	<strong><img alt="" src="http://media.nngroup.com.s3.amazonaws.com/media/editor/2013/01/02/08_mobiliar_00_team_photo_.jpg" style="width: 460px; height: 347px;" /> </strong></p>
<p>
	<strong>Swiss Mobiliar Insurance &amp; Pensions (RedNet) team </strong> (top photo, left to right): Fabian Buser, Bj&ouml;rn B&ouml;ller, Franziska Buchenhorner, Martin Doriot, and Klaus Volken; (bottom photo, left to right) Carole Scheurer, Pia Gueblaoui, and Karin Simmen.<br />
	&nbsp;</p>
<h2>
	WorkSafeBC, the Worker&rsquo;s Compensation Board of British Columbia</h2>
<p>
	With only three months to improve the site, the WorkSafeBC team created a SharePoint 2010 site that informs and engages employees. The site boasts myriad opportunities for colleagues to compliment, encourage, and educate one another.</p>
<p>
	<strong><img alt="" src="http://media.nngroup.com.s3.amazonaws.com/media/editor/2013/01/02/09_WorkSafeBC - CIS team photo_.jpg" style="width: 460px; height: 306px;" /> </strong></p>
<p>
	<strong>WorkSafeBC, intranet team </strong> (back row, left to right): Trevor Seguin and Laine Dalby; (middle row, left to right): Chris Datcu, Lauralee Kuzik, Sharleen Gairdner, David Lesjak, and Glenda Troup; (front row, left to right): Joanna Gould and Terence Little.</p>
<p>
	&nbsp;</p>
<h2>
	XL Group plc</h2>
<p>
	A gutsy design, collaborative features, and the right content available when employees need it make this a marvelous intranet for today and for the future.</p>
<p>
	<strong><img alt="" src="http://media.nngroup.com.s3.amazonaws.com/media/editor/2013/01/02/10c_XL_3248 RETOUCHED bw2_reduced_cropped_.jpg" style="width: 460px; height: 265px;" /> </strong></p>
<p>
	<strong>XL Group core intranet team </strong> (back row, left to right): Marcy DeMassa, Satvinder Khurana, Shahar Tamari, Tom Paterno, Arvind Popli, William Piedmont, Meera Dharmalingam, Clint San Antonio, and Sangeeta Patel; (front row, left to right): Adam Worobec, Igor Dynkin, Chru Singh, John Flannery, Rich Williams, and Puneet Gupta.</p>
<p>
	<strong><img alt="" src="http://media.nngroup.com.s3.amazonaws.com/media/editor/2013/01/02/10a_XLGroup_Huge_Brooklyn_Team__.jpg" style="width: 460px; height: 306px;" /> </strong></p>
<p>
	<strong>The HUGE Brooklyn team </strong> (clockwise, left to right): Chris Michaud, Brock Boddie, Leala Abbott, Lilie Chang, and Felipe Memoria.</p>
<p>
	<strong><img alt="" src="http://media.nngroup.com.s3.amazonaws.com/media/editor/2013/01/02/10b_XLGroup_Huge_Rio_Team_.jpg" style="width: 460px; height: 222px;" /> </strong></p>
<p>
	<strong>The HUGE Rio team </strong> (left to right): Rafael Gomes, Junior Sim&otilde;es (&ldquo;Junimba&rdquo;), Jos&eacute; Roberto Coccorese (&ldquo;Zero&rdquo;), Sergio Salvador, Fabr&iacute;cio Marchezini, Jo&atilde;o Paulo Teixeira (&ldquo;JP&rdquo;), Leandro Brasil (&ldquo;Led&rdquo;), and Jo&atilde;o Mermolia.<br />
	<br />
	<a href="../../../reports/10-best-intranets-2013/index.php">2013 Intranet Design Annual details and purchase information </a></p>

            </div>
        </article>
    </div>
</div>


    </div>
    




<footer id="page-footer">
    <div class="row hide-for-print">
        <div class="column small-12 medium-6">
            <section class="footer-popular">
                
                <h1>Popular Topics</h1>
                <div style="float: left;  width: 15em; display: block;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../../topic/applications/index.php">Application Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../../topic/e-commerce/index.php">E-Commerce Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../../topic/eyetracking/index.php">Eyetracking </a></li>
	<li style="padding-bottom: .5em;"><a href="../../../topic/human-computer-interaction/index.php">Human Computer Interaction </a></li>
	<li style="padding-bottom: .5em;"><a href="../../../topic/information-architecture/index.php">Information Architecture </a></li>
	<li style="padding-bottom: .5em;"><a href="../../../topic/intranets/index.php">Intranet Design </a></li>
	<li><a href="../../../topic/interaction-design/index.php">Interaction Design </a></li>
</ul>
</div>

<div style="float: left;  width: 15em; display: block; list-style: none;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../../topic/mobile-and-tablet-design/index.php">Mobile and Tablet Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../../topic/credibility-persuasion/index.php">Persuasive Design</a></li>
	<li style="padding-bottom: .5em;"><a href="../../../topic/research-methods/index.php">Research Methods</a></li>
	<li style="padding-bottom: .5em;"><a href="../../../topic/user-testing/index.php">User Testing </a></li>
	<li style="padding-bottom: .5em;"><a href="../../../topic/web-usability/index.php">Web Usability </a></li>
	<li style="padding-bottom: .5em;"><a href="../../../topic/writing-web/index.php">Web Writing &amp; Content Strategy </a></li>
</ul>
</div>

<p>&nbsp;</p>

                
            </section>
        </div>

        <div class="column small-12 medium-6">
            <div class="row">
                <div class="column small-6">
                    <section class="footer-about">
                        <h1>Follow Us</h1>
                        <ul>
	<li><a href="../../../articles/subscribe/index.php">Subscribe to our weekly UX newsletter</a>,&nbsp;Jakob Nielsen&#39;s Alertbox</li>
	<li>Visit Don Norman&#39;s website, <a href="http://www.jnd.org/"> www.jnd.org </a></li>
	<li>Visit Bruce Tognazzini&#39;s website, <a href="http://www.asktog.com/"> www.asktog.com </a></li>
	<li style="padding-top: 6px;"><a class="twitter-follow-button" data-show-count="false" data-size="large" href="https://twitter.com/nngroup"><img alt="Follow @nngroup on Twitter" height="20" src="https://media.nngroup.com/media/editor/2016/12/30/twitter-follow-button.png" style="width: 117px; height: 20px;" width="117" /></a></li>
	<li style="padding-top: 6px;"><img alt="" height="14" src="https://media.nngroup.com/media/editor/2016/07/26/feed-icon-14x14.png" style="vertical-align:text-top;width:14px;margin-right:.25em;" width="14" /><a href="../../../feed/rss/index.php">Subscribe to our RSS feed</a></li>
</ul>

                    </section>
                </div>

                <div class="column small-6">
                    <section class="footer-about right-column">
                        <h1>About
</h1>
                        <ul class="no-bullet">
	<li><a href="../../../about/contact/index.php">Contact information</a></li>
	<li><a href="../../../about/index.php">About Us</a></li>
	<li><a href="../../../about/why-nng/index.php">Why NN/g? </a></li>
	<li><a href="../../../return-policy/index.php">Return Policy</a></li>
</ul>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="copyright column small-12">
            <p><a href="../../../copyright-and-reprint-info/index.php">Copyright</a> &copy; 1998-2017 Nielsen Norman Group, All Rights Reserved.</p>
        </div>
    </div>
</footer>


    


        <script src="https://media.nngroup.com/static/vendor/fastclick/js/fastclick.min.js"></script>
        <script src="https://media.nngroup.com/static/vendor/foundation/js/foundation.min.js"></script>
        <script src="http://media.nngroup.com/static/js/app.js"></script>
        <script src="http://media.nngroup.com/static/js/scroll-to-fixed.js"></script><script>var externalLinks = document.querySelectorAll('a[href^="http"]');for (var i = externalLinks.length-1; i >= 0; i--) {externalLinks[i].addEventListener("click", function(event) { alert("External links are disabled for this test."); event.preventDefault(); }, false);}var mailLinks = document.querySelectorAll('a[href^="mailto"]');for (var i = mailLinks.length-1; i >= 0; i--) {mailLinks[i].addEventListener("click", function(event) { alert("Email links are disabled for this test."); event.preventDefault(); }, false);}var externalForms = document.querySelectorAll('form');for (var i = externalForms.length-1; i >= 0; i--) {externalForms[i].addEventListener("submit", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false); externalForms[i].addEventListener("click", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false);}</script>

        <script>
            
            $(function() {
                $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
                $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
            });
        </script>

        


        
        <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1072699960;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072699960/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
    </body>

<!-- Mirrored from www.nngroup.com/news/item/2013-intranet-design-awards/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:13:16 GMT -->
</html>
