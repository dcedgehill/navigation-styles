<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->





    
<!-- Mirrored from www.nngroup.com/people-jakob-nielsen-publications/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:10:41 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1026.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"beacon":"bam.nr-data.net","queueTime":0,"applicationTime":90,"agent":"","licenseKey":"1a30ef9931","errorBeacon":"bam.nr-data.net","applicationID":"2048992","transactionName":"NFxSMRBUX0EDUkIPDA0WdhAMVkVbDV8ZFgIEXFIMFkYfRAtUQRVZM1hXADRcVEVMVlMS"}</script>
        <title>Jakob Nielsen&#39;s publication list</title><meta property="og:title" content="Jakob Nielsen&#39;s publication list" />
  
        
  
        
	
        

        

        <meta name="robots" content="noarchive">
        <meta name="viewport" content="width=device-width">

        <link rel="search" type="application/opensearchdescription+xml" title="NN/g Search" href="../opensearch.xml" />
        <link rel="shortcut icon" href="http://media.nngroup.com/static/img/favicon.ico" />

        <link rel="stylesheet" href="https://media.nngroup.com/static/vendor/font-awesome/css/font-awesome.min.css">

        
        <link rel="stylesheet" href="https://media.nngroup.com/static/css/7232a960f1df.css" type="text/css" />
        <link rel="stylesheet" href="http://fast.fonts.com/t/1.css?apiType=css&amp;projectid=c5846260-9def-46ec-9407-0f1349bd2d64" type="text/css">

        <script src="https://media.nngroup.com/static/vendor/modernizr/js/modernizr.min.js"></script>

        <script type="text/javascript" src="https://media.nngroup.com/static/js/3283be5def74.js"></script>
        

        

        
        
        <!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=229034,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('http://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->


<!-- Google Analytics Tracking Code for http://nngroup.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23132681-1', 'nngroup.com');
  ga('require', 'linkid', 'https://www.nngroup.com/people-jakob-nielsen-publications/linkid.js');
  ga('require', 'displayfeatures');

// Optimizely Universal Analytics Integration
 window.optimizely = window.optimizely || [];
 window.optimizely.push("activateUniversalAnalytics");

  ga('send', 'pageview');

</script>
<script>
/**
* Function to track a click on an outbound link in Google Analytics.
*/
function trackOutboundLink(label, url) {
    if(window.ga && ga.loaded) {
        ga('send', 'event', 'outbound', 'click', label, {'hitCallback': function(){document.location = url;}
        });
    } else {
        document.location = url;
    }
}
/**
* Function to track a click on a link in the article announcement area.
*/
$('#articleAnnouncement a').on('click', function(){
  ga('send', 'event', 'link', 'click', 'articleAnnouncement');
});
</script>

<script type="text/javascript">
 setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2421.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



<!-- Hotjar Tracking Code for http://nngroup.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:121605,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

        <script src="https://media.nngroup.com/static/js/analytics/track_outbound_link.js"></script>
        
        
        
    </head>
    <body class="location-about">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
    <header id="page-header">
        <div id="cart" class="shopping-cart mobile-cart show-for-small-down"></div>
        <div class="row">
            <div id="masthead" class="masthead small-12 medium-7 column">

                <!-- The button to toggles menu on small screen -->

                <h1><a href="../index.php"><img src="http://media.nngroup.com/static/img/logo-wordmark.png" width="74" height="30"> Nielsen <span class="norman">Norman</span> Group</a></h1>
                <p class="tagline show-for-medium-up">Evidence-Based User Experience Research, Training, and Consulting</p>

                <!-- The button to toggles form search on small screen -->


                <div style="clear: right;width:  100%;height: 1px;"></div><?php define('ROOT_PATH',$_SERVER["DOCUMENT_ROOT"] . '/apps/navigation_styles/navigations/4'); ?><?php include(ROOT_PATH)."/navigation.php"; ?>

                <!-- Drop down menu on small screen -->
                <div id="hideNav" class="show-for-small-down">
                  
                  <ul class="menu-content">
                    <li><a href="../index.php">Home</a></li>
                    <li class="has-submenu">
                      <a>Training</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../training/index.php">Overview</a></li>
                        
<li id="nav-ux-conference"><a href="../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../online-seminars/index.php"><span>Online Seminars</span></a></li>

                      </ul>
                    </li>
                    <li class="has-submenu">
                      <a>Consulting</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
  <li><a href="../consulting/index.php">Overview</a></li>
  
  <li><a href="../consulting/benchmarking/index.php">Benchmarking</a></li>
  
  <li><a href="../consulting/expert-design-review/index.php">Expert Design Review</a></li>
  
  <li><a href="../consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
  
  <li><a href="../consulting/usability-testing/index.php">Usability Testing </a></li>
  
  <li><a href="../consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
  
  <li><a href="../in-house-training/index.php">In-House Training</a></li>
</ul>

                    </li>

                    <li><a href="../reports/index.php">Reports</a></li>
                    <li><a href="../articles/index.php">Articles</a></li>
                    <li class="has-submenu"><a>About NN/G</a><i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../about/index.php">Overview</a></li>
                        <li><a href="../people/index.php">People</a></li>
                        <li><a href="../about/why-nng/index.php">Why NN/g?</a></li>
                        <li><a href="../about/contact/index.php">Contact</a></li>
                        <li><a href="../news/index.php">News</a></li>
                        <li><a href="../about/history/index.php">History</a></li>
                        <li><a href="../books/index.php">Books</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                </div>

                <hr class="header-divider show-for-small-only"/>


            </div>
       </div>

        <div id="hideNav">            <nav class="primary-nav small-12 column">
                <ul>
                    <li><a class="navitem" id="nav-home" href="../index.php">Home</a></li>
                    <li><a class="navitem" id="nav-training" href="../training/index.php">Training</a></li>
                    <li><a class="navitem" id="nav-consulting" href="../consulting/index.php">Consulting</a></li>
                    <li><a class="navitem" id="nav-reports" href="../reports/index.php">Reports</a></li>
                    <li><a class="navitem" id="nav-articles" href="../articles/index.php">Articles</a></li>
                    <li><a class="navitem" id="nav-about" href="../about/index.php">About NN/G</a></li>
                </ul>
            </nav>
            <nav class="secondary-nav small-12 medium-12 large-12 columns">
                
                
            </nav>
        </div>
    </header>
    <div class="l-content">
    
<div class="row">
    <div class="small-12 column l-subsection">
        <h1>Jakob Nielsen&#39;s Publication List</h1>

<p>&nbsp;</p>

<p><span style="font-size: 1.5em; margin-left: 75px;">Books</span></p>

<ol style="margin-left: 5em;">
	<li>Nielsen, J., and Budiu, R. (2012). <a href="../books/mobile-usability/index.php"> Mobile Usability</a>, New Riders Press, ISBN 0-321-88448-5.</li>
	<li>Nielsen, J., and Pernice, K. (2010). <a href="../books/eyetracking-web-usability/index.php"> <cite> Eyetracking Web Usability</cite></a>, New Riders Press, ISBN 0-321-49836-4.</li>
	<li>Nielsen, J., and Loranger, H. (2006). <a href="../books/prioritizing-web-usability/index.php"> <cite> Prioritizing Web Usability</cite></a>, New Riders Press, ISBN 0-321-35031-6.</li>
	<li>Nielsen, J., and Tahir, M. (2001). <a href="../books/homepage-usability/index.php"> <cite> Homepage Usability: 50 Websites Deconstructed</cite></a>. New Riders Publishing, Indianapolis, ISBN 0-73571-102-X.</li>
	<li>Nielsen, J., Molich, R., Snyder, C., and Farrell, S. (2001). <a href="../reports/e-commerce-user-experience/index.php"> <cite> E-Commerce User Experience</cite></a>. Nielsen Norman Group, Fremont, CA, ISBN 0-9706072-0-2.</li>
	<li>Nielsen, J. (1999). <a href="../books/designing-web-usability/index.php"> <cite> Designing Web Usability: The Practice of Simplicity</cite></a>. New Riders Publishing, Indianapolis, ISBN 1-56205-810-X. Translated into 21 additional languages.</li>
	<li>del Galdo, E., and Nielsen, J. (Eds.) (1996). <a href="../books/international-user-interfaces/index.php"> <cite> International User Interfaces</cite></a>. John Wiley &amp; Sons, New York, NY, ISBN 0-471-14965-9.</li>
	<li>Nielsen, J. (Ed.) (1995). <a href="../books/advances-in-human-computer-interaction/index.php"> <cite> Advances in HCI Vol. 5</cite>.</a> Ablex, Norwood, NJ, ISBN 1-56750-199-0 (hardcover), 1-56750-196-6 (softcover).</li>
	<li>Nielsen, J. (1995). <a href="../books/multimedia-and-hypertext/index.php"> <cite> Multimedia and Hypertext: The Internet and Beyond</cite></a>. AP Professional, Boston. ISBN 0-12-518408-5. German edition: <cite> Multimedia, Hypertext und Internet: Grundlagen und Praxis des elektronischen Publizierens</cite>, Vieweg Verlag, ISBN 3-528-05525-1. Excerpts: Chapter 3: The History of Hypertext; &nbsp;The Future of Hypertext.</li>
	<li>Nielsen, J., and Mack, R. L. (Eds.) (1994). <a href="../books/usability-inspection-methods/index.php"> <cite> Usability Inspection Methods</cite></a>. John Wiley &amp; Sons, New York, NY, ISBN 0-471-01877-5.</li>
	<li>Nielsen, J. (1993). <a href="../books/usability-engineering/index.php"> <cite> Usability Engineering</cite></a>. Academic Press, Boston, ISBN 0-12-518405-0 (hardcover), 0-12-518406-9 (softcover). Japanese translation ISBN 4-8101-9009-9.</li>
	<li>Nielsen, J. (1990). <a href="../books/hypertext-and-hypermedia/index.php"> <cite> Hypertext and Hypermedia</cite></a>. Academic Press, Boston, ISBN 0-12-518410-7 (hardcover), 0-12-518411-5 (paperback). Japanese translation ISBN 4-8337-8583-8.</li>
	<li>Nielsen, J. (Ed.) (1990). <a href="../books/designing-user-interfaces-international-use/index.php"> <cite> Designing User Interfaces for International Use</cite></a>. Elsevier Science Publishers, Amsterdam, ISBN 0-444-88428-9.</li>
	<li>Nielsen, J. (Ed.) (1989). <a href="../books/coordinating-user-interfaces-for-consistency/index.php"> <cite> Coordinating User Interfaces for Consistency</cite></a>. Academic Press, Boston, ISBN 0-12-518400-X. Reissued 2002 by Morgan Kaufmann Publishers, San Francisco, ISBN 0-12-518400-X.
	<p>&nbsp;</p>
	<span style="font-size: 1.5em;">Patents</span><br />
	<em>(All of these patents were invented from 1995 to 1997 when I worked at Sun Microsystems, so Oracle now owns the rights to the inventions, even for those patents that issued years later due to the slowness of the U.S. Patent Office.)</em></li>
	<li>Nielsen, J.: Adaptive meta-tagging of websites, <strong> U.S. Patent 7,636,732 </strong> (2009)</li>
	<li>Nielsen, J.: Latency-reducing bandwidth-prioritization for network servers and clients, <strong> U.S. Patent 6,968,379 </strong> (2005)</li>
	<li>Nielsen, J.: Internet-activated callback service, <strong> U.S. Patent 6,944,278 </strong> (2005)</li>
	<li>Nielsen, J.: Scroll bars with user feedback, <strong> U.S. Patent 6,882,354 </strong> (2005)</li>
	<li>Nielsen, J.: Tooltips on webpages, <strong> U.S. Patent 6,819,336 </strong> (2004)</li>
	<li>Nielsen, J.: Method and system for prioritized downloading of embedded web objects, <strong> U.S. Patent 6,789,075 </strong> (2004)</li>
	<li>Nielsen, J.: Method and system for escrowed backup of hotelled world wide web sites, <strong> U.S. Patent 6,778,668 </strong> (2004)</li>
	<li>Nielsen, J., Tognazzini, B., and Glass, R.: Eyetrack-driven illumination and information display, <strong> U.S. Patent 6,734,845 </strong> (2004)</li>
	<li>Nielsen, J.: Adaptive font sizes for network browsing, <strong> U.S. Patent 6,665,842 </strong> (2003)</li>
	<li>Nielsen, J.: Retrieving information from a broadcast signal, <strong> U.S. Patent 6,658,662 </strong> (2003)</li>
	<li>Tognazzini, B., Nielsen, J., and Glass, B.: Tilt-scrolling on the SunPad, <strong> U.S. Patent 6,624,824 </strong> (2003) [note that what we called a <em>SunPad </em>because we worked at Sun is what people today call a <em>tablet</em>]</li>
	<li>Tognazzini, B., and Nielsen, J.: Opaque screen visualizer, <strong> U.S. Patent 6,532,021 </strong> (2003)</li>
	<li>Nielsen, J.: System for managing and automatically deleting network address identified and stored during a network communication session when the network address is visited, <strong> U.S. Patent 6,510,461 </strong> (2003)</li>
	<li>Tognazzini, B., Nielsen, J., and Glass, R.: Transparent SunPad for home shopping, <strong> U.S. Patent 6,480,204 </strong> (2002) [note that what we called a <em>SunPad</em> because we worked at Sun is what people today call a <em>tablet</em>]</li>
	<li>Nielsen, J.: Method and apparatus for identifying and discarding junk electronic mail, <strong> U.S. Patent 6,453,327 </strong> (2002)</li>
	<li>Nielsen, J., Tognazzini, B., and Glass, R.: Method and apparatus for eyetrack-mediated downloading, <strong> U.S. Patent 6,437,758 </strong> (2002)</li>
	<li>Nielsen, J.: Method and system for updating email addresses, <strong> U.S. Patent 6,405,243 </strong> (2002)</li>
	<li>Nielsen, J.: Local sorting of downloaded tables, <strong> U.S. Patent 6,373,504 </strong> (2002)</li>
	<li>Nielsen, J.: Method and apparatus for facilitating popup links in a hypertext-enabled computer system, <strong> U.S. Patent 6,373,502 </strong> (2002)</li>
	<li>Nielsen, J.: Method and apparatus that processes a video signal to generate a random number generator seed, <strong> U.S. Patent 6,353,669 </strong> (2002)</li>
	<li>Nielsen, J.: Relevance-enhanced scrolling, <strong> U.S. Patent 6,339,437 </strong> (2002)</li>
	<li>Nielsen, J.: Visualizing degrees of information object attributes, <strong> U.S. Patent 6,337,699 </strong> (2002)</li>
	<li>Fogg, B.J., and Nielsen, J.: Re-linking technology for a moving web site, <strong> U.S. Patent 6,321,242 </strong> (2001)</li>
	<li>Nielsen, J.: Adaptive font sizes for network browsing, <strong> U.S. Patent 6,278,465 </strong> (2001)</li>
	<li>Glass, B., Nielsen, J., and Fogg, B.J.: Restoring broken links utilizing a spider process, <strong> U.S. Patent 6,253,204 </strong> (2001)</li>
	<li>Nielsen, J., and Fogg, B.J.: Techniques for navigating layers of a user interface, <strong> U.S. Patent 6,246,406 </strong> (2001)</li>
	<li>Nielsen, J.: Pre-scheduled callback service, <strong> U.S. Patent 6,212,268 </strong> (2001)</li>
	<li>Nielsen, J.: Method and apparatus for displaying information on a computer controlled display device, <strong> U.S. Patent 6,199,080 </strong> (2001)</li>
	<li>Nielsen, J.: Method and apparatus for archiving hypertext documents, <strong> U.S. Patent 6,199,071 </strong> (2001)</li>
	<li>Nielsen, J.: Password helper using a client-side master password which automatically presents the appropriate server-side password in a particular remote server, <strong> U.S. Patent 6,182,229 </strong> (2001)</li>
	<li>Fogg, B.J., and Nielsen, J.: Probabilistic web link viability marker and web page ratings, <strong> U.S. Patent 6,163,778 </strong> (2000)</li>
	<li>Nielsen, J., and Fogg, B.J.: Techniques for navigating layers of a user interface, <strong> U.S. Patent 6,147,684 </strong> (2000)</li>
	<li>Nielsen, J.: System for reminding a sender of an email if recipient of the email does not respond by a selected time set by the sender, <strong> U.S. Patent 6,108,688 </strong> (2000)</li>
	<li>Nielsen, J.: Tooltips on webpages, <strong> U.S. Patent 6,078,935 </strong> (2000)</li>
	<li>Nielsen, J.: Method and system for efficient organization of selectable elements on a graphical user interface, <strong> U.S. Patent 6,069,625 </strong> (2000)</li>
	<li>Nielsen, J.: Subscribed update monitors, <strong> U.S. Patent 6,055,570 </strong> (2000)</li>
	<li>Nielsen, J.: Apparatus and method for displaying enhanced hypertext link anchor information regarding hypertext page availability and content, <strong> U.S. Patent 6,021,435 </strong> (2000)</li>
	<li>Nielsen, J.: Password helper using a client-side master password which automatically presents the appropriate server-side password to a particular remote server, <strong> U.S. Patent 6,006,333 </strong> (1999)</li>
	<li>Nielsen, J.: Method and system for efficient organization of selectable elements on a graphical user interface, <strong> U.S. Patent 6,005,567 </strong> (1999)</li>
	<li>Nielsen, J.: Automatic development and display of context information in structured documents on the world wide web, <strong> U.S. Patent 6,003,046 </strong> (1999)</li>
	<li>Nielsen, J.: Method and system for regulating discounts on merchandise distributed through networked computer systems, <strong> U.S. Patent 6,002,771 </strong> (1999)</li>
	<li>Nielsen, J.: Method and apparatus for detecting and presenting client side image map attributes including sound attributes using page layout data strings, <strong> U.S. Patent 5,991,781 </strong> (1999)</li>
	<li>Nielsen, J.: Method and apparatus for printing a hyperspacial document, <strong> U.S. Patent 5,991,514 </strong> (1999)</li>
	<li>Nielsen, J.: Internet-based spelling checker dictionary system with automatic updating, <strong> U.S. Patent 5,970,492 </strong> (1999)</li>
	<li>Nielsen, J.: Method, apparatus and program product for updating visual bookmarks, <strong> U.S. Patent 5,963,964 </strong> (1999)</li>
	<li>Nielsen, J., Johnson, E., and Gentner, D.R.: Method and system for facilitating access to selectable elements on a graphical user interface, <strong> U.S. Patent 5,963,950 </strong> (1999)</li>
	<li>Nielsen, J., Tognazzini, B., and Glass, R.: Method and system for providing relevance-enhanced image reduction in computer systems, <strong> U.S. Patent 5,960,126 </strong> (1999)</li>
	<li>Nielsen, J.: Fingerprinting plain text information, <strong> U.S. Patent 5,953,415 </strong> (1999)</li>
	<li>Nielsen, J.: Method and system for facilitating the exchange of information between human users in a networked computer system, <strong> U.S. Patent 5,948,054 </strong> (1999)</li>
	<li>Nielsen, J.: Tooltips on webpages, <strong> U.S. Patent 5,937,417 </strong> (1999)</li>
	<li>Nielsen, J.: Method and apparatus for obtaining and displaying network server information, <strong> U.S. Patent 5,925,106 </strong> (1999)</li>
	<li>Nielsen, J.: Client-side, server-side and collaborative spell check of URLs, <strong> U.S. Patent 5,907,680 </strong> (1999)</li>
	<li>Nielsen, J.: Processing HTML to embed sound in a web page, <strong> U.S. Patent 5,903,727 </strong> (1999)</li>
	<li>Nielsen, J.: Style sheets for speech-based presentation of web pages, <strong> U.S. Patent 5,899,975 </strong> (1999)</li>
	<li>Tognazzini, B., Nielsen, J., and Glass, R.: Method and apparatus for eyetrack-driven captioning, <strong> U.S. Patent 5,898,423 </strong> (1999)</li>
	<li>Nielsen, J.: Method and system for efficient organization of selectable elements on a graphical user interface, <strong> U.S. Patent 5,897,670 </strong> (1999)</li>
	<li>Nielsen, J.: Methods and apparatus for fixed canvas presentations detecting canvas specifications including aspect ratio specifications within HTML data streams, <strong> U.S. Patent 5,897,644 </strong> (1999)</li>
	<li>Nielsen, J.: Spell checking universal resource locator (URL) by comparing the URL against a cache containing entries relating incorrect URLs submitted by users to corresponding correct URLs, <strong> U.S. Patent 5,892,919 </strong> (1999)</li>
	<li>Nielsen, J.: Estimating the degree of change of web pages, <strong> U.S. Patent 5,890,164 </strong> (1999)</li>
	<li>Tognazzini, B., Nielsen, J., and Glass, R.: Method and apparatus for eyetrack-driven information retrieval, <strong> U.S. Patent 5,886,683 </strong> (1999)</li>
	<li>Nielsen, J.: Internet-based spelling checker dictionary system with automatic updating, <strong> U.S. Patent 5,875,443 </strong> (1999)</li>
	<li>Nielsen, J.: Method and apparatus for altering sent electronic mail messages, <strong> U.S. Patent 5,870,548 </strong> (1999)</li>
	<li>Nielsen, J.: Method and apparatus for managing subscriptions to distribution lists, <strong> U.S. Patent 5,864,684 </strong> (1999)</li>
	<li>Nielsen, J.: Prospective view for web backtrack, <strong> U.S. Patent 5,854,630 </strong> (1998)</li>
	<li>Nielsen, J., and Mankoski, A.: Method and apparatus for allowing a user to select from a set of mutually exclusive options, <strong> U.S. Patent 5,845,122 </strong> (1998)</li>
	<li>Nielsen, J., Tognazzini, B., and Glass, R.: Eyetrack-driven illumination and information display, <strong> U.S. Patent 5,835,083 </strong> (1998)</li>
	<li>Tognazzini, B., Nielsen, J., and Glass, R.: X-Y viewport scroll using location of display with respect to a point, <strong> U.S. Patent 5,831,614 </strong> (1998)</li>
	<li>Tognazzini, B., Nielsen, J., and Glass, R.: Method and apparatus for eyetrack derived backtrack, <strong> U.S. Patent 5,831,594 </strong> (1998)</li>
	<li>Nielsen, J.: Method and system for prioritized downloading of embedded web objects, <strong> U.S. Patent 5,826,031 </strong> (1998)</li>
	<li>Nielsen, J.: Method and apparatus for receiving electronic mail, <strong> U.S. Patent 5,826,022 </strong> (1998)</li>
	<li>Nielsen, J.: Method and apparatus for automatic generaton of documents with single-layered backgrounds from documents with multi-layered backgrounds, <strong> U.S. Patent 5,819,302 </strong> (1998)</li>
	<li>Nielsen, J.: Automatic updates of bookmarks in a client computer, <strong> U.S. Patent 5,813,007 </strong> (1998)</li>
	<li>Nielsen, J.: Method and system for escrowed backup of hotelled world wide web sites, <strong> U.S. Patent 5,812,398 </strong> (1998)</li>
	<li>Nielsen, J.: Method and system for resizing the subtitles of a video, <strong> U.S. Patent 5,805,153 </strong> (1998)</li>
	<li>Nielsen, J.: System and method for temporally varying pointer icons, <strong> U.S. Patent 5,784,056 </strong> (1998)</li>
	<li>Nielsen, J., and Tognazzini, B.: Videostream management system, <strong> U.S. Patent 5,778,137 </strong> (1998)</li>
	<li>Nielsen, J.: Method and apparatus that processes a video signal to generate a random number generator seed, <strong> U.S. Patent 5,774,549 </strong> (1998)</li>
	<li>Nielsen, J.: Method and apparatus for combining truncated hyperlinks to form a hyperlink aggregate, <strong> U.S. Patent 5,761,436 </strong> (1998)</li>
	<li>Tognazzini, B., Nielsen, J., and Glass, R.: Method and apparatus for eyetrack-driven text enlargement, <strong> U.S. Patent 5,731,805 </strong> (1998)</li>
	<li>Nielsen, J.: Method and system for implementing hypertext scroll attributes, <strong> U.S. Patent 5,659,729 </strong> (1997)
	<p>&nbsp;</p>
	<span style="font-size: 1.5em;">Papers in Refereed Journals and Prestigious Conferences</span><br />
	<em>Please note that I cannot supply reprints of these papers. A few of the papers (linked below) are available in online versions. </em></li>
	<li>Nielsen, J. (2005). Usability for the Masses. <cite> Journal of Usability Studies </cite> <strong> 1</strong>, 1 (Nov), 2-3. ( <a href="http://www.upassoc.org/upa_publications/jus/2005_november/nielsen.pdf"> Online as PDF file</a>.)</li>
	<li>Nielsen, J. (1999). User interface directions for the Web. <cite> Communications of the ACM </cite> <strong> 42</strong>, 1 (January), 65-72.</li>
	<li>Gentner, D., and Nielsen, J. (1996). <a href="../articles/anti-mac-interface/index.php"> The Anti-Mac interface</a>. <cite> Communications of the ACM </cite> <strong> 39</strong>, 6 (August), 70-82.</li>
	<li>Nielsen, J., and Faber, J. M. (1996). <a href="../articles/parallel-design/index.php"> Improving System Usability Through Parallel Design</a>. <cite> IEEE Computer </cite> , <strong> 29</strong>, 2 (February), 29-35.</li>
	<li>Nielsen, J. (1995). The electronic business card: An experiment in half-dead hypertext. <cite> The New Review of Hypermedia and Multimedia </cite> <strong> 1</strong>, 155-168.</li>
	<li>Nielsen, J. (1994). Estimating the number of subjects needed for a thinking aloud test. <cite> International Journal of Human-Computer Studies </cite> <strong> 41</strong>, 3 (September), 385-397.</li>
	<li>Nielsen, J. (1994). Enhancing the explanatory power of usability heuristics. <cite> Proc. ACM CHI&#39;94 Conf. </cite> (Boston, MA, April 24-28), 152-158.</li>
	<li>Nielsen, J., and Levy, J. (1994). Measuring usability - preference vs. performance. <cite> Communications of the ACM </cite> <strong> 37</strong>, 4 (April), 66-75.</li>
	<li>Nielsen, J. (1993). <a href="../articles/iterative-design/index.php"> Iterative user interface design </a> . <cite> IEEE Computer </cite> <strong> 26</strong>, 11 (November), 32-41.</li>
	<li>Nielsen, J., and Schaefer, L. (1993). Sound effects as an interface element for older users. <cite> Behaviour &amp; Information Technology </cite> <strong> 12</strong>, 4, 208-215.</li>
	<li>Nielsen, J., and Landauer, T. K. (1993). A mathematical model of the finding of usability problems. <cite> Proc. ACM INTERCHI&#39;93 Conf. </cite> (Amsterdam, the Netherlands, 24-29 April), 206-213.</li>
	<li>Nielsen, J., and Phillips, V. L. (1993). Estimating the relative usability of two interfaces: Heuristic, formal, and empirical methods compared. <cite> Proc. ACM INTERCHI&#39;93 Conf. </cite> (Amsterdam, the Netherlands, 24-29 April), 214-221.</li>
	<li>Nielsen, J. (1993). <a href="../articles/noncommand/index.php"> Noncommand user interfaces</a>. Communications of the ACM <strong> 36</strong>, 4 (April), 83-99.</li>
	<li>Brothers, L., Hollan, J., Nielsen, J., Stornetta, S., Abney, S., Furnas, G., and Littman, M. (1992). Supporting informal communication via ephemeral interest groups. <cite> Proc. ACM CSCW&#39;92 Conf. Computer-Supported Cooperative Work </cite> (Toronto, Canada, 1-4 November), 84-90.</li>
	<li>Nielsen, J. (1992). Finding usability problems through heuristic evaluation. <cite> Proc. ACM CHI&#39;92 </cite> (Monterey, CA, 3-7 May), 373-380.</li>
	<li>Nielsen, J., Bush, R. M., Dayton, T., Mond, N. E., Muller, M. J., and Root, R. W. (1992). Teaching experienced developers to design graphical user interfaces. <cite> Proc. ACM CHI&#39;92 </cite> (Monterey, CA, 3-7 May), 557-564.</li>
	<li>Nielsen, J. (1992). The usability engineering life cycle. <cite> IEEE Computer </cite> <strong> 25</strong>, 3 (March), 12-22.</li>
	<li>Nielsen, J., Frehr, I., and Nymand, H. O. (1991). The learnability of HyperCard as an object-oriented programming system. <cite> Behaviour &amp; Information Technology </cite> <strong> 10</strong>, 2 (March-April), 111-120.</li>
	<li>Thovtrup, H., and Nielsen, J. (1991). <a href="../articles/assessing-usability-user-interface-standard/index.php"> Assessing the usability of a user interface standard</a>. <cite> Proc. ACM CHI&#39;91 Conf. Human Factors in Computing Systems </cite> (New Orleans, LA, 28 April-2 May), 335-341.</li>
	<li>Nielsen, J. (1990). Miniatures versus icons as a visual cache for videotex browsing. <cite> Behaviour &amp; Information Technology </cite> <strong> 9</strong>, 6 (Nov.-Dec.), 441-449.</li>
	<li>Nielsen, J. (1990). Traditional dialogue design applied to modern user interfaces. <cite> Communications of the ACM </cite> <strong> 33</strong>, 10 (October), 109-118.</li>
	<li>Nielsen, J. (1990). A meta-model for interacting with computers. <cite> Interacting with Computers </cite> <strong> 2</strong>, 2 (August), 147-160.</li>
	<li>Nielsen, J., and Molich, R. (1990). Heuristic evaluation of user interfaces. <cite> Proc. ACM CHI&#39;90 </cite> (Seattle, WA, 1-5 April), 249-256.</li>
	<li>Nielsen, J. (1990). The art of navigating through hypertext. <cite> Communications of the ACM </cite> <strong> 33</strong>, 3 (March), 296-310.</li>
	<li>Molich, R., and Nielsen, J. (1990). Improving a human-computer dialogue. <cite> Communications of the ACM </cite> <strong> 33</strong>, 3 (March), 338-348.</li>
	<li>Andersen, M. H., Nielsen, J., and Rasmussen, H. (1989). A similarity-based hypertext browser for reading the Unix network news. <cite> Hypermedia </cite> <strong> 1</strong>, 3, 255-265.</li>
	<li>Nielsen, J. (1989). The matters that really matter for hypertext usability. <cite> Proc. ACM Hypertext&#39;89 Conf. </cite> (Pittsburgh, PA, 5-8 November), 239-248.</li>
	<li>Nielsen, J. (1989). What do users really want? <cite> Int.J. Human-Computer Interaction </cite> <strong> 1</strong>, 2, 137-147.</li>
	<li>Nielsen, J., and Richards, J. T. (1989). The experience of learning and using Smalltalk. <cite> IEEE Software </cite> <strong> 6</strong>, 3 (May), 73-77.</li>
	<li>Nielsen, J. (1986). A virtual protocol model for computer-human interaction. <cite> International Journal of Man-Machine Studies </cite> <strong> 24 </strong> (1986), 301-312.</li>
	<li>Nielsen, J., Mack, R. L., Bergendorff, K. H., and Grischkowsky, N. L. (1986). Integrated software in the professional work environment: Evidence from questionnaires and interviews. <cite> Proceedings of the Association for Computing Machinery Conference Human Factors in Computing Systems CHI&#39;86 </cite> (Boston, 13-17 April), 162-167.
	<p>&nbsp;</p>
	<span style="font-size: 1.5em;">Other Published Papers</span></li>
	<li><a href="../articles/author/jakob-nielsen/index.php">Alertbox column</a> published on the Web since June 1995.</li>
	<li>Norman, D., and Nielsen, J. (2010). <a href="http://www.jnd.org/dn.mss/gestural_interfaces_a_step_backwards_in_usability_6.php"> Gestural Interfaces: A Step Backwards in Usability</a>, <cite> Interactions </cite> <strong> 12</strong>, 5 (September 2010).</li>
	<li>Tognazzini, B., and Nielsen, J. (2001). <a href="http://www.eweek.com/c/a/Web-Services-Web-20-and-SOA/Beyond-the-Browser/"> Beyond the Browser</a>, <cite> Inter@ctive Week</cite>, March 26, 2001.</li>
	<li>Nielsen, J., and Coyne, K.P. (2001). A Useful Investment: Usability testing costs - but it pays for itself in the long run, <cite> CIO Magazine </cite> , February 15, 2001</li>
	<li>Nielsen, J., and Tahir, M. (2001). <a href="http://www.drdobbs.com/building-web-sites-with-depth-web-techni/184413518"> Building Web Sites With Depth</a>, <cite> WebTechniques</cite>, February 2001.</li>
	<li>Nielsen, J., Coyne, K.P., and Tahir, M. (2001). <a href="http://www.pcmag.com/article2/0,2817,33821,00.asp"> Make your site usable</a>, <cite> PC Magazine</cite>, January 2001.</li>
	<li>Nielsen, J., and Tahir, M. (2000). Keep Your Users In Mind: Incorporating Feedback at Several Stages into an Ongoing Design Process Will Help a Site to Thrive, <cite> InternetWorld</cite>, December 15, 2000.</li>
	<li>Nielsen, J. and Norman, D.A. (2000) <a href="http://www.jnd.org/dn.mss/usability_is_not_a_l.php"> Web-Site Usability: Usability on The Web Isn&#39;t A Luxury</a>, <em>InformationWeek</em>, January 14, 2000.</li>
	<li>Nielsen, J. (1998). Sun&#39;s New Web Design, www.sun.com cover story for January 13, 1998: http://www.sun.com/980113/sunonnet/</li>
	<li>Nielsen, J. (1997). Learning from the real world. <cite> IEEE Software </cite> <strong> 14</strong>, 4 (July), 98-99.</li>
	<li>Nielsen, J. (1997). Something is better than nothing. <cite> IEEE Software </cite> <strong> 14</strong>, 4 (July), 27-28.</li>
	<li>Nielsen, J. (1997). Usability testing. In Salvendy, G. (Ed.), <a href="http://www.amazon.com/exec/obidos/ISBN%3D0471116904/useitcomusableinA/"> <cite> Handbook of Human Factors and Ergonomics, Second Edition</cite></a>, John Wiley &amp; Sons. 1543-1568.</li>
	<li>Nielsen, J. (1997). Let&#39;s Ask the Users. <cite> IEEE Software </cite> <strong> 14</strong>, 3 (May), 110-111.</li>
	<li>Nielsen, J. (1997). <a href="../articles/guidelines-for-multimedia-on-the-web/index.php"> Guidelines for multimedia on the Web</a>. World Wide Web Consortium&#39;s <cite> World Wide Web Journal </cite> <strong> 2</strong>, 1 (Winter). Invited paper for the special issue on &quot;Advancing HTML: Style and Substance&quot;.</li>
	<li>Nielsen, J. (1997). Usability engineering. In Tucker, A. B. (Ed.), <a href="http://www.amazon.com/exec/obidos/ISBN%3D0849329094/useitcomusableinA/"> <cite> The Computer Science and Engineering Handbook</cite></a> , CRC Press. 1440-1460.</li>
	<li>Nielsen, J. (1997). <a href="../articles/focus-groups/index.php"> The use and misuse of focus groups</a>. <cite> IEEE Software </cite> <strong> 14</strong>, 1 (January), 94-95.</li>
	<li>Nielsen, J. (1996). Usability metrics: Tracking interface improvements. <cite> IEEE Software </cite> <strong> 13</strong>, 6 (November), 12-13.</li>
	<li>Nielsen, J. (1996). <a href="http://www.nytimes.com/library/cyber/week/0920candidates.php"> A Web design expert reviews the candidates&#39; sites</a>. <cite> The New York Times</cite>, September 20, 1996. http://www.nytimes.com/library/cyber/week/0920candidates.php</li>
	<li>Nielsen, J. (1996). Designing to seduce the user. <cite> IEEE Software </cite> <strong> 13</strong>, 5 (September), 18-20.</li>
	<li>Nielsen, J. (1996). Going global with user testing. <cite> IEEE Software </cite> <strong> 13</strong>, 4 (July), 129-130.</li>
	<li>Nielsen, J. (1996). Putting the user in user-interface testing. <cite> IEEE Software </cite> <strong> 13</strong>, 3 (May), 89-90.</li>
	<li>Nielsen, J. (1996). <a href="../articles/file-death/index.php"> The impending demise of file systems</a>. <cite> IEEE Software </cite> <strong> 13</strong>, 2 (March).</li>
	<li>Nielsen, J. (1996). The importance of being beautiful. <cite> IEEE Software </cite> <strong> 13</strong>, 1 (January).</li>
	<li>Nielsen, J. (1995). Using paper prototypes in home-page design. <cite> IEEE Software </cite> <strong> 12</strong>, 4 (July), 88-89, 97.</li>
	<li>Nielsen, J. (1995). A home-page overhaul using other Web sites. <cite> IEEE Software </cite> <strong> 12</strong>, 3 (May), 75-78.</li>
	<li>Nielsen, J., and Bergman, M. (1995). Independent iterative design: A method that didn&#39;t work. In Nielsen, J. (Ed.), <cite> Advances in HCI Vol. 5 </cite> , Ablex. 235-248.</li>
	<li>Nielsen, J. (1995). Scenarios in discount usability engineering. In Carroll, J. M. (Ed.), <a href="http://www.amazon.com/exec/obidos/ISBN%3D0471076597/useitcomusableinA/"> <cite> Scenario-Based Design</cite></a>, John Wiley &amp; Sons, New York, 59-83.</li>
	<li>Nielsen, J. (1995). Applying discount usability engineering. <cite> IEEE Software </cite> <strong> 12</strong>, 1 (January), 98-100.</li>
	<li>Nielsen, J. (1994). As they may work. <cite> ACM interactions </cite> <strong> 1</strong>, 4 (October), 19-24.</li>
	<li>Nielsen, J., and Sano, D. (1994). <a href="../articles/1994-design-sunweb-sun-microsystems-intranet/index.php"> SunWeb: User interface design for Sun Microsystem&#39;s internal web</a>. <cite> Proc. 2nd World Wide Web Conf. &#39;94: Mosaic and the Web </cite> (Chicago, IL, October 17-20), 547-557.</li>
	<li>Nielsen, J. (1994). Why GUI panic is good panic. <cite> ACM interactions </cite> <strong> 1</strong>, 2 (April), 55-58.</li>
	<li>Nielsen, J. (1994). <a href="../articles/guerrilla-hci/index.php"> Guerrilla HCI: Using discount usability engineering to penetrate the intimidation barrier</a>. In Bias, R. G., and Mayhew, D. J. (Eds.), <a href="http://www.amazon.com/exec/obidos/ISBN%3D0120958104/useitcomusableinA/"> <cite> Cost-Justifying Usability</cite></a> . Academic Press, Boston, MA, 245-272.</li>
	<li>Nielsen, J. (1994). <a href="../articles/usability-labs/index.php"> Usability laboratories</a>. <cite> Behaviour &amp; Information Technology </cite> <strong> 13</strong>, 1&amp;2, 3-8.</li>
	<li>Nielsen, J. (1993). Is usability engineering really worth it? <cite> IEEE Software </cite> <strong> 10</strong>, 6 (November) 90-92.</li>
	<li>Mack, R., and Nielsen, J. (1993). Usability inspection methods. <cite> ACM SIGCHI Bulletin </cite> <strong> 25</strong>, 1 (January), 28-33.</li>
	<li>Dumais, S.T., and Nielsen, J. (1992). Automating the assignment of submitted manuscripts to reviewers. <cite> Proc. ACM SIGIR&#39;92 </cite> 15th International Conference on Research and Development in Information Retrieval (Copenhagen, Denmark, 21-24 June), 233-244.</li>
	<li>Nielsen, J. (1992). Evaluating the thinking aloud technique for use by computer scientists. In Hartson, H. R. and Hix, D. (Eds.): <cite> Advances in Human-Computer Interaction Vol. 3 </cite> , Ablex. 69-82.</li>
	<li>Nielsen, J. (1991). Usability considerations in introducing hypertext. In Brown, H. (Ed.), <cite> Hypermedia/Hypertext and Object-Oriented Databases </cite> , Chapman &amp; Hall, London, 3-15.</li>
	<li>Kain, H., and Nielsen, J. (1991). Estimating the market diffusion curve for hypertext. <cite> Impact Assessment Bulletin </cite> <strong> 9</strong>, 1-2 (Spring), 145-157.</li>
	<li>Nielsen, J. (1990). <a href="../articles/cdrom/index.php"> Three medium-sized hypertexts on CD-ROM</a>. <cite> ACM SIGIR Forum </cite> <strong> 24</strong>, 1, 2-10.</li>
	<li>Nielsen, J. (1990). Paper versus computer implementations as mockup scenarios for heuristic evaluation. <cite> Proc. INTERACT&#39;90 3rd IFIP Conf. Human-Computer Interaction </cite> (Cambridge, U.K., 27-31 August), 315-320.</li>
	<li>Nielsen, J. (1990). Usability testing of international interfaces. In Nielsen, J. (Ed.), <a href="../books/designing-user-interfaces-international-use/index.php"> <cite> Designing User Interfaces for International Use</cite></a>, Elsevier Science Publishers, Amsterdam, 39-44.</li>
	<li>Nielsen, J. (1990). Big paybacks from &#39;discount&#39; usability engineering. <cite> IEEE Software </cite> <strong> 7</strong>, 3 (May), 107-108.</li>
	<li>Nielsen, J. (1990). International user interfaces: An exercise. <cite> ACM SIGCHI Bulletin </cite> <strong> 21</strong>, 4 (April), 50-51.</li>
	<li>Nielsen, J. (1990). Evaluating hypertext usability. In Jonassen, D.H., and Mandl, H. (Eds.), <cite> Designing Hypertext/Hypermedia for Learning </cite> , Springer- Verlag, Heidelberg, Germany, 147-168. (Paper presented at the NATO Advanced Research Workshop on Designing Hypertext/Hypermedia for Learning, T&Yuml;bingen, West Germany, 3-7 July 1989).</li>
	<li>Nielsen, J., and Lyngbaek, U. (1990). Two field studies of hypermedia usability. In McAleese, R., and Green, C. (Eds.), <cite> Hypertext: State of the Art </cite> , Ablex, 64-72. (Paper presented at Hypertext&#39;2, York, United Kingdom, 29-30 June 1989)</li>
	<li>Nielsen, J. (1989). Usability engineering at a discount. In Salvendy, G. and Smith, M.J. (Eds.), <cite> Designing and Using Human-Computer Interfaces and Knowledge Based Systems </cite> , Elsevier Science Publishers, Amsterdam, 394-401.</li>
	<li>Nielsen, J. (1989). Prototyping user interfaces using an object-oriented hypertext programming system. <cite> Proc. NordDATA&#39;89 </cite> Joint Scandinavian Computer Conference (Copenhagen, Denmark, 19-22 June), 485-490.</li>
	<li>Nielsen, J. (1989). Hypertext bibliography. <cite> Hypermedia </cite> <strong> 1</strong>, 1 (Spring), 74-91.</li>
	<li>Nielsen, J. (1987). Classification of dialog techniques. <cite> ACM SIGCHI Bulletin </cite> <strong> 19</strong>, 2 (October), 30-35.</li>
	<li>Nielsen, J. (1987). A user interface case study of the Macintosh. In Salvendy, G. (Ed.), <cite> Cognitive Engineering in the Design of Human-Computer Interaction and Expert Systems </cite> . Amsterdam: Elsevier Science Publishers. 241-248.</li>
	<li>Nielsen, J. (1987). Using scenarios to develop user friendly videotex systems. <cite> Proceedings NordDATA&#39;87 </cite> Joint Scandinavian Computer Conference (Trondheim, Norway, 15-18 June), 133-138.</li>
	<li>Nielsen, J. (1986). Online documentation and reader annotation. <cite> Proceedings of the International Conference on Work With Display Units </cite> (Stockholm, 12-15 May), 526-529.</li>
	<li>Nielsen, J. (1985). Learning difficulties of programmers faced with a new programming environment. <cite> Proceedings of IEEE COMPINT&#39;85 </cite> Conference on Computer Aided Technologies (Montr&eacute;al, Canada, 9-12 September).
	<p>&nbsp;</p>
	<span style="font-size: 1.5em;">Panel Statements and Other Published Position Papers </span></li>
	<li>Czerwinski, M., Mountford, S.J., Nielsen, J., Tognazzini, T., and Instone, K. (1997). Web interfaces live: what&#39;s hot, what&#39;s not? <cite> ACM CHI&#39;97 Extended Abstracts </cite> (Atlanta, GA, March 22-27), 103-104.</li>
	<li>Nielsen, J., Shafrir, E., Niederst, J., Rosenberg, J., and Wanliss-Orlebar, A. (1995). Cool or content: Design strategies for a million-site Web. <cite> World Wide Web Consortium WWW&#39;4 Conf. </cite> (Boston, MA, December 11-14).</li>
	<li>Card, S., Gentner, D., Nielsen, J., Henderson, A., and Norman, D. (1995). <a href="http://www.acm.org/sigchi/chi95/proceedings/panels/gen_bdy.htm"> The Anti-Mac: Violating the Macintosh human interface guidelines</a>. <cite> ACM CHI&#39;95 Conference Companion </cite> (Denver, CO, May 7-11), 183-184.</li>
	<li>Nielsen, J., Fernandes, T., Wagner, A., Wolf, R., and Ehrlich, K. (1994). Diversified parallel design: Contrasting design aproaches. <cite> ACM CHI&#39;94 Conference Companion </cite> (Boston, MA, April 24-28).</li>
	<li>Leventhal, L., Teasley, B., Stone, D., Lancaster, A-M., Marcus, A., Nardi, B., Nielsen, J., Kurosu, M., and Heller, R. (1994). Designing for diverse users: Will just a better interface do? <cite> ACM CHI&#39;94 Conference Companion </cite> (Boston, MA, April 24-28).</li>
	<li>Nielsen, J., Desurvire, H., Kerr, R., Rosenberg, D., Salomon, G., Molich, R., and Stewart, T. (1993). Comparative design review: An exercise in parallel design. <cite> Proc. ACM INTERCHI&#39;93 </cite> (Amsterdam, the Netherlands, 24-29 April), 414-417.</li>
	<li>Nielsen, J., Pemberton, S., and Morris, J. (1993). <a href="http://homepages.cwi.nl/~steven/sigchi/visions-for-sigchi-elecpub.php"> Visions for SIGCHI Electronic Publishing</a>, ACM Special Interest Group on Computer-Human Interaction</li>
	<li>Mulligan, R.M., Dieli, M., Nielsen, J., Poltrock, S., Rosenberg, D., and Rudman, S.E. (1992). Designing usable systems under real-world constraints: A practitioners forum. <cite> Proc. ACM CHI&#39;92 </cite> (Monterey, CA, 3-7 May), 149-152.</li>
	<li>Nielsen, J., Dray, S.M., Foley, J.D., Walsh, P., and Wright, P. (1990). Usability engineering on a budget. <cite> Proc. IFIP INTERACT&#39;90 </cite> 3rd IFIP Conf. Human-Computer Interaction (Cambridge, U.K., 27-31 August), 1067-1070.</li>
	<li>Perlman, G., Egan, D.E., Ehrlich, K., Marchionini, G., Nielsen, J., and Shneiderman, B. (1990). Evaluating hypermedia systems. <cite> Proc. ACM CHI&#39;90 </cite> (Seattle, WA, 1-5 April), 387-390.</li>
	<li>Nielsen, J., del Galdo, E.M., Sprung, R.C. and Sukaviriya, P. (1990). Designing for international use (panel). <cite> Proc. ACM CHI&#39;90 </cite> (Seattle, WA, 1-5 April), 291-294.
	<p>&nbsp;</p>
	<span style="font-size: 1.5em;">Writings on Educational Issues in User Interface Design </span></li>
	<li>Nielsen, J., and Molich, R. (1989). Teaching user interface design based on usability engineering. <cite> ACM SIGCHI Bulletin </cite> <strong> 21</strong>, 1 (July 1989), 45-48.</li>
	<li>Nielsen, J. (1985). An experimental course in software ergonomics. <cite> IFIP INTERACT </cite> <strong> No. 14 </strong> (February 1985), 4-6.</li>
	<li>Nielsen, J. (1985). A continuing education course in software ergonomics for programming school teachers. <cite> ACM SIGCHI Bulletin </cite> <strong> 16 </strong> , 3 (January 1985), 32-35.</li>
	<li>Nielsen, J. (1984). Three approaches to teaching software ergonomics. Technical Report <strong> PB-184</strong>, Computer Science Department, Aarhus University, Denmark, December 1984.
	<p>&nbsp;</p>
	<a href="../topic/trip-reports/index.php"><span style="font-size: 1.5em;">Trip Reports </span> </a></li>
	<li>Esther Dyson&#39;s <a href="../articles/pc-forum-2001-conference-report/index.php"> PC Forum 2001</a> (Scottsdale, AZ, 2001)</li>
	<li><a href="../articles/trip-report-world-economic-forum/index.php">World Economic Forum</a> (Davos, Switzerland, 2001).</li>
	<li><a href="../articles/trip-report-chi-94/index.php">CHI&#39;94</a> (Boston, MA, April 24-28), <cite> IEEE Software </cite> <strong> 11</strong>, 4 (July), 110-112.</li>
	<li><a href="../articles/trip-report-upa-93/index.php">Usability Professionals Association Annual Meeting</a> (Redmond, WA, July 21-23, 1993), SIGCHI Bulletin <strong> 26</strong>, 2 (April 1994).</li>
	<li><a href="../articles/trip-report-chi-92/index.php">CHI&#39;92</a> (Monterey, CA, 3-7 May 1992), <cite> IEEE Software </cite> <strong> 9 </strong> , 4 (July 1992), pp. 78- 79.</li>
	<li><a href="../articles/trip-report-chi-90/index.php">CHI&#39;90</a> (Seattle, WA, 1-5 April 1990), <cite> SIGCHI Bulletin </cite> <strong> 22 </strong> , 2 (October 1990), pp. 20-25.</li>
	<li><a href="../articles/trip-report-hypertext-89/index.php">Hypertext&#39;89</a> (Pittsburgh, PA, 5-8 November 1989), <cite> SIGCHI Bulletin </cite> <strong> 21</strong>, 4 (April 1990), pp. 52-61.</li>
	<li><a href="../articles/trip-report-hypertext-2/index.php">Hypertext II</a> (York, UK, 29-30 June 1989), <cite> SIGCHI Bulletin </cite> <strong> 21</strong>, 2 (October 1989), pp. 41-47.</li>
	<li><a href="../articles/trip-report-chi-89/index.php">CHI&#39;89</a> (Austin, TX, 30 April - 4 May 1989), <cite> SIGCHI Bulletin </cite> <strong> 21</strong>, 2 (October 1989), pp. 28-40.</li>
	<li>British Computer Society HyperHyper workshop (London, UK, 23 February 1989), <cite> SIGCHI Bulletin </cite> <strong> 21</strong>, 1 (July 1989), pp. 65-67.</li>
	<li>International Conference on Fifth Generation Computer Systems (Tokyo, Japan, 28 November - 2 December 1988), <cite> SIGCHI Bulletin </cite> <strong> 21</strong>, 1 (July 1989), pp. 68-71.</li>
	<li>British Computer Society HCI&#39;88, <cite> SIGCHI Bulletin </cite> <strong> 20</strong>, 3 (January 1989), pp. 90-93.</li>
	<li><a href="../articles/trip-report-chi-88/index.php">CHI&#39;88 </a> (Washington, DC, 15-19 May 1988), <cite> SIGCHI Bulletin </cite> <strong> 20</strong>, 2 (October 1988), pp. 58-66.</li>
	<li><a href="../articles/trip-report-hypertext-87/index.php">HyperTEXT&#39;87 </a> (Chapel Hill, NC, 13-15 November 1987), <cite> SIGCHI Bulletin </cite> <strong> 19</strong>, 4 (April 1988), pp. 27-35. Also exists in a hypertext version in HyperCard format.</li>
	<li>INTERACT&#39;87 (Stuttgart, FRG, 1-4 September 1987), <cite> SIGCHI Bulletin </cite> <strong> 19</strong>, 4 (April 1988), pp. 36-42 and <cite> IFIP INTERACT Newsletter </cite> No. <strong> 21</strong>.</li>
	<li>NordDATA&#39;87 Joint Scandinavian Computer Conference (Trondheim, Norway, 15-18 June 1987). Originally written as a hypertext document in Guide format.</li>
	<li>CHI+GI&#39;87 (Toronto, Canada, 5-9 April 1987), ACM <cite> SIGCHI Bulletin </cite> <strong> 19</strong>, 2 (October 1987), pp. 58-66.</li>
	<li><a href="../articles/trip-report-cscw-86/index.php">Computer-Supported Cooperative Work&#39;86 </a> (Austin, TX, 3-5 December 1986), ACM <cite> SIGCHI Bulletin </cite> <strong> 19</strong>, 1 (July 1987), pp. 54-61.</li>
	<li>International Scientific Conference on Work With Display Units (Stockholm, Sweden, 12-15 May 1986), Technical Report <strong> ID-TR-1986-13</strong>, Department of Computer Science, Technical University of Denmark.</li>
	<li>CHI&#39;86 (Boston, MA, 13-17 April 1986), <cite> IFIP INTERACT Newsletter </cite> No. <strong> 17</strong>.</li>
	<li>CHI&#39;85 (San Francisco, CA, 14-18 April 1985), unpublished.</li>
	<li>International Conference on Fifth Generation Computer Systems (Tokyo, Japan, 6-9 November 1984), Technical Report <strong> IR-54</strong>, Computer Science Department, Aarhus University, Denmark. In Danish.</li>
	<li>INTERACT&#39;84 (London, UK, 3-7 September 1984), unpublished. In Danish.</li>
	<li>CHI&#39;83 (Boston, MA, 12-15 December 1983), unpublished. In Danish.
	<p>&nbsp;</p>
	<span style="font-size: 1.5em;">Book Reviews</span></li>
	<li>Review of Doug Menuez: &quot;Defying Gravity: The Making of Newton,&quot; ACM <cite> SIGCHI Bulletin </cite> <strong> 26</strong>, 1 (January 1994), 73.</li>
	<li>Review of Bruce Tognazzini: &quot;Tog on Interface,&quot; ACM <cite> SIGCHI Bulletin </cite> <strong> 25</strong>, 2 (April 1993), 54-55.</li>
	<li>Review of Aaron Marcus: &quot;Graphic Design for Electronic Documents and User Interfaces,&quot; <cite> Science of Computer Programming </cite> <strong> 18 </strong> , 2 (April 1992), 218-221.</li>
	<li>Review of BBC Interactive Television Unit: &quot;Ecodisc,&quot; <cite> Hypermedia </cite> <strong> 2</strong>, 2 (1990), 176-182.</li>
	<li>Review of Martin Helander (Ed:): &quot;Handbook of Human-Computer Interaction,&quot; ACM <cite> SIGCHI Bulletin </cite> <strong> 21</strong>, 3 (January 1990), 107-108.</li>
	<li>Review of Adele Goldberg (Ed.): &quot;A History of Personal Workstations,&quot; ACM <cite> SIGCHI Bulletin </cite> <strong> 21</strong>, 1 (July 1989), 116-119.</li>
	<li>Review of Mike Saenz: &quot;Iron Man Crash,&quot; ACM <cite> Computer Graphics </cite> <strong> 22</strong>, 5 (October 1988), p. 270.</li>
	<li>Review of Ben Shneiderman: &quot;Designing the User Interface: Strategies for Effective Human-Computer Interaction,&quot; ACM <cite> SIGCHI Bulletin </cite> <strong> 18</strong>, 3 (January 1987), 85-86.</li>
	<li>Review of Andrew Monk (Ed.): &quot;Fundamentals of Human-Computer Interaction,&quot; <cite> Behaviour and Information Technology </cite> <strong> 5</strong>, 3 (July-September 1986), 297-298.
	<p>&nbsp;</p>
	<span style="font-size: 1.5em;">Tutorials</span></li>
	<li><cite>User Interface Design for the WWW, </cite> presented at the World Wide Web Consortium WWW&#39;4 conference, Boston, MA, December 11, 1995; the ACM CHI&#39;96 conference, Vancouver, B.C., April 13, 1996; the World Wide Web Consortium WWW&#39;5 conference, Paris, France, May 6, 1996; National Physical Laboratory, London, U.K., May 14, 1996; User Interface&#39;96, Boston, MA, October 9, 1996; ACM CHI&#39;97, Atlanta, GA, 22 March 1997; World Wide Web Consortium WWW&#39;6 conference, Santa Clara, CA, April 7, 1997.</li>
	<li><cite>Usability Inspection Methods, </cite> presented at the ACM CHI&#39;94 conference, Boston, MA, April 25, 1994; ACM CHI&#39;95, Denver, CO, May 9, 1995; IFIP INTERACT&#39;95, Lillehammer, Norway, June 26, 1995.</li>
	<li><cite>Usability Engineering, </cite> presented at the ACM Greater Boston chapter, Boston, MA, October 16, 1993; National Physical Laboratory, London, January 25, 1994.</li>
	<li><cite>Next-Generation User Interfaces, </cite> presented at Metodica seminar, Copenhagen, Denmark, June 16, 1992; Metodica seminar, Copenhagen, Denmark, May 5, 1993; HCI International&#39;93 conference, Orlando, FL, August 10, 1993; National Physical Laboratory, London, January 26, 1994.</li>
	<li><cite>Usability Evaluation and Inspection Methods, </cite> presented at the ACM INTERCHI&#39;93 conference, Amsterdam, the Netherlands, 26 April 1993 (had the highest attendance of the 29 tutorials at INTERCHI&#39;93).</li>
	<li><cite>Introduction to Hypertext and Hypermedia, </cite> presented at the Danish Datamatics Center, 15 September 1988; National University of Singapore, 18 November 1988; Japan Institute of Systems Research, Osaka, 22 November 1988; ACM conference on Document Processing Systems, Santa Fe, NM, 5 December 1988; the British Computer Society HCI Specialist Group, London, UK, 22 February 1989; ACM CHI&#39;89, Austin, TX, 30 April 1989; University of Technology, Sydney, Australia, August 1989; Third International Conference on Human-Computer Interaction, Boston, MA, 19 September 1989; Swedish Multimedia conference, Link&ouml;ping, Sweden, 10 October 1989; ACM Hypertext&#39;89 conference, Pittsburgh, PA, 5 November 1989; ACM CHI&#39;90, Seattle, WA, 1 April 1990; ACM COIS&#39;90 conference on Office Information Systems, Cambridge, MA, 24 April 1990; AAAI&#39;90 National Artificial Intelligence Conference, Boston, MA, 30 July 1990 (co-presented with Gerhard Fischer); ACM SIGGRAPH&#39;90, Dallas, TX, 6 August 1990 (co-presented with John Leggett and Hannah Kain); IFIP INTERACT&#39;90, Cambridge, UK, 27 August 1990; Bellcore/BCC EDD&#39;91 Conference on Electronic Document Delivery, East Brunswick, NJ, 25 March 1991, ACM SIGIR&#39;92, Copenhagen, Denmark, 21 June 1992; ACM Hypertext&#39;93 conference, Seattle, WA 14 November 1993.</li>
	<li><cite>Evaluating Hypertext Usability, </cite> co-presented with Lynda Hardman at ACM Hypertext&#39;89 conference, Pittsburgh, PA, 5 November 1989.</li>
	<li><cite>Hypertext, Hypermedia, and HyperCard, </cite> presented at the British Computer Society Human-Computer Interaction&#39;88 conference, Manchester, UK, 5 September 1988.</li>
	<li><cite>Introduction to Computer Science Concepts for Human-Computer Interaction, </cite> presented at IFIP INTERACT&#39;87, Stuttgart, FRG, 1 September 1987.</li>
	<li>Presentation on usability engineering in the <cite> <a href="http://www.cs.umd.edu/projects/uis/UIS94/index.php"> User Interface Strategies &#39;94</a> </cite> satellite television broadcast from the University of Maryland.
	<p>&nbsp;</p>
	<span style="font-size: 1.5em;">Selected Technical Reports</span></li>
	<li>Nielsen, J. (Ed.) (1987). <cite>Record of the ACM CHI+GI&#39;87 Workshop on Classification of Dialog Techniques</cite>. Technical Report <strong> ID-TR-1987-25</strong>, Computer Science Department, Technical University of Denmark, August 1987. Contributions by: L.J. Bass, T.W. Bleser, J. Johnson, M.R. Moseley, B.A. Myers, R. Neches, J. Nielsen, M. Schwartz, E. Tarlin, M.J. Tauber, J.E. Ziegler.</li>
	<li>Mack, R.L., and Nielsen, J. (1987). <cite>Software integration in the Professional Work Environment: Observations on requirements, usage, and interface issues</cite>. Research Report <strong>RC12677</strong>, IBM T.J. Watson Research Center.</li>
</ol>

    </div>
</div>

    </div>
    




<footer id="page-footer">
    <div class="row hide-for-print">
        <div class="column small-12 medium-6">
            <section class="footer-popular">
                
                <h1>Popular Topics</h1>
                <div style="float: left;  width: 15em; display: block;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../topic/applications/index.php">Application Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../topic/e-commerce/index.php">E-Commerce Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../topic/eyetracking/index.php">Eyetracking </a></li>
	<li style="padding-bottom: .5em;"><a href="../topic/human-computer-interaction/index.php">Human Computer Interaction </a></li>
	<li style="padding-bottom: .5em;"><a href="../topic/information-architecture/index.php">Information Architecture </a></li>
	<li style="padding-bottom: .5em;"><a href="../topic/intranets/index.php">Intranet Design </a></li>
	<li><a href="../topic/interaction-design/index.php">Interaction Design </a></li>
</ul>
</div>

<div style="float: left;  width: 15em; display: block; list-style: none;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../topic/mobile-and-tablet-design/index.php">Mobile and Tablet Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../topic/credibility-persuasion/index.php">Persuasive Design</a></li>
	<li style="padding-bottom: .5em;"><a href="../topic/research-methods/index.php">Research Methods</a></li>
	<li style="padding-bottom: .5em;"><a href="../topic/user-testing/index.php">User Testing </a></li>
	<li style="padding-bottom: .5em;"><a href="../topic/web-usability/index.php">Web Usability </a></li>
	<li style="padding-bottom: .5em;"><a href="../topic/writing-web/index.php">Web Writing &amp; Content Strategy </a></li>
</ul>
</div>

<p>&nbsp;</p>

                
            </section>
        </div>

        <div class="column small-12 medium-6">
            <div class="row">
                <div class="column small-6">
                    <section class="footer-about">
                        <h1>Follow Us</h1>
                        <ul>
	<li><a href="../articles/subscribe/index.php">Subscribe to our weekly UX newsletter</a>,&nbsp;Jakob Nielsen&#39;s Alertbox</li>
	<li>Visit Don Norman&#39;s website, <a href="http://www.jnd.org/"> www.jnd.org </a></li>
	<li>Visit Bruce Tognazzini&#39;s website, <a href="http://www.asktog.com/"> www.asktog.com </a></li>
	<li style="padding-top: 6px;"><a class="twitter-follow-button" data-show-count="false" data-size="large" href="https://twitter.com/nngroup"><img alt="Follow @nngroup on Twitter" height="20" src="https://media.nngroup.com/media/editor/2016/12/30/twitter-follow-button.png" style="width: 117px; height: 20px;" width="117" /></a></li>
	<li style="padding-top: 6px;"><img alt="" height="14" src="https://media.nngroup.com/media/editor/2016/07/26/feed-icon-14x14.png" style="vertical-align:text-top;width:14px;margin-right:.25em;" width="14" /><a href="../feed/rss/index.php">Subscribe to our RSS feed</a></li>
</ul>

                    </section>
                </div>

                <div class="column small-6">
                    <section class="footer-about right-column">
                        <h1>About
</h1>
                        <ul class="no-bullet">
	<li><a href="../about/contact/index.php">Contact information</a></li>
	<li><a href="../about/index.php">About Us</a></li>
	<li><a href="../about/why-nng/index.php">Why NN/g? </a></li>
	<li><a href="../return-policy/index.php">Return Policy</a></li>
</ul>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="copyright column small-12">
            <p><a href="../copyright-and-reprint-info/index.php">Copyright</a> &copy; 1998-2017 Nielsen Norman Group, All Rights Reserved.</p>
        </div>
    </div>
</footer>


    


        <script src="https://media.nngroup.com/static/vendor/fastclick/js/fastclick.min.js"></script>
        <script src="https://media.nngroup.com/static/vendor/foundation/js/foundation.min.js"></script>
        <script src="http://media.nngroup.com/static/js/app.js"></script>
        <script src="http://media.nngroup.com/static/js/scroll-to-fixed.js"></script><script>var externalLinks = document.querySelectorAll('a[href^="http"]');for (var i = externalLinks.length-1; i >= 0; i--) {externalLinks[i].addEventListener("click", function(event) { alert("External links are disabled for this test."); event.preventDefault(); }, false);}var mailLinks = document.querySelectorAll('a[href^="mailto"]');for (var i = mailLinks.length-1; i >= 0; i--) {mailLinks[i].addEventListener("click", function(event) { alert("Email links are disabled for this test."); event.preventDefault(); }, false);}var externalForms = document.querySelectorAll('form');for (var i = externalForms.length-1; i >= 0; i--) {externalForms[i].addEventListener("submit", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false); externalForms[i].addEventListener("click", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false);}</script>

        <script>
            
            $(function() {
                $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
                $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
            });
        </script>

        


        
        <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1072699960;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072699960/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
    </body>

<!-- Mirrored from www.nngroup.com/people-jakob-nielsen-publications/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:10:41 GMT -->
</html>
