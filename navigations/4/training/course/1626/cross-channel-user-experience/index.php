<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->





    
<!-- Mirrored from www.nngroup.com/training/course/1626/cross-channel-user-experience/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:10:25 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1026.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"licenseKey":"1a30ef9931","errorBeacon":"bam.nr-data.net","applicationID":"2048992","transactionName":"NFxSMRBUX0EDUkIPDA0WdhAMVkVbDV8ZAxUGV0QWTENYVxVCDCMVBldEJg1AQ0EHdVMSAgpVHgIHQQ==","beacon":"bam.nr-data.net","queueTime":1,"applicationTime":565,"agent":""}</script>
        <title>Customer Journeys &amp; Omnichannel User Experience | Nielsen Norman Group UX Training Course</title><meta property="og:title" content="Customer Journeys &amp; Omnichannel User Experience | Nielsen Norman Group UX Training Course" />
  
        
        <meta name="description" content="Full day course taught at Nielsen Norman Group’s UX Conference. Learn how to create an engaging and usable cross-channel experience.">
        <meta property="og:description" content="Full day course taught at Nielsen Norman Group’s UX Conference. Learn how to create an engaging and usable cross-channel experience." />
        
  
        
	
        

        

        <meta name="robots" content="noarchive">
        <meta name="viewport" content="width=device-width">

        <link rel="search" type="application/opensearchdescription+xml" title="NN/g Search" href="../../../../opensearch.xml" />
        <link rel="shortcut icon" href="http://media.nngroup.com/static/img/favicon.ico" />

        <link rel="stylesheet" href="https://media.nngroup.com/static/vendor/font-awesome/css/font-awesome.min.css">

        
        <link rel="stylesheet" href="https://media.nngroup.com/static/css/7232a960f1df.css" type="text/css" />
        <link rel="stylesheet" href="http://fast.fonts.com/t/1.css?apiType=css&amp;projectid=c5846260-9def-46ec-9407-0f1349bd2d64" type="text/css">

        <script src="https://media.nngroup.com/static/vendor/modernizr/js/modernizr.min.js"></script>

        <script type="text/javascript" src="https://media.nngroup.com/static/js/3283be5def74.js"></script>
        

        
    <link rel="canonical" href="../../../../courses/cross-channel-user-experience/index.php" />


        
        
        <!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=229034,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('http://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->


<!-- Google Analytics Tracking Code for http://nngroup.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23132681-1', 'nngroup.com');
  ga('require', 'linkid', 'https://www.nngroup.com/training/course/1626/cross-channel-user-experience/linkid.js');
  ga('require', 'displayfeatures');

// Optimizely Universal Analytics Integration
 window.optimizely = window.optimizely || [];
 window.optimizely.push("activateUniversalAnalytics");

  ga('send', 'pageview');

</script>
<script>
/**
* Function to track a click on an outbound link in Google Analytics.
*/
function trackOutboundLink(label, url) {
    if(window.ga && ga.loaded) {
        ga('send', 'event', 'outbound', 'click', label, {'hitCallback': function(){document.location = url;}
        });
    } else {
        document.location = url;
    }
}
/**
* Function to track a click on a link in the article announcement area.
*/
$('#articleAnnouncement a').on('click', function(){
  ga('send', 'event', 'link', 'click', 'articleAnnouncement');
});
</script>

<script type="text/javascript">
 setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2421.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



<!-- Hotjar Tracking Code for http://nngroup.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:121605,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

        <script src="https://media.nngroup.com/static/js/analytics/track_outbound_link.js"></script>
        
        
        
    </head>
    <body class="location-training location-event-course-detail course-detail">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
    <header id="page-header">
        <div id="cart" class="shopping-cart mobile-cart show-for-small-down"></div>
        <div class="row">
            <div id="masthead" class="masthead small-12 medium-7 column">

                <!-- The button to toggles menu on small screen -->

                <h1><a href="../../../../index.php"><img src="http://media.nngroup.com/static/img/logo-wordmark.png" width="74" height="30"> Nielsen <span class="norman">Norman</span> Group</a></h1>
                <p class="tagline show-for-medium-up">Evidence-Based User Experience Research, Training, and Consulting</p>

                <!-- The button to toggles form search on small screen -->


                <div style="clear: right;width:  100%;height: 1px;"></div><?php define('ROOT_PATH',$_SERVER["DOCUMENT_ROOT"] . '/apps/navigation_styles/navigations/4'); ?><?php include(ROOT_PATH)."/navigation.php"; ?>

                <!-- Drop down menu on small screen -->
                <div id="hideNav" class="show-for-small-down">
                  
                  <ul class="menu-content">
                    <li><a href="../../../../index.php">Home</a></li>
                    <li class="has-submenu">
                      <a>Training</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../../index.php">Overview</a></li>
                        
<li id="nav-ux-conference"><a href="../../../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../../../online-seminars/index.php"><span>Online Seminars</span></a></li>

                      </ul>
                    </li>
                    <li class="has-submenu">
                      <a>Consulting</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
  <li><a href="../../../../consulting/index.php">Overview</a></li>
  
  <li><a href="../../../../consulting/benchmarking/index.php">Benchmarking</a></li>
  
  <li><a href="../../../../consulting/expert-design-review/index.php">Expert Design Review</a></li>
  
  <li><a href="../../../../consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
  
  <li><a href="../../../../consulting/usability-testing/index.php">Usability Testing </a></li>
  
  <li><a href="../../../../consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
  
  <li><a href="../../../../in-house-training/index.php">In-House Training</a></li>
</ul>

                    </li>

                    <li><a href="../../../../reports/index.php">Reports</a></li>
                    <li><a href="../../../../articles/index.php">Articles</a></li>
                    <li class="has-submenu"><a>About NN/G</a><i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../../../about/index.php">Overview</a></li>
                        <li><a href="../../../../people/index.php">People</a></li>
                        <li><a href="../../../../about/why-nng/index.php">Why NN/g?</a></li>
                        <li><a href="../../../../about/contact/index.php">Contact</a></li>
                        <li><a href="../../../../news/index.php">News</a></li>
                        <li><a href="../../../../about/history/index.php">History</a></li>
                        <li><a href="../../../../books/index.php">Books</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                </div>

                <hr class="header-divider show-for-small-only"/>


            </div>
       </div>

        <div id="hideNav">            <nav class="primary-nav small-12 column">
                <ul>
                    <li><a class="navitem" id="nav-home" href="../../../../index.php">Home</a></li>
                    <li><a class="navitem" id="nav-training" href="../../../index.php">Training</a></li>
                    <li><a class="navitem" id="nav-consulting" href="../../../../consulting/index.php">Consulting</a></li>
                    <li><a class="navitem" id="nav-reports" href="../../../../reports/index.php">Reports</a></li>
                    <li><a class="navitem" id="nav-articles" href="../../../../articles/index.php">Articles</a></li>
                    <li><a class="navitem" id="nav-about" href="../../../../about/index.php">About NN/G</a></li>
                </ul>
            </nav>
            <nav class="secondary-nav small-12 medium-12 large-12 columns">
                
  <ul class="inline-list">
  
<li id="nav-ux-conference"><a href="../../../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../../../online-seminars/index.php"><span>Online Seminars</span></a></li>

</ul>


            </nav>
        </div>
    </header>
    <div class="l-content">
    

<article class="course-detail">
    <div class="row">
        <p class="small-12 column">
            
            Full day training course
            
            offered at <a href="../../../washington-dc/index.php">The UX Conference Washington DC</a>
        </p>
    </div>
    <div class="row">
        <div class="l-section-intro small-12 medium-8 column">
            <header>
                <h1>Omnichannel Journeys and Customer Experience</h1>
                <h2>Create a usable cross-channel experience, including the web, email, social media, phone, print, apps and online chat</h2>
            </header>
        </div>

        <div class="small-12 column show-for-small-down l-subsection">
            
            <h2 class="course-dates">Course Date: May 18, 2017</h2>
            

            
            <a class="button course-register medium register-button" href="../../../register/90/index.php">
                Register
            </a>
            
            <div style="clear: both"></div>
            <hr style="border-color: #eee"/>
        </div>
    </div>


    
    <div class="row">
        <div class="l-course-content small-12 medium-8 column">
            <div class="l-usercontent mainCourseContent">
                <p>Customers interact with businesses not only through many channels, but also on many devices. Rather than seeing each of these interactions as a separate experience, users view all interactions with an organization as part of one larger user experience. Thus, a company may think, &ldquo;multi-channel service,&rdquo; but a customer thinks, &ldquo;one company, one user experience.&rdquo;</p>

<p>For this reason, organizations must create a cohesive ecosystem of business channels and touchpoints where each not only provides their own unique benefits, but also compliments and seamlessly connects to the experience as a whole.</p>

<p>This course will focus on identifying a customer&rsquo;s journey as it relates to your business and understanding how to leverage these insights. We will also discuss guidelines for creating a usable experience across many channels, including the web, print, mobile devices, email, social media (Facebook, Twitter, etc.), phone, and online chat.</p>

            </div>
            
            <div class="l-usercontent mainCourseContent collapsable-content">

                <h1 class="collapse-link show-for-small-down">Topics Covered</h1>
                <h2 class="show-for-medium-up">Topics Covered</h2>

                <div class="collapsable">
                    <ul>
	<li>What is omnichannel user experience?</li>
	<li>The case for providing an exceptional&nbsp;omnichannel experience</li>
	<li>How users engage with organizations
	<ul>
		<li>Digital, physical and hybrid touch points</li>
		<li>Static vs. interactive</li>
		<li>Understanding how users choose devices and channels</li>
		<li>Transitioning between channels</li>
		<li>Using multiple channels in parallel</li>
	</ul>
	</li>
	<li>Understanding the customer journey
	<ul>
		<li>Identifying and defining touchpoints</li>
		<li>Understanding interactions with customers over time</li>
	</ul>
	</li>
	<li>Developing a cross-channel strategy and experience
	<ul>
		<li>Explore common pitfalls and ideas that work</li>
		<li>Overcoming common barriers</li>
		<li>Data continuity &ndash; Integrating messaging across touch points</li>
	</ul>
	</li>
	<li>Elements that create an excellent cross-channel experience
	<ul>
		<li>Consistent UX across touch points</li>
		<li>Optimized experience for every touchpoint</li>
		<li>Seamless transitions between channels</li>
		<li>Synchronization of information across channels</li>
		<li>Collaboration between channels</li>
		<li>Leveraging channel-specific capabilities</li>
	</ul>
	</li>
	<li>Channel-specific usability recommendations
	<ul>
		<li>Web: desktop and mobile</li>
		<li>Social media</li>
		<li>Print&nbsp;</li>
		<li>In-store and in-person</li>
		<li>Email</li>
		<li>Phone</li>
		<li>SMS/Text</li>
	</ul>
	</li>
	<li>Designing for future channels
	<ul>
		<li>Designing for the experience, not for the page or screen</li>
	</ul>
	</li>
	<li>Evaluating your omnichannel experience and researching the customer journey
	<ul>
		<li>Running your own omnichannel research study</li>
		<li>Measuring changes and improvements</li>
		<li>Using customer journey maps as a diagnostic tool</li>
	</ul>
	</li>
</ul>

                </div>

            </div>
            

            
            <div class="l-usercontent mainCourseContent collapsable-content">

                <h1 class="collapse-link show-for-small-down">Format</h1>
                <h2 class="show-for-medium-up">Format</h2>

                <div class="collapsable">
                    <p>The course is mainly in lecture format with a couple of group exercises to reinforce the learned principles and guidelines.</p>

<p>The course also includes</p>

<ul>
	<li>Findings from our own usability studies</li>
	<li>Videos from usability testing of people&#39;s behavior</li>
	<li>Screenshots of designs that work and don&#39;t work</li>
	<li>Opportunities to ask questions and get answers</li>
</ul>

                </div>

            </div>
            

            
            <div class="l-usercontent mainCourseContent collapsable-content">

                <h1 class="collapse-link show-for-small-down">Related Course</h1>
                <h2 class="show-for-medium-up">Related Course</h2>

                <div class="collapsable">
                    <p>A related NN/g course,&nbsp;<a href="../../../../courses/journey-mapping/index.php">Journey Mapping to Better Understand Customer Needs</a>, focuses on learning the process for capturing and communicating UX insights across complex interactions, including:</p>

<ul>
	<li>Determining when to apply journey mapping as a design tool, and how to convince others of its merit</li>
	<li>Learning how to gather and compile research into truthful narratives, and find out which research methods are best for the journey mapping process</li>
	<li>Understanding how to visualize findings in the end visual and use that artifact to communicate your insights to others</li>
</ul>

                </div>

            </div>
            

            

            

            

            

            
                



<div class="row certification-info">
  <section class="l-subsection collapsable-content small-12 medium-8 columns">
    <h1 class="collapse-link show-for-small-down">UX Certification Credit</h1>
    <h2 class="hide-for-small-down">UX Certification Credit</h2>
    <div class="collapsable">
      <p>
      Attending this course and passing the exam earns <strong>1 UX Certification credit</strong>, which also counts towards the optional <a href="../../../../ux-certification/interaction-design-specialty/index.php">Interaction Design Specialty</a>.
      </p>

      
      <p>
      This course credit may also be used as the required <em>Mobile Foundations</em> credit for the <a href="../../../../ux-certification/mobile-design-specialty/index.php">Mobile Design Specialty</a>.
      </p>
      

      <p>
      Learn more about NN/g's <a href="../../../../ux-certification/index.php">UX Certification</a> Program.
      </p>
    </div>
  </section>
  <div class="hide-for-small-down medium-4 columns badge">
    <img alt="UX Certification Badge from Nielsen Norman Group" src="https://media.nngroup.com/nng-uxc-badge.png">
  </div>
</div>

            

            
<div class="l-usercontent mainCourseContent collapsable-content participant-comments">
  <h1 class="collapse-link show-for-small-down">Participant Comments</h1>
  <h2 class="show-for-medium-up">Participant Comments</h2>
  <div class="collapse-content collapsable">
    <blockquote>&quot;The course has by far exceeded my expectations. It informs about omnichannel journey but much more about the concept of well optimised omnichannel organisation. I loved it!&quot;</blockquote>

<p>Jelena Krsmanovic, Delivery Hero</p>

<blockquote>&quot;Very relevant training for anyone designing for more than one platform. Makes you think about changing how organizations are thinking and organized.&quot;</blockquote>

<p>Harry van Mierloo, Fox It</p>

<blockquote>&quot;Solid overview of an emerging field. Difficult to divorce the conceptual execution of an omnichannel strategy from the technical delivery, but this course did good job of sparking thought and conversation.&quot;</blockquote>

<p>&nbsp;</p>

    <ul id="commentAccordian" class="accordion" data-accordion>
      <li class="accordion-navigation">
        <a href="#cc" class="closed" id="commentCollapseLink"></a>
        <div id="cc" class="content more-comments">
          <blockquote>&quot;This course was very well delivered, good examples and enjoyable team exercises. Good for beginners as well as more experienced UX designers.&quot;</blockquote>

<p>Angela Arnold, Which, UK</p>

<blockquote>&quot;A very good overview of methodologies that help UX designers to create omnichannel experiences.&quot;</blockquote>

<p>Michal Blazej, Lighting Beetle s.r.o.</p>

<blockquote>&quot;Kim was a great instructor &mdash; clear and engaging, and obviously passionate about her subject matter. The day flew by!&quot;</blockquote>

<p>Meghan Bortolan</p>

<blockquote>&quot;I enjoyed this course because it was very different from other courses offered. I appreciated being able to consider UX beyond just websites. Thanks!&quot;</blockquote>

<p>Lisa Murray, Zion&amp;Zion</p>

<blockquote>&quot;Great overview of omnichannel UX, a lot of content covered during the course.&quot;</blockquote>

<p>Codi Okonkwo, QRC</p>

<blockquote>&quot;Really good insight to omnichannel with some good walkthrough examples.&quot;</blockquote>

<p>Fraser Crick, Estee Lauder</p>

        </div>
      </li>
    </ul>
  </div>
</div>
<script>
  $('#commentAccordian').on('toggled', function (event, accordion) {
    $link = $('#commentCollapseLink');
    $link.toggleClass('closed');
    if($link.hasClass('closed')) {
      $link.text('');
    } else {
      $link.text('Hide Participant Comments');
    }
  });
</script>



            <div class="l-usercontent"></div>

            
            
            <section class="course-instructors l-subsection collapsable-content">
                <h1 class="show-for-medium-up">Instructor</h1>
                <h1 class="show-for-small-down collapse-link">Instructor</h1>
                <div class="collapsable">
                    
                    <article class="course-instructor collapsable-content">
                        <h2><a href="../../../../people/kim-flaherty/index.php">Kim Flaherty</a></h2>
                        <div class="row">
                            <div class="medium-3 column hide-for-small-only">
                                <a href="../../../../people/kim-flaherty/index.php"><img src="https://media.nngroup.com/media/people/photos/Kim-Flaherty-Headshot.png.200x200_q95_autocrop_crop_upscale.png"></a>
                            </div>
                            <div class="small-12 medium-9 column">
                                <a href="../../../../people/kim-flaherty/index.php"><img class="show-for-small-only" src="https://media.nngroup.com/media/people/photos/Kim-Flaherty-Headshot.png.200x200_q95_autocrop_crop_upscale.png"></a>
                                <div class="usercontent"><p>Kim Flaherty is a User Experience Specialist with Nielsen Norman Group. Flaherty combines her background as a developer and education in Computer Science with her user experience expertise, particularly around complex applications, to bring well-rounded insights to her work. <a href="../../../../people/kim-flaherty/index.php">Read more about Kim</a>.</p>
</div>
                            </div>
                        </div>
                    </article>
                    
                </div>
            </section>
            <div class="show-for-small-down">
                



<div class="collapsable-content">
  <section class="l-subsection related-images">
    <h2 class="hide-for-small-down">Scenes From Past Trainings</h2>
    <h1 class="show-for-small-down collapse-link">Scenes From Past Trainings</h1>
    <div class="collapsable">
    <p>At the UX Conference, you get a full day of in-depth training with expert instructors. Learn more about <a href="../../../why-attend/index.php">why you should attend</a>.</p>

    
      
        
          
            <img srcset="https://media.nngroup.com/media/files/images/IMG_4023_copy_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale@2x.jpg 2x" src="https://media.nngroup.com/media/files/images/IMG_4023_copy_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale.jpg" alt="">
          
        
      
    
      
        
          
            <img srcset="https://media.nngroup.com/media/files/images/IMG_7582_copy_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale@2x.jpg 2x" src="https://media.nngroup.com/media/files/images/IMG_7582_copy_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale.jpg" alt="">
          
        
      
    
      
        
          
            <img srcset="https://media.nngroup.com/media/files/images/IMG_1049_copy_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale@2x.jpg 2x" src="https://media.nngroup.com/media/files/images/IMG_1049_copy_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale.jpg" alt="">
          
        
      
    
      
        
          
            <img srcset="https://media.nngroup.com/media/files/images/IMG_4881_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale@2x.jpg 2x" src="https://media.nngroup.com/media/files/images/IMG_4881_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale.jpg" alt="">
          
        
      
    
    </div>
  </section>
</div>


            </div>
        </div>

        <div class="l-event-sidebar medium-4 small-12 column sidebar">
            <div class="show-for-medium-up">
                <div class="l-subsection">
                    
                    <h2>Course Date: May 18, 2017</h2>
                    
                </div>

                
                    
                        <p><a class="button medium register-button" href="../../../register/90/index.php">Register</a></p>
                    
                
            </div>

            
            <div class="show-for-small-down" style="margin: 2rem 0;">
                <a class="button expand register-button" href="../../../register/90/index.php">Register</a>
            </div>
            


            <section class="l-subsection l-uw-sidebar collapsable-content">
                <h2 class="show-for-medium-up">The UX Conference Washington DC Courses</h2>
                <h1 class="show-for-small-down collapse-link">UX Conference Washington DC Courses</h1>

                <div class="collapsable">
                    
                    <ul class="no-bullet">
                        
                        <li><strong>Sunday, May 14</strong></li>
                        <ul>
                            
                            
                            
                            <li>
                                
                                <a href="../../1609/ux-vp-director/index.php">The UX VP/Director</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1608/ux-basic-training/index.php">UX Basic Training</a>
                                
                            </li>
                            
                            
                            
                        </ul>
                        
                        <li><strong>Monday, May 15</strong></li>
                        <ul>
                            
                            
                            
                            <li>
                                
                                <a href="../../1612/content-strategy/index.php">Content Strategy</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1611/becoming-ux-strategist/index.php">Becoming a UX Strategist</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1610/web-page-design/index.php">Web Page UX Design</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1613/human-mind/index.php">The Human Mind and Usability</a>
                                
                            </li>
                            
                            
                            
                        </ul>
                        
                        <li><strong>Tuesday, May 16</strong></li>
                        <ul>
                            
                            
                            
                            <li>
                                
                                <a href="../../1616/usability-testing/index.php">Usability Testing</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1614/journey-mapping/index.php">Journey Mapping to Understand Customer Needs</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1615/cross-functional-team/index.php">Working Effectively in Cross-Functional Teams</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1617/stakeholder-ux-approval/index.php">Engaging Stakeholders to Build Buy-In</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1618/generating-big-ideas/index.php">Generating Big Ideas with Design Thinking</a>
                                
                            </li>
                            
                            
                            
                        </ul>
                        
                        <li><strong>Wednesday, May 17</strong></li>
                        <ul>
                            
                            
                            
                            <li>
                                
                                <a href="../../1620/ideation/index.php">Effective Ideation Techniques for UX Design</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1623/application-ux/index.php">Application Design for Web and Desktop</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1622/personas/index.php">Personas: Turn User Data Into User-Centered Design</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1621/lean-ux-and-agile/index.php">Lean UX and Agile</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1619/information-architecture/index.php">Information Architecture</a>
                                
                            </li>
                            
                            
                            
                        </ul>
                        
                        <li><strong>Thursday, May 18</strong></li>
                        <ul>
                            
                            
                            
                            <li>
                                
                                <a href="../../1628/communicating-design/index.php">Communicating Design</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1624/facilitating-ux-workshops/index.php">Facilitating UX Workshops</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1625/measuring-ux/index.php">Measuring User Experience</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <strong>Omnichannel Journeys and Customer Experience</strong>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1627/one-person-ux-team/index.php">The One-Person UX Team Tool Box</a>
                                
                            </li>
                            
                            
                            
                        </ul>
                        
                        <li><strong>Friday, May 19</strong></li>
                        <ul>
                            
                            
                            
                            <li>
                                
                                <a href="../../1631/wireframing-and-prototyping/index.php">Wireframing and Prototyping</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1629/translating-brand/index.php">Translating Brand into User Interactions</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1630/ux-deliverables/index.php">UX Deliverables</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1632/writing/index.php">Writing Compelling Digital Copy</a>
                                
                            </li>
                            
                            
                            
                        </ul>
                        
                    </ul>
                </div>
            </section>

            <div class="hide-for-small-down">
                



<div class="collapsable-content">
  <section class="l-subsection related-images">
    <h2 class="hide-for-small-down">Scenes From Past Trainings</h2>
    <h1 class="show-for-small-down collapse-link">Scenes From Past Trainings</h1>
    <div class="collapsable">
    <p>At the UX Conference, you get a full day of in-depth training with expert instructors. Learn more about <a href="../../../why-attend/index.php">why you should attend</a>.</p>

    
      
        
          
            <img srcset="https://media.nngroup.com/media/files/images/IMG_4023_copy_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale@2x.jpg 2x" src="https://media.nngroup.com/media/files/images/IMG_4023_copy_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale.jpg" alt="">
          
        
      
    
      
        
          
            <img srcset="https://media.nngroup.com/media/files/images/IMG_7582_copy_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale@2x.jpg 2x" src="https://media.nngroup.com/media/files/images/IMG_7582_copy_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale.jpg" alt="">
          
        
      
    
      
        
          
            <img srcset="https://media.nngroup.com/media/files/images/IMG_1049_copy_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale@2x.jpg 2x" src="https://media.nngroup.com/media/files/images/IMG_1049_copy_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale.jpg" alt="">
          
        
      
    
      
        
          
            <img srcset="https://media.nngroup.com/media/files/images/IMG_4881_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale@2x.jpg 2x" src="https://media.nngroup.com/media/files/images/IMG_4881_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale.jpg" alt="">
          
        
      
    
    </div>
  </section>
</div>


            </div>

            <p class="other-cities"><a href="../../../../courses/cross-channel-user-experience/index.php">Other cities</a> where Omnichannel Journeys and Customer Experience is offered.</p>

        </div>
    </div>
</article>

    </div>
    




<footer id="page-footer">
    <div class="row hide-for-print">
        <div class="column small-12 medium-6">
            <section class="footer-popular">
                
                <h1>Popular Topics</h1>
                <div style="float: left;  width: 15em; display: block;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../../../topic/applications/index.php">Application Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../../../topic/e-commerce/index.php">E-Commerce Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../../../topic/eyetracking/index.php">Eyetracking </a></li>
	<li style="padding-bottom: .5em;"><a href="../../../../topic/human-computer-interaction/index.php">Human Computer Interaction </a></li>
	<li style="padding-bottom: .5em;"><a href="../../../../topic/information-architecture/index.php">Information Architecture </a></li>
	<li style="padding-bottom: .5em;"><a href="../../../../topic/intranets/index.php">Intranet Design </a></li>
	<li><a href="../../../../topic/interaction-design/index.php">Interaction Design </a></li>
</ul>
</div>

<div style="float: left;  width: 15em; display: block; list-style: none;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../../../topic/mobile-and-tablet-design/index.php">Mobile and Tablet Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../../../topic/credibility-persuasion/index.php">Persuasive Design</a></li>
	<li style="padding-bottom: .5em;"><a href="../../../../topic/research-methods/index.php">Research Methods</a></li>
	<li style="padding-bottom: .5em;"><a href="../../../../topic/user-testing/index.php">User Testing </a></li>
	<li style="padding-bottom: .5em;"><a href="../../../../topic/web-usability/index.php">Web Usability </a></li>
	<li style="padding-bottom: .5em;"><a href="../../../../topic/writing-web/index.php">Web Writing &amp; Content Strategy </a></li>
</ul>
</div>

<p>&nbsp;</p>

                
            </section>
        </div>

        <div class="column small-12 medium-6">
            <div class="row">
                <div class="column small-6">
                    <section class="footer-about">
                        <h1>Follow Us</h1>
                        <ul>
	<li><a href="../../../../articles/subscribe/index.php">Subscribe to our weekly UX newsletter</a>,&nbsp;Jakob Nielsen&#39;s Alertbox</li>
	<li>Visit Don Norman&#39;s website, <a href="http://www.jnd.org/"> www.jnd.org </a></li>
	<li>Visit Bruce Tognazzini&#39;s website, <a href="http://www.asktog.com/"> www.asktog.com </a></li>
	<li style="padding-top: 6px;"><a class="twitter-follow-button" data-show-count="false" data-size="large" href="https://twitter.com/nngroup"><img alt="Follow @nngroup on Twitter" height="20" src="https://media.nngroup.com/media/editor/2016/12/30/twitter-follow-button.png" style="width: 117px; height: 20px;" width="117" /></a></li>
	<li style="padding-top: 6px;"><img alt="" height="14" src="https://media.nngroup.com/media/editor/2016/07/26/feed-icon-14x14.png" style="vertical-align:text-top;width:14px;margin-right:.25em;" width="14" /><a href="../../../../feed/rss/index.php">Subscribe to our RSS feed</a></li>
</ul>

                    </section>
                </div>

                <div class="column small-6">
                    <section class="footer-about right-column">
                        <h1>About
</h1>
                        <ul class="no-bullet">
	<li><a href="../../../../about/contact/index.php">Contact information</a></li>
	<li><a href="../../../../about/index.php">About Us</a></li>
	<li><a href="../../../../about/why-nng/index.php">Why NN/g? </a></li>
	<li><a href="../../../../return-policy/index.php">Return Policy</a></li>
</ul>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="copyright column small-12">
            <p><a href="../../../../copyright-and-reprint-info/index.php">Copyright</a> &copy; 1998-2017 Nielsen Norman Group, All Rights Reserved.</p>
        </div>
    </div>
</footer>


    


        <script src="https://media.nngroup.com/static/vendor/fastclick/js/fastclick.min.js"></script>
        <script src="https://media.nngroup.com/static/vendor/foundation/js/foundation.min.js"></script>
        <script src="http://media.nngroup.com/static/js/app.js"></script>
        <script src="http://media.nngroup.com/static/js/scroll-to-fixed.js"></script><script>var externalLinks = document.querySelectorAll('a[href^="http"]');for (var i = externalLinks.length-1; i >= 0; i--) {externalLinks[i].addEventListener("click", function(event) { alert("External links are disabled for this test."); event.preventDefault(); }, false);}var mailLinks = document.querySelectorAll('a[href^="mailto"]');for (var i = mailLinks.length-1; i >= 0; i--) {mailLinks[i].addEventListener("click", function(event) { alert("Email links are disabled for this test."); event.preventDefault(); }, false);}var externalForms = document.querySelectorAll('form');for (var i = externalForms.length-1; i >= 0; i--) {externalForms[i].addEventListener("submit", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false); externalForms[i].addEventListener("click", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false);}</script>

        <script>
            
            $(function() {
                $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
                $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
            });
        </script>

        
    

    <script>
        $(function() {
            var toggle = $("#collapse-toggle");
            toggle.on("click", function() {
                $(".collapsable").toggleClass("collapsed");
                toggle.toggleClass("collapsed");
                return(false);
            })
        });
    </script>


        
        <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1072699960;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072699960/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
    </body>

<!-- Mirrored from www.nngroup.com/training/course/1626/cross-channel-user-experience/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:10:25 GMT -->
</html>
