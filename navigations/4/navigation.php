<!doctype html>
<html id="doc" class="no-js">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="/apps/navigation_styles/app/app.js"></script>
    <script>
        window.onclick = function() { addCoordinates() };
        window.addEventListener("touchstart", function() { addCoordinates() }, false);
    </script>
    <link href="/apps/navigation_styles/navigations/4/assets/style.css" rel="stylesheet"/>
</head>
<body>
<ul id="navs" data-open="Menu -" data-close="Menu +">
    <li><a href="/apps/navigation_styles/navigations/4/index.php">Home</a></li>
    <li><a href="/apps/navigation_styles/navigations/4/reports/index.php">Reports</a></li>
    <li><a href="/apps/navigation_styles/navigations/4/articles/index.php">Articles</a></li>
    <li><a href="/apps/navigation_styles/navigations/4/training/index.php">Training</a></li>
    <li><a href="/apps/navigation_styles/navigations/4/consulting/index.php">Consulting</a></li>
    <li><a href="/apps/navigation_styles/navigations/4/about/index.php">About NN/G</a></li>
</ul>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="/apps/navigation_styles/navigations/4/assets/scripts.js"></script>
</body>
</html>
