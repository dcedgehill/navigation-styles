<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->





    
<!-- Mirrored from www.nngroup.com/books-prioritizing-web-usability-toc-german/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:13:19 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1026.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"beacon":"bam.nr-data.net","queueTime":14,"applicationTime":139,"agent":"","licenseKey":"1a30ef9931","errorBeacon":"bam.nr-data.net","applicationID":"2048992","transactionName":"NFxSMRBUX0EDUkIPDA0WdhAMVkVbDV8ZFgIEXFIMFkYfRAtUQRVZM1hXADRcVEVMVlMS"}</script>
        <title>Prioritizing Web Usability book: German Translation</title><meta property="og:title" content="Prioritizing Web Usability book: German Translation" />
  
        
        <meta name="description" content="German Translation of Prioritizing Web Usability, book by Nielsen Norman Group">
        <meta property="og:description" content="German Translation of Prioritizing Web Usability, book by Nielsen Norman Group" />
        
  
        
	
        

        

        <meta name="robots" content="noarchive">
        <meta name="viewport" content="width=device-width">

        <link rel="search" type="application/opensearchdescription+xml" title="NN/g Search" href="../opensearch.xml" />
        <link rel="shortcut icon" href="http://media.nngroup.com/static/img/favicon.ico" />

        <link rel="stylesheet" href="https://media.nngroup.com/static/vendor/font-awesome/css/font-awesome.min.css">

        
        <link rel="stylesheet" href="https://media.nngroup.com/static/css/7232a960f1df.css" type="text/css" />
        <link rel="stylesheet" href="http://fast.fonts.com/t/1.css?apiType=css&amp;projectid=c5846260-9def-46ec-9407-0f1349bd2d64" type="text/css">

        <script src="https://media.nngroup.com/static/vendor/modernizr/js/modernizr.min.js"></script>

        <script type="text/javascript" src="https://media.nngroup.com/static/js/3283be5def74.js"></script>
        

        

        
        
        <!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=229034,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('http://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->


<!-- Google Analytics Tracking Code for http://nngroup.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23132681-1', 'nngroup.com');
  ga('require', 'linkid', 'https://www.nngroup.com/books-prioritizing-web-usability-toc-german/linkid.js');
  ga('require', 'displayfeatures');

// Optimizely Universal Analytics Integration
 window.optimizely = window.optimizely || [];
 window.optimizely.push("activateUniversalAnalytics");

  ga('send', 'pageview');

</script>
<script>
/**
* Function to track a click on an outbound link in Google Analytics.
*/
function trackOutboundLink(label, url) {
    if(window.ga && ga.loaded) {
        ga('send', 'event', 'outbound', 'click', label, {'hitCallback': function(){document.location = url;}
        });
    } else {
        document.location = url;
    }
}
/**
* Function to track a click on a link in the article announcement area.
*/
$('#articleAnnouncement a').on('click', function(){
  ga('send', 'event', 'link', 'click', 'articleAnnouncement');
});
</script>

<script type="text/javascript">
 setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2421.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



<!-- Hotjar Tracking Code for http://nngroup.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:121605,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

        <script src="https://media.nngroup.com/static/js/analytics/track_outbound_link.js"></script>
        
        
        
    </head>
    <body class="location-about">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
    <header id="page-header">
        <div id="cart" class="shopping-cart mobile-cart show-for-small-down"></div>
        <div class="row">
            <div id="masthead" class="masthead small-12 medium-7 column">

                <!-- The button to toggles menu on small screen -->

                <h1><a href="../index.php"><img src="http://media.nngroup.com/static/img/logo-wordmark.png" width="74" height="30"> Nielsen <span class="norman">Norman</span> Group</a></h1>
                <p class="tagline show-for-medium-up">Evidence-Based User Experience Research, Training, and Consulting</p>

                <!-- The button to toggles form search on small screen -->


                <div style="clear: right;width:  100%;height: 1px;"></div><?php define('ROOT_PATH',$_SERVER["DOCUMENT_ROOT"] . '/apps/navigation_styles/navigations/4'); ?><?php include(ROOT_PATH)."/navigation.php"; ?>

                <!-- Drop down menu on small screen -->
                <div id="hideNav" class="show-for-small-down">
                  
                  <ul class="menu-content">
                    <li><a href="../index.php">Home</a></li>
                    <li class="has-submenu">
                      <a>Training</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../training/index.php">Overview</a></li>
                        
<li id="nav-ux-conference"><a href="../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../online-seminars/index.php"><span>Online Seminars</span></a></li>

                      </ul>
                    </li>
                    <li class="has-submenu">
                      <a>Consulting</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
  <li><a href="../consulting/index.php">Overview</a></li>
  
  <li><a href="../consulting/benchmarking/index.php">Benchmarking</a></li>
  
  <li><a href="../consulting/expert-design-review/index.php">Expert Design Review</a></li>
  
  <li><a href="../consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
  
  <li><a href="../consulting/usability-testing/index.php">Usability Testing </a></li>
  
  <li><a href="../consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
  
  <li><a href="../in-house-training/index.php">In-House Training</a></li>
</ul>

                    </li>

                    <li><a href="../reports/index.php">Reports</a></li>
                    <li><a href="../articles/index.php">Articles</a></li>
                    <li class="has-submenu"><a>About NN/G</a><i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../about/index.php">Overview</a></li>
                        <li><a href="../people/index.php">People</a></li>
                        <li><a href="../about/why-nng/index.php">Why NN/g?</a></li>
                        <li><a href="../about/contact/index.php">Contact</a></li>
                        <li><a href="../news/index.php">News</a></li>
                        <li><a href="../about/history/index.php">History</a></li>
                        <li><a href="../books/index.php">Books</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                </div>

                <hr class="header-divider show-for-small-only"/>


            </div>
       </div>

        <div id="hideNav">            <nav class="primary-nav small-12 column">
                <ul>
                    <li><a class="navitem" id="nav-home" href="../index.php">Home</a></li>
                    <li><a class="navitem" id="nav-training" href="../training/index.php">Training</a></li>
                    <li><a class="navitem" id="nav-consulting" href="../consulting/index.php">Consulting</a></li>
                    <li><a class="navitem" id="nav-reports" href="../reports/index.php">Reports</a></li>
                    <li><a class="navitem" id="nav-articles" href="../articles/index.php">Articles</a></li>
                    <li><a class="navitem" id="nav-about" href="../about/index.php">About NN/G</a></li>
                </ul>
            </nav>
            <nav class="secondary-nav small-12 medium-12 large-12 columns">
                
                
            </nav>
        </div>
    </header>
    <div class="l-content">
    
<div class="row">
    <div class="small-12 column l-subsection">
        <h1>
 <cite>
  Web Usability
 </cite>
 : German Translation of
 <cite>
  Prioritizing Web Usability
 </cite>
</h1>
<div style="float: right; width: 200px; margin: 0 0 1ex 1.5ex">
 <a href="http://www.amazon.de/gp/product/3827327636/" title="Book page at Amazon.de">
  <img alt="Prioritizing Web Usability - German Book Cover" src="http://media.nngroup.com.s3.amazonaws.com/media/editor/2012/12/31/prioritizing_web_usability_german-cover-small_.gif" style="width: 200px; height: 259px;" />
  <cite>
   Web Usability
  </cite>
 </a>
 <br />
 Addison Wesley Verlag
 <br />
 ISBN 3-8273-2763-6
</div>
<p>
 German table of contents for
 <a href="../books/prioritizinig-web-usability/index.php">
  Prioritizing Web Usability
 </a>
 , by Jakob Nielsen and Hoa Loranger:
 <span lang="DE">
 </span>
</p>
<h2>
 <span lang="DE">
  Inhaltsverzeichnis
 </span>
</h2>
<h3>
 <span lang="DE">
  Vorwort
 </span>
</h3>
<p>
 <span lang="DE">
  <em>
   Was ist Usability?
  </em>
  <br />
  <em>
   Quellen f&uuml;r detaillierte Usability-Forschungsergebnisse
  </em>
  <br />
  Usability einst und heute
  <br />
  Wer sollte dieses Buch lesen?
 </span>
</p>
<p>
 &nbsp;
</p>
<h3>
 <span lang="DE">
  1 Einf&uuml;hrung: Nichts zu verbergen
 </span>
</h3>
<p>
 <span lang="DE">
  Woher bekommen wir unsere Daten
  <br />
  Wie f&uuml;hrten wir die Untersuchung f&uuml;r dieses Buch durch?
  <br />
  <em>
   Getestete Sites
  </em>
  <br />
  Und wenn sich eine Site ge&auml;ndert hat?
  <br />
  Noch einmal: Warum sollten Sie Benutzertests durchf&uuml;hren?
  <br />
  <em>
   Benutzertests in drei Tagen
  </em>
  <br />
  Die Ausnahmen
 </span>
</p>
<h3>
 <span lang="DE">
  2 Die Erfahrung der User im Web
 </span>
</h3>
<p>
 <span lang="DE">
  Wie gut nutzen Anwender das Web?
  <br />
  <em>
   Die Ma&szlig;einheit des Erfolgs
  </em>
  <br />
  Webweite Erfolgsraten
  <br />
  Erfolg durch Erfahrung
  <br />
  Die Zufriedenheit der Nutzer mit Websites
  <br />
  <em>
   Drei Richtlinien zur Unterst&uuml;tzung von tiefen Links
  </em>
  <br />
  Wie Anwender Websites nutzen
  <br />
  <em>
   Vier Ziele in drei&szlig;ig Sekunden
  </em>
  <br />
  Die Homepage: Viel zu sagen, wenig Zeit
  <br />
  Verhalten auf den inneren Seiten
  <br />
  <em>
   Tipp: Innere Seitenlinks verbessern
  </em>
  <br />
  <em>
   Der Aufstieg der &quot;Antwortmaschinen&quot;
  </em>
  <br />
  Die Dominanz der Suchmaschinen
  <br />
  <em>
   Vier M&ouml;glichkeiten, aus den Nutzern von Suchmaschinen Kapital zu schlagen
  </em>
  <br />
  <em>
   Organische contra gesponserte Links
  </em>
  <br />
  Wie die Benutzer die Ergebnisseite der Suchmaschine nutzen
  <br />
  <em>
   Die Nummer-1-Richtlinie f&uuml;r die Suchmaschinenoptimierung
  </em>
  <br />
  Keyword-Preise zum Absch&auml;tzen von Usability-Verbesserungen
  <br />
  <em>
   Wie man das optimale Gebot f&uuml;r eine Keyword-Anzeige in der Suchmaschine ermittelt
  </em>
  <br />
  <em>
   Wie viel ist verbesserte Usability wert?
  </em>
  <br />
  <em>
   Drei Gr&uuml;nde, Ihre Site zu verbessern
  </em>
  <br />
  <em>
   Tipp: Gestaltung f&uuml;r k&uuml;rzeres Scrollen
  </em>
  <br />
  Scrollen
  <br />
  <em>
   Definition: Standards und Konventionen
  </em>
  <br />
  &Uuml;bereinstimmung mit Design-Konventionen und Usability-Richtlinien
  <br />
  <em>
   Sieben Gr&uuml;nde f&uuml;r Standard-Design-Elemente
  </em>
  <br />
  <em>
   Empfehlenswerte Lekt&uuml;re
  </em>
  <br />
  &quot;Information Foraging&quot;
  <br />
  &quot;Information Scent&quot; - die Informationsf&auml;hrte
  <br />
  Nahrungswahl: Welche Sites sollen besucht werden?
  <br />
  <em>
   Drei M&ouml;glichkeiten, die Informationsf&auml;hrte zu verbessern
  </em>
  <br />
  Das Verlassen der F&auml;hrte: Wann sollte die Jagd anderswo fortgesetzt werden?
  <br />
  <em>
   Neue Design-Strategien zum Anlocken Informationshungriger
  </em>
  <br />
  <em>
   Weitere Informationen
  </em>
  <br />
  Das Navigationsverhalten des Informationsj&auml;gers
 </span>
</p>
<h3>
 <span lang="DE">
  3 Fr&uuml;here Web-Usability-Erkenntnisse auf dem Pr&uuml;fstand
 </span>
</h3>
<p>
 <span lang="DE">
  <em>
   Under Construction
  </em>
  <br />
  Acht Probleme haben sich nicht ge&auml;ndert
  <br />
  Besuchte Links &auml;ndern ihre Farbe nicht
  <br />
  <em>
   Warum Designer uns nicht glauben
  </em>
  <br />
  Deaktivieren der Zur&uuml;ck-Schaltfl&auml;che
  <br />
  <em>
   Fitts&#39; Gesetz von der Klickzeit
  </em>
  <br />
  Neue Browserfenster
  <br />
  <em>
   Der Fluch der Maximierung
  </em>
  <br />
  <em>
   Wie k&ouml;nnen Sie Windows verwenden, obwohl Sie nicht mit Fenstern umgehen k&ouml;nnen?
  </em>
  69
  <br />
  Pop-Up-Fenster
  <br />
  <em>
   Die meistgehassten Anzeigentechniken
  </em>
  <br />
  Design-Elemente, die wie Anzeigen aussehen
  <br />
  <em>
   Beeinflussen Sie die Nutzer w&auml;hrend der Tests nicht
  </em>
  <br />
  Die Verletzung webweiter Konventionen
  <br />
  Schwammige Inhalte und leeres Tamtam
  <br />
  Gedr&auml;ngte Inhalte und nicht scannbarer Text
  <br />
  Technologische &Auml;nderungen: Die Auswirkungen auf die Usability
  <br />
  <em>
   Die Richtlinien der Air Force von 1986 bestehen den Test der Zeit
  </em>
  <br />
  <em>
   Don Normans drei Ebenen des emotionalen Designs
  </em>
  <br />
  Lange Downloadzeiten
  <br />
  Frames
  <br />
  Unn&uuml;tze Flash-Elemente
  <br />
  <em>
   Flash: Gut, schlecht und brauchbar
  </em>
  <br />
  Suchergebnisse mit geringer Relevanz
  <br />
  Multimedia und lange Videos
  <br />
  <em>
   Teenager: Meister der Technologie?
  </em>
  90
  <br />
  Statische Layouts
  <br />
  <em>
   Die Mac-Trag&ouml;die
  </em>
  <br />
  Plattform&uuml;bergreifende Inkompatibilit&auml;t
  <br />
  <em>
   Mobile Ausgabeger&auml;te: Ein neues Argument f&uuml;r das plattform&uuml;bergreifende Design?
  </em>
  94
  <br />
  Anpassung: Wie Benutzer die Usability beeinflusst haben
  <br />
  Zweifelhafte Anklickbarkeit
  <br />
  Links, die nicht blau sind
  <br />
  Scrollen
  <br />
  Registrierung
  <br />
  Komplexe URLs
  <br />
  Pulldown- und verschachtelte Men&uuml;s
  <br />
  Beschr&auml;nkung: Wie Designer Usability-Probleme verringerten
  <br />
  Plug-ins und neueste Technologie
  <br />
  3D-Benutzeroberfl&auml;chen
  <br />
  Aufgebl&auml;htes Design
  <br />
  Splashscreens
  <br />
  Bewegte Grafiken und scrollender Text
  <br />
  Eigene Bedienelemente
  <br />
  <em>
   &quot;&Uuml;ber uns&quot;-Seiten sagen nicht genug aus
  </em>
  <br />
  Fehlende Angaben, wer hinter den Informationen steht
  <br />
  Kunstw&ouml;rter
  <br />
  Veraltete Inhalte
  <br />
  Inkonsistenz innerhalb einer Website
  <br />
  Voreilige Anforderung pers&ouml;nlicher Informationen
  <br />
  Mehrfache Sites
  <br />
  Verwaiste Seiten
  <br />
  Auswertung der fr&uuml;hen Ergebnisse
  <br />
  <em>
   Weitere Informationen
  </em>
 </span>
</p>
<h3>
 <span lang="DE">
  4 Usability-Probleme priorisieren
 </span>
</h3>
<p>
 <span lang="DE">
  <em>
   Wie schwerwiegend ist das Problem?
  </em>
  <br />
  <em>
   Den Schweregrad beziffern
  </em>
  <br />
  Wann ein Problem schwerwiegend ist
  <br />
  <em>
   Usability in Krankenh&auml;usern: Ein kritischer Zustand
  </em>
  <br />
  Das Skala des Elends
  <br />
  <em>
   Tipp: Das erste Gebot des E-Commerce
  </em>
  <br />
  Warum Benutzer scheitern
  <br />
  <em>
   Die f&uuml;nf h&auml;ufigsten Gr&uuml;nde f&uuml;r das Scheitern der Nutzer
  </em>
  <br />
  Gen&uuml;gt es, sich auf die schlimmsten Probleme zu konzentrieren?
 </span>
</p>
<h3>
 <span lang="DE">
  5 Die Suche
 </span>
</h3>
<p>
 <span lang="DE">
  <em>
   Tipp: Wann ben&ouml;tigen Sie eine Suchfunktion?
  </em>
  <br />
  Der momentane Zustand der Suchfunktionen
  <br />
  <em>
   Drei einfache Schritte zur besseren Suchfunktion
  </em>
  <br />
  <em>
   Folgendes erwarten Nutzer von der Suchfunktion
  </em>
  <br />
  <em>
   Tipp: Wann ist eine Suche keine Suche?
  </em>
  <br />
  Wie die Suche funktionieren sollte
  <br />
  Die Benutzeroberfl&auml;che der Suchfunktion
  <br />
  <em>
   Tipp: Ihre Site ist keine Suchmaschine
  </em>
  <br />
  <em>
   Tipp: Gestalten Sie ein breites Suchfeld
  </em>
  <br />
  Die L&auml;nge der Suchbegriffe und die Breite des Suchfelds
  <br />
  Die erweiterte Suche
  <br />
  <em>
   Gestaltung von Klickzielen
  </em>
  <br />
  Die Ergebnisseiten der Suchmaschinen
  <br />
  <em>
   Tipp: Datumskonventionen f&uuml;r Ergebnisseiten
  </em>
  <br />
  <em>
   Tipp: Unterst&uuml;tzen Sie Nutzer mit schlechter Rechtschreibung
  </em>
  <br />
  &quot;Best Bets&quot;
  <br />
  <em>
   Vier M&ouml;glichkeiten, um &quot;Best Bets&quot; zu erzeugen
  </em>
  <br />
  <em>
   Die &quot;Best Bets&quot; verwalten
  </em>
  <br />
  Die Ergebnisseite sortieren
  <br />
  Keine Ergebnisse gefunden
  <br />
  Ein einziges Ergebnis gefunden
  <br />
  Die Suchmaschinenoptimierung
  <br />
  <em>
   Black-Hat-Tricks zur Suchmaschinenoptimierung
  </em>
  <br />
  <em>
   Namen vergeben
  </em>
  <br />
  <em>
   Tipp: Die Vorteile von Nur-Text-Werbung
  </em>
  <br />
  <em>
   Tipp: Den Wert von Suchanzeigen verfolgen
  </em>
  <br />
  <em>
   Die wichtigste Richtlinie f&uuml;r die linguistische Suchmaschinenoptimierung
  </em>
  <br />
  Linguistische Suchmaschinenoptimierung
  <br />
  <em>
   Tipp: Die &uuml;berm&auml;&szlig;ige Verwendung von Schl&uuml;sselw&ouml;rtern bringt nichts
  </em>
  <br />
  <em>
   Tipp: Denken Sie nicht in Schl&uuml;sselw&ouml;rtern, sondern in Redewendungen
  </em>
  <br />
  Architektonische Suchmaschinenoptimierung
  <br />
  <em>
   Wie die Suchmaschinen die Reputation einer Site bestimmen
  </em>
  <br />
  Suchmaschinenoptimierung nach Link-Reputation
 </span>
</p>
<h3>
 <span lang="DE">
  6 Navigations- und Informationsarchitektur: Bin ich schon am Ziel?
 </span>
</h3>
<p>
 <span lang="DE">
  <em>
   Literaturempfehlung zur Informationsarchitektur
  </em>
  <br />
  Passen Sie die Site-Struktur an die Erwartungen der Benutzer an
  <br />
  <em>
   Informationsarchitektur im Intranet
  </em>
  <br />
  Navigation: Bleiben Sie konsistent
  <br />
  <em>
   Kinder m&ouml;gen Minesweeping
  </em>
  <br />
  Navigation: Vermeiden Sie den Coolness-Faktor
  <br />
  <em>
   Gehen Sie keinen Doubles auf den Leim
  </em>
  <br />
  Vermeiden Sie vollgestopfte Seiten und &Uuml;berfl&uuml;ssiges
  <br />
  Link- und Button-Beschriftungen: Seien Sie deutlich
  <br />
  Vertikale Dropdown-Men&uuml;s: In der K&uuml;rze liegt die W&uuml;rze
  <br />
  Men&uuml;s mit mehreren Ebenen: Weniger ist mehr
  <br />
  Kann ich das anklicken?
  <br />
  <em>
   Affordances
  </em>
  <br />
  Direkter Zugriff von der Homepage
  <br />
  <em>
   Weitere Informationen
  </em>
 </span>
</p>
<h3>
 <span lang="DE">
  7 Typografie: Lesbarkeit &amp; Leserlichkeit
 </span>
</h3>
<p>
 <span lang="DE">
  <em>
   Tipp: Die Nachteile von Blindtext
  </em>
  <br />
  <em>
   Vier Grundregeln zum Schriftbild
  </em>
  <br />
  Der Textk&ouml;rper: Die Zehn-Punkt-Regel
  <br />
  <em>
   Tipp: Vermeiden Sie Kantengl&auml;ttung
  </em>
  <br />
  Das Alter ist nicht das Problem
  <br />
  <em>
   Tipp: Der gleiche Schriftgrad wirkt nicht immer gleich gro&szlig;
  </em>
  <br />
  Hardware-Unterschiede einplanen
  <br />
  Gebr&auml;uchliche Bildschirmaufl&ouml;sungen
  <br />
  <em>
   Zug&auml;nglichkeit betrifft uns alle
  </em>
  <br />
  <em>
   Die Regel von der relativen Gr&ouml;&szlig;e
  </em>
  <br />
  Relative Definitionen
  <br />
  Die Gestaltung f&uuml;r Benutzer mit eingeschr&auml;nkter Sehkraft
  <br />
  Die Auswahl der Schriftarten
  <br />
  Im Zweifelsfall immer Verdana
  <br />
  <em>
   Wann werden Bildschirmmedien so gut lesbar sein wie Druckerzeugnisse?
  </em>
  <br />
  Schriften und Farben mischen
  <br />
  <em>
   Pl&auml;doyer gegen Gro&szlig;buchstaben
  </em>
  <br />
  Text- und Hintergrundkontraste
  <br />
  <em>
   Zwei Wege zu auff&auml;lligen Farben
  </em>
  <br />
  Rot-Gr&uuml;n-Sehschw&auml;che ist weit verbreitet
  <br />
  Grafische Texte
  <br />
  Animierter Text
  <br />
  <em>
   Weitere Informationen
  </em>
 </span>
</p>
<h3>
 <span lang="DE">
  8 F&uuml;r das Web texten
 </span>
</h3>
<p>
 <span lang="DE">
  Wie Websites durch schlechte Texte zu Flops werden
  <br />
  <em>
   Tipp: Besorgen Sie sich einen Webtexter
  </em>
  <br />
  Wie Webnutzer lesen
  <br />
  <em>
   Warum Nutzer scannen
  </em>
  <br />
  <em>
   Tipp: Machen Sie sich mit Ihrer Zielgruppe vertraut
  </em>
  <br />
  Schreiben Sie f&uuml;r Ihre Leser
  <br />
  <em>
   Drei Richtlinien f&uuml;r bessere Webtexte
  </em>
  <br />
  Schreiben Sie in einfacher Sprache
  <br />
  <em>
   Leseschwierigkeiten ber&uuml;cksichtigen
  </em>
  <br />
  R&uuml;hren Sie die Werbetrommel nicht zu stark
  <br />
  <em>
   Tipp: Wann und wo Sie die Lobges&auml;nge anstimmen k&ouml;nnen
  </em>
  <br />
  <em>
   Textbeispiele: Vorher und nachher
  </em>
  <br />
  <em>
   Tipp: Der Test mit den zwei S&auml;tzen
  </em>
  <br />
  <em>
   Formulieren Sie kurz und knackig
  </em>
  <br />
  Schl&uuml;sselpunkte zusammenfassen und reduzieren
  <br />
  <em>
   Tipp: Aussagekr&auml;ftige Beschriftung verwenden
  </em>
  <br />
  <em>
   Usability beschleunigt
  </em>
  <br />
  Texte lesbar formatieren
  <br />
  Die Hervorhebung von Schl&uuml;sselw&ouml;rtern
  <br />
  Knappe und erl&auml;uternde Seitentitel und &Uuml;berschriften
  <br />
  <em>
   Drei Richtlinien f&uuml;r die Anordnung von &Uuml;berschriften
  </em>
  <br />
  Verwenden Sie Aufz&auml;hlungen und Nummerierungen
  <br />
  <em>
   Die sieben wichtigsten Richtlinien f&uuml;r Listen
  </em>
  <br />
  <em>
   Tipp: Paralleler Satzbau ist wichtig
  </em>
  <br />
  <em>
   Weitere Informationen
  </em>
  <br />
  Verwenden Sie kurze Abs&auml;tze
 </span>
</p>
<h3>
 <span lang="DE">
  9 Gute Produktinformationen bereitstellen
 </span>
</h3>
<p>
 <span lang="DE">
  <em>
   Tipp: Wo sollten die Preise stehen?
  </em>
  <br />
  Zeigen Sie die Preise
  <br />
  Keine Ausreden
  <br />
  <em>
   Tipp: Sch&auml;tzpreise sind besser als nichts
  </em>
  <br />
  Zus&auml;tzliche Kosten offenlegen
  <br />
  Gewinnen Sie das Vertrauen Ihrer Kunden
  <br />
  Beschreiben Sie das Produkt
  <br />
  Zeigen Sie Produktabbildungen und Produktillustrationen
  <br />
  <em>
   Testfahrt auf einer Automobil-Site
  </em>
  <br />
  <em>
   F&uuml;nf Hauptfehler bei der Bebilderung
  </em>
  <br />
  Pr&auml;sentieren Sie Ihre Produkte schichtweise
  <br />
  Zeigen Sie Ihre Referenzen
  <br />
  Erleichtern Sie Vergleiche
  <br />
  Filtern und Sortieren
  <br />
  F&ouml;rdern Sie Ihre Verk&auml;ufe mit hochwertigen Inhalten
  <br />
  <em>
   Vier Gr&uuml;nde f&uuml;r informative Texte
  </em>
  <br />
  <em>
   Die verkaufen nichts, oder?
  </em>
  <br />
  <em>
   Weitere Informationen
  </em>
 </span>
</p>
<h3>
 <span lang="DE">
  10 Seitenelemente pr&auml;sentieren
 </span>
</h3>
<p>
 <span lang="DE">
  <em>
   Die &quot;Drei-Klick-Regel&quot; kann sich verheerend auswirken
  </em>
  <br />
  Sollen Seiten scrollbar sein?
  <br />
  <em>
   Vier Regeln zum Scrollen
  </em>
  <br />
  <em>
   Tipp: Es gibt keine magische Zahl
  </em>
  <br />
  Die Nutzer bei der Hand nehmen
  <br />
  Gleichartiges zusammenhalten
  <br />
  Schlampig formatierte Formulare
  <br />
  <em>
   Sieh mich an!
  </em>
  <br />
  Erf&uuml;llen Sie die Erwartungen Ihrer Benutzer
  <br />
  Leerraum verwenden
 </span>
</p>
<h3>
 <span lang="DE">
  11 Technologie an die Bed&uuml;rfnisse der Nutzer anpassen
 </span>
</h3>
<p>
 <span lang="DE">
  <em>
   R&uuml;ckblick in das Jahr 2000: Eine Anmerkung von Jakob Nielsen
  </em>
  <br />
  Verwenden Sie Multimedia, wenn es Ihrem Publikum weiterhilft
  <br />
  <em>
   Tipp: Alternativen Zugang erm&ouml;glichen
  </em>
  <br />
  Multimediale H&uuml;rden &uuml;berwinden
  <br />
  Denken Sie an die Benutzer mit Low-Tech-Ausr&uuml;stung
  <br />
  <em>
   Sites f&uuml;r Kinder: Bleiben Sie auf dem Boden
  </em>
  <br />
  Richten Sie sich nach der Verbindungsgeschwindigkeit Ihrer Zielgruppe
  <br />
  Bieten Sie eine einfache und fehlerfreie Statusanzeige
  <br />
  <em>
   Achten Sie auf Ihre Ausdrucksweise
  </em>
  <br />
  Gehen Sie von einem geringen technischen Know-how aus
  <br />
  Ermitteln Sie die Bandbreite der Nutzer
  <br />
  Bleiben Sie bei vertrauten Elementen
  <br />
  <em>
   Tipp: Pop-Ups bringen meist nichts
  </em>
  <br />
  <em>
   Tipp: Scrollbalken sollten standardm&auml;&szlig;ig aussehen
  </em>
  <br />
  <em>
   Tipp: Multimedia oder nicht?
  </em>
  <br />
  Vermeiden Sie Multimedia im &Uuml;berfluss
  <br />
  <em>
   Wie schaltet man das aus?
  </em>
  <br />
  Drehen Sie die Lautst&auml;rke herunter
  <br />
  <em>
   Tipp: Der richtige Zeitpunkt f&uuml;r Werbepausen
  </em>
  <br />
  Produzieren Sie Videos f&uuml;r das Netz
  <br />
  &Uuml;ben Sie sich in Einfachheit
  <br />
  <em>
   Die Verbesserung Ihrer Site: Fr&uuml;her oder sp&auml;ter?
  </em>
  <br />
  <em>
   Drei Tipps: Vereinfachen. Vereinfachen. Vereinfachen
  </em>
  <br />
  <em>
   Weitere Informationen
  </em>
  <br />
  Der Weg zu einem eleganteren Design
 </span>
</p>
<h3>
 <span lang="DE">
  12 Zum Schluss: Gestaltung, die funktioniert
 </span>
</h3>
<p>
 <span lang="DE">
  &Uuml;berpr&uuml;fen Sie Ihre Annahmen
 </span>
</p>
<p>
 &nbsp;
</p>
<h3>
 <span lang="DE">
  Index
 </span>
</h3>

    </div>
</div>

    </div>
    




<footer id="page-footer">
    <div class="row hide-for-print">
        <div class="column small-12 medium-6">
            <section class="footer-popular">
                
                <h1>Popular Topics</h1>
                <div style="float: left;  width: 15em; display: block;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../topic/applications/index.php">Application Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../topic/e-commerce/index.php">E-Commerce Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../topic/eyetracking/index.php">Eyetracking </a></li>
	<li style="padding-bottom: .5em;"><a href="../topic/human-computer-interaction/index.php">Human Computer Interaction </a></li>
	<li style="padding-bottom: .5em;"><a href="../topic/information-architecture/index.php">Information Architecture </a></li>
	<li style="padding-bottom: .5em;"><a href="../topic/intranets/index.php">Intranet Design </a></li>
	<li><a href="../topic/interaction-design/index.php">Interaction Design </a></li>
</ul>
</div>

<div style="float: left;  width: 15em; display: block; list-style: none;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../topic/mobile-and-tablet-design/index.php">Mobile and Tablet Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../topic/credibility-persuasion/index.php">Persuasive Design</a></li>
	<li style="padding-bottom: .5em;"><a href="../topic/research-methods/index.php">Research Methods</a></li>
	<li style="padding-bottom: .5em;"><a href="../topic/user-testing/index.php">User Testing </a></li>
	<li style="padding-bottom: .5em;"><a href="../topic/web-usability/index.php">Web Usability </a></li>
	<li style="padding-bottom: .5em;"><a href="../topic/writing-web/index.php">Web Writing &amp; Content Strategy </a></li>
</ul>
</div>

<p>&nbsp;</p>

                
            </section>
        </div>

        <div class="column small-12 medium-6">
            <div class="row">
                <div class="column small-6">
                    <section class="footer-about">
                        <h1>Follow Us</h1>
                        <ul>
	<li><a href="../articles/subscribe/index.php">Subscribe to our weekly UX newsletter</a>,&nbsp;Jakob Nielsen&#39;s Alertbox</li>
	<li>Visit Don Norman&#39;s website, <a href="http://www.jnd.org/"> www.jnd.org </a></li>
	<li>Visit Bruce Tognazzini&#39;s website, <a href="http://www.asktog.com/"> www.asktog.com </a></li>
	<li style="padding-top: 6px;"><a class="twitter-follow-button" data-show-count="false" data-size="large" href="https://twitter.com/nngroup"><img alt="Follow @nngroup on Twitter" height="20" src="https://media.nngroup.com/media/editor/2016/12/30/twitter-follow-button.png" style="width: 117px; height: 20px;" width="117" /></a></li>
	<li style="padding-top: 6px;"><img alt="" height="14" src="https://media.nngroup.com/media/editor/2016/07/26/feed-icon-14x14.png" style="vertical-align:text-top;width:14px;margin-right:.25em;" width="14" /><a href="../feed/rss/index.php">Subscribe to our RSS feed</a></li>
</ul>

                    </section>
                </div>

                <div class="column small-6">
                    <section class="footer-about right-column">
                        <h1>About
</h1>
                        <ul class="no-bullet">
	<li><a href="../about/contact/index.php">Contact information</a></li>
	<li><a href="../about/index.php">About Us</a></li>
	<li><a href="../about/why-nng/index.php">Why NN/g? </a></li>
	<li><a href="../return-policy/index.php">Return Policy</a></li>
</ul>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="copyright column small-12">
            <p><a href="../copyright-and-reprint-info/index.php">Copyright</a> &copy; 1998-2017 Nielsen Norman Group, All Rights Reserved.</p>
        </div>
    </div>
</footer>


    


        <script src="https://media.nngroup.com/static/vendor/fastclick/js/fastclick.min.js"></script>
        <script src="https://media.nngroup.com/static/vendor/foundation/js/foundation.min.js"></script>
        <script src="http://media.nngroup.com/static/js/app.js"></script>
        <script src="http://media.nngroup.com/static/js/scroll-to-fixed.js"></script><script>var externalLinks = document.querySelectorAll('a[href^="http"]');for (var i = externalLinks.length-1; i >= 0; i--) {externalLinks[i].addEventListener("click", function(event) { alert("External links are disabled for this test."); event.preventDefault(); }, false);}var mailLinks = document.querySelectorAll('a[href^="mailto"]');for (var i = mailLinks.length-1; i >= 0; i--) {mailLinks[i].addEventListener("click", function(event) { alert("Email links are disabled for this test."); event.preventDefault(); }, false);}var externalForms = document.querySelectorAll('form');for (var i = externalForms.length-1; i >= 0; i--) {externalForms[i].addEventListener("submit", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false); externalForms[i].addEventListener("click", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false);}</script>

        <script>
            
            $(function() {
                $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
                $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
            });
        </script>

        


        
        <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1072699960;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072699960/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
    </body>

<!-- Mirrored from www.nngroup.com/books-prioritizing-web-usability-toc-german/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:13:19 GMT -->
</html>
