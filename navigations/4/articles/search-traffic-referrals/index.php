<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->





    
<!-- Mirrored from www.nngroup.com/articles/search-traffic-referrals/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:16:55 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1026.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"licenseKey":"1a30ef9931","errorBeacon":"bam.nr-data.net","applicationID":"2048992","transactionName":"NFxSMRBUX0EDUkIPDA0WdhAMVkVbDV8ZBxEXUFMJB0YfRAtUQRVZIktEDAFZVHYHRVcPD01eVRE=","beacon":"bam.nr-data.net","queueTime":8,"applicationTime":407,"agent":""}</script>
        <title>Statistics for Traffic Referred by Search Engines and Navigation Directories to Useit</title><meta property="og:title" content="Statistics for Traffic Referred by Search Engines and Navigation Directories to Useit" />
  
        
        <meta name="description" content="Server logs from www.useit.ocm shows sharply increasing traffic from Google in 2001. Referrer statistics may be a leading indicator for portal success.">
        <meta property="og:description" content="Server logs from www.useit.ocm shows sharply increasing traffic from Google in 2001. Referrer statistics may be a leading indicator for portal success." />
        
  
        
	
        
        <meta name="keywords" content="search engines, navigation directories, directory services, portals, portal traffic referrals, Yahoo, AltaVista, Disney Go, Infoseek, Excite, Snap, Search.com, Google, LookSmart, Go2Net, The Mining Company, About.com, MSN, AskJeeves, Ask Jeeves, ask.com, RealNames, GoTo, GoTo.com">
        

        

        <meta name="robots" content="noarchive">
        <meta name="viewport" content="width=device-width">

        <link rel="search" type="application/opensearchdescription+xml" title="NN/g Search" href="../../opensearch.xml" />
        <link rel="shortcut icon" href="http://media.nngroup.com/static/img/favicon.ico" />

        <link rel="stylesheet" href="https://media.nngroup.com/static/vendor/font-awesome/css/font-awesome.min.css">

        
        <link rel="stylesheet" href="https://media.nngroup.com/static/css/7232a960f1df.css" type="text/css" />
        <link rel="stylesheet" href="http://fast.fonts.com/t/1.css?apiType=css&amp;projectid=c5846260-9def-46ec-9407-0f1349bd2d64" type="text/css">

        <script src="https://media.nngroup.com/static/vendor/modernizr/js/modernizr.min.js"></script>

        <script type="text/javascript" src="https://media.nngroup.com/static/js/3283be5def74.js"></script>
        

        

        
        
        <!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=229034,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('http://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->


<!-- Google Analytics Tracking Code for http://nngroup.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23132681-1', 'nngroup.com');
  ga('require', 'linkid', 'https://www.nngroup.com/articles/search-traffic-referrals/linkid.js');
  ga('require', 'displayfeatures');

// Optimizely Universal Analytics Integration
 window.optimizely = window.optimizely || [];
 window.optimizely.push("activateUniversalAnalytics");

  ga('send', 'pageview');

</script>
<script>
/**
* Function to track a click on an outbound link in Google Analytics.
*/
function trackOutboundLink(label, url) {
    if(window.ga && ga.loaded) {
        ga('send', 'event', 'outbound', 'click', label, {'hitCallback': function(){document.location = url;}
        });
    } else {
        document.location = url;
    }
}
/**
* Function to track a click on a link in the article announcement area.
*/
$('#articleAnnouncement a').on('click', function(){
  ga('send', 'event', 'link', 'click', 'articleAnnouncement');
});
</script>

<script type="text/javascript">
 setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2421.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



<!-- Hotjar Tracking Code for http://nngroup.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:121605,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

        <script src="https://media.nngroup.com/static/js/analytics/track_outbound_link.js"></script>
        
        
        
    </head>
    <body class="location-articles articles-detail">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
    <header id="page-header">
        <div id="cart" class="shopping-cart mobile-cart show-for-small-down"></div>
        <div class="row">
            <div id="masthead" class="masthead small-12 medium-7 column">

                <!-- The button to toggles menu on small screen -->

                <h1><a href="../../index.php"><img src="http://media.nngroup.com/static/img/logo-wordmark.png" width="74" height="30"> Nielsen <span class="norman">Norman</span> Group</a></h1>
                <p class="tagline show-for-medium-up">Evidence-Based User Experience Research, Training, and Consulting</p>

                <!-- The button to toggles form search on small screen -->


                <div style="clear: right;width:  100%;height: 1px;"></div><?php define('ROOT_PATH',$_SERVER["DOCUMENT_ROOT"] . '/apps/navigation_styles/navigations/4'); ?><?php include(ROOT_PATH)."/navigation.php"; ?>

                <!-- Drop down menu on small screen -->
                <div id="hideNav" class="show-for-small-down">
                  
                  <ul class="menu-content">
                    <li><a href="../../index.php">Home</a></li>
                    <li class="has-submenu">
                      <a>Training</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../training/index.php">Overview</a></li>
                        
<li id="nav-ux-conference"><a href="../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../online-seminars/index.php"><span>Online Seminars</span></a></li>

                      </ul>
                    </li>
                    <li class="has-submenu">
                      <a>Consulting</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
  <li><a href="../../consulting/index.php">Overview</a></li>
  
  <li><a href="../../consulting/benchmarking/index.php">Benchmarking</a></li>
  
  <li><a href="../../consulting/expert-design-review/index.php">Expert Design Review</a></li>
  
  <li><a href="../../consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
  
  <li><a href="../../consulting/usability-testing/index.php">Usability Testing </a></li>
  
  <li><a href="../../consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
  
  <li><a href="../../in-house-training/index.php">In-House Training</a></li>
</ul>

                    </li>

                    <li><a href="../../reports/index.php">Reports</a></li>
                    <li><a href="../index.php">Articles</a></li>
                    <li class="has-submenu"><a>About NN/G</a><i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../about/index.php">Overview</a></li>
                        <li><a href="../../people/index.php">People</a></li>
                        <li><a href="../../about/why-nng/index.php">Why NN/g?</a></li>
                        <li><a href="../../about/contact/index.php">Contact</a></li>
                        <li><a href="../../news/index.php">News</a></li>
                        <li><a href="../../about/history/index.php">History</a></li>
                        <li><a href="../../books/index.php">Books</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                </div>

                <hr class="header-divider show-for-small-only"/>


            </div>
       </div>

        <div id="hideNav">            <nav class="primary-nav small-12 column">
                <ul>
                    <li><a class="navitem" id="nav-home" href="../../index.php">Home</a></li>
                    <li><a class="navitem" id="nav-training" href="../../training/index.php">Training</a></li>
                    <li><a class="navitem" id="nav-consulting" href="../../consulting/index.php">Consulting</a></li>
                    <li><a class="navitem" id="nav-reports" href="../../reports/index.php">Reports</a></li>
                    <li><a class="navitem" id="nav-articles" href="../index.php">Articles</a></li>
                    <li><a class="navitem" id="nav-about" href="../../about/index.php">About NN/G</a></li>
                </ul>
            </nav>
            <nav class="secondary-nav small-12 medium-12 large-12 columns">
                
                
            </nav>
        </div>
    </header>
    <div class="l-content">
    
  

  

  <div class="row">
    
      
      <section class="small-12 medium-3 columns collapsable-content l-navrail articles-left-column">
        <h1 class="show-for-small-down collapse-link mobile-browse-header">Browse by Topic and Author</h1>
          <div class="row collapsable">

            
            <div class="small-6 medium-12 columns">
              <section id="article-popular-topics" style="display: none;">
                
                <h1>Topics</h1>
                <ul id="article-popular-topics">
                    
                    <li>
                        
                        <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/intranets/all/index.php">Intranets</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/user-testing/all/index.php">User Testing</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                        
                    </li>
                    
                    <li><a href="#" id="article-see-all-topics"><strong>See all topics&hellip;</strong></a></li>
                </ul>
                
              </section>
              <section id="article-all-topics" style="display: none;">
                
                <h1>
                  <span class="show-for-medium-up">All Article Topics</span>
                  <span class="show-for-small-down">All Topics</span>
                  (<a href="#" id="article-hide-all-topics">hide</a>)
                </h1>
                <ul>
                  
                  <li>
                    
                    <a href="../../topic/accessibility/all/index.php">Accessibility</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/agile/all/index.php">Agile</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/analytics-and-metrics/all/index.php">Analytics &amp; Metrics</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/applications/all/index.php">Application Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/b2b-websites/all/index.php">B2B Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/behavior-patterns/all/index.php">Behavior Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/branding/all/index.php">Branding</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/content-strategy/all/index.php">Content Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/corporate-websites/all/index.php">Corporate Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/design-patterns/all/index.php">Design Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-design-process/all/index.php">Design Process</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/email/all/index.php">Email</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/eyetracking/all/index.php">Eyetracking</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/heuristic-evaluation/all/index.php">Heuristic Evaluation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/human-computer-interaction/all/index.php">Human Computer Interaction</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-ideation/all/index.php">Ideation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/information-architecture/all/index.php">Information Architecture</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/interaction-design/all/index.php">Interaction Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/international-users/all/index.php">International Users</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/intranets/all/index.php">Intranets</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-management/all/index.php">Management</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/navigation/all/index.php">Navigation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/non-profit-websites/all/index.php">Non-Profit Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/personas/all/index.php">Personas</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/persuasive-design/all/index.php">Persuasive Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/prototyping/all/index.php">Prototyping</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/psychology-and-ux/all/index.php">Psychology and UX</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/research-methods/all/index.php">Research Methods</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/search/all/index.php">Search</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/social-media/all/index.php">Social Media</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/strategy/all/index.php">Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/user-testing/all/index.php">User Testing</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-teams/all/index.php">UX Teams</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/visual-design/all/index.php">Visual Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/writing-web/all/index.php">Writing for the Web</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/young-users/all/index.php">Young Users</a>
                    
                  </li>
                  
                </ul>
                
              </section>
            </div>
            <script>
              /* Determine which topics list will be displayed: the full or the
               * partial.  We are inlining this close to the list itself to
               * prevent rendering delays on the client
               */
              var topic_list = $.cookie("article-topics");

              var popular_topics = $('#article-popular-topics');
              var all_topics = $('#article-all-topics');

              if(topic_list == 1) {
                  popular_topics.hide();
                  all_topics.show();
              } else {
                  popular_topics.show();
                  all_topics.hide();
              }
            </script>
            

            
            <div class="small-6 medium-12 columns">
              
              <section id="article-initial-authors">
                  <h1>Author</h1>
                  <ul>
                    
                    <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                    
                    <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                    
                    <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                    
                    <li><a href="#" onClick="$('#article-initial-authors').hide(); $('#article-all-authors').show();"><strong>See all authors&hellip;</strong></a></li>
                  </ul>
              </section>
              <section id="article-all-authors" style="display: none;">
                <h1>
                  All Authors
                  (<a href="#" id="article-hide-all-authors">hide</a>)
                </h1>
                <ul>
                  
                  <li><a href="../author/raluca-budiu/index.php">Raluca Budiu</a></li>
                  
                  <li><a href="../author/patty-caya/index.php">Patty Caya</a></li>
                  
                  <li><a href="../author/susan-farrell/index.php">Susan Farrell</a></li>
                  
                  <li><a href="../author/therese-fessenden/index.php">Therese Fessenden</a></li>
                  
                  <li><a href="../author/kim-flaherty/index.php">Kim Flaherty</a></li>
                  
                  <li><a href="../author/sarah-gibbons/index.php">Sarah Gibbons</a></li>
                  
                  <li><a href="../author/aurora-harley/index.php">Aurora Harley</a></li>
                  
                  <li><a href="../author/kate-kaplan/index.php">Kate Kaplan</a></li>
                  
                  <li><a href="../author/page-laubheimer/index.php">Page Laubheimer</a></li>
                  
                  <li><a href="../author/angela-li/index.php">Angie Li</a></li>
                  
                  <li><a href="../author/hoa-loranger/index.php">Hoa Loranger</a></li>
                  
                  <li><a href="../author/kate-meyer/index.php">Kate Meyer</a></li>
                  
                  <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                  
                  <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                  
                  <li><a href="../author/kara-pernice/index.php">Kara Pernice</a></li>
                  
                  <li><a href="../author/christian-rohrer/index.php">Christian Rohrer</a></li>
                  
                  <li><a href="../author/amy-schade/index.php">Amy Schade</a></li>
                  
                  <li><a href="../author/katie-sherwin/index.php">Katie Sherwin</a></li>
                  
                  <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                  
                  <li><a href="../author/kathryn-whitenton/index.php">Kathryn Whitenton</a></li>
                  
                </ul>
              </section>
              
            </div>
            
          </div>

          
          <section class="hide-for-small-down">
            
              
              <h1>Recent Articles</h1>
              
              <ul>
                  
                  <li><a href="../seamless-cross-channel/index.php">Seamlessness in the Omnichannel User Experience</a></li>
                  
                  <li><a href="../brand-intention-interpretation/index.php">Experience Design: Bridging Brand Intention and Brand Interpretation</a></li>
                  
                  <li><a href="../eyetracking-tasks-efficient-scanning/index.php">Scanning Patterns on the Web Are Optimized for the Current Task</a></li>
                  
                  <li><a href="../flat-design-best-practices/index.php">Flat-Design Best Practices</a></li>
                  
                  <li><a href="../ux-user-stories/index.php">Accounting for UX Work with User Stories in Agile Projects</a></li>
                  
                  <li><a href="../index.php"><strong>See all articles&hellip;</strong></a></li>
              </ul>
              
            
          </section>
          

          
          
          <section class="hide-for-small-down">
            <h1>Popular Articles</h1>
            
            <ul>
                
                <li><a href="../usability-101-introduction-to-usability/index.php">Usability 101: Introduction to Usability</a></li>
                
                <li><a href="../top-10-mistakes-web-design/index.php">Top 10 Mistakes in Web Design</a></li>
                
                <li><a href="../how-users-read-on-the-web/index.php">How Users Read on the Web</a></li>
                
                <li><a href="../f-shaped-pattern-reading-web-content/index.php">F-Shaped Pattern For Reading Web Content</a></li>
                
                <li><a href="../ten-usability-heuristics/index.php">10 Usability Heuristics for User Interface Design</a></li>
                
                <li><a href="../intranet-design-2016/index.php">10 Best Intranets of 2016</a></li>
                
                <li><a href="../which-ux-research-methods/index.php">When to Use Which User-Experience Research Methods</a></li>
                
                <li><a href="../response-times-3-important-limits/index.php">Response Times: The 3 Important Limits</a></li>
                
                <li><a href="../why-you-only-need-to-test-with-5-users/index.php">Why You Only Need to Test with 5 Users</a></li>
                
                <li><a href="../page-fold-manifesto/index.php">The Fold Manifesto: Why the Page Fold Still Matters</a></li>
                
            </ul>
          </section>
          
          

          
            <section class="hide-for-small-down">
              
            </section>
          

      </section>
      

      
        <div class="small-12 medium-9 columns content-body">
          




<article class="l-subsection alertbox-article-content article-detail-bl">
  
  <header>
    <div class="l-subsection">
      <h1>Statistics for Traffic Referred by Search Engines and Navigation Directories to Useit</h1>
      <div class="author-meta">by
        
          
            
            <a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a>
            
          
        on  April 11, 2004
        
      </div>

      

      
      <div class="topic-links">
        Topics:
        
          <ul>

</ul>
        
      </div>
      
    </div>
  </header>
  

  
  
  

  
  <section id="article-body"><p>
 <em>
  by
  <a href="../../people/jakob-nielsen/index.php" title="Author biography">
   Jakob Nielsen
  </a>
 </em>
</p>
<p>
 The following table shows the number of visits that have been recorded in the Useit server logs as coming from search engines and directory services (so-called &quot;portals&quot;) in a one-month period (March) in each of the years from 1998 through 2004. The true number of visits is slightly larger since some browsers don&#39;t report accurate referrer URLs. Also note that the number of users is likely to be smaller than the number of visits since people sometimes use multiple search engines or repeat a search.
</p>
<p>
 Portals and search engines accounted for about half of the &quot;unique visitors&quot; to Useit. In comparison, the
 <a href="http://www.eastgate.com/HypertextNow/archives/Portal.php" title="Mark Bernstein: 'Beyond the Portal', Dec. 1999">
  HypertextNow site gets 30-40%
 </a>
 of its visitors from portals and search engines.
</p>
<p>
 Overall
 <strong>
  search-engine-driven traffic increased by 9%
 </strong>
 during the last year. This was much slower growth than the
 <strong>
  95%
 </strong>
 recorded from 2001 to 2002. The extreme growth in 2001 was likely due to the emergence of Google as the dominant search engine, since Google has a tendency to drive traffic to high-quality websites.
</p>
<p>
 The slower growth since 2002 is realistic for the more stable Web we are seeing now that Internet use is close to saturation in English-speaking countries. Even though Useit has an international outlook, it is written in English and thus attracts the majority of its traffic from the U.S., Canada, the U.K., and Australia. Other countries still have rapid growth in Internet use, but people are unlikely to find an English-language site through search engines if they enter query terms in other languages.
</p>
<p>
 Traffic to a site from a search engine depends on three factors:
</p>
<ul>
 <li>
  the engine&#39;s
  <strong>
   raw traffic
  </strong>
  (so almost everybody gets a lot of visitors from Google and Yahoo)
 </li>
 <li>
  the
  <strong>
   interests of people
  </strong>
  using a certain kind of engine (so Useit got uncommonly many visitors from Google in its early years since early adopters tend to be the kind of advanced users who care about my topic)
 </li>
 <li>
  whether the engine emphasizes
  <strong>
   quality or quantity
  </strong>
  (so, since Useit aims at high quality content, it will get more visitors from Google, Ask Jeeves, and Mining Co. because of their emphasis on guiding users to the best sites - in contrast Useit gets relatively little traffic from weblog-dominated search engines like DayPop because Useit only posts a new article every two weeks).
 </li>
</ul>
<p>
 The most interesting observations from the table are:
</p>
<ul>
 <li>
  <strong>
   Google continues to dominate Internet search
  </strong>
  . It accounted for 76% of the referrals in 2004 and 72% in 2003. In a study of
  <a href="../../reports/pr-websites/index.php" title="Report: Designing Websites to Maximize Press Relations:Guidelines from Usability Studies with Journalists">
   how journalists use the Web
  </a>
  in early 2003, we found that half of the journalists turned to Google as their first action when asked to perform research for a story assignment. Still, Google is not the only game in town. Yahoo and MSN are important as well.
 </li>
 <li>
  Looking back, Google went from number six in 1999 to a clear winner in 2001 and a virtual &quot;takes-all&quot; position in 2002, showing that the
  <a href="../internet-stock-valuation/index.php" title="Alertbox Jan. 1999: Internet stock valuation and future user characteristics">
   Web is nowhere close to being locked down
  </a>
  : there is still plenty of
  <strong>
   opportunity for new sites
  </strong>
  . Google didn&#39;t even exist the first year I collected these statistics.
 </li>
 <li>
  Still, things seem to have
  <strong>
   quieted down
  </strong>
  somewhat lately: the top-10 list was the same in 2004 as in 2003, and the only change in the rank ordering of the top-10 search engines was that Netscape and About swapped places (#8 and #9). A few
  <strong>
   new search engines
  </strong>
  appeared and bear watching even though they haven&#39;t yet cracked the top-10: myway.com, MyWebSearch, and WebSearch.com.
 </li>
 <li>
  <strong>
   Microsoft Network
  </strong>
  is getting to be big as a source of visitors. This is probably partly due to an increased customer base, but it must also indicate that MSN is better than AOL at letting users out on the open Internet and helping them find higher-quality content than simply the service&#39;s own walled garden. AOL is much bigger than MSN and yet lets a much smaller part of its users find Useit.
 </li>
 <li>
  There are only about
  <strong>
   7 search engines worth paying attention to
  </strong>
  : Google, Yahoo, MSN, Ask Jeeves, Lycos, AltaVista, and AOL. Together they account for 97% of traffic (same as in 2003).
 </li>
 <li>
  Services can
  <strong>
   stagnate
  </strong>
  as was the case for
  <strong>
   AltaVista
  </strong>
  ,
  <strong>
   Lycos
  </strong>
  , and
  <strong>
   About
  </strong>
  (originally known as The Mining Company).
 </li>
 <li>
  <strong>
   Excite
  </strong>
  virtually died in late 2001. In fact, since Excite@Home closed shortly after suffering this big drop in traffic, it&#39;s remarkable that there are
  <em>
   any
  </em>
  visitors from Excite.
 </li>
 <li>
  <strong>
   Disney&#39;s Go (formerly Infoseek)
  </strong>
  went out of business in 2001; in fact, it is surprising to find
  <em>
   any
  </em>
  referrals from a dead search engine. More interesting, I predicted Go&#39;s troubles based on my analysis of the search referrals in March 2000.
 </li>
</ul>
<p>
 The type of statistics shown in this table may be seen as a
 <strong>
  leading indicator for the future of portal companies
 </strong>
 : their ability to guide users to high-quality content is likely to influence their future use since people will tend to stay with services that give them good results and will tend to recommend the good services to their friends. Services that do not succeed in guiding users to good pages may suffer a drop in usage over the long term since people will change service when they discover a better one.
</p>
<p>
 &nbsp;
</p>
<p>
 <em>
  Number of visits referred by navigation services during one-month periods.
 </em>
</p>
<table border="0">
 <caption align="bottom">
  &nbsp;
 </caption>
 <tbody>
  <tr>
   <td>
    &nbsp;
   </td>
   <th align="right" width="70">
    March
    <br />
    1998
   </th>
   <th align="right" width="70">
    March
    <br />
    1999
   </th>
   <th align="right" width="70">
    March
    <br />
    2000
   </th>
   <th align="right" width="70">
    March
    <br />
    2001
   </th>
   <th align="right" width="70">
    March
    <br />
    2002
   </th>
   <th align="right" width="70">
    March
    <br />
    2003
   </th>
   <th align="right" width="70">
    March
    <br />
    2004
   </th>
   <th align="right" width="70">
    Growth
    <br />
    2003
    <br />
    -2004
   </th>
  </tr>
  <tr>
   <td height="6">
    &nbsp;
   </td>
  </tr>
  <tr>
   <td>
    Google
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    455
   </td>
   <td align="right">
    4,582
   </td>
   <td align="right">
    23,778
   </td>
   <td align="right">
    67,784
   </td>
   <td align="right">
    85,065
   </td>
   <td align="right">
    98,604
   </td>
   <td align="right">
    16%
   </td>
  </tr>
  <tr>
   <td>
    Yahoo
   </td>
   <td align="right">
    2,482
   </td>
   <td align="right">
    3,698
   </td>
   <td align="right">
    5,402
   </td>
   <td align="right">
    10,895
   </td>
   <td align="right">
    15,676
   </td>
   <td align="right">
    15,718
   </td>
   <td align="right">
    14,735
   </td>
   <td align="right">
    -6%
   </td>
  </tr>
  <tr>
   <td>
    Search.MSN
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    68
   </td>
   <td align="right">
    1,636
   </td>
   <td align="right">
    3,534
   </td>
   <td align="right">
    5,445
   </td>
   <td align="right">
    7,240
   </td>
   <td align="right">
    7,512
   </td>
   <td align="right">
    4%
   </td>
  </tr>
  <tr>
   <td>
    Ask Jeeves
   </td>
   <td align="right">
    12
   </td>
   <td align="right">
    85
   </td>
   <td align="right">
    1,127
   </td>
   <td align="right">
    1,422
   </td>
   <td align="right">
    1,633
   </td>
   <td align="right">
    3,062
   </td>
   <td align="right">
    1,800
   </td>
   <td align="right">
    -41%
   </td>
  </tr>
  <tr>
   <td>
    Lycos
   </td>
   <td align="right">
    211
   </td>
   <td align="right">
    163
   </td>
   <td align="right">
    2,586
   </td>
   <td align="right">
    2,266
   </td>
   <td align="right">
    3,670
   </td>
   <td align="right">
    1,980
   </td>
   <td align="right">
    1,396
   </td>
   <td align="right">
    -30%
   </td>
  </tr>
  <tr>
   <td height="6">
    &nbsp;
   </td>
  </tr>
  <tr>
   <td>
    AltaVista
   </td>
   <td align="right">
    771
   </td>
   <td align="right">
    3,033
   </td>
   <td align="right">
    5,015
   </td>
   <td align="right">
    3,188
   </td>
   <td align="right">
    3,157
   </td>
   <td align="right">
    1,310
   </td>
   <td align="right">
    1,210
   </td>
   <td align="right">
    -8%
   </td>
  </tr>
  <tr>
   <td>
    AOLsearch
   </td>
   <td align="right">
    51
   </td>
   <td align="right">
    72
   </td>
   <td align="right">
    514
   </td>
   <td align="right">
    220
   </td>
   <td align="right">
    916
   </td>
   <td align="right">
    1,096
   </td>
   <td align="right">
    901
   </td>
   <td align="right">
    -18%
   </td>
  </tr>
  <tr>
   <td>
    Netscape Directory
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    28
   </td>
   <td align="right">
    541
   </td>
   <td align="right">
    525
   </td>
   <td align="right">
    219
   </td>
   <td align="right">
    586
   </td>
   <td align="right">
    633
   </td>
   <td align="right">
    8%
   </td>
  </tr>
  <tr>
   <td>
    About/Mining Co.
   </td>
   <td align="right">
    73
   </td>
   <td align="right">
    312
   </td>
   <td align="right">
    1,184
   </td>
   <td align="right">
    1,062
   </td>
   <td align="right">
    777
   </td>
   <td align="right">
    636
   </td>
   <td align="right">
    578
   </td>
   <td align="right">
    -9%
   </td>
  </tr>
  <tr>
   <td>
    WebFetch/Dogpile
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    485
   </td>
   <td align="right">
    423
   </td>
   <td align="right">
    358
   </td>
   <td align="right">
    437
   </td>
   <td align="right">
    435
   </td>
   <td align="right">
    0%
   </td>
  </tr>
  <tr>
   <td height="6">
    &nbsp;
   </td>
  </tr>
  <tr>
   <td>
    Search.myway.com
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    39
   </td>
   <td align="right">
    533
   </td>
   <td align="right">
    657%
   </td>
  </tr>
  <tr>
   <td>
    MyWebSearch
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    532
   </td>
   <td align="right">
    -
   </td>
  </tr>
  <tr>
   <td>
    WebSearch
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    383
   </td>
   <td align="right">
    -
   </td>
  </tr>
  <tr>
   <td>
    AllTheWeb
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    191
   </td>
   <td align="right">
    381
   </td>
   <td align="right">
    253
   </td>
   <td align="right">
    250
   </td>
   <td align="right">
    169
   </td>
   <td align="right">
    -32%
   </td>
  </tr>
  <tr>
   <td>
    Mamma
   </td>
   <td align="right">
    4
   </td>
   <td align="right">
    32
   </td>
   <td align="right">
    90
   </td>
   <td align="right">
    97
   </td>
   <td align="right">
    95
   </td>
   <td align="right">
    88
   </td>
   <td align="right">
    167
   </td>
   <td align="right">
    90%
   </td>
  </tr>
  <tr>
   <td height="6">
    &nbsp;
   </td>
  </tr>
  <tr>
   <td>
    Go2Net
   </td>
   <td align="right">
    159
   </td>
   <td align="right">
    387
   </td>
   <td align="right">
    675
   </td>
   <td align="right">
    350
   </td>
   <td align="right">
    100
   </td>
   <td align="right">
    176
   </td>
   <td align="right">
    151
   </td>
   <td align="right">
    -10%
   </td>
  </tr>
  <tr>
   <td>
    Earthlink
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    10
   </td>
   <td align="right">
    247
   </td>
   <td align="right">
    138
   </td>
   <td align="right">
    144
   </td>
   <td align="right">
    5%
   </td>
  </tr>
  <tr>
   <td>
    Cometsystems
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    152
   </td>
   <td align="right">
    135
   </td>
   <td align="right">
    143
   </td>
   <td align="right">
    6%
   </td>
  </tr>
  <tr>
   <td>
    Excite
   </td>
   <td align="right">
    275
   </td>
   <td align="right">
    736
   </td>
   <td align="right">
    300
   </td>
   <td align="right">
    1,673
   </td>
   <td align="right">
    329
   </td>
   <td align="right">
    172
   </td>
   <td align="right">
    140
   </td>
   <td align="right">
    -19%
   </td>
  </tr>
  <tr>
   <td>
    MySearch
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    154
   </td>
   <td align="right">
    119
   </td>
   <td align="right">
    -25%
   </td>
  </tr>
  <tr>
   <td height="6">
    &nbsp;
   </td>
  </tr>
  <tr>
   <td>
    HotBot
   </td>
   <td align="right">
    149
   </td>
   <td align="right">
    118
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    209
   </td>
   <td align="right">
    119
   </td>
   <td align="right">
    -43%
   </td>
  </tr>
  <tr>
   <td>
    3721.com
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    117
   </td>
   <td align="right">
    -
   </td>
  </tr>
  <tr>
   <td>
    Search.com/Snap
   </td>
   <td align="right">
    12
   </td>
   <td align="right">
    653
   </td>
   <td align="right">
    536
   </td>
   <td align="right">
    212
   </td>
   <td align="right">
    256
   </td>
   <td align="right">
    33
   </td>
   <td align="right">
    116
   </td>
   <td align="right">
    252%
   </td>
  </tr>
  <tr>
   <td>
    Overture/GoTo
   </td>
   <td align="right">
    25
   </td>
   <td align="right">
    85
   </td>
   <td align="right">
    136
   </td>
   <td align="right">
    634
   </td>
   <td align="right">
    361
   </td>
   <td align="right">
    211
   </td>
   <td align="right">
    106
   </td>
   <td align="right">
    -50%
   </td>
  </tr>
  <tr>
   <td>
    iWon
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    57
   </td>
   <td align="right">
    123
   </td>
   <td align="right">
    86
   </td>
   <td align="right">
    122
   </td>
   <td align="right">
    90
   </td>
   <td align="right">
    -26
   </td>
  </tr>
  <tr>
   <td height="6">
    &nbsp;
   </td>
  </tr>
  <tr>
   <td>
    Teoma
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    16
   </td>
   <td align="right">
    80
   </td>
   <td align="right">
    59
   </td>
   <td align="right">
    -27%
   </td>
  </tr>
  <tr>
   <td>
    Naver
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    9
   </td>
   <td align="right">
    143
   </td>
   <td align="right">
    59
   </td>
   <td align="right">
    -27%
   </td>
  </tr>
  <tr>
   <td>
    LookSmart
   </td>
   <td align="right">
    194
   </td>
   <td align="right">
    388
   </td>
   <td align="right">
    393
   </td>
   <td align="right">
    236
   </td>
   <td align="right">
    81
   </td>
   <td align="right">
    18
   </td>
   <td align="right">
    35
   </td>
   <td align="right">
    94%
   </td>
  </tr>
  <tr>
   <td>
    BBC search
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    22
   </td>
   <td align="right">
    56
   </td>
   <td align="right">
    30
   </td>
   <td align="right">
    -47%
   </td>
  </tr>
  <tr>
   <td>
    Kvasir
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    18
   </td>
   <td align="right">
    7
   </td>
   <td align="right">
    11
   </td>
   <td align="right">
    26
   </td>
   <td align="right">
    12
   </td>
   <td align="right">
    18
   </td>
   <td align="right">
    52%
   </td>
  </tr>
  <tr>
   <td height="6">
    &nbsp;
   </td>
  </tr>
  <tr>
   <td>
    Sympatico.ca
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    28
   </td>
   <td align="right">
    132
   </td>
   <td align="right">
    93
   </td>
   <td align="right">
    16
   </td>
   <td align="right">
    -83%
   </td>
  </tr>
  <tr>
   <td>
    SirSearch
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    16
   </td>
   <td align="right">
    -
   </td>
  </tr>
  <tr>
   <td>
    DirectHit
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    140
   </td>
   <td align="right">
    238
   </td>
   <td align="right">
    92
   </td>
   <td align="right">
    230
   </td>
   <td align="right">
    8
   </td>
   <td align="right">
    -96%
   </td>
  </tr>
  <tr>
   <td>
    DayPop
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    270
   </td>
   <td align="right">
    77
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    &nbsp;
   </td>
  </tr>
  <tr>
   <td>
    Evreka
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    65
   </td>
   <td align="right">
    27
   </td>
   <td align="right">
    65
   </td>
   <td align="right">
    57
   </td>
   <td align="right">
    29
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    &nbsp;
   </td>
  </tr>
  <tr>
   <td height="6">
    &nbsp;
   </td>
  </tr>
  <tr>
   <td>
    Findexe
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    275
   </td>
   <td align="right">
    23
   </td>
   <td align="right">
    24
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    &nbsp;
   </td>
  </tr>
  <tr>
   <td>
    WebCrawler
   </td>
   <td align="right">
    285
   </td>
   <td align="right">
    81
   </td>
   <td align="right">
    356
   </td>
   <td align="right">
    43
   </td>
   <td align="right">
    85
   </td>
   <td align="right">
    6
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    &nbsp;
   </td>
  </tr>
  <tr>
   <td>
    Go/Infoseek
   </td>
   <td align="right">
    1,304
   </td>
   <td align="right">
    2,266
   </td>
   <td align="right">
    1,769
   </td>
   <td align="right">
    253
   </td>
   <td align="right">
    5
   </td>
   <td align="right">
    5
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    &nbsp;
   </td>
  </tr>
  <tr>
   <td>
    EuroSeek
   </td>
   <td align="right">
    61
   </td>
   <td align="right">
    24
   </td>
   <td align="right">
    8
   </td>
   <td align="right">
    6
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    2
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    &nbsp;
   </td>
  </tr>
  <tr>
   <td>
    RealNames
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    19
   </td>
   <td align="right">
    25
   </td>
   <td align="right">
    127
   </td>
   <td align="right">
    4
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    &nbsp;
   </td>
  </tr>
  <tr>
   <td>
    Searchopolis
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    296
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    &nbsp;
   </td>
  </tr>
  <tr>
   <td>
    Newssearch.Userland
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    9
   </td>
   <td align="right">
    24
   </td>
   <td align="right">
    5
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    &nbsp;
   </td>
  </tr>
  <tr>
   <td>
    Infind
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    75
   </td>
   <td align="right">
    4
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    &nbsp;
   </td>
  </tr>
  <tr>
   <td>
    Savvysearch
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    7
   </td>
   <td align="right">
    45
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    &nbsp;
   </td>
  </tr>
  <tr>
   <td>
    Underhound
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    93
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td>
    &nbsp;
   </td>
  </tr>
  <tr>
   <td>
    Metafind
   </td>
   <td align="right">
    3
   </td>
   <td align="right">
    12
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    &nbsp;
   </td>
  </tr>
  <tr>
   <td>
    DevSearch
   </td>
   <td align="right">
    144
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td>
    &nbsp;
   </td>
  </tr>
  <tr>
   <td>
    McKinley
   </td>
   <td align="right">
    12
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td align="right">
    &nbsp;
   </td>
   <td>
    &nbsp;
   </td>
  </tr>
  <tr>
   <td height="6">
    &nbsp;
   </td>
  </tr>
  <tr style="background-color: #DDDDDD">
   <td>
    <strong>
     Total
    </strong>
   </td>
   <td align="right">
    <strong>
     6,227
    </strong>
   </td>
   <td align="right">
    <strong>
     12,926
    </strong>
   </td>
   <td align="right">
    <strong>
     27,678
    </strong>
   </td>
   <td align="right">
    <strong>
     52,380
    </strong>
   </td>
   <td align="right">
    <strong>
     102,336
    </strong>
   </td>
   <td align="right">
    <strong>
     119,628
    </strong>
   </td>
   <td align="right">
    <strong>
     130,410
    </strong>
   </td>
   <td align="right">
    <strong>
     9%
    </strong>
   </td>
  </tr>
 </tbody>
</table>
<p>
 The next table compares the referral statistics to Useit in 2004 with general referral statistics for a large sample of the Web at
 <a href="http://www.statmarket.com/SM?c=Search_Engines">
  StatMarket
 </a>
 during the same period.
</p>
<p>
 &nbsp;
</p>
<p>
 <em>
  Proportion of referrals from various navigation services to Useit and on the Web in general in early 2004.
 </em>
</p>
<table border="0">
 <caption align="bottom">
  &nbsp;
 </caption>
 <tbody>
  <tr>
   <td>
    &nbsp;
   </td>
   <th align="right" width="80">
    to
    <br />
    Useit
   </th>
   <th align="right" width="80">
    across
    <br />
    the Web
   </th>
  </tr>
  <tr>
   <td height="6">
    &nbsp;
   </td>
  </tr>
  <tr>
   <td>
    Google
   </td>
   <td align="right">
    75.6%
   </td>
   <td align="right">
    40.9%
   </td>
  </tr>
  <tr>
   <td>
    Yahoo
   </td>
   <td align="right">
    11.3%
   </td>
   <td align="right">
    27.4%
   </td>
  </tr>
  <tr>
   <td>
    MSN
   </td>
   <td align="right">
    5.8%
   </td>
   <td align="right">
    19.6%
   </td>
  </tr>
  <tr>
   <td>
    Others
   </td>
   <td align="right">
    7.3%
   </td>
   <td align="right">
    21.1%
   </td>
  </tr>
 </tbody>
</table>
<p>
 The last table compares the referral statistics to Useit in 1999 with general referral statistics for a large sample of the Web at
 <a href="http://www.statmarket.com/SM?c=Search_Engines">
  StatMarket
 </a>
 during the same period.
</p>
<p>
 &nbsp;
</p>
<p>
 <em>
  Proportion of referrals from various navigation services to Useit and on the Web in general in early 1999. A * indicates missing data.
 </em>
</p>
<table border="0">
 <caption align="bottom">
  &nbsp;
 </caption>
 <tbody>
  <tr>
   <td>
    &nbsp;
   </td>
   <th align="right" width="80">
    to
    <br />
    Useit
   </th>
   <th align="right" width="80">
    across
    <br />
    the Web
   </th>
  </tr>
  <tr>
   <td height="6">
    &nbsp;
   </td>
  </tr>
  <tr>
   <td>
    Yahoo
   </td>
   <td align="right">
    28.7%
   </td>
   <td align="right">
    43.4%
   </td>
  </tr>
  <tr>
   <td>
    AltaVista
   </td>
   <td align="right">
    23.5%
   </td>
   <td align="right">
    10.5%
   </td>
  </tr>
  <tr>
   <td>
    Go/Infoseek
   </td>
   <td align="right">
    17.6%
   </td>
   <td align="right">
    4.5%
   </td>
  </tr>
  <tr>
   <td>
    Excite
   </td>
   <td align="right">
    5.7%
   </td>
   <td align="right">
    22.3%
   </td>
  </tr>
  <tr>
   <td>
    Snap
   </td>
   <td align="right">
    5.1%
   </td>
   <td align="right">
    2.7%
   </td>
  </tr>
  <tr>
   <td height="6">
    &nbsp;
   </td>
  </tr>
  <tr>
   <td>
    Google
   </td>
   <td align="right">
    3.5%
   </td>
   <td align="right">
    *
   </td>
  </tr>
  <tr>
   <td>
    LookSmart
   </td>
   <td align="right">
    3.0%
   </td>
   <td align="right">
    0.7%
   </td>
  </tr>
  <tr>
   <td>
    Go2Net
   </td>
   <td align="right">
    3.0%
   </td>
   <td align="right">
    *
   </td>
  </tr>
  <tr>
   <td>
    Mining Co.
   </td>
   <td align="right">
    2.4%
   </td>
   <td align="right">
    *
   </td>
  </tr>
  <tr>
   <td>
    Lycos
   </td>
   <td align="right">
    1.3%
   </td>
   <td align="right">
    1.8%
   </td>
  </tr>
  <tr>
   <td>
    HotBot
   </td>
   <td align="right">
    0.9%
   </td>
   <td align="right">
    3.0%
   </td>
  </tr>
  <tr>
   <td>
    Underhound
   </td>
   <td align="right">
    0.7%
   </td>
   <td align="right">
    *
   </td>
  </tr>
  <tr>
   <td>
    GoTo
   </td>
   <td align="right">
    0.7%
   </td>
   <td align="right">
    2.0%
   </td>
  </tr>
  <tr>
   <td>
    AskJeeves
   </td>
   <td align="right">
    0.7%
   </td>
   <td align="right">
    *
   </td>
  </tr>
  <tr>
   <td>
    WebCrawler
   </td>
   <td align="right">
    0.6%
   </td>
   <td align="right">
    3.6%
   </td>
  </tr>
  <tr>
   <td height="6">
    &nbsp;
   </td>
  </tr>
  <tr>
   <td>
    Netfind.AOL
   </td>
   <td align="right">
    0.6%
   </td>
   <td align="right">
    0.2%
   </td>
  </tr>
  <tr>
   <td>
    Search.MSN
   </td>
   <td align="right">
    0.5%
   </td>
   <td align="right">
    3.0%
   </td>
  </tr>
  <tr>
   <td>
    Evreka
   </td>
   <td align="right">
    0.5%
   </td>
   <td align="right">
    *
   </td>
  </tr>
  <tr>
   <td>
    Mamma
   </td>
   <td align="right">
    0.3%
   </td>
   <td align="right">
    0.5%
   </td>
  </tr>
  <tr>
   <td>
    Netscape Directory
   </td>
   <td align="right">
    0.2%
   </td>
   <td align="right">
    *
   </td>
  </tr>
  <tr>
   <td height="6">
    &nbsp;
   </td>
  </tr>
  <tr>
   <td>
    EuroSeek
   </td>
   <td align="right">
    0.2%
   </td>
   <td align="right">
    0.1%
   </td>
  </tr>
  <tr>
   <td>
    RealNames
   </td>
   <td align="right">
    0.2%
   </td>
   <td align="right">
    *
   </td>
  </tr>
  <tr>
   <td>
    Kvasir
   </td>
   <td align="right">
    0.1%
   </td>
   <td align="right">
    *
   </td>
  </tr>
  <tr>
   <td>
    Metafind
   </td>
   <td align="right">
    0.1%
   </td>
   <td align="right">
    *
   </td>
  </tr>
  <tr>
   <td>
    Newssearch.UserLand
   </td>
   <td align="right">
    0.1%
   </td>
   <td align="right">
    *
   </td>
  </tr>
  <tr>
   <td height="6">
    &nbsp;
   </td>
  </tr>
  <tr>
   <td>
    Savvysearch
   </td>
   <td align="right">
    0.1%
   </td>
   <td align="right">
    *
   </td>
  </tr>
 </tbody>
</table>
<p>
 &nbsp;
</p>
</section>
  

  
  <section>
    
        
    
  </section>
  
  
  
  
  

  


  <section>
    <br />
    <p class="share"><b>Share <span class="hide-for-small-down">this article</span>:</b> <a href="https://twitter.com/intent/tweet?url=http://www.nngroup.com/articles/search-traffic-referrals/&amp;text=Statistics%20for%20Traffic%20Referred%20by%20Search%20Engines%20and%20Navigation%20Directories%20to%20Useit&amp;via=nngroup">Twitter</a> | <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=http://www.nngroup.com/articles/search-traffic-referrals/&amp;title=Statistics%20for%20Traffic%20Referred%20by%20Search%20Engines%20and%20Navigation%20Directories%20to%20Useit&amp;source=Nielsen%20Norman%20Group">LinkedIn</a> | <a href="https://plus.google.com/share?url=http://www.nngroup.com/articles/search-traffic-referrals/">Google+</a> | <a href="mailto:?subject=NN/g Article: Statistics for Traffic Referred by Search Engines and Navigation Directories to Useit&amp;body=http://www.nngroup.com/articles/search-traffic-referrals/">Email</a></p>
    <hr />
  </section>

  <footer class="l-subsection">
    
    
    

    <div class="row show-for-small-down related-content">
      
      
      

      
      
      

      
      
      

      
      
      

      
      
      
    </div>

    <div class="hide-for-small-down row related-content">
      <div class="medium-6 columns">
        
        
        

        

      </div>
      <div class="medium-6 columns">
        
        
      </div>
    </div>
  </footer>
</article>

        </div>

      

    
  </div>
  <div class="row">
    
      <div class="column small-12 show-for-small-down l-navrail announcements">
        
          
        
      </div>
    
    
  </div>

    </div>
    




<footer id="page-footer">
    <div class="row hide-for-print">
        <div class="column small-12 medium-6">
            <section class="footer-popular">
                
                <h1>Popular Topics</h1>
                <div style="float: left;  width: 15em; display: block;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/applications/index.php">Application Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/e-commerce/index.php">E-Commerce Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/eyetracking/index.php">Eyetracking </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/information-architecture/index.php">Information Architecture </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/intranets/index.php">Intranet Design </a></li>
	<li><a href="../../topic/interaction-design/index.php">Interaction Design </a></li>
</ul>
</div>

<div style="float: left;  width: 15em; display: block; list-style: none;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/mobile-and-tablet-design/index.php">Mobile and Tablet Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/credibility-persuasion/index.php">Persuasive Design</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/research-methods/index.php">Research Methods</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/user-testing/index.php">User Testing </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/web-usability/index.php">Web Usability </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/writing-web/index.php">Web Writing &amp; Content Strategy </a></li>
</ul>
</div>

<p>&nbsp;</p>

                
            </section>
        </div>

        <div class="column small-12 medium-6">
            <div class="row">
                <div class="column small-6">
                    <section class="footer-about">
                        <h1>Follow Us</h1>
                        <ul>
	<li><a href="../subscribe/index.php">Subscribe to our weekly UX newsletter</a>,&nbsp;Jakob Nielsen&#39;s Alertbox</li>
	<li>Visit Don Norman&#39;s website, <a href="http://www.jnd.org/"> www.jnd.org </a></li>
	<li>Visit Bruce Tognazzini&#39;s website, <a href="http://www.asktog.com/"> www.asktog.com </a></li>
	<li style="padding-top: 6px;"><a class="twitter-follow-button" data-show-count="false" data-size="large" href="https://twitter.com/nngroup"><img alt="Follow @nngroup on Twitter" height="20" src="https://media.nngroup.com/media/editor/2016/12/30/twitter-follow-button.png" style="width: 117px; height: 20px;" width="117" /></a></li>
	<li style="padding-top: 6px;"><img alt="" height="14" src="https://media.nngroup.com/media/editor/2016/07/26/feed-icon-14x14.png" style="vertical-align:text-top;width:14px;margin-right:.25em;" width="14" /><a href="../../feed/rss/index.php">Subscribe to our RSS feed</a></li>
</ul>

                    </section>
                </div>

                <div class="column small-6">
                    <section class="footer-about right-column">
                        <h1>About
</h1>
                        <ul class="no-bullet">
	<li><a href="../../about/contact/index.php">Contact information</a></li>
	<li><a href="../../about/index.php">About Us</a></li>
	<li><a href="../../about/why-nng/index.php">Why NN/g? </a></li>
	<li><a href="../../return-policy/index.php">Return Policy</a></li>
</ul>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="copyright column small-12">
            <p><a href="../../copyright-and-reprint-info/index.php">Copyright</a> &copy; 1998-2017 Nielsen Norman Group, All Rights Reserved.</p>
        </div>
    </div>
</footer>


    


        <script src="https://media.nngroup.com/static/vendor/fastclick/js/fastclick.min.js"></script>
        <script src="https://media.nngroup.com/static/vendor/foundation/js/foundation.min.js"></script>
        <script src="http://media.nngroup.com/static/js/app.js"></script>
        <script src="http://media.nngroup.com/static/js/scroll-to-fixed.js"></script><script>var externalLinks = document.querySelectorAll('a[href^="http"]');for (var i = externalLinks.length-1; i >= 0; i--) {externalLinks[i].addEventListener("click", function(event) { alert("External links are disabled for this test."); event.preventDefault(); }, false);}var mailLinks = document.querySelectorAll('a[href^="mailto"]');for (var i = mailLinks.length-1; i >= 0; i--) {mailLinks[i].addEventListener("click", function(event) { alert("Email links are disabled for this test."); event.preventDefault(); }, false);}var externalForms = document.querySelectorAll('form');for (var i = externalForms.length-1; i >= 0; i--) {externalForms[i].addEventListener("submit", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false); externalForms[i].addEventListener("click", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false);}</script>

        <script>
            
            $(function() {
                $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
                $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
            });
        </script>

        
<script src="https://media.nngroup.com/static/js/articles.js"></script>


        
        <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1072699960;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072699960/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
    </body>

<!-- Mirrored from www.nngroup.com/articles/search-traffic-referrals/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:16:55 GMT -->
</html>
