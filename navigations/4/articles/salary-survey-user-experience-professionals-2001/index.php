<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->





    
<!-- Mirrored from www.nngroup.com/articles/salary-survey-user-experience-professionals-2001/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:15:51 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1026.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"licenseKey":"1a30ef9931","queueTime":4,"beacon":"bam.nr-data.net","errorBeacon":"bam.nr-data.net","applicationID":"2048992","transactionName":"NFxSMRBUX0EDUkIPDA0WdhAMVkVbDV8ZBxEXUFMJB0YfRAtUQRVZIktEDAFZVHYHRVcPD01eVRE=","applicationTime":653,"agent":""}</script>
        <title>Salary Survey: User Experience Professionals 2001</title><meta property="og:title" content="Salary Survey: User Experience Professionals 2001" />
  
        
        <meta name="description" content="Salary data from 1,078 user experience professionals in United States, Europe, Asia, and Australia. A usability specialist in California with five years&#39; experience averaged $90,118 in 2001.">
        <meta property="og:description" content="Salary data from 1,078 user experience professionals in United States, Europe, Asia, and Australia. A usability specialist in California with five years&#39; experience averaged $90,118 in 2001." />
        
  
        
	
        
        <meta name="keywords" content="annual salary, compensation, salaries, pay, Europe, London, U.K., United Kingdom, Britain, Germany, Sweden, Denmark, Finland, United States, USA, US, Canada, New York, California, Massachusetts, Washington, Texas, New Jersey, Pennsylvania, NY, NJ, DC, CA, MA, TX, PA, Asia, Japan, Hong Kong, Australia, usability professionals, Web designers, online writers, writing">
        

        

        <meta name="robots" content="noarchive">
        <meta name="viewport" content="width=device-width">

        <link rel="search" type="application/opensearchdescription+xml" title="NN/g Search" href="../../opensearch.xml" />
        <link rel="shortcut icon" href="http://media.nngroup.com/static/img/favicon.ico" />

        <link rel="stylesheet" href="https://media.nngroup.com/static/vendor/font-awesome/css/font-awesome.min.css">

        
        <link rel="stylesheet" href="https://media.nngroup.com/static/css/7232a960f1df.css" type="text/css" />
        <link rel="stylesheet" href="http://fast.fonts.com/t/1.css?apiType=css&amp;projectid=c5846260-9def-46ec-9407-0f1349bd2d64" type="text/css">

        <script src="https://media.nngroup.com/static/vendor/modernizr/js/modernizr.min.js"></script>

        <script type="text/javascript" src="https://media.nngroup.com/static/js/3283be5def74.js"></script>
        

        

        
        
        <!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=229034,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('http://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->


<!-- Google Analytics Tracking Code for http://nngroup.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23132681-1', 'nngroup.com');
  ga('require', 'linkid', 'https://www.nngroup.com/articles/salary-survey-user-experience-professionals-2001/linkid.js');
  ga('require', 'displayfeatures');

// Optimizely Universal Analytics Integration
 window.optimizely = window.optimizely || [];
 window.optimizely.push("activateUniversalAnalytics");

  ga('send', 'pageview');

</script>
<script>
/**
* Function to track a click on an outbound link in Google Analytics.
*/
function trackOutboundLink(label, url) {
    if(window.ga && ga.loaded) {
        ga('send', 'event', 'outbound', 'click', label, {'hitCallback': function(){document.location = url;}
        });
    } else {
        document.location = url;
    }
}
/**
* Function to track a click on a link in the article announcement area.
*/
$('#articleAnnouncement a').on('click', function(){
  ga('send', 'event', 'link', 'click', 'articleAnnouncement');
});
</script>

<script type="text/javascript">
 setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2421.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



<!-- Hotjar Tracking Code for http://nngroup.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:121605,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

        <script src="https://media.nngroup.com/static/js/analytics/track_outbound_link.js"></script>
        
        
        
    </head>
    <body class="location-articles articles-detail">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
    <header id="page-header">
        <div id="cart" class="shopping-cart mobile-cart show-for-small-down"></div>
        <div class="row">
            <div id="masthead" class="masthead small-12 medium-7 column">

                <!-- The button to toggles menu on small screen -->

                <h1><a href="../../index.php"><img src="http://media.nngroup.com/static/img/logo-wordmark.png" width="74" height="30"> Nielsen <span class="norman">Norman</span> Group</a></h1>
                <p class="tagline show-for-medium-up">Evidence-Based User Experience Research, Training, and Consulting</p>

                <!-- The button to toggles form search on small screen -->


                <div style="clear: right;width:  100%;height: 1px;"></div><?php define('ROOT_PATH',$_SERVER["DOCUMENT_ROOT"] . '/apps/navigation_styles/navigations/4'); ?><?php include(ROOT_PATH)."/navigation.php"; ?>

                <!-- Drop down menu on small screen -->
                <div id="hideNav" class="show-for-small-down">
                  
                  <ul class="menu-content">
                    <li><a href="../../index.php">Home</a></li>
                    <li class="has-submenu">
                      <a>Training</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../training/index.php">Overview</a></li>
                        
<li id="nav-ux-conference"><a href="../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../online-seminars/index.php"><span>Online Seminars</span></a></li>

                      </ul>
                    </li>
                    <li class="has-submenu">
                      <a>Consulting</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
  <li><a href="../../consulting/index.php">Overview</a></li>
  
  <li><a href="../../consulting/benchmarking/index.php">Benchmarking</a></li>
  
  <li><a href="../../consulting/expert-design-review/index.php">Expert Design Review</a></li>
  
  <li><a href="../../consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
  
  <li><a href="../../consulting/usability-testing/index.php">Usability Testing </a></li>
  
  <li><a href="../../consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
  
  <li><a href="../../in-house-training/index.php">In-House Training</a></li>
</ul>

                    </li>

                    <li><a href="../../reports/index.php">Reports</a></li>
                    <li><a href="../index.php">Articles</a></li>
                    <li class="has-submenu"><a>About NN/G</a><i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../about/index.php">Overview</a></li>
                        <li><a href="../../people/index.php">People</a></li>
                        <li><a href="../../about/why-nng/index.php">Why NN/g?</a></li>
                        <li><a href="../../about/contact/index.php">Contact</a></li>
                        <li><a href="../../news/index.php">News</a></li>
                        <li><a href="../../about/history/index.php">History</a></li>
                        <li><a href="../../books/index.php">Books</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                </div>

                <hr class="header-divider show-for-small-only"/>


            </div>
       </div>

        <div id="hideNav">            <nav class="primary-nav small-12 column">
                <ul>
                    <li><a class="navitem" id="nav-home" href="../../index.php">Home</a></li>
                    <li><a class="navitem" id="nav-training" href="../../training/index.php">Training</a></li>
                    <li><a class="navitem" id="nav-consulting" href="../../consulting/index.php">Consulting</a></li>
                    <li><a class="navitem" id="nav-reports" href="../../reports/index.php">Reports</a></li>
                    <li><a class="navitem" id="nav-articles" href="../index.php">Articles</a></li>
                    <li><a class="navitem" id="nav-about" href="../../about/index.php">About NN/G</a></li>
                </ul>
            </nav>
            <nav class="secondary-nav small-12 medium-12 large-12 columns">
                
                
            </nav>
        </div>
    </header>
    <div class="l-content">
    
  

  

  <div class="row">
    
      
      <section class="small-12 medium-3 columns collapsable-content l-navrail articles-left-column">
        <h1 class="show-for-small-down collapse-link mobile-browse-header">Browse by Topic and Author</h1>
          <div class="row collapsable">

            
            <div class="small-6 medium-12 columns">
              <section id="article-popular-topics" style="display: none;">
                
                <h1>Topics</h1>
                <ul id="article-popular-topics">
                    
                    <li>
                        
                        <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/intranets/all/index.php">Intranets</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/user-testing/all/index.php">User Testing</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                        
                    </li>
                    
                    <li><a href="#" id="article-see-all-topics"><strong>See all topics&hellip;</strong></a></li>
                </ul>
                
              </section>
              <section id="article-all-topics" style="display: none;">
                
                <h1>
                  <span class="show-for-medium-up">All Article Topics</span>
                  <span class="show-for-small-down">All Topics</span>
                  (<a href="#" id="article-hide-all-topics">hide</a>)
                </h1>
                <ul>
                  
                  <li>
                    
                    <a href="../../topic/accessibility/all/index.php">Accessibility</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/agile/all/index.php">Agile</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/analytics-and-metrics/all/index.php">Analytics &amp; Metrics</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/applications/all/index.php">Application Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/b2b-websites/all/index.php">B2B Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/behavior-patterns/all/index.php">Behavior Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/branding/all/index.php">Branding</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/content-strategy/all/index.php">Content Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/corporate-websites/all/index.php">Corporate Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/design-patterns/all/index.php">Design Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-design-process/all/index.php">Design Process</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/email/all/index.php">Email</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/eyetracking/all/index.php">Eyetracking</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/heuristic-evaluation/all/index.php">Heuristic Evaluation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/human-computer-interaction/all/index.php">Human Computer Interaction</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-ideation/all/index.php">Ideation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/information-architecture/all/index.php">Information Architecture</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/interaction-design/all/index.php">Interaction Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/international-users/all/index.php">International Users</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/intranets/all/index.php">Intranets</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-management/all/index.php">Management</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/navigation/all/index.php">Navigation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/non-profit-websites/all/index.php">Non-Profit Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/personas/all/index.php">Personas</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/persuasive-design/all/index.php">Persuasive Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/prototyping/all/index.php">Prototyping</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/psychology-and-ux/all/index.php">Psychology and UX</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/research-methods/all/index.php">Research Methods</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/search/all/index.php">Search</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/social-media/all/index.php">Social Media</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/strategy/all/index.php">Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/user-testing/all/index.php">User Testing</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-teams/all/index.php">UX Teams</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/visual-design/all/index.php">Visual Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/writing-web/all/index.php">Writing for the Web</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/young-users/all/index.php">Young Users</a>
                    
                  </li>
                  
                </ul>
                
              </section>
            </div>
            <script>
              /* Determine which topics list will be displayed: the full or the
               * partial.  We are inlining this close to the list itself to
               * prevent rendering delays on the client
               */
              var topic_list = $.cookie("article-topics");

              var popular_topics = $('#article-popular-topics');
              var all_topics = $('#article-all-topics');

              if(topic_list == 1) {
                  popular_topics.hide();
                  all_topics.show();
              } else {
                  popular_topics.show();
                  all_topics.hide();
              }
            </script>
            

            
            <div class="small-6 medium-12 columns">
              
              <section id="article-initial-authors">
                  <h1>Author</h1>
                  <ul>
                    
                    <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                    
                    <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                    
                    <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                    
                    <li><a href="#" onClick="$('#article-initial-authors').hide(); $('#article-all-authors').show();"><strong>See all authors&hellip;</strong></a></li>
                  </ul>
              </section>
              <section id="article-all-authors" style="display: none;">
                <h1>
                  All Authors
                  (<a href="#" id="article-hide-all-authors">hide</a>)
                </h1>
                <ul>
                  
                  <li><a href="../author/raluca-budiu/index.php">Raluca Budiu</a></li>
                  
                  <li><a href="../author/patty-caya/index.php">Patty Caya</a></li>
                  
                  <li><a href="../author/susan-farrell/index.php">Susan Farrell</a></li>
                  
                  <li><a href="../author/therese-fessenden/index.php">Therese Fessenden</a></li>
                  
                  <li><a href="../author/kim-flaherty/index.php">Kim Flaherty</a></li>
                  
                  <li><a href="../author/sarah-gibbons/index.php">Sarah Gibbons</a></li>
                  
                  <li><a href="../author/aurora-harley/index.php">Aurora Harley</a></li>
                  
                  <li><a href="../author/kate-kaplan/index.php">Kate Kaplan</a></li>
                  
                  <li><a href="../author/page-laubheimer/index.php">Page Laubheimer</a></li>
                  
                  <li><a href="../author/angela-li/index.php">Angie Li</a></li>
                  
                  <li><a href="../author/hoa-loranger/index.php">Hoa Loranger</a></li>
                  
                  <li><a href="../author/kate-meyer/index.php">Kate Meyer</a></li>
                  
                  <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                  
                  <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                  
                  <li><a href="../author/kara-pernice/index.php">Kara Pernice</a></li>
                  
                  <li><a href="../author/christian-rohrer/index.php">Christian Rohrer</a></li>
                  
                  <li><a href="../author/amy-schade/index.php">Amy Schade</a></li>
                  
                  <li><a href="../author/katie-sherwin/index.php">Katie Sherwin</a></li>
                  
                  <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                  
                  <li><a href="../author/kathryn-whitenton/index.php">Kathryn Whitenton</a></li>
                  
                </ul>
              </section>
              
            </div>
            
          </div>

          
          <section class="hide-for-small-down">
            
              
              <h1>Recent Articles</h1>
              
              <ul>
                  
                  <li><a href="../seamless-cross-channel/index.php">Seamlessness in the Omnichannel User Experience</a></li>
                  
                  <li><a href="../brand-intention-interpretation/index.php">Experience Design: Bridging Brand Intention and Brand Interpretation</a></li>
                  
                  <li><a href="../eyetracking-tasks-efficient-scanning/index.php">Scanning Patterns on the Web Are Optimized for the Current Task</a></li>
                  
                  <li><a href="../flat-design-best-practices/index.php">Flat-Design Best Practices</a></li>
                  
                  <li><a href="../ux-user-stories/index.php">Accounting for UX Work with User Stories in Agile Projects</a></li>
                  
                  <li><a href="../index.php"><strong>See all articles&hellip;</strong></a></li>
              </ul>
              
            
          </section>
          

          
          
          <section class="hide-for-small-down">
            <h1>Popular Articles</h1>
            
            <ul>
                
                <li><a href="../usability-101-introduction-to-usability/index.php">Usability 101: Introduction to Usability</a></li>
                
                <li><a href="../top-10-mistakes-web-design/index.php">Top 10 Mistakes in Web Design</a></li>
                
                <li><a href="../how-users-read-on-the-web/index.php">How Users Read on the Web</a></li>
                
                <li><a href="../f-shaped-pattern-reading-web-content/index.php">F-Shaped Pattern For Reading Web Content</a></li>
                
                <li><a href="../ten-usability-heuristics/index.php">10 Usability Heuristics for User Interface Design</a></li>
                
                <li><a href="../intranet-design-2016/index.php">10 Best Intranets of 2016</a></li>
                
                <li><a href="../which-ux-research-methods/index.php">When to Use Which User-Experience Research Methods</a></li>
                
                <li><a href="../response-times-3-important-limits/index.php">Response Times: The 3 Important Limits</a></li>
                
                <li><a href="../why-you-only-need-to-test-with-5-users/index.php">Why You Only Need to Test with 5 Users</a></li>
                
                <li><a href="../page-fold-manifesto/index.php">The Fold Manifesto: Why the Page Fold Still Matters</a></li>
                
            </ul>
          </section>
          
          

          
            <section class="hide-for-small-down">
              
            </section>
          

      </section>
      

      
        <div class="small-12 medium-9 columns content-body">
          




<article class="l-subsection alertbox-article-content article-detail-bl">
  
  <header>
    <div class="l-subsection">
      <h1>Salary Survey: User Experience Professionals 2001</h1>
      <div class="author-meta">by
        
          
            
            <a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a>
            
          
        on  December 31, 2001
        
      </div>

      

      
      <div class="topic-links">
        Topics:
        
          <ul>

  <li><a href="../../topic/ux-management/index.php">Management</a></li>

  <li><a href="../../topic/strategy/index.php">Strategy</a></li>

</ul>
        
      </div>
      
    </div>
  </header>
  

  
  
  <section>
    <p class="lede"><strong>Summary:</strong> Usability is a well-paying profession these days: A usability specialist in California with five years&#39; experience had an estimated cash compensation of $90,118 a year in 2001, not counting stock options or other benefits. This number is at the high end of our detailed survey, which analyzes salary data from 1,078 professionals who attended the User Experience World Tour from November 2000 to April 2001. The survey respondents represent a response rate of 40% of the 2,682 conference attendees. Because we surveyed people at a high-end professional conference, the data probably reflects the salaries of good user experience professionals.</p>
    <br />
  </section>
  
  

  
  <section id="article-body"><p>
 See also
 <a href="../salary-trends-usability-professionals/index.php">
  2008 update of these salary statistics
 </a>
 .
</p>
<h2>
 Executive Summary
</h2>
<p>
 We asked respondents to state their total annual compensation from salary and bonuses; we did not include stock options and other benefits. Given that most stock options have been under water recently, cash compensation may be the most important number anyway.
</p>
<h3>
 Usability Specialists: Annual Compensation
</h3>
<p>
 Our multiple regression analysis of the salary data produced the following amounts as the best estimate of expected starting salaries for usability specialists in different parts of the world. The middle column is in U.S. dollars; the right column indicates the corresponding amount in local currencies, using the exchange rates prevalent when we collected the data in early 2001 as well as the exchange rates in July 2005.
</p>
<table border="0" cellpadding="2" cellspacing="0">
 <tbody>
  <tr>
  </tr>
 </tbody>
</table>
<table border="1" cellpadding="0" cellspacing="0" class="MsoNormalTable" style="border-collapse:collapse;border:none">
 <tbody>
  <tr>
   <td style="width:92.5pt;border-top:windowtext 1.0pt;
  border-left:red 2.25pt;border-bottom:maroon 2.25pt;border-right:windowtext 1.0pt;
  border-style:solid;background:#D9D9D9;padding:1.45pt 5.75pt 1.45pt 5.75pt" valign="top" width="136">
    <p class="TableHeading">
     Region
    </p>
   </td>
   <td style="width:119.25pt;border-top:solid windowtext 1.0pt;
  border-left:none;border-bottom:solid maroon 2.25pt;border-right:solid windowtext 1.0pt;
  background:#D9D9D9;padding:1.45pt 5.75pt 1.45pt 5.75pt" valign="top" width="176">
    <p class="TableHeading">
     Compensation
     <br />
     (U.S. Dollars, 2001 exchange rates)
    </p>
   </td>
   <td style="width:118.0pt;border-top:solid windowtext 1.0pt;
  border-left:none;border-bottom:solid maroon 2.25pt;border-right:solid windowtext 1.0pt;
  background:#D9D9D9;padding:1.45pt 5.75pt 1.45pt 5.75pt" valign="top" width="174">
    <p class="TableHeading">
     Compensation
     <br />
     (U.S. Dollars, 2005 exchange rates)
    </p>
   </td>
   <td style="width:113.75pt;border-top:solid windowtext 1.0pt;
  border-left:none;border-bottom:solid maroon 2.25pt;border-right:solid windowtext 1.0pt;
  background:#D9D9D9;padding:1.45pt 5.75pt 1.45pt 5.75pt" valign="top" width="167">
    <p class="TableHeading">
     Compensation
     <br />
     (Local Currency)
    </p>
   </td>
  </tr>
  <tr>
   <td style="width:92.5pt;border-top:none;border-left:
  solid red 2.25pt;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="136">
    <p class="TableHeading2">
     United States
    </p>
   </td>
   <td style="width:119.25pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="176">
    <p align="right" class="TableBody" style="margin-top:2.0pt;margin-right:51.5pt;
  margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;text-align:right">
     $67,118
    </p>
   </td>
   <td style="width:118.0pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="174">
    <p align="right" class="TableBody" style="margin-top:2.0pt;margin-right:42.5pt;
  margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;text-align:right">
     &nbsp;
    </p>
   </td>
   <td style="width:113.75pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="167">
    <p align="right" class="TableBody" style="margin-top:2.0pt;margin-right:42.5pt;
  margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;text-align:right">
     &nbsp;
    </p>
   </td>
  </tr>
  <tr>
   <td style="width:92.5pt;border-top:none;border-left:
  solid red 2.25pt;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="136">
    <p class="TableHeading2">
     Canada
    </p>
   </td>
   <td style="width:119.25pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="176">
    <p align="right" class="TableBody" style="margin-top:2.0pt;margin-right:51.5pt;
  margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;text-align:right">
     $49,866
    </p>
   </td>
   <td style="width:118.0pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="174">
    <p align="right" class="TableBody" style="margin-top:2.0pt;margin-right:42.5pt;
  margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;text-align:right">
     $60,466
    </p>
   </td>
   <td style="width:113.75pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="167">
    <p align="right" class="TableBody" style="margin-top:2.0pt;margin-right:42.5pt;
  margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;text-align:right">
     <span style="font-size:8.0pt">
      C
     </span>
     $74,796
    </p>
   </td>
  </tr>
  <tr>
   <td style="width:92.5pt;border-top:none;border-left:
  solid red 2.25pt;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="136">
    <p class="TableHeading2">
     Asia
    </p>
   </td>
   <td style="width:119.25pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="176">
    <p align="right" class="TableBody" style="margin-top:2.0pt;margin-right:51.5pt;
  margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;text-align:right">
     $49,297
    </p>
   </td>
   <td style="width:118.0pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="174">
    <p align="right" class="TableBody" style="margin-top:2.0pt;margin-right:42.5pt;
  margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;text-align:right">
     $50,811
    </p>
   </td>
   <td style="width:113.75pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="167">
    <p align="right" class="TableBody" style="margin-top:2.0pt;margin-right:42.5pt;
  margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;text-align:right">
     &yen;5,709,579
    </p>
   </td>
  </tr>
  <tr>
   <td style="width:92.5pt;border-top:none;border-left:
  solid red 2.25pt;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="136">
    <p class="TableHeading2">
     Europe
    </p>
   </td>
   <td style="width:119.25pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="176">
    <p align="right" class="TableBody" style="margin-top:2.0pt;margin-right:51.5pt;
  margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;text-align:right">
     $36,166
    </p>
   </td>
   <td style="width:118.0pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="174">
    <p align="right" class="TableBody" style="margin-top:2.0pt;margin-right:42.5pt;
  margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;text-align:right">
     $50,074
    </p>
   </td>
   <td style="width:113.75pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="167">
    <p align="right" class="TableBody" style="margin-top:2.0pt;margin-right:42.5pt;
  margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;text-align:right">
     &euro;41,476
    </p>
   </td>
  </tr>
  <tr>
   <td style="width:92.5pt;border-top:none;border-left:
  solid red 2.25pt;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="136">
    <p class="TableHeading2">
     Australia/NZ
    </p>
   </td>
   <td style="width:119.25pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="176">
    <p align="right" class="TableBody" style="margin-top:2.0pt;margin-right:51.5pt;
  margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;text-align:right">
     $35,961
    </p>
   </td>
   <td style="width:118.0pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="174">
    <p align="right" class="TableBody" style="margin-top:2.0pt;margin-right:42.5pt;
  margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;text-align:right">
     $51,643
    </p>
   </td>
   <td style="width:113.75pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="167">
    <p align="right" class="TableBody" style="margin-top:2.0pt;margin-right:42.5pt;
  margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;text-align:right">
     <span style="font-size:8.0pt">
      A
     </span>
     $68,321
    </p>
   </td>
  </tr>
 </tbody>
</table>
<p>
 Note that almost all of our Asian respondents worked in Japan, Hong Kong, Taiwan, Korea, or Singapore. Salaries are likely lower in poorer Asian countries.
</p>
<h3>
 Designers and Writers Earn Less
</h3>
<p>
 In addition to compensation for usability specialists, our survey also includes data from other groups of user experience professionals. The results? Compared with the annual salaries of usability specialists,
</p>
<ul style="margin-top:0in" type="disc">
 <li class="MsoNormal">
  designers made $6,181 less, and
 </li>
 <li class="MsoNormal">
  writers made $9,709 less.
 </li>
</ul>
<p>
 These estimates are based on a multiple regression analysis that accounts for geographical location, job title, and years of experience. Thus, having the job title &ldquo;designer&rdquo; would on average predict a salary that was $6,181 lower than having the job title &ldquo;usability specialist.&rdquo;
</p>
<p>
 There are many different kinds of designers, and in the interest of keeping the survey short, we did not attempt to break out professional interaction designers from graphic designers or other web designers.
</p>
<h3>
 Experienced Staff Earns More
</h3>
<p>
 Compensation increased by $2,265 for each year of professional experience. Thus, somebody with five years&rsquo; experience would make an average of $11,325 more than the starting salaries shown in the table above.
</p>
<h3>
 Impact of Individual Skills
</h3>
<p>
 Our full multiple regression analysis accounts for only 44% of the salary variance in the responses. In other words, 44% of salary variability is explained by job function, geographical location, and years of experience. This leaves 56% of the variability to be explained by other variables.
</p>
<p>
 Intuitively, most of the residual variance is probably explained by differences in individual skill. Some people are just better at what they do than others, and people&rsquo;s ability to understand user behavior and its impact on design is not solely determined by how long they&rsquo;ve been in the business.
</p>
<p>
 Unfortunately, we could not find a simple way to assess professional skills in a survey. We joked about adding the question,
 <i>
  &ldquo;Are you any good?&rdquo;
 </i>
 with responses ranging from
 <i>
  &ldquo;1 = I mess up constantly&rdquo;
 </i>
 to
 <i>
  &ldquo;5 = Brilliant work every time.&rdquo;
 </i>
</p>
<p>
 Realistically, though, the only way to assess skill would be to subject respondents to an exam or collect evaluations from their bosses. Neither tactic would have done our response rate much good.
</p>
<h3>
 Are They Worth It?
</h3>
<p>
 Are user experience professionals worth their high salaries? Absolutely. In fact, the writers deserve an immediate raise. Online writing is not just content, it&rsquo;s the core value on most sites and the first thing users look at. Writers are certainly worth more than what our respondents are being paid.
</p>
<p>
 Nonetheless, it&rsquo;s easier to justify usability specialists&rsquo; salaries because their work contributes directly to the bottom line. It increases productivity for work-oriented sites, and increases conversion rates for marketing-oriented sites. For example: We recently studied a corporate intranet site and, by fixing just one of the usability problems we found, the company gained an estimated $330,000 per year in employee productivity.
</p>
<h2>
 Data Collection
</h2>
<p>
 From November 2000 to April 2001, Nielsen Norman Group organized a series of
 <a href="../../worldtour/index.php">
  conferences called the User Experience World Tour
 </a>
 . The World Tour visited the following cities:
</p>
<ul>
 <li>
  United States:
 </li>
 <li class="MsoNormal" style="text-indent:.5in">
  New York, NY, November 14, 2000
 </li>
 <li class="MsoNormal" style="margin-left:.5in">
  Chicago, IL, November 17, 2000
 </li>
 <li class="MsoNormal" style="margin-left:.5in">
  Austin, TX, January 8, 2001
 </li>
 <li class="MsoNormal" style="margin-left:.5in">
  San Francisco, CA, January 18, 2001
 </li>
 <li class="MsoNormal" style="margin-left:.5in">
  Seattle, WA, April 6, 2001
 </li>
 <li>
  Europe:
 </li>
 <li class="MsoNormal" style="text-indent:.5in">
  London, U.K., November 29, 2000
 </li>
 <li class="MsoNormal" style="text-indent:.5in">
  Munich, Germany, December 4, 2000
 </li>
 <li class="MsoNormal" style="text-indent:.5in">
  Stockholm, Sweden, December 7, 2000
 </li>
 <li>
  Asia:
 </li>
 <li class="MsoNormal" style="text-indent:.5in">
  Tokyo, Japan, February 19, 2001
 </li>
 <li class="MsoNormal" style="text-indent:.5in">
  Hong Kong, China, February 22, 2001
 </li>
 <li>
  Australia:
 </li>
 <li class="MsoNormal" style="text-indent:.5in">
  Sydney, New South Wales, March 1, 2001
 </li>
</ul>
<p>
 (For more information about the User Experience World Tour and interviews with some of the conference participants, see
 <a href="../../worldtour/index.php">
  http://www.nngroup.com/worldtour
 </a>
 .)
</p>
<p>
 A total of 2,682 user experience professionals participated in the tour&rsquo;s &ldquo;Main Event,&rdquo; which was a day of keynote presentations by Dr. Jakob Nielsen, Dr. Brenda Laurel, Bruce &ldquo;Tog&rdquo; Tognazzini, and Dr. Donald A. Norman. Many other people participated in tutorials and extension events; these were held in Los Angeles and New Jersey in April 2001 for participants who missed the sold-out tutorials during the main tour.
</p>
<p>
 We distributed our salary survey to the 2,682 Main Event participants and collected 1,078 completed surveys, for a response rate of 40%. The survey form was anonymous: we did not ask respondents for their name or the name of their company. However, we did give respondents the opportunity to drop their business cards in a separate box for a chance to win a free copy of Nielsen Norman Group&rsquo;s 214 design guidelines for e-commerce user experience.
</p>
<p>
 The following table shows the distribution of responses by regional location.
</p>
<table border="1" cellpadding="0" cellspacing="0" class="MsoNormalTable" style="border-collapse:collapse;border:none">
 <tbody>
  <tr>
   <td style="width:167.75pt;border-top:windowtext 1.0pt;
  border-left:red 2.25pt;border-bottom:maroon 2.25pt;border-right:windowtext 1.0pt;
  border-style:solid;background:#D9D9D9;padding:1.45pt 5.75pt 1.45pt 5.75pt" valign="top" width="247">
    <p class="TableHeading">
     Region
    </p>
   </td>
   <td style="width:167.75pt;border-top:solid windowtext 1.0pt;
  border-left:none;border-bottom:solid maroon 2.25pt;border-right:solid windowtext 1.0pt;
  background:#D9D9D9;padding:1.45pt 5.75pt 1.45pt 5.75pt" valign="top" width="247">
    <p align="center" class="TableHeading" style="text-align:center">
     Responses
    </p>
   </td>
  </tr>
  <tr>
   <td style="width:167.75pt;border-top:none;border-left:
  solid red 2.25pt;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="247">
    <p class="TableHeading2">
     United States
    </p>
   </td>
   <td style="width:167.75pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="247">
    <p align="right" class="TableBody" style="margin-top:2.0pt;margin-right:1.0in;
  margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;text-align:right">
     573
    </p>
   </td>
  </tr>
  <tr>
   <td style="width:167.75pt;border-top:none;border-left:
  solid red 2.25pt;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="247">
    <p class="TableHeading2">
     Canada
    </p>
   </td>
   <td style="width:167.75pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="247">
    <p align="right" class="TableBody" style="margin-top:2.0pt;margin-right:1.0in;
  margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;text-align:right">
     33
    </p>
   </td>
  </tr>
  <tr>
   <td style="width:167.75pt;border-top:none;border-left:
  solid red 2.25pt;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="247">
    <p class="TableHeading2">
     Latin America
    </p>
   </td>
   <td style="width:167.75pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="247">
    <p align="right" class="TableBody" style="margin-top:2.0pt;margin-right:1.0in;
  margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;text-align:right">
     7
    </p>
   </td>
  </tr>
  <tr>
   <td style="width:167.75pt;border-top:none;border-left:
  solid red 2.25pt;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="247">
    <p class="TableHeading2">
     Europe
    </p>
   </td>
   <td style="width:167.75pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="247">
    <p align="right" class="TableBody" style="margin-top:2.0pt;margin-right:1.0in;
  margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;text-align:right">
     299
    </p>
   </td>
  </tr>
  <tr>
   <td style="width:167.75pt;border-top:none;border-left:
  solid red 2.25pt;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="247">
    <p class="TableHeading2">
     Asia
    </p>
   </td>
   <td style="width:167.75pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="247">
    <p align="right" class="TableBody" style="margin-top:2.0pt;margin-right:1.0in;
  margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;text-align:right">
     62
    </p>
   </td>
  </tr>
  <tr>
   <td style="width:167.75pt;border-top:none;border-left:
  solid red 2.25pt;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="247">
    <p class="TableHeading2">
     Australia/NZ
    </p>
   </td>
   <td style="width:167.75pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="247">
    <p align="right" class="TableBody" style="margin-top:2.0pt;margin-right:1.0in;
  margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;text-align:right">
     89
    </p>
   </td>
  </tr>
  <tr>
   <td style="width:167.75pt;border-top:none;border-left:
  solid red 2.25pt;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="247">
    <p class="TableHeading2">
     Africa
    </p>
   </td>
   <td style="width:167.75pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="247">
    <p align="right" class="TableBody" style="margin-top:2.0pt;margin-right:1.0in;
  margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;text-align:right">
     1
    </p>
   </td>
  </tr>
 </tbody>
</table>
<p>
 For the purposes of data analysis in this report, Israel was coded with Europe. Also, as our regional response rate shows, we did not have sufficient data to estimate salaries for Latin America and Africa.
</p>
<h3>
 Bias Due to Source of Data
</h3>
<p>
 The fact that we collected salary data from the audience of a high-end professional user experience conference provides several sources of bias. First, only people with a strong commitment to the profession are likely to attend this type of conference. People who are less committed, less knowledgeable, or less talented may prefer lower-level events. Second, only successful professionals are likely to convince their management to cover the expense of attending this type of event (the registration fee alone was US$680 for early registrations and US$750 for late registrations).
</p>
<p>
 Because we collected responses from this biased sample, rather than a random sample, it is likely that we have surveyed the high end. In other words, the salary data we report reflects the compensation of
 <i>
  good
 </i>
 user experience professionals. Less accomplished staff may well earn less money.
</p>
<h3>
 Survey Focused on Cash Compensation
</h3>
<p>
 We asked respondents the following question:
</p>
<p class="UserQuote">
 What is your total yearly compensation, including salary and bonuses?
</p>
<p>
 We did not ask about benefits, which (from the employer&rsquo;s perspective) can add substantially to the total compensation.
</p>
<p>
 Also, while people working in the Internet industry or for start-up companies often receive stock options, we did not include them in this survey. Stock options were particularly lucrative in 1999, but during the survey period (November 2000 to April 2001), most stock prices were dropping and many stock options were under water. Thus, for this salary survey, including stock options would probably not have added much to most respondents&rsquo; total compensation.
</p>
<h2>
 Basic Compensation Model
</h2>
<p>
 A multiple regression analysis of the salary survey provides the following model of the average annual compensation for user experience professionals:
</p>
<p>
 Starting salary: $67,118.
</p>
<p>
 For each year&rsquo;s experience, add $2,265.
</p>
<h3>
 Salary by Job Function
</h3>
<p>
 These numbers are for usability professionals working in the United States. Modifications should be made as follows for other job titles:
</p>
<table border="1" cellpadding="0" cellspacing="0" class="MsoNormalTable" style="border-collapse:collapse;border:none">
 <tbody>
  <tr>
   <td style="width:167.75pt;border-top:windowtext 1.0pt;
  border-left:red 2.25pt;border-bottom:maroon 2.25pt;border-right:windowtext 1.0pt;
  border-style:solid;background:#D9D9D9;padding:1.45pt 5.75pt 1.45pt 5.75pt" valign="top" width="247">
    <p class="TableHeading">
     Job Function
    </p>
   </td>
   <td style="width:167.75pt;border-top:solid windowtext 1.0pt;
  border-left:none;border-bottom:solid maroon 2.25pt;border-right:solid windowtext 1.0pt;
  background:#D9D9D9;padding:1.45pt 5.75pt 1.45pt 5.75pt" valign="top" width="247">
    <p class="TableHeading">
     Salary Modification
    </p>
   </td>
  </tr>
  <tr>
   <td style="width:167.75pt;border-top:none;border-left:
  solid red 2.25pt;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="247">
    <p class="TableHeading2">
     Usability Professional
    </p>
   </td>
   <td style="width:167.75pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="247">
    <p align="right" class="TableBody" style="margin-top:2.0pt;margin-right:63.0pt;
  margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;text-align:right">
     -
    </p>
   </td>
  </tr>
  <tr>
   <td style="width:167.75pt;border-top:none;border-left:
  solid red 2.25pt;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="247">
    <p class="TableHeading2">
     Designer
    </p>
   </td>
   <td style="width:167.75pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="247">
    <p align="right" class="TableBody" style="margin-top:2.0pt;margin-right:63.0pt;
  margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;text-align:right">
     &ndash; $6,181
    </p>
   </td>
  </tr>
  <tr>
   <td style="width:167.75pt;border-top:none;border-left:
  solid red 2.25pt;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="247">
    <p class="TableHeading2">
     Writer
    </p>
   </td>
   <td style="width:167.75pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="247">
    <p align="right" class="TableBody" style="margin-top:2.0pt;margin-right:63.0pt;
  margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;text-align:right">
     &ndash; $9,709
    </p>
   </td>
  </tr>
 </tbody>
</table>
<p>
 For a manager, add $10,254.
</p>
<h3>
 Salary by Region
</h3>
<p>
 To estimate salaries for other regions, modify the salary estimate as follows:
</p>
<table border="1" cellpadding="0" cellspacing="0" class="MsoNormalTable" style="border-collapse:collapse;border:none">
 <tbody>
  <tr>
   <td style="width:167.75pt;border-top:windowtext 1.0pt;
  border-left:red 2.25pt;border-bottom:maroon 2.25pt;border-right:windowtext 1.0pt;
  border-style:solid;background:#D9D9D9;padding:1.45pt 5.75pt 1.45pt 5.75pt" valign="top" width="247">
    <p class="TableHeading">
     Location
    </p>
   </td>
   <td style="width:167.75pt;border-top:solid windowtext 1.0pt;
  border-left:none;border-bottom:solid maroon 2.25pt;border-right:solid windowtext 1.0pt;
  background:#D9D9D9;padding:1.45pt 5.75pt 1.45pt 5.75pt" valign="top" width="247">
    <p class="TableHeading">
     Salary Modification
    </p>
   </td>
  </tr>
  <tr>
   <td style="width:167.75pt;border-top:none;border-left:
  solid red 2.25pt;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="247">
    <p class="TableHeading2">
     USA
    </p>
   </td>
   <td style="width:167.75pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="247">
    <p align="right" class="TableBody" style="margin-top:2.0pt;margin-right:63.0pt;
  margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;text-align:right">
     -
    </p>
   </td>
  </tr>
  <tr>
   <td style="width:167.75pt;border-top:none;border-left:
  solid red 2.25pt;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="247">
    <p class="TableHeading2">
     Canada
    </p>
   </td>
   <td style="width:167.75pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="247">
    <p align="right" class="TableBody" style="margin-top:2.0pt;margin-right:63.0pt;
  margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;text-align:right">
     &ndash; $17,252
    </p>
   </td>
  </tr>
  <tr>
   <td style="width:167.75pt;border-top:none;border-left:
  solid red 2.25pt;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="247">
    <p class="TableHeading2">
     Europe
    </p>
   </td>
   <td style="width:167.75pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="247">
    <p align="right" class="TableBody" style="margin-top:2.0pt;margin-right:63.0pt;
  margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;text-align:right">
     &ndash; $30,952
    </p>
   </td>
  </tr>
  <tr>
   <td style="width:167.75pt;border-top:none;border-left:
  solid red 2.25pt;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="247">
    <p class="TableHeading2">
     Asia
    </p>
   </td>
   <td style="width:167.75pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="247">
    <p align="right" class="TableBody" style="margin-top:2.0pt;margin-right:63.0pt;
  margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;text-align:right">
     &ndash; $17,821
    </p>
   </td>
  </tr>
  <tr>
   <td style="width:167.75pt;border-top:none;border-left:
  solid red 2.25pt;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="247">
    <p class="TableHeading2">
     Australia/NZ
    </p>
   </td>
   <td style="width:167.75pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="247">
    <p align="right" class="TableBody" style="margin-top:2.0pt;margin-right:63.0pt;
  margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;text-align:right">
     &ndash; $31,157
    </p>
   </td>
  </tr>
 </tbody>
</table>
<p>
 (The above numbers are in U.S. Dollars.)
</p>
<p>
 Insufficient data to estimate salary modifications for Latin America and Africa.
</p>
<h3>
 Using the Multiple Regression Estimates
</h3>
<p>
 We derived all salary estimates from multiple regression analyses that calculate the best-fitting equation that predicts the dependent variable (annual compensation) based on the values of the independent variables (experience, job function, and job location).
</p>
<p>
 To estimate the salary for a person with certain characteristics, begin with the starting salary and then add the amount corresponding to the number of years of professional experience. If job title or location are known, modify the salary estimate accordingly by adding or subtracting the corresponding amounts.
</p>
<p>
 The multiple regression analysis accounts for only 44% of the variability in the underlying survey data; individual skills and other factors account for the remaining 56%. Thus, you should not use the mathematical model to predict or determine the actual salary of specific individuals. However, the model is quite useful for providing a picture of salaries for different levels of experience, job functions, and locations. Some companies probably pay their employees above these estimates, while others pay below them.
</p>
<p>
 The estimates above are the output from our general multiple regression analysis of all the surveys. These numbers best describe the overall state of world-wide user experience salaries in early 2001.
</p>
<p>
 In the following sections, we offer more detailed models for specific countries and regions. We recommend that you use these models when considering salaries for jobs in specific areas.
</p>
<p>
 There are small differences in the global and regional estimates. For example, the world-wide compensation model estimates a starting salary of $67,118 for usability specialists in the U.S., whereas the detailed U.S. model lists the amount as $59,301.
</p>
<p>
 Such differences are due to the fact that the models take different variables into account. For example, the world-wide model does not account for different income levels in different parts of the U.S. From a global perspective, the U.S. is one country.
</p>
<p>
 In contrast, the detailed U.S. salary model accounts for regional variations; we collected many responses from several states, which let us estimate how much they differ from the rest of the country. The detailed U.S. model thus has a lower base, since we added to the estimate for many jobs based on location. For example, we estimate that jobs based in California pay $20,012 more than jobs in most other states.
</p>
<p>
 Ultimately, the only states for which we gathered enough data to reliably estimate pay differential relative to other states are those where people tend to earn more money. This makes sense, since these states also tend to have more advanced businesses that send more people to high-end professional conferences. Thus, the detailed regression model for the U.S. ends up with a lower default salary because it applies only to people located in states with lower pay. In contrast, the world-wide model does not consider individual states and thus its default salary estimate must apply to people in all states, including the high-paying ones.
</p>
<h2>
 Detailed Compensation Model for the United States
</h2>
<p>
 This section only considers salary data for user experience professionals located in the U.S. Amounts are stated in U.S. Dollars.
</p>
<p>
 Starting salary for usability professionals: $59,301.
</p>
<p>
 For each year&rsquo;s experience, add $2,161.
</p>
<h3>
 Salary by Job Function
</h3>
<p>
 The above numbers are for usability professionals working in the United States. Modifications should be made as follows for other job titles:
</p>
<table border="1" cellpadding="0" cellspacing="0" class="MsoNormalTable" style="border-collapse:collapse;border:none">
 <tbody>
  <tr>
   <td style="width:167.75pt;border-top:windowtext 1.0pt;
  border-left:red 2.25pt;border-bottom:maroon 2.25pt;border-right:windowtext 1.0pt;
  border-style:solid;background:#D9D9D9;padding:1.45pt 5.75pt 1.45pt 5.75pt" valign="top" width="247">
    <p class="TableHeading">
     Job Function
    </p>
   </td>
   <td style="width:167.4pt;border-top:solid windowtext 1.0pt;
  border-left:none;border-bottom:solid maroon 2.25pt;border-right:solid windowtext 1.0pt;
  background:#D9D9D9;padding:1.45pt 5.75pt 1.45pt 5.75pt" valign="top" width="246">
    <p class="TableHeading">
     Salary Modification
    </p>
   </td>
  </tr>
  <tr>
   <td style="width:167.75pt;border-top:none;border-left:
  solid red 2.25pt;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="247">
    <p class="TableHeading2">
     Usability Professional
    </p>
   </td>
   <td style="width:167.4pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="246">
    <p align="right" class="TableBody" style="margin-top:2.0pt;margin-right:62.65pt;
  margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;text-align:right">
     -
    </p>
   </td>
  </tr>
  <tr>
   <td style="width:167.75pt;border-top:none;border-left:
  solid red 2.25pt;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="247">
    <p class="TableHeading2">
     Designer
    </p>
   </td>
   <td style="width:167.4pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="246">
    <p align="right" class="TableBody" style="margin-top:2.0pt;margin-right:62.65pt;
  margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;text-align:right">
     &ndash; $8,169
    </p>
   </td>
  </tr>
  <tr>
   <td style="width:167.75pt;border-top:none;border-left:
  solid red 2.25pt;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="247">
    <p class="TableHeading2">
     Writer
    </p>
   </td>
   <td style="width:167.4pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="246">
    <p align="right" class="TableBody" style="margin-top:2.0pt;margin-right:62.65pt;
  margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;text-align:right">
     &ndash; $12,748
    </p>
   </td>
  </tr>
 </tbody>
</table>
<p>
 For a manager, add $11,119.
</p>
<h3>
 Salary by State
</h3>
<p>
 For the following states, modify the salary estimate as indicated:
</p>
<p align="center" class="MsoNormal" style="text-align:center">
 &nbsp;
</p>
<table border="1" cellpadding="0" cellspacing="0" class="MsoNormalTable" style="border-collapse:collapse;border:none">
 <tbody>
  <tr>
   <td style="width:167.75pt;border-top:windowtext 1.0pt;
  border-left:red 2.25pt;border-bottom:maroon 2.25pt;border-right:windowtext 1.0pt;
  border-style:solid;background:#D9D9D9;padding:1.45pt 5.75pt 1.45pt 5.75pt" valign="top" width="247">
    <p class="TableHeading">
     Location
    </p>
   </td>
   <td style="width:167.4pt;border-top:solid windowtext 1.0pt;
  border-left:none;border-bottom:solid maroon 2.25pt;border-right:solid windowtext 1.0pt;
  background:#D9D9D9;padding:1.45pt 5.75pt 1.45pt 5.75pt" valign="top" width="246">
    <p class="TableHeading">
     Salary Modification
    </p>
   </td>
  </tr>
  <tr>
   <td style="width:167.75pt;border-top:none;border-left:
  solid red 2.25pt;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="247">
    <p class="TableHeading2">
     New York
    </p>
   </td>
   <td style="width:167.4pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="246">
    <p align="right" class="TableBody" style="margin-top:2.0pt;margin-right:62.65pt;
  margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;text-align:right">
     + $20,432
    </p>
   </td>
  </tr>
  <tr>
   <td style="width:167.75pt;border-top:none;border-left:
  solid red 2.25pt;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="247">
    <p class="TableHeading2">
     California
    </p>
   </td>
   <td style="width:167.4pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="246">
    <p align="right" class="TableBody" style="margin-top:2.0pt;margin-right:62.65pt;
  margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;text-align:right">
     + $20,012
    </p>
   </td>
  </tr>
  <tr>
   <td style="width:167.75pt;border-top:none;border-left:
  solid red 2.25pt;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="247">
    <p class="TableHeading2">
     Massachusetts
    </p>
   </td>
   <td style="width:167.4pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="246">
    <p align="right" class="TableBody" style="margin-top:2.0pt;margin-right:62.65pt;
  margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;text-align:right">
     + $12,032
    </p>
   </td>
  </tr>
  <tr>
   <td style="width:167.75pt;border-top:none;border-left:
  solid red 2.25pt;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="247">
    <p class="TableHeading2">
     Washington, DC
    </p>
   </td>
   <td style="width:167.4pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="246">
    <p align="right" class="TableBody" style="margin-top:2.0pt;margin-right:62.65pt;
  margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;text-align:right">
     + $8,131
    </p>
   </td>
  </tr>
  <tr>
   <td style="width:167.75pt;border-top:none;border-left:
  solid red 2.25pt;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="247">
    <p class="TableHeading2">
     Texas
    </p>
   </td>
   <td style="width:167.4pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="246">
    <p align="right" class="TableBody" style="margin-top:2.0pt;margin-right:62.65pt;
  margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;text-align:right">
     + $7,630
    </p>
   </td>
  </tr>
  <tr>
   <td style="width:167.75pt;border-top:none;border-left:
  solid red 2.25pt;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="247">
    <p class="TableHeading2">
     New Jersey
    </p>
   </td>
   <td style="width:167.4pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="246">
    <p align="right" class="TableBody" style="margin-top:2.0pt;margin-right:62.65pt;
  margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;text-align:right">
     + $6,726
    </p>
   </td>
  </tr>
  <tr>
   <td style="width:167.75pt;border-top:none;border-left:
  solid red 2.25pt;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="247">
    <p class="TableHeading2">
     Pennsylvania
    </p>
   </td>
   <td style="width:167.4pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="246">
    <p align="right" class="TableBody" style="margin-top:2.0pt;margin-right:62.65pt;
  margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;text-align:right">
     + $5,587
    </p>
   </td>
  </tr>
 </tbody>
</table>
<p>
 Our data was insufficient to break out numbers for other states.
</p>
<h2>
 Detailed Compensation Model for Europe
</h2>
<p>
 This section only considers salary data for user experience professionals located in Europe.
</p>
<p>
 Amounts are stated in Euros (&euro;). To convert into U.S. Dollars, use the currency exchange rate for the data-collection period of December 2000 (
 <span style="font-size:8.0pt">
  US
 </span>
 $1.00 = &euro;1.14682).
</p>
<p>
 Starting salary for usability professionals: &euro;45,978.
</p>
<p>
 For each year&rsquo;s experience, add &euro;1,733.
</p>
<h3>
 Salary by Job Function
</h3>
<p>
 The above numbers are for usability professionals. For other user experience professionals, modify the amount as follows:
</p>
<table border="1" cellpadding="0" cellspacing="0" class="MsoNormalTable" style="border-collapse:collapse;border:none">
 <tbody>
  <tr>
   <td style="width:167.75pt;border-top:windowtext 1.0pt;
  border-left:red 2.25pt;border-bottom:maroon 2.25pt;border-right:windowtext 1.0pt;
  border-style:solid;background:#D9D9D9;padding:1.45pt 5.75pt 1.45pt 5.75pt" valign="top" width="247">
    <p class="TableHeading">
     Job Function
    </p>
   </td>
   <td style="width:167.4pt;border-top:solid windowtext 1.0pt;
  border-left:none;border-bottom:solid maroon 2.25pt;border-right:solid windowtext 1.0pt;
  background:#D9D9D9;padding:1.45pt 5.75pt 1.45pt 5.75pt" valign="top" width="246">
    <p class="TableHeading">
     Salary Modification
    </p>
   </td>
  </tr>
  <tr>
   <td style="width:167.75pt;border-top:none;border-left:
  solid red 2.25pt;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="247">
    <p class="TableHeading2">
     Usability Professional
    </p>
   </td>
   <td style="width:167.4pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="246">
    <p align="right" class="TableBody" style="margin-top:2.0pt;margin-right:62.65pt;
  margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;text-align:right">
     -
    </p>
   </td>
  </tr>
  <tr>
   <td style="width:167.75pt;border-top:none;border-left:
  solid red 2.25pt;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="247">
    <p class="TableHeading2">
     Designer
    </p>
   </td>
   <td style="width:167.4pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="246">
    <p align="right" class="TableBody" style="margin-top:2.0pt;margin-right:62.65pt;
  margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;text-align:right">
     &ndash; &euro;6,268
    </p>
   </td>
  </tr>
  <tr>
   <td style="width:167.75pt;border-top:none;border-left:
  solid red 2.25pt;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="247">
    <p class="TableHeading2">
     Writer
    </p>
   </td>
   <td style="width:167.4pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="246">
    <p align="right" class="TableBody" style="margin-top:2.0pt;margin-right:62.65pt;
  margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;text-align:right">
     &ndash; &euro;8,737
    </p>
   </td>
  </tr>
 </tbody>
</table>
<p>
 For a manager, add &euro;8,203.
</p>
<h3>
 Salary by Country
</h3>
<p>
 The salary estimate should be modified as follows for certain countries:
</p>
<table border="1" cellpadding="0" cellspacing="0" class="MsoNormalTable" style="border-collapse:collapse;border:none">
 <tbody>
  <tr>
   <td style="width:167.75pt;border-top:windowtext 1.0pt;
  border-left:red 2.25pt;border-bottom:maroon 2.25pt;border-right:windowtext 1.0pt;
  border-style:solid;background:#D9D9D9;padding:1.45pt 5.75pt 1.45pt 5.75pt" valign="top" width="247">
    <p class="TableHeading">
     Location
    </p>
   </td>
   <td style="width:167.4pt;border-top:solid windowtext 1.0pt;
  border-left:none;border-bottom:solid maroon 2.25pt;border-right:solid windowtext 1.0pt;
  background:#D9D9D9;padding:1.45pt 5.75pt 1.45pt 5.75pt" valign="top" width="246">
    <p class="TableHeading">
     Salary Modification
    </p>
   </td>
  </tr>
  <tr>
   <td style="width:167.75pt;border-top:none;border-left:
  solid red 2.25pt;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="247">
    <p class="TableHeading2">
     United Kingdom
    </p>
   </td>
   <td style="width:167.4pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="246">
    <p align="right" class="TableBody" style="margin-top:2.0pt;margin-right:62.65pt;
  margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;text-align:right">
     + &euro;9,201
    </p>
   </td>
  </tr>
  <tr>
   <td style="width:167.75pt;border-top:none;border-left:
  solid red 2.25pt;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="247">
    <p class="TableHeading2">
     Denmark
    </p>
   </td>
   <td style="width:167.4pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="246">
    <p align="right" class="TableBody" style="margin-top:2.0pt;margin-right:62.65pt;
  margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;text-align:right">
     + &euro;6,876
    </p>
   </td>
  </tr>
  <tr>
   <td style="width:167.75pt;border-top:none;border-left:
  solid red 2.25pt;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="247">
    <p class="TableHeading2">
     Germany
    </p>
   </td>
   <td style="width:167.4pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="246">
    <p align="right" class="TableBody" style="margin-top:2.0pt;margin-right:62.65pt;
  margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;text-align:right">
     + &euro;3,193
    </p>
   </td>
  </tr>
  <tr>
   <td style="width:167.75pt;border-top:none;border-left:
  solid red 2.25pt;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="247">
    <p class="TableHeading2">
     Sweden
    </p>
   </td>
   <td style="width:167.4pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="246">
    <p align="right" class="TableBody" style="margin-top:2.0pt;margin-right:62.65pt;
  margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;text-align:right">
     &ndash; &euro;8,744
    </p>
   </td>
  </tr>
  <tr>
   <td style="width:167.75pt;border-top:none;border-left:
  solid red 2.25pt;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="247">
    <p class="TableHeading2">
     Finland
    </p>
   </td>
   <td style="width:167.4pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="246">
    <p align="right" class="TableBody" style="margin-top:2.0pt;margin-right:62.65pt;
  margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;text-align:right">
     &ndash; &euro;18,986
    </p>
   </td>
  </tr>
 </tbody>
</table>
<p>
 Our data was insufficient to estimate numbers for other countries.
</p>
<h2>
 Detailed Compensation Model for the United Kingdom
</h2>
<p>
 This section only considers salary data for user experience professionals located in the U.K.
</p>
<p>
 Amounts are stated in Pounds Sterling (&pound;). To convert into U.S. Dollars, use the currency exchange rate for the data-collection period of November 2000 (
 <span style="font-size:8.0pt">
  US
 </span>
 $1.00 = &pound;0.70287).
</p>
<p>
 To convert into Euros, use the exchange rate &pound;1.00 = &euro;1.66030.
</p>
<p>
 Starting salary for usability professionals: &pound;30,876.
</p>
<p>
 For each year&rsquo;s experience, add &pound;1,028.
</p>
<h3>
 Salary by Job Function
</h3>
<p>
 The above numbers are for usability professionals. For other user experience professionals, modify the amount as follows:
</p>
<p>
 &nbsp;
</p>
<table border="1" cellpadding="0" cellspacing="0" class="MsoNormalTable" style="border-collapse:collapse;border:none">
 <tbody>
  <tr>
   <td style="width:167.75pt;border-top:windowtext 1.0pt;
  border-left:red 2.25pt;border-bottom:maroon 2.25pt;border-right:windowtext 1.0pt;
  border-style:solid;background:#D9D9D9;padding:1.45pt 5.75pt 1.45pt 5.75pt" valign="top" width="247">
    <p class="TableHeading">
     Job Function
    </p>
   </td>
   <td style="width:167.4pt;border-top:solid windowtext 1.0pt;
  border-left:none;border-bottom:solid maroon 2.25pt;border-right:solid windowtext 1.0pt;
  background:#D9D9D9;padding:1.45pt 5.75pt 1.45pt 5.75pt" valign="top" width="246">
    <p class="TableHeading">
     Salary Modification
    </p>
   </td>
  </tr>
  <tr>
   <td style="width:167.75pt;border-top:none;border-left:
  solid red 2.25pt;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="247">
    <p class="TableHeading2">
     Usability Professional
    </p>
   </td>
   <td style="width:167.4pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="246">
    <p align="right" class="TableBody" style="margin-top:2.0pt;margin-right:62.65pt;
  margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;text-align:right">
     -
    </p>
   </td>
  </tr>
  <tr>
   <td style="width:167.75pt;border-top:none;border-left:
  solid red 2.25pt;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="247">
    <p class="TableHeading2">
     Designer
    </p>
   </td>
   <td style="width:167.4pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="246">
    <p align="right" class="TableBody" style="margin-top:2.0pt;margin-right:62.65pt;
  margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;text-align:right">
     - &pound;7,967
    </p>
   </td>
  </tr>
 </tbody>
</table>
<p>
 For a manager, add &pound;21,022.
</p>
<p>
 Our data was insufficient to estimate numbers for jobs in writing.
</p>
<h3>
 Salaries in London
</h3>
<p>
 If the job is based in London, add &pound;8,941.
</p>
<h2>
 Detailed Compensation Model for Asia
</h2>
<p>
 This section only considers salary data for user experience professionals located in Asia.
</p>
<p>
 Amounts are stated in U.S. Dollars, based on the following exchange rates from the data-collection period of February 2001:
</p>
<table border="1" cellpadding="0" cellspacing="0" class="MsoNormalTable" style="border-collapse:collapse;border:none">
 <tbody>
  <tr>
   <td style="width:167.75pt;border-top:windowtext 1.0pt;
  border-left:red 2.25pt;border-bottom:maroon 2.25pt;border-right:windowtext 1.0pt;
  border-style:solid;background:#D9D9D9;padding:1.45pt 5.75pt 1.45pt 5.75pt" valign="top" width="247">
    <p class="TableHeading">
     Currency
    </p>
   </td>
   <td style="width:167.4pt;border-top:solid windowtext 1.0pt;
  border-left:none;border-bottom:solid maroon 2.25pt;border-right:solid windowtext 1.0pt;
  background:#D9D9D9;padding:1.45pt 5.75pt 1.45pt 5.75pt" valign="top" width="246">
    <p align="right" class="TableHeading" style="margin-right:35.65pt;text-align:
  right">
     Exchange Rate
    </p>
   </td>
  </tr>
  <tr>
   <td style="width:167.75pt;border-top:none;border-left:
  solid red 2.25pt;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="247">
    <p class="TableHeading2">
     Japanese Yen
    </p>
   </td>
   <td style="width:167.4pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="246">
    <p align="right" class="TableBody" style="margin-top:2.0pt;margin-right:35.65pt;
  margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;text-align:right">
     <span style="font-size:8.0pt">
      US
     </span>
     $1.00 = &yen;115.820
    </p>
   </td>
  </tr>
  <tr>
   <td style="width:167.75pt;border-top:none;border-left:
  solid red 2.25pt;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="247">
    <p class="TableHeading2">
     Hong Kong Dollars
    </p>
   </td>
   <td style="width:167.4pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="246">
    <p align="right" class="TableBody" style="margin-top:2.0pt;margin-right:35.65pt;
  margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;text-align:right">
     <span style="font-size:8.0pt">
      US
     </span>
     $1.00 =
     <span style="font-size:8.0pt">
      HK
     </span>
     $7.79990
    </p>
   </td>
  </tr>
  <tr>
   <td style="width:167.75pt;border-top:none;border-left:
  solid red 2.25pt;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="247">
    <p class="TableHeading2">
     Chinese Yuan Renminbi
    </p>
   </td>
   <td style="width:167.4pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="246">
    <p align="right" class="TableBody" style="margin-top:2.0pt;margin-right:35.65pt;
  margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;text-align:right">
     <span style="font-size:8.0pt">
      US
     </span>
     $1.00 =
     <span style="font-size:8.0pt">
      CNY
     </span>
     8.27803
    </p>
   </td>
  </tr>
  <tr>
   <td style="width:167.75pt;border-top:none;border-left:
  solid red 2.25pt;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="247">
    <p class="TableHeading2">
     Taiwan Dollars
    </p>
   </td>
   <td style="width:167.4pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="246">
    <p align="right" class="TableBody" style="margin-top:2.0pt;margin-right:35.65pt;
  margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;text-align:right">
     <span style="font-size:8.0pt">
      US
     </span>
     $1.00 =
     <span style="font-size:8.0pt">
      TWD
     </span>
     32.300
    </p>
   </td>
  </tr>
  <tr>
   <td style="width:167.75pt;border-top:none;border-left:
  solid red 2.25pt;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="247">
    <p class="TableHeading2">
     Singapore Dollars
    </p>
   </td>
   <td style="width:167.4pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="246">
    <p align="right" class="TableBody" style="margin-top:2.0pt;margin-right:35.65pt;
  margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;text-align:right">
     <span style="font-size:8.0pt">
      US
     </span>
     $1.00 =
     <span style="font-size:8.0pt">
      SG
     </span>
     $1.74500
    </p>
   </td>
  </tr>
  <tr>
   <td style="width:167.75pt;border-top:none;border-left:
  solid red 2.25pt;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="247">
    <p class="TableHeading2">
     Korean Won
    </p>
   </td>
   <td style="width:167.4pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="246">
    <p align="right" class="TableBody" style="margin-top:2.0pt;margin-right:35.65pt;
  margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;text-align:right">
     <span style="font-size:8.0pt">
      US
     </span>
     $1.00 =
     <span style="font-size:8.0pt">
      KRW
     </span>
     1,240.00
    </p>
   </td>
  </tr>
  <tr>
   <td style="width:167.75pt;border-top:none;border-left:
  solid red 2.25pt;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="247">
    <p class="TableHeading2">
     Malaysian Ringgit
    </p>
   </td>
   <td style="width:167.4pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="246">
    <p align="right" class="TableBody" style="margin-top:2.0pt;margin-right:35.65pt;
  margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;text-align:right">
     <span style="font-size:8.0pt">
      US
     </span>
     $1.00 =
     <span style="font-size:8.0pt">
      MYR
     </span>
     3.80050
    </p>
   </td>
  </tr>
 </tbody>
</table>
<p>
 Starting salary for usability professionals in Asia:
 <span style="font-size:8.0pt">
  US
 </span>
 $29,947.
</p>
<p>
 For each year&rsquo;s experience, add
 <span style="font-size:8.0pt">
  US
 </span>
 $4,879.
</p>
<h3>
 Salary by Job Function
</h3>
<p>
 For other user experience professionals, modify the amount as follows:
</p>
<table border="1" cellpadding="0" cellspacing="0" class="MsoNormalTable" style="border-collapse:collapse;border:none">
 <tbody>
  <tr>
   <td style="width:167.75pt;border-top:windowtext 1.0pt;
  border-left:red 2.25pt;border-bottom:maroon 2.25pt;border-right:windowtext 1.0pt;
  border-style:solid;background:#D9D9D9;padding:1.45pt 5.75pt 1.45pt 5.75pt" valign="top" width="247">
    <p class="TableHeading">
     Job Function
    </p>
   </td>
   <td style="width:167.4pt;border-top:solid windowtext 1.0pt;
  border-left:none;border-bottom:solid maroon 2.25pt;border-right:solid windowtext 1.0pt;
  background:#D9D9D9;padding:1.45pt 5.75pt 1.45pt 5.75pt" valign="top" width="246">
    <p class="TableHeading">
     Salary Modification
    </p>
   </td>
  </tr>
  <tr>
   <td style="width:167.75pt;border-top:none;border-left:
  solid red 2.25pt;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="247">
    <p class="TableHeading2">
     Usability Professional
    </p>
   </td>
   <td style="width:167.4pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="246">
    <p align="right" class="TableBody" style="margin-top:2.0pt;margin-right:62.65pt;
  margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;text-align:right">
     -
    </p>
   </td>
  </tr>
  <tr>
   <td style="width:167.75pt;border-top:none;border-left:
  solid red 2.25pt;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="247">
    <p class="TableHeading2">
     Designer
    </p>
   </td>
   <td style="width:167.4pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="246">
    <p align="right" class="TableBody" style="margin-top:2.0pt;margin-right:62.65pt;
  margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;text-align:right">
     -
     <span style="font-size:8.0pt">
      US
     </span>
     $4,171
    </p>
   </td>
  </tr>
 </tbody>
</table>
<p>
 Our data was insufficient to estimate numbers for jobs in writing.
</p>
<h3>
 Salary by Country
</h3>
<p>
 The salary estimate should be modified as follows for certain countries:
</p>
<table border="1" cellpadding="0" cellspacing="0" class="MsoNormalTable" style="border-collapse:collapse;border:none">
 <tbody>
  <tr>
   <td style="width:167.75pt;border-top:windowtext 1.0pt;
  border-left:red 2.25pt;border-bottom:maroon 2.25pt;border-right:windowtext 1.0pt;
  border-style:solid;background:#D9D9D9;padding:1.45pt 5.75pt 1.45pt 5.75pt" valign="top" width="247">
    <p class="TableHeading">
     Location
    </p>
   </td>
   <td style="width:167.4pt;border-top:solid windowtext 1.0pt;
  border-left:none;border-bottom:solid maroon 2.25pt;border-right:solid windowtext 1.0pt;
  background:#D9D9D9;padding:1.45pt 5.75pt 1.45pt 5.75pt" valign="top" width="246">
    <p class="TableHeading">
     Salary Modification
    </p>
   </td>
  </tr>
  <tr>
   <td style="width:167.75pt;border-top:none;border-left:
  solid red 2.25pt;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="247">
    <p class="TableHeading2">
     Japan
    </p>
   </td>
   <td style="width:167.4pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="246">
    <p align="right" class="TableBody" style="margin-top:2.0pt;margin-right:62.65pt;
  margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;text-align:right">
     +
     <span style="font-size:8.0pt">
      US
     </span>
     $4,203
    </p>
   </td>
  </tr>
  <tr>
   <td style="width:167.75pt;border-top:none;border-left:
  solid red 2.25pt;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="247">
    <p class="TableHeading2">
     Hong Kong
    </p>
   </td>
   <td style="width:167.4pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="246">
    <p align="right" class="TableBody" style="margin-top:2.0pt;margin-right:62.65pt;
  margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;text-align:right">
     +
     <span style="font-size:8.0pt">
      US
     </span>
     $20,947
    </p>
   </td>
  </tr>
 </tbody>
</table>
<p>
 Our data was insufficient to estimate numbers for other countries.
</p>
<h2>
 Detailed Compensation Model for Australia/New Zealand
</h2>
<p>
 This section only considers salary data for user experience professionals located in Australia and New Zealand. However, since almost all the respondents work in Australia, the results may not be highly indicative for New Zealand.
</p>
<p>
 Amounts are stated in Australian Dollars (
 <span style="font-size:8.0pt">
  A
 </span>
 $). To convert into U.S. Dollars, use the currency exchange rate for the data-collection period of March 2001 (
 <span style="font-size:8.0pt">
  US
 </span>
 $1.00 =
 <span style="font-size:8.0pt">
  A
 </span>
 $1.89985).
</p>
<p>
 To convert into New Zealand Dollars, use the exchange rate
 <span style="font-size:8.0pt">
  A
 </span>
 $1.00 =
 <span style="font-size:8.0pt">
  NZ
 </span>
 $1.22430.
</p>
<p>
 Starting salary for usability professionals:
 <span style="font-size:8.0pt">
  A
 </span>
 $68,354.
</p>
<p>
 For each year&rsquo;s experience, add
 <span style="font-size:8.0pt">
  A
 </span>
 $5,931.
</p>
<h3>
 Salary by Job Function
</h3>
<p>
 For other user experience professionals, modify the amount as follows:
</p>
<table border="1" cellpadding="0" cellspacing="0" class="MsoNormalTable" style="border-collapse:collapse;border:none">
 <tbody>
  <tr>
   <td style="width:167.75pt;border-top:windowtext 1.0pt;
  border-left:red 2.25pt;border-bottom:maroon 2.25pt;border-right:windowtext 1.0pt;
  border-style:solid;background:#D9D9D9;padding:1.45pt 5.75pt 1.45pt 5.75pt" valign="top" width="247">
    <p class="TableHeading">
     Job Function
    </p>
   </td>
   <td style="width:167.4pt;border-top:solid windowtext 1.0pt;
  border-left:none;border-bottom:solid maroon 2.25pt;border-right:solid windowtext 1.0pt;
  background:#D9D9D9;padding:1.45pt 5.75pt 1.45pt 5.75pt" valign="top" width="246">
    <p class="TableHeading">
     Salary Modification
    </p>
   </td>
  </tr>
  <tr>
   <td style="width:167.75pt;border-top:none;border-left:
  solid red 2.25pt;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="247">
    <p class="TableHeading2">
     Usability Professional
    </p>
   </td>
   <td style="width:167.4pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="246">
    <p align="right" class="TableBody" style="margin-top:2.0pt;margin-right:62.65pt;
  margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;text-align:right">
     -
    </p>
   </td>
  </tr>
  <tr>
   <td style="width:167.75pt;border-top:none;border-left:
  solid red 2.25pt;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="247">
    <p class="TableHeading2">
     Designer
    </p>
   </td>
   <td style="width:167.4pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="246">
    <p align="right" class="TableBody" style="margin-top:2.0pt;margin-right:62.65pt;
  margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;text-align:right">
     -
     <span style="font-size:8.0pt">
      A
     </span>
     $15,981
    </p>
   </td>
  </tr>
  <tr>
   <td style="width:167.75pt;border-top:none;border-left:
  solid red 2.25pt;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="247">
    <p class="TableHeading2">
     Writer
    </p>
   </td>
   <td style="width:167.4pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.75pt 0in 5.75pt" valign="top" width="246">
    <p align="right" class="TableBody" style="margin-top:2.0pt;margin-right:62.65pt;
  margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;text-align:right">
     -
     <span style="font-size:8.0pt">
      A
     </span>
     $24,902
    </p>
   </td>
  </tr>
 </tbody>
</table>
<h2>
 More Recent Salary Surveys
</h2>
<p>
 See the
 <a href="http://www.useit.com/alertbox/salaries.php">
  2008 update of these salary statistics
 </a>
 .
</p>
<h2>
 Acknowledgements
</h2>
<p class="MsoNormal" style="text-autospace:none">
 I would like to thank each of the 1,078 user experience professionals who completed the survey.
</p>
<p class="MsoNormal" style="text-autospace:none">
 Also, I thank the following people for help with administering the survey and collecting the data:
</p>
<ul>
 <li>
  <a href="../../people/kara-pernice/index.php">
   Kara Pernice
  </a>
 </li>
 <li>
  Lori D&#39;Alessio
 </li>
 <li>
  Shuli Gilutz
 </li>
 <li>
  <a href="../../people/luice-hwang/index.php">
   Luice Hwang
  </a>
 </li>
 <li>
  <a href="http://www.alom.com/">
   Hannah Kain
  </a>
 </li>
 <li>
  <a href="../../people/hoa-loranger/index.php">
   Hoa Loranger
  </a>
 </li>
 <li>
  Ronnye Schreiber
 </li>
 <li>
  <a href="http://www.word1editorial.com/">
   Keri Schreiner
  </a>
 </li>
 <li>
  Marie Tahir
 </li>
</ul>
<!--21 nbsp's for spacing-->
<p>
 <span>
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
 </span>
 &nbsp;&nbsp; &nbsp;
</p>
<p>
 &nbsp;
</p>
</section>
  

  
  <section>
    
        
    
  </section>
  
  
  
  
  

  


  <section>
    <br />
    <p class="share"><b>Share <span class="hide-for-small-down">this article</span>:</b> <a href="https://twitter.com/intent/tweet?url=http://www.nngroup.com/articles/salary-survey-user-experience-professionals-2001/&amp;text=Salary%20Survey:%20User%20Experience%20Professionals%202001&amp;via=nngroup">Twitter</a> | <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=http://www.nngroup.com/articles/salary-survey-user-experience-professionals-2001/&amp;title=Salary%20Survey:%20User%20Experience%20Professionals%202001&amp;source=Nielsen%20Norman%20Group">LinkedIn</a> | <a href="https://plus.google.com/share?url=http://www.nngroup.com/articles/salary-survey-user-experience-professionals-2001/">Google+</a> | <a href="mailto:?subject=NN/g Article: Salary Survey: User Experience Professionals 2001&amp;body=http://www.nngroup.com/articles/salary-survey-user-experience-professionals-2001/">Email</a></p>
    <hr />
  </section>

  <footer class="l-subsection">
    
    
    <div class="learn-more">
      <h2>Learn More</h2>
      <p><a href="../subscribe/index.php">Subscribe to the weekly newsletter</a> to get notified about future articles.</p>
      </div>
    
    

    <div class="row show-for-small-down related-content">
      
      
      

      
      
        <div class="column small-12 topic-links">All articles about:
          <ul>
            
            <li><a href="../../topic/ux-management/index.php">Management</a></li>
            
            <li><a href="../../topic/strategy/index.php">Strategy</a></li>
            
          </ul>
        </div>
      
      

      
      
        <div class="column small-12">
          <h3>UX Conference Training Courses</h3>
           <ul class="no-bullet">
    
        <li><a href="../../courses/ux-basic-training/index.php">UX Basic Training</a></li>
    
        <li><a href="../../courses/journey-mapping/index.php">Journey Mapping to Understand Customer Needs</a></li>
    
        <li><a href="../../courses/facilitating-ux-workshops/index.php">Facilitating UX Workshops</a></li>
    
        <li><a href="../../courses/analytics-and-user-experience/index.php">Analytics and User Experience</a></li>
    
        <li><a href="../../courses/ux-deliverables/index.php">UX Deliverables</a></li>
    
</ul>
        </div>
      
      

      
      
        <div class="small-12 column">
          <h3>Research Reports</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../reports/usability-return-on-investment-roi/index.php">Return on Investment (ROI) for Usability</a></li>
              
            
              
                <li><a href="../../reports/agile-development-user-experience/index.php"> Effective Agile UX Product Development</a></li>
              
            
              
                <li><a href="../../reports/user-experience-careers/index.php">User Experience Careers</a></li>
              
            
          </ul>
        </div>
      
      

      
      
        <div class="small-12 column">
          <h3>Online Seminars</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../online-seminars/ux-real-world/index.php">UX in the Real World</a></li>
              
            
              
                <li><a href="../../online-seminars/how-change-your-corporate-culture/index.php">How to Change Your Corporate Culture to be User First</a></li>
              
            
          </ul>
        </div>
      
      
    </div>

    <div class="hide-for-small-down row related-content">
      <div class="medium-6 columns">
        
        
          <div>
            <h3>Articles</h3>
            <ul class="no-bullet">
              
                
                <li><a href="../apple-makes-grandmothers/index.php">Apple Is Making Us All My Grandmother</a></li>
                
              
                
                <li><a href="../salary-trends-usability-professionals/index.php">Salary Trends for UX Professionals</a></li>
                
              
                
                <li><a href="../micro-conversions/index.php">Define Micro Conversions to Measure Incremental UX Improvements</a></li>
                
              
                
                <li><a href="../quality-assurance-ux/index.php">QA &amp; UX</a></li>
                
              
                
                <li><a href="../ux-maturity-stages-5-8/index.php">Corporate UX Maturity: Stages 5-8</a></li>
                
              
            </ul>
          </div>
        
        

        
          
          <div>
            <h3>Research Reports</h3>
            <ul class="no-bullet">
              
                
                  <li><a href="../../reports/usability-return-on-investment-roi/index.php">Return on Investment (ROI) for Usability</a></li>
                
              
                
                  <li><a href="../../reports/agile-development-user-experience/index.php"> Effective Agile UX Product Development</a></li>
                
              
                
                  <li><a href="../../reports/user-experience-careers/index.php">User Experience Careers</a></li>
                
              
            </ul>
          </div>
          
        

      </div>
      <div class="medium-6 columns">
        
          
          <div>
            <h3>UX Conference Training Courses</h3>
             <ul class="no-bullet">
    
        <li><a href="../../courses/ux-basic-training/index.php">UX Basic Training</a></li>
    
        <li><a href="../../courses/journey-mapping/index.php">Journey Mapping to Understand Customer Needs</a></li>
    
        <li><a href="../../courses/facilitating-ux-workshops/index.php">Facilitating UX Workshops</a></li>
    
        <li><a href="../../courses/analytics-and-user-experience/index.php">Analytics and User Experience</a></li>
    
        <li><a href="../../courses/ux-deliverables/index.php">UX Deliverables</a></li>
    
</ul>
          </div>
          
        
        
          
          <div>
            <h3>Online Seminars</h3>
            <ul class="no-bullet">
            
                <li><a href="../../online-seminars/ux-real-world/index.php">UX in the Real World</a></li>
            
                <li><a href="../../online-seminars/how-change-your-corporate-culture/index.php">How to Change Your Corporate Culture to be User First</a></li>
            
            </ul>
          </div>
          
        
      </div>
    </div>
  </footer>
</article>

        </div>

      

    
  </div>
  <div class="row">
    
      <div class="column small-12 show-for-small-down l-navrail announcements">
        
          
        
      </div>
    
    
  </div>

    </div>
    




<footer id="page-footer">
    <div class="row hide-for-print">
        <div class="column small-12 medium-6">
            <section class="footer-popular">
                
                <h1>Popular Topics</h1>
                <div style="float: left;  width: 15em; display: block;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/applications/index.php">Application Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/e-commerce/index.php">E-Commerce Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/eyetracking/index.php">Eyetracking </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/information-architecture/index.php">Information Architecture </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/intranets/index.php">Intranet Design </a></li>
	<li><a href="../../topic/interaction-design/index.php">Interaction Design </a></li>
</ul>
</div>

<div style="float: left;  width: 15em; display: block; list-style: none;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/mobile-and-tablet-design/index.php">Mobile and Tablet Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/credibility-persuasion/index.php">Persuasive Design</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/research-methods/index.php">Research Methods</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/user-testing/index.php">User Testing </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/web-usability/index.php">Web Usability </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/writing-web/index.php">Web Writing &amp; Content Strategy </a></li>
</ul>
</div>

<p>&nbsp;</p>

                
            </section>
        </div>

        <div class="column small-12 medium-6">
            <div class="row">
                <div class="column small-6">
                    <section class="footer-about">
                        <h1>Follow Us</h1>
                        <ul>
	<li><a href="../subscribe/index.php">Subscribe to our weekly UX newsletter</a>,&nbsp;Jakob Nielsen&#39;s Alertbox</li>
	<li>Visit Don Norman&#39;s website, <a href="http://www.jnd.org/"> www.jnd.org </a></li>
	<li>Visit Bruce Tognazzini&#39;s website, <a href="http://www.asktog.com/"> www.asktog.com </a></li>
	<li style="padding-top: 6px;"><a class="twitter-follow-button" data-show-count="false" data-size="large" href="https://twitter.com/nngroup"><img alt="Follow @nngroup on Twitter" height="20" src="https://media.nngroup.com/media/editor/2016/12/30/twitter-follow-button.png" style="width: 117px; height: 20px;" width="117" /></a></li>
	<li style="padding-top: 6px;"><img alt="" height="14" src="https://media.nngroup.com/media/editor/2016/07/26/feed-icon-14x14.png" style="vertical-align:text-top;width:14px;margin-right:.25em;" width="14" /><a href="../../feed/rss/index.php">Subscribe to our RSS feed</a></li>
</ul>

                    </section>
                </div>

                <div class="column small-6">
                    <section class="footer-about right-column">
                        <h1>About
</h1>
                        <ul class="no-bullet">
	<li><a href="../../about/contact/index.php">Contact information</a></li>
	<li><a href="../../about/index.php">About Us</a></li>
	<li><a href="../../about/why-nng/index.php">Why NN/g? </a></li>
	<li><a href="../../return-policy/index.php">Return Policy</a></li>
</ul>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="copyright column small-12">
            <p><a href="../../copyright-and-reprint-info/index.php">Copyright</a> &copy; 1998-2017 Nielsen Norman Group, All Rights Reserved.</p>
        </div>
    </div>
</footer>


    


        <script src="https://media.nngroup.com/static/vendor/fastclick/js/fastclick.min.js"></script>
        <script src="https://media.nngroup.com/static/vendor/foundation/js/foundation.min.js"></script>
        <script src="http://media.nngroup.com/static/js/app.js"></script>
        <script src="http://media.nngroup.com/static/js/scroll-to-fixed.js"></script><script>var externalLinks = document.querySelectorAll('a[href^="http"]');for (var i = externalLinks.length-1; i >= 0; i--) {externalLinks[i].addEventListener("click", function(event) { alert("External links are disabled for this test."); event.preventDefault(); }, false);}var mailLinks = document.querySelectorAll('a[href^="mailto"]');for (var i = mailLinks.length-1; i >= 0; i--) {mailLinks[i].addEventListener("click", function(event) { alert("Email links are disabled for this test."); event.preventDefault(); }, false);}var externalForms = document.querySelectorAll('form');for (var i = externalForms.length-1; i >= 0; i--) {externalForms[i].addEventListener("submit", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false); externalForms[i].addEventListener("click", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false);}</script>

        <script>
            
            $(function() {
                $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
                $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
            });
        </script>

        
<script src="https://media.nngroup.com/static/js/articles.js"></script>


        
        <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1072699960;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072699960/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
    </body>

<!-- Mirrored from www.nngroup.com/articles/salary-survey-user-experience-professionals-2001/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:15:52 GMT -->
</html>
