<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->





    
<!-- Mirrored from www.nngroup.com/articles/hypertext-future/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:13:20 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1026.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"licenseKey":"1a30ef9931","errorBeacon":"bam.nr-data.net","applicationID":"2048992","transactionName":"NFxSMRBUX0EDUkIPDA0WdhAMVkVbDV8ZBxEXUFMJB0YfRAtUQRVZIktEDAFZVHYHRVcPD01eVRE=","beacon":"bam.nr-data.net","queueTime":7,"applicationTime":442,"agent":""}</script>
        <title>Future of Hypertext 1998-2015 (As Predicted in 1995) | Jakob Nielsen</title><meta property="og:title" content="Future of Hypertext 1998-2015 (As Predicted in 1995) | Jakob Nielsen" />
  
        
        <meta name="description" content="Excerpt from Jakob Nielsen&#39;s 1995 book, Multimedia and Hypertext: The Internet and Beyond, offers predictions for the short term (3 to 5 year) and medium term (5 to 10 year) and long term (10 to 20 year) future of hypertext and the internet.">
        <meta property="og:description" content="Excerpt from Jakob Nielsen&#39;s 1995 book, Multimedia and Hypertext: The Internet and Beyond, offers predictions for the short term (3 to 5 year) and medium term (5 to 10 year) and long term (10 to 20 year) future of hypertext and the internet." />
        
  
        
	
        

        

        <meta name="robots" content="noarchive">
        <meta name="viewport" content="width=device-width">

        <link rel="search" type="application/opensearchdescription+xml" title="NN/g Search" href="../../opensearch.xml" />
        <link rel="shortcut icon" href="http://media.nngroup.com/static/img/favicon.ico" />

        <link rel="stylesheet" href="https://media.nngroup.com/static/vendor/font-awesome/css/font-awesome.min.css">

        
        <link rel="stylesheet" href="https://media.nngroup.com/static/css/7232a960f1df.css" type="text/css" />
        <link rel="stylesheet" href="http://fast.fonts.com/t/1.css?apiType=css&amp;projectid=c5846260-9def-46ec-9407-0f1349bd2d64" type="text/css">

        <script src="https://media.nngroup.com/static/vendor/modernizr/js/modernizr.min.js"></script>

        <script type="text/javascript" src="https://media.nngroup.com/static/js/3283be5def74.js"></script>
        

        

        
        
        <!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=229034,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('http://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->


<!-- Google Analytics Tracking Code for http://nngroup.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23132681-1', 'nngroup.com');
  ga('require', 'linkid', 'https://www.nngroup.com/articles/hypertext-future/linkid.js');
  ga('require', 'displayfeatures');

// Optimizely Universal Analytics Integration
 window.optimizely = window.optimizely || [];
 window.optimizely.push("activateUniversalAnalytics");

  ga('send', 'pageview');

</script>
<script>
/**
* Function to track a click on an outbound link in Google Analytics.
*/
function trackOutboundLink(label, url) {
    if(window.ga && ga.loaded) {
        ga('send', 'event', 'outbound', 'click', label, {'hitCallback': function(){document.location = url;}
        });
    } else {
        document.location = url;
    }
}
/**
* Function to track a click on a link in the article announcement area.
*/
$('#articleAnnouncement a').on('click', function(){
  ga('send', 'event', 'link', 'click', 'articleAnnouncement');
});
</script>

<script type="text/javascript">
 setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2421.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



<!-- Hotjar Tracking Code for http://nngroup.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:121605,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

        <script src="https://media.nngroup.com/static/js/analytics/track_outbound_link.js"></script>
        
        
        
    </head>
    <body class="location-articles articles-detail">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
    <header id="page-header">
        <div id="cart" class="shopping-cart mobile-cart show-for-small-down"></div>
        <div class="row">
            <div id="masthead" class="masthead small-12 medium-7 column">

                <!-- The button to toggles menu on small screen -->

                <h1><a href="../../index.php"><img src="http://media.nngroup.com/static/img/logo-wordmark.png" width="74" height="30"> Nielsen <span class="norman">Norman</span> Group</a></h1>
                <p class="tagline show-for-medium-up">Evidence-Based User Experience Research, Training, and Consulting</p>

                <!-- The button to toggles form search on small screen -->


                <div style="clear: right;width:  100%;height: 1px;"></div><?php define('ROOT_PATH',$_SERVER["DOCUMENT_ROOT"] . '/apps/navigation_styles/navigations/4'); ?><?php include(ROOT_PATH)."/navigation.php"; ?>

                <!-- Drop down menu on small screen -->
                <div id="hideNav" class="show-for-small-down">
                  
                  <ul class="menu-content">
                    <li><a href="../../index.php">Home</a></li>
                    <li class="has-submenu">
                      <a>Training</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../training/index.php">Overview</a></li>
                        
<li id="nav-ux-conference"><a href="../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../online-seminars/index.php"><span>Online Seminars</span></a></li>

                      </ul>
                    </li>
                    <li class="has-submenu">
                      <a>Consulting</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
  <li><a href="../../consulting/index.php">Overview</a></li>
  
  <li><a href="../../consulting/benchmarking/index.php">Benchmarking</a></li>
  
  <li><a href="../../consulting/expert-design-review/index.php">Expert Design Review</a></li>
  
  <li><a href="../../consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
  
  <li><a href="../../consulting/usability-testing/index.php">Usability Testing </a></li>
  
  <li><a href="../../consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
  
  <li><a href="../../in-house-training/index.php">In-House Training</a></li>
</ul>

                    </li>

                    <li><a href="../../reports/index.php">Reports</a></li>
                    <li><a href="../index.php">Articles</a></li>
                    <li class="has-submenu"><a>About NN/G</a><i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../about/index.php">Overview</a></li>
                        <li><a href="../../people/index.php">People</a></li>
                        <li><a href="../../about/why-nng/index.php">Why NN/g?</a></li>
                        <li><a href="../../about/contact/index.php">Contact</a></li>
                        <li><a href="../../news/index.php">News</a></li>
                        <li><a href="../../about/history/index.php">History</a></li>
                        <li><a href="../../books/index.php">Books</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                </div>

                <hr class="header-divider show-for-small-only"/>


            </div>
       </div>

        <div id="hideNav">            <nav class="primary-nav small-12 column">
                <ul>
                    <li><a class="navitem" id="nav-home" href="../../index.php">Home</a></li>
                    <li><a class="navitem" id="nav-training" href="../../training/index.php">Training</a></li>
                    <li><a class="navitem" id="nav-consulting" href="../../consulting/index.php">Consulting</a></li>
                    <li><a class="navitem" id="nav-reports" href="../../reports/index.php">Reports</a></li>
                    <li><a class="navitem" id="nav-articles" href="../index.php">Articles</a></li>
                    <li><a class="navitem" id="nav-about" href="../../about/index.php">About NN/G</a></li>
                </ul>
            </nav>
            <nav class="secondary-nav small-12 medium-12 large-12 columns">
                
                
            </nav>
        </div>
    </header>
    <div class="l-content">
    
  

  

  <div class="row">
    
      
      <section class="small-12 medium-3 columns collapsable-content l-navrail articles-left-column">
        <h1 class="show-for-small-down collapse-link mobile-browse-header">Browse by Topic and Author</h1>
          <div class="row collapsable">

            
            <div class="small-6 medium-12 columns">
              <section id="article-popular-topics" style="display: none;">
                
                <h1>Topics</h1>
                <ul id="article-popular-topics">
                    
                    <li>
                        
                        <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/intranets/all/index.php">Intranets</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/user-testing/all/index.php">User Testing</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                        
                    </li>
                    
                    <li><a href="#" id="article-see-all-topics"><strong>See all topics&hellip;</strong></a></li>
                </ul>
                
              </section>
              <section id="article-all-topics" style="display: none;">
                
                <h1>
                  <span class="show-for-medium-up">All Article Topics</span>
                  <span class="show-for-small-down">All Topics</span>
                  (<a href="#" id="article-hide-all-topics">hide</a>)
                </h1>
                <ul>
                  
                  <li>
                    
                    <a href="../../topic/accessibility/all/index.php">Accessibility</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/agile/all/index.php">Agile</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/analytics-and-metrics/all/index.php">Analytics &amp; Metrics</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/applications/all/index.php">Application Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/b2b-websites/all/index.php">B2B Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/behavior-patterns/all/index.php">Behavior Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/branding/all/index.php">Branding</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/content-strategy/all/index.php">Content Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/corporate-websites/all/index.php">Corporate Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/design-patterns/all/index.php">Design Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-design-process/all/index.php">Design Process</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/email/all/index.php">Email</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/eyetracking/all/index.php">Eyetracking</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/heuristic-evaluation/all/index.php">Heuristic Evaluation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/human-computer-interaction/all/index.php">Human Computer Interaction</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-ideation/all/index.php">Ideation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/information-architecture/all/index.php">Information Architecture</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/interaction-design/all/index.php">Interaction Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/international-users/all/index.php">International Users</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/intranets/all/index.php">Intranets</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-management/all/index.php">Management</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/navigation/all/index.php">Navigation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/non-profit-websites/all/index.php">Non-Profit Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/personas/all/index.php">Personas</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/persuasive-design/all/index.php">Persuasive Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/prototyping/all/index.php">Prototyping</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/psychology-and-ux/all/index.php">Psychology and UX</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/research-methods/all/index.php">Research Methods</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/search/all/index.php">Search</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/social-media/all/index.php">Social Media</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/strategy/all/index.php">Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/user-testing/all/index.php">User Testing</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-teams/all/index.php">UX Teams</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/visual-design/all/index.php">Visual Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/writing-web/all/index.php">Writing for the Web</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/young-users/all/index.php">Young Users</a>
                    
                  </li>
                  
                </ul>
                
              </section>
            </div>
            <script>
              /* Determine which topics list will be displayed: the full or the
               * partial.  We are inlining this close to the list itself to
               * prevent rendering delays on the client
               */
              var topic_list = $.cookie("article-topics");

              var popular_topics = $('#article-popular-topics');
              var all_topics = $('#article-all-topics');

              if(topic_list == 1) {
                  popular_topics.hide();
                  all_topics.show();
              } else {
                  popular_topics.show();
                  all_topics.hide();
              }
            </script>
            

            
            <div class="small-6 medium-12 columns">
              
              <section id="article-initial-authors">
                  <h1>Author</h1>
                  <ul>
                    
                    <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                    
                    <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                    
                    <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                    
                    <li><a href="#" onClick="$('#article-initial-authors').hide(); $('#article-all-authors').show();"><strong>See all authors&hellip;</strong></a></li>
                  </ul>
              </section>
              <section id="article-all-authors" style="display: none;">
                <h1>
                  All Authors
                  (<a href="#" id="article-hide-all-authors">hide</a>)
                </h1>
                <ul>
                  
                  <li><a href="../author/raluca-budiu/index.php">Raluca Budiu</a></li>
                  
                  <li><a href="../author/patty-caya/index.php">Patty Caya</a></li>
                  
                  <li><a href="../author/susan-farrell/index.php">Susan Farrell</a></li>
                  
                  <li><a href="../author/therese-fessenden/index.php">Therese Fessenden</a></li>
                  
                  <li><a href="../author/kim-flaherty/index.php">Kim Flaherty</a></li>
                  
                  <li><a href="../author/sarah-gibbons/index.php">Sarah Gibbons</a></li>
                  
                  <li><a href="../author/aurora-harley/index.php">Aurora Harley</a></li>
                  
                  <li><a href="../author/kate-kaplan/index.php">Kate Kaplan</a></li>
                  
                  <li><a href="../author/page-laubheimer/index.php">Page Laubheimer</a></li>
                  
                  <li><a href="../author/angela-li/index.php">Angie Li</a></li>
                  
                  <li><a href="../author/hoa-loranger/index.php">Hoa Loranger</a></li>
                  
                  <li><a href="../author/kate-meyer/index.php">Kate Meyer</a></li>
                  
                  <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                  
                  <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                  
                  <li><a href="../author/kara-pernice/index.php">Kara Pernice</a></li>
                  
                  <li><a href="../author/christian-rohrer/index.php">Christian Rohrer</a></li>
                  
                  <li><a href="../author/amy-schade/index.php">Amy Schade</a></li>
                  
                  <li><a href="../author/katie-sherwin/index.php">Katie Sherwin</a></li>
                  
                  <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                  
                  <li><a href="../author/kathryn-whitenton/index.php">Kathryn Whitenton</a></li>
                  
                </ul>
              </section>
              
            </div>
            
          </div>

          
          <section class="hide-for-small-down">
            
              
              <h1>Recent Articles</h1>
              
              <ul>
                  
                  <li><a href="../seamless-cross-channel/index.php">Seamlessness in the Omnichannel User Experience</a></li>
                  
                  <li><a href="../brand-intention-interpretation/index.php">Experience Design: Bridging Brand Intention and Brand Interpretation</a></li>
                  
                  <li><a href="../eyetracking-tasks-efficient-scanning/index.php">Scanning Patterns on the Web Are Optimized for the Current Task</a></li>
                  
                  <li><a href="../flat-design-best-practices/index.php">Flat-Design Best Practices</a></li>
                  
                  <li><a href="../ux-user-stories/index.php">Accounting for UX Work with User Stories in Agile Projects</a></li>
                  
                  <li><a href="../index.php"><strong>See all articles&hellip;</strong></a></li>
              </ul>
              
            
          </section>
          

          
          
          <section class="hide-for-small-down">
            <h1>Popular Articles</h1>
            
            <ul>
                
                <li><a href="../usability-101-introduction-to-usability/index.php">Usability 101: Introduction to Usability</a></li>
                
                <li><a href="../top-10-mistakes-web-design/index.php">Top 10 Mistakes in Web Design</a></li>
                
                <li><a href="../how-users-read-on-the-web/index.php">How Users Read on the Web</a></li>
                
                <li><a href="../f-shaped-pattern-reading-web-content/index.php">F-Shaped Pattern For Reading Web Content</a></li>
                
                <li><a href="../ten-usability-heuristics/index.php">10 Usability Heuristics for User Interface Design</a></li>
                
                <li><a href="../intranet-design-2016/index.php">10 Best Intranets of 2016</a></li>
                
                <li><a href="../which-ux-research-methods/index.php">When to Use Which User-Experience Research Methods</a></li>
                
                <li><a href="../response-times-3-important-limits/index.php">Response Times: The 3 Important Limits</a></li>
                
                <li><a href="../why-you-only-need-to-test-with-5-users/index.php">Why You Only Need to Test with 5 Users</a></li>
                
                <li><a href="../page-fold-manifesto/index.php">The Fold Manifesto: Why the Page Fold Still Matters</a></li>
                
            </ul>
          </section>
          
          

          
            <section class="hide-for-small-down">
              
            </section>
          

      </section>
      

      
        <div class="small-12 medium-9 columns content-body">
          




<article class="l-subsection alertbox-article-content article-detail-bl">
  
  <header>
    <div class="l-subsection">
      <h1>The Future of Hypertext</h1>
      <div class="author-meta">by
        
          
            
            <a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a>
            
          
        on  February 1, 1995
        
      </div>

      

      
      <div class="topic-links">
        Topics:
        
          <ul>

  <li><a href="../../topic/web-usability/index.php">Web Usability</a></li>

</ul>
        
      </div>
      
    </div>
  </header>
  

  
  
  <section>
    <p class="lede"><strong>Summary:</strong> From intellectual property rights and distribution methods to technical standards, the coming years promise rapid evolution of hypertext and the internet.</p>
    <br />
  </section>
  
  

  
  <section id="article-body"><p><em>This is an excerpt from Jakob Nielsen's book <a href="../../books/multimedia-and-hypertext/index.php"> Multimedia and Hypertext: The Internet and Beyond</a>, AP Professional 1995. (Figures and other images have been left out of this online version.) </em></p>

<h2>What Happened to the Predictions from my Earlier Book?</h2>

<p>My first hypertext book, "<a href="../../books/hypertext-and-hypermedia/index.php">Hypertext and Hypermedia</a>," was published in 1990 so it is appropriate to consider how the predictions in that book compare to reality five years later. In 1990 I predicted that "three to five years later" (that is, at the time of this writing), we would see the emergence of a mass market for hypertext. Indeed, this has more or less come to pass with CD-ROM encyclopedia outselling printed ones and with large numbers of hypermedia productions available for sale in computer shops. This "mass market" is still fairly small, though, compared with the market for printed books, magazines, and newspapers, but it is getting there.</p>

<p>I also commented on the fact that most hypertext products in 1990 ran on the Macintosh and predicted a shift to the IBM PCs as a natural consequence of the mass market trend I discussed. This shift has certainly come to pass and most new hot hypermedia products ship for the PC long before they come out in Mac format (many are never converted).</p>

<p>My second main prediction for 1995 was the integration of hypertext with other computer facilities. This has mostly not come to pass, even though the PenPoint operating system did include hypertext features as a standard part of the system. Most computer systems still have impoverished linking abilities and make do with a few hypertext-like help features like the Macintosh balloon help that allows users to point to an object on the screen and get a pop-up balloon to explain its use.</p>

<p>More generally integrated hypertext features must await the fully object-oriented operating systems that are currently being developed by most leading computer vendors. It turned out to be too difficult to add new elements to the computational infrastructure of existing systems and expect the mass of applications to comply with them. We are currently living through the last years of the Age of Great Applications with hundreds or thousands of features. These mastodons are ill suited to flexible linking and have too much momentum to adjust to fundamentally new basic concepts like universal linking and embedded help. I predict that in another five to ten years a fundamental change will happen in the world of computing and that integrated object-oriented systems will take over. This will happen slowly, though, because the current generation of Great Applications is powerful indeed. It will take some time for the smaller, more agile mammals -- sorry, modules -- to win.</p>

<p>A final prediction failed miserably only a few years after it was made: I predicted that universities would start exchanging Intermedia webs on a regular basis to build up extensive curricula across schools. In fact what happened was that the Intermedia project got its funding canceled and the system does not even run on current computers since it has not been maintained for years. I stand by my original conviction that Intermedia was the best hypertext system available in 1990. It deserved to have succeeded, but short-sighted funding agencies are of course nothing new.</p>

<h2>Short-Term Future: Three to Five Years (1998-2000)</h2>

<p>In the short term of three to five years, I don't really expect significant changes in the way hypertext is done compared to the currently known systems. Of course new stuff will be invented all the time, but just getting the things we already have in the laboratory out into the world will be more than enough. I expect to see three major changes:</p>

<ul>
	<li>the consolidation of the mass market for hypertext</li>
	<li>commercial information services on the Internet</li>
	<li>the integration of hypertext and other computer facilities</li>
</ul>

<h3>Consolidating the Mass Market for Hypertext</h3>

<p>Hypermedia products are already selling fairly well even though it is still only a minority of personal computers that have sufficiently good multimedia capabilities to do them justice. I would expect very few personal computers to be sold in the industrialized world from now on without multimedia capabilities, so the market for hypermedia should get much larger in the next few years.</p>

<p>We already have one product category, encyclopedias, that is selling better in hypermedia form than in paper form. I would expect a few more such product categories to appear, but I would expect most product categories to be dominated by traditional media in the short term. One product category that is a prime candidate for initial multimedia dominance is movie guides. The moving images and the ease of updating make this domain a natural for a combination of CD-ROM and online publishing, and several large studios have indeed started to distribute trailers for their films through America Online and other services. Microsoft Cinemania was one of the first hypermedia movie guides and I would expect many more to appear. TV guides will also move from print to hypermedia for much the same reasons. Furthermore, cable TV services with 500 or more channels are just too much for a printed guide to describe in any kind of usable format. Instead of scanning TV listings, users will have to start using a combination of search methods and agent-based program recommendations [1]. For example, if my TV guide knows that I normally watch Star Trek and that most other Star Trek fans seem to like a certain new show on channel 329, then it might prompt me with a recommendation to watch that show one day.</p>

<p>I also expect the emergence of many more engaging interactive multimedia games and entertainment products as the convergence of Hollywood and Silicon Valley shakes out. Current computer games have a paucity of storytelling talent and production values and thus have to make progress through the story needlessly difficult to draw out the experience enough to make it worth the purchase price for the buyer. With more powerful computers and with more talented writers, directors, and other creative talent, computerized entertainment should become much smoother to navigate and should present richer worlds for the user to explore without being killed every few turns. Truly new entertainment concepts will have to await the next century, though: I don't expect to see really intelligent environments that change the story to fit the individual user until after the year 2000.</p>

<h3>Commercial Hypertext on the Internet</h3>

<p>We will definitely start seeing charging mechanisms on the Internet. In order to get good quality information, people will have to pay something for the necessary authoring and editing efforts. It will only be a matter of a very short time before a NetCash system gets established to allow people to pay for their consumption of information over the Internet.</p>

<h3>Integrating Hypertext with Other Computer Facilities</h3>

<p>The reader will probably have noticed that I am quite enthusiastic about the possibilities of hypertext. Even so, it must be said that many of the better applications of hypertext require additional features to plain hypertext.</p>

<p>We are currently seeing a trend for hypertext systems to be integrated with other advanced types of computer facilities. For example, there are several systems that integrate hypertext with artificial intelligence (AI).</p>

<p>One such system was built by Scott M. Stevens [9] at the Software Engineering Institute at Carnegie Mellon University for teaching the software engineering technique called code inspection. Code inspection basically involves discussing various aspects of a program during a meeting where the participants each have specified roles such as reviewer, moderator, or designer of the program. It turns out that it is impossible to teach people this method without first having them participate in a number of meetings where they are assigned the various roles. This process can be quite expensive if the other meeting participants have to be experienced humans.</p>

<p>It is possible, however, to simulate the other participants on a computer by the use of artificial intelligence. By doing so, the person learning the code inspection method can have as many training meetings as necessary, and the student can even go through the same meeting several times to take on the different roles. The major part of the meeting simulation system is therefore an AI method for finding out how the other meeting participants would react to whatever behavior the student exhibits. But the system also includes a lot of text in the form of the actual program being discussed, its design specifications, and various textbooks and reports on the code inspection method. These texts are linked using hypertext techniques.</p>

<p>Another example of the integration of hypertext with other techniques is the commercial product called <cite> The Election of 1912 </cite> from Eastgate Systems. Whereas the code inspection meeting simulator was primarily an AI system with hypertext thrown in for support, <cite> The Election of 1912 </cite> is primarily a hypertext system but also includes a simulation.</p>

<p>Most of the 1912 system is a hypertext about the political events in the United States in 1912 with special focus on the presidential election of that year. It is possible to read the hypertext in the normal way to learn about this historical period and its people. There is also a political simulation to increase student motivation for reading through the material.</p>

<p>Basically the simulation allows the user to participate in "running" for president in 1912 by "being" the campaign manager for Teddy Roosevelt. The user can plan the travel schedule for the candidate, what people he should meet with, and what issues he should speak out on in which cities. During the simulation, the user can call up a map showing the "result" of opinion polls for each state.</p>

<p>The simulation is hooked into the hypertext system in such a way that the user can jump from the simulated information about a state to the actual historical information about a state to understand why the voters in that state have reacted as they have to the candidate's speeches.</p>

<p>There are also hypertext links from the meetings with various possible supporters and famous people in the simulation to the hypertext's information about these people and from the issues in the simulation to the discussion about these issues in the real election.</p>

<h2>Medium-Term Future: Five to Ten Years (2000-2005)</h2>

<p>Toward the turn of the century we should expect to see widespread publishing of hypertext material. It is also likely that the various forms of video, which are currently quite expensive, will be part of the regular personal computers people have in their homes and offices. So we should expect to see true hypermedia documents for widespread distribution.</p>

<p>Within this timeframe there is also some hope for a solution to the practical problems of data compatibility between systems. Currently it is impossible for a user of, say, an IBM OS/2 system to read a hypertext that has been written on, say, a Unix Sun in KMS. In the world of traditional, linear text, the data compatibility problem has been more or less solved some time ago, with the possibility to transfer word processor and spreadsheet documents between Windows and Macs being just one of the more spectacular success stories.</p>

<p>Current work on the "hypertext abstract machine" (HAM) or similar ideas for interchange formats will almost certainly succeed to the extent that it will be possible to consider publishing the same hypertext on several different platforms. This change will increase the size of the market even more and will therefore add to the trend toward focusing on content. Of course there will still be many hypertexts that will need to take advantage of the special features available on some specific platform like the Macintosh, and these hypertexts will then not be universally available.</p>

<p>The availability of hypertext documents on several platforms is not just a commercial question of the size of the market for the seller. It is also a social issue for the readers of hypertext. Just imagine how you would feel as a reader of traditional printed books if there were some mechanism which restricted you to reading books printed in a specific typeface like Helvetica. But this is exactly the current situation with hypertext: If you own Hyperties, then you can read hypertext documents written in Hyperties but not those written in KMS, HyperCard, Guide, Intermedia, NoteCards, ....</p>

<p>An interchange format will partly resolve this problem. But the problem will be solved only partly because of the special nature of hypertext, which involves dynamic links to other documents. It is not even remotely likely that we will see Ted Nelson's original Xanadu vision of having all the world's literature online in a single unified system fulfilled in the medium-term future. Therefore hypertext documents will get into trouble as soon as they start linking to outside documents: Some readers will have them, and others will not. It is likely that most hypertexts in the medium-term future will remain as isolated islands of information without a high degree of connectedness to other documents. This has been called the <strong> docuislands </strong> scenario.</p>

<p>Even to the extent that many documents are served over the WWW, they may still be isolated from a hypertext perspective since inter-document linking lacks much to be desired. People mostly link between documents on their own server and under their own control since cross-site links are subject to become dangling without any warning as the remote site changes its file structure.</p>

<p>Several of the current practical problems with hypertext documents should be expected to be solved in the medium-term future. For example, there is currently no standard way to refer to a hypertext document the way there is with traditional books, which have a standard ISBN numbering system and recognized publishers with established connections to bookshops. If you want a book, you go to the bookshop and buy it. And if they don't have it, they can get it for you.</p>

<p>Hypertexts are published by a strange assortment of companies to the extent that they are published at all. Many a good hypertext is available only from the individual or university who produced it. And the sales channels for hypertexts are either nonexistent or a mixture of mail order and computer shops, many of which do not carry hypertext.</p>

<p>In a similar way, regular libraries almost never include hypertexts in their collections and wouldn't know how to deal with electronic documents anyway. There are a few online services that store a small number of electronic documents, but since there is no systematic way to determine where a given document is stored and since most hypertext documents are not available online anyway, these services cannot substitute for traditional libraries.</p>

<p>It is very likely, however, that the medium-term future will see established publishers and sales channels for hypertext documents. Whether they will be the same as the current system for printed books remains to be seen and would depend on the level of conservatism in the management of publishers and major booksellers. Libraries will certainly have to handle electronic documents, and many of the better and larger libraries are already now starting to invite hypertext specialists to consult on ways to deal with these issues.</p>

<h3>Intellectual Property Rights</h3>

<p>I expect major changes in the way intellectual property rights (e.g., copyright) are handled. These changes really ought to happen within my short-term time frame since they are sorely needed, but realistically speaking, changes in the legal system always trail technological changes, so I don't expect major changes until the year 2000.</p>

<p>There are two major problems with the current approach to copyright: first, "information wants to be free" as the hackers' credo goes, and second, the administration of permissions and royalties adds significant overhead to the efforts of people who work within the rules. Under current law, hypermedia authors are not allowed to include information produced by others without explicit permission. Getting this permission is estimated to cost about $220 on the average in purely administrative costs such as tracking down the copyright owner, writing letters back and forth, and possibly mailing a check with the royalty payment. If you are negotiating the rights to the large-scale reproduction of an entire book or all the paintings in a museum then $220 for administration is negligible, but that is not so if you want to assemble a large number of smaller information objects.</p>

<p>To overcome the problem with administrative overhead, it is possible that information objects will start to include simplified mechanisms for royalty payment. Each object could include attributes indicating the copyright owner, the owner's NetCash account number, and the requested royalty for various uses of the material. This approach would be similar to the Copyright Clearance Center codes often found printed in each of the articles in a research journal. With the current system, every time somebody copies an article from one of the journals associated with the Copyright Clearance Center, they (or their library management) can send the Center the article code and a check for the $2.00 or so that are the requested royalty for that article. The Copyright Clearance Center codes require manual interpretation and would not be suited for information objects costing a cent or less per copy, but an object-oriented data representation would allow computers to process the payments automatically. If each data object came with payment information, the holders of intellectual property rights should not mind wanton distribution of copies of their materials since they would get paid every time somebody actually used the information.</p>

<p>The more basic problem, though, is that "information wants to be free." Once you know something, it is very hard to pretend that you don't. Thus, it is difficult to charge people every time they use some information. Also, information does not get worn or diminished by being copied. This is in contrast to traditional goods like a cake: if I let you have a bite of my cake, there is less left for me to eat, but if I let you look at the front page of my newspaper, I will not suffer any adverse effects.</p>

<p>It is possible to establish barriers to information dissemination, either by legal and moral pressure ("don't copy that floppy") or by actual copy protection. Users tend to resent such barriers and have been known to boycott vendors who used particularly onerous copy protection mechanisms. An alternative would be to eliminate copyright protection of the information itself and let information providers be compensated in other ways. It is already the case that software vendors generate most of their revenues from upgrades and not from the original sales of the software, so it might be feasible to give away the software and have the vendors live off upgrades and service. Moving software from a sales model to a subscription model (possibly with weekly or daily upgrades) would make it similar in nature to magazines and newspapers where people pay to get the latest version delivered as soon as possible.</p>

<p>It is also possible for intellectual property owners to get compensated, not from the information itself, but from a derived value associated with some other phenomenon that is harder or impossible to copy. This is the way university professors currently get compensated for their research results. They normally give away the papers and reports describing the results and get paid in "units of fame" that determine their chance of getting tenure, promotions, and jobs at steadily more prestigious institutions. Fame may also provide a reasonable compensation model in many other cases. For example, people visit the Louvre to see the real Mona Lisa even though they already have seen countless reproductions of the image. In fact, the more the Mona Lisa is reproduced, the more famous it gets, and the more people will want to visit Paris and go to the Louvre. Therefore, the Louvre should welcome hypermedia authors who want to use Mona Lisa bitmaps and should allow them to do so for free. Similarly, a rock band might make its money from tours and concerts and give away its CDs (or at least stop worrying whether people duplicate the CD with DAT recorders), and a professional society could get funded by conference fees instead of journal sales. The trick is to use the information (e.g., music CDs and tapes) to increase the value of the physical or unique event for which it is possible to charge.</p>

<h2>Long-Term Future: Ten to Twenty Years: 2005-2015</h2>

<p>What will happen in the really long-term future, more than twenty years from now? That is for the science fiction authors to tell. In the computer business, ten to twenty years counts as long term future indeed, so I will restrict myself to that horizon in the following comments.</p>

<p>Some people like Ted Nelson expect to see the appearance of the global hypertext (e.g. Xanadu) as what has been called the <strong> docuverse </strong> (universe of documents). I don't really expect this to happen completely, but we will very likely see the emergence of very large hypertexts and shared information spaces at universities and certain larger companies.</p>

<p>Already we are seeing small shared information spaces in teaching applications, but they are restricted to the students taking a single class. In the future we might expect students at large numbers of universities to be connected together. Another example of a shared information space is a project to connect case workers at branch offices of a certain large organization with specialists at the main office. The staff at the branch office does not follow the detailed legal developments in the domain that is to be supported by the system since they also have other responsibilities. Therefore the specialists would maintain a hypertext structure of laws, court decisions, commentaries, etc., which would be made available to the case workers at the branches. Also, these local people would describe any cases falling within this domain in the system itself with pointers to the relevant hypertext nodes. In this way they would in turn help the specialists build an understanding of practice and they would also be able to get more pointed help from the specialists regarding, for instance, any misapplication of legal theory.</p>

<p>So given these examples which are already being implemented, I would certainly expect to see the growth of shared information spaces in future hypertext systems. There are several social problems inherent in such shared spaces, however. If thousands or even millions of people add information to a hypertext, then it is likely that some of the links will be "perverted" and not be useful for other readers. As a simple example, think of somebody who has inserted a link from every occurrence of the term "Federal Reserve Bank" to a picture of Uncle Scrooge's money bin. You may think that it is funny to see that cartoon come up the first time you click on the Bank's name, but pretty soon you would be discouraged from the free browsing that is the heartblood of hypertext because the frequency of actually getting to some useful information would be too low due to the many spurious links.</p>

<p>These perverted links might have been inserted simply as jokes or by actual vandals. In any case, the "structure" of the resulting hypertext would end up being what Jef Raskin has compared to New York City subway cars painted over by graffiti in multiple uncoordinated layers.</p>

<p>Even without malicious tampering with the hypertext, just the fact that so many people add to it will cause it to overflow with junk information. Of course the problem is that something I consider to be junk may be considered to be art or a profound commentary by you, so it might not be possible to delete just the "junk."</p>

<p>A likely development to reduce these problems will be the establishment of hypertext "journals" consisting of "official" nodes and links that have been recommended by some trusted set of editors. This approach is of course exactly the way paper publishing has always been structured.</p>

<p>An example of such a development can be seen in the way many users have given up following the flow of postings to the Usenet newsgroups. Instead of reading the News themselves, many people rely on friends to send them interesting articles or they subscribe to electronic magazines [3], which are put out by human editors who read through the mass of messages to find those of interest for whatever is the topic of their magazine. [See also my later report on the <a href="../../reports/newsletters/index.php"> usability of email newsletters</a>.]</p>

<p>It would also be possible to utilize the hypertext mechanism itself to build up records of reader "votes" on the relevance of the individual hypertext nodes and links. Every time users followed a hypertext link, they would indicate whether it led them somewhere relevant in relation to their point of departure. The averages of these votes could then be used as a filter by future readers to determine whether it would be worthwhile to follow the link.</p>

<p>Another potential social problem is the long-term effects of nonsequentiality. Large parts of our culture have the built-in assumption that people read things in a linear order and that it makes sense to ask people to read "from page 100 to 150." For example, that is how one gives reading assignments to students. With a hypertext-based syllabus students may show up at exam time and complain about one of the questions because they never found any information about it in the assigned reading. The professor may claim that there was a hypertext link to the information in question, but the students may be justified in their counter-claim that the link was almost invisible and not likely to be found by a person who was not already an expert in the subject matter.</p>

<p>The reverse problem would occur when the professor was grading the students' essays, which would of course be written in hypertext form. What happens if the professor misses the single hypertext link leading to the part of the paper with all the goodies and then fails the student? Actually the solution to this problem would be to consider hypertext design part of the skills being tested in the assignment. If students cannot build information structures that clearly put forward their position, then they deserve to fail even though the information may be in there in some obscure way.</p>

<p>A further problem in the learning process is that novices do not know in which order they need to read the material or how much they should read. They don't know what they don't know. Therefore learners might be sidetracked into some obscure corner of the information space instead of covering the important basic information. To avoid this problem, it might be necessary to include an AI monitoring system that could nudge the student in the right direction at times.</p>

<p>There is also the potential problem that the non-linear structure of hypertext as being split into multitudes of small distinct nodes could have the long-term effect of giving people a fragmented world view. We simply do not know about this one, and we probably will not know until it is too late if there is such an effect from, say, twenty years of reading hypertext. On the other hand, it could just as well be true that the cross-linking inherent in hypertext encourages people to see the connections among different aspects of the world so that they will get less fragmented knowledge. Experience with the use of Intermedia to teach English literature at Brown University [2] and Perseus to teach Classics [4] did indicate that students were several times as active in class after the introduction of hypertext. They were able to discover new connections and raise new questions.</p>

<h3>Attribute-Rich Objects</h3>

<p>Most current hypermedia systems are based on the notion that each information object has one canonical representation, meaning that a given node should always look the same. The WYSIWYG (what you see is what you get) principle has actually been fundamental for almost all modern user interfaces, whether for hypertext systems or for traditional computer systems. There is something nice, safe, and simple about this single-view model, and it is certainly better than the previous model where all information on the computer screen was presented in monospaced green characters no matter how the document would look when printed.</p>

<p>The single-view model is best suited for information objects that have an impoverished internal representation in the computer because the computer will have no way to compute alternative views and thus has to faithfully reproduce the layout given by the human user. It may take more than ten years to get to the next model, but eventually I expect the computer to process information objects with significantly more attributes than it currently has. Once the computer knows more about each information object, it will be able to display them in various ways depending on the user's specific needs for the user's current task.</p>

<p>One example of a multi-view hypertext is the SIROG (situation-related operational guidance) project at Siemens [8]. SIROG contains all parts of the operational manual for accidents in a Siemens nuclear power plant and is connected to the process control system and its model of the state of the plant. The hypertext objects have been manually encoded by the authors with SGML attributes that inform SIROG about the part of the plant and the kind of situation they describe. The system can match these attributes with the actual situation of the plant and present the relevant parts of the manual to the operators. Obviously, unexpected situations can occur and the system may not have a perfect understanding of the plant state. Therefore, SIROG relies on the operators' abilities to read and understand the content of the manual: it is not an intelligent system to diagnose nuclear power plant problems; it is a system that tries to display the most relevant part of the manual first rather than having the manual always look the same every time the operators bring it up.</p>

<p>Program code is another example where multiple views are both relevant and Possible [FN 1] [6]. Sometimes a programmer wants to see only the executable code, sometimes a commented view is better, sometimes an overview of the program's structure is most helpful, and sometimes a view annotated with debugging or performance metering will be needed. For third-party programmers who want to reuse a program through its API simplified views of the interface structure will suffice (and indeed, a view of the internal structure would be harmful).</p>

<p>The CastingNet system [5] uses a frame-based model with highly structured nodes. Each node is a frame with a number of named slots that represent its properties and the type of the node determines what frame will be used to represent its content. Each slot has a mapping function that maps each possible value of that slot onto an axis. In one CastingNet example, hypertext nodes that represent research conferences may have slots for the country in which the conference is held, the date of the conference, some classification of the topic of the conference, and many more attributes of conferences and conference announcements.</p>

<p>The conference-venue slot would map onto a country axis that could be represented as a linear list, as a world map, or in many other ways. The attribute axes can be used to construct many different two- or three-dimensional overview diagrams of the hypertext according to criteria that are of interest to the user. The frame-based representation also allows the system to construct links between nodes that are that have identical or closely related values along one or more axes. For example, it would be possible to link from a given conference node to nodes representing other conferences taking place in the same location at the same time (or the following or previous week) as a conference a user was thinking of going to. Also, due to the mapping functions, it will be possible to link to nodes that have different, but compatible, slots. For example, instead of linking to other conference nodes with similar venue slots, it would be possible to link to the user's personal information base of friends and contacts to see what people lived in the same town and might be visited in connection with a conference trip. This link would be possible because the address nodes for each friend would have slots for city and ZIP code that could be mapped to the same axis as the venue slot in the conference frame.</p>

<p>The frame-axis model of CastingNet provides flexible ways for the user to restructure the views of the hypertext and to link the information in new ways as new needs are discovered. This type of flexibility will be very important in the future as people start accumulating large personal information bases, but it will only be possible if the computer has ways of representing the same information in multiple ways depending on the interpretations that are currently of most value to the user.</p>

<h2>Hypermedia Publishing: Monopoly or Anarchy?</h2>

<p>Assume that in fact most information will migrate online and that it will be published in hypertext form with substantial multimedia components. What will this change imply for the publishing industry and the dissemination of information in the future?</p>

<p>Two opposite trends might be possible: either information publishing will be concentrated in a few, near-monopolistic companies, or the ability to publish information will be distributed over many more companies than are now involved. Both trends actually seem plausible due to different characteristics of hypermedia publishing.</p>

<p>Information publishing currently involves five different stages:</p>

<ol>
	<li>Creating the content</li>
	<li>Editing the content, including the selection of the authors</li>
	<li>Manufacturing copies of the resulting product</li>
	<li>Distributing copies of the product through the sales channels</li>
	<li>Advertising and promoting the product</li>
</ol>

<p>The middle stage, manufacturing physical copies of the information product, will more or less go away for hypermedia publishing because the users will be creating their own copies on demand as they download information from the Internet. Even for non-Internet hypermedia products, the physical duplication of CD-ROMs or floppy disks will account for an increasingly small part of the overall cost of the information products. Currently, the cost of printing and binding books (or newspapers or other information products) is one of the factors that limit the number of publishers of information products. The widespread availability of desktop publishing and photocopies have changed the picture somewhat and has made it economically feasible for people outside the established publishing houses to produce information products in small numbers.</p>

<p>Even though anybody can print, say, 100 copies of a booklet, the last two stages of the publishing process, distribution and promotion, have limited the actual sale and utilization of small-scale information products with pre-hypertext technology. What do you do after you receive the 100 copies of your booklet back from the QuickPrint shop? To get any sales you would have to get bookstores to carry it, newspapers to review it, and you would need a way to ship a copy to the one resident of New Zealand who might happen to be one of your customers.</p>

<p>Production, distribution, and promotion limitations have caused the publication of paper-based information products to be concentrated in the hands of a number of publishers that is much smaller than the number of potential authors or providers of information. At the same time, the barriers to setting up a new publishing company are fairly low and there is in fact a large number of publishers in business all over the world, some of which have very small market shares but are still profitable.</p>

<p>With the move to distributing information products over the Internet, distribution will go away as a limiting factor for information publishing, supporting a trend towards having more publishers. Essentially everybody with a workstation and an Internet subscription can become a publishing house and sell information products over the net. Indeed, with the WWW, the trend so far has been for a large number of people to set up their own individual "publishing houses" in the form of their home pages and associated hyperlinked material. Promotion and advertising will still tend to favor larger publishers with more resources, but the most effective type of promotion for Internet hypertext will be having others link to your material, and an author can potentially be linked to by many others without being affiliated with a major publisher.</p>

<p>The trends discussed so far in this section would seem to favor a kind of information anarchy where many more authors get published than is currently the case and where the publishing gets done by a larger number of companies. There are several opposing trends, though. The first is the information overload that is already taking its toll on people's ability to check out new WWW sites. With more and more information placed on the Internet, a backlash might cause many users to stay within safe Web sites that are known to offer high-quality material. As a parallel consider the way many people get highly aggravated by junk mail and deliberately try to avoid it.</p>

<p>Information monopolies are encouraged by three phenomena: production values, fame, and critical mass. With the move from simple text-based hypertext to multimedia-oriented hypermedia production values become steadily more important. For a text-only hypertext product, an individual author can craft a good product, but a full-scale multimedia product with video and animation takes a crew of graphic artists, set designers, actors, make-up specialists, camera-people, a director, etc. It may still be possible for a small company like Knowledge Adventure to produce a better dinosaur CD-ROM than Microsoft [FN 2] by including 3-D dinosaur attack animations, but material produced by the average dinosaur enthusiast pale in comparison with either CD-ROM. Professionally produced multimedia titles with good production values look so much better than amateur productions but they require much higher capitalization and thus can only be produced by a fairly small number of companies. We are already seeing a trend to higher production values on the WWW with the major computer companies hiring specialized production staff and graphic artists to dress up their home pages and smaller companies with boring pages (or worse, ugly graphics designed by a programmer) will lose the production-value battle for attention.</p>

<p>Fame is the second factor that tends to support monopoly in information products. By definition, fame favors a few people or companies that are particularly well known, and the more people read or hear about someone famous, the more famous that person becomes. Consider two hypothetical new film releases, one starring Arnold Schwarzenegger and one with another hunk with equal acting talent whom nobody has heard of before. Which film will attract the largest audience? Why, Arnold's, of course. Thus, after these two films have played the world, Schwarzenegger will be more famous than before (since many people will have seen him) and the unknown hunk will remain unknown.[FN 3]</p>

<p>Even though production values and fame both tend to favor a small number of large hypermedia producers, the third factor, critical mass, may be the killer monopoly enhancer. Hypertext products derive significant added value from links and add-on information. Consider again the example of dinosaurs. Microsoft Encarta (a hypertext encyclopedia) could have hypertext links to Microsoft Dinosaurs, thus simultaneously increasing sales of Microsoft Dinosaurs and adding value to Encarta. Of course, a third-party hypertext encyclopedia could have a link to 3-D Dinosaur Adventure but depending on the way distribution and royalty mechanisms play out, there may not be much synergy to doing so. Also, independent software vendors (ISVs) may produce add-on modules to widely sold products like Encarta to provide special effects such as an animated physics simulation to better explain, say, gravity. All experience from the PC market shows that ISVs only port software to the most popular platforms, so the availability of add-on modules will tend to favor hypermedia titles that have already established critical mass with respect to market share.</p>

<p>It is currently impossible to predict which of the two opposing trends to anarchy and to monopoly will prove the strongest. It is possible that a third way will emerge where the hypermedia market is dominated by a small number of near-monopoly players who set standards and mainline directions while a much larger number of garage-shop publishers add variety and have the ability to explore new options faster than the giants.</p>

<p>It is possible that a new phenomenon will emerge in the form of "temporary monopolies" that dominate for a short period only to be replaced virtually overnight. The Internet seems to encourage temporary monopolies because of its ability to instantly distribute software to millions of users who all are part of the same discussion groups and rumor mills. Consider two examples: the Netscape browser for the WWW increased its market share from 0.1% to 64% during the four-month period from August to December 1994 (and the previous market leader, Mosaic, dropped from 73% to 21% in the same period), and usage of the Lycos search server increased at an annualized rate of 130 million percent during the fall of 1994.</p>

<h3>Creative Centers</h3>

<p>For printed materials, there has not traditionally been a single city or geographic area that dominated world market. Books and newspapers are published throughout the world and it is not particularly advantageous for an author to move to a specific location in order to be published. Many countries have a concentration of large book publishers and newspapers in their capitals but traditionally most provincial towns have had their own newspaper(s). In the U.S., New York City serves as the closest approximation to a creative center for word production. Two of the three national newspapers in the U.S., <cite> The New York Times </cite> and the <cite> Wall Street Journal </cite> , are published in New York (the third, <cite> USA Today </cite> , is published in Arlington, Virginia). However, the combined circulation of the three national newspapers is only 4.5 million copies which is 7% of the circulation of all newspapers in the U.S. In other words, 93% of newspaper circulation in the U.S. is due to local papers.[FN 4]</p>

<p>In contrast to the print industry, the film and computer industries have had a disproportionate part of their most important companies located in a very small number of geographic areas. The film industry is concentrated in the Los Angeles area not just with respect to the U.S. market, but with respect to the world market: 80% of the box office receipts in Europe go to American films [FN 5] and all major top-grossing blockbuster releases [FN 6] for the last several years have been American.[FN 7] The computer industry is not quite as concentrated as the film industry,[FN 8] but the Silicon Valley area between San Francisco and San Jose still serves as the main creative center for this industry with a very large number of established and start-up companies. In order to benefit from the availability of skilled staff, suppliers, customers, and other partners in the creative center, 20% of the largest 50 software companies that were founded in Europe have since moved their headquarters to the U.S.[FN 9]</p>

<p>The existence of internationally dominant creative centers for the film and computer industry serves to motivate high-caliber personnel to relocate to those areas from all other parts of the world, leading to further centralization of activity. By concentrating so many people who work in the same field, creative centers ensure an extraordinary degree of cross-fertilization which again leads to rapid refinement and dissemination of ideas [7]. Technology transfer by warm bodies is known to be much more effective than technology transfer by technical reports, and the extensive job hopping that takes place in a creative center therefore benefits all the companies in the area by increasing their exposure to new ideas and techniques.</p>

<p>It is currently too early to predict whether the multimedia and hypermedia industry will be dominated by creative centers in the way the film and computer industries are, or whether it will follow the less centralized precedent set by the publishing industry. In principle, the Internet allows people to work together on a multimedia project without the need for them to live in the same area, but in practice intensely creative work, brainstorming, design, and the informal contacts that lead to most business still require people to be colocated in the same physically proximate reality (PPR). Virtual reality and cyberspace are not the same as dinner at Il Fornaio.</p>

<p>Since the two main components of the multimedia industry are the computers and moviemaking, the most likely candidates for a creative center for multimedia are Silicon Valley and Hollywood. Whether the resulting "Siliwood" will form around San Francisco's "multimedia gulch" or around the Hollywood studios is currently the topic of much debate, but there is no doubt that California is a very likely home for one or more creative centers for multimedia. Of course, if the monopoly scenario outlined above turns out to happen, there will be definite advantages to be located close to Microsoft headquarters in Redmond, WA, and the Seattle area is already attracting a large number of highly creative multimedia companies.</p>

<h2>References</h2>

<p>[1] Isbister, K., and Layton, T. (1995). Agents: What (or who) are they? In Nielsen, J. (Ed.), Advances in Human-Computer Interaction vol. 5. Ablex.</p>

<p>[2] Landow, G. P. (1989). Hypertext in literary education, criticism, and scholarship. Computers and the Humanities 23, 173-198.</p>

<p>[3] Maltz, D., and Ehrlich, K. (1995). Pointing the way: Active collaborative filtering. Proc. ACM CHI'95 Conf.</p>

<p>[4] Marchionini, G., and Crane, G. (1994). Evaluating hypermedia and learning: Methods and results from the Perseus Project. ACM Trans. Information Systems 12, 1 (January), 5-34.</p>

<p>[5] Masuda, Y., Ishitoba, Y., and Ueda, M. (1994). Frame-axis model for automatic information organizing and spatial navigation. Proc. ACM ECHT'94 European Conference on Hypermedia Technology (Edinburgh, U.K., September 18-23), 146-157.</p>

<p>[6] Oesterbye, K., and Noermark, K. (1994). An interaction engine for rich hypertext. Proc. ACM ECHT'94 European Conference on Hypermedia Technology (Edinburgh, U.K., September 18-23), 167-176.</p>

<p>[7] Saxenian, A. (1994). Regional Advantage: Cultural and Competition in Silicon Valley and Route 128. Harvard University Press.</p>

<p>[8] Simon, L., and Erdmann, J. (1994). SIROG - A responsive hypertext manual. Proc. ACM ECHT'94 European Conference on Hypermedia Technology (Edinburgh, U.K., September 18-23), 108-116.</p>

<p>[9] Stevens, S. M. (1989). Intelligent interactive video simulation of a code inspection. Communications of the ACM 32, 7 (July), 832-843.</p>

<h2>Footnotes</h2>

<p>[FN 1] Multi-view representations of program code are particularly easy to produce because so many software development tools exist to parse the code and interpret it in various ways. For other kinds of information objects we still need better ways of letting the computer manipulate structure and meaning.</p>

<p>[FN 2] To be fair, I should mention that I don't know for sure whether Knowledge Adventure's 3-D Dinosaur Adventure is better than Microsoft Dinosaurs. I just know that 3-D Dinosaur Adventure got the best review of the two in a magazine I was reading, and since I thought that one dino CD would be enough, I only bought 3-D Dinosaur Adventure. This proves my main point, though: when there are too many dinosaur hypermedia titles available, people tend to concentrate on a few published by reputable sources (Microsoft) or that have particularly good production values (3-D animations). I do not have time to fully research the market for dino CDs; I just read reviews of the two that seemed the best bets.</p>

<p>[FN 3] The movie star example is not entirely irrelevant to a discussion of hypermedia markets. Compton's Interactive Encyclopedia is using Patrick Stewart (British actor famous for his role as the captain in Star Trek-The Next Generation) as a narrator in the hope that some of his star quality will rub off on the hypermedia product.</p>

<p>[FN 4] The influence of New York as a creative center for the print industry is larger than the market share of its newspapers indicates: The New York Times and the Wall Street Journal are read disproportionally by influential decision makers and opinion leaders and the city publishes magazines accounting for nearly half of the magazine industry's national revenues.</p>

<p>[FN 5 For comparison, European films only have a 1% market share of the U.S. film market.</p>

<p>[FN 6 All films grossing more than $100 million outside U.S. in 1993 were American: Jurassic Park ($530M), The Bodyguard ($248M), Aladdin ($185M), The Fugitive ($170M), Indecent Proposal ($152M), Cliffhanger ($139M), Bram Stoker's Dracula ($107M), and Home Alone 2 ($106M). The only non-U.S. film to come close was Les Visiteurs, which scored a $90M non-U.S. box office. But since Les Visiteurs had zero dollars in U.S. sales in 1993, it ended as number 27 on the list of world-wide box office receipts-all the top 26 films were American.</p>

<p>[FN 7] For the week before Christmas in 1994, only 26 of the 110 films on the top-10 lists in Belgium, Brazil, Denmark, France, Italy, The Netherlands, Norway, South Africa, Spain, Sweden, and Switzerland were non-American according to Variety magazine. Disney's The Lion King was number one in seven of the eleven countries and number two in two more ( <cite> Variety </cite> article "Lion king over Euro holiday B.O.," January 8, 1995, p. 18).</p>

<p>[FN 8] The top-five companies only account for 33% of the software market, whereas the top-five film companies have an 81% market share.</p>

<p>[FN 9] Classified by the location of their headquarters, the distribution of the 30 software companies with the largest sales in Europe is as follows: U.S. 19, France 5, Germany 4, Italy 1, U.K. 1.</p>
</section>
  

  
  <section>
    
        
    
  </section>
  
  
  
  
  

  


  <section>
    <br />
    <p class="share"><b>Share <span class="hide-for-small-down">this article</span>:</b> <a href="https://twitter.com/intent/tweet?url=http://www.nngroup.com/articles/hypertext-future/&amp;text=The%20Future%20of%20Hypertext&amp;via=nngroup">Twitter</a> | <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=http://www.nngroup.com/articles/hypertext-future/&amp;title=The%20Future%20of%20Hypertext&amp;source=Nielsen%20Norman%20Group">LinkedIn</a> | <a href="https://plus.google.com/share?url=http://www.nngroup.com/articles/hypertext-future/">Google+</a> | <a href="mailto:?subject=NN/g Article: The Future of Hypertext&amp;body=http://www.nngroup.com/articles/hypertext-future/">Email</a></p>
    <hr />
  </section>

  <footer class="l-subsection">
    
    
    <div class="learn-more">
      <h2>Learn More</h2>
      <p><a href="../subscribe/index.php">Subscribe to the weekly newsletter</a> to get notified about future articles.</p>
      </div>
    
    

    <div class="row show-for-small-down related-content">
      
      
      

      
      
        <div class="column small-12 topic-links">All articles about:
          <ul>
            
            <li><a href="../../topic/technology/index.php">new technologies</a></li>
            
            <li><a href="../../topic/web-usability/index.php">Web Usability</a></li>
            
          </ul>
        </div>
      
      

      
      
        <div class="column small-12">
          <h3>UX Conference Training Courses</h3>
           <ul class="no-bullet">
    
        <li><a href="../../courses/ux-basic-training/index.php">UX Basic Training</a></li>
    
        <li><a href="../../courses/human-mind/index.php">The Human Mind and Usability</a></li>
    
        <li><a href="../../courses/emerging-patterns-web-design/index.php">Emerging Patterns for Web Design</a></li>
    
        <li><a href="../../courses/web-page-design/index.php">Web Page UX Design</a></li>
    
</ul>
        </div>
      
      

      
      
        <div class="small-12 column">
          <h3>Research Reports</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../reports/b2b-websites-usability/index.php">B2B Website Usability for Converting Users into Leads and Customers</a></li>
              
            
              
                <li><a href="../../reports/strategic-design-faqs/index.php">Strategic Design for Frequently Asked Questions</a></li>
              
            
          </ul>
        </div>
      
      

      
      
        <div class="small-12 column">
          <h3>Online Seminars</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../online-seminars/fundamental-flaw-flat-design/index.php">The Fundamental Flaw in Flat Design</a></li>
              
            
              
                <li><a href="../../online-seminars/millennials-online/index.php">Millennials Online</a></li>
              
            
              
                <li><a href="../../online-seminars/beyond-screens/index.php">Beyond Screens</a></li>
              
            
              
                <li><a href="../../online-seminars/university-websites/index.php">University Website UX Essentials</a></li>
              
            
          </ul>
        </div>
      
      
    </div>

    <div class="hide-for-small-down row related-content">
      <div class="medium-6 columns">
        
        
          <div>
            <h3>Articles</h3>
            <ul class="no-bullet">
              
                
                <li><a href="../smart-tv-usability/index.php">Smart-TV Usability: Accessing Content is Key </a></li>
                
              
                
                <li><a href="../computer-screens-getting-bigger/index.php">Computer Screens Getting Bigger</a></li>
                
              
                
                <li><a href="../let-users-control-font-size/index.php">Let Users Control Font Size</a></li>
                
              
                
                <li><a href="../flash-99-percent-bad/index.php">Flash: 99% Bad</a></li>
                
              
                
                <li><a href="../finally-progress-in-internet-client-design/index.php">Finally Progress in Internet Client Design</a></li>
                
              
            </ul>
          </div>
        
        

        
          
          <div>
            <h3>Research Reports</h3>
            <ul class="no-bullet">
              
                
                  <li><a href="../../reports/b2b-websites-usability/index.php">B2B Website Usability for Converting Users into Leads and Customers</a></li>
                
              
                
                  <li><a href="../../reports/strategic-design-faqs/index.php">Strategic Design for Frequently Asked Questions</a></li>
                
              
            </ul>
          </div>
          
        

      </div>
      <div class="medium-6 columns">
        
          
          <div>
            <h3>UX Conference Training Courses</h3>
             <ul class="no-bullet">
    
        <li><a href="../../courses/ux-basic-training/index.php">UX Basic Training</a></li>
    
        <li><a href="../../courses/human-mind/index.php">The Human Mind and Usability</a></li>
    
        <li><a href="../../courses/emerging-patterns-web-design/index.php">Emerging Patterns for Web Design</a></li>
    
        <li><a href="../../courses/web-page-design/index.php">Web Page UX Design</a></li>
    
</ul>
          </div>
          
        
        
          
          <div>
            <h3>Online Seminars</h3>
            <ul class="no-bullet">
            
                <li><a href="../../online-seminars/fundamental-flaw-flat-design/index.php">The Fundamental Flaw in Flat Design</a></li>
            
                <li><a href="../../online-seminars/millennials-online/index.php">Millennials Online</a></li>
            
                <li><a href="../../online-seminars/beyond-screens/index.php">Beyond Screens</a></li>
            
                <li><a href="../../online-seminars/university-websites/index.php">University Website UX Essentials</a></li>
            
            </ul>
          </div>
          
        
      </div>
    </div>
  </footer>
</article>

        </div>

      

    
  </div>
  <div class="row">
    
      <div class="column small-12 show-for-small-down l-navrail announcements">
        
          
        
      </div>
    
    
  </div>

    </div>
    




<footer id="page-footer">
    <div class="row hide-for-print">
        <div class="column small-12 medium-6">
            <section class="footer-popular">
                
                <h1>Popular Topics</h1>
                <div style="float: left;  width: 15em; display: block;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/applications/index.php">Application Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/e-commerce/index.php">E-Commerce Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/eyetracking/index.php">Eyetracking </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/information-architecture/index.php">Information Architecture </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/intranets/index.php">Intranet Design </a></li>
	<li><a href="../../topic/interaction-design/index.php">Interaction Design </a></li>
</ul>
</div>

<div style="float: left;  width: 15em; display: block; list-style: none;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/mobile-and-tablet-design/index.php">Mobile and Tablet Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/credibility-persuasion/index.php">Persuasive Design</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/research-methods/index.php">Research Methods</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/user-testing/index.php">User Testing </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/web-usability/index.php">Web Usability </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/writing-web/index.php">Web Writing &amp; Content Strategy </a></li>
</ul>
</div>

<p>&nbsp;</p>

                
            </section>
        </div>

        <div class="column small-12 medium-6">
            <div class="row">
                <div class="column small-6">
                    <section class="footer-about">
                        <h1>Follow Us</h1>
                        <ul>
	<li><a href="../subscribe/index.php">Subscribe to our weekly UX newsletter</a>,&nbsp;Jakob Nielsen&#39;s Alertbox</li>
	<li>Visit Don Norman&#39;s website, <a href="http://www.jnd.org/"> www.jnd.org </a></li>
	<li>Visit Bruce Tognazzini&#39;s website, <a href="http://www.asktog.com/"> www.asktog.com </a></li>
	<li style="padding-top: 6px;"><a class="twitter-follow-button" data-show-count="false" data-size="large" href="https://twitter.com/nngroup"><img alt="Follow @nngroup on Twitter" height="20" src="https://media.nngroup.com/media/editor/2016/12/30/twitter-follow-button.png" style="width: 117px; height: 20px;" width="117" /></a></li>
	<li style="padding-top: 6px;"><img alt="" height="14" src="https://media.nngroup.com/media/editor/2016/07/26/feed-icon-14x14.png" style="vertical-align:text-top;width:14px;margin-right:.25em;" width="14" /><a href="../../feed/rss/index.php">Subscribe to our RSS feed</a></li>
</ul>

                    </section>
                </div>

                <div class="column small-6">
                    <section class="footer-about right-column">
                        <h1>About
</h1>
                        <ul class="no-bullet">
	<li><a href="../../about/contact/index.php">Contact information</a></li>
	<li><a href="../../about/index.php">About Us</a></li>
	<li><a href="../../about/why-nng/index.php">Why NN/g? </a></li>
	<li><a href="../../return-policy/index.php">Return Policy</a></li>
</ul>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="copyright column small-12">
            <p><a href="../../copyright-and-reprint-info/index.php">Copyright</a> &copy; 1998-2017 Nielsen Norman Group, All Rights Reserved.</p>
        </div>
    </div>
</footer>


    


        <script src="https://media.nngroup.com/static/vendor/fastclick/js/fastclick.min.js"></script>
        <script src="https://media.nngroup.com/static/vendor/foundation/js/foundation.min.js"></script>
        <script src="http://media.nngroup.com/static/js/app.js"></script>
        <script src="http://media.nngroup.com/static/js/scroll-to-fixed.js"></script><script>var externalLinks = document.querySelectorAll('a[href^="http"]');for (var i = externalLinks.length-1; i >= 0; i--) {externalLinks[i].addEventListener("click", function(event) { alert("External links are disabled for this test."); event.preventDefault(); }, false);}var mailLinks = document.querySelectorAll('a[href^="mailto"]');for (var i = mailLinks.length-1; i >= 0; i--) {mailLinks[i].addEventListener("click", function(event) { alert("Email links are disabled for this test."); event.preventDefault(); }, false);}var externalForms = document.querySelectorAll('form');for (var i = externalForms.length-1; i >= 0; i--) {externalForms[i].addEventListener("submit", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false); externalForms[i].addEventListener("click", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false);}</script>

        <script>
            
            $(function() {
                $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
                $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
            });
        </script>

        
<script src="https://media.nngroup.com/static/js/articles.js"></script>


        
        <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1072699960;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072699960/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
    </body>

<!-- Mirrored from www.nngroup.com/articles/hypertext-future/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:13:21 GMT -->
</html>
