<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->





    
<!-- Mirrored from www.nngroup.com/articles/intranet-design-2015/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:07:37 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1026.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"licenseKey":"1a30ef9931","errorBeacon":"bam.nr-data.net","applicationID":"2048992","transactionName":"NFxSMRBUX0EDUkIPDA0WdhAMVkVbDV8ZBxEXUFMJB0YfRAtUQRVZIktEDAFZVHYHRVcPD01eVRE=","beacon":"bam.nr-data.net","queueTime":5,"applicationTime":411,"agent":""}</script>
        <title>10 Best Intranets of 2015, Nielsen Norman Group&#39;s Intranet Design Annual</title><meta property="og:title" content="10 Best Intranets of 2015, Nielsen Norman Group&#39;s Intranet Design Annual" />
  
        
        <meta name="description" content="Intranet teams continue to grow, streamline processes, and produce innovative designs. Some feature trends include: responsive design, search filters, flat design, and megamenus.">
        <meta property="og:description" content="Intranet teams continue to grow, streamline processes, and produce innovative designs. Some feature trends include: responsive design, search filters, flat design, and megamenus." />
        
  
        
	
        
        <meta name="keywords" content="10 Best Intranets, 2015, Intranet Design Annual, Intranet, Intranet trends, Intranet Best Practices, Intranet winners, Intranet award">
        

        

        <meta name="robots" content="noarchive">
        <meta name="viewport" content="width=device-width">

        <link rel="search" type="application/opensearchdescription+xml" title="NN/g Search" href="../../opensearch.xml" />
        <link rel="shortcut icon" href="http://media.nngroup.com/static/img/favicon.ico" />

        <link rel="stylesheet" href="https://media.nngroup.com/static/vendor/font-awesome/css/font-awesome.min.css">

        
        <link rel="stylesheet" href="https://media.nngroup.com/static/css/7232a960f1df.css" type="text/css" />
        <link rel="stylesheet" href="http://fast.fonts.com/t/1.css?apiType=css&amp;projectid=c5846260-9def-46ec-9407-0f1349bd2d64" type="text/css">

        <script src="https://media.nngroup.com/static/vendor/modernizr/js/modernizr.min.js"></script>

        <script type="text/javascript" src="https://media.nngroup.com/static/js/3283be5def74.js"></script>
        

        

        
        
        <!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=229034,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('http://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->


<!-- Google Analytics Tracking Code for http://nngroup.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23132681-1', 'nngroup.com');
  ga('require', 'linkid', 'https://www.nngroup.com/articles/intranet-design-2015/linkid.js');
  ga('require', 'displayfeatures');

// Optimizely Universal Analytics Integration
 window.optimizely = window.optimizely || [];
 window.optimizely.push("activateUniversalAnalytics");

  ga('send', 'pageview');

</script>
<script>
/**
* Function to track a click on an outbound link in Google Analytics.
*/
function trackOutboundLink(label, url) {
    if(window.ga && ga.loaded) {
        ga('send', 'event', 'outbound', 'click', label, {'hitCallback': function(){document.location = url;}
        });
    } else {
        document.location = url;
    }
}
/**
* Function to track a click on a link in the article announcement area.
*/
$('#articleAnnouncement a').on('click', function(){
  ga('send', 'event', 'link', 'click', 'articleAnnouncement');
});
</script>

<script type="text/javascript">
 setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2421.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



<!-- Hotjar Tracking Code for http://nngroup.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:121605,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

        <script src="https://media.nngroup.com/static/js/analytics/track_outbound_link.js"></script>
        
        
        
    </head>
    <body class="location-articles articles-detail">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
    <header id="page-header">
        <div id="cart" class="shopping-cart mobile-cart show-for-small-down"></div>
        <div class="row">
            <div id="masthead" class="masthead small-12 medium-7 column">

                <!-- The button to toggles menu on small screen -->

                <h1><a href="../../index.php"><img src="http://media.nngroup.com/static/img/logo-wordmark.png" width="74" height="30"> Nielsen <span class="norman">Norman</span> Group</a></h1>
                <p class="tagline show-for-medium-up">Evidence-Based User Experience Research, Training, and Consulting</p>

                <!-- The button to toggles form search on small screen -->


                <div style="clear: right;width:  100%;height: 1px;"></div><?php define('ROOT_PATH',$_SERVER["DOCUMENT_ROOT"] . '/apps/navigation_styles/navigations/4'); ?><?php include(ROOT_PATH)."/navigation.php"; ?>

                <!-- Drop down menu on small screen -->
                <div id="hideNav" class="show-for-small-down">
                  
                  <ul class="menu-content">
                    <li><a href="../../index.php">Home</a></li>
                    <li class="has-submenu">
                      <a>Training</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../training/index.php">Overview</a></li>
                        
<li id="nav-ux-conference"><a href="../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../online-seminars/index.php"><span>Online Seminars</span></a></li>

                      </ul>
                    </li>
                    <li class="has-submenu">
                      <a>Consulting</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
  <li><a href="../../consulting/index.php">Overview</a></li>
  
  <li><a href="../../consulting/benchmarking/index.php">Benchmarking</a></li>
  
  <li><a href="../../consulting/expert-design-review/index.php">Expert Design Review</a></li>
  
  <li><a href="../../consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
  
  <li><a href="../../consulting/usability-testing/index.php">Usability Testing </a></li>
  
  <li><a href="../../consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
  
  <li><a href="../../in-house-training/index.php">In-House Training</a></li>
</ul>

                    </li>

                    <li><a href="../../reports/index.php">Reports</a></li>
                    <li><a href="../index.php">Articles</a></li>
                    <li class="has-submenu"><a>About NN/G</a><i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../about/index.php">Overview</a></li>
                        <li><a href="../../people/index.php">People</a></li>
                        <li><a href="../../about/why-nng/index.php">Why NN/g?</a></li>
                        <li><a href="../../about/contact/index.php">Contact</a></li>
                        <li><a href="../../news/index.php">News</a></li>
                        <li><a href="../../about/history/index.php">History</a></li>
                        <li><a href="../../books/index.php">Books</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                </div>

                <hr class="header-divider show-for-small-only"/>


            </div>
       </div>

        <div id="hideNav">            <nav class="primary-nav small-12 column">
                <ul>
                    <li><a class="navitem" id="nav-home" href="../../index.php">Home</a></li>
                    <li><a class="navitem" id="nav-training" href="../../training/index.php">Training</a></li>
                    <li><a class="navitem" id="nav-consulting" href="../../consulting/index.php">Consulting</a></li>
                    <li><a class="navitem" id="nav-reports" href="../../reports/index.php">Reports</a></li>
                    <li><a class="navitem" id="nav-articles" href="../index.php">Articles</a></li>
                    <li><a class="navitem" id="nav-about" href="../../about/index.php">About NN/G</a></li>
                </ul>
            </nav>
            <nav class="secondary-nav small-12 medium-12 large-12 columns">
                
                
            </nav>
        </div>
    </header>
    <div class="l-content">
    
  

  

  <div class="row">
    
      
      <section class="small-12 medium-3 columns collapsable-content l-navrail articles-left-column">
        <h1 class="show-for-small-down collapse-link mobile-browse-header">Browse by Topic and Author</h1>
          <div class="row collapsable">

            
            <div class="small-6 medium-12 columns">
              <section id="article-popular-topics" style="display: none;">
                
                <h1>Topics</h1>
                <ul id="article-popular-topics">
                    
                    <li>
                        
                        <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/intranets/all/index.php">Intranets</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/user-testing/all/index.php">User Testing</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                        
                    </li>
                    
                    <li><a href="#" id="article-see-all-topics"><strong>See all topics&hellip;</strong></a></li>
                </ul>
                
              </section>
              <section id="article-all-topics" style="display: none;">
                
                <h1>
                  <span class="show-for-medium-up">All Article Topics</span>
                  <span class="show-for-small-down">All Topics</span>
                  (<a href="#" id="article-hide-all-topics">hide</a>)
                </h1>
                <ul>
                  
                  <li>
                    
                    <a href="../../topic/accessibility/all/index.php">Accessibility</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/agile/all/index.php">Agile</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/analytics-and-metrics/all/index.php">Analytics &amp; Metrics</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/applications/all/index.php">Application Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/b2b-websites/all/index.php">B2B Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/behavior-patterns/all/index.php">Behavior Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/branding/all/index.php">Branding</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/content-strategy/all/index.php">Content Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/corporate-websites/all/index.php">Corporate Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/design-patterns/all/index.php">Design Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-design-process/all/index.php">Design Process</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/email/all/index.php">Email</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/eyetracking/all/index.php">Eyetracking</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/heuristic-evaluation/all/index.php">Heuristic Evaluation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/human-computer-interaction/all/index.php">Human Computer Interaction</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-ideation/all/index.php">Ideation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/information-architecture/all/index.php">Information Architecture</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/interaction-design/all/index.php">Interaction Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/international-users/all/index.php">International Users</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/intranets/all/index.php">Intranets</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-management/all/index.php">Management</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/navigation/all/index.php">Navigation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/non-profit-websites/all/index.php">Non-Profit Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/personas/all/index.php">Personas</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/persuasive-design/all/index.php">Persuasive Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/prototyping/all/index.php">Prototyping</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/psychology-and-ux/all/index.php">Psychology and UX</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/research-methods/all/index.php">Research Methods</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/search/all/index.php">Search</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/social-media/all/index.php">Social Media</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/strategy/all/index.php">Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/user-testing/all/index.php">User Testing</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-teams/all/index.php">UX Teams</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/visual-design/all/index.php">Visual Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/writing-web/all/index.php">Writing for the Web</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/young-users/all/index.php">Young Users</a>
                    
                  </li>
                  
                </ul>
                
              </section>
            </div>
            <script>
              /* Determine which topics list will be displayed: the full or the
               * partial.  We are inlining this close to the list itself to
               * prevent rendering delays on the client
               */
              var topic_list = $.cookie("article-topics");

              var popular_topics = $('#article-popular-topics');
              var all_topics = $('#article-all-topics');

              if(topic_list == 1) {
                  popular_topics.hide();
                  all_topics.show();
              } else {
                  popular_topics.show();
                  all_topics.hide();
              }
            </script>
            

            
            <div class="small-6 medium-12 columns">
              
              <section id="article-initial-authors">
                  <h1>Author</h1>
                  <ul>
                    
                    <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                    
                    <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                    
                    <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                    
                    <li><a href="#" onClick="$('#article-initial-authors').hide(); $('#article-all-authors').show();"><strong>See all authors&hellip;</strong></a></li>
                  </ul>
              </section>
              <section id="article-all-authors" style="display: none;">
                <h1>
                  All Authors
                  (<a href="#" id="article-hide-all-authors">hide</a>)
                </h1>
                <ul>
                  
                  <li><a href="../author/raluca-budiu/index.php">Raluca Budiu</a></li>
                  
                  <li><a href="../author/patty-caya/index.php">Patty Caya</a></li>
                  
                  <li><a href="../author/susan-farrell/index.php">Susan Farrell</a></li>
                  
                  <li><a href="../author/therese-fessenden/index.php">Therese Fessenden</a></li>
                  
                  <li><a href="../author/kim-flaherty/index.php">Kim Flaherty</a></li>
                  
                  <li><a href="../author/sarah-gibbons/index.php">Sarah Gibbons</a></li>
                  
                  <li><a href="../author/aurora-harley/index.php">Aurora Harley</a></li>
                  
                  <li><a href="../author/kate-kaplan/index.php">Kate Kaplan</a></li>
                  
                  <li><a href="../author/page-laubheimer/index.php">Page Laubheimer</a></li>
                  
                  <li><a href="../author/angela-li/index.php">Angie Li</a></li>
                  
                  <li><a href="../author/hoa-loranger/index.php">Hoa Loranger</a></li>
                  
                  <li><a href="../author/kate-meyer/index.php">Kate Meyer</a></li>
                  
                  <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                  
                  <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                  
                  <li><a href="../author/kara-pernice/index.php">Kara Pernice</a></li>
                  
                  <li><a href="../author/christian-rohrer/index.php">Christian Rohrer</a></li>
                  
                  <li><a href="../author/amy-schade/index.php">Amy Schade</a></li>
                  
                  <li><a href="../author/katie-sherwin/index.php">Katie Sherwin</a></li>
                  
                  <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                  
                  <li><a href="../author/kathryn-whitenton/index.php">Kathryn Whitenton</a></li>
                  
                </ul>
              </section>
              
            </div>
            
          </div>

          
          <section class="hide-for-small-down">
            
              
              <h1>Recent Articles</h1>
              
              <ul>
                  
                  <li><a href="../seamless-cross-channel/index.php">Seamlessness in the Omnichannel User Experience</a></li>
                  
                  <li><a href="../brand-intention-interpretation/index.php">Experience Design: Bridging Brand Intention and Brand Interpretation</a></li>
                  
                  <li><a href="../eyetracking-tasks-efficient-scanning/index.php">Scanning Patterns on the Web Are Optimized for the Current Task</a></li>
                  
                  <li><a href="../flat-design-best-practices/index.php">Flat-Design Best Practices</a></li>
                  
                  <li><a href="../ux-user-stories/index.php">Accounting for UX Work with User Stories in Agile Projects</a></li>
                  
                  <li><a href="../index.php"><strong>See all articles&hellip;</strong></a></li>
              </ul>
              
            
          </section>
          

          
          
          <section class="hide-for-small-down">
            <h1>Popular Articles</h1>
            
            <ul>
                
                <li><a href="../usability-101-introduction-to-usability/index.php">Usability 101: Introduction to Usability</a></li>
                
                <li><a href="../top-10-mistakes-web-design/index.php">Top 10 Mistakes in Web Design</a></li>
                
                <li><a href="../how-users-read-on-the-web/index.php">How Users Read on the Web</a></li>
                
                <li><a href="../f-shaped-pattern-reading-web-content/index.php">F-Shaped Pattern For Reading Web Content</a></li>
                
                <li><a href="../ten-usability-heuristics/index.php">10 Usability Heuristics for User Interface Design</a></li>
                
                <li><a href="../intranet-design-2016/index.php">10 Best Intranets of 2016</a></li>
                
                <li><a href="../which-ux-research-methods/index.php">When to Use Which User-Experience Research Methods</a></li>
                
                <li><a href="../response-times-3-important-limits/index.php">Response Times: The 3 Important Limits</a></li>
                
                <li><a href="../why-you-only-need-to-test-with-5-users/index.php">Why You Only Need to Test with 5 Users</a></li>
                
                <li><a href="../page-fold-manifesto/index.php">The Fold Manifesto: Why the Page Fold Still Matters</a></li>
                
            </ul>
          </section>
          
          

          
            <section class="hide-for-small-down">
              
            </section>
          

      </section>
      

      
        <div class="small-12 medium-9 columns content-body">
          




<article class="l-subsection alertbox-article-content article-detail-bl">
  
  <header>
    <div class="l-subsection">
      <h1>10 Best Intranets of 2015</h1>
      <div class="author-meta">by
        
          
           
              <a href="../author/amy-schade/index.php">Amy Schade</a>, 
           
              <a href="../author/kara-pernice/index.php">Kara Pernice</a>, and 
           
              <a href="../author/patty-caya/index.php">Patty Caya</a>
           
          
        on  January 4, 2015
        
      </div>

      

      
      <div class="topic-links">
        Topics:
        
          <ul>

  <li><a href="../../topic/intranets/index.php">Intranets</a></li>

</ul>
        
      </div>
      
    </div>
  </header>
  

  
  
  <section>
    <p class="lede"><strong>Summary:</strong> While intranet teams continue to grow they simultaneously streamline processes and work faster, resulting in innovative designs. Common feature trends include: responsive design, search filters, flat design, and megamenus, to name a few. </p>
    <br />
  </section>
  
  

  
  <section id="article-body"><div>
<h2><span style="line-height: 1.2;">Organizations</span></h2>
</div>

<p>The organizations with the <a href="../../reports/intranet-design-annual/index.php">10 best-designed intranets for 2015</a> are:</p>

<ul>
	<li><strong>Accolade</strong> (The Netherlands), a social housing office for affordable homes</li>
	<li><strong>Adobe</strong> (United States), a digital marketing and media solutions company</li>
	<li><strong>ConocoPhillips</strong> (United States), an explorer and producer of crude oil and natural gas energy</li>
	<li><strong>Klick Health</strong> (Canada), a digital health agency</li>
	<li><strong>Saudi Food &amp; Drug Authority</strong> (Saudi Arabia), a government agency ensuring the safety of food and drugs, biological and chemical substances, and electronic products</li>
	<li><strong>Sprint</strong> (United States), a provider of wireless telecommunications products and services</li>
	<li><strong>TAURON Polska Energia</strong> (Poland), hard coal mining and generation, distribution and supply of electricity and heat; the largest distributor and supplier of electricity in Poland</li>
	<li><strong>The Foschini Group</strong> (South Africa), an independent chain store retail group</li>
	<li><strong>UniCredit S.p.A.</strong> (Italy), a commercial bank spanning 50 markets and operating in more than 17 European countries</li>
	<li><strong>Verizon Communications</strong> (United States), a telecommunications company</li>
</ul>

<p>Most of our <a href="../../news/item/2015-intranet-design-awards/index.php">winners</a> have full-scale intranet applications designed to serve their entire organization and a variety of job roles. Two of this year’s winners have specialized intranets: Verizon’s Human Resources portal and The Foschini Group’s site for head-office employees.</p>

<h2>A Three-Time Winner</h2>

<p>This is the third time Verizon Communications has won in our Design Annual, a first for any company. Recognized this year for its Human Resources portal, Verizon also had winning designs in <a href="../../reports/10-best-intranets-2005/index.php">2005</a> and <a href="../../reports/10-best-intranets-2011/index.php">2011</a>.</p>

<p><span style="line-height: 1.6;">As every strong intranet team knows, work on an intranet is ongoing. Verizon’s team demonstrates that a redesign project is only as good as its maintenance, upkeep, and consistent reevaluation. Organizational needs, technology offerings, and user expectations change over time, but the most successful intranet teams respond to these changes.</span></p>

<p>It’s our honor to include this repeat winner in our report, recognizing those who continually work toward creating optimal intranet systems. This organization directly reflects that work toward a great system is never truly finished.</p>

<h2>Great Intranets Were Created Faster This Year</h2>

<p>Today’s Intranet teams are streamlining processes and are working faster. A trend we saw last year that repeated this year is for teams to go live with iterative changes rather than wait to launch one new, huge intranet design. <a href="../../courses/lean-ux-and-agile/index.php">Agile or Agile-like approaches</a> were used effectively in development and contributed to the lower average time required to create the Intranet. The operative word here is <em>effectively</em>, as employing Agile is not a silver bullet; if done well, however, it can certainly streamline a project. (If done without proper design integration, Agile coding will create a disjointed, substandard user experience.)</p>

<p>The noted trend toward an iterative process has changed the way we’ve been measuring intranet-project completion in the last two years. Prior to that we only looked at one single release of the “full” intranet. But now, for some designs, we’re looking at iterations of the design, or even at just the functional elements that were most recently changed.</p>

<p>This way of working is potentially more practical for an organization producing an up-to-date and useful design. The development challenges may seem less daunting with an iterative approach, and the ability to see working results sooner may be more gratifying too. As improvements occur incrementally, employees are generally happier too, which can help with employee retention.</p>

<p>A possible setback of an iterative approach, of course, is if the way employees do a task is changed out from under them (as it happens too often),  and leaves them less productive and likely disgruntled. Thus, as designs iterate, it’s important that employees be able to still accomplish tasks without difficulty. No employee should wonder what mystery version of the intranet will appear each day when logging in. Their top tasks, global navigation, and core content sections should be designed early and stay static. This will help provide the concrete foundation that users need.</p>

<p>On average, <strong>creating a new intranet took this year’s winning teams 1.4 years (17.3 months)</strong>, which is about the same as the average time spent by 2014’s winners (that time was 1.4 years or 16.7 months).. This is a major drop from the 2013 winners, who spent an average of 2.2 years (26.6 months), and the 2012 winners, who spent 4 years (47.4 months) on average.</p>

<p><img alt="Line chart displaying average years spent" height="176" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2014/12/12/average-years-spent-creating-intranets.png" width="400"/></p>

<p><em>Average Years Spent Creating Intranets: 2001–2015. Since 2001, intranet teams have spent an average of 3.2 years (38.3 months) creating their winning designs. Like last year, this year’s winners took less than 1.5 years (17.3 months) to create their designs.</em></p>

<h2>Average Organization Size Is Up</h2>

<p>The size of the winning organizations is up this year, bolstered by the employee numbers of Verizon, UniCredit, and Sprint, which are 230,000, 110,000, and 106,000, respectively. On the other end, Accolade has 200 employees and Klick Health has 467. The <strong>average size across all winners is 52,200 employees</strong>, with a median of 14,250.</p>

<p>Both the average and median are higher compared to recent years. Last year’s average was 11,600 employees with a median of 5,500. This year, only four of our winning sites serve fewer than 10,000 employees, while seven sites from last year supported smaller groups.</p>

<p>These average numbers, essentially the same from 2009 through 2011, dropped in the last three years. Winning sites across all years of the Intranet Design Annual have supported an average of 53,600 employees.</p>

<p>The spectrum of small to large organizations shows that powerful and user-centric intranet designs can be created regardless of company size, given the right team, tools, goals, leadership, and support.</p>

<p><img alt="Line chart of average organization sizes" height="213" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2014/12/12/average-size-of-organizations.png" width="400"/></p>

<p><em>Average Size of Organizations: 2001–2015. The intranets in 2015 supported an average of 52,200 employees, ranging from 200 (Accolade) to 230,000 (Verizon). This year’s median was 14,250 employees. The high average in 2010 was due to Walmart’s site, which supported 1.4 million store associates. The average for 2010 excluding Walmart was 39,100.</em></p>

<h2>Continued Slow Growth of Team Size</h2>

<p><a href="../../reports/past-intranet-design-annuals/index.php">Over the years of the Design Annual</a>, we’ve seen gradual growth in the size of the teams responsible for the noteworthy intranets we recognize. This year’s average team size is 19, up from 16 a year ago and continuing an ascending trend over time. (Two years ago, AT&amp;T’s substantial intranet team of 107 people drove that year’s average even higher.)</p>

<p>This year’s team sizes range from a low of 6 at Adobe (supporting 12,000 employees) to a high of 43 members at UniCredit (supporting 110,000 employees).</p>

<p>Our count of total-team size includes all employees working full or part time on the site, as well as outside consultants contributing to the project.</p>

<p><img alt="Line chart of average intranet team sizes" height="162" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2014/12/12/average-intranet-team-size.png" width="400"/></p>

<p><em>Average Intranet Team Size: 2001–2015. Average team size increased to 19 employees per organization, continuing what we see as a positive trend over time to staff intranet teams with more employees. While the curve has a few bumps, the long-term trend is extraordinarily strong.</em></p>

<p>We are happy to see the observed growth in team sizes over time, which reflects an increasing commitment to intranets. It is important to maintain these teams after a redesign is finished to uphold proper governance, support iterative design processes, and ensure routine upkeep and maintenance.</p>

<h2>Teams Size Relative to the Number of Employees Supported by the Intranet</h2>

<p>The size of the Intranet team alone, however, is only part of the picture. A 10-person team for an organization having 10,000 employees is very different from the same size of team serving 100 employees. In comparison to organization size, the average team size is only up slightly. Looking at team size as a percentage of organization size, we see a drop from the growth in the past few years, down to .036%.</p>

<p>Company size, of course, is a consideration in this percentage. The percentage is larger than it was for the winning intranets in 2003 and 2008, which supported a similar average number of employees.</p>

<p><img alt="Line chart of teams size as a percent of organization size" height="240" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2014/12/12/team-size-as-a-percentage-of-company-size.png" width="400"/></p>

<p><em>Team Size as a Percentage of Company Size: 2001–2015. This year’s winning intranet teams comprise 0.036% of the organizations they support. This curve has more outliers than the previous figure but the long-term trend is still fairly certain: going up.</em></p>

<p>An appropriate <em>percentage</em> of team size to organization size will vary as much as an appropriate number of Intranet team members per organization would. For example, it is probably not realistic for Verizon, with 230,000 employees, to have last year’s average value of 0.138% staff working on the intranet, as that would result in 317 people working on the site full or part time. Nor would the same percentage value work at Accolade, supporting 200 employees, as that would warrant just a quarter of a person on the intranet team.</p>

<p>As such, we compared team size to organization size over the last six years of our Intranet Design Annual by segmenting organizations into three size ranges. For small organizations of fewer than 5,000 employees, the range averaged 0.93%, or nine employees per thousand. For organizations between 5,000 and 20,000 employees, the average was 0.19%, or 1.9 employees per thousand. For organizations having 20,000 or more employees, the average was 0.04%, or 0.4 employees per thousand.</p>

<p>So it’s certainly possible to create an intranet with fewer staff. For example, Luleå University of Technology won a Design Annual award in <a href="../../reports/10-best-intranets-2001/index.php">2001</a> with just three members on its intranet team. And North Tyneside College won in <a href="../../reports/10-best-intranets-2003/index.php">2003</a> with a team of one person.</p>

<p>You may also find it necessary to augment in-house teams with a variable number of <a href="../who-should-you-hire-to-design-your-web-site/index.php">external contributors</a> at different points in the project. The numbers in our calculations include consultants who helped in various phases of the intranet projects. Intranet teams left to maintain the systems are composed of far fewer people than the team sizes in the following table. So intranet teams grew during the creation of the intranet as external resources helped out, then reduced again to the fixed number of in-house staff once intranets projects were finished.</p>

<p>It is essential to keep an intranet team sufficiently staffed when a redesign project is over. Even the best intranet will suffer if no one maintains it.</p>

<p><img alt="Table of team size compared to company" height="261" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2014/12/12/table_intranet-team-size-compared-to-company-size.png" width="400"/></p>

<p><em>Intranet Team Size Compared to Company Size.</em></p>

<h2><span style="line-height: 1.6;">Outside Consultants are Often Necessary</span></h2>

<p>8 of the winning organizations this year had help from agencies and consultants on their intranet projects. Sometimes external resources were brought in to lead the redesign effort. Many of the external contributors were specialists at developing and implementing a particular technology. Agency staff and consultants add real-world experience and varied specialization to the in-house team’s skill set and knowledge of organizational workings.</p>

<p>This year’s winning organizations looked to outside resources for help with the following activities and technology:</p>

<ul>
	<li><a href="../../courses/content-strategy-day-1/index.php">Content strategy</a> and training</li>
	<li><a href="../../courses/intranet-usability/index.php">Design</a></li>
	<li>Development</li>
	<li>Implementation</li>
	<li><a href="../../courses/information-architecture/index.php">Information architecture</a></li>
	<li>Integration with third-party tools</li>
	<li>SharePoint</li>
	<li><a href="../../reports/intranet-social-features-case-studies/index.php">Social</a> tools</li>
	<li>Task-management tools</li>
	<li><a href="../../consulting/ux-research-usability-testing/index.php">Usability and user experience research</a></li>
</ul>

<h2>Feature Trends</h2>

<p>As for the user interface, intranets often take a cue from web design, but in some areas intranets lead the way. Strong trends in intranet features this year include:</p>

<ul>
	<li><strong>Responsive Design. </strong>Like last year, <a href="../5-reasons-for-responsive-design-intranet/index.php">responsive intranet design</a> is significant again, with 5 of the 10 winners implementing intranets in a <a href="../responsive-web-design-definition/index.php">responsive</a> way. Organizations overcame the usual concerns around intranet security and offer employees access to expected content in <a href="../responsive-design-intranets/index.php">varying ways</a>.</li>
	<li><strong>Search Filters.</strong> The most common new trend on intranets this year is <a href="../filters-vs-facets/index.php">faceted search</a>. Eight of the 10 winning intranets offer methods for refining search results by selecting from sets of facets. Accolade, Adobe, ConocoPhillips, SFDA, Sprint, TFG, TAURON Polska Energia, and UniCredit all make it possible to manipulate results based on tags related to search results. Search technology and planned content management with descriptive keywords make this feature work for the users.</li>
	<li><strong>Hover effects for immediate information about search.</strong> Intranet designers today focus on getting employees more information faster with less user effort. Content on pages is more thorough, yet concise. Rather than clutter pages, designs make use of <a href="../mouse-vs-fingers-input-device/index.php">hover effects</a> to display more information before a user makes a commitment to click and follow through. Most commonly, pausing the cursor over a search result displays more information about that result item. ConocoPhillips, SFDA, and TFG employ this effect to help people make better use of search results quickly.</li>
	<li><strong>Federated Search. </strong>This is just a borderline trend as only a few organizations are doing this. But ConocoPhillips and UniCredit are leading the charge, offering search capabilities that effectively query multiple knowledge repositories, thus removing invisible awareness barriers often found on intranets. A word of advice: make the searches in the various areas good before attempting to federate.</li>
	<li><strong>Flat Design.</strong> Bevels, shadows, and elaborate framing effects seem to have become about as necessary as the human appendix, at least for this year. Six of the 10 winners use a <a href="../flat-design/index.php">flat aesthetic</a>, trading traditional-looking buttons for one-dimensional rectangles and circles. Intranets for Accolade, ConocoPhillips, SFDA, Sprint, TFG, and UniCredit all employ a flat aesthetic.</li>
	<li><strong>Carousels.</strong> As in recent years, carousels have a prominent presence on the intranet homepage. Adobe, ConocoPhillips, TFG, and UniCredit all make good use of the limited homepage real estate by offering <a href="../designing-effective-carousels/index.php">carousels</a> that rotate at least three different pieces of content in each. In Design Annual fashion, the way these organizations present the navigation and content further progresses carousel design for intranets.</li>
	<li><strong>Company Performance on Homepage.</strong> To inform and motivate employees, Accolade and ConocoPhillips offer updates about company performance on the homepage. Similarly<strong>, </strong>Sprint and UniCredit display the stock price on the homepage.</li>
	<li><strong>Megamenus. </strong>Also seen in years past, <a href="../killing-global-navigation-one-trend-avoid/index.php">megamenus</a> are helping employees discover layers deep in the IA hierarchy with a simple waive of the mouse. Adobe, ConocoPhillips, and Sprint intranets all use megamenus effectively.</li>
	<li><strong>Clever Use of Video.</strong> Today’s intranet designers understand the potential and power of <a href="../video-usability/index.php">video</a>. And they are moving away from the idea of limiting how or when video can be used on intranets. For example, Klick offers knowledge sharing via iPhone video. The process begins when an employee submits a question. The content team then identifies an expert on the topic, walks to the expert’s desk with an iPhone in hand, records the expert’s answer as video, and immediately uploads the video to the intranet for all to access. In another example, Adobe has 90-second recaps of top news stories, which the Employee Communications team produces biweekly. Finally, Sprint displays relevant news as video, and its sales staff often create and post short videos about an assortment of sales topics.</li>
	<li><strong>Fat Footers.</strong> ConocoPhillips, TAURON Polska Energia, and Verizon all display <a href="../../courses/web-page-design/index.php">large footers</a> at the bottom of intranet pages, giving employees one more chance to find what they need when down there. These oversized footers, separated from the main content area with a different background color and containing distinctly headed sections, are an expected and obliging anchor on intranets.</li>
</ul>

<h2>Full Report</h2>

<p>For more information about themes, intranet best practices, and 149 full-color screenshots of the 10 winners download the <a href="../../reports/10-best-intranets-2015/index.php">2015 Intranet Design Annual</a>. This year's report download comes with a folder containing each image as a .png to make it easier to zoom in on and study the designs.</p>
</section>
  

  
  <section>
    
        
    
  </section>
  
  
  
  
  

  


  <section>
    <br />
    <p class="share"><b>Share <span class="hide-for-small-down">this article</span>:</b> <a href="https://twitter.com/intent/tweet?url=http://www.nngroup.com/articles/intranet-design-2015/&amp;text=10%20Best%20Intranets%20of%202015&amp;via=nngroup">Twitter</a> | <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=http://www.nngroup.com/articles/intranet-design-2015/&amp;title=10%20Best%20Intranets%20of%202015&amp;source=Nielsen%20Norman%20Group">LinkedIn</a> | <a href="https://plus.google.com/share?url=http://www.nngroup.com/articles/intranet-design-2015/">Google+</a> | <a href="mailto:?subject=NN/g Article: 10 Best Intranets of 2015&amp;body=http://www.nngroup.com/articles/intranet-design-2015/">Email</a></p>
    <hr />
  </section>

  <footer class="l-subsection">
    
    
    <div class="learn-more">
      <h2>Learn More</h2>
      <p><a href="../subscribe/index.php">Subscribe to the weekly newsletter</a> to get notified about future articles.</p>
      </div>
    
    

    <div class="row show-for-small-down related-content">
      
      
      

      
      
        <div class="column small-12 topic-links">All articles about:
          <ul>
            
            <li><a href="../../topic/intranets/index.php">Intranets</a></li>
            
          </ul>
        </div>
      
      

      
      
      

      
      
        <div class="small-12 column">
          <h3>Research Reports</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../reports/intranet-design-annual/index.php">2017 Intranet Design Annual</a></li>
              
            
              
                <li><a href="../../reports/intranet-usability-guidelines/index.php">Intranet Usability Guidelines: Findings from User Testing of 42 Intranets</a></li>
              
            
              
                <li><a href="../../reports/intranet-portals-experiences-real-life-projects/index.php">Intranet Portals: UX Design Experience from Real-Life Projects</a></li>
              
            
              
                <li><a href="../../reports/intranet-information-architecture-design-methods/index.php">Intranet Information Architecture Design Methods and Case Studies</a></li>
              
            
              
                <li><a href="../../reports/best-merged-intranets/index.php">Mergers and Acquisitions, and the Resulting Intranets 2007-2016</a></li>
              
            
          </ul>
        </div>
      
      

      
      
        <div class="small-12 column">
          <h3>Online Seminars</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../online-seminars/improving-intranet-content/index.php">Improving Intranet Content</a></li>
              
            
              
                <li><a href="../../online-seminars/intranet-users-find/index.php">Helping Intranet Users Find What They Need</a></li>
              
            
          </ul>
        </div>
      
      
    </div>

    <div class="hide-for-small-down row related-content">
      <div class="medium-6 columns">
        
        
          <div>
            <h3>Articles</h3>
            <ul class="no-bullet">
              
                
                <li><a href="../intranet-design/index.php">10 Best Intranets of 2017</a></li>
                
              
                
                <li><a href="../top-intranet-design-mistakes/index.php">The Top Enduring Intranet-Design Mistakes: 7 Deadly Sins</a></li>
                
              
                
                <li><a href="../top-intranet-trends/index.php">Top 10 Intranet Trends of 2016</a></li>
                
              
                
                <li><a href="../intranet-content-authors/index.php">3 Ways to Inspire Intranet Content Authors</a></li>
                
              
                
                <li><a href="../sharepoint-intranet-ux/index.php">Design a Brilliant SharePoint Intranet</a></li>
                
              
            </ul>
          </div>
        
        

        
          
          <div>
            <h3>Research Reports</h3>
            <ul class="no-bullet">
              
                
                  <li><a href="../../reports/intranet-design-annual/index.php">2017 Intranet Design Annual</a></li>
                
              
                
                  <li><a href="../../reports/intranet-usability-guidelines/index.php">Intranet Usability Guidelines: Findings from User Testing of 42 Intranets</a></li>
                
              
                
                  <li><a href="../../reports/intranet-portals-experiences-real-life-projects/index.php">Intranet Portals: UX Design Experience from Real-Life Projects</a></li>
                
              
                
                  <li><a href="../../reports/intranet-information-architecture-design-methods/index.php">Intranet Information Architecture Design Methods and Case Studies</a></li>
                
              
                
                  <li><a href="../../reports/best-merged-intranets/index.php">Mergers and Acquisitions, and the Resulting Intranets 2007-2016</a></li>
                
              
            </ul>
          </div>
          
        

      </div>
      <div class="medium-6 columns">
        
        
          
          <div>
            <h3>Online Seminars</h3>
            <ul class="no-bullet">
            
                <li><a href="../../online-seminars/improving-intranet-content/index.php">Improving Intranet Content</a></li>
            
                <li><a href="../../online-seminars/intranet-users-find/index.php">Helping Intranet Users Find What They Need</a></li>
            
            </ul>
          </div>
          
        
      </div>
    </div>
  </footer>
</article>

        </div>

      

    
  </div>
  <div class="row">
    
      <div class="column small-12 show-for-small-down l-navrail announcements">
        
          
        
      </div>
    
    
  </div>

    </div>
    




<footer id="page-footer">
    <div class="row hide-for-print">
        <div class="column small-12 medium-6">
            <section class="footer-popular">
                
                <h1>Popular Topics</h1>
                <div style="float: left;  width: 15em; display: block;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/applications/index.php">Application Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/e-commerce/index.php">E-Commerce Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/eyetracking/index.php">Eyetracking </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/information-architecture/index.php">Information Architecture </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/intranets/index.php">Intranet Design </a></li>
	<li><a href="../../topic/interaction-design/index.php">Interaction Design </a></li>
</ul>
</div>

<div style="float: left;  width: 15em; display: block; list-style: none;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/mobile-and-tablet-design/index.php">Mobile and Tablet Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/credibility-persuasion/index.php">Persuasive Design</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/research-methods/index.php">Research Methods</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/user-testing/index.php">User Testing </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/web-usability/index.php">Web Usability </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/writing-web/index.php">Web Writing &amp; Content Strategy </a></li>
</ul>
</div>

<p>&nbsp;</p>

                
            </section>
        </div>

        <div class="column small-12 medium-6">
            <div class="row">
                <div class="column small-6">
                    <section class="footer-about">
                        <h1>Follow Us</h1>
                        <ul>
	<li><a href="../subscribe/index.php">Subscribe to our weekly UX newsletter</a>,&nbsp;Jakob Nielsen&#39;s Alertbox</li>
	<li>Visit Don Norman&#39;s website, <a href="http://www.jnd.org/"> www.jnd.org </a></li>
	<li>Visit Bruce Tognazzini&#39;s website, <a href="http://www.asktog.com/"> www.asktog.com </a></li>
	<li style="padding-top: 6px;"><a class="twitter-follow-button" data-show-count="false" data-size="large" href="https://twitter.com/nngroup"><img alt="Follow @nngroup on Twitter" height="20" src="https://media.nngroup.com/media/editor/2016/12/30/twitter-follow-button.png" style="width: 117px; height: 20px;" width="117" /></a></li>
	<li style="padding-top: 6px;"><img alt="" height="14" src="https://media.nngroup.com/media/editor/2016/07/26/feed-icon-14x14.png" style="vertical-align:text-top;width:14px;margin-right:.25em;" width="14" /><a href="../../feed/rss/index.php">Subscribe to our RSS feed</a></li>
</ul>

                    </section>
                </div>

                <div class="column small-6">
                    <section class="footer-about right-column">
                        <h1>About
</h1>
                        <ul class="no-bullet">
	<li><a href="../../about/contact/index.php">Contact information</a></li>
	<li><a href="../../about/index.php">About Us</a></li>
	<li><a href="../../about/why-nng/index.php">Why NN/g? </a></li>
	<li><a href="../../return-policy/index.php">Return Policy</a></li>
</ul>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="copyright column small-12">
            <p><a href="../../copyright-and-reprint-info/index.php">Copyright</a> &copy; 1998-2017 Nielsen Norman Group, All Rights Reserved.</p>
        </div>
    </div>
</footer>


    


        <script src="https://media.nngroup.com/static/vendor/fastclick/js/fastclick.min.js"></script>
        <script src="https://media.nngroup.com/static/vendor/foundation/js/foundation.min.js"></script>
        <script src="http://media.nngroup.com/static/js/app.js"></script>
        <script src="http://media.nngroup.com/static/js/scroll-to-fixed.js"></script><script>var externalLinks = document.querySelectorAll('a[href^="http"]');for (var i = externalLinks.length-1; i >= 0; i--) {externalLinks[i].addEventListener("click", function(event) { alert("External links are disabled for this test."); event.preventDefault(); }, false);}var mailLinks = document.querySelectorAll('a[href^="mailto"]');for (var i = mailLinks.length-1; i >= 0; i--) {mailLinks[i].addEventListener("click", function(event) { alert("Email links are disabled for this test."); event.preventDefault(); }, false);}var externalForms = document.querySelectorAll('form');for (var i = externalForms.length-1; i >= 0; i--) {externalForms[i].addEventListener("submit", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false); externalForms[i].addEventListener("click", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false);}</script>

        <script>
            
            $(function() {
                $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
                $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
            });
        </script>

        
<script src="https://media.nngroup.com/static/js/articles.js"></script>


        
        <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1072699960;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072699960/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
    </body>

<!-- Mirrored from www.nngroup.com/articles/intranet-design-2015/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:07:37 GMT -->
</html>
