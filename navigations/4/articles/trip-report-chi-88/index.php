<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->





    
<!-- Mirrored from www.nngroup.com/articles/trip-report-chi-88/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:14:36 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1026.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"licenseKey":"1a30ef9931","errorBeacon":"bam.nr-data.net","applicationID":"2048992","transactionName":"NFxSMRBUX0EDUkIPDA0WdhAMVkVbDV8ZBxEXUFMJB0YfRAtUQRVZIktEDAFZVHYHRVcPD01eVRE=","beacon":"bam.nr-data.net","queueTime":5,"applicationTime":530,"agent":""}</script>
        <title>CHI&#39;88 Trip Report: Article by Jakob Nielsen</title><meta property="og:title" content="CHI&#39;88 Trip Report: Article by Jakob Nielsen" />
  
        
        <meta name="description" content="Jakob Nielsen&#39;s trip report for the 1988 Computer-Human Interaction conference (CHI &#39;88).">
        <meta property="og:description" content="Jakob Nielsen&#39;s trip report for the 1988 Computer-Human Interaction conference (CHI &#39;88)." />
        
  
        
	
        

        

        <meta name="robots" content="noarchive">
        <meta name="viewport" content="width=device-width">

        <link rel="search" type="application/opensearchdescription+xml" title="NN/g Search" href="../../opensearch.xml" />
        <link rel="shortcut icon" href="http://media.nngroup.com/static/img/favicon.ico" />

        <link rel="stylesheet" href="https://media.nngroup.com/static/vendor/font-awesome/css/font-awesome.min.css">

        
        <link rel="stylesheet" href="https://media.nngroup.com/static/css/7232a960f1df.css" type="text/css" />
        <link rel="stylesheet" href="http://fast.fonts.com/t/1.css?apiType=css&amp;projectid=c5846260-9def-46ec-9407-0f1349bd2d64" type="text/css">

        <script src="https://media.nngroup.com/static/vendor/modernizr/js/modernizr.min.js"></script>

        <script type="text/javascript" src="https://media.nngroup.com/static/js/3283be5def74.js"></script>
        

        

        
        
        <!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=229034,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('http://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->


<!-- Google Analytics Tracking Code for http://nngroup.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23132681-1', 'nngroup.com');
  ga('require', 'linkid', 'https://www.nngroup.com/articles/trip-report-chi-88/linkid.js');
  ga('require', 'displayfeatures');

// Optimizely Universal Analytics Integration
 window.optimizely = window.optimizely || [];
 window.optimizely.push("activateUniversalAnalytics");

  ga('send', 'pageview');

</script>
<script>
/**
* Function to track a click on an outbound link in Google Analytics.
*/
function trackOutboundLink(label, url) {
    if(window.ga && ga.loaded) {
        ga('send', 'event', 'outbound', 'click', label, {'hitCallback': function(){document.location = url;}
        });
    } else {
        document.location = url;
    }
}
/**
* Function to track a click on a link in the article announcement area.
*/
$('#articleAnnouncement a').on('click', function(){
  ga('send', 'event', 'link', 'click', 'articleAnnouncement');
});
</script>

<script type="text/javascript">
 setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2421.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



<!-- Hotjar Tracking Code for http://nngroup.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:121605,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

        <script src="https://media.nngroup.com/static/js/analytics/track_outbound_link.js"></script>
        
        
        
    </head>
    <body class="location-articles articles-detail">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
    <header id="page-header">
        <div id="cart" class="shopping-cart mobile-cart show-for-small-down"></div>
        <div class="row">
            <div id="masthead" class="masthead small-12 medium-7 column">

                <!-- The button to toggles menu on small screen -->

                <h1><a href="../../index.php"><img src="http://media.nngroup.com/static/img/logo-wordmark.png" width="74" height="30"> Nielsen <span class="norman">Norman</span> Group</a></h1>
                <p class="tagline show-for-medium-up">Evidence-Based User Experience Research, Training, and Consulting</p>

                <!-- The button to toggles form search on small screen -->


                <div style="clear: right;width:  100%;height: 1px;"></div><?php define('ROOT_PATH',$_SERVER["DOCUMENT_ROOT"] . '/apps/navigation_styles/navigations/4'); ?><?php include(ROOT_PATH)."/navigation.php"; ?>

                <!-- Drop down menu on small screen -->
                <div id="hideNav" class="show-for-small-down">
                  
                  <ul class="menu-content">
                    <li><a href="../../index.php">Home</a></li>
                    <li class="has-submenu">
                      <a>Training</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../training/index.php">Overview</a></li>
                        
<li id="nav-ux-conference"><a href="../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../online-seminars/index.php"><span>Online Seminars</span></a></li>

                      </ul>
                    </li>
                    <li class="has-submenu">
                      <a>Consulting</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
  <li><a href="../../consulting/index.php">Overview</a></li>
  
  <li><a href="../../consulting/benchmarking/index.php">Benchmarking</a></li>
  
  <li><a href="../../consulting/expert-design-review/index.php">Expert Design Review</a></li>
  
  <li><a href="../../consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
  
  <li><a href="../../consulting/usability-testing/index.php">Usability Testing </a></li>
  
  <li><a href="../../consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
  
  <li><a href="../../in-house-training/index.php">In-House Training</a></li>
</ul>

                    </li>

                    <li><a href="../../reports/index.php">Reports</a></li>
                    <li><a href="../index.php">Articles</a></li>
                    <li class="has-submenu"><a>About NN/G</a><i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../about/index.php">Overview</a></li>
                        <li><a href="../../people/index.php">People</a></li>
                        <li><a href="../../about/why-nng/index.php">Why NN/g?</a></li>
                        <li><a href="../../about/contact/index.php">Contact</a></li>
                        <li><a href="../../news/index.php">News</a></li>
                        <li><a href="../../about/history/index.php">History</a></li>
                        <li><a href="../../books/index.php">Books</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                </div>

                <hr class="header-divider show-for-small-only"/>


            </div>
       </div>

        <div id="hideNav">            <nav class="primary-nav small-12 column">
                <ul>
                    <li><a class="navitem" id="nav-home" href="../../index.php">Home</a></li>
                    <li><a class="navitem" id="nav-training" href="../../training/index.php">Training</a></li>
                    <li><a class="navitem" id="nav-consulting" href="../../consulting/index.php">Consulting</a></li>
                    <li><a class="navitem" id="nav-reports" href="../../reports/index.php">Reports</a></li>
                    <li><a class="navitem" id="nav-articles" href="../index.php">Articles</a></li>
                    <li><a class="navitem" id="nav-about" href="../../about/index.php">About NN/G</a></li>
                </ul>
            </nav>
            <nav class="secondary-nav small-12 medium-12 large-12 columns">
                
                
            </nav>
        </div>
    </header>
    <div class="l-content">
    
  

  

  <div class="row">
    
      
      <section class="small-12 medium-3 columns collapsable-content l-navrail articles-left-column">
        <h1 class="show-for-small-down collapse-link mobile-browse-header">Browse by Topic and Author</h1>
          <div class="row collapsable">

            
            <div class="small-6 medium-12 columns">
              <section id="article-popular-topics" style="display: none;">
                
                <h1>Topics</h1>
                <ul id="article-popular-topics">
                    
                    <li>
                        
                        <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/intranets/all/index.php">Intranets</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/user-testing/all/index.php">User Testing</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                        
                    </li>
                    
                    <li><a href="#" id="article-see-all-topics"><strong>See all topics&hellip;</strong></a></li>
                </ul>
                
              </section>
              <section id="article-all-topics" style="display: none;">
                
                <h1>
                  <span class="show-for-medium-up">All Article Topics</span>
                  <span class="show-for-small-down">All Topics</span>
                  (<a href="#" id="article-hide-all-topics">hide</a>)
                </h1>
                <ul>
                  
                  <li>
                    
                    <a href="../../topic/accessibility/all/index.php">Accessibility</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/agile/all/index.php">Agile</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/analytics-and-metrics/all/index.php">Analytics &amp; Metrics</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/applications/all/index.php">Application Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/b2b-websites/all/index.php">B2B Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/behavior-patterns/all/index.php">Behavior Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/branding/all/index.php">Branding</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/content-strategy/all/index.php">Content Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/corporate-websites/all/index.php">Corporate Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/design-patterns/all/index.php">Design Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-design-process/all/index.php">Design Process</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/email/all/index.php">Email</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/eyetracking/all/index.php">Eyetracking</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/heuristic-evaluation/all/index.php">Heuristic Evaluation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/human-computer-interaction/all/index.php">Human Computer Interaction</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-ideation/all/index.php">Ideation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/information-architecture/all/index.php">Information Architecture</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/interaction-design/all/index.php">Interaction Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/international-users/all/index.php">International Users</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/intranets/all/index.php">Intranets</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-management/all/index.php">Management</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/navigation/all/index.php">Navigation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/non-profit-websites/all/index.php">Non-Profit Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/personas/all/index.php">Personas</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/persuasive-design/all/index.php">Persuasive Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/prototyping/all/index.php">Prototyping</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/psychology-and-ux/all/index.php">Psychology and UX</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/research-methods/all/index.php">Research Methods</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/search/all/index.php">Search</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/social-media/all/index.php">Social Media</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/strategy/all/index.php">Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/user-testing/all/index.php">User Testing</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-teams/all/index.php">UX Teams</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/visual-design/all/index.php">Visual Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/writing-web/all/index.php">Writing for the Web</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/young-users/all/index.php">Young Users</a>
                    
                  </li>
                  
                </ul>
                
              </section>
            </div>
            <script>
              /* Determine which topics list will be displayed: the full or the
               * partial.  We are inlining this close to the list itself to
               * prevent rendering delays on the client
               */
              var topic_list = $.cookie("article-topics");

              var popular_topics = $('#article-popular-topics');
              var all_topics = $('#article-all-topics');

              if(topic_list == 1) {
                  popular_topics.hide();
                  all_topics.show();
              } else {
                  popular_topics.show();
                  all_topics.hide();
              }
            </script>
            

            
            <div class="small-6 medium-12 columns">
              
              <section id="article-initial-authors">
                  <h1>Author</h1>
                  <ul>
                    
                    <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                    
                    <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                    
                    <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                    
                    <li><a href="#" onClick="$('#article-initial-authors').hide(); $('#article-all-authors').show();"><strong>See all authors&hellip;</strong></a></li>
                  </ul>
              </section>
              <section id="article-all-authors" style="display: none;">
                <h1>
                  All Authors
                  (<a href="#" id="article-hide-all-authors">hide</a>)
                </h1>
                <ul>
                  
                  <li><a href="../author/raluca-budiu/index.php">Raluca Budiu</a></li>
                  
                  <li><a href="../author/patty-caya/index.php">Patty Caya</a></li>
                  
                  <li><a href="../author/susan-farrell/index.php">Susan Farrell</a></li>
                  
                  <li><a href="../author/therese-fessenden/index.php">Therese Fessenden</a></li>
                  
                  <li><a href="../author/kim-flaherty/index.php">Kim Flaherty</a></li>
                  
                  <li><a href="../author/sarah-gibbons/index.php">Sarah Gibbons</a></li>
                  
                  <li><a href="../author/aurora-harley/index.php">Aurora Harley</a></li>
                  
                  <li><a href="../author/kate-kaplan/index.php">Kate Kaplan</a></li>
                  
                  <li><a href="../author/page-laubheimer/index.php">Page Laubheimer</a></li>
                  
                  <li><a href="../author/angela-li/index.php">Angie Li</a></li>
                  
                  <li><a href="../author/hoa-loranger/index.php">Hoa Loranger</a></li>
                  
                  <li><a href="../author/kate-meyer/index.php">Kate Meyer</a></li>
                  
                  <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                  
                  <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                  
                  <li><a href="../author/kara-pernice/index.php">Kara Pernice</a></li>
                  
                  <li><a href="../author/christian-rohrer/index.php">Christian Rohrer</a></li>
                  
                  <li><a href="../author/amy-schade/index.php">Amy Schade</a></li>
                  
                  <li><a href="../author/katie-sherwin/index.php">Katie Sherwin</a></li>
                  
                  <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                  
                  <li><a href="../author/kathryn-whitenton/index.php">Kathryn Whitenton</a></li>
                  
                </ul>
              </section>
              
            </div>
            
          </div>

          
          <section class="hide-for-small-down">
            
              
              <h1>Recent Articles</h1>
              
              <ul>
                  
                  <li><a href="../seamless-cross-channel/index.php">Seamlessness in the Omnichannel User Experience</a></li>
                  
                  <li><a href="../brand-intention-interpretation/index.php">Experience Design: Bridging Brand Intention and Brand Interpretation</a></li>
                  
                  <li><a href="../eyetracking-tasks-efficient-scanning/index.php">Scanning Patterns on the Web Are Optimized for the Current Task</a></li>
                  
                  <li><a href="../flat-design-best-practices/index.php">Flat-Design Best Practices</a></li>
                  
                  <li><a href="../ux-user-stories/index.php">Accounting for UX Work with User Stories in Agile Projects</a></li>
                  
                  <li><a href="../index.php"><strong>See all articles&hellip;</strong></a></li>
              </ul>
              
            
          </section>
          

          
          
          <section class="hide-for-small-down">
            <h1>Popular Articles</h1>
            
            <ul>
                
                <li><a href="../usability-101-introduction-to-usability/index.php">Usability 101: Introduction to Usability</a></li>
                
                <li><a href="../top-10-mistakes-web-design/index.php">Top 10 Mistakes in Web Design</a></li>
                
                <li><a href="../how-users-read-on-the-web/index.php">How Users Read on the Web</a></li>
                
                <li><a href="../f-shaped-pattern-reading-web-content/index.php">F-Shaped Pattern For Reading Web Content</a></li>
                
                <li><a href="../ten-usability-heuristics/index.php">10 Usability Heuristics for User Interface Design</a></li>
                
                <li><a href="../intranet-design-2016/index.php">10 Best Intranets of 2016</a></li>
                
                <li><a href="../which-ux-research-methods/index.php">When to Use Which User-Experience Research Methods</a></li>
                
                <li><a href="../response-times-3-important-limits/index.php">Response Times: The 3 Important Limits</a></li>
                
                <li><a href="../why-you-only-need-to-test-with-5-users/index.php">Why You Only Need to Test with 5 Users</a></li>
                
                <li><a href="../page-fold-manifesto/index.php">The Fold Manifesto: Why the Page Fold Still Matters</a></li>
                
            </ul>
          </section>
          
          

          
            <section class="hide-for-small-down">
              
            </section>
          

      </section>
      

      
        <div class="small-12 medium-9 columns content-body">
          




<article class="l-subsection alertbox-article-content article-detail-bl">
  
  <header>
    <div class="l-subsection">
      <h1>CHI&#39;88 Trip Report</h1>
      <div class="author-meta">by
        
          
            
            <a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a>
            
          
        on  June 1, 1988
        
      </div>

      

      
      <div class="topic-links">
        Topics:
        
          <ul>

  <li><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction</a></li>

</ul>
        
      </div>
      
    </div>
  </header>
  

  
  
  

  
  <section id="article-body"><p><em>Washington, D.C., 15-19 May 1988.</em></p>

<p>"Do you want to spend the rest of your life selling sugared water or do you want a chance to change the world?" By asking this question, the then chairman of Apple Steve Jobs enticed John Sculley away from his fast track career in Pepsi. Similar considerations form a large part of my own motivation for working in the user interface field. As Ben Shneiderman said in his keynote speech at CHI'86: Our work will influence how people live in the next century.</p>

<p>On this background, CHI'88 was somewhat disappointing. I did not have the same feeling of breakthrough enthusiasm that I have had at several earlier CHIs. Of course some new developments were reported and nice research results presented. But in most cases they were refinements of earlier work. My real problem is probably just that progress in the field this year has been only 10% instead of the 30-40% I am used to, and of course most fields would be overjoyed to see 10% annual progress rates. <img alt="Sample Pie Menu" src="http://media.nngroup.com/media/editor/2012/11/01/piemenu.gif" style="width:150px; height:150px; float:right"/></p>

<p>Some new stuff was presented such as the <strong> pie menus </strong> studied by Callahan, Hopkins, Weiser, and Shneiderman from the University of Maryland. When used as pop-up menus, pies have the advantage that any menu item can be selected by equally small movements of the mouse and the study did indeed show that users performed about 15% faster using a pie menu than using a linear menu. Pie menus also have some potential disadvantages, especially when used with many menu items or in cases that call for hierarchical pop-ups.</p>

<p>In spite of this and some other novelty items, the main feel of CHI'88 was that of improvements of earlier stuff rather than revolutionary new discoveries. Every year, I am able to summarize the <a href="../trip-report-chi-themes/index.php"> main theme</a> of a CHI conference and this year I am not in doubt that the theme was that we are currently slowed down to <strong> steady, evolutionary progress </strong> in the user interface field.</p>

<p>In spite of these comments, I still enjoyed CHI'88 tremendously. As discussed below, lots of interesting things happened, and as usual "everybody" in the user interface field were there. There were plenty of opportunities to meet people and talk (which is the most important aspect of a conference anyway) and twice I even had the quite un-Danish experience of meetings during "business breakfast" at 7:30 in the morning.</p>

<p>One day I was hanging out in one of the courtesy suites enjoying the free Diet Pepsi when suddenly a person whom I had never met before asked me if I were the author of an electronic report on hypertext. I said yes, indeed I am, but how do you know? It turned out that he recognized me from the scanned photo of myself which I had been vain enough to include in the electronic document to make reading it more of a multi-media experience. I furthermore learned that he had gotten hold of the file by downloading it from the GEnie network service (to which it must have been uploaded by yet another person as I don't have a GEnie account). So the moral of this story is that electronic publishing of online documents may work fairly efficiently and get you a wide distribution even though it also involves a lot of difficulties and is not yet "officially" recognized.</p>

<h2>User Interface Paradigms</h2>

<p>One reason for the slower progress at the moment may be that we are pushing the limits of the current user interface paradigm. <a href="../noncommand/index.php"> My classification of these paradigms</a> is as follows:</p>

<ul>
	<li>First generation: Batch (0-dimensional interactions: pass/fail).</li>
	<li>Second generation: Line/command-oriented (1-dimensional).</li>
	<li>Third generation: Full screen/form filling (2-dimensional).</li>
	<li>Fourth generation: Window/bitmapped interfaces (3-dimensional because of overlapping, multiple windows).</li>
</ul>

<p>It is likely that the next generation of interfaces will begin to utilize a new dimension in the interaction, most likely that of <strong> time</strong>. Time can be used to animate icons and other interface features and to use sound effects. William Gaver from UCSD had a nice poster on his <strong> SonicFinder </strong> operating system for the Macintosh where files make different sounds when you drag them depending on their size and type. Unfortunately the poster was entirely visual without any possibility for hearing the sounds! Gaver had brought a disk with his system along but was unable during most of the conference to find a Macintosh on which to demo the SonicFinder. Unfortunately, there were very poor facilities for informal demonstrations of the interface software which several people (including myself) brought with them to Washington. We need a room full of PCs and Macs, somewhat like the laboratory setup at CHI+GI'87, which is available for anybody who bring their own diskettes. Instead there was an empty room waiting for people fanatic enough to travel with their computer.</p>

<p>Other next-generation concepts discussed at this conference were the use of video film (in Palenque, see report below) and more knowledge-based interactions. But these effects have not entered the mainstream of user interface design.</p>

<h2>Videodesk: Computing on the Desktop</h2>

<p>Current marketing trends in the personal computer business emphasize "desktop this" and "desktop that" - desktop publishing, desktop presentations, desktop video, desktop CAD... as a catch phrase for doing things on small, desktop computers. It is also possible, however, to actually do computing on the desktop itself. This was demonstrated by Myron Krueger from the Artificial Reality Corporation in the <strong> Videodesk </strong> system: Videodesk consists of a large surface over which you move your arms, hands, and fingers. A video camera mounted over the desk picks up these movements and use them as input to the computer which then shows then as an outline on the display. This display is currently separate from the desktop surface but one might imagine that a future system would feel even more natural to the user by having the output display projected directly onto the input surface.</p>

<p>Several applications were shown. One of the most immediately understandable was a finger painting system where the color used was determined by the number of fingers shown. I asked Krueger why the system deposited the paint over the user's finger rather than under it which might have seemed more natural. His answer was that sometimes one would not want the hand to obscure the work being drawn.</p>

<p>The painting was cleared by spreading all fingers. Some of these <strong> gestures </strong> seemed very natural, including the clearing gesture. Gestures in other applications were not that obvious but still frequently very nice, such as having a straight line appear between two fingertips in a CAD-system. One problem they had in developing their gestural language was in parsing hand movements to determine when you just want to move your hand to another part of the screen and when you want to issue a command. In general, there seemed not to be much consistency in the interaction techniques used in the different parts of the system with the exception of the technique of reaching to the upper right corner of the screen to pull out the main menu.</p>

<p>Videodesk is really a special version of the older Videoplace system where the computer is an entire room which you enter to use your body as input device. As such, Videodesk was yet another example of the evolutionary trend at this CHI. The full Videoplace system was not available for the conference as it was installed as part of a large exhibit on Computers and Art at the IBM Building in New York. This was a very interesting exhibition which I had seen by accident before coming to Washington: I had originally jumped on the M2 bus to go uptown to the Metropolitan Museum when I looked out the window and saw a poster at the IBM Building for their special exhibition. Yet another advantage of not using a constrained "transport interface" like the subway: You can change your mind.</p>

<h2>Novices Can't Use a Mouse</h2>

<p>Marilyn Mantei from Electronic Data Systems (now at the University of Toronto) was one of the panelists at the panel on Video: Data for Studying Human-Computer Interaction. I did not attend that panel because it conflicted with something else but later I met Mantei in the lobby and she showed me her video tape anyway in the speakers' prep room. It was a very nice study of the many peculiar ways novices approach a mouse. One user turned the mouse over and used it as a trackball. Another moved it all over the table to test its limits, while yet another user positioned the mouse, then released it, and finally put down her finger on the button in a graceful, slow curve for each click. One user finished using the computer by removing the mouse from the table all together, leaving it hanging over the edge by its cable (tail?) so that the desktop was nicely cleared. After all, who wants a messy thing like a <em> mouse </em> on his/her desk?</p>

<p>All these mouse movements on the video were shown coordinated with <cite> An der schönen, blauen Donau </cite> as soundtrack. If I should pick a piece of music for my own use of the mouse, it might be <cite> C Jam Blues </cite> because of the many double-clicks inherent in the overloading of functionality in my single-mouse system.</p>

<p>The many funny episodes and the dainty sound track in the waltzing mouse video may detract somewhat from the very real underlying problem of learning how to use a mouse: One just has to acknowledge that the mouse is not a very intuitive input device. Recently I had occasion to advice someone on the design of a simple, user-driven information system for museums and I had to convince them that they could not just use a mouse as a pointing device for people coming in off the street.</p>

<h2>Personality in Help</h2>

<p>This year, conference ergonomics were substantially improved by having the CHI video show piped into individual hotel rooms through the conference hotel TV system 24 hours a day. Consequently, it was possible to watch computer-oriented videos before going to bed instead of having to spend time on them during prime conference time. As a result of this, instead of the Tonight Show, I suddenly got to see Jeff Kelley (IBM Watson Research) starring as an advisory system in the video specialist help using multiple media by Donald Kirson et al. from IBM. The system used several media to provide help on the theory that this would help users differentiate the different help messages. Some of the help was given in the form of filmed anecdotal statements from "other users", and this is where Jeff comes into the picture.</p>

<p>I am certainly able to remember the help provided by Jeff, but this may of course be because I know him personally and was surprised to see him in a computer system. Other aspects of the multi-media help seemed more clumsy, especially the use of synthesized voice which was slow and hard to understand.</p>

<h2>Literal Interfaces</h2>

<p>Possibly the most conceptually provocative video was that of <strong> Viewpoint </strong> by Scott Kim from Information Appliance (system built at Xerox PARC). The fundamental idea of Viewpoint is that the pixels on the screen are identical to the user's data instead of just being a representation of them: a <strong> literal interface paradigm </strong> This is somewhat like the difference between pixel-oriented PAINT programs and object-oriented DRAW programs for personal computers but taken to its extreme. One immediate problem with having the entire state of the system encoded on the screen is that there is limited space on the screen. One could however imagine some kind of compromise system which allowed users to scroll windows over larger virtual planes of pixels.</p>

<p>Some demos were given in the video of how to build up some reasonable level of functionality using literal pixels: One could build a drawing of a keyboard on the screen and bind it to the real keyboard so that whenever one edited the font on the screen, one changed what would be typed. But in general I must say that the demo was not very convincing as a usable system for real users. It was a convincing exploration of an unusual research idea and it certainly made me think.</p>

<p>One small interesting interaction technique shown in Viewpoint was that the <strong> cursor was semi-transparent </strong> so that it did not hide what was underneath it. This seems a more natural way of getting this effect than the Videodesk approach of letting the paint go over the painting finger rather than under it.</p>

<p>Otherwise the videos were not all that new to me: Xerox Colab, NASA's head-mounted display, Myers' Peridot, Anderson's intelligent tutors: All very interesting, but I had seen the systems before-in many cases at earlier CHI conferences. Kristee Kreitman from Apple showed a video of her very nice MacWorld Information Kiosk (which I had also seen before) which used a very graphical interface and real-world metaphors to structure information about a large trade show. Kreitman mentioned on the video that the system had been developed by rapid iterative redesign (using HyperCard) based on informal empirical testing but unfortunately she did not show any examples of the usability problems discovered during these tests or how they were solved. We only saw the perfect result (which was of course impressive) but not the process (from which we would have learned more).</p>

<p>Take the Nielsen challenge: Dare show several versions of your system and explain to us what you learned from one iteration to the next. There is no shame in not being able to arrive at a perfect design the first time but hopefully we can see that the final version is better than the first.</p>

<h2>Graphics Programming Shootout</h2>

<p>One of the most lively parts of the conference was the panel on Making Interactive Graphics Accessible, chaired by Gary Olson. The issue being discussed was how to make it possible to <strong> easily program dynamic graphics</strong>. Everybody who has tried programming a graphical user interface using a traditional language such as Pascal knows that it is hard to do. Too hard. Several new approaches have been proposed, including User Interface Management Systems and new kinds of programming languages. This panel presented four different approaches to the problem:</p>

<ul>
	<li><strong>NoPumpG </strong> by Clayton Lewis from the University of Colorado: A system based on the spreadsheet metaphor where the designer specifies relationships between cells that contain values for graphical objects (e.g. their x and y coordinates). The weakness of this approach according to Lewis is that too much of the work is done in the cell domain instead of in the graphical domain (i.e. it is not design-by-example).</li>
	<li><strong>ThingLab </strong> by Alan Borning from the University of Washington: A system for constraint-based programming (if you e.g. specify that one line is constrained to have the same length as another line, the first line will change its length as you change the second line).</li>
	<li><strong>Boxer </strong> by Andrea diSessa from the University of California in Berkeley: A programming system derived from the Logo tradition and possibly the most "traditional" programming language of the four, based as it was on nested procedures, control structures etc..</li>
	<li>The <strong> Alternate Reality Kit </strong> (ARK) by Randy Smith from Xerox PARC: More in the nature of an object-oriented graphical construction kit than a programming language.</li>
</ul>

<p>What made this panel so interesting was that instead of traditional presentations of the four systems, it relied on having the four presenters show how they would solve a given set of problems in their system. Each presenter had been asked to submit two problems for the list: One which would be easy to solve on his system and one which would be hard. This gave a fairly long set of problems and the weakness of the panel was that we did not get to hear any details about how all the problems had been solved in all the systems and a comparison between the solutions. But if the presenters would do the required work, they have some good material for writing a comparative paper. This format made the panel interesting and unusual but it also made it quite hard to report what happened. Much of the time was spent in going over program details and explaining how they solved the problems. Most of the systems had an implicit way of dealing with parallelism (since they were quite object oriented). Boxer, however, had to solve the "bird problem" of having two birds fly across the screen flapping their wings with a speed dynamically controlled by the user in the following way:</p>

<table align="center" border="0" cols="2">
	<tbody>
		<tr>
			<td align="right" style="border-style=none; border-color=gray border-width=medium" valign="top"><code>repeat 1000 </code></td>
			<td align="left" style="border-style=solid; border-color=gray border-width=medium" valign="top"><code>tell bird1 tick<br/>
			tell bird2 tick<br/>
			handle_input </code></td>
		</tr>
	</tbody>
</table>

<p>The bird problem had been proposed by Clayton Lewis as his easy problem, and he indeed solved it very easily by defining a few spreadsheet cells. The actual birds were only stick figures however, while Randy Smith in his ARK solution animated some nicely drawn pigeons. In general, Smith showed the most flashy and immediately convincing solutions. On the other hand, his system relied very much on a large number of predefined objects which made it somewhat harder for the outsider to quickly understand what the system could and could not do.</p>

<p>In the concluding discussion, Borning said that one in the long run might want a more hybrid programming model but that it is interesting to try to push a single concept as far as you can and see what happens.</p>

<h2>The World has Three Dimensions (Fred Brooks)</h2>

<p>Fred Brooks from the University of North Carolina gave one of the invited lectures. He is probably best known as the author of the book <cite> <a href="http://www.amazon.com/exec/obidos/ISBN=0201835959/useitcomusableinA/"> The Mythical Man-Month</a> </cite>which may well be the single best book in computer science. He did not talk about general design or software engineering, however, but about how one can get better insights through computer graphics simulations.</p>

<p>Brooks stressed the importance of three dimensional graphics since the world has three dimensions. A few objects, such as the "desktop metaphor" are 2D, but most objects in the physical world are 3D. He also emphasized the need for real time interactive graphics where the 3D image would change in immediate response to user actions. Since in most cases we can only show a 2D projection on a computer screen, a 3D illusion requires that we can rotate or otherwise manipulate the simulated object in real time. Some people approach computer graphics by wanting to make images look as real as possible, but Brooks was more interested in making them move as if real, even if that meant that they would not look quite as good: 20 frames per second is required for good representaiton of movement. He mentioned that it was hard for users to understand simulated 3D spaces (except for those which they knew well in advance) but that user-controlled lighting and camera/viewpoint (movements) really enhanced the interface. Users should change how they view a scene to understand it rather than change the model itself.</p>

<p>One really needs maps and overviews for navigating the 3D virtual worlds. In Brook's experience, users start out navigating by looking at the map, while experienced users move around directly by looking at the screen and only every now and then look at the map.</p>

<p>One of the problems in 3D systems is which input/output devices to use. Brooks showed a force output device (a kind of 3D joystick) with which users could manipulate the 3D structures being shown on a screen while feeling the forces acting on the objects: If one moved something in a direction where there was very little force acting on it in the simulated world, the manipulation device would offer little resistance to the user movement while it would be physically hard to move something where there would be strong forces acting on it. One example of use of this device was moving simulated molecules around while feeling the forces acting on them.</p>

<p>Another system showed by Brooks was Walkthrough which was used to let users (architects and/or their clients) "walk" through the rooms of an unbuilt building. Computer screens show the colored 3D scene which a person would see while walking through the building (doors, walls, floors, etc.) and change in real time. Users may move either by "flying" through the building with a joystick or by actually walking on an exercise treadmill, which is a fairly unusual input devices for a computer system. It is not known which of these two actually gives users the most realistic feeling of moving through a building. I do know that I would have liked to try the treadmill interface if it had been demoed at CHI instead of just shown on a video tape. One of the practical points of this simulation is that they had to program it so as not to allow users to walk through walls without using the doors.</p>

<p>Brooks also discussed an interaction technique problem of more fundamental interest which he called the <strong> two-cursor problem</strong>. The problem is that one wants to have one cursor to specify the commands and one cursor to specify the operands (where one wants the commands to act). This problem is even known in text processing where one has both a pointing cursor for use in scrolling, pulling down menus, etc. and an insertion point cursor for use in placing keyboard input on the screen (my own studies of novice Macintosh users indicate that the two-cursor problem in dealing with text in graphics programs such as MacPaint is one of the harder learning difficulties to overcome). But in 3D interactive graphics the problem becomes worse and cause the user to loose visual continuity and placement in the virtual world each time they have to go to the menu. One of the best solutions to the two cursor problem according to Brooks is that of using command-keys as substitutes for picking commands with the mouse.</p>

<p>Today graphics is mainly used for communicating already established results to others, and especially flashy graphics is often used in reports to funding agencies. Brooks wanted to move to using graphics to generate the insights in the first place. As an example, they had produced a video tape with 40 different visualizations of a molecule and found that they were useful for different kinds of insights. So the goal should not be to find the one best visualization but rather to let scientists build many different visualizations from the same data set.</p>

<p>Brooks also commented on the nature of research results: Some may be useful but too generalized to be 100% true, while others could well be true but too narrow to be useful. He felt that the very essence of a user interface was the conceptual integrity of the whole, meaning that atomized research results might not be useful for designing interfaces.</p>

<h2>Command Reuse</h2>

<p>In today's plastic society, "reuse" has a nice sound to it. Reusing the history of an interaction has the potential for helping users be more efficient in their computer usage and is at least reasonably easy to achieve in a command-based system. History reuse is somewhat more difficult in a direct manipulation interface, but to even begin designing such a gesture reuse principle, we need to have a deeper insight into what reuse is in the first place. Some interesting work on such a conceptual understanding was presented by Yiya Yang from the Scottish HCI Centre in Edinburgh.</p>

<p>Unfortunately her model was presented in terms of a quite compact notation which is no doubt very useful in writing papers but which was fairly hard to follow at first sight. Yang <strong> classified commands </strong> as <em> primitive</em>, <em> compound </em> (several commands issued together), <em> macro </em> (a named sequence of primitives and/or compounds) and <em> meta </em> (a command acting on other commands for the purpose of reuse or user recovery of an earlier state). She then proceeded to classify different kinds of undo and command reuse together with the formal criteria for when they can be used. After the presentation of a quite large number of different kinds of undo/reuse commands, I asked Yang which of these functions actually would be useful to ordinary users who would not want to have to analyze the effects of their actions according to a complex model. The answer was that studies were currently being conducted but that results were not yet available.</p>

<p>This conceptual study of undo/reuse is the kind of work which we need more of in the HCI field. Agreed, it is not a comprehensive model which can explain/predict an entire user-system interaction, but it is a nicely packaged piece of insight which may help in the design of a part of a system and which one can look at when one needs it.</p>

<p>The other paper in the reuse session was an empirical study of the <strong> history mechanism in Unix </strong> and was presented by Saul Greenberg from the University of Calgary in Canada. Greenberg started by reviewing the advantages of reuse: Cognitively, it is easier to recognize how a command looks from a history list than to have to remember yourself how it should be constructed, and motorically, it is easier to select/click on a command than to enter it. The reason history mechanisms have some hope of success is that most actions are really repetitions of previous ones. As an example, Greenberg referred to telephone use where most of the numbers one dials have been dialed before by the same user even though there are millions of potential telephone numbers to choose from.</p>

<p>In his empirical study of Unix, Greenberg found that <strong> between 68% and 80% of the command lines entered by users had been entered before in exactly the same form </strong> for the entire line. Of course, in some cases one will have to reach a substantial distance back in the history list to get a line for reuse, but because of locality of reference, about half of all possible reusable command lines could be made available by showing users a list of only the last five lines. Interestingly enough, the command line most often reused was not the one immediately before the line being entered but rather the second command before it. The Unix system studied did provide a history mechanism for users to reuse their commands but it was used by only 20% of the novice users. 92% of expert users did use it at least once but only about 4% of their commands were reused even though they could theoretically have reused 44% just by selecting from their last five commands.</p>

<h2>A Far-Out Scenario (Hypertext)</h2>

<p>CHI had a panel on hypertext which was to some extent a rehash of discussions at the <a href="../trip-report-hypertext-87/index.php"> Hypertext'87</a> workshop in North Carolina a few months before. Gerhard Fischer from the University of Colorado was co-chairing the panel and asked whether hypertext would enhance or reverse the trend in the U.S. to decreasing reading skills in the general public, but unfortunately this very important question was not really addressed in the ensuing discussion. .;William P. Jones from the AI section of Arthur D. Little Inc. warned us against believing in getting to the "Nelson vision" of immense, integrated hypertext. He compared with the fields of natural language processing, intelligent tutoring systems, and voice recognition and asked rhetorically what we have heard from these fields lately. The general problem is that it is hard to scale up from small systems to large-scale applications.</p>

<p>Randy Trigg from Xerox PARC was working on a <strong> guided tour </strong> card for NoteCards which was intended to let authors guide the user's path through a hypertext network by "holding the user's hand a little bit". A guided tour should not be just a pass through the network, however, but should include annotation and special explanation.</p>

<p>Much of the discussion of hypertext in the corridors at this conference centered around Apple's new HyperCard product, so of course Steve Weyer from Apple mentioned HyperCard but said that it did not come out of the hypertext tradition but rather was a prototyping and programming environment which also had some hypertext capabilities.</p>

<p>Alan Kay (also from Apple) discussed the original paper by Vannevar Bush from 1945 in which he proposed a kind of hypertext system. Bush was one of the computer pioneers and knew a lot about the computer technology available in 1945 but he knew next to nothing about photography. So his dream was about a photography-based information system rather than of a desktop computer. We are now getting the kind of functionality Bush dreamed about, but implemented on computer systems rather than on microfilm. From this story, Kay concluded that the stuff which we know most about is probably where we are most inaccurate in predicting the future since this is where we see all the problems.</p>

<p>Kay then showed a video tape narrated by John Sculley (the president of Apple) with a scenario of how HyperCard might look in 1992. I also saw a Kay-Sculley video of a version of the <strong> Knowledge Navigator </strong> set even farther in the future and I will comment on both tapes together. Both tapes assumed screens with laserprinter-like resolution capable of showing color video which is probably a reasonable assumption. Both tapes also included the presence of a user agent on the screen in the form of a "butler" complete with bow tie. In the 1992 video the agent was a set of animated line drawings capable of only a few facial expressions while the far-future tape had a live video image of a "simulated" person. The intention was that the user communicates with the agent in more or less everyday natural language while the agent then orders the rest of the computer to do what is necessary.</p>

<p>It is probably extremely unlikely that this kind of natural language understanding agent will be available in 1992, even though the tape did show the agent saying "I do not understand" at some point where the user was not very clear. Of more fundamental interest for user interface scientists is whether users would like to communicate with their computer in spoken language at all. In many cases one could imagine that users would be more comfortable with using a combination of gesture-oriented direct manipulation and menu/command/form-filling dialogues to directly operate the computer rather than going through an intermediary agent. On the other hand, I must admit that "speech I/O" was the absolute top scorer in the small survey I conducted of 57 Danish computer professionals, asking them to list the most important probable changes in user interfaces in the year 2000 compared with 1986.</p>

<p>What makes these videos extremely interesting is not so much whether one agrees or not with some of the individual predictions about a user interface of the future but rather that somebody has done the job of making a set of extremely professionally produced <strong> scenarios</strong>. It is much easier to discuss the potentials of ideas such as agents and technologies such as high-resolution displays and high-bandwidth networks on the basis of concrete scenarios than based purely on theoretical writing. In general, I advice people to use scenarios because they form part of the method for doing human factors work cheaply, but these scenarios must have cost plenty to design and produce. Even so, I would like to recommend that other research centers try to produce scenarios of their visions of the future so that we can get some debate going about in which directions we would want to pull technology.</p>

<h2>Research on the Fast Track (Nicholas Negroponte)</h2>

<p>I normally list the MIT Media Lab as one of the top 5 research centers in the world in the user interface area, but unfortunately their activity mostly lies in inventing new things and not in communicating their results to other people. For example, they present next to none papers at CHI and only very few at SIGGRAPH. This year we had succeeded in getting Nick Negroponte who is the leader of the Media Lab to agree to give one of the invited lectures at CHI and he did give a nice survey of much of the exciting work they have been doing.</p>

<p>Negroponte started by saying that just 10 years ago user interface work was regarded as "sissy computer science". In 1977 they had developed the Spatial Data Management System with a kind of desktop metaphor to let people remember where things are based on where they are placed on the screen. Negroponte had shown slides of this system at conferences and received quite harsh remarks: "Who would want to put a calculator on a screen?". Now the user interface area has been widely recognized and they are 150 people at the Media Lab divided about equally between applications driven work and the "technical imperative" of advancing computer science. The work is 80% funded by industry since Negroponte has found that it is much easier to get money from industry than from government grants.</p>

<p>One of the media they are working with is <strong> speech</strong>. Negroponte is a real speech fan and claimed that many of the people in SIGGRAPH are just wasting their time in image rendering and would serve the user interface better by working on speech instead. Many people think that speech I/O has to be natural language but he wanted to decouple these two techniques to enable use of speech now. He said that speech is really a very long arm which allows you to interact with objects at a distance and it is also often a channel that is unused while the hands and eyes are otherwise occupied. The most important challenge is to get connected rather than discrete speech recognition, while speaker-dependence (which is far easier to achieve than independence) is OK according to Negroponte because that is what one wants to do anyway with personal computers. If you are in a phone booth wanting to talk to an airline computer, Negroponte recommended that you just call your own computer and let it talk to the airline computer in ASCII. Of course there are tremendous integration and protocol problems to be solved before one can really do this.</p>

<p>Besides speech, the Media Lab is of course also working on vision which they think is one of the ways to get computer-human interaction to approximate human-human interaction. One of the limitations of computers currently is that the machine doesn't know the difference between when you just lift your hands off the keyboard to pause or when you go to have lunch. They built a simple vision system to let the computer know if the user was there or not. The long term goal would be recognition of facial expressions but currently the computer will just react as you leave it by using larger and larger letters on the screen so that you can still read it.</p>

<p>A lively discussion ensued after Negroponte had discussed these and other systems. Ben Shneiderman was <strong> mystified by the human-human interaction metaphor </strong> and claimed that <strong> mature technology does not just mimic nature</strong>: Airplanes do not look like birds and flap their wings. Instead Shneiderman wanted to empower users by going beyond the limitations of human-human communication. Negroponte's answer was that we are dealing with a totally different kind of problem than mechanistic design because we are more close to the cognitive abilities of humans and that we are not nearly a mature technology anyway. If we wanted another metaphor, we could take his dog which can recognize the tone of his voice.</p>

<p>Bill Buxton (Xerox EuroPARC) said that cognitive perspectives should be brought more into play and one should not just look at the sensory pattern of communication. He found the Media Lab's work to be compelling and full of wonderful ideas but criticized them for never performing an articulating analysis of the results of their work. Negroponte agreed that they had not faced up to the cognitive and AI side of interactions. He said that they got a lot of critique for just building products and then not testing them but that this method of work was intentional: It is almost enough if Buxton finds the product compelling - Negroponte wanted big changes and systems that got people to think differently. If one needs testing to prove one's point, then it is probably not worth doing. As the answer to another comment that we should not rule out cheaper pencil-and-paper exercises, Negroponte said that the user interface area is so important that expense should not be an issue. The only way we can convince people about our ideas is by <em> showing </em> them. As an example, he pointed to new cars which people would not buy from a paper-and-pencil description-they want to test drive them.</p>

<h2>Guided Learning for Children</h2>

<p>Later at CHI an example was shown of practical use of some of the ideas from the Media Lab. Back in 1978, Andy Lippman produced the "Movie Map" videodisk of Aspen which allowed users to travel around the streets of this town on the computer, making right or left turns at will at any intersection and have the screen show photos of what one would see if one was actually driving around Aspen. This was a powerful demonstration but not really a useful application. Now Kathleen S. Wilson from Bank Street College of Education in New York presented the <strong> Palenque </strong> interactive videodisk which seemed to have real educational value in teaching children about Mexican archeology.</p>

<p>In Palenque, the user/child goes on a simulated voyage to an ancient Maya site and can move around it in the same way as in the Aspen system. One can also move up the stairs of the pyramids and then scan the panoramic view from the top. Users can personalize the interaction by taking "snapshots" of the screen which are then pasted into a simulated album.</p>

<p>In contrast to the Media Lab method, Bank Street did use testing and iterative design in the development of Palenque. One thing they found when asking children how they would like to visit an ancient Maya site in the jungle was a strong feeling that they would not like to do so alone. So they used the video medium to provide users with companionship and guidance in the form of filmed characters from the TV show The Second Voyage of the Mimi.</p>

<p>Palenque is implemented using the DVI CD-ROM encoding which is capable of compressing moving video images to a very great extent. This compression introduces some blur in some cases and a person who has more experience with digital video than myself later told me that he had indeed noticed a few such problems with the video clips from Palenque. I did not notice any problems myself except from the general lower quality of the NTSC standard compared to PAL.</p>

<h2>SIG Business</h2>

<p>During the SIGCHI business meeting it was announced that we are the second fastest growing special interest group of the ACM. The software engineering group is growing even faster. One of the issues discussed at the meeting was whether or not to initiate publication of a new <cite> ACM Transactions on Computer-Human Interaction </cite> . On the one hand, the extreme importance of our field certainly warrants making a journal available to the ACM membership but on the other hand, several journals published by independent publishers already exist and starting a new one backed by the ACM might seem as unfair competition with those who took the risks when the user interface field was not as recognized as it is today.</p>

<p>A more general problem mentioned was that the proliferation of journals and conferences in the user interface field risks diluting the field because there is simply not that much good work being done. That there is some truth to this could be seen even at CHI which is the most prestigious event in the field. The CHI conference would have been much better if it had included, say the three best papers from the <a href="../trip-report-hypertext-87/index.php">Hypertext'87</a> workshop in North Carolina in November 1987 and the five best papers from the Computer-Supported Cooperative Work conference in Portland in September 1988. Instead we have two additional conferences with a substantial overlap in subject and audience with the CHI conference.</p>
</section>
  

  
  <section>
    
        
    
  </section>
  
  
  
  
  

  


  <section>
    <br />
    <p class="share"><b>Share <span class="hide-for-small-down">this article</span>:</b> <a href="https://twitter.com/intent/tweet?url=http://www.nngroup.com/articles/trip-report-chi-88/&amp;text=CHI'88%20Trip%20Report&amp;via=nngroup">Twitter</a> | <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=http://www.nngroup.com/articles/trip-report-chi-88/&amp;title=CHI'88%20Trip%20Report&amp;source=Nielsen%20Norman%20Group">LinkedIn</a> | <a href="https://plus.google.com/share?url=http://www.nngroup.com/articles/trip-report-chi-88/">Google+</a> | <a href="mailto:?subject=NN/g Article: CHI&#39;88 Trip Report&amp;body=http://www.nngroup.com/articles/trip-report-chi-88/">Email</a></p>
    <hr />
  </section>

  <footer class="l-subsection">
    
    
    <div class="learn-more">
      <h2>Learn More</h2>
      <p><a href="../subscribe/index.php">Subscribe to the weekly newsletter</a> to get notified about future articles.</p>
      </div>
    
    

    <div class="row show-for-small-down related-content">
      
      
      

      
      
        <div class="column small-12 topic-links">All articles about:
          <ul>
            
            <li><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction</a></li>
            
          </ul>
        </div>
      
      

      
      
        <div class="column small-12">
          <h3>UX Conference Training Courses</h3>
           <ul class="no-bullet">
    
        <li><a href="../../courses/ux-basic-training/index.php">UX Basic Training</a></li>
    
        <li><a href="../../courses/hci/index.php">User Interface Principles Every Designer Must Know</a></li>
    
        <li><a href="../../courses/interaction-design-3-day-course/index.php">Interaction Design: 3-Day Course</a></li>
    
</ul>
        </div>
      
      

      
      
      

      
      
      
    </div>

    <div class="hide-for-small-down row related-content">
      <div class="medium-6 columns">
        
        
          <div>
            <h3>Articles</h3>
            <ul class="no-bullet">
              
                
                <li><a href="../embarrassment/index.php">Computer-Assisted Embarrassment</a></li>
                
              
                
                <li><a href="../enhancement/index.php">The Role of Enhancement in Web Design</a></li>
                
              
                
                <li><a href="../lazy-users/index.php">Why Designers Think Users Are Lazy: 3 Human Behaviors</a></li>
                
              
                
                <li><a href="http://www.jnd.org/dn.mss/apples_products_are.php">Apple&#39;s products are getting harder to use because they ignore principles of design</a></li>
                
              
                
                <li><a href="../efficiency-vs-expectations/index.php">Don’t Prioritize Efficiency Over Expectations</a></li>
                
              
            </ul>
          </div>
        
        

        

      </div>
      <div class="medium-6 columns">
        
          
          <div>
            <h3>UX Conference Training Courses</h3>
             <ul class="no-bullet">
    
        <li><a href="../../courses/ux-basic-training/index.php">UX Basic Training</a></li>
    
        <li><a href="../../courses/hci/index.php">User Interface Principles Every Designer Must Know</a></li>
    
        <li><a href="../../courses/interaction-design-3-day-course/index.php">Interaction Design: 3-Day Course</a></li>
    
</ul>
          </div>
          
        
        
      </div>
    </div>
  </footer>
</article>

        </div>

      

    
  </div>
  <div class="row">
    
      <div class="column small-12 show-for-small-down l-navrail announcements">
        
          
        
      </div>
    
    
  </div>

    </div>
    




<footer id="page-footer">
    <div class="row hide-for-print">
        <div class="column small-12 medium-6">
            <section class="footer-popular">
                
                <h1>Popular Topics</h1>
                <div style="float: left;  width: 15em; display: block;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/applications/index.php">Application Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/e-commerce/index.php">E-Commerce Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/eyetracking/index.php">Eyetracking </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/information-architecture/index.php">Information Architecture </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/intranets/index.php">Intranet Design </a></li>
	<li><a href="../../topic/interaction-design/index.php">Interaction Design </a></li>
</ul>
</div>

<div style="float: left;  width: 15em; display: block; list-style: none;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/mobile-and-tablet-design/index.php">Mobile and Tablet Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/credibility-persuasion/index.php">Persuasive Design</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/research-methods/index.php">Research Methods</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/user-testing/index.php">User Testing </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/web-usability/index.php">Web Usability </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/writing-web/index.php">Web Writing &amp; Content Strategy </a></li>
</ul>
</div>

<p>&nbsp;</p>

                
            </section>
        </div>

        <div class="column small-12 medium-6">
            <div class="row">
                <div class="column small-6">
                    <section class="footer-about">
                        <h1>Follow Us</h1>
                        <ul>
	<li><a href="../subscribe/index.php">Subscribe to our weekly UX newsletter</a>,&nbsp;Jakob Nielsen&#39;s Alertbox</li>
	<li>Visit Don Norman&#39;s website, <a href="http://www.jnd.org/"> www.jnd.org </a></li>
	<li>Visit Bruce Tognazzini&#39;s website, <a href="http://www.asktog.com/"> www.asktog.com </a></li>
	<li style="padding-top: 6px;"><a class="twitter-follow-button" data-show-count="false" data-size="large" href="https://twitter.com/nngroup"><img alt="Follow @nngroup on Twitter" height="20" src="https://media.nngroup.com/media/editor/2016/12/30/twitter-follow-button.png" style="width: 117px; height: 20px;" width="117" /></a></li>
	<li style="padding-top: 6px;"><img alt="" height="14" src="https://media.nngroup.com/media/editor/2016/07/26/feed-icon-14x14.png" style="vertical-align:text-top;width:14px;margin-right:.25em;" width="14" /><a href="../../feed/rss/index.php">Subscribe to our RSS feed</a></li>
</ul>

                    </section>
                </div>

                <div class="column small-6">
                    <section class="footer-about right-column">
                        <h1>About
</h1>
                        <ul class="no-bullet">
	<li><a href="../../about/contact/index.php">Contact information</a></li>
	<li><a href="../../about/index.php">About Us</a></li>
	<li><a href="../../about/why-nng/index.php">Why NN/g? </a></li>
	<li><a href="../../return-policy/index.php">Return Policy</a></li>
</ul>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="copyright column small-12">
            <p><a href="../../copyright-and-reprint-info/index.php">Copyright</a> &copy; 1998-2017 Nielsen Norman Group, All Rights Reserved.</p>
        </div>
    </div>
</footer>


    


        <script src="https://media.nngroup.com/static/vendor/fastclick/js/fastclick.min.js"></script>
        <script src="https://media.nngroup.com/static/vendor/foundation/js/foundation.min.js"></script>
        <script src="http://media.nngroup.com/static/js/app.js"></script>
        <script src="http://media.nngroup.com/static/js/scroll-to-fixed.js"></script><script>var externalLinks = document.querySelectorAll('a[href^="http"]');for (var i = externalLinks.length-1; i >= 0; i--) {externalLinks[i].addEventListener("click", function(event) { alert("External links are disabled for this test."); event.preventDefault(); }, false);}var mailLinks = document.querySelectorAll('a[href^="mailto"]');for (var i = mailLinks.length-1; i >= 0; i--) {mailLinks[i].addEventListener("click", function(event) { alert("Email links are disabled for this test."); event.preventDefault(); }, false);}var externalForms = document.querySelectorAll('form');for (var i = externalForms.length-1; i >= 0; i--) {externalForms[i].addEventListener("submit", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false); externalForms[i].addEventListener("click", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false);}</script>

        <script>
            
            $(function() {
                $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
                $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
            });
        </script>

        
<script src="https://media.nngroup.com/static/js/articles.js"></script>


        
        <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1072699960;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072699960/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
    </body>

<!-- Mirrored from www.nngroup.com/articles/trip-report-chi-88/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:14:37 GMT -->
</html>
