<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->





    
<!-- Mirrored from www.nngroup.com/articles/readers-comments-on-linkrot/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:17:22 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1026.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"licenseKey":"1a30ef9931","queueTime":0,"beacon":"bam.nr-data.net","errorBeacon":"bam.nr-data.net","applicationID":"2048992","transactionName":"NFxSMRBUX0EDUkIPDA0WdhAMVkVbDV8ZBxEXUFMJB0YfRAtUQRVZIktEDAFZVHYHRVcPD01eVRE=","applicationTime":334,"agent":""}</script>
        <title>Readers&#39; Comments on Linkrot</title><meta property="og:title" content="Readers&#39; Comments on Linkrot" />
  
        
        <meta name="description" content="Difficulty of getting other sites to update their links when a site moves. Suggested solution: a link-update feedback form.">
        <meta property="og:description" content="Difficulty of getting other sites to update their links when a site moves. Suggested solution: a link-update feedback form." />
        
  
        
	
        
        <meta name="keywords" content="quality of search engines, linkrot, moving site domain, webmaster feedback forms, link updates, SiteDeath, site death">
        

        

        <meta name="robots" content="noarchive">
        <meta name="viewport" content="width=device-width">

        <link rel="search" type="application/opensearchdescription+xml" title="NN/g Search" href="../../opensearch.xml" />
        <link rel="shortcut icon" href="http://media.nngroup.com/static/img/favicon.ico" />

        <link rel="stylesheet" href="https://media.nngroup.com/static/vendor/font-awesome/css/font-awesome.min.css">

        
        <link rel="stylesheet" href="https://media.nngroup.com/static/css/7232a960f1df.css" type="text/css" />
        <link rel="stylesheet" href="http://fast.fonts.com/t/1.css?apiType=css&amp;projectid=c5846260-9def-46ec-9407-0f1349bd2d64" type="text/css">

        <script src="https://media.nngroup.com/static/vendor/modernizr/js/modernizr.min.js"></script>

        <script type="text/javascript" src="https://media.nngroup.com/static/js/3283be5def74.js"></script>
        

        

        
        
        <!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=229034,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('http://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->


<!-- Google Analytics Tracking Code for http://nngroup.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23132681-1', 'nngroup.com');
  ga('require', 'linkid', 'https://www.nngroup.com/articles/readers-comments-on-linkrot/linkid.js');
  ga('require', 'displayfeatures');

// Optimizely Universal Analytics Integration
 window.optimizely = window.optimizely || [];
 window.optimizely.push("activateUniversalAnalytics");

  ga('send', 'pageview');

</script>
<script>
/**
* Function to track a click on an outbound link in Google Analytics.
*/
function trackOutboundLink(label, url) {
    if(window.ga && ga.loaded) {
        ga('send', 'event', 'outbound', 'click', label, {'hitCallback': function(){document.location = url;}
        });
    } else {
        document.location = url;
    }
}
/**
* Function to track a click on a link in the article announcement area.
*/
$('#articleAnnouncement a').on('click', function(){
  ga('send', 'event', 'link', 'click', 'articleAnnouncement');
});
</script>

<script type="text/javascript">
 setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2421.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



<!-- Hotjar Tracking Code for http://nngroup.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:121605,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

        <script src="https://media.nngroup.com/static/js/analytics/track_outbound_link.js"></script>
        
        
        
    </head>
    <body class="location-articles articles-detail">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
    <header id="page-header">
        <div id="cart" class="shopping-cart mobile-cart show-for-small-down"></div>
        <div class="row">
            <div id="masthead" class="masthead small-12 medium-7 column">

                <!-- The button to toggles menu on small screen -->

                <h1><a href="../../index.php"><img src="http://media.nngroup.com/static/img/logo-wordmark.png" width="74" height="30"> Nielsen <span class="norman">Norman</span> Group</a></h1>
                <p class="tagline show-for-medium-up">Evidence-Based User Experience Research, Training, and Consulting</p>

                <!-- The button to toggles form search on small screen -->


                <div style="clear: right;width:  100%;height: 1px;"></div><?php define('ROOT_PATH',$_SERVER["DOCUMENT_ROOT"] . '/apps/navigation_styles/navigations/4'); ?><?php include(ROOT_PATH)."/navigation.php"; ?>

                <!-- Drop down menu on small screen -->
                <div id="hideNav" class="show-for-small-down">
                  
                  <ul class="menu-content">
                    <li><a href="../../index.php">Home</a></li>
                    <li class="has-submenu">
                      <a>Training</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../training/index.php">Overview</a></li>
                        
<li id="nav-ux-conference"><a href="../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../online-seminars/index.php"><span>Online Seminars</span></a></li>

                      </ul>
                    </li>
                    <li class="has-submenu">
                      <a>Consulting</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
  <li><a href="../../consulting/index.php">Overview</a></li>
  
  <li><a href="../../consulting/benchmarking/index.php">Benchmarking</a></li>
  
  <li><a href="../../consulting/expert-design-review/index.php">Expert Design Review</a></li>
  
  <li><a href="../../consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
  
  <li><a href="../../consulting/usability-testing/index.php">Usability Testing </a></li>
  
  <li><a href="../../consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
  
  <li><a href="../../in-house-training/index.php">In-House Training</a></li>
</ul>

                    </li>

                    <li><a href="../../reports/index.php">Reports</a></li>
                    <li><a href="../index.php">Articles</a></li>
                    <li class="has-submenu"><a>About NN/G</a><i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../about/index.php">Overview</a></li>
                        <li><a href="../../people/index.php">People</a></li>
                        <li><a href="../../about/why-nng/index.php">Why NN/g?</a></li>
                        <li><a href="../../about/contact/index.php">Contact</a></li>
                        <li><a href="../../news/index.php">News</a></li>
                        <li><a href="../../about/history/index.php">History</a></li>
                        <li><a href="../../books/index.php">Books</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                </div>

                <hr class="header-divider show-for-small-only"/>


            </div>
       </div>

        <div id="hideNav">            <nav class="primary-nav small-12 column">
                <ul>
                    <li><a class="navitem" id="nav-home" href="../../index.php">Home</a></li>
                    <li><a class="navitem" id="nav-training" href="../../training/index.php">Training</a></li>
                    <li><a class="navitem" id="nav-consulting" href="../../consulting/index.php">Consulting</a></li>
                    <li><a class="navitem" id="nav-reports" href="../../reports/index.php">Reports</a></li>
                    <li><a class="navitem" id="nav-articles" href="../index.php">Articles</a></li>
                    <li><a class="navitem" id="nav-about" href="../../about/index.php">About NN/G</a></li>
                </ul>
            </nav>
            <nav class="secondary-nav small-12 medium-12 large-12 columns">
                
                
            </nav>
        </div>
    </header>
    <div class="l-content">
    
  

  

  <div class="row">
    
      
      <section class="small-12 medium-3 columns collapsable-content l-navrail articles-left-column">
        <h1 class="show-for-small-down collapse-link mobile-browse-header">Browse by Topic and Author</h1>
          <div class="row collapsable">

            
            <div class="small-6 medium-12 columns">
              <section id="article-popular-topics" style="display: none;">
                
                <h1>Topics</h1>
                <ul id="article-popular-topics">
                    
                    <li>
                        
                        <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/intranets/all/index.php">Intranets</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/user-testing/all/index.php">User Testing</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                        
                    </li>
                    
                    <li><a href="#" id="article-see-all-topics"><strong>See all topics&hellip;</strong></a></li>
                </ul>
                
              </section>
              <section id="article-all-topics" style="display: none;">
                
                <h1>
                  <span class="show-for-medium-up">All Article Topics</span>
                  <span class="show-for-small-down">All Topics</span>
                  (<a href="#" id="article-hide-all-topics">hide</a>)
                </h1>
                <ul>
                  
                  <li>
                    
                    <a href="../../topic/accessibility/all/index.php">Accessibility</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/agile/all/index.php">Agile</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/analytics-and-metrics/all/index.php">Analytics &amp; Metrics</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/applications/all/index.php">Application Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/b2b-websites/all/index.php">B2B Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/behavior-patterns/all/index.php">Behavior Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/branding/all/index.php">Branding</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/content-strategy/all/index.php">Content Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/corporate-websites/all/index.php">Corporate Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/design-patterns/all/index.php">Design Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-design-process/all/index.php">Design Process</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/email/all/index.php">Email</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/eyetracking/all/index.php">Eyetracking</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/heuristic-evaluation/all/index.php">Heuristic Evaluation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/human-computer-interaction/all/index.php">Human Computer Interaction</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-ideation/all/index.php">Ideation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/information-architecture/all/index.php">Information Architecture</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/interaction-design/all/index.php">Interaction Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/international-users/all/index.php">International Users</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/intranets/all/index.php">Intranets</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-management/all/index.php">Management</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/navigation/all/index.php">Navigation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/non-profit-websites/all/index.php">Non-Profit Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/personas/all/index.php">Personas</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/persuasive-design/all/index.php">Persuasive Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/prototyping/all/index.php">Prototyping</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/psychology-and-ux/all/index.php">Psychology and UX</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/research-methods/all/index.php">Research Methods</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/search/all/index.php">Search</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/social-media/all/index.php">Social Media</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/strategy/all/index.php">Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/user-testing/all/index.php">User Testing</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-teams/all/index.php">UX Teams</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/visual-design/all/index.php">Visual Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/writing-web/all/index.php">Writing for the Web</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/young-users/all/index.php">Young Users</a>
                    
                  </li>
                  
                </ul>
                
              </section>
            </div>
            <script>
              /* Determine which topics list will be displayed: the full or the
               * partial.  We are inlining this close to the list itself to
               * prevent rendering delays on the client
               */
              var topic_list = $.cookie("article-topics");

              var popular_topics = $('#article-popular-topics');
              var all_topics = $('#article-all-topics');

              if(topic_list == 1) {
                  popular_topics.hide();
                  all_topics.show();
              } else {
                  popular_topics.show();
                  all_topics.hide();
              }
            </script>
            

            
            <div class="small-6 medium-12 columns">
              
              <section id="article-initial-authors">
                  <h1>Author</h1>
                  <ul>
                    
                    <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                    
                    <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                    
                    <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                    
                    <li><a href="#" onClick="$('#article-initial-authors').hide(); $('#article-all-authors').show();"><strong>See all authors&hellip;</strong></a></li>
                  </ul>
              </section>
              <section id="article-all-authors" style="display: none;">
                <h1>
                  All Authors
                  (<a href="#" id="article-hide-all-authors">hide</a>)
                </h1>
                <ul>
                  
                  <li><a href="../author/raluca-budiu/index.php">Raluca Budiu</a></li>
                  
                  <li><a href="../author/patty-caya/index.php">Patty Caya</a></li>
                  
                  <li><a href="../author/susan-farrell/index.php">Susan Farrell</a></li>
                  
                  <li><a href="../author/therese-fessenden/index.php">Therese Fessenden</a></li>
                  
                  <li><a href="../author/kim-flaherty/index.php">Kim Flaherty</a></li>
                  
                  <li><a href="../author/sarah-gibbons/index.php">Sarah Gibbons</a></li>
                  
                  <li><a href="../author/aurora-harley/index.php">Aurora Harley</a></li>
                  
                  <li><a href="../author/kate-kaplan/index.php">Kate Kaplan</a></li>
                  
                  <li><a href="../author/page-laubheimer/index.php">Page Laubheimer</a></li>
                  
                  <li><a href="../author/angela-li/index.php">Angie Li</a></li>
                  
                  <li><a href="../author/hoa-loranger/index.php">Hoa Loranger</a></li>
                  
                  <li><a href="../author/kate-meyer/index.php">Kate Meyer</a></li>
                  
                  <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                  
                  <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                  
                  <li><a href="../author/kara-pernice/index.php">Kara Pernice</a></li>
                  
                  <li><a href="../author/christian-rohrer/index.php">Christian Rohrer</a></li>
                  
                  <li><a href="../author/amy-schade/index.php">Amy Schade</a></li>
                  
                  <li><a href="../author/katie-sherwin/index.php">Katie Sherwin</a></li>
                  
                  <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                  
                  <li><a href="../author/kathryn-whitenton/index.php">Kathryn Whitenton</a></li>
                  
                </ul>
              </section>
              
            </div>
            
          </div>

          
          <section class="hide-for-small-down">
            
              
              <h1>Recent Articles</h1>
              
              <ul>
                  
                  <li><a href="../seamless-cross-channel/index.php">Seamlessness in the Omnichannel User Experience</a></li>
                  
                  <li><a href="../brand-intention-interpretation/index.php">Experience Design: Bridging Brand Intention and Brand Interpretation</a></li>
                  
                  <li><a href="../eyetracking-tasks-efficient-scanning/index.php">Scanning Patterns on the Web Are Optimized for the Current Task</a></li>
                  
                  <li><a href="../flat-design-best-practices/index.php">Flat-Design Best Practices</a></li>
                  
                  <li><a href="../ux-user-stories/index.php">Accounting for UX Work with User Stories in Agile Projects</a></li>
                  
                  <li><a href="../index.php"><strong>See all articles&hellip;</strong></a></li>
              </ul>
              
            
          </section>
          

          
          
          <section class="hide-for-small-down">
            <h1>Popular Articles</h1>
            
            <ul>
                
                <li><a href="../usability-101-introduction-to-usability/index.php">Usability 101: Introduction to Usability</a></li>
                
                <li><a href="../top-10-mistakes-web-design/index.php">Top 10 Mistakes in Web Design</a></li>
                
                <li><a href="../how-users-read-on-the-web/index.php">How Users Read on the Web</a></li>
                
                <li><a href="../f-shaped-pattern-reading-web-content/index.php">F-Shaped Pattern For Reading Web Content</a></li>
                
                <li><a href="../ten-usability-heuristics/index.php">10 Usability Heuristics for User Interface Design</a></li>
                
                <li><a href="../intranet-design-2016/index.php">10 Best Intranets of 2016</a></li>
                
                <li><a href="../which-ux-research-methods/index.php">When to Use Which User-Experience Research Methods</a></li>
                
                <li><a href="../response-times-3-important-limits/index.php">Response Times: The 3 Important Limits</a></li>
                
                <li><a href="../why-you-only-need-to-test-with-5-users/index.php">Why You Only Need to Test with 5 Users</a></li>
                
                <li><a href="../page-fold-manifesto/index.php">The Fold Manifesto: Why the Page Fold Still Matters</a></li>
                
            </ul>
          </section>
          
          

          
            <section class="hide-for-small-down">
              
            </section>
          

      </section>
      

      
        <div class="small-12 medium-9 columns content-body">
          




<article class="l-subsection alertbox-article-content article-detail-bl">
  
  <header>
    <div class="l-subsection">
      <h1>Readers&#39; Comments on Linkrot</h1>
      <div class="author-meta">by
        
          
            
            <a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a>
            
          
        on  June 14, 1998
        
      </div>

      

      
      <div class="topic-links">
        Topics:
        
          <ul>

</ul>
        
      </div>
      
    </div>
  </header>
  

  
  
  

  
  <section id="article-body"><p>
 <em>
  Sidebar to
  <a href="../../people/jakob-nielsen/index.php" title="Author biography">
   Jakob Nielsen
  </a>
  &#39;s column on
  <a href="../fighting-linkrot/index.php">
   linkrot
  </a>
  .
 </em>
</p>
<p>
 I have received some interesting user comments on my June Alertbox on linkrot.
</p>
<h2>
 Old Links Keep Propagating
</h2>
<p>
 <em>
  Karl M. Bunday writes:
 </em>
</p>
<blockquote>
 Your Alertbox
 <em>
  &quot;Fighting Linkrot&quot;
 </em>
 came out just as I was engaging in a laborious procedure to fight linkrot--notifying more than 100 Web masters, over two days, by E-mail about the current URL for my site, formerly hosted on first one, then another commercial on-line service. My
 <cite>
  School Is Dead, Learn in Freedom!
 </cite>
 site has had its domain name and thus own URL,
 <code>
  http://learninfreedom.org/
 </code>
 since mid-December 1997. Trying to find ways to alert webmasters of sites that link to my earlier sites about my new URL helped me discover some of the reasons linkrot is such a persistent and growing problem.
 <p>
  My very earliest Web presence began in early 1995. A pioneer in on-line commerce offered me some space on his site to post some of my FAQ files and gave me my own directory on his server. It didn&#39;t take me long at all to see that my link from a famous virtual library page about my site&#39;s subject had been copied on to several other Web sites, and the Yahoo link was very widely copied. I think the Yahoo code is how my site became known to the
  <cite>
   Point Survey Top 5 Percent Award
  </cite>
  people when I won their award.
  <strong>
   I have not publicized that site for two years now, but it still gets new links set up to it all the time
  </strong>
  . That site has been defunct since about November 1997--just as it won a favorable review in that month&#39;s issue of
  <cite>
   Yahoo Internet Life
  </cite>
  magazine. Many people writing Web pages copy links they see on other sites and put those links on their own sites. Links propagate by plagiarism more than they propagate by bookmarking.
 </p>
 <p>
  In January of 1996 I got my own site on a commercial on-line service with an awkward, too-long domain-directory name. That site&#39;s provider didn&#39;t give me access statistics. By March of 1997 I had set up yet another commercial on-line service directory as my site, and that provider gave me access statistics. As soon as I tested the second site, I put up &quot;moving notices&quot; on the earlier commercial on-line service site to send people to the new site. Quite a few people came over to the second site. By December of 1997 a friendly site master who shares my subject-matter interest helped me set the
  <cite>
   School Is Dead, Learn in Freedom!
  </cite>
  site with my own URL,
  <code>
   learninfreedom.org
  </code>
  , and I then changed my &quot;moving notices&quot; and began heavily publicizing my permanent domain name.
 </p>
 <p>
  But as I write this, there are
  <strong>
   still many more links, according to AltaVista, to old URLs of my site than to the current URL
  </strong>
  . I have been visiting those linking sites (and also visiting referring sites that appear in the March 1997 site&#39;s access logs) to tell the webmasters to update their links. I am
  <em>
   appalled
  </em>
  at how often I find pages, for example, by college students that purport to be original works of research that are actually stolen from a professional association, or family site link pages that copy Yahoo&#39;s directory pages verbatim, or other pages that engage in blatant plagiarism. Surely any page written since November 1997 that links to my very earliest Web site is entirely in error, and was probably written by a webmaster who didn&#39;t visit links before posting them. I did a first round of notifying webmasters back in February 1998, and now in June 1998 I have done another round, only to see many new sites appear with my oldest, long obsolete link.
 </p>
 <p>
  Search engine behavior only exacerbates this problem. I agree, after thinking about it, with your suggestion that my two earlier Web sites that provide only &quot;moving notices&quot; now should be kept up as long as possible, even though I&#39;m paying for them out of a too-limited budget. But the
  <strong>
   search engines sometimes rank those obsolete sites
   <em>
    higher
   </em>
   in well-constructed searches
  </strong>
  (even after I&#39;ve pared down those pages to being just redirects) than my current site. That&#39;s crazy. There ought to be a universally observed search engine adherence to the META robots tag so that
  <code>
   &lt;META NAME=&quot;robots&quot; CONTENT=&quot;noindex&quot;&gt;
  </code>
  really made sure that a page didn&#39;t get indexed--I have had all my old obsolete pages marked that way for a long time, but they still get indexed and reindexed. It would be better still to develop a new LINK tag that would inform compliant robots (which all robots should be) that a page was obsolete and replaced by a new page, something like
  <code>
   &lt;LINK REV=&quot;dead&quot; REL=&quot;current&quot; HREF=&quot;http://learninfreedom.org/&quot;&gt;
  </code>
  or something like that.
 </p>
</blockquote>
<p>
 <em>
  Jakob&#39;s reply:
 </em>
 It is indeed a pain to get other sites to update their listings. Currently, the only two approaches are
</p>
<ul>
 <li>
  Manual email to the webmaster, pointing out the obsolete link. A large site gets so much email that they often don&#39;t deal with it (indeed, it is one of the challenges in running a large site to set up processes for handling email)
 </li>
 <li>
  Hoping that the other site runs a link validator that notices redirects and causes the links to be updated with the new URLs
 </li>
</ul>
<p>
 Neither approach is very effective. In the long term, I hope for an automated solution that would allow the owner of a site to send an standardized message (digitally signed, of course) to an automated agent at the remote webserver that would cause the server to update the obsolete URLs with the new ones. Message authentication is necessary to prevent link kidnapping where competitors causes links to go to their site.
</p>
<p>
 A recent study found that the main search engines had between 2% and 5% linkrot. This is less than the Web average of 6% but still too much for comfort. Search engines should focus more on their core competency and clean up their databases to remove dead links and update redirected links as soon as possible. Many other steps are necessary to enhance search usability: for example, search engines could record what links users actually follow when presented with a hitlist for a query term and then promote these useful links to a more prominent position for the next user.
</p>
<p>
 As a short term solution, if you run a large site, consider
 <strong>
  implementing a special subpage under your feedback area for reporting link updates
 </strong>
 . This page should contain standard fields to report the page on your site that needs change, the old URL, the new URL, and a contact email in case of problems. You can then automatically retrieve the old and the new URL and present your Web gardener with a view of three pages: your own page (highlighting the link that might need change) as well as the result of retrieving the old and the new URL. If everything seems to be OK (i.e., not a link kidnap), then the gardener could click a single button to update the site without the need for any manual HTML editing.
</p>
<p>
 <em>
  Alan Levine writes:
 </em>
</p>
<blockquote>
 There is a nifty, well maintained site called &quot;
 <a href="http://disobey.com/ghostsites/">
  Ghostsites
 </a>
 &quot; that chronicles once-hyped web sites that now R.I.P.
 <p>
  We&#39;ve been combing our error logs and are amazed at the URLs people are attempting to reach:
  <strong>
   sites that have not had an active link in more than 4 years
  </strong>
  and other pages that were never linked from other pages.
 </p>
</blockquote>
<h2>
 Should Old Pages Live Forever?
</h2>
<p>
 <em>
  Ihor Prociuk writes:
 </em>
</p>
<blockquote>
 You stated that:
 <em>
  &quot;Any URL that has ever been exposed to the Internet should live forever:...&quot;
 </em>
 <p>
  I agree that in all instances, pages that have been moved or removed should have, at the very minimum, a link page pointing from the old location to the new one. And any decent webserver/browser should be able to handle redirects and automatic bookmark updating. However if, as you say, &quot;6% of the links on the Web are broken&quot;, after a while, the number of redirection pages could become quite large. (The percentage will not increase at the same rate as the increase in the number of overall links). I&#39;m not sure what impact this would have on search engines. One of the annoying things about search engines is all the broken links they return. You would think they could run &quot;link validators&quot; more often to reduce clutter in their databases and improve overall performance.
 </p>
 <p>
  I think that a time-limited notice (one year?) or redirect would be sufficient for webmasters to update their outbound links. And, if as you suggest, link validators be run on a regular basis, this would catch most of the bad links. Think of a company that is about to move. It makes reasonable efforts to inform its current customers that it is about the change phone numbers. However, after a while, the letterhead just gives the new number, It certainly doesn&#39;t have
  <em>
   &quot;5 years ago our phone number was xxx-xxxx and 2 years ago it was yyy-yyyy, now it&#39;s zzz-zzzz&quot;
  </em>
  . Phone companies take this approach. When a number changes, they tell you the new number. My phone company doesn&#39;t offer an option that would allow me to connect to the new number without redialing. They want me to update whatever record system (addressbook, auto-dialer, etc.) I&#39;m using. If I want to avoid the message, I&#39;ll do the update.
 </p>
</blockquote>
<p>
 <em>
  Jakob&#39;s reply:
 </em>
 Unfortunately, a year is not nearly enough time for redirecting links from old pages to the new ones. Even the search engines seem to be more than a year behind in many cases, despite having substantially more resources and professional staff than the average site. Bunday&#39;s case story above shows that two-year-old links are still rampant.
</p>
<p>
 Luckily, the resources needed for redirections are relatively limited: typically a single line per obsolete URL in a server-configuration file. Thus, it should be possible to keep redirects alive for several years. I do agree that they probably can&#39;t literally live forever, but I advise extreme conservatism in pruning the database of URL redirects. The server log files will keep track of the frequency of access to the old URLs, and I would keep a redirect active as long as the old URL got at least one hit per month. After all, the cost of that extra line in the configuration file
 <em>
  is
 </em>
 very low. Once, say, two months have gone by without any hits, then it may be time to prune the redirect database.
</p>
<h2>
 SiteDeath: The Ultimate Linkrot
</h2>
<p>
 <em>
  Mark Nottingham writes:
 </em>
</p>
<blockquote>
 I had the extreme misfortune to sit through a Sybase marketing session about their involvement in the soccer World Cup today (lots of marketing speak, no content). I won&#39;t even go into the usability issues I spotted when they demo&#39;d the France &#39;98 Web site, but one thing struck me out of the blue:
 <p>
  What&#39;s going to happen to the content?
 </p>
 <p>
  Sybase, EDS and other companies have gone to enormous expense to showcase their abilities in creating an &#39;end-to-end solution&#39;. There are copious amounts of data on the site. It&#39;s getting millions of hits a day, apparently.
  <em>
   But,
  </em>
  when the whole thing is over,
  <strong>
   how long will they keep it up?
  </strong>
  A week? A year? How long before it isn&#39;t profitable, and then what happens?
 </p>
 <p>
  It&#39;s the
  <strong>
   ultimate form of link rot
  </strong>
  .
 </p>
 <p>
  I&#39;ve done a quick survey of
  <strong>
   Olymics sites
  </strong>
  ;
  <code>
   www.atlantagames.com
  </code>
  is no more, but
  <code>
   www.olympic.nbc.com
  </code>
  is still going, as is www.nagano.olympic.org. But as events get more corporatized (such as the World Cup), will we see content disappearing? It&#39;d be a shame to see all of those dead links, as well as losing material for future study of the early days of the Internet.
 </p>
 <p>
  To me, this brings up issues about the ownership of Internet content, and what it engenders in the user base.
 </p>
</blockquote>
<p>
 <em>
  Jakob&#39;s reply:
 </em>
 Many of these showcase sites have horrible usability because they were
 <strong>
  designed to show off the sponsoring company&#39;s technology and not to provide a useful service
 </strong>
 to users. Quite often, they are built by technology companies that don&#39;t have a single usability engineer on staff. An interesting exception is the
 <cite>
  World Cup USA 1994
 </cite>
 which was designed by Darrell Sano: he
 <em>
  does
 </em>
 know what he is doing, as evidenced by the analysis of the project in Sano&#39;s book
 <a href="http://www.amazon.com/exec/obidos/ISBN%3D047114276X/useitcomusableinA/" title="Amazon.com book page: buy at 20% discount">
  Designing Large-Scale Web Sites: A Visual Design Methodology
 </a>
 . The 1994 site is currently preserved at mirror servers in London and Tokyo, even though the main site in California is long gone.
</p>
<p>
 For major sports events, I advise the World Cup organizers and the International Olympic Committee to
 <strong>
  make sponsors sign a contract to keep their sites alive
 </strong>
 for at least ten years. These sites form the major historical presence for the games after the event, so it is sad to see them go. I would also include a contractual requirement for formal usability testing: after all, the organizers&#39; reputation is at stake, and a bad site reflects poorly on their commitment to their fans.
</p>
<p>
 <strong>
  Update added July 2000
 </strong>
 :
</p>
<ul style="margin-top: 0; margin-bottom: 0">
 <li>
  The London mirror of the 1994 World Cup is now gone
 </li>
 <li>
  The Tokyo mirror is still going strong (good work, Japan)
 </li>
 <li>
  The 1998 World Cup site in France is still operational
 </li>
 <li>
  The 1998 NBC Olympics site now redirects to a site for the 2000 Olympics in Sydney - probably fine, but there is no way to get the old content about what happened two years ago
 </li>
 <li>
  The official site for the Nagano Olympics also redirects to a site for the Sydney games - less appropriate in my view
 </li>
</ul>
<p>
 <strong>
  Update added July 2005
 </strong>
 :
</p>
<ul style="margin-top: 0; margin-bottom: 0">
 <li>
  The Tokyo mirror continues to be available
 </li>
 <li>
  The 1998 World Cup site in France gone; the domain is now an advertising site for various soccer-related products
 </li>
 <li>
  NBC Olympic site is now gone (used to be the site for the 1998 winter Olympics)
 </li>
 <li>
  The Nagano Olympics site now redirects to a general site for the International Olympic Committee -- an OK redirect, but at the loss of the original data
 </li>
</ul>
<p>
 Of six sports sites mentioned in 1998, only one retains the original mission and content in 2005.
 <strong>
  83% of the services are no longer available
 </strong>
 . (And we&#39;re talking sites for billion-dollar operations like the Olympics and soccer World Cup.)
</p>
<h2>
 Content Rot
</h2>
<p>
 <em>
  Pavel Podvoiski writes:
 </em>
</p>
<blockquote>
 WWW is rotting, and not only because of the rotten links. Content is rotting at ever greater rate :(
 <p>
  Once designed for sharing information, WWW now seems to be
  <em>
   hiding
  </em>
  that information. I was just pissed off one more time, so I decided to write to you in hope your voice would be heard.
 </p>
 <p>
  Examples:
 </p>
 <ul>
  <li>
   In mid 1996 I visited www.microsoft.com for help in russification (i&#39;m from Russia) of Windows NT 3.51. In few minutes I had found it just by following links.
  </li>
  <li>
   In 1997 I requested this information again (NT died). Surprise!! I can&#39;t find it. After approx. 30 minutes of struggling with &quot;search&quot; engine I found this document again.
  </li>
  <li>
   In 1998, just for curiousity, I repeated this experiment. No surprise, I didn&#39;t find it.
   <strong>
    A lot of buzz and almost no facts
   </strong>
   . BTW, can you tell me, does MS Internet Information Server 4.0 support FTP transfer resume or not? I was unable to figure it out from the MS site. (BTW, can you find IIS info on MS site anymore?)
  </li>
 </ul>
 <p>
  Another example, 3Com this time:
 </p>
 <ul>
  <li>
   In April 1998 we where upgraiding from 10MBit to 100Mbit Ethernet. In five or so clicks i was right there. Info on cards and hubs was right in my hands with useful figures and suggestions.
  </li>
  <li>
   Yesterday, 20-Jan-1999, I visited this site again (company is moving+expanding) and found none, NONE!!!!! useful information, - a lot of &quot;how good we are&quot; and outdated outdated tables with corps of info (when i said outdated it means you trying to find product listed in table and got nothing). Finaly i and my collegue followed links to _dealers_ and found some info.
  </li>
 </ul>
 <p>
  I can go on and on. Yahoo, even NCSA and a lot of others ..... :( :( :(
 </p>
</blockquote>
<h2>
 Update Link Destinations
</h2>
<p>
 <em>
  <a class="out" href="http://members.aon.at/ai-management/index.htm" title="Personal home page in German">
   Helmut Wollmersdorfer
  </a>
  writes:
 </em>
</p>
<blockquote>
 Not only dead links but also wrong links can be a problem.
 <p>
  <strong>
   Wrong links
  </strong>
  mean that the link exists, but it links to another page; not the page the designer wanted it to link.
 </p>
 <p>
  To avoid both - dead and wrong links - I go through my pages each month, test each link and look what happens. Additionally I do the same procedure after each update of my pages.
 </p>
 <p>
  Also, each month I ask myself: Are my links to foreign pages a good tip for my users, are there some links to remove, or some that should be changed to better ones concerning the same topic.
 </p>
</blockquote>
<p>
 <em>
  Jakob&#39;s reply:
 </em>
 This is a great point. For a large site it may not be feasible to check every single link destinations every month, but doing some form of check would be a good task for the
 <strong>
  content gardener
 </strong>
 (a job category I have proposed for large sites to maintain their old content). Guidelines for checking the destinations of old links include:
</p>
<ul>
 <li>
  Know what old content gets the most page views: check the links on these pages more frequently than links on less-visited pages
 </li>
 <li>
  Have a robot that visits linked pages at regular intervals and compares their current content with an archived snapshot of their content the day you linked to them. Destinations that change the most are likely candidates for replacement. Of course, a change could be caused by an update or an improvement in the destination page, so the simple fact of the change is not enough to yank the link. Also an unchanging page can become obsolete, meaning that it would be better to link to a new source on the Web.
 </li>
</ul>
</section>
  

  
  <section>
    
        
    
  </section>
  
  
  
  
  

  


  <section>
    <br />
    <p class="share"><b>Share <span class="hide-for-small-down">this article</span>:</b> <a href="https://twitter.com/intent/tweet?url=http://www.nngroup.com/articles/readers-comments-on-linkrot/&amp;text=Readers'%20Comments%20on%20Linkrot&amp;via=nngroup">Twitter</a> | <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=http://www.nngroup.com/articles/readers-comments-on-linkrot/&amp;title=Readers'%20Comments%20on%20Linkrot&amp;source=Nielsen%20Norman%20Group">LinkedIn</a> | <a href="https://plus.google.com/share?url=http://www.nngroup.com/articles/readers-comments-on-linkrot/">Google+</a> | <a href="mailto:?subject=NN/g Article: Readers&#39; Comments on Linkrot&amp;body=http://www.nngroup.com/articles/readers-comments-on-linkrot/">Email</a></p>
    <hr />
  </section>

  <footer class="l-subsection">
    
    
    

    <div class="row show-for-small-down related-content">
      
      
      

      
      
      

      
      
      

      
      
      

      
      
      
    </div>

    <div class="hide-for-small-down row related-content">
      <div class="medium-6 columns">
        
        
        

        

      </div>
      <div class="medium-6 columns">
        
        
      </div>
    </div>
  </footer>
</article>

        </div>

      

    
  </div>
  <div class="row">
    
      <div class="column small-12 show-for-small-down l-navrail announcements">
        
          
        
      </div>
    
    
  </div>

    </div>
    




<footer id="page-footer">
    <div class="row hide-for-print">
        <div class="column small-12 medium-6">
            <section class="footer-popular">
                
                <h1>Popular Topics</h1>
                <div style="float: left;  width: 15em; display: block;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/applications/index.php">Application Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/e-commerce/index.php">E-Commerce Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/eyetracking/index.php">Eyetracking </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/information-architecture/index.php">Information Architecture </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/intranets/index.php">Intranet Design </a></li>
	<li><a href="../../topic/interaction-design/index.php">Interaction Design </a></li>
</ul>
</div>

<div style="float: left;  width: 15em; display: block; list-style: none;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/mobile-and-tablet-design/index.php">Mobile and Tablet Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/credibility-persuasion/index.php">Persuasive Design</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/research-methods/index.php">Research Methods</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/user-testing/index.php">User Testing </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/web-usability/index.php">Web Usability </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/writing-web/index.php">Web Writing &amp; Content Strategy </a></li>
</ul>
</div>

<p>&nbsp;</p>

                
            </section>
        </div>

        <div class="column small-12 medium-6">
            <div class="row">
                <div class="column small-6">
                    <section class="footer-about">
                        <h1>Follow Us</h1>
                        <ul>
	<li><a href="../subscribe/index.php">Subscribe to our weekly UX newsletter</a>,&nbsp;Jakob Nielsen&#39;s Alertbox</li>
	<li>Visit Don Norman&#39;s website, <a href="http://www.jnd.org/"> www.jnd.org </a></li>
	<li>Visit Bruce Tognazzini&#39;s website, <a href="http://www.asktog.com/"> www.asktog.com </a></li>
	<li style="padding-top: 6px;"><a class="twitter-follow-button" data-show-count="false" data-size="large" href="https://twitter.com/nngroup"><img alt="Follow @nngroup on Twitter" height="20" src="https://media.nngroup.com/media/editor/2016/12/30/twitter-follow-button.png" style="width: 117px; height: 20px;" width="117" /></a></li>
	<li style="padding-top: 6px;"><img alt="" height="14" src="https://media.nngroup.com/media/editor/2016/07/26/feed-icon-14x14.png" style="vertical-align:text-top;width:14px;margin-right:.25em;" width="14" /><a href="../../feed/rss/index.php">Subscribe to our RSS feed</a></li>
</ul>

                    </section>
                </div>

                <div class="column small-6">
                    <section class="footer-about right-column">
                        <h1>About
</h1>
                        <ul class="no-bullet">
	<li><a href="../../about/contact/index.php">Contact information</a></li>
	<li><a href="../../about/index.php">About Us</a></li>
	<li><a href="../../about/why-nng/index.php">Why NN/g? </a></li>
	<li><a href="../../return-policy/index.php">Return Policy</a></li>
</ul>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="copyright column small-12">
            <p><a href="../../copyright-and-reprint-info/index.php">Copyright</a> &copy; 1998-2017 Nielsen Norman Group, All Rights Reserved.</p>
        </div>
    </div>
</footer>


    


        <script src="https://media.nngroup.com/static/vendor/fastclick/js/fastclick.min.js"></script>
        <script src="https://media.nngroup.com/static/vendor/foundation/js/foundation.min.js"></script>
        <script src="http://media.nngroup.com/static/js/app.js"></script>
        <script src="http://media.nngroup.com/static/js/scroll-to-fixed.js"></script><script>var externalLinks = document.querySelectorAll('a[href^="http"]');for (var i = externalLinks.length-1; i >= 0; i--) {externalLinks[i].addEventListener("click", function(event) { alert("External links are disabled for this test."); event.preventDefault(); }, false);}var mailLinks = document.querySelectorAll('a[href^="mailto"]');for (var i = mailLinks.length-1; i >= 0; i--) {mailLinks[i].addEventListener("click", function(event) { alert("Email links are disabled for this test."); event.preventDefault(); }, false);}var externalForms = document.querySelectorAll('form');for (var i = externalForms.length-1; i >= 0; i--) {externalForms[i].addEventListener("submit", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false); externalForms[i].addEventListener("click", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false);}</script>

        <script>
            
            $(function() {
                $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
                $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
            });
        </script>

        
<script src="https://media.nngroup.com/static/js/articles.js"></script>


        
        <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1072699960;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072699960/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
    </body>

<!-- Mirrored from www.nngroup.com/articles/readers-comments-on-linkrot/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:17:22 GMT -->
</html>
