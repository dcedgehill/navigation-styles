<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->





    
<!-- Mirrored from www.nngroup.com/articles/cdrom/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:15:33 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1026.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"beacon":"bam.nr-data.net","queueTime":33,"applicationTime":2599,"agent":"","licenseKey":"1a30ef9931","errorBeacon":"bam.nr-data.net","applicationID":"2048992","transactionName":"NFxSMRBUX0EDUkIPDA0WdhAMVkVbDV8ZBxEXUFMJB0YfRAtUQRVZIktEDAFZVHYHRVcPD01eVRE="}</script>
        <title>Three Large HyperCard Stacks: A 1989 Review: Article by Jakob Nielsen</title><meta property="og:title" content="Three Large HyperCard Stacks: A 1989 Review: Article by Jakob Nielsen" />
  
        
        <meta name="description" content="Jakob Nielsen&#39;s analysis of three CD-ROMs from 1989; focus on usability, hypertext structure, navigational dimensions and transitions (with screenshots of The Manhole, Whole Earth Catalog, Time Table of History)">
        <meta property="og:description" content="Jakob Nielsen&#39;s analysis of three CD-ROMs from 1989; focus on usability, hypertext structure, navigational dimensions and transitions (with screenshots of The Manhole, Whole Earth Catalog, Time Table of History)" />
        
  
        
	
        

        

        <meta name="robots" content="noarchive">
        <meta name="viewport" content="width=device-width">

        <link rel="search" type="application/opensearchdescription+xml" title="NN/g Search" href="../../opensearch.xml" />
        <link rel="shortcut icon" href="http://media.nngroup.com/static/img/favicon.ico" />

        <link rel="stylesheet" href="https://media.nngroup.com/static/vendor/font-awesome/css/font-awesome.min.css">

        
        <link rel="stylesheet" href="https://media.nngroup.com/static/css/7232a960f1df.css" type="text/css" />
        <link rel="stylesheet" href="http://fast.fonts.com/t/1.css?apiType=css&amp;projectid=c5846260-9def-46ec-9407-0f1349bd2d64" type="text/css">

        <script src="https://media.nngroup.com/static/vendor/modernizr/js/modernizr.min.js"></script>

        <script type="text/javascript" src="https://media.nngroup.com/static/js/3283be5def74.js"></script>
        

        

        
        
        <!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=229034,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('http://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->


<!-- Google Analytics Tracking Code for http://nngroup.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23132681-1', 'nngroup.com');
  ga('require', 'linkid', 'https://www.nngroup.com/articles/cdrom/linkid.js');
  ga('require', 'displayfeatures');

// Optimizely Universal Analytics Integration
 window.optimizely = window.optimizely || [];
 window.optimizely.push("activateUniversalAnalytics");

  ga('send', 'pageview');

</script>
<script>
/**
* Function to track a click on an outbound link in Google Analytics.
*/
function trackOutboundLink(label, url) {
    if(window.ga && ga.loaded) {
        ga('send', 'event', 'outbound', 'click', label, {'hitCallback': function(){document.location = url;}
        });
    } else {
        document.location = url;
    }
}
/**
* Function to track a click on a link in the article announcement area.
*/
$('#articleAnnouncement a').on('click', function(){
  ga('send', 'event', 'link', 'click', 'articleAnnouncement');
});
</script>

<script type="text/javascript">
 setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2421.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



<!-- Hotjar Tracking Code for http://nngroup.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:121605,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

        <script src="https://media.nngroup.com/static/js/analytics/track_outbound_link.js"></script>
        
        
        
    </head>
    <body class="location-articles articles-detail">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
    <header id="page-header">
        <div id="cart" class="shopping-cart mobile-cart show-for-small-down"></div>
        <div class="row">
            <div id="masthead" class="masthead small-12 medium-7 column">

                <!-- The button to toggles menu on small screen -->

                <h1><a href="../../index.php"><img src="http://media.nngroup.com/static/img/logo-wordmark.png" width="74" height="30"> Nielsen <span class="norman">Norman</span> Group</a></h1>
                <p class="tagline show-for-medium-up">Evidence-Based User Experience Research, Training, and Consulting</p>

                <!-- The button to toggles form search on small screen -->


                <div style="clear: right;width:  100%;height: 1px;"></div><?php define('ROOT_PATH',$_SERVER["DOCUMENT_ROOT"] . '/apps/navigation_styles/navigations/4'); ?><?php include(ROOT_PATH)."/navigation.php"; ?>

                <!-- Drop down menu on small screen -->
                <div id="hideNav" class="show-for-small-down">
                  
                  <ul class="menu-content">
                    <li><a href="../../index.php">Home</a></li>
                    <li class="has-submenu">
                      <a>Training</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../training/index.php">Overview</a></li>
                        
<li id="nav-ux-conference"><a href="../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../online-seminars/index.php"><span>Online Seminars</span></a></li>

                      </ul>
                    </li>
                    <li class="has-submenu">
                      <a>Consulting</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
  <li><a href="../../consulting/index.php">Overview</a></li>
  
  <li><a href="../../consulting/benchmarking/index.php">Benchmarking</a></li>
  
  <li><a href="../../consulting/expert-design-review/index.php">Expert Design Review</a></li>
  
  <li><a href="../../consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
  
  <li><a href="../../consulting/usability-testing/index.php">Usability Testing </a></li>
  
  <li><a href="../../consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
  
  <li><a href="../../in-house-training/index.php">In-House Training</a></li>
</ul>

                    </li>

                    <li><a href="../../reports/index.php">Reports</a></li>
                    <li><a href="../index.php">Articles</a></li>
                    <li class="has-submenu"><a>About NN/G</a><i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../about/index.php">Overview</a></li>
                        <li><a href="../../people/index.php">People</a></li>
                        <li><a href="../../about/why-nng/index.php">Why NN/g?</a></li>
                        <li><a href="../../about/contact/index.php">Contact</a></li>
                        <li><a href="../../news/index.php">News</a></li>
                        <li><a href="../../about/history/index.php">History</a></li>
                        <li><a href="../../books/index.php">Books</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                </div>

                <hr class="header-divider show-for-small-only"/>


            </div>
       </div>

        <div id="hideNav">            <nav class="primary-nav small-12 column">
                <ul>
                    <li><a class="navitem" id="nav-home" href="../../index.php">Home</a></li>
                    <li><a class="navitem" id="nav-training" href="../../training/index.php">Training</a></li>
                    <li><a class="navitem" id="nav-consulting" href="../../consulting/index.php">Consulting</a></li>
                    <li><a class="navitem" id="nav-reports" href="../../reports/index.php">Reports</a></li>
                    <li><a class="navitem" id="nav-articles" href="../index.php">Articles</a></li>
                    <li><a class="navitem" id="nav-about" href="../../about/index.php">About NN/G</a></li>
                </ul>
            </nav>
            <nav class="secondary-nav small-12 medium-12 large-12 columns">
                
                
            </nav>
        </div>
    </header>
    <div class="l-content">
    
  

  

  <div class="row">
    
      
      <section class="small-12 medium-3 columns collapsable-content l-navrail articles-left-column">
        <h1 class="show-for-small-down collapse-link mobile-browse-header">Browse by Topic and Author</h1>
          <div class="row collapsable">

            
            <div class="small-6 medium-12 columns">
              <section id="article-popular-topics" style="display: none;">
                
                <h1>Topics</h1>
                <ul id="article-popular-topics">
                    
                    <li>
                        
                        <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/intranets/all/index.php">Intranets</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/user-testing/all/index.php">User Testing</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                        
                    </li>
                    
                    <li><a href="#" id="article-see-all-topics"><strong>See all topics&hellip;</strong></a></li>
                </ul>
                
              </section>
              <section id="article-all-topics" style="display: none;">
                
                <h1>
                  <span class="show-for-medium-up">All Article Topics</span>
                  <span class="show-for-small-down">All Topics</span>
                  (<a href="#" id="article-hide-all-topics">hide</a>)
                </h1>
                <ul>
                  
                  <li>
                    
                    <a href="../../topic/accessibility/all/index.php">Accessibility</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/agile/all/index.php">Agile</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/analytics-and-metrics/all/index.php">Analytics &amp; Metrics</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/applications/all/index.php">Application Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/b2b-websites/all/index.php">B2B Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/behavior-patterns/all/index.php">Behavior Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/branding/all/index.php">Branding</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/content-strategy/all/index.php">Content Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/corporate-websites/all/index.php">Corporate Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/design-patterns/all/index.php">Design Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-design-process/all/index.php">Design Process</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/email/all/index.php">Email</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/eyetracking/all/index.php">Eyetracking</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/heuristic-evaluation/all/index.php">Heuristic Evaluation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/human-computer-interaction/all/index.php">Human Computer Interaction</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-ideation/all/index.php">Ideation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/information-architecture/all/index.php">Information Architecture</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/interaction-design/all/index.php">Interaction Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/international-users/all/index.php">International Users</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/intranets/all/index.php">Intranets</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-management/all/index.php">Management</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/navigation/all/index.php">Navigation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/non-profit-websites/all/index.php">Non-Profit Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/personas/all/index.php">Personas</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/persuasive-design/all/index.php">Persuasive Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/prototyping/all/index.php">Prototyping</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/psychology-and-ux/all/index.php">Psychology and UX</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/research-methods/all/index.php">Research Methods</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/search/all/index.php">Search</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/social-media/all/index.php">Social Media</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/strategy/all/index.php">Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/user-testing/all/index.php">User Testing</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-teams/all/index.php">UX Teams</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/visual-design/all/index.php">Visual Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/writing-web/all/index.php">Writing for the Web</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/young-users/all/index.php">Young Users</a>
                    
                  </li>
                  
                </ul>
                
              </section>
            </div>
            <script>
              /* Determine which topics list will be displayed: the full or the
               * partial.  We are inlining this close to the list itself to
               * prevent rendering delays on the client
               */
              var topic_list = $.cookie("article-topics");

              var popular_topics = $('#article-popular-topics');
              var all_topics = $('#article-all-topics');

              if(topic_list == 1) {
                  popular_topics.hide();
                  all_topics.show();
              } else {
                  popular_topics.show();
                  all_topics.hide();
              }
            </script>
            

            
            <div class="small-6 medium-12 columns">
              
              <section id="article-initial-authors">
                  <h1>Author</h1>
                  <ul>
                    
                    <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                    
                    <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                    
                    <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                    
                    <li><a href="#" onClick="$('#article-initial-authors').hide(); $('#article-all-authors').show();"><strong>See all authors&hellip;</strong></a></li>
                  </ul>
              </section>
              <section id="article-all-authors" style="display: none;">
                <h1>
                  All Authors
                  (<a href="#" id="article-hide-all-authors">hide</a>)
                </h1>
                <ul>
                  
                  <li><a href="../author/raluca-budiu/index.php">Raluca Budiu</a></li>
                  
                  <li><a href="../author/patty-caya/index.php">Patty Caya</a></li>
                  
                  <li><a href="../author/susan-farrell/index.php">Susan Farrell</a></li>
                  
                  <li><a href="../author/therese-fessenden/index.php">Therese Fessenden</a></li>
                  
                  <li><a href="../author/kim-flaherty/index.php">Kim Flaherty</a></li>
                  
                  <li><a href="../author/sarah-gibbons/index.php">Sarah Gibbons</a></li>
                  
                  <li><a href="../author/aurora-harley/index.php">Aurora Harley</a></li>
                  
                  <li><a href="../author/kate-kaplan/index.php">Kate Kaplan</a></li>
                  
                  <li><a href="../author/page-laubheimer/index.php">Page Laubheimer</a></li>
                  
                  <li><a href="../author/angela-li/index.php">Angie Li</a></li>
                  
                  <li><a href="../author/hoa-loranger/index.php">Hoa Loranger</a></li>
                  
                  <li><a href="../author/kate-meyer/index.php">Kate Meyer</a></li>
                  
                  <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                  
                  <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                  
                  <li><a href="../author/kara-pernice/index.php">Kara Pernice</a></li>
                  
                  <li><a href="../author/christian-rohrer/index.php">Christian Rohrer</a></li>
                  
                  <li><a href="../author/amy-schade/index.php">Amy Schade</a></li>
                  
                  <li><a href="../author/katie-sherwin/index.php">Katie Sherwin</a></li>
                  
                  <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                  
                  <li><a href="../author/kathryn-whitenton/index.php">Kathryn Whitenton</a></li>
                  
                </ul>
              </section>
              
            </div>
            
          </div>

          
          <section class="hide-for-small-down">
            
              
              <h1>Recent Articles</h1>
              
              <ul>
                  
                  <li><a href="../seamless-cross-channel/index.php">Seamlessness in the Omnichannel User Experience</a></li>
                  
                  <li><a href="../brand-intention-interpretation/index.php">Experience Design: Bridging Brand Intention and Brand Interpretation</a></li>
                  
                  <li><a href="../eyetracking-tasks-efficient-scanning/index.php">Scanning Patterns on the Web Are Optimized for the Current Task</a></li>
                  
                  <li><a href="../flat-design-best-practices/index.php">Flat-Design Best Practices</a></li>
                  
                  <li><a href="../ux-user-stories/index.php">Accounting for UX Work with User Stories in Agile Projects</a></li>
                  
                  <li><a href="../index.php"><strong>See all articles&hellip;</strong></a></li>
              </ul>
              
            
          </section>
          

          
          
          <section class="hide-for-small-down">
            <h1>Popular Articles</h1>
            
            <ul>
                
                <li><a href="../usability-101-introduction-to-usability/index.php">Usability 101: Introduction to Usability</a></li>
                
                <li><a href="../top-10-mistakes-web-design/index.php">Top 10 Mistakes in Web Design</a></li>
                
                <li><a href="../how-users-read-on-the-web/index.php">How Users Read on the Web</a></li>
                
                <li><a href="../f-shaped-pattern-reading-web-content/index.php">F-Shaped Pattern For Reading Web Content</a></li>
                
                <li><a href="../ten-usability-heuristics/index.php">10 Usability Heuristics for User Interface Design</a></li>
                
                <li><a href="../intranet-design-2016/index.php">10 Best Intranets of 2016</a></li>
                
                <li><a href="../which-ux-research-methods/index.php">When to Use Which User-Experience Research Methods</a></li>
                
                <li><a href="../response-times-3-important-limits/index.php">Response Times: The 3 Important Limits</a></li>
                
                <li><a href="../why-you-only-need-to-test-with-5-users/index.php">Why You Only Need to Test with 5 Users</a></li>
                
                <li><a href="../page-fold-manifesto/index.php">The Fold Manifesto: Why the Page Fold Still Matters</a></li>
                
            </ul>
          </section>
          
          

          
            <section class="hide-for-small-down">
              
            </section>
          

      </section>
      

      
        <div class="small-12 medium-9 columns content-body">
          




<article class="l-subsection alertbox-article-content article-detail-bl">
  
  <header>
    <div class="l-subsection">
      <h1>Three HyperCard Stacks on CD-ROM: A Review </h1>
      <div class="author-meta">by
        
          
            
            <a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a>
            
          
        on  January 1, 1989
        
      </div>

      

      
      <div class="topic-links">
        Topics:
        
          <ul>

  <li><a href="../../topic/navigation/index.php">Navigation</a></li>

</ul>
        
      </div>
      
    </div>
  </header>
  

  
  
  <section>
    <p class="lede"><strong>Summary:</strong> A review of the Macintosh CD-ROM versions of The Manhole, the Time Table of History, and the Electronic Whole Earth Catalog with emphasis on their usability and their support of hypertext navigation. Based on the discussion of these hypertexts the following general principles are found to be useful for analyzing hypertext user interfaces: Navigational dimensions and their explicitness, directionality and literalness, landmarks, locational orientation, history lists, and backtrack mechanisms.</p>
    <br />
  </section>
  
  

  
  <section id="article-body"><p>
	<em>Originally published as:  Nielsen, J. (1990). Three medium-sized hypertexts on CD-ROM. ACM SIGIR Forum 24, 1, 2-10. </em></p>
<h2>
	Introduction</h2>
<p>
	Current <a href="../../books/multimedia-and-hypertext/index.php"> research on hypertext usability</a> has mostly looked at smaller hypertext structures with perhaps up to a few hundred nodes of information. One reason is that it is simply difficult to construct and test larger structures, but another is the data storage problems inherent in bigger information bases. The CD-ROM offers us the potential of about 600 megabytes of data storage which should be enough for fairly large hypertexts. The three CD-ROMs discussed in this article cannot be classified as big hypertexts, however, since they contain less than ten thousand nodes. Truly big hypertexts would contain hundreds of thousands or even millions of nodes, leading to challenging new issues in navigation support.</p>
<p>
	 </p>
<table border="1" cols="7">
	<caption valign="bottom">
		<strong>Table 1. </strong> Summary of data on the three CD-ROMs discussed in this article. They are all in Macintosh-readable format and contain HyperCard stacks. For each disc the table shows the number of HyperCard cards in the stacks on that disc. The "data" column shows the size in mega-bytes of the non-sound information in these stacks while the "sound" column shows the storage taken up by digitized sound on each disc. The "index" column shows the size of the separate files containing indices to the primary data.</caption>
	<tbody>
		<tr>
			<th>
				CD-ROM</th>
			<th>
				HyperCard cards<br/>
				(i.e., number of "pages")</th>
			<th>
				data storage</th>
			<th>
				sound storage</th>
			<th>
				index storage</th>
			<th>
				disc size</th>
			<th>
				list price<br/>
				(in 1989)</th>
		</tr>
		<tr align="center">
			<th align="left">
				The Manhole</th>
			<td>
				753</td>
			<td>
				3 MB</td>
			<td>
				48 MB</td>
			<td>
				N/A</td>
			<td>
				52 MB</td>
			<td>
				$60</td>
		</tr>
		<tr align="center">
			<th align="left">
				Time Table of History</th>
			<td>
				6199</td>
			<td>
				10 MB</td>
			<td>
				9 MB</td>
			<td>
				4 MB</td>
			<td>
				23 MB</td>
			<td>
				$150</td>
		</tr>
		<tr align="center">
			<th align="left">
				Electronic Whole Earth Catalog</th>
			<td>
				9742</td>
			<td>
				34 MB</td>
			<td>
				344 MB</td>
			<td>
				24 MB</td>
			<td>
				413 MB</td>
			<td>
				$150</td>
		</tr>
	</tbody>
</table>
<p>
	This article reviews the three CD-ROMs listed in Table 1. All three are Macintosh HyperCard stacks and therefore the one most relevant number for an estimate of their hypertext complexity is the number of HyperCard cards since that forms an approximation of the number of nodes in the hypertext network. The data spent on storing digitized sound looks impressive and can certainly add to the experience of using the discs. But on these discs, sound is purely used for passive play-back and not as active hypertext items with which the user can interact.</p>
<p>
	The amount of storage space listed as "data" in the table is a somewhat misleading indicator of the amount and complexity of information in the hypertexts. For example, in the Time Table of History, about five of the ten megabytes are really scanned images used to illustrate a small subset of the entries in the time table. The time table itself is only about five megabytes of data, and even that number should be seen in light of the storage overhead associated with using HyperCard. If the actual information in the Time Table was stored in a more efficient (but less accessible) format, it would probably be just a single megabyte.</p>
<h2>
	The Manhole: Interactive Fiction in Hyperspace</h2>
<p>
	The Manhole is an interactive fiction for "children of all ages". It takes place in a fantasy world with talking animals and dragons where magic beanstalks grow into the sky. The fantasy world is displayed to the user in a first-person perspective (i.e. graphically showing what you would actually see if you were positioned at the current location in the world) and users move through the world by clicking on the place they want to go.</p>
<p>
	<strong><img alt="Fig. 1a: The Manhole's beanstalk at ground level" src="http://media.nngroup.com/media/editor/2012/10/30/manhole1.gif" style="width: 513px; height: 343px; "/> </strong></p>
<p>
	<strong><img alt="Fig. 1b: The Manhole's beanstalk peeking through at the sky level" src="http://media.nngroup.com/media/editor/2012/10/30/manhole2.gif" style="width: 513px; height: 343px; "/> </strong></p>
<p>
	<strong>Figure 1 a and b. </strong> <em> Two screens from The Manhole showing support for literal navigational dimensions through the use of a landmark: The beanstalk is a linear object which connects the sea, ground, and sky levels. In figure 1a the user's viewpoint is slightly above ground level since the figure is a snapshot from an animation of climbing down the beanstalk. The manhole (which gives the story its name) is seen at the ground level, and through it we can also see the sea level, where the beanstalk grows up from an island. In figure 1b we are in the sky level kingdom and we can see the top of the beanstalk in the background (it grows up into the sky). If the user moves to the beanstalk in figure 1b and then climbs down, figure 1a will be displayed. The fact that the beanstalk is visible in the background of figure 1b gives the user a sense of location when looking at this screen, since it would not otherwise be obvious that the tower was located at the sky level. </em></p>
<p>
	It took me approximately two hours to get through all of The Manhole's navigation space. The fantasy world of the disc actually has some structure to it in a one-dimensional ordering of the locations from the bottom of the sea via the sea surface and the street level with the manhole itself to a tower in the sky. There is a magic beanstalk which covers this distance, thus providing a kind of landmark. This navigation space is set up with little regard to normal conventions regarding landmarks and orderly user movement, however, and users are frequently magically transported to new locations and have to discover where they are in the navigation space. The very structure and connectivity of the navigation space did not even become apparent to me before I had spent about one hour in it.</p>
<p>
	Figure 1 shows one example of navigational support provided by the beanstalk landmark while Figure 2 shows an example of navigation violating traditional expectations. This latter kind of "magical" movement in the world of an interactive fiction adds spice to the experience of using the system and is probably good in a system having entertainment as its main purpose. It also seems to make it harder to acquire a conceptual model of the navigation space, however, so it would probably not be suited for more work-oriented situations.</p>
<p>
	The Manhole uses verbal language to a large extent in the form of messages from various characters to the user. These messages are printed on the screen in cartoon-like speech-bubbles and are also read out loud by the system using good quality sound and some interesting voice characterizations. There are four main characters with which the user interacts: An elephant which paddles the user around in a small boat, a dragon, a walrus, and a rabbit. Each character has a tone of voice consistent with the way it behaves (e.g. the walrus is lazy and the dragon is a hip dude). Other sound effects, including both music and various naturalistic sounds are also used to good effect.</p>
<p>
	A test user (an adult business professional) enjoyed using the system for about 30 minutes but frequently complained about the difficulty of knowing which parts of the screen were active hypertext anchors (clickable areas) and what the results of her actions would be. Also, she found it difficult to know when the system was in a state where it would respond to input from the user and when it was running through a pre-defined animation sequence. This was probably because The Manhole sometimes will display an animated screen (e.g. containing drifting seaweed) or play music in situations where it is really waiting for user input. In other situations, the user is prevented from performing any action until the system has finished displaying some animation. If the user clicks anyway, there is often a risk that the system will "remember" that click and use it to activate some choice on whatever screen is displayed at the end of the animated sequence. The test user frequently had problems with having a click made on one screen become active on the next screen but this is probably more a problem with the underlying HyperCard system than with The Manhole.</p>
<p>
	<strong><img alt="Fig. 2a: The Manhole's tower" src="http://media.nngroup.com/media/editor/2012/10/30/manhole3.gif" style="width: 513px; height: 343px; "/> </strong></p>
<p>
	<strong><img alt="Fig. 2b: The Manhole's chess board" src="http://media.nngroup.com/media/editor/2012/10/30/manhole4.gif" style="width: 513px; height: 343px; "/> </strong></p>
<p>
	<strong>Figure 2 a and b. </strong> <em> An example of a magical navigational dimension from The Manhole: When users move into the tower from Figure 1b, they will see figure 2a after having been through some screens showing a winding stairway leading to the top of the tower. Because of this movement and the presence of the flamingo on the top of the tower, users initially view the tower as a traditional building located in the forest seen in the background of figure 1b. When users move to the objects seen outside the tower in figure 2a, they get to figure 2b where it is revealed that the tower has been magically transformed to a rook on a chess board. At the same time, the user's navigational location has been moved from the sky level to the sea level where the chess board floats in a labyrinth. </em></p>
<p>
	To conclude, The Manhole is indeed enjoyable and your initial experience the first hour or so you use it will give you a good idea of the feeling of reading/participating in the open or truly large-scale interactive fictions we will probably see in the future. Children will probably take more time to discover the closed nature of the fantasy universe and may even continue to enjoy moving through then-familiar territory after they have been through the entire navigation space and have discovered its internal structure.</p>
<h2>
	Time Table of History: Unfulfilled Promise</h2>
<p>
	The first thing to note about this product is that it is not a full history of the world but mainly a history of science and technology. The full benefit of a hypertext history will not be realized until we have a system which integrates all aspects of history in one hyperspace, no matter whether we are talking about scientific innovation, literature and cultural events, or wars and political and social history, since those distinctions are partly artificial and limit our view of the total human situation through the ages. The Time Table has a small bit of integration in the form of its timeline which is shown in Figure 3. Each article about a historical event (such as that in Figure 4) is labeled with the year it occurred and has a link to the part of the timeline containing that year, thus enabling users to see what major events took place at the same time. It is also possible to go from the timeline to a menu of articles related to a specific interval of years, and to jump from that menu to the individual articles.</p>
<p>
	<strong><img alt="Fig. 3: time line from Time Table of History" src="http://media.nngroup.com/media/editor/2012/10/30/timetable1.gif" style="width: 513px; height: 343px; "/> </strong></p>
<p>
	<strong>Figure 3. </strong> <em> Part of the actual time line from The Time Table of History. Clicking on one of the time intervals listed in boxes gives a menu of the articles associated with years in that interval. </em></p>
<p>
	As a first approach at making a hypertext history, the Time Table succeeds partly, but only partly. Its main problem is that it seems to have been constructed fairly quickly, and as a result it does not provide comprehensive coverage even of the history of science. For example, there does not seem to be an article about the discovery of electromagnetism. Other cases present evidence of poor editing. For example, I performed a search for articles containing the word "Nielsen" and got a menu of two articles: "Audimeter" and "Nielsen Audimeter".</p>
<p>
	The Audimeter article was card 3483, contained the following text: "In 1935 the Audimeter is introduced, and becomes the basis for Nielsen ratings", and had the following keywords listed: "broadcasting, business, communication, information, television". The Nielsen Audimeter article was card 3513 (thirty cards after the Audimeter article), contained the following text: "The foundation of Nielsen ratings, the audimeter, is introduced as a research tool" (in the year 1935 which was listed in a separate field), and had the following keywords listed: "communication, telecommunications".</p>
<p>
	Obviously, the writers of the Time Table did not coordinate their articles to ensure that the same event would only be described once. The fact that the same invention is described with two quite different sets of keywords also considerably decreases our confidence in the usefulness of the keywords.</p>
<p>
	<strong><img alt="Fig. 4: Sample article screen, Time Table of History" src="http://media.nngroup.com/media/editor/2012/10/30/timetable2.gif" style="width: 513px; height: 343px; "/> </strong></p>
<p>
	<strong>Figure 4. </strong> <em> Screen design of the actual articles about historical events in the Time Table of History. Clicking on the "world" icon takes the user to a map showing the geographical location of the event. The icons to the right of the "world" icon are not clickable but are used to indicate the general classification of the article in seven different fields of human endeavor. Here the category is "science". The seven category icons hide a control panel of buttons which when activated allows the user to go to help screens, the time line screen containing the year associated with the current article, and a few other options. The left and right arrows shown here are displayed when no alphabetic search is in effect and allow users to step through the articles in chronological order. </em></p>
<p>
	Another classification problem can be seen in Figure 4 which shows the screen layout for the historical articles. The article shown is about the 1987 discovery of a homo habilis skeleton and is therefore classified as a 1987 event. There is no discussion of the homo habilis species, however, in the chronological sequence of the Time Table which deals with the early humans. This section has articles on the emergence of the homo erectus and homo sapiens species but no cross reference to the homo habilis article which will only be discovered by a reader conducting an alphabetic search for occurrences of the word "homo".</p>
<p>
	There are also several inconsistencies in the user interface. The time line shown in Figure 3 is called "Time Line" in the system's main menu but "overview" in the control panel on the historical articles. And the return-to-previous-screen icon is displayed in the lower left corner of the world map, in the upper right corner of the time line, and in the lower right corner of digitized illustrations. A more serious problem is that there is no return option at all in connection with the various keyword searches. When users perform a search, they are put in a new system state with no access to their prior navigation behavior. For example, I read an article on the history of the Macintosh that mentioned Alan Kay. From there I did a general search on Kay's name to find what of his other work was discussed. This led on to some interesting articles on the Disney Tron movie on which Kay served as a consultant, thus showing the usefulness of having hypertext facilities on a history CD-ROM. But when I wanted to return to my primary reading (about the Macintosh), there was no way to backtrack. Since I remembered having searched for the word "Macintosh", I was able to redo the search and thereby find my way back to the article I had been reading when I was sidetracked by Kay. But humans should not be required to remember search terms and navigational locations, since computers are supposed to be good at exactly that kind of thing.</p>
<p>
	<strong><img alt="Fig. 5: Search results, Time Table of History" src="http://media.nngroup.com/media/editor/2012/10/30/timetable3.gif" style="width: 513px; height: 343px; "/> </strong></p>
<p>
	<strong>Figure 5. </strong> <em> The menu resulting from an alphabetic search for the word "Macintosh" in the Time Table of History. By clicking on one of the lines in the menu, the user can go directly to an article. The strange arrow icons are shown instead of the regular arrows when the user has performed a search and allow the user to go through the articles found in the search. </em></p>
<p>
	The alphabetic search is one of the most powerful features of the Time Table. A search is initiated simply by clicking on a word on the screen. This contextually situated low-overhead operation actually means that the searches can be viewed as a kind of hypertext link in a richly connected hypertext. When everything is an anchor, users don't need special symbols to draw their attention to areas where they can click, leading to less visual clutter on the screen and less learning overhead. The disadvantage is that the Time Table does not give author-specified cross references to other articles of special relevance to the article currently being read. Such other articles of interest will not always have keywords in common with the current article, so the user will not be able to find them. For example, there is no reference to the article on HyperCard in the menu of articles about the Macintosh shown in Figure 5 because the word "Macintosh" is not used in that article.</p>
<p>
	Because index files have been pre-computed and stored on the CD-ROM, a search typically takes only five seconds which is fairly acceptable. The result of a search is an indication of the number of articles found, whereupon the reader is given options to abandon the search or to see a menu of the hits. Getting the menu takes an additional eight seconds, making the total time for a search operation about 15 secs if we assume a reaction time of about two seconds. This response time is slightly too long and indicates that CD-ROMs have so slow seek time and data transfer characteristics that even more precomputing will be necessary than done on this CD-ROM. For example, the menu of hits for each possible search term could have been stored directly on the CD-ROM instead of having to be constructed on the fly. It is also possible to search for boolean combinations of terms by calling up a special dialog box. Even in this case, precomputed menus could still be used since merging and pruning prestored lists for each term would be much faster than constructing a completely new menu.</p>
<p>
	The result of a search for the word "Macintosh" is shown in Figure 5. The system lists both the year associated with an article and its headline. This is a nice feature which enables us to skip the articles dealing with the inventor of the waterproof raincoat without having to go to see the articles, if our interest is actually the computer with the same name as that inventor. One reason this list works well in spite of the very short label for each article, is that historical articles have a very powerful organizing parameter in the form of years which can easily be sorted into linear lists preserving their natural order. The other reason is that the editors (in most cases) have been able to come up with understandable and meaningful short titles for the articles. This is an advantage of a human-generated hypertext which is often not found in computer-generated structures where meaningful titles are hard to come by for many hypertext nodes.</p>
<p>
	Several other features of the Time Table also show the promise of a hypertext history of the world. Articles about events with a specific geographic location have links to a world map where that location is pointed out. It is possible to perform a search on the name of a country and get a menu of all articles associated with that country but it is not possible to click on the map to get such menus. For off-planet events, there is a link to an animated sequence inspired by the classic film Powers of Ten which zooms out from the Earth surface to Earth orbit, the Solar system, the Milky Way galaxy, etc. This animation is a good application of the power of the computer to do things with online documents which are impossible on the printed page. As a special kind of hypertext, articles dealing with chemical elements are linked to a screen with the periodic table of the elements where fundamental properties of that element can be seen. But again there are no reverse hypertext links from the periodic table to the relevant articles. Users have to perform keyword searches which removes them one step from the potential discovery of connections between the classification of an element in the periodic table and its use in science and technology.</p>
<p>
	To conclude, The Time Table of History shows a lot of promise but it has to be developed much further and edited more carefully to actually fulfill that promise. It should be seen as a preview of things to come in the hypertext field.</p>
<h2>
	Electronic Whole Earth Catalog: Structure to the Universe</h2>
<p>
	The Electronic Whole Earth Catalog is a CD-ROM version of the printed Whole Earth Catalogs which has been updated to early 1989. The main difference between the printed and the CD-ROM versions is that the CD-ROM contains a large number of digitized sound clips of the various records reviewed in the Catalog. The CD-ROM seems to contain the same illustrations as the printed version, albeit using the somewhat poorer resolution of scanned Macintosh screen images.</p>
<p>
	<img alt="Fig. 6: Whole Earth Catalog landmark (ToC)" src="http://media.nngroup.com/media/editor/2012/10/30/wholeearth1.gif" style="width: 512px; height: 342px; "/></p>
<p>
	<strong>Figure 6. </strong> <em> The main table of contents of the Electronic Whole Earth Catalog. This screen has the status of a landmark in the hypertext since it is always directly accessible from any other screen by a click on the Earth icon. </em></p>
<p>
	The sound clips are a major advantage of the electronic version but it also has better search options than the printed book. Figure 7 shows the result of a search for the word "Macintosh". The system indicates the number of hits and the user can page through them one at a time, seeing the search word in context on the screen in Figure 7 and jump from there to the actual article if so desired. The disadvantage is that the user never gets the overview of all the hits which is provided by a single out-of-context list such as that used by the Time Table of History (see Figure 5).</p>
<p>
	<img alt="Fig. 7: Whole Earth Catalog search results listing" src="http://media.nngroup.com/media/editor/2012/10/30/wholeearth2.gif" style="width: 512px; height: 342px; "/></p>
<p>
	<strong>Figure 7. </strong> <em> The result of a full text search in the Electronic Whole Earth Catalog for the word "Macintosh". </em></p>
<p>
	The Electronic Whole Earth Catalog is big enough a hypertext with its almost 10,000 nodes to require really good support for user navigation, and this has been achieved through a fairly strict structuring of the information. The Catalog is first divided into so-called domains which would correspond to chapters in regular books. The main table of contents screen in Figure 6 shows a list of the 13 domains. The domains are further subdivided into sections which again are subdivided into clusters (sub-sections) which then contain the actual articles. Figure 8 shows a progression of domain-section-cluster contents screens. Note that each level of the hierarchy has its own layout of the listed items and that each of these layouts is repeated in a stylized representation in the icon for "go one step up the hierarchy" in the level below.</p>
<p>
	The entire main screen-domain-section-cluster-article hierarchy has five levels. This might in many cases be considered as too confusing for especially the novice or casual users who will probably dominate as users of this kind of reference work. Fortunately, careful design of the user interface has alleviated much of the potential confusion. First, as already noted, the screens for the different levels of the hierarchy have visibly different layouts. This makes it possible for somewhat experienced users to recognize the level on which they are at any given time. And second, each domain has a unique background pattern which is shown prominently on the domain card (e.g. Fig. 8a) and in a simplified representation on the main table of contents (Figure 6). This background is repeated on each card belonging to that domain on both the section, cluster, and article levels as shown in Figures 8b, 8c, and 9a. Figure 9b shows that the background pattern will indicate to users when they have moved to another domain through a hypertext jump.</p>
<p>
	It will require some experience in the use of the Catalog to be able to identify all the domains from the small piece of background visible on most of the cards but it is not absolutely necessary to be able to do so. The system includes a facility for displaying the names of the current domain, section, and cluster through the pop-up menu shown in Figure 9b and this menu can also be used to directly jump to the relevant higher-level node. The user's sense of navigational location and hierarchical depth is reenforced through the visual techniques used to transfer the user to the destination for such a menu-based jump up the hierarchy. The system will take the user up the levels in the tree one level at a time by a fairly rapid series of fades from the current screen through each of the intermediate higher levels of screens until the destination is reached.</p>
<p>
	The fades are actually HyperCard dissolve visual techniques which are also used when users choose to move up a single step in the hierarchy by clicking on the icon below the Earth icon. This visual effect works fine when seen by itself but unfortunately contrasts with the use of an iris open visual effect for the process of moving to lower levels of the hierarchy. To emphasize that the two movements are opposite directions of the same navigational dimensions, I would have preferred to have seen similar visual effects used for both these transitions. For example, the iris open could have been retained for moving deeper into the hierarchy, while an iris close could have been used for the opposite movement of going up the hierarchy.</p>
<p>
	<img alt="Fig. 8a: Whole Earth Catalog chapter overview" src="http://media.nngroup.com/media/editor/2012/10/30/wholeearth3.gif" style="width: 512px; height: 342px; "/></p>
<p>
	<img alt="Fig. 8b: Whole Earth Catalog section overview" src="http://media.nngroup.com/media/editor/2012/10/30/wholeearth4.gif" style="width: 512px; height: 342px; "/></p>
<p>
	<img alt="Fig. 8c: Whole Earth Catalog cluster overview" src="http://media.nngroup.com/media/editor/2012/10/30/wholeearth5.gif" style="width: 512px; height: 342px; "/></p>
<p>
	<strong>Figure 8 a, b, and c. </strong> <em> The hierarchy of graphical overview of the contents of the Electronic Whole Earth Catalog's "domain" (=chapter), "section", and "cluster" (=subsection) levels. From the domain card in Fig. 8a we click on "Small Business" to go to the section card in Fig. 8b and from there we click on "Workshops and Seminars" to go to the cluster card in Fig. 8c. From this screen we can finally get to read the actual contents of the Catalog by clicking on the titles of various articles. We could go to Fig. 9a by clicking on "Organizing... Profitable Workshop Classes", or we could take a hypertext jump to another "domain" by clicking on the cross reference to "How to Make Meetings Work" which would take us to Fig. 9b. </em></p>
<p>
	A combination of icon design and animated visual effects is used to help users understand the difference between moving between screens within one given article and between different articles. In the article screens shown in Figure 9, the regular left and right arrows at the bottom of the screen are used to let the user read the series of screens belonging to that article while the triangular arrows at the right side of the screen are used for transfers to other articles in the current cluster. It would have been easy to confuse these two different kinds of movement if it had not been for the visual effects. For movements within the article level, standard speed scroll left/right effects are used to give the feeling of turning the pages of a book. And for moving between articles, the same scroll effects are used but with a slower speed indicating that the user is moving a longer distance. The two different kinds of scrolls really look quite different but still give the user the feeling of moving in the same general class of one-dimensional navigational dimension (i.e. horizontally within a given level of the hierarchy). One might assume that movements on even higher levels of the hierarchy should have been associated with progressively slower visual effects, but they would probably have been too slow to be comfortable. Indeed, the Catalog's user interface designers have chosen to use regular speed scrolls for movements on the higher levels. The best conceptual model to give users for the scroll speeds might then be not that they indicate the absolute distance traveled, but that regular speed indicates movement on the current level and that slow speed indicates movement on a higher level than the one to which the current screen belongs.</p>
<p>
	Even though the user interface support for navigation is very good, the Catalog has problems in other parts of its user interface. For example, there are two kinds of screens showing the information in a given domain. One is the graphical overview shown in Figure 8a and another is a textual outline not shown here which looks very much like a traditional table of contents listing major and minor headings with various levels of indentation. The screen with the main table of contents (Figure 6) gives the user the following explanation of how to get to this domain information: The user is told to "Click NAME to enter domain" and to "Click PICTURE for domain contents," which means that going to the graphical overview is called "entering the domain" and that going to the textual outline is called "seeing the domain contents." This choice of words is rather poor since it is not obvious what the difference between the two descriptions is, and there is no good association between the descriptions and the results either. People going to the textual outline could also be said to having entered the domain because they can go directly to the articles by clicking on their listings in the outline, and people going to the graphical overview could also be said to having seen the domain contents, albeit only on a high level. This problem is enlarged by having the NAME fields in Figure 6 refer to the mainly graphical overview screens while the PICTURE fields refer to the textual outline screens. Using the opposite link assignments would have maximized the stimulus-response compatibility and thus reduced the user's need to read the explanatory text.</p>
<p>
	Even though the Catalog includes many hypertext cross reference links (as shown in Figure 8c) and an option to jump to the location of a search term (as shown in Figure 7), it has no explicit option for users to return to wherever their location in the information space was prior to the jump. If users know how to do so, they can of course use HyperCard's built-in backtrack facility to go to previously seen cards one at a time, but since users often micro-navigate upon arrival at a new destination, there might be a fair number of intervening cards before the user actually returns to the point of departure for a jump.</p>
<p>
	Other problems are more related to the information which is put into the framework of the user interface design, and not to that design itself. Of course this distinction is only of interest to user interface professionals, since normal users will only care whether it is easy to do what they want to do, and not why it may be easy or difficult. An example of this class of problem is the clusters in the Research section with the names "Reference Books I", "Reference Books II", and "Reference Books III" which surely do not reveal very much about their respective contents. The design of the Catalog user interface requires that articles are grouped in meaningful clusters which can be given telling names for display in section cards, pop-op menus, etc. If that is not possible, then an alternative user interface design is called for, but in this case I suspect that it would have been possible to group the 18 articles in two or three semantically significant clusters.</p>
<p>
	<img alt="Fig. 9a: Whole Earth Catalog sample article" src="http://media.nngroup.com/media/editor/2012/10/30/wholeearth6.gif" style="width: 512px; height: 342px; "/></p>
<p>
	<img alt="Fig. 9b: Whole Earth Catalog article with structural hypertext pop-up" src="http://media.nngroup.com/media/editor/2012/10/30/wholeearth7.gif" style="width: 512px; height: 342px; "/></p>
<p>
	<strong>Figure 9 a and b. </strong> <em> Two articles from the Electronic Whole Earth Catalog. Fig. 9a is an article from the "Livelihood" domain, while Fig. 9b is from the "Community" domain as indicated by its differing background picture showing a large group of people. In Fig. 9b we have also called up the pop-up menu which is accessible from most screens by clicking on the menu button. The top four menu items list the levels of the hierarchy above the current article (main screen, domain, section, cluster), thus providing at the same time an indication of the user's location in the hypertext structure ("where am I?") and a way to jump several levels up the hierarchy in a single operation ("where can I go from here?"). The only information missing in this user interface is "where did I come from?" </em></p>
<p>
	Another problem is that some articles are too long for the purely linear access mechanism in this user interface design. For example, the article "Intelligence Magazines Survey" takes up 25 screens, starting with a nine screen survey discussing the merits of various espionage magazines followed by 16 screens of addresses for magazine subscriptions. It would have been nice to have been able to take a hypertext jump directly from the discussion of a magazine in the first part of the article to the address of that magazine at the end of the article, instead of being forced to do a linear search for the magazine through all 25 screens, but the Catalog does not support hypertext links to destinations within a given article. Again, a change to the overall user interface design might not have been needed if the text of this article had been split up into several smaller articles.</p>
<p>
	To conclude, the Electronic Whole Earth Catalog has an impressive user interface design using structure and various interaction techniques to support the user's navigation and sense of location. It is also useful for actual, real world tasks because of its high-quality information and good search and navigation facilities.</p>
<h2>
	Conclusion</h2>
<p>
	When analyzing the usability of hypertext products, many of the same <a href="../ten-usability-heuristics/index.php"> principles will apply as for analysis of other human-computer interfaces</a>. For example, the principle of consistency can be used to discover that there is a problem with the placement of the backtrack icon in three different corners of the screen in different parts of the Time Table of History. From our discussion of the three hypertexts reviewed here it seems that we can identify some additional principles for the analysis of hypertext usability.</p>
<p>
	Since hypertext is heavily based on navigation, it is no surprise that the issue of navigational dimensions is one such principle. Even a primitive navigational dimension such as the beanstalk in The Manhole helps users orient themselves, and a complex structure such as the Electronic Whole Earth Catalog can make good use of several navigational dimensions if the user can keep them apart. So our principle should be that we should look for navigational dimensions in a hypertext structure and explicitly support the user's understanding of these dimensions by use of graphics design, animated transitions between nodes using visual techniques to emphasize the directionality of navigational movement along the dimension traveled, and possibly other interaction techniques. By explicitly supporting the user's understanding, we mean that we should aid the user in forming a conceptual model of the information space which can be structured according to these dimensions. Directionality should not only be used to keep navigational dimensions separate (e.g. an up-down dimension vs. a left-right dimension) but also to emphasize users' sense of moving back and forth in opposite directions along the same dimension when they perform a navigational transition and its inverse. The Electronic Whole Earth Catalog partly fails with respect to this latter principle. Directionality is especially important for users in the case of literal navigational dimensions such as the page-turning mechanism for moving between the screens within individual articles in the Electronic Whole Earth Catalog and it is likely that such literal dimensions will be the easiest for users to understand.</p>
<p>
	The user should be given answers to the questions "where am I?", "where can I go from here", and "how did I get here/where did I come from?". These questions are not unique to hypertext and were indeed discussed in one of the early papers on general usability principles [Nievergelt and Weydert 1980]. Addressing these questions is especially important for hypertext user interfaces, however, and we should look for the following four elements in a hypertext:</p>
<ul>
	<li>
		<strong>Landmarks </strong> as the beanstalk in The Manhole or the main screen in the Electronic Whole Earth Catalog are special locations in the information space which have been given prominence in some way and which are easily accessible. The Electronic Whole Earth Catalog main screen is reachable from any other screen by a click on the Earth icon while the beanstalk in The Manhole must be found by the user. This is only acceptable because The Manhole is an entertainment product.</li>
	<li>
		<strong>Locational orientation </strong> can partly be provided relative to landmarks but should also be supported by overview diagrams (not used in these three hypertexts) or by a distinct graphics design to identify the current neighborhood in the information space such as the varying domain backgrounds in the Electronic Whole Earth Catalog.</li>
	<li>
		<strong>History lists </strong> provide users with information about their previous locations in the information space and allow them to easily abandon a disgression. None of the hypertexts reviewed here had such a facility and this proved irritating at several occasions.</li>
	<li>
		<strong>Backtrack </strong> mechanisms are a special form of implicit history list used to allow users to return to their previous location in a single operation. A backtrack facility will encourage exploratory behavior if users can rely on always finding it uniformly wherever they go, but unfortunately none of the three hypertexts provided backtrack consistently.</li>
</ul>
<h3>
	Reference</h3>
<ol>
	<li>
		Nievergelt, J. and Weydert, J.: "Sites, modes and trails: Telling the user of an interactive system where he is, what he can do, and how to get to places," in: Guedj, R.A., ten Hagen, P.J.W., Hopgood, F.R.A., Tucker, H.A., and Duce, D.A. (Eds.): Methodology of Interaction, North Holland Publishing Company, 1980, pp. 327-338.</li>
</ol>
</section>
  

  
  <section>
    
        
    
  </section>
  
  
  
  
  

  


  <section>
    <br />
    <p class="share"><b>Share <span class="hide-for-small-down">this article</span>:</b> <a href="https://twitter.com/intent/tweet?url=http://www.nngroup.com/articles/cdrom/&amp;text=Three%20HyperCard%20Stacks%20on%20CD-ROM:%20A%20Review%20&amp;via=nngroup">Twitter</a> | <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=http://www.nngroup.com/articles/cdrom/&amp;title=Three%20HyperCard%20Stacks%20on%20CD-ROM:%20A%20Review%20&amp;source=Nielsen%20Norman%20Group">LinkedIn</a> | <a href="https://plus.google.com/share?url=http://www.nngroup.com/articles/cdrom/">Google+</a> | <a href="mailto:?subject=NN/g Article: Three HyperCard Stacks on CD-ROM: A Review &amp;body=http://www.nngroup.com/articles/cdrom/">Email</a></p>
    <hr />
  </section>

  <footer class="l-subsection">
    
    
    <div class="learn-more">
      <h2>Learn More</h2>
      <p><a href="../subscribe/index.php">Subscribe to the weekly newsletter</a> to get notified about future articles.</p>
      </div>
    
    

    <div class="row show-for-small-down related-content">
      
      
      

      
      
        <div class="column small-12 topic-links">All articles about:
          <ul>
            
            <li><a href="../../topic/navigation/index.php">Navigation</a></li>
            
            <li><a href="../../topic/technology/index.php">new technologies</a></li>
            
          </ul>
        </div>
      
      

      
      
        <div class="column small-12">
          <h3>UX Conference Training Courses</h3>
           <ul class="no-bullet">
    
        <li><a href="../../courses/emerging-patterns-web-design/index.php">Emerging Patterns for Web Design</a></li>
    
        <li><a href="../../courses/information-architecture/index.php">Information Architecture</a></li>
    
        <li><a href="../../courses/navigation-design/index.php">Navigation Design</a></li>
    
</ul>
        </div>
      
      

      
      
        <div class="small-12 column">
          <h3>Research Reports</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../reports/intranet-navigation-layout-and-text/index.php">Vol. 07: Navigation and Page Layout</a></li>
              
            
              
                <li><a href="../../reports/intranet-information-architecture-design-methods/index.php">Intranet Information Architecture Design Methods and Case Studies</a></li>
              
            
              
                <li><a href="../../reports/university/index.php">University Websites</a></li>
              
            
          </ul>
        </div>
      
      

      
      
        <div class="small-12 column">
          <h3>Online Seminars</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../online-seminars/intranet-users-find/index.php">Helping Intranet Users Find What They Need</a></li>
              
            
              
                <li><a href="../../online-seminars/beyond-screens/index.php">Beyond Screens</a></li>
              
            
          </ul>
        </div>
      
      
    </div>

    <div class="hide-for-small-down row related-content">
      <div class="medium-6 columns">
        
        
          <div>
            <h3>Articles</h3>
            <ul class="no-bullet">
              
                
                <li><a href="../universal-navigation/index.php">Universal Navigation: Connecting Subsites to Main Sites</a></li>
                
              
                
                <li><a href="../centered-logos/index.php">Centered Logos Hurt Website Navigation</a></li>
                
              
                
                <li><a href="../title-attribute/index.php">Using the Title Attribute to Help Users Predict Where They Are Going</a></li>
                
              
                
                <li><a href="../duplicate-links/index.php">The Same Link Twice on the Same Page: Do Duplicates Help or Hurt?</a></li>
                
              
                
                <li><a href="../learn-more-links/index.php">“Learn More” Links: You Can Do Better</a></li>
                
              
            </ul>
          </div>
        
        

        
          
          <div>
            <h3>Research Reports</h3>
            <ul class="no-bullet">
              
                
                  <li><a href="../../reports/intranet-navigation-layout-and-text/index.php">Vol. 07: Navigation and Page Layout</a></li>
                
              
                
                  <li><a href="../../reports/intranet-information-architecture-design-methods/index.php">Intranet Information Architecture Design Methods and Case Studies</a></li>
                
              
                
                  <li><a href="../../reports/university/index.php">University Websites</a></li>
                
              
            </ul>
          </div>
          
        

      </div>
      <div class="medium-6 columns">
        
          
          <div>
            <h3>UX Conference Training Courses</h3>
             <ul class="no-bullet">
    
        <li><a href="../../courses/emerging-patterns-web-design/index.php">Emerging Patterns for Web Design</a></li>
    
        <li><a href="../../courses/information-architecture/index.php">Information Architecture</a></li>
    
        <li><a href="../../courses/navigation-design/index.php">Navigation Design</a></li>
    
</ul>
          </div>
          
        
        
          
          <div>
            <h3>Online Seminars</h3>
            <ul class="no-bullet">
            
                <li><a href="../../online-seminars/intranet-users-find/index.php">Helping Intranet Users Find What They Need</a></li>
            
                <li><a href="../../online-seminars/beyond-screens/index.php">Beyond Screens</a></li>
            
            </ul>
          </div>
          
        
      </div>
    </div>
  </footer>
</article>

        </div>

      

    
  </div>
  <div class="row">
    
      <div class="column small-12 show-for-small-down l-navrail announcements">
        
          
        
      </div>
    
    
  </div>

    </div>
    




<footer id="page-footer">
    <div class="row hide-for-print">
        <div class="column small-12 medium-6">
            <section class="footer-popular">
                
                <h1>Popular Topics</h1>
                <div style="float: left;  width: 15em; display: block;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/applications/index.php">Application Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/e-commerce/index.php">E-Commerce Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/eyetracking/index.php">Eyetracking </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/information-architecture/index.php">Information Architecture </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/intranets/index.php">Intranet Design </a></li>
	<li><a href="../../topic/interaction-design/index.php">Interaction Design </a></li>
</ul>
</div>

<div style="float: left;  width: 15em; display: block; list-style: none;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/mobile-and-tablet-design/index.php">Mobile and Tablet Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/credibility-persuasion/index.php">Persuasive Design</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/research-methods/index.php">Research Methods</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/user-testing/index.php">User Testing </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/web-usability/index.php">Web Usability </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/writing-web/index.php">Web Writing &amp; Content Strategy </a></li>
</ul>
</div>

<p>&nbsp;</p>

                
            </section>
        </div>

        <div class="column small-12 medium-6">
            <div class="row">
                <div class="column small-6">
                    <section class="footer-about">
                        <h1>Follow Us</h1>
                        <ul>
	<li><a href="../subscribe/index.php">Subscribe to our weekly UX newsletter</a>,&nbsp;Jakob Nielsen&#39;s Alertbox</li>
	<li>Visit Don Norman&#39;s website, <a href="http://www.jnd.org/"> www.jnd.org </a></li>
	<li>Visit Bruce Tognazzini&#39;s website, <a href="http://www.asktog.com/"> www.asktog.com </a></li>
	<li style="padding-top: 6px;"><a class="twitter-follow-button" data-show-count="false" data-size="large" href="https://twitter.com/nngroup"><img alt="Follow @nngroup on Twitter" height="20" src="https://media.nngroup.com/media/editor/2016/12/30/twitter-follow-button.png" style="width: 117px; height: 20px;" width="117" /></a></li>
	<li style="padding-top: 6px;"><img alt="" height="14" src="https://media.nngroup.com/media/editor/2016/07/26/feed-icon-14x14.png" style="vertical-align:text-top;width:14px;margin-right:.25em;" width="14" /><a href="../../feed/rss/index.php">Subscribe to our RSS feed</a></li>
</ul>

                    </section>
                </div>

                <div class="column small-6">
                    <section class="footer-about right-column">
                        <h1>About
</h1>
                        <ul class="no-bullet">
	<li><a href="../../about/contact/index.php">Contact information</a></li>
	<li><a href="../../about/index.php">About Us</a></li>
	<li><a href="../../about/why-nng/index.php">Why NN/g? </a></li>
	<li><a href="../../return-policy/index.php">Return Policy</a></li>
</ul>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="copyright column small-12">
            <p><a href="../../copyright-and-reprint-info/index.php">Copyright</a> &copy; 1998-2017 Nielsen Norman Group, All Rights Reserved.</p>
        </div>
    </div>
</footer>


    


        <script src="https://media.nngroup.com/static/vendor/fastclick/js/fastclick.min.js"></script>
        <script src="https://media.nngroup.com/static/vendor/foundation/js/foundation.min.js"></script>
        <script src="http://media.nngroup.com/static/js/app.js"></script>
        <script src="http://media.nngroup.com/static/js/scroll-to-fixed.js"></script><script>var externalLinks = document.querySelectorAll('a[href^="http"]');for (var i = externalLinks.length-1; i >= 0; i--) {externalLinks[i].addEventListener("click", function(event) { alert("External links are disabled for this test."); event.preventDefault(); }, false);}var mailLinks = document.querySelectorAll('a[href^="mailto"]');for (var i = mailLinks.length-1; i >= 0; i--) {mailLinks[i].addEventListener("click", function(event) { alert("Email links are disabled for this test."); event.preventDefault(); }, false);}var externalForms = document.querySelectorAll('form');for (var i = externalForms.length-1; i >= 0; i--) {externalForms[i].addEventListener("submit", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false); externalForms[i].addEventListener("click", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false);}</script>

        <script>
            
            $(function() {
                $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
                $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
            });
        </script>

        
<script src="https://media.nngroup.com/static/js/articles.js"></script>


        
        <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1072699960;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072699960/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
    </body>

<!-- Mirrored from www.nngroup.com/articles/cdrom/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:15:33 GMT -->
</html>
