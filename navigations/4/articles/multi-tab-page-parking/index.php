<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->





    
<!-- Mirrored from www.nngroup.com/articles/multi-tab-page-parking/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:13:13 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1026.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"beacon":"bam.nr-data.net","queueTime":4,"applicationTime":844,"agent":"","licenseKey":"1a30ef9931","errorBeacon":"bam.nr-data.net","applicationID":"2048992","transactionName":"NFxSMRBUX0EDUkIPDA0WdhAMVkVbDV8ZBxEXUFMJB0YfRAtUQRVZIktEDAFZVHYHRVcPD01eVRE="}</script>
        <title>Page Parking: Millennials&#39; Multi-Tab Mania</title><meta property="og:title" content="Page Parking: Millennials&#39; Multi-Tab Mania" />
  
        
        <meta name="description" content="Browser tabs separate the collection and comparing of information and serve as memory aids for the alternatives considered in shopping or researching tasks.">
        <meta property="og:description" content="Browser tabs separate the collection and comparing of information and serve as memory aids for the alternatives considered in shopping or researching tasks." />
        
  
        
	
        
        <meta name="keywords" content="browsers, web browsers, tabs, tab, multitab, parallel browsing, Millennials, young adults, young adult users, page parking, memory, pogo-sticking, pogosticking, information foraging, information hunting, information digestion, comparing options, comparisons, comparison, SERP, SERPs, search, favicon, favicons, page title, page titles, breadcrumbs, navigation, new links, links, analytics">
        

        

        <meta name="robots" content="noarchive">
        <meta name="viewport" content="width=device-width">

        <link rel="search" type="application/opensearchdescription+xml" title="NN/g Search" href="../../opensearch.xml" />
        <link rel="shortcut icon" href="http://media.nngroup.com/static/img/favicon.ico" />

        <link rel="stylesheet" href="https://media.nngroup.com/static/vendor/font-awesome/css/font-awesome.min.css">

        
        <link rel="stylesheet" href="https://media.nngroup.com/static/css/7232a960f1df.css" type="text/css" />
        <link rel="stylesheet" href="http://fast.fonts.com/t/1.css?apiType=css&amp;projectid=c5846260-9def-46ec-9407-0f1349bd2d64" type="text/css">

        <script src="https://media.nngroup.com/static/vendor/modernizr/js/modernizr.min.js"></script>

        <script type="text/javascript" src="https://media.nngroup.com/static/js/3283be5def74.js"></script>
        

        

        
        
        <!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=229034,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('http://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->


<!-- Google Analytics Tracking Code for http://nngroup.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23132681-1', 'nngroup.com');
  ga('require', 'linkid', 'https://www.nngroup.com/articles/multi-tab-page-parking/linkid.js');
  ga('require', 'displayfeatures');

// Optimizely Universal Analytics Integration
 window.optimizely = window.optimizely || [];
 window.optimizely.push("activateUniversalAnalytics");

  ga('send', 'pageview');

</script>
<script>
/**
* Function to track a click on an outbound link in Google Analytics.
*/
function trackOutboundLink(label, url) {
    if(window.ga && ga.loaded) {
        ga('send', 'event', 'outbound', 'click', label, {'hitCallback': function(){document.location = url;}
        });
    } else {
        document.location = url;
    }
}
/**
* Function to track a click on a link in the article announcement area.
*/
$('#articleAnnouncement a').on('click', function(){
  ga('send', 'event', 'link', 'click', 'articleAnnouncement');
});
</script>

<script type="text/javascript">
 setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2421.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



<!-- Hotjar Tracking Code for http://nngroup.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:121605,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

        <script src="https://media.nngroup.com/static/js/analytics/track_outbound_link.js"></script>
        
        
        
    </head>
    <body class="location-articles articles-detail">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
    <header id="page-header">
        <div id="cart" class="shopping-cart mobile-cart show-for-small-down"></div>
        <div class="row">
            <div id="masthead" class="masthead small-12 medium-7 column">

                <!-- The button to toggles menu on small screen -->

                <h1><a href="../../index.php"><img src="http://media.nngroup.com/static/img/logo-wordmark.png" width="74" height="30"> Nielsen <span class="norman">Norman</span> Group</a></h1>
                <p class="tagline show-for-medium-up">Evidence-Based User Experience Research, Training, and Consulting</p>

                <!-- The button to toggles form search on small screen -->


                <div style="clear: right;width:  100%;height: 1px;"></div><?php define('ROOT_PATH',$_SERVER["DOCUMENT_ROOT"] . '/apps/navigation_styles/navigations/4'); ?><?php include(ROOT_PATH)."/navigation.php"; ?>

                <!-- Drop down menu on small screen -->
                <div id="hideNav" class="show-for-small-down">
                  
                  <ul class="menu-content">
                    <li><a href="../../index.php">Home</a></li>
                    <li class="has-submenu">
                      <a>Training</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../training/index.php">Overview</a></li>
                        
<li id="nav-ux-conference"><a href="../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../online-seminars/index.php"><span>Online Seminars</span></a></li>

                      </ul>
                    </li>
                    <li class="has-submenu">
                      <a>Consulting</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
  <li><a href="../../consulting/index.php">Overview</a></li>
  
  <li><a href="../../consulting/benchmarking/index.php">Benchmarking</a></li>
  
  <li><a href="../../consulting/expert-design-review/index.php">Expert Design Review</a></li>
  
  <li><a href="../../consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
  
  <li><a href="../../consulting/usability-testing/index.php">Usability Testing </a></li>
  
  <li><a href="../../consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
  
  <li><a href="../../in-house-training/index.php">In-House Training</a></li>
</ul>

                    </li>

                    <li><a href="../../reports/index.php">Reports</a></li>
                    <li><a href="../index.php">Articles</a></li>
                    <li class="has-submenu"><a>About NN/G</a><i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../about/index.php">Overview</a></li>
                        <li><a href="../../people/index.php">People</a></li>
                        <li><a href="../../about/why-nng/index.php">Why NN/g?</a></li>
                        <li><a href="../../about/contact/index.php">Contact</a></li>
                        <li><a href="../../news/index.php">News</a></li>
                        <li><a href="../../about/history/index.php">History</a></li>
                        <li><a href="../../books/index.php">Books</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                </div>

                <hr class="header-divider show-for-small-only"/>


            </div>
       </div>

        <div id="hideNav">            <nav class="primary-nav small-12 column">
                <ul>
                    <li><a class="navitem" id="nav-home" href="../../index.php">Home</a></li>
                    <li><a class="navitem" id="nav-training" href="../../training/index.php">Training</a></li>
                    <li><a class="navitem" id="nav-consulting" href="../../consulting/index.php">Consulting</a></li>
                    <li><a class="navitem" id="nav-reports" href="../../reports/index.php">Reports</a></li>
                    <li><a class="navitem" id="nav-articles" href="../index.php">Articles</a></li>
                    <li><a class="navitem" id="nav-about" href="../../about/index.php">About NN/G</a></li>
                </ul>
            </nav>
            <nav class="secondary-nav small-12 medium-12 large-12 columns">
                
                
            </nav>
        </div>
    </header>
    <div class="l-content">
    
  

  

  <div class="row">
    
      
      <section class="small-12 medium-3 columns collapsable-content l-navrail articles-left-column">
        <h1 class="show-for-small-down collapse-link mobile-browse-header">Browse by Topic and Author</h1>
          <div class="row collapsable">

            
            <div class="small-6 medium-12 columns">
              <section id="article-popular-topics" style="display: none;">
                
                <h1>Topics</h1>
                <ul id="article-popular-topics">
                    
                    <li>
                        
                        <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/intranets/all/index.php">Intranets</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/user-testing/all/index.php">User Testing</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                        
                    </li>
                    
                    <li><a href="#" id="article-see-all-topics"><strong>See all topics&hellip;</strong></a></li>
                </ul>
                
              </section>
              <section id="article-all-topics" style="display: none;">
                
                <h1>
                  <span class="show-for-medium-up">All Article Topics</span>
                  <span class="show-for-small-down">All Topics</span>
                  (<a href="#" id="article-hide-all-topics">hide</a>)
                </h1>
                <ul>
                  
                  <li>
                    
                    <a href="../../topic/accessibility/all/index.php">Accessibility</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/agile/all/index.php">Agile</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/analytics-and-metrics/all/index.php">Analytics &amp; Metrics</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/applications/all/index.php">Application Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/b2b-websites/all/index.php">B2B Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/behavior-patterns/all/index.php">Behavior Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/branding/all/index.php">Branding</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/content-strategy/all/index.php">Content Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/corporate-websites/all/index.php">Corporate Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/design-patterns/all/index.php">Design Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-design-process/all/index.php">Design Process</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/email/all/index.php">Email</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/eyetracking/all/index.php">Eyetracking</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/heuristic-evaluation/all/index.php">Heuristic Evaluation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/human-computer-interaction/all/index.php">Human Computer Interaction</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-ideation/all/index.php">Ideation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/information-architecture/all/index.php">Information Architecture</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/interaction-design/all/index.php">Interaction Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/international-users/all/index.php">International Users</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/intranets/all/index.php">Intranets</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-management/all/index.php">Management</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/navigation/all/index.php">Navigation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/non-profit-websites/all/index.php">Non-Profit Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/personas/all/index.php">Personas</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/persuasive-design/all/index.php">Persuasive Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/prototyping/all/index.php">Prototyping</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/psychology-and-ux/all/index.php">Psychology and UX</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/research-methods/all/index.php">Research Methods</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/search/all/index.php">Search</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/social-media/all/index.php">Social Media</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/strategy/all/index.php">Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/user-testing/all/index.php">User Testing</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-teams/all/index.php">UX Teams</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/visual-design/all/index.php">Visual Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/writing-web/all/index.php">Writing for the Web</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/young-users/all/index.php">Young Users</a>
                    
                  </li>
                  
                </ul>
                
              </section>
            </div>
            <script>
              /* Determine which topics list will be displayed: the full or the
               * partial.  We are inlining this close to the list itself to
               * prevent rendering delays on the client
               */
              var topic_list = $.cookie("article-topics");

              var popular_topics = $('#article-popular-topics');
              var all_topics = $('#article-all-topics');

              if(topic_list == 1) {
                  popular_topics.hide();
                  all_topics.show();
              } else {
                  popular_topics.show();
                  all_topics.hide();
              }
            </script>
            

            
            <div class="small-6 medium-12 columns">
              
              <section id="article-initial-authors">
                  <h1>Author</h1>
                  <ul>
                    
                    <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                    
                    <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                    
                    <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                    
                    <li><a href="#" onClick="$('#article-initial-authors').hide(); $('#article-all-authors').show();"><strong>See all authors&hellip;</strong></a></li>
                  </ul>
              </section>
              <section id="article-all-authors" style="display: none;">
                <h1>
                  All Authors
                  (<a href="#" id="article-hide-all-authors">hide</a>)
                </h1>
                <ul>
                  
                  <li><a href="../author/raluca-budiu/index.php">Raluca Budiu</a></li>
                  
                  <li><a href="../author/patty-caya/index.php">Patty Caya</a></li>
                  
                  <li><a href="../author/susan-farrell/index.php">Susan Farrell</a></li>
                  
                  <li><a href="../author/therese-fessenden/index.php">Therese Fessenden</a></li>
                  
                  <li><a href="../author/kim-flaherty/index.php">Kim Flaherty</a></li>
                  
                  <li><a href="../author/sarah-gibbons/index.php">Sarah Gibbons</a></li>
                  
                  <li><a href="../author/aurora-harley/index.php">Aurora Harley</a></li>
                  
                  <li><a href="../author/kate-kaplan/index.php">Kate Kaplan</a></li>
                  
                  <li><a href="../author/page-laubheimer/index.php">Page Laubheimer</a></li>
                  
                  <li><a href="../author/angela-li/index.php">Angie Li</a></li>
                  
                  <li><a href="../author/hoa-loranger/index.php">Hoa Loranger</a></li>
                  
                  <li><a href="../author/kate-meyer/index.php">Kate Meyer</a></li>
                  
                  <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                  
                  <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                  
                  <li><a href="../author/kara-pernice/index.php">Kara Pernice</a></li>
                  
                  <li><a href="../author/christian-rohrer/index.php">Christian Rohrer</a></li>
                  
                  <li><a href="../author/amy-schade/index.php">Amy Schade</a></li>
                  
                  <li><a href="../author/katie-sherwin/index.php">Katie Sherwin</a></li>
                  
                  <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                  
                  <li><a href="../author/kathryn-whitenton/index.php">Kathryn Whitenton</a></li>
                  
                </ul>
              </section>
              
            </div>
            
          </div>

          
          <section class="hide-for-small-down">
            
              
              <h1>Recent Articles</h1>
              
              <ul>
                  
                  <li><a href="../seamless-cross-channel/index.php">Seamlessness in the Omnichannel User Experience</a></li>
                  
                  <li><a href="../brand-intention-interpretation/index.php">Experience Design: Bridging Brand Intention and Brand Interpretation</a></li>
                  
                  <li><a href="../eyetracking-tasks-efficient-scanning/index.php">Scanning Patterns on the Web Are Optimized for the Current Task</a></li>
                  
                  <li><a href="../flat-design-best-practices/index.php">Flat-Design Best Practices</a></li>
                  
                  <li><a href="../ux-user-stories/index.php">Accounting for UX Work with User Stories in Agile Projects</a></li>
                  
                  <li><a href="../index.php"><strong>See all articles&hellip;</strong></a></li>
              </ul>
              
            
          </section>
          

          
          
          <section class="hide-for-small-down">
            <h1>Popular Articles</h1>
            
            <ul>
                
                <li><a href="../usability-101-introduction-to-usability/index.php">Usability 101: Introduction to Usability</a></li>
                
                <li><a href="../top-10-mistakes-web-design/index.php">Top 10 Mistakes in Web Design</a></li>
                
                <li><a href="../how-users-read-on-the-web/index.php">How Users Read on the Web</a></li>
                
                <li><a href="../f-shaped-pattern-reading-web-content/index.php">F-Shaped Pattern For Reading Web Content</a></li>
                
                <li><a href="../ten-usability-heuristics/index.php">10 Usability Heuristics for User Interface Design</a></li>
                
                <li><a href="../intranet-design-2016/index.php">10 Best Intranets of 2016</a></li>
                
                <li><a href="../which-ux-research-methods/index.php">When to Use Which User-Experience Research Methods</a></li>
                
                <li><a href="../response-times-3-important-limits/index.php">Response Times: The 3 Important Limits</a></li>
                
                <li><a href="../why-you-only-need-to-test-with-5-users/index.php">Why You Only Need to Test with 5 Users</a></li>
                
                <li><a href="../page-fold-manifesto/index.php">The Fold Manifesto: Why the Page Fold Still Matters</a></li>
                
            </ul>
          </section>
          
          

          
            <section class="hide-for-small-down">
              
            </section>
          

      </section>
      

      
        <div class="small-12 medium-9 columns content-body">
          




<article class="l-subsection alertbox-article-content article-detail-bl">
  
  <header>
    <div class="l-subsection">
      <h1>Page Parking: Millennials&#39; Multi-Tab Mania</h1>
      <div class="author-meta">by
        
          
            
            <a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a>
            
            and
            
            <a href="../author/kate-meyer/index.php">Kate Meyer</a>
            
          
        on  November 1, 2015
        
      </div>

      

      
      <div class="topic-links">
        Topics:
        
          <ul>

  <li><a href="../../topic/young-users/index.php">Young Users</a></li>

  <li><a href="../../topic/behavior-patterns/index.php">Behavior Patterns</a></li>

  <li><a href="../../topic/web-usability/index.php">Web Usability</a></li>

</ul>
        
      </div>
      
    </div>
  </header>
  

  
  
  <section>
    <p class="lede"><strong>Summary:</strong> Browser tabs separate the stages of collection and comparing and serve as memory aids to keep many alternate pages available for consideration as users are shopping or researching.  Follow 7 UX guidelines to better support this user behavior, which is particularly common among younger users.</p>
    <br />
  </section>
  
  

  
  <section id="article-body"><p>How do people use the tabs in modern browsers? The ability to <strong>keep multiple pages open</strong> at the same time in different tabs can be used for <strong>parallel browsing,</strong> where a user alternates between tasks and resurfaces a tab when it’s time to work on the task in that tab. For example, a user might keep Facebook open all day in a tab that’s checked for updates from time to time.</p>

<p>Our recent user studies for the course <a href="../../courses/designing-millennials/index.php">Designing for Young Adult Users</a> found that young adult users engage in another tab-related behavior, which we call <strong>page parking</strong>: opening multiple pages in rapid-fire succession as a way to save the items on those pages and revisit them at a later stage. This behavior often occurs when shopping, researching, or reading news, but can happen in any task where it’s useful to open several similar items, each in a separate tab. Later, after users review the content in the tabs, they may cross off many of the parked items and close the corresponding tabs.</p>

<p>Parallel browsing and page parking have quite different characteristics:</p>

<table border="1" cellpadding="0" cellspacing="0">
	<tbody>
		<tr>
			<td style="width:34%;">
			<p> </p>
			</td>
			<td style="width:33%;">
			<p style="font-size: 120%;"><strong>Parallel Browsing</strong></p>
			</td>
			<td style="width:33%;">
			<p style="font-size: 120%;"><strong>Page Parking</strong></p>
			</td>
		</tr>
		<tr>
			<td>
			<p><strong>Who tends to exhibit the behavior</strong></p>
			</td>
			<td>
			<p>Power users</p>
			</td>
			<td>
			<p>Young adult users (“<a href="../millennials-digital-natives/index.php">Millennials</a>”)</p>
			</td>
		</tr>
		<tr>
			<td>
			<p><strong>Relationship between tabs and user tasks</strong></p>
			</td>
			<td>
			<p>Different tasks in each tab</p>
			</td>
			<td>
			<p>The same task in many tabs</p>
			</td>
		</tr>
		<tr>
			<td>
			<p><strong>Relationship between tabs and websites</strong></p>
			</td>
			<td>
			<p>Mainly one-to-one mapping between sites and tabs</p>
			</td>
			<td>
			<p>Each site often spans many tabs, each holding one item from the site.</p>
			</td>
		</tr>
		<tr>
			<td>
			<p><strong>Relationship of tabs to each other</strong></p>
			</td>
			<td>
			<p>Each tab is usually independent of the other tabs.</p>
			</td>
			<td>
			<p>A few tabs are “parent” or hub pages (e.g., the list of car models on Toyota’s website), and the other tabs are “child” pages that contain items similar to each other, but often competing (e.g., different car models).</p>
			</td>
		</tr>
		<tr>
			<td>
			<p><strong>How many new tabs opened</strong></p>
			</td>
			<td>
			<p>One a time, when a new task is started</p>
			</td>
			<td>
			<p>Many new tabs opened at once, often from a hub such as a <a href="../../reports/ecommerce-ux-homepages-and-category-pages/index.php">category page</a> or search engine results page (SERP)</p>
			</td>
		</tr>
		<tr>
			<td>
			<p><strong>Lifetime of a tab, before it’s closed</strong></p>
			</td>
			<td>
			<p>Long, maybe all day or across several days</p>
			</td>
			<td>
			<p>Often short, as items are considered and then dismissed</p>
			</td>
		</tr>
		<tr>
			<td>
			<p><strong>Duration of each visit to a tab</strong></p>
			</td>
			<td>
			<p>Several minutes</p>
			</td>
			<td>
			<p>Varies from a few seconds (if an item is found wanting) to longer visits</p>
			</td>
		</tr>
	</tbody>
</table>

<p>For sure, saying that page parking is characteristic of younger adults doesn’t mean that older adults don’t do it. However, we observe page parking substantially more often in user research with <a href="../millennials-digital-natives/index.php">Millennials</a> than with other age groups. Conversely, of course, many Millennials are power users and thus also employ parallel browsing, in addition to page parking. The fact that some users engage in both parallel browsing and page parking doesn’t change the fact that these are two different behaviors, used for different purposes, which should be analyzed separately.</p>

<p>The following chart shows an example of page parking: it represents the number of tabs opened and closed as a young adult shopped for clothes for 11 minutes and 20 seconds. During the first minute, she opened many tabs, each with a different fashion item. This was followed by 3 minutes of viewing these alternatives and 2 minutes of eliminating choices by closing some of the tabs. Finally, the user opened several new tabs with additional alternatives and then spent the rest of the session considering them. Alternating periods of rapid <strong>information hunting</strong> (spawning of new tabs) with periods of <strong>information digestion</strong> (consideration and consolidation, when some of those tabs are closed again) is a typical behavior, often associated with page parking.</p>

<figure class="caption"><img alt="Bar chart of the number of open and closed tabs while a Millennial user was online" border="0" height="486" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2015/10/27/multitab-opening-and-closing.png" width="750"/>
<figcaption><em>Growth in the number of browser tabs over time during one usability-testing task. The currently active tabs are shown in blue. Red represents tabs that have been closed by the user; those tabs were thus no longer present in the browser’s tab bar. In total, the study participant employed 12 different tabs for this task, but the maximum number that was open at any one time was 9.</em></figcaption>
</figure>

<h2>Why Millennials Love Multi-Tabs</h2>

<p>Page parking is a reaction against “<a href="../pogo-sticking/index.php">pogo sticking</a>” where users bounce back and forth between a central hub and the pages it links to. Pogo sticking fragments the user experience and is often seen on sites with poor navigation design.</p>

<p>One young adult user said, <em>“I prefer related info to be opened in a new window, so I can keep reading, so I don’t waste my time and have to go all the way back.”</em> Another user said, <em>“I’m just putting them </em>[a bunch of individual products]<em> in tabs so I can see what it looks like, after.”</em> The word “after” is key here: users like page parking in tabs because it allows them to <strong>separate two phases of research</strong>:</p>

<ol>
	<li><strong>Hunting</strong> for information. In one stage (e.g., between timestamps 00:25 and 01:10 in the above example), the user <strong>scans a list of available options</strong> and decides which ones may be worth further consideration. These items are parked in new tabs but are not considered in detail at this stage. Thus, the user’s attention is not sidetracked by analyzing detailed information while he or she is still in an overview mode.</li>
	<li><strong>Digesting</strong> the information. Later, the user turns to <strong>inspecting individual tabs</strong>. During the digestion stage, pages that were saved in the hunting stage may be read or summarily rejected, but in either case, the user can focus on one page at a time. The user may also compare items by rapidly switching back and forth between two tabs.</li>
</ol>

<p>In pogo sticking users are forced to continuously alternate the hunting and the digesting of information, while in page parking these stages can be separated and done one at a time. Why is that good? First, because users don’t need to switch and save context between the two <a href="../the-3cs-of-critical-web-use-collect-compare-choose/index.php">fundamental tasks of collecting and respectively comparing</a>. (Switching task context <a href="../minimize-cognitive-load/index.php">increases the cognitive load</a>, as users must retrieve from memory the details specific to the new task.)</p>

<p>Second, while the pages are parked, the tabs serve as a <strong>memory aid</strong>, freeing users from having to remember the items they are interested in. (The tabs are a form of external memory, or, to quote our colleague Don Norman, “knowledge in the world” as opposed to “knowledge in the head” — see his book <a href="../../books/design-everyday-things-revised/index.php">The Design of Everyday Things</a>.) The ability to offload a memory burden to the user interface is usually a boon to usability and is discussed further in our course on <a href="../../courses/hci/index.php">HCI fundamentals</a>.</p>

<p>Young adult users are impatient. A third benefit of page parking is that it <strong>bypasses waiting for slow pages</strong> to load, since loading happens in a tab that’s currently not visible. This is no excuse for <a href="../website-response-times/index.php">slow response times</a>, however, since users also need to be able to navigate directly to pages without parking them.</p>

<p>The following chart visualizes the tab-opening behavior during 5 minutes of a user researching a purchase. This user’s behavior was slightly unusual: firing off 3 different queries (Q1, Q2, and Q3 in the chart) in 3 new tabs in rapid succession, and then going straight to the 3<sup>rd</sup> SERP first (the search engine results page — Q3 in the chart), returning to the first query (Q1) only late in the session. (Most users have <a href="../incompetent-search-skills/index.php">impoverished query reformulation skills</a>, but this user realized from the beginning that a variety of query strategies would be needed for the task.)</p>

<figure class="caption"><img alt="Timeline showing how 'parent' tabs spawned multiple 'child' tabs during a Millennial user's online research task" border="0" height="434" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2015/10/27/multi-tab-page-parking-activity.png" width="750"/>
<figcaption><em>Page-parking example from a product-research task: each circle indicates one new tab opened by the user. Bigger circles indicate tabs used for search engine queries (Q1–Q4). Note the parent–child relationship between many of the tabs, including “grandchildren,” as some of the destination pages (opened from the search-results page) caused the user to spawn even more tabs. Toward the end of the session, the user attempted one more Google search (Q4) in a new tab and only clicked on one of the hits. Even this hit was first parked in a new tab, following the user’s habitual behavior pattern. This last instance of page parking was unnecessary (and entailed extraneous clicks) since the user didn’t pursue any of the other entries from this search listing.</em></figcaption>
</figure>

<h2>Implications of Page Parking</h2>

<p>We used to say that on the web, your competition is only a click away. That’s why anything but an awesome website is a loser. With page parking, this old argument for the business impact of user experience is ratcheted up a notch: now, several of your competitors’ sites <em>have already been opened</em> and are parked in tabs next to your site. Thus, page parking intensifies all the many existing <a href="../../courses/web-ux-design-guidelines/index.php">guidelines for web UX</a>: any difficulty using your site, and the customer closes your tab and turns to the competition’s tabs. Beyond our old advice, also follow these 7 guidelines for supporting page parking:</p>

<ol>
	<li>First and foremost, allow page parking to happen: <strong>allow users to select a link and open it in a new tab</strong>, using the standard browser controls. Many sites offend horribly against this guideline by encoding their links as JavaScript or other weird code that only allows the link to open within the current tab. Considering that page-parking users are highly engaged customers who’re showing interest in several of your offerings, it’s beyond belief that sites invest in extra programming work only to antagonize these customers.</li>
	<li>Design a good <strong>favicon</strong> so that users can identify the tabs that belong to your site. (The favicon — short for “favorite icon” — is the small 16×16 pixel icon that identifies a website in tabs and the <em>Bookmarks</em>/<em>Favorites</em> menu.) When the tab bar gets sufficiently crowded, the favicon is the one remaining visual to emit <a href="../information-scent/index.php">information scent</a> and remind users to review the pages they’ve parked from your site. Follow guidelines for <a href="../icon-usability/index.php">icon usability</a>, but be sure to make the tiny favicon clean and use fewer details than for regular icons.</li>
	<li><strong>Start each page title with information-carrying words</strong> that differentiate that page from other pages. At best, each tab will show <a href="../first-2-words-a-signal-for-scanning/index.php">2–3 words</a>, but after enough tabs have been opened, only a few characters will show per tab. These may not be enough to allow users to pick the page they want from their parking lot of open tabs. A test user in our study of Canadian Millennials was researching a car purchase and had 12 tabs open with the following text showing in each (including 2 tabs about something else she was also looking for):
	<ul>
		<li style="list-style-type: none; margin: 0px; padding: 0px;">samsung g</li>
		<li style="list-style-type: none; margin: 0px; padding: 0px;">Samsung S</li>
		<li style="list-style-type: none; margin: 0px; padding: 0px;">Honda.ca</li>
		<li style="list-style-type: none; margin: 0px; padding: 0px;">best fuel ec</li>
		<li style="list-style-type: none; margin: 0px; padding: 0px;">2015 Best a</li>
		<li style="list-style-type: none; margin: 0px; padding: 0px;">Compare Si</li>
		<li style="list-style-type: none; margin: 0px; padding: 0px;">Compare Si</li>
		<li style="list-style-type: none; margin: 0px; padding: 0px;">Forbes.com</li>
		<li style="list-style-type: none; margin: 0px; padding: 0px;">Types of El</li>
		<li style="list-style-type: none; margin: 0px; padding: 0px;">best cars fo</li>
		<li style="list-style-type: none; margin: 0px; padding: 0px;">Forbes.com</li>
		<li style="list-style-type: none; margin-top: 0px; padding-top: 0px;">10 Best Use</li>
	</ul>
	Which of these labels allow you to easily remember what you parked in that tab? Later, that same user had 15 tabs open, each showing even less text — making it even harder to remember the content.</li>
	<li value="4"><strong>Use </strong><a href="../breadcrumb-navigation-useful/index.php"><strong>breadcrumbs</strong></a><strong> and “</strong><a href="../navigation-you-are-here/index.php"><strong>you are here</strong></a><strong>”</strong> navigational indicators to remind people how a page relates to the rest of your site. Several minutes may pass between parking a page and visiting it, and users will forget a lot during that time. Also provide <a href="../related-content-pageviews/index.php">related links</a> to allow users to easily get to additional pages (and remember that users may want to park those related pages as well).</li>
	<li value="5">In <a href="../analytics-user-experience/index.php"><strong>website</strong> <strong>analytics</strong></a>, we can’t measure a page’s visit duration as the interval between when a page was opened and when the user left the page. The page may well have been parked in a hidden tab during much of this time, which shouldn’t be counted as active “visit” time.</li>
	<li value="6">Conversely, you may want to add a new analytics metric to <strong>measure the extent of page parking</strong> that happens on your site. The more page parkers, and the more pages they park, the more enticing your content must have been. (Though if the users end up leaving without converting, something’s wrong: content shouldn’t just <em>appear</em> attractive enough to be parked after an initial glance, it should also <em>deliver</em> once users turn their full attention to it later.)</li>
	<li value="7">Finally, <strong>don’t force</strong> page parking on users by automatically opening links in new tabs. Opening a new tab should always be an option under the control of the user and only activated when the user asks for it by using the appropriate browser command. Those users who want to park pages know perfectly well how to do it, but many users don’t want to park pages but want to go straight to the link destination within the current tab.</li>
</ol>

<p>While page parking is a common behavior, it’s not ubiquitous. Regular browsing continues to account for a big share of web usage for both Millennials and older users. Thus, it’s important to continue to support regular <a href="../search-not-enough/index.php">navigation and search</a>, as well as the <em>Back</em> button as a way to move to previously seen pages. The need to support page parking simply adds to the existing duties of the web designer.</p>

<p>(More on the special online behaviors of the Millennial generation and these users' attitudes toward websites in our full-day course <a href="../../courses/designing-millennials/index.php">Designing for Young Adult Users</a>. More on users' web-usage behavior patterns in general in the course <a href="../../courses/web-ux-design-guidelines/index.php">Top Web UX Design Guidelines</a>.)</p>
</section>
  

  
  <section>
    
        
    
  </section>
  
  
  
  
  

  


  <section>
    <br />
    <p class="share"><b>Share <span class="hide-for-small-down">this article</span>:</b> <a href="https://twitter.com/intent/tweet?url=http://www.nngroup.com/articles/multi-tab-page-parking/&amp;text=Page%20Parking:%20Millennials'%20Multi-Tab%20Mania&amp;via=nngroup">Twitter</a> | <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=http://www.nngroup.com/articles/multi-tab-page-parking/&amp;title=Page%20Parking:%20Millennials'%20Multi-Tab%20Mania&amp;source=Nielsen%20Norman%20Group">LinkedIn</a> | <a href="https://plus.google.com/share?url=http://www.nngroup.com/articles/multi-tab-page-parking/">Google+</a> | <a href="mailto:?subject=NN/g Article: Page Parking: Millennials&#39; Multi-Tab Mania&amp;body=http://www.nngroup.com/articles/multi-tab-page-parking/">Email</a></p>
    <hr />
  </section>

  <footer class="l-subsection">
    
    
    <div class="learn-more">
      <h2>Learn More</h2>
      <p><a href="../subscribe/index.php">Subscribe to the weekly newsletter</a> to get notified about future articles.</p>
      </div>
    
    

    <div class="row show-for-small-down related-content">
      
      
      

      
      
        <div class="column small-12 topic-links">All articles about:
          <ul>
            
            <li><a href="../../topic/behavior-patterns/index.php">Behavior Patterns</a></li>
            
            <li><a href="../../topic/millennial-users/index.php">millennial users</a></li>
            
            <li><a href="../../topic/page-parking/index.php">page parking</a></li>
            
            <li><a href="../../topic/web-usability/index.php">Web Usability</a></li>
            
            <li><a href="../../topic/young-users/index.php">Young Users</a></li>
            
          </ul>
        </div>
      
      

      
      
        <div class="column small-12">
          <h3>UX Conference Training Courses</h3>
           <ul class="no-bullet">
    
        <li><a href="../../courses/ux-basic-training/index.php">UX Basic Training</a></li>
    
        <li><a href="../../courses/human-mind/index.php">The Human Mind and Usability</a></li>
    
        <li><a href="../../courses/hci/index.php">User Interface Principles Every Designer Must Know</a></li>
    
        <li><a href="../../courses/emerging-patterns-web-design/index.php">Emerging Patterns for Web Design</a></li>
    
        <li><a href="../../courses/web-page-design/index.php">Web Page UX Design</a></li>
    
</ul>
        </div>
      
      

      
      
        <div class="small-12 column">
          <h3>Research Reports</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../reports/designing-for-young-adults/index.php">Designing for Young Adults (Ages 18-25)</a></li>
              
            
              
                <li><a href="../../reports/teenagers-on-the-web/index.php">Teenagers (Ages 13-17) on the Web</a></li>
              
            
              
                <li><a href="../../reports/children-on-the-web/index.php">Children (Ages 3-12) on the Web</a></li>
              
            
              
                <li><a href="../../reports/b2b-websites-usability/index.php">B2B Website Usability for Converting Users into Leads and Customers</a></li>
              
            
              
                <li><a href="../../reports/university/index.php">University Websites</a></li>
              
            
          </ul>
        </div>
      
      

      
      
        <div class="small-12 column">
          <h3>Online Seminars</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../online-seminars/fundamental-flaw-flat-design/index.php">The Fundamental Flaw in Flat Design</a></li>
              
            
              
                <li><a href="../../online-seminars/millennials-online/index.php">Millennials Online</a></li>
              
            
              
                <li><a href="../../online-seminars/university-websites/index.php">University Website UX Essentials</a></li>
              
            
          </ul>
        </div>
      
      
    </div>

    <div class="hide-for-small-down row related-content">
      <div class="medium-6 columns">
        
        
          <div>
            <h3>Articles</h3>
            <ul class="no-bullet">
              
                
                <li><a href="../needy-design-patterns/index.php">Needy Design Patterns: Please-Don’t-Go Popups &amp; Get-Back-to-Me Tabs</a></li>
                
              
                
                <li><a href="../young-adults-flat-design/index.php">Young Adults Appreciate Flat Design More than Their Parents Do</a></li>
                
              
                
                <li><a href="../millennials-digital-natives/index.php">Millennials as Digital Natives: Myths and Realities</a></li>
                
              
                
                <li><a href="../young-adults-ux/index.php">Young Adults/Millennials as Web Users (Ages 18–25)</a></li>
                
              
                
                <li><a href="../social-media-natives/index.php">Social Media Natives: Growing Up with Social Networking</a></li>
                
              
            </ul>
          </div>
        
        

        
          
          <div>
            <h3>Research Reports</h3>
            <ul class="no-bullet">
              
                
                  <li><a href="../../reports/designing-for-young-adults/index.php">Designing for Young Adults (Ages 18-25)</a></li>
                
              
                
                  <li><a href="../../reports/teenagers-on-the-web/index.php">Teenagers (Ages 13-17) on the Web</a></li>
                
              
                
                  <li><a href="../../reports/children-on-the-web/index.php">Children (Ages 3-12) on the Web</a></li>
                
              
                
                  <li><a href="../../reports/b2b-websites-usability/index.php">B2B Website Usability for Converting Users into Leads and Customers</a></li>
                
              
                
                  <li><a href="../../reports/university/index.php">University Websites</a></li>
                
              
            </ul>
          </div>
          
        

      </div>
      <div class="medium-6 columns">
        
          
          <div>
            <h3>UX Conference Training Courses</h3>
             <ul class="no-bullet">
    
        <li><a href="../../courses/ux-basic-training/index.php">UX Basic Training</a></li>
    
        <li><a href="../../courses/human-mind/index.php">The Human Mind and Usability</a></li>
    
        <li><a href="../../courses/hci/index.php">User Interface Principles Every Designer Must Know</a></li>
    
        <li><a href="../../courses/emerging-patterns-web-design/index.php">Emerging Patterns for Web Design</a></li>
    
        <li><a href="../../courses/web-page-design/index.php">Web Page UX Design</a></li>
    
</ul>
          </div>
          
        
        
          
          <div>
            <h3>Online Seminars</h3>
            <ul class="no-bullet">
            
                <li><a href="../../online-seminars/fundamental-flaw-flat-design/index.php">The Fundamental Flaw in Flat Design</a></li>
            
                <li><a href="../../online-seminars/millennials-online/index.php">Millennials Online</a></li>
            
                <li><a href="../../online-seminars/university-websites/index.php">University Website UX Essentials</a></li>
            
            </ul>
          </div>
          
        
      </div>
    </div>
  </footer>
</article>

        </div>

      

    
  </div>
  <div class="row">
    
      <div class="column small-12 show-for-small-down l-navrail announcements">
        
          
        
      </div>
    
    
  </div>

    </div>
    




<footer id="page-footer">
    <div class="row hide-for-print">
        <div class="column small-12 medium-6">
            <section class="footer-popular">
                
                <h1>Popular Topics</h1>
                <div style="float: left;  width: 15em; display: block;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/applications/index.php">Application Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/e-commerce/index.php">E-Commerce Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/eyetracking/index.php">Eyetracking </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/information-architecture/index.php">Information Architecture </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/intranets/index.php">Intranet Design </a></li>
	<li><a href="../../topic/interaction-design/index.php">Interaction Design </a></li>
</ul>
</div>

<div style="float: left;  width: 15em; display: block; list-style: none;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/mobile-and-tablet-design/index.php">Mobile and Tablet Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/credibility-persuasion/index.php">Persuasive Design</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/research-methods/index.php">Research Methods</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/user-testing/index.php">User Testing </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/web-usability/index.php">Web Usability </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/writing-web/index.php">Web Writing &amp; Content Strategy </a></li>
</ul>
</div>

<p>&nbsp;</p>

                
            </section>
        </div>

        <div class="column small-12 medium-6">
            <div class="row">
                <div class="column small-6">
                    <section class="footer-about">
                        <h1>Follow Us</h1>
                        <ul>
	<li><a href="../subscribe/index.php">Subscribe to our weekly UX newsletter</a>,&nbsp;Jakob Nielsen&#39;s Alertbox</li>
	<li>Visit Don Norman&#39;s website, <a href="http://www.jnd.org/"> www.jnd.org </a></li>
	<li>Visit Bruce Tognazzini&#39;s website, <a href="http://www.asktog.com/"> www.asktog.com </a></li>
	<li style="padding-top: 6px;"><a class="twitter-follow-button" data-show-count="false" data-size="large" href="https://twitter.com/nngroup"><img alt="Follow @nngroup on Twitter" height="20" src="https://media.nngroup.com/media/editor/2016/12/30/twitter-follow-button.png" style="width: 117px; height: 20px;" width="117" /></a></li>
	<li style="padding-top: 6px;"><img alt="" height="14" src="https://media.nngroup.com/media/editor/2016/07/26/feed-icon-14x14.png" style="vertical-align:text-top;width:14px;margin-right:.25em;" width="14" /><a href="../../feed/rss/index.php">Subscribe to our RSS feed</a></li>
</ul>

                    </section>
                </div>

                <div class="column small-6">
                    <section class="footer-about right-column">
                        <h1>About
</h1>
                        <ul class="no-bullet">
	<li><a href="../../about/contact/index.php">Contact information</a></li>
	<li><a href="../../about/index.php">About Us</a></li>
	<li><a href="../../about/why-nng/index.php">Why NN/g? </a></li>
	<li><a href="../../return-policy/index.php">Return Policy</a></li>
</ul>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="copyright column small-12">
            <p><a href="../../copyright-and-reprint-info/index.php">Copyright</a> &copy; 1998-2017 Nielsen Norman Group, All Rights Reserved.</p>
        </div>
    </div>
</footer>


    


        <script src="https://media.nngroup.com/static/vendor/fastclick/js/fastclick.min.js"></script>
        <script src="https://media.nngroup.com/static/vendor/foundation/js/foundation.min.js"></script>
        <script src="http://media.nngroup.com/static/js/app.js"></script>
        <script src="http://media.nngroup.com/static/js/scroll-to-fixed.js"></script><script>var externalLinks = document.querySelectorAll('a[href^="http"]');for (var i = externalLinks.length-1; i >= 0; i--) {externalLinks[i].addEventListener("click", function(event) { alert("External links are disabled for this test."); event.preventDefault(); }, false);}var mailLinks = document.querySelectorAll('a[href^="mailto"]');for (var i = mailLinks.length-1; i >= 0; i--) {mailLinks[i].addEventListener("click", function(event) { alert("Email links are disabled for this test."); event.preventDefault(); }, false);}var externalForms = document.querySelectorAll('form');for (var i = externalForms.length-1; i >= 0; i--) {externalForms[i].addEventListener("submit", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false); externalForms[i].addEventListener("click", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false);}</script>

        <script>
            
            $(function() {
                $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
                $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
            });
        </script>

        
<script src="https://media.nngroup.com/static/js/articles.js"></script>


        
        <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1072699960;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072699960/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
    </body>

<!-- Mirrored from www.nngroup.com/articles/multi-tab-page-parking/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:13:13 GMT -->
</html>
