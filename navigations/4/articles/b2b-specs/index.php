<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->





    
<!-- Mirrored from www.nngroup.com/articles/b2b-specs/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:13:04 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1026.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"beacon":"bam.nr-data.net","queueTime":3,"applicationTime":2421,"agent":"","licenseKey":"1a30ef9931","errorBeacon":"bam.nr-data.net","applicationID":"2048992","transactionName":"NFxSMRBUX0EDUkIPDA0WdhAMVkVbDV8ZBxEXUFMJB0YfRAtUQRVZIktEDAFZVHYHRVcPD01eVRE="}</script>
        <title>B2B Product Specifications Guidelines</title><meta property="og:title" content="B2B Product Specifications Guidelines" />
  
        
        <meta name="description" content="Effective product information on B2B sites satisfies 5 main criteria and can be put together using our spec lists for physical and software products.
">
        <meta property="og:description" content="Effective product information on B2B sites satisfies 5 main criteria and can be put together using our spec lists for physical and software products.
" />
        
  
        
	
        
        <meta name="keywords" content="B2B, Specs, Product specs, specifications, product features, product details, UX, user experience, usability, content">
        

        

        <meta name="robots" content="noarchive">
        <meta name="viewport" content="width=device-width">

        <link rel="search" type="application/opensearchdescription+xml" title="NN/g Search" href="../../opensearch.xml" />
        <link rel="shortcut icon" href="http://media.nngroup.com/static/img/favicon.ico" />

        <link rel="stylesheet" href="https://media.nngroup.com/static/vendor/font-awesome/css/font-awesome.min.css">

        
        <link rel="stylesheet" href="https://media.nngroup.com/static/css/7232a960f1df.css" type="text/css" />
        <link rel="stylesheet" href="http://fast.fonts.com/t/1.css?apiType=css&amp;projectid=c5846260-9def-46ec-9407-0f1349bd2d64" type="text/css">

        <script src="https://media.nngroup.com/static/vendor/modernizr/js/modernizr.min.js"></script>

        <script type="text/javascript" src="https://media.nngroup.com/static/js/3283be5def74.js"></script>
        

        

        
        
        <!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=229034,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('http://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->


<!-- Google Analytics Tracking Code for http://nngroup.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23132681-1', 'nngroup.com');
  ga('require', 'linkid', 'https://www.nngroup.com/articles/b2b-specs/linkid.js');
  ga('require', 'displayfeatures');

// Optimizely Universal Analytics Integration
 window.optimizely = window.optimizely || [];
 window.optimizely.push("activateUniversalAnalytics");

  ga('send', 'pageview');

</script>
<script>
/**
* Function to track a click on an outbound link in Google Analytics.
*/
function trackOutboundLink(label, url) {
    if(window.ga && ga.loaded) {
        ga('send', 'event', 'outbound', 'click', label, {'hitCallback': function(){document.location = url;}
        });
    } else {
        document.location = url;
    }
}
/**
* Function to track a click on a link in the article announcement area.
*/
$('#articleAnnouncement a').on('click', function(){
  ga('send', 'event', 'link', 'click', 'articleAnnouncement');
});
</script>

<script type="text/javascript">
 setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2421.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



<!-- Hotjar Tracking Code for http://nngroup.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:121605,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

        <script src="https://media.nngroup.com/static/js/analytics/track_outbound_link.js"></script>
        
        
        
    </head>
    <body class="location-articles articles-detail">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
    <header id="page-header">
        <div id="cart" class="shopping-cart mobile-cart show-for-small-down"></div>
        <div class="row">
            <div id="masthead" class="masthead small-12 medium-7 column">

                <!-- The button to toggles menu on small screen -->

                <h1><a href="../../index.php"><img src="http://media.nngroup.com/static/img/logo-wordmark.png" width="74" height="30"> Nielsen <span class="norman">Norman</span> Group</a></h1>
                <p class="tagline show-for-medium-up">Evidence-Based User Experience Research, Training, and Consulting</p>

                <!-- The button to toggles form search on small screen -->


                <div style="clear: right;width:  100%;height: 1px;"></div><?php define('ROOT_PATH',$_SERVER["DOCUMENT_ROOT"] . '/apps/navigation_styles/navigations/4'); ?><?php include(ROOT_PATH)."/navigation.php"; ?>

                <!-- Drop down menu on small screen -->
                <div id="hideNav" class="show-for-small-down">
                  
                  <ul class="menu-content">
                    <li><a href="../../index.php">Home</a></li>
                    <li class="has-submenu">
                      <a>Training</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../training/index.php">Overview</a></li>
                        
<li id="nav-ux-conference"><a href="../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../online-seminars/index.php"><span>Online Seminars</span></a></li>

                      </ul>
                    </li>
                    <li class="has-submenu">
                      <a>Consulting</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
  <li><a href="../../consulting/index.php">Overview</a></li>
  
  <li><a href="../../consulting/benchmarking/index.php">Benchmarking</a></li>
  
  <li><a href="../../consulting/expert-design-review/index.php">Expert Design Review</a></li>
  
  <li><a href="../../consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
  
  <li><a href="../../consulting/usability-testing/index.php">Usability Testing </a></li>
  
  <li><a href="../../consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
  
  <li><a href="../../in-house-training/index.php">In-House Training</a></li>
</ul>

                    </li>

                    <li><a href="../../reports/index.php">Reports</a></li>
                    <li><a href="../index.php">Articles</a></li>
                    <li class="has-submenu"><a>About NN/G</a><i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../about/index.php">Overview</a></li>
                        <li><a href="../../people/index.php">People</a></li>
                        <li><a href="../../about/why-nng/index.php">Why NN/g?</a></li>
                        <li><a href="../../about/contact/index.php">Contact</a></li>
                        <li><a href="../../news/index.php">News</a></li>
                        <li><a href="../../about/history/index.php">History</a></li>
                        <li><a href="../../books/index.php">Books</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                </div>

                <hr class="header-divider show-for-small-only"/>


            </div>
       </div>

        <div id="hideNav">            <nav class="primary-nav small-12 column">
                <ul>
                    <li><a class="navitem" id="nav-home" href="../../index.php">Home</a></li>
                    <li><a class="navitem" id="nav-training" href="../../training/index.php">Training</a></li>
                    <li><a class="navitem" id="nav-consulting" href="../../consulting/index.php">Consulting</a></li>
                    <li><a class="navitem" id="nav-reports" href="../../reports/index.php">Reports</a></li>
                    <li><a class="navitem" id="nav-articles" href="../index.php">Articles</a></li>
                    <li><a class="navitem" id="nav-about" href="../../about/index.php">About NN/G</a></li>
                </ul>
            </nav>
            <nav class="secondary-nav small-12 medium-12 large-12 columns">
                
                
            </nav>
        </div>
    </header>
    <div class="l-content">
    
  

  

  <div class="row">
    
      
      <section class="small-12 medium-3 columns collapsable-content l-navrail articles-left-column">
        <h1 class="show-for-small-down collapse-link mobile-browse-header">Browse by Topic and Author</h1>
          <div class="row collapsable">

            
            <div class="small-6 medium-12 columns">
              <section id="article-popular-topics" style="display: none;">
                
                <h1>Topics</h1>
                <ul id="article-popular-topics">
                    
                    <li>
                        
                        <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/intranets/all/index.php">Intranets</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/user-testing/all/index.php">User Testing</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                        
                    </li>
                    
                    <li><a href="#" id="article-see-all-topics"><strong>See all topics&hellip;</strong></a></li>
                </ul>
                
              </section>
              <section id="article-all-topics" style="display: none;">
                
                <h1>
                  <span class="show-for-medium-up">All Article Topics</span>
                  <span class="show-for-small-down">All Topics</span>
                  (<a href="#" id="article-hide-all-topics">hide</a>)
                </h1>
                <ul>
                  
                  <li>
                    
                    <a href="../../topic/accessibility/all/index.php">Accessibility</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/agile/all/index.php">Agile</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/analytics-and-metrics/all/index.php">Analytics &amp; Metrics</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/applications/all/index.php">Application Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/b2b-websites/all/index.php">B2B Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/behavior-patterns/all/index.php">Behavior Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/branding/all/index.php">Branding</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/content-strategy/all/index.php">Content Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/corporate-websites/all/index.php">Corporate Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/design-patterns/all/index.php">Design Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-design-process/all/index.php">Design Process</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/email/all/index.php">Email</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/eyetracking/all/index.php">Eyetracking</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/heuristic-evaluation/all/index.php">Heuristic Evaluation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/human-computer-interaction/all/index.php">Human Computer Interaction</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-ideation/all/index.php">Ideation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/information-architecture/all/index.php">Information Architecture</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/interaction-design/all/index.php">Interaction Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/international-users/all/index.php">International Users</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/intranets/all/index.php">Intranets</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-management/all/index.php">Management</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/navigation/all/index.php">Navigation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/non-profit-websites/all/index.php">Non-Profit Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/personas/all/index.php">Personas</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/persuasive-design/all/index.php">Persuasive Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/prototyping/all/index.php">Prototyping</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/psychology-and-ux/all/index.php">Psychology and UX</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/research-methods/all/index.php">Research Methods</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/search/all/index.php">Search</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/social-media/all/index.php">Social Media</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/strategy/all/index.php">Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/user-testing/all/index.php">User Testing</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-teams/all/index.php">UX Teams</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/visual-design/all/index.php">Visual Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/writing-web/all/index.php">Writing for the Web</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/young-users/all/index.php">Young Users</a>
                    
                  </li>
                  
                </ul>
                
              </section>
            </div>
            <script>
              /* Determine which topics list will be displayed: the full or the
               * partial.  We are inlining this close to the list itself to
               * prevent rendering delays on the client
               */
              var topic_list = $.cookie("article-topics");

              var popular_topics = $('#article-popular-topics');
              var all_topics = $('#article-all-topics');

              if(topic_list == 1) {
                  popular_topics.hide();
                  all_topics.show();
              } else {
                  popular_topics.show();
                  all_topics.hide();
              }
            </script>
            

            
            <div class="small-6 medium-12 columns">
              
              <section id="article-initial-authors">
                  <h1>Author</h1>
                  <ul>
                    
                    <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                    
                    <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                    
                    <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                    
                    <li><a href="#" onClick="$('#article-initial-authors').hide(); $('#article-all-authors').show();"><strong>See all authors&hellip;</strong></a></li>
                  </ul>
              </section>
              <section id="article-all-authors" style="display: none;">
                <h1>
                  All Authors
                  (<a href="#" id="article-hide-all-authors">hide</a>)
                </h1>
                <ul>
                  
                  <li><a href="../author/raluca-budiu/index.php">Raluca Budiu</a></li>
                  
                  <li><a href="../author/patty-caya/index.php">Patty Caya</a></li>
                  
                  <li><a href="../author/susan-farrell/index.php">Susan Farrell</a></li>
                  
                  <li><a href="../author/therese-fessenden/index.php">Therese Fessenden</a></li>
                  
                  <li><a href="../author/kim-flaherty/index.php">Kim Flaherty</a></li>
                  
                  <li><a href="../author/sarah-gibbons/index.php">Sarah Gibbons</a></li>
                  
                  <li><a href="../author/aurora-harley/index.php">Aurora Harley</a></li>
                  
                  <li><a href="../author/kate-kaplan/index.php">Kate Kaplan</a></li>
                  
                  <li><a href="../author/page-laubheimer/index.php">Page Laubheimer</a></li>
                  
                  <li><a href="../author/angela-li/index.php">Angie Li</a></li>
                  
                  <li><a href="../author/hoa-loranger/index.php">Hoa Loranger</a></li>
                  
                  <li><a href="../author/kate-meyer/index.php">Kate Meyer</a></li>
                  
                  <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                  
                  <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                  
                  <li><a href="../author/kara-pernice/index.php">Kara Pernice</a></li>
                  
                  <li><a href="../author/christian-rohrer/index.php">Christian Rohrer</a></li>
                  
                  <li><a href="../author/amy-schade/index.php">Amy Schade</a></li>
                  
                  <li><a href="../author/katie-sherwin/index.php">Katie Sherwin</a></li>
                  
                  <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                  
                  <li><a href="../author/kathryn-whitenton/index.php">Kathryn Whitenton</a></li>
                  
                </ul>
              </section>
              
            </div>
            
          </div>

          
          <section class="hide-for-small-down">
            
              
              <h1>Recent Articles</h1>
              
              <ul>
                  
                  <li><a href="../seamless-cross-channel/index.php">Seamlessness in the Omnichannel User Experience</a></li>
                  
                  <li><a href="../brand-intention-interpretation/index.php">Experience Design: Bridging Brand Intention and Brand Interpretation</a></li>
                  
                  <li><a href="../eyetracking-tasks-efficient-scanning/index.php">Scanning Patterns on the Web Are Optimized for the Current Task</a></li>
                  
                  <li><a href="../flat-design-best-practices/index.php">Flat-Design Best Practices</a></li>
                  
                  <li><a href="../ux-user-stories/index.php">Accounting for UX Work with User Stories in Agile Projects</a></li>
                  
                  <li><a href="../index.php"><strong>See all articles&hellip;</strong></a></li>
              </ul>
              
            
          </section>
          

          
          
          <section class="hide-for-small-down">
            <h1>Popular Articles</h1>
            
            <ul>
                
                <li><a href="../usability-101-introduction-to-usability/index.php">Usability 101: Introduction to Usability</a></li>
                
                <li><a href="../top-10-mistakes-web-design/index.php">Top 10 Mistakes in Web Design</a></li>
                
                <li><a href="../how-users-read-on-the-web/index.php">How Users Read on the Web</a></li>
                
                <li><a href="../f-shaped-pattern-reading-web-content/index.php">F-Shaped Pattern For Reading Web Content</a></li>
                
                <li><a href="../ten-usability-heuristics/index.php">10 Usability Heuristics for User Interface Design</a></li>
                
                <li><a href="../intranet-design-2016/index.php">10 Best Intranets of 2016</a></li>
                
                <li><a href="../which-ux-research-methods/index.php">When to Use Which User-Experience Research Methods</a></li>
                
                <li><a href="../response-times-3-important-limits/index.php">Response Times: The 3 Important Limits</a></li>
                
                <li><a href="../why-you-only-need-to-test-with-5-users/index.php">Why You Only Need to Test with 5 Users</a></li>
                
                <li><a href="../page-fold-manifesto/index.php">The Fold Manifesto: Why the Page Fold Still Matters</a></li>
                
            </ul>
          </section>
          
          

          
            <section class="hide-for-small-down">
              
            </section>
          

      </section>
      

      
        <div class="small-12 medium-9 columns content-body">
          




<article class="l-subsection alertbox-article-content article-detail-bl">
  
  <header>
    <div class="l-subsection">
      <h1>B2B Product Specifications: Guidelines for Creating Useful Product-Specs Pages</h1>
      <div class="author-meta">by
        
          
            
            <a href="../author/page-laubheimer/index.php">Page Laubheimer</a>
            
          
        on  July 3, 2016
        
      </div>

      

      
      <div class="topic-links">
        Topics:
        
          <ul>

  <li><a href="../../topic/b2b-websites/index.php">B2B Websites</a></li>

</ul>
        
      </div>
      
    </div>
  </header>
  

  
  
  <section>
    <p class="lede"><strong>Summary:</strong> Effective product information on B2B sites satisfies 5 main criteria and can be put together using our spec lists for physical and software products.
</p>
    <br />
  </section>
  
  

  
  <section id="article-body"><h2>Introduction</h2>

<p>Business-to-business websites have to answer a lot of questions from prospective customers and must address a variety of user needs: both those of novice, first-time visitors and those of educated users who know the product space well. The typical B2B buying cycle has several stages, starting with an initial phase where users research their company’s problem to find possible product solutions, and then moving into a phase where they compare product options from multiple vendors and assess which solution fits their needs best. Users at different stages of the conversion funnel need different levels of detail about products and solutions.<br/>
To raise the interest of first-time visitors during the initial research stages, B2B sites need to present a compelling argument for why their products best serve their prospects’ needs and focus on high-level benefits rather than just on features and details. However, content that describes the problems solved by a product (rather than a laundry list of features or marketing slogans) helps draw initial interest, but that content won’t always close the sale.</p>

<p>Once prospects are interested in a product, they will want clear answers to make sure that the product fits their specific requirements, so they can also compare against competitors. Product-specification lists need to go beyond the product benefits and high-level descriptions, and include clear, specific, realistic details that can speak in detailed terms about all of the important aspects of how a product works, its physical characteristics, and how it integrates into larger systems. These details are not only useful for late-stage decision makers; complete specifications also help draw in users in the earlier research stage because they provide specific, valuable content that’s beneficial for SEO. Incomplete, vague, or misleading spec pages frustrate users, forcing them either to call your customer-support line for answers (which is an expensive proposition for your company, and frustrating for your users), or, worse, walk away from your site entirely.</p>

<p>It’s much better to provide the specifications directly on your site, without forcing prospects to email or call (or, as many people do, turn to Google — who knows what they’ll find there). First and foremost, talk to your users and find out which specifications matter to them. Ensure that your content speaks to <a href="../b2b-vs-b2c/index.php">both the decision-makers at your customer’s companies and to the technical users</a> that tend to collaborate on the decision-making process and make recommendations for which product to ultimately purchase.</p>

<h1>Guidelines for Creating Product Specifications</h1>

<p>Think about the following basic attributes or your product, and include these basic details in the format most applicable for your industry and product:</p>

<ol>
	<li><strong>Footprint</strong> (i.e. the physical size for physical products or resources consumed by software products)</li>
	<li><strong>Ingredients </strong>(what key components, parts, technologies, or materials are used)</li>
	<li><strong>Requirements</strong> <strong>and integrations</strong> (environmental conditions, consumable physical resources, system requirements, and integrations with software or other physical products needed to form a complete solution to your prospect’s problems)</li>
	<li><strong>Performance</strong> (measurements of desired capabilities, behaviors, operation, and output, as well as any waste byproducts that are produced in operation)</li>
	<li><strong>Tolerances and durability</strong> (allowed level of deviation from listed specs, error rate, resistance to environmental conditions)</li>
</ol>

<p>Due to the sheer variety of important key details from industry to industry and product type to product type, the checklists below do not contain every possible type of specification of interest for all industries, nor do they apply to every product. They are meant to provide your team with a framework for deciding which information to include on your product pages. Wherever possible, reach out to your customers with surveys, talk to your customer-support and sales teams about which questions they answer the most often, or <a href="../testing-content-websites/index.php">test the content on your website</a>, and find out which specific details users need to know to make a purchase decision. Include those details in your product-specifications pages.</p>

<p>Below we provide starter lists with typical specifications for physical and software products. These lists are aggregated from the commonalities we found in <a href="../../reports/b2b-websites-usability/index.php">testing B2B sites from a broad range of industries</a> with their target business customers. Although these lists do not include every possible individual specification, they offer common inclusions that are worth adding to your spec list. Be sure to review it with your internal subject-matter experts and with representative users to determine whether all of these specs are appropriate, helpful, and exhaustive for your product.</p>

<h3>Physical-Product Specs: A Starter List</h3>

<table border="1" cellpadding="0" cellspacing="0">
	<tbody>
		<tr>
			<td style="width:312px;">
			<p>Type of Specification</p>
			</td>
			<td style="width:312px;">
			<p>Examples and Notes</p>
			</td>
		</tr>
		<tr>
			<td style="width:312px;">
			<p><strong>Detailed physical dimensions of products </strong></p>
			</td>
			<td style="width:312px;">
			<ul>
				<li>Engineering diagrams with measurements</li>
				<li>Physical space required for installation</li>
				<li>Ventilation space needed for safe operation</li>
			</ul>
			</td>
		</tr>
		<tr>
			<td style="width:312px;">
			<p><strong>Weight of item </strong></p>
			</td>
			<td style="width:312px;">
			<ul>
				<li>Variations for both loaded and unloaded weights (such as a vehicle loaded with fuel or payload)</li>
			</ul>
			</td>
		</tr>
		<tr>
			<td style="width:312px;">
			<p><strong><a href="../b2b-vs-b2c/index.php">Physical connections with other products </a></strong></p>

			<p> </p>
			</td>
			<td style="width:312px;">
			<ul>
				<li>Interconnects</li>
				<li>Ports</li>
				<li>Diagram of physical layout of ports and connectors, including dimensions, so users can determine if there is enough physical space for convenient hookup</li>
			</ul>
			</td>
		</tr>
		<tr>
			<td style="width:312px;">
			<p><strong>Key components</strong></p>

			<p> </p>
			</td>
			<td style="width:312px;">
			<ul>
				<li>Types and grades of materials used for critical components, such as:
				<ul style="list-style-type:circle;">
					<li>Metals</li>
					<li>Plastics</li>
					<li>Wiring</li>
					<li>Tubing</li>
					<li>Insulation</li>
				</ul>
				</li>
				<li>Manufacturer and product names of key hardware components produced by third parties, such as:
				<ul style="list-style-type:circle;">
					<li>Engines</li>
					<li>Processors</li>
					<li>Robotics components</li>
					<li>Power supplies</li>
				</ul>
				</li>
			</ul>
			</td>
		</tr>
		<tr>
			<td style="width:312px;">
			<p><strong>Resistance to common environmental conditions for your products</strong></p>
			</td>
			<td style="width:312px;">
			<ul>
				<li>Water and particulate matter (typically listed in<a href="https://webstore.iec.ch/publication/2452"> International Protection codes</a>, such as IP68)</li>
				<li>Temperature</li>
				<li>Scratching</li>
				<li>Physical impact</li>
				<li>UV</li>
				<li>Chemicals and corrosives</li>
				<li>Atmospheric pressure</li>
				<li>Any other typical exposures your product may meet in use</li>
			</ul>
			</td>
		</tr>
		<tr>
			<td style="width:312px;">
			<p><strong>Operational qualities</strong></p>
			</td>
			<td style="width:312px;">
			<ul>
				<li>Operating temperature output (how hot the product gets while in use)</li>
				<li>Sound-pressure levels (how loud the product is while in use)</li>
			</ul>
			</td>
		</tr>
		<tr>
			<td style="width:312px;">
			<p><strong>Resource consumption</strong></p>

			<p> </p>
			</td>
			<td style="width:312px;">
			<ul>
				<li>Power</li>
				<li>Water</li>
				<li>Raw materials</li>
			</ul>
			</td>
		</tr>
		<tr>
			<td style="width:312px;">
			<p><strong>Capacities</strong></p>
			</td>
			<td style="width:312px;">
			<ul>
				<li>Consumable resource storage
				<ul style="list-style-type:circle;">
					<li>Paper trays</li>
					<li>Fuel tanks</li>
				</ul>
				</li>
				<li>Byproduct holding tanks
				<ul style="list-style-type:circle;">
					<li>Sewage</li>
					<li>Runoff</li>
					<li>Dust</li>
					<li>Trash</li>
				</ul>
				</li>
			</ul>
			</td>
		</tr>
		<tr>
			<td style="width:312px;">
			<p><strong>Performance characteristics</strong></p>
			</td>
			<td style="width:312px;">
			<ul>
				<li>Measurement conditions and margins of errors for performance metrics</li>
				<li>Details on durability, longevity, tolerances, and failure rates</li>
				<li>Key metrics, such as:
				<ul style="list-style-type:circle;">
					<li>Speed</li>
					<li>Power</li>
					<li>Displacement</li>
					<li>Tensile strength</li>
					<li>Hardness</li>
					<li>Mechanical, chemical, or electrical properties that impact performance and utility</li>
					<li>Output quality and speed (especially for items like printers, manufacturing equipment, etc.)</li>
					<li>Duty cycle</li>
					<li>Runtime</li>
				</ul>
				</li>
			</ul>
			</td>
		</tr>
		<tr>
			<td style="width:312px;">
			<p><strong>Safety and exposure information</strong></p>
			</td>
			<td style="width:312px;">
			<ul>
				<li>MSDS (material-safety data sheet) information</li>
				<li>UL certification</li>
			</ul>
			</td>
		</tr>
	</tbody>
</table>

<p> </p>

<figure class="caption"><img alt="Damen.com product comparison and specifications table" border="0" height="388" src="https://media.nngroup.com/media/editor/2016/06/24/damen.png" width="721"/>
<figcaption><em>Damen.com allows users to select between metric and U.S. units on its specs and product-comparison tools for dredging equipment. However, in this example, the line-graph “meters” for the two bottom specifications are confusing, because the blue segment is presented as a portion of a larger unspecified gray segment (the gray segment corresponds to top orange option, which is the main product on this page).</em></figcaption>
</figure>

<figure class="caption"><img alt="Acrylink product specs table with standards information and links" border="0" height="933" src="https://media.nngroup.com/media/editor/2016/06/24/acrylink.png" width="720"/>
<figcaption><em>Acrylink.com’s specifications table shows the standards used to measure each spec, along with a link to more information about each standard from the organization that manages that standard. The table also shows ranges of typical performance for some of the data (such as ±0.1% listed on the </em>Solids<em> data). The table has some excellent characteristics: the left-column specification labels are left aligned, bolded, and easy to scan. However, the dark grey background reduces the amount of contrast with the text, making this harder to read, and some numeric specifications don’t include the unit of measure within the cell (such as </em>Hardness<em>, </em>SIR<em>, and </em>Thermal Emittance<em>).</em></figcaption>
</figure>

<p> </p>

<figure class="caption"><img alt="Rethink Robotics specifications tables" border="0" height="1144" src="https://media.nngroup.com/media/editor/2016/06/24/rethink-robotics.png" width="516"/>
<figcaption><em>Rethink Robotics has a useful product-specs list that covers all of the 5 major categories: (1) physical footprint (and software resources), (2) ingredients (such as the third-party processor), (3) requirements (power consumption), (4) performance (efficiency ratings), and (5) tolerances (both the tolerance to power supply fluctuations, and listed ranges for some specs). Also, by breaking the lists of specs into discrete chunks located in different tables with clear header descriptions, this layout helps users jump through a lot of complex information to find the content important to them. The zebra-striping on the table enables users to follow text across table rows efficiently. However, the legibility of this design could be further improved by increasing the contrast between the specification text and the table background. And the right alignment of the first table column makes the specifications less scannable.</em></figcaption>
</figure>

<h3>Software-Product Specs: A Starter List</h3>

<table border="1" cellpadding="0" cellspacing="0">
	<tbody>
		<tr>
			<td style="width:312px;">
			<p><strong>Type of Specification</strong></p>
			</td>
			<td style="width:312px;">
			<p><strong>Examples and Notes</strong></p>
			</td>
		</tr>
		<tr>
			<td style="width:312px;">
			<p><strong><a href="../b2b-vs-b2c/index.php">Integration and compatibility with other systems</a></strong></p>

			<p> </p>
			</td>
			<td style="width:312px;">
			<ul>
				<li>Detailed information on APIs and other forms of intersystem communication and data exchange</li>
			</ul>
			</td>
		</tr>
		<tr>
			<td style="width:312px;">
			<p><strong>Feature parity </strong></p>

			<p> </p>
			</td>
			<td style="width:312px;">
			<ul>
				<li>Feature inclusions between versions or product tiers</li>
				<li>Plug-in, application, and API-access differences between versions or product tiers</li>
			</ul>
			</td>
		</tr>
		<tr>
			<td style="width:312px;">
			<p><strong>System requirements</strong></p>
			</td>
			<td style="width:312px;">
			<ul>
				<li>Operating system</li>
				<li>Memory requirements (and typical usage)</li>
				<li>Storage space needed (and typical usage)</li>
				<li>Bandwidth available (and typical usage)</li>
				<li>Web or application-server software</li>
				<li>Database type, version, and any configuration requirements</li>
				<li>File servers</li>
				<li>Programming languages installed (such as PHP, Python, or Java)</li>
				<li>Libraries, frameworks, and components (Rails, Node.js, etc.)</li>
				<li>Package managers (such as Bower, npm, etc.)</li>
				<li>Any other middleware required</li>
			</ul>

			<p> </p>
			</td>
		</tr>
	</tbody>
</table>

<p> </p>

<p> </p>

<figure class="caption"><img alt="IBM Software product specification page" border="0" height="690" src="https://media.nngroup.com/media/editor/2016/06/24/ibm-b2b.png" width="720"/>
<figcaption><em>IBM.com’s specifications page for the Sterling B2B Integrator software features full details on all the requirements needed to run this product, including application-server software, package managers, programming languages needed, hardware, and so forth. It helpfully chunks this content into sections, and thus supports spec review in the product-comparison phase of the purchase process. It also offers print-friendly and PDF options, which are sometimes needed for collaborative decision making. The design of this page does, however, suffer from several egregious flaws: (a) the anchor links (for </em>Application Servers<em>, </em>Installation, Java,<em> etc.) below each table add visual noise and potentially confuse users; (b) the mysterious unlabeled icons in the Components columns only present detailed information about component support upon hover; (c) The </em>Filter<em> boxes at the top left corner of each table require users to type a filter value instead of allowing them to select one; (d) the yellow background color for some table columns makes them stand out, but it’s not clear why and in what way they are different from other columns.</em></figcaption>
</figure>

<p> </p>

<h2>Format Guidelines</h2>

<ul>
	<li>Ensure that all specifications are available on the website in HTML format, not just in PDF documents such as data sheets. <a href="../avoid-pdf-for-on-screen-reading/index.php">PDF is a poor format for viewing</a> online since it is built for printing and <a href="../direct-vs-sequential-access/index.php">sequential access</a>. However, in many technical industries, the PDF version is also expected to be available, so be sure to offer the <strong>same</strong> information in both formats.</li>
	<li>Use industry-standard units and realistic measurement conditions.
	<ol style="list-style-type:lower-alpha;">
		<li>Offer the ability to toggle between <a href="../international-sites-requirements/index.php">metric and U.S. units</a>, if appropriate.</li>
		<li>For any specs that use a <a href="../b2b-vs-b2c/index.php">standardized measurement</a> (such as an ISO specification, ANSI standard, etc.), provide a link to the body that manages the standard, to show what it measures and the testing protocol.</li>
		<li>If you have an independent agency verifying compliance with standards, include the name of the agency and a link to its site.</li>
	</ol>
	</li>
</ul>

<ul>
	<li>For long, complex specification lists, break the content into meaningful chunks to help users jump to the appropriate section quickly, without having to exhaustively review every row in a long table. Consider separating the content into several tables, with clear subheader text indicating the category of information found in each table.</li>
	<li>Support horizontal scanning in long tables by using borders or zebra striping for alternate rows. These features are especially important with numeric data.</li>
	<li>Support vertical scanning of terms in the leftmost column by:
	<ol style="list-style-type:lower-alpha;">
		<li>Left aligning text labels</li>
		<li><a href="../first-2-words-a-signal-for-scanning/index.php">Frontloading text labels</a> with the strongest information-carrying terms</li>
	</ol>
	</li>
	<li>Increase legibility by using text colors and styles that<a href="../low-contrast/index.php"> provide sufficient contrast</a> against the table background color.</li>
</ul>

<h2>Conclusion</h2>

<p>Since each industry and product type varies so drastically, creating an effective spec list for complex B2B products can be a challenge. Focus on providing specs in five key areas: footprint (either physical size, or virtual resources), ingredients (such as materials and key components), requirements (resources needed or other required products), performance (both in desired output performance such as speed or capacity, and in byproduct output, such as waste, heat, or temperature), and tolerances (common error and failure rates, environmental sensitivities, and margin of deviation from listed specs).</p>

<p>Learn more about product specifications in the latest edition of our <a href="../../reports/b2b-websites-usability/index.php">B2B Usability Report</a>, which includes 188 guidelines and 301 screenshot examples.</p>
</section>
  

  
  <section>
    
        
    
  </section>
  
  
  
  
  

  


  <section>
    <br />
    <p class="share"><b>Share <span class="hide-for-small-down">this article</span>:</b> <a href="https://twitter.com/intent/tweet?url=http://www.nngroup.com/articles/b2b-specs/&amp;text=B2B%20Product%20Specifications:%20Guidelines%20for%20Creating%20Useful%20Product-Specs%20Pages&amp;via=nngroup">Twitter</a> | <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=http://www.nngroup.com/articles/b2b-specs/&amp;title=B2B%20Product%20Specifications:%20Guidelines%20for%20Creating%20Useful%20Product-Specs%20Pages&amp;source=Nielsen%20Norman%20Group">LinkedIn</a> | <a href="https://plus.google.com/share?url=http://www.nngroup.com/articles/b2b-specs/">Google+</a> | <a href="mailto:?subject=NN/g Article: B2B Product Specifications: Guidelines for Creating Useful Product-Specs Pages&amp;body=http://www.nngroup.com/articles/b2b-specs/">Email</a></p>
    <hr />
  </section>

  <footer class="l-subsection">
    
    
    <div class="learn-more">
      <h2>Learn More</h2>
      <p><a href="../subscribe/index.php">Subscribe to the weekly newsletter</a> to get notified about future articles.</p>
      </div>
    
    

    <div class="row show-for-small-down related-content">
      
      
      

      
      
        <div class="column small-12 topic-links">All articles about:
          <ul>
            
            <li><a href="../../topic/b2b-websites/index.php">B2B Websites</a></li>
            
          </ul>
        </div>
      
      

      
      
      

      
      
        <div class="small-12 column">
          <h3>Research Reports</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../reports/b2b-websites-usability/index.php">B2B Website Usability for Converting Users into Leads and Customers</a></li>
              
            
          </ul>
        </div>
      
      

      
      
      
    </div>

    <div class="hide-for-small-down row related-content">
      <div class="medium-6 columns">
        
        
          <div>
            <h3>Articles</h3>
            <ul class="no-bullet">
              
                
                <li><a href="../international-b2b/index.php">International B2B Audiences: Top 5 Ways to Improve Your Site for Global Users</a></li>
                
              
                
                <li><a href="../b2b-vs-b2c/index.php">B2B vs. B2C Websites: Key UX Differences </a></li>
                
              
                
                <li><a href="../content-behind-forms/index.php">When to Hide Content Behind Forms and When to Give Content Away </a></li>
                
              
                
                <li><a href="../specialized-words-specialized-audience/index.php">Use Specialized Language for Specialized Audiences</a></li>
                
              
                
                <li><a href="../format-based-navigation/index.php">Avoid Format-Based Primary Navigation</a></li>
                
              
            </ul>
          </div>
        
        

        
          
          <div>
            <h3>Research Reports</h3>
            <ul class="no-bullet">
              
                
                  <li><a href="../../reports/b2b-websites-usability/index.php">B2B Website Usability for Converting Users into Leads and Customers</a></li>
                
              
            </ul>
          </div>
          
        

      </div>
      <div class="medium-6 columns">
        
        
      </div>
    </div>
  </footer>
</article>

        </div>

      

    
  </div>
  <div class="row">
    
      <div class="column small-12 show-for-small-down l-navrail announcements">
        
          
        
      </div>
    
    
  </div>

    </div>
    




<footer id="page-footer">
    <div class="row hide-for-print">
        <div class="column small-12 medium-6">
            <section class="footer-popular">
                
                <h1>Popular Topics</h1>
                <div style="float: left;  width: 15em; display: block;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/applications/index.php">Application Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/e-commerce/index.php">E-Commerce Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/eyetracking/index.php">Eyetracking </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/information-architecture/index.php">Information Architecture </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/intranets/index.php">Intranet Design </a></li>
	<li><a href="../../topic/interaction-design/index.php">Interaction Design </a></li>
</ul>
</div>

<div style="float: left;  width: 15em; display: block; list-style: none;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/mobile-and-tablet-design/index.php">Mobile and Tablet Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/credibility-persuasion/index.php">Persuasive Design</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/research-methods/index.php">Research Methods</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/user-testing/index.php">User Testing </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/web-usability/index.php">Web Usability </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/writing-web/index.php">Web Writing &amp; Content Strategy </a></li>
</ul>
</div>

<p>&nbsp;</p>

                
            </section>
        </div>

        <div class="column small-12 medium-6">
            <div class="row">
                <div class="column small-6">
                    <section class="footer-about">
                        <h1>Follow Us</h1>
                        <ul>
	<li><a href="../subscribe/index.php">Subscribe to our weekly UX newsletter</a>,&nbsp;Jakob Nielsen&#39;s Alertbox</li>
	<li>Visit Don Norman&#39;s website, <a href="http://www.jnd.org/"> www.jnd.org </a></li>
	<li>Visit Bruce Tognazzini&#39;s website, <a href="http://www.asktog.com/"> www.asktog.com </a></li>
	<li style="padding-top: 6px;"><a class="twitter-follow-button" data-show-count="false" data-size="large" href="https://twitter.com/nngroup"><img alt="Follow @nngroup on Twitter" height="20" src="https://media.nngroup.com/media/editor/2016/12/30/twitter-follow-button.png" style="width: 117px; height: 20px;" width="117" /></a></li>
	<li style="padding-top: 6px;"><img alt="" height="14" src="https://media.nngroup.com/media/editor/2016/07/26/feed-icon-14x14.png" style="vertical-align:text-top;width:14px;margin-right:.25em;" width="14" /><a href="../../feed/rss/index.php">Subscribe to our RSS feed</a></li>
</ul>

                    </section>
                </div>

                <div class="column small-6">
                    <section class="footer-about right-column">
                        <h1>About
</h1>
                        <ul class="no-bullet">
	<li><a href="../../about/contact/index.php">Contact information</a></li>
	<li><a href="../../about/index.php">About Us</a></li>
	<li><a href="../../about/why-nng/index.php">Why NN/g? </a></li>
	<li><a href="../../return-policy/index.php">Return Policy</a></li>
</ul>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="copyright column small-12">
            <p><a href="../../copyright-and-reprint-info/index.php">Copyright</a> &copy; 1998-2017 Nielsen Norman Group, All Rights Reserved.</p>
        </div>
    </div>
</footer>


    


        <script src="https://media.nngroup.com/static/vendor/fastclick/js/fastclick.min.js"></script>
        <script src="https://media.nngroup.com/static/vendor/foundation/js/foundation.min.js"></script>
        <script src="http://media.nngroup.com/static/js/app.js"></script>
        <script src="http://media.nngroup.com/static/js/scroll-to-fixed.js"></script><script>var externalLinks = document.querySelectorAll('a[href^="http"]');for (var i = externalLinks.length-1; i >= 0; i--) {externalLinks[i].addEventListener("click", function(event) { alert("External links are disabled for this test."); event.preventDefault(); }, false);}var mailLinks = document.querySelectorAll('a[href^="mailto"]');for (var i = mailLinks.length-1; i >= 0; i--) {mailLinks[i].addEventListener("click", function(event) { alert("Email links are disabled for this test."); event.preventDefault(); }, false);}var externalForms = document.querySelectorAll('form');for (var i = externalForms.length-1; i >= 0; i--) {externalForms[i].addEventListener("submit", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false); externalForms[i].addEventListener("click", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false);}</script>

        <script>
            
            $(function() {
                $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
                $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
            });
        </script>

        
<script src="https://media.nngroup.com/static/js/articles.js"></script>


        
        <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1072699960;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072699960/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
    </body>

<!-- Mirrored from www.nngroup.com/articles/b2b-specs/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:13:04 GMT -->
</html>
