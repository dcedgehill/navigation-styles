<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->





    
<!-- Mirrored from www.nngroup.com/articles/trip-report-chi-90/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:14:36 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1026.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"licenseKey":"1a30ef9931","queueTime":2,"beacon":"bam.nr-data.net","errorBeacon":"bam.nr-data.net","applicationID":"2048992","transactionName":"NFxSMRBUX0EDUkIPDA0WdhAMVkVbDV8ZBxEXUFMJB0YfRAtUQRVZIktEDAFZVHYHRVcPD01eVRE=","applicationTime":760,"agent":""}</script>
        <title>CHI&#39;90 Trip Report: Article by Jakob Nielsen</title><meta property="og:title" content="CHI&#39;90 Trip Report: Article by Jakob Nielsen" />
  
        
        <meta name="description" content="Jakob Nielsen&#39;s trip report from the  1990 Computer-Human Interaction (CHI &#39;90) conference.">
        <meta property="og:description" content="Jakob Nielsen&#39;s trip report from the  1990 Computer-Human Interaction (CHI &#39;90) conference." />
        
  
        
	
        

        

        <meta name="robots" content="noarchive">
        <meta name="viewport" content="width=device-width">

        <link rel="search" type="application/opensearchdescription+xml" title="NN/g Search" href="../../opensearch.xml" />
        <link rel="shortcut icon" href="http://media.nngroup.com/static/img/favicon.ico" />

        <link rel="stylesheet" href="https://media.nngroup.com/static/vendor/font-awesome/css/font-awesome.min.css">

        
        <link rel="stylesheet" href="https://media.nngroup.com/static/css/7232a960f1df.css" type="text/css" />
        <link rel="stylesheet" href="http://fast.fonts.com/t/1.css?apiType=css&amp;projectid=c5846260-9def-46ec-9407-0f1349bd2d64" type="text/css">

        <script src="https://media.nngroup.com/static/vendor/modernizr/js/modernizr.min.js"></script>

        <script type="text/javascript" src="https://media.nngroup.com/static/js/3283be5def74.js"></script>
        

        

        
        
        <!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=229034,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('http://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->


<!-- Google Analytics Tracking Code for http://nngroup.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23132681-1', 'nngroup.com');
  ga('require', 'linkid', 'https://www.nngroup.com/articles/trip-report-chi-90/linkid.js');
  ga('require', 'displayfeatures');

// Optimizely Universal Analytics Integration
 window.optimizely = window.optimizely || [];
 window.optimizely.push("activateUniversalAnalytics");

  ga('send', 'pageview');

</script>
<script>
/**
* Function to track a click on an outbound link in Google Analytics.
*/
function trackOutboundLink(label, url) {
    if(window.ga && ga.loaded) {
        ga('send', 'event', 'outbound', 'click', label, {'hitCallback': function(){document.location = url;}
        });
    } else {
        document.location = url;
    }
}
/**
* Function to track a click on a link in the article announcement area.
*/
$('#articleAnnouncement a').on('click', function(){
  ga('send', 'event', 'link', 'click', 'articleAnnouncement');
});
</script>

<script type="text/javascript">
 setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2421.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



<!-- Hotjar Tracking Code for http://nngroup.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:121605,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

        <script src="https://media.nngroup.com/static/js/analytics/track_outbound_link.js"></script>
        
        
        
    </head>
    <body class="location-articles articles-detail">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
    <header id="page-header">
        <div id="cart" class="shopping-cart mobile-cart show-for-small-down"></div>
        <div class="row">
            <div id="masthead" class="masthead small-12 medium-7 column">

                <!-- The button to toggles menu on small screen -->

                <h1><a href="../../index.php"><img src="http://media.nngroup.com/static/img/logo-wordmark.png" width="74" height="30"> Nielsen <span class="norman">Norman</span> Group</a></h1>
                <p class="tagline show-for-medium-up">Evidence-Based User Experience Research, Training, and Consulting</p>

                <!-- The button to toggles form search on small screen -->


                <div style="clear: right;width:  100%;height: 1px;"></div><?php define('ROOT_PATH',$_SERVER["DOCUMENT_ROOT"] . '/apps/navigation_styles/navigations/4'); ?><?php include(ROOT_PATH)."/navigation.php"; ?>

                <!-- Drop down menu on small screen -->
                <div id="hideNav" class="show-for-small-down">
                  
                  <ul class="menu-content">
                    <li><a href="../../index.php">Home</a></li>
                    <li class="has-submenu">
                      <a>Training</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../training/index.php">Overview</a></li>
                        
<li id="nav-ux-conference"><a href="../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../online-seminars/index.php"><span>Online Seminars</span></a></li>

                      </ul>
                    </li>
                    <li class="has-submenu">
                      <a>Consulting</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
  <li><a href="../../consulting/index.php">Overview</a></li>
  
  <li><a href="../../consulting/benchmarking/index.php">Benchmarking</a></li>
  
  <li><a href="../../consulting/expert-design-review/index.php">Expert Design Review</a></li>
  
  <li><a href="../../consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
  
  <li><a href="../../consulting/usability-testing/index.php">Usability Testing </a></li>
  
  <li><a href="../../consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
  
  <li><a href="../../in-house-training/index.php">In-House Training</a></li>
</ul>

                    </li>

                    <li><a href="../../reports/index.php">Reports</a></li>
                    <li><a href="../index.php">Articles</a></li>
                    <li class="has-submenu"><a>About NN/G</a><i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../about/index.php">Overview</a></li>
                        <li><a href="../../people/index.php">People</a></li>
                        <li><a href="../../about/why-nng/index.php">Why NN/g?</a></li>
                        <li><a href="../../about/contact/index.php">Contact</a></li>
                        <li><a href="../../news/index.php">News</a></li>
                        <li><a href="../../about/history/index.php">History</a></li>
                        <li><a href="../../books/index.php">Books</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                </div>

                <hr class="header-divider show-for-small-only"/>


            </div>
       </div>

        <div id="hideNav">            <nav class="primary-nav small-12 column">
                <ul>
                    <li><a class="navitem" id="nav-home" href="../../index.php">Home</a></li>
                    <li><a class="navitem" id="nav-training" href="../../training/index.php">Training</a></li>
                    <li><a class="navitem" id="nav-consulting" href="../../consulting/index.php">Consulting</a></li>
                    <li><a class="navitem" id="nav-reports" href="../../reports/index.php">Reports</a></li>
                    <li><a class="navitem" id="nav-articles" href="../index.php">Articles</a></li>
                    <li><a class="navitem" id="nav-about" href="../../about/index.php">About NN/G</a></li>
                </ul>
            </nav>
            <nav class="secondary-nav small-12 medium-12 large-12 columns">
                
                
            </nav>
        </div>
    </header>
    <div class="l-content">
    
  

  

  <div class="row">
    
      
      <section class="small-12 medium-3 columns collapsable-content l-navrail articles-left-column">
        <h1 class="show-for-small-down collapse-link mobile-browse-header">Browse by Topic and Author</h1>
          <div class="row collapsable">

            
            <div class="small-6 medium-12 columns">
              <section id="article-popular-topics" style="display: none;">
                
                <h1>Topics</h1>
                <ul id="article-popular-topics">
                    
                    <li>
                        
                        <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/intranets/all/index.php">Intranets</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/user-testing/all/index.php">User Testing</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                        
                    </li>
                    
                    <li><a href="#" id="article-see-all-topics"><strong>See all topics&hellip;</strong></a></li>
                </ul>
                
              </section>
              <section id="article-all-topics" style="display: none;">
                
                <h1>
                  <span class="show-for-medium-up">All Article Topics</span>
                  <span class="show-for-small-down">All Topics</span>
                  (<a href="#" id="article-hide-all-topics">hide</a>)
                </h1>
                <ul>
                  
                  <li>
                    
                    <a href="../../topic/accessibility/all/index.php">Accessibility</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/agile/all/index.php">Agile</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/analytics-and-metrics/all/index.php">Analytics &amp; Metrics</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/applications/all/index.php">Application Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/b2b-websites/all/index.php">B2B Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/behavior-patterns/all/index.php">Behavior Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/branding/all/index.php">Branding</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/content-strategy/all/index.php">Content Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/corporate-websites/all/index.php">Corporate Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/design-patterns/all/index.php">Design Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-design-process/all/index.php">Design Process</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/email/all/index.php">Email</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/eyetracking/all/index.php">Eyetracking</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/heuristic-evaluation/all/index.php">Heuristic Evaluation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/human-computer-interaction/all/index.php">Human Computer Interaction</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-ideation/all/index.php">Ideation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/information-architecture/all/index.php">Information Architecture</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/interaction-design/all/index.php">Interaction Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/international-users/all/index.php">International Users</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/intranets/all/index.php">Intranets</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-management/all/index.php">Management</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/navigation/all/index.php">Navigation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/non-profit-websites/all/index.php">Non-Profit Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/personas/all/index.php">Personas</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/persuasive-design/all/index.php">Persuasive Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/prototyping/all/index.php">Prototyping</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/psychology-and-ux/all/index.php">Psychology and UX</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/research-methods/all/index.php">Research Methods</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/search/all/index.php">Search</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/social-media/all/index.php">Social Media</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/strategy/all/index.php">Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/user-testing/all/index.php">User Testing</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-teams/all/index.php">UX Teams</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/visual-design/all/index.php">Visual Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/writing-web/all/index.php">Writing for the Web</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/young-users/all/index.php">Young Users</a>
                    
                  </li>
                  
                </ul>
                
              </section>
            </div>
            <script>
              /* Determine which topics list will be displayed: the full or the
               * partial.  We are inlining this close to the list itself to
               * prevent rendering delays on the client
               */
              var topic_list = $.cookie("article-topics");

              var popular_topics = $('#article-popular-topics');
              var all_topics = $('#article-all-topics');

              if(topic_list == 1) {
                  popular_topics.hide();
                  all_topics.show();
              } else {
                  popular_topics.show();
                  all_topics.hide();
              }
            </script>
            

            
            <div class="small-6 medium-12 columns">
              
              <section id="article-initial-authors">
                  <h1>Author</h1>
                  <ul>
                    
                    <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                    
                    <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                    
                    <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                    
                    <li><a href="#" onClick="$('#article-initial-authors').hide(); $('#article-all-authors').show();"><strong>See all authors&hellip;</strong></a></li>
                  </ul>
              </section>
              <section id="article-all-authors" style="display: none;">
                <h1>
                  All Authors
                  (<a href="#" id="article-hide-all-authors">hide</a>)
                </h1>
                <ul>
                  
                  <li><a href="../author/raluca-budiu/index.php">Raluca Budiu</a></li>
                  
                  <li><a href="../author/patty-caya/index.php">Patty Caya</a></li>
                  
                  <li><a href="../author/susan-farrell/index.php">Susan Farrell</a></li>
                  
                  <li><a href="../author/therese-fessenden/index.php">Therese Fessenden</a></li>
                  
                  <li><a href="../author/kim-flaherty/index.php">Kim Flaherty</a></li>
                  
                  <li><a href="../author/sarah-gibbons/index.php">Sarah Gibbons</a></li>
                  
                  <li><a href="../author/aurora-harley/index.php">Aurora Harley</a></li>
                  
                  <li><a href="../author/kate-kaplan/index.php">Kate Kaplan</a></li>
                  
                  <li><a href="../author/page-laubheimer/index.php">Page Laubheimer</a></li>
                  
                  <li><a href="../author/angela-li/index.php">Angie Li</a></li>
                  
                  <li><a href="../author/hoa-loranger/index.php">Hoa Loranger</a></li>
                  
                  <li><a href="../author/kate-meyer/index.php">Kate Meyer</a></li>
                  
                  <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                  
                  <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                  
                  <li><a href="../author/kara-pernice/index.php">Kara Pernice</a></li>
                  
                  <li><a href="../author/christian-rohrer/index.php">Christian Rohrer</a></li>
                  
                  <li><a href="../author/amy-schade/index.php">Amy Schade</a></li>
                  
                  <li><a href="../author/katie-sherwin/index.php">Katie Sherwin</a></li>
                  
                  <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                  
                  <li><a href="../author/kathryn-whitenton/index.php">Kathryn Whitenton</a></li>
                  
                </ul>
              </section>
              
            </div>
            
          </div>

          
          <section class="hide-for-small-down">
            
              
              <h1>Recent Articles</h1>
              
              <ul>
                  
                  <li><a href="../seamless-cross-channel/index.php">Seamlessness in the Omnichannel User Experience</a></li>
                  
                  <li><a href="../brand-intention-interpretation/index.php">Experience Design: Bridging Brand Intention and Brand Interpretation</a></li>
                  
                  <li><a href="../eyetracking-tasks-efficient-scanning/index.php">Scanning Patterns on the Web Are Optimized for the Current Task</a></li>
                  
                  <li><a href="../flat-design-best-practices/index.php">Flat-Design Best Practices</a></li>
                  
                  <li><a href="../ux-user-stories/index.php">Accounting for UX Work with User Stories in Agile Projects</a></li>
                  
                  <li><a href="../index.php"><strong>See all articles&hellip;</strong></a></li>
              </ul>
              
            
          </section>
          

          
          
          <section class="hide-for-small-down">
            <h1>Popular Articles</h1>
            
            <ul>
                
                <li><a href="../usability-101-introduction-to-usability/index.php">Usability 101: Introduction to Usability</a></li>
                
                <li><a href="../top-10-mistakes-web-design/index.php">Top 10 Mistakes in Web Design</a></li>
                
                <li><a href="../how-users-read-on-the-web/index.php">How Users Read on the Web</a></li>
                
                <li><a href="../f-shaped-pattern-reading-web-content/index.php">F-Shaped Pattern For Reading Web Content</a></li>
                
                <li><a href="../ten-usability-heuristics/index.php">10 Usability Heuristics for User Interface Design</a></li>
                
                <li><a href="../intranet-design-2016/index.php">10 Best Intranets of 2016</a></li>
                
                <li><a href="../which-ux-research-methods/index.php">When to Use Which User-Experience Research Methods</a></li>
                
                <li><a href="../response-times-3-important-limits/index.php">Response Times: The 3 Important Limits</a></li>
                
                <li><a href="../why-you-only-need-to-test-with-5-users/index.php">Why You Only Need to Test with 5 Users</a></li>
                
                <li><a href="../page-fold-manifesto/index.php">The Fold Manifesto: Why the Page Fold Still Matters</a></li>
                
            </ul>
          </section>
          
          

          
            <section class="hide-for-small-down">
              
            </section>
          

      </section>
      

      
        <div class="small-12 medium-9 columns content-body">
          




<article class="l-subsection alertbox-article-content article-detail-bl">
  
  <header>
    <div class="l-subsection">
      <h1>CHI&#39;90 Trip Report</h1>
      <div class="author-meta">by
        
          
            
            <a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a>
            
          
        on  June 1, 1990
        
      </div>

      

      
      <div class="topic-links">
        Topics:
        
          <ul>

  <li><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction</a></li>

</ul>
        
      </div>
      
    </div>
  </header>
  

  
  
  

  
  <section id="article-body"><p><em>The conference proceedings can be <a href="http://www.amazon.com/exec/obidos/ISBN=0897913450/useitcomusableinA/"> bought online</a> Seattle, WA, 1-5 April 1990. </em><br/>
 </p>

<h2>HCI as a Profession</h2>

<p>CHI'90 was the largest CHI-conference yet with 2,300 participants: up 39% from 1,650 the year before. As a matter of fact, the growth of <strong> human-computer interaction as a profession </strong> was an important theme in itself this year. SIGCHI has been the fastest growing ACM special interest group for several years and continues to retain this distinction.</p>

<p>Traditionally, HCI specialists have graduated with "regular" computer science or psychology degrees and have acquired the necessary HCI-specific skills gradually on the job. Now, several universities have established special HCI programs aiming at our new profession. For example, I visited the University of Toronto after CHI and found that the computer science department had just had a B.S. <strong> HCI degree </strong> approved. As another example, the Scottish HCI Centre at Heriott-Watt University in Edinburgh initiates a M.S. program in HCI on 1st October 1990. Also, Michael Dertouzos from MIT took advantage of his keynote talk (see below) to announce that MIT has endowed a user interface professorship with two million dollars from the X Windows consortium. This development of HCI from a job performed by graduates of other fields to an independent profession is historically very similar to the development of computer science as a discipline. Computer science also started as an offshot of mathematics, electrical engineering, physics, and similar disciplines.</p>

<p><strong>Terry Winograd </strong> from Stanford University gave an invited talk discussing how to teach human-computer interaction. This should not just include the user interface itself but also the work structure and how it is changed by the use of computers. Winograd saw HCI as a <strong> design discipline </strong> where the focus is on what we can build. This is in contrast to traditional engineering where one more or less knows what one wants to build (say, a bridge), and the focus is on how to build it using underlying systematic principles. Therefore, we should teach HCI in similar ways to those used in other design disciplines. Winograd's new course is based on having small groups of students actually design prototype interfaces that serve the same purpose as studio models serve for architects.</p>

<p>As another indication of the growth of HCI in relation to its constituent disciplines of computer science, psychology, and human factors was that the Ergonomics Society has decided to allow their members to choose between the traditional Ergonomics journal and the HCI-oriented Behaviour and Information Technology journal as their membership journal. This was announced at an interesting panel on HCI journals chaired by Jack Carroll.; from the IBM Watson Center. The panel showed that we now have five HCI journals (Behaviour and Information Technology, the Human-Computer Interaction journal, Interacting with Computers (the British Computer Society's journal), the International Journal of Human-Computer Interaction, and the International Journal of Man-Machine Studies) plus even a secondary publication with abstracts from the other journals. The older journals typically have slightly more than a thousand subscribers whereas the newer journals only have a few hundred at the moment. Because of these low subscription figures, it seems that the CHI conference proceedings (distributed to about 6,000 people) have taken over the role of archival publication normally served by journals in other professions. As an example, I have started seeing newsnet messages of the type "please give me some good references about such-and-such HCI topic" where the posters state that they have already checked the collected CHI proceedings themselves.</p>

<p>As a further example of the change of publishing focus from the original disciplines to the new one, the softcover edition of Don Norman's entertaining book <cite> The <strong> Psychology </strong> of Everyday Things </cite> has been retitled <cite> <a href="http://www.amazon.com/exec/obidos/ISBN=0385267746/useitcomusableinA/"> The <strong> Design </strong> of Everyday Things</a></cite>, even though the new acronym is less catchy than POET. The rise of user interface design as a new discipline is seen by some as reducing the "purity" of the work presented at the CHI conference, and some grumbling could be heard from traditionalist psychology and computer science proponents.</p>

<p>My final example of the view of HCI as a separate profession comes from the panel on <strong> technology transfer </strong> chaired by Keith Butler from Boeing. <strong> Chuck Price </strong> from the Boeing Computer Services described his need as a line manager to know what skills to look for when hiring HCI staff and complained that these skills had not been defined well enough. He came to the CHI conference to get some skills and methods to use in his project, and he emphasized that his focus was on the development process and <strong> people development </strong> rather than on necessarily getting the latest widget. He felt the need to have people trained in the multiple necessary HCI skills as a single discipline rather than having to put together teams of many diverse specialists.</p>

<p>The panel also discussed the bottom line impact of using usability engineering. <strong> John Thomas </strong> from NYNEX mentioned one case where they shaved a few seconds off the interaction time for a certain transaction. Because of the huge number of users of their product, this productivity improvement can be shown to translate into hundreds of millions of dollars, but unfortunately that money is never collected in one place and seen by management. So it is hard to really prove that the use of our methods is actually worth something to the company. Also, their users (telephone operators and residential customers) are very different from the college sophomores used in most usability research, so he was not certain that the external validity was good enough to transfer the state of the art in user interface design with no changes from the academic environment to NYNEX.</p>

<p>The other side of technology transfer is to transfer practical considerations and experiences to the academic research environment. Doing so can be just as hard as the transfer of research results to development projects. For example, <strong> David Kieras </strong> from the University of Michigan was "aching" for access to a real design process under appropriate conditions. Mostly, development projects are so focused on shipping their product that they have no time to collaborate with researchers wanting to study them. John Bennett.; from IBM pointed out that very difference in <strong> time scales </strong> between the two environments is a major inhibitor for collaboration. The academic focus is on writing papers that may be published years later and the time scale is often that of a Ph.D. thesis project. Even so, the panel did provide hope for technology transfer enthusiasts: Kieras emphasized the thrill he gets every time somebody calls him up and wants to use some elements in one of his papers. And Price's company was willing to commit some resources to long-range projects with some risk.</p>

<h2>Non-Command-Based Interaction Paradigms</h2>

<p>In spite of Chuck Price's lack of interest in the latest widgets, it still seems to me that this year's <strong> conference theme </strong> was symbolized by a collection of somewhat peculiar widgets. For the first time in several years, the CHI conference was permeated with breakthroughs in the underlying technology, and I felt that the main conference theme was the emergence of the next-generation interaction paradigm. And this was about time too, given that the last several conferences had been mostly dominated by current-generation WIMP interfaces. (WIMP = Windows, Icons, Menus, and a Pointing device. The acronym was probably invented by a real hacker who does not eat quiche.)</p>

<p>Anyway, the fifth generation user interface paradigm seems to be centered around <strong> <a href="../noncommand/index.php"> non-command based dialogues</a></strong>. This term is a somewhat negative way of characterizing a new forms of interaction but so far, the unifying concept does seem to be exactly the abandonment of the principle underlying all earlier interaction paradigms: That a dialogue has to be controlled by specific and precise commands issued by the user and processed and replied to by the computer. The new interfaces are often not even dialogues in the traditional meaning of the word, even though they obviously can be analyzed as having some dialogue content at some level since they do involve the exchange of information between a user and a computer.</p>

<p>The principles shown at CHI'90 which I am summarizing as being based on non-command-based interaction paradigms are eye tracking interfaces, artificial realities, play-along music accompaniment, and agents.</p>

<h3>Eye Tracking</h3>

<p>Eye tracking has long been an esoteric and very expensive technique but this year, a quite practical system was part of the conference demos. I tried it for regular eye tracking applications such as "typing" by looking at a picture of a keyboard, but I have to admit that I did not find it very pleasant to type in this way (even though the technique is of course great for many handicapped users). A more convincing demonstration of eye tracking was a paddle-ball video game where I controlled the paddle by eye tracking. Therefore, I could just look at the screen where the ball was going to hit, and presto!-the paddle was right under the ball. I am normally not all that good at video games but I kept this paddleball game going for a long time. I would call this a non-command-based interface because I was not consciously controlling the paddle; I was looking at the ball and the paddle automatically did what I wanted it to do. In contrast, even a direct manipulation video game would involve some kind of command to move the paddle as such (for example by moving the mouse). The difference is one of the level of the dialogue: In the eye tracking paddleball you look at the ball and the paddle keeps up by itself, whereas you have to tell the computer to move the paddle left and right in the direct manipulation paddleball game. Therefore, your focus of attention remains on a higher level in the eye tracking version of the game.</p>

<p>Possibly the most interesting paper presentation I attended at CHI'90 was given by <strong> Robert Jacob </strong> from the Naval Research Laboratory. He was developing special interaction techniques for the new input medium provided by eye tracking but mostly did not want to use eye tracking as the only or main input device. Users do not have full control over their eye movements and the eyes "run all the time"-even when you do not intend to have the computer do anything. Since it is impossible to distinguish times when users mean something by a look from times where they are just looking around or are resting their gaze, Jacob needed to develop suitable interaction techniques for special cases only. For example, it proved possible to move an icon on the screen by selecting it by looking at it and pressing a selection button (to prevent accidental selection) and then looking where it had to go.</p>

<p>One interesting application was a naval display of ships on a map. The screen also contained a window with more detailed information about the ships, and whenever the user looked from the map window to the information window, the information window contained information about the last ship the user had looked at on the map. This interaction technique is appropriate because no harm is done by updating the information window as the user looks around on the map. Therefore, it does not matter whether a look at a ship is intentional or not. The non-command-based nature of this interface comes from the usage situation: The user goes back and forth between looking at the overview map and the detailed information, and always finds the relevant information without ever having to issue any explicit selection or retrieval commands. Of course, looking at a ship on the map does constitute a command, but it does not feel like a command, since the action can be performed many times without any apparent result. It is only when the user looks at the information window that the result of the information retrieval is made salient to the user.</p>

<p>Another non-command-based eye tracking system was presented by <strong> Richard Bolt</strong> from the MIT Media Lab and was based on the <strong> patterns of the user's eye movements </strong> instead of the individual fixations. The application was a children's story based on the book <cite> The Little Prince</cite>. The computer screen shows a 3-D graphic model of the miniature planet where the Little Prince lives, and synthesized speech gives a continuous narration about the planet. As long as the user's pattern of eye movements indicates that the user is glancing about the screen in general, the story will be about the planet as a whole, but if the user starts to pay special attention to certain features on the planet, the story will go into more detail about those features. For example, if the user gazes back and forth between several staircases, the system will infer that the user is interested in staircases as a group and will talk about staircases. And if the user mostly looks at a particular staircase, the system will provide a story about that one staircase.</p>

<p>Using this simple interaction technique, the user controls the flow of the narration, thus achieving some kind of <strong> interactive fiction </strong> effect without any explicit commands.</p>

<h3>Artificial Realities</h3>

<p>Several artificial reality systems were shown at CHI'90, including a video of the NASA headmounted display where the user looked quite groggy when returned to the real world by taking off the helmet. The Canadian <strong> Mandala </strong> system was shown both in a hands-on exhibit and at the official "Empowered" show where.; Vincent J. Vincent gave a virtuoso performance on <strong> virtual drums</strong>: The performer waved his hands in thin air in front of a camera and could see himself superimposed on the computer screen with a complete set of bongos. I tried a game of <strong> virtual ice hockey </strong> where my body was superimposed on an image of the goal. The computer kept throwing pucks at me and I could try to block them without having to worry about broken teeth. Great fun. Both these application were non-command-based in that the user did not issue any specific commands to the system (except for a few gestures reserved as commands for quitting the applications or for special-purpose actions): Generating music was done by banging the drums, and playing the game was done by reaching for the pucks as they came flying.</p>

<p>Many of the artificial reality systems used the DataGlove as the primary input device and <strong> Nintendo </strong> showed a version called the <strong> PowerGlove </strong> which was cheap enough to be sold with regular video games. I tried a three dimensional version of Breakout which was somewhat like squash: I could "throw" the ball on the screen by making a throwing gesture with my hand and on the rebound I could either catch the ball again by grabbing it with the glove or just swat at it to get it to fly off in a new direction. This was a very limited artificial reality and it was not even very real (for example, I had a hard time throwing the ball in the right direction). But it was a genuine product and was going to be sold on the consumer market.</p>

<h3>Agents</h3>

<p><strong>Brenda Laurel </strong> and <strong> Abbe Don </strong> presented an experimental system from Apple for navigating a hypermedia space by the help of computerized <strong> agents</strong>. The underlying hypertext covered the history of the United States from 1800 to 1850 with text, images, sounds, animated maps, and video clips. Interestingly, user testing revealed a bias where users felt that those parts of the system that looked like TV were less believable than those parts that looked like books. This is in contrast to many traditional studies showing that the television news have greater credibility than newspapers.</p>

<p>To be guided through the system, users could activate a number of agents in the form of videotaped archetypes of the period (settler, trader, Indian, soldier, etc.). A further agent was dressed in modern clothes and represented meta-information about the system itself. This agent introduced herself by saying "when you need help, click on me."</p>

<p>Each agent had a simple process to determine what would be the most interesting part of the hypermedia space to go to given the current location and the perspective of the person represented by the agent. This decision could conceivably be made by an advanced artificial intelligence model of the stereotype represented by the agent, but was in fact made on the basis of simple information retrieval-type similarity ratings. The top of the screen would show several such agents and the ones who had something special to recommend (i.e., gave a high similarity score to some other hypertext node) would raise their hands or even jump up and down to attract attention.</p>

<p>These agents represent a form of non-command-based interaction because they allow users to navigate the information space from different perspectives without explicitly having to represent their interests. At any given time, the user can choose to follow the advice of a new agent, and the recommendations for next location are calculated without any intervention of the user and are only shown when the system judges them to be relevant.</p>

<p>A different sort of computerized agent was part of a show called <strong> Empowered </strong> where computers would play <strong> music to accompany human players</strong>. From the audience perspective, most of these performances were somewhat disappointing because it really does not matter to the listener whether a given piece of music is being generated by a large number of humans or by a single human plus a computer substituting for the rest of the orchestra. For the performer, however, the ability to have the computer play along must give a real feeling of empowerment. From the interaction paradigm perspective, these systems allowed the user to control the music output of the computer not by regular commands such as the specification of notes and tempo but simply by playing the trumpet, guitar, or some other instrument. The computer would analyze the user's music and play along.</p>

<h2>So What Else is New?</h2>

<p>Admittedly, the individual techniques and gadgets described above were really nothing new. Eye tracking has been used in user interface research for many years, artificial realities were shown at CHI'86 by NASA (an early headmounted display) and Myron Krueger (the <strong> Videoplace </strong> system) and were the focus of Michael McGreevy's keynote talk at CHI'89, the DataGlove was presented at CHI+GI'87, agents have been studied for many years at the Vivarium project, and play-along computer accompaniment for a flute player was part of the CHI'86 video show.</p>

<p>The main differences were that these developments had now moved from the fringe to the center place of the conference and that they had reached a realistic stage with respect to useful applications. I have always found futuristic user interfaces interesting and I have indeed reported on them in my coverage of earlier CHI conferences. This year, however, I became convinced that non-command-based interfaces were not just smart tricks but that they would form a major part of future interactive systems.</p>

<h2>Realistic Usability Engineering</h2>

<p>Certainly, CHI'90 had a lot of additional content besides the themes of the user interface profession and non-command-based paradigms. The Director of the MIT Laboratory for Computer Science, <strong> Michael Dertouzos</strong>, gave the keynote address and echoed last year's conference theme that <strong> user interfaces mean business</strong>. He stressed the need for us to accept total responsibility for the use of computers and not just to worry about fine-tuning the mouse or such. Dertouzos wanted us to measure the gains in <strong> total productivity </strong> from our work and to ensure that office worker productivity would grow by at least 3% per year. As a vivid example of current low productivity he reported that he had spent as much as 22 hours using current desktop presentation software to produce the slides for his one-hour talk.</p>

<p>Further examples of pragmatic work aimed at improving current practice were the two methods for simplified user interface evaluation presented by <strong> Clayton Lewis </strong> from the University of Colorado and myself. Both methods involved evaluating a user interface on the basis of a simply <em> looking </em> at it instead of relying on user testing (which is seen as expensive by some practitioners) or formal techniques (which are seen as intimidating by most practitioners). Lewis' technique was a theory-based <strong> walkthrough </strong> where the evaluator steps though an interaction with the system and fills in a checklist for each dialogue state, and it can therefore be said to be semi-formal. In contrast, the <a href="../../topic/heuristic-evaluation/index.php"> heuristic evaluation</a> technique presented by myself is completely informal and relies on the evaluator's general knowledge of established usability principles for the discovery of usability problems. Both methods seemed to work quite well, and the best results were actually achieved using Lewis' walkthrough method. Unfortunately, the two papers cannot be directly compared as Lewis and his team has used themselves as the test evaluators for their own method, whereas we had used regular software developers and students as our test evaluators. Therefore, the superior performance reported by the Lewis team might only be due to their personal abilities and deeper understanding of usability issues and not to any quality of the method itself.</p>

<p>To round out the session on usability methodology, <strong> Marcy Telles </strong> from <strong> WordStar </strong> International reported on the issues involved in <strong> updating an older interface </strong> to handle modern usability requirements. She mentioned that the word processor companies had been involved in a "features war" until recently, but that they were now conducting an "ease of use war" instead since almost all word processors now include all the features most users could dream of anyway. Therefore, WordStar has been updated from its classic command-key based interface to a modern, menu-based interface. Unfortunately, changing an existing product is much harder than designing a brand new one, as one has to keep the installed user base happy by allowing them to keep using the obscure key combinations that they have grown to love. For example, WordStar released a product called WordStar 2000 with a mnemonic command set which was presumably much better than the old command set, but even so, users did not migrate from the old system to the new one.</p>

<h2>Customized Buttons</h2>

<p><strong>Allan MacLean </strong> and <strong> Kathleen Carter </strong> from Xerox EuroPARC presented a paper on <strong> user-tailorable buttons </strong> that could encapsulate some command or other action and have a visible presence on the screen. The first, simple level of customization was to have parameters for some buttons, such as giving a print button a parameter for the number of copies to be printed. In most systems, the next level after parameterization would require users to program their own functions, possibly using a semi-task-oriented macro language as those found in spreadsheets. The Buttons system does allow users to program any possible Lisp function, but it also includes facilities to enable users to progress along a less steep learning curve towards greater customization.</p>

<p>One such facility is <strong> situated creation </strong> where the Buttons subsystem captures some properties of the total system into a button. For example, the user can capture some phrase from a text processing application in a button which will then insert the phrase whenever it is pressed. As another example, it is possible to create a button in the window system which will reestablish a window at a later date no matter where it came from.</p>

<p>Users can also copy and email buttons among themselves, thus leading to further customization of the individual user's work environment as that user collects relevant buttons. The buttons provide easy access to many attributes which can be changed without any need to access the underlying code. The code can also be edited to provide new buttons with slightly changed functionality. Sometimes, users who did not know Lisp programming were in fact able to modify buttons copied from other users because they could isolate relevant text strings in the code and change them.</p>

<h2>Videos</h2>

<p>The video show was mostly somewhat boring-maybe because many of the more advanced systems were shown live this year instead of being in the video show as they usually are. Notable videos included a programming-by-demonstration system called <strong> Metamouse </strong> by David Maulsby from the University of Calgary and a rule-based system by George Furnas from Bellcore where the production rules were bitmapped graphic illustrations of how elements of the screen looked before and after activation of each rule. Some aspects of user interfaces seem to be easier to describe graphically than by traditional textual rules.</p>

<p>A tape from Xerox filmed in 1982 showed their <strong> Star </strong> interface and was mostly of historical interest. Some of the operations in the interface seemed slightly awkward in retrospect, such as having to move icons on the desktop by pressing a move-key on the keyboard and then clicking a mouse where the icon is to go. This interaction technique did not use direct manipulation on the syntax level of the dialogue, even though it obviously was direct on the lexical level (specifying the location by a mouse-click, and specifying the command by pressing a dedicated key). To compensate for the lack of smoothness in the interaction technique, it was at least a generic command consistent with the way move actions were accomplished in the rest of the interface (In other systems, different methods are used for moving files in the operating system and data in the applications. This can lead to trouble for novice users trying to transfer skills from one part of the total computer system to another.) Furthermore, the availability of separate move and copy keys avoided the difficulty of distinguishing between these two operations commonly seen in systems where the complete commands are specified by direct manipulation only.</p>

<p>Finally, Brad Myers from CMU had produced a great video entitled <strong> All the Widgets </strong> with film segments showing the different ways various companies have implemented common interaction techniques like scroll bars or buttons. Since most of these techniques are not documented elsewhere, Myers has made a major contribution to the field, and the tape is a must-have for any faculty member teaching user interfaces and for any user interface design team working on basic interaction techniques. An entire set of student assignments can be derived with almost no effort from the video: Analyze the examples of such-and-such interaction technique: What are the main differences and how do they relate to established usability principles and empirical data? It would also be interesting to develop a hypermedia system for teaching user interface design using the variety of widgets on Myers' tape, but doing so would be a major project.</p>
</section>
  

  
  <section>
    
        
    
  </section>
  
  
  
  
  

  


  <section>
    <br />
    <p class="share"><b>Share <span class="hide-for-small-down">this article</span>:</b> <a href="https://twitter.com/intent/tweet?url=http://www.nngroup.com/articles/trip-report-chi-90/&amp;text=CHI'90%20Trip%20Report&amp;via=nngroup">Twitter</a> | <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=http://www.nngroup.com/articles/trip-report-chi-90/&amp;title=CHI'90%20Trip%20Report&amp;source=Nielsen%20Norman%20Group">LinkedIn</a> | <a href="https://plus.google.com/share?url=http://www.nngroup.com/articles/trip-report-chi-90/">Google+</a> | <a href="mailto:?subject=NN/g Article: CHI&#39;90 Trip Report&amp;body=http://www.nngroup.com/articles/trip-report-chi-90/">Email</a></p>
    <hr />
  </section>

  <footer class="l-subsection">
    
    
    <div class="learn-more">
      <h2>Learn More</h2>
      <p><a href="../subscribe/index.php">Subscribe to the weekly newsletter</a> to get notified about future articles.</p>
      </div>
    
    

    <div class="row show-for-small-down related-content">
      
      
      

      
      
        <div class="column small-12 topic-links">All articles about:
          <ul>
            
            <li><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction</a></li>
            
          </ul>
        </div>
      
      

      
      
        <div class="column small-12">
          <h3>UX Conference Training Courses</h3>
           <ul class="no-bullet">
    
        <li><a href="../../courses/ux-basic-training/index.php">UX Basic Training</a></li>
    
        <li><a href="../../courses/hci/index.php">User Interface Principles Every Designer Must Know</a></li>
    
        <li><a href="../../courses/interaction-design-3-day-course/index.php">Interaction Design: 3-Day Course</a></li>
    
</ul>
        </div>
      
      

      
      
      

      
      
      
    </div>

    <div class="hide-for-small-down row related-content">
      <div class="medium-6 columns">
        
        
          <div>
            <h3>Articles</h3>
            <ul class="no-bullet">
              
                
                <li><a href="../embarrassment/index.php">Computer-Assisted Embarrassment</a></li>
                
              
                
                <li><a href="../enhancement/index.php">The Role of Enhancement in Web Design</a></li>
                
              
                
                <li><a href="../lazy-users/index.php">Why Designers Think Users Are Lazy: 3 Human Behaviors</a></li>
                
              
                
                <li><a href="http://www.jnd.org/dn.mss/apples_products_are.php">Apple&#39;s products are getting harder to use because they ignore principles of design</a></li>
                
              
                
                <li><a href="../efficiency-vs-expectations/index.php">Don’t Prioritize Efficiency Over Expectations</a></li>
                
              
            </ul>
          </div>
        
        

        

      </div>
      <div class="medium-6 columns">
        
          
          <div>
            <h3>UX Conference Training Courses</h3>
             <ul class="no-bullet">
    
        <li><a href="../../courses/ux-basic-training/index.php">UX Basic Training</a></li>
    
        <li><a href="../../courses/hci/index.php">User Interface Principles Every Designer Must Know</a></li>
    
        <li><a href="../../courses/interaction-design-3-day-course/index.php">Interaction Design: 3-Day Course</a></li>
    
</ul>
          </div>
          
        
        
      </div>
    </div>
  </footer>
</article>

        </div>

      

    
  </div>
  <div class="row">
    
      <div class="column small-12 show-for-small-down l-navrail announcements">
        
          
        
      </div>
    
    
  </div>

    </div>
    




<footer id="page-footer">
    <div class="row hide-for-print">
        <div class="column small-12 medium-6">
            <section class="footer-popular">
                
                <h1>Popular Topics</h1>
                <div style="float: left;  width: 15em; display: block;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/applications/index.php">Application Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/e-commerce/index.php">E-Commerce Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/eyetracking/index.php">Eyetracking </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/information-architecture/index.php">Information Architecture </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/intranets/index.php">Intranet Design </a></li>
	<li><a href="../../topic/interaction-design/index.php">Interaction Design </a></li>
</ul>
</div>

<div style="float: left;  width: 15em; display: block; list-style: none;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/mobile-and-tablet-design/index.php">Mobile and Tablet Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/credibility-persuasion/index.php">Persuasive Design</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/research-methods/index.php">Research Methods</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/user-testing/index.php">User Testing </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/web-usability/index.php">Web Usability </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/writing-web/index.php">Web Writing &amp; Content Strategy </a></li>
</ul>
</div>

<p>&nbsp;</p>

                
            </section>
        </div>

        <div class="column small-12 medium-6">
            <div class="row">
                <div class="column small-6">
                    <section class="footer-about">
                        <h1>Follow Us</h1>
                        <ul>
	<li><a href="../subscribe/index.php">Subscribe to our weekly UX newsletter</a>,&nbsp;Jakob Nielsen&#39;s Alertbox</li>
	<li>Visit Don Norman&#39;s website, <a href="http://www.jnd.org/"> www.jnd.org </a></li>
	<li>Visit Bruce Tognazzini&#39;s website, <a href="http://www.asktog.com/"> www.asktog.com </a></li>
	<li style="padding-top: 6px;"><a class="twitter-follow-button" data-show-count="false" data-size="large" href="https://twitter.com/nngroup"><img alt="Follow @nngroup on Twitter" height="20" src="https://media.nngroup.com/media/editor/2016/12/30/twitter-follow-button.png" style="width: 117px; height: 20px;" width="117" /></a></li>
	<li style="padding-top: 6px;"><img alt="" height="14" src="https://media.nngroup.com/media/editor/2016/07/26/feed-icon-14x14.png" style="vertical-align:text-top;width:14px;margin-right:.25em;" width="14" /><a href="../../feed/rss/index.php">Subscribe to our RSS feed</a></li>
</ul>

                    </section>
                </div>

                <div class="column small-6">
                    <section class="footer-about right-column">
                        <h1>About
</h1>
                        <ul class="no-bullet">
	<li><a href="../../about/contact/index.php">Contact information</a></li>
	<li><a href="../../about/index.php">About Us</a></li>
	<li><a href="../../about/why-nng/index.php">Why NN/g? </a></li>
	<li><a href="../../return-policy/index.php">Return Policy</a></li>
</ul>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="copyright column small-12">
            <p><a href="../../copyright-and-reprint-info/index.php">Copyright</a> &copy; 1998-2017 Nielsen Norman Group, All Rights Reserved.</p>
        </div>
    </div>
</footer>


    


        <script src="https://media.nngroup.com/static/vendor/fastclick/js/fastclick.min.js"></script>
        <script src="https://media.nngroup.com/static/vendor/foundation/js/foundation.min.js"></script>
        <script src="http://media.nngroup.com/static/js/app.js"></script>
        <script src="http://media.nngroup.com/static/js/scroll-to-fixed.js"></script><script>var externalLinks = document.querySelectorAll('a[href^="http"]');for (var i = externalLinks.length-1; i >= 0; i--) {externalLinks[i].addEventListener("click", function(event) { alert("External links are disabled for this test."); event.preventDefault(); }, false);}var mailLinks = document.querySelectorAll('a[href^="mailto"]');for (var i = mailLinks.length-1; i >= 0; i--) {mailLinks[i].addEventListener("click", function(event) { alert("Email links are disabled for this test."); event.preventDefault(); }, false);}var externalForms = document.querySelectorAll('form');for (var i = externalForms.length-1; i >= 0; i--) {externalForms[i].addEventListener("submit", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false); externalForms[i].addEventListener("click", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false);}</script>

        <script>
            
            $(function() {
                $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
                $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
            });
        </script>

        
<script src="https://media.nngroup.com/static/js/articles.js"></script>


        
        <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1072699960;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072699960/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
    </body>

<!-- Mirrored from www.nngroup.com/articles/trip-report-chi-90/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:14:36 GMT -->
</html>
