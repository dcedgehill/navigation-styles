<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->





    
<!-- Mirrored from www.nngroup.com/articles/context-specific-cross-channel/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:01:27 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1026.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"licenseKey":"1a30ef9931","queueTime":0,"beacon":"bam.nr-data.net","errorBeacon":"bam.nr-data.net","applicationID":"2048992","transactionName":"NFxSMRBUX0EDUkIPDA0WdhAMVkVbDV8ZBxEXUFMJB0YfRAtUQRVZIktEDAFZVHYHRVcPD01eVRE=","applicationTime":438,"agent":""}</script>
        <title>Optimizing for Context in the Omnichannel User Experience</title><meta property="og:title" content="Optimizing for Context in the Omnichannel User Experience" />
  
        
        <meta name="description" content="Design for each channel’s unique strengths and role in the customer journey to create usable context-specific experiences.">
        <meta property="og:description" content="Design for each channel’s unique strengths and role in the customer journey to create usable context-specific experiences." />
        
  
        
	
        
        <meta name="keywords" content="Omnichannel,  cross-channel, customer experience, customer journey, ">
        

        

        <meta name="robots" content="noarchive">
        <meta name="viewport" content="width=device-width">

        <link rel="search" type="application/opensearchdescription+xml" title="NN/g Search" href="../../opensearch.xml" />
        <link rel="shortcut icon" href="http://media.nngroup.com/static/img/favicon.ico" />

        <link rel="stylesheet" href="https://media.nngroup.com/static/vendor/font-awesome/css/font-awesome.min.css">

        
        <link rel="stylesheet" href="https://media.nngroup.com/static/css/7232a960f1df.css" type="text/css" />
        <link rel="stylesheet" href="http://fast.fonts.com/t/1.css?apiType=css&amp;projectid=c5846260-9def-46ec-9407-0f1349bd2d64" type="text/css">

        <script src="https://media.nngroup.com/static/vendor/modernizr/js/modernizr.min.js"></script>

        <script type="text/javascript" src="https://media.nngroup.com/static/js/3283be5def74.js"></script>
        

        

        
        
        <!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=229034,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('http://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->


<!-- Google Analytics Tracking Code for http://nngroup.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23132681-1', 'nngroup.com');
  ga('require', 'linkid', 'https://www.nngroup.com/articles/context-specific-cross-channel/linkid.js');
  ga('require', 'displayfeatures');

// Optimizely Universal Analytics Integration
 window.optimizely = window.optimizely || [];
 window.optimizely.push("activateUniversalAnalytics");

  ga('send', 'pageview');

</script>
<script>
/**
* Function to track a click on an outbound link in Google Analytics.
*/
function trackOutboundLink(label, url) {
    if(window.ga && ga.loaded) {
        ga('send', 'event', 'outbound', 'click', label, {'hitCallback': function(){document.location = url;}
        });
    } else {
        document.location = url;
    }
}
/**
* Function to track a click on a link in the article announcement area.
*/
$('#articleAnnouncement a').on('click', function(){
  ga('send', 'event', 'link', 'click', 'articleAnnouncement');
});
</script>

<script type="text/javascript">
 setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2421.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



<!-- Hotjar Tracking Code for http://nngroup.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:121605,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

        <script src="https://media.nngroup.com/static/js/analytics/track_outbound_link.js"></script>
        
        
        
    </head>
    <body class="location-articles articles-detail">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
    <header id="page-header">
        <div id="cart" class="shopping-cart mobile-cart show-for-small-down"></div>
        <div class="row">
            <div id="masthead" class="masthead small-12 medium-7 column">

                <!-- The button to toggles menu on small screen -->

                <h1><a href="../../index.php"><img src="http://media.nngroup.com/static/img/logo-wordmark.png" width="74" height="30"> Nielsen <span class="norman">Norman</span> Group</a></h1>
                <p class="tagline show-for-medium-up">Evidence-Based User Experience Research, Training, and Consulting</p>

                <!-- The button to toggles form search on small screen -->


                <div style="clear: right;width:  100%;height: 1px;"></div><?php define('ROOT_PATH',$_SERVER["DOCUMENT_ROOT"] . '/apps/navigation_styles/navigations/4'); ?><?php include(ROOT_PATH)."/navigation.php"; ?>

                <!-- Drop down menu on small screen -->
                <div id="hideNav" class="show-for-small-down">
                  
                  <ul class="menu-content">
                    <li><a href="../../index.php">Home</a></li>
                    <li class="has-submenu">
                      <a>Training</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../training/index.php">Overview</a></li>
                        
<li id="nav-ux-conference"><a href="../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../online-seminars/index.php"><span>Online Seminars</span></a></li>

                      </ul>
                    </li>
                    <li class="has-submenu">
                      <a>Consulting</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
  <li><a href="../../consulting/index.php">Overview</a></li>
  
  <li><a href="../../consulting/benchmarking/index.php">Benchmarking</a></li>
  
  <li><a href="../../consulting/expert-design-review/index.php">Expert Design Review</a></li>
  
  <li><a href="../../consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
  
  <li><a href="../../consulting/usability-testing/index.php">Usability Testing </a></li>
  
  <li><a href="../../consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
  
  <li><a href="../../in-house-training/index.php">In-House Training</a></li>
</ul>

                    </li>

                    <li><a href="../../reports/index.php">Reports</a></li>
                    <li><a href="../index.php">Articles</a></li>
                    <li class="has-submenu"><a>About NN/G</a><i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../about/index.php">Overview</a></li>
                        <li><a href="../../people/index.php">People</a></li>
                        <li><a href="../../about/why-nng/index.php">Why NN/g?</a></li>
                        <li><a href="../../about/contact/index.php">Contact</a></li>
                        <li><a href="../../news/index.php">News</a></li>
                        <li><a href="../../about/history/index.php">History</a></li>
                        <li><a href="../../books/index.php">Books</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                </div>

                <hr class="header-divider show-for-small-only"/>


            </div>
       </div>

        <div id="hideNav">            <nav class="primary-nav small-12 column">
                <ul>
                    <li><a class="navitem" id="nav-home" href="../../index.php">Home</a></li>
                    <li><a class="navitem" id="nav-training" href="../../training/index.php">Training</a></li>
                    <li><a class="navitem" id="nav-consulting" href="../../consulting/index.php">Consulting</a></li>
                    <li><a class="navitem" id="nav-reports" href="../../reports/index.php">Reports</a></li>
                    <li><a class="navitem" id="nav-articles" href="../index.php">Articles</a></li>
                    <li><a class="navitem" id="nav-about" href="../../about/index.php">About NN/G</a></li>
                </ul>
            </nav>
            <nav class="secondary-nav small-12 medium-12 large-12 columns">
                
                
            </nav>
        </div>
    </header>
    <div class="l-content">
    
  

  

  <div class="row">
    
      
      <section class="small-12 medium-3 columns collapsable-content l-navrail articles-left-column">
        <h1 class="show-for-small-down collapse-link mobile-browse-header">Browse by Topic and Author</h1>
          <div class="row collapsable">

            
            <div class="small-6 medium-12 columns">
              <section id="article-popular-topics" style="display: none;">
                
                <h1>Topics</h1>
                <ul id="article-popular-topics">
                    
                    <li>
                        
                        <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/intranets/all/index.php">Intranets</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/user-testing/all/index.php">User Testing</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                        
                    </li>
                    
                    <li><a href="#" id="article-see-all-topics"><strong>See all topics&hellip;</strong></a></li>
                </ul>
                
              </section>
              <section id="article-all-topics" style="display: none;">
                
                <h1>
                  <span class="show-for-medium-up">All Article Topics</span>
                  <span class="show-for-small-down">All Topics</span>
                  (<a href="#" id="article-hide-all-topics">hide</a>)
                </h1>
                <ul>
                  
                  <li>
                    
                    <a href="../../topic/accessibility/all/index.php">Accessibility</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/agile/all/index.php">Agile</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/analytics-and-metrics/all/index.php">Analytics &amp; Metrics</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/applications/all/index.php">Application Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/b2b-websites/all/index.php">B2B Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/behavior-patterns/all/index.php">Behavior Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/branding/all/index.php">Branding</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/content-strategy/all/index.php">Content Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/corporate-websites/all/index.php">Corporate Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/design-patterns/all/index.php">Design Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-design-process/all/index.php">Design Process</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/email/all/index.php">Email</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/eyetracking/all/index.php">Eyetracking</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/heuristic-evaluation/all/index.php">Heuristic Evaluation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/human-computer-interaction/all/index.php">Human Computer Interaction</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-ideation/all/index.php">Ideation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/information-architecture/all/index.php">Information Architecture</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/interaction-design/all/index.php">Interaction Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/international-users/all/index.php">International Users</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/intranets/all/index.php">Intranets</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-management/all/index.php">Management</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/navigation/all/index.php">Navigation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/non-profit-websites/all/index.php">Non-Profit Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/personas/all/index.php">Personas</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/persuasive-design/all/index.php">Persuasive Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/prototyping/all/index.php">Prototyping</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/psychology-and-ux/all/index.php">Psychology and UX</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/research-methods/all/index.php">Research Methods</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/search/all/index.php">Search</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/social-media/all/index.php">Social Media</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/strategy/all/index.php">Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/user-testing/all/index.php">User Testing</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-teams/all/index.php">UX Teams</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/visual-design/all/index.php">Visual Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/writing-web/all/index.php">Writing for the Web</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/young-users/all/index.php">Young Users</a>
                    
                  </li>
                  
                </ul>
                
              </section>
            </div>
            <script>
              /* Determine which topics list will be displayed: the full or the
               * partial.  We are inlining this close to the list itself to
               * prevent rendering delays on the client
               */
              var topic_list = $.cookie("article-topics");

              var popular_topics = $('#article-popular-topics');
              var all_topics = $('#article-all-topics');

              if(topic_list == 1) {
                  popular_topics.hide();
                  all_topics.show();
              } else {
                  popular_topics.show();
                  all_topics.hide();
              }
            </script>
            

            
            <div class="small-6 medium-12 columns">
              
              <section id="article-initial-authors">
                  <h1>Author</h1>
                  <ul>
                    
                    <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                    
                    <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                    
                    <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                    
                    <li><a href="#" onClick="$('#article-initial-authors').hide(); $('#article-all-authors').show();"><strong>See all authors&hellip;</strong></a></li>
                  </ul>
              </section>
              <section id="article-all-authors" style="display: none;">
                <h1>
                  All Authors
                  (<a href="#" id="article-hide-all-authors">hide</a>)
                </h1>
                <ul>
                  
                  <li><a href="../author/raluca-budiu/index.php">Raluca Budiu</a></li>
                  
                  <li><a href="../author/patty-caya/index.php">Patty Caya</a></li>
                  
                  <li><a href="../author/susan-farrell/index.php">Susan Farrell</a></li>
                  
                  <li><a href="../author/therese-fessenden/index.php">Therese Fessenden</a></li>
                  
                  <li><a href="../author/kim-flaherty/index.php">Kim Flaherty</a></li>
                  
                  <li><a href="../author/sarah-gibbons/index.php">Sarah Gibbons</a></li>
                  
                  <li><a href="../author/aurora-harley/index.php">Aurora Harley</a></li>
                  
                  <li><a href="../author/kate-kaplan/index.php">Kate Kaplan</a></li>
                  
                  <li><a href="../author/page-laubheimer/index.php">Page Laubheimer</a></li>
                  
                  <li><a href="../author/angela-li/index.php">Angie Li</a></li>
                  
                  <li><a href="../author/hoa-loranger/index.php">Hoa Loranger</a></li>
                  
                  <li><a href="../author/kate-meyer/index.php">Kate Meyer</a></li>
                  
                  <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                  
                  <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                  
                  <li><a href="../author/kara-pernice/index.php">Kara Pernice</a></li>
                  
                  <li><a href="../author/christian-rohrer/index.php">Christian Rohrer</a></li>
                  
                  <li><a href="../author/amy-schade/index.php">Amy Schade</a></li>
                  
                  <li><a href="../author/katie-sherwin/index.php">Katie Sherwin</a></li>
                  
                  <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                  
                  <li><a href="../author/kathryn-whitenton/index.php">Kathryn Whitenton</a></li>
                  
                </ul>
              </section>
              
            </div>
            
          </div>

          
          <section class="hide-for-small-down">
            
              
              <h1>Recent Articles</h1>
              
              <ul>
                  
                  <li><a href="../seamless-cross-channel/index.php">Seamlessness in the Omnichannel User Experience</a></li>
                  
                  <li><a href="../brand-intention-interpretation/index.php">Experience Design: Bridging Brand Intention and Brand Interpretation</a></li>
                  
                  <li><a href="../eyetracking-tasks-efficient-scanning/index.php">Scanning Patterns on the Web Are Optimized for the Current Task</a></li>
                  
                  <li><a href="../flat-design-best-practices/index.php">Flat-Design Best Practices</a></li>
                  
                  <li><a href="../ux-user-stories/index.php">Accounting for UX Work with User Stories in Agile Projects</a></li>
                  
                  <li><a href="../index.php"><strong>See all articles&hellip;</strong></a></li>
              </ul>
              
            
          </section>
          

          
          
          <section class="hide-for-small-down">
            <h1>Popular Articles</h1>
            
            <ul>
                
                <li><a href="../usability-101-introduction-to-usability/index.php">Usability 101: Introduction to Usability</a></li>
                
                <li><a href="../top-10-mistakes-web-design/index.php">Top 10 Mistakes in Web Design</a></li>
                
                <li><a href="../how-users-read-on-the-web/index.php">How Users Read on the Web</a></li>
                
                <li><a href="../f-shaped-pattern-reading-web-content/index.php">F-Shaped Pattern For Reading Web Content</a></li>
                
                <li><a href="../ten-usability-heuristics/index.php">10 Usability Heuristics for User Interface Design</a></li>
                
                <li><a href="../intranet-design-2016/index.php">10 Best Intranets of 2016</a></li>
                
                <li><a href="../which-ux-research-methods/index.php">When to Use Which User-Experience Research Methods</a></li>
                
                <li><a href="../response-times-3-important-limits/index.php">Response Times: The 3 Important Limits</a></li>
                
                <li><a href="../why-you-only-need-to-test-with-5-users/index.php">Why You Only Need to Test with 5 Users</a></li>
                
                <li><a href="../page-fold-manifesto/index.php">The Fold Manifesto: Why the Page Fold Still Matters</a></li>
                
            </ul>
          </section>
          
          

          
            <section class="hide-for-small-down">
              
            </section>
          

      </section>
      

      
        <div class="small-12 medium-9 columns content-body">
          




<article class="l-subsection alertbox-article-content article-detail-bl">
  
  <header>
    <div class="l-subsection">
      <h1>Optimizing for Context in the Omnichannel User Experience</h1>
      <div class="author-meta">by
        
          
            
            <a href="../author/kim-flaherty/index.php">Kim Flaherty</a>
            
          
        on  February 26, 2017
        
      </div>

      

      
      <div class="topic-links">
        Topics:
        
          <ul>

  <li><a href="../../topic/mobile-and-tablet-design/index.php">Mobile &amp; Tablet</a></li>

  <li><a href="../../topic/strategy/index.php">Strategy</a></li>

  <li><a href="../../topic/e-commerce/index.php">E-commerce</a></li>

</ul>
        
      </div>
      
    </div>
  </header>
  

  
  
  <section>
    <p class="lede"><strong>Summary:</strong> Design for each channel’s unique strengths and role in the customer journey to create usable context-specific experiences.</p>
    <br />
  </section>
  
  

  
  <section id="article-body"><p>Users interact with organizations through many <a href="../channels-devices-touchpoints/index.php">channels</a>, including the web, email, mobile and tablet applications, kiosks, online chat, telephone, and by visiting physical locations. Regardless of the channel, people expect companies to provide a usable experience. For this reason, it's important to understand the channel’s role in the customer journey and design for that channel’s context while creating the omnichannel user experience (sometimes referred to as cross-channel user experience).</p>

<p>Our user research on omnichannel user experience identified 5 key components of a usable cross-channel experience:</p>

<ul>
	<li><a href="../omnichannel-consistency/index.php">Consistent</a></li>
	<li>Optimized for context</li>
	<li><a href="../seamless-cross-channel/index.php">Seamless</a></li>
	<li>Orchestrated</li>
	<li>Collaborative</li>
</ul>

<p>The following article discusses the role of the second component — design optimized for context— in the omnichannel experience.</p>

<h2>What Makes Up Context of Use</h2>

<p>Before discussing why design optimized for context is essential for omnichannel experience, we need to clarify the constituents of context. Here are four elements that are essential to understanding context of use and to delivering right experience at the right time: understand:</p>

<ol>
	<li>Users’ <strong>most important and common tasks</strong>: why people engage with the business</li>
	<li><strong>When and where</strong> they complete these tasks: the typical setting, environment, and point in time</li>
	<li><strong>What devices and channels</strong> they use to complete these tasks at different stages of the customer journey, as well as the most common tasks on each of these channels</li>
	<li>The <strong>strengths of each device and channel</strong> and the most common context of use for each of them</li>
</ol>

<p>From looking at this list, it’s easy to see that some of the elements refer to users and their tasks and some refer to devices and channels. While the user-related contextual elements will be specific to your business and the services it provides, many of the device-related contextual elements are general and apply across a variety of organization types. We’ll discuss these in the next section.</p>

<h2>Channel Strengths</h2>

<p>Each device has strengths that influence the assumptions we can make about why, how and when users might choose to use these devices to interact with an organization.  For example:</p>

<ul>
	<li>The desktop has a large screen with efficient input devices like the keyboard and mouse. We can assume that most users on the desktop or laptop are looking to multitask, be productive and efficient, and complete complex tasks.</li>
	<li>The mobile phone is portable and used everywhere. It has unique strengths like geolocation, photo-input capabilities, or fingerprint-based authentication, but sessions are short and often interrupted by external events. We can assume that users on the phone are either at home or on the go, but they are looking to complete smaller microtasks or access targeted pieces of information quickly.</li>
</ul>

<p>Being aware of these device strengths and common contexts of use will help teams understand each device’s role in the customer journey and will enable them to design experiences that include practical and easy-to-use elements which meet users’ needs and expectations on each device and channel and at specific points in their journey.</p>

<div style="width:100%;overflow:auto;">
<table border="1" cellpadding="0" cellspacing="1" style="width:720px;">
	<tbody>
		<tr>
			<td width="10%"><strong>Device</strong></td>
			<td width="20%"><strong>Common Channels</strong></td>
			<td width="28%"><strong>Device Strengths</strong></td>
			<td width="37%"><strong>Context Assumptions</strong></td>
		</tr>
		<tr>
			<td>Desktop</td>
			<td valign="top">
			<p>Web</p>

			<p>Email</p>

			<p>Social media</p>

			<p>Online chat</p>
			</td>
			<td align="left" valign="top">
			<p>Large Screen</p>

			<p>High Processing power</p>

			<p>Access to input devices (keyboard and mouse)</p>
			</td>
			<td align="left" valign="top">
			<p>At home or at work</p>

			<p>Used for productivity in complex tasks and multitasking</p>
			</td>
		</tr>
		<tr>
			<td>Tablet</td>
			<td valign="top">
			<p>Web</p>

			<p>Email</p>

			<p>Social media</p>

			<p>Online chat</p>

			<p>Tablet Applications</p>
			</td>
			<td valign="top">
			<p>Portable</p>

			<p>Camera</p>

			<p>Location-aware</p>

			<p>Gestures and touch input</p>

			<p>Medium-size high-resolution portable screen</p>
			</td>
			<td valign="top">
			<p>Media consumption such as streaming video, reading books, checking email)</p>

			<p>Used in environments not conducive for a keyboard and mouse</p>

			<p>Mainly used in the home</p>
			</td>
		</tr>
		<tr>
			<td>Smartphone</td>
			<td valign="top">
			<p>Web</p>

			<p>Email</p>

			<p>Social media</p>

			<p>Online chat</p>

			<p>Mobile phone applications</p>

			<p>Telephone</p>

			<p>Text Message</p>
			</td>
			<td valign="top">
			<p>Highly portable</p>

			<p>Camera</p>

			<p>Location-aware</p>

			<p>Dialing and talking</p>

			<p>Gestures and touch input</p>
			</td>
			<td valign="top">
			<p>Always within reach</p>

			<p>Used for micro tasks – sending, receiving and retrieving small pieces of information</p>

			<p>Used for spur of the moment activity</p>

			<p>Used away from the home</p>
			</td>
		</tr>
	</tbody>
</table>
</div>

<p><em>This table outlines the common devices together with the channels commonly associated with them, their strengths, and the assumptions we can make about the context of use for sessions on these devices.</em></p>

<h2>Optimizing Channel Experiences Based on Context</h2>

<p>Below we will discuss some of the most common devices, and how to optimize the experience on the channels associated with those devices. </p>

<h2>Desktop/Laptop Channels</h2>

<ul>
	<li><strong>Support complex tasks.</strong> Complex activities such as filling out a long form, viewing a complex dashboard, or even making a large purchase online are often done on a laptop or desktop device. The unique strengths of these devices make them well suited for such activities: the large screen, the ease of typing on a keyboard, the Internet speed and processing power, the lower likelihood of being interrupted and making mistakes.</li>
</ul>

<p style="margin-left: 40px;">Here’s a quote from a participant in one of our studies, giving some of his reasons for using the desktop site of e*Trade, an online investment platform:</p>

<p style="margin-left: 40px;"> “Well I always do day-to-day trading on the e*Trade mobile app, and I’ll use the app for checking notifications and status updates. But, when I’m having a call with my [financial] advisor, I’ll pull up my investment dashboard on the desktop so I can spread the browser out and see longer trends easier. Also, If I’m researching a new stock or something I do that on my desktop because I am going to be doing a lot of reading and typing and most likely I’ll have a lot of browser tabs going at one time.”</p>

<ul>
	<li><strong>Provide rich, scannable content on desktop.</strong> Those conducting research in the early stages of a customer journey commonly use desktop websites. Users expect this channel to provide detailed content and support decision-making tasks best.</li>
</ul>

<p style="margin-left: 40px;">A participant in one of our studies was considering purchasing a Nest Protect smoke-and-carbon-monoxide alarm. He visited the Nest website on his laptop to research the product and had this to say about his experience: “I was trying to research Nest’s voice interaction, and see all the ways the Nest interacts with the iPhone. I thought the website would have the most information on the product capabilities. I used my laptop because I figured the large monitor would best allow me to look through the site and interact with multiple windows. I disliked how the information was [presented] only in little blurbs and not in full detail.”</p>

<p style="margin-left: 40px;">This user’s information needs were not met at this point in his journey: he was looking for detailed specs and a comparison of the various models available, but the page only provided general snippets of information.</p>

<figure class="caption"><img alt="Nest Protect Desktop " height="1633" src="https://media.nngroup.com/media/editor/2017/02/01/nest-protect-desktop.png" width="720"/>
<figcaption><em>Nest.com: The Nest Protect product-detail page did not provide rich product information.</em></figcaption>
</figure>

<p style="margin-left: 40px;">In contrast, Jiffy Lube’s desktop website was nicely optimized for the desktop: it had rich information outlining the array of vehicle services that the company offers, as well as easy access to complex activities such as applying for a credit card, purchasing a gift card, and requesting information about fleet services. The brake-services page included rich, scannable information, presenting all of the available brake services in detail.</p>

<figure class="caption"><img alt="Jiffylube desktop" height="570" src="https://media.nngroup.com/media/editor/2017/02/01/jiffylube-desktop.png" width="720"/>
<figcaption><em>Jiffy Lube’s desktop website provided in-depth content and easy access to complex activities.</em></figcaption>
</figure>

<h2>Mobile-Phone and Tablet Channels</h2>

<ul>
	<li><strong>For mobile phones, emphasize content and features needed on the go</strong>. The GPS feature available to mobile applications and websites allows them to detect users’ physical location and provide content (e.g., local promotions, distance to points of interest) tailored to it. Many activities on the go are centered around the “here and now”, which is why store finders and other locators should be especially prominent in mobile designs.</li>
</ul>

<p style="margin-left: 40px;">The Jiffy Lube mobile website took advantage of the smartphone’s strengths by detecting user’s current location and  displaying information about the closest service center.  The site also presented location-specific mobile coupons, which could be shown and scanned right from a customers phone, making a seamless transition from the digital to the physical world.</p>

<figure class="caption"><img alt="Jiffylube mobile" height="831" src="https://media.nngroup.com/media/editor/2017/02/01/jiffylube-mobile.png" width="350"/>
<figcaption><em>Jiffy Lube’s mobile website emphasized content and features needed on the go.</em></figcaption>
</figure>

<ul>
	<li><strong>Provide layered and consumable information on mobile devices. </strong>Because mobile screens are smaller and mobile sessions are shorter and more likely to be interrupted, it is important to <a href="../condense-mobile-content/index.php">condense mobile content</a>, prioritize the gist of the information first, and then delegate <a href="../defer-secondary-content-for-mobile/index.php">secondary details to secondary views</a>.</li>
</ul>

<p style="margin-left: 40px;">Jiffy Lube’s mobile site layered information to make it easily consumable on a small screen. The site presented the list of available services in a menu; within the detail page for each service, it provided a high level description of the service and hid additional details (likely to be needed by some users only)  under accordions. This layered approach allowed mobile users to scan available topics and dig deeper into those of interest, thus making the rich content more manageable on a small screen. Each service-detail page included a link to nearby locations that offered the corresponding service; thus taking advantage of the smartphone’s strengths.</p>

<figure class="caption"><img alt="Jiffylube Mobile Menu" height="621" src="https://media.nngroup.com/media/editor/2017/02/01/jiffylube-mobile-menu.png" width="720"/>
<figcaption><em>Jiffy Lube’s mobile website: The site menu (left) and vehicle-service page (right) layer the information for mobile viewing and optimize the experience for the mobile context.</em></figcaption>
</figure>

<ul>
	<li><strong>Integrate channel capabilities</strong>. Smartphones and tablets have unique capabilities like geolocation, built-in cameras and text capabilities that can be leveraged to deliver simplified and contextual experiences on mobile and tablet channels.</li>
</ul>

<p style="margin-left: 40px;">Chase Bank uses text messages to alert customers of potential fraudulent purchases on their credit cards.  Text messaging is often seen as more immediate and accessible than email on mobile devices. They’re lightweight, actionable and tend to arrive very quickly. Email on the other hand includes a bit more overhead. It can take more time to arrive, require a more substantial amount of data, and users must open their email application and locate the action item within the email.</p>

<p style="margin-left: 40px;">After making several large purchases in a single, day, one study participant had her Chase credit card denied when she tried another purchase. While at the register, she received a Chase fraud-alert text message, asking her to approve or decline the charges. She said,  “It was awesome, I felt my phone buzz and it was a text from Chase. All I had to do was reply to accept the charges and then my card immediately went through. Turns out they sent me an email about it too and they called my phone and left a message. I hardly had any service in the mall though, so I didn’t get those until I left. I doubt I would have noticed a new email and checked it at the register anyway.”</p>

<p style="margin-left: 40px;">Chase understood that SMS is an effective channel to reach customers who are on the go and potentially preoccupied with other things. By utilizing the phone’s messaging capabilities and using text to push urgent communications, Chase optimized the experience for the context of the customer journey and for the device that is commonly on hand while shopping.</p>

<figure class="caption"><img alt="Chase Bank Text Message" height="336" src="https://media.nngroup.com/media/editor/2017/02/06/chasebank-fraudalert-v2.png" width="347"/>
<figcaption><em>Chase Bank sent a fraud alert via text message; the alert was easy to notice and allowed users to react to it quickly.</em></figcaption>
</figure>

<figure class="caption"><img alt="Turbotax photo upload" height="360" src="https://media.nngroup.com/media/editor/2017/02/01/turbotax-photo-upload.png" width="720"/>
<figcaption><em>Turbotax software took advantage of the mobile phone’s camera functionality to allow users to quickly input income information by photographing the forms received from their employers (e.g., W2)  for automatic upload.</em></figcaption>
</figure>

<figure class="caption"><img alt="Navy Federal iPad TouchID" height="526" src="https://media.nngroup.com/media/editor/2017/02/01/navyfederal-cu-ipad-touchid.png" width="720"/>
<figcaption><em>The Navy Federal Credit Union’s iPad application took advantage of the iPad’s Touch ID capabilities by allowing customers to log into their accounts using their fingerprints. This made login much simpler by not requiring users to remember a password, while still safeguarding personal financial information.</em></figcaption>
</figure>

<ul>
	<li><strong>Emphasize media consumption in tablet experiences.</strong> Although tablets can be quite portable, they aren’t used on the go as heavily as the smartphones.  The larger, high-resolution tablet screen is great for reading, viewing photos, and watching videos. That’s why tablets are commonly used for content consumption and for other tasks that don’t require large amounts of text input.</li>
</ul>

<p style="margin-left: 40px;">Our Nest Protect customer moved from desktop to tablet when he finished comparing the different features of candidate products and wanted to watch installation videos for each model. He said, “I decided to buy the Nest Protect, but I wanted to compare the wireless and wired versions and what goes along with the install of each model. I used my tablet to watch the installation videos because I like using my tablet when I will be watching videos and I know I won't be clicking through multiple screens. It's nicer to just sit down and watch videos that way.”</p>

<p style="margin-left: 40px;">Tablet designs should take advantage of the larger  mobile touch screen. Emphasize media content and create immersive experiences around common activities.</p>

<p style="margin-left: 40px;">Allrecipes.com’s iPad application is optimized to facilitate meal preparation in the kitchen. The large fonts can be read from far away, and the page layout provides easy access to ingredients and directions, with very little need for scrolling and tapping by users who may have wet or messy hands. The large I’m Done button is touch-friendly; the page devotes a large amount of space to a video demonstrating how the dish is prepared. This information exists on other mobile and desktop channels as well, but the immersive presentation and emphasis on easy consumption is unique to the iPad application.</p>

<figure class="caption"><img alt="Allrecipes iPad" height="540" src="https://media.nngroup.com/media/editor/2017/02/01/allrecpies-ipad-app.png" width="720"/>
<figcaption><em>AllRecipes.com’ iPad application is designed to assist users during meal preparation; it is optimized for the tablet strengths and context of use.</em></figcaption>
</figure>

<h2>Optimization vs. Consistency</h2>

<p>Optimizing for one device or channel often means designing an experience that is uniquely tailored to that channel. To do that, it may mean to occasionally forego crosschannel consistency — at least to some extent.</p>

<p>Consistency is one of the most important components in a successful omnichannel experience. Experiences across various channels should have a consistent look and feel and consistent core functionality in order to create a cohesive, learnable, efficient experience across all channels. However, consistency does not mean that we should provide the exact same experience on all channels, because we also strive for an experience that is optimized for the device, screen size and the context of use. The goal is to provide core consistency, but make compromises to optimize the experience where it is appropriate.</p>

<p>Although Jiffy Lube’s mobile website emphasized location-based content on the mobile device, it still offered a consistent experience by providing a clear path in the navigation to the same vehicle-service information that was provided on the desktop. The experience was consistent at its core, but it emphasized mobile tasks.</p>

<p>Geico, an insurance company, is another example of a good compromise between consistency and optimization for context of use and device strengths.</p>

<p>Geico’s desktop website and mobile app exhibited the same visual design and brand identity, and both experiences provided access to core content and features such as policy and billing information. However, this content was presented differently on different devices, using layouts that were optimized for the size of the screen.  The desktop experience also supported more complex or specialized user tasks such as managing the policy, getting proof of insurance, and printing a policy-cancellation letter.  These type of tasks are better suited for the desktop strengths (e.g., larger screen, access to a printer, easier input).</p>

<p>Geico’s mobile-application experience was optimized for the mobile-device capabilities and the mobile users’ needs. For example, the app allowed fingerprint authentication with iOS Touch ID and  emphasized information and tools that may be needed while traveling (e.g.,   roadside assistance, glass damage, claims). Some of the app features were not available on the desktop:  getting a ride in case of a roadside breakdown, or finding gas stations nearby. Another mobile feature, Your Wallet, was specifically designed for customers who need quick access to important documents and vehicle information.</p>

<figure class="caption"><img alt="Geico Desktop" height="1554" src="https://media.nngroup.com/media/editor/2017/02/06/geicodt.png" width="750"/>
<figcaption><em>Image 1 of 2: Geico’s desktop experience gave users access to a combination of core features and complex features likely to be accessed on  a desktop.</em></figcaption>
</figure>

<figure class="caption"><img alt="Geico Mobile app" height="619" src="https://media.nngroup.com/media/editor/2017/02/06/geicomobile.png" width="750"/>
<figcaption><em>Image 2 of 2. The Geico iPhone app had the same visual look-and-feel as the desktop site and supported the core features in a layout appropriate for the small mobile screen. Additional features were provided for  users traveling or away from home</em>.</figcaption>
</figure>

<h2>Optimized for Context: #2 of 5 Recommended Omnichannel Components</h2>

<p>In a world where customers use multiple channels and devices to interact with organizations, understanding each device’s and channel’s role in a customer’s journey can help organizations incorporate helpful and usable context-specific elements necessary to create an exceptional user experience.</p>

<p>In order to focus design efforts, teams must understand the common tasks completed by users within each channel. Research how customers use all of the channels you support to uncover contextual details unique to your organization and identify opportunities for creating channel-optimized experiences that will build user loyalty and differentiate your organization from competitors.</p>

<p>In addition to being optimized for the channel, successful omnichannel experiences are <a href="../omnichannel-consistency/index.php">consistent</a>, <a href="../seamless-cross-channel/index.php">seamless</a>, orchestrated and collaborative. Our full day course on <a href="../../courses/cross-channel-user-experience/index.php">Omnichannel Journeys and Customer Experience</a> covers these recommended characteristics further.</p>

<h2>Related Video</h2>

<p>Watch Kim Flaherty explain the key points of omnichannel user experience (2 min. video):</p>

<div style="width:350px;">
<div class="flex-video"><iframe allowfullscreen="" frameborder="0" scrolling="no" src="https://www.youtube.com/embed/mhmu0XUaTuM?rel=0"></iframe></div>
</div>
</section>
  

  
  <section>
    
        
    
  </section>
  
  
  
  
  

  


  <section>
    <br />
    <p class="share"><b>Share <span class="hide-for-small-down">this article</span>:</b> <a href="https://twitter.com/intent/tweet?url=http://www.nngroup.com/articles/context-specific-cross-channel/&amp;text=Optimizing%20for%20Context%20in%20the%20Omnichannel%20User%20Experience&amp;via=nngroup">Twitter</a> | <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=http://www.nngroup.com/articles/context-specific-cross-channel/&amp;title=Optimizing%20for%20Context%20in%20the%20Omnichannel%20User%20Experience&amp;source=Nielsen%20Norman%20Group">LinkedIn</a> | <a href="https://plus.google.com/share?url=http://www.nngroup.com/articles/context-specific-cross-channel/">Google+</a> | <a href="mailto:?subject=NN/g Article: Optimizing for Context in the Omnichannel User Experience&amp;body=http://www.nngroup.com/articles/context-specific-cross-channel/">Email</a></p>
    <hr />
  </section>

  <footer class="l-subsection">
    
    
    <div class="learn-more">
      <h2>Learn More</h2>
      <p><a href="../subscribe/index.php">Subscribe to the weekly newsletter</a> to get notified about future articles.</p>
      </div>
    
    

    <div class="row show-for-small-down related-content">
      
      
      

      
      
        <div class="column small-12 topic-links">All articles about:
          <ul>
            
            <li><a href="../../topic/customer-journeys/index.php">Customer Journeys</a></li>
            
            <li><a href="../../topic/e-commerce/index.php">E-commerce</a></li>
            
            <li><a href="../../topic/mobile-and-tablet-design/index.php">Mobile &amp; Tablet</a></li>
            
            <li><a href="../../topic/omnichannel/index.php">omnichannel</a></li>
            
            <li><a href="../../topic/strategy/index.php">Strategy</a></li>
            
          </ul>
        </div>
      
      

      
      
        <div class="column small-12">
          <h3>UX Conference Training Courses</h3>
           <ul class="no-bullet">
    
        <li><a href="../../courses/ux-basic-training/index.php">UX Basic Training</a></li>
    
        <li><a href="../../courses/journey-mapping/index.php">Journey Mapping to Understand Customer Needs</a></li>
    
        <li><a href="../../courses/facilitating-ux-workshops/index.php">Facilitating UX Workshops</a></li>
    
        <li><a href="../../courses/analytics-and-user-experience/index.php">Analytics and User Experience</a></li>
    
        <li><a href="../../courses/generating-big-ideas/index.php">Generating Big Ideas with Design Thinking</a></li>
    
</ul>
        </div>
      
      

      
      
        <div class="small-12 column">
          <h3>Research Reports</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../reports/tablets/index.php">Tablet Website and Application UX</a></li>
              
            
              
                <li><a href="../../reports/ecommerce-user-experience/index.php">E-Commerce User Experience</a></li>
              
            
              
                <li><a href="../../reports/mobile-website-and-application-usability/index.php">User Experience for Mobile Applications and Websites</a></li>
              
            
              
                <li><a href="../../reports/usability-return-on-investment-roi/index.php">Return on Investment (ROI) for Usability</a></li>
              
            
              
                <li><a href="../../reports/b2b-websites-usability/index.php">B2B Website Usability for Converting Users into Leads and Customers</a></li>
              
            
          </ul>
        </div>
      
      

      
      
        <div class="small-12 column">
          <h3>Online Seminars</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../online-seminars/ux-real-world/index.php">UX in the Real World</a></li>
              
            
              
                <li><a href="../../online-seminars/every-word-count/index.php">Making Every Word Count</a></li>
              
            
              
                <li><a href="../../online-seminars/how-change-your-corporate-culture/index.php">How to Change Your Corporate Culture to be User First</a></li>
              
            
              
                <li><a href="../../online-seminars/mobile-user-testing/index.php">Mobile User Testing</a></li>
              
            
          </ul>
        </div>
      
      
    </div>

    <div class="hide-for-small-down row related-content">
      <div class="medium-6 columns">
        
        
          <div>
            <h3>Articles</h3>
            <ul class="no-bullet">
              
                
                <li><a href="../channels-devices-touchpoints/index.php">How Channels, Devices, and Touchpoints Impact the Customer Journey</a></li>
                
              
                
                <li><a href="../journey-mapping-ux-practitioners/index.php">Journey Mapping in Real Life: A Survey of UX Practitioners</a></li>
                
              
                
                <li><a href="../3-user-experiences-reshaping-industries/index.php">Beyond Usability: 3 User Experiences Reshaping Their Industries </a></li>
                
              
                
                <li><a href="../omnichannel-consistency/index.php">Consistency in the Omnichannel Experience</a></li>
                
              
                
                <li><a href="../customer-service-omnichannel-ux/index.php">Minimize the Need for Customer Service to Improve the Omnichannel UX</a></li>
                
              
            </ul>
          </div>
        
        

        
          
          <div>
            <h3>Research Reports</h3>
            <ul class="no-bullet">
              
                
                  <li><a href="../../reports/tablets/index.php">Tablet Website and Application UX</a></li>
                
              
                
                  <li><a href="../../reports/ecommerce-user-experience/index.php">E-Commerce User Experience</a></li>
                
              
                
                  <li><a href="../../reports/mobile-website-and-application-usability/index.php">User Experience for Mobile Applications and Websites</a></li>
                
              
                
                  <li><a href="../../reports/usability-return-on-investment-roi/index.php">Return on Investment (ROI) for Usability</a></li>
                
              
                
                  <li><a href="../../reports/b2b-websites-usability/index.php">B2B Website Usability for Converting Users into Leads and Customers</a></li>
                
              
            </ul>
          </div>
          
        

      </div>
      <div class="medium-6 columns">
        
          
          <div>
            <h3>UX Conference Training Courses</h3>
             <ul class="no-bullet">
    
        <li><a href="../../courses/ux-basic-training/index.php">UX Basic Training</a></li>
    
        <li><a href="../../courses/journey-mapping/index.php">Journey Mapping to Understand Customer Needs</a></li>
    
        <li><a href="../../courses/facilitating-ux-workshops/index.php">Facilitating UX Workshops</a></li>
    
        <li><a href="../../courses/analytics-and-user-experience/index.php">Analytics and User Experience</a></li>
    
        <li><a href="../../courses/generating-big-ideas/index.php">Generating Big Ideas with Design Thinking</a></li>
    
</ul>
          </div>
          
        
        
          
          <div>
            <h3>Online Seminars</h3>
            <ul class="no-bullet">
            
                <li><a href="../../online-seminars/ux-real-world/index.php">UX in the Real World</a></li>
            
                <li><a href="../../online-seminars/every-word-count/index.php">Making Every Word Count</a></li>
            
                <li><a href="../../online-seminars/how-change-your-corporate-culture/index.php">How to Change Your Corporate Culture to be User First</a></li>
            
                <li><a href="../../online-seminars/mobile-user-testing/index.php">Mobile User Testing</a></li>
            
            </ul>
          </div>
          
        
      </div>
    </div>
  </footer>
</article>

        </div>

      

    
  </div>
  <div class="row">
    
      <div class="column small-12 show-for-small-down l-navrail announcements">
        
          
        
      </div>
    
    
  </div>

    </div>
    




<footer id="page-footer">
    <div class="row hide-for-print">
        <div class="column small-12 medium-6">
            <section class="footer-popular">
                
                <h1>Popular Topics</h1>
                <div style="float: left;  width: 15em; display: block;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/applications/index.php">Application Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/e-commerce/index.php">E-Commerce Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/eyetracking/index.php">Eyetracking </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/information-architecture/index.php">Information Architecture </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/intranets/index.php">Intranet Design </a></li>
	<li><a href="../../topic/interaction-design/index.php">Interaction Design </a></li>
</ul>
</div>

<div style="float: left;  width: 15em; display: block; list-style: none;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/mobile-and-tablet-design/index.php">Mobile and Tablet Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/credibility-persuasion/index.php">Persuasive Design</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/research-methods/index.php">Research Methods</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/user-testing/index.php">User Testing </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/web-usability/index.php">Web Usability </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/writing-web/index.php">Web Writing &amp; Content Strategy </a></li>
</ul>
</div>

<p>&nbsp;</p>

                
            </section>
        </div>

        <div class="column small-12 medium-6">
            <div class="row">
                <div class="column small-6">
                    <section class="footer-about">
                        <h1>Follow Us</h1>
                        <ul>
	<li><a href="../subscribe/index.php">Subscribe to our weekly UX newsletter</a>,&nbsp;Jakob Nielsen&#39;s Alertbox</li>
	<li>Visit Don Norman&#39;s website, <a href="http://www.jnd.org/"> www.jnd.org </a></li>
	<li>Visit Bruce Tognazzini&#39;s website, <a href="http://www.asktog.com/"> www.asktog.com </a></li>
	<li style="padding-top: 6px;"><a class="twitter-follow-button" data-show-count="false" data-size="large" href="https://twitter.com/nngroup"><img alt="Follow @nngroup on Twitter" height="20" src="https://media.nngroup.com/media/editor/2016/12/30/twitter-follow-button.png" style="width: 117px; height: 20px;" width="117" /></a></li>
	<li style="padding-top: 6px;"><img alt="" height="14" src="https://media.nngroup.com/media/editor/2016/07/26/feed-icon-14x14.png" style="vertical-align:text-top;width:14px;margin-right:.25em;" width="14" /><a href="../../feed/rss/index.php">Subscribe to our RSS feed</a></li>
</ul>

                    </section>
                </div>

                <div class="column small-6">
                    <section class="footer-about right-column">
                        <h1>About
</h1>
                        <ul class="no-bullet">
	<li><a href="../../about/contact/index.php">Contact information</a></li>
	<li><a href="../../about/index.php">About Us</a></li>
	<li><a href="../../about/why-nng/index.php">Why NN/g? </a></li>
	<li><a href="../../return-policy/index.php">Return Policy</a></li>
</ul>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="copyright column small-12">
            <p><a href="../../copyright-and-reprint-info/index.php">Copyright</a> &copy; 1998-2017 Nielsen Norman Group, All Rights Reserved.</p>
        </div>
    </div>
</footer>


    


        <script src="https://media.nngroup.com/static/vendor/fastclick/js/fastclick.min.js"></script>
        <script src="https://media.nngroup.com/static/vendor/foundation/js/foundation.min.js"></script>
        <script src="http://media.nngroup.com/static/js/app.js"></script>
        <script src="http://media.nngroup.com/static/js/scroll-to-fixed.js"></script><script>var externalLinks = document.querySelectorAll('a[href^="http"]');for (var i = externalLinks.length-1; i >= 0; i--) {externalLinks[i].addEventListener("click", function(event) { alert("External links are disabled for this test."); event.preventDefault(); }, false);}var mailLinks = document.querySelectorAll('a[href^="mailto"]');for (var i = mailLinks.length-1; i >= 0; i--) {mailLinks[i].addEventListener("click", function(event) { alert("Email links are disabled for this test."); event.preventDefault(); }, false);}var externalForms = document.querySelectorAll('form');for (var i = externalForms.length-1; i >= 0; i--) {externalForms[i].addEventListener("submit", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false); externalForms[i].addEventListener("click", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false);}</script>

        <script>
            
            $(function() {
                $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
                $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
            });
        </script>

        
<script src="https://media.nngroup.com/static/js/articles.js"></script>


        
        <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1072699960;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072699960/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
    </body>

<!-- Mirrored from www.nngroup.com/articles/context-specific-cross-channel/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:01:27 GMT -->
</html>
