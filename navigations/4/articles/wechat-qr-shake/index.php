<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->





    
<!-- Mirrored from www.nngroup.com/articles/wechat-qr-shake/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:04:02 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1026.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"beacon":"bam.nr-data.net","queueTime":2,"applicationTime":413,"agent":"","licenseKey":"1a30ef9931","errorBeacon":"bam.nr-data.net","applicationID":"2048992","transactionName":"NFxSMRBUX0EDUkIPDA0WdhAMVkVbDV8ZBxEXUFMJB0YfRAtUQRVZIktEDAFZVHYHRVcPD01eVRE="}</script>
        <title>Scan and Shake: A Lesson in Technology Adoption from China’s WeChat</title><meta property="og:title" content="Scan and Shake: A Lesson in Technology Adoption from China’s WeChat" />
  
        
        <meta name="description" content="QR-code scanning and shake serve as effective ways of communication between the online and the offline worlds and enjoy wider use in China than in the US.">
        <meta property="og:description" content="QR-code scanning and shake serve as effective ways of communication between the online and the offline worlds and enjoy wider use in China than in the US." />
        
  
        
	
        
        <meta name="keywords" content="QR code, UX, innovation, user interface, mobile usability, shake gesture">
        

        

        <meta name="robots" content="noarchive">
        <meta name="viewport" content="width=device-width">

        <link rel="search" type="application/opensearchdescription+xml" title="NN/g Search" href="../../opensearch.xml" />
        <link rel="shortcut icon" href="http://media.nngroup.com/static/img/favicon.ico" />

        <link rel="stylesheet" href="https://media.nngroup.com/static/vendor/font-awesome/css/font-awesome.min.css">

        
        <link rel="stylesheet" href="https://media.nngroup.com/static/css/7232a960f1df.css" type="text/css" />
        <link rel="stylesheet" href="http://fast.fonts.com/t/1.css?apiType=css&amp;projectid=c5846260-9def-46ec-9407-0f1349bd2d64" type="text/css">

        <script src="https://media.nngroup.com/static/vendor/modernizr/js/modernizr.min.js"></script>

        <script type="text/javascript" src="https://media.nngroup.com/static/js/3283be5def74.js"></script>
        

        

        
        
        <!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=229034,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('http://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->


<!-- Google Analytics Tracking Code for http://nngroup.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23132681-1', 'nngroup.com');
  ga('require', 'linkid', 'https://www.nngroup.com/articles/wechat-qr-shake/linkid.js');
  ga('require', 'displayfeatures');

// Optimizely Universal Analytics Integration
 window.optimizely = window.optimizely || [];
 window.optimizely.push("activateUniversalAnalytics");

  ga('send', 'pageview');

</script>
<script>
/**
* Function to track a click on an outbound link in Google Analytics.
*/
function trackOutboundLink(label, url) {
    if(window.ga && ga.loaded) {
        ga('send', 'event', 'outbound', 'click', label, {'hitCallback': function(){document.location = url;}
        });
    } else {
        document.location = url;
    }
}
/**
* Function to track a click on a link in the article announcement area.
*/
$('#articleAnnouncement a').on('click', function(){
  ga('send', 'event', 'link', 'click', 'articleAnnouncement');
});
</script>

<script type="text/javascript">
 setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2421.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



<!-- Hotjar Tracking Code for http://nngroup.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:121605,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

        <script src="https://media.nngroup.com/static/js/analytics/track_outbound_link.js"></script>
        
        
        
    </head>
    <body class="location-articles articles-detail">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
    <header id="page-header">
        <div id="cart" class="shopping-cart mobile-cart show-for-small-down"></div>
        <div class="row">
            <div id="masthead" class="masthead small-12 medium-7 column">

                <!-- The button to toggles menu on small screen -->

                <h1><a href="../../index.php"><img src="http://media.nngroup.com/static/img/logo-wordmark.png" width="74" height="30"> Nielsen <span class="norman">Norman</span> Group</a></h1>
                <p class="tagline show-for-medium-up">Evidence-Based User Experience Research, Training, and Consulting</p>

                <!-- The button to toggles form search on small screen -->


                <div style="clear: right;width:  100%;height: 1px;"></div><?php define('ROOT_PATH',$_SERVER["DOCUMENT_ROOT"] . '/apps/navigation_styles/navigations/4'); ?><?php include(ROOT_PATH)."/navigation.php"; ?>

                <!-- Drop down menu on small screen -->
                <div id="hideNav" class="show-for-small-down">
                  
                  <ul class="menu-content">
                    <li><a href="../../index.php">Home</a></li>
                    <li class="has-submenu">
                      <a>Training</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../training/index.php">Overview</a></li>
                        
<li id="nav-ux-conference"><a href="../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../online-seminars/index.php"><span>Online Seminars</span></a></li>

                      </ul>
                    </li>
                    <li class="has-submenu">
                      <a>Consulting</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
  <li><a href="../../consulting/index.php">Overview</a></li>
  
  <li><a href="../../consulting/benchmarking/index.php">Benchmarking</a></li>
  
  <li><a href="../../consulting/expert-design-review/index.php">Expert Design Review</a></li>
  
  <li><a href="../../consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
  
  <li><a href="../../consulting/usability-testing/index.php">Usability Testing </a></li>
  
  <li><a href="../../consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
  
  <li><a href="../../in-house-training/index.php">In-House Training</a></li>
</ul>

                    </li>

                    <li><a href="../../reports/index.php">Reports</a></li>
                    <li><a href="../index.php">Articles</a></li>
                    <li class="has-submenu"><a>About NN/G</a><i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../about/index.php">Overview</a></li>
                        <li><a href="../../people/index.php">People</a></li>
                        <li><a href="../../about/why-nng/index.php">Why NN/g?</a></li>
                        <li><a href="../../about/contact/index.php">Contact</a></li>
                        <li><a href="../../news/index.php">News</a></li>
                        <li><a href="../../about/history/index.php">History</a></li>
                        <li><a href="../../books/index.php">Books</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                </div>

                <hr class="header-divider show-for-small-only"/>


            </div>
       </div>

        <div id="hideNav">            <nav class="primary-nav small-12 column">
                <ul>
                    <li><a class="navitem" id="nav-home" href="../../index.php">Home</a></li>
                    <li><a class="navitem" id="nav-training" href="../../training/index.php">Training</a></li>
                    <li><a class="navitem" id="nav-consulting" href="../../consulting/index.php">Consulting</a></li>
                    <li><a class="navitem" id="nav-reports" href="../../reports/index.php">Reports</a></li>
                    <li><a class="navitem" id="nav-articles" href="../index.php">Articles</a></li>
                    <li><a class="navitem" id="nav-about" href="../../about/index.php">About NN/G</a></li>
                </ul>
            </nav>
            <nav class="secondary-nav small-12 medium-12 large-12 columns">
                
                
            </nav>
        </div>
    </header>
    <div class="l-content">
    
  

  

  <div class="row">
    
      
      <section class="small-12 medium-3 columns collapsable-content l-navrail articles-left-column">
        <h1 class="show-for-small-down collapse-link mobile-browse-header">Browse by Topic and Author</h1>
          <div class="row collapsable">

            
            <div class="small-6 medium-12 columns">
              <section id="article-popular-topics" style="display: none;">
                
                <h1>Topics</h1>
                <ul id="article-popular-topics">
                    
                    <li>
                        
                        <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/intranets/all/index.php">Intranets</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/user-testing/all/index.php">User Testing</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                        
                    </li>
                    
                    <li><a href="#" id="article-see-all-topics"><strong>See all topics&hellip;</strong></a></li>
                </ul>
                
              </section>
              <section id="article-all-topics" style="display: none;">
                
                <h1>
                  <span class="show-for-medium-up">All Article Topics</span>
                  <span class="show-for-small-down">All Topics</span>
                  (<a href="#" id="article-hide-all-topics">hide</a>)
                </h1>
                <ul>
                  
                  <li>
                    
                    <a href="../../topic/accessibility/all/index.php">Accessibility</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/agile/all/index.php">Agile</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/analytics-and-metrics/all/index.php">Analytics &amp; Metrics</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/applications/all/index.php">Application Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/b2b-websites/all/index.php">B2B Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/behavior-patterns/all/index.php">Behavior Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/branding/all/index.php">Branding</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/content-strategy/all/index.php">Content Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/corporate-websites/all/index.php">Corporate Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/design-patterns/all/index.php">Design Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-design-process/all/index.php">Design Process</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/email/all/index.php">Email</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/eyetracking/all/index.php">Eyetracking</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/heuristic-evaluation/all/index.php">Heuristic Evaluation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/human-computer-interaction/all/index.php">Human Computer Interaction</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-ideation/all/index.php">Ideation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/information-architecture/all/index.php">Information Architecture</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/interaction-design/all/index.php">Interaction Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/international-users/all/index.php">International Users</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/intranets/all/index.php">Intranets</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-management/all/index.php">Management</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/navigation/all/index.php">Navigation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/non-profit-websites/all/index.php">Non-Profit Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/personas/all/index.php">Personas</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/persuasive-design/all/index.php">Persuasive Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/prototyping/all/index.php">Prototyping</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/psychology-and-ux/all/index.php">Psychology and UX</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/research-methods/all/index.php">Research Methods</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/search/all/index.php">Search</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/social-media/all/index.php">Social Media</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/strategy/all/index.php">Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/user-testing/all/index.php">User Testing</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-teams/all/index.php">UX Teams</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/visual-design/all/index.php">Visual Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/writing-web/all/index.php">Writing for the Web</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/young-users/all/index.php">Young Users</a>
                    
                  </li>
                  
                </ul>
                
              </section>
            </div>
            <script>
              /* Determine which topics list will be displayed: the full or the
               * partial.  We are inlining this close to the list itself to
               * prevent rendering delays on the client
               */
              var topic_list = $.cookie("article-topics");

              var popular_topics = $('#article-popular-topics');
              var all_topics = $('#article-all-topics');

              if(topic_list == 1) {
                  popular_topics.hide();
                  all_topics.show();
              } else {
                  popular_topics.show();
                  all_topics.hide();
              }
            </script>
            

            
            <div class="small-6 medium-12 columns">
              
              <section id="article-initial-authors">
                  <h1>Author</h1>
                  <ul>
                    
                    <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                    
                    <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                    
                    <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                    
                    <li><a href="#" onClick="$('#article-initial-authors').hide(); $('#article-all-authors').show();"><strong>See all authors&hellip;</strong></a></li>
                  </ul>
              </section>
              <section id="article-all-authors" style="display: none;">
                <h1>
                  All Authors
                  (<a href="#" id="article-hide-all-authors">hide</a>)
                </h1>
                <ul>
                  
                  <li><a href="../author/raluca-budiu/index.php">Raluca Budiu</a></li>
                  
                  <li><a href="../author/patty-caya/index.php">Patty Caya</a></li>
                  
                  <li><a href="../author/susan-farrell/index.php">Susan Farrell</a></li>
                  
                  <li><a href="../author/therese-fessenden/index.php">Therese Fessenden</a></li>
                  
                  <li><a href="../author/kim-flaherty/index.php">Kim Flaherty</a></li>
                  
                  <li><a href="../author/sarah-gibbons/index.php">Sarah Gibbons</a></li>
                  
                  <li><a href="../author/aurora-harley/index.php">Aurora Harley</a></li>
                  
                  <li><a href="../author/kate-kaplan/index.php">Kate Kaplan</a></li>
                  
                  <li><a href="../author/page-laubheimer/index.php">Page Laubheimer</a></li>
                  
                  <li><a href="../author/angela-li/index.php">Angie Li</a></li>
                  
                  <li><a href="../author/hoa-loranger/index.php">Hoa Loranger</a></li>
                  
                  <li><a href="../author/kate-meyer/index.php">Kate Meyer</a></li>
                  
                  <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                  
                  <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                  
                  <li><a href="../author/kara-pernice/index.php">Kara Pernice</a></li>
                  
                  <li><a href="../author/christian-rohrer/index.php">Christian Rohrer</a></li>
                  
                  <li><a href="../author/amy-schade/index.php">Amy Schade</a></li>
                  
                  <li><a href="../author/katie-sherwin/index.php">Katie Sherwin</a></li>
                  
                  <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                  
                  <li><a href="../author/kathryn-whitenton/index.php">Kathryn Whitenton</a></li>
                  
                </ul>
              </section>
              
            </div>
            
          </div>

          
          <section class="hide-for-small-down">
            
              
              <h1>Recent Articles</h1>
              
              <ul>
                  
                  <li><a href="../seamless-cross-channel/index.php">Seamlessness in the Omnichannel User Experience</a></li>
                  
                  <li><a href="../brand-intention-interpretation/index.php">Experience Design: Bridging Brand Intention and Brand Interpretation</a></li>
                  
                  <li><a href="../eyetracking-tasks-efficient-scanning/index.php">Scanning Patterns on the Web Are Optimized for the Current Task</a></li>
                  
                  <li><a href="../flat-design-best-practices/index.php">Flat-Design Best Practices</a></li>
                  
                  <li><a href="../ux-user-stories/index.php">Accounting for UX Work with User Stories in Agile Projects</a></li>
                  
                  <li><a href="../index.php"><strong>See all articles&hellip;</strong></a></li>
              </ul>
              
            
          </section>
          

          
          
          <section class="hide-for-small-down">
            <h1>Popular Articles</h1>
            
            <ul>
                
                <li><a href="../usability-101-introduction-to-usability/index.php">Usability 101: Introduction to Usability</a></li>
                
                <li><a href="../top-10-mistakes-web-design/index.php">Top 10 Mistakes in Web Design</a></li>
                
                <li><a href="../how-users-read-on-the-web/index.php">How Users Read on the Web</a></li>
                
                <li><a href="../f-shaped-pattern-reading-web-content/index.php">F-Shaped Pattern For Reading Web Content</a></li>
                
                <li><a href="../ten-usability-heuristics/index.php">10 Usability Heuristics for User Interface Design</a></li>
                
                <li><a href="../intranet-design-2016/index.php">10 Best Intranets of 2016</a></li>
                
                <li><a href="../which-ux-research-methods/index.php">When to Use Which User-Experience Research Methods</a></li>
                
                <li><a href="../response-times-3-important-limits/index.php">Response Times: The 3 Important Limits</a></li>
                
                <li><a href="../why-you-only-need-to-test-with-5-users/index.php">Why You Only Need to Test with 5 Users</a></li>
                
                <li><a href="../page-fold-manifesto/index.php">The Fold Manifesto: Why the Page Fold Still Matters</a></li>
                
            </ul>
          </section>
          
          

          
            <section class="hide-for-small-down">
              
            </section>
          

      </section>
      

      
        <div class="small-12 medium-9 columns content-body">
          




<article class="l-subsection alertbox-article-content article-detail-bl">
  
  <header>
    <div class="l-subsection">
      <h1>Scan and Shake: A Lesson in Technology Adoption from China’s WeChat</h1>
      <div class="author-meta">by
        
          
            
            <a href="../author/yuno-cheng/index.php">Yunnuo Cheng</a>
            
            and
            
            <a href="../author/raluca-budiu/index.php">Raluca Budiu</a>
            
          
        on  October 16, 2016
        
      </div>

      

      
      <div class="topic-links">
        Topics:
        
          <ul>

  <li><a href="../../topic/mobile-and-tablet-design/index.php">Mobile &amp; Tablet</a></li>

  <li><a href="../../topic/international-users/index.php">International Users</a></li>

</ul>
        
      </div>
      
    </div>
  </header>
  

  
  
  <section>
    <p class="lede"><strong>Summary:</strong> QR-code scanning and shake serve as effective ways of communication between the online and the offline worlds and enjoy wider use in China than in the US.</p>
    <br />
  </section>
  
  

  
  <section id="article-body"><p>A few years back, QR codes, 2-dimensional “quick response” barcodes traditionally made up of black squares placed on a white square grid, were touted as the next big thing: you could use them to quickly scan a URL in a magazine or on a poster and read more information about the product. However, they never caught on and they have pretty much fallen into oblivion. If I asked you to scan a QR code on your phone today, chances are that you wouldn’t be able to.</p>

<figure class="caption"><img alt="" height="407" src="https://media.nngroup.com/media/editor/2016/10/16/qr-scan-china.jpg" style="width: 400px; height: 307px;" width="530"/>
<figcaption><em>A QR code for the Chinese company Juewei advertising its snacks on a subway car. The QR code takes users to a page in WeChat where they can purchase the products.</em></figcaption>
</figure>

<p>That is, in the US. In China QR codes are ubiquitous, partly due to the highly popular WeChat service, which had 700 million users in April 2016. In our <a href="../wechat-integrated-ux/index.php">diary study conducted in China with WeChat users</a>, many participants mentioned this feature and assigned a central role to it in their mental model of WeChat. Our study participants also reported using the shake gesture in WeChat to interact with various TV shows and businesses, or to get more information about a song.</p>

<p>Why are these interactions widespread in China, yet they never caught on in the US? And what is the benefit that they provide to UX?</p>

<h2>Adoption</h2>

<p>How do you make an interaction technique adopted by a wide variety of people? Three conditions need to be satisfied:</p>

<ol>
	<li><strong>Usefulness</strong>: Foremost, the technique has to provide some sort of <strong>benefit</strong>. Nobody cares about useless innovations.</li>
	<li><strong>Ease</strong> <strong>of use</strong>: The technique has to be easy to use — that is, it should be learnable and have low <a href="../interaction-cost-definition/index.php">interaction cost</a> once it’s been learned. If people have to jump many hurdles to take advantage of it, then most will give up.</li>
	<li><strong>Discoverability</strong>: Last but not least, the technology has to be <a href="../navigation-ia-tests/index.php">discoverable</a> by a wide population — there are many useful, usable inventions that have fallen under oblivion simply because the word hasn’t spread fast enough.</li>
</ol>

<p>QR codes and the shake gesture have met all these three criteria in China, but not in the US. In the US, to scan a QR code encountered on the street, in a store, or in a magazine, you’d need a QR-code scanner — that is, you’d need to install a special application dedicated to this operation. Just taking a picture of the code with the phone’s camera won’t work. Thus, QR codes fall the ease-of-use test in the US: to use them, people will have to do some initial work to find an app and download it, and, later on, they will have to remember what this app is to use it for more scanning.</p>

<p>In contrast, in China, WeChat’s mobile application has had an embedded QR-code scanner since 2012, so the millions of WeChat users don’t need to install any special app to have access to this functionality — they can simply take advantage of the preinstalled WeChat.</p>

<p>But that is not enough — WeChat has many other features that are hardly discoverable and rarely used by its customers. QR-code scanning was, however, quickly adopted in WeChat because, early on, it was associated with two popular features, both still widely used today:</p>

<ul>
	<li>Adding a new contact: People could become WeChat contacts by scanning each other’s codes. Because WeChat did not support searching for people by name, scanning a personal code saved both input effort and user memory, as users did not need to type anything, nor did they need to remember (or get access to) a user ID or a phone number. An added benefit was that two persons could become WeChat contacts without knowing each other’s phone numbers.</li>
</ul>

<figure class="caption"><img alt="" src="https://media.nngroup.com/media/editor/2016/10/07/wechat-code.png" style="border-width: 0px; border-style: solid; width: 350px; height: 623px;"/>
<figcaption><em>People can scan someone else’s QR code to add that person to their contact lists.</em></figcaption>
</figure>

<ul>
	<li>Group chat: Creating a group meant generating an associated QR code, and joining it was simply a matter of scanning the code. This new feature simplified a fairly complex process; because it provided a substantial ease-of-use benefit over the status quo, it made users willing to try it. Here’s a quote from one of our diary-study participants “<em>I needed to start a WeChat group for work, so I created a group and added the people who are already my friends on WeChat. Then I emailed the [group] QR code to other people who should be included. I got more people joining the group in the afternoon.”</em></li>
</ul>

<p>While users adopted the QR codes in WeChat because they shortened complicated processes, the adoption of the shake gesture took a different path. The shake gesture was available early on in WeChat for connecting with others who were shaking the phone at the same time, and a later plugin supported photo transfer from desktop to phone; however, these uses were not that common. What made WeChat’s shake gesture popular and relatively widespread was a <strong>tangible financial motivation.</strong> Users shaking the phones at specified moments during the most popular TV show of the year, New Year’s Spring Festival Gala 2014, were offered red envelopes (traditional monetary gifts). As a result of this promotion, the company saw hundreds of millions of shakes per minute and it’s probably safe to say that all Chinese smartphone owners had at least heard of the gesture, if not directly tried it. This campaign made the shake gesture well known to a large number of users, and afterwards Tencent could take advantage of its familiarity in other contexts and for other functionalities.</p>

<p>Last but not least, WeChat already enjoyed a wide audience when the QR-code scanning and the shake gesture became popular — many millions of users had already installed its app. Had its audience been smaller, it’s less likely that QR code scanning or shaking will have entered the public conscience so quickly.</p>

<p>In contrast, none of the big mobile players in the US have made any of their central functionality reliant on QR-code reading or on shaking; in fact neither iOS nor Android support QR code scanning in the proprietary apps (such as Camera) that get shipped with the phone. Shake is used in applications such as Mail for iOS to implement undo or Google Maps to send feedback, but many users never discover it, since it is not part of the core functionality of these apps. The other route for increasing motivation — that of offering some direct compensation (the equivalent of red envelopes in China) to those users who perform the gesture — is perhaps less culturally realistic or appealing for American users.</p>

<h2>Mediation Between Online and Offline Worlds</h2>

<p>In our Chinese diary study we found that, while people did use shake and QR codes fairly regularly, the principal function of these interactions was, however, not earning money or responding to advertisements; instead, the users’ goal was communication between the online and offline worlds: <a href="../customer-journeys-omnichannel/index.php">omnichannel UX</a> made simple. (The term “O2O” — “online to offline” — is often used in the Chinese technology press.)</p>

<p>QR codes act as a <strong>convenient offline index into the online world</strong>: taking a picture of a QR code could expediently take users to the right webpage without having to type in any query (something that’s particularly <a href="../mobile-ux/index.php">arduous on mobile devices</a>). You could for instance scan a company’s QR code to subscribe to its newsletter or to access its WeChat official-account page and interact with it. As one user put it, <em>“I used an app which recommended their company’s official account. I don’t want to remember the name of the official account and search – there might be a lot of fake ones. So I just made a screenshot of their QR code and scanned it in WeChat.”</em> Or, if you’ve just seen a movie and you want your WeChat friends to know about it, you could simply scan the QR code present on the movie poster, access the page corresponding to that code, and then share it in WeChat. It’s easier than typing the movie name (or a movie URL) in WeChat, and it offers more information to the readers.</p>

<p>Not only can QR codes provide quick access to websites and a replacement for URL typing, but they can also <strong>add information to physical objects</strong>. Many physical objects bear QR codes, and people can easily scan them to augment their interaction with these objects. One study participant reported, <em>“I got a gift from a friend and didn’t know how to use it. So I just scanned the QR code to read the instructions (and awkwardly also saw the price).</em>”</p>

<p>There are countless examples of physical objects enriched by a QR code — they range from eggs in the refrigerator, to classified ads on a physical bulletin board, to postcards that embed pictures and audio on a piece of paper by simply affixing a QR code to it.</p>

<figure class="caption"><img alt="" src="https://media.nngroup.com/media/editor/2016/10/07/claw.PNG" style="border-width: 0px; border-style: solid; width: 350px; height: 557px;"/>
<figcaption><em>Scanning the QR code on this claw crane helps people buy the token needed for the machine.</em></figcaption>
</figure>

<figure class="caption"><img alt="" src="https://media.nngroup.com/media/editor/2016/10/07/karaoke.PNG" style="border-width: 0px; border-style: solid; width: 600px; height: 446px;"/>
<figcaption><em>Karaoke parlor: people can scan the QR code and pick the song they want to sing next.</em></figcaption>
</figure>

<p> </p>

<figure class="caption"><img alt="" src="https://media.nngroup.com/media/editor/2016/10/07/postcard.PNG" style="border-width: 0px; border-style: solid; width: 300px; height: 266px;"/>
<figcaption><em>Instructions for attaching a video or an image to a QR code on a paper postcard.</em></figcaption>
</figure>

<p>Not only do QR codes serve as an index into the online world, but they can also act as validations for processes that happened only virtually. For instance, a QR code can be a proof of purchase for a ticket that was bought online and never printed. (The Western world is most familiar with this use of passcodes: we’re using electronic bar codes to board airplanes or to get access to movie theaters and museums.)</p>

<p>The shake gesture also allows people to link the physical world to the online one and augment it, but usually the context — the timing or the location — of the interaction determines how the gesture is interpreted by WeChat. For example, people can shake their phones in the proximity of a store and WeChat will give them coupons or discounts about that store, or will provide information that is relevant to customers (e.g., parking, current sales). Participants in an event or watchers of a TV program may shake phones to vote in a poll, to express an opinion, or even to get a turn to ask a question. Shaking the phone while a song is being played will help the listener identify the song. People (collocated or not) could shake their phones simultaneously to exchange contact information and befriend each other on WeChat — this method makes it easy for complete strangers to connect if they wish to meet new people.</p>

<h2>Payments</h2>

<p>Payments are a major source of revenue for WeChat and are based on QR codes. Both WeChat Pay and AliPay, the two major online payment services in China, support the generation of a QR code for getting paid or sending money to a random person or business. Thus, in a store, people can scan the merchant’s QR code and pay it automatically using WeChat. Vendors can also scan a customer’s WeChat generated QR code to get paid. Small business with no credit card support can still enable cash-free transactions through QR codes. During our diary study, about a third of WeChat users’ activities were payments; the majority of these payments (more than half) were to offline businesses, followed payments to WeChat contacts. Many of our diary participants commented on the convenience of using QR codes for payment:</p>

<p><em>“I just wanted to grab some vegetables to cook for dinner in the market. I don’t want to take my wallet out of my backpack but luckily they accept WeChat! It’s much more convenient to take out my phone in my pocket.”</em></p>

<p><em>“All I need to do is scan the code, no more changes and coins! I can also get my expense recorded in real time so that I can track my money.”</em></p>

<figure class="caption"><img alt="" src="https://media.nngroup.com/media/editor/2016/10/07/wechat-payment.PNG" style="border-width: 0px; border-style: solid; width: 700px; height: 412px;"/>
<figcaption><em>WeChat Pay allows users to pay vendors or get paid.</em></figcaption>
</figure>

<p> </p>

<p>Although to a lesser extent, QR codes are also used for online payment on the desktop or on other sites (similarly to checking out with PayPal in the US): people use an app such as WeChat or AliPay to scan a uniquely generated QR code presented by the site at checkout. Once users scan the QR code, their money is sent by WeChat to the site, and the payment is completed.</p>

<h2>Communication Between Multiple Devices</h2>

<p>QR codes are also used to authenticate WeChat users on a different device. The assumption is that users will be always logged in to WeChat on their smartphone. When they want to log in to WeChat on a different device (e.g., tablet or desktop), instead of typing their credentials, they could use WeChat’s phone app to scan a QR code presented on the second device. WeChat severs would then detect which mobile account was used to scan the QR code, and log in the user to that account. (We’ve seen this technique being used recently in the US as well — for instance, Dropbox users can log in on a desktop by scanning an image with the Dropbox mobile app.)</p>

<figure class="caption"><img alt="" src="https://media.nngroup.com/media/editor/2016/10/07/wechat-login.PNG" style="border-width: 0px; border-style: solid; width: 287px; height: 384px;"/>
<figcaption><em>Users who attempt to log in to WeChat on a desktop computer are shown a QR code; when the code is scanned with the WeChat mobile app, the user is automatically logged in.</em></figcaption>
</figure>

<p> </p>

<figure class="caption"><img alt="" src="https://media.nngroup.com/media/editor/2016/10/07/dropbox.PNG" style="border-width: 0px; border-style: solid; width: 440px; height: 494px;"/>
<figcaption><em>Dropbox: To log in on the desktop without typing their passwords, users scan this image displayed on the monitor using the Dropbox mobile app.</em></figcaption>
</figure>

<p>So standard have QR codes become that some webpages use them instead of links, especially when they point to WeChat official accounts (which are mini webpages that live inside WeChat). This use has probably emerged because, until <a href="../ios-9-back-to-app-button/index.php">iOS 9</a>, it was fairly difficult to provide a deep link to a page inside an app. An added advantage is that QR codes support crossdevice handoff better than links — for example, if code is presented on a tablet or desktop screen, it can be scanned on a phone and the interaction can be continued on that device. (However, even some pages inside WeChat use QR codes instead of links, which seems to make little sense unless you assume that the user will want to open those links on a different phone — perhaps that of a friend.)</p>

<figure class="caption"><img alt="" src="https://media.nngroup.com/media/editor/2016/10/07/wechart-link.png" style="border-width: 0px; border-style: solid; width: 700px; height: 606px;"/>
<figcaption><em>A long press on the QR code will allow the user to extract it and go to the WeChat subscription account for a show.</em></figcaption>
</figure>

<h2>Usability Issues</h2>

<p>In spite of QR codes being widely used, the WeChat implementation is still not perfect. For example, in our study, participants experienced difficulty extracting a QR code sent through chat. In order to do so, they had to tap the picture and view it on a separate page, then long press it. But many participants attempted the long press in the conversation-thread view and were disappointed when they could not extract the QR code there.</p>

<figure class="caption"><img alt="" src="https://media.nngroup.com/media/editor/2016/10/07/chat.png" style="border-width: 0px; border-style: solid; width: 700px; height: 614px;"/>
<figcaption><em>A QR code sent through chat cannot be scanned in the conversation view (left). Users must tap on it to open it in a separate view (right) and then they can scan the code by performing a long press on the picture.</em></figcaption>
</figure>

<p>Another more general problem with QR codes is that they traditionally have no <a href="../information-scent/index.php">information scent</a> and need additional verbiage nearby to explain to the user what they stand for. However, nowadays QR codes can be customized and made to look almost like an icon, which provides an opportunity of adding some meaning to an otherwise nonsensical image.</p>

<figure class="caption"><img alt="" src="https://media.nngroup.com/media/editor/2016/10/07/artistic-qr.png" style="border-width: 0px; border-style: solid; width: 500px; height: 229px;"/>
<figcaption><em>Artistic QR codes add some information scent to an image devoid of meaning. Left: QR code for a Youtube video about New York; Right: A QR code for a personal blog includes a Chinese character standing for happiness and fortune.</em></figcaption>
</figure>

<p>When QR codes replace links, not only do they need extra screen space for communicating their meaning, but they also take more space than a regular link or a button. Especially on mobile, where screen real estate is particularly precious, too many QR codes on a page can degrade the user experience significantly.</p>

<h2>How Does UX Benefit?</h2>

<p>Although QR-code scanning and shake obviously brought Tencent a lot of revenue and substantially impacted the growth of its business into the behemoth that it is today, they also improved the overall user experience. These interaction techniques are examples of successful adoption of innovative technologies. Innovation often means going against the status quo, and it usually works only if users get the chance to use the new technology often (so they can learn it) and if the <a href="../power-law-learning/index.php">learning curve</a> drops fast, so people can achieve optimal performance quickly.</p>

<p>Once a new interaction technique becomes common knowledge, designers can start using it in other contexts, and everybody benefits, users included. In China, QR codes are not the appanage of WeChat — other applications and services also use them. For example, in 2013, Alibaba created a QR-code based promotion for its 11.11 Shopping Festival (a big shopping day in China, similar to the Black Friday and Cyber Monday in the US), which enabled its customers to add physical products seen in stores to a virtual shopping cart to complete the process online.</p>

<p>Similarly, because the shake gesture enjoys a reasonable amount of familiarity in China, other apps can capitalize on it and take advantage of it to implement desired functionality. For example, beyond WeChat, the shake gesture is used in other apps for functions such as:</p>

<ul>
	<li>Report a bug (Zhihu; this ressembles the use of shake to send feedback in Google Maps)</li>
	<li>Recover closed tabs (Smartisan mobile operating system)</li>
	<li>Restore location if you lost your place in a long thread (Moke)</li>
	<li>Stop the current song and jump to the next (Kugou)</li>
	<li>Update the list of suggested apps (Yingyongbao)</li>
</ul>

<p>Both code scanning and shake are two new interactions that enrich the <a href="../customer-journeys-omnichannel/index.php">omnichannel</a> user experience: they make it easier for people combine the offline and the online worlds by speeding up tedious input. The shake gesture has to fight a lack of signifiers, since, barring an ad or specific instructions,nothing in the environment prompts people to shake, but as people become more and more familiar with it, they will be more likely to remember to use it even in the absence of external cues. As a result, this gesture also has the potential to improve the single-channel UX, because mobile designers can start to free up precious screen real estate by delegating a feature to an invisible, yet familiar gesture. There are many apps that use these interactions in the US, yet, because most users are not familiar with them, these apps cannot take full advantage of their UX benefits and must implement redundancy (e.g., shake functionality doubled in the visible UI) or relegate these interactions to noncore functionality to make sure that users are able to use the app. Once these interactions will become standard, there will be no need for redundancy.</p>

<p>Things may change as Western companies try to emulate the success of WeChat and learn from its example. Already, Facebook Messenger has introduced features reminiscent of WeChat (e.g., personal codes for users, the ability to pay friends, the ability to chat with merchants). We still have to see whether in the future we will be able to scan a Facebook code to pay a merchant, or shake our phone and get to chat with the sales representative in a physical store. It seems that Facebook’s task is more daunting in a world where we already have many other interaction alternatives (we could pay using Apple Pay, PayPal, or even an ubiquitously accepted credit card, so it’s unclear why we’d choose Facebook Messenger over all these other alternatives). But if Facebook does win and we get to use shake and QR codes on our phones every day, the user experience will also win, as we’ll be able to speed up the communication among different interaction channels and experience smoother transitions between all these different domains.</p>
</section>
  

  
  <section>
    
        
    
  </section>
  
  
  
  
  

  


  <section>
    <br />
    <p class="share"><b>Share <span class="hide-for-small-down">this article</span>:</b> <a href="https://twitter.com/intent/tweet?url=http://www.nngroup.com/articles/wechat-qr-shake/&amp;text=Scan%20and%20Shake:%20A%20Lesson%20in%20Technology%20Adoption%20from%20China%e2%80%99s%20WeChat&amp;via=nngroup">Twitter</a> | <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=http://www.nngroup.com/articles/wechat-qr-shake/&amp;title=Scan%20and%20Shake:%20A%20Lesson%20in%20Technology%20Adoption%20from%20China%e2%80%99s%20WeChat&amp;source=Nielsen%20Norman%20Group">LinkedIn</a> | <a href="https://plus.google.com/share?url=http://www.nngroup.com/articles/wechat-qr-shake/">Google+</a> | <a href="mailto:?subject=NN/g Article: Scan and Shake: A Lesson in Technology Adoption from China’s WeChat&amp;body=http://www.nngroup.com/articles/wechat-qr-shake/">Email</a></p>
    <hr />
  </section>

  <footer class="l-subsection">
    
    
    <div class="learn-more">
      <h2>Learn More</h2>
      <p><a href="../subscribe/index.php">Subscribe to the weekly newsletter</a> to get notified about future articles.</p>
      </div>
    
    

    <div class="row show-for-small-down related-content">
      
      
      

      
      
        <div class="column small-12 topic-links">All articles about:
          <ul>
            
            <li><a href="../../topic/gestures/index.php">gestures</a></li>
            
            <li><a href="../../topic/international-users/index.php">International Users</a></li>
            
            <li><a href="../../topic/mobile-and-tablet-design/index.php">Mobile &amp; Tablet</a></li>
            
            <li><a href="../../topic/omnichannel/index.php">omnichannel</a></li>
            
            <li><a href="../../topic/web-trends/index.php">web trends</a></li>
            
          </ul>
        </div>
      
      

      
      
        <div class="column small-12">
          <h3>UX Conference Training Courses</h3>
           <ul class="no-bullet">
    
        <li><a href="../../courses/cross-channel-user-experience/index.php">Omnichannel Journeys and Customer Experience</a></li>
    
        <li><a href="../../courses/emerging-patterns-web-design/index.php">Emerging Patterns for Web Design</a></li>
    
        <li><a href="../../courses/usability-mobile-websites-apps/index.php">Mobile User Experience</a></li>
    
        <li><a href="../../courses/scaling-responsive-design/index.php">Scaling User Interfaces</a></li>
    
        <li>Visual Design for Mobile and Tablet: <a href="../../courses/vis-mob-1/index.php">Day 1</a> and <a href="../../courses/vis-mob-2/index.php">Day 2</a></li>
    
</ul>
        </div>
      
      

      
      
        <div class="small-12 column">
          <h3>Research Reports</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../reports/tablets/index.php">Tablet Website and Application UX</a></li>
              
            
              
                <li><a href="../../reports/mobile-website-and-application-usability/index.php">User Experience for Mobile Applications and Websites</a></li>
              
            
              
                <li><a href="../../reports/enterprise-mobile-showcase/index.php">Mobile Intranets and Enterprise Apps</a></li>
              
            
              
                <li><a href="../../reports/wap-usability/index.php">WAP Usability</a></li>
              
            
              
                <li><a href="../../reports/ipad-app-and-website-usability/index.php">iPad App and Website Usability</a></li>
              
            
          </ul>
        </div>
      
      

      
      
        <div class="small-12 column">
          <h3>Online Seminars</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../online-seminars/fundamental-flaw-flat-design/index.php">The Fundamental Flaw in Flat Design</a></li>
              
            
              
                <li><a href="../../online-seminars/every-word-count/index.php">Making Every Word Count</a></li>
              
            
              
                <li><a href="../../online-seminars/mobile-user-testing/index.php">Mobile User Testing</a></li>
              
            
          </ul>
        </div>
      
      
    </div>

    <div class="hide-for-small-down row related-content">
      <div class="medium-6 columns">
        
        
          <div>
            <h3>Articles</h3>
            <ul class="no-bullet">
              
                
                <li><a href="../app-lockers/index.php">App Lockers for Shared Phone Use in India</a></li>
                
              
                
                <li><a href="../large-touchscreens/index.php">Large Touchscreens: What&#39;s Different?</a></li>
                
              
                
                <li><a href="../visual-indicators-differentiators/index.php">Visual Indicators to Differentiate Items in a List</a></li>
                
              
                
                <li><a href="../china-website-complexity/index.php">Are Chinese Websites Too Complex?</a></li>
                
              
                
                <li><a href="../international-b2b/index.php">International B2B Audiences: Top 5 Ways to Improve Your Site for Global Users</a></li>
                
              
            </ul>
          </div>
        
        

        
          
          <div>
            <h3>Research Reports</h3>
            <ul class="no-bullet">
              
                
                  <li><a href="../../reports/tablets/index.php">Tablet Website and Application UX</a></li>
                
              
                
                  <li><a href="../../reports/mobile-website-and-application-usability/index.php">User Experience for Mobile Applications and Websites</a></li>
                
              
                
                  <li><a href="../../reports/enterprise-mobile-showcase/index.php">Mobile Intranets and Enterprise Apps</a></li>
                
              
                
                  <li><a href="../../reports/wap-usability/index.php">WAP Usability</a></li>
                
              
                
                  <li><a href="../../reports/ipad-app-and-website-usability/index.php">iPad App and Website Usability</a></li>
                
              
            </ul>
          </div>
          
        

      </div>
      <div class="medium-6 columns">
        
          
          <div>
            <h3>UX Conference Training Courses</h3>
             <ul class="no-bullet">
    
        <li><a href="../../courses/cross-channel-user-experience/index.php">Omnichannel Journeys and Customer Experience</a></li>
    
        <li><a href="../../courses/emerging-patterns-web-design/index.php">Emerging Patterns for Web Design</a></li>
    
        <li><a href="../../courses/usability-mobile-websites-apps/index.php">Mobile User Experience</a></li>
    
        <li><a href="../../courses/scaling-responsive-design/index.php">Scaling User Interfaces</a></li>
    
        <li>Visual Design for Mobile and Tablet: <a href="../../courses/vis-mob-1/index.php">Day 1</a> and <a href="../../courses/vis-mob-2/index.php">Day 2</a></li>
    
</ul>
          </div>
          
        
        
          
          <div>
            <h3>Online Seminars</h3>
            <ul class="no-bullet">
            
                <li><a href="../../online-seminars/fundamental-flaw-flat-design/index.php">The Fundamental Flaw in Flat Design</a></li>
            
                <li><a href="../../online-seminars/every-word-count/index.php">Making Every Word Count</a></li>
            
                <li><a href="../../online-seminars/mobile-user-testing/index.php">Mobile User Testing</a></li>
            
            </ul>
          </div>
          
        
      </div>
    </div>
  </footer>
</article>

        </div>

      

    
  </div>
  <div class="row">
    
      <div class="column small-12 show-for-small-down l-navrail announcements">
        
          
        
      </div>
    
    
  </div>

    </div>
    




<footer id="page-footer">
    <div class="row hide-for-print">
        <div class="column small-12 medium-6">
            <section class="footer-popular">
                
                <h1>Popular Topics</h1>
                <div style="float: left;  width: 15em; display: block;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/applications/index.php">Application Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/e-commerce/index.php">E-Commerce Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/eyetracking/index.php">Eyetracking </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/information-architecture/index.php">Information Architecture </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/intranets/index.php">Intranet Design </a></li>
	<li><a href="../../topic/interaction-design/index.php">Interaction Design </a></li>
</ul>
</div>

<div style="float: left;  width: 15em; display: block; list-style: none;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/mobile-and-tablet-design/index.php">Mobile and Tablet Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/credibility-persuasion/index.php">Persuasive Design</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/research-methods/index.php">Research Methods</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/user-testing/index.php">User Testing </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/web-usability/index.php">Web Usability </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/writing-web/index.php">Web Writing &amp; Content Strategy </a></li>
</ul>
</div>

<p>&nbsp;</p>

                
            </section>
        </div>

        <div class="column small-12 medium-6">
            <div class="row">
                <div class="column small-6">
                    <section class="footer-about">
                        <h1>Follow Us</h1>
                        <ul>
	<li><a href="../subscribe/index.php">Subscribe to our weekly UX newsletter</a>,&nbsp;Jakob Nielsen&#39;s Alertbox</li>
	<li>Visit Don Norman&#39;s website, <a href="http://www.jnd.org/"> www.jnd.org </a></li>
	<li>Visit Bruce Tognazzini&#39;s website, <a href="http://www.asktog.com/"> www.asktog.com </a></li>
	<li style="padding-top: 6px;"><a class="twitter-follow-button" data-show-count="false" data-size="large" href="https://twitter.com/nngroup"><img alt="Follow @nngroup on Twitter" height="20" src="https://media.nngroup.com/media/editor/2016/12/30/twitter-follow-button.png" style="width: 117px; height: 20px;" width="117" /></a></li>
	<li style="padding-top: 6px;"><img alt="" height="14" src="https://media.nngroup.com/media/editor/2016/07/26/feed-icon-14x14.png" style="vertical-align:text-top;width:14px;margin-right:.25em;" width="14" /><a href="../../feed/rss/index.php">Subscribe to our RSS feed</a></li>
</ul>

                    </section>
                </div>

                <div class="column small-6">
                    <section class="footer-about right-column">
                        <h1>About
</h1>
                        <ul class="no-bullet">
	<li><a href="../../about/contact/index.php">Contact information</a></li>
	<li><a href="../../about/index.php">About Us</a></li>
	<li><a href="../../about/why-nng/index.php">Why NN/g? </a></li>
	<li><a href="../../return-policy/index.php">Return Policy</a></li>
</ul>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="copyright column small-12">
            <p><a href="../../copyright-and-reprint-info/index.php">Copyright</a> &copy; 1998-2017 Nielsen Norman Group, All Rights Reserved.</p>
        </div>
    </div>
</footer>


    


        <script src="https://media.nngroup.com/static/vendor/fastclick/js/fastclick.min.js"></script>
        <script src="https://media.nngroup.com/static/vendor/foundation/js/foundation.min.js"></script>
        <script src="http://media.nngroup.com/static/js/app.js"></script>
        <script src="http://media.nngroup.com/static/js/scroll-to-fixed.js"></script><script>var externalLinks = document.querySelectorAll('a[href^="http"]');for (var i = externalLinks.length-1; i >= 0; i--) {externalLinks[i].addEventListener("click", function(event) { alert("External links are disabled for this test."); event.preventDefault(); }, false);}var mailLinks = document.querySelectorAll('a[href^="mailto"]');for (var i = mailLinks.length-1; i >= 0; i--) {mailLinks[i].addEventListener("click", function(event) { alert("Email links are disabled for this test."); event.preventDefault(); }, false);}var externalForms = document.querySelectorAll('form');for (var i = externalForms.length-1; i >= 0; i--) {externalForms[i].addEventListener("submit", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false); externalForms[i].addEventListener("click", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false);}</script>

        <script>
            
            $(function() {
                $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
                $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
            });
        </script>

        
<script src="https://media.nngroup.com/static/js/articles.js"></script>


        
        <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1072699960;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072699960/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
    </body>

<!-- Mirrored from www.nngroup.com/articles/wechat-qr-shake/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:04:02 GMT -->
</html>
