<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->





    
<!-- Mirrored from www.nngroup.com/articles/readers-comments-alertbox-five-years-retrospective/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:16:10 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1026.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"licenseKey":"1a30ef9931","queueTime":5,"beacon":"bam.nr-data.net","errorBeacon":"bam.nr-data.net","applicationID":"2048992","transactionName":"NFxSMRBUX0EDUkIPDA0WdhAMVkVbDV8ZBxEXUFMJB0YfRAtUQRVZIktEDAFZVHYHRVcPD01eVRE=","applicationTime":390,"agent":""}</script>
        <title>Readers&#39; Comments on Alertbox Five Years Retrospective  </title><meta property="og:title" content="Readers&#39; Comments on Alertbox Five Years Retrospective  " />
  
        
        <meta name="description" content="Readers say that WAP may provide the first micropayments (another reader: I-Mode already does in Japan!). Also: why the Alertbox reaches further than the page view stats show.">
        <meta property="og:description" content="Readers say that WAP may provide the first micropayments (another reader: I-Mode already does in Japan!). Also: why the Alertbox reaches further than the page view stats show." />
        
  
        
	
        
        <meta name="keywords" content="Readership, page views, WAP micropayments, Web content writing, piracy, copyright violations, plagiarism, pirates, illegal copies, I-Mode, NTT DoCoMo, discounts, student rates, developing countries, differential pricing">
        

        

        <meta name="robots" content="noarchive">
        <meta name="viewport" content="width=device-width">

        <link rel="search" type="application/opensearchdescription+xml" title="NN/g Search" href="../../opensearch.xml" />
        <link rel="shortcut icon" href="http://media.nngroup.com/static/img/favicon.ico" />

        <link rel="stylesheet" href="https://media.nngroup.com/static/vendor/font-awesome/css/font-awesome.min.css">

        
        <link rel="stylesheet" href="https://media.nngroup.com/static/css/7232a960f1df.css" type="text/css" />
        <link rel="stylesheet" href="http://fast.fonts.com/t/1.css?apiType=css&amp;projectid=c5846260-9def-46ec-9407-0f1349bd2d64" type="text/css">

        <script src="https://media.nngroup.com/static/vendor/modernizr/js/modernizr.min.js"></script>

        <script type="text/javascript" src="https://media.nngroup.com/static/js/3283be5def74.js"></script>
        

        

        
        
        <!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=229034,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('http://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->


<!-- Google Analytics Tracking Code for http://nngroup.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23132681-1', 'nngroup.com');
  ga('require', 'linkid', 'https://www.nngroup.com/articles/readers-comments-alertbox-five-years-retrospective/linkid.js');
  ga('require', 'displayfeatures');

// Optimizely Universal Analytics Integration
 window.optimizely = window.optimizely || [];
 window.optimizely.push("activateUniversalAnalytics");

  ga('send', 'pageview');

</script>
<script>
/**
* Function to track a click on an outbound link in Google Analytics.
*/
function trackOutboundLink(label, url) {
    if(window.ga && ga.loaded) {
        ga('send', 'event', 'outbound', 'click', label, {'hitCallback': function(){document.location = url;}
        });
    } else {
        document.location = url;
    }
}
/**
* Function to track a click on a link in the article announcement area.
*/
$('#articleAnnouncement a').on('click', function(){
  ga('send', 'event', 'link', 'click', 'articleAnnouncement');
});
</script>

<script type="text/javascript">
 setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2421.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



<!-- Hotjar Tracking Code for http://nngroup.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:121605,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

        <script src="https://media.nngroup.com/static/js/analytics/track_outbound_link.js"></script>
        
        
        
    </head>
    <body class="location-articles articles-detail">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
    <header id="page-header">
        <div id="cart" class="shopping-cart mobile-cart show-for-small-down"></div>
        <div class="row">
            <div id="masthead" class="masthead small-12 medium-7 column">

                <!-- The button to toggles menu on small screen -->

                <h1><a href="../../index.php"><img src="http://media.nngroup.com/static/img/logo-wordmark.png" width="74" height="30"> Nielsen <span class="norman">Norman</span> Group</a></h1>
                <p class="tagline show-for-medium-up">Evidence-Based User Experience Research, Training, and Consulting</p>

                <!-- The button to toggles form search on small screen -->


                <div style="clear: right;width:  100%;height: 1px;"></div><?php define('ROOT_PATH',$_SERVER["DOCUMENT_ROOT"] . '/apps/navigation_styles/navigations/4'); ?><?php include(ROOT_PATH)."/navigation.php"; ?>

                <!-- Drop down menu on small screen -->
                <div id="hideNav" class="show-for-small-down">
                  
                  <ul class="menu-content">
                    <li><a href="../../index.php">Home</a></li>
                    <li class="has-submenu">
                      <a>Training</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../training/index.php">Overview</a></li>
                        
<li id="nav-ux-conference"><a href="../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../online-seminars/index.php"><span>Online Seminars</span></a></li>

                      </ul>
                    </li>
                    <li class="has-submenu">
                      <a>Consulting</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
  <li><a href="../../consulting/index.php">Overview</a></li>
  
  <li><a href="../../consulting/benchmarking/index.php">Benchmarking</a></li>
  
  <li><a href="../../consulting/expert-design-review/index.php">Expert Design Review</a></li>
  
  <li><a href="../../consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
  
  <li><a href="../../consulting/usability-testing/index.php">Usability Testing </a></li>
  
  <li><a href="../../consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
  
  <li><a href="../../in-house-training/index.php">In-House Training</a></li>
</ul>

                    </li>

                    <li><a href="../../reports/index.php">Reports</a></li>
                    <li><a href="../index.php">Articles</a></li>
                    <li class="has-submenu"><a>About NN/G</a><i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../about/index.php">Overview</a></li>
                        <li><a href="../../people/index.php">People</a></li>
                        <li><a href="../../about/why-nng/index.php">Why NN/g?</a></li>
                        <li><a href="../../about/contact/index.php">Contact</a></li>
                        <li><a href="../../news/index.php">News</a></li>
                        <li><a href="../../about/history/index.php">History</a></li>
                        <li><a href="../../books/index.php">Books</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                </div>

                <hr class="header-divider show-for-small-only"/>


            </div>
       </div>

        <div id="hideNav">            <nav class="primary-nav small-12 column">
                <ul>
                    <li><a class="navitem" id="nav-home" href="../../index.php">Home</a></li>
                    <li><a class="navitem" id="nav-training" href="../../training/index.php">Training</a></li>
                    <li><a class="navitem" id="nav-consulting" href="../../consulting/index.php">Consulting</a></li>
                    <li><a class="navitem" id="nav-reports" href="../../reports/index.php">Reports</a></li>
                    <li><a class="navitem" id="nav-articles" href="../index.php">Articles</a></li>
                    <li><a class="navitem" id="nav-about" href="../../about/index.php">About NN/G</a></li>
                </ul>
            </nav>
            <nav class="secondary-nav small-12 medium-12 large-12 columns">
                
                
            </nav>
        </div>
    </header>
    <div class="l-content">
    
  

  

  <div class="row">
    
      
      <section class="small-12 medium-3 columns collapsable-content l-navrail articles-left-column">
        <h1 class="show-for-small-down collapse-link mobile-browse-header">Browse by Topic and Author</h1>
          <div class="row collapsable">

            
            <div class="small-6 medium-12 columns">
              <section id="article-popular-topics" style="display: none;">
                
                <h1>Topics</h1>
                <ul id="article-popular-topics">
                    
                    <li>
                        
                        <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/intranets/all/index.php">Intranets</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/user-testing/all/index.php">User Testing</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                        
                    </li>
                    
                    <li><a href="#" id="article-see-all-topics"><strong>See all topics&hellip;</strong></a></li>
                </ul>
                
              </section>
              <section id="article-all-topics" style="display: none;">
                
                <h1>
                  <span class="show-for-medium-up">All Article Topics</span>
                  <span class="show-for-small-down">All Topics</span>
                  (<a href="#" id="article-hide-all-topics">hide</a>)
                </h1>
                <ul>
                  
                  <li>
                    
                    <a href="../../topic/accessibility/all/index.php">Accessibility</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/agile/all/index.php">Agile</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/analytics-and-metrics/all/index.php">Analytics &amp; Metrics</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/applications/all/index.php">Application Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/b2b-websites/all/index.php">B2B Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/behavior-patterns/all/index.php">Behavior Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/branding/all/index.php">Branding</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/content-strategy/all/index.php">Content Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/corporate-websites/all/index.php">Corporate Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/design-patterns/all/index.php">Design Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-design-process/all/index.php">Design Process</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/email/all/index.php">Email</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/eyetracking/all/index.php">Eyetracking</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/heuristic-evaluation/all/index.php">Heuristic Evaluation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/human-computer-interaction/all/index.php">Human Computer Interaction</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-ideation/all/index.php">Ideation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/information-architecture/all/index.php">Information Architecture</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/interaction-design/all/index.php">Interaction Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/international-users/all/index.php">International Users</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/intranets/all/index.php">Intranets</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-management/all/index.php">Management</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/navigation/all/index.php">Navigation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/non-profit-websites/all/index.php">Non-Profit Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/personas/all/index.php">Personas</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/persuasive-design/all/index.php">Persuasive Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/prototyping/all/index.php">Prototyping</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/psychology-and-ux/all/index.php">Psychology and UX</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/research-methods/all/index.php">Research Methods</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/search/all/index.php">Search</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/social-media/all/index.php">Social Media</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/strategy/all/index.php">Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/user-testing/all/index.php">User Testing</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-teams/all/index.php">UX Teams</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/visual-design/all/index.php">Visual Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/writing-web/all/index.php">Writing for the Web</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/young-users/all/index.php">Young Users</a>
                    
                  </li>
                  
                </ul>
                
              </section>
            </div>
            <script>
              /* Determine which topics list will be displayed: the full or the
               * partial.  We are inlining this close to the list itself to
               * prevent rendering delays on the client
               */
              var topic_list = $.cookie("article-topics");

              var popular_topics = $('#article-popular-topics');
              var all_topics = $('#article-all-topics');

              if(topic_list == 1) {
                  popular_topics.hide();
                  all_topics.show();
              } else {
                  popular_topics.show();
                  all_topics.hide();
              }
            </script>
            

            
            <div class="small-6 medium-12 columns">
              
              <section id="article-initial-authors">
                  <h1>Author</h1>
                  <ul>
                    
                    <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                    
                    <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                    
                    <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                    
                    <li><a href="#" onClick="$('#article-initial-authors').hide(); $('#article-all-authors').show();"><strong>See all authors&hellip;</strong></a></li>
                  </ul>
              </section>
              <section id="article-all-authors" style="display: none;">
                <h1>
                  All Authors
                  (<a href="#" id="article-hide-all-authors">hide</a>)
                </h1>
                <ul>
                  
                  <li><a href="../author/raluca-budiu/index.php">Raluca Budiu</a></li>
                  
                  <li><a href="../author/patty-caya/index.php">Patty Caya</a></li>
                  
                  <li><a href="../author/susan-farrell/index.php">Susan Farrell</a></li>
                  
                  <li><a href="../author/therese-fessenden/index.php">Therese Fessenden</a></li>
                  
                  <li><a href="../author/kim-flaherty/index.php">Kim Flaherty</a></li>
                  
                  <li><a href="../author/sarah-gibbons/index.php">Sarah Gibbons</a></li>
                  
                  <li><a href="../author/aurora-harley/index.php">Aurora Harley</a></li>
                  
                  <li><a href="../author/kate-kaplan/index.php">Kate Kaplan</a></li>
                  
                  <li><a href="../author/page-laubheimer/index.php">Page Laubheimer</a></li>
                  
                  <li><a href="../author/angela-li/index.php">Angie Li</a></li>
                  
                  <li><a href="../author/hoa-loranger/index.php">Hoa Loranger</a></li>
                  
                  <li><a href="../author/kate-meyer/index.php">Kate Meyer</a></li>
                  
                  <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                  
                  <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                  
                  <li><a href="../author/kara-pernice/index.php">Kara Pernice</a></li>
                  
                  <li><a href="../author/christian-rohrer/index.php">Christian Rohrer</a></li>
                  
                  <li><a href="../author/amy-schade/index.php">Amy Schade</a></li>
                  
                  <li><a href="../author/katie-sherwin/index.php">Katie Sherwin</a></li>
                  
                  <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                  
                  <li><a href="../author/kathryn-whitenton/index.php">Kathryn Whitenton</a></li>
                  
                </ul>
              </section>
              
            </div>
            
          </div>

          
          <section class="hide-for-small-down">
            
              
              <h1>Recent Articles</h1>
              
              <ul>
                  
                  <li><a href="../seamless-cross-channel/index.php">Seamlessness in the Omnichannel User Experience</a></li>
                  
                  <li><a href="../brand-intention-interpretation/index.php">Experience Design: Bridging Brand Intention and Brand Interpretation</a></li>
                  
                  <li><a href="../eyetracking-tasks-efficient-scanning/index.php">Scanning Patterns on the Web Are Optimized for the Current Task</a></li>
                  
                  <li><a href="../flat-design-best-practices/index.php">Flat-Design Best Practices</a></li>
                  
                  <li><a href="../ux-user-stories/index.php">Accounting for UX Work with User Stories in Agile Projects</a></li>
                  
                  <li><a href="../index.php"><strong>See all articles&hellip;</strong></a></li>
              </ul>
              
            
          </section>
          

          
          
          <section class="hide-for-small-down">
            <h1>Popular Articles</h1>
            
            <ul>
                
                <li><a href="../usability-101-introduction-to-usability/index.php">Usability 101: Introduction to Usability</a></li>
                
                <li><a href="../top-10-mistakes-web-design/index.php">Top 10 Mistakes in Web Design</a></li>
                
                <li><a href="../how-users-read-on-the-web/index.php">How Users Read on the Web</a></li>
                
                <li><a href="../f-shaped-pattern-reading-web-content/index.php">F-Shaped Pattern For Reading Web Content</a></li>
                
                <li><a href="../ten-usability-heuristics/index.php">10 Usability Heuristics for User Interface Design</a></li>
                
                <li><a href="../intranet-design-2016/index.php">10 Best Intranets of 2016</a></li>
                
                <li><a href="../which-ux-research-methods/index.php">When to Use Which User-Experience Research Methods</a></li>
                
                <li><a href="../response-times-3-important-limits/index.php">Response Times: The 3 Important Limits</a></li>
                
                <li><a href="../why-you-only-need-to-test-with-5-users/index.php">Why You Only Need to Test with 5 Users</a></li>
                
                <li><a href="../page-fold-manifesto/index.php">The Fold Manifesto: Why the Page Fold Still Matters</a></li>
                
            </ul>
          </section>
          
          

          
            <section class="hide-for-small-down">
              
            </section>
          

      </section>
      

      
        <div class="small-12 medium-9 columns content-body">
          




<article class="l-subsection alertbox-article-content article-detail-bl">
  
  <header>
    <div class="l-subsection">
      <h1>Readers&#39; Comments on Alertbox Five Years Retrospective  </h1>
      <div class="author-meta">by
        
          
            
            <a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a>
            
          
        on  May 28, 2000
        
      </div>

      

      
      <div class="topic-links">
        Topics:
        
          <ul>

</ul>
        
      </div>
      
    </div>
  </header>
  

  
  
  

  
  <section id="article-body"><p>
	<em>Sidebar to <a href="../../people/jakob-nielsen/index.php" title="Author biography"> Jakob Nielsen</a>'s column on <a href="../alertbox-5-years-retrospective/index.php"> Alertbox 5 Years</a>. </em></p>
<p>
	I have received many warm and congratulatory messages from readers thanking me for the five years of columns. <em> I really appreciate your feedback: thank you for the kind words! </em></p>
<p>
	For the sake of brevity, I am not reprinting these thank-you messages on this comment page.</p>
<h2>
	Size of Alertbox Readership</h2>
<p>
	<em>Marc Cooper from Southwest Missouri State University writes: </em></p>
<blockquote>
	You may be reaching proportionately fewer web designers but <strong:> those you do reach attract more visitors. </strong:>
	<p>
		Our little history department site had 100,000 page views this year. Thanks.</p>
</blockquote>
<p>
	<em>Jakob's reply: </em> This is a great point. If people don't read the Alertbox, then their sites don't attract very many users (or at least not many <em> repeat </em> users).</p>
<p>
	Compare these two metrics:</p>
<ol style="margin-top: 0">
	<li>
		Number of page views to the Alertbox relative to number of websites</li>
	<li>
		Number of page views to sites designed by Web professionals who read the Alertbox relative to the number of page views designed by people who are ignorant of the Alertbox</li>
</ol>
<p>
	The first statistic may not seem too promising (less than 10% of sites have read even the most basic usability rules), but the second statistic could be quite good (even though it is impossible to verify).</p>
<h3>
	Readership Bigger Than Logs Indicate</h3>
<p>
	<em>Jan M. Faber from Arthur D. Little writes: </em></p>
<blockquote>
	I think you are short changing yourself a bit by calculating how many hits you get on the web site because I've seen several of your columns copy/pasted into e-mail and sent around, especially <a href="../how-users-read-on-the-web/index.php"> How Users Read on the Web</a>. (I know this defeats the purpose of a web-based column, but it's the reality of slow starting browsers and corporate firewalls.)</blockquote>
<p>
	<em>Along similar lines, Jonathan O'Donnell from Melbourne IT Research and Technology writes: </em></p>
<blockquote>
	I don't think that you are taking into account that you influence people who teach. Therefore, there is a pyramid effect, where your thoughts go out to teachers who pass them on to students, etc.
	<p>
		Admittedly, those students should be visiting your site, but it doesn't always happen that way. Have a look at "Dr Bandwidth's seven deadly sins"</p>
	<p>
		I know that the teacher who taught Dr Bandwidth (James Corless) is a big fan of yours. That teacher teaches your principles. He has taught a lot of students over the years. They all would have picked up your ethos, but not all of them would have visited your pages.</p>
</blockquote>
<h3>
	Pirates</h3>
<p>
	And then of course there are the pirates. For example, a newspaper in Kazakhstan recently ran <a href="../top-10-mistakes-of-web-management/index.php"> Top Ten Mistakes of Web Management</a> under the byline of a local writer.</p>
<p>
	And the number of websites that have copied <a href="../top-10-mistakes-web-design/index.php"> Top Ten Mistakes of Web Design</a> without permission is legion. Some even run it under their own byline, adding insult to injury.</p>
<p>
	Luckily such piracy often backfires. As one manager wrote me: <strong> "after seeing your article pirated on [design firm's site], I know where <em> not </em> to take my company's business." </strong></p>
<p>
	Advice to pirates: if you want illegal content on your site, then it is better to rip off somebody who is not too well known.</p>
<h2>
	WAP for Micropayments</h2>
<p>
	<em>Jaroslaw Milewski, ATM S.A. in Poland writes: </em></p>
<blockquote>
	In your last column, "Alertbox Five Years Retrospective" you point out the micropayments as your biggest defeat in predicting the business models for the Web.
	<p>
		I think micropayments are coming at a quick pace, but not exactly in the way you have considered - the keyword is WAP. In contrast to the "general" Internet, mobile telephony has a built-in (and a very precise!) billing system. It is natural for a WAP site owner to come to an agreement with the mobile telephony operator about getting a percentage of every bill for the connection to the WAP site. The business is very interesting for both parties: the WAP site operator gets the micropayments and the mobile net operator gets more connections.</p>
	<p>
		Since more and more Web services are being equipped now with a WAP alternative to the "main" Web site, I can easily imagine a situation when the Web site is used for promotion and financed e.g. by ads, while the accompanying WAP site is financed by micropayments.</p>
	<p>
		General remark: think more about WAP! I was in Amsterdam on WWW9 and mobile Web was one of the hottest topics (taking into account that the US are at least 2 years behind Europe and Japan...)</p>
</blockquote>
<p>
	<em>Jakob's reply: </em> You may well be right. The huge benefit of WAP (or any mobile technology) is that the user already has a billing arrangement in place with somebody who is metering their use of the system. And people are used to paying extra for mobile services. Finally, the current generation of WAP screens are much too small for advertising, so they <em> need </em> a different business model.</p>
<p>
	More on WAP and micropayments: <em> Erwin Boogert from The Netherlands writes: </em></p>
<blockquote>
	In your most recent column you talked <em> again </em> about micropayments becoming generally accepted soon. I thought you might be interested in the following. It's about the combination of micropayments and mobile communications (WAP) and, more specific, the developments I see happening around me in The Netherlands.
	<p>
		There are two main developments going on in the fields described.</p>
	<ol>
		<li>
			integration of excisting payment standards, facilitated on the web</li>
		<li>
			combination of wireless internet and content.</li>
	</ol>
	<p>
		 </p>
	<ol>
		<li>
			A Dutch company called Bibit has been developping a web payment system which supports 38 different international payment standards, including small bank payments.
			<ul>
				<li>
					<a href="http://www.bibit.nl/Payment/">Payment systems supported by Bibit</a> (includes Dutch micropayments by bank)</li>
				<li>
					Bibit recently started working together with Intrum Justitia to develop an Electronic Bill Presentment &amp; Payment (EBPP). <a href="http://www.intrum.com/"> Intrum Justita</a> claims to be 'Europe's largest and most international Debt Collection and Credit Management Group'.</li>
				<li>
					This is a complete <a href="http://www.bibit.nl/clients/"> list of its clients</a></li>
				<li>
					Recently Bibit announced that it would facillitate payments through KPN Mobile's WAP services.</li>
			</ul>
		</li>
		<li>
			KPN Mobile is subsidiairy of the Dutch national telecommunications carrier KPN. KPN Mobile will go public next month and already exploits a WAP-portal called m-info.nl. In this a lot of national content providers participate (newspapers, yellow pages, going out, order tickets, etc., etc.). In this IPO NTT DoCoMo will have a 15% stake.
			<ul>
				<li>
					In the deal the Japanese enhance the reach of their services; the Dutch may 'use' the Japanese know-how of high-speed multimedia mobile telecommunications (a.k.a. 'i-mode').
					<ul>
						<li>
							<a href="http://www.kpn.com/">corporate KPN site</a> (in Dutch only)</li>
					</ul>
				</li>
			</ul>
		</li>
	</ol>
	I thought this might be interest to you. More specific: the combination of Bibit's services and those of KPN Mobile.</blockquote>
<h2>
	I-Mode Better Than WAP</h2>
<p>
	<em>Renfield Kuroda from Japan writes: </em></p>
<blockquote>
	While several readers have responded to your micropayments column with predictions about WAP, we here in Japan have already been enjoying extensive micropayment use.
	<p>
		The largest cellular phone provider, <strong> NTT DoCoMo</strong>, started an Internet cellular phone service called I-Mode in February of 1999. It is a digital packet-switched network with contents in <strong> compactHTML </strong> (not WML).</p>
	<p>
		Since the beginning, micropayments have been a core part of the service, and are very popular. Basically, many sites on the gateway menu (several hundred of the nearly 1000 official gateway sites) have <strong> pay contents of up to 500 yen per month</strong>. This charge is added directly to your cellular phone bill. Users sign up for pay contents by explicitly acknowledging their understanding of the payment obligation AND by entering a 4 digit handset password.</p>
	<p>
		Furthermore, by the end of the year the newest handsets will run Java (same KVM as for Palm Pilots, most likely), enabling 128 SSL encryption, digital signatures, and digit certificates. Thus a whole new general of payments and transaction settlements will be born.</p>
	<p>
		To date there are 7.5 <em> million </em> I-Mode users, and growing at over 1 million new users a month. (FYI the 2nd largest ISP in Japan is @Nifty, with only 3.5 million customers connecting to the Internet via PCs.)</p>
	<p>
		A typical user pays 300 yen a month for pay contents, generating millions of dollars of revenue a year.</p>
	<p>
		The <strong> I-Mode success highlights several important Internet trends</strong>:</p>
	<ul>
		<li>
			The micropayment model has been proven</li>
		<li>
			The wireless Internet <em> will </em> be bigger than the wired Internet</li>
		<li>
			Screen size, technical specs, and other "limitations" of wireless handsets are good for contents and users; contents MUST be written well, menus and navigation <em> must </em> make sense and be easy to use</li>
		<li>
			Users who surf in 5-30 seconds bursts (waiting for the train, riding the bus, waiting for the light to change) are considerably less tolerant of anything that gets in the way of getting to the desired information</li>
		<li>
			Broadband, stereo sound, and complex plugins are not necessary to fulfil the basic need of the Internet: facilitating information communication</li>
	</ul>
	Unfortunately there is not much in English written about I-Mode, but I'm afraid focusing on the hopes of WAP and it minimal successes to date clouds the real issues of the wireless Internet and the future of the web. For more information please do not hesitate to contact me. You can also get some English information from English sites of Japanese news services like <a class="out" href="http://www.nni.nikkei.co.jp/" title="Nikkei Net Interactive"> http://www.nni.nikkei.co.jp</a> or just searching for "I-Mode" on the Web.</blockquote>
<h2>
	Reduced Micropayments for Poor Countries</h2>
<p>
	<em>Attila from Hungary writes: </em></p>
<blockquote>
	Although there is no doubt that this system would improve the quality of web content (which is highly needed!) it seems to be unable to deal with the price differences between different countries (apart from currency exchange costs). I mean one of the great things about internet is that it is "international" without traditional barriers -- I can surf around the world sitting here in my small flat in Budapest, getting access to information from all over the world, paying <em> local </em> fees. For instance from your site, for which I would actually pay. But once it was common practice everywhere, soon I think I would be discouraged to use foreign sites much, and would feel -- as others as well -- to be forced to visit mainly local websites. <strong> A few cents/page might be very cheap for an American, it is not the same for someone living in Hungary</strong>, and definitely not for someone from a "Third World" country. This would build up barriers on the net and would work against its "international" nature dividing "rich" and "poor" regions.</blockquote>
<p>
	<em>Jakob's reply: </em> Very good point, and it should be possible to have several different levels of fees for users from different countries. Implementing such differential pricing is not that easy, though. We cannot simply check the IP address to see whether somebody is from a poor country and thus entitled to a cheaper rate since the country code suffix is notoriously unreliable. Also, would, say, the German Ambassador to Ghana be entitled to a potentially lower African rate? I think I would charge the European rate. So the user's physical location would not be sufficient.</p>
<p>
	In general, a micropayment system might include several options for users to be classified (and certified) as belonging to various categories that could qualify for reduced rates. Students form a category that would need to be recertified every year, so that's not something the individual website would want to do.</p>
<p>
	Once the micropayment infrastructure supports the notion of special user categories, it would be up to the individual website to set its own pricing strategy. It would often make good business sense to offer discounted rates to certain users. For example, if you offer students a good rate, you often hook them and get their full-paying business once they become rich professionals. And with developing countries, it would often make sense to sell page views at a discount because <em> some </em> money is better than nothing (and it doesn't cost very much to ship an incremental page view off to some remote country as long as the Internet traffic itself is not charged by distance).</p>
<h2>
	Micropayments May Cause New Taxes</h2>
<p>
	<em>Mike Demmers from Portland writes: </em></p>
<blockquote>
	While micropayments seem on the surface to be something that would be useful to content providers, there is one reason why I hope they do not become common soon.
	<p>
		The moment micropayments become a common, easily-used method of paying for transactions over the internet, the group of thugs and looters that collectively refers to themselves as 'The Congress of the United States' ('America's only native criminal class' - Mark Twain) will have a ready mechanism to add the equivalent of a national sales tax. Past experience shows they will do this as soon as they can generate enough fear, uncertainty and doubt on the part of the public, and greed on the part of those who would directly benefit, to make it politically viable. I would not expect this to take long, and a likely carrot may well be a government enforced standard.</p>
	<p>
		Once the foot is in the door with a national sales tax on ANYTHING, it will not be long before it is expanded.</p>
	<p>
		For this reason alone, I will not be supporting any efforts to make micropayments viable.</p>
	<p>
		Our taxes are, by historical standards, high enough to be at the level where resistance and lack of compliance are becoming a serious problem. The idea of having taxes automatically deducted by computers must look like tax heaven to the people who came up with 'voluntary' witholding taxes and the like.</p>
	<p>
		The internet is ALREADY heavily taxed. The telephone lines we use individually to connect are taxed at the state, federal, and usually the local level as well. The major trunks are taxed. The computers we use are taxed when sold, and anyone who makes a profit from any part of the internet is heavily taxed on that. Telephone and cable companys are already more heavily taxed and controlled than most businesses. This idea that people who buy from out of state locations should be taxed is a NEW one, an attempt on the state's part to shamelessly expand their tax base.</p>
	<p>
		There is no tax break on the internet, despite what the politicans are trying to propagandize us with. It is a fiction designed to pave the way for more taxation, nothing more.</p>
	<p>
		So I will fight this idea of micropayments in any way I can, until I am convinced that it will not be used as yet another transfer payment mechanism by our sociopathic political class. I am simply not willing to leave the back door open so the thieves can freely loot me.</p>
	<p>
		PS - Just about the only place on your whole site where a link COULD be used but IS NOT used is your e-mail address. Someone should do an article on the clever tricks being used by famous people to have their e-mail address online, but provide some kind of IQ test to access it.</p>
	<p>
		I especially enjoyed the approach taken by John Walker of Autodesk:</p>
	<ul>
		<li>
			<a href="http://www.fourmilab.to/nav/topics/faq.php" title="out">http://www.fourmilab.to/nav/topics/faq.php </a></li>
		<li>
			<a href="http://www.fourmilab.to/documents/titanium/" title="out">http://www.fourmilab.to/documents/titanium/ </a></li>
	</ul>
	(I did not send him e-mail. ;-) )
	<p>
		I am curious to know how much of an effect just the simple act of leaving off the mailto link has had for you.</p>
</blockquote>
<p>
	<em>Jakob's reply: </em> One of my friends once said that it is dangerous to discuss ideas for new taxes in public: you are just going to give somebody an idea, and before you know it, the tax is there.</p>
<p>
	I can see the risk of an Internet tax, but I don't think it is a necessary outcome. For example, if I was charging a micropayment for this very page view, then obviously my income would go up, and thus the government would get its cut from the existing tax rules. So even as the economy shifts to the Internet, the old taxes still apply because all revenues ultimately accrue to companies and their employees and shareholders.</p>
<h2>
	Web-Appropriate Writing</h2>
<p>
	<em>A reader from Australia writes: </em></p>
<blockquote>
	I'll keep this brief. I am a computer industry writer with a background in journalism. I now have approx. 20 years experience. I have been reading your columns and books for some time now and you have a convert. However, the problem I constantly encounter is not my inability to change the way I write to suit the circumstances, but the <strong> company's inability to see that good writing is even important </strong> . Current example, I am working with a company who is making a large database of problem solutions available as FAQs on their web site. There is not a writer on the project. They see it as a technology problem, not an information problem. The area in which I live abounds with over-designed, poorly constructed web sites with spelling mistakes. Any one can write apparently.
	<p>
		I am on the verge of giving up and growing vegetables.</p>
</blockquote>
</section>
  

  
  <section>
    
        
    
  </section>
  
  
  
  
  

  


  <section>
    <br />
    <p class="share"><b>Share <span class="hide-for-small-down">this article</span>:</b> <a href="https://twitter.com/intent/tweet?url=http://www.nngroup.com/articles/readers-comments-alertbox-five-years-retrospective/&amp;text=Readers'%20Comments%20on%20Alertbox%20Five%20Years%20Retrospective%20%20&amp;via=nngroup">Twitter</a> | <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=http://www.nngroup.com/articles/readers-comments-alertbox-five-years-retrospective/&amp;title=Readers'%20Comments%20on%20Alertbox%20Five%20Years%20Retrospective%20%20&amp;source=Nielsen%20Norman%20Group">LinkedIn</a> | <a href="https://plus.google.com/share?url=http://www.nngroup.com/articles/readers-comments-alertbox-five-years-retrospective/">Google+</a> | <a href="mailto:?subject=NN/g Article: Readers&#39; Comments on Alertbox Five Years Retrospective  &amp;body=http://www.nngroup.com/articles/readers-comments-alertbox-five-years-retrospective/">Email</a></p>
    <hr />
  </section>

  <footer class="l-subsection">
    
    
    

    <div class="row show-for-small-down related-content">
      
      
      

      
      
      

      
      
      

      
      
      

      
      
      
    </div>

    <div class="hide-for-small-down row related-content">
      <div class="medium-6 columns">
        
        
        

        

      </div>
      <div class="medium-6 columns">
        
        
      </div>
    </div>
  </footer>
</article>

        </div>

      

    
  </div>
  <div class="row">
    
      <div class="column small-12 show-for-small-down l-navrail announcements">
        
          
        
      </div>
    
    
  </div>

    </div>
    




<footer id="page-footer">
    <div class="row hide-for-print">
        <div class="column small-12 medium-6">
            <section class="footer-popular">
                
                <h1>Popular Topics</h1>
                <div style="float: left;  width: 15em; display: block;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/applications/index.php">Application Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/e-commerce/index.php">E-Commerce Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/eyetracking/index.php">Eyetracking </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/information-architecture/index.php">Information Architecture </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/intranets/index.php">Intranet Design </a></li>
	<li><a href="../../topic/interaction-design/index.php">Interaction Design </a></li>
</ul>
</div>

<div style="float: left;  width: 15em; display: block; list-style: none;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/mobile-and-tablet-design/index.php">Mobile and Tablet Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/credibility-persuasion/index.php">Persuasive Design</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/research-methods/index.php">Research Methods</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/user-testing/index.php">User Testing </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/web-usability/index.php">Web Usability </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/writing-web/index.php">Web Writing &amp; Content Strategy </a></li>
</ul>
</div>

<p>&nbsp;</p>

                
            </section>
        </div>

        <div class="column small-12 medium-6">
            <div class="row">
                <div class="column small-6">
                    <section class="footer-about">
                        <h1>Follow Us</h1>
                        <ul>
	<li><a href="../subscribe/index.php">Subscribe to our weekly UX newsletter</a>,&nbsp;Jakob Nielsen&#39;s Alertbox</li>
	<li>Visit Don Norman&#39;s website, <a href="http://www.jnd.org/"> www.jnd.org </a></li>
	<li>Visit Bruce Tognazzini&#39;s website, <a href="http://www.asktog.com/"> www.asktog.com </a></li>
	<li style="padding-top: 6px;"><a class="twitter-follow-button" data-show-count="false" data-size="large" href="https://twitter.com/nngroup"><img alt="Follow @nngroup on Twitter" height="20" src="https://media.nngroup.com/media/editor/2016/12/30/twitter-follow-button.png" style="width: 117px; height: 20px;" width="117" /></a></li>
	<li style="padding-top: 6px;"><img alt="" height="14" src="https://media.nngroup.com/media/editor/2016/07/26/feed-icon-14x14.png" style="vertical-align:text-top;width:14px;margin-right:.25em;" width="14" /><a href="../../feed/rss/index.php">Subscribe to our RSS feed</a></li>
</ul>

                    </section>
                </div>

                <div class="column small-6">
                    <section class="footer-about right-column">
                        <h1>About
</h1>
                        <ul class="no-bullet">
	<li><a href="../../about/contact/index.php">Contact information</a></li>
	<li><a href="../../about/index.php">About Us</a></li>
	<li><a href="../../about/why-nng/index.php">Why NN/g? </a></li>
	<li><a href="../../return-policy/index.php">Return Policy</a></li>
</ul>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="copyright column small-12">
            <p><a href="../../copyright-and-reprint-info/index.php">Copyright</a> &copy; 1998-2017 Nielsen Norman Group, All Rights Reserved.</p>
        </div>
    </div>
</footer>


    


        <script src="https://media.nngroup.com/static/vendor/fastclick/js/fastclick.min.js"></script>
        <script src="https://media.nngroup.com/static/vendor/foundation/js/foundation.min.js"></script>
        <script src="http://media.nngroup.com/static/js/app.js"></script>
        <script src="http://media.nngroup.com/static/js/scroll-to-fixed.js"></script><script>var externalLinks = document.querySelectorAll('a[href^="http"]');for (var i = externalLinks.length-1; i >= 0; i--) {externalLinks[i].addEventListener("click", function(event) { alert("External links are disabled for this test."); event.preventDefault(); }, false);}var mailLinks = document.querySelectorAll('a[href^="mailto"]');for (var i = mailLinks.length-1; i >= 0; i--) {mailLinks[i].addEventListener("click", function(event) { alert("Email links are disabled for this test."); event.preventDefault(); }, false);}var externalForms = document.querySelectorAll('form');for (var i = externalForms.length-1; i >= 0; i--) {externalForms[i].addEventListener("submit", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false); externalForms[i].addEventListener("click", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false);}</script>

        <script>
            
            $(function() {
                $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
                $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
            });
        </script>

        
<script src="https://media.nngroup.com/static/js/articles.js"></script>


        
        <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1072699960;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072699960/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
    </body>

<!-- Mirrored from www.nngroup.com/articles/readers-comments-alertbox-five-years-retrospective/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:16:10 GMT -->
</html>
