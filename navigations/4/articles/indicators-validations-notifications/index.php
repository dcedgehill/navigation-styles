<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->





    
<!-- Mirrored from www.nngroup.com/articles/indicators-validations-notifications/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:06:22 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1026.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"licenseKey":"1a30ef9931","queueTime":6,"beacon":"bam.nr-data.net","errorBeacon":"bam.nr-data.net","applicationID":"2048992","transactionName":"NFxSMRBUX0EDUkIPDA0WdhAMVkVbDV8ZBxEXUFMJB0YfRAtUQRVZIktEDAFZVHYHRVcPD01eVRE=","applicationTime":346,"agent":""}</script>
        <title>Indicators, Validations, and Notifications: Pick the Correct Communication Option</title><meta property="og:title" content="Indicators, Validations, and Notifications: Pick the Correct Communication Option" />
  
        
        <meta name="description" content="Status feedback is crucial to the success of any system. Knowing when to use 3 common communication methods is key to supporting users.">
        <meta property="og:description" content="Status feedback is crucial to the success of any system. Knowing when to use 3 common communication methods is key to supporting users." />
        
  
        
	
        
        <meta name="keywords" content="indicators, validation, notifications, feedback, intrusive notifications, non-intrusive notifications, error messages, system alerts">
        

        

        <meta name="robots" content="noarchive">
        <meta name="viewport" content="width=device-width">

        <link rel="search" type="application/opensearchdescription+xml" title="NN/g Search" href="../../opensearch.xml" />
        <link rel="shortcut icon" href="http://media.nngroup.com/static/img/favicon.ico" />

        <link rel="stylesheet" href="https://media.nngroup.com/static/vendor/font-awesome/css/font-awesome.min.css">

        
        <link rel="stylesheet" href="https://media.nngroup.com/static/css/7232a960f1df.css" type="text/css" />
        <link rel="stylesheet" href="http://fast.fonts.com/t/1.css?apiType=css&amp;projectid=c5846260-9def-46ec-9407-0f1349bd2d64" type="text/css">

        <script src="https://media.nngroup.com/static/vendor/modernizr/js/modernizr.min.js"></script>

        <script type="text/javascript" src="https://media.nngroup.com/static/js/3283be5def74.js"></script>
        

        

        
        
        <!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=229034,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('http://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->


<!-- Google Analytics Tracking Code for http://nngroup.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23132681-1', 'nngroup.com');
  ga('require', 'linkid', 'https://www.nngroup.com/articles/indicators-validations-notifications/linkid.js');
  ga('require', 'displayfeatures');

// Optimizely Universal Analytics Integration
 window.optimizely = window.optimizely || [];
 window.optimizely.push("activateUniversalAnalytics");

  ga('send', 'pageview');

</script>
<script>
/**
* Function to track a click on an outbound link in Google Analytics.
*/
function trackOutboundLink(label, url) {
    if(window.ga && ga.loaded) {
        ga('send', 'event', 'outbound', 'click', label, {'hitCallback': function(){document.location = url;}
        });
    } else {
        document.location = url;
    }
}
/**
* Function to track a click on a link in the article announcement area.
*/
$('#articleAnnouncement a').on('click', function(){
  ga('send', 'event', 'link', 'click', 'articleAnnouncement');
});
</script>

<script type="text/javascript">
 setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2421.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



<!-- Hotjar Tracking Code for http://nngroup.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:121605,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

        <script src="https://media.nngroup.com/static/js/analytics/track_outbound_link.js"></script>
        
        
        
    </head>
    <body class="location-articles articles-detail">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
    <header id="page-header">
        <div id="cart" class="shopping-cart mobile-cart show-for-small-down"></div>
        <div class="row">
            <div id="masthead" class="masthead small-12 medium-7 column">

                <!-- The button to toggles menu on small screen -->

                <h1><a href="../../index.php"><img src="http://media.nngroup.com/static/img/logo-wordmark.png" width="74" height="30"> Nielsen <span class="norman">Norman</span> Group</a></h1>
                <p class="tagline show-for-medium-up">Evidence-Based User Experience Research, Training, and Consulting</p>

                <!-- The button to toggles form search on small screen -->


                <div style="clear: right;width:  100%;height: 1px;"></div><?php define('ROOT_PATH',$_SERVER["DOCUMENT_ROOT"] . '/apps/navigation_styles/navigations/4'); ?><?php include(ROOT_PATH)."/navigation.php"; ?>

                <!-- Drop down menu on small screen -->
                <div id="hideNav" class="show-for-small-down">
                  
                  <ul class="menu-content">
                    <li><a href="../../index.php">Home</a></li>
                    <li class="has-submenu">
                      <a>Training</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../training/index.php">Overview</a></li>
                        
<li id="nav-ux-conference"><a href="../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../online-seminars/index.php"><span>Online Seminars</span></a></li>

                      </ul>
                    </li>
                    <li class="has-submenu">
                      <a>Consulting</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
  <li><a href="../../consulting/index.php">Overview</a></li>
  
  <li><a href="../../consulting/benchmarking/index.php">Benchmarking</a></li>
  
  <li><a href="../../consulting/expert-design-review/index.php">Expert Design Review</a></li>
  
  <li><a href="../../consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
  
  <li><a href="../../consulting/usability-testing/index.php">Usability Testing </a></li>
  
  <li><a href="../../consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
  
  <li><a href="../../in-house-training/index.php">In-House Training</a></li>
</ul>

                    </li>

                    <li><a href="../../reports/index.php">Reports</a></li>
                    <li><a href="../index.php">Articles</a></li>
                    <li class="has-submenu"><a>About NN/G</a><i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../about/index.php">Overview</a></li>
                        <li><a href="../../people/index.php">People</a></li>
                        <li><a href="../../about/why-nng/index.php">Why NN/g?</a></li>
                        <li><a href="../../about/contact/index.php">Contact</a></li>
                        <li><a href="../../news/index.php">News</a></li>
                        <li><a href="../../about/history/index.php">History</a></li>
                        <li><a href="../../books/index.php">Books</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                </div>

                <hr class="header-divider show-for-small-only"/>


            </div>
       </div>

        <div id="hideNav">            <nav class="primary-nav small-12 column">
                <ul>
                    <li><a class="navitem" id="nav-home" href="../../index.php">Home</a></li>
                    <li><a class="navitem" id="nav-training" href="../../training/index.php">Training</a></li>
                    <li><a class="navitem" id="nav-consulting" href="../../consulting/index.php">Consulting</a></li>
                    <li><a class="navitem" id="nav-reports" href="../../reports/index.php">Reports</a></li>
                    <li><a class="navitem" id="nav-articles" href="../index.php">Articles</a></li>
                    <li><a class="navitem" id="nav-about" href="../../about/index.php">About NN/G</a></li>
                </ul>
            </nav>
            <nav class="secondary-nav small-12 medium-12 large-12 columns">
                
                
            </nav>
        </div>
    </header>
    <div class="l-content">
    
  

  

  <div class="row">
    
      
      <section class="small-12 medium-3 columns collapsable-content l-navrail articles-left-column">
        <h1 class="show-for-small-down collapse-link mobile-browse-header">Browse by Topic and Author</h1>
          <div class="row collapsable">

            
            <div class="small-6 medium-12 columns">
              <section id="article-popular-topics" style="display: none;">
                
                <h1>Topics</h1>
                <ul id="article-popular-topics">
                    
                    <li>
                        
                        <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/intranets/all/index.php">Intranets</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/user-testing/all/index.php">User Testing</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                        
                    </li>
                    
                    <li><a href="#" id="article-see-all-topics"><strong>See all topics&hellip;</strong></a></li>
                </ul>
                
              </section>
              <section id="article-all-topics" style="display: none;">
                
                <h1>
                  <span class="show-for-medium-up">All Article Topics</span>
                  <span class="show-for-small-down">All Topics</span>
                  (<a href="#" id="article-hide-all-topics">hide</a>)
                </h1>
                <ul>
                  
                  <li>
                    
                    <a href="../../topic/accessibility/all/index.php">Accessibility</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/agile/all/index.php">Agile</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/analytics-and-metrics/all/index.php">Analytics &amp; Metrics</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/applications/all/index.php">Application Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/b2b-websites/all/index.php">B2B Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/behavior-patterns/all/index.php">Behavior Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/branding/all/index.php">Branding</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/content-strategy/all/index.php">Content Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/corporate-websites/all/index.php">Corporate Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/design-patterns/all/index.php">Design Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-design-process/all/index.php">Design Process</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/email/all/index.php">Email</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/eyetracking/all/index.php">Eyetracking</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/heuristic-evaluation/all/index.php">Heuristic Evaluation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/human-computer-interaction/all/index.php">Human Computer Interaction</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-ideation/all/index.php">Ideation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/information-architecture/all/index.php">Information Architecture</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/interaction-design/all/index.php">Interaction Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/international-users/all/index.php">International Users</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/intranets/all/index.php">Intranets</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-management/all/index.php">Management</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/navigation/all/index.php">Navigation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/non-profit-websites/all/index.php">Non-Profit Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/personas/all/index.php">Personas</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/persuasive-design/all/index.php">Persuasive Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/prototyping/all/index.php">Prototyping</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/psychology-and-ux/all/index.php">Psychology and UX</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/research-methods/all/index.php">Research Methods</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/search/all/index.php">Search</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/social-media/all/index.php">Social Media</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/strategy/all/index.php">Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/user-testing/all/index.php">User Testing</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-teams/all/index.php">UX Teams</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/visual-design/all/index.php">Visual Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/writing-web/all/index.php">Writing for the Web</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/young-users/all/index.php">Young Users</a>
                    
                  </li>
                  
                </ul>
                
              </section>
            </div>
            <script>
              /* Determine which topics list will be displayed: the full or the
               * partial.  We are inlining this close to the list itself to
               * prevent rendering delays on the client
               */
              var topic_list = $.cookie("article-topics");

              var popular_topics = $('#article-popular-topics');
              var all_topics = $('#article-all-topics');

              if(topic_list == 1) {
                  popular_topics.hide();
                  all_topics.show();
              } else {
                  popular_topics.show();
                  all_topics.hide();
              }
            </script>
            

            
            <div class="small-6 medium-12 columns">
              
              <section id="article-initial-authors">
                  <h1>Author</h1>
                  <ul>
                    
                    <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                    
                    <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                    
                    <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                    
                    <li><a href="#" onClick="$('#article-initial-authors').hide(); $('#article-all-authors').show();"><strong>See all authors&hellip;</strong></a></li>
                  </ul>
              </section>
              <section id="article-all-authors" style="display: none;">
                <h1>
                  All Authors
                  (<a href="#" id="article-hide-all-authors">hide</a>)
                </h1>
                <ul>
                  
                  <li><a href="../author/raluca-budiu/index.php">Raluca Budiu</a></li>
                  
                  <li><a href="../author/patty-caya/index.php">Patty Caya</a></li>
                  
                  <li><a href="../author/susan-farrell/index.php">Susan Farrell</a></li>
                  
                  <li><a href="../author/therese-fessenden/index.php">Therese Fessenden</a></li>
                  
                  <li><a href="../author/kim-flaherty/index.php">Kim Flaherty</a></li>
                  
                  <li><a href="../author/sarah-gibbons/index.php">Sarah Gibbons</a></li>
                  
                  <li><a href="../author/aurora-harley/index.php">Aurora Harley</a></li>
                  
                  <li><a href="../author/kate-kaplan/index.php">Kate Kaplan</a></li>
                  
                  <li><a href="../author/page-laubheimer/index.php">Page Laubheimer</a></li>
                  
                  <li><a href="../author/angela-li/index.php">Angie Li</a></li>
                  
                  <li><a href="../author/hoa-loranger/index.php">Hoa Loranger</a></li>
                  
                  <li><a href="../author/kate-meyer/index.php">Kate Meyer</a></li>
                  
                  <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                  
                  <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                  
                  <li><a href="../author/kara-pernice/index.php">Kara Pernice</a></li>
                  
                  <li><a href="../author/christian-rohrer/index.php">Christian Rohrer</a></li>
                  
                  <li><a href="../author/amy-schade/index.php">Amy Schade</a></li>
                  
                  <li><a href="../author/katie-sherwin/index.php">Katie Sherwin</a></li>
                  
                  <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                  
                  <li><a href="../author/kathryn-whitenton/index.php">Kathryn Whitenton</a></li>
                  
                </ul>
              </section>
              
            </div>
            
          </div>

          
          <section class="hide-for-small-down">
            
              
              <h1>Recent Articles</h1>
              
              <ul>
                  
                  <li><a href="../seamless-cross-channel/index.php">Seamlessness in the Omnichannel User Experience</a></li>
                  
                  <li><a href="../brand-intention-interpretation/index.php">Experience Design: Bridging Brand Intention and Brand Interpretation</a></li>
                  
                  <li><a href="../eyetracking-tasks-efficient-scanning/index.php">Scanning Patterns on the Web Are Optimized for the Current Task</a></li>
                  
                  <li><a href="../flat-design-best-practices/index.php">Flat-Design Best Practices</a></li>
                  
                  <li><a href="../ux-user-stories/index.php">Accounting for UX Work with User Stories in Agile Projects</a></li>
                  
                  <li><a href="../index.php"><strong>See all articles&hellip;</strong></a></li>
              </ul>
              
            
          </section>
          

          
          
          <section class="hide-for-small-down">
            <h1>Popular Articles</h1>
            
            <ul>
                
                <li><a href="../usability-101-introduction-to-usability/index.php">Usability 101: Introduction to Usability</a></li>
                
                <li><a href="../top-10-mistakes-web-design/index.php">Top 10 Mistakes in Web Design</a></li>
                
                <li><a href="../how-users-read-on-the-web/index.php">How Users Read on the Web</a></li>
                
                <li><a href="../f-shaped-pattern-reading-web-content/index.php">F-Shaped Pattern For Reading Web Content</a></li>
                
                <li><a href="../ten-usability-heuristics/index.php">10 Usability Heuristics for User Interface Design</a></li>
                
                <li><a href="../intranet-design-2016/index.php">10 Best Intranets of 2016</a></li>
                
                <li><a href="../which-ux-research-methods/index.php">When to Use Which User-Experience Research Methods</a></li>
                
                <li><a href="../response-times-3-important-limits/index.php">Response Times: The 3 Important Limits</a></li>
                
                <li><a href="../why-you-only-need-to-test-with-5-users/index.php">Why You Only Need to Test with 5 Users</a></li>
                
                <li><a href="../page-fold-manifesto/index.php">The Fold Manifesto: Why the Page Fold Still Matters</a></li>
                
            </ul>
          </section>
          
          

          
            <section class="hide-for-small-down">
              
            </section>
          

      </section>
      

      
        <div class="small-12 medium-9 columns content-body">
          




<article class="l-subsection alertbox-article-content article-detail-bl">
  
  <header>
    <div class="l-subsection">
      <h1>Indicators, Validations, and Notifications: Pick the Correct Communication Option</h1>
      <div class="author-meta">by
        
          
            
            <a href="../author/kim-flaherty/index.php">Kim Flaherty</a>
            
          
        on  July 26, 2015
        
      </div>

      

      
      <div class="topic-links">
        Topics:
        
          <ul>

  <li><a href="../../topic/applications/index.php">Application Design</a></li>

</ul>
        
      </div>
      
    </div>
  </header>
  

  
  
  <section>
    <p class="lede"><strong>Summary:</strong> Status feedback is crucial to the success of any system. Knowing when to use 3 common communication methods is key to supporting users.</p>
    <br />
  </section>
  
  

  
  <section id="article-body"><p>In interaction design, a system, whether an application, website, or piece of hardware (anything from a <a href="../smartwatch/index.php">smartwatch</a> to a <a href="../emotional-design-fail/index.php">thermostat</a>), should always <strong>keep users informed</strong>, by providing appropriate feedback. Ensuring that the state of the system is always visible is one of the <a href="../ten-usability-heuristics/index.php">10 usability heuristics for interface design</a>. Information about system status, such as error messages and notifications of system activity, allows users to fully understand the current context.</p>

<p>The best way to communicate system status varies depending on several key factors:</p>

<ul>
	<li>The type of information being communicated</li>
	<li>The urgency of the information — how important it is that the user sees it immediately</li>
	<li>Whether the user needs to take action as a result of the information</li>
</ul>

<p>Three common approaches for status communication include validation, notifications, and status indicators. These terms are used sometimes interchangeably in product design, but they stand for different communication methods that should be used in different circumstances. Understanding the differences between them will help you sharpen your feedback to users by choosing the best option for each need.</p>

<h2>Indicators</h2>

<p style="margin-left:1.0pt;">An indicator is a way of making a page element (be it content or part of the user interface) <strong>stand out to inform the user that there is something special</strong> about it that warrants the user’s attention. Often, the indicator will denote that there has been some change to the item represented by that element.</p>

<p style="margin-left:1.0pt;">Although, as we’ll see later, indicators are used quite frequently to signal validation errors or notifications, they can also be used on their own.  Indicators are visual cues intended to attract users’ attention to a particular piece of content or UI element that is dynamic in nature.  (If something always looks the same, it’s not an indicator, no matter how flamboyantly it’s designed.)</p>

<p style="margin-left:1.0pt;">There are at least three possible ways of implementing indicators:</p>

<ul>
	<li>Oftentimes, but not always, indicators are implemented as <strong>icons</strong>. Easily <a href="../icon-usability/index.php">recognizable icons</a> can make very effective communication tools.</li>
	<li><strong>Typographical</strong> variations can also be used as indicators; examples include the common convention of boldfacing unread email messages or color-coding stock symbols in an investment account if their price has changed substantially.</li>
	<li>Though less common, enlarged <strong>size </strong>or<strong> animation</strong> (e.g., vibration) can also be used to make certain items stand out from the crowd and thus serve as an indicator.          </li>
</ul>

<figure class="caption"><img alt="" height="565" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2015/07/14/yelp_indicator.png" width="350"/>
<figcaption><em>Yelp used a green-tag indicator in the search results to indicate that Tea Market had a special deal running. This indicator communicated additional information about Tea Market.</em></figcaption>
</figure>

<p style="margin-left:.25in;"><strong>Characteristics of indicators:</strong></p>

<ul>
	<li>Indicators are <strong>contextual</strong>. They are associated with a UI element or with a piece of content, and should be shown in close proximity to that element.</li>
	<li>Indicators are <strong>conditional</strong>— they are not always present, but appear or change depending on certain conditions. For example, a stock-performance indicator, such as the one in the American Century example below, may change to indicate if the stock price is increasing or decreasing.  Additionally, the tag indicator in the Yelp example above only appears if there is a deal at that business.</li>
	<li>Indicators are <strong>passive</strong>.  They do not require that a user take action, but are used as a communication tool to cue the user to something of note.</li>
</ul>

<figure class="caption"><img alt="" height="338" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2015/07/14/americancenturyinv_indicator.png" width="720"/>
<figcaption><em>American Century Investments used a conditional indicator to provide information regarding a specific stock’s performance. When the daily change was negative, the indicator was a red arrow pointing down. When the daily change was positive, it showed a green arrow pointing up. The condition of the stock performance impacted the indicator that was shown next to the price.</em></figcaption>
</figure>

<p>Indicators can introduce noise and clutter to your overall interface, and may distract users, so it is important to consider how many (if any) indicators to use in your design.</p>

<p>Consider the following when deciding if an indicator is appropriate:</p>

<ul>
	<li>How important is the information to the user? Is it worth taking up space on the page to inform the user?
	<ul style="list-style-type:circle;">
		<li>How often is the information used?</li>
		<li>Would the user expect to see the information?</li>
		<li>Would it be missed if it weren’t provided?</li>
	</ul>
	</li>
	<li>How important is it for the application that the user discovers the information?</li>
</ul>

<h2><span style="line-height: 1.2;">Validations</span></h2>

<p>Validation messages are <strong>error messages</strong> related to users’ inputs: they communicate whether the data just entered was incomplete or incorrect. For example, in e-commerce systems, validation messages often involve information such as name, billing address, and credit-card information.  </p>

<figure class="caption"><img alt="" border="0" height="473" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2015/07/14/cobragolf_validation.png" width="720"/>
<figcaption><em>Cobragolf.com provided a validation message that clearly indicated which field was in error. The message however was complex and confusing.  A better message might say</em> “Please enter a valid email address.”</figcaption>
</figure>

<p><strong style="line-height: 1.6;">      Characteristics of validation:</strong></p>

<ul>
	<li>A user needs to take action to clear the validation message.</li>
	<li>The information in the validation message is contextual and applies to a specific user input that has a problem.</li>
</ul>

<p>The way in which validation should be implemented varies based on the unique needs of the form.  However, in general, if the user’s input is incorrect, the system should inform the user by providing an identifiable and clear message that aids in correcting the error. Validation messages should follow the <a href="../error-message-guidelines/index.php">guidelines for error messages</a><span style="line-height: 1.6;"> rather than simply identifying the problem, they should tell users how to fix it.  For instance,  don’t  state “Field is blank.” Please enter your street address” is more polite and directs to a solution.</span></p>

<p>Since validation is contextual, it can be helpful to use an <strong>icon indicator</strong> along with the validation message to help communicate which input(s) are missing or need corrected.</p>

<figure class="caption"><img alt="" border="0" height="316" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2015/07/14/bestbuy_validation.png" width="720"/>
<figcaption><em>Bestbuy.com provided a helpful validation message telling users how to fix the problem and also used an icon indicator and a different color to attract users’ attention to the field that needed correction.</em></figcaption>
</figure>

<h2><span style="line-height: 1.2;">Notifications</span></h2>

<p>Notifications are informational messages that <strong>alert the user of general occurrences within a system</strong>. Unlike validation, notifications may <strong>not be directly tied to user input</strong> or even to the user’s current activity in the system, but they usually inform the user of a change in the system state or of an event that may be of interest. In the case of email, social networks, and mobile-phone applications, notifications can even be delivered while a user is away from the application.  </p>

<p>Notifications can be <strong>contextual</strong> —applying to a specific UI element— or <strong>global </strong>—applying to the system as a whole.</p>

<figure class="caption"><img alt="" border="0" height="563" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2015/07/14/facebookapp_notification.png" width="350"/>
<figcaption><em>The Facebook App used a contextual notification in the news feed to communicate that newer stories had been added to the feed. This notification does not require the user to take action.</em></figcaption>
</figure>

<p><strong style="line-height: 1.6;">      Characteristics of notifications:</strong></p>

<ul>
	<li>They are not triggered by users’ immediate actions.</li>
	<li>They announce an event that has some significance to the user.</li>
</ul>

<p>There are two main types of notifications, which differ based on whether the user is required to act upon the notification:</p>

<ul>
	<li><strong>Action-required notifications</strong> alert the user of an event that requires a user action. In this sense, they are similar to validation, but since they were not sparked by the user’s own action, they require a different design.  </li>
</ul>

<p> Action-required notifications are often urgent and should be intrusive; for instance, they could be implemented as modal popups that interrupt the user, forcing immediate attention and requiring an action to be dismissed.</p>

<figure class="caption"><img alt="" border="0" height="339" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2015/07/14/osx-notification.png" width="720"/>
<figcaption><em>The Mac operating system used a notification to inform users of available system updates. The user had to explicitly dismiss it from the screen, by opting to either install the update or to be reminded again at a later time. This is an intrusive notification that requires that the user take action.</em></figcaption>
</figure>

<ul>
	<li><strong style="line-height: 1.6;">Passive notifications </strong><span style="line-height: 1.6;">are informational; they report a system occurrence that does not require any user action.  Many notifications in mobile apps are passive: they usually announce an event of potential interest to the user.</span></li>
</ul>

<p style="margin-left:.25in;">Passive notifications are typically not urgent and should be less intrusive. A typical implementation of a passive notification may be a badge icon or a small nonmodal popover in a corner of a screen.  Passive notifications can easily be missed, since they require no user action. When the information provided by the notification is key to the understanding of the system, an easy-to-ignore passive notification can be problematic.</p>

<figure class="caption"><img alt="" border="0" height="439" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2015/07/14/creativecloud_notification.png" width="720"/>
<figcaption><em>Adobe Creative Cloud used a nonintrusive passive notification to inform the user of an available application update. This notification appeared on screen for several seconds before disappearing. The user did not need to take any action on it.</em></figcaption>
</figure>

<p> </p>

<figure class="caption"><img alt="" border="0" height="232" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2015/07/14/uniqlo_notification.png" width="720"/>
<figcaption><em>Uniqlo.com used a nonintrusive passive notification to provide feedback that an item was added to the shopping cart. No action was necessary to dismiss the notification. Such notifications sometimes cause issues for e-commerce shoppers who do not notice the brief messaging. Users who miss the message may respond by adding an item to the cart multiple times, or by disrupting their shopping flow to check the cart to see what items were added.</em></figcaption>
</figure>

<p>Notifications have the design challenge that they are not the immediate and obvious result of a specific user action. On the contrary, the user is likely in the middle of doing something different and may not be thinking about the issue raised by the notification. This requires notifications to establish more context and provide users with sufficient background information to understand what the notification is about.</p>

<p>(In contrast, with a validation, the user has just done the thing that needs to be corrected. Thus, the validation message doesn’t need to educate users about the task at hand. For example, if an e-commerce checkout form has a field for a credit-card expiration date that was left blank, the validation message doesn’t need to say <em>“Please provide the expiration date for the credit card you want to have charged $29.90 to pay for the blue sleeveless dress you are in the process of buying on Uniqlo.com.” </em>However, a notification the following day that the dress has been shipped from the warehouse would need to say more than <em>“Your package has shipped.”</em>)</p>

<p>If a notification is contextual and relates to a specific element in the interface, an icon indicator on the element can communicate where that notification applies and catch the user’s attention.  For instance, an indicator badge on a mobile-app icon shows that the user has received a notification from the corresponding app.</p>

<figure class="caption"><img alt="" border="0" height="427" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2015/07/14/iphonemessaging_notification.png" width="720"/>
<figcaption><em>The iPhone messaging app created a notification to communicate that a new message was received. Along with the notification, an indicator badge was placed on the messaging-app icon to communicate where the notification applied. To clear the indicator, the user had to view the message.</em></figcaption>
</figure>

<p> </p>

<figure class="caption"><img alt="" border="0" height="438" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2015/07/14/mint_validation.png" width="720"/>
<figcaption><em>Mint.com used an indicator together with a notification to communicate that an account needed attention. The warning indicator </em>(1)<em> appeared in close proximity to the summary of the account that needed attention, while the notification </em>(2) <em>appeared in the central area of the page with other important information. The actual text in the notification message could have been more helpful, though.</em></figcaption>
</figure>

<h2><span style="line-height: 1.2;">Picking the Right Communication Option Is Important</span></h2>

<p>Using the wrong communication method can have a negative impact on the users’ experience. Let’s refer back to the scenario above where Yelp utilized a green-tag indicator in the search results to indicate that Tea Market had a special deal running. This information is contextual and important to users who have specifically searched for a place to have tea.</p>

<p>You may think that an alternative way of alerting users of potential tea deals would be to send them a notification when such a deal has become available. Wrong! A notification sent irrespective of the current user goal would likely be ignored, and may even annoy users because it will disrupt their current task and be irrelevant to their current needs.</p>

<p>(In general, <a href="../making-web-advertisements-work/index.php">any type of ad tends to be ignored unless it is related to the users’ aims and mindset</a>.)</p>

<p>Alternatively, a toast (a small nonmodal popup that disappears after a few seconds, like the <em>New Stories </em>one used by the Facebook app), while appropriate for passive notifications, would be a bad way to implement an error message, be it validation or otherwise. In fact, one of our mobile users spent 5 minutes waiting for some content to load only because she hadn’t notice the little error message presented at the bottom of the screen that quickly faded away after 5 seconds.  </p>

<figure class="caption"><img alt="" border="0" height="458" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2015/07/14/allmodern_notification.png" width="720"/>
<figcaption><em>Allmodern.com used an action-required notification to communicate that a product was saved as a favorite.  For a user that is saving a lot of items to their favorites, this can be a bothersome and intrusive way of providing feedback. This may be better communicated by showing a passive nonmodal popover in the corner of the screen that can be seen, but doesn’t require the user to take action to clear it.</em></figcaption>
</figure>

<h2><span style="line-height: 1.2;">Conclusion</span></h2>

<p>Remember the key differences between the three communication methods are:</p>

<ul>
	<li>Indicators provide supplementary information about a dynamic piece of content or UI element. They are conditional —that is, they may appear or change under specific conditions.</li>
	<li>Validations are tied to a user’s action or input.</li>
	<li>Notifications are focused on system-related events.</li>
</ul>

<p>These differences are summarized in the table below:</p>

<table border="1" cellpadding="0" cellspacing="0" style="width:619px;" width="464">
	<tbody>
		<tr>
			<td style="width:235px;height:20px;"> </td>
			<td style="width:129px;height:20px;">
			<p align="center"><strong>Validation</strong></p>
			</td>
			<td style="width:143px;height:20px;">
			<p align="center"><strong>Notifications</strong></p>
			</td>
			<td style="width:112px;height:20px;">
			<p align="center"><strong>Indicators</strong></p>
			</td>
		</tr>
		<tr>
			<td style="width:235px;height:20px;">
			<p>Global vs. contextual: Related to a global system event or to a particular page element</p>
			</td>
			<td style="width:129px;height:20px;">
			<p align="center">Contextual</p>
			</td>
			<td style="width:143px;height:20px;">
			<p align="center">Global or contextual</p>
			</td>
			<td style="width:112px;height:20px;">
			<p align="center">Contextual</p>
			</td>
		</tr>
		<tr>
			<td style="width:235px;height:20px;">
			<p>Passive vs. requiring a user action</p>
			</td>
			<td style="width:129px;height:20px;">
			<p align="center">Action required</p>
			</td>
			<td style="width:143px;height:20px;">
			<p align="center">Action required or passive</p>
			</td>
			<td style="width:112px;height:20px;">
			<p align="center">Passive</p>
			</td>
		</tr>
		<tr>
			<td style="width:235px;height:20px;">
			<p>Triggered by user action vs. system event</p>
			</td>
			<td style="width:129px;height:20px;">
			<p align="center">User action</p>
			</td>
			<td style="width:143px;height:20px;">
			<p align="center">System event</p>
			</td>
			<td style="width:112px;height:20px;">
			<p align="center">User action or system event</p>
			</td>
		</tr>
	</tbody>
</table>

<p>Understanding when and how to use each of these feedback tools is important in order to build consistency in the communication to users. By assessing the type of information delivered, we can determine the correct mechanism to use.</p>
</section>
  

  
  <section>
    
        
    
  </section>
  
  
  
  
  

  


  <section>
    <br />
    <p class="share"><b>Share <span class="hide-for-small-down">this article</span>:</b> <a href="https://twitter.com/intent/tweet?url=http://www.nngroup.com/articles/indicators-validations-notifications/&amp;text=Indicators,%20Validations,%20and%20Notifications:%20Pick%20the%20Correct%20Communication%20Option&amp;via=nngroup">Twitter</a> | <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=http://www.nngroup.com/articles/indicators-validations-notifications/&amp;title=Indicators,%20Validations,%20and%20Notifications:%20Pick%20the%20Correct%20Communication%20Option&amp;source=Nielsen%20Norman%20Group">LinkedIn</a> | <a href="https://plus.google.com/share?url=http://www.nngroup.com/articles/indicators-validations-notifications/">Google+</a> | <a href="mailto:?subject=NN/g Article: Indicators, Validations, and Notifications: Pick the Correct Communication Option&amp;body=http://www.nngroup.com/articles/indicators-validations-notifications/">Email</a></p>
    <hr />
  </section>

  <footer class="l-subsection">
    
    
    <div class="learn-more">
      <h2>Learn More</h2>
      <p><a href="../subscribe/index.php">Subscribe to the weekly newsletter</a> to get notified about future articles.</p>
      </div>
    
    

    <div class="row show-for-small-down related-content">
      
      
      

      
      
        <div class="column small-12 topic-links">All articles about:
          <ul>
            
            <li><a href="../../topic/applications/index.php">Application Design</a></li>
            
            <li><a href="../../topic/standards-conventions/index.php">standards &amp; conventions</a></li>
            
          </ul>
        </div>
      
      

      
      
        <div class="column small-12">
          <h3>UX Conference Training Courses</h3>
           <ul class="no-bullet">
    
        <li><a href="../../courses/hci/index.php">User Interface Principles Every Designer Must Know</a></li>
    
        <li><a href="../../courses/application-ux/index.php">Application Design for Web and Desktop</a></li>
    
        <li><a href="../../courses/emotional-design/index.php">Emotional Design</a></li>
    
</ul>
        </div>
      
      

      
      
        <div class="small-12 column">
          <h3>Research Reports</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../reports/best-applications-2/index.php">Application Design Showcase: 2012</a></li>
              
            
              
                <li><a href="../../reports/enterprise-mobile-showcase/index.php">Mobile Intranets and Enterprise Apps</a></li>
              
            
              
                <li><a href="../../reports/website-tools-and-applications-flash/index.php">Website Tools and Applications with Flash</a></li>
              
            
              
                <li><a href="../../reports/customization-features/index.php">Customization Features Done Correctly for the Right Reasons</a></li>
              
            
              
                <li><a href="../../reports/best-applications-1/index.php">Application Design Showcase: 2008</a></li>
              
            
          </ul>
        </div>
      
      

      
      
        <div class="small-12 column">
          <h3>Online Seminars</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../online-seminars/ux-big-data/index.php">UX for Big Data</a></li>
              
            
          </ul>
        </div>
      
      
    </div>

    <div class="hide-for-small-down row related-content">
      <div class="medium-6 columns">
        
        
          <div>
            <h3>Articles</h3>
            <ul class="no-bullet">
              
                
                <li><a href="../tabs-used-right/index.php">Tabs, Used Right</a></li>
                
              
                
                <li><a href="../ok-cancel-or-cancel-ok/index.php">OK-Cancel or Cancel-OK?</a></li>
                
              
                
                <li><a href="../defeated-by-a-dialog-box/index.php">Defeated By a Dialog Box</a></li>
                
              
                
                <li><a href="../passwords-memory/index.php">Help People Create Passwords That They Can Actually Remember</a></li>
                
              
                
                <li><a href="../progress-indicators/index.php">Progress Indicators Make a Slow System Less Insufferable</a></li>
                
              
            </ul>
          </div>
        
        

        
          
          <div>
            <h3>Research Reports</h3>
            <ul class="no-bullet">
              
                
                  <li><a href="../../reports/best-applications-2/index.php">Application Design Showcase: 2012</a></li>
                
              
                
                  <li><a href="../../reports/enterprise-mobile-showcase/index.php">Mobile Intranets and Enterprise Apps</a></li>
                
              
                
                  <li><a href="../../reports/website-tools-and-applications-flash/index.php">Website Tools and Applications with Flash</a></li>
                
              
                
                  <li><a href="../../reports/customization-features/index.php">Customization Features Done Correctly for the Right Reasons</a></li>
                
              
                
                  <li><a href="../../reports/best-applications-1/index.php">Application Design Showcase: 2008</a></li>
                
              
            </ul>
          </div>
          
        

      </div>
      <div class="medium-6 columns">
        
          
          <div>
            <h3>UX Conference Training Courses</h3>
             <ul class="no-bullet">
    
        <li><a href="../../courses/hci/index.php">User Interface Principles Every Designer Must Know</a></li>
    
        <li><a href="../../courses/application-ux/index.php">Application Design for Web and Desktop</a></li>
    
        <li><a href="../../courses/emotional-design/index.php">Emotional Design</a></li>
    
</ul>
          </div>
          
        
        
          
          <div>
            <h3>Online Seminars</h3>
            <ul class="no-bullet">
            
                <li><a href="../../online-seminars/ux-big-data/index.php">UX for Big Data</a></li>
            
            </ul>
          </div>
          
        
      </div>
    </div>
  </footer>
</article>

        </div>

      

    
  </div>
  <div class="row">
    
      <div class="column small-12 show-for-small-down l-navrail announcements">
        
          
        
      </div>
    
    
  </div>

    </div>
    




<footer id="page-footer">
    <div class="row hide-for-print">
        <div class="column small-12 medium-6">
            <section class="footer-popular">
                
                <h1>Popular Topics</h1>
                <div style="float: left;  width: 15em; display: block;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/applications/index.php">Application Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/e-commerce/index.php">E-Commerce Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/eyetracking/index.php">Eyetracking </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/information-architecture/index.php">Information Architecture </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/intranets/index.php">Intranet Design </a></li>
	<li><a href="../../topic/interaction-design/index.php">Interaction Design </a></li>
</ul>
</div>

<div style="float: left;  width: 15em; display: block; list-style: none;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/mobile-and-tablet-design/index.php">Mobile and Tablet Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/credibility-persuasion/index.php">Persuasive Design</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/research-methods/index.php">Research Methods</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/user-testing/index.php">User Testing </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/web-usability/index.php">Web Usability </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/writing-web/index.php">Web Writing &amp; Content Strategy </a></li>
</ul>
</div>

<p>&nbsp;</p>

                
            </section>
        </div>

        <div class="column small-12 medium-6">
            <div class="row">
                <div class="column small-6">
                    <section class="footer-about">
                        <h1>Follow Us</h1>
                        <ul>
	<li><a href="../subscribe/index.php">Subscribe to our weekly UX newsletter</a>,&nbsp;Jakob Nielsen&#39;s Alertbox</li>
	<li>Visit Don Norman&#39;s website, <a href="http://www.jnd.org/"> www.jnd.org </a></li>
	<li>Visit Bruce Tognazzini&#39;s website, <a href="http://www.asktog.com/"> www.asktog.com </a></li>
	<li style="padding-top: 6px;"><a class="twitter-follow-button" data-show-count="false" data-size="large" href="https://twitter.com/nngroup"><img alt="Follow @nngroup on Twitter" height="20" src="https://media.nngroup.com/media/editor/2016/12/30/twitter-follow-button.png" style="width: 117px; height: 20px;" width="117" /></a></li>
	<li style="padding-top: 6px;"><img alt="" height="14" src="https://media.nngroup.com/media/editor/2016/07/26/feed-icon-14x14.png" style="vertical-align:text-top;width:14px;margin-right:.25em;" width="14" /><a href="../../feed/rss/index.php">Subscribe to our RSS feed</a></li>
</ul>

                    </section>
                </div>

                <div class="column small-6">
                    <section class="footer-about right-column">
                        <h1>About
</h1>
                        <ul class="no-bullet">
	<li><a href="../../about/contact/index.php">Contact information</a></li>
	<li><a href="../../about/index.php">About Us</a></li>
	<li><a href="../../about/why-nng/index.php">Why NN/g? </a></li>
	<li><a href="../../return-policy/index.php">Return Policy</a></li>
</ul>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="copyright column small-12">
            <p><a href="../../copyright-and-reprint-info/index.php">Copyright</a> &copy; 1998-2017 Nielsen Norman Group, All Rights Reserved.</p>
        </div>
    </div>
</footer>


    


        <script src="https://media.nngroup.com/static/vendor/fastclick/js/fastclick.min.js"></script>
        <script src="https://media.nngroup.com/static/vendor/foundation/js/foundation.min.js"></script>
        <script src="http://media.nngroup.com/static/js/app.js"></script>
        <script src="http://media.nngroup.com/static/js/scroll-to-fixed.js"></script><script>var externalLinks = document.querySelectorAll('a[href^="http"]');for (var i = externalLinks.length-1; i >= 0; i--) {externalLinks[i].addEventListener("click", function(event) { alert("External links are disabled for this test."); event.preventDefault(); }, false);}var mailLinks = document.querySelectorAll('a[href^="mailto"]');for (var i = mailLinks.length-1; i >= 0; i--) {mailLinks[i].addEventListener("click", function(event) { alert("Email links are disabled for this test."); event.preventDefault(); }, false);}var externalForms = document.querySelectorAll('form');for (var i = externalForms.length-1; i >= 0; i--) {externalForms[i].addEventListener("submit", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false); externalForms[i].addEventListener("click", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false);}</script>

        <script>
            
            $(function() {
                $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
                $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
            });
        </script>

        
<script src="https://media.nngroup.com/static/js/articles.js"></script>


        
        <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1072699960;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072699960/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
    </body>

<!-- Mirrored from www.nngroup.com/articles/indicators-validations-notifications/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:06:22 GMT -->
</html>
