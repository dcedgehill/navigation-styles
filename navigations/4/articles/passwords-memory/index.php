<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->





    
<!-- Mirrored from www.nngroup.com/articles/passwords-memory/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:06:24 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1026.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"licenseKey":"1a30ef9931","errorBeacon":"bam.nr-data.net","applicationID":"2048992","transactionName":"NFxSMRBUX0EDUkIPDA0WdhAMVkVbDV8ZBxEXUFMJB0YfRAtUQRVZIktEDAFZVHYHRVcPD01eVRE=","beacon":"bam.nr-data.net","queueTime":3,"applicationTime":407,"agent":""}</script>
        <title>Help People Create Passwords That They Can Actually Remember</title><meta property="og:title" content="Help People Create Passwords That They Can Actually Remember" />
  
        
        <meta name="description" content="Human memory studies can inform design so they help people remember passwords. This makes customers happy, saves money and time, and increases security.">
        <meta property="og:description" content="Human memory studies can inform design so they help people remember passwords. This makes customers happy, saves money and time, and increases security." />
        
  
        
	
        
        <meta name="keywords" content="password, passwords, security, secure password, memory, human memory, encoding, encode, storage, store, remember, retrieve, retrieval, activation, distraction, focus, workflow, interruption, recognition, recall, gamification, repetition, rehearse, rehearsing, repeat, chunk, chunking, chunks, magical number 7, Miller, Baddeley, Thompson, Buchanan">
        

        

        <meta name="robots" content="noarchive">
        <meta name="viewport" content="width=device-width">

        <link rel="search" type="application/opensearchdescription+xml" title="NN/g Search" href="../../opensearch.xml" />
        <link rel="shortcut icon" href="http://media.nngroup.com/static/img/favicon.ico" />

        <link rel="stylesheet" href="https://media.nngroup.com/static/vendor/font-awesome/css/font-awesome.min.css">

        
        <link rel="stylesheet" href="https://media.nngroup.com/static/css/7232a960f1df.css" type="text/css" />
        <link rel="stylesheet" href="http://fast.fonts.com/t/1.css?apiType=css&amp;projectid=c5846260-9def-46ec-9407-0f1349bd2d64" type="text/css">

        <script src="https://media.nngroup.com/static/vendor/modernizr/js/modernizr.min.js"></script>

        <script type="text/javascript" src="https://media.nngroup.com/static/js/3283be5def74.js"></script>
        

        

        
        
        <!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=229034,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('http://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->


<!-- Google Analytics Tracking Code for http://nngroup.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23132681-1', 'nngroup.com');
  ga('require', 'linkid', 'https://www.nngroup.com/articles/passwords-memory/linkid.js');
  ga('require', 'displayfeatures');

// Optimizely Universal Analytics Integration
 window.optimizely = window.optimizely || [];
 window.optimizely.push("activateUniversalAnalytics");

  ga('send', 'pageview');

</script>
<script>
/**
* Function to track a click on an outbound link in Google Analytics.
*/
function trackOutboundLink(label, url) {
    if(window.ga && ga.loaded) {
        ga('send', 'event', 'outbound', 'click', label, {'hitCallback': function(){document.location = url;}
        });
    } else {
        document.location = url;
    }
}
/**
* Function to track a click on a link in the article announcement area.
*/
$('#articleAnnouncement a').on('click', function(){
  ga('send', 'event', 'link', 'click', 'articleAnnouncement');
});
</script>

<script type="text/javascript">
 setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2421.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



<!-- Hotjar Tracking Code for http://nngroup.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:121605,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

        <script src="https://media.nngroup.com/static/js/analytics/track_outbound_link.js"></script>
        
        
        
    </head>
    <body class="location-articles articles-detail">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
    <header id="page-header">
        <div id="cart" class="shopping-cart mobile-cart show-for-small-down"></div>
        <div class="row">
            <div id="masthead" class="masthead small-12 medium-7 column">

                <!-- The button to toggles menu on small screen -->

                <h1><a href="../../index.php"><img src="http://media.nngroup.com/static/img/logo-wordmark.png" width="74" height="30"> Nielsen <span class="norman">Norman</span> Group</a></h1>
                <p class="tagline show-for-medium-up">Evidence-Based User Experience Research, Training, and Consulting</p>

                <!-- The button to toggles form search on small screen -->


                <div style="clear: right;width:  100%;height: 1px;"></div><?php define('ROOT_PATH',$_SERVER["DOCUMENT_ROOT"] . '/apps/navigation_styles/navigations/4'); ?><?php include(ROOT_PATH)."/navigation.php"; ?>

                <!-- Drop down menu on small screen -->
                <div id="hideNav" class="show-for-small-down">
                  
                  <ul class="menu-content">
                    <li><a href="../../index.php">Home</a></li>
                    <li class="has-submenu">
                      <a>Training</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../training/index.php">Overview</a></li>
                        
<li id="nav-ux-conference"><a href="../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../online-seminars/index.php"><span>Online Seminars</span></a></li>

                      </ul>
                    </li>
                    <li class="has-submenu">
                      <a>Consulting</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
  <li><a href="../../consulting/index.php">Overview</a></li>
  
  <li><a href="../../consulting/benchmarking/index.php">Benchmarking</a></li>
  
  <li><a href="../../consulting/expert-design-review/index.php">Expert Design Review</a></li>
  
  <li><a href="../../consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
  
  <li><a href="../../consulting/usability-testing/index.php">Usability Testing </a></li>
  
  <li><a href="../../consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
  
  <li><a href="../../in-house-training/index.php">In-House Training</a></li>
</ul>

                    </li>

                    <li><a href="../../reports/index.php">Reports</a></li>
                    <li><a href="../index.php">Articles</a></li>
                    <li class="has-submenu"><a>About NN/G</a><i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../about/index.php">Overview</a></li>
                        <li><a href="../../people/index.php">People</a></li>
                        <li><a href="../../about/why-nng/index.php">Why NN/g?</a></li>
                        <li><a href="../../about/contact/index.php">Contact</a></li>
                        <li><a href="../../news/index.php">News</a></li>
                        <li><a href="../../about/history/index.php">History</a></li>
                        <li><a href="../../books/index.php">Books</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                </div>

                <hr class="header-divider show-for-small-only"/>


            </div>
       </div>

        <div id="hideNav">            <nav class="primary-nav small-12 column">
                <ul>
                    <li><a class="navitem" id="nav-home" href="../../index.php">Home</a></li>
                    <li><a class="navitem" id="nav-training" href="../../training/index.php">Training</a></li>
                    <li><a class="navitem" id="nav-consulting" href="../../consulting/index.php">Consulting</a></li>
                    <li><a class="navitem" id="nav-reports" href="../../reports/index.php">Reports</a></li>
                    <li><a class="navitem" id="nav-articles" href="../index.php">Articles</a></li>
                    <li><a class="navitem" id="nav-about" href="../../about/index.php">About NN/G</a></li>
                </ul>
            </nav>
            <nav class="secondary-nav small-12 medium-12 large-12 columns">
                
                
            </nav>
        </div>
    </header>
    <div class="l-content">
    
  

  

  <div class="row">
    
      
      <section class="small-12 medium-3 columns collapsable-content l-navrail articles-left-column">
        <h1 class="show-for-small-down collapse-link mobile-browse-header">Browse by Topic and Author</h1>
          <div class="row collapsable">

            
            <div class="small-6 medium-12 columns">
              <section id="article-popular-topics" style="display: none;">
                
                <h1>Topics</h1>
                <ul id="article-popular-topics">
                    
                    <li>
                        
                        <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/intranets/all/index.php">Intranets</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/user-testing/all/index.php">User Testing</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                        
                    </li>
                    
                    <li><a href="#" id="article-see-all-topics"><strong>See all topics&hellip;</strong></a></li>
                </ul>
                
              </section>
              <section id="article-all-topics" style="display: none;">
                
                <h1>
                  <span class="show-for-medium-up">All Article Topics</span>
                  <span class="show-for-small-down">All Topics</span>
                  (<a href="#" id="article-hide-all-topics">hide</a>)
                </h1>
                <ul>
                  
                  <li>
                    
                    <a href="../../topic/accessibility/all/index.php">Accessibility</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/agile/all/index.php">Agile</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/analytics-and-metrics/all/index.php">Analytics &amp; Metrics</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/applications/all/index.php">Application Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/b2b-websites/all/index.php">B2B Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/behavior-patterns/all/index.php">Behavior Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/branding/all/index.php">Branding</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/content-strategy/all/index.php">Content Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/corporate-websites/all/index.php">Corporate Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/design-patterns/all/index.php">Design Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-design-process/all/index.php">Design Process</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/email/all/index.php">Email</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/eyetracking/all/index.php">Eyetracking</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/heuristic-evaluation/all/index.php">Heuristic Evaluation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/human-computer-interaction/all/index.php">Human Computer Interaction</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-ideation/all/index.php">Ideation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/information-architecture/all/index.php">Information Architecture</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/interaction-design/all/index.php">Interaction Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/international-users/all/index.php">International Users</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/intranets/all/index.php">Intranets</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-management/all/index.php">Management</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/navigation/all/index.php">Navigation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/non-profit-websites/all/index.php">Non-Profit Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/personas/all/index.php">Personas</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/persuasive-design/all/index.php">Persuasive Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/prototyping/all/index.php">Prototyping</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/psychology-and-ux/all/index.php">Psychology and UX</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/research-methods/all/index.php">Research Methods</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/search/all/index.php">Search</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/social-media/all/index.php">Social Media</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/strategy/all/index.php">Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/user-testing/all/index.php">User Testing</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-teams/all/index.php">UX Teams</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/visual-design/all/index.php">Visual Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/writing-web/all/index.php">Writing for the Web</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/young-users/all/index.php">Young Users</a>
                    
                  </li>
                  
                </ul>
                
              </section>
            </div>
            <script>
              /* Determine which topics list will be displayed: the full or the
               * partial.  We are inlining this close to the list itself to
               * prevent rendering delays on the client
               */
              var topic_list = $.cookie("article-topics");

              var popular_topics = $('#article-popular-topics');
              var all_topics = $('#article-all-topics');

              if(topic_list == 1) {
                  popular_topics.hide();
                  all_topics.show();
              } else {
                  popular_topics.show();
                  all_topics.hide();
              }
            </script>
            

            
            <div class="small-6 medium-12 columns">
              
              <section id="article-initial-authors">
                  <h1>Author</h1>
                  <ul>
                    
                    <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                    
                    <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                    
                    <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                    
                    <li><a href="#" onClick="$('#article-initial-authors').hide(); $('#article-all-authors').show();"><strong>See all authors&hellip;</strong></a></li>
                  </ul>
              </section>
              <section id="article-all-authors" style="display: none;">
                <h1>
                  All Authors
                  (<a href="#" id="article-hide-all-authors">hide</a>)
                </h1>
                <ul>
                  
                  <li><a href="../author/raluca-budiu/index.php">Raluca Budiu</a></li>
                  
                  <li><a href="../author/patty-caya/index.php">Patty Caya</a></li>
                  
                  <li><a href="../author/susan-farrell/index.php">Susan Farrell</a></li>
                  
                  <li><a href="../author/therese-fessenden/index.php">Therese Fessenden</a></li>
                  
                  <li><a href="../author/kim-flaherty/index.php">Kim Flaherty</a></li>
                  
                  <li><a href="../author/sarah-gibbons/index.php">Sarah Gibbons</a></li>
                  
                  <li><a href="../author/aurora-harley/index.php">Aurora Harley</a></li>
                  
                  <li><a href="../author/kate-kaplan/index.php">Kate Kaplan</a></li>
                  
                  <li><a href="../author/page-laubheimer/index.php">Page Laubheimer</a></li>
                  
                  <li><a href="../author/angela-li/index.php">Angie Li</a></li>
                  
                  <li><a href="../author/hoa-loranger/index.php">Hoa Loranger</a></li>
                  
                  <li><a href="../author/kate-meyer/index.php">Kate Meyer</a></li>
                  
                  <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                  
                  <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                  
                  <li><a href="../author/kara-pernice/index.php">Kara Pernice</a></li>
                  
                  <li><a href="../author/christian-rohrer/index.php">Christian Rohrer</a></li>
                  
                  <li><a href="../author/amy-schade/index.php">Amy Schade</a></li>
                  
                  <li><a href="../author/katie-sherwin/index.php">Katie Sherwin</a></li>
                  
                  <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                  
                  <li><a href="../author/kathryn-whitenton/index.php">Kathryn Whitenton</a></li>
                  
                </ul>
              </section>
              
            </div>
            
          </div>

          
          <section class="hide-for-small-down">
            
              
              <h1>Recent Articles</h1>
              
              <ul>
                  
                  <li><a href="../seamless-cross-channel/index.php">Seamlessness in the Omnichannel User Experience</a></li>
                  
                  <li><a href="../brand-intention-interpretation/index.php">Experience Design: Bridging Brand Intention and Brand Interpretation</a></li>
                  
                  <li><a href="../eyetracking-tasks-efficient-scanning/index.php">Scanning Patterns on the Web Are Optimized for the Current Task</a></li>
                  
                  <li><a href="../flat-design-best-practices/index.php">Flat-Design Best Practices</a></li>
                  
                  <li><a href="../ux-user-stories/index.php">Accounting for UX Work with User Stories in Agile Projects</a></li>
                  
                  <li><a href="../index.php"><strong>See all articles&hellip;</strong></a></li>
              </ul>
              
            
          </section>
          

          
          
          <section class="hide-for-small-down">
            <h1>Popular Articles</h1>
            
            <ul>
                
                <li><a href="../usability-101-introduction-to-usability/index.php">Usability 101: Introduction to Usability</a></li>
                
                <li><a href="../top-10-mistakes-web-design/index.php">Top 10 Mistakes in Web Design</a></li>
                
                <li><a href="../how-users-read-on-the-web/index.php">How Users Read on the Web</a></li>
                
                <li><a href="../f-shaped-pattern-reading-web-content/index.php">F-Shaped Pattern For Reading Web Content</a></li>
                
                <li><a href="../ten-usability-heuristics/index.php">10 Usability Heuristics for User Interface Design</a></li>
                
                <li><a href="../intranet-design-2016/index.php">10 Best Intranets of 2016</a></li>
                
                <li><a href="../which-ux-research-methods/index.php">When to Use Which User-Experience Research Methods</a></li>
                
                <li><a href="../response-times-3-important-limits/index.php">Response Times: The 3 Important Limits</a></li>
                
                <li><a href="../why-you-only-need-to-test-with-5-users/index.php">Why You Only Need to Test with 5 Users</a></li>
                
                <li><a href="../page-fold-manifesto/index.php">The Fold Manifesto: Why the Page Fold Still Matters</a></li>
                
            </ul>
          </section>
          
          

          
            <section class="hide-for-small-down">
              
            </section>
          

      </section>
      

      
        <div class="small-12 medium-9 columns content-body">
          




<article class="l-subsection alertbox-article-content article-detail-bl">
  
  <header>
    <div class="l-subsection">
      <h1>Help People Create Passwords That They Can Actually Remember</h1>
      <div class="author-meta">by
        
          
            
            <a href="../author/kara-pernice/index.php">Kara Pernice</a>
            
          
        on  June 14, 2015
        
      </div>

      

      
      <div class="topic-links">
        Topics:
        
          <ul>

  <li><a href="../../topic/applications/index.php">Application Design</a></li>

  <li><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction</a></li>

  <li><a href="../../topic/web-usability/index.php">Web Usability</a></li>

</ul>
        
      </div>
      
    </div>
  </header>
  

  
  
  <section>
    <p class="lede"><strong>Summary:</strong> Create designs that capitalize on what we know about human memory. Help people remember passwords by encouraging them to: focus when creating, and rehearse; choose passwords that relate to important events, are chunked, have limited characters, and can be articulated in 2 seconds.</p>
    <br />
  </section>
  
  

  
  <section id="article-body"><p>Imagine you went on vacation for two glorious weeks. Upon return you are energized with ideas you are raring to share in your project database. You switch on your computer and are prompted for your password. You have no idea what it is, and the post-it note you had previously affixed to your monitor is gone. You begin the familiar dance of switching the order of names of your kids and their birthdays until you are ultimately blocked and must now reset the password. Answering the challenge questions, you’re surprised that your favorite movie is no longer “Silence of the Lambs”, and you forget whether your answer to the “your first car” prompt is that borrowed Gremlin you drove first, the used Nova you owned first, or the new Toyota Camry you bought first. But you press on, rushing through creating a new password. You immediately open the project database only to find that you have forgotten half of the ideas you had, and you have kicked off your first week back to work with a negative undertone—just because you forgot your password.</p>

<p>Many people who encounter these nuisances or even more debilitating scenarios consider the password to be a nosy bodyguard: inconvenient yet a respected protector. We can blame 2 major UX-related roadblocks: passwords are difficult to (1) create, and (2) remember.</p>

<p>Over the years, the usability of creating passwords that meet strict requirements has improved somewhat as the following innovations were introduced:</p>

<ol>
	<li><strong>Informing people </strong>about the password requirements before password creation.</li>
	<li><strong>Reminding</strong> people of these rules during <a href="../password-creation/index.php">password creation</a>. Complex requirements tax the human mind’s <a href="../short-term-memory-and-web-usability/index.php">short-term memory</a>; in other words, they are hard to remember. So always displaying the password-format rules can save users from having to <a href="../minimize-cognitive-load/index.php">keep these rules in their working memory</a>, so they can create a password that is acceptable to the system.</li>
	<li>Showing real-time password <strong>feedback</strong> (such as strength meters) when users create their passwords. This type of formative feedback makes users aware of how much their partially created password adheres to the rules. Many designs also judge the secureness of the password, from weak to strong, to help people create safer passwords. Some designs also foray into <a href="../../courses/emerging-patterns-web-design/index.php">gamification</a>, by giving people goals and <a href="../progress-indicators/index.php">small affirmations</a> as they type and encouraging them to attain more or all possible confirmations that the system has to offer (such as a big checkbox next to each requirement, and a red to green strength barometer with signposts “weak” to “strong”.)</li>
</ol>

<figure class="caption"><img alt="" height="174" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2015/05/15/yahoo_password_instructions.PNG" width="650"/>
<figcaption><em>YAHOO! Displays green boxes as a strength barometer.</em></figcaption>
</figure>

<p>These are all effective UI-design tactics in password creation, and help to solve issues that I repeatedly observe in research studies: meeting the password requirements, recalling or recognizing the requirements, and creating stronger (safer) passwords.</p>

<p>As good as these designs are, there is a calamitous omission in them. They are so focused on getting past the <em>creation</em> phase that <strong>none of them help people <em>remember</em> passwords</strong>. In fact, some of these designs actually hamper the memory-retrieval process.</p>

<h2>Forgetting Passwords Instigates Serious Issues</h2>

<p>We may decide that making strong passwords is more important than remembering those passwords. However, forgetting a password causes serious issues to individuals and organizations, including the following:</p>

<ul>
	<li><strong>Security breach:</strong> When people think they won’t be able to recall a password, <a href="../security-and-human-factors/index.php">they write the passwords down</a>. Common places where they store these? On a post-it note stuck on the computer monitor, under the mousepad, under the keyboard, in a top drawer, saved in their phone, and in a document or email on the computer. Some of these places are relatively safe, but more are not.</li>
	<li><strong>User frustration:</strong> Being challenged prevents people from proceeding and being successful, takes away control, and makes them feel inept. And if the disruption occurs at the very start of the session, people begin their experience negatively. All of these things combined make for a very unpleasant user experience.</li>
	<li><strong>Time and money wasted, sales lost: </strong><a href="../suggested-employee-search/index.php">Nonproductive time</a> occurs when users are wasting time trying to log in instead of focusing on their goals. Here are some examples of what can go wrong for the users:
	<ul style="list-style-type:circle;">
		<li>Being blocked from completing their workflow. As a result, their focus is broken and they must tend to the system instead of the content or work they were doing or planning to do.</li>
		<li>Repeatedly trying to guess their password.</li>
		<li>Possibly using too many attempts and being locked out of the system, punished for a time that the organization deems adequate.</li>
		<li>Dealing with challenge questions whose answers (or answer formats) they may have forgotten.</li>
		<li>Contacting a helpdesk to retrieve or reset their password.</li>
		<li>Waiting for the password to be reset.</li>
		<li>Leaving a website or application because they can’t or won’t deal with password retrieval.</li>
	</ul>
	</li>
	<li><strong>Helpdesk time monopolization:</strong> Paying staff to deal with password resets is a hefty cost in enterprise computing.</li>
</ul>

<figure class="caption"><img alt="Kayak gives too many instructions to remember" height="431" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2015/05/26/kayak_reset_password_message.PNG" width="700"/>
<figcaption><em>Even KAYAK’s thorough message gives a sense of what a hindrance forgetting a password can be.</em></figcaption>
</figure>

<ul>
	<li><strong>Opportunity cost</strong>: Consider all the other work or activities people (including customers and your own IT and helpdesk staff) could be doing instead of dealing with lost passwords. The notion of <a href="http://www.businessdictionary.com/definition/opportunity-cost.php">opportunity cost</a> is one of the most underrated business concepts in UX design. Increased efficiency means less wasted time and more time spent on the right things for the organization.</li>
</ul>

<p>Some systems help people retrieve a forgotten password by answering personal questions, such as the “name of the street you grew up on”, or “your first pet’s name”. These can help, but they too can be problematic if users forget their answers or the specific way in which they entered their answers. For example, you may answer the question, “What’s your favorite sports team?” as “The Boston Red Sox.” But when asked to answer the same question months later (and after you forgot your password), you may write, “The Red Sox”, “Red Sox”, “the red sox”, “the sox”, or my favorite “RedSoxRule!”. Ironic that the help for a particular problem suffers its same limitations.</p>

<h2>How People Remember Passwords: A Look at Memory</h2>

<p>Information first enters human memory through <strong>encoding</strong><em>,</em> then it gets <strong>stored</strong>, then, later on, we remember it through <strong>retrieval</strong>.  All these processes influence the <strong><a href="../recognition-and-recall/index.php">activation</a> </strong>of information in memory and how fast and easily that information will be remembered.</p>

<p>If we want to be able to recall passwords better, we need to do a better job of encoding them.  Psychologists have studied human memory at length and have come up with many methods that help people better encode and remember information. Below are a few of them with recommendations for how to design with them in mind.</p>

<h2>Help Users Pay Attention When They Create Passwords </h2>

<p>We usually blame <a href="../usability-for-senior-citizens/index.php">age</a> or stress (or in rare cases, neurological disorders) as password roadblocks. And these are certainly culprits, but often the issues are related to something far more basic: <strong>paying attention</strong>.</p>

<p>Before people can remember something, they first have to perceive it and register it. Basically, they have to focus on what they are doing and not give in to distractions. How much they concentrate on what they are doing is an extremely important factor in whether they will actually remember it later.</p>

<p>One of the reasons for which passwords are easily forgotten is that at the time when people create them, people usually have a completely different goal in mind. Creating a password is an <a href="../login-walls/index.php">obstacle to pass</a> in order to reach their real goal. People usually <strong>try to rush past this hurdle, which is counter to being engaged</strong>, focused, and eliminating disturbances.</p>

<p>One potential UI solution could be to stop people in their tracks with a modal dialog telling them to halt and pay attention, but this would be very annoying. Instead, the design can help people focus more on the task of creating a password in these ways:</p>

<ul>
	<li><strong>Eliminate distractions:</strong> Don’t include promotions, and consider using a <a href="../overuse-of-overlays/index.php">lightbox</a> or a page with no content except password-related information. This will help people to completely focus on creating and remembering the password.</li>
	<li><strong>Encourage people to pay attention to the passwords as they create them.</strong> Add a short sidebar tip about how to remember the passwords.</li>
	<li><strong>Make suggestions</strong> that will help them choose a memorable password, and offer examples. For example, if the password should be changed often (such as weekly) recommend that users create a password using a phrase that matters to them this week (possibly something personal but not easy for others to guess). String the words and numbers together to meet password requirements. Offer examples, such as:
	<ul>
		<li>You are not sure if your good friend Hannah's birthday is January 19 and you have to check. Make your password: <em>Jan19HannahBDay?</em></li>
		<li>You looked in your yard after a long winter and saw 3 beautiful pink tulips fighting the raw New England Spring. This made you hopeful. Make your password this week: <em>3TulipsBloomed!</em></li>
		<li>Window washing keeps moving to the bottom of the list of household chores, but you made a pact with yourself that you will wash 10 windows this week. Make your password: <em>Wash10Windows</em>.</li>
		<li>You are selling your truck. The going rate for a truck like yours seems to be $6,000, which is an acceptable amount to you. Make your password: <em>SellDodge$6K</em>.</li>
	</ul>
	</li>
	<li><strong>At less busy times, encourage people to reset passwords.</strong> Suggest a password change at moments when people are focused on updating their settings, and possibly at other times when they are less focused on a particular task. Add a tip about changing and remembering their password in the <em>Settings</em> area, and in your email newsletter. And if the app or website has a “tip of the day” feature, create some password memory tips. Tip example: <em>Make sure you remember your password. Use a phrase that is important to you now, like: </em>Aug1SharkTour!</li>
	<li><strong>Encourage people to create their own password guidelines.</strong> Users don’t just need a password, they need a framework for all of their passwords. Passwords they will keep for only a day or a week can especially benefit from guidelines, such as:
	<ul>
		<li>Capitalize the first letter of all words in the phrase, and make the rest of the words lowercase.</li>
		<li>Add punctuation at the end of the phrase.</li>
		<li>Avoid punctuation, such as apostrophes, within the phrase.</li>
		<li>Always use a digit instead of the word, so <em>8</em> instead of <em>eight</em>.</li>
	</ul>
	</li>
	<li><strong>Offer tips at both the password creation phase, and on the dialog(s) that appear after the user forgets his password.</strong> Add a link called “How to remember your password” or a sidebar called “Tips for remembering your password”. Then hit them with this information when it hurts: after they have forgotten the password and gone through the pain of answering challenge questions or getting locked out.</li>
</ul>

<h2>Just a Few Words and Digits</h2>

<p>Shorter is usually easier to remember than longer.</p>

<ul>
	<li><strong>Recommend that users keep their passwords short and strong.</strong> Since most passwords need to contain numbers, capital letters, and punctuation; suggest password examples with<strong> 3 lower-case letters, 1 digit, 2 or 3 capital letters, and 1 punctuation mark</strong>.</li>
	<li><strong>Recommend that passwords be short enough to say in 2 seconds.</strong> A classic memory study (Baddeley, Thompson, and Buchanan, 1975) looked at memory for a list of words in relation to the amount of time it takes to say the words in the list out loud. It turned out that it was easier for people to remember a list such as <em>wit, sum, harm, bay, top </em>than <em>university, opportunity, aluminum, constitutional, auditorium. </em> People could recall the amount of words that could be said in about 2 seconds.</li>
</ul>

<h2>Chunk It</h2>

<p><a href="../chunking/index.php"><em>Chunking</em></a> is a popular and effective memory technique in which small pieces or information are divided into larger memorable groups.</p>

<p>One of the most famous memory studies is Miller’s “Magical Number Seven” (Miller, 1956). Miller reviewed several published memory experiments and determined that, when shown lists of elements of various length, most people could roughly remember 7 (plus or minus 2) elements. But Miller’s insight was that while people could remember only about 7 letters from a random sequence of letters, their memory span could suddenly be increased to 21 letters (corresponding to 7 words that consist of 3 letters) when they were shown a random sequence of 3-letter words. So what mattered for memory was not the amount of information (number of letters) per se, but the number of meaningful chunks.<strong> People could remember more information if it was appropriately structured in chunks.</strong></p>

<p>A password with meaningful chunks such as <em>The;Capital;Of;Scotland;Is;Edinburgh </em>is going to be easier to remember than <em>uapTCei;Ih;ttrghafOl;SEbdn;dnal;cos.</em></p>

<p>Using chunking with passwords maybe be difficult for some people because there are no standard characters, such as dashes, used to visually separate the chunks or numbers or words. While some people can do fine with no separator (such as, “mypasswordisthis”), others may be stymied. In my example above, I used capitalization and semicolons to separate the words. One technique you can recommend people do is to capitalize every word in the phrase, for example:</p>

<p><em>3TulipsBloomed!</em></p>

<p><em>Wash10Windows</em></p>

<p><em>Jan19HannahBDay?</em></p>

<p>Or as part of their own personal guidelines, they may always use an ampersand, for example, in lieu of spaces.</p>

<h2>Meaningful</h2>

<p>People tend to be <strong>better at remembering things that are significant to them</strong> than they are at remembering things that are not meaningful, mainly because they can <strong>connect the information to related material that is already stored in their long-term memory</strong>. The more associations an item has to other well-known facts in our background knowledge, the easier it will be to <a href="../recognition-and-recall/index.php">retrieve</a> that item.  </p>

<p>People naturally choose personal information when creating passwords, sometimes so personal—pets’ names, a wedding anniversary and street address—that they become easy to infer and mar the system’s security. But we can encourage people to instead use details that are not easy to guess, such as a special story. For example: I took my Cairn Terrier Columbo kayaking. I wasn’t sure if I would be able to paddle with him sitting in the bow of the vessel, but we traveled 4 nautical miles together. Make the password: <em>ColumboPaddled4:)</em></p>

<p>If a story is difficult to shorten, users could use a mnemonic instead. For example, you were golfing at The Breakers and actually got a hole in one on the 15<sup>th</sup> hole. You’ll never forget that. Make your password: <em>TBHI115!</em></p>

<ul>
	<li><strong>Recommend users to select passwords related to a story important to them. </strong></li>
</ul>

<h2>Repetition</h2>

<p>We all know that “practice makes perfect” and “repetition is the mother of learning.” <strong>The more we practice an item, the better we can remember it.</strong> <em>Rehearsing</em> is a memory technique in which people mentally repeat items.</p>

<p>For passwords, once created, we usually only need to recall them when we attempt to log in. This may be too late: by the time we need the password, we may have already forgotten it. We could spot-quiz users while they are on the website or in the app, but while this might reinforce password memory, it would be completely disruptive and bothersome. Instead we can recommend that users rehearse the password when they first create it. Tip example: <em>To help yourself remember this password, say it to yourself three times now.</em></p>

<ul>
	<li>Upon password creation, or after the user forgets and needs to create a new password, <strong>suggest in tips</strong> that they at least<strong> mentally repeat the password to themselves 3 times.</strong></li>
</ul>

<p>Password-creation designs usually ask people to enter their passwords twice, mainly to ensure that there are no typos. But there is a positive byproduct to that second entry: It is a small form of rehearsal. Since the interaction cost of entering the new password a second time on a phone is quite high, we recommend instead asking for the new password only once, but displaying the password characters as they are entered.</p>

<ul>
	<li>For <strong>desktop and tablet interfaces, ask users to enter the password twice when they are creating it</strong>. But since the interaction cost of entering passwords on a phone is so great, <strong>ask for the password only one time in the phone UI, but display the characters</strong> as users type them so they <strong>may check for errors and encode</strong> the password.</li>
</ul>

<figure class="caption"><img alt="Enter current password field, then 2 fields for new password." height="587" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2015/05/26/centurylink-change-password-bad-button-labels.png" width="700"/>
<figcaption><em>CenturyLink asks that the new password be entered twice.</em></figcaption>
</figure>

<h2>See the Password</h2>

<p>Memories for each of our 5 senses—sight, smell, hearing, taste, and touch—are stored in different areas of the brain. These redundancies help people recall multiple pieces of information based on just one cue, and this <strong>cross-referencing helps us learn</strong>.</p>

<ul>
	<li>Upon password creation, or after the user forgets and needs to create a new password, <strong>suggest in tips</strong> that (if they are alone) they <strong>say the password out loud 3 times. </strong>(This also has the advantage of accomplishing repetition.)</li>
</ul>

<p>For security reasons, passwords are usually masked. However, the visual representation of the password can create one extra association in memory and thus can make it easier to retrieve. <a href="../stop-password-masking/index.php">Hiding the password as it is entered</a> impairs encoding.</p>

<ul>
	<li>If possible, <strong>offer a checkbox that can be selected to designate to display the password</strong> as it is being typed.</li>
</ul>

<p>Studies show that students learn quickly when they can visualize a concept and develop mental pictures of it. Even pretending to write words on a whiteboard (really writing in the air with a finger) helps students to visualize the order of letters in a word.</p>

<ul>
	<li>Upon password creation, or after the user forgets and needs to create a new password, <strong>suggest in tips</strong> that they <strong>write the password in the air, as if writing on a whiteboard.</strong></li>
</ul>

<h2>Password Memorability Design</h2>

<p>Organizations and individuals want to protect their online privacy and information, and thus usually have some respect for their passwords. If there is a high commitment level to the website or application, people will tolerate some of the difficulties the accompany them. But it is better to so save your organization and your users’ time, money, and frustration by helping them better encode their passwords so they can easily remember them later (even after a long vacation).</p>

<p>Learn more about human memory in our classes on <a href="../../courses/hci/index.php">User Interface Principles that Every Designer Must Know</a> and <a href="../../courses/human-mind/index.php">The Human Mind and Usability</a>.</p>

<p> </p>

<h2>References</h2>

<p>Baddeley, A.D., &amp; Hitch, G. (1974). Working memory. In G.H. Bower (Ed.), <em>The psychology of learning and motivation: Advances in Research and Theory</em> (Vol. 8, pp. 47–89). New York: Academic Press.</p>

<p>Miller, G. (1956). The magical number seven, plus or minus two: Some limits on our capacity for processing information. <em>Psychological Review</em>, 63, 81-97.</p>
</section>
  

  
  <section>
    
        
    
  </section>
  
  
  
  
  

  


  <section>
    <br />
    <p class="share"><b>Share <span class="hide-for-small-down">this article</span>:</b> <a href="https://twitter.com/intent/tweet?url=http://www.nngroup.com/articles/passwords-memory/&amp;text=Help%20People%20Create%20Passwords%20That%20They%20Can%20Actually%20Remember&amp;via=nngroup">Twitter</a> | <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=http://www.nngroup.com/articles/passwords-memory/&amp;title=Help%20People%20Create%20Passwords%20That%20They%20Can%20Actually%20Remember&amp;source=Nielsen%20Norman%20Group">LinkedIn</a> | <a href="https://plus.google.com/share?url=http://www.nngroup.com/articles/passwords-memory/">Google+</a> | <a href="mailto:?subject=NN/g Article: Help People Create Passwords That They Can Actually Remember&amp;body=http://www.nngroup.com/articles/passwords-memory/">Email</a></p>
    <hr />
  </section>

  <footer class="l-subsection">
    
    
    <div class="learn-more">
      <h2>Learn More</h2>
      <p><a href="../subscribe/index.php">Subscribe to the weekly newsletter</a> to get notified about future articles.</p>
      </div>
    
    

    <div class="row show-for-small-down related-content">
      
      
      

      
      
        <div class="column small-12 topic-links">All articles about:
          <ul>
            
            <li><a href="../../topic/applications/index.php">Application Design</a></li>
            
            <li><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction</a></li>
            
            <li><a href="../../topic/web-usability/index.php">Web Usability</a></li>
            
          </ul>
        </div>
      
      

      
      
        <div class="column small-12">
          <h3>UX Conference Training Courses</h3>
           <ul class="no-bullet">
    
        <li><a href="../../courses/ux-basic-training/index.php">UX Basic Training</a></li>
    
        <li><a href="../../courses/human-mind/index.php">The Human Mind and Usability</a></li>
    
        <li><a href="../../courses/hci/index.php">User Interface Principles Every Designer Must Know</a></li>
    
        <li><a href="../../courses/emerging-patterns-web-design/index.php">Emerging Patterns for Web Design</a></li>
    
        <li><a href="../../courses/web-page-design/index.php">Web Page UX Design</a></li>
    
</ul>
        </div>
      
      

      
      
        <div class="small-12 column">
          <h3>Research Reports</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../reports/b2b-websites-usability/index.php">B2B Website Usability for Converting Users into Leads and Customers</a></li>
              
            
              
                <li><a href="../../reports/best-applications-2/index.php">Application Design Showcase: 2012</a></li>
              
            
              
                <li><a href="../../reports/strategic-design-faqs/index.php">Strategic Design for Frequently Asked Questions</a></li>
              
            
              
                <li><a href="../../reports/enterprise-mobile-showcase/index.php">Mobile Intranets and Enterprise Apps</a></li>
              
            
              
                <li><a href="../../reports/website-tools-and-applications-flash/index.php">Website Tools and Applications with Flash</a></li>
              
            
          </ul>
        </div>
      
      

      
      
        <div class="small-12 column">
          <h3>Online Seminars</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../online-seminars/ux-big-data/index.php">UX for Big Data</a></li>
              
            
              
                <li><a href="../../online-seminars/fundamental-flaw-flat-design/index.php">The Fundamental Flaw in Flat Design</a></li>
              
            
              
                <li><a href="../../online-seminars/millennials-online/index.php">Millennials Online</a></li>
              
            
              
                <li><a href="../../online-seminars/university-websites/index.php">University Website UX Essentials</a></li>
              
            
          </ul>
        </div>
      
      
    </div>

    <div class="hide-for-small-down row related-content">
      <div class="medium-6 columns">
        
        
          <div>
            <h3>Articles</h3>
            <ul class="no-bullet">
              
                
                <li><a href="../tabs-used-right/index.php">Tabs, Used Right</a></li>
                
              
                
                <li><a href="../the-need-for-speed/index.php">The Need for Speed</a></li>
                
              
                
                <li><a href="../indicators-validations-notifications/index.php">Indicators, Validations, and Notifications: Pick the Correct Communication Option</a></li>
                
              
                
                <li><a href="../expandable-menus/index.php">Expandable Menus: Pull-Down, Square, or Pie? </a></li>
                
              
                
                <li><a href="../enhancement/index.php">The Role of Enhancement in Web Design</a></li>
                
              
            </ul>
          </div>
        
        

        
          
          <div>
            <h3>Research Reports</h3>
            <ul class="no-bullet">
              
                
                  <li><a href="../../reports/b2b-websites-usability/index.php">B2B Website Usability for Converting Users into Leads and Customers</a></li>
                
              
                
                  <li><a href="../../reports/best-applications-2/index.php">Application Design Showcase: 2012</a></li>
                
              
                
                  <li><a href="../../reports/strategic-design-faqs/index.php">Strategic Design for Frequently Asked Questions</a></li>
                
              
                
                  <li><a href="../../reports/enterprise-mobile-showcase/index.php">Mobile Intranets and Enterprise Apps</a></li>
                
              
                
                  <li><a href="../../reports/website-tools-and-applications-flash/index.php">Website Tools and Applications with Flash</a></li>
                
              
            </ul>
          </div>
          
        

      </div>
      <div class="medium-6 columns">
        
          
          <div>
            <h3>UX Conference Training Courses</h3>
             <ul class="no-bullet">
    
        <li><a href="../../courses/ux-basic-training/index.php">UX Basic Training</a></li>
    
        <li><a href="../../courses/human-mind/index.php">The Human Mind and Usability</a></li>
    
        <li><a href="../../courses/hci/index.php">User Interface Principles Every Designer Must Know</a></li>
    
        <li><a href="../../courses/emerging-patterns-web-design/index.php">Emerging Patterns for Web Design</a></li>
    
        <li><a href="../../courses/web-page-design/index.php">Web Page UX Design</a></li>
    
</ul>
          </div>
          
        
        
          
          <div>
            <h3>Online Seminars</h3>
            <ul class="no-bullet">
            
                <li><a href="../../online-seminars/ux-big-data/index.php">UX for Big Data</a></li>
            
                <li><a href="../../online-seminars/fundamental-flaw-flat-design/index.php">The Fundamental Flaw in Flat Design</a></li>
            
                <li><a href="../../online-seminars/millennials-online/index.php">Millennials Online</a></li>
            
                <li><a href="../../online-seminars/university-websites/index.php">University Website UX Essentials</a></li>
            
            </ul>
          </div>
          
        
      </div>
    </div>
  </footer>
</article>

        </div>

      

    
  </div>
  <div class="row">
    
      <div class="column small-12 show-for-small-down l-navrail announcements">
        
          
        
      </div>
    
    
  </div>

    </div>
    




<footer id="page-footer">
    <div class="row hide-for-print">
        <div class="column small-12 medium-6">
            <section class="footer-popular">
                
                <h1>Popular Topics</h1>
                <div style="float: left;  width: 15em; display: block;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/applications/index.php">Application Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/e-commerce/index.php">E-Commerce Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/eyetracking/index.php">Eyetracking </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/information-architecture/index.php">Information Architecture </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/intranets/index.php">Intranet Design </a></li>
	<li><a href="../../topic/interaction-design/index.php">Interaction Design </a></li>
</ul>
</div>

<div style="float: left;  width: 15em; display: block; list-style: none;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/mobile-and-tablet-design/index.php">Mobile and Tablet Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/credibility-persuasion/index.php">Persuasive Design</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/research-methods/index.php">Research Methods</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/user-testing/index.php">User Testing </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/web-usability/index.php">Web Usability </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/writing-web/index.php">Web Writing &amp; Content Strategy </a></li>
</ul>
</div>

<p>&nbsp;</p>

                
            </section>
        </div>

        <div class="column small-12 medium-6">
            <div class="row">
                <div class="column small-6">
                    <section class="footer-about">
                        <h1>Follow Us</h1>
                        <ul>
	<li><a href="../subscribe/index.php">Subscribe to our weekly UX newsletter</a>,&nbsp;Jakob Nielsen&#39;s Alertbox</li>
	<li>Visit Don Norman&#39;s website, <a href="http://www.jnd.org/"> www.jnd.org </a></li>
	<li>Visit Bruce Tognazzini&#39;s website, <a href="http://www.asktog.com/"> www.asktog.com </a></li>
	<li style="padding-top: 6px;"><a class="twitter-follow-button" data-show-count="false" data-size="large" href="https://twitter.com/nngroup"><img alt="Follow @nngroup on Twitter" height="20" src="https://media.nngroup.com/media/editor/2016/12/30/twitter-follow-button.png" style="width: 117px; height: 20px;" width="117" /></a></li>
	<li style="padding-top: 6px;"><img alt="" height="14" src="https://media.nngroup.com/media/editor/2016/07/26/feed-icon-14x14.png" style="vertical-align:text-top;width:14px;margin-right:.25em;" width="14" /><a href="../../feed/rss/index.php">Subscribe to our RSS feed</a></li>
</ul>

                    </section>
                </div>

                <div class="column small-6">
                    <section class="footer-about right-column">
                        <h1>About
</h1>
                        <ul class="no-bullet">
	<li><a href="../../about/contact/index.php">Contact information</a></li>
	<li><a href="../../about/index.php">About Us</a></li>
	<li><a href="../../about/why-nng/index.php">Why NN/g? </a></li>
	<li><a href="../../return-policy/index.php">Return Policy</a></li>
</ul>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="copyright column small-12">
            <p><a href="../../copyright-and-reprint-info/index.php">Copyright</a> &copy; 1998-2017 Nielsen Norman Group, All Rights Reserved.</p>
        </div>
    </div>
</footer>


    


        <script src="https://media.nngroup.com/static/vendor/fastclick/js/fastclick.min.js"></script>
        <script src="https://media.nngroup.com/static/vendor/foundation/js/foundation.min.js"></script>
        <script src="http://media.nngroup.com/static/js/app.js"></script>
        <script src="http://media.nngroup.com/static/js/scroll-to-fixed.js"></script><script>var externalLinks = document.querySelectorAll('a[href^="http"]');for (var i = externalLinks.length-1; i >= 0; i--) {externalLinks[i].addEventListener("click", function(event) { alert("External links are disabled for this test."); event.preventDefault(); }, false);}var mailLinks = document.querySelectorAll('a[href^="mailto"]');for (var i = mailLinks.length-1; i >= 0; i--) {mailLinks[i].addEventListener("click", function(event) { alert("Email links are disabled for this test."); event.preventDefault(); }, false);}var externalForms = document.querySelectorAll('form');for (var i = externalForms.length-1; i >= 0; i--) {externalForms[i].addEventListener("submit", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false); externalForms[i].addEventListener("click", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false);}</script>

        <script>
            
            $(function() {
                $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
                $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
            });
        </script>

        
<script src="https://media.nngroup.com/static/js/articles.js"></script>


        
        <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1072699960;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072699960/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
    </body>

<!-- Mirrored from www.nngroup.com/articles/passwords-memory/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:06:24 GMT -->
</html>
