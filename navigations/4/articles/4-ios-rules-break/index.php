<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->





    
<!-- Mirrored from www.nngroup.com/articles/4-ios-rules-break/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:13:06 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1026.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"licenseKey":"1a30ef9931","queueTime":5,"beacon":"bam.nr-data.net","errorBeacon":"bam.nr-data.net","applicationID":"2048992","transactionName":"NFxSMRBUX0EDUkIPDA0WdhAMVkVbDV8ZBxEXUFMJB0YfRAtUQRVZIktEDAFZVHYHRVcPD01eVRE=","applicationTime":374,"agent":""}</script>
        <title>iOS Design Rules to Break </title><meta property="og:title" content="iOS Design Rules to Break " />
  
        
        <meta name="description" content="Reconsider using iOS recommended design patterns: Page control (dots), Submit at top, the Plus (+) and Move icons. These cause usability problems in testing.">
        <meta property="og:description" content="Reconsider using iOS recommended design patterns: Page control (dots), Submit at top, the Plus (+) and Move icons. These cause usability problems in testing." />
        
  
        
	
        
        <meta name="keywords" content="iOS, design rule, design guidelines, standards, iOS recommended patterns, patterns, page control (dots), Submit at top, Plus icon,  + icon, Move icon, move, add, mobile, consistency">
        

        

        <meta name="robots" content="noarchive">
        <meta name="viewport" content="width=device-width">

        <link rel="search" type="application/opensearchdescription+xml" title="NN/g Search" href="../../opensearch.xml" />
        <link rel="shortcut icon" href="http://media.nngroup.com/static/img/favicon.ico" />

        <link rel="stylesheet" href="https://media.nngroup.com/static/vendor/font-awesome/css/font-awesome.min.css">

        
        <link rel="stylesheet" href="https://media.nngroup.com/static/css/7232a960f1df.css" type="text/css" />
        <link rel="stylesheet" href="http://fast.fonts.com/t/1.css?apiType=css&amp;projectid=c5846260-9def-46ec-9407-0f1349bd2d64" type="text/css">

        <script src="https://media.nngroup.com/static/vendor/modernizr/js/modernizr.min.js"></script>

        <script type="text/javascript" src="https://media.nngroup.com/static/js/3283be5def74.js"></script>
        

        

        
        
        <!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=229034,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('http://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->


<!-- Google Analytics Tracking Code for http://nngroup.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23132681-1', 'nngroup.com');
  ga('require', 'linkid', 'https://www.nngroup.com/articles/4-ios-rules-break/linkid.js');
  ga('require', 'displayfeatures');

// Optimizely Universal Analytics Integration
 window.optimizely = window.optimizely || [];
 window.optimizely.push("activateUniversalAnalytics");

  ga('send', 'pageview');

</script>
<script>
/**
* Function to track a click on an outbound link in Google Analytics.
*/
function trackOutboundLink(label, url) {
    if(window.ga && ga.loaded) {
        ga('send', 'event', 'outbound', 'click', label, {'hitCallback': function(){document.location = url;}
        });
    } else {
        document.location = url;
    }
}
/**
* Function to track a click on a link in the article announcement area.
*/
$('#articleAnnouncement a').on('click', function(){
  ga('send', 'event', 'link', 'click', 'articleAnnouncement');
});
</script>

<script type="text/javascript">
 setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2421.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



<!-- Hotjar Tracking Code for http://nngroup.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:121605,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

        <script src="https://media.nngroup.com/static/js/analytics/track_outbound_link.js"></script>
        
        
        
    </head>
    <body class="location-articles articles-detail">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
    <header id="page-header">
        <div id="cart" class="shopping-cart mobile-cart show-for-small-down"></div>
        <div class="row">
            <div id="masthead" class="masthead small-12 medium-7 column">

                <!-- The button to toggles menu on small screen -->

                <h1><a href="../../index.php"><img src="http://media.nngroup.com/static/img/logo-wordmark.png" width="74" height="30"> Nielsen <span class="norman">Norman</span> Group</a></h1>
                <p class="tagline show-for-medium-up">Evidence-Based User Experience Research, Training, and Consulting</p>

                <!-- The button to toggles form search on small screen -->


                <div style="clear: right;width:  100%;height: 1px;"></div><?php define('ROOT_PATH',$_SERVER["DOCUMENT_ROOT"] . '/apps/navigation_styles/navigations/4'); ?><?php include(ROOT_PATH)."/navigation.php"; ?>

                <!-- Drop down menu on small screen -->
                <div id="hideNav" class="show-for-small-down">
                  
                  <ul class="menu-content">
                    <li><a href="../../index.php">Home</a></li>
                    <li class="has-submenu">
                      <a>Training</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../training/index.php">Overview</a></li>
                        
<li id="nav-ux-conference"><a href="../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../online-seminars/index.php"><span>Online Seminars</span></a></li>

                      </ul>
                    </li>
                    <li class="has-submenu">
                      <a>Consulting</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
  <li><a href="../../consulting/index.php">Overview</a></li>
  
  <li><a href="../../consulting/benchmarking/index.php">Benchmarking</a></li>
  
  <li><a href="../../consulting/expert-design-review/index.php">Expert Design Review</a></li>
  
  <li><a href="../../consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
  
  <li><a href="../../consulting/usability-testing/index.php">Usability Testing </a></li>
  
  <li><a href="../../consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
  
  <li><a href="../../in-house-training/index.php">In-House Training</a></li>
</ul>

                    </li>

                    <li><a href="../../reports/index.php">Reports</a></li>
                    <li><a href="../index.php">Articles</a></li>
                    <li class="has-submenu"><a>About NN/G</a><i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../about/index.php">Overview</a></li>
                        <li><a href="../../people/index.php">People</a></li>
                        <li><a href="../../about/why-nng/index.php">Why NN/g?</a></li>
                        <li><a href="../../about/contact/index.php">Contact</a></li>
                        <li><a href="../../news/index.php">News</a></li>
                        <li><a href="../../about/history/index.php">History</a></li>
                        <li><a href="../../books/index.php">Books</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                </div>

                <hr class="header-divider show-for-small-only"/>


            </div>
       </div>

        <div id="hideNav">            <nav class="primary-nav small-12 column">
                <ul>
                    <li><a class="navitem" id="nav-home" href="../../index.php">Home</a></li>
                    <li><a class="navitem" id="nav-training" href="../../training/index.php">Training</a></li>
                    <li><a class="navitem" id="nav-consulting" href="../../consulting/index.php">Consulting</a></li>
                    <li><a class="navitem" id="nav-reports" href="../../reports/index.php">Reports</a></li>
                    <li><a class="navitem" id="nav-articles" href="../index.php">Articles</a></li>
                    <li><a class="navitem" id="nav-about" href="../../about/index.php">About NN/G</a></li>
                </ul>
            </nav>
            <nav class="secondary-nav small-12 medium-12 large-12 columns">
                
                
            </nav>
        </div>
    </header>
    <div class="l-content">
    
  

  

  <div class="row">
    
      
      <section class="small-12 medium-3 columns collapsable-content l-navrail articles-left-column">
        <h1 class="show-for-small-down collapse-link mobile-browse-header">Browse by Topic and Author</h1>
          <div class="row collapsable">

            
            <div class="small-6 medium-12 columns">
              <section id="article-popular-topics" style="display: none;">
                
                <h1>Topics</h1>
                <ul id="article-popular-topics">
                    
                    <li>
                        
                        <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/intranets/all/index.php">Intranets</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/user-testing/all/index.php">User Testing</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                        
                    </li>
                    
                    <li><a href="#" id="article-see-all-topics"><strong>See all topics&hellip;</strong></a></li>
                </ul>
                
              </section>
              <section id="article-all-topics" style="display: none;">
                
                <h1>
                  <span class="show-for-medium-up">All Article Topics</span>
                  <span class="show-for-small-down">All Topics</span>
                  (<a href="#" id="article-hide-all-topics">hide</a>)
                </h1>
                <ul>
                  
                  <li>
                    
                    <a href="../../topic/accessibility/all/index.php">Accessibility</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/agile/all/index.php">Agile</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/analytics-and-metrics/all/index.php">Analytics &amp; Metrics</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/applications/all/index.php">Application Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/b2b-websites/all/index.php">B2B Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/behavior-patterns/all/index.php">Behavior Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/branding/all/index.php">Branding</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/content-strategy/all/index.php">Content Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/corporate-websites/all/index.php">Corporate Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/design-patterns/all/index.php">Design Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-design-process/all/index.php">Design Process</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/email/all/index.php">Email</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/eyetracking/all/index.php">Eyetracking</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/heuristic-evaluation/all/index.php">Heuristic Evaluation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/human-computer-interaction/all/index.php">Human Computer Interaction</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-ideation/all/index.php">Ideation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/information-architecture/all/index.php">Information Architecture</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/interaction-design/all/index.php">Interaction Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/international-users/all/index.php">International Users</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/intranets/all/index.php">Intranets</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-management/all/index.php">Management</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/navigation/all/index.php">Navigation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/non-profit-websites/all/index.php">Non-Profit Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/personas/all/index.php">Personas</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/persuasive-design/all/index.php">Persuasive Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/prototyping/all/index.php">Prototyping</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/psychology-and-ux/all/index.php">Psychology and UX</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/research-methods/all/index.php">Research Methods</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/search/all/index.php">Search</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/social-media/all/index.php">Social Media</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/strategy/all/index.php">Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/user-testing/all/index.php">User Testing</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-teams/all/index.php">UX Teams</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/visual-design/all/index.php">Visual Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/writing-web/all/index.php">Writing for the Web</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/young-users/all/index.php">Young Users</a>
                    
                  </li>
                  
                </ul>
                
              </section>
            </div>
            <script>
              /* Determine which topics list will be displayed: the full or the
               * partial.  We are inlining this close to the list itself to
               * prevent rendering delays on the client
               */
              var topic_list = $.cookie("article-topics");

              var popular_topics = $('#article-popular-topics');
              var all_topics = $('#article-all-topics');

              if(topic_list == 1) {
                  popular_topics.hide();
                  all_topics.show();
              } else {
                  popular_topics.show();
                  all_topics.hide();
              }
            </script>
            

            
            <div class="small-6 medium-12 columns">
              
              <section id="article-initial-authors">
                  <h1>Author</h1>
                  <ul>
                    
                    <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                    
                    <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                    
                    <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                    
                    <li><a href="#" onClick="$('#article-initial-authors').hide(); $('#article-all-authors').show();"><strong>See all authors&hellip;</strong></a></li>
                  </ul>
              </section>
              <section id="article-all-authors" style="display: none;">
                <h1>
                  All Authors
                  (<a href="#" id="article-hide-all-authors">hide</a>)
                </h1>
                <ul>
                  
                  <li><a href="../author/raluca-budiu/index.php">Raluca Budiu</a></li>
                  
                  <li><a href="../author/patty-caya/index.php">Patty Caya</a></li>
                  
                  <li><a href="../author/susan-farrell/index.php">Susan Farrell</a></li>
                  
                  <li><a href="../author/therese-fessenden/index.php">Therese Fessenden</a></li>
                  
                  <li><a href="../author/kim-flaherty/index.php">Kim Flaherty</a></li>
                  
                  <li><a href="../author/sarah-gibbons/index.php">Sarah Gibbons</a></li>
                  
                  <li><a href="../author/aurora-harley/index.php">Aurora Harley</a></li>
                  
                  <li><a href="../author/kate-kaplan/index.php">Kate Kaplan</a></li>
                  
                  <li><a href="../author/page-laubheimer/index.php">Page Laubheimer</a></li>
                  
                  <li><a href="../author/angela-li/index.php">Angie Li</a></li>
                  
                  <li><a href="../author/hoa-loranger/index.php">Hoa Loranger</a></li>
                  
                  <li><a href="../author/kate-meyer/index.php">Kate Meyer</a></li>
                  
                  <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                  
                  <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                  
                  <li><a href="../author/kara-pernice/index.php">Kara Pernice</a></li>
                  
                  <li><a href="../author/christian-rohrer/index.php">Christian Rohrer</a></li>
                  
                  <li><a href="../author/amy-schade/index.php">Amy Schade</a></li>
                  
                  <li><a href="../author/katie-sherwin/index.php">Katie Sherwin</a></li>
                  
                  <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                  
                  <li><a href="../author/kathryn-whitenton/index.php">Kathryn Whitenton</a></li>
                  
                </ul>
              </section>
              
            </div>
            
          </div>

          
          <section class="hide-for-small-down">
            
              
              <h1>Recent Articles</h1>
              
              <ul>
                  
                  <li><a href="../seamless-cross-channel/index.php">Seamlessness in the Omnichannel User Experience</a></li>
                  
                  <li><a href="../brand-intention-interpretation/index.php">Experience Design: Bridging Brand Intention and Brand Interpretation</a></li>
                  
                  <li><a href="../eyetracking-tasks-efficient-scanning/index.php">Scanning Patterns on the Web Are Optimized for the Current Task</a></li>
                  
                  <li><a href="../flat-design-best-practices/index.php">Flat-Design Best Practices</a></li>
                  
                  <li><a href="../ux-user-stories/index.php">Accounting for UX Work with User Stories in Agile Projects</a></li>
                  
                  <li><a href="../index.php"><strong>See all articles&hellip;</strong></a></li>
              </ul>
              
            
          </section>
          

          
          
          <section class="hide-for-small-down">
            <h1>Popular Articles</h1>
            
            <ul>
                
                <li><a href="../usability-101-introduction-to-usability/index.php">Usability 101: Introduction to Usability</a></li>
                
                <li><a href="../top-10-mistakes-web-design/index.php">Top 10 Mistakes in Web Design</a></li>
                
                <li><a href="../how-users-read-on-the-web/index.php">How Users Read on the Web</a></li>
                
                <li><a href="../f-shaped-pattern-reading-web-content/index.php">F-Shaped Pattern For Reading Web Content</a></li>
                
                <li><a href="../ten-usability-heuristics/index.php">10 Usability Heuristics for User Interface Design</a></li>
                
                <li><a href="../intranet-design-2016/index.php">10 Best Intranets of 2016</a></li>
                
                <li><a href="../which-ux-research-methods/index.php">When to Use Which User-Experience Research Methods</a></li>
                
                <li><a href="../response-times-3-important-limits/index.php">Response Times: The 3 Important Limits</a></li>
                
                <li><a href="../why-you-only-need-to-test-with-5-users/index.php">Why You Only Need to Test with 5 Users</a></li>
                
                <li><a href="../page-fold-manifesto/index.php">The Fold Manifesto: Why the Page Fold Still Matters</a></li>
                
            </ul>
          </section>
          
          

          
            <section class="hide-for-small-down">
              
            </section>
          

      </section>
      

      
        <div class="small-12 medium-9 columns content-body">
          




<article class="l-subsection alertbox-article-content article-detail-bl">
  
  <header>
    <div class="l-subsection">
      <h1>4 iOS Rules to Break </h1>
      <div class="author-meta">by
        
          
           
              <a href="../author/amy-schade/index.php">Amy Schade</a>, 
           
              <a href="../author/aurora-harley/index.php">Aurora Harley</a>, 
           
              <a href="../author/kara-pernice/index.php">Kara Pernice</a>, and 
           
              <a href="../author/raluca-budiu/index.php">Raluca Budiu</a>
           
          
        on  July 19, 2015
        
      </div>

      

      
      <div class="topic-links">
        Topics:
        
          <ul>

  <li><a href="../../topic/mobile-and-tablet-design/index.php">Mobile &amp; Tablet</a></li>

  <li><a href="../../topic/design-patterns/index.php">Design Patterns</a></li>

  <li><a href="../../topic/visual-design/index.php">Visual Design</a></li>

</ul>
        
      </div>
      
    </div>
  </header>
  

  
  
  <section>
    <p class="lede"><strong>Summary:</strong> Page control (dots), Submit at top, and the Plus (+) and Move icons are 4 common iOS patterns that cause usability problems in testing.</p>
    <br />
  </section>
  
  

  
  <section id="article-body"><p>Large software organizations (such as Apple, Microsoft, and Google) create design guidelines to help both users and designers.</p>

<ul>
	<li>On the one hand, designers and developers can get a head start on creating interfaces that will hopefully be good, without needing to invent (and test) all-new UI elements.</li>
	<li>On the other hand, users get a consistent look-and-feel across applications running on the same operating system if all designers comply with these guidelines.</li>
</ul>

<p>Following style guides is almost always recommended. But there are some cases where the “official” design does not work well in practice. Nonetheless, for reasons unknown—maybe the recommendation was a trade-off, it wasn't thoroughly researched, or it seemed to be the best possible a solution to a very difficult design challenge—it still made it in the style guide.</p>

<p>The following are 4 common iOS patterns that Apple has used extensively in its apps and that have been followed by many other designers. Some of them are part of the <a href="https://developer.apple.com/library/ios/documentation/UserExperience/Conceptual/MobileHIG/index.php">iOS human-interface guidelines</a>. Time and again, we have seen these designs cause usability problems. The Apple gods may strike us down with lightning, but we <strong>recommend against following these patterns </strong>because they fail in usability testing:</p>

<ol>
	<li>Page control: dots to indicate pages</li>
	<li>Form <em>Submit</em> button at the top</li>
	<li><em>Plus</em> (+) icon</li>
	<li><em>Move</em> icon</li>
</ol>

<h2>1. Page Control: Dots to Indicate Pages</h2>

<p>Many iOS users are familiar with the page control. The iOS page control is a <strong>line of horizontal dots that represent open views or pages</strong>. The currently selected page is represented with an opaque dot and the other pages are represented with translucent dots.</p>

<figure class="caption"><img alt="iOS home screen uses dots (page control) to indicate the current view" height="354" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2015/07/12/apps.png" width="200"/>
<figcaption><em>The iOS home screen uses dots (page control) to indicate the current view and the number of additional views (that is, additional pages of apps pages) available. While this is one of the most recognized and common examples of the dots it’s also one of the few cases where they work tolerably well, because users know that they have multiple screens full of a muddled mix of icons that people swipe through in the attempt to locate a desired application. (The overall UX of locating icons is not ideal, but the page dots are not the problem.)</em></figcaption>
</figure>

<p>Some apps and sites use this interface element to indicate that users can swipe through pages of content while others use it in a partial area of the page to show a carousel of content. It’s a popular pattern in both mobile-web and app design, but it is also one <strong>commonly overlooked by users</strong>. In usability testing, these dots are often <strong>too subtle in the interface</strong> to clearly indicate to users that there is another view of content available. As such, they should <strong>never be used for key functionality</strong>, such as navigating between features, or as the sole method for accessing information.</p>

<figure class="caption"><img alt="use dots at the bottom of the page and swipe " height="438" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2015/07/13/stockmarket_dots_tr_rs.png" width="500"/>
<figcaption><em>In the iOS app Stock Market HD Stocks and Shares, the only apparent way to navigate between the </em>Watchlist, Wishlist, Sold <em>and </em>My Holdings<em> views is to notice the dots at the bottom of the page and swipe to discover the additional functionality.</em></figcaption>
</figure>

<p>While designers and developers can select what colors to use for the tint of the circles, it is difficult to make a small, subtle, and translucent design element stand out on a page. Many designs use these circles on top of busy images, which causes an already subtle design to further <strong>fade into the background</strong>. If using the dots, help users notice them by increasing the color contrast between the dots and background, and placing the dots on a solid color background when possible.</p>

<figure class="caption"><img alt="zappos dots barely visible" height="438" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2015/07/13/zappos_carousel_tr_rs.png" width="500"/>
<figcaption><em>The Zappos iOS app uses dots to indicate multiple views of content in the top half of the page. The dots are barely visible on the background of the shoe in the first image and disappear completely into the background picture of the chair in the second image.</em></figcaption>
</figure>

<p>Some sites and apps take liberties even with the existing iOS conventions, using squares or other shapes or moving the dots around the page. Users struggle to locate and use these dots even when they follow the design conventions described by the iOS guidelines. Changing the elements to diverge from these standards makes them that much <strong>more difficult to identify and understand</strong>. If using the dots, center them and place them directly below the elements they control.</p>

<figure class="caption"><img alt="Fab app for Android borrows the iOS-inspired dots" height="355" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2015/07/12/fab_dotscarousel.png" width="200"/>
<figcaption><em>The Fab app for Android borrows the iOS-inspired dots, but places them to the right side of the carousel rather than centering them.</em></figcaption>
</figure>

<p>Even if users do notice the dots, an essential usability problem remains. The dots <strong>allow only </strong><a href="../direct-vs-sequential-access/index.php"><strong>sequential access</strong></a> <strong>to content</strong>, and provide no indication of what the content is. Whether the dots indicate items in a carousel or stand for separate pages in a deck-of-cards pattern, the same <a href="../designing-effective-carousels/index.php">usability concerns</a> apply. In particular, carousels using symbols to represent pages limit users’ control over their experience. Users have no way to know what’s coming next and no way to navigate directly to content of interest.</p>

<h3><strong>Instead, we recommend:</strong></h3>

<ul>
	<li>Consider if content should even be accessed via swiping. It may be better accessed via navigation or text links.</li>
	<li>Cut off a piece of content (text or image) to visually indicate that users can swipe to reveal more information.</li>
</ul>

<figure class="caption"><img alt="iOS App Store avoids the use of dots and instead uses cut off " height="354" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2015/07/12/appstore_carousel.png" width="200"/>
<figcaption><em>The iOS App Store avoids the use of dots and instead uses cut off elements to show users that the navigational carousels are movable.</em></figcaption>
</figure>

<h2>2. Form Submit Button (or Equivalent) at the Top</h2>

<p>On iOS, the <em>Done</em> button is often displayed in a navigation bar at the top of the page. Sometimes the form <em>Submit</em> button (whether called <em>Submit</em> or something else — for instance, <em>Place Order</em>) is also placed at the top of the form. This pattern has started to trickle into Android apps as well.</p>

<figure class="caption"><img alt="Add button that submits form is in the top right corner" height="433" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2015/07/13/calendar.png" width="500"/>
<figcaption><em>Calendar for iOS (left): The </em>Add <em>button that submits the form is placed in the top right corner, in the navigation bar. </em>Todoist <em>(a to-do–list app) for Android (right) borrows the iOS pattern and places the </em>Submit <em>button (the checkmark icon) in the top right corner. (Note also that the form fields violate <a href="../mobile-input-checklist/index.php">many of our guidelines</a>: the labels are inside the field, and can you guess what GMT-8:00 stands for?)</em></figcaption>
</figure>

<p>Even in iOS apps we recommend against following this pattern for the simple reason that it <strong>goes against the natural top–bottom workflow</strong> on the page. As users fill in the form, they usually do it top to bottom. When they get to the end of it, they expect to find a <em>Submit</em> button right there, next to the last field they have completed. Most of the time, when people don’t find it there, they get confused and start looking around, not knowing what to do.</p>

<p>In the examples below (Pinkberry and Nordstrom), the <em>Sign In</em> and <em>Place Order</em> buttons need to be pressed after the user has filled in the form. Placing these buttons at the top of the screen is against the natural flow of filling in the form: once done with all the fields of the form, users find themselves at the bottom of the page, left with nothing to do next. Even on a small mobile screen, looking for and finding the corresponding <em>Submit</em> button requires unnecessary extra effort that could be saved if that button was placed at the bottom of the page, under the last field, as it is customary on most forms on the desktop.</p>

<p>(The generally accepted design guideline for any GUI — mobile or not — is <a href="../closeness-of-actions-and-objects-gui/index.php">proximity between actions and objects</a>, not to place the action button as far away as possible from the data it acts upon.)</p>

<figure class="caption"><img alt="working area is disconnected from the Submit button" height="429" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2015/07/13/pinkberry-nordstrom.png" width="500"/>
<figcaption><em>The working area in Pinkberry for iPhone (left) and in Nordstrom for iPhone (right) is disconnected from the </em>Submit <em>buttons (labeled </em>Sign In <em>and </em>Place Order,<em> respectively), which are placed at the top of the screen.</em></figcaption>
</figure>

<p>One of the advantages of having the <em>Submit</em> button in the page header is that, because this header is sticky in iOS apps, users can access the button easily at all times—whether they have filled in all the form fields and reached the bottom of the list, or not. (For the Calendar app it may be reasonable to assume that users won’t bother with all fields, but for a login form it’s not.) In any case, if having the button accessible at all times is important for you, consider instead a sticky Submit button at the bottom of the page, like in the Hotel Tonight app. Yes, you will sacrifice some screen real estate, but in return you will gain a better, more usable interaction flow.</p>

<figure class="caption"><img alt="The Pay button at the bottom of the page is persistent and accessible at all times" height="355" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2015/07/12/hoteltonight.png" width="200"/>
<figcaption><em>Hotel Tonight: The </em>Pay <em>button at the bottom of the page is persistent and accessible at all times. This allows users to interact with the page as much as necessary while having consistent access to the payment button, which appears in a predictable and logical location.</em></figcaption>
</figure>

<h3><strong>Instead, we recommend:</strong></h3>

<p>Display the form <em>Submit</em> button (or equivalent) under the form fields rather than at the top of the page.</p>

<h2>3. <em>Plus</em> (+) Icon</h2>

<p>Throughout many mobile apps, the <em>Plus</em> icon is used to represent a variety of functions. When placed within a top navigation bar, it most commonly means “Add,” but when placed within a table or list of items the icon can signify either adding that item to some sort of list or group, or expanding that list item for more detailed information. When multiple interpretations exist, it becomes <a href="../icon-usability/index.php">difficult for users to consistently understand an icon's meaning</a>.</p>

<figure class="caption"><img alt="a Plus icon most commonly means to add a new item" height="354" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2015/07/12/plusicon_example1-addnew_border.png" width="200"/>
<figcaption><em>When located in the top navigation bar, a Plus icon (top right corner) most commonly means to add a new item, as used in the MyFitnessPal app to allow adding a friend.</em></figcaption>
</figure>

<p>The usability of the <em>Plus</em> icon is highly dependent on where it is located within a UI. When found in a common place, such as a navigation bar, the meaning is usually correctly understood as long as adding new items makes sense for the type of content shown on that screen. However, when the <em>Plus</em> icon appears within the main content of an interface, it can have <strong>multiple meanings, which creates confusion</strong> for users.</p>

<p>For example, a previous version of the app <em>Any.do</em> displayed the <em>Plus</em> icon alongside the label of existing to-do lists. In this context, it is unclear whether tapping the <em>Plus</em> icon would expand the list to view the items it contained, or if it would trigger adding a new to-do item to the corresponding list. The current version of the app has solved this ambiguity by relocating the <em>Plus</em> to the navigation bar: however it is now used to add an entirely new to-do list.</p>

<figure class="caption"><img alt="the meaning of the Plus icon was unclear" height="439" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2015/07/12/plusicon_example2-differentmeanings.png" width="500"/>
<figcaption><em>In a previous version of the Any.do iOS app (left), the meaning of the Plus icon was unclear, as it could be interpreted as either allowing the expansion of the list or adding an item to a specific list. In the current version (right), the Plus icon is part of the top navigation bar, and is now used to add an entirely new to-do list.</em></figcaption>
</figure>

<p>Leveraging the <em>Plus</em> icon to trigger actions is particularly harmful when the user misinterprets what will occur. Since the <em>Plus</em> icon is regularly used on the web and in mobile apps to communicate that a list can be expanded—together with the arrow and the caret—users often do not expect that same <em>Plus</em> icon to actually perform some sort of action. In the LinkedIn mobile app, a <em>Plus</em> icon in a circle is used to <em>Follow</em> a company or <em>Join</em> a group depending on where it is located. In usability testing, several users were surprised when they accidentally joined groups they were intending to merely get more information about.</p>

<figure class="caption"><img alt="LinkedIn app reuses the same Plus icon throughout the app" height="355" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2015/07/12/plusicon_example3-follow.png" width="200"/>
<figcaption><em>The LinkedIn app reuses the same </em>Plus <em>icon throughout the app for a variety of functions. On the main home screen shown above, the </em>Plus <em>icon can be tapped to follow a company (without confirming this is what the user intended). When searching in the app, the </em>Plus <em>icon can also be used to quickly join groups. Thankfully the questionable icon is omitted from the search results for People or Jobs, as that could lead to some interesting consequences.</em></figcaption>
</figure>

<p>Avoid adding the <em>Plus</em> icon haphazardly throughout an app, as it can be interpreted in several different ways depending on its exact context within the interface.</p>

<h3><strong>Instead, we recommend:</strong></h3>

<ul>
	<li>While the navigation bar is a fairly safe location, displaying the + icon in other areas in a design must be tested with users to ensure the meaning is correctly understood.</li>
	<li>The only way to fully avoid the issue is to also avoid the icon, and to use an arrow or caret for expandable lists, and text labels for buttons triggering any action associated with a list item.</li>
</ul>

<h2>4. Move Icon</h2>

<figure class="caption"><img alt="move icon" height="53" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2015/07/12/move.png" width="55"/>
<figcaption><em>Move icon</em></figcaption>
</figure>

<p>Like many icons for mobile devices, the move icon does not clearly reveal what it does. By looking at it you probably will not recognize that this icon will move an item in a list. The 3 horizontal lines used for the <em>Move</em> icon may represent a list, or maybe, as one reader pointed out, "ridges preventing the finger from slipping off the item while dragging it." When this icon appears within a row of a list, users are supposed to tap the icon and hold it while dragging the related item to its rightful place in the list. There are a few usability issues with this pattern.</p>

<p>When items are movable in a list, users expect to tap directly on an item (word, button, etc.) and drag it. They don’t expect to need to touch a small, hard-to-decipher icon instead and drag that. The icon is a much smaller and harder to tap target than the item in the list itself is. So it increases the <a href="../interaction-cost-definition/index.php">interaction cost</a> and makes users work harder to tap and drag than they should need to. Additionally, in list views, users expect all the elements in a row to be associated with the same action: in other words, they think that dragging the item or the icon should do the same thing.</p>

<figure class="caption"><img alt="app displays the shadow text of the item as it moves" height="436" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2015/07/12/yahoo-finance_before_drag_after.png" width="750"/>
<figcaption><em>YAHOO! Finance uses Apple’s iOS recommendation for moving items in a list. Tap the 3-horizontal-line icon on the right (left) and drag and move the selected item. The app displays the shadow text of the item as it moves (middle) and the change takes effect (right). The list items themselves are a much larger tap target, but tapping and dragging them does not work.</em></figcaption>
</figure>

<p>Finally, nearly the exact same icon recommended in the iOS style guide has come to mean something completely different on the mobile web and elsewhere: The “hamburger” icon looks incredibly similar to the move icon.</p>

<figure class="caption"><img alt="move and hamburger" height="53" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2015/07/12/move_hamburger.png" width="129"/>
<figcaption><em>On the left is a Move icon, on the right is a hamburger menu.</em></figcaption>
</figure>

<p>It can be disconcerting and confusing to people when the same thing, or what is perceived to be the same thing, is used to invoke different commands. If the hamburger menu continues to gain traction, more users will learn that clicking three horizontal lines opens a menu (even if they may still overlook this feature more often than we would like). But this is not how the move icon works. This disagreement hurts users’ ability to <a href="../recognition-and-recall/index.php">recall</a> what the icons do after they have already used them.</p>

<h3><strong>Instead, we recommend:</strong></h3>

<ul>
	<li>Make it possible to tap and drag items in place without needing to click a particular icon to do so.</li>
	<li>Instead of the hamburger icon alone for a menu, display it with the word <em>Menu</em> to indicate that a menu will open, or use the word <em>Menu</em> alone.</li>
</ul>

<h2>Conclusion</h2>

<p>Deviating from well-researched interface patterns is usually not recommended. It is better to follow the probable best practices, knowing that consistency and knowledge transferred from use of other iOS apps will prime <a href="../efficiency-vs-expectations/index.php">users’ expectations</a>. With any guidelines or style guides, doing usability testing can demonstrate or disprove if the guideline works well in your own design. Through watching people use these designs, we have witnessed enough consistent issues to recommend against using these 4 patterns.</p>
</section>
  

  
  <section>
    
        
    
  </section>
  
  
  
  
  

  


  <section>
    <br />
    <p class="share"><b>Share <span class="hide-for-small-down">this article</span>:</b> <a href="https://twitter.com/intent/tweet?url=http://www.nngroup.com/articles/4-ios-rules-break/&amp;text=4%20iOS%20Rules%20to%20Break%20&amp;via=nngroup">Twitter</a> | <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=http://www.nngroup.com/articles/4-ios-rules-break/&amp;title=4%20iOS%20Rules%20to%20Break%20&amp;source=Nielsen%20Norman%20Group">LinkedIn</a> | <a href="https://plus.google.com/share?url=http://www.nngroup.com/articles/4-ios-rules-break/">Google+</a> | <a href="mailto:?subject=NN/g Article: 4 iOS Rules to Break &amp;body=http://www.nngroup.com/articles/4-ios-rules-break/">Email</a></p>
    <hr />
  </section>

  <footer class="l-subsection">
    
    
    <div class="learn-more">
      <h2>Learn More</h2>
      <p><a href="../subscribe/index.php">Subscribe to the weekly newsletter</a> to get notified about future articles.</p>
      </div>
    
    

    <div class="row show-for-small-down related-content">
      
      
      

      
      
        <div class="column small-12 topic-links">All articles about:
          <ul>
            
            <li><a href="../../topic/design-patterns/index.php">Design Patterns</a></li>
            
            <li><a href="../../topic/ios/index.php">ios</a></li>
            
            <li><a href="../../topic/mobile-and-tablet-design/index.php">Mobile &amp; Tablet</a></li>
            
            <li><a href="../../topic/standards-conventions/index.php">standards &amp; conventions</a></li>
            
            <li><a href="../../topic/visual-design/index.php">Visual Design</a></li>
            
          </ul>
        </div>
      
      

      
      
        <div class="column small-12">
          <h3>UX Conference Training Courses</h3>
           <ul class="no-bullet">
    
        <li><a href="../../courses/hci/index.php">User Interface Principles Every Designer Must Know</a></li>
    
        <li><a href="../../courses/cross-channel-user-experience/index.php">Omnichannel Journeys and Customer Experience</a></li>
    
        <li><a href="../../courses/emerging-patterns-web-design/index.php">Emerging Patterns for Web Design</a></li>
    
        <li><a href="../../courses/usability-mobile-websites-apps/index.php">Mobile User Experience</a></li>
    
        <li><a href="../../courses/web-page-design/index.php">Web Page UX Design</a></li>
    
</ul>
        </div>
      
      

      
      
        <div class="small-12 column">
          <h3>Research Reports</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../reports/tablets/index.php">Tablet Website and Application UX</a></li>
              
            
              
                <li><a href="../../reports/mobile-website-and-application-usability/index.php">User Experience for Mobile Applications and Websites</a></li>
              
            
              
                <li><a href="../../reports/enterprise-mobile-showcase/index.php">Mobile Intranets and Enterprise Apps</a></li>
              
            
              
                <li><a href="../../reports/wap-usability/index.php">WAP Usability</a></li>
              
            
              
                <li><a href="../../reports/ipad-app-and-website-usability/index.php">iPad App and Website Usability</a></li>
              
            
          </ul>
        </div>
      
      

      
      
        <div class="small-12 column">
          <h3>Online Seminars</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../online-seminars/every-word-count/index.php">Making Every Word Count</a></li>
              
            
              
                <li><a href="../../online-seminars/informal-sketching/index.php">Informal Sketching Techniques</a></li>
              
            
              
                <li><a href="../../online-seminars/mobile-user-testing/index.php">Mobile User Testing</a></li>
              
            
          </ul>
        </div>
      
      
    </div>

    <div class="hide-for-small-down row related-content">
      <div class="medium-6 columns">
        
        
          <div>
            <h3>Articles</h3>
            <ul class="no-bullet">
              
                
                <li><a href="../ios-9-back-to-app-button/index.php">iOS 9 App Switching and the Back-to-App Button</a></li>
                
              
                
                <li><a href="../mobile-content/index.php">Reading Content on Mobile Devices</a></li>
                
              
                
                <li><a href="../ios-7/index.php">iOS 7 User-Experience Appraisal</a></li>
                
              
                
                <li><a href="../mobile-behavior-india/index.php">Mobile User Behavior in India</a></li>
                
              
                
                <li><a href="../zen-mode/index.php">Why Zen Mode Isn’t the Answer to Everything</a></li>
                
              
            </ul>
          </div>
        
        

        
          
          <div>
            <h3>Research Reports</h3>
            <ul class="no-bullet">
              
                
                  <li><a href="../../reports/tablets/index.php">Tablet Website and Application UX</a></li>
                
              
                
                  <li><a href="../../reports/mobile-website-and-application-usability/index.php">User Experience for Mobile Applications and Websites</a></li>
                
              
                
                  <li><a href="../../reports/enterprise-mobile-showcase/index.php">Mobile Intranets and Enterprise Apps</a></li>
                
              
                
                  <li><a href="../../reports/wap-usability/index.php">WAP Usability</a></li>
                
              
                
                  <li><a href="../../reports/ipad-app-and-website-usability/index.php">iPad App and Website Usability</a></li>
                
              
            </ul>
          </div>
          
        

      </div>
      <div class="medium-6 columns">
        
          
          <div>
            <h3>UX Conference Training Courses</h3>
             <ul class="no-bullet">
    
        <li><a href="../../courses/hci/index.php">User Interface Principles Every Designer Must Know</a></li>
    
        <li><a href="../../courses/cross-channel-user-experience/index.php">Omnichannel Journeys and Customer Experience</a></li>
    
        <li><a href="../../courses/emerging-patterns-web-design/index.php">Emerging Patterns for Web Design</a></li>
    
        <li><a href="../../courses/usability-mobile-websites-apps/index.php">Mobile User Experience</a></li>
    
        <li><a href="../../courses/web-page-design/index.php">Web Page UX Design</a></li>
    
</ul>
          </div>
          
        
        
          
          <div>
            <h3>Online Seminars</h3>
            <ul class="no-bullet">
            
                <li><a href="../../online-seminars/every-word-count/index.php">Making Every Word Count</a></li>
            
                <li><a href="../../online-seminars/informal-sketching/index.php">Informal Sketching Techniques</a></li>
            
                <li><a href="../../online-seminars/mobile-user-testing/index.php">Mobile User Testing</a></li>
            
            </ul>
          </div>
          
        
      </div>
    </div>
  </footer>
</article>

        </div>

      

    
  </div>
  <div class="row">
    
      <div class="column small-12 show-for-small-down l-navrail announcements">
        
          
        
      </div>
    
    
  </div>

    </div>
    




<footer id="page-footer">
    <div class="row hide-for-print">
        <div class="column small-12 medium-6">
            <section class="footer-popular">
                
                <h1>Popular Topics</h1>
                <div style="float: left;  width: 15em; display: block;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/applications/index.php">Application Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/e-commerce/index.php">E-Commerce Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/eyetracking/index.php">Eyetracking </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/information-architecture/index.php">Information Architecture </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/intranets/index.php">Intranet Design </a></li>
	<li><a href="../../topic/interaction-design/index.php">Interaction Design </a></li>
</ul>
</div>

<div style="float: left;  width: 15em; display: block; list-style: none;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/mobile-and-tablet-design/index.php">Mobile and Tablet Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/credibility-persuasion/index.php">Persuasive Design</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/research-methods/index.php">Research Methods</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/user-testing/index.php">User Testing </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/web-usability/index.php">Web Usability </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/writing-web/index.php">Web Writing &amp; Content Strategy </a></li>
</ul>
</div>

<p>&nbsp;</p>

                
            </section>
        </div>

        <div class="column small-12 medium-6">
            <div class="row">
                <div class="column small-6">
                    <section class="footer-about">
                        <h1>Follow Us</h1>
                        <ul>
	<li><a href="../subscribe/index.php">Subscribe to our weekly UX newsletter</a>,&nbsp;Jakob Nielsen&#39;s Alertbox</li>
	<li>Visit Don Norman&#39;s website, <a href="http://www.jnd.org/"> www.jnd.org </a></li>
	<li>Visit Bruce Tognazzini&#39;s website, <a href="http://www.asktog.com/"> www.asktog.com </a></li>
	<li style="padding-top: 6px;"><a class="twitter-follow-button" data-show-count="false" data-size="large" href="https://twitter.com/nngroup"><img alt="Follow @nngroup on Twitter" height="20" src="https://media.nngroup.com/media/editor/2016/12/30/twitter-follow-button.png" style="width: 117px; height: 20px;" width="117" /></a></li>
	<li style="padding-top: 6px;"><img alt="" height="14" src="https://media.nngroup.com/media/editor/2016/07/26/feed-icon-14x14.png" style="vertical-align:text-top;width:14px;margin-right:.25em;" width="14" /><a href="../../feed/rss/index.php">Subscribe to our RSS feed</a></li>
</ul>

                    </section>
                </div>

                <div class="column small-6">
                    <section class="footer-about right-column">
                        <h1>About
</h1>
                        <ul class="no-bullet">
	<li><a href="../../about/contact/index.php">Contact information</a></li>
	<li><a href="../../about/index.php">About Us</a></li>
	<li><a href="../../about/why-nng/index.php">Why NN/g? </a></li>
	<li><a href="../../return-policy/index.php">Return Policy</a></li>
</ul>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="copyright column small-12">
            <p><a href="../../copyright-and-reprint-info/index.php">Copyright</a> &copy; 1998-2017 Nielsen Norman Group, All Rights Reserved.</p>
        </div>
    </div>
</footer>


    


        <script src="https://media.nngroup.com/static/vendor/fastclick/js/fastclick.min.js"></script>
        <script src="https://media.nngroup.com/static/vendor/foundation/js/foundation.min.js"></script>
        <script src="http://media.nngroup.com/static/js/app.js"></script>
        <script src="http://media.nngroup.com/static/js/scroll-to-fixed.js"></script><script>var externalLinks = document.querySelectorAll('a[href^="http"]');for (var i = externalLinks.length-1; i >= 0; i--) {externalLinks[i].addEventListener("click", function(event) { alert("External links are disabled for this test."); event.preventDefault(); }, false);}var mailLinks = document.querySelectorAll('a[href^="mailto"]');for (var i = mailLinks.length-1; i >= 0; i--) {mailLinks[i].addEventListener("click", function(event) { alert("Email links are disabled for this test."); event.preventDefault(); }, false);}var externalForms = document.querySelectorAll('form');for (var i = externalForms.length-1; i >= 0; i--) {externalForms[i].addEventListener("submit", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false); externalForms[i].addEventListener("click", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false);}</script>

        <script>
            
            $(function() {
                $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
                $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
            });
        </script>

        
<script src="https://media.nngroup.com/static/js/articles.js"></script>


        
        <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1072699960;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072699960/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
    </body>

<!-- Mirrored from www.nngroup.com/articles/4-ios-rules-break/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:13:06 GMT -->
</html>
