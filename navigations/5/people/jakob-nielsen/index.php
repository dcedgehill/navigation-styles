<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->





    
<!-- Mirrored from www.nngroup.com/people/jakob-nielsen/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 14:59:35 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1026.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"beacon":"bam.nr-data.net","queueTime":0,"applicationTime":305,"agent":"","licenseKey":"1a30ef9931","errorBeacon":"bam.nr-data.net","applicationID":"2048992","transactionName":"NFxSMRBUX0EDUkIPDA0WdhAMVkVbDV8ZFgYMSVwATENYVxVCDDYGEUpfCyZQRVMLXRgBBhc="}</script>
        <title>Jakob Nielsen, Ph.D. and Principal at Nielsen Norman Group</title><meta property="og:title" content="Jakob Nielsen, Ph.D. and Principal at Nielsen Norman Group" />
  
        
        <meta name="description" content="Biography: Jakob Nielsen, Ph.D., is a Usability Advocate and principal of the Nielsen Norman Group, User Experience (UX) Research, Training, and Consulting">
        <meta property="og:description" content="Biography: Jakob Nielsen, Ph.D., is a Usability Advocate and principal of the Nielsen Norman Group, User Experience (UX) Research, Training, and Consulting" />
        
  
        
	
        

        

        <meta name="robots" content="noarchive">
        <meta name="viewport" content="width=device-width">

        <link rel="search" type="application/opensearchdescription+xml" title="NN/g Search" href="../../opensearch.xml" />
        <link rel="shortcut icon" href="http://media.nngroup.com/static/img/favicon.ico" />

        <link rel="stylesheet" href="https://media.nngroup.com/static/vendor/font-awesome/css/font-awesome.min.css">

        
        <link rel="stylesheet" href="https://media.nngroup.com/static/css/7232a960f1df.css" type="text/css" />
        <link rel="stylesheet" href="http://fast.fonts.com/t/1.css?apiType=css&amp;projectid=c5846260-9def-46ec-9407-0f1349bd2d64" type="text/css">

        <script src="https://media.nngroup.com/static/vendor/modernizr/js/modernizr.min.js"></script>

        <script type="text/javascript" src="https://media.nngroup.com/static/js/3283be5def74.js"></script>
        

        

        
        
        <!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=229034,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('http://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->


<!-- Google Analytics Tracking Code for http://nngroup.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23132681-1', 'nngroup.com');
  ga('require', 'linkid', 'https://www.nngroup.com/people/jakob-nielsen/linkid.js');
  ga('require', 'displayfeatures');

// Optimizely Universal Analytics Integration
 window.optimizely = window.optimizely || [];
 window.optimizely.push("activateUniversalAnalytics");

  ga('send', 'pageview');

</script>
<script>
/**
* Function to track a click on an outbound link in Google Analytics.
*/
function trackOutboundLink(label, url) {
    if(window.ga && ga.loaded) {
        ga('send', 'event', 'outbound', 'click', label, {'hitCallback': function(){document.location = url;}
        });
    } else {
        document.location = url;
    }
}
/**
* Function to track a click on a link in the article announcement area.
*/
$('#articleAnnouncement a').on('click', function(){
  ga('send', 'event', 'link', 'click', 'articleAnnouncement');
});
</script>

<script type="text/javascript">
 setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2421.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



<!-- Hotjar Tracking Code for http://nngroup.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:121605,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

        <script src="https://media.nngroup.com/static/js/analytics/track_outbound_link.js"></script>
        
        
        
    </head>
    <body class="location-about location-people person-detail">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
    <header id="page-header">
        <div id="cart" class="shopping-cart mobile-cart show-for-small-down"></div>
        <div class="row">
            <div id="masthead" class="masthead small-12 medium-7 column">

                <!-- The button to toggles menu on small screen -->

                <h1><a href="../../index.php"><img src="http://media.nngroup.com/static/img/logo-wordmark.png" width="74" height="30"> Nielsen <span class="norman">Norman</span> Group</a></h1>
                <p class="tagline show-for-medium-up">Evidence-Based User Experience Research, Training, and Consulting</p>

                <!-- The button to toggles form search on small screen -->


                <div style="clear: right;width:  100%;height: 1px;"></div><?php define('ROOT_PATH',$_SERVER["DOCUMENT_ROOT"] . '/apps/navigation_styles/navigations/5'); ?><?php include(ROOT_PATH)."/navigation.php"; ?>

                <!-- Drop down menu on small screen -->
                <div id="hideNav" class="show-for-small-down">
                  
                  <ul class="menu-content">
                    <li><a href="../../index.php">Home</a></li>
                    <li class="has-submenu">
                      <a>Training</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../training/index.php">Overview</a></li>
                        
<li id="nav-ux-conference"><a href="../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../online-seminars/index.php"><span>Online Seminars</span></a></li>

                      </ul>
                    </li>
                    <li class="has-submenu">
                      <a>Consulting</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
  <li><a href="../../consulting/index.php">Overview</a></li>
  
  <li><a href="../../consulting/benchmarking/index.php">Benchmarking</a></li>
  
  <li><a href="../../consulting/expert-design-review/index.php">Expert Design Review</a></li>
  
  <li><a href="../../consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
  
  <li><a href="../../consulting/usability-testing/index.php">Usability Testing </a></li>
  
  <li><a href="../../consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
  
  <li><a href="../../in-house-training/index.php">In-House Training</a></li>
</ul>

                    </li>

                    <li><a href="../../reports/index.php">Reports</a></li>
                    <li><a href="../../articles/index.php">Articles</a></li>
                    <li class="has-submenu"><a>About NN/G</a><i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../about/index.php">Overview</a></li>
                        <li><a href="../index.php">People</a></li>
                        <li><a href="../../about/why-nng/index.php">Why NN/g?</a></li>
                        <li><a href="../../about/contact/index.php">Contact</a></li>
                        <li><a href="../../news/index.php">News</a></li>
                        <li><a href="../../about/history/index.php">History</a></li>
                        <li><a href="../../books/index.php">Books</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                </div>

                <hr class="header-divider show-for-small-only"/>


            </div>
       </div>

        <div id="hideNav">            <nav class="primary-nav small-12 column">
                <ul>
                    <li><a class="navitem" id="nav-home" href="../../index.php">Home</a></li>
                    <li><a class="navitem" id="nav-training" href="../../training/index.php">Training</a></li>
                    <li><a class="navitem" id="nav-consulting" href="../../consulting/index.php">Consulting</a></li>
                    <li><a class="navitem" id="nav-reports" href="../../reports/index.php">Reports</a></li>
                    <li><a class="navitem" id="nav-articles" href="../../articles/index.php">Articles</a></li>
                    <li><a class="navitem" id="nav-about" href="../../about/index.php">About NN/G</a></li>
                </ul>
            </nav>
            <nav class="secondary-nav small-12 medium-12 large-12 columns">
                
<ul class="inline-list">
    <li id="nav-people"><a href="../index.php"><span>People</span></a></li>
    <li id="nav-whynng"><a href="../../about/why-nng/index.php"><span>Why NN/g?</span></a></li>
    <li id="nav-contact"><a href="../../about/contact/index.php"><span>Contact</span></a></li>
    <li id="nav-news"><a href="../../news/index.php"><span>News</span></a></li>
    <li id="nav-history"><a href="../../about/history/index.php"><span>History</span></a></li>
    <li id="nav-books"><a href="../../books/index.php"><span>Books</span></a></li>
</ul>

            </nav>
        </div>
    </header>
    <div class="l-content"><span id="preventClose" style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776; Menu</span>
    
<div class="row">
    <div class="l-section-intro small-12 medium-8 large-8 columns">
        <header>
            <h1>Jakob Nielsen</h1>
            <h2>Principal</h2>
        </header>
    </div>
    <div class="small-12 medium-4 large-4 columns">
    </div>
</div>

<div class="row">
    <div class="small-12 medium-8 large-8 medium-push-4 large-push-4 columns">
        <img class="show-for-small-only avatar_wrapped_text" src="https://media.nngroup.com/media/people/photos/jakob-nielsen_cropped.jpg.400x400_q95_autocrop_crop_upscale.jpg">
        <div class="bio l-subsection"><p>Jakob Nielsen, Ph.D., is a User Advocate and principal of the Nielsen Norman Group which he co-founded with Dr. Donald A. Norman (former VP of research at Apple Computer). Dr. Nielsen established the &quot;discount usability engineering&quot; movement for fast and cheap improvements of user interfaces and has invented several usability methods, including heuristic evaluation. He holds 79 United States patents, mainly on ways of making the Internet easier to use.</p>

<h4>Jakob Nielsen has been called:</h4>

<ul style="margin-left: 40px;">
	<li>&quot;the king of usability&quot; (Internet Magazine)</li>
	<li>&quot;the guru of Web page usability&quot; (The New York Times)</li>
	<li>&quot;the next best thing to a true time machine&quot; (USA Today)</li>
	<li>&quot;the smartest person on the Web&quot; (ZDNet AnchorDesk)</li>
	<li>&quot;the world&#39;s leading expert on Web usability&quot; (U.S. News &amp; World Report)</li>
	<li>one of the top 10 minds in small business (FORTUNE Small Business)</li>
	<li>&quot;the world&#39;s leading expert on user-friendly design&quot; (Stuttgarter Zeitung, Germany)</li>
	<li>&quot;knows more about what makes Web sites work than anyone else on the planet&quot; (Chicago Tribune)</li>
	<li>&quot;one of the world&#39;s foremost experts in Web usability&quot; (Business Week)</li>
	<li>&quot;the Web&#39;s usability czar&quot; (WebReference.com)</li>
	<li>&quot;the reigning guru of Web usability&quot; (FORTUNE)</li>
	<li>&quot;eminent Web usability guru&quot; (CNN)</li>
	<li>&quot;perhaps the best-known design and usability guru on the Internet&quot; (Financial Times)</li>
	<li>&quot;the usability Pope&quot; (Wirtschaftswoche Magazine, Germany)</li>
	<li>&quot;new-media pioneer&quot; (Newsweek)</li>
	<li>One of the &quot;world&#39;s most influential designers&quot; (Businessweek)</li>
</ul>

<h3>Usability Articles</h3>

<p>Since 1995, Dr. Nielsen&#39;s&nbsp;<a href="../../articles/index.php">Alertbox</a>&nbsp;articles on usability topics have been essential reading for usability and user experience professionals. Currently, the Alertbox receives over 12 million page views annually.</p>

<p><a href="../../articles/subscribe/index.php">Subscribe to the Alertbox newsletter</a></p>

<h3>Books</h3>

<ul style="margin-left: 40px;">
	<li><a href="../../books/mobile-usability/index.php">Mobile Usability</a>, 2012</li>
	<li><a href="../../books/eyetracking-web-usability/index.php">Eyetracking Web Usability</a>, 2010</li>
	<li><a href="../../books/prioritizing-web-usability/index.php">Prioritizing Web Usability</a>, 2006</li>
	<li><a href="../../books/homepage-usability/index.php">Homepage Usability: 50 Websites Deconstructed</a>, 2001 (113 guidelines for homepage design)</li>
	<li><a href="../../books/designing-web-usability/index.php">Designing Web Usability: The Practice of Simplicity</a>, 1999: a quarter million copies in print; 22 languages</li>
	<li><a href="../../books/international-user-interfaces/index.php">International User Interfaces</a>, 1996 (co-editor with with Elisa del Galdo)</li>
	<li><a href="../../books/multimedia-and-hypertext/index.php">Multimedia and Hypertext: The Internet and Beyond</a>, 1995: second edition of textbook on linked online information</li>
	<li>Advances in Human-Computer Interaction Vol. 5, 1995 (editor)</li>
	<li><a href="../../books/usability-inspection-methods/index.php">Usability Inspection Methods</a>, 1994 (co-editor with Robert L. Mack): with chapters by each of the inventors of these methods</li>
	<li><a href="../../books/usability-engineering/index.php">Usability Engineering</a>, 1993: textbook on methods to make interfaces easier to use</li>
	<li><a href="../../books/hypertext-and-hypermedia/index.php">Hypertext and Hypermedia</a>, 1990: first edition of the classic textbook (no longer in print)</li>
	<li><a href="../../books/designing-user-interfaces-international-use/index.php">Designing User Interfaces for International Use</a>, 1990 (editor)</li>
	<li><a href="../../books/coordinating-user-interfaces-for-consistency/index.php">Coordinating User Interfaces for Consistency</a>, 1989 (editor): still the best book on how to get a standard look-and-feel (reprint edition published 2002)</li>
</ul>

<h3>Professional Background</h3>

<p>Jakob Nielsen holds a Ph.D. in human&ndash;computer interaction (<a href="../../topic/human-computer-interaction/index.php">HCI</a>) from the Technical University of Denmark in Copenhagen.</p>

<p>From 1994 to 1998 he was a Sun Microsystems Distinguished Engineer. He was hired to make heavy-duty enterprise software easier to use, since large-scale applications had been the focus of most of his projects at the phone company and IBM. But luckily the job definition of a Distinguished Engineer is &quot;you&#39;re supposed to be the world&#39;s leading expert in your field, so <em>you </em>figure out what would be most important for the company for you to work on.&quot; Therefore, Dr. Nielsen ended up spending most of his time at Sun on defining the emerging field of Web usability. He was usability lead for several design rounds of Sun&#39;s website and intranet (SunWeb), including the original SunWeb design in 1994.</p>

<p>Dr. Nielsen&#39;s earlier affiliations include Bellcore (Bell Communications Research, Morristown, NJ), the IBM User Interface Institute at the T.J. Watson Research Center (Yorktown Heights, NY), the Technical University of Denmark (Copenhagen), and Aarhus University (&Aring;rhus, Denmark).</p>

<p>Professional journal editorial board memberships: <em>Behaviour &amp; Information Technology</em>, <em>Foundations and Trends in Human-Computer Interaction</em>, <em>Interacting with Computers</em>, <em>Journal of Usability Studies </em>(JUS), <em>International Journal of Human-Computer Interaction</em>, <em>The New Review of Hypermedia and Multimedia</em>. For each journal, contact the editor-in-chief to submit a manuscript.</p>

<p>In June 2000, Dr. Nielsen was inducted into the Scandinavian Interactive Media Hall of Fame and in April 2006, he was inducted into the ACM Computer-Human Interaction Academy. He was the 2013 recipient of the <a href="../../news/item/jakob-nielsen-lifetime-achievement-award/index.php">Lifetime Achievement Award for Human&ndash;Computer Interaction Practice</a> from SIGCHI, the premiere professional society in the HCI field.</p>

<h3>Profiles</h3>

<p><strong>Some profiles and interviews on other sites:</strong></p>

<ul style="margin-left: 40px;">
	<li><em>Digital Web Magazine</em> <a href="http://www.digital-web.com/articles/jakob_nielsen/">An interview with Dr. Jakob Nielsen, usability expert</a></li>
	<li><em>eLearningPost</em> <a href="http://www.elearningpost.com/articles/archives/jakob_nielsen_on_e_learning/">Jakob Nielsen on e-learning</a></li>
	<li><em>New York Time</em>s <a href="http://www.nytimes.com/library/tech/98/07/cyber/articles/13usability.php">Making Web Sites More &#39;Usable&#39;</a></li>
	<li><em>SitePoint</em> <a href="http://www.sitepoint.com/interview-jakob-nielsen/">Interview with Jakob Nielsen, Ph.D.</a></li>
	<li><em>BusinessWeek</em> <a href="http://images.businessweek.com/ss/10/02/0201_worlds_most_influential_designers/21.htm">World&#39;s Most Influential Designers</a></li>
	<li><em>IT Conversations</em> <a href="http://itc.conversationsnetwork.org/shows/detail670.php">Jakob Nielsen podcast</a></li>
	<li><em>CIO Insight</em> <a href="http://www.cioinsight.com/c/a/Expert-Voices/Time-for-a-Redesign-Dr-Jakob-Nielsen/">Time for a Redesign: Dr. Jakob Nielsen</a></li>
	<li><em>Webdesigner Depot</em> <a href="http://www.webdesignerdepot.com/2009/09/interview-with-web-usability-guru-jakob-nielsen/">Interview with Web Usability Guru, Jakob Nielsen</a></li>
	<li>UTest.com <a href="http://blog.utest.com/testing-the-limits-with-jakob-nielsen-part-i/2011/04/">Testing the Limits With Jakob Nielsen</a></li>
</ul>

<p><a href="../../people-jakob-nielsen-feature-stories/index.php">Cover stories and features stories</a>&nbsp;with Jakob Nielsen from the printed press.</p>

<h3>Parodies</h3>

<p>Many sites have made fun of me. Here are some of the more funny ones:</p>

<ul style="margin-left: 40px;">
	<li>US Press News <a href="http://www.bbspot.com/News/2003/07/letter_c.php">Jakob Nielsen Declares the Letter &quot;C&quot; Unusable</a></li>
	<li>NTK.net <a href="http://www.ntk.net/nielsen2004/">Nielsen for 2004 - At Least We&#39;ll Know Who Won This Time</a> (Actually, I can&#39;t run for President, being born overseas.)</li>
	<li>Davezilla <a href="http://www.davezilla.com/2006/02/19/jakob-nielsen-fighting-styles/">Jakob Nielsen&rsquo;s Usability Fighting Styles</a> (Note that I am originally from Denmark, not Sweden, so associating me with Swedish meatballs is slightly offensive &mdash; everybody knows that Danish meatballs taste better :-)</li>
	<li>GroGraphics <a href="http://www.grographics.com/usabilitysucks/magritte.php">Jakob Nielsen&#39;s review of Magritte&#39;s &quot;The Betrayal of Images&quot;</a></li>
</ul>

<h3>See Also</h3>

<ul>
	<li><a href="../../news/type/press-mentions/index.php">Interviews and press coverage</a></li>
	<li><a href="../../people-jakob-nielsen-publications/index.php">Full publications list</a></li>
	<li><a href="../../people-jakob-nielsen-photos/index.php">High-resolution photos</a></li>
</ul>

<h3>Standard Name Identifiers</h3>

<ul>
	<li>International Standard Name Identifier: <a href="http://isni-url.oclc.nl/isni/0000000084079633">ISNI 0000 0000 8407 9633</a></li>
	<li>Virtual International Authority File <a href="http://viaf.org/viaf/110674979/">VIAF ID 110674979</a></li>
	<li>Library of Congress Control Number: <a href="http://id.loc.gov/authorities/names/n88177029">LCCN n88177029</a></li>
	<li>Open Researcher and Contributor ID: <a href="http://orcid.org/0000-0002-1554-3062">ORCID 0000-0002-1554-3062</a></li>
	<li><a href="http://www.worldcat.org/wcidentities/lccn-n88-177029">WorldCat</a></li>
</ul>

<p>Preferred Katakana:&nbsp; ヤコブ&middot;ニールセン</p>
</div>

        

        
    </div>
    <div class="small-12 medium-4 large-4 medium-pull-8 large-pull-8 columns columns">
        <div class="l-subsection hide-for-small-only">
            <img src="https://media.nngroup.com/media/people/photos/jakob-nielsen_cropped.jpg.400x400_q95_autocrop_crop_upscale.jpg">
        </div>

        <section class="l-subsection collapsable-content">
            <h1 class="hide-for-medium-up collapse-link">NN/g People</h1>
            <h1 class="show-for-medium-up">NN/g People</h1>
            <div class="collapsable">
                <ul class="no-bullet">
                    
                    <li>
                        
                            <a href="../raluca-budiu/index.php">Raluca Budiu</a>
                        
                    </li>
                    
                    <li>
                        
                            <a href="../susan-farrell/index.php">Susan Farrell</a>
                        
                    </li>
                    
                    <li>
                        
                            <a href="../therese-fessenden/index.php">Therese Fessenden</a>
                        
                    </li>
                    
                    <li>
                        
                            <a href="../kim-flaherty/index.php">Kim Flaherty</a>
                        
                    </li>
                    
                    <li>
                        
                            <a href="../tabitha-frahm/index.php">Tabitha Frahm</a>
                        
                    </li>
                    
                    <li>
                        
                            <a href="../sarah-gibbons/index.php">Sarah Gibbons</a>
                        
                    </li>
                    
                    <li>
                        
                            <a href="../garrett-goldfield/index.php">Garrett Goldfield</a>
                        
                    </li>
                    
                    <li>
                        
                            <a href="../aurora-harley/index.php">Aurora Harley</a>
                        
                    </li>
                    
                    <li>
                        
                            <a href="../luice-hwang/index.php">Luice Hwang</a>
                        
                    </li>
                    
                    <li>
                        
                            <a href="../kate-kaplan/index.php">Kate Kaplan</a>
                        
                    </li>
                    
                    <li>
                        
                            <a href="../page-laubheimer/index.php">Page Laubheimer</a>
                        
                    </li>
                    
                    <li>
                        
                            <a href="../angela-li/index.php">Angie Li</a>
                        
                    </li>
                    
                    <li>
                        
                            <a href="../hoa-loranger/index.php">Hoa Loranger</a>
                        
                    </li>
                    
                    <li>
                        
                            <a href="../kate-meyer/index.php">Kate Meyer</a>
                        
                    </li>
                    
                    <li>
                        
                            <strong>Jakob Nielsen</strong>
                        
                    </li>
                    
                    <li>
                        
                            <a href="../don-norman/index.php">Don Norman</a>
                        
                    </li>
                    
                    <li>
                        
                            <a href="../kara-pernice/index.php">Kara Pernice</a>
                        
                    </li>
                    
                    <li>
                        
                            <a href="../jeanette-pidanick/index.php">Jeanette Pidanick</a>
                        
                    </li>
                    
                    <li>
                        
                            <a href="../amy-schade/index.php">Amy Schade</a>
                        
                    </li>
                    
                    <li>
                        
                            <a href="../katie-sherwin/index.php">Katie Sherwin</a>
                        
                    </li>
                    
                    <li>
                        
                            <a href="../bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a>
                        
                    </li>
                    
                    <li>
                        
                            <a href="../kathryn-whitenton/index.php">Kathryn Whitenton</a>
                        
                    </li>
                    
                </ul>
            </div>
        </section>
    </div>
</div>


    </div>
    




<footer id="page-footer">
    <div class="row hide-for-print">
        <div class="column small-12 medium-6">
            <section class="footer-popular">
                
                <h1>Popular Topics</h1>
                <div style="float: left;  width: 15em; display: block;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/applications/index.php">Application Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/e-commerce/index.php">E-Commerce Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/eyetracking/index.php">Eyetracking </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/information-architecture/index.php">Information Architecture </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/intranets/index.php">Intranet Design </a></li>
	<li><a href="../../topic/interaction-design/index.php">Interaction Design </a></li>
</ul>
</div>

<div style="float: left;  width: 15em; display: block; list-style: none;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/mobile-and-tablet-design/index.php">Mobile and Tablet Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/credibility-persuasion/index.php">Persuasive Design</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/research-methods/index.php">Research Methods</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/user-testing/index.php">User Testing </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/web-usability/index.php">Web Usability </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/writing-web/index.php">Web Writing &amp; Content Strategy </a></li>
</ul>
</div>

<p>&nbsp;</p>

                
            </section>
        </div>

        <div class="column small-12 medium-6">
            <div class="row">
                <div class="column small-6">
                    <section class="footer-about">
                        <h1>Follow Us</h1>
                        <ul>
	<li><a href="../../articles/subscribe/index.php">Subscribe to our weekly UX newsletter</a>,&nbsp;Jakob Nielsen&#39;s Alertbox</li>
	<li>Visit Don Norman&#39;s website, <a href="http://www.jnd.org/"> www.jnd.org </a></li>
	<li>Visit Bruce Tognazzini&#39;s website, <a href="http://www.asktog.com/"> www.asktog.com </a></li>
	<li style="padding-top: 6px;"><a class="twitter-follow-button" data-show-count="false" data-size="large" href="https://twitter.com/nngroup"><img alt="Follow @nngroup on Twitter" height="20" src="https://media.nngroup.com/media/editor/2016/12/30/twitter-follow-button.png" style="width: 117px; height: 20px;" width="117" /></a></li>
	<li style="padding-top: 6px;"><img alt="" height="14" src="https://media.nngroup.com/media/editor/2016/07/26/feed-icon-14x14.png" style="vertical-align:text-top;width:14px;margin-right:.25em;" width="14" /><a href="../../feed/rss/index.php">Subscribe to our RSS feed</a></li>
</ul>

                    </section>
                </div>

                <div class="column small-6">
                    <section class="footer-about right-column">
                        <h1>About
</h1>
                        <ul class="no-bullet">
	<li><a href="../../about/contact/index.php">Contact information</a></li>
	<li><a href="../../about/index.php">About Us</a></li>
	<li><a href="../../about/why-nng/index.php">Why NN/g? </a></li>
	<li><a href="../../return-policy/index.php">Return Policy</a></li>
</ul>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="copyright column small-12">
            <p><a href="../../copyright-and-reprint-info/index.php">Copyright</a> &copy; 1998-2017 Nielsen Norman Group, All Rights Reserved.</p>
        </div>
    </div>
</footer>


    


        <script src="https://media.nngroup.com/static/vendor/fastclick/js/fastclick.min.js"></script>
        <script src="https://media.nngroup.com/static/vendor/foundation/js/foundation.min.js"></script>
        <script src="http://media.nngroup.com/static/js/app.js"></script>
        <script src="http://media.nngroup.com/static/js/scroll-to-fixed.js"></script><script>var externalLinks = document.querySelectorAll('a[href^="http"]');for (var i = externalLinks.length-1; i >= 0; i--) {externalLinks[i].addEventListener("click", function(event) { alert("External links are disabled for this test."); event.preventDefault(); }, false);}var mailLinks = document.querySelectorAll('a[href^="mailto"]');for (var i = mailLinks.length-1; i >= 0; i--) {mailLinks[i].addEventListener("click", function(event) { alert("Email links are disabled for this test."); event.preventDefault(); }, false);}var externalForms = document.querySelectorAll('form');for (var i = externalForms.length-1; i >= 0; i--) {externalForms[i].addEventListener("submit", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false); externalForms[i].addEventListener("click", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false);}</script>

        <script>
            
            $(function() {
                $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
                $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
            });
        </script>

        


        
        <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1072699960;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072699960/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
    </body>

<!-- Mirrored from www.nngroup.com/people/jakob-nielsen/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 14:59:35 GMT -->
</html>
