<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->





    
<!-- Mirrored from www.nngroup.com/articles/flexible-usability-testing/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:13:55 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1026.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"licenseKey":"1a30ef9931","errorBeacon":"bam.nr-data.net","applicationID":"2048992","transactionName":"NFxSMRBUX0EDUkIPDA0WdhAMVkVbDV8ZBxEXUFMJB0YfRAtUQRVZIktEDAFZVHYHRVcPD01eVRE=","beacon":"bam.nr-data.net","queueTime":3,"applicationTime":435,"agent":""}</script>
        <title>Flexible Usability Testing Tips</title><meta property="og:title" content="Flexible Usability Testing Tips" />
  
        
        <meta name="description" content="Observe a greater range of site features when tasks change as client questions are answered and as tasks are tweaked or created for individual participants.">
        <meta property="og:description" content="Observe a greater range of site features when tasks change as client questions are answered and as tasks are tweaked or created for individual participants." />
        
  
        
	
        
        <meta name="keywords" content="methodology, usability methodology, user testing, qualitative studies, iteration, test tasks, usability tasks, tasks, writing tasks, test plan, study plan, study planning">
        

        

        <meta name="robots" content="noarchive">
        <meta name="viewport" content="width=device-width">

        <link rel="search" type="application/opensearchdescription+xml" title="NN/g Search" href="../../opensearch.xml" />
        <link rel="shortcut icon" href="http://media.nngroup.com/static/img/favicon.ico" />

        <link rel="stylesheet" href="https://media.nngroup.com/static/vendor/font-awesome/css/font-awesome.min.css">

        
        <link rel="stylesheet" href="https://media.nngroup.com/static/css/7232a960f1df.css" type="text/css" />
        <link rel="stylesheet" href="http://fast.fonts.com/t/1.css?apiType=css&amp;projectid=c5846260-9def-46ec-9407-0f1349bd2d64" type="text/css">

        <script src="https://media.nngroup.com/static/vendor/modernizr/js/modernizr.min.js"></script>

        <script type="text/javascript" src="https://media.nngroup.com/static/js/3283be5def74.js"></script>
        

        

        
        
        <!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=229034,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('http://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->


<!-- Google Analytics Tracking Code for http://nngroup.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23132681-1', 'nngroup.com');
  ga('require', 'linkid', 'https://www.nngroup.com/articles/flexible-usability-testing/linkid.js');
  ga('require', 'displayfeatures');

// Optimizely Universal Analytics Integration
 window.optimizely = window.optimizely || [];
 window.optimizely.push("activateUniversalAnalytics");

  ga('send', 'pageview');

</script>
<script>
/**
* Function to track a click on an outbound link in Google Analytics.
*/
function trackOutboundLink(label, url) {
    if(window.ga && ga.loaded) {
        ga('send', 'event', 'outbound', 'click', label, {'hitCallback': function(){document.location = url;}
        });
    } else {
        document.location = url;
    }
}
/**
* Function to track a click on a link in the article announcement area.
*/
$('#articleAnnouncement a').on('click', function(){
  ga('send', 'event', 'link', 'click', 'articleAnnouncement');
});
</script>

<script type="text/javascript">
 setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2421.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



<!-- Hotjar Tracking Code for http://nngroup.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:121605,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

        <script src="https://media.nngroup.com/static/js/analytics/track_outbound_link.js"></script>
        
        
        
    </head>
    <body class="location-articles articles-detail">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
    <header id="page-header">
        <div id="cart" class="shopping-cart mobile-cart show-for-small-down"></div>
        <div class="row">
            <div id="masthead" class="masthead small-12 medium-7 column">

                <!-- The button to toggles menu on small screen -->

                <h1><a href="../../index.php"><img src="http://media.nngroup.com/static/img/logo-wordmark.png" width="74" height="30"> Nielsen <span class="norman">Norman</span> Group</a></h1>
                <p class="tagline show-for-medium-up">Evidence-Based User Experience Research, Training, and Consulting</p>

                <!-- The button to toggles form search on small screen -->


                <div style="clear: right;width:  100%;height: 1px;"></div><?php define('ROOT_PATH',$_SERVER["DOCUMENT_ROOT"] . '/apps/navigation_styles/navigations/5'); ?><?php include(ROOT_PATH)."/navigation.php"; ?>

                <!-- Drop down menu on small screen -->
                <div id="hideNav" class="show-for-small-down">
                  
                  <ul class="menu-content">
                    <li><a href="../../index.php">Home</a></li>
                    <li class="has-submenu">
                      <a>Training</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../training/index.php">Overview</a></li>
                        
<li id="nav-ux-conference"><a href="../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../online-seminars/index.php"><span>Online Seminars</span></a></li>

                      </ul>
                    </li>
                    <li class="has-submenu">
                      <a>Consulting</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
  <li><a href="../../consulting/index.php">Overview</a></li>
  
  <li><a href="../../consulting/benchmarking/index.php">Benchmarking</a></li>
  
  <li><a href="../../consulting/expert-design-review/index.php">Expert Design Review</a></li>
  
  <li><a href="../../consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
  
  <li><a href="../../consulting/usability-testing/index.php">Usability Testing </a></li>
  
  <li><a href="../../consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
  
  <li><a href="../../in-house-training/index.php">In-House Training</a></li>
</ul>

                    </li>

                    <li><a href="../../reports/index.php">Reports</a></li>
                    <li><a href="../index.php">Articles</a></li>
                    <li class="has-submenu"><a>About NN/G</a><i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../about/index.php">Overview</a></li>
                        <li><a href="../../people/index.php">People</a></li>
                        <li><a href="../../about/why-nng/index.php">Why NN/g?</a></li>
                        <li><a href="../../about/contact/index.php">Contact</a></li>
                        <li><a href="../../news/index.php">News</a></li>
                        <li><a href="../../about/history/index.php">History</a></li>
                        <li><a href="../../books/index.php">Books</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                </div>

                <hr class="header-divider show-for-small-only"/>


            </div>
       </div>

        <div id="hideNav">            <nav class="primary-nav small-12 column">
                <ul>
                    <li><a class="navitem" id="nav-home" href="../../index.php">Home</a></li>
                    <li><a class="navitem" id="nav-training" href="../../training/index.php">Training</a></li>
                    <li><a class="navitem" id="nav-consulting" href="../../consulting/index.php">Consulting</a></li>
                    <li><a class="navitem" id="nav-reports" href="../../reports/index.php">Reports</a></li>
                    <li><a class="navitem" id="nav-articles" href="../index.php">Articles</a></li>
                    <li><a class="navitem" id="nav-about" href="../../about/index.php">About NN/G</a></li>
                </ul>
            </nav>
            <nav class="secondary-nav small-12 medium-12 large-12 columns">
                
                
            </nav>
        </div>
    </header>
    <div class="l-content"><span id="preventClose" style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776; Menu</span>
    
  

  

  <div class="row">
    
      
      <section class="small-12 medium-3 columns collapsable-content l-navrail articles-left-column">
        <h1 class="show-for-small-down collapse-link mobile-browse-header">Browse by Topic and Author</h1>
          <div class="row collapsable">

            
            <div class="small-6 medium-12 columns">
              <section id="article-popular-topics" style="display: none;">
                
                <h1>Topics</h1>
                <ul id="article-popular-topics">
                    
                    <li>
                        
                        <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/intranets/all/index.php">Intranets</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/user-testing/all/index.php">User Testing</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                        
                    </li>
                    
                    <li><a href="#" id="article-see-all-topics"><strong>See all topics&hellip;</strong></a></li>
                </ul>
                
              </section>
              <section id="article-all-topics" style="display: none;">
                
                <h1>
                  <span class="show-for-medium-up">All Article Topics</span>
                  <span class="show-for-small-down">All Topics</span>
                  (<a href="#" id="article-hide-all-topics">hide</a>)
                </h1>
                <ul>
                  
                  <li>
                    
                    <a href="../../topic/accessibility/all/index.php">Accessibility</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/agile/all/index.php">Agile</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/analytics-and-metrics/all/index.php">Analytics &amp; Metrics</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/applications/all/index.php">Application Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/b2b-websites/all/index.php">B2B Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/behavior-patterns/all/index.php">Behavior Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/branding/all/index.php">Branding</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/content-strategy/all/index.php">Content Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/corporate-websites/all/index.php">Corporate Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/design-patterns/all/index.php">Design Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-design-process/all/index.php">Design Process</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/email/all/index.php">Email</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/eyetracking/all/index.php">Eyetracking</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/heuristic-evaluation/all/index.php">Heuristic Evaluation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/human-computer-interaction/all/index.php">Human Computer Interaction</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-ideation/all/index.php">Ideation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/information-architecture/all/index.php">Information Architecture</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/interaction-design/all/index.php">Interaction Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/international-users/all/index.php">International Users</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/intranets/all/index.php">Intranets</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-management/all/index.php">Management</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/navigation/all/index.php">Navigation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/non-profit-websites/all/index.php">Non-Profit Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/personas/all/index.php">Personas</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/persuasive-design/all/index.php">Persuasive Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/prototyping/all/index.php">Prototyping</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/psychology-and-ux/all/index.php">Psychology and UX</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/research-methods/all/index.php">Research Methods</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/search/all/index.php">Search</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/social-media/all/index.php">Social Media</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/strategy/all/index.php">Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/user-testing/all/index.php">User Testing</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-teams/all/index.php">UX Teams</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/visual-design/all/index.php">Visual Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/writing-web/all/index.php">Writing for the Web</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/young-users/all/index.php">Young Users</a>
                    
                  </li>
                  
                </ul>
                
              </section>
            </div>
            <script>
              /* Determine which topics list will be displayed: the full or the
               * partial.  We are inlining this close to the list itself to
               * prevent rendering delays on the client
               */
              var topic_list = $.cookie("article-topics");

              var popular_topics = $('#article-popular-topics');
              var all_topics = $('#article-all-topics');

              if(topic_list == 1) {
                  popular_topics.hide();
                  all_topics.show();
              } else {
                  popular_topics.show();
                  all_topics.hide();
              }
            </script>
            

            
            <div class="small-6 medium-12 columns">
              
              <section id="article-initial-authors">
                  <h1>Author</h1>
                  <ul>
                    
                    <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                    
                    <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                    
                    <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                    
                    <li><a href="#" onClick="$('#article-initial-authors').hide(); $('#article-all-authors').show();"><strong>See all authors&hellip;</strong></a></li>
                  </ul>
              </section>
              <section id="article-all-authors" style="display: none;">
                <h1>
                  All Authors
                  (<a href="#" id="article-hide-all-authors">hide</a>)
                </h1>
                <ul>
                  
                  <li><a href="../author/raluca-budiu/index.php">Raluca Budiu</a></li>
                  
                  <li><a href="../author/patty-caya/index.php">Patty Caya</a></li>
                  
                  <li><a href="../author/susan-farrell/index.php">Susan Farrell</a></li>
                  
                  <li><a href="../author/therese-fessenden/index.php">Therese Fessenden</a></li>
                  
                  <li><a href="../author/kim-flaherty/index.php">Kim Flaherty</a></li>
                  
                  <li><a href="../author/sarah-gibbons/index.php">Sarah Gibbons</a></li>
                  
                  <li><a href="../author/aurora-harley/index.php">Aurora Harley</a></li>
                  
                  <li><a href="../author/kate-kaplan/index.php">Kate Kaplan</a></li>
                  
                  <li><a href="../author/page-laubheimer/index.php">Page Laubheimer</a></li>
                  
                  <li><a href="../author/angela-li/index.php">Angie Li</a></li>
                  
                  <li><a href="../author/hoa-loranger/index.php">Hoa Loranger</a></li>
                  
                  <li><a href="../author/kate-meyer/index.php">Kate Meyer</a></li>
                  
                  <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                  
                  <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                  
                  <li><a href="../author/kara-pernice/index.php">Kara Pernice</a></li>
                  
                  <li><a href="../author/christian-rohrer/index.php">Christian Rohrer</a></li>
                  
                  <li><a href="../author/amy-schade/index.php">Amy Schade</a></li>
                  
                  <li><a href="../author/katie-sherwin/index.php">Katie Sherwin</a></li>
                  
                  <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                  
                  <li><a href="../author/kathryn-whitenton/index.php">Kathryn Whitenton</a></li>
                  
                </ul>
              </section>
              
            </div>
            
          </div>

          
          <section class="hide-for-small-down">
            
              
              <h1>Recent Articles</h1>
              
              <ul>
                  
                  <li><a href="../seamless-cross-channel/index.php">Seamlessness in the Omnichannel User Experience</a></li>
                  
                  <li><a href="../brand-intention-interpretation/index.php">Experience Design: Bridging Brand Intention and Brand Interpretation</a></li>
                  
                  <li><a href="../eyetracking-tasks-efficient-scanning/index.php">Scanning Patterns on the Web Are Optimized for the Current Task</a></li>
                  
                  <li><a href="../flat-design-best-practices/index.php">Flat-Design Best Practices</a></li>
                  
                  <li><a href="../ux-user-stories/index.php">Accounting for UX Work with User Stories in Agile Projects</a></li>
                  
                  <li><a href="../index.php"><strong>See all articles&hellip;</strong></a></li>
              </ul>
              
            
          </section>
          

          
          
          <section class="hide-for-small-down">
            <h1>Popular Articles</h1>
            
            <ul>
                
                <li><a href="../usability-101-introduction-to-usability/index.php">Usability 101: Introduction to Usability</a></li>
                
                <li><a href="../top-10-mistakes-web-design/index.php">Top 10 Mistakes in Web Design</a></li>
                
                <li><a href="../how-users-read-on-the-web/index.php">How Users Read on the Web</a></li>
                
                <li><a href="../f-shaped-pattern-reading-web-content/index.php">F-Shaped Pattern For Reading Web Content</a></li>
                
                <li><a href="../ten-usability-heuristics/index.php">10 Usability Heuristics for User Interface Design</a></li>
                
                <li><a href="../intranet-design-2016/index.php">10 Best Intranets of 2016</a></li>
                
                <li><a href="../which-ux-research-methods/index.php">When to Use Which User-Experience Research Methods</a></li>
                
                <li><a href="../response-times-3-important-limits/index.php">Response Times: The 3 Important Limits</a></li>
                
                <li><a href="../why-you-only-need-to-test-with-5-users/index.php">Why You Only Need to Test with 5 Users</a></li>
                
                <li><a href="../page-fold-manifesto/index.php">The Fold Manifesto: Why the Page Fold Still Matters</a></li>
                
            </ul>
          </section>
          
          

          
            <section class="hide-for-small-down">
              
            </section>
          

      </section>
      

      
        <div class="small-12 medium-9 columns content-body">
          




<article class="l-subsection alertbox-article-content article-detail-bl">
  
  <header>
    <div class="l-subsection">
      <h1>Flexible Usability Testing: 10 Tips to Make your Sessions Adapt to Your Clients’ Needs</h1>
      <div class="author-meta">by
        
          
            
            <a href="../author/jen-cardello/index.php">Jennifer Cardello</a>
            
          
        on  August 31, 2013
        
      </div>

      

      
      <div class="topic-links">
        Topics:
        
          <ul>

  <li><a href="../../topic/research-methods/index.php">Research Methods</a></li>

  <li><a href="../../topic/user-testing/index.php">User Testing</a></li>

</ul>
        
      </div>
      
    </div>
  </header>
  

  
  
  <section>
    <p class="lede"><strong>Summary:</strong> For testing assignments where client teams are ready, willing and able to take immediate action, being flexible with tasks within and between participants can offer better bang for your buck. </p>
    <br />
  </section>
  
  

  
  <section id="article-body"><p>
	Many times, when a client wants to do a usability testing evaluation of a currently in-service environment/application, they are looking for a traditional summative test: A smallish set of rigid tasks geared toward measuring key performance indicators (success rate, task completion time, and errors) with the goals of 1) discovering issues and 2) providing evidence to support investment in fixing those issues.</p>
<p>
	However, as more organizations embrace <a href="../agile-user-experience-projects/index.php">agile and lean methods</a>, decision-making has become decentralized and traditional metric-based evidence is not as crucial for some. Organizations still want to hire usability experts to plan and conduct usability testing (until it becomes an internal competence), but they may not be as interested in generating “evidence” to secure budget for changes – instead, they want actionable findings and recommendations covering the broadest scope of their environment/application. To meet this need, we’ve found there is tremendous value in being more flexible by:</p>
<ul>
	<li>
		<b>Iterating tasks</b> between and within sessions</li>
	<li>
		Improvising and <b>customizing tasks</b> to better suit individual participants</li>
	<li>
		Inviting <b>real-time client participation</b></li>
</ul>
<p>
	(Note that by "clients" we simply mean the people who have to take action on the usability finding. Your clients can be a traditional external consulting client if you're a consultant or work in an agency, or they can be internal stakeholders if you work inside the organization.)</p>
<p>
	For flexible testing you still create a test guide, but the tasks within are not treated as unchanging components. Instead, the tasks are treated as a starting point.</p>
<p>
	There are two benefits to this:</p>
<ul>
	<li>
		Decreased pressure on the moderator/planner to create the perfect tasks right out of the gate: Tasks that we agree will be tested and changed if necessary are a lot easier to create.</li>
	<li>
		Decreased pressure on the client: Often times, members of the client team have never observed a real usability test, so to ask them to come up with or approve a definitive list of tasks and expecting it to be thorough is silly; they simply don’t know what they don’t know. Once they start observing testing, the task ideas start flowing. </li>
</ul>
<p>
	<b>Sample Test Guide Contents:</b></p>
<ul>
	<li>
		Study logistics (times, locations, number of participants, session duration, incentives, moderator)</li>
	<li>
		Target participant criteria</li>
	<li>
		Study goals</li>
	<li>
		Study questions</li>
	<li>
		Tasks</li>
</ul>
<h2>
	Top 10 tips for Flexible testing</h2>
<p>
	To get the most out of flexible testing, we have provided some advice focused on the most critical aspects of planning, facilitating and management.</p>
<h2>
	<b>1. Define testing goals and intentions first</b></h2>
<p>
	You need to determine two things:</p>
<ul>
	<li>
		Why the client is sponsoring the study.</li>
	<li>
		What they intend on doing with the results.</li>
</ul>
<p>
	It is imperative that the client answers both these questions honestly and that they are engaged in a conversation about how these goals and intentions will inform the research method selection and output. Of all your discussions, this is the one that will bear the most significant impact on your ability to meet expectations.</p>
<table border="1" cellpadding="0" cellspacing="0" class="MsoTableLightListAccent1" style="border-collapse:collapse;border:none">
	<tbody>
		<tr>
			<td colspan="3" style="width:476.6pt;border:solid #BFBFBF 1.0pt; background:#17365D;padding:0in 5.4pt 0in 5.4pt" valign="top" width="635">
				<p align="center" class="MsoNormal" style="margin:0in;margin-bottom:.0001pt; text-align:center">
					<b><span style="color:white">Matching Client Goals/Intentions with Testing Methods</span></b></p>
			</td>
		</tr>
		<tr>
			<td style="width:3.2in;border:solid #BFBFBF 1.0pt; border-top:none;padding:0in 5.4pt 0in 5.4pt" valign="top" width="307">
				<p class="MsoNormal" style="margin:0in;margin-bottom:.0001pt">
					 </p>
			</td>
			<td style="width:124.7pt;border-top:none;border-left: none;border-bottom:solid #BFBFBF 1.0pt;border-right:solid #BFBFBF 1.0pt; background:#95B3D7;padding:0in 5.4pt 0in 5.4pt" valign="top" width="166">
				<p align="center" class="MsoNormal" style="margin:0in;margin-bottom:.0001pt; text-align:center">
					<b>Summative Testing</b></p>
			</td>
			<td style="width:121.5pt;border-top:none;border-left: none;border-bottom:solid #BFBFBF 1.0pt;border-right:solid #BFBFBF 1.0pt; background:#95B3D7;padding:0in 5.4pt 0in 5.4pt" valign="top" width="162">
				<p align="center" class="MsoNormal" style="margin:0in;margin-bottom:.0001pt; text-align:center">
					<b>Flexible Testing</b></p>
			</td>
		</tr>
		<tr>
			<td style="width:3.2in;border:solid #BFBFBF 1.0pt;border-top:none; padding:0in 5.4pt 0in 5.4pt" width="307">
				<p class="MsoNormal" style="margin:0in;margin-bottom:.0001pt">
					Generate statistical evidence</p>
			</td>
			<td style="width:124.7pt;border-top:none;border-left:none; border-bottom:solid #BFBFBF 1.0pt;border-right:solid #BFBFBF 1.0pt; padding:0in 5.4pt 0in 5.4pt" width="166">
				<p align="center" class="MsoNormal" style="margin:0in;margin-bottom:.0001pt; text-align:center">
					<b>x</b></p>
			</td>
			<td style="width:121.5pt;border-top:none;border-left:none; border-bottom:solid #BFBFBF 1.0pt;border-right:solid #BFBFBF 1.0pt; padding:0in 5.4pt 0in 5.4pt" width="162">
				<p align="center" class="MsoNormal" style="margin:0in;margin-bottom:.0001pt; text-align:center">
					 </p>
			</td>
		</tr>
		<tr>
			<td style="width:3.2in;border:solid #BFBFBF 1.0pt;border-top:none; padding:0in 5.4pt 0in 5.4pt" width="307">
				<p class="MsoNormal" style="margin:0in;margin-bottom:.0001pt">
					Convince executives to make changes/allocate budget</p>
			</td>
			<td style="width:124.7pt;border-top:none;border-left:none; border-bottom:solid #BFBFBF 1.0pt;border-right:solid #BFBFBF 1.0pt; padding:0in 5.4pt 0in 5.4pt" width="166">
				<p align="center" class="MsoNormal" style="margin:0in;margin-bottom:.0001pt; text-align:center">
					<b>x</b></p>
			</td>
			<td style="width:121.5pt;border-top:none;border-left:none; border-bottom:solid #BFBFBF 1.0pt;border-right:solid #BFBFBF 1.0pt; padding:0in 5.4pt 0in 5.4pt" width="162">
				<p align="center" class="MsoNormal" style="margin:0in;margin-bottom:.0001pt; text-align:center">
					<b>x</b></p>
			</td>
		</tr>
		<tr>
			<td style="width:3.2in;border:solid #BFBFBF 1.0pt;border-top:none; padding:0in 5.4pt 0in 5.4pt" width="307">
				<p class="MsoNormal" style="margin:0in;margin-bottom:.0001pt">
					Inform design strategy</p>
			</td>
			<td style="width:124.7pt;border-top:none;border-left:none; border-bottom:solid #BFBFBF 1.0pt;border-right:solid #BFBFBF 1.0pt; padding:0in 5.4pt 0in 5.4pt" width="166">
				<p align="center" class="MsoNormal" style="margin:0in;margin-bottom:.0001pt; text-align:center">
					 </p>
			</td>
			<td style="width:121.5pt;border-top:none;border-left:none; border-bottom:solid #BFBFBF 1.0pt;border-right:solid #BFBFBF 1.0pt; padding:0in 5.4pt 0in 5.4pt" width="162">
				<p align="center" class="MsoNormal" style="margin:0in;margin-bottom:.0001pt; text-align:center">
					<b>x</b></p>
			</td>
		</tr>
		<tr>
			<td style="width:3.2in;border:solid #BFBFBF 1.0pt;border-top:none; padding:0in 5.4pt 0in 5.4pt" width="307">
				<p class="MsoNormal" style="margin:0in;margin-bottom:.0001pt">
					Guide design specifications</p>
			</td>
			<td style="width:124.7pt;border-top:none;border-left:none; border-bottom:solid #BFBFBF 1.0pt;border-right:solid #BFBFBF 1.0pt; padding:0in 5.4pt 0in 5.4pt" width="166">
				<p align="center" class="MsoNormal" style="margin:0in;margin-bottom:.0001pt; text-align:center">
					 </p>
			</td>
			<td style="width:121.5pt;border-top:none;border-left:none; border-bottom:solid #BFBFBF 1.0pt;border-right:solid #BFBFBF 1.0pt; padding:0in 5.4pt 0in 5.4pt" width="162">
				<p align="center" class="MsoNormal" style="margin:0in;margin-bottom:.0001pt; text-align:center">
					<b>x</b></p>
			</td>
		</tr>
		<tr>
			<td style="width:3.2in;border:solid #BFBFBF 1.0pt;border-top:none; padding:0in 5.4pt 0in 5.4pt" width="307">
				<p class="MsoNormal" style="margin:0in;margin-bottom:.0001pt">
					Inform design changes</p>
			</td>
			<td style="width:124.7pt;border-top:none;border-left:none; border-bottom:solid #BFBFBF 1.0pt;border-right:solid #BFBFBF 1.0pt; padding:0in 5.4pt 0in 5.4pt" width="166">
				<p align="center" class="MsoNormal" style="margin:0in;margin-bottom:.0001pt; text-align:center">
					 </p>
			</td>
			<td style="width:121.5pt;border-top:none;border-left:none; border-bottom:solid #BFBFBF 1.0pt;border-right:solid #BFBFBF 1.0pt; padding:0in 5.4pt 0in 5.4pt" width="162">
				<p align="center" class="MsoNormal" style="margin:0in;margin-bottom:.0001pt; text-align:center">
					<b>x</b></p>
			</td>
		</tr>
		<tr>
			<td style="width:3.2in;border:solid #BFBFBF 1.0pt;border-top:none; padding:0in 5.4pt 0in 5.4pt" width="307">
				<p class="MsoNormal" style="margin:0in;margin-bottom:.0001pt">
					Identify/prioritize issues with existing design and content</p>
			</td>
			<td style="width:124.7pt;border-top:none;border-left:none; border-bottom:solid #BFBFBF 1.0pt;border-right:solid #BFBFBF 1.0pt; padding:0in 5.4pt 0in 5.4pt" width="166">
				<p align="center" class="MsoNormal" style="margin:0in;margin-bottom:.0001pt; text-align:center">
					<b>x</b></p>
			</td>
			<td style="width:121.5pt;border-top:none;border-left:none; border-bottom:solid #BFBFBF 1.0pt;border-right:solid #BFBFBF 1.0pt; padding:0in 5.4pt 0in 5.4pt" width="162">
				<p align="center" class="MsoNormal" style="margin:0in;margin-bottom:.0001pt; text-align:center">
					<b>x</b></p>
			</td>
		</tr>
		<tr>
			<td style="width:3.2in;border:solid #BFBFBF 1.0pt;border-top:none; padding:0in 5.4pt 0in 5.4pt" width="307">
				<p class="MsoNormal" style="margin:0in;margin-bottom:.0001pt">
					Identify good characteristics of existing site</p>
			</td>
			<td style="width:124.7pt;border-top:none;border-left:none; border-bottom:solid #BFBFBF 1.0pt;border-right:solid #BFBFBF 1.0pt; padding:0in 5.4pt 0in 5.4pt" width="166">
				<p align="center" class="MsoNormal" style="margin:0in;margin-bottom:.0001pt; text-align:center">
					<b>x</b></p>
			</td>
			<td style="width:121.5pt;border-top:none;border-left:none; border-bottom:solid #BFBFBF 1.0pt;border-right:solid #BFBFBF 1.0pt; padding:0in 5.4pt 0in 5.4pt" width="162">
				<p align="center" class="MsoNormal" style="margin:0in;margin-bottom:.0001pt; text-align:center">
					<b>x</b></p>
			</td>
		</tr>
		<tr>
			<td style="width:3.2in;border:solid #BFBFBF 1.0pt;border-top:none; padding:0in 5.4pt 0in 5.4pt" width="307">
				<p class="MsoNormal" style="margin:0in;margin-bottom:.0001pt">
					Acquire a “safe feeling” of signing off on an exact, unchanging test plan before the research starts</p>
			</td>
			<td style="width:124.7pt;border-top:none;border-left:none; border-bottom:solid #BFBFBF 1.0pt;border-right:solid #BFBFBF 1.0pt; padding:0in 5.4pt 0in 5.4pt" width="166">
				<p align="center" class="MsoNormal" style="margin:0in;margin-bottom:.0001pt; text-align:center">
					<b>x</b></p>
			</td>
			<td style="width:121.5pt;border-top:none;border-left:none; border-bottom:solid #BFBFBF 1.0pt;border-right:solid #BFBFBF 1.0pt; padding:0in 5.4pt 0in 5.4pt" width="162">
				<p align="center" class="MsoNormal" style="margin:0in;margin-bottom:.0001pt; text-align:center">
					 </p>
			</td>
		</tr>
	</tbody>
</table>
<h2>
	<b>2. Generate very specific questions the study must answer</b></h2>
<p>
	The easiest way to come up with tasks to test is to start with the questions the study should be answering. I used to focus on asking clients to tell me their “mission critical tasks”, but that can limit your ability to come up with tasks that actually touch and test attributes that clients believe are problematic.<br/>
	After goals and intentions are defined, ask the client to give you a list of questions they want answered in this study. You can use some of these sample questions to get the ideas flowing:</p>
<table border="1" cellpadding="0" cellspacing="0" class="MsoTableGrid" style="border-collapse:collapse;border:none">
	<tbody>
		<tr>
			<td style="width:6.65in;border:solid #BFBFBF 1.0pt; background:#17365D;padding:0in 5.4pt 0in 5.4pt" valign="top" width="638">
				<p align="center" class="MsoNormal" style="margin:0in;margin-bottom:.0001pt; text-align:center">
					<b><span style="color:white">Potential Questions to be answered in Usability Testing </span></b></p>
				<p align="center" class="MsoNormal" style="margin:0in;margin-bottom:.0001pt; text-align:center">
					<b><span style="color:white">(Web site/Application)</span></b></p>
			</td>
		</tr>
		<tr>
			<td style="width:6.65in;border:solid #BFBFBF 1.0pt; border-top:none;background:#95B3D7;padding:0in 5.4pt 0in 5.4pt" valign="top" width="638">
				<p class="MsoNormal" style="margin:0in;margin-bottom:.0001pt">
					<b>Navigation</b></p>
			</td>
		</tr>
		<tr>
			<td style="width:6.65in;border:solid #BFBFBF 1.0pt; border-top:none;padding:0in 5.4pt 0in 5.4pt" valign="top" width="638">
				<p class="MsoNormal" style="margin:0in;margin-bottom:.0001pt">
					Do people recognize the global navigation as navigation?</p>
			</td>
		</tr>
		<tr>
			<td style="width:6.65in;border:solid #BFBFBF 1.0pt; border-top:none;padding:0in 5.4pt 0in 5.4pt" valign="top" width="638">
				<p class="MsoNormal" style="margin:0in;margin-bottom:.0001pt">
					Do people use local navigation in [location]?</p>
			</td>
		</tr>
		<tr>
			<td style="width:6.65in;border:solid #BFBFBF 1.0pt; border-top:none;padding:0in 5.4pt 0in 5.4pt" valign="top" width="638">
				<p class="MsoNormal" style="margin:0in;margin-bottom:.0001pt">
					Do people use the navigation system to understand where they are?</p>
			</td>
		</tr>
		<tr>
			<td style="width:6.65in;border:solid #BFBFBF 1.0pt; border-top:none;background:#95B3D7;padding:0in 5.4pt 0in 5.4pt" valign="top" width="638">
				<p class="MsoNormal" style="margin:0in;margin-bottom:.0001pt">
					<b>Product/Content Findability</b></p>
			</td>
		</tr>
		<tr>
			<td style="width:6.65in;border:solid #BFBFBF 1.0pt; border-top:none;padding:0in 5.4pt 0in 5.4pt" valign="top" width="638">
				<p class="MsoNormal" style="margin:0in;margin-bottom:.0001pt">
					Does site search yield good relevant results?</p>
			</td>
		</tr>
		<tr>
			<td style="width:6.65in;border:solid #BFBFBF 1.0pt; border-top:none;padding:0in 5.4pt 0in 5.4pt" valign="top" width="638">
				<p class="MsoNormal" style="margin:0in;margin-bottom:.0001pt">
					Do overview pages effectively route users down a path?</p>
			</td>
		</tr>
		<tr>
			<td style="width:6.65in;border:solid #BFBFBF 1.0pt; border-top:none;padding:0in 5.4pt 0in 5.4pt" valign="top" width="638">
				<p class="MsoNormal" style="margin:0in;margin-bottom:.0001pt">
					Do people use filters? Under what circumstances?</p>
			</td>
		</tr>
		<tr>
			<td style="width:6.65in;border:solid #BFBFBF 1.0pt; border-top:none;background:#95B3D7;padding:0in 5.4pt 0in 5.4pt" valign="top" width="638">
				<p class="MsoNormal" style="margin:0in;margin-bottom:.0001pt">
					<b>Product information</b></p>
			</td>
		</tr>
		<tr>
			<td style="width:6.65in;border:solid #BFBFBF 1.0pt; border-top:none;padding:0in 5.4pt 0in 5.4pt" valign="top" width="638">
				<p class="MsoNormal" style="margin:0in;margin-bottom:.0001pt">
					What information do people fixate on?</p>
			</td>
		</tr>
		<tr>
			<td style="width:6.65in;border:solid #BFBFBF 1.0pt; border-top:none;padding:0in 5.4pt 0in 5.4pt" valign="top" width="638">
				<p class="MsoNormal" style="margin:0in;margin-bottom:.0001pt">
					What information do they have trouble finding?</p>
			</td>
		</tr>
		<tr>
			<td style="width:6.65in;border:solid #BFBFBF 1.0pt; border-top:none;padding:0in 5.4pt 0in 5.4pt" valign="top" width="638">
				<p class="MsoNormal" style="margin:0in;margin-bottom:.0001pt">
					What information do they want to see?</p>
			</td>
		</tr>
		<tr>
			<td style="width:6.65in;border:solid #BFBFBF 1.0pt; border-top:none;padding:0in 5.4pt 0in 5.4pt" valign="top" width="638">
				<p class="MsoNormal" style="margin:0in;margin-bottom:.0001pt">
					What is information is confusing?</p>
			</td>
		</tr>
		<tr>
			<td style="width:6.65in;border:solid #BFBFBF 1.0pt; border-top:none;padding:0in 5.4pt 0in 5.4pt" valign="top" width="638">
				<p class="MsoNormal" style="margin:0in;margin-bottom:.0001pt">
					Do users use related links?</p>
			</td>
		</tr>
		<tr>
			<td style="width:6.65in;border:solid #BFBFBF 1.0pt; border-top:none;padding:0in 5.4pt 0in 5.4pt" valign="top" width="638">
				<p class="MsoNormal" style="margin:0in;margin-bottom:.0001pt">
					Do users seek reviews?</p>
			</td>
		</tr>
		<tr>
			<td style="width:6.65in;border:solid #BFBFBF 1.0pt; border-top:none;background:#95B3D7;padding:0in 5.4pt 0in 5.4pt" valign="top" width="638">
				<p class="MsoNormal" style="margin:0in;margin-bottom:.0001pt">
					<b>Checkout</b></p>
			</td>
		</tr>
		<tr>
			<td style="width:6.65in;border:solid #BFBFBF 1.0pt; border-top:none;padding:0in 5.4pt 0in 5.4pt" valign="top" width="638">
				<p class="MsoNormal" style="margin:0in;margin-bottom:.0001pt">
					Can people easily find a way to checkout?</p>
			</td>
		</tr>
		<tr>
			<td style="width:6.65in;border:solid #BFBFBF 1.0pt; border-top:none;padding:0in 5.4pt 0in 5.4pt" valign="top" width="638">
				<p class="MsoNormal" style="margin:0in;margin-bottom:.0001pt">
					Are they nervous about any required/requested information?</p>
			</td>
		</tr>
		<tr>
			<td style="width:6.65in;border:solid #BFBFBF 1.0pt; border-top:none;padding:0in 5.4pt 0in 5.4pt" valign="top" width="638">
				<p class="MsoNormal" style="margin:0in;margin-bottom:.0001pt">
					Are there any issues filling out forms?</p>
			</td>
		</tr>
	</tbody>
</table>
<h2>
	<b>3. Design tasks to answer questions</b></h2>
<p>
	The best way to ensure clients get what they need is to create testing tasks that expose the qualities of the site they are interested in evaluating. This doesn’t mean that you have to create a task for each question. It’s likely that typical user task scenarios will address several questions each and may overlap as well – and that’s OK.</p>
<h2>
	<b>4. Get as many client observers as possible</b></h2>
<p>
	Testing is always better when <a href="../stakeholders-and-user-testing/index.php">observed by as many stakeholders, creators, and controllers</a> as possible. But this is even truer when the tasks are intended to change. More brains lead to more ideas, which lead to more movement.</p>
<h2>
	<b>5. Incorporate competitive offerings into the test plan</b></h2>
<p>
	If possible, you want to include some tasks to be performed on competitive sites so there is context. Also, this tends to be one of the most controversial topics in any organization along the lines of “So-and-so does it this way and it’s better.” The first benefit of observing competitors in any usability study is that it can prove/disprove those statements. The second benefit, specific to flexible testing, is that observing people using environments beyond your own can generate all sorts of additional task ideas.</p>
<h2>
	6. Use pre-task questions to inform task improvisation</h2>
<p>
	Define a series of questions to ask at the beginning of each session to better understand your participants.<br/>
	Questions can focus on:</p>
<ul>
	<li>
		<b>Needs:</b> What are they seeking?</li>
	<li>
		<b>Experience:</b> How do they use particular information/content/products?</li>
	<li>
		<b>Intentions:</b> Why do they use particular information/content/products?</li>
	<li>
		<b>Knowledge:</b> What do they know about this particular topic?</li>
</ul>
<h2>
	7. Physically separate the client team from moderator/participant</h2>
<p>
	You want your team to be reacting and discussing together, so they can suggest task changes and formulate questions for the moderator between sessions. If they are in the same room as the participant, team interaction is limited to facial expressions and maybe passing notes (which, by the way, is totally noticeable to the participant and can be unnerving). It’s best to keep your client team in a separate room from the facilitation. This applies to traditional “lab” sessions as well as remote-user sessions where, ideally, the facilitator should be alone in the room.</p>
<h2>
	8. Define ground rules for real-time participation so clients know how to help instead of hurt</h2>
<p>
	Inform your client team of what is happening and how they can optimize the process for their benefit:</p>
<ul>
	<li>
		Tell them this testing is flexible –<b> it’s intended to change</b>.</li>
	<li>
		Tell them <b>why</b>: Because as you watch the testing, you may get additional task ideas or become curious about other features or content that may be suitable to a particular participant (you know your site best!).</li>
	<li>
		Do not allow task suggestions/changes in the first session – ask the team to watch it without real-time feedback.</li>
	<li>
		Reserve a generous amount of time (at least one hour) to debrief after the first session: Collect feedback and make test plan changes.</li>
	<li>
		Have the team <b>elect one representative</b> who will be responsible for sending you real-time task suggestions starting with the second session.</li>
	<li>
		Tell them <b>what is useful and what is not</b>:
		<ul>
			<li>
				Do not send requests to ask for user opinion (“Do you like this logo?”).</li>
			<li>
				Do send ideas about products you have that this user may find useful (“This user said she likes to golf, we sell a line of golf shoes – maybe we can see if she realizes that and then see if she’s able to find a pair that meets her needs?”).</li>
			<li>
				Do send ideas about features/content that might suit a user based on their pre-test answers or behavior (“This user seems really fixated on peer reviews; any way to see how they react to our Q&amp;A content?”).</li>
		</ul>
	</li>
</ul>
<h2>
	9. Make sure users are minimally disturbed by the conversation between moderator and client</h2>
<p>
	If you are using conference calling technology to broadcast and/or record your sessions (e.g., GoTo Meeting, Webex), do not also use that channel for the client team to talk with you. Cell phone texting (on mute) is less disruptive, so the user does not lose their train of thought or see information intended only for the research and/or client team.</p>
<h2>
	10. Debrief and prep between each session</h2>
<p>
	Between each session, reserve enough time to chat with the client team, get feedback on the facilitation and information derived, and make changes to the tasks.</p>
<ul>
	<li>
		Review each task and ask for edits.</li>
	<li>
		Ask if they want to retire any tasks: This can happen when a team has seen enough  people failing on the same task. If they saw it two or three times, they’d rather use that time to expand the scope.</li>
	<li>
		Quickly outline the tasks for the next participant.</li>
	<li>
		Make your task sheets (if facilitating in person): If you have access to a printer, then type these up, but it’s fine to use pen and paper when pressed for time.</li>
</ul>
<h2>
	Getting the Most from Flexible Testing</h2>
<p>
	Using a flexible testing method can help you observe a greater range of site features because tasks are allowed to change as client questions are answered and as tasks are adjusted or created to better suit individual participants. This method works well for organizations who are less concerned with generating metrics from usability testing and more interested in immediate action items.</p>
<p>
	Ideally, use experienced usability facilitators that can accommodate this constantly changing game plan and welcome the challenge. Experience helps, because there's no time to pilot-test the changes, so you need to get them right the first time. However, inexperienced staff can use a modified version of flexible usability testing where they treat the entire study as an extended exercise in iterative pilot testing and gradually make their tasks and test methodology more and more appropriate. Not as good, of course, but then the only way anybody <a href="../becoming-a-usability-professional/index.php">gets to be an experienced usability expert</a> is to start off with no experience and gradually get better. Flexible testing allows you to get better faster.</p>
</section>
  

  
  <section>
    
        
    
  </section>
  
  
  
  
  

  


  <section>
    <br />
    <p class="share"><b>Share <span class="hide-for-small-down">this article</span>:</b> <a href="https://twitter.com/intent/tweet?url=http://www.nngroup.com/articles/flexible-usability-testing/&amp;text=Flexible%20Usability%20Testing:%2010%20Tips%20to%20Make%20your%20Sessions%20Adapt%20to%20Your%20Clients%e2%80%99%20Needs&amp;via=nngroup">Twitter</a> | <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=http://www.nngroup.com/articles/flexible-usability-testing/&amp;title=Flexible%20Usability%20Testing:%2010%20Tips%20to%20Make%20your%20Sessions%20Adapt%20to%20Your%20Clients%e2%80%99%20Needs&amp;source=Nielsen%20Norman%20Group">LinkedIn</a> | <a href="https://plus.google.com/share?url=http://www.nngroup.com/articles/flexible-usability-testing/">Google+</a> | <a href="mailto:?subject=NN/g Article: Flexible Usability Testing: 10 Tips to Make your Sessions Adapt to Your Clients’ Needs&amp;body=http://www.nngroup.com/articles/flexible-usability-testing/">Email</a></p>
    <hr />
  </section>

  <footer class="l-subsection">
    
    
    <div class="learn-more">
      <h2>Learn More</h2>
      <p><a href="../subscribe/index.php">Subscribe to the weekly newsletter</a> to get notified about future articles.</p>
      </div>
    
    

    <div class="row show-for-small-down related-content">
      
      
      

      
      
        <div class="column small-12 topic-links">All articles about:
          <ul>
            
            <li><a href="../../topic/research-methods/index.php">Research Methods</a></li>
            
            <li><a href="../../topic/user-testing/index.php">User Testing</a></li>
            
          </ul>
        </div>
      
      

      
      
        <div class="column small-12">
          <h3>UX Conference Training Courses</h3>
           <ul class="no-bullet">
    
        <li><a href="../../courses/ux-basic-training/index.php">UX Basic Training</a></li>
    
        <li><a href="../../courses/journey-mapping/index.php">Journey Mapping to Understand Customer Needs</a></li>
    
        <li><a href="../../courses/measuring-ux/index.php">Measuring User Experience</a></li>
    
        <li><a href="../../courses/analytics-and-user-experience/index.php">Analytics and User Experience</a></li>
    
        <li><a href="../../courses/usability-testing/index.php">Usability Testing</a></li>
    
</ul>
        </div>
      
      

      
      
        <div class="small-12 column">
          <h3>Research Reports</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../reports/paper-prototyping-training-video/index.php">Paper Prototyping Training Video</a></li>
              
            
              
                <li><a href="../../reports/user-experience-careers/index.php">User Experience Careers</a></li>
              
            
              
                <li><a href="../../reports/how-to-conduct-eyetracking-studies/index.php">How to Conduct Eyetracking Studies</a></li>
              
            
              
                <li><a href="../../reports/how-to-recruit-participants-usability-studies/index.php">How to Recruit Participants for Usability Studies</a></li>
              
            
              
                <li><a href="../../reports/how-to-conduct-usability-studies-accessibility/index.php">How to Conduct Usability Studies for Accessibility</a></li>
              
            
          </ul>
        </div>
      
      

      
      
        <div class="small-12 column">
          <h3>Online Seminars</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../online-seminars/mobile-user-testing/index.php">Mobile User Testing</a></li>
              
            
          </ul>
        </div>
      
      
    </div>

    <div class="hide-for-small-down row related-content">
      <div class="medium-6 columns">
        
        
          <div>
            <h3>Articles</h3>
            <ul class="no-bullet">
              
                
                <li><a href="../tips-user-research-field/index.php">27 Tips and Tricks for Conducting Successful User Research in the Field</a></li>
                
              
                
                <li><a href="../field-studies/index.php">Field Studies</a></li>
                
              
                
                <li><a href="../qualitative-surveys/index.php">28 Tips for Creating Great Qualitative Surveys</a></li>
                
              
                
                <li><a href="../pilot-testing/index.php">Pilot Testing: Getting It Right (Before) the First Time</a></li>
                
              
                
                <li><a href="../redesign-competitive-testing/index.php">Redesigning Your Website? Don’t Ditch Your Old Design So Soon</a></li>
                
              
            </ul>
          </div>
        
        

        
          
          <div>
            <h3>Research Reports</h3>
            <ul class="no-bullet">
              
                
                  <li><a href="../../reports/paper-prototyping-training-video/index.php">Paper Prototyping Training Video</a></li>
                
              
                
                  <li><a href="../../reports/user-experience-careers/index.php">User Experience Careers</a></li>
                
              
                
                  <li><a href="../../reports/how-to-conduct-eyetracking-studies/index.php">How to Conduct Eyetracking Studies</a></li>
                
              
                
                  <li><a href="../../reports/how-to-recruit-participants-usability-studies/index.php">How to Recruit Participants for Usability Studies</a></li>
                
              
                
                  <li><a href="../../reports/how-to-conduct-usability-studies-accessibility/index.php">How to Conduct Usability Studies for Accessibility</a></li>
                
              
            </ul>
          </div>
          
        

      </div>
      <div class="medium-6 columns">
        
          
          <div>
            <h3>UX Conference Training Courses</h3>
             <ul class="no-bullet">
    
        <li><a href="../../courses/ux-basic-training/index.php">UX Basic Training</a></li>
    
        <li><a href="../../courses/journey-mapping/index.php">Journey Mapping to Understand Customer Needs</a></li>
    
        <li><a href="../../courses/measuring-ux/index.php">Measuring User Experience</a></li>
    
        <li><a href="../../courses/analytics-and-user-experience/index.php">Analytics and User Experience</a></li>
    
        <li><a href="../../courses/usability-testing/index.php">Usability Testing</a></li>
    
</ul>
          </div>
          
        
        
          
          <div>
            <h3>Online Seminars</h3>
            <ul class="no-bullet">
            
                <li><a href="../../online-seminars/mobile-user-testing/index.php">Mobile User Testing</a></li>
            
            </ul>
          </div>
          
        
      </div>
    </div>
  </footer>
</article>

        </div>

      

    
  </div>
  <div class="row">
    
      <div class="column small-12 show-for-small-down l-navrail announcements">
        
          
        
      </div>
    
    
  </div>

    </div>
    




<footer id="page-footer">
    <div class="row hide-for-print">
        <div class="column small-12 medium-6">
            <section class="footer-popular">
                
                <h1>Popular Topics</h1>
                <div style="float: left;  width: 15em; display: block;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/applications/index.php">Application Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/e-commerce/index.php">E-Commerce Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/eyetracking/index.php">Eyetracking </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/information-architecture/index.php">Information Architecture </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/intranets/index.php">Intranet Design </a></li>
	<li><a href="../../topic/interaction-design/index.php">Interaction Design </a></li>
</ul>
</div>

<div style="float: left;  width: 15em; display: block; list-style: none;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/mobile-and-tablet-design/index.php">Mobile and Tablet Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/credibility-persuasion/index.php">Persuasive Design</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/research-methods/index.php">Research Methods</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/user-testing/index.php">User Testing </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/web-usability/index.php">Web Usability </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/writing-web/index.php">Web Writing &amp; Content Strategy </a></li>
</ul>
</div>

<p>&nbsp;</p>

                
            </section>
        </div>

        <div class="column small-12 medium-6">
            <div class="row">
                <div class="column small-6">
                    <section class="footer-about">
                        <h1>Follow Us</h1>
                        <ul>
	<li><a href="../subscribe/index.php">Subscribe to our weekly UX newsletter</a>,&nbsp;Jakob Nielsen&#39;s Alertbox</li>
	<li>Visit Don Norman&#39;s website, <a href="http://www.jnd.org/"> www.jnd.org </a></li>
	<li>Visit Bruce Tognazzini&#39;s website, <a href="http://www.asktog.com/"> www.asktog.com </a></li>
	<li style="padding-top: 6px;"><a class="twitter-follow-button" data-show-count="false" data-size="large" href="https://twitter.com/nngroup"><img alt="Follow @nngroup on Twitter" height="20" src="https://media.nngroup.com/media/editor/2016/12/30/twitter-follow-button.png" style="width: 117px; height: 20px;" width="117" /></a></li>
	<li style="padding-top: 6px;"><img alt="" height="14" src="https://media.nngroup.com/media/editor/2016/07/26/feed-icon-14x14.png" style="vertical-align:text-top;width:14px;margin-right:.25em;" width="14" /><a href="../../feed/rss/index.php">Subscribe to our RSS feed</a></li>
</ul>

                    </section>
                </div>

                <div class="column small-6">
                    <section class="footer-about right-column">
                        <h1>About
</h1>
                        <ul class="no-bullet">
	<li><a href="../../about/contact/index.php">Contact information</a></li>
	<li><a href="../../about/index.php">About Us</a></li>
	<li><a href="../../about/why-nng/index.php">Why NN/g? </a></li>
	<li><a href="../../return-policy/index.php">Return Policy</a></li>
</ul>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="copyright column small-12">
            <p><a href="../../copyright-and-reprint-info/index.php">Copyright</a> &copy; 1998-2017 Nielsen Norman Group, All Rights Reserved.</p>
        </div>
    </div>
</footer>


    


        <script src="https://media.nngroup.com/static/vendor/fastclick/js/fastclick.min.js"></script>
        <script src="https://media.nngroup.com/static/vendor/foundation/js/foundation.min.js"></script>
        <script src="http://media.nngroup.com/static/js/app.js"></script>
        <script src="http://media.nngroup.com/static/js/scroll-to-fixed.js"></script><script>var externalLinks = document.querySelectorAll('a[href^="http"]');for (var i = externalLinks.length-1; i >= 0; i--) {externalLinks[i].addEventListener("click", function(event) { alert("External links are disabled for this test."); event.preventDefault(); }, false);}var mailLinks = document.querySelectorAll('a[href^="mailto"]');for (var i = mailLinks.length-1; i >= 0; i--) {mailLinks[i].addEventListener("click", function(event) { alert("Email links are disabled for this test."); event.preventDefault(); }, false);}var externalForms = document.querySelectorAll('form');for (var i = externalForms.length-1; i >= 0; i--) {externalForms[i].addEventListener("submit", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false); externalForms[i].addEventListener("click", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false);}</script>

        <script>
            
            $(function() {
                $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
                $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
            });
        </script>

        
<script src="https://media.nngroup.com/static/js/articles.js"></script>


        
        <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1072699960;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072699960/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
    </body>

<!-- Mirrored from www.nngroup.com/articles/flexible-usability-testing/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:13:55 GMT -->
</html>
