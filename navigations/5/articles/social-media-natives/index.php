<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->





    
<!-- Mirrored from www.nngroup.com/articles/social-media-natives/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:12:27 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1026.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"licenseKey":"1a30ef9931","errorBeacon":"bam.nr-data.net","applicationID":"2048992","transactionName":"NFxSMRBUX0EDUkIPDA0WdhAMVkVbDV8ZBxEXUFMJB0YfRAtUQRVZIktEDAFZVHYHRVcPD01eVRE=","beacon":"bam.nr-data.net","queueTime":6,"applicationTime":366,"agent":""}</script>
        <title>Social Media Natives: Growing Up with Social Networking</title><meta property="og:title" content="Social Media Natives: Growing Up with Social Networking" />
  
        
        <meta name="description" content="Social media use has altered how Millennials think about friendships and relationships. This impact stands as a valuable reminder of the consequences of UX-design decisions.">
        <meta property="og:description" content="Social media use has altered how Millennials think about friendships and relationships. This impact stands as a valuable reminder of the consequences of UX-design decisions." />
        
  
        
	
        
        <meta name="keywords" content="social media, social networking, digital natives, millennials">
        

        

        <meta name="robots" content="noarchive">
        <meta name="viewport" content="width=device-width">

        <link rel="search" type="application/opensearchdescription+xml" title="NN/g Search" href="../../opensearch.xml" />
        <link rel="shortcut icon" href="http://media.nngroup.com/static/img/favicon.ico" />

        <link rel="stylesheet" href="https://media.nngroup.com/static/vendor/font-awesome/css/font-awesome.min.css">

        
        <link rel="stylesheet" href="https://media.nngroup.com/static/css/7232a960f1df.css" type="text/css" />
        <link rel="stylesheet" href="http://fast.fonts.com/t/1.css?apiType=css&amp;projectid=c5846260-9def-46ec-9407-0f1349bd2d64" type="text/css">

        <script src="https://media.nngroup.com/static/vendor/modernizr/js/modernizr.min.js"></script>

        <script type="text/javascript" src="https://media.nngroup.com/static/js/3283be5def74.js"></script>
        

        

        
        
        <!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=229034,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('http://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->


<!-- Google Analytics Tracking Code for http://nngroup.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23132681-1', 'nngroup.com');
  ga('require', 'linkid', 'https://www.nngroup.com/articles/social-media-natives/linkid.js');
  ga('require', 'displayfeatures');

// Optimizely Universal Analytics Integration
 window.optimizely = window.optimizely || [];
 window.optimizely.push("activateUniversalAnalytics");

  ga('send', 'pageview');

</script>
<script>
/**
* Function to track a click on an outbound link in Google Analytics.
*/
function trackOutboundLink(label, url) {
    if(window.ga && ga.loaded) {
        ga('send', 'event', 'outbound', 'click', label, {'hitCallback': function(){document.location = url;}
        });
    } else {
        document.location = url;
    }
}
/**
* Function to track a click on a link in the article announcement area.
*/
$('#articleAnnouncement a').on('click', function(){
  ga('send', 'event', 'link', 'click', 'articleAnnouncement');
});
</script>

<script type="text/javascript">
 setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2421.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



<!-- Hotjar Tracking Code for http://nngroup.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:121605,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

        <script src="https://media.nngroup.com/static/js/analytics/track_outbound_link.js"></script>
        
        
        
    </head>
    <body class="location-articles articles-detail">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
    <header id="page-header">
        <div id="cart" class="shopping-cart mobile-cart show-for-small-down"></div>
        <div class="row">
            <div id="masthead" class="masthead small-12 medium-7 column">

                <!-- The button to toggles menu on small screen -->

                <h1><a href="../../index.php"><img src="http://media.nngroup.com/static/img/logo-wordmark.png" width="74" height="30"> Nielsen <span class="norman">Norman</span> Group</a></h1>
                <p class="tagline show-for-medium-up">Evidence-Based User Experience Research, Training, and Consulting</p>

                <!-- The button to toggles form search on small screen -->


                <div style="clear: right;width:  100%;height: 1px;"></div><?php define('ROOT_PATH',$_SERVER["DOCUMENT_ROOT"] . '/apps/navigation_styles/navigations/5'); ?><?php include(ROOT_PATH)."/navigation.php"; ?>

                <!-- Drop down menu on small screen -->
                <div id="hideNav" class="show-for-small-down">
                  
                  <ul class="menu-content">
                    <li><a href="../../index.php">Home</a></li>
                    <li class="has-submenu">
                      <a>Training</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../training/index.php">Overview</a></li>
                        
<li id="nav-ux-conference"><a href="../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../online-seminars/index.php"><span>Online Seminars</span></a></li>

                      </ul>
                    </li>
                    <li class="has-submenu">
                      <a>Consulting</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
  <li><a href="../../consulting/index.php">Overview</a></li>
  
  <li><a href="../../consulting/benchmarking/index.php">Benchmarking</a></li>
  
  <li><a href="../../consulting/expert-design-review/index.php">Expert Design Review</a></li>
  
  <li><a href="../../consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
  
  <li><a href="../../consulting/usability-testing/index.php">Usability Testing </a></li>
  
  <li><a href="../../consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
  
  <li><a href="../../in-house-training/index.php">In-House Training</a></li>
</ul>

                    </li>

                    <li><a href="../../reports/index.php">Reports</a></li>
                    <li><a href="../index.php">Articles</a></li>
                    <li class="has-submenu"><a>About NN/G</a><i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../about/index.php">Overview</a></li>
                        <li><a href="../../people/index.php">People</a></li>
                        <li><a href="../../about/why-nng/index.php">Why NN/g?</a></li>
                        <li><a href="../../about/contact/index.php">Contact</a></li>
                        <li><a href="../../news/index.php">News</a></li>
                        <li><a href="../../about/history/index.php">History</a></li>
                        <li><a href="../../books/index.php">Books</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                </div>

                <hr class="header-divider show-for-small-only"/>


            </div>
       </div>

        <div id="hideNav">            <nav class="primary-nav small-12 column">
                <ul>
                    <li><a class="navitem" id="nav-home" href="../../index.php">Home</a></li>
                    <li><a class="navitem" id="nav-training" href="../../training/index.php">Training</a></li>
                    <li><a class="navitem" id="nav-consulting" href="../../consulting/index.php">Consulting</a></li>
                    <li><a class="navitem" id="nav-reports" href="../../reports/index.php">Reports</a></li>
                    <li><a class="navitem" id="nav-articles" href="../index.php">Articles</a></li>
                    <li><a class="navitem" id="nav-about" href="../../about/index.php">About NN/G</a></li>
                </ul>
            </nav>
            <nav class="secondary-nav small-12 medium-12 large-12 columns">
                
                
            </nav>
        </div>
    </header>
    <div class="l-content"><span id="preventClose" style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776; Menu</span>
    
  

  

  <div class="row">
    
      
      <section class="small-12 medium-3 columns collapsable-content l-navrail articles-left-column">
        <h1 class="show-for-small-down collapse-link mobile-browse-header">Browse by Topic and Author</h1>
          <div class="row collapsable">

            
            <div class="small-6 medium-12 columns">
              <section id="article-popular-topics" style="display: none;">
                
                <h1>Topics</h1>
                <ul id="article-popular-topics">
                    
                    <li>
                        
                        <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/intranets/all/index.php">Intranets</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/user-testing/all/index.php">User Testing</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                        
                    </li>
                    
                    <li><a href="#" id="article-see-all-topics"><strong>See all topics&hellip;</strong></a></li>
                </ul>
                
              </section>
              <section id="article-all-topics" style="display: none;">
                
                <h1>
                  <span class="show-for-medium-up">All Article Topics</span>
                  <span class="show-for-small-down">All Topics</span>
                  (<a href="#" id="article-hide-all-topics">hide</a>)
                </h1>
                <ul>
                  
                  <li>
                    
                    <a href="../../topic/accessibility/all/index.php">Accessibility</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/agile/all/index.php">Agile</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/analytics-and-metrics/all/index.php">Analytics &amp; Metrics</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/applications/all/index.php">Application Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/b2b-websites/all/index.php">B2B Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/behavior-patterns/all/index.php">Behavior Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/branding/all/index.php">Branding</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/content-strategy/all/index.php">Content Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/corporate-websites/all/index.php">Corporate Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/design-patterns/all/index.php">Design Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-design-process/all/index.php">Design Process</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/email/all/index.php">Email</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/eyetracking/all/index.php">Eyetracking</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/heuristic-evaluation/all/index.php">Heuristic Evaluation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/human-computer-interaction/all/index.php">Human Computer Interaction</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-ideation/all/index.php">Ideation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/information-architecture/all/index.php">Information Architecture</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/interaction-design/all/index.php">Interaction Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/international-users/all/index.php">International Users</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/intranets/all/index.php">Intranets</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-management/all/index.php">Management</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/navigation/all/index.php">Navigation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/non-profit-websites/all/index.php">Non-Profit Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/personas/all/index.php">Personas</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/persuasive-design/all/index.php">Persuasive Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/prototyping/all/index.php">Prototyping</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/psychology-and-ux/all/index.php">Psychology and UX</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/research-methods/all/index.php">Research Methods</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/search/all/index.php">Search</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/social-media/all/index.php">Social Media</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/strategy/all/index.php">Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/user-testing/all/index.php">User Testing</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-teams/all/index.php">UX Teams</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/visual-design/all/index.php">Visual Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/writing-web/all/index.php">Writing for the Web</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/young-users/all/index.php">Young Users</a>
                    
                  </li>
                  
                </ul>
                
              </section>
            </div>
            <script>
              /* Determine which topics list will be displayed: the full or the
               * partial.  We are inlining this close to the list itself to
               * prevent rendering delays on the client
               */
              var topic_list = $.cookie("article-topics");

              var popular_topics = $('#article-popular-topics');
              var all_topics = $('#article-all-topics');

              if(topic_list == 1) {
                  popular_topics.hide();
                  all_topics.show();
              } else {
                  popular_topics.show();
                  all_topics.hide();
              }
            </script>
            

            
            <div class="small-6 medium-12 columns">
              
              <section id="article-initial-authors">
                  <h1>Author</h1>
                  <ul>
                    
                    <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                    
                    <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                    
                    <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                    
                    <li><a href="#" onClick="$('#article-initial-authors').hide(); $('#article-all-authors').show();"><strong>See all authors&hellip;</strong></a></li>
                  </ul>
              </section>
              <section id="article-all-authors" style="display: none;">
                <h1>
                  All Authors
                  (<a href="#" id="article-hide-all-authors">hide</a>)
                </h1>
                <ul>
                  
                  <li><a href="../author/raluca-budiu/index.php">Raluca Budiu</a></li>
                  
                  <li><a href="../author/patty-caya/index.php">Patty Caya</a></li>
                  
                  <li><a href="../author/susan-farrell/index.php">Susan Farrell</a></li>
                  
                  <li><a href="../author/therese-fessenden/index.php">Therese Fessenden</a></li>
                  
                  <li><a href="../author/kim-flaherty/index.php">Kim Flaherty</a></li>
                  
                  <li><a href="../author/sarah-gibbons/index.php">Sarah Gibbons</a></li>
                  
                  <li><a href="../author/aurora-harley/index.php">Aurora Harley</a></li>
                  
                  <li><a href="../author/kate-kaplan/index.php">Kate Kaplan</a></li>
                  
                  <li><a href="../author/page-laubheimer/index.php">Page Laubheimer</a></li>
                  
                  <li><a href="../author/angela-li/index.php">Angie Li</a></li>
                  
                  <li><a href="../author/hoa-loranger/index.php">Hoa Loranger</a></li>
                  
                  <li><a href="../author/kate-meyer/index.php">Kate Meyer</a></li>
                  
                  <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                  
                  <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                  
                  <li><a href="../author/kara-pernice/index.php">Kara Pernice</a></li>
                  
                  <li><a href="../author/christian-rohrer/index.php">Christian Rohrer</a></li>
                  
                  <li><a href="../author/amy-schade/index.php">Amy Schade</a></li>
                  
                  <li><a href="../author/katie-sherwin/index.php">Katie Sherwin</a></li>
                  
                  <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                  
                  <li><a href="../author/kathryn-whitenton/index.php">Kathryn Whitenton</a></li>
                  
                </ul>
              </section>
              
            </div>
            
          </div>

          
          <section class="hide-for-small-down">
            
              
              <h1>Recent Articles</h1>
              
              <ul>
                  
                  <li><a href="../seamless-cross-channel/index.php">Seamlessness in the Omnichannel User Experience</a></li>
                  
                  <li><a href="../brand-intention-interpretation/index.php">Experience Design: Bridging Brand Intention and Brand Interpretation</a></li>
                  
                  <li><a href="../eyetracking-tasks-efficient-scanning/index.php">Scanning Patterns on the Web Are Optimized for the Current Task</a></li>
                  
                  <li><a href="../flat-design-best-practices/index.php">Flat-Design Best Practices</a></li>
                  
                  <li><a href="../ux-user-stories/index.php">Accounting for UX Work with User Stories in Agile Projects</a></li>
                  
                  <li><a href="../index.php"><strong>See all articles&hellip;</strong></a></li>
              </ul>
              
            
          </section>
          

          
          
          <section class="hide-for-small-down">
            <h1>Popular Articles</h1>
            
            <ul>
                
                <li><a href="../usability-101-introduction-to-usability/index.php">Usability 101: Introduction to Usability</a></li>
                
                <li><a href="../top-10-mistakes-web-design/index.php">Top 10 Mistakes in Web Design</a></li>
                
                <li><a href="../how-users-read-on-the-web/index.php">How Users Read on the Web</a></li>
                
                <li><a href="../f-shaped-pattern-reading-web-content/index.php">F-Shaped Pattern For Reading Web Content</a></li>
                
                <li><a href="../ten-usability-heuristics/index.php">10 Usability Heuristics for User Interface Design</a></li>
                
                <li><a href="../intranet-design-2016/index.php">10 Best Intranets of 2016</a></li>
                
                <li><a href="../which-ux-research-methods/index.php">When to Use Which User-Experience Research Methods</a></li>
                
                <li><a href="../response-times-3-important-limits/index.php">Response Times: The 3 Important Limits</a></li>
                
                <li><a href="../why-you-only-need-to-test-with-5-users/index.php">Why You Only Need to Test with 5 Users</a></li>
                
                <li><a href="../page-fold-manifesto/index.php">The Fold Manifesto: Why the Page Fold Still Matters</a></li>
                
            </ul>
          </section>
          
          

          
            <section class="hide-for-small-down">
              
            </section>
          

      </section>
      

      
        <div class="small-12 medium-9 columns content-body">
          




<article class="l-subsection alertbox-article-content article-detail-bl">
  
  <header>
    <div class="l-subsection">
      <h1>Social Media Natives: Growing Up with Social Networking</h1>
      <div class="author-meta">by
        
          
            
            <a href="../author/kate-meyer/index.php">Kate Meyer</a>
            
          
        on  August 28, 2016
        
      </div>

      

      
      <div class="topic-links">
        Topics:
        
          <ul>

  <li><a href="../../topic/social-media/index.php">Social Media</a></li>

  <li><a href="../../topic/young-users/index.php">Young Users</a></li>

</ul>
        
      </div>
      
    </div>
  </header>
  

  
  
  <section>
    <p class="lede"><strong>Summary:</strong> Social media use has altered how Millennials think about friendships and relationships. This impact stands as a valuable reminder of the consequences of UX-design decisions.</p>
    <br />
  </section>
  
  

  
  <section id="article-body"><p>Our previous research on the Millennial generation shows little evidence for the theory that “<a href="../millennials-digital-natives/index.php">digital natives</a>” are fundamentally different from “digital immigrants.” That said, <a href="../young-adults-ux/index.php">our research</a> does suggest that growing up with information technology <em>has</em> influenced Millennials’ behaviors, expectations, and preferences.</p>

<p>As a reminder, the Millennial generation (in its broadest definition) refers to people born between 1980 and 2000. In this article, we explore how <strong>early exposure to social media has influenced their approach to friendships, relationships, and self-image</strong>. Specifically:</p>

<ul>
	<li>Previously subtle relationships and social interactions became explicit and visible.</li>
	<li>Previously separate social contexts became merged and entangled.</li>
</ul>

<p>In a <a href="../young-adults-ux/index.php">large multinational qualitative study</a>, we conducted interviews and usability testing with 91 Millennial participants. We supplemented these findings on Millennial users with naturalistic recordings, a diary study, and a survey.</p>

<h2>Millennial Participation in Social Media</h2>

<p>Social media saturation among Millennials has dramatically increased over the past ten years. In 2006, when many Millennials were in middle and high school, 55% of teenagers reported having at least one social media account, according to the Pew Research Center. In 2010, 73% of younger Millennials (middle and high schoolers) reported having a social media account, while 78% of Millennials aged 18–29 (college-aged young adults) reported being on social media.</p>

<p>Today, <strong>around 90% of Millennials (currently teens and young adults) have at least one social media account</strong>. Many of them have more than one. In our 2015 <a href="../young-adults-ux/index.php">research with young adults</a>, the average reported number of social media accounts was 4.</p>

<h2>Invisible Relationships Became Visible</h2>

<p>Social media introduced new dynamics to young people’s social lives. The standard progression of friendships and romantic relationships is now somewhat different than it was 15 years ago.</p>

<p>We can see some of the best evidence of this evolution in the way youth vocabulary has shifted.</p>

<ul>
	<li><strong>“Friending”:</strong> A verb, which, despite appearances, is not equivalent to actually becoming friends. Young people might “friend” acquaintances or strangers. Participants in our study admitted to not knowing all of their Facebook “friends” or Instagram followers personally.</li>
	<li>“<strong>Facebook official” (</strong>abbreviated variant: “Facebook offish”)<strong>:</strong> A relatively new stage in relationships, when romantic partners decide to change their Facebook statuses to reflect the officiality (and/or exclusivity) of their relationship. According to Pew Research, 18% of teens report breaking up with someone or having been broken up with by someone via Facebook relationship-status change. Many young couples actually choose not to take this step, but the social pattern has already cemented its place in popular language. One young adult might ask another, “Are Jessi and Paul official yet?”</li>
	<li><strong>“Talking”:</strong> Another new stage in relationships, when the couple is getting to know each other and interested in a potential relationship, but have not yet attained official status. “Talking” is to “official<em>”</em> what “dating” was to “going steady.”</li>
	<li><strong>“FOMO”</strong> (Fear Of Missing Out): A state of social anxiety that your peers or friends may be having fun doing something without you. FOMO is often triggered by a social media post. (I assume this inspired the title of comedian Mindy Kaling’s book, <a href="https://www.amazon.com/Everyone-Hanging-Without-Other-Concerns/dp/0307886271"><em>Is Everyone Hanging Out Without Me? And Other Concerns</em></a><em>.</em><em>)</em></li>
</ul>

<p>(<em>Note:</em> You may already be familiar with these terms, even if you aren’t a Millennial. That’s because these social media constructs impact users of all ages, not just Millennials. It’s probably also because research has shown that <a href="http://www.smithsonianmag.com/smart-news/teenage-girls-have-been-revolutionizing-language-16th-century-180956216/?no-ist">young people tend to be language disruptors</a>, and the linguistic patterns that they adopt eventually become common usage.)</p>

<p>Social media has also become a means of expressing an evaluation of relationships, and of quantifying them. One memorable example of this is MySpace’s infamous <em>Top 8</em> feature. Many Millennials were early users of MySpace during its peak popularity in 2004. At that time, teens could manually choose their top 8 closest friends, and display them on their profile page. For many young Millennials, that was an anxiety-inducing decision. MySpace has since removed the feature.</p>

<p>Likes and favorites (ubiquitous on most social media platforms) are a very visible expression of vague positive feeling toward a person, event, or opinion — and they’re <em>counted</em>. Researchers at UCLA recently studied the effects of likes on adolescence brains. They found that when participants saw a photo with many likes, there was more activation in the reward centers of their brains. They also noted that adolescents were more likely to contribute their own like to the photo — regardless of the intrinsic qualities of the photo. Likes are a reinforcing form of <a href="../social-proof-ux/index.php">social proof</a>. For young people, likes can be a visual manifestation of peer pressure.</p>

<h2>Merging Contexts</h2>

<p>In any given day, we all have a variety of social contexts that we have to deal with. Different audiences and contexts require different social behaviors, and we try to respond appropriately. These social contexts are easier to manage in the offline world, where they generally stay separate and visible. A college student talking to her friends in a hallway can see who her audience is, and can modify her speech and behavior when she walks away from her friends and starts speaking to a professor.</p>

<p>When we participate in social media, we experience what youth researcher danah boyd describes as a <em>context collapse. </em>A context collapse occurs when people have to <strong>simultaneously manage disparate social contexts</strong> which require different social behaviors. When that same college student writes a Facebook post, she suddenly needs to manage a variety of different social contexts at a single time. Her audience is nearly invisible and difficult to predict, especially in platforms like Facebook where her words may be preserved and searchable. Her closest friends might see her posts, but so might her acquaintances, her grandma, her neighbor, her professor, or her potential employer sometime in the future. Each different audience may require a different social behavior or have different interpretations of her post.</p>

<p>Of course, everyone who uses social media experiences context collapse to some degree. But context collapse can be much more challenging for adolescents, who are undergoing an <strong>intense period of self-definition </strong>and are often <strong>the heaviest social media users</strong>.</p>

<p>Through trial and error, Millennials have developed strategies to help them manage the merging of separate contexts. In our research, participants expressed an awareness of their audiences on the various social media platforms they used.</p>

<ul>
	<li>They described <strong>Facebook</strong> as most diverse in terms of connections: a place where they were connected to their friends, but also their family and potential employers. In 2014, more than half of all online seniors (65 or older) were on Facebook. This is a big shift from ten years earlier, when site’s early adopters were almost all young adults.</li>
	<li>They reported using <strong>Twitter</strong> more for following special interests (news, celebrities, musicians, companies) than for connecting with friends or family.</li>
	<li>They described <strong>Instagram</strong> as the most entertainment-oriented platform, used for sharing photos of their experiences or interests with their friends.</li>
</ul>

<p>Millennials’ decisions about where to share posts or photos <strong>depended on the audience of each platform, as well as the visibility of the post</strong> afforded by the functionality of the platform. When asked how she decided where to post a photo, one Millennial participant responded: “It depends on the quality of the picture, and who would see it. On Facebook it’d be primarily family, because those are the people who pay attention most to my page. On Twitter, [...] depending on the time of day, it might not be seen at all.”</p>

<p>When we consider the stress of managing multiple audiences and social contexts across multiple platforms, it’s easy to see the appeal of Snapchat and other autodelete apps, and why, according to Pew Research Center, more than 40% of young adults use it. The impermanence of these interactions allows adolescents to connect with their peers without worrying so much about self-presentation.</p>

<h2>Consequences for UX Design</h2>

<p>Why should UX professionals care about these social shifts in the Millennial generation?</p>

<p>First, the <strong>consequences of social media on adolescents is an illustration of the far-reaching impact </strong>we can have as UX professionals. Did MySpace’s product team anticipate the emotional impact its <em>Top 8</em> feature would have before it added it? Did Facebook know it’d shift the language used to describe young relationships?</p>

<p>We can’t always foresee all of the significance of our design choices, but this should be a reminder of the potential consequences. As designers, even if we don’t work on social products, we need to understand and respect that our choices can deeply impact our users, their emotions, and relationships.</p>

<p>Second, there’s <strong>intrinsic value in knowing how our users think and feel.</strong> Empathy with our users’ experiences and preferences improves our ability to connect with them and satisfy their needs (and is the first stage in the <a href="../design-thinking/index.php">design-thinking process</a>). For example, an understanding of Millennials’ anxiety about the permanence of shared photos gave Evan Spiegel the idea that became Snapchat.</p>

<figure class="caption"><img alt="" height="717" src="https://media.nngroup.com/media/editor/2016/08/24/forever-fomo.png" width="1440"/>
<figcaption><em>In its fat footer, youth-targeting Forever 21 tries to leverage FOMO to connect with its audience, and encourage people to sign up for its newsletter. (Interestingly, nothing in the page copy explicitly indicates that people are supposed to sign up for a newsletter or provide their email address. Users have to infer what they have to do from previous experience. I suspect this is a conscious choice, since the words “newsletter,” “sign-up,” and “email address” aren’t inherently ‘cool.’)</em></figcaption>
</figure>

<p>Follow these recommendations to apply these Millennial insights to your social media strategy.</p>

<ul>
	<li><strong>Contrary to popular advice, you <em>don’t</em> have to be on social media to reach Millennials.</strong> Yes, Millennials are heavy and frequent social media users. Yes, social media can be a useful tool for branding and marketing. But it isn’t necessary for every organization. If you don’t have a topic that easily lends itself to social sharing, or if social media is out-of-character for your brand, you don’t need it. Millennials still rely on Google when they want to find out more about organizations. As long as you have a site, they can find you. Zero social media presence is better than a poorly executed or half-hearted social media presence, which will build negative brand equity.</li>
	<li><strong>Consider and clarify the consequences of social media integration.</strong> Know that your users are managing their online personas among conflicting social contexts, and respect it. If you encourage users to connect their social media accounts to your application, don’t post information about their activities by default. Many Millennials are suspicious that social signin will lead to a loss of control over their accounts. Help them overcome these fears by being explicit about what will happen. Just including social-platform icons on a page isn’t clear enough — that Facebook <em>f</em> could be a link to your Facebook page, or it could be embedded social sharing.</li>
</ul>

<figure class="caption"><img alt="" height="742" src="https://media.nngroup.com/media/editor/2016/08/24/screen-shot-2016-08-24-at-82746-pm.png" width="1100"/>
<figcaption><em>heineken.com</em></figcaption>
</figure>

<figure class="caption"><img alt="" height="742" src="https://media.nngroup.com/media/editor/2016/08/24/screen-shot-2016-08-24-at-82755-pm.png" width="1100"/>
<figcaption><em>heineken.com: Organizations affiliated with socially sensitive topics, like alcohol, should use extra caution with social features. This beer brand’s website offers social signin, but doesn’t clarify what the social connection will do before requesting login information.</em></figcaption>
</figure>

<ul>
	<li><strong>Do as the Millennials do: Think strategically about social media platforms.</strong> If you’re set on a social media presence, decide which platform you’ll use based on who you’re targeting, and what kind of content you’ll share. Is your audience primarily young adults or teens? Snapchat might be a good platform. Are you able to routinely produce high-quality photographs of things that are relevant to your organization? Instagram might work. Unless you have a dedicated social media team, you should prioritize your efforts for each platform accordingly. And remember, some platforms are integrated and allow you to (for example) publish the same post on Instagram, Facebook, and Twitter all at once.</li>
	<li><strong>Build a relationship by offering value. </strong>Don’t abuse your access to your audience. Once you get a base of connections or followers, it’s a valuable channel of communication with your users. Don’t flood their feeds by posting too frequently, or by posting irrelevant content or content that’s only interesting <em>to</em> <em>you</em>. Provide an added value: a special sale, an inclusive update about an upcoming event or product launch, and so on.</li>
</ul>

<figure class="caption"><img alt="" height="594" src="https://media.nngroup.com/media/editor/2016/08/24/target-insta.jpg" width="934"/>
<figcaption><em>Target’s Instagram account hits the right balance for an appealing corporate account. This post is relevant to a current event (if you consider “Chocolate Chip Cookie Day” to be an event), incorporates a trending hashtag, and offers a fun recipe — and it just so happens you can buy everything you need at Target. Its quirky, lighthearted tone fits with Target’s other channels, like TV ads. Notice the level of professionalism in the copy and the photograph. Target’s posts are obviously planned in advance and executed by dedicated social media experts.</em></figcaption>
</figure>

<h2>What About Gen Z?</h2>

<p>The effects that social media use has had on the Millennial generation will likely be even more pronounced for its successor, Generation Z (a.k.a. iGen — people born after 2000). As of 2015, 89% of the older, teenage Gen Zers had at least one social media account. Despite platform policies that try to keep tweens and children off their sites, many younger members of Gen Z are also already active users. Case in point: My young cousin, who has been a social media user from the age of 11.</p>

<figure class="caption"><img alt="" height="512" src="https://media.nngroup.com/media/editor/2016/08/24/screenshot_20160811-095344.png" width="300"/>
<figcaption><em>A typical Gen Z kid, my cousin uses Instagram to share gems like this with his friends. At the time I’m writing this, he currently has twice as many Instagram posts as this Millennial author, and many times as many followers.</em></figcaption>
</figure>

<p>For more research findings and recommendations for designing for young adults and Millennials:</p>

<ul>
	<li>see our 216-page <a href="../../reports/designing-for-young-adults/index.php">Designing for Young Adults (Ages 18-25)</a> report, or</li>
	<li>our full-day training course, <a href="../../courses/designing-millennials/index.php">Designing for Millennials</a>.</li>
</ul>

<h2>Resources</h2>

<p>boyd, danah. It’s Complicated: The Social Lives of Networked Teens (2014). <a href="https://www.amazon.com/Its-Complicated-Social-Lives-Networked/dp/0300166311">https://www.amazon.com/Its-Complicated-Social-Lives-Networked/dp/0300166311</a></p>

<p>Pew Research Center. Mobile Messaging and Social Media 2015 (2015) <a href="http://www.pewinternet.org/2015/08/19/mobile-messaging-and-social-media-2015/">http://www.pewinternet.org/2015/08/19/mobile-messaging-and-social-media-2015/</a></p>

<p>Pew Research Center. Social Media and Young Adults (2010). <a href="http://www.pewinternet.org/2010/02/03/social-media-and-young-adults/">http://www.pewinternet.org/2010/02/03/social-media-and-young-adults/</a></p>

<p>Pew Research Center. Social Media Update 2014 (2015). <a href="http://www.pewinternet.org/2015/01/09/social-media-update-2014/">http://www.pewinternet.org/2015/01/09/social-media-update-2014/</a></p>

<p>Pew Research Center. Social Media Usage: 2005-2015 (2015). <a href="http://www.pewinternet.org/2015/10/08/social-networking-usage-2005-2015/">http://www.pewinternet.org/2015/10/08/social-networking-usage-2005-2015/</a></p>

<p>Pew Research Center. Teens, Social Media, and Privacy (2013). <a href="http://www.pewinternet.org/2013/05/21/teens-social-media-and-privacy/">http://www.pewinternet.org/2013/05/21/teens-social-media-and-privacy/</a></p>

<p>Pew Research Center. Teens, Social Media, and Technology Overview 2015 (2015) <a href="http://www.pewinternet.org/2015/04/09/teens-social-media-technology-2015/">http://www.pewinternet.org/2015/04/09/teens-social-media-technology-2015/</a></p>

<p>Pew Research Center. Teens, Technology, and Romantic Relationships (2015). <a href="http://www.pewinternet.org/2015/10/01/teens-technology-and-romantic-relationships/">http://www.pewinternet.org/2015/10/01/teens-technology-and-romantic-relationships/</a></p>

<p>Wolpert, Stuart. The teenage brain on social media (2016). UCLA Newsroom. <a href="http://newsroom.ucla.edu/releases/the-teenage-brain-on-social-media">http://newsroom.ucla.edu/releases/the-teenage-brain-on-social-media</a></p>
</section>
  

  
  <section>
    
        
    
  </section>
  
  
  
  
  

  


  <section>
    <br />
    <p class="share"><b>Share <span class="hide-for-small-down">this article</span>:</b> <a href="https://twitter.com/intent/tweet?url=http://www.nngroup.com/articles/social-media-natives/&amp;text=Social%20Media%20Natives:%20Growing%20Up%20with%20Social%20Networking&amp;via=nngroup">Twitter</a> | <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=http://www.nngroup.com/articles/social-media-natives/&amp;title=Social%20Media%20Natives:%20Growing%20Up%20with%20Social%20Networking&amp;source=Nielsen%20Norman%20Group">LinkedIn</a> | <a href="https://plus.google.com/share?url=http://www.nngroup.com/articles/social-media-natives/">Google+</a> | <a href="mailto:?subject=NN/g Article: Social Media Natives: Growing Up with Social Networking&amp;body=http://www.nngroup.com/articles/social-media-natives/">Email</a></p>
    <hr />
  </section>

  <footer class="l-subsection">
    
    
    <div class="learn-more">
      <h2>Learn More</h2>
      <p><a href="../subscribe/index.php">Subscribe to the weekly newsletter</a> to get notified about future articles.</p>
      </div>
    
    

    <div class="row show-for-small-down related-content">
      
      
      

      
      
        <div class="column small-12 topic-links">All articles about:
          <ul>
            
            <li><a href="../../topic/social-media/index.php">Social Media</a></li>
            
            <li><a href="../../topic/young-users/index.php">Young Users</a></li>
            
          </ul>
        </div>
      
      

      
      
        <div class="column small-12">
          <h3>UX Conference Training Courses</h3>
           <ul class="no-bullet">
    
        <li><a href="../../courses/cross-channel-user-experience/index.php">Omnichannel Journeys and Customer Experience</a></li>
    
        <li><a href="../../courses/translating-brand/index.php">Translating Brand into User Interactions</a></li>
    
        <li><a href="../../courses/website-design-lessons-social-psychology/index.php">Website Design Lessons from Social Psychology</a></li>
    
</ul>
        </div>
      
      

      
      
        <div class="small-12 column">
          <h3>Research Reports</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../reports/designing-for-young-adults/index.php">Designing for Young Adults (Ages 18-25)</a></li>
              
            
              
                <li><a href="../../reports/teenagers-on-the-web/index.php">Teenagers (Ages 13-17) on the Web</a></li>
              
            
              
                <li><a href="../../reports/children-on-the-web/index.php">Children (Ages 3-12) on the Web</a></li>
              
            
              
                <li><a href="../../reports/university/index.php">University Websites</a></li>
              
            
              
                <li><a href="../../reports/social-media-user-experience/index.php">Social Media User Experience</a></li>
              
            
          </ul>
        </div>
      
      

      
      
        <div class="small-12 column">
          <h3>Online Seminars</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../online-seminars/millennials-online/index.php">Millennials Online</a></li>
              
            
              
                <li><a href="../../online-seminars/university-websites/index.php">University Website UX Essentials</a></li>
              
            
          </ul>
        </div>
      
      
    </div>

    <div class="hide-for-small-down row related-content">
      <div class="medium-6 columns">
        
        
          <div>
            <h3>Articles</h3>
            <ul class="no-bullet">
              
                
                <li><a href="../university-sites/index.php">University Websites: Top 10 Design Guidelines</a></li>
                
              
                
                <li><a href="../young-adults-ux/index.php">Young Adults/Millennials as Web Users (Ages 18–25)</a></li>
                
              
                
                <li><a href="../millennials-digital-natives/index.php">Millennials as Digital Natives: Myths and Realities</a></li>
                
              
                
                <li><a href="../social-media-outsourcing-can-be-risky/index.php">Social Media Outsourcing Can Be Risky</a></li>
                
              
                
                <li><a href="../twitter-postings-iterative-design/index.php">Twitter Postings: Iterative Design</a></li>
                
              
            </ul>
          </div>
        
        

        
          
          <div>
            <h3>Research Reports</h3>
            <ul class="no-bullet">
              
                
                  <li><a href="../../reports/designing-for-young-adults/index.php">Designing for Young Adults (Ages 18-25)</a></li>
                
              
                
                  <li><a href="../../reports/teenagers-on-the-web/index.php">Teenagers (Ages 13-17) on the Web</a></li>
                
              
                
                  <li><a href="../../reports/children-on-the-web/index.php">Children (Ages 3-12) on the Web</a></li>
                
              
                
                  <li><a href="../../reports/university/index.php">University Websites</a></li>
                
              
                
                  <li><a href="../../reports/social-media-user-experience/index.php">Social Media User Experience</a></li>
                
              
            </ul>
          </div>
          
        

      </div>
      <div class="medium-6 columns">
        
          
          <div>
            <h3>UX Conference Training Courses</h3>
             <ul class="no-bullet">
    
        <li><a href="../../courses/cross-channel-user-experience/index.php">Omnichannel Journeys and Customer Experience</a></li>
    
        <li><a href="../../courses/translating-brand/index.php">Translating Brand into User Interactions</a></li>
    
        <li><a href="../../courses/website-design-lessons-social-psychology/index.php">Website Design Lessons from Social Psychology</a></li>
    
</ul>
          </div>
          
        
        
          
          <div>
            <h3>Online Seminars</h3>
            <ul class="no-bullet">
            
                <li><a href="../../online-seminars/millennials-online/index.php">Millennials Online</a></li>
            
                <li><a href="../../online-seminars/university-websites/index.php">University Website UX Essentials</a></li>
            
            </ul>
          </div>
          
        
      </div>
    </div>
  </footer>
</article>

        </div>

      

    
  </div>
  <div class="row">
    
      <div class="column small-12 show-for-small-down l-navrail announcements">
        
          
        
      </div>
    
    
  </div>

    </div>
    




<footer id="page-footer">
    <div class="row hide-for-print">
        <div class="column small-12 medium-6">
            <section class="footer-popular">
                
                <h1>Popular Topics</h1>
                <div style="float: left;  width: 15em; display: block;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/applications/index.php">Application Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/e-commerce/index.php">E-Commerce Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/eyetracking/index.php">Eyetracking </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/information-architecture/index.php">Information Architecture </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/intranets/index.php">Intranet Design </a></li>
	<li><a href="../../topic/interaction-design/index.php">Interaction Design </a></li>
</ul>
</div>

<div style="float: left;  width: 15em; display: block; list-style: none;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/mobile-and-tablet-design/index.php">Mobile and Tablet Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/credibility-persuasion/index.php">Persuasive Design</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/research-methods/index.php">Research Methods</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/user-testing/index.php">User Testing </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/web-usability/index.php">Web Usability </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/writing-web/index.php">Web Writing &amp; Content Strategy </a></li>
</ul>
</div>

<p>&nbsp;</p>

                
            </section>
        </div>

        <div class="column small-12 medium-6">
            <div class="row">
                <div class="column small-6">
                    <section class="footer-about">
                        <h1>Follow Us</h1>
                        <ul>
	<li><a href="../subscribe/index.php">Subscribe to our weekly UX newsletter</a>,&nbsp;Jakob Nielsen&#39;s Alertbox</li>
	<li>Visit Don Norman&#39;s website, <a href="http://www.jnd.org/"> www.jnd.org </a></li>
	<li>Visit Bruce Tognazzini&#39;s website, <a href="http://www.asktog.com/"> www.asktog.com </a></li>
	<li style="padding-top: 6px;"><a class="twitter-follow-button" data-show-count="false" data-size="large" href="https://twitter.com/nngroup"><img alt="Follow @nngroup on Twitter" height="20" src="https://media.nngroup.com/media/editor/2016/12/30/twitter-follow-button.png" style="width: 117px; height: 20px;" width="117" /></a></li>
	<li style="padding-top: 6px;"><img alt="" height="14" src="https://media.nngroup.com/media/editor/2016/07/26/feed-icon-14x14.png" style="vertical-align:text-top;width:14px;margin-right:.25em;" width="14" /><a href="../../feed/rss/index.php">Subscribe to our RSS feed</a></li>
</ul>

                    </section>
                </div>

                <div class="column small-6">
                    <section class="footer-about right-column">
                        <h1>About
</h1>
                        <ul class="no-bullet">
	<li><a href="../../about/contact/index.php">Contact information</a></li>
	<li><a href="../../about/index.php">About Us</a></li>
	<li><a href="../../about/why-nng/index.php">Why NN/g? </a></li>
	<li><a href="../../return-policy/index.php">Return Policy</a></li>
</ul>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="copyright column small-12">
            <p><a href="../../copyright-and-reprint-info/index.php">Copyright</a> &copy; 1998-2017 Nielsen Norman Group, All Rights Reserved.</p>
        </div>
    </div>
</footer>


    


        <script src="https://media.nngroup.com/static/vendor/fastclick/js/fastclick.min.js"></script>
        <script src="https://media.nngroup.com/static/vendor/foundation/js/foundation.min.js"></script>
        <script src="http://media.nngroup.com/static/js/app.js"></script>
        <script src="http://media.nngroup.com/static/js/scroll-to-fixed.js"></script><script>var externalLinks = document.querySelectorAll('a[href^="http"]');for (var i = externalLinks.length-1; i >= 0; i--) {externalLinks[i].addEventListener("click", function(event) { alert("External links are disabled for this test."); event.preventDefault(); }, false);}var mailLinks = document.querySelectorAll('a[href^="mailto"]');for (var i = mailLinks.length-1; i >= 0; i--) {mailLinks[i].addEventListener("click", function(event) { alert("Email links are disabled for this test."); event.preventDefault(); }, false);}var externalForms = document.querySelectorAll('form');for (var i = externalForms.length-1; i >= 0; i--) {externalForms[i].addEventListener("submit", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false); externalForms[i].addEventListener("click", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false);}</script>

        <script>
            
            $(function() {
                $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
                $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
            });
        </script>

        
<script src="https://media.nngroup.com/static/js/articles.js"></script>


        
        <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1072699960;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072699960/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
    </body>

<!-- Mirrored from www.nngroup.com/articles/social-media-natives/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:12:27 GMT -->
</html>
