<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->





    
<!-- Mirrored from www.nngroup.com/articles/radio-buttons-default-selection/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:06:27 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1026.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"licenseKey":"1a30ef9931","queueTime":12,"beacon":"bam.nr-data.net","errorBeacon":"bam.nr-data.net","applicationID":"2048992","transactionName":"NFxSMRBUX0EDUkIPDA0WdhAMVkVbDV8ZBxEXUFMJB0YfRAtUQRVZIktEDAFZVHYHRVcPD01eVRE=","applicationTime":328,"agent":""}</script>
        <title>Radio Buttons: Always Select One?</title><meta property="og:title" content="Radio Buttons: Always Select One?" />
  
        
        <meta name="description" content="Select a single radio button by default in most cases. Reasons to deviate or not: expedite tasks, the power of suggestion, user expectations, safety nets.">
        <meta property="og:description" content="Select a single radio button by default in most cases. Reasons to deviate or not: expedite tasks, the power of suggestion, user expectations, safety nets." />
        
  
        
	
        
        <meta name="keywords" content="radio button, check box, checkbox, default, default selection, blank, none selected, radio, power of suggestion, lists, UI control">
        

        

        <meta name="robots" content="noarchive">
        <meta name="viewport" content="width=device-width">

        <link rel="search" type="application/opensearchdescription+xml" title="NN/g Search" href="../../opensearch.xml" />
        <link rel="shortcut icon" href="http://media.nngroup.com/static/img/favicon.ico" />

        <link rel="stylesheet" href="https://media.nngroup.com/static/vendor/font-awesome/css/font-awesome.min.css">

        
        <link rel="stylesheet" href="https://media.nngroup.com/static/css/7232a960f1df.css" type="text/css" />
        <link rel="stylesheet" href="http://fast.fonts.com/t/1.css?apiType=css&amp;projectid=c5846260-9def-46ec-9407-0f1349bd2d64" type="text/css">

        <script src="https://media.nngroup.com/static/vendor/modernizr/js/modernizr.min.js"></script>

        <script type="text/javascript" src="https://media.nngroup.com/static/js/3283be5def74.js"></script>
        

        

        
        
        <!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=229034,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('http://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->


<!-- Google Analytics Tracking Code for http://nngroup.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23132681-1', 'nngroup.com');
  ga('require', 'linkid', 'https://www.nngroup.com/articles/radio-buttons-default-selection/linkid.js');
  ga('require', 'displayfeatures');

// Optimizely Universal Analytics Integration
 window.optimizely = window.optimizely || [];
 window.optimizely.push("activateUniversalAnalytics");

  ga('send', 'pageview');

</script>
<script>
/**
* Function to track a click on an outbound link in Google Analytics.
*/
function trackOutboundLink(label, url) {
    if(window.ga && ga.loaded) {
        ga('send', 'event', 'outbound', 'click', label, {'hitCallback': function(){document.location = url;}
        });
    } else {
        document.location = url;
    }
}
/**
* Function to track a click on a link in the article announcement area.
*/
$('#articleAnnouncement a').on('click', function(){
  ga('send', 'event', 'link', 'click', 'articleAnnouncement');
});
</script>

<script type="text/javascript">
 setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2421.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



<!-- Hotjar Tracking Code for http://nngroup.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:121605,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

        <script src="https://media.nngroup.com/static/js/analytics/track_outbound_link.js"></script>
        
        
        
    </head>
    <body class="location-articles articles-detail">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
    <header id="page-header">
        <div id="cart" class="shopping-cart mobile-cart show-for-small-down"></div>
        <div class="row">
            <div id="masthead" class="masthead small-12 medium-7 column">

                <!-- The button to toggles menu on small screen -->

                <h1><a href="../../index.php"><img src="http://media.nngroup.com/static/img/logo-wordmark.png" width="74" height="30"> Nielsen <span class="norman">Norman</span> Group</a></h1>
                <p class="tagline show-for-medium-up">Evidence-Based User Experience Research, Training, and Consulting</p>

                <!-- The button to toggles form search on small screen -->


                <div style="clear: right;width:  100%;height: 1px;"></div><?php define('ROOT_PATH',$_SERVER["DOCUMENT_ROOT"] . '/apps/navigation_styles/navigations/5'); ?><?php include(ROOT_PATH)."/navigation.php"; ?>

                <!-- Drop down menu on small screen -->
                <div id="hideNav" class="show-for-small-down">
                  
                  <ul class="menu-content">
                    <li><a href="../../index.php">Home</a></li>
                    <li class="has-submenu">
                      <a>Training</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../training/index.php">Overview</a></li>
                        
<li id="nav-ux-conference"><a href="../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../online-seminars/index.php"><span>Online Seminars</span></a></li>

                      </ul>
                    </li>
                    <li class="has-submenu">
                      <a>Consulting</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
  <li><a href="../../consulting/index.php">Overview</a></li>
  
  <li><a href="../../consulting/benchmarking/index.php">Benchmarking</a></li>
  
  <li><a href="../../consulting/expert-design-review/index.php">Expert Design Review</a></li>
  
  <li><a href="../../consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
  
  <li><a href="../../consulting/usability-testing/index.php">Usability Testing </a></li>
  
  <li><a href="../../consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
  
  <li><a href="../../in-house-training/index.php">In-House Training</a></li>
</ul>

                    </li>

                    <li><a href="../../reports/index.php">Reports</a></li>
                    <li><a href="../index.php">Articles</a></li>
                    <li class="has-submenu"><a>About NN/G</a><i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../about/index.php">Overview</a></li>
                        <li><a href="../../people/index.php">People</a></li>
                        <li><a href="../../about/why-nng/index.php">Why NN/g?</a></li>
                        <li><a href="../../about/contact/index.php">Contact</a></li>
                        <li><a href="../../news/index.php">News</a></li>
                        <li><a href="../../about/history/index.php">History</a></li>
                        <li><a href="../../books/index.php">Books</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                </div>

                <hr class="header-divider show-for-small-only"/>


            </div>
       </div>

        <div id="hideNav">            <nav class="primary-nav small-12 column">
                <ul>
                    <li><a class="navitem" id="nav-home" href="../../index.php">Home</a></li>
                    <li><a class="navitem" id="nav-training" href="../../training/index.php">Training</a></li>
                    <li><a class="navitem" id="nav-consulting" href="../../consulting/index.php">Consulting</a></li>
                    <li><a class="navitem" id="nav-reports" href="../../reports/index.php">Reports</a></li>
                    <li><a class="navitem" id="nav-articles" href="../index.php">Articles</a></li>
                    <li><a class="navitem" id="nav-about" href="../../about/index.php">About NN/G</a></li>
                </ul>
            </nav>
            <nav class="secondary-nav small-12 medium-12 large-12 columns">
                
                
            </nav>
        </div>
    </header>
    <div class="l-content"><span id="preventClose" style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776; Menu</span>
    
  

  

  <div class="row">
    
      
      <section class="small-12 medium-3 columns collapsable-content l-navrail articles-left-column">
        <h1 class="show-for-small-down collapse-link mobile-browse-header">Browse by Topic and Author</h1>
          <div class="row collapsable">

            
            <div class="small-6 medium-12 columns">
              <section id="article-popular-topics" style="display: none;">
                
                <h1>Topics</h1>
                <ul id="article-popular-topics">
                    
                    <li>
                        
                        <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/intranets/all/index.php">Intranets</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/user-testing/all/index.php">User Testing</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                        
                    </li>
                    
                    <li><a href="#" id="article-see-all-topics"><strong>See all topics&hellip;</strong></a></li>
                </ul>
                
              </section>
              <section id="article-all-topics" style="display: none;">
                
                <h1>
                  <span class="show-for-medium-up">All Article Topics</span>
                  <span class="show-for-small-down">All Topics</span>
                  (<a href="#" id="article-hide-all-topics">hide</a>)
                </h1>
                <ul>
                  
                  <li>
                    
                    <a href="../../topic/accessibility/all/index.php">Accessibility</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/agile/all/index.php">Agile</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/analytics-and-metrics/all/index.php">Analytics &amp; Metrics</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/applications/all/index.php">Application Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/b2b-websites/all/index.php">B2B Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/behavior-patterns/all/index.php">Behavior Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/branding/all/index.php">Branding</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/content-strategy/all/index.php">Content Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/corporate-websites/all/index.php">Corporate Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/design-patterns/all/index.php">Design Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-design-process/all/index.php">Design Process</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/email/all/index.php">Email</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/eyetracking/all/index.php">Eyetracking</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/heuristic-evaluation/all/index.php">Heuristic Evaluation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/human-computer-interaction/all/index.php">Human Computer Interaction</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-ideation/all/index.php">Ideation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/information-architecture/all/index.php">Information Architecture</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/interaction-design/all/index.php">Interaction Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/international-users/all/index.php">International Users</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/intranets/all/index.php">Intranets</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-management/all/index.php">Management</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/navigation/all/index.php">Navigation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/non-profit-websites/all/index.php">Non-Profit Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/personas/all/index.php">Personas</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/persuasive-design/all/index.php">Persuasive Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/prototyping/all/index.php">Prototyping</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/psychology-and-ux/all/index.php">Psychology and UX</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/research-methods/all/index.php">Research Methods</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/search/all/index.php">Search</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/social-media/all/index.php">Social Media</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/strategy/all/index.php">Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/user-testing/all/index.php">User Testing</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-teams/all/index.php">UX Teams</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/visual-design/all/index.php">Visual Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/writing-web/all/index.php">Writing for the Web</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/young-users/all/index.php">Young Users</a>
                    
                  </li>
                  
                </ul>
                
              </section>
            </div>
            <script>
              /* Determine which topics list will be displayed: the full or the
               * partial.  We are inlining this close to the list itself to
               * prevent rendering delays on the client
               */
              var topic_list = $.cookie("article-topics");

              var popular_topics = $('#article-popular-topics');
              var all_topics = $('#article-all-topics');

              if(topic_list == 1) {
                  popular_topics.hide();
                  all_topics.show();
              } else {
                  popular_topics.show();
                  all_topics.hide();
              }
            </script>
            

            
            <div class="small-6 medium-12 columns">
              
              <section id="article-initial-authors">
                  <h1>Author</h1>
                  <ul>
                    
                    <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                    
                    <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                    
                    <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                    
                    <li><a href="#" onClick="$('#article-initial-authors').hide(); $('#article-all-authors').show();"><strong>See all authors&hellip;</strong></a></li>
                  </ul>
              </section>
              <section id="article-all-authors" style="display: none;">
                <h1>
                  All Authors
                  (<a href="#" id="article-hide-all-authors">hide</a>)
                </h1>
                <ul>
                  
                  <li><a href="../author/raluca-budiu/index.php">Raluca Budiu</a></li>
                  
                  <li><a href="../author/patty-caya/index.php">Patty Caya</a></li>
                  
                  <li><a href="../author/susan-farrell/index.php">Susan Farrell</a></li>
                  
                  <li><a href="../author/therese-fessenden/index.php">Therese Fessenden</a></li>
                  
                  <li><a href="../author/kim-flaherty/index.php">Kim Flaherty</a></li>
                  
                  <li><a href="../author/sarah-gibbons/index.php">Sarah Gibbons</a></li>
                  
                  <li><a href="../author/aurora-harley/index.php">Aurora Harley</a></li>
                  
                  <li><a href="../author/kate-kaplan/index.php">Kate Kaplan</a></li>
                  
                  <li><a href="../author/page-laubheimer/index.php">Page Laubheimer</a></li>
                  
                  <li><a href="../author/angela-li/index.php">Angie Li</a></li>
                  
                  <li><a href="../author/hoa-loranger/index.php">Hoa Loranger</a></li>
                  
                  <li><a href="../author/kate-meyer/index.php">Kate Meyer</a></li>
                  
                  <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                  
                  <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                  
                  <li><a href="../author/kara-pernice/index.php">Kara Pernice</a></li>
                  
                  <li><a href="../author/christian-rohrer/index.php">Christian Rohrer</a></li>
                  
                  <li><a href="../author/amy-schade/index.php">Amy Schade</a></li>
                  
                  <li><a href="../author/katie-sherwin/index.php">Katie Sherwin</a></li>
                  
                  <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                  
                  <li><a href="../author/kathryn-whitenton/index.php">Kathryn Whitenton</a></li>
                  
                </ul>
              </section>
              
            </div>
            
          </div>

          
          <section class="hide-for-small-down">
            
              
              <h1>Recent Articles</h1>
              
              <ul>
                  
                  <li><a href="../seamless-cross-channel/index.php">Seamlessness in the Omnichannel User Experience</a></li>
                  
                  <li><a href="../brand-intention-interpretation/index.php">Experience Design: Bridging Brand Intention and Brand Interpretation</a></li>
                  
                  <li><a href="../eyetracking-tasks-efficient-scanning/index.php">Scanning Patterns on the Web Are Optimized for the Current Task</a></li>
                  
                  <li><a href="../flat-design-best-practices/index.php">Flat-Design Best Practices</a></li>
                  
                  <li><a href="../ux-user-stories/index.php">Accounting for UX Work with User Stories in Agile Projects</a></li>
                  
                  <li><a href="../index.php"><strong>See all articles&hellip;</strong></a></li>
              </ul>
              
            
          </section>
          

          
          
          <section class="hide-for-small-down">
            <h1>Popular Articles</h1>
            
            <ul>
                
                <li><a href="../usability-101-introduction-to-usability/index.php">Usability 101: Introduction to Usability</a></li>
                
                <li><a href="../top-10-mistakes-web-design/index.php">Top 10 Mistakes in Web Design</a></li>
                
                <li><a href="../how-users-read-on-the-web/index.php">How Users Read on the Web</a></li>
                
                <li><a href="../f-shaped-pattern-reading-web-content/index.php">F-Shaped Pattern For Reading Web Content</a></li>
                
                <li><a href="../ten-usability-heuristics/index.php">10 Usability Heuristics for User Interface Design</a></li>
                
                <li><a href="../intranet-design-2016/index.php">10 Best Intranets of 2016</a></li>
                
                <li><a href="../which-ux-research-methods/index.php">When to Use Which User-Experience Research Methods</a></li>
                
                <li><a href="../response-times-3-important-limits/index.php">Response Times: The 3 Important Limits</a></li>
                
                <li><a href="../why-you-only-need-to-test-with-5-users/index.php">Why You Only Need to Test with 5 Users</a></li>
                
                <li><a href="../page-fold-manifesto/index.php">The Fold Manifesto: Why the Page Fold Still Matters</a></li>
                
            </ul>
          </section>
          
          

          
            <section class="hide-for-small-down">
              
            </section>
          

      </section>
      

      
        <div class="small-12 medium-9 columns content-body">
          




<article class="l-subsection alertbox-article-content article-detail-bl">
  
  <header>
    <div class="l-subsection">
      <h1>Radio Buttons: Select One by Default or Leave All Unselected?</h1>
      <div class="author-meta">by
        
          
            
            <a href="../author/kara-pernice/index.php">Kara Pernice</a>
            
          
        on  June 1, 2014
        
      </div>

      

      
      <div class="topic-links">
        Topics:
        
          <ul>

  <li><a href="../../topic/applications/index.php">Application Design</a></li>

</ul>
        
      </div>
      
    </div>
  </header>
  

  
  
  <section>
    <p class="lede"><strong>Summary:</strong> Select a single radio button by default in most cases. Reasons to deviate or not: expedite tasks, the power of suggestion, user expectations, safety nets.   </p>
    <br />
  </section>
  
  

  
  <section id="article-body"><h2>Introduction</h2>

<p>You are probably wondering how anyone could muster enough words for an entire article about the humble, ubiquitous radio button—and specifically, whether the designers should choose one of the options to be selected by default or not. Before you hit the <em>Back </em>button, think of the old adage "the devil is in the details," and that so many radio buttons today are downright devious. So read on to avoid creating a diabolical design.</p>

<p>First and foremost, a quick reminder of what radio buttons do: They display a set of mutually exclusive options from which a person may <strong>select exactly one</strong>. People sometimes <a href="../checkboxes-vs-radio-buttons/index.php">confuse radio buttons and checkboxes</a>, but checkboxes are the GUI widget to employ when users can select any number of the choices (zero, one, or many.)</p>

<p><img alt="4 radio buttons with option 1 selected, options 2,3, and 4 not selected" height="125" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2014/05/27/radio-wireframe.png" width="143"/></p>

<p><em>A radio button wireframe.</em></p>

<p>In traditional application design, the first radio button in the list was always selected by default. But the unbridled world of website design challenged this practice, making it fairly common to have no radio button selected by default. Today’s websites, forms, and applications inconsistently select one or leave all radio buttons blank by default. If none of the buttons is selected by default, users have no way to revert to this original state after they’ve made a selection. The lack of a standard can be confusing to users.</p>

<h2>Historical Metaphor</h2>

<p>The time of analog radios has long passed. The used Chevy Impala I drove (that is when the engine actually turned over) in the 1980’s boasted a radio with mechanical buttons, like the one in the image below.</p>

<p><img alt="mechanical radio with chrome buttons" height="358" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2014/05/27/radio_old-chevy.png" width="650"/></p>

<p><em>An example of a radio with mechanical buttons, found in older cars.</em></p>

<p>Using this and other radios of the age required physical strength to push in the buttons and change the station. You also needed a master’s degree to successfully “program” one of the buttons. (Extra credit: What are the steps to program one of these buttons to a radio station? Answer at the end of this article.)</p>

<p>The software radio button was modeled after these physical radio buttons. Skeuomorphism naysayers may baulk, but using metaphors that simulate the physical world can be helpful and engaging. However, in this particular case these antiquated radios are foreign to most of the web-user population today. This is an argument against matching this real-world metaphor.</p>

<p>A second reason for abandoning this unique metaphor is because the physical design itself was flawed. Namely, most people did not realize that it was possible to have no buttons selected, because there were no <a href="http://www.jnd.org/dn.mss/affordances_and.php">perceived affordance</a> signals. In typical usage, pressing a button would pop out (deselect) the already selected button. But you could also go back to the “none selected” state by pulling quite hard on the selected button until it came out to the deselected state. How would people know this? They had to be told, read it in the owner's manual, or just plain guess it was possible. (My sister Deb told me when we were kids, and I thought she was Einstein from then on.)</p>

<p>This original design defect continues to mar today’s websites, applications, and mobile designs. Basically, designers don't know which way is up (or in or out) with the rules about default radio button selection.</p>

<h2>Select a Radio Button by Default</h2>

<p>The main guideline for radio button design is to <strong>select one of the radio buttons by default </strong>(i.e., when the page loads, in the case of a web application.) This is good usability and elegant design. I say this not because I am waving a banner for traditionalism, or even for consistency. Rather the banners I’d parade in this case would be decorated with concepts such as the following:</p>

<ul>
	<li>user control,</li>
	<li>expediting tasks, and</li>
	<li>the power of suggestion.</li>
</ul>

<h2>1. Give People Control and Align with Their Expectations</h2>

<p>One of the <a href="../ten-usability-heuristics/index.php">10 heuristics of UI design</a> says that users should be able to undo (and redo) their actions. This means enabling people to set a UI control back to its original state. For most controls you may “un-choose” a selection you make. (Required elements may catch you with a note or error message later.) But you cannot click or tap a selected radio button to deselect it.</p>

<p>The finality of the action of selecting is not conveyed when none are selected by default. This interaction model is confusing and unexpected, and causes people to feel out of control. And it also instigates people to do crazy things, such as go back and possibly lose content, refresh the page, double- and triple-click (or tap) the radio button to deselect it, and act in other superstitious ways.</p>

<p>Selecting a radio button by default prevents these issues and communicates to the user from the start that he is required to choose one in the set.</p>

<h2>2. Expedite Tasks</h2>

<p>When a choice in a set of radio buttons is known to be the most desired or frequently selected one, it is very helpful to select this by default in the design. Doing this reduces the <a href="../interaction-cost-definition/index.php">interaction cost</a> and can save the user time and clicks.</p>

<p>For example, years ago I joined Care.com to find a dog sitter for my Cairn terrier, Columbo, when I traveled. After I found someone, I decided to unjoin the service. As I canceled my membership the site astutely presented a page with two radio buttons: one for unsubscribing and one for keeping my membership in case I needed backup care, or needed other types of care. (Perhaps my fig tree needed tending?)</p>

<p>The message the site presented--<em>Yes, switch my membership and save up to 83%</em>--may have sold some people on postponing canceling, as did placing the promotion in the first position in the radio-button list. But, understanding that most people really do want to unsubscribe if they have reached this particular point, the designers selected by default the radio button which reads <em>No thanks, continue downgrading my membership. </em>This default enabled me to just scan the choices and click the <em>Continue </em>button, rather than needing to click and select the <em>No,</em> and feeling annoyed by having to do that when I had just told the site I wanted to unsubscribe.</p>

<p><img alt="car.com message says you're almost done, but shows benefits of keeping the mebership" height="305" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2014/05/27/caredotcom_good-radio-selected.png" width="650"/></p>

<p><em>Care.com smartly made the radio button associated with canceling the membership the default selection.</em></p>

<p>On Massachusetts’ Department of Transportation site, the payment page for renewing a car registration offers payment methods: <em>Electronic Check </em>or <em>Credit Card,</em> but no default selection.</p>

<p><img alt="electronic check and credit card radio buttons are both blank" height="249" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2014/05/27/masdot_car-reg-renew.png" width="420"/></p>

<p><ema default="" em="" go="" make="" payment="" selection="" task="" the="" would=""></ema></p>

<p>A review of site <a href="../../courses/analytics-and-user-experience/index.php">metrics</a> and placed orders could quickly determine the most popular payment method, tipping designers off on which of these should be the default selection.</p>

<h2>3. Harness the Power of Suggestion</h2>

<p>Designs with a radio button selected by default make a strong suggestion—an endorsement even—to the user. The default choice may lead the user into making the best decision, and may increase his confidence as he proceeds. In particular, default radio selections can assist the person, and sway a person in the direction in which the organization wants him to go.</p>

<h3><strong>Assist the Person</strong></h3>

<p>Defaults help users. They are especially obliging in situations where the choices may be complex or unfamiliar. When the labels and descriptions are foreign, a default directs the user to the best choice among ones he may not understand. For example, one of Dropbox's install screens selects the radio button labelled <em>Typical </em>to help people get their install started. Additionally, the term <em>Advanced </em>juxtaposed against <em>Typical </em>makes the latter more attractive to less savvy users. And the words <em>recommended </em>and <em>normal </em>also instill confidence.</p>

<p><img alt="typical radio is selected, advanced is not" height="472" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2014/05/27/dropbox_setup_6_setup-type.png" width="500"/></p>

<p><em>An install dialog in Dropbox assists users by offering a default selection.</em></p>

<h3><strong>Sway the Person</strong></h3>

<p>It’s no secret that organizations have motives and often want to persuade their users to act in ways that are beneficial to them. <a href="../the-power-of-defaults/index.php">Default selections can coax people</a> down a particular path. A default selection may also cause a person to perceive the organization a particular way, or may affect his feeling of belonging.</p>

<p>Take the example of donating money to a cause—a sensitive topic for people who are self-conscious about the amount they plan to donate. Consider how a person might react to the amounts suggested by the radio buttons, and especially to the selected default. Let’s think of 2 short scenarios to guide us:</p>

<p><em><strong>Scenario 1: Person plans to give $30. He sees the radio buttons and:</strong></em></p>

<ul>
	<li>decides that $30 is less than what the organization wants, so he does not donate at all. This situation is negative to the organization and user.</li>
	<li>overextends himself to give the $50. The decision is positive to the organization (at least in the short term) and negative to the user.</li>
</ul>

<p><em><strong>Scenario 2: Person wants to donate $10,000. He sees the radio buttons and:</strong></em></p>

<ul>
	<li>decides that giving all $10,000 is exorbitant and decreases his donation to a much smaller amount. This selection is negative to the organization.</li>
	<li>follows the recommendation and gives $50 (and donates the rest of the $10K elsewhere.) Again, this decision is negative to the organization.</li>
</ul>

<p>Occasionally, users’ assumptions may be wrong or they may go against the organization’s goals or best interest. They may also make the user feel alienated if his intended choice is very different from the available or suggested choices. A selected amount could be presumptuous, off-putting to some, and may lower donation amounts of others. So, to make the best selection, <strong>be sure to know your organization’s goals and your users’ typical behavior.</strong></p>

<p>If you can’t confidently collect the information about your users, consider an alternative UI control (besides radio buttons with none selected.) Maybe do what the World Wildlife Fund does for their monthly donation section for the polar bears: an empty field with a suggested amount appearing after the field.</p>

<p><img alt="There is no default amount in a donation field" height="435" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2014/05/27/wwwf_polarbears_donation-empty-field.png" width="650"/></p>

<p><em>World Wildlife Fund allows people to type the amount they want to donate monthly, but makes a suggestion for the amount after the field.</em></p>

<p>In another example, the Environmental Defense Fund website suggests donation amounts between $25 and $5,000. Notwithstanding the default selection, the mere fact that there are proposed amounts sets the stage by telling users that the organization expects people to donate at least $25.</p>

<p><img alt="the $50 radio is selected, $25, $100, $500, $1,000, and $5,000 are blank" height="520" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2014/05/27/edf_radio-selected-by-default.png" width="398"/>               <img alt="Radio buttons wrap on the phone making it hard to tell which amount corresponds with which radio" height="520" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2014/05/27/edf_phone_default-select_cropped.png" width="180"/></p>

<p><em>The Environmental Defense Fund website sets $50 as the default donation, as seen in two <a href="../responsive-web-design-definition/index.php">responsive-design</a> versions on a laptop (left) and a phone (right.)</em></p>

<p>The radio button associated with $50 is selected by default. I don’t know exactly how EDF arrived at this default, but I can guess that they chose it to assists users, and that conceivably $50 is the most common donation amount. Or maybe the site is personalized or uses a cookie, and knows that $50 is what the person donated in the past.</p>

<p>But let’s assume for a moment that the motive is to sway people into <a href="../donation-usability/index.php">donating</a> $50 or more. In this design people see that it’s plausible that people donate $5,000; the $50 is <a href="../../courses/credibility-and-persuasive-web-design/index.php">trivial in comparison</a>. So the site is basically saying, “People give 5,000, but we’d be happy with $50.” This humility may even charm some donators to bestowing $100 or $500.</p>

<p><strong>Design Notes</strong></p>

<ul>
	<li>Horizontal radio buttons are sometimes difficult to scan. As seen from the EDF examples, the horizontal arrangement of the radio buttons can make it challenging to tell with which label the radio button corresponds: the one before the button or the one after. This problem is even more noticeable when this EDF responsive-design site is viewed on a phone. Vertical positioning of radio buttons is safer.</li>
	<li>Radio buttons are tiny in nature, and, thus, according to <a href="../../courses/hci/index.php">Fitts’ law</a>, they can be hard to click or tap. To enlarge the target area, let users select an option by clicking or tapping not just that button, but also the label or associated words. This is easier than having to acquire the tiny target of the button itself.</li>
</ul>

<h2>Overcome the Excuses</h2>

<p>Below I identify a few common defenses for leaving radios blank.</p>

<h3><strong>Not Knowing What Most Users Want or Do</strong></h3>

<p>It is important to study users when suggesting any choices, and it is possible to do this effectively with methods listed in the next section of this article.</p>

<h3><strong>Avoiding a Presumptuous or Estranging Default Selection</strong></h3>

<p>A radio button default selection that designers often struggle with is whether to select a gender or a designation such as Mr., Mrs., Ms., or Miss. Designers don't want to offend customers or risk addressing a person with the wrong designation, so they leave the radio buttons all blank by default. This doesn't solve the problem though, because even the order in which designations are presented may have an effect similar to that of a default selection. Better alternatives to no default selection include the following:</p>

<ul>
	<li>Study your user population to determine the designation most people fall under. Select it by default.</li>
	<li>Ensure that the radio buttons are visible so the person can approve or change the selection.</li>
	<li>If you already know who the user is (for example, via a role-based system,) select the correct designation.</li>
	<li>Offer an open field in which people can type the designation, and the website does the work on the backend to keep the database clean.</li>
	<li>Consider whether you need to ask for this information at all. Knowing the user’s designation does provide the opportunity to add a level of formality and respect to you future correspondences with the person. But if that gesture is not particularly important, you might take a cue form sites such as JohnDeereGifts.com and ask for the person’s name alone, with no designation.</li>
</ul>

<p><img alt="The form asks for first name and last name, but not mr. mrs. ms. miss, etcis" height="429" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2014/05/27/johndeeregifts_no-mr-mrs.png" width="520"/></p>

<p><em>JohnDeereGifts.com doesn’t ask for Mr., Mrs., Miss, Ms. designations at all.</em></p>

<h3><strong>Needing a Safety Net</strong></h3>

<p>Some designers fear that people won’t see a default selection option. Or an organization’s attorneys or federal agencies mandate that users actively click and accept a choice. In some of these situations designs offer radios with no default selection. The user may be unable to submit a form, for example, until he has selected a radio button, or if he can submit the form he will get an error message scolding him to make a selection.</p>

<p>This is an unimaginative design safety net. It is unpleasant to deal with, and is an ungraceful way to force attention. Consider other options, such as:</p>

<ul>
	<li>Use a linear step-by-step process that leads people though the possible choices.</li>
	<li>Design the page layout with adequate spacing, text, size, and color to direct users' attention to an important choice.</li>
	<li>Consider restructuring the content and offering a checkbox for confirming a choice.</li>
	<li>If simple enough, include the confirmation as part of the button name that proceeds or submits the page or dialog.</li>
</ul>

<h2>How to Determine the Best Default Radio-Button Choice</h2>

<p>Designers have many choices for UX research methods—rapid or involved, cheap or expensive, high or low confidence—to help them find the answer about what’s best for users. Start with these research methods to determine what a reasonable default may be:</p>

<ul>
	<li>track site <a href="../../courses/analytics-and-user-experience/index.php">metrics</a>, review past orders and selections (and consider personalization and cookies for repeat users)</li>
	<li><a href="../keep-online-surveys-short/index.php">survey</a> users</li>
	<li>conduct task analysis and <a href="../../consulting-field-studies/index.php">ethnographic studies</a></li>
	<li>refer to <a href="../persona/index.php">personas</a>, stories, and scenarios</li>
</ul>

<p>As for the business needs and desires, discuss with the powers that be what the business needs to excel, and direct people to those selections, overtly and scrupulously.</p>

<h2>Summary: Select One Radio Button by Default</h2>

<p>Design questions and choices used in radio buttons in such a way a default selection is helpful and makes a suggestion in a positive way. If you are considering selecting no radio button by default, think hard about your reasons for doing so.</p>

<p><strong>Reasons for A Default Selection</strong></p>

<ul>
	<li>Match the metaphor of the old radio (Bad): This is not a good reason because it is not necessary (or likely helpful) to match this particular metaphor.</li>
	<li>Expedite tasks (Good): Making a default selection is helpful if you have conducted task analysis and most users make the same choice.</li>
	<li>Give people control and align with their expectations (Good): It is better to have a selected radio button by default, given that people cannot deselect and set the button back to its original state once one has been selected. A default selection sets the correct user expectation.</li>
	<li>Power of suggestion (Good): Selecting is directive. Some things to watch out for though are being presumptuous, pushy, offensive, or alienating. Consider the content and the default selection, how users will react to it, and the effect the selection will have on the user and the organization in both the short and long term. Unless it’s been thoroughly tested (via observational methods as well as with metrics) and proven successful, avoid presumptuous default selections.</li>
</ul>

<p><strong>Excuses for No Default Selection</strong></p>

<ul>
	<li>Not knowing what users want or do: In this and any design situation, it behooves us to know our users and what they want and need, and do most often.</li>
	<li>Presumptuous or alienating choice: This is probably the most valid reason for not selecting a radio button by default. But consider whether you need to ask for the information at all, or whether you can use a different UI control to capture the data you need.</li>
	<li>Safety net: Catch people, ensure they see and actively select a choice: There is probably a more elegant way to design the interaction than to expect the user to miss a command and catch him with an error message after the fact. Seek an alternate method to direct the users’ attention.</li>
</ul>

<p>If you cannot confidently and contently provide a default selection in the list of radio buttons, consider using an alternate UI control that accommodates the particular situation and its goals and constraints.</p>

<h2>Extra Credit Answer</h2>

<p>Do you give up about how to “program” a station with those old-school, mechanical buttons? Here’s how to do it. Use the tuner dial to get to the station you want then locate the button to “program” the station to. Now pull that button OUT, past the "not selected" state. Pull it hard as though you are really trying to break the radio. Then push it back all the way in so it is selected. That works, usually. To test it, change the station, then press the button again and see if it tunes to the station you wanted it to. If it works, relax with nice bit of Neil Diamond. If it doesn't work then repeat, better.</p>

<p><strong>For more about radio buttons and other GUI widgets, also see our <a href="../../courses/application-ux/index.php">Application Design for Web and Desktop</a> course.</strong></p>
</section>
  

  
  <section>
    
        
    
  </section>
  
  
  
  
  

  


  <section>
    <br />
    <p class="share"><b>Share <span class="hide-for-small-down">this article</span>:</b> <a href="https://twitter.com/intent/tweet?url=http://www.nngroup.com/articles/radio-buttons-default-selection/&amp;text=Radio%20Buttons:%20Select%20One%20by%20Default%20or%20Leave%20All%20Unselected?&amp;via=nngroup">Twitter</a> | <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=http://www.nngroup.com/articles/radio-buttons-default-selection/&amp;title=Radio%20Buttons:%20Select%20One%20by%20Default%20or%20Leave%20All%20Unselected?&amp;source=Nielsen%20Norman%20Group">LinkedIn</a> | <a href="https://plus.google.com/share?url=http://www.nngroup.com/articles/radio-buttons-default-selection/">Google+</a> | <a href="mailto:?subject=NN/g Article: Radio Buttons: Select One by Default or Leave All Unselected?&amp;body=http://www.nngroup.com/articles/radio-buttons-default-selection/">Email</a></p>
    <hr />
  </section>

  <footer class="l-subsection">
    
    
    <div class="learn-more">
      <h2>Learn More</h2>
      <p><a href="../subscribe/index.php">Subscribe to the weekly newsletter</a> to get notified about future articles.</p>
      </div>
    
    

    <div class="row show-for-small-down related-content">
      
      
      

      
      
        <div class="column small-12 topic-links">All articles about:
          <ul>
            
            <li><a href="../../topic/applications/index.php">Application Design</a></li>
            
            <li><a href="../../topic/forms/index.php">forms</a></li>
            
          </ul>
        </div>
      
      

      
      
        <div class="column small-12">
          <h3>UX Conference Training Courses</h3>
           <ul class="no-bullet">
    
        <li><a href="../../courses/hci/index.php">User Interface Principles Every Designer Must Know</a></li>
    
        <li><a href="../../courses/application-ux/index.php">Application Design for Web and Desktop</a></li>
    
        <li><a href="../../courses/emotional-design/index.php">Emotional Design</a></li>
    
</ul>
        </div>
      
      

      
      
        <div class="small-12 column">
          <h3>Research Reports</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../reports/best-applications-2/index.php">Application Design Showcase: 2012</a></li>
              
            
              
                <li><a href="../../reports/enterprise-mobile-showcase/index.php">Mobile Intranets and Enterprise Apps</a></li>
              
            
              
                <li><a href="../../reports/website-tools-and-applications-flash/index.php">Website Tools and Applications with Flash</a></li>
              
            
              
                <li><a href="../../reports/customization-features/index.php">Customization Features Done Correctly for the Right Reasons</a></li>
              
            
              
                <li><a href="../../reports/best-applications-1/index.php">Application Design Showcase: 2008</a></li>
              
            
          </ul>
        </div>
      
      

      
      
        <div class="small-12 column">
          <h3>Online Seminars</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../online-seminars/ux-big-data/index.php">UX for Big Data</a></li>
              
            
          </ul>
        </div>
      
      
    </div>

    <div class="hide-for-small-down row related-content">
      <div class="medium-6 columns">
        
        
          <div>
            <h3>Articles</h3>
            <ul class="no-bullet">
              
                
                <li><a href="../tabs-used-right/index.php">Tabs, Used Right</a></li>
                
              
                
                <li><a href="../forms-vs-applications/index.php">Forms vs. Applications</a></li>
                
              
                
                <li><a href="../indicators-validations-notifications/index.php">Indicators, Validations, and Notifications: Pick the Correct Communication Option</a></li>
                
              
                
                <li><a href="../passwords-memory/index.php">Help People Create Passwords That They Can Actually Remember</a></li>
                
              
                
                <li><a href="../mobile-input-checklist/index.php">A Checklist for Designing Mobile Input Fields</a></li>
                
              
            </ul>
          </div>
        
        

        
          
          <div>
            <h3>Research Reports</h3>
            <ul class="no-bullet">
              
                
                  <li><a href="../../reports/best-applications-2/index.php">Application Design Showcase: 2012</a></li>
                
              
                
                  <li><a href="../../reports/enterprise-mobile-showcase/index.php">Mobile Intranets and Enterprise Apps</a></li>
                
              
                
                  <li><a href="../../reports/website-tools-and-applications-flash/index.php">Website Tools and Applications with Flash</a></li>
                
              
                
                  <li><a href="../../reports/customization-features/index.php">Customization Features Done Correctly for the Right Reasons</a></li>
                
              
                
                  <li><a href="../../reports/best-applications-1/index.php">Application Design Showcase: 2008</a></li>
                
              
            </ul>
          </div>
          
        

      </div>
      <div class="medium-6 columns">
        
          
          <div>
            <h3>UX Conference Training Courses</h3>
             <ul class="no-bullet">
    
        <li><a href="../../courses/hci/index.php">User Interface Principles Every Designer Must Know</a></li>
    
        <li><a href="../../courses/application-ux/index.php">Application Design for Web and Desktop</a></li>
    
        <li><a href="../../courses/emotional-design/index.php">Emotional Design</a></li>
    
</ul>
          </div>
          
        
        
          
          <div>
            <h3>Online Seminars</h3>
            <ul class="no-bullet">
            
                <li><a href="../../online-seminars/ux-big-data/index.php">UX for Big Data</a></li>
            
            </ul>
          </div>
          
        
      </div>
    </div>
  </footer>
</article>

        </div>

      

    
  </div>
  <div class="row">
    
      <div class="column small-12 show-for-small-down l-navrail announcements">
        
          
        
      </div>
    
    
  </div>

    </div>
    




<footer id="page-footer">
    <div class="row hide-for-print">
        <div class="column small-12 medium-6">
            <section class="footer-popular">
                
                <h1>Popular Topics</h1>
                <div style="float: left;  width: 15em; display: block;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/applications/index.php">Application Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/e-commerce/index.php">E-Commerce Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/eyetracking/index.php">Eyetracking </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/information-architecture/index.php">Information Architecture </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/intranets/index.php">Intranet Design </a></li>
	<li><a href="../../topic/interaction-design/index.php">Interaction Design </a></li>
</ul>
</div>

<div style="float: left;  width: 15em; display: block; list-style: none;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/mobile-and-tablet-design/index.php">Mobile and Tablet Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/credibility-persuasion/index.php">Persuasive Design</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/research-methods/index.php">Research Methods</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/user-testing/index.php">User Testing </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/web-usability/index.php">Web Usability </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/writing-web/index.php">Web Writing &amp; Content Strategy </a></li>
</ul>
</div>

<p>&nbsp;</p>

                
            </section>
        </div>

        <div class="column small-12 medium-6">
            <div class="row">
                <div class="column small-6">
                    <section class="footer-about">
                        <h1>Follow Us</h1>
                        <ul>
	<li><a href="../subscribe/index.php">Subscribe to our weekly UX newsletter</a>,&nbsp;Jakob Nielsen&#39;s Alertbox</li>
	<li>Visit Don Norman&#39;s website, <a href="http://www.jnd.org/"> www.jnd.org </a></li>
	<li>Visit Bruce Tognazzini&#39;s website, <a href="http://www.asktog.com/"> www.asktog.com </a></li>
	<li style="padding-top: 6px;"><a class="twitter-follow-button" data-show-count="false" data-size="large" href="https://twitter.com/nngroup"><img alt="Follow @nngroup on Twitter" height="20" src="https://media.nngroup.com/media/editor/2016/12/30/twitter-follow-button.png" style="width: 117px; height: 20px;" width="117" /></a></li>
	<li style="padding-top: 6px;"><img alt="" height="14" src="https://media.nngroup.com/media/editor/2016/07/26/feed-icon-14x14.png" style="vertical-align:text-top;width:14px;margin-right:.25em;" width="14" /><a href="../../feed/rss/index.php">Subscribe to our RSS feed</a></li>
</ul>

                    </section>
                </div>

                <div class="column small-6">
                    <section class="footer-about right-column">
                        <h1>About
</h1>
                        <ul class="no-bullet">
	<li><a href="../../about/contact/index.php">Contact information</a></li>
	<li><a href="../../about/index.php">About Us</a></li>
	<li><a href="../../about/why-nng/index.php">Why NN/g? </a></li>
	<li><a href="../../return-policy/index.php">Return Policy</a></li>
</ul>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="copyright column small-12">
            <p><a href="../../copyright-and-reprint-info/index.php">Copyright</a> &copy; 1998-2017 Nielsen Norman Group, All Rights Reserved.</p>
        </div>
    </div>
</footer>


    


        <script src="https://media.nngroup.com/static/vendor/fastclick/js/fastclick.min.js"></script>
        <script src="https://media.nngroup.com/static/vendor/foundation/js/foundation.min.js"></script>
        <script src="http://media.nngroup.com/static/js/app.js"></script>
        <script src="http://media.nngroup.com/static/js/scroll-to-fixed.js"></script><script>var externalLinks = document.querySelectorAll('a[href^="http"]');for (var i = externalLinks.length-1; i >= 0; i--) {externalLinks[i].addEventListener("click", function(event) { alert("External links are disabled for this test."); event.preventDefault(); }, false);}var mailLinks = document.querySelectorAll('a[href^="mailto"]');for (var i = mailLinks.length-1; i >= 0; i--) {mailLinks[i].addEventListener("click", function(event) { alert("Email links are disabled for this test."); event.preventDefault(); }, false);}var externalForms = document.querySelectorAll('form');for (var i = externalForms.length-1; i >= 0; i--) {externalForms[i].addEventListener("submit", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false); externalForms[i].addEventListener("click", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false);}</script>

        <script>
            
            $(function() {
                $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
                $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
            });
        </script>

        
<script src="https://media.nngroup.com/static/js/articles.js"></script>


        
        <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1072699960;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072699960/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
    </body>

<!-- Mirrored from www.nngroup.com/articles/radio-buttons-default-selection/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:06:27 GMT -->
</html>
