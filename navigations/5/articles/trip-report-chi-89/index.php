<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->





    
<!-- Mirrored from www.nngroup.com/articles/trip-report-chi-89/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:14:36 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1026.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"licenseKey":"1a30ef9931","queueTime":0,"beacon":"bam.nr-data.net","errorBeacon":"bam.nr-data.net","applicationID":"2048992","transactionName":"NFxSMRBUX0EDUkIPDA0WdhAMVkVbDV8ZBxEXUFMJB0YfRAtUQRVZIktEDAFZVHYHRVcPD01eVRE=","applicationTime":470,"agent":""}</script>
        <title>CHI&#39;89 Trip Report: Article by Jakob Nielsen</title><meta property="og:title" content="CHI&#39;89 Trip Report: Article by Jakob Nielsen" />
  
        
        <meta name="description" content="Jakob Nielsen&#39;s trip report from the 1989 Computer-Human Interaction (CHI &#39;89) conference held in Austin, Texas.">
        <meta property="og:description" content="Jakob Nielsen&#39;s trip report from the 1989 Computer-Human Interaction (CHI &#39;89) conference held in Austin, Texas." />
        
  
        
	
        

        

        <meta name="robots" content="noarchive">
        <meta name="viewport" content="width=device-width">

        <link rel="search" type="application/opensearchdescription+xml" title="NN/g Search" href="../../opensearch.xml" />
        <link rel="shortcut icon" href="http://media.nngroup.com/static/img/favicon.ico" />

        <link rel="stylesheet" href="https://media.nngroup.com/static/vendor/font-awesome/css/font-awesome.min.css">

        
        <link rel="stylesheet" href="https://media.nngroup.com/static/css/7232a960f1df.css" type="text/css" />
        <link rel="stylesheet" href="http://fast.fonts.com/t/1.css?apiType=css&amp;projectid=c5846260-9def-46ec-9407-0f1349bd2d64" type="text/css">

        <script src="https://media.nngroup.com/static/vendor/modernizr/js/modernizr.min.js"></script>

        <script type="text/javascript" src="https://media.nngroup.com/static/js/3283be5def74.js"></script>
        

        

        
        
        <!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=229034,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('http://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->


<!-- Google Analytics Tracking Code for http://nngroup.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23132681-1', 'nngroup.com');
  ga('require', 'linkid', 'https://www.nngroup.com/articles/trip-report-chi-89/linkid.js');
  ga('require', 'displayfeatures');

// Optimizely Universal Analytics Integration
 window.optimizely = window.optimizely || [];
 window.optimizely.push("activateUniversalAnalytics");

  ga('send', 'pageview');

</script>
<script>
/**
* Function to track a click on an outbound link in Google Analytics.
*/
function trackOutboundLink(label, url) {
    if(window.ga && ga.loaded) {
        ga('send', 'event', 'outbound', 'click', label, {'hitCallback': function(){document.location = url;}
        });
    } else {
        document.location = url;
    }
}
/**
* Function to track a click on a link in the article announcement area.
*/
$('#articleAnnouncement a').on('click', function(){
  ga('send', 'event', 'link', 'click', 'articleAnnouncement');
});
</script>

<script type="text/javascript">
 setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2421.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



<!-- Hotjar Tracking Code for http://nngroup.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:121605,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

        <script src="https://media.nngroup.com/static/js/analytics/track_outbound_link.js"></script>
        
        
        
    </head>
    <body class="location-articles articles-detail">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
    <header id="page-header">
        <div id="cart" class="shopping-cart mobile-cart show-for-small-down"></div>
        <div class="row">
            <div id="masthead" class="masthead small-12 medium-7 column">

                <!-- The button to toggles menu on small screen -->

                <h1><a href="../../index.php"><img src="http://media.nngroup.com/static/img/logo-wordmark.png" width="74" height="30"> Nielsen <span class="norman">Norman</span> Group</a></h1>
                <p class="tagline show-for-medium-up">Evidence-Based User Experience Research, Training, and Consulting</p>

                <!-- The button to toggles form search on small screen -->


                <div style="clear: right;width:  100%;height: 1px;"></div><?php define('ROOT_PATH',$_SERVER["DOCUMENT_ROOT"] . '/apps/navigation_styles/navigations/5'); ?><?php include(ROOT_PATH)."/navigation.php"; ?>

                <!-- Drop down menu on small screen -->
                <div id="hideNav" class="show-for-small-down">
                  
                  <ul class="menu-content">
                    <li><a href="../../index.php">Home</a></li>
                    <li class="has-submenu">
                      <a>Training</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../training/index.php">Overview</a></li>
                        
<li id="nav-ux-conference"><a href="../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../online-seminars/index.php"><span>Online Seminars</span></a></li>

                      </ul>
                    </li>
                    <li class="has-submenu">
                      <a>Consulting</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
  <li><a href="../../consulting/index.php">Overview</a></li>
  
  <li><a href="../../consulting/benchmarking/index.php">Benchmarking</a></li>
  
  <li><a href="../../consulting/expert-design-review/index.php">Expert Design Review</a></li>
  
  <li><a href="../../consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
  
  <li><a href="../../consulting/usability-testing/index.php">Usability Testing </a></li>
  
  <li><a href="../../consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
  
  <li><a href="../../in-house-training/index.php">In-House Training</a></li>
</ul>

                    </li>

                    <li><a href="../../reports/index.php">Reports</a></li>
                    <li><a href="../index.php">Articles</a></li>
                    <li class="has-submenu"><a>About NN/G</a><i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../about/index.php">Overview</a></li>
                        <li><a href="../../people/index.php">People</a></li>
                        <li><a href="../../about/why-nng/index.php">Why NN/g?</a></li>
                        <li><a href="../../about/contact/index.php">Contact</a></li>
                        <li><a href="../../news/index.php">News</a></li>
                        <li><a href="../../about/history/index.php">History</a></li>
                        <li><a href="../../books/index.php">Books</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                </div>

                <hr class="header-divider show-for-small-only"/>


            </div>
       </div>

        <div id="hideNav">            <nav class="primary-nav small-12 column">
                <ul>
                    <li><a class="navitem" id="nav-home" href="../../index.php">Home</a></li>
                    <li><a class="navitem" id="nav-training" href="../../training/index.php">Training</a></li>
                    <li><a class="navitem" id="nav-consulting" href="../../consulting/index.php">Consulting</a></li>
                    <li><a class="navitem" id="nav-reports" href="../../reports/index.php">Reports</a></li>
                    <li><a class="navitem" id="nav-articles" href="../index.php">Articles</a></li>
                    <li><a class="navitem" id="nav-about" href="../../about/index.php">About NN/G</a></li>
                </ul>
            </nav>
            <nav class="secondary-nav small-12 medium-12 large-12 columns">
                
                
            </nav>
        </div>
    </header>
    <div class="l-content"><span id="preventClose" style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776; Menu</span>
    
  

  

  <div class="row">
    
      
      <section class="small-12 medium-3 columns collapsable-content l-navrail articles-left-column">
        <h1 class="show-for-small-down collapse-link mobile-browse-header">Browse by Topic and Author</h1>
          <div class="row collapsable">

            
            <div class="small-6 medium-12 columns">
              <section id="article-popular-topics" style="display: none;">
                
                <h1>Topics</h1>
                <ul id="article-popular-topics">
                    
                    <li>
                        
                        <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/intranets/all/index.php">Intranets</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/user-testing/all/index.php">User Testing</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                        
                    </li>
                    
                    <li><a href="#" id="article-see-all-topics"><strong>See all topics&hellip;</strong></a></li>
                </ul>
                
              </section>
              <section id="article-all-topics" style="display: none;">
                
                <h1>
                  <span class="show-for-medium-up">All Article Topics</span>
                  <span class="show-for-small-down">All Topics</span>
                  (<a href="#" id="article-hide-all-topics">hide</a>)
                </h1>
                <ul>
                  
                  <li>
                    
                    <a href="../../topic/accessibility/all/index.php">Accessibility</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/agile/all/index.php">Agile</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/analytics-and-metrics/all/index.php">Analytics &amp; Metrics</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/applications/all/index.php">Application Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/b2b-websites/all/index.php">B2B Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/behavior-patterns/all/index.php">Behavior Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/branding/all/index.php">Branding</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/content-strategy/all/index.php">Content Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/corporate-websites/all/index.php">Corporate Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/design-patterns/all/index.php">Design Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-design-process/all/index.php">Design Process</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/email/all/index.php">Email</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/eyetracking/all/index.php">Eyetracking</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/heuristic-evaluation/all/index.php">Heuristic Evaluation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/human-computer-interaction/all/index.php">Human Computer Interaction</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-ideation/all/index.php">Ideation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/information-architecture/all/index.php">Information Architecture</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/interaction-design/all/index.php">Interaction Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/international-users/all/index.php">International Users</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/intranets/all/index.php">Intranets</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-management/all/index.php">Management</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/navigation/all/index.php">Navigation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/non-profit-websites/all/index.php">Non-Profit Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/personas/all/index.php">Personas</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/persuasive-design/all/index.php">Persuasive Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/prototyping/all/index.php">Prototyping</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/psychology-and-ux/all/index.php">Psychology and UX</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/research-methods/all/index.php">Research Methods</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/search/all/index.php">Search</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/social-media/all/index.php">Social Media</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/strategy/all/index.php">Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/user-testing/all/index.php">User Testing</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-teams/all/index.php">UX Teams</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/visual-design/all/index.php">Visual Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/writing-web/all/index.php">Writing for the Web</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/young-users/all/index.php">Young Users</a>
                    
                  </li>
                  
                </ul>
                
              </section>
            </div>
            <script>
              /* Determine which topics list will be displayed: the full or the
               * partial.  We are inlining this close to the list itself to
               * prevent rendering delays on the client
               */
              var topic_list = $.cookie("article-topics");

              var popular_topics = $('#article-popular-topics');
              var all_topics = $('#article-all-topics');

              if(topic_list == 1) {
                  popular_topics.hide();
                  all_topics.show();
              } else {
                  popular_topics.show();
                  all_topics.hide();
              }
            </script>
            

            
            <div class="small-6 medium-12 columns">
              
              <section id="article-initial-authors">
                  <h1>Author</h1>
                  <ul>
                    
                    <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                    
                    <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                    
                    <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                    
                    <li><a href="#" onClick="$('#article-initial-authors').hide(); $('#article-all-authors').show();"><strong>See all authors&hellip;</strong></a></li>
                  </ul>
              </section>
              <section id="article-all-authors" style="display: none;">
                <h1>
                  All Authors
                  (<a href="#" id="article-hide-all-authors">hide</a>)
                </h1>
                <ul>
                  
                  <li><a href="../author/raluca-budiu/index.php">Raluca Budiu</a></li>
                  
                  <li><a href="../author/patty-caya/index.php">Patty Caya</a></li>
                  
                  <li><a href="../author/susan-farrell/index.php">Susan Farrell</a></li>
                  
                  <li><a href="../author/therese-fessenden/index.php">Therese Fessenden</a></li>
                  
                  <li><a href="../author/kim-flaherty/index.php">Kim Flaherty</a></li>
                  
                  <li><a href="../author/sarah-gibbons/index.php">Sarah Gibbons</a></li>
                  
                  <li><a href="../author/aurora-harley/index.php">Aurora Harley</a></li>
                  
                  <li><a href="../author/kate-kaplan/index.php">Kate Kaplan</a></li>
                  
                  <li><a href="../author/page-laubheimer/index.php">Page Laubheimer</a></li>
                  
                  <li><a href="../author/angela-li/index.php">Angie Li</a></li>
                  
                  <li><a href="../author/hoa-loranger/index.php">Hoa Loranger</a></li>
                  
                  <li><a href="../author/kate-meyer/index.php">Kate Meyer</a></li>
                  
                  <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                  
                  <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                  
                  <li><a href="../author/kara-pernice/index.php">Kara Pernice</a></li>
                  
                  <li><a href="../author/christian-rohrer/index.php">Christian Rohrer</a></li>
                  
                  <li><a href="../author/amy-schade/index.php">Amy Schade</a></li>
                  
                  <li><a href="../author/katie-sherwin/index.php">Katie Sherwin</a></li>
                  
                  <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                  
                  <li><a href="../author/kathryn-whitenton/index.php">Kathryn Whitenton</a></li>
                  
                </ul>
              </section>
              
            </div>
            
          </div>

          
          <section class="hide-for-small-down">
            
              
              <h1>Recent Articles</h1>
              
              <ul>
                  
                  <li><a href="../seamless-cross-channel/index.php">Seamlessness in the Omnichannel User Experience</a></li>
                  
                  <li><a href="../brand-intention-interpretation/index.php">Experience Design: Bridging Brand Intention and Brand Interpretation</a></li>
                  
                  <li><a href="../eyetracking-tasks-efficient-scanning/index.php">Scanning Patterns on the Web Are Optimized for the Current Task</a></li>
                  
                  <li><a href="../flat-design-best-practices/index.php">Flat-Design Best Practices</a></li>
                  
                  <li><a href="../ux-user-stories/index.php">Accounting for UX Work with User Stories in Agile Projects</a></li>
                  
                  <li><a href="../index.php"><strong>See all articles&hellip;</strong></a></li>
              </ul>
              
            
          </section>
          

          
          
          <section class="hide-for-small-down">
            <h1>Popular Articles</h1>
            
            <ul>
                
                <li><a href="../usability-101-introduction-to-usability/index.php">Usability 101: Introduction to Usability</a></li>
                
                <li><a href="../top-10-mistakes-web-design/index.php">Top 10 Mistakes in Web Design</a></li>
                
                <li><a href="../how-users-read-on-the-web/index.php">How Users Read on the Web</a></li>
                
                <li><a href="../f-shaped-pattern-reading-web-content/index.php">F-Shaped Pattern For Reading Web Content</a></li>
                
                <li><a href="../ten-usability-heuristics/index.php">10 Usability Heuristics for User Interface Design</a></li>
                
                <li><a href="../intranet-design-2016/index.php">10 Best Intranets of 2016</a></li>
                
                <li><a href="../which-ux-research-methods/index.php">When to Use Which User-Experience Research Methods</a></li>
                
                <li><a href="../response-times-3-important-limits/index.php">Response Times: The 3 Important Limits</a></li>
                
                <li><a href="../why-you-only-need-to-test-with-5-users/index.php">Why You Only Need to Test with 5 Users</a></li>
                
                <li><a href="../page-fold-manifesto/index.php">The Fold Manifesto: Why the Page Fold Still Matters</a></li>
                
            </ul>
          </section>
          
          

          
            <section class="hide-for-small-down">
              
            </section>
          

      </section>
      

      
        <div class="small-12 medium-9 columns content-body">
          




<article class="l-subsection alertbox-article-content article-detail-bl">
  
  <header>
    <div class="l-subsection">
      <h1>CHI&#39;89 Trip Report</h1>
      <div class="author-meta">by
        
          
            
            <a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a>
            
          
        on  June 1, 1989
        
      </div>

      

      
      <div class="topic-links">
        Topics:
        
          <ul>

  <li><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction</a></li>

</ul>
        
      </div>
      
    </div>
  </header>
  

  
  
  

  
  <section id="article-body"><p><em>Austin, TX, 30 April-4 May 1989. </em></p>

<p>The excitement is back! Even though plenty of interesting things happened at <a href="../trip-report-chi-88/index.php"> CHI'88</a>, there is no denying that it had a little of a dull feel to it as if the user interface field had slowed down somewhat. Well, CHI'89 certainly had no such problem.</p>

<p>Some of the credit probably goes to the conference chair, <strong> Bill Curtis </strong> himself, who was good at whipping up a good Texan cheer, even though his plan to give every conference participant a cowboy hat failed because the 2000 hats he had delivered turned out to be so poor plastic imitations that handing them out would have been no fun. Another part of the credit is due to pure random coincidence, but there is still some excitement credit left over. It does seem to me that the user interface field is speeding up again after a short period of me-too interfaces. As an example, two nice videos showed polished ways to use handwriting recognition to achieve smooth interfaces. Sure, we have seen handwriting recognition before but mostly as an isolated technology for getting text into the computer. The "<strong>paper-like interface</strong>" by Cathy Wolf et al. from IBM Yorktown Heights showed a very nice integration of the recognition technology with a formula editor where users' performance could probably be improved by an order of magnitude compared with non-gestural ways of entering formulas. Mark Rosenstein from MCC had a way of using handdrawn sketches of proposed user interface designs together with other tools in the HITS Human Interface Tool Set. He called this type of interactive worksurface "silicon paper" but the really interesting point was again the integration of the gestural interaction with other aspects of the interface.</p>

<h2>The Great Copyright Debate</h2>

<p>One of the reasons people are now designing new user interfaces is that various copyright suits have scared them away from just copying the Macintosh user interface. It is quite new for most user interface professionals to consider the legal implications of our work but it certainly seems to be necessary to do so. To present these issues, Pamela Samuelson from the University of Pittsburgh School of Law had organized a panel debate on the <strong> copyright of user interface "look and feel" </strong> with two lawyers to present the opposing views.</p>

<p>There are really two questions in the copyright debate: One is whether user interfaces are copyrightable under the current law. And the second is whether they ought to be copyrightable. The first question can be determined empirically in each legal jurisdiction by looking at the outcomes of various lawsuits. One can also argue the cases using levels of legal sophistication varying from the "pocket lawyers" on the Usenet to the quite complicated theories constructed by real lawyers and law school researchers. In the long term it is the second, more political question which is of interest, however, since laws can be changed by acts of Congress and Parliaments around the world. Interestingly, most of the arguments from the two lawyers in the panel were really of a political nature and looked at what copyright principle would lead to the best user interfaces in the future.</p>

<p>Regarding the legal arguments about the meaning of the current law, it seems to me from reading Pamela Samuelson's excellent review of US court decisions in the May 1989 <cite> Communications of the ACM </cite> that most of these decisions may have had the right outcome but for the wrong reasons. For example, in one case two programs had the same command names which in both cases had the first two characters capitalized (for use as shortcuts). The names themselves could apparently not be copyrighted but having the second program display these names with the same letters capitalized was ruled to be a copyright infringement because there is a large number of ways to capitalize letters. Since the second program capitalized the same letters as the first, it was judged to have copied its screen design. From a usability perspective, however, it seems obvious that one cannot just capitalize a random selection of letters in a command if one wants users to have a chance to remember the shortcuts. Given the basic principle of using abbreviations of listed command names as shortcuts, the designer actually has very little freedom to select the capitalization method, though of course there is the possibility of using vowel deletion as an alternative abbreviation method to truncation.</p>

<p>Many user interface specialists would probably have testified as expert witnesses that such examples of user interface design are not just based on stylistic decisions but are founded in real human factors issues with impact on the products' utility which apparently is not copyrightable. It could be that one would want to protect specific solutions to these usability issues such as e.g. having a certain set of command names or even the idea of using abbreviations as shortcuts. That would be a fair way to reward people who invent good new dialogue techniques.</p>

<p>The meaning of the current law is certainly murky and it was therefore quite nice for the CHI audience that the lawyers in the panel debate did not spend too much time on that. Jack E. Brown was the lawyer in favor of strong copyright protection which he felt would foster a creative environment for user interface development since people would want to come up with new ways of doing things. In quoting Bruce Tognazzini, Brown said that fixed standards would seriously impede innovation and he preferred having incentives for change. When people could not just copy previous work, they would have to come up with something new and thus move the state of the art ahead. He felt that there was so much work involved in creating an entire user interface that it should not be allowed to just rip it off. Also, one could not just limit copyright protection to 100% copies since that would encourage evasion but on the other hand it should be no problem to reuse a single icon. In a comment to this, Annette Wegner from Apple said that she certainly did want protection for her icon designs just as people get copyright when they create logos.</p>

<p>Thomas M.S. Hemnes presented the liberal view and also argued that his approach would lead to the best user interfaces. He felt that strict copyright protection would lead to more slow advances in user interfaces since user interface design is an <strong> accumulative technology </strong> where it should be possible to build upon previous advances and refine them. (An example of a non-accumulative technology is the pharmaceutical industry where a drug which works for one thing will generally have nothing to do with a drug designed for another thing.) As an example of another accumulative technology, Hemnes referred to the aircraft industry where the only way to build a state-of-the-art aircraft in the US during the First World War was to have the government impose required cross-licensing between the various inventors who each held patents on crucial aspects of flight technology. Hemnes also claimed that only 1% of the value was coming up with the new idea and that the 99% was in implementing it. I certainly agree that duplicating modern interaction techniques take a lot of work, but I do have more respect than that for the hard work of getting the idea in the first place which often requires extensive amounts of research and hair pulling. Because copyright infringement is proved on the basis of both access to the previous work and a substantial similarity, Hemnes feared that either new user interface designs would have to be made by people without knowledge of the field or that all designs would be made as different as possible, leading to risk of user errors. Finally, Hemnes was opposed to the use of copyright for user interfaces since it extends over the lifetime of the author plus an additional 50 years which is really the same as "forever" in the computer business.</p>

<p>In a rebuttal to this last statement, Brown said that CP/M was once thought to be the only good way to use personal computers but now just a few years later it has been passed by by innovation. So a user interface is not forever. Nobody will get a lifetime monopoly on making usable computers just because they get the copyright to a certain dialogue technique.</p>

<p>As an example of negative impact of copyright, Mike Lesk from Bellcore who was the panel discussant, referred to the copyright on typefaces in Europe. This had made it difficult to produce laser printers with the result that now the laser printer business is in the United States where fonts cannot be copyrighted. In an even more general analogy, Lesk said that the publishing business would not be very popular is every company had to print different sizes of books.</p>

<h2>The Specter of Standards</h2>

<p>At the same time as everybody was discussing whether or not they will be permitted to use the famous trash can icon, a sub-committee under the International Standards Organization, ISO is gladly working on producing a draft <strong> international standard for icons </strong> in the user interface by November 1989. Nigel Bevan from the National Physical Laboratory in England showed a poster on the ISO activities in user interface standardization but unfortunately it did not present the actual icons since they are still under deliberation. The poster was fair enough to also mention the potential disadvantages of user interface standards which mainly are that standards are premature as long as we do not yet know enough about what makes truly usable products and that a standard may inhibit advances in the field. In spite of this, various committees are about to start cranking out draft standards for comment by the active researchers and practitioners in the field.</p>

<p>One slightly unusual standard is the proposal for a standard for the usability process itself and not for the end result. There may be more promise in such a standard since we do know quite a lot about which methodologies to use to increase the likelihood of getting good products. And in many cases, we need some form of external pressure to get various companies to actually devote sufficient resources to usability. On the other hand, there are also many other cases where non-standard methodologies are more appropriate. There is a big risk that many people who do not have real expertise in the our field will come to equate usability with having produced the correct set of voluminous reports in the same way that certain military documentation standards can result in a lower quality of documentation. We can only hope that the specter of standards will haunt Europe and the rest of the world in a spirit similar to Glasnost with sufficient openness to enable user interface professionals to fit their solutions to the unique characteristics of each problem.</p>

<h2>Demos are not Enough (Larry Tesler)</h2>

<p>Often a program will look great when it is shown in a demo by its creator or other wizard user. But Larry Tesler from Apple warned against trusting such great demos as indicators of the true usability of an interface. We need to know whether it will really be that easy for other people to use the system. As an example of this, Tesler showed some video tapes from <strong> user testing of a prerelease version of HyperCard</strong>. One of the problems was that newly created buttons had their visibility attribute set to invisible by default. Expert users zipped right through button creation since they already knew what was going on. Anne Nicol tested some novice users, however, and they were very puzzled when their "create button" commands apparently had no result. They had indeed created a button, but it was invisible...</p>

<p>Another usability problem was the concept of the <strong> "marked card" </strong> which was used as a <strong> hidden state </strong> for certain hypertext related operations. An expert may have been able to grasp this concept easily but for most novices the only card they could really relate to was the one on the screen. Because of these findings, the released version of HyperCard makes nice visible buttons which look like standard Macintosh interface objects so that users can easily recognize them as being buttons and there is no "marked card."</p>

<p>Since HyperCard is one of the most talked about user interface tools in recent years, it was exciting to see videos of the original design and hear a discussion of some of the user interface issues leading to the redesign for the current version. Tesler dared take the "Nielsen challenge" I put forward about in my CHI'88 report where I asked CHI presenters to show us their user interface designs both in "before" and "after" versions and let us judge whether their usability work had improved the design. Actually, Tesler did not show the "after" version of HyperCard but it must have been very familiar to most of the audience. In any case, my verdict is that of these two designs, I will pick the released version for my user interface projects.</p>

<p>Tesler is the Vice President of advanced technology at Apple and it is not often that one hears a corporate VP from a major computer company discuss internal problems and mistakes. According to Clayton Lewis who introduced him as keynote speaker, Tesler was the person who was responsible for introducing user testing at both Xerox and Apple, however. And for people who are used to user testing it is very natural that such a test will reveal some difficulties in an initial design. This will always happen-and that is why you do the test. So an interface snag in an original design is not a mistake as long as it is found and corrected.</p>

<p>Tesler also discussed other aspects of the user interface development process and said that it is not the job of the user interface to access the program but it is the job of the program to implement the user interface.</p>

<h2><em>My </em> CEO Cares More About Interfaces Than <em> Your </em> CEO</h2>

<p>The most popular event at this CHI according to the size of the audience was the panel entitled "My user interface is the best because...." I was lucky enough to at least get to sit on the floor but many people couldn't find room at all. In this panel, people representing the Macintosh, NeXT, Open Look, and Motif interfaces presented the interfaces and boasted of their strengths.</p>

<p>Actually, the panel might almost have been entitled "My CEO cares more than yours" since all the panelists took great care to explain how deeply committed the CEOs of their various companies were to having the best user interface on the market. In this category, I think the prize must go to Bill Parkhurst from NeXT who reported that his CEO (Steve Jobs) had come into his office Saturday night at midnight to discuss user interface design issues.</p>

<p>Parkhurst also explained that the <strong> NeXT machine's interface was designed to take advantage of capabilities which had not been present in earlier, smaller personal computers</strong>. The NeXT design had to deal with multiple processes and a large disk file system with many files. Therefore they integrated inter-process communication with the basic interface: for example, the NeXT machine has the ability to hypertextually link from the WriteNow wordprocessor to its online <cite> Webster's Dictionary </cite> to look up the definition of a word. They also had a large screen, so they put their menus in windows instead of at the top of the screen. Because of the fast CPU in the NeXT machine, they could scroll or move entire windows and not just their outlines. They also decided to go with a multi-color interface from the start to avoid what Parkhurst called the "one-bit mindset" in interface design. Actually, they only have two bits per pixel in their current screen but that is still a lot more than one. They took advantage of the grayscales offered by their screen to provide a richer look and a partly three dimensional appearance of objects through a consistent lighting model.</p>

<p>I was not really sure what Parkhurst meant by "lighting model" but luckily I had breakfast with Bill Verplank the next morning and got the term explained. The idea is to pretend that the interface actually is three-dimensional and that the various elements in the dialog boxes etc. have real depth. You furthermore pretend that this three-dimensional object is flooded with light from a single source placed in one specific corner of the screen and note how the shadows and highlights would fall. And then the graphical design of the actual (flat) interface objects are colored to reflect these shadows and highlights so that they appear almost three dimensional to the eye.</p>

<p>NeXT also produced powerful development tools to help third party developers and to ensure that their products are consistent with the the NeXT interface. During a post-conference visit to Texas A&amp;M University, I took the opportunity to try out a NeXT machine under more quiet circumstances than a conference demo and immediately found a consistency problem in the user interface: When users quit an application, they are sometimes asked "Are you sure you want to Quit?" and sometimes "Do you really want to Quit?" Of course this inconsistency does not constitute a usability catastrophe but it does indicate that fancy graphics will not ensure conformance with all the interface guidelines. A worse problem is that these questions do not point out the real issue which is whether people want to save their files first before their work is irrevocably lost upon quitting. And that can lead to a catastrophe for novice users who do not understand that they are editing copies of the permanent disk files.</p>

<p>Tony Hoeber from Sun presented the <strong> Open Look </strong> design which he claimed was legally unencumbered. He kept stressing that they built on the foundation designed by Xerox at PARC and that they had licensed the rights from Xerox. Actually, Sun and AT&amp;T were willing to warrantee the use of Open Look against any copyright challenges. This may be a significant advantage in the marketplace but it is certainly language which we have not heard in the user interface field before. If companies would also start giving warrantees that users are able to learn their interfaces in a given amount of time, then maybe we will be getting somewhere. A member of the audience said that he was from a company which spent millions of dollars on buying software each year and that he would have liked to see real data proving that a specific interface was good. He especially wanted numeric ratings of effectiveness somewhat like the MPG ratings for cars.</p>

<p>Anyway, Open Look did want to be close to "accepted standards" (the M-word) so they had used e.g. scroll bars to move in a window but with changes such as having the direction arrow buttons on the elevator box instead of at the ends of the scroll bar to reduce the need for mouse movement.</p>

<p>Open Look was designed for environments with quite different I/O equipment, so they had to deal with mice with 1,2, or 3 buttons as well as with varying displays etc. Unfortunately, Hoeber did not discuss the design problems raised by these hardware differences in any great depth. He did say that they had produced a 450 page user interface style guide (250 pages design rationale and 200 pages with examples of good and bad designs). They had a coherent use of color to get foreground/background effects where the foremost item on the screen is the user's current selection and is given the most highly saturated color. In this way, colors are used to achieve a three-dimensional look to the interface.</p>

<p>Ira Goldstein from Hewlett-Packard described the Open Software Foundation's <strong> Motif </strong> interface. This design was mostly compatible with the IBM Presentation Manager, and in general Goldstein claimed that none of the currently popular user interfaces differ by more than 20 %. In the future, customers will require that products from different suppliers are interoperable. The most obvious example today is the need for networking capabilities, but user interface consistency will also be a demand.</p>

<p>Motif had been developed in just one year, and Goldstein claimed that it was the best interface because it was the only one to satisfy the usability goals both for the development process and for the actual end product.</p>

<p>For this panel, the moderator might have selected only one of the contesting standard Unix interfaces since Open Look and Motif seemed fairly similar to me. Of course there are lots of differences but the format of this panel did not offer much opportunity to analyze specific interaction techniques in detail. From a practical perspective, of course, a lot of the people in the audience were probably more interested in comparing Open Look and Motif than they were in hearing about NeXT and the Macintosh so it might have been the right decision anyway to include both. In any case, I sure would not have wanted the responsibility of picking one of these two contenders for the Unix throne before the market has spoken.</p>

<p>In a rebuttal to the other presentations, Tom Erickson from Apple said that the colored <strong> 3-D look may have an artsy effect but that colored text is hard to read</strong>. So he preferred the <strong> simple black-and-white model </strong> for the sake of readability. Erickson also discussed hardware usability which had not been mentioned by the other speakers and said that the Macintosh was getting to be very easy to set up. For example, its cables did not require the use of any special tools. In an "out of the box" study, they had found that some users needed to spend 2-3 hours from getting a Mac in a box until they had a running system but these problems were being addressed.</p>

<p>Erickson also discussed the Macintosh toolbox and other tools for developers which he said were necessary but not sufficient to guarantee good interfaces. The key problem was that an interface is not produced by a single source but by a collection of many companies, so Apple had chosen to also provide cultural support for their developers by having user interface evangelists and having their CEO, John Sculley "talk more about user interfaces than four other CEOs combined."</p>

<p>As a result, the Macintosh had a community of more then 2500 developers who were all interested in user interfaces, and Erickson's claim that the Mac not only was the best interface but would also remain the best interface rested on exactly this third party development community and its high degree of interface orientation.</p>

<p>Towards the end of the panel session, one person from the audience dared to attack the trend towards graphical interfaces and defend the traditional command line interface to Unix. Unfortunately this person was booed by the rest of the audience because there is so much trouble with standard Unix that having to learn it is regarded by many user interface specialists as almost a fate worse than DOS. Of course it impolite to boo people and there are two additional reasons why this lone CLIer should have been treated with more respect. One is that the ultimate usability parameter is whether people prefer the interface, and this person obviously did prefer the line oriented interface (as do many other hard-liners who were probably not at this conference at all). The second, more conceptually important reason is that text interfaces can do a lot of things which graphical interfaces currently have difficulties doing, such as various history mechanisms (including multi-level undo-redo, macro editing, etc.) and aliasing.</p>

<h2>Interfaces Mean Business</h2>

<p>This year's conference theme certainly was that Interfaces Mean Business. IMB is not just a permutation of IBM even though they have been trying recently to move their PS/2 boxes with the argument that the Presentation Manager is more usable than DOS. At this conference, we heard a corporate vice president of a major computer company who was not a stuffed shirt but could actually give a keynote talk addressing real interface issues. We heard representatives from several companies brag about the involvement of their CEOs in usability. And we heard about the user interface copyright in commercial software.</p>

<p>The conclusion from all this is that user interface design is rapidly getting to be one of the most important aspects of the computer business. As a matter of fact, it may soon be the singlemost important aspect when almost all computers can do almost all you want them to do anyway. Then the real difference between computers will be their user interfaces. Because of this interest in the user interface field, the conference received coverage in several newspapers from the local Austin paper that remarked that the CHI attendees had been too intellectual to stir up the local nightlife very much to the New York Times which covered the legal debate and described the Apple Info Kiosk and several of the video tapes.</p>

<p>By the way, it was announced at the SIGCHI business meeting that we are now the fastest growing of all the special interest groups of the ACM both when measured in absolute numbers of new members and when measured in relative growth. This is yet another indication of the growing importance of the user interface field.</p>

<h2>User Interfaces as a Profession</h2>

<p>Before we start to relax and think that the world has been saved, we need to deal with the large number of people who still do not involve user interface professionals in user interface design. It is very common for people who are not aware of the user interface literature or current events in the field to view themselves as qualified to comment on interface issues. Of course we should be glad that non-professionals are interested in our field but we need to change the attitude that everybody is equally qualified since "we are all users."</p>

<p>As an example of this category of problem, Jonathan Grudin presented the results of a survey of user interface design in large corporations. Over <strong> half of the user interface professionals said that their manager's manager did not understand their work</strong>, thus pointing out one of the reasons for the lack of recognition many of us face. Grudin asked software engineers to rate their knowledge of various fields and found that 49% rated their knowledge of human factors as good. This should be compared with the much smaller proportion of software engineers who think that they have good knowledge about industrial design (8%) or how to conduct training (18%). The only profession doing "worse" than human factors in the study was technical writing where 64% of software engineers believed that they had good knowledge.</p>

<p>On the other hand, we should note that 93% of software engineers said that they did need outside help with human factors issues but that only 21% said that the experts were available when needed. So it may be that the need is there but is not fulfilled by the current small budgets for user interface professionals in most organizations. And as a result of this, programmers are forced to pick up bits and pieces of user interface design knowledge.</p>

<p>Only 57% of user interface professionals got involved in projects before the start of implementation while 100% of course said that they would like to get involved at such an early stage of the system life cycle. The only group doing worse were the technical writers of which only 28% got involved before the start of implementation.</p>

<h2>Guerrilla CHI</h2>

<p>My suggested solution to the lack of use of user interface methods is to use a "<a href="../guerrilla-hci/index.php">guerrilla CHI</a>" strategy where we user interface professionals penetrate the various development groups and "live with the peasants." We can get other software professionals to use discount usability methods which are cheap enough that even the smallest company can afford them.</p>

<p>I promoted this philosophy at the closing plenary where I was one of the speakers. Unfortunately my report on events at this session is not nearly as extensive as my report on other events at the conference because I did not concentrate on making notes. One memorable quote which I did write down was Jan Walker saying that the 80s had been the decade of the novices in CHI research but that the 90s would be the <strong> decade of the expert user</strong>.</p>

<p>At the plenary panel, one person from the audience commented that we had abandoned the majority of the world's computer users in our fascination with advanced graphical interfaces. He had not seen any studies of the text-only terminals which are still used by millions of people. Well, the Canadian "conversational hypertext" discussed below actually was a text-only system using the line-oriented interface which is an even older interaction paradigm than the full-screen systems used on most text terminals. But in general, this voice of the people was right: We do look more at neat systems than at traditional systems.</p>

<p>Jan Walker's notecd that this was not really a problem since everybody would get modern, graphical computer systems within a very short time. I am not totally convinced of this as I know several companies which have major trouble with technological inertia because of large investments in obsolete terminals which people are not about to just throw away. So unfortunately, systems will still have to be usable through text-only interfaces in many years to come. We can hope for cross-paradigm interfaces which can be used both on text-only terminals and on graphical workstations. The point I made during the panel was that most of our methods would also apply for the development of textual interfaces so that it would indeed be possible to make usable interfaces even on old terminals. Of course, there are also many things which cannot be done and an important point which I did not make then is that we need to take care that the lowest common denominator effect does not drag the development of new systems so much into the mud that users with modern equipment must settle for the same interface offered to people with old equipment.</p>

<h2>Two Dimensions are Not Enough</h2>

<p>A second conference theme in addition to "interfaces mean business" could be that two dimensions are not enough for modern user interfaces. As mentioned under the discussion of the "my interface is best" panel, the new interface designs coming out currently use pseudo-three dimensional graphical design and lighting models. This is only a small Æ and does not really change the interaction techniques used in the interfaces. But we are also seeing a real trend towards a bigger Æ in the form of actual three dimensional interfaces. This larger change could herald a new generation of interaction paradigms.</p>

<p>Since the conference was in Texas, it was natural to have one of the invited speakers come from NASA. Actually, Michael W. McGreevy came from the NASA Ames Research Center in California, but he still gave a very expansive speech on the topic of personal simulators and planetary exploration. The basic idea of the personal simulator is to create an artificial reality in which the user can be immersed in the data rather than having the user manipulate the data from the outside. This interaction technique is especially useful to make sense of the huge amounts of data gathered from space probes.</p>

<p>From a user interface perspective, these virtual realities should offer the user a utilitarian realism which McGreevy defined as having the environment seem real with respect to the exploration, manipulation and general interaction which the user wants to perform.</p>

<p>McGreevy gave a short <strong> history of artificial realities </strong> from the Chinese emperor's tomb containing an artificial army of terra-cotta soldiers to the flight simulators which trained thousands of pilots during the second world war. It is interesting to consider that flight simulators were regarded as toys before WWII and were mostly sold to amusement parks whereas now they are seen as indispensable. Maybe the same change in attitude will take place towards some of the more fanciful current interface ideas such as the use of iconic sounds or three dimensional video.</p>

<p>In 1984, McGreevy decided to get into personal simulators through the use of <strong> head-mounted displays</strong>. The only problem was that available head-mounted displays cost a million dollars so instead NASA built their own prototype system by using LCDs from pocket TVs which were of course much cheaper because they were consumer electronics. These 1984 displays only had a resolution of 100 x 100 pixels for each eye but their current system has 320 x 240 and they aim to double that in the near future. This means that it will soon be possible to have users wear a head-mounted display which gives the same resolution as a workstation but of course does so three-dimensionally because each eye sees its own display. These prototype systems are intended for use on Earth but they are also developing a new design for use on board the space station which will be smaller and transparent to allow the user to see both the physical reality and the artificial reality at the same time.</p>

<p>One of the initial applications of the NASA head-mounted display was a walk through the space of an air traffic control situation. This is an obvious three-dimensional situation. Other applications include telerobotics and planetary exploration such as rover path traversal planning but they also have plans to test a three-dimensional multimedia dataspace like the science fictional cyberspace where the user can be surrounded by pop-out data items.</p>

<p>Another system with some three-dimensional texture is <strong> Myron Krueger's Videoplace </strong> which had also been displayed at several earlier CHIs. In Videoplace, the user's body serves at the "input device." Actually, the input is only two-dimensional as the user is filmed by a single camera but there is still some three-dimensional feel to being able to walk, dance, and gesture in front of the computer. Videoplace displays a silhouette of the user on the computer screen and lets that silhouette interact with various computer-generated creatures in a playful way.</p>

<p>What was new this year was that a Videoplace system with full-body input had been connected to a Videodesk system at the other end of the hall where another user could input hand and arm movements to the system. The computer displays at both ends of the hall showed the same image consisting of the body silhouette from the Videoplace and the hand-arm silhouette from the Videodesk superimposed on each other. Because of the differences in scale, the arm of one user was displayed as much larger than the body of the other user, thus leading to some fun (and potentially dominating) interactions between the two users. Because of this, the Videoplace/Videodesk setup this year could be seen as a computer-supported cooperative work (CSCW) event instead of a user interface event. Or at least as "computer-supported cooperative play." as one participant observed. It is not all that important exactly how we view this because "the sausage tastes the same from both ends," to cite a proverb from the Danish Cultural Heritage. Proverbs can mean anything you want, and in this case it means that it is rewarding to view Videoplace as an innovation in the user interface field (which is why it has a place at a CHI conference) at the same time as it is rewarding to consider the opportunities for using computers to let people play together. And the "sausage" (=Videoplace) is one of the important systems to taste these years.</p>

<h2>Drama and Personality in Interaction Design</h2>

<p>Krueger was also on the panel on drama and personality in interaction design where he gave the history behind the development of the Videoplace interface. The panel moderator was Joy Mountford from Apple who said that one place to look for inspiration for interface design was the performing arts and especially the theater. She felt that animated graphics had not yet had the impact on user interfaces that they should.</p>

<p>As an example of the use of drama, the interface consultant Brenda Laurel showed a video of a prototype computer system for the teaching of history. The computer would show the user selected video clips where Laurel played various roles such as e.g. a pioneer woman from 1849. Laurel also discussed the use of agents in more general types of user interfaces. The user can delegate all or part of a task to a computer agent which will then perform it. Candidates for delegation include tasks which are too tedious or time consuming for users to want to bother with them, tasks where the computer has a unique expertise (e.g. how to route packets through a network), or tasks where the computer is asked for its judgement (such as a travel planner). This does not mean that agents need to be perfect AIs. Laurel actually said that they would have to pass an "anti-Turing test" so that users would understand that the agents were not real people. They should be less complex to understand and control but more interesting then real people.</p>

<p>Laurel argued that agents should be seen as dramatic characters with certain traits which users could use to understand and predict the behavior of the agent. She felt that agents should have cognitive hooks for understanding and emotional hooks for engaging the user. This view might be related to a presentation by Clayton Lewis in another session at the conference (discussed further below). Lewis had built a model of how users explain causality in interaction events on the basis of eight heuristic rules. One example of such a rule is the "no baggage" rule saying that every element of an interaction controls something (i.e. the designer has not introduced the element in the interface for no reason). Laurel wants users to use another kind of heuristics to understand interactions and it seems to be an open question whether users could then switch back and forth between various ways of understanding different aspects of an interface or whether the entire dialogue model would have to be based on the agent principle.</p>

<p>In another presentation at the Drama panel, Laurie Vertelney from Apple discussed the use of video taped scenarios in preliminary testing of user interface ideas. Vertelney argued that it is best to be able to tell a good story about what your interface will do before you implement it. These "good stories" can be constructed as scenarios of some specific things users might do with the system and how they would do it. The advantage of the scenarios is that they can depict systems which have not yet been built and which are going to use futuristic technology. They can then be used to engage test users in the designs much better than any written specification.</p>

<p>Vertelney used <strong> videotaped scenarios </strong> at Apple, one of which was called <strong> "A Day in the Life" </strong> and basically followed a character named Joe from breakfast to bedtime to see how future technology would be embedded in his life. This kind of video tape constitutes a concrete visualization of your design ideas and can be shown to people to get feedback much cheaper than actually building the interface. The disadvantage is that users do not get to experience the interactive nature of the system from watching a video tape and that the feedback is of a qualitative nature which has to be interpreted.</p>

<p>An additional disadvantage from producing video tapes of how you guess that future technology might be is that the scenarios may be interpreted as being real. Actually Apple had received an order from a customer for delivery of the Knowledge Navigator (another far-out design which exists only on an Apple video). Furthermore, <cite> Business Week </cite> had actually said that one reason for a drop in the value of Apple stock had been that they had "lost credibility" by showing the Knowledge Navigator video of "unrealistic technology." Personally I would prefer to invest in a company that looks farther ahead than just to the next two quarterly reports but that may not be the view among certain stock market analysis.</p>

<h2>Wang Freestyle Desktop</h2>

<p>When I read that Wang was demoing a product called Freestyle, I have to admit that I was somewhat underwhelmed. I mean, who needs to see yet another drawing program. Word of mouth during the conference had it, however, that Freestyle was a hot design worth seeing, so during a boring lecture I decided to give it a try. It turned out that Wang Freestyle did indeed represent interesting interface innovations and that it was not related to the popular graphics program Freehand with which I had probably confused it subconsciously.</p>

<p>The first remarkable thing about Freestyle is that it uses a <strong> stylus </strong> on a tablet instead of the ubiquitous mouse. The stylus is of course not so interesting in itself but it is an indication of the nature of the Freestyle product. I would characterize Freestyle as a <strong> desktop metaphor to the third degree </strong> . It has a reasonably big monochrome graphics display which shows images of the various objects in the system. A file is simply a full-screen sized drawing area which is shown either full size (taking over the screen) or shrunk to a miniature on the "desktop" screen. These <strong> miniatures </strong> are somewhat larger than traditional icons and show a reduced image of the drawing in the file. In an unusual deviation from most computer systems, the files are not named, just as you do not give names to the pieces of paper you have on your real desk. To find a file, the user must be able to recognize its miniature and its location on the desktop screen. The one exception to the namelessness of the Freestyle universe are the objects representing recipients of electronic mail. Here named generic icons are used, even though I might have considered representing a person by a scanned photo. The Freestyle demo was given by several knowledgeable Wang people, including Ellen Francik who had conducted several prerelease usability tests. She explained that one reason for using explicit names for email recipients was that users would worry whether they were mailing to the correct person since it is sometimes a disaster to send certain letters to the wrong person.</p>

<p>To collect several pages together, you place their miniatures over each other and apply a stapler (which is also a graphical object on the screen). Unfortunately, a stapled set of pages are shown just by the miniature of the one page which happens to be the topmost in the pile. Miniatures are much higher in interface richness than normal icons but to achieve the ultimate fidelity to the reference system of the real desktop, it would have been nice if miniatures of a pile could have indicated the thickness of the pile in some way.</p>

<p>I didn't like having to move pieces of paper on the desktop screen by pressing down on them with the stylus. Essentially the same action feels much more natural using a mouse than using a stylus. This is probably because of the different connotations of the two devices: Moving a stylus is like moving a pen for making marks, while moving the small box called a mouse involves the same muscles as moving physical objects in general: the mouse is higher in the <strong> articulatory directness </strong> mapping between the physical movement and the lexical token specified ( <code> move </code> ). Exactly the same phenomenon of course works in favor of the stylus when it comes to sketching or even writing in the Freestyle graphics editor. I wrote a letter and signed it with no trouble using the stylus-but try to produce a recognizable signature using a mouse! Another advantage of the stylus is that it can be turned over to have its top used as an eraser. In this way, the user is freed from having special commands to select between drawing mode and erasing mode as in almost all drawing programs. This method of using physical attributes of input devices to determine their semantic meaning is similar to the physical eraser used in the University of Tokyo Tron project which I discussed in my trip report from the <a href="../trip-report-fifth-generation/index.php"> 1988 Fifth Generation conference</a>.</p>

<p>A final interesting feature of Freestyle is their ability to integrate graphics, spoken comments, and animated gestures in one email message. It was possible to take e.g. a scanned road map, use the stylus to draw in directions to go somewhere, and record a running commentary about what landmarks to watch for. The resulting file could be sent by electronic mail to another Freestyle user who could play it back and watch the directions being redrawn on the background road map synchronized with the spoken comments. In the program for SIGGRAPH'89 this kind of interface is called a conversational document which is not really true in my opinion since the receiving user cannot alter or question the presentation designed by the sending user. For true conversational documents we must await sufficient advances in AI to allow the inclusion of model of the sending user's thinking about the document's topic which is detailed enough that receiving users can query that agent in the same way they could discuss the issues if the sender had traveled in person.</p>

<h2>To Hyper or not to Hyper</h2>

<p>Since I am currently doing a lot of research and consulting on various hypertext issues, I dutifully attended the session entitled "Hypermedia." It actually turned out that the papers in this session were not all that much about hypertext while there were several hypertext papers presented in other sessions.</p>

<p>First, Thomas Whalen from the Communications Research Centre in Canada presented a so-called "conversational hypertext" system. The word "conversational" referred to the use of the old question-answer dialogue style instead of the point-and-click style more commonly used in hypertext. The basic principle of the system was to present the user with a given piece of text (corresponding to a node in the hypertext network) and then await some natural language input from the user. The system interprets the user's input according to the set of legal hypertext links out of the current node and chooses the next node as that giving the best match with the user's input.</p>

<p>The two good points of this interface design are that it provides a means for getting a fair natural language interface with only a small investment in AI (because of the extreme context-sensitivity of the interpretation) and that it provides a method for accessing a hypertext through a line-oriented interface. Most people of course would not want to access hypertext through a line-oriented text-only interface, but for Wahlen's application it did make some sense. They had developed a hypertext information base about AIDS and wanted to make it available to dial-in use by home computer owners having a modem.</p>

<p>The interface was developed by iterative design which had raised the proportion of user queries successfully answered from 20% to 70%. In the beginning when the success rate was low they looked at unsatisfied queries and modified the information base to be able to handle them. According to Wahlen, an added advantage of their approach to hypertext and natural language is that it would be very easy to scale up to larger information bases: It would just require getting a bigger computer.</p>

<p>A question from the audience was whether Wahlen had considered translating the information base to French or other languages. He answered that they had indeed considered doing this since they worked in Canada, but that it was not just a question of translating the text. The very information base tended to be culturally specific and people would ask different questions. For example, a visitor to their lab from Japan had entered questions such as "Where was the origin of AIDS" and "This was a nice demonstration, thank you very much," neither of which their natural language recognition had been able to handle based on its iterative development from questions asked by Canadians.</p>

<p>In a slightly more ambitious project, <strong> Bob Glushko </strong> from Search Technology had at least used a limited windowing paradigm for a hypertext system developed to run on ordinary IBM PC ATs. The focus of Glushko's talk was not so much on the hypertext aspects of his system as on the general problem of transforming printed text to an online form. He had seen several good example of hypertext systems but no clearcut methodology for transforming existing text, so they set out to develop what he called hypertext engineering as they were actually doing the work. The project was converting the Engineering Data Compendium from a four volume printed text with 3000 pages of text and 2000 illustrations to a CD-ROM version which could be displayed on an IBM PC screen. Glushko stressed that this was big enough not to be a toy "Hypertext for Hobbyists" type project.</p>

<p>One of the main problems was not so much related to hypertext as such but to the limitations of the PC screen. The illustrations could only be displayed in a coarse resolution and it was impossible to keep the page layouts from the original book. The page layout problem was especially bad because the printed book had been carefully designed with the two-page spread as the basic unit to have related text, graphics, and tables visible at the same time. In the electronic version this was not possible, but according to Glushko, the use of alternative hypertext access structures still made it possible for them to do a decent job of meeting the users' needs.</p>

<p>During this project, they had started by a document analysis to understand the logical and physical structure of the existing book. They had studied the existing access structures such as the index and table of contents and found that this book already had a rich internal access structure which they could use in their hypertext. Glushko felt that it would be impossible to automatically construct an index of the same high quality as that provided by the original designers of the book, so they based their design on that.</p>

<p>In addition to their own analysis of the printed book, Glushko and his colleagues also wanted to learn about the rationale of the book design from the original designer and editor. In general, Glushko was very big on wanting to understand how users would work with the document since he wanted to be user-driven and document-driven rather than technology-driven as other hypertext projects have been. So he had based the construction of the hypertext links on a task analysis of what users would be likely to want to do with the document. His basic philosophy was that one should only add a hypertext link if one could find a reason from the task analysis to put it in.</p>

<p>Glushko did not want to support blind navigation in hyperspace but only the user's need for information in a specified context. This sounds extremely convincing, especially considering the extreme importance of the task circumstances on users' performance in user interface studies. On the other hand, the Bellcore designers of SuperBook (discussed below) specifically promote its abilities to support information needs which had not been taken into account by the author. The key question here must be whether we actually can conduct a sufficiently thorough task analysis to be sure to capture everything that a reasonable user might want to do with the document - But how do we know that "unreasonable" users are not the ones who could really do some great and innovative work if we only had supported them?</p>

<p>The third paper in the hypertext session was The <strong> Tourist Artificial Reality</strong>, presented by Kim Fairchild and Greg Meredith from MCC. They started their co-presentation with a very nice survey of other artificial realities and in passing mentioned Randy Smith's Artificial Reality Kit as the best current system. They also gave a taxonomy of artificial realities which seemed to be well thought out and useful. I say "seem" because they showed their analysis on a series of slides packed with text and only showed each slide for a few seconds so that it was really impossible to gain an understanding of the analysis. Also, this analysis is not in their paper in the proceedings but rumor has it that it may be in one of the journals Real Soon Now.</p>

<p>Fairchild and Meredith had implemented a system based on a tourist metaphor and it was probably because of this travel aspect that this paper had been placed in the hypertext session. They showed a video of the system which was unfortunately somewhat overdone to get laughs and did not really convince me of the utility of their approach. But of course, plain fun in computer systems should not be disparaged. In any case, they said that the most fun part of artificial realities came when you had several people interacting with each other in the reality such as in the Videoplace/Videodesk setup discussed above.</p>

<p>Frank Halasz and George Furnas gave a joint presentation of the big picture as discussants for the session. They defined two principal metaphors for information access: One method is <strong> making the information space explicit </strong> to the user and as visible as possible and then using travel or navigation as the metaphor for moving through the space. The alternative method is the <strong> information retrieval metaphor of using searches </strong> as the access mechanism. The user is not presented with any explicit conceptual model of the information space or its structure but works in an "ask and you shall receive" mode relying on an agent in the computer to fish relevant nuggets of information out of a black bag. The tourist artificial reality project tries to push the space metaphor while the conversational hypertext project tries to split the two metaphors by having users navigate but having them do so through queries and hiding the structure of the information space. Glushko's hypertext engineering was finally a mixed approach where the access metaphors were picked from a mix according to the task at hand.</p>

<p>Another system which can be classified according to this metaphor dimension is the <strong> SuperBook </strong> from Bellcore which was presented by Dennis Egan in a session I did not attend. But since I visited the SuperBook group at Bellcore when I arrived in New York before going to Austin, I can comment on it anyway. In SuperBook, the structure of the information space is made explicit to the user through the use of a fisheye view of the book's table of contents. And at the same time, the user can get directly at nodes buried deep within this structure by performing full text searches. The result of such a search is then shown integrated with the overview diagram by annotating it with the number of hits in each node or supernode (holophrasted set of nodes in the fisheye view). Therefore we could say that SuperBook is a way of marrying the two metaphors in a single design. A related approach I am currently trying, is to construct hypertext links between nodes automatically on the fly based on similarity ratings with weights determined by user queries and relevance feedback.</p>

<p>Another good hypertext paper was Gerhard Fischer et al.'s proposal for a design integrating an AI system which advices on design issues with a hypertext system containing the rationale for the advice given by the AI system. The hypertext system uses an alternative implementation of the IBIS Issue-Based Information System method made famous by the MCC gIBIS system to structure the arguments for and against the various design options, and the AI system can then dump the user at the location in this hypertext which corresponds to the user's current undecided design problem. Many people talk about the use of AI in hypertext but this is one of the few examples I have seen where I could easily see that the AI and hypertext aspects would complement each other resulting in increased usability. The domain of the system was somewhat peculiar (design of kitchens) but that seems to be true of a lot of AI systems. AI components with such glamorous names as sink-critic and refrigerator-critic would find out if the user proposed placing a kitchen element in an inconvenient location relative to other elements. These rules could give the user explanations such as "sink should be NEXT-TO a dishwasher" in the traditional AI style. But assuming that the user either did not understand that explanation or wanted to take issue with it, the system would transfer the user to the hypertext system for further explanation and perusal of argument structures. Actually it could not really do so because the AI subsystem ran on a Symbolics while the hypertext subsystem originally ran on a Macintosh-but that has been fixed by a new version which moved everything to the Symbolics.</p>

<h2>Information Kiosks</h2>

<p>The Apple Human Interface Group had set up a bunch of Macintoshes at the conference for use as information kiosks. They contained information about the conference program with semi-animated sound-and-graphics presentations from several of the conference speakers as well as a slightly confused hypertext guide to the city of Austin. The most popular part of the info kiosk, however, seemed to be the CHI Yearbook with digitized photos and addresses of many of the conference attendees. Apple had also organized a number of photo booths with digitizing cameras and Macintoshes where people could fill in their name and address as well as a question they would like to ask the other conference attendees. The information gathered from these photo booths was transferred to the Info Kiosk Macs on a regular basis by the primitive but effective means of moving an external hard disk around between the Macs and copying the updated information over.</p>

<p>My question in the Info Kiosk was "Do you own software which you don't use? And why don't you use it?" In my own case, I have observed that I have bought a large number of software packages over the years which simply sit unused on the shelf. For example, I own about 4-5 other word processors but still use Microsoft Word exclusively.</p>

<p>Reading other people's questions was fun and one reason why many attendees spent more time browsing the Info Kiosks this year than have been the case at earlier CHI public computer displays. Another reason was that the digitized photos provided a method to find out how other people looked so that it was easier to meet them at the conference. And there was also some utility to the information about which hotel people stayed at. All these advantages followed directly from the dynamic nature of the combined photo booth and Info Kiosk approach, but of course it was also a lot of work to design such an extensive system and to keep it continuously updated.</p>

<p>In spite of the nice work Apple had done on the Information Kiosks, the part of the system having information about the conference program itself lost out to the printed participants' program. Claudia Raun had designed the best conference program ever. It was physically easy to use because of its size and spiral binding which made it possible to keep it open at the listing of the current session. All papers, panels, demos, and special interest group meetings taking place at any given time were listed on the same page of the program thus making spur of the moment decisions easy for conference participants. It seems that somebody actually studied the users and their task before doing this design. It is currently impossible for any hypertext system to compete with such a well-designed printed information package which can be accessed in a crowded lecture room or during a coffee break while one is in line to get the last strawberry.</p>

<h2>Artifacts as Theories</h2>

<p>The very detailed interaction theories do not seem to have a very large impact on real user interface design. <strong> Jack Carroll </strong> from IBM Yorktown Heights gave a talk on the theory problem in HCI and said that some people would respond to this problem by saying that the reason was that there is no possible theory. It could also be, however, that our problem is that we have been locked into positivism in a search for a normative philosophy of science to tell us how science should be, how designs should look, and therefore enable us to deduce designs on the basis of a scientifically tight theory. But experience has shown that that kind of theory only leads us to toy-scale designs.</p>

<p>An alternative would be a descriptive philosophy of science which would study user tasks and design artifacts. We know that the artifact changes the task and vice versa, so that we have a design cycle. This approach leads to a design-based theory such as e.g. the concept of direct manipulation which of course was embodied in real designs long before the emergence of theories trying to explain it.</p>

<p>If we take this alternative approach, we don't need to conclude that there is no theory in HCI. Instead we can take artifacts seriously since they embody testable "claims" about usability. Carroll used the peculiar term theory-nexus to refine his view of the usability claims embodied in an interface because one should not just view an interface as a list of separate usability principles: All the usability claims in an interface depend on each other in complex artifacts. We can view artifacts as the media for demonstrating our understanding of HCI. Therefore artifacts are the appropriate focus for theoretical work in HCI, and since they are the way the practice of HCI works anyway they could lead to a tighter integration between theory and practice.</p>

<p>Another interesting theoretical development is the steady progress of Clayton Lewis from the University of Colorado on explaining how users make inferences and predictions about interactive systems (alluded to above under the discussion of interface Agents). This progress is somewhat slower than I would like since I see Lewis' inference principles as a possible practical design aid. But at least the progress is steady. This year, Lewis had done some empirical testing and asked users to choose between various interpretations of a set of dialogues. The result was that people confirmed that in theory several interpretations were possible but that they preferred the interpretation predicted by Lewis' inference heuristics. So it does seem to be reasonable to try to design systems which follow the principles - we now have a formalization of the principle of least astonishment.</p>

<p>One concrete result from Lewis' study was that aliasing (having several names for the same thing) makes it harder for users to generalize since they void one of his heuristics. Therefore the mere presence of an aliasing possibility in an interface would have a larger impact on the usability of the entire interface than would be indicated by a rule-oriented analysis where the multiple names would only impact the rules for those parts of the interface where they were actually used.</p>

<h2>Things I Missed</h2>

<p>You always miss some things at conferences but CHI conferences are worse than most in this regard. And CHI'89 was so overloaded with interesting events that choosing what to do at any given time was as frustrating as selecting the main course at a restaurant with three stars in the <cite> Michelin Guide</cite>. There is so much you have to miss. I worked every day during the conference from various committee and journal editorial board breakfasts at 7:30 in the morning to in-house videos after midnight, but still missed a lot. The conference video tapes were shown 24 hours a day on the hotel TV-system but I never got to see them all anyway. I had hoped to catch up on the videos after the closing session of the conference but unfortunately the video system was shut down by mistake as soon as the conference was officially closed. The original plan was to keep playing the videos on the hotel system for about one more day since many people do not leave the moment the conference closes. Actually, for future conferences it should be considered to also start the in-house video a few days before the conference to the benefit of the many people who arrive early for tutorials, workshops, or other events. I would love to have something intellectual to watch on TV when I wake up at 4 AM the first few days because of the time difference when coming from Europe.</p>

<p>Wendy Mackay, Thomas Malone, and several other authors had a paper with some empirical evidence on <strong> how people use the knowledge-based rules in the Information Lens</strong>. I have waited anxiously for such a paper since I first heard Malone's presentation of the Information Lens at CHI'85 because I have always been sceptical about the abilities of ordinary users to write the expert system-like rules used by the Information Lens to sort the incoming electronic mail. Well, the advantage of papers are that it is possible to read them later even if you miss hearing the presentation, so I read this one when I returned home.</p>

<p>I have always liked the Information Lens and wanted to have one for myself (if it had been available for the strange combination of a Macintosh and an IBM VM mainframe I use for email) but I have been less optimistic about its use by people with less programming ability. It turns out from Mackay <em>et al.</em>'s study that people are indeed able to write rules themselves even if they are not programmers. The only qualifying caution is that the study was conducted at an unnamed research laboratory sounding suspiciously like Xerox PARC which is a collection of people with above-average abilities and interest in new technology, even outside the computer science department.</p>

<p>These users created rules of at least medium complexity, such as "if this message is from person NN and has foobar as its subject, then delete it." Such rules with more than one condition were more common for deletion actions than for classification actions, possibly because it is more dangerous to delete an incoming email message. Sometimes the rules can be pretty strange. One user had a rule which gave higher priority to messages with the string "BITNET" in the from: field because they were normally from overseas colleagues. Since I send my email from the Bitnet, I can only applaud such a rule but it does give rise to some potential touchy social issues where the name of the network or domain you are on becomes important for whether you are heard.</p>

<p>One good aspect of the Mackay <em>et al.</em> paper is that it documents the real life use of a system which design and underlying concepts have been highly visible in the research literature over the last five years. Another positive aspect is that the study was conducted longitudinally over a period of 18 months. For systems such as advanced electronic mail it is much more important how people use them over an extended period of time than how they use them initially. It would also be interesting to see how the use changed over time but that is unfortunately not discussed in the paper.</p>

<h2>Conference Ergonomics</h2>

<p>I have already mentioned that Claudia Raun's conference program was extremely user friendly. The other traditional gripe is the visual aids used by the speakers but they were mostly fine. One person did use overheads with what seemed like a 12 point font, and a joint presentation by two speakers zipped through a large pile of slides much too quickly for anybody to read them.</p>

<p>For my own presentation I had prepared an overhead with a 36 point font because of the huge audience in the plenary session. During the conference I wanted to produce an additional foil but the speakers' prep room did not have a computer system. Luckily a student volunteer helped me check the various administrative offices and finally found a system with a laserprinter. Now my only problem was that the word processor on that computer would not produce text larger than 24 point. But the hard disk also contained Microsoft Excel which was able to print large fonts. So my final overhead was produced in a spreadsheet....</p>

<p>The name badges were fine and readable. The lights in the lecture rooms almost always stayed up enough to allow the audience to stay awake and take notes. So it almost seems like the conference ran so smoothly that there is no fun in being a critic. The only bad event was the industrial tour to various presumably interesting companies in Austin. Some companies had set up nice tours of their facilities but many just kept us in a single lecture room for a standard lecture or video tape about their company. A minimum requirement for an industrial tour must be that the the participants are shown some actual computer equipment and laboratory facilities since there is otherwise no reason to ride a bus for hours: the prize for best tour goes to <strong> Ozz Research</strong>, a small company working in interactive video, CD-ROMs, and hypertext.</p>
</section>
  

  
  <section>
    
        
    
  </section>
  
  
  
  
  

  


  <section>
    <br />
    <p class="share"><b>Share <span class="hide-for-small-down">this article</span>:</b> <a href="https://twitter.com/intent/tweet?url=http://www.nngroup.com/articles/trip-report-chi-89/&amp;text=CHI'89%20Trip%20Report&amp;via=nngroup">Twitter</a> | <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=http://www.nngroup.com/articles/trip-report-chi-89/&amp;title=CHI'89%20Trip%20Report&amp;source=Nielsen%20Norman%20Group">LinkedIn</a> | <a href="https://plus.google.com/share?url=http://www.nngroup.com/articles/trip-report-chi-89/">Google+</a> | <a href="mailto:?subject=NN/g Article: CHI&#39;89 Trip Report&amp;body=http://www.nngroup.com/articles/trip-report-chi-89/">Email</a></p>
    <hr />
  </section>

  <footer class="l-subsection">
    
    
    <div class="learn-more">
      <h2>Learn More</h2>
      <p><a href="../subscribe/index.php">Subscribe to the weekly newsletter</a> to get notified about future articles.</p>
      </div>
    
    

    <div class="row show-for-small-down related-content">
      
      
      

      
      
        <div class="column small-12 topic-links">All articles about:
          <ul>
            
            <li><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction</a></li>
            
            <li><a href="../../topic/hypertext/index.php">hypertext</a></li>
            
          </ul>
        </div>
      
      

      
      
        <div class="column small-12">
          <h3>UX Conference Training Courses</h3>
           <ul class="no-bullet">
    
        <li><a href="../../courses/ux-basic-training/index.php">UX Basic Training</a></li>
    
        <li><a href="../../courses/hci/index.php">User Interface Principles Every Designer Must Know</a></li>
    
        <li><a href="../../courses/interaction-design-3-day-course/index.php">Interaction Design: 3-Day Course</a></li>
    
</ul>
        </div>
      
      

      
      
      

      
      
      
    </div>

    <div class="hide-for-small-down row related-content">
      <div class="medium-6 columns">
        
        
          <div>
            <h3>Articles</h3>
            <ul class="no-bullet">
              
                
                <li><a href="../embarrassment/index.php">Computer-Assisted Embarrassment</a></li>
                
              
                
                <li><a href="../enhancement/index.php">The Role of Enhancement in Web Design</a></li>
                
              
                
                <li><a href="../trip-report-hypertext-89/index.php">Hypertext&#39;89 Trip Report</a></li>
                
              
                
                <li><a href="../trip-report-hyper-hyper-89/index.php">HyperHyper&#39;89 Trip Report</a></li>
                
              
                
                <li><a href="../trip-report-hypertext-87/index.php">Hypertext&#39;87 Trip Report</a></li>
                
              
            </ul>
          </div>
        
        

        

      </div>
      <div class="medium-6 columns">
        
          
          <div>
            <h3>UX Conference Training Courses</h3>
             <ul class="no-bullet">
    
        <li><a href="../../courses/ux-basic-training/index.php">UX Basic Training</a></li>
    
        <li><a href="../../courses/hci/index.php">User Interface Principles Every Designer Must Know</a></li>
    
        <li><a href="../../courses/interaction-design-3-day-course/index.php">Interaction Design: 3-Day Course</a></li>
    
</ul>
          </div>
          
        
        
      </div>
    </div>
  </footer>
</article>

        </div>

      

    
  </div>
  <div class="row">
    
      <div class="column small-12 show-for-small-down l-navrail announcements">
        
          
        
      </div>
    
    
  </div>

    </div>
    




<footer id="page-footer">
    <div class="row hide-for-print">
        <div class="column small-12 medium-6">
            <section class="footer-popular">
                
                <h1>Popular Topics</h1>
                <div style="float: left;  width: 15em; display: block;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/applications/index.php">Application Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/e-commerce/index.php">E-Commerce Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/eyetracking/index.php">Eyetracking </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/information-architecture/index.php">Information Architecture </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/intranets/index.php">Intranet Design </a></li>
	<li><a href="../../topic/interaction-design/index.php">Interaction Design </a></li>
</ul>
</div>

<div style="float: left;  width: 15em; display: block; list-style: none;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/mobile-and-tablet-design/index.php">Mobile and Tablet Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/credibility-persuasion/index.php">Persuasive Design</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/research-methods/index.php">Research Methods</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/user-testing/index.php">User Testing </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/web-usability/index.php">Web Usability </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/writing-web/index.php">Web Writing &amp; Content Strategy </a></li>
</ul>
</div>

<p>&nbsp;</p>

                
            </section>
        </div>

        <div class="column small-12 medium-6">
            <div class="row">
                <div class="column small-6">
                    <section class="footer-about">
                        <h1>Follow Us</h1>
                        <ul>
	<li><a href="../subscribe/index.php">Subscribe to our weekly UX newsletter</a>,&nbsp;Jakob Nielsen&#39;s Alertbox</li>
	<li>Visit Don Norman&#39;s website, <a href="http://www.jnd.org/"> www.jnd.org </a></li>
	<li>Visit Bruce Tognazzini&#39;s website, <a href="http://www.asktog.com/"> www.asktog.com </a></li>
	<li style="padding-top: 6px;"><a class="twitter-follow-button" data-show-count="false" data-size="large" href="https://twitter.com/nngroup"><img alt="Follow @nngroup on Twitter" height="20" src="https://media.nngroup.com/media/editor/2016/12/30/twitter-follow-button.png" style="width: 117px; height: 20px;" width="117" /></a></li>
	<li style="padding-top: 6px;"><img alt="" height="14" src="https://media.nngroup.com/media/editor/2016/07/26/feed-icon-14x14.png" style="vertical-align:text-top;width:14px;margin-right:.25em;" width="14" /><a href="../../feed/rss/index.php">Subscribe to our RSS feed</a></li>
</ul>

                    </section>
                </div>

                <div class="column small-6">
                    <section class="footer-about right-column">
                        <h1>About
</h1>
                        <ul class="no-bullet">
	<li><a href="../../about/contact/index.php">Contact information</a></li>
	<li><a href="../../about/index.php">About Us</a></li>
	<li><a href="../../about/why-nng/index.php">Why NN/g? </a></li>
	<li><a href="../../return-policy/index.php">Return Policy</a></li>
</ul>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="copyright column small-12">
            <p><a href="../../copyright-and-reprint-info/index.php">Copyright</a> &copy; 1998-2017 Nielsen Norman Group, All Rights Reserved.</p>
        </div>
    </div>
</footer>


    


        <script src="https://media.nngroup.com/static/vendor/fastclick/js/fastclick.min.js"></script>
        <script src="https://media.nngroup.com/static/vendor/foundation/js/foundation.min.js"></script>
        <script src="http://media.nngroup.com/static/js/app.js"></script>
        <script src="http://media.nngroup.com/static/js/scroll-to-fixed.js"></script><script>var externalLinks = document.querySelectorAll('a[href^="http"]');for (var i = externalLinks.length-1; i >= 0; i--) {externalLinks[i].addEventListener("click", function(event) { alert("External links are disabled for this test."); event.preventDefault(); }, false);}var mailLinks = document.querySelectorAll('a[href^="mailto"]');for (var i = mailLinks.length-1; i >= 0; i--) {mailLinks[i].addEventListener("click", function(event) { alert("Email links are disabled for this test."); event.preventDefault(); }, false);}var externalForms = document.querySelectorAll('form');for (var i = externalForms.length-1; i >= 0; i--) {externalForms[i].addEventListener("submit", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false); externalForms[i].addEventListener("click", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false);}</script>

        <script>
            
            $(function() {
                $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
                $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
            });
        </script>

        
<script src="https://media.nngroup.com/static/js/articles.js"></script>


        
        <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1072699960;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072699960/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
    </body>

<!-- Mirrored from www.nngroup.com/articles/trip-report-chi-89/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:14:36 GMT -->
</html>
