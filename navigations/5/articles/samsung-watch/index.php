<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->





    
<!-- Mirrored from www.nngroup.com/articles/samsung-watch/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:10:30 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1026.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"licenseKey":"1a30ef9931","queueTime":0,"beacon":"bam.nr-data.net","errorBeacon":"bam.nr-data.net","applicationID":"2048992","transactionName":"NFxSMRBUX0EDUkIPDA0WdhAMVkVbDV8ZBxEXUFMJB0YfRAtUQRVZIktEDAFZVHYHRVcPD01eVRE=","applicationTime":502,"agent":""}</script>
        <title>UX Review of Samsung Galaxy Smartwatch</title><meta property="og:title" content="UX Review of Samsung Galaxy Smartwatch" />
  
        
        <meta name="description" content="The Samsung Galaxy Gear smartwatch poses unique problems due to the tiny touchscreen. The use of gestures and streamlining content are reasonable solutions, but need to be implemented in a more usable manner.">
        <meta property="og:description" content="The Samsung Galaxy Gear smartwatch poses unique problems due to the tiny touchscreen. The use of gestures and streamlining content are reasonable solutions, but need to be implemented in a more usable manner." />
        
  
        
	
        
        <meta name="keywords" content="smartwatch, Samsung Galaxy Gear, usability, touchscreen, mobile, user interface, mobile user experience, smartwatch user experience">
        

        

        <meta name="robots" content="noarchive">
        <meta name="viewport" content="width=device-width">

        <link rel="search" type="application/opensearchdescription+xml" title="NN/g Search" href="../../opensearch.xml" />
        <link rel="shortcut icon" href="http://media.nngroup.com/static/img/favicon.ico" />

        <link rel="stylesheet" href="https://media.nngroup.com/static/vendor/font-awesome/css/font-awesome.min.css">

        
        <link rel="stylesheet" href="https://media.nngroup.com/static/css/7232a960f1df.css" type="text/css" />
        <link rel="stylesheet" href="http://fast.fonts.com/t/1.css?apiType=css&amp;projectid=c5846260-9def-46ec-9407-0f1349bd2d64" type="text/css">

        <script src="https://media.nngroup.com/static/vendor/modernizr/js/modernizr.min.js"></script>

        <script type="text/javascript" src="https://media.nngroup.com/static/js/3283be5def74.js"></script>
        

        

        
        
        <!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=229034,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('http://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->


<!-- Google Analytics Tracking Code for http://nngroup.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23132681-1', 'nngroup.com');
  ga('require', 'linkid', 'https://www.nngroup.com/articles/samsung-watch/linkid.js');
  ga('require', 'displayfeatures');

// Optimizely Universal Analytics Integration
 window.optimizely = window.optimizely || [];
 window.optimizely.push("activateUniversalAnalytics");

  ga('send', 'pageview');

</script>
<script>
/**
* Function to track a click on an outbound link in Google Analytics.
*/
function trackOutboundLink(label, url) {
    if(window.ga && ga.loaded) {
        ga('send', 'event', 'outbound', 'click', label, {'hitCallback': function(){document.location = url;}
        });
    } else {
        document.location = url;
    }
}
/**
* Function to track a click on a link in the article announcement area.
*/
$('#articleAnnouncement a').on('click', function(){
  ga('send', 'event', 'link', 'click', 'articleAnnouncement');
});
</script>

<script type="text/javascript">
 setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2421.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



<!-- Hotjar Tracking Code for http://nngroup.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:121605,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

        <script src="https://media.nngroup.com/static/js/analytics/track_outbound_link.js"></script>
        
        
        
    </head>
    <body class="location-articles articles-detail">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
    <header id="page-header">
        <div id="cart" class="shopping-cart mobile-cart show-for-small-down"></div>
        <div class="row">
            <div id="masthead" class="masthead small-12 medium-7 column">

                <!-- The button to toggles menu on small screen -->

                <h1><a href="../../index.php"><img src="http://media.nngroup.com/static/img/logo-wordmark.png" width="74" height="30"> Nielsen <span class="norman">Norman</span> Group</a></h1>
                <p class="tagline show-for-medium-up">Evidence-Based User Experience Research, Training, and Consulting</p>

                <!-- The button to toggles form search on small screen -->


                <div style="clear: right;width:  100%;height: 1px;"></div><?php define('ROOT_PATH',$_SERVER["DOCUMENT_ROOT"] . '/apps/navigation_styles/navigations/5'); ?><?php include(ROOT_PATH)."/navigation.php"; ?>

                <!-- Drop down menu on small screen -->
                <div id="hideNav" class="show-for-small-down">
                  
                  <ul class="menu-content">
                    <li><a href="../../index.php">Home</a></li>
                    <li class="has-submenu">
                      <a>Training</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../training/index.php">Overview</a></li>
                        
<li id="nav-ux-conference"><a href="../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../online-seminars/index.php"><span>Online Seminars</span></a></li>

                      </ul>
                    </li>
                    <li class="has-submenu">
                      <a>Consulting</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
  <li><a href="../../consulting/index.php">Overview</a></li>
  
  <li><a href="../../consulting/benchmarking/index.php">Benchmarking</a></li>
  
  <li><a href="../../consulting/expert-design-review/index.php">Expert Design Review</a></li>
  
  <li><a href="../../consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
  
  <li><a href="../../consulting/usability-testing/index.php">Usability Testing </a></li>
  
  <li><a href="../../consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
  
  <li><a href="../../in-house-training/index.php">In-House Training</a></li>
</ul>

                    </li>

                    <li><a href="../../reports/index.php">Reports</a></li>
                    <li><a href="../index.php">Articles</a></li>
                    <li class="has-submenu"><a>About NN/G</a><i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../about/index.php">Overview</a></li>
                        <li><a href="../../people/index.php">People</a></li>
                        <li><a href="../../about/why-nng/index.php">Why NN/g?</a></li>
                        <li><a href="../../about/contact/index.php">Contact</a></li>
                        <li><a href="../../news/index.php">News</a></li>
                        <li><a href="../../about/history/index.php">History</a></li>
                        <li><a href="../../books/index.php">Books</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                </div>

                <hr class="header-divider show-for-small-only"/>


            </div>
       </div>

        <div id="hideNav">            <nav class="primary-nav small-12 column">
                <ul>
                    <li><a class="navitem" id="nav-home" href="../../index.php">Home</a></li>
                    <li><a class="navitem" id="nav-training" href="../../training/index.php">Training</a></li>
                    <li><a class="navitem" id="nav-consulting" href="../../consulting/index.php">Consulting</a></li>
                    <li><a class="navitem" id="nav-reports" href="../../reports/index.php">Reports</a></li>
                    <li><a class="navitem" id="nav-articles" href="../index.php">Articles</a></li>
                    <li><a class="navitem" id="nav-about" href="../../about/index.php">About NN/G</a></li>
                </ul>
            </nav>
            <nav class="secondary-nav small-12 medium-12 large-12 columns">
                
                
            </nav>
        </div>
    </header>
    <div class="l-content"><span id="preventClose" style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776; Menu</span>
    
  

  

  <div class="row">
    
      
      <section class="small-12 medium-3 columns collapsable-content l-navrail articles-left-column">
        <h1 class="show-for-small-down collapse-link mobile-browse-header">Browse by Topic and Author</h1>
          <div class="row collapsable">

            
            <div class="small-6 medium-12 columns">
              <section id="article-popular-topics" style="display: none;">
                
                <h1>Topics</h1>
                <ul id="article-popular-topics">
                    
                    <li>
                        
                        <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/intranets/all/index.php">Intranets</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/user-testing/all/index.php">User Testing</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                        
                    </li>
                    
                    <li><a href="#" id="article-see-all-topics"><strong>See all topics&hellip;</strong></a></li>
                </ul>
                
              </section>
              <section id="article-all-topics" style="display: none;">
                
                <h1>
                  <span class="show-for-medium-up">All Article Topics</span>
                  <span class="show-for-small-down">All Topics</span>
                  (<a href="#" id="article-hide-all-topics">hide</a>)
                </h1>
                <ul>
                  
                  <li>
                    
                    <a href="../../topic/accessibility/all/index.php">Accessibility</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/agile/all/index.php">Agile</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/analytics-and-metrics/all/index.php">Analytics &amp; Metrics</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/applications/all/index.php">Application Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/b2b-websites/all/index.php">B2B Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/behavior-patterns/all/index.php">Behavior Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/branding/all/index.php">Branding</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/content-strategy/all/index.php">Content Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/corporate-websites/all/index.php">Corporate Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/design-patterns/all/index.php">Design Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-design-process/all/index.php">Design Process</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/email/all/index.php">Email</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/eyetracking/all/index.php">Eyetracking</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/heuristic-evaluation/all/index.php">Heuristic Evaluation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/human-computer-interaction/all/index.php">Human Computer Interaction</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-ideation/all/index.php">Ideation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/information-architecture/all/index.php">Information Architecture</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/interaction-design/all/index.php">Interaction Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/international-users/all/index.php">International Users</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/intranets/all/index.php">Intranets</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-management/all/index.php">Management</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/navigation/all/index.php">Navigation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/non-profit-websites/all/index.php">Non-Profit Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/personas/all/index.php">Personas</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/persuasive-design/all/index.php">Persuasive Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/prototyping/all/index.php">Prototyping</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/psychology-and-ux/all/index.php">Psychology and UX</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/research-methods/all/index.php">Research Methods</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/search/all/index.php">Search</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/social-media/all/index.php">Social Media</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/strategy/all/index.php">Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/user-testing/all/index.php">User Testing</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-teams/all/index.php">UX Teams</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/visual-design/all/index.php">Visual Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/writing-web/all/index.php">Writing for the Web</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/young-users/all/index.php">Young Users</a>
                    
                  </li>
                  
                </ul>
                
              </section>
            </div>
            <script>
              /* Determine which topics list will be displayed: the full or the
               * partial.  We are inlining this close to the list itself to
               * prevent rendering delays on the client
               */
              var topic_list = $.cookie("article-topics");

              var popular_topics = $('#article-popular-topics');
              var all_topics = $('#article-all-topics');

              if(topic_list == 1) {
                  popular_topics.hide();
                  all_topics.show();
              } else {
                  popular_topics.show();
                  all_topics.hide();
              }
            </script>
            

            
            <div class="small-6 medium-12 columns">
              
              <section id="article-initial-authors">
                  <h1>Author</h1>
                  <ul>
                    
                    <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                    
                    <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                    
                    <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                    
                    <li><a href="#" onClick="$('#article-initial-authors').hide(); $('#article-all-authors').show();"><strong>See all authors&hellip;</strong></a></li>
                  </ul>
              </section>
              <section id="article-all-authors" style="display: none;">
                <h1>
                  All Authors
                  (<a href="#" id="article-hide-all-authors">hide</a>)
                </h1>
                <ul>
                  
                  <li><a href="../author/raluca-budiu/index.php">Raluca Budiu</a></li>
                  
                  <li><a href="../author/patty-caya/index.php">Patty Caya</a></li>
                  
                  <li><a href="../author/susan-farrell/index.php">Susan Farrell</a></li>
                  
                  <li><a href="../author/therese-fessenden/index.php">Therese Fessenden</a></li>
                  
                  <li><a href="../author/kim-flaherty/index.php">Kim Flaherty</a></li>
                  
                  <li><a href="../author/sarah-gibbons/index.php">Sarah Gibbons</a></li>
                  
                  <li><a href="../author/aurora-harley/index.php">Aurora Harley</a></li>
                  
                  <li><a href="../author/kate-kaplan/index.php">Kate Kaplan</a></li>
                  
                  <li><a href="../author/page-laubheimer/index.php">Page Laubheimer</a></li>
                  
                  <li><a href="../author/angela-li/index.php">Angie Li</a></li>
                  
                  <li><a href="../author/hoa-loranger/index.php">Hoa Loranger</a></li>
                  
                  <li><a href="../author/kate-meyer/index.php">Kate Meyer</a></li>
                  
                  <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                  
                  <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                  
                  <li><a href="../author/kara-pernice/index.php">Kara Pernice</a></li>
                  
                  <li><a href="../author/christian-rohrer/index.php">Christian Rohrer</a></li>
                  
                  <li><a href="../author/amy-schade/index.php">Amy Schade</a></li>
                  
                  <li><a href="../author/katie-sherwin/index.php">Katie Sherwin</a></li>
                  
                  <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                  
                  <li><a href="../author/kathryn-whitenton/index.php">Kathryn Whitenton</a></li>
                  
                </ul>
              </section>
              
            </div>
            
          </div>

          
          <section class="hide-for-small-down">
            
              
              <h1>Recent Articles</h1>
              
              <ul>
                  
                  <li><a href="../seamless-cross-channel/index.php">Seamlessness in the Omnichannel User Experience</a></li>
                  
                  <li><a href="../brand-intention-interpretation/index.php">Experience Design: Bridging Brand Intention and Brand Interpretation</a></li>
                  
                  <li><a href="../eyetracking-tasks-efficient-scanning/index.php">Scanning Patterns on the Web Are Optimized for the Current Task</a></li>
                  
                  <li><a href="../flat-design-best-practices/index.php">Flat-Design Best Practices</a></li>
                  
                  <li><a href="../ux-user-stories/index.php">Accounting for UX Work with User Stories in Agile Projects</a></li>
                  
                  <li><a href="../index.php"><strong>See all articles&hellip;</strong></a></li>
              </ul>
              
            
          </section>
          

          
          
          <section class="hide-for-small-down">
            <h1>Popular Articles</h1>
            
            <ul>
                
                <li><a href="../usability-101-introduction-to-usability/index.php">Usability 101: Introduction to Usability</a></li>
                
                <li><a href="../top-10-mistakes-web-design/index.php">Top 10 Mistakes in Web Design</a></li>
                
                <li><a href="../how-users-read-on-the-web/index.php">How Users Read on the Web</a></li>
                
                <li><a href="../f-shaped-pattern-reading-web-content/index.php">F-Shaped Pattern For Reading Web Content</a></li>
                
                <li><a href="../ten-usability-heuristics/index.php">10 Usability Heuristics for User Interface Design</a></li>
                
                <li><a href="../intranet-design-2016/index.php">10 Best Intranets of 2016</a></li>
                
                <li><a href="../which-ux-research-methods/index.php">When to Use Which User-Experience Research Methods</a></li>
                
                <li><a href="../response-times-3-important-limits/index.php">Response Times: The 3 Important Limits</a></li>
                
                <li><a href="../why-you-only-need-to-test-with-5-users/index.php">Why You Only Need to Test with 5 Users</a></li>
                
                <li><a href="../page-fold-manifesto/index.php">The Fold Manifesto: Why the Page Fold Still Matters</a></li>
                
            </ul>
          </section>
          
          

          
            <section class="hide-for-small-down">
              
            </section>
          

      </section>
      

      
        <div class="small-12 medium-9 columns content-body">
          




<article class="l-subsection alertbox-article-content article-detail-bl">
  
  <header>
    <div class="l-subsection">
      <h1>Smartwatches Are the Future—But Samsung Galaxy Gear Only Partway There</h1>
      <div class="author-meta">by
        
          
            
            <a href="../author/raluca-budiu/index.php">Raluca Budiu</a>
            
          
        on  December 8, 2013
        
      </div>

      

      
      <div class="topic-links">
        Topics:
        
          <ul>

  <li><a href="../../topic/mobile-and-tablet-design/index.php">Mobile &amp; Tablet</a></li>

</ul>
        
      </div>
      
    </div>
  </header>
  

  
  
  <section>
    <p class="lede"><strong>Summary:</strong> The Samsung Galaxy Gear smartwatch poses unique problems due to the tiny touchscreen. The use of gestures and streamlining content are reasonable solutions, but need to be implemented in a more usable manner.</p>
    <br />
  </section>
  
  

  
  <section id="article-body"><p>The computer watch (or smartwatch) will be the <strong>next important platform</strong>, extending the <a href="../transmedia-design-for-3-screens/index.php">range of screen sizes to design for</a> down one notch: desktop, tablet, phone, and now the watch. Or, really, we should say "soon" the watch, because <strong>current smartwatches are not there</strong> yet.</p>

<p>The 2 takeaways from this article are:</p>

<ul>
	<li><strong>Consumer </strong>advice: don't buy the Samsung Gear. Not useful enough and poor usability. Wait for the next generation before getting a watch computer.</li>
	<li><strong>Business</strong> advice: do buy one, to support planning what services (if any) you should offer for the watch platform(s) in a year or so, when better devices become available.</li>
</ul>

<p>Recently I bought a new watch to accompany my new Android smartphone: the <strong>Samsung Galaxy Gear</strong> smartwatch. The Samsung Gear is a fine addition to a James Bond-like arsenal of tech gadgets and comes in handy if you are eager to strike conversations with strangers in airports or elsewhere.</p>

<p>Maybe it’s no coincidence that the Samsung watch has been first released as a companion to the Samsung Galaxy Note phablet. This phone is a large, 6-inch phone that, as the AT&amp;T salesman pointed out, can be quite inconvenient to take out of the pocket or purse. For quick tasks such as checking text messages, answering a phone call, and taking a picture, users may spare themselves a workout and use the smartwatch instead. (A camera and a microphone are embedded in the watch wristband.)</p>

<p>In theory, the smartwatch could be a nice complement to a smartphone (as my colleague Bruce ‘Tog’ Tognazzini  points out in <a href="http://asktog.com/atc/apple-iwatch/">his article on the iWatch</a>). Does the Samsung watch live up to the hype?</p>

<h2>Functionality</h2>

<p>Samsung provides a small number of apps that come with the watch, and a few other 3rd-party apps are offered for download. Needless to say, it’s the range of apps available that will determine the utility of the watch.</p>

<p>Here are some of the apps that come preinstalled with the Gear:</p>

<ul>
	<li>Notifications, which you can configure for events such as getting new emails or text messages</li>
	<li>S-Voice, a voice recognition app that lets you send a text message, call someone, check your schedule, find the weather, and a few other things, but, notably, does not let you search the web</li>
	<li>dialer and contacts apps, that allow users to call from their watch</li>
	<li>Gallery, a photo gallery of pictures taken with the watch camera</li>
	<li>Media Controller, a music player</li>
	<li>Pedometer</li>
	<li>Timer and stopwatch apps</li>
	<li>a calendar app that displays the schedule for the day</li>
	<li>a voice-memo app for recording voice memos</li>
	<li>a weather app for getting the weather in your favorite city</li>
</ul>

<p>Besides these apps, you can install a few more using the companion phone. The most notable are:</p>

<ul>
	<li>eBay, an app that display notifications for various eBay events (e.g., auction ending soon, being outbid on an item)</li>
	<li>Evernote, a simplified version of Evernote  that lets users store pictures or voice memos to their Evernote account</li>
	<li>FBQuickview, a very minimalistic  Facebook app</li>
	<li>Glympse, a location-sharing app</li>
	<li>My Fitness Pal, an app that helps you keep track of your caloric intake</li>
	<li>Pocket, which gives you access to the stories that you saved</li>
	<li>Vivino, an app that scans wine labels and provides additional information about the wine</li>
	<li>Zite, a news reader</li>
</ul>

<p>The various apps for the smartwatch are installed and managed through an application called Gear Manager, which runs on the companion smartphone. The Gear Manager lets users install Gear apps, decide what apps get prioritized on the Gear screen and which apps can send notifications to the smartwatch.  Samsung took the right decision in forcing users to do customization on the phone: the watch screen is too small to support even the fairly simple information architecture that characterizes most phone apps.</p>

<div><img alt="" height="622" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2013/12/03/gear-manager.png" width="350"/>
<p><em>The Gear Manager is a phone app that supports watch customization and the installation of new apps.</em></p>
</div>

<p>When the phone and the watch are in close proximity, the phone is automatically unlocked. When the phone can't detect the watch, the user needs to enter her password to unlock the phone. Entering passwords on mobile is notoriously difficult <a href="../stop-password-masking/index.php">for many reasons</a>, and this feature, while very useful, is just a small first step toward making <strong>authentication</strong> easy on mobile devices.  Let’s hope that other apps will use the watch as an easy way to log in—maybe by taking advantage of passwords stored in the watch, as Tog suggests in his article.</p>

<p>Another useful functionality is <em>Find my phone</em> (provided that you are within the Bluetooth range); a symmetrical <em>Find my Gear</em> makes the watch beep if you have the phone but not the watch. (The entire idea of a watch-sized computer is that you strap it to your body to have the device available at all times, so hopefully you won't mislay it too often.)</p>

<p>Receiving a text message or an email triggers a <strong>notification</strong> on the watch. Text messages are fully displayed on the Gear screen (anecdotally, this feature seems to make the watch popular among teenagers who need an unconspicuous way to keep up to date with incoming text messages in class — although replying to these messages remains problematic if you must be silent).  But emails are not shown, at least not if you use the Gmail app:  your smartwatch screen may indicate just that you got a new mail in Gmail, but it will not display the subject or the sender of that email. However, if you picked up your phone immediately after that notification was received, you’d be immediately taken to the email app.</p>

<div><img alt="" height="200" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2013/12/03/gmail-notifications.png" width="438"/>
<p><em>The watch lets you know that a new mail was received by Gmail, but does not display any of its contents.</em></p>
</div>

<p>If you choose to use the Email app instead of Gmail, you can, however, see the sender, subject, and the first few lines of the message on the watch, and you can even erase it. Again, you can have that messaged opened on your phone right away, without having to navigate to the phone Email app.</p>

<div><img alt="" height="633" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2013/12/04/email.png" width="437"/>
<p><em>If you allow your Email app on the phone to send notifications to your watch, you can see a list of messages (left) with no sender displayed. Tapping on the message leads to a detail view (right), where you can read the sender, subject, and the first few words of the email message. Enough to decide if you need to read it (or erase it).</em></p>
</div>

<p>Clearly, usability of watch-based email requires the watch to display enough information about a new message to allow the user to determine whether it's worth the trouble to pull out the phone to read the message. Just saying the equivalent of "you've got mail" isn't enough. In general, any application that requires the user to transition between devices should provide enough <a href="../information-scent/index.php">information scent</a> on the first device to allow users to determine whether to go to the second device now or later.</p>

<p>Other apps such as eBay, Facebook, and Glympse can also send notifications to the watch. The content available in these notifications varies in complexity; for instance, the Glympse notification gives access to a full map, but the map is static: users cannot zoom in or move around it.</p>

<div><img alt="" height="200" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2013/12/03/device-2013-11-26-233523.png" width="200"/>
<p><em>Glympse shows a location map —either yours (if you want to share your current location with someone else), or the location of a friend who sent you a “Glympse” (i.e., a map with their location). However, you cannot zoom or navigate through the map using the watch (but you could do so on your phone). A plain map like the one shown here may often have too little context to be understood. Even overlaying the map with the city name and/or the neighborhood could help.</em></p>
</div>

<h2>Gestures: A Part of the Interface</h2>

<p>With the advent of the touchscreen smartphones such as the iPhone, we thought that we could forget almost everything we knew about feature-phone design and focus on the larger-screen devices.  Guess what? The small smartwatch screen takes us back in time to the feature-phone era. Its 1.2-inch screen is tiny compared with modern smartphones, but pretty standard for basic feature phones of yester year.  (And it is quite huge for a watch, making it less appealing to those with small hands.)</p>

<p>These are 2 big differences, however: unlike feature phones, the smartwatch has a touchscreen, but no keyboard. (Well, that’s not entirely true — the phone app does have a soft numerical keypad that allows dialing a new number.) Inputting information into the watch is done either by voice or by using the camera.  That seriously restricts the types of possible apps and their complexity.</p>

<p>Because the screen is so small, there is little room for displaying content, and even less room for interface widgets. Smartwatches, with their tiny screen space, are the natural application for gestural interfaces: by getting rid of the interface controls and replacing them with gestures, designers can take full advantage if the limited screen real estate. However, as described in <a href="http://www.jnd.org/dn.mss/gestural_interfaces.php">this article</a> (at jnd.org), gestures suffer from many problems, discoverability and memorability being the most serious ones.</p>

<p>The swipe is the primary way of navigating through Gear apps:</p>

<ol>
	<li>Swiping left or right on the vertical edges moves back and forth in the app space (we count these 2 options as one gesture, because the left–right mapping to a linear progression is a direct mapping where left and right swipes are each others' natural opposite).</li>
</ol>

<div><img alt="" height="201" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2013/12/04/watch-horiz-swipe.png" width="438"/>
<p><em>Swiping horizontally on the homescreen loops through the list of available apps.</em></p>
</div>

<ol start="2">
	<li>Swiping up takes the user back to the previous screen.</li>
	<li>In many apps, horizontal swiping is the way to move from one page to the next.  The gestures take a while to discover, because the watch touchscreen is not particularly sensitive (perhaps rightly so, to avoid accidental touches).</li>
</ol>

<div><img alt="" height="200" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2013/12/03/within-app-swipe.png" width="438"/>
<p><em>The Settings app allows users to select the type of information displayed on the homescreen. Users can swipe horizontally between the various available options.</em></p>
</div>

<ol start="4">
	<li>Swiping down on the homescreen from the top edge provides quick access to the camera (or some other app that you may choose using the Gear Manager on your phone).</li>
	<li>Swiping up from the bottom edge on the homescreen takes the user to the phone app.</li>
	<li>Double tapping the power button (the only physical button on the watch) opens the voice app.</li>
	<li>Pressing and holding any screen with 2 fingers brings up the list of recent apps.</li>
	<li>And double tapping with 2 fingers on any screen shows a control panel displaying the battery status and the volume and brightness controls.</li>
</ol>

<div><img alt="" height="200" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2013/12/03/double-tapping.png" width="438"/>
<p><em>Double tapping displays the brightness and volume controls, as well as the battery and bluetooth status.</em></p>
</div>

<p>Are you confused yet?</p>

<ol start="9">
	<li>Beside these touchscreen gestures, the Gear can be woken up if you raise your arm. In practice, this gesture is difficult to execute correctly, given the sensitivity of the sensors — many times I ended up thrusting my arm violently to no effect.   Better not stand next to a Gear user if you don't want a punch in the nose.</li>
</ol>

<p>That is <strong>a lot to remember</strong>. The different meaning of swiping on the homescreen compared to the other screens goes against <a href="../overloaded-vs-generic-commands/index.php">our guidelines for contextual gestures and command overload</a>. (We got proof of how disastrous changing the meaning of a gesture depending on the context can be when <a href="../windows-8-disappointing-usability/index.php">we tested the first generation of Windows 8</a>—users did not realize that the same gesture could expose different controls depending on which page they were on. <strong>Swipe ambiguity</strong>, most recently <a href="../ios-7/index.php">discussed in the context of iOS7</a>, is another case where assigning different meanings to the same gestures —this time on the same page—trips users.) And the 2-finger gestures are really unfamiliar.</p>

<div><img alt="" height="622" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2013/12/03/device-2013-11-26-135126-instructions.png" width="350"/>
<p><em>Different gestures supported by the Samsung smartwatch, as described in the associated Gear Manager running on the companion Android phone.</em></p>
</div>

<p>There are, however, arguments in favor of using this multitude of gestures: (1) they save precious screen space; (2) users are not forced to use all of them; in fact, some (e.g., the swipes on the homescreen) are redundant—that is, people can access the camera, the phone, and the watch settings in a more roundabout way if they do not remember these gestures; (3) these gestures are ingrained in the operating-system interface, so people will (presumably) get to make them often and thus have many learning opportunities.</p>

<p>Unfortunately, although there are only a few apps available for the Samsung Gear (most of them provided by Samsung), some of these apps do not use the standard gestures in standard ways, destroying all hope of having users learn the gestures. For instance, in the Pedometer app (as well as in most apps), swiping down on the top edge takes the user back to the previous screen. However, in the S-Voice app, this gesture doesn’t work; instead, users have to swipe horizontally on the left edge to navigate back.</p>

<div><img alt="" height="200" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2013/12/03/svoice-transition.png" width="514"/>
<p><em>In the S-voice app, swiping down on the top edge to go back does not work.</em></p>
</div>

<h2>Content</h2>

<p>The watch-screen size seriously limits the amount of content that can be displayed at once on the screen.  Yet many apps waste screen space with fat sticky headings that take up almost a quarter of the screen. The Glympse screenshot above is one example.  Other examples include the <em>Help</em> screen in the Pedometer app, deleting emails in the Notifications app, or the screen for selecting sounds for notifications, in the Settings app. (Note also how the different sounds for notifications in the Settings app are eloquently called <em>Notification1</em>, <em>Notification2</em>. And if a heading must be used, better choose one that is descriptive—“Notification sounds”?)</p>

<div>
<p><img alt="" height="200" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2013/12/03/fat-header.png" width="438"/></p>

<p><img alt="" height="200" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2013/12/03/device-2013-11-27-204235.png" width="200"/></p>

<p><em>In all these screenshots the headings (and the buttons, where applicable) are sticky [don't scroll]. Whereas you want the buttons to be sticky at the bottom of the screen (simply because the list may be too long and you don’t want to force users to scroll down through many tiny screens all the way to the bottom of  the page to find the controls), the headers add less information, and could easily be scrolled off the screen. (The argument for the headings being persistent is that users may get interrupted, switch attention to something in the environment, and forget what they were doing when they came back. That’s true, but, most likely, the title won’t be enough in those situations anyhow. )</em></p>
</div>

<p>We seem doomed to rediscover the same usability guidelines again and again, as each new generation of designers commit the same blunders as their forebears.  In <a href="../wap-mobile-phones-field-study-findings/index.php">our testing of WAP phones</a> 13 years ago, we discovered usability problems from screens that provided too little new information because they allocated too much space to repeated information. As further discussed in <a href="../../courses/ia-2/%5d/index.php">our seminar on navigation design</a>, it is definitely a guideline on big screens to situate people through confirmations of the user's current location and recent navigational actions. But as screens get smaller and smaller, such situating context has to be reduced and a bigger share of the screen spent on content.</p>

<p>Another problem related to the screen size is that content sometimes is truncated. In the Contacts app, the names of the contacts get truncated, perhaps understandably so. What’s less acceptable is that you cannot see the full name even when you select a contact by tapping on it.</p>

<div><img alt="" height="200" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2013/12/03/contacts.png" width="438"/>
<p><em>Contacts app for Gear truncates some of the contact names.(Phone numbers are blurred for privacy reasons.)</em></p>
</div>

<p>If you want to kill time and you are too lazy to take out your phone, you can quickly scan through the news of the day using Zite. Zite shows the title of the news story, and, or, sometimes, only part of that title. Even though people are unlikely to read long text on a small screen, it would be more efficient if they could at least read the full title. Tapping on the story just brings up a message that tells the user that the story is being opened in the mobile device. But there’s no way of saving the story so you can read it later if using your phone is not feasible or convenient— in violation of the l<strong>aw of minimum device switches.</strong> (Device switches have a high <a href="../interaction-cost-definition/index.php">interaction cost</a>; so designers should attempt to avoid them whenever possible.)</p>

<p>Why not show the full title, a button that saves the story in Zite for later reading, and then tell people that the story is being opened in the phone?  Note how the Zite logo wastes the top third of the screen; also, the busy image background, while pretty, can make reading harder, especially in the variable lighting conditions in which the watch is likely to be used.</p>

<div><em><img alt="" height="200" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2013/12/03/zite.png" width="438"/> </em>

<p><em><em>Zite truncates story titles, but wastes a third of the screen space with their logo. It also does not allow users to save the article for later reading (on the phone).</em></em></p>
</div>

<p>The app Pocket has an interesting solution to the problem of presenting content on the small screen: instead of having you read the content, it reads it to you. The phone transmits the sound, so if you want to listen to the stories that you saved while, say, exercising, you can do that easily, provided that you wear headphones connected to your phone. The assumption is that it’s easy to glance at your watch and select a story, and less convenient to take out your (huge) phone and do so.</p>

<div><img alt="" height="200" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2013/12/03/device-2013-11-27-220555.png" width="200"/>   <img alt="" height="200" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2013/12/03/device-2013-11-27-221330.png" width="200"/>
<p><em><em>Pocket reads content from the webpages that you saved elsewhere. Note that design is slightly better than Zite’s: although it also shows an image, it uses a dark box as background for text. The “1 min” time mark in the first screenshot is completely useless (and wrong): this is a 1-year old story.  Like Zite, it truncates long titles, although there is plenty of space to display them.</em></em></p>
</div>

<h2>Interaction</h2>

<p>Much of the interaction with such a tiny small screen is done via gestures— there is just too little space for controls. Still, every now and then users do need to input information or press buttons.</p>

<p>The other two ways of entering information are the camera and the voice. S-Voice lets users send text messages and open apps using voice. (Surprisingly, though, the other apps that I’ve looked at don’t use voice as an input method.) Apps such as Vivino and My Fitness Pal use the camera to let people search for products (wines, in the case of Vivino, and foods, in the case of My Fitness Pal).</p>

<div><img alt="" height="200" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2013/12/03/input.png" width="437"/>
<p><em>My Fitness Pal (left) and Vivino (right) allow users to scan food barcodes and, respectively, wine labels to enter information into the app.</em></p>
</div>

<p>Sliders have their moment of glory with smartwatches: normally, any quantity is tediously entered with a slider (and we tend to recommend against them on desktops and on mobile devices; typing is often quicker and more precise), but in the absence of a keypad, they’re as good as it gets. In fact, they are much preferable to scanning through long lists of options.</p>

<div><img alt="" height="200" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2013/12/03/sliders.png" width="438"/>
<p><em>Sliders (in the Pedometer app on left) are easier than scrolling through long lists on a tiny screen (in the Vivino app on the right).</em></p>
</div>

<p>In the Contacts app, a different type of slider is used for navigation. The letters on the right side of the screen are pretty much the equivalent of a scroll bar: they allow users to move to random regions of the name space (the targets are too small and crowded to be accurately reachable, and it’s unlikely that people would have any special predilection for names starting with one of the letters “A”, “M”, “N”, or “Z”). What’s more disappointing is that the displayed letters are the same independently of where the user is in the scroll space.</p>

<div><img alt="" height="200" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2013/12/03/device-2013-11-26-225827.png" width="200"/>
<p><em>As users move up or down in the scroll space, the letters on the right stay unchanged. The users can essentially move to the first or last half of the alphabet, but then they have to scroll to reach to the desired letter.</em></p>
</div>

<p>The tiny letters on the side of the Contacts app bring us to the question of target size. The Gear is a touchscreen device, and hence it does need fat targets suited for fat fingers. We normally <a href="../../courses/mobile-apps-touchscreens/index.php">recommend 1cmx1cm as the minimum touch-target size</a>.However, the Gear’s screen is only 1.2 inches X 1.2 inches (or 3cm X 3cm); if you have two buttons you’ve taken up 22% of the screen area. (Sometimes even more, as in the slider screenshot above.)</p>

<p>So what does that mean? Can you get away with smaller targets? No, because our fingers don't get any smaller just because they're touching a watch instead of a phone.</p>

<p>Sometimes you may be able to pad the targets, in order to accommodate extra content. For instance, in the Vivino app, an edit button appears in the top right corner, next to the name of the wine. The name of the wine acts as a page title (and is not clickable or sticky), and the target is padded, so that if you can touch nearby and still trigger the edit action. (However, padding is not perfect because users do not usually know about it; according to Fitts’ law, the time they’ll take to reach the target will still be longer than if the target was bigger —see <a href="../../courses/hci/index.php">our class on Human-Computer Interface</a> for a discussion of Fitts’ law.)</p>

<div><img alt="" height="200" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2013/12/03/device-2013-11-27-223338.png" width="200"/>
<p><em>Vivino app has a small edit button in the top corner. The button is about a quarter of our recommended target area, but it works because the target is padded and there are no other targets nearby. In the same screenshot, the stars are smaller than the recommended target area and also close to each other, but hitting the wrong one can be easily corrected by sliding the finger left or right. It is, of course, a solution highly specific to ratings.</em></p>
</div>

<p>Other strategies that can be used to make it easy for people to reach targets is to mind Fitts’ law and place them at corners or around edges (not hard, if you consider the screen size) , and to avoid having more than 1–2 targets on a screen. Otherwise, you may end up with too little content and many accidental touches.</p>

<h2>Should You Design for Smartwatches?</h2>

<p>When the iPad first came out in 2010, some were wondering what it was for. Would people find a use for it? After all, everything it offered was already available on your laptop or on your phone, only the laptop had a bigger screen and a real keyboard and the phone was more portable. Yet, tablets have shown tremendous growth over the past 3 years, and have proven to be a convenience device: You can live without them, but life is in fact easier with them.</p>

<p>Will the smartwatches prove to be the next platform, just like tablets provided a new platform to occupy the middle of the design space between desktops and phones? With a smartwatch, you can do some of the tasks you now do on your phone, only your phone is better and bigger. Smartwatches are a convenience device, like tablets. But will they simplify our lives enough? The answer is in the apps that we can invent for them.</p>

<p>One big reason to believe in watches is that this form factor has already been victorious. Historically, before wristwatches were invented, many people carried pocket watches. But the time needed to fish a device out of your pocket was much more than the time needed to whip around your wrist to face your face. Wristwatches are a much faster way to check the time (and maybe date) than using a pocket watch. As a result, you hardly ever see a pocket watch these days.</p>

<p>In the case of computers, the wrist computer (smartwatch) will not eradicate the pocket computer (mobile phone) the way wristwatches eradicated pocket watches, because a phone-sized screen can do so much more than a watch-sized screen. Most likely people will carry both.</p>

<p>The key is simplicity (you can do it more easily on a watch) and spontaneity (you can do it right away). Phones already encourage spontaneous use; watches will be for those moments when phones are too big and too slow to access. We’ll need to learn ways to make the apps more direct and distill their essence, so that a quick glimpse on a tiny screen will be enough to get what we need.</p>
</section>
  

  
  <section>
    
        
    
  </section>
  
  
  
  
  

  


  <section>
    <br />
    <p class="share"><b>Share <span class="hide-for-small-down">this article</span>:</b> <a href="https://twitter.com/intent/tweet?url=http://www.nngroup.com/articles/samsung-watch/&amp;text=Smartwatches%20Are%20the%20Future%e2%80%94But%20Samsung%20Galaxy%20Gear%20Only%20Partway%20There&amp;via=nngroup">Twitter</a> | <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=http://www.nngroup.com/articles/samsung-watch/&amp;title=Smartwatches%20Are%20the%20Future%e2%80%94But%20Samsung%20Galaxy%20Gear%20Only%20Partway%20There&amp;source=Nielsen%20Norman%20Group">LinkedIn</a> | <a href="https://plus.google.com/share?url=http://www.nngroup.com/articles/samsung-watch/">Google+</a> | <a href="mailto:?subject=NN/g Article: Smartwatches Are the Future—But Samsung Galaxy Gear Only Partway There&amp;body=http://www.nngroup.com/articles/samsung-watch/">Email</a></p>
    <hr />
  </section>

  <footer class="l-subsection">
    
    
    <div class="learn-more">
      <h2>Learn More</h2>
      <p><a href="../subscribe/index.php">Subscribe to the weekly newsletter</a> to get notified about future articles.</p>
      </div>
    
    

    <div class="row show-for-small-down related-content">
      
      
      

      
      
        <div class="column small-12 topic-links">All articles about:
          <ul>
            
            <li><a href="../../topic/mobile-and-tablet-design/index.php">Mobile &amp; Tablet</a></li>
            
            <li><a href="../../topic/technology/index.php">new technologies</a></li>
            
            <li><a href="../../topic/smartwatches/index.php">smartwatches</a></li>
            
          </ul>
        </div>
      
      

      
      
        <div class="column small-12">
          <h3>UX Conference Training Courses</h3>
           <ul class="no-bullet">
    
        <li><a href="../../courses/cross-channel-user-experience/index.php">Omnichannel Journeys and Customer Experience</a></li>
    
        <li><a href="../../courses/emerging-patterns-web-design/index.php">Emerging Patterns for Web Design</a></li>
    
        <li><a href="../../courses/usability-mobile-websites-apps/index.php">Mobile User Experience</a></li>
    
        <li><a href="../../courses/scaling-responsive-design/index.php">Scaling User Interfaces</a></li>
    
        <li>Visual Design for Mobile and Tablet: <a href="../../courses/vis-mob-1/index.php">Day 1</a> and <a href="../../courses/vis-mob-2/index.php">Day 2</a></li>
    
</ul>
        </div>
      
      

      
      
        <div class="small-12 column">
          <h3>Research Reports</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../reports/tablets/index.php">Tablet Website and Application UX</a></li>
              
            
              
                <li><a href="../../reports/mobile-website-and-application-usability/index.php">User Experience for Mobile Applications and Websites</a></li>
              
            
              
                <li><a href="../../reports/enterprise-mobile-showcase/index.php">Mobile Intranets and Enterprise Apps</a></li>
              
            
              
                <li><a href="../../reports/wap-usability/index.php">WAP Usability</a></li>
              
            
              
                <li><a href="../../reports/ipad-app-and-website-usability/index.php">iPad App and Website Usability</a></li>
              
            
          </ul>
        </div>
      
      

      
      
        <div class="small-12 column">
          <h3>Online Seminars</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../online-seminars/beyond-screens/index.php">Beyond Screens</a></li>
              
            
              
                <li><a href="../../online-seminars/every-word-count/index.php">Making Every Word Count</a></li>
              
            
              
                <li><a href="../../online-seminars/mobile-user-testing/index.php">Mobile User Testing</a></li>
              
            
          </ul>
        </div>
      
      
    </div>

    <div class="hide-for-small-down row related-content">
      <div class="medium-6 columns">
        
        
          <div>
            <h3>Articles</h3>
            <ul class="no-bullet">
              
                
                <li><a href="../smartwatch/index.php">The Apple Watch: User-Experience Appraisal</a></li>
                
              
                
                <li><a href="../mobile-content/index.php">Reading Content on Mobile Devices</a></li>
                
              
                
                <li><a href="../mobile-behavior-india/index.php">Mobile User Behavior in India</a></li>
                
              
                
                <li><a href="../augmented-reality-ux/index.php">Augmented Reality: What Does It Mean for UX?</a></li>
                
              
                
                <li><a href="../mobile-navigation-patterns/index.php">Basic Patterns for Mobile Navigation: A Primer</a></li>
                
              
            </ul>
          </div>
        
        

        
          
          <div>
            <h3>Research Reports</h3>
            <ul class="no-bullet">
              
                
                  <li><a href="../../reports/tablets/index.php">Tablet Website and Application UX</a></li>
                
              
                
                  <li><a href="../../reports/mobile-website-and-application-usability/index.php">User Experience for Mobile Applications and Websites</a></li>
                
              
                
                  <li><a href="../../reports/enterprise-mobile-showcase/index.php">Mobile Intranets and Enterprise Apps</a></li>
                
              
                
                  <li><a href="../../reports/wap-usability/index.php">WAP Usability</a></li>
                
              
                
                  <li><a href="../../reports/ipad-app-and-website-usability/index.php">iPad App and Website Usability</a></li>
                
              
            </ul>
          </div>
          
        

      </div>
      <div class="medium-6 columns">
        
          
          <div>
            <h3>UX Conference Training Courses</h3>
             <ul class="no-bullet">
    
        <li><a href="../../courses/cross-channel-user-experience/index.php">Omnichannel Journeys and Customer Experience</a></li>
    
        <li><a href="../../courses/emerging-patterns-web-design/index.php">Emerging Patterns for Web Design</a></li>
    
        <li><a href="../../courses/usability-mobile-websites-apps/index.php">Mobile User Experience</a></li>
    
        <li><a href="../../courses/scaling-responsive-design/index.php">Scaling User Interfaces</a></li>
    
        <li>Visual Design for Mobile and Tablet: <a href="../../courses/vis-mob-1/index.php">Day 1</a> and <a href="../../courses/vis-mob-2/index.php">Day 2</a></li>
    
</ul>
          </div>
          
        
        
          
          <div>
            <h3>Online Seminars</h3>
            <ul class="no-bullet">
            
                <li><a href="../../online-seminars/beyond-screens/index.php">Beyond Screens</a></li>
            
                <li><a href="../../online-seminars/every-word-count/index.php">Making Every Word Count</a></li>
            
                <li><a href="../../online-seminars/mobile-user-testing/index.php">Mobile User Testing</a></li>
            
            </ul>
          </div>
          
        
      </div>
    </div>
  </footer>
</article>

        </div>

      

    
  </div>
  <div class="row">
    
      <div class="column small-12 show-for-small-down l-navrail announcements">
        
          
        
      </div>
    
    
  </div>

    </div>
    




<footer id="page-footer">
    <div class="row hide-for-print">
        <div class="column small-12 medium-6">
            <section class="footer-popular">
                
                <h1>Popular Topics</h1>
                <div style="float: left;  width: 15em; display: block;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/applications/index.php">Application Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/e-commerce/index.php">E-Commerce Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/eyetracking/index.php">Eyetracking </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/information-architecture/index.php">Information Architecture </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/intranets/index.php">Intranet Design </a></li>
	<li><a href="../../topic/interaction-design/index.php">Interaction Design </a></li>
</ul>
</div>

<div style="float: left;  width: 15em; display: block; list-style: none;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/mobile-and-tablet-design/index.php">Mobile and Tablet Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/credibility-persuasion/index.php">Persuasive Design</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/research-methods/index.php">Research Methods</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/user-testing/index.php">User Testing </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/web-usability/index.php">Web Usability </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/writing-web/index.php">Web Writing &amp; Content Strategy </a></li>
</ul>
</div>

<p>&nbsp;</p>

                
            </section>
        </div>

        <div class="column small-12 medium-6">
            <div class="row">
                <div class="column small-6">
                    <section class="footer-about">
                        <h1>Follow Us</h1>
                        <ul>
	<li><a href="../subscribe/index.php">Subscribe to our weekly UX newsletter</a>,&nbsp;Jakob Nielsen&#39;s Alertbox</li>
	<li>Visit Don Norman&#39;s website, <a href="http://www.jnd.org/"> www.jnd.org </a></li>
	<li>Visit Bruce Tognazzini&#39;s website, <a href="http://www.asktog.com/"> www.asktog.com </a></li>
	<li style="padding-top: 6px;"><a class="twitter-follow-button" data-show-count="false" data-size="large" href="https://twitter.com/nngroup"><img alt="Follow @nngroup on Twitter" height="20" src="https://media.nngroup.com/media/editor/2016/12/30/twitter-follow-button.png" style="width: 117px; height: 20px;" width="117" /></a></li>
	<li style="padding-top: 6px;"><img alt="" height="14" src="https://media.nngroup.com/media/editor/2016/07/26/feed-icon-14x14.png" style="vertical-align:text-top;width:14px;margin-right:.25em;" width="14" /><a href="../../feed/rss/index.php">Subscribe to our RSS feed</a></li>
</ul>

                    </section>
                </div>

                <div class="column small-6">
                    <section class="footer-about right-column">
                        <h1>About
</h1>
                        <ul class="no-bullet">
	<li><a href="../../about/contact/index.php">Contact information</a></li>
	<li><a href="../../about/index.php">About Us</a></li>
	<li><a href="../../about/why-nng/index.php">Why NN/g? </a></li>
	<li><a href="../../return-policy/index.php">Return Policy</a></li>
</ul>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="copyright column small-12">
            <p><a href="../../copyright-and-reprint-info/index.php">Copyright</a> &copy; 1998-2017 Nielsen Norman Group, All Rights Reserved.</p>
        </div>
    </div>
</footer>


    


        <script src="https://media.nngroup.com/static/vendor/fastclick/js/fastclick.min.js"></script>
        <script src="https://media.nngroup.com/static/vendor/foundation/js/foundation.min.js"></script>
        <script src="http://media.nngroup.com/static/js/app.js"></script>
        <script src="http://media.nngroup.com/static/js/scroll-to-fixed.js"></script><script>var externalLinks = document.querySelectorAll('a[href^="http"]');for (var i = externalLinks.length-1; i >= 0; i--) {externalLinks[i].addEventListener("click", function(event) { alert("External links are disabled for this test."); event.preventDefault(); }, false);}var mailLinks = document.querySelectorAll('a[href^="mailto"]');for (var i = mailLinks.length-1; i >= 0; i--) {mailLinks[i].addEventListener("click", function(event) { alert("Email links are disabled for this test."); event.preventDefault(); }, false);}var externalForms = document.querySelectorAll('form');for (var i = externalForms.length-1; i >= 0; i--) {externalForms[i].addEventListener("submit", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false); externalForms[i].addEventListener("click", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false);}</script>

        <script>
            
            $(function() {
                $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
                $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
            });
        </script>

        
<script src="https://media.nngroup.com/static/js/articles.js"></script>


        
        <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1072699960;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072699960/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
    </body>

<!-- Mirrored from www.nngroup.com/articles/samsung-watch/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:10:30 GMT -->
</html>
