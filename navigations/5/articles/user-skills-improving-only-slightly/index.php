<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->





    
<!-- Mirrored from www.nngroup.com/articles/user-skills-improving-only-slightly/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:15:05 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1026.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"licenseKey":"1a30ef9931","errorBeacon":"bam.nr-data.net","applicationID":"2048992","transactionName":"NFxSMRBUX0EDUkIPDA0WdhAMVkVbDV8ZBxEXUFMJB0YfRAtUQRVZIktEDAFZVHYHRVcPD01eVRE=","beacon":"bam.nr-data.net","queueTime":3,"applicationTime":414,"agent":""}</script>
        <title>User Skills Improving, But Only Slightly</title><meta property="og:title" content="User Skills Improving, But Only Slightly" />
  
        
        <meta name="description" content="Users now do basic operations with confidence and perform with skill on sites they use often. But when users try new sites, well-known usability problems still cause failures.">
        <meta property="og:description" content="Users now do basic operations with confidence and perform with skill on sites they use often. But when users try new sites, well-known usability problems still cause failures." />
        
  
        
	
        
        <meta name="keywords" content="user skills, skilled performance, high-end users, test participants, B2B, business professionals, clicking, confidence, search, expert behavior, loyalty, frequent operations, reading skills, literacy, conceptual models, comparisons, tunnel vision, queries, query reformulation, search strategy, SERP, evaluating search results, email newsletter, newsletters, opening new browser windows, links, link color, scrollbars, interaction techniques, UI conventions, GUI standards, PDF, applications, selection, metaphor, usability guidelines, splash screens, intros, skip intro, Google gullibility, privacy, registration, information architecture, IA">
        

        

        <meta name="robots" content="noarchive">
        <meta name="viewport" content="width=device-width">

        <link rel="search" type="application/opensearchdescription+xml" title="NN/g Search" href="../../opensearch.xml" />
        <link rel="shortcut icon" href="http://media.nngroup.com/static/img/favicon.ico" />

        <link rel="stylesheet" href="https://media.nngroup.com/static/vendor/font-awesome/css/font-awesome.min.css">

        
        <link rel="stylesheet" href="https://media.nngroup.com/static/css/7232a960f1df.css" type="text/css" />
        <link rel="stylesheet" href="http://fast.fonts.com/t/1.css?apiType=css&amp;projectid=c5846260-9def-46ec-9407-0f1349bd2d64" type="text/css">

        <script src="https://media.nngroup.com/static/vendor/modernizr/js/modernizr.min.js"></script>

        <script type="text/javascript" src="https://media.nngroup.com/static/js/3283be5def74.js"></script>
        

        

        
        
        <!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=229034,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('http://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->


<!-- Google Analytics Tracking Code for http://nngroup.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23132681-1', 'nngroup.com');
  ga('require', 'linkid', 'https://www.nngroup.com/articles/user-skills-improving-only-slightly/linkid.js');
  ga('require', 'displayfeatures');

// Optimizely Universal Analytics Integration
 window.optimizely = window.optimizely || [];
 window.optimizely.push("activateUniversalAnalytics");

  ga('send', 'pageview');

</script>
<script>
/**
* Function to track a click on an outbound link in Google Analytics.
*/
function trackOutboundLink(label, url) {
    if(window.ga && ga.loaded) {
        ga('send', 'event', 'outbound', 'click', label, {'hitCallback': function(){document.location = url;}
        });
    } else {
        document.location = url;
    }
}
/**
* Function to track a click on a link in the article announcement area.
*/
$('#articleAnnouncement a').on('click', function(){
  ga('send', 'event', 'link', 'click', 'articleAnnouncement');
});
</script>

<script type="text/javascript">
 setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2421.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



<!-- Hotjar Tracking Code for http://nngroup.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:121605,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

        <script src="https://media.nngroup.com/static/js/analytics/track_outbound_link.js"></script>
        
        
        
    </head>
    <body class="location-articles articles-detail">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
    <header id="page-header">
        <div id="cart" class="shopping-cart mobile-cart show-for-small-down"></div>
        <div class="row">
            <div id="masthead" class="masthead small-12 medium-7 column">

                <!-- The button to toggles menu on small screen -->

                <h1><a href="../../index.php"><img src="http://media.nngroup.com/static/img/logo-wordmark.png" width="74" height="30"> Nielsen <span class="norman">Norman</span> Group</a></h1>
                <p class="tagline show-for-medium-up">Evidence-Based User Experience Research, Training, and Consulting</p>

                <!-- The button to toggles form search on small screen -->


                <div style="clear: right;width:  100%;height: 1px;"></div><?php define('ROOT_PATH',$_SERVER["DOCUMENT_ROOT"] . '/apps/navigation_styles/navigations/5'); ?><?php include(ROOT_PATH)."/navigation.php"; ?>

                <!-- Drop down menu on small screen -->
                <div id="hideNav" class="show-for-small-down">
                  
                  <ul class="menu-content">
                    <li><a href="../../index.php">Home</a></li>
                    <li class="has-submenu">
                      <a>Training</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../training/index.php">Overview</a></li>
                        
<li id="nav-ux-conference"><a href="../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../online-seminars/index.php"><span>Online Seminars</span></a></li>

                      </ul>
                    </li>
                    <li class="has-submenu">
                      <a>Consulting</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
  <li><a href="../../consulting/index.php">Overview</a></li>
  
  <li><a href="../../consulting/benchmarking/index.php">Benchmarking</a></li>
  
  <li><a href="../../consulting/expert-design-review/index.php">Expert Design Review</a></li>
  
  <li><a href="../../consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
  
  <li><a href="../../consulting/usability-testing/index.php">Usability Testing </a></li>
  
  <li><a href="../../consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
  
  <li><a href="../../in-house-training/index.php">In-House Training</a></li>
</ul>

                    </li>

                    <li><a href="../../reports/index.php">Reports</a></li>
                    <li><a href="../index.php">Articles</a></li>
                    <li class="has-submenu"><a>About NN/G</a><i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../about/index.php">Overview</a></li>
                        <li><a href="../../people/index.php">People</a></li>
                        <li><a href="../../about/why-nng/index.php">Why NN/g?</a></li>
                        <li><a href="../../about/contact/index.php">Contact</a></li>
                        <li><a href="../../news/index.php">News</a></li>
                        <li><a href="../../about/history/index.php">History</a></li>
                        <li><a href="../../books/index.php">Books</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                </div>

                <hr class="header-divider show-for-small-only"/>


            </div>
       </div>

        <div id="hideNav">            <nav class="primary-nav small-12 column">
                <ul>
                    <li><a class="navitem" id="nav-home" href="../../index.php">Home</a></li>
                    <li><a class="navitem" id="nav-training" href="../../training/index.php">Training</a></li>
                    <li><a class="navitem" id="nav-consulting" href="../../consulting/index.php">Consulting</a></li>
                    <li><a class="navitem" id="nav-reports" href="../../reports/index.php">Reports</a></li>
                    <li><a class="navitem" id="nav-articles" href="../index.php">Articles</a></li>
                    <li><a class="navitem" id="nav-about" href="../../about/index.php">About NN/G</a></li>
                </ul>
            </nav>
            <nav class="secondary-nav small-12 medium-12 large-12 columns">
                
                
            </nav>
        </div>
    </header>
    <div class="l-content"><span id="preventClose" style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776; Menu</span>
    
  

  

  <div class="row">
    
      
      <section class="small-12 medium-3 columns collapsable-content l-navrail articles-left-column">
        <h1 class="show-for-small-down collapse-link mobile-browse-header">Browse by Topic and Author</h1>
          <div class="row collapsable">

            
            <div class="small-6 medium-12 columns">
              <section id="article-popular-topics" style="display: none;">
                
                <h1>Topics</h1>
                <ul id="article-popular-topics">
                    
                    <li>
                        
                        <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/intranets/all/index.php">Intranets</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/user-testing/all/index.php">User Testing</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                        
                    </li>
                    
                    <li><a href="#" id="article-see-all-topics"><strong>See all topics&hellip;</strong></a></li>
                </ul>
                
              </section>
              <section id="article-all-topics" style="display: none;">
                
                <h1>
                  <span class="show-for-medium-up">All Article Topics</span>
                  <span class="show-for-small-down">All Topics</span>
                  (<a href="#" id="article-hide-all-topics">hide</a>)
                </h1>
                <ul>
                  
                  <li>
                    
                    <a href="../../topic/accessibility/all/index.php">Accessibility</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/agile/all/index.php">Agile</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/analytics-and-metrics/all/index.php">Analytics &amp; Metrics</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/applications/all/index.php">Application Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/b2b-websites/all/index.php">B2B Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/behavior-patterns/all/index.php">Behavior Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/branding/all/index.php">Branding</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/content-strategy/all/index.php">Content Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/corporate-websites/all/index.php">Corporate Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/design-patterns/all/index.php">Design Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-design-process/all/index.php">Design Process</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/email/all/index.php">Email</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/eyetracking/all/index.php">Eyetracking</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/heuristic-evaluation/all/index.php">Heuristic Evaluation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/human-computer-interaction/all/index.php">Human Computer Interaction</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-ideation/all/index.php">Ideation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/information-architecture/all/index.php">Information Architecture</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/interaction-design/all/index.php">Interaction Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/international-users/all/index.php">International Users</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/intranets/all/index.php">Intranets</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-management/all/index.php">Management</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/navigation/all/index.php">Navigation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/non-profit-websites/all/index.php">Non-Profit Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/personas/all/index.php">Personas</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/persuasive-design/all/index.php">Persuasive Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/prototyping/all/index.php">Prototyping</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/psychology-and-ux/all/index.php">Psychology and UX</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/research-methods/all/index.php">Research Methods</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/search/all/index.php">Search</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/social-media/all/index.php">Social Media</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/strategy/all/index.php">Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/user-testing/all/index.php">User Testing</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-teams/all/index.php">UX Teams</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/visual-design/all/index.php">Visual Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/writing-web/all/index.php">Writing for the Web</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/young-users/all/index.php">Young Users</a>
                    
                  </li>
                  
                </ul>
                
              </section>
            </div>
            <script>
              /* Determine which topics list will be displayed: the full or the
               * partial.  We are inlining this close to the list itself to
               * prevent rendering delays on the client
               */
              var topic_list = $.cookie("article-topics");

              var popular_topics = $('#article-popular-topics');
              var all_topics = $('#article-all-topics');

              if(topic_list == 1) {
                  popular_topics.hide();
                  all_topics.show();
              } else {
                  popular_topics.show();
                  all_topics.hide();
              }
            </script>
            

            
            <div class="small-6 medium-12 columns">
              
              <section id="article-initial-authors">
                  <h1>Author</h1>
                  <ul>
                    
                    <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                    
                    <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                    
                    <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                    
                    <li><a href="#" onClick="$('#article-initial-authors').hide(); $('#article-all-authors').show();"><strong>See all authors&hellip;</strong></a></li>
                  </ul>
              </section>
              <section id="article-all-authors" style="display: none;">
                <h1>
                  All Authors
                  (<a href="#" id="article-hide-all-authors">hide</a>)
                </h1>
                <ul>
                  
                  <li><a href="../author/raluca-budiu/index.php">Raluca Budiu</a></li>
                  
                  <li><a href="../author/patty-caya/index.php">Patty Caya</a></li>
                  
                  <li><a href="../author/susan-farrell/index.php">Susan Farrell</a></li>
                  
                  <li><a href="../author/therese-fessenden/index.php">Therese Fessenden</a></li>
                  
                  <li><a href="../author/kim-flaherty/index.php">Kim Flaherty</a></li>
                  
                  <li><a href="../author/sarah-gibbons/index.php">Sarah Gibbons</a></li>
                  
                  <li><a href="../author/aurora-harley/index.php">Aurora Harley</a></li>
                  
                  <li><a href="../author/kate-kaplan/index.php">Kate Kaplan</a></li>
                  
                  <li><a href="../author/page-laubheimer/index.php">Page Laubheimer</a></li>
                  
                  <li><a href="../author/angela-li/index.php">Angie Li</a></li>
                  
                  <li><a href="../author/hoa-loranger/index.php">Hoa Loranger</a></li>
                  
                  <li><a href="../author/kate-meyer/index.php">Kate Meyer</a></li>
                  
                  <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                  
                  <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                  
                  <li><a href="../author/kara-pernice/index.php">Kara Pernice</a></li>
                  
                  <li><a href="../author/christian-rohrer/index.php">Christian Rohrer</a></li>
                  
                  <li><a href="../author/amy-schade/index.php">Amy Schade</a></li>
                  
                  <li><a href="../author/katie-sherwin/index.php">Katie Sherwin</a></li>
                  
                  <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                  
                  <li><a href="../author/kathryn-whitenton/index.php">Kathryn Whitenton</a></li>
                  
                </ul>
              </section>
              
            </div>
            
          </div>

          
          <section class="hide-for-small-down">
            
              
              <h1>Recent Articles</h1>
              
              <ul>
                  
                  <li><a href="../seamless-cross-channel/index.php">Seamlessness in the Omnichannel User Experience</a></li>
                  
                  <li><a href="../brand-intention-interpretation/index.php">Experience Design: Bridging Brand Intention and Brand Interpretation</a></li>
                  
                  <li><a href="../eyetracking-tasks-efficient-scanning/index.php">Scanning Patterns on the Web Are Optimized for the Current Task</a></li>
                  
                  <li><a href="../flat-design-best-practices/index.php">Flat-Design Best Practices</a></li>
                  
                  <li><a href="../ux-user-stories/index.php">Accounting for UX Work with User Stories in Agile Projects</a></li>
                  
                  <li><a href="../index.php"><strong>See all articles&hellip;</strong></a></li>
              </ul>
              
            
          </section>
          

          
          
          <section class="hide-for-small-down">
            <h1>Popular Articles</h1>
            
            <ul>
                
                <li><a href="../usability-101-introduction-to-usability/index.php">Usability 101: Introduction to Usability</a></li>
                
                <li><a href="../top-10-mistakes-web-design/index.php">Top 10 Mistakes in Web Design</a></li>
                
                <li><a href="../how-users-read-on-the-web/index.php">How Users Read on the Web</a></li>
                
                <li><a href="../f-shaped-pattern-reading-web-content/index.php">F-Shaped Pattern For Reading Web Content</a></li>
                
                <li><a href="../ten-usability-heuristics/index.php">10 Usability Heuristics for User Interface Design</a></li>
                
                <li><a href="../intranet-design-2016/index.php">10 Best Intranets of 2016</a></li>
                
                <li><a href="../which-ux-research-methods/index.php">When to Use Which User-Experience Research Methods</a></li>
                
                <li><a href="../response-times-3-important-limits/index.php">Response Times: The 3 Important Limits</a></li>
                
                <li><a href="../why-you-only-need-to-test-with-5-users/index.php">Why You Only Need to Test with 5 Users</a></li>
                
                <li><a href="../page-fold-manifesto/index.php">The Fold Manifesto: Why the Page Fold Still Matters</a></li>
                
            </ul>
          </section>
          
          

          
            <section class="hide-for-small-down">
              
            </section>
          

      </section>
      

      
        <div class="small-12 medium-9 columns content-body">
          




<article class="l-subsection alertbox-article-content article-detail-bl">
  
  <header>
    <div class="l-subsection">
      <h1>User Skills Improving, But Only Slightly</h1>
      <div class="author-meta">by
        
          
            
            <a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a>
            
          
        on  February 4, 2008
        
      </div>

      

      
      <div class="topic-links">
        Topics:
        
          <ul>

  <li><a href="../../topic/web-usability/index.php">Web Usability</a></li>

</ul>
        
      </div>
      
    </div>
  </header>
  

  
  
  <section>
    <p class="lede"><strong>Summary:</strong> Users now do basic operations with confidence and perform with skill on sites they use often. But when users try new sites, well-known usability problems still cause failures.</p>
    <br />
  </section>
  
  

  
  <section id="article-body"><p>Enemies of usability have two <strong> counter-arguments </strong> against <a class="new" href="../../reports/index.php" title="Nielsen Norman Group: List of reports with usability guidelines derived from user research"> design guidelines</a> that are based on user research:</p>

<ul>
	<li>"You're <strong> testing idiots </strong> — most users are smarter and don't mind complexity."</li>
	<li>"You were <strong> right in the past </strong> , but users have now learned how to use advanced websites, so simplicity isn't a requirement anymore."</li>
</ul>

<p>I decided to put these claims to the test in a new study we're currently conducting. We'll use the new insights generated by the study to update our course on <a class="new" href="../../courses/fundamentals/index.php" title="Nielsen Norman Group: detailed course outline for training tutorial"> Fundamental Guidelines for Web Usability</a>.</p>

<p>Because we're testing <em> this year's sites </em> with <em> this year's users </em> , the study automatically assesses the second claim.</p>

<p>We can't directly assess whether our study participants are idiots, since we don't subject them to an IQ test. But participants' comments during all of our studies these past 14 years indicate that we've mainly had plenty smart test users. Unless a specific study calls for participants with a different profile, we mostly recruit people with respectable jobs — an engineering consultant, an equity trader, a lawyer, an office manager, a real estate agent, a speech therapist, and a teacher, to take some of the job titles from the first week of our current study.</p>

<p>One part of the current study tests <a class="old" href="../../topic/b2b-websites/index.php" title="Alertbox: Business-to-Business Usability"> B2B sites</a> since many of our seminar audience work on such sites. This time, we chose sites targeting <strong> dentists </strong> in clinical practice, <strong> IT managers </strong> from big corporations, and <strong> CEOs </strong> of small businesses. Thus, we have disproportionally many users with these job descriptions. They aren't stupid.</p>

<p>One way of quantifying the level of users we're currently testing is to look at their annual income. In our screening, we look at the user's personal income, rather than his or her household income. We also recruit an equal number of people making: below $50,000, $50,000–99,999, and $100,000 or more. The following table compares our users with the entire U.S. population (according to the <a class="out" href="http://pubdb3.census.gov/macro/032007/perinc/new01_001.htm" title="United States Census Bureau: Current Population Survey, Annual Social and Economic  Supplement, Selected Characteristics of People by Total Money Income in 2006"> Census Bureau</a>) within the study's target age range (20–60 years; we've covered <a class="old" href="../childrens-websites-usability-issues/index.php" title="Alertbox: Kids' Corner -  Website Usability for Children"> kids</a>, <a class="old" href="../usability-of-websites-for-teenagers/index.php" title="Alertbox: Usability of Websites for Teenagers"> teens</a>, and <a class="old" href="../usability-for-senior-citizens/index.php" title="Alertbox: Usability for Senior Citizens"> seniors</a> in other research):</p>

<table align="center" style="border-style: solid; border-color: #cccccc; border-width: 1px; border-collapse: collapse; ">
	<tbody>
		<tr>
			<th style=" border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left; background-color:  #003366; color: #ffffff; width: 34%;  ">User's Annual Income</th>
			<th style=" border-style: solid; border-width: 1px; padding: 0.5ex; text-align: center; background-color:  #003366; color: #ffffff; width: 33%; ">Our Participants</th>
			<th style=" border-style: solid; border-width: 1px; padding: 0.5ex; text-align: center; background-color:  #003366; color: #ffffff ; width: 33%; ">U.S. Population<br/>
			(age 20–60)</th>
		</tr>
		<tr>
			<th style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left;">&lt;$50,000</th>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: center;">33%</td>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: center;">70%</td>
		</tr>
		<tr>
			<th style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left;">$50,000–99,999</th>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: center;">33%</td>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: center;">22%</td>
		</tr>
		<tr>
			<th style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left;">≥$100,000</th>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: center;">33%</td>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: center;">8%</td>
		</tr>
	</tbody>
</table>

<p>We're definitely testing people who are much more successful than the average. We decided to <strong> bias the study in favor of high-salary users </strong> for three reasons:</p>

<ul>
	<li>We need to test many business professionals and doctors because so many of our <a class="new" href="../../training-companies-list/index.php" title="Nielsen Norman Group: list of companies sending participants to usability conferences"> seminar participants</a> target these groups, whether for websites or intranets.</li>
	<li>Wealthy users have more money to spend and are thus more important to seminar attendees who work on <a class="new" href="../../reports/ecommerce-user-experience/index.php" title="Nielsen Norman Group report: Usability Guidelines for E-commerce User Experience Design"> e-commerce sites</a>.</li>
	<li>Even conference attendees who target a broad consumer audience benefit from presentations that are based mainly on studies of wealthy users because that fact helps them overcome the <a class="old" href="../are-users-stupid/index.php" title="Alertbox: Are Users Stupid?"> "dumb users" objection</a> when they take the guidelines back to their teams.</li>
</ul>

<p>We're not neglecting poor people — we have enough of them in the study to learn about their needs. But our participant profile is clearly such that no one could claim that the findings don't apply to high-end users.</p>

<h2>Improved User Skills</h2>

<p>So, with the qualifications about our research out of the way, what have we found in recent studies? We've seen several indications that users are indeed <strong> getting a bit better </strong> at using the Web. Almost all users:</p>

<ul>
	<li>are better at <strong> physical operations</strong>, such as mouse movements and scrolling;</li>
	<li>are more <strong> confident </strong> at clicking, and less afraid that they'll break something; and</li>
	<li>know the basics of using <strong> search </strong> and use it more often than we saw in the past.</li>
</ul>

<p>In addition,</p>

<ul>
	<li>some users are exhibiting <strong> expert behaviors</strong>, such as opening a second browser window to compare two websites or changing a PDF file's zoom level.</li>
</ul>

<p>When performing <strong> common tasks on sites they often use</strong>, most users are incredibly <strong> fast and competent</strong>. This fact leads us to two interesting conclusions:</p>

<ul>
	<li>Many sites are now good enough that users reward them with <strong> loyalty </strong> and frequent use.</li>
	<li>When people revisit such sites, they tend to do the same things repeatedly and develop a high degree of <strong> skilled performance </strong> — something we rarely saw on websites in the past.</li>
</ul>

<p>As an example, one user failed almost every task on unfamiliar websites, yet was highly confident and extremely fast in using her bank's site to transfer money between two of her accounts.</p>

<h2>Browsing and Research Skills Still Poor</h2>

<p>Even though users are remarkably good at repeated tasks on their favorite sites, they're <strong> stumped </strong> by the smallest usability problems when they <strong> visit new sites for the first time</strong>.</p>

<p>People are very bad at <a class="old" href="../fixing-information-architecture/index.php" title="Alertbox: 6 Ways to Fix a Confused Information Architecture (IA)"> coping with information architectures</a>  that deviate from their view of the problem space. They also fail to readjust their content interpretation to compensate for <strong> changing contexts </strong> . For example, when users jump from one IA area to another, they often continue to think that the information addresses the previous topic.</p>

<p>Users are also <strong> overwhelmed </strong> by the sheer amount of information that many sites dump on them. For example, a beginning investor tested E-Trade, which could be a great site to support his initial investments and might gradually grow his site involvement over time. Instead, E-Trade's first few pages were littered with scary jargon like "ADR" and "ETF." To escape, he clicked the <em> Active Trading </em> link, assuming this would help him understand how to trade. In fact, it took him to an area for highly experienced investors and it had even more mumbo-jumbo. So, this hot prospect concluded that he didn't dare open an E-Trade account.</p>

<p>First-time visitors to a site don't have the <strong> conceptual model </strong> needed to correctly interpret menu options and navigate to the appropriate place. Lacking this contextual understanding, they waste time in the wrong site areas and misinterpret the area content.</p>

<p>People's <strong> reading skills </strong> are the same as they have always been, emphasizing the importance of <a class="new" href="../../topic/writing-web/index.php" title="Nielsen Norman Group: detailed course outline for 'Content Usability' training tutorial"> writing for the Web</a> In earlier research, we have studied <a class="old" href="../writing-for-lower-literacy-users/index.php" title="Alertbox: Low-Literacy Use"> lower-literacy users</a>, but even the higher-literacy users in our current study had problems with the dense content on many sites. For example, when testing NASA.gov, we asked users to find out when the rings around Saturn were formed. One user did find a page about Saturn, but ended up picking a wrong answer, 1980, which is when additional ringlets were <em> discovered</em>.</p>

<p>To help new users find their way, sites must provide much more <strong> handholding </strong> and much more simplified content.</p>

<p><a class="old" href="../the-3cs-of-critical-web-use-collect-compare-choose/index.php" title="Alertbox: The 3Cs of Critical Web Use - Collect, Compare, Choose">Making comparisons</a> is one of the most important tasks on the Web, and yet users have great difficulty doing so on most sites. The test participants were particularly happy with those websites that <strong> do the comparing and consolidating for them</strong>, like kayak.com.</p>

<p>Why worry about new users' ability to understand your site when your experienced users are clearly having a jolly old time performing frequent tasks? Because people develop into loyal, experienced users only after <strong> passing through the new-user stage </strong> . To grow your business, you have to accommodate first-time visitors for whom small difficulties loom large and often spell defeat.</p>

<p>Also, it's important to <strong> expand your loyal users' interaction vocabulary </strong> to further increase their loyalty. Because they move so fast, experienced users don't waste much time learning new features. Users have <strong> tunnel vision </strong> on their favorite sites: unless a new feature immediately proves its worth, users will stick to safe, familiar territory where they can quickly accomplish their tasks and leave.</p>

<p>By now, our test participants have extensive experience using the Web (mostly 3+ years), and they're still running into substantial problems online. Waiting for people to get <em> even more </em> experience is not likely to resolve the issues. Websites are just too darn difficult.</p>

<h2>Google Gullibility</h2>

<p>Users live by search, but they also die by search.</p>

<p>People turn to search as their first step — or as their second step, if their first attempt at navigating fails. Users typically <strong> formulate good initial queries</strong>, and vaguely understand how to tickle the search engine into coughing up desired sites when they <strong> appropriately modify their main keywords</strong>. For example, in our new study, a user looking for a modest gift for a football fan searched for "football trinket." Five years ago, such a user would most likely have searched "football" and been buried by the results.</p>

<p>Still, today's users <strong> rarely change their search strategy </strong> when the initial query fails. They might modify their first attempt, but they typically stick with the same general approach rather than trying something genuinely new.</p>

<p>For example, one user tested the Mayo Clinic's site to find out how to ensure that a child with a milk allergy would receive sufficient calcium. The user attempted multiple queries with the keyword "calcium," but never tried the words "milk" or "allergy."</p>

<p>Also, users are incredibly <strong> bad at interpreting SERP listings </strong> (SERP = Search Engine Results Page). Admittedly, SERPs from Google and the other main search engines typically offer unreadable gibberish rather than decent website descriptions. Still, an expert searcher (like me) can look at the listings and predict a destination site's quality much better than average users.</p>

<p>When it comes to search, users face three problems:</p>

<ul>
	<li>Inability to <strong> retarget queries </strong> to a different search strategy</li>
	<li>Inability to understand the search results and <strong> properly evaluate </strong> each destination site's likely usefulness</li>
	<li>Inability to sort through the SERP's polluted mass of <strong> poor results</strong>, whether from blogs or from heavily SEO-optimized sites that are insufficiently specific to really address the user's problem</li>
</ul>

<p>Given these difficulties, many users are at the search engine's mercy and <strong> mainly click the top links </strong> — a behavior we might call <em> Google Gullibility </em> . Sadly, while these top links are often not what they really need, users don't know how to do better.</p>

<p>I use "Google" in labeling the behavior only because it's the search engine used by the vast majority of our test users. People using other search engines have the same problems. Still, it's vital to <strong> reestablish competition </strong> in the search engine field: it would be a tragedy for democracy to let 3 guys at one company determine what billions of people read, learn, and ultimately think.</p>

<h2>Guidelines Reconfirmed</h2>

<p>Our work is generating many interesting <strong> new findings </strong> on questions such as: What makes a website credible? What inspires user loyalty? We're running more studies to dig into these issues, which are among the most important for <a class="old" href="../usability-roi-declining-but-still-strong/index.php" title="Alertbox: Usability ROI Declining, But Still Strong"> improving website profitability over the next decade</a>. Once we've analyzed the mountains of data we're collecting, we'll announce the new findings at our upcoming <a class="new" href="../../training/index.php" title="Nielsen Norman Group: overview of conference with full-day usability training courses"> usability conference</a>.</p>

<p>For now, one thing is clear: we're confirming more and more of the old usability guidelines. Even though we have new issues to consider, the old issues aren't going away. A few examples:</p>

<ul>
	<li style="margin-bottom: 0.8ex;"><a class="old" href="../e-mail-newsletters-increasing-usability/index.php" title="Alertbox: Email Newsletters - Surviving Inbox Congestion">Email <strong> newsletters</strong></a> remain the best way to drive users back to websites. It's incredible how often our study participants say that a newsletter is their main reason for revisiting a site. Most business professionals are not very interested in podcasts or newsfeeds (RSS).</li>
	<li style="margin-bottom: 0.8ex;"><strong>Opening new browser windows </strong> is highly confusing for most users. Although many users can cope with extra windows that they've opened <em> themselves</em>, few understand why the Back button suddenly stops working in a new window that <em> the computer </em> initiated. Opening new windows was #2 on my list of <a class="old" href="../the-top-ten-web-design-mistakes-of-1999/index.php" title="Alertbox: The Top Ten Web Design Mistakes of 1999"> top-10 Web design mistakes of <strong> 1999</strong></a>; that this design approach continues to hurt users exemplifies both the longevity of usability guidelines and the limited improvement in user skills.</li>
	<li style="margin-bottom: 0.8ex;"><a class="old" href="../change-the-color-of-visited-links/index.php" title="Alertbox: Change the Color of Visited Links">Links that don't change color</a> when clicked still create confusion, making users unsure about what they've already seen on a site.</li>
	<li style="margin-bottom: 0.8ex;"><strong>Splash screens and intros </strong> are still incredibly annoying: users look for the "skip intro" button — if not found, they often leave. One user wanted to buy custom-tailored shirts and first visited Turnbull &amp; Asser because of their reputation. Clicking the appropriate link led to a page where a video started to play without warning and without a way to skip it and proceed directly to actual info about the service. The user watched a few seconds; got more and more agitated about the lack of options to bypass the intro, and finally closed down the site and went to a competitor. Customer lost.</li>
	<li style="margin-bottom: 0.8ex;">A fairly large minority of users still don't know that they can get to a site's homepage by clicking its logo, so I still have to recommend having an <strong> explicit "home" link </strong> on all interior pages (not on the homepage, of course, because no-op links that point to the current page are confusing — yet another guideline we saw confirmed again several times last week). It particularly irks me to have to retain the "explicit home link" guideline, because I had hoped to get rid of this stupid extra link. But many users really do change very slowly, so we'll probably have to keep this guideline in force until 2020 — maybe longer. At least <a class="old" href="../breadcrumb-navigation-useful/index.php" title="Alertbox: Breadcrumb Navigation Increasingly Useful"> breadcrumbs</a> are a simple way to satisfy this need.</li>
	<li style="margin-bottom: 0.8ex;">People are still very wary, sometimes more so than in the past, about <strong> giving out personal information</strong>. In particular, the B2B sites in this new study failed in exactly the same way as most B2B sites in our <a class="new" href="../../reports/b2b-websites-usability/index.php" title="Nielsen Norman Group report: Business-to-Business Website Usability -
144 Design Guidelines for Converting Business Users Into Leads and Customers"> major B2B research</a>: by hitting users with a registration screen before they were sufficiently committed to the site.</li>
	<li><a class="old" href="../scrolling-and-scrollbars/index.php" title="Alertbox: Scrolling and Scrollbars">Non-standard scrollbars</a> are often overlooked and make people miss most of the site's offerings. The following screens show two examples from last week's testing.</li>
</ul>

<p style="text-align:center"><img alt="Partial screenshot of the upper right of the nutrition info page on Carl's Jr., showing options that include a drop-down menu of various dishes as well as buttons to display PDF files" height="307" src="http://media.nngroup.com/media/editor/alertbox/carlsjr-nonstandard-scrolling-menu.gif" width="470"/></p>

<p>On the Carl's Jr. hamburger chain website, we asked users to look up nutritional information for various meals. Many participants thought the quick item view menu covered only breakfast items, because those were the only choices visible without scrolling (see above). Users overlooked the non-standard scrollbar, and instead often <a class="old" href="../pdf-unfit-for-human-consumption/index.php" title="Alertbox: PDF - Unfit for Human Consumption"> suffered through the PDF files</a> available through the <em> nutrition guide </em> link. (These PDF files caused many other problems, confirming more age-old usability guidelines. That said, some users are now skillful enough to adjust PDF views so that they're slightly more readable. Still, it's a painful process.)</p>

<p style="text-align:center"><img alt="Screenshot from the group events area of the Sundance Resort's website, showing a set of photos of events at the resort." height="400" src="http://media.nngroup.com/media/editor/alertbox/sundanceresort-nonstandard-scrolling.jpg" width="700"/></p>

<p>On the Sundance Resort's site, one user was thrilled to see photos of celebrations hosted at the resort. She eagerly clicked through all five visible thumbnails (see above), but never noticed the small triangles at the top and bottom that let users scroll to see more photos (the circle at left shows the bottom arrow at full size).</p>

<p>Web usability guidelines are not the only guidelines our new studies confirm. On VW's site, we asked participants to use the configurators to customize a car according to their preferences. Unfortunately, this <a class="old" href="../ephemeral-web-based-applications/index.php" title="Alertbox: Ephemeral Web-Based Applications"> mini-application</a> violated some of the basic <a class="new" href="../../courses/topic/applications/index.php" title="Nielsen Norman Group: overview of usability training course 'Application Usability 1: Page-Level Building Blocks for Feature Design'"> application usability guidelines</a>, causing people many problems.</p>

<p style="text-align:center"><img alt="Partial screenshot of the build-your-own-car area on VW's website, showing the step in the process where the user is selecting the wheels." height="422" src="http://media.nngroup.com/media/editor/alertbox/vw-nonstandard-selection.jpg" width="497"/></p>

<p>As the above screenshot shows, users can select their car's wheel style from two options (shown below the car). This simple operation was difficult and error prone, however, because the option for the wheel that's currently mounted on the car was grayed out — a GUI convention that's supposed to mean that something is <em> unavailable</em>, not that it's the current selection. It would have been much better to show both available wheels at all times, placing a selection rectangle — or some other graphical highlighting convention — around the current selection. (Poor feedback is #4 on my list of <a class="old" href="../top-10-application-design-mistakes/index.php" title="Alertbox"> top-10 mistakes of application design</a>.)</p>

<p><a class="old" href="../the-need-for-web-design-standards/index.php" title="Alertbox: The Need for Web Design Standards">Interface conventions exist for a reason</a>: they allow users to focus on your content (in this case, the car and its options). When all interface elements work as expected, users know how to operate the UI to get the desired effect. Conversely, when you deviate from user expectations, you erect a great barrier between users and their ability to get things done. Some designers think this makes the site more <em> exciting </em> . In reality, non-standard design makes the site more <em> frustrating </em> and drastically reduces the user's chance of success. Users are thus more likely to quickly leave the site.</p>

<p>In VW's case, the designers probably suffered from a case of <strong> metaphor overload</strong>: the design mimics the experience of actually assembling a physical car in a real workshop. If you had two wheels on the workshop floor and mounted one on the car, then the chosen wheel would no longer be on the floor.</p>

<p>In reality, though, users are not greasemonkeys. They're clicking on interface elements, and they expect the picture of a wheel to behave like a GUI element.</p>

<p>We're confirming hundreds more of the existing usability guidelines every week as our testing continues. Even though we have upscale users and it's a new study testing new sites, most of the findings are the same as we've seen year after year after year. <a class="old" href="../usability-guidelines-change/index.php" title="Alertbox: Change vs. Stability in Web Usability Guidelines"> Usability guidelines remain remarkably constant</a> over time, because basic human characteristics stay the same.</p>
</section>
  

  
  <section>
    
        
    
  </section>
  
  
  
  
  

  


  <section>
    <br />
    <p class="share"><b>Share <span class="hide-for-small-down">this article</span>:</b> <a href="https://twitter.com/intent/tweet?url=http://www.nngroup.com/articles/user-skills-improving-only-slightly/&amp;text=User%20Skills%20Improving,%20But%20Only%20Slightly&amp;via=nngroup">Twitter</a> | <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=http://www.nngroup.com/articles/user-skills-improving-only-slightly/&amp;title=User%20Skills%20Improving,%20But%20Only%20Slightly&amp;source=Nielsen%20Norman%20Group">LinkedIn</a> | <a href="https://plus.google.com/share?url=http://www.nngroup.com/articles/user-skills-improving-only-slightly/">Google+</a> | <a href="mailto:?subject=NN/g Article: User Skills Improving, But Only Slightly&amp;body=http://www.nngroup.com/articles/user-skills-improving-only-slightly/">Email</a></p>
    <hr />
  </section>

  <footer class="l-subsection">
    
    
    <div class="learn-more">
      <h2>Learn More</h2>
      <p><a href="../subscribe/index.php">Subscribe to the weekly newsletter</a> to get notified about future articles.</p>
      </div>
    
    

    <div class="row show-for-small-down related-content">
      
      
      

      
      
        <div class="column small-12 topic-links">All articles about:
          <ul>
            
            <li><a href="../../topic/web-usability/index.php">Web Usability</a></li>
            
          </ul>
        </div>
      
      

      
      
        <div class="column small-12">
          <h3>UX Conference Training Courses</h3>
           <ul class="no-bullet">
    
        <li><a href="../../courses/ux-basic-training/index.php">UX Basic Training</a></li>
    
        <li><a href="../../courses/human-mind/index.php">The Human Mind and Usability</a></li>
    
        <li><a href="../../courses/emerging-patterns-web-design/index.php">Emerging Patterns for Web Design</a></li>
    
        <li><a href="../../courses/web-page-design/index.php">Web Page UX Design</a></li>
    
</ul>
        </div>
      
      

      
      
        <div class="small-12 column">
          <h3>Research Reports</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../reports/b2b-websites-usability/index.php">B2B Website Usability for Converting Users into Leads and Customers</a></li>
              
            
              
                <li><a href="../../reports/strategic-design-faqs/index.php">Strategic Design for Frequently Asked Questions</a></li>
              
            
          </ul>
        </div>
      
      

      
      
        <div class="small-12 column">
          <h3>Online Seminars</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../online-seminars/fundamental-flaw-flat-design/index.php">The Fundamental Flaw in Flat Design</a></li>
              
            
              
                <li><a href="../../online-seminars/millennials-online/index.php">Millennials Online</a></li>
              
            
              
                <li><a href="../../online-seminars/university-websites/index.php">University Website UX Essentials</a></li>
              
            
          </ul>
        </div>
      
      
    </div>

    <div class="hide-for-small-down row related-content">
      <div class="medium-6 columns">
        
        
          <div>
            <h3>Articles</h3>
            <ul class="no-bullet">
              
                
                <li><a href="../ux-thanks/index.php">Give Thanks for Good UX</a></li>
                
              
                
                <li><a href="../top-10-enduring/index.php">Top 10 Enduring Web-Design Mistakes</a></li>
                
              
                
                <li><a href="../university-sites/index.php">University Websites: Top 10 Design Guidelines</a></li>
                
              
                
                <li><a href="../overuse-of-overlays/index.php">Overuse of Overlays: How to Avoid Misusing Lightboxes</a></li>
                
              
                
                <li><a href="../pop-up-adaptive-help/index.php">Pop-ups and Adaptive Help Get a Refresh</a></li>
                
              
            </ul>
          </div>
        
        

        
          
          <div>
            <h3>Research Reports</h3>
            <ul class="no-bullet">
              
                
                  <li><a href="../../reports/b2b-websites-usability/index.php">B2B Website Usability for Converting Users into Leads and Customers</a></li>
                
              
                
                  <li><a href="../../reports/strategic-design-faqs/index.php">Strategic Design for Frequently Asked Questions</a></li>
                
              
            </ul>
          </div>
          
        

      </div>
      <div class="medium-6 columns">
        
          
          <div>
            <h3>UX Conference Training Courses</h3>
             <ul class="no-bullet">
    
        <li><a href="../../courses/ux-basic-training/index.php">UX Basic Training</a></li>
    
        <li><a href="../../courses/human-mind/index.php">The Human Mind and Usability</a></li>
    
        <li><a href="../../courses/emerging-patterns-web-design/index.php">Emerging Patterns for Web Design</a></li>
    
        <li><a href="../../courses/web-page-design/index.php">Web Page UX Design</a></li>
    
</ul>
          </div>
          
        
        
          
          <div>
            <h3>Online Seminars</h3>
            <ul class="no-bullet">
            
                <li><a href="../../online-seminars/fundamental-flaw-flat-design/index.php">The Fundamental Flaw in Flat Design</a></li>
            
                <li><a href="../../online-seminars/millennials-online/index.php">Millennials Online</a></li>
            
                <li><a href="../../online-seminars/university-websites/index.php">University Website UX Essentials</a></li>
            
            </ul>
          </div>
          
        
      </div>
    </div>
  </footer>
</article>

        </div>

      

    
  </div>
  <div class="row">
    
      <div class="column small-12 show-for-small-down l-navrail announcements">
        
          
        
      </div>
    
    
  </div>

    </div>
    




<footer id="page-footer">
    <div class="row hide-for-print">
        <div class="column small-12 medium-6">
            <section class="footer-popular">
                
                <h1>Popular Topics</h1>
                <div style="float: left;  width: 15em; display: block;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/applications/index.php">Application Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/e-commerce/index.php">E-Commerce Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/eyetracking/index.php">Eyetracking </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/information-architecture/index.php">Information Architecture </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/intranets/index.php">Intranet Design </a></li>
	<li><a href="../../topic/interaction-design/index.php">Interaction Design </a></li>
</ul>
</div>

<div style="float: left;  width: 15em; display: block; list-style: none;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/mobile-and-tablet-design/index.php">Mobile and Tablet Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/credibility-persuasion/index.php">Persuasive Design</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/research-methods/index.php">Research Methods</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/user-testing/index.php">User Testing </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/web-usability/index.php">Web Usability </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/writing-web/index.php">Web Writing &amp; Content Strategy </a></li>
</ul>
</div>

<p>&nbsp;</p>

                
            </section>
        </div>

        <div class="column small-12 medium-6">
            <div class="row">
                <div class="column small-6">
                    <section class="footer-about">
                        <h1>Follow Us</h1>
                        <ul>
	<li><a href="../subscribe/index.php">Subscribe to our weekly UX newsletter</a>,&nbsp;Jakob Nielsen&#39;s Alertbox</li>
	<li>Visit Don Norman&#39;s website, <a href="http://www.jnd.org/"> www.jnd.org </a></li>
	<li>Visit Bruce Tognazzini&#39;s website, <a href="http://www.asktog.com/"> www.asktog.com </a></li>
	<li style="padding-top: 6px;"><a class="twitter-follow-button" data-show-count="false" data-size="large" href="https://twitter.com/nngroup"><img alt="Follow @nngroup on Twitter" height="20" src="https://media.nngroup.com/media/editor/2016/12/30/twitter-follow-button.png" style="width: 117px; height: 20px;" width="117" /></a></li>
	<li style="padding-top: 6px;"><img alt="" height="14" src="https://media.nngroup.com/media/editor/2016/07/26/feed-icon-14x14.png" style="vertical-align:text-top;width:14px;margin-right:.25em;" width="14" /><a href="../../feed/rss/index.php">Subscribe to our RSS feed</a></li>
</ul>

                    </section>
                </div>

                <div class="column small-6">
                    <section class="footer-about right-column">
                        <h1>About
</h1>
                        <ul class="no-bullet">
	<li><a href="../../about/contact/index.php">Contact information</a></li>
	<li><a href="../../about/index.php">About Us</a></li>
	<li><a href="../../about/why-nng/index.php">Why NN/g? </a></li>
	<li><a href="../../return-policy/index.php">Return Policy</a></li>
</ul>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="copyright column small-12">
            <p><a href="../../copyright-and-reprint-info/index.php">Copyright</a> &copy; 1998-2017 Nielsen Norman Group, All Rights Reserved.</p>
        </div>
    </div>
</footer>


    


        <script src="https://media.nngroup.com/static/vendor/fastclick/js/fastclick.min.js"></script>
        <script src="https://media.nngroup.com/static/vendor/foundation/js/foundation.min.js"></script>
        <script src="http://media.nngroup.com/static/js/app.js"></script>
        <script src="http://media.nngroup.com/static/js/scroll-to-fixed.js"></script><script>var externalLinks = document.querySelectorAll('a[href^="http"]');for (var i = externalLinks.length-1; i >= 0; i--) {externalLinks[i].addEventListener("click", function(event) { alert("External links are disabled for this test."); event.preventDefault(); }, false);}var mailLinks = document.querySelectorAll('a[href^="mailto"]');for (var i = mailLinks.length-1; i >= 0; i--) {mailLinks[i].addEventListener("click", function(event) { alert("Email links are disabled for this test."); event.preventDefault(); }, false);}var externalForms = document.querySelectorAll('form');for (var i = externalForms.length-1; i >= 0; i--) {externalForms[i].addEventListener("submit", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false); externalForms[i].addEventListener("click", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false);}</script>

        <script>
            
            $(function() {
                $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
                $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
            });
        </script>

        
<script src="https://media.nngroup.com/static/js/articles.js"></script>


        
        <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1072699960;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072699960/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
    </body>

<!-- Mirrored from www.nngroup.com/articles/user-skills-improving-only-slightly/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:15:05 GMT -->
</html>
