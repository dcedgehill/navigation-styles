<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->





    
<!-- Mirrored from www.nngroup.com/articles/seamless-cross-channel/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 14:54:49 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1026.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"licenseKey":"1a30ef9931","errorBeacon":"bam.nr-data.net","applicationID":"2048992","transactionName":"NFxSMRBUX0EDUkIPDA0WdhAMVkVbDV8ZBxEXUFMJB0YfRAtUQRVZIktEDAFZVHYHRVcPD01eVRE=","beacon":"bam.nr-data.net","queueTime":0,"applicationTime":484,"agent":""}</script>
        <title>Seamlessness in the Omnichannel User Experience</title><meta property="og:title" content="Seamlessness in the Omnichannel User Experience" />
  
        
        <meta name="description" content="Companies must support painless transitions across channels in order to create a usable omnichannel experience.">
        <meta property="og:description" content="Companies must support painless transitions across channels in order to create a usable omnichannel experience." />
        
  
        
	
        
        <meta name="keywords" content="cross-channel, channel, seamless, interruptions, recovery, interrupted use, touchpoint, touch point, touchpoints, touch points, omnichannel, customer journey, user journey, journey map">
        

        

        <meta name="robots" content="noarchive">
        <meta name="viewport" content="width=device-width">

        <link rel="search" type="application/opensearchdescription+xml" title="NN/g Search" href="../../opensearch.xml" />
        <link rel="shortcut icon" href="http://media.nngroup.com/static/img/favicon.ico" />

        <link rel="stylesheet" href="https://media.nngroup.com/static/vendor/font-awesome/css/font-awesome.min.css">

        
        <link rel="stylesheet" href="https://media.nngroup.com/static/css/7232a960f1df.css" type="text/css" />
        <link rel="stylesheet" href="http://fast.fonts.com/t/1.css?apiType=css&amp;projectid=c5846260-9def-46ec-9407-0f1349bd2d64" type="text/css">

        <script src="https://media.nngroup.com/static/vendor/modernizr/js/modernizr.min.js"></script>

        <script type="text/javascript" src="https://media.nngroup.com/static/js/3283be5def74.js"></script>
        

        

        
        
        <!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=229034,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('http://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->


<!-- Google Analytics Tracking Code for http://nngroup.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23132681-1', 'nngroup.com');
  ga('require', 'linkid', 'https://www.nngroup.com/articles/seamless-cross-channel/linkid.js');
  ga('require', 'displayfeatures');

// Optimizely Universal Analytics Integration
 window.optimizely = window.optimizely || [];
 window.optimizely.push("activateUniversalAnalytics");

  ga('send', 'pageview');

</script>
<script>
/**
* Function to track a click on an outbound link in Google Analytics.
*/
function trackOutboundLink(label, url) {
    if(window.ga && ga.loaded) {
        ga('send', 'event', 'outbound', 'click', label, {'hitCallback': function(){document.location = url;}
        });
    } else {
        document.location = url;
    }
}
/**
* Function to track a click on a link in the article announcement area.
*/
$('#articleAnnouncement a').on('click', function(){
  ga('send', 'event', 'link', 'click', 'articleAnnouncement');
});
</script>

<script type="text/javascript">
 setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2421.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



<!-- Hotjar Tracking Code for http://nngroup.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:121605,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

        <script src="https://media.nngroup.com/static/js/analytics/track_outbound_link.js"></script>
        
        
        
    </head>
    <body class="location-articles articles-detail">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
    <header id="page-header">
        <div id="cart" class="shopping-cart mobile-cart show-for-small-down"></div>
        <div class="row">
            <div id="masthead" class="masthead small-12 medium-7 column">

                <!-- The button to toggles menu on small screen -->

                <h1><a href="../../index.php"><img src="http://media.nngroup.com/static/img/logo-wordmark.png" width="74" height="30"> Nielsen <span class="norman">Norman</span> Group</a></h1>
                <p class="tagline show-for-medium-up">Evidence-Based User Experience Research, Training, and Consulting</p>

                <!-- The button to toggles form search on small screen -->


                <div style="clear: right;width:  100%;height: 1px;"></div><?php define('ROOT_PATH',$_SERVER["DOCUMENT_ROOT"] . '/apps/navigation_styles/navigations/5'); ?><?php include(ROOT_PATH)."/navigation.php"; ?>

                <!-- Drop down menu on small screen -->
                <div id="hideNav" class="show-for-small-down">
                  
                  <ul class="menu-content">
                    <li><a href="../../index.php">Home</a></li>
                    <li class="has-submenu">
                      <a>Training</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../training/index.php">Overview</a></li>
                        
<li id="nav-ux-conference"><a href="../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../online-seminars/index.php"><span>Online Seminars</span></a></li>

                      </ul>
                    </li>
                    <li class="has-submenu">
                      <a>Consulting</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
  <li><a href="../../consulting/index.php">Overview</a></li>
  
  <li><a href="../../consulting/benchmarking/index.php">Benchmarking</a></li>
  
  <li><a href="../../consulting/expert-design-review/index.php">Expert Design Review</a></li>
  
  <li><a href="../../consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
  
  <li><a href="../../consulting/usability-testing/index.php">Usability Testing </a></li>
  
  <li><a href="../../consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
  
  <li><a href="../../in-house-training/index.php">In-House Training</a></li>
</ul>

                    </li>

                    <li><a href="../../reports/index.php">Reports</a></li>
                    <li><a href="../index.php">Articles</a></li>
                    <li class="has-submenu"><a>About NN/G</a><i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../about/index.php">Overview</a></li>
                        <li><a href="../../people/index.php">People</a></li>
                        <li><a href="../../about/why-nng/index.php">Why NN/g?</a></li>
                        <li><a href="../../about/contact/index.php">Contact</a></li>
                        <li><a href="../../news/index.php">News</a></li>
                        <li><a href="../../about/history/index.php">History</a></li>
                        <li><a href="../../books/index.php">Books</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                </div>

                <hr class="header-divider show-for-small-only"/>


            </div>
       </div>

        <div id="hideNav">            <nav class="primary-nav small-12 column">
                <ul>
                    <li><a class="navitem" id="nav-home" href="../../index.php">Home</a></li>
                    <li><a class="navitem" id="nav-training" href="../../training/index.php">Training</a></li>
                    <li><a class="navitem" id="nav-consulting" href="../../consulting/index.php">Consulting</a></li>
                    <li><a class="navitem" id="nav-reports" href="../../reports/index.php">Reports</a></li>
                    <li><a class="navitem" id="nav-articles" href="../index.php">Articles</a></li>
                    <li><a class="navitem" id="nav-about" href="../../about/index.php">About NN/G</a></li>
                </ul>
            </nav>
            <nav class="secondary-nav small-12 medium-12 large-12 columns">
                
                
            </nav>
        </div>
    </header>
    <div class="l-content"><span id="preventClose" style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776; Menu</span>
    
  

  

  <div class="row">
    
      
      <section class="small-12 medium-3 columns collapsable-content l-navrail articles-left-column">
        <h1 class="show-for-small-down collapse-link mobile-browse-header">Browse by Topic and Author</h1>
          <div class="row collapsable">

            
            <div class="small-6 medium-12 columns">
              <section id="article-popular-topics" style="display: none;">
                
                <h1>Topics</h1>
                <ul id="article-popular-topics">
                    
                    <li>
                        
                        <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/intranets/all/index.php">Intranets</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/user-testing/all/index.php">User Testing</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                        
                    </li>
                    
                    <li><a href="#" id="article-see-all-topics"><strong>See all topics&hellip;</strong></a></li>
                </ul>
                
              </section>
              <section id="article-all-topics" style="display: none;">
                
                <h1>
                  <span class="show-for-medium-up">All Article Topics</span>
                  <span class="show-for-small-down">All Topics</span>
                  (<a href="#" id="article-hide-all-topics">hide</a>)
                </h1>
                <ul>
                  
                  <li>
                    
                    <a href="../../topic/accessibility/all/index.php">Accessibility</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/agile/all/index.php">Agile</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/analytics-and-metrics/all/index.php">Analytics &amp; Metrics</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/applications/all/index.php">Application Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/b2b-websites/all/index.php">B2B Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/behavior-patterns/all/index.php">Behavior Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/branding/all/index.php">Branding</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/content-strategy/all/index.php">Content Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/corporate-websites/all/index.php">Corporate Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/design-patterns/all/index.php">Design Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-design-process/all/index.php">Design Process</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/email/all/index.php">Email</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/eyetracking/all/index.php">Eyetracking</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/heuristic-evaluation/all/index.php">Heuristic Evaluation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/human-computer-interaction/all/index.php">Human Computer Interaction</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-ideation/all/index.php">Ideation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/information-architecture/all/index.php">Information Architecture</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/interaction-design/all/index.php">Interaction Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/international-users/all/index.php">International Users</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/intranets/all/index.php">Intranets</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-management/all/index.php">Management</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/navigation/all/index.php">Navigation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/non-profit-websites/all/index.php">Non-Profit Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/personas/all/index.php">Personas</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/persuasive-design/all/index.php">Persuasive Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/prototyping/all/index.php">Prototyping</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/psychology-and-ux/all/index.php">Psychology and UX</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/research-methods/all/index.php">Research Methods</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/search/all/index.php">Search</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/social-media/all/index.php">Social Media</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/strategy/all/index.php">Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/user-testing/all/index.php">User Testing</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-teams/all/index.php">UX Teams</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/visual-design/all/index.php">Visual Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/writing-web/all/index.php">Writing for the Web</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/young-users/all/index.php">Young Users</a>
                    
                  </li>
                  
                </ul>
                
              </section>
            </div>
            <script>
              /* Determine which topics list will be displayed: the full or the
               * partial.  We are inlining this close to the list itself to
               * prevent rendering delays on the client
               */
              var topic_list = $.cookie("article-topics");

              var popular_topics = $('#article-popular-topics');
              var all_topics = $('#article-all-topics');

              if(topic_list == 1) {
                  popular_topics.hide();
                  all_topics.show();
              } else {
                  popular_topics.show();
                  all_topics.hide();
              }
            </script>
            

            
            <div class="small-6 medium-12 columns">
              
              <section id="article-initial-authors">
                  <h1>Author</h1>
                  <ul>
                    
                    <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                    
                    <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                    
                    <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                    
                    <li><a href="#" onClick="$('#article-initial-authors').hide(); $('#article-all-authors').show();"><strong>See all authors&hellip;</strong></a></li>
                  </ul>
              </section>
              <section id="article-all-authors" style="display: none;">
                <h1>
                  All Authors
                  (<a href="#" id="article-hide-all-authors">hide</a>)
                </h1>
                <ul>
                  
                  <li><a href="../author/raluca-budiu/index.php">Raluca Budiu</a></li>
                  
                  <li><a href="../author/patty-caya/index.php">Patty Caya</a></li>
                  
                  <li><a href="../author/susan-farrell/index.php">Susan Farrell</a></li>
                  
                  <li><a href="../author/therese-fessenden/index.php">Therese Fessenden</a></li>
                  
                  <li><a href="../author/kim-flaherty/index.php">Kim Flaherty</a></li>
                  
                  <li><a href="../author/sarah-gibbons/index.php">Sarah Gibbons</a></li>
                  
                  <li><a href="../author/aurora-harley/index.php">Aurora Harley</a></li>
                  
                  <li><a href="../author/kate-kaplan/index.php">Kate Kaplan</a></li>
                  
                  <li><a href="../author/page-laubheimer/index.php">Page Laubheimer</a></li>
                  
                  <li><a href="../author/angela-li/index.php">Angie Li</a></li>
                  
                  <li><a href="../author/hoa-loranger/index.php">Hoa Loranger</a></li>
                  
                  <li><a href="../author/kate-meyer/index.php">Kate Meyer</a></li>
                  
                  <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                  
                  <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                  
                  <li><a href="../author/kara-pernice/index.php">Kara Pernice</a></li>
                  
                  <li><a href="../author/christian-rohrer/index.php">Christian Rohrer</a></li>
                  
                  <li><a href="../author/amy-schade/index.php">Amy Schade</a></li>
                  
                  <li><a href="../author/katie-sherwin/index.php">Katie Sherwin</a></li>
                  
                  <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                  
                  <li><a href="../author/kathryn-whitenton/index.php">Kathryn Whitenton</a></li>
                  
                </ul>
              </section>
              
            </div>
            
          </div>

          
          <section class="hide-for-small-down">
            
              
              <h1>Recent Articles</h1>
              
              <ul>
                  
                  <li><a href="index.php">Seamlessness in the Omnichannel User Experience</a></li>
                  
                  <li><a href="../brand-intention-interpretation/index.php">Experience Design: Bridging Brand Intention and Brand Interpretation</a></li>
                  
                  <li><a href="../eyetracking-tasks-efficient-scanning/index.php">Scanning Patterns on the Web Are Optimized for the Current Task</a></li>
                  
                  <li><a href="../flat-design-best-practices/index.php">Flat-Design Best Practices</a></li>
                  
                  <li><a href="../ux-user-stories/index.php">Accounting for UX Work with User Stories in Agile Projects</a></li>
                  
                  <li><a href="../index.php"><strong>See all articles&hellip;</strong></a></li>
              </ul>
              
            
          </section>
          

          
          
          <section class="hide-for-small-down">
            <h1>Popular Articles</h1>
            
            <ul>
                
                <li><a href="../usability-101-introduction-to-usability/index.php">Usability 101: Introduction to Usability</a></li>
                
                <li><a href="../top-10-mistakes-web-design/index.php">Top 10 Mistakes in Web Design</a></li>
                
                <li><a href="../how-users-read-on-the-web/index.php">How Users Read on the Web</a></li>
                
                <li><a href="../f-shaped-pattern-reading-web-content/index.php">F-Shaped Pattern For Reading Web Content</a></li>
                
                <li><a href="../ten-usability-heuristics/index.php">10 Usability Heuristics for User Interface Design</a></li>
                
                <li><a href="../intranet-design-2016/index.php">10 Best Intranets of 2016</a></li>
                
                <li><a href="../which-ux-research-methods/index.php">When to Use Which User-Experience Research Methods</a></li>
                
                <li><a href="../response-times-3-important-limits/index.php">Response Times: The 3 Important Limits</a></li>
                
                <li><a href="../why-you-only-need-to-test-with-5-users/index.php">Why You Only Need to Test with 5 Users</a></li>
                
                <li><a href="../page-fold-manifesto/index.php">The Fold Manifesto: Why the Page Fold Still Matters</a></li>
                
            </ul>
          </section>
          
          

          
            <section class="hide-for-small-down">
              
            </section>
          

      </section>
      

      
        <div class="small-12 medium-9 columns content-body">
          




<article class="l-subsection alertbox-article-content article-detail-bl">
  
  <header>
    <div class="l-subsection">
      <h1>Seamlessness in the Omnichannel User Experience</h1>
      <div class="author-meta">by
        
          
            
            <a href="../author/kim-flaherty/index.php">Kim Flaherty</a>
            
          
        on  March 19, 2017
        
      </div>

      

      
      <div class="topic-links">
        Topics:
        
          <ul>

  <li><a href="../../topic/e-commerce/index.php">E-commerce</a></li>

  <li><a href="../../topic/strategy/index.php">Strategy</a></li>

</ul>
        
      </div>
      
    </div>
  </header>
  

  
  
  <section>
    <p class="lede"><strong>Summary:</strong> A seamless user experience, regardless of channel or device, is one of the 5 components of a usable omnichannel experience. Companies that make it simple to move across channels have a competitive advantage.</p>
    <br />
  </section>
  
  

  
  <section id="article-body"><p>Users interact with companies and organizations through many channels, including the web, mobile and tablet applications, email, kiosks, online chat, and by speaking with customer representatives in physical locations or over the phone. When users engage with an organization through a specific channel, they see it as one of the many interactions that make up their overall user experience with the company. UX failures on any one channel reflect poorly on the experience as a whole.</p>

<p>Our user research on omnichannel user experience identified five key elements of a usable omnichannel experience:</p>

<ul>
	<li style="margin-left: 40px;"><a href="../omnichannel-consistency/index.php">Consistent</a></li>
	<li style="margin-left: 40px;"><a href="../context-specific-cross-channel/index.php">Optimized</a></li>
	<li style="margin-left: 40px;">Seamless</li>
	<li style="margin-left: 40px;">Orchestrated</li>
	<li style="margin-left: 40px;">Collaborative</li>
</ul>

<p>This article discusses why seamlessness is important in the omnichannel experience.</p>

<h2>Designing for the Entire Journey, Not for a Single Interaction</h2>

<p>Often users <strong>don’t complete an activity in one sitting</strong> or through a single channel. Some of the most common reasons why users change channels include:</p>

<ul>
	<li><strong>There is an external interruption or change in context</strong>. In some situations, users are not able to complete an activity in one go because they are interrupted. Imagine you’re in the middle of purchasing a new car-insurance policy on your desktop computer when it’s time to go pick up your child from school. You might continue the registration process on your mobile device while you wait in the parking lot, taking advantage of the fact that your vehicle identification number and license plate are easily available.</li>
	<li><strong>The task is better suited for another channel</strong>. Users will often change channels because their activity has become too laborious on the current channel and a different channel appears more effective or efficient. Imagine discussing a charge on your mobile phone bill via online chat. If the circumstances are complex and require a lot of explanation, you might choose to call the carrier to continue the discussion over the phone instead of typing a lot of information. Or you may start to watch a live basketball game on your mobile phone on your commute from work, but, once you get home, you finish it on your desktop computer or large television.</li>
	<li>T<strong>he activity requires it</strong>. Some activities by nature require users to move across various channels to complete them. For example, preparing a meal from an online recipe can require multiple channels. It may begin on the desktop as you select a recipe, then move to the mobile app as you shop the list of ingredients in the store, and finish in the kitchen on a tablet so you can reference the recipe as you cook it.</li>
</ul>

<p>Understanding why and when users move across channels can help you design efficient and streamlined channel transitions. Aim to string together these various <a href="../channels-devices-touchpoints/index.php">touchpoints</a> to create a seamless journey rather than a collection of disjointed interactions.</p>

<p><strong>Definition: Seamlessness</strong> is a quality of any crosschannel customer journey where the transitions (or handoffs) from one channel to the next involve zero  or minimal overhead for the users. Basically, if you can pick up where you left off, the user experience will be seamless. But if users have to reestablish their contexts and/or redo work when switching to a new channel, then the experience will feel bumpy.</p>

<h2>The Necessity of Seamlessness</h2>

<p>With the rise of mobility and the proliferation of new devices and interaction channels, customers expect an experience that’s efficient, accessible, and that moves along with them as they change devices and context. It’s no longer enough for organizations to provide workable solutions on various channels, but they must begin blurring the lines between these experiences, allowing users to move seamlessly between them without difficulty. An enjoyable experience with your organization regardless of the channels involved can be an important differentiator between you and your competitor’s offerings.</p>

<p>Cinemark’s design features facilitate a seamless customer journey. The web and the mobile-app experiences identify the user’s location and suggest a favorite theater. Users can browse movies on the desktop, but purchase movie tickets on their mobile devices if they want to take advantage of quick payment options like Apple Pay. Once the user switches to the mobile application, the movies playing at the favorite (or closest) theater will be displayed, so the user will be able to easily resume the task. At the theater, tickets can be picked up at the counter, or  at a kiosk by inputting the transaction confirmation number or scanning the credit card used for the purchase.  (It would be even better of users could show their confirmation number — or an equivalent <a href="../wechat-qr-shake/index.php">QR code</a> — at the entrance, without having to pick up tickets.)</p>

<p>Once users enter the theater, a push notification to their mobile phones asks them to turn their phones to “CineMode”, which silences the phones during the movie. Those who opt in to CineMode immediately receive a reward redeemable at the concession stand. They can easily show the reward on their phone to the concession staff and will likely purchase even more!</p>

<p>This journey includes 5 touchpoints and 4 different channels, yet for users it’s one unique great experience, because it’s so easy to transition from one channel to the next without any significant roadblocks.</p>

<figure class="caption"><img alt="Cinemark Seamless Journey" height="665" src="https://media.nngroup.com/media/editor/2017/03/16/cinemark.png" width="720"/>
<figcaption><em>Cinemark created a seamless journey across different interaction channels.</em></figcaption>
</figure>

<p>Most customers do not respond well to difficulties while completing their tasks. If the <a href="../interaction-cost-definition/index.php">interaction cost</a> of doing an activity overcomes its <a href="../perceived-value/index.php">perceived value</a>, users will abandon the task or the brand — either now, or in future interactions.  Only those who are extremely motivated will work to overcome barriers.</p>

<p>One participant in our research study purchased a baby swing from Walmart.com and chose to pick it up in the store. However, at the store, the barcode received through email could not be scanned. Later, after discovering that the swing was broken, she attempted to return it to the store, but the store’s system for processing online-order returns was down, so she was asked to call Walmart.com to process the return.  The return process via the phone was long and arduous. She had to ship the product back to a warehouse, but she did not have a shipping box since she hadn’t been given one at pickup. Moreover, she was confused about which shipping carrier to use for the return, due to conflicting information received through multiple emails from Walmart. The entire customer journey included 14 interactions and 5 roadblocks she had to overcome along the way. At the end of the experience, the user was frustrated and exhausted saying, “That’s probably the last time I order anything from Walmart.” This user’s full journey is shown below, with each roadblock marked with a red X icon.</p>

<figure class="caption"><img alt="Walmart Customer Journey" height="1440" src="https://media.nngroup.com/media/editor/2017/03/16/walmartcustomerjourney.png" width="720"/>
<figcaption><em>One study participant had to overcome 5 roadblocks (marked by red X icons) to purchase and return a baby swing from Walmart.com. Each roadblock represents a moment in her customer journey when she was not able to complete the interaction as desired and needed to find an alternative option.</em></figcaption>
</figure>

<h2>Identify Journey Roadblocks, Triggers, and Next Steps </h2>

<p>Creating a seamless customer journey requires organizations to understand how customers move across channels to complete tasks, and also identify and eliminate potential roadblocks in the journey. </p>

<p>Start by conducting research about how your users complete key activities with your organization over time. Ask yourself: </p>

<ul>
	<li><strong>Are there times when the activity requires users to change channels?</strong> If so, do you facilitate the transition or do you leave this burden up to your users? </li>
</ul>

<p style="margin-left: 40px;">HyVee Grocery Stores Aisles Online allows people to order groceries for pickup at the store. The confirmation email provides detailed instructions about how to pick up the items.</p>

<figure class="caption"><img alt="HyVee Store Pickup" height="351" src="https://media.nngroup.com/media/editor/2017/03/16/hyveeaisles.png" width="720"/>
<figcaption><em>Store pickup is required when ordering groceries online from HyVee. The confirmation email has clear pickup instructions, and signage at the store location helps users identify the pickup location.</em></figcaption>
</figure>

<figure class="caption"><img alt="Best Buy Order Pickup" height="622" src="https://media.nngroup.com/media/editor/2017/03/16/bestbuytextmessage.png" width="350"/>
<figcaption><em>Best Buy’s order-pickup text message did not provide instructions about how to pick up items at the store. One user in our study was confused about where she should park, as her items were large and heavy.  </em></figcaption>
</figure>

<ul>
	<li><strong>Are there workflows that your users commonly divide across multiple channels?</strong> If so, can you help automate the cross-channel transition?  For example, many Google Maps users  who do a desktop search for directions will  often make the same search on their mobile devices when they get into their vehicles. The Google Maps web interface offers an option to directly send the directions to the phone.</li>
</ul>

<figure class="caption"><img alt="Google Maps Seamless Transition" height="323" src="https://media.nngroup.com/media/editor/2017/03/16/googlemaps.png" width="720"/>
<figcaption><em>Google Maps facilitates a seamless transition from desktop to phone by allowing users to send directions to their phones.</em></figcaption>
</figure>

<ul>
	<li><strong>Do you trigger an action that drives users to another channel?</strong> If so, what are the next steps on that channel and will users be able to proceed successfully? </li>
</ul>

<p style="margin-left: 40px;">For example, one study participant who was shopping for paint at Home Depot found a touchscreen display that allowed her to scan barcodes from the paint swatches in order to see the paint color in a room. The display let her send the project to herself so she could reference the information later. Although this feature seemed helpful, the next steps in the process were problematic. The user remarked, “I chose the texting option. Sadly, the whole experience broke down when I clicked the link they texted back with my project, as the page was completely zoomed out. It was not meant for viewing on my phone even though they texted me! Once I zoomed in, I saw that it didn't even include the main information I expected it to: specifically, the names of the colors I chose.  It only showed the colored photo! This seems like such an obvious omission, as having the color combination written down was really the only thing I wanted.”</p>

<figure class="caption"><img alt="Behr Paint Kiosk" height="360" src="https://media.nngroup.com/media/editor/2017/03/16/behrpaint.png" width="720"/>
<figcaption><em>A website provide by an interactive display in Home Depot for Behr paint was not built for traffic coming from a mobile device and lacked the important details needed to move forward with the activity. </em></figcaption>
</figure>

<figure class="caption"><img alt="DailyLook Email" height="589" src="https://media.nngroup.com/media/editor/2017/03/16/dailylookemail.png" width="720"/>
<figcaption><em>Dailylook sent an email prompting the user to enroll in its fashion-subscription program. When the user tapped Get Started Now, the website did not display the registration page, but instead loaded product listings.</em></figcaption>
</figure>

<ul>
	<li><strong>Do your customers encounter a roadblock? </strong>What can you do to solve it? A  grocery shopper tried to use the grocery store’s mobile application to scan a digital coupon displayed on a carton of eggs for extra savings at the register. Unfortunately, the app was not able to scan the barcode. The user said, “I tried to scan the barcode for the app discount. No luck, it couldn't read it for some reason. Then I tried to search for the eggs in the app, and the discount wasn't there! Very confusing and frustrating, and I ended up not even getting the eggs.” </li>
</ul>

<p style="margin-left: 40px;">The app should have provided alternative ways to access the coupon if the barcode could not be scanned — for example, by  allowing users to search for the product and access the coupon manually or to type a numeric code corresponding to the barcode.</p>

<figure class="caption"><img alt="Egg Barcode Coupon" height="508" src="https://media.nngroup.com/media/editor/2017/03/16/eggsbarcodescan.png" width="720"/>
<figcaption><em>The store’s mobile app couldn’t scan the barcode, and there was no other way to get the discount.</em></figcaption>
</figure>

<p>User research such as field studies and diary studies can help you answer these questions and uncover ways to streamline the customer journey. In particular, <a href="../diary-studies/index.php">diary studies</a> help you understand experiences that unfold over time and provide data for <a href="../customer-journey-mapping/index.php">customer-journey maps</a>, which are important  for identifying, analyzing, and streamlining situations when the journey breaks down.</p>

<h2>Streamline the Customer Journey </h2>

<p>Once you have examined the customer journey, implement solutions to resolve roadblocks and facilitate user transitions. While every organization will have unique circumstances and challenges, some common tactics for streamlining any journey include:</p>

<ul>
	<li><strong>Make it easy to continue activities where they were left off.</strong> Many activities are continuous and should be easily resumed on a different channel. Find ways to make this transition as easy as possible. Some solutions include:

	<ul>
		<li><strong>Authentication</strong>. Requiring users to sign in as they use a channel is an easy solution, although <a href="../login-walls/index.php">not always acceptable</a>. While sites such as Netflix and Facebook prevent use without an account, it is <a href="../optional-registration/index.php">unreasonable to ask people to log in</a> before they’re allowed to purchase batteries on a random ecommerce site. </li>
		<li><strong>Send link to resume</strong>. Allowing users to email to themselves a link through which they could resume an activity is a low-cost way to ensure seamlessness. A version of this method is sending a QR code or simply a passcode, that, when entered, will enable users to finish their task on a different device.</li>
		<li><strong>Hand off</strong>. Devices within the Apple ecosystem allow users to hand off an experience to a different device — for example, a movie being watched on the smartphone Hulu app could be continued on the iPad Hulu app by the touch of a button. Designers should take advantage of these operating-system provided opportunities. </li>
	</ul>
	</li>
</ul>

<figure class="caption"><img alt="Netflix Resume Task" height="267" src="https://media.nngroup.com/media/editor/2017/03/16/netflixresumetask.png" width="720"/>
<figcaption><em>Netflix makes it easy for users to continue watching a program they haven’t finished yet by presenting them in a separate list.</em></figcaption>
</figure>

<ul>
	<li><strong>Provide sandboxes for users to store items</strong>. Make it easy to re-access information or save progress when users get interrupted or switch channels mid task. Make sure your solutions provide ways to stop and start an activity without losing progress or doing rework.</li>
</ul>

<p style="margin-left: 40px;">On Amazon.com, there are multiple ways to save items of interest for easy access at another time: various wish lists, save-for-later feature at checkout, and also a list of recently viewed products.  All these save time in the future, because the process of browsing through products and narrowing down options or locating a specific item can be time-consuming.</p>

<p style="margin-left: 40px;"> </p>

<figure class="caption"><img alt="Amazon Sandbox Checkout Page" border="1" height="1008" src="https://media.nngroup.com/media/editor/2017/03/17/amazonsandboxes.png" width="936"/>
<figcaption><em> Amazon allows users to add items of interest to wishlists (top) or to save them for later in the shopping cart (bottom). If they decide to purchase a product at a later date, they won’t have to locate it again.</em></figcaption>
</figure>

<p style="margin-left: 40px;">Making information easily accessible for future interactions can be as simple as providing a link to your web content and making sure that the link is accessible across all devices. Many people will email themselves a link to a particular page to remember to visit it again or to simplify the task of reaccessing the content, so ensure that your site allows deep linking. (<a href="../deep-linking-is-good-linking/index.php">Deep linking</a> refers to supporting links to pages other than your site’s homepage.)</p>

<figure class="caption"><img alt="RealtorTextShare" height="670" src="https://media.nngroup.com/media/editor/2017/03/16/realtortextshare.png" width="504"/>
<figcaption><em>Realtor.com has email and text sharing capabilities on real estate listings within its iPad application.</em></figcaption>
</figure>

<ul>
	<li><strong>Facilitate next steps.</strong> Be proactive — identify patterns of use and see if you can support them. Whenever you trigger an action that drives users to a different channel or you find users following a consistent crosschannel workflow, find ways to help them complete the next action rather than leaving this burden up to them. Google Maps’ Send to phone feature discussed above shows that designers understood how people use directions and supported them in transitioning to a different device. This is an example of a user-initiated pattern of use that was identified and facilitated by the design.</li>
</ul>

<p style="margin-left: 40px;">Think about when people may switch channels and iron any wrinkles in the transition. In the baby-swing example, Walmart did a nice job of facilitating the transition to the store for pickup. The email notifying the user that the items were ready for pickup provided step-by-step instructions and also showed a picture of the signage that customers should look for at the store. In addition, it informed users about a mobile application that could  speed up the process.</p>

<figure class="caption"><img alt="Walmart Store Pickup Email" height="1441" src="https://media.nngroup.com/media/editor/2017/03/16/walmartstorepickupemail.png" width="720"/>
<figcaption><em>When Walmart emailed the user that her order was ready for pickup it included detailed information about how to proceed with the task in the store.</em></figcaption>
</figure>

<figure class="caption"><img alt="UPS My Choice Roadblock" height="783" src="https://media.nngroup.com/media/editor/2017/03/16/ups-mychoice.png" width="440"/>
<figcaption><em>The UPS My Choice app provided an error message when the user entered an address not listed as residential. The error message suggested to contact the company, but it was up to the user to find the phone number or contact page. A clickable support number and a link to the support page on the website should have been part of the error message in order to facilitate users’ next step.</em></figcaption>
</figure>

<h2>Seamless Experiences Often Require Changes Behind the Scenes</h2>

<p>Many roadblocks or problematic transitions between channels originate in issues much deeper than the front-end design of the channel experiences. As organizations have grown to accommodate new channels, technologies and procedures were added incrementally and often completely separately. This lack of coordination in processes has created a fragmented backend infrastructure for many organizations. A truly seamless experience often requires investment in integrating backend systems to prevent roadblocks stemming back to system constraints. For example, in the Walmart example, the store was not able to process returns on online orders. This indicates separate, isolated systems for the online and offline commerce. What may seem as an internal problem that should not affect users ends up seriously impacting the user experience.</p>

<p>Backend technologies are only one aspect of the changes required behind the scene. Other constraints could be caused by separate fulfillment processes, disjointed organizational hierarchies and working teams, or old procedures that no longer work in the new multichannel world. Creating a seamless omnichannel experience depends on breaking down silos and integrating previously discrete experiences into a continuum.</p>

<h2>Seamless: 3 of 5 Recommended Omnichannel Components</h2>

<p>As companies and organizations design for the larger user experience, they should strive for supporting painless user transitions across channels. Seamless customer journeys allow users to interact with organizations on their terms, resulting in great customer experiences and lasting relationships.</p>

<p>In addition to being seamless, omnichannelchannel experiences must be <a href="../omnichannel-consistency/index.php">consistent</a>, <a href="../context-specific-cross-channel/index.php">optimized for context</a>, orchestrated, and collaborative.</p>

<p>Our full day course on <a href="../../courses/cross-channel-user-experience/index.php">Omnichannel Journeys and Customer Experience</a> covers these recommended characteristics further.</p>

<h2>Related Video</h2>

<p>Watch Kim Flaherty explain omnichannel user experience (2 min. video):</p>

<div style="width:350px;">
<div class="flex-video"><iframe allowfullscreen="" frameborder="0" scrolling="no" src="https://www.youtube.com/embed/mhmu0XUaTuM?rel=0"></iframe></div>
</div>
</section>
  

  
  <section>
    
        
    
  </section>
  
  
  
  
    <section>
    <br>
    <p>
      <em>(An earlier version of this article was originally published November 24, 2013. The article was last updated and revised on March 19, 2017.)</em>
    </p>
    </section>
  
  

  


  <section>
    <br />
    <p class="share"><b>Share <span class="hide-for-small-down">this article</span>:</b> <a href="https://twitter.com/intent/tweet?url=http://www.nngroup.com/articles/seamless-cross-channel/&amp;text=Seamlessness%20in%20the%20Omnichannel%20User%20Experience&amp;via=nngroup">Twitter</a> | <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=http://www.nngroup.com/articles/seamless-cross-channel/&amp;title=Seamlessness%20in%20the%20Omnichannel%20User%20Experience&amp;source=Nielsen%20Norman%20Group">LinkedIn</a> | <a href="https://plus.google.com/share?url=http://www.nngroup.com/articles/seamless-cross-channel/">Google+</a> | <a href="mailto:?subject=NN/g Article: Seamlessness in the Omnichannel User Experience&amp;body=http://www.nngroup.com/articles/seamless-cross-channel/">Email</a></p>
    <hr />
  </section>

  <footer class="l-subsection">
    
    
    <div class="learn-more">
      <h2>Learn More</h2>
      <p><a href="../subscribe/index.php">Subscribe to the weekly newsletter</a> to get notified about future articles.</p>
      </div>
    
    

    <div class="row show-for-small-down related-content">
      
      
      

      
      
        <div class="column small-12 topic-links">All articles about:
          <ul>
            
            <li><a href="../../topic/customer-journeys/index.php">Customer Journeys</a></li>
            
            <li><a href="../../topic/e-commerce/index.php">E-commerce</a></li>
            
            <li><a href="../../topic/omnichannel/index.php">omnichannel</a></li>
            
            <li><a href="../../topic/strategy/index.php">Strategy</a></li>
            
          </ul>
        </div>
      
      

      
      
        <div class="column small-12">
          <h3>UX Conference Training Courses</h3>
           <ul class="no-bullet">
    
        <li><a href="../../courses/ux-basic-training/index.php">UX Basic Training</a></li>
    
        <li><a href="../../courses/journey-mapping/index.php">Journey Mapping to Understand Customer Needs</a></li>
    
        <li><a href="../../courses/facilitating-ux-workshops/index.php">Facilitating UX Workshops</a></li>
    
        <li><a href="../../courses/analytics-and-user-experience/index.php">Analytics and User Experience</a></li>
    
        <li><a href="../../courses/generating-big-ideas/index.php">Generating Big Ideas with Design Thinking</a></li>
    
</ul>
        </div>
      
      

      
      
        <div class="small-12 column">
          <h3>Research Reports</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../reports/ecommerce-user-experience/index.php">E-Commerce User Experience</a></li>
              
            
              
                <li><a href="../../reports/usability-return-on-investment-roi/index.php">Return on Investment (ROI) for Usability</a></li>
              
            
              
                <li><a href="../../reports/b2b-websites-usability/index.php">B2B Website Usability for Converting Users into Leads and Customers</a></li>
              
            
              
                <li><a href="../../reports/celebrating-holidays-and-current-events-web/index.php">Celebrating Holidays and Current Events on the Web</a></li>
              
            
          </ul>
        </div>
      
      

      
      
        <div class="small-12 column">
          <h3>Online Seminars</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../online-seminars/ux-real-world/index.php">UX in the Real World</a></li>
              
            
              
                <li><a href="../../online-seminars/how-change-your-corporate-culture/index.php">How to Change Your Corporate Culture to be User First</a></li>
              
            
          </ul>
        </div>
      
      
    </div>

    <div class="hide-for-small-down row related-content">
      <div class="medium-6 columns">
        
        
          <div>
            <h3>Articles</h3>
            <ul class="no-bullet">
              
                
                <li><a href="../context-specific-cross-channel/index.php">Optimizing for Context in the Omnichannel User Experience</a></li>
                
              
                
                <li><a href="../omnichannel-consistency/index.php">Consistency in the Omnichannel Experience</a></li>
                
              
                
                <li><a href="../customer-service-omnichannel-ux/index.php">Minimize the Need for Customer Service to Improve the Omnichannel UX</a></li>
                
              
                
                <li><a href="../diary-studies/index.php">Diary Studies: Understanding Long-Term User Behavior and Experiences </a></li>
                
              
                
                <li><a href="../customer-journeys-omnichannel/index.php">Customer Journeys and Omnichannel User Experience</a></li>
                
              
            </ul>
          </div>
        
        

        
          
          <div>
            <h3>Research Reports</h3>
            <ul class="no-bullet">
              
                
                  <li><a href="../../reports/ecommerce-user-experience/index.php">E-Commerce User Experience</a></li>
                
              
                
                  <li><a href="../../reports/usability-return-on-investment-roi/index.php">Return on Investment (ROI) for Usability</a></li>
                
              
                
                  <li><a href="../../reports/b2b-websites-usability/index.php">B2B Website Usability for Converting Users into Leads and Customers</a></li>
                
              
                
                  <li><a href="../../reports/celebrating-holidays-and-current-events-web/index.php">Celebrating Holidays and Current Events on the Web</a></li>
                
              
            </ul>
          </div>
          
        

      </div>
      <div class="medium-6 columns">
        
          
          <div>
            <h3>UX Conference Training Courses</h3>
             <ul class="no-bullet">
    
        <li><a href="../../courses/ux-basic-training/index.php">UX Basic Training</a></li>
    
        <li><a href="../../courses/journey-mapping/index.php">Journey Mapping to Understand Customer Needs</a></li>
    
        <li><a href="../../courses/facilitating-ux-workshops/index.php">Facilitating UX Workshops</a></li>
    
        <li><a href="../../courses/analytics-and-user-experience/index.php">Analytics and User Experience</a></li>
    
        <li><a href="../../courses/generating-big-ideas/index.php">Generating Big Ideas with Design Thinking</a></li>
    
</ul>
          </div>
          
        
        
          
          <div>
            <h3>Online Seminars</h3>
            <ul class="no-bullet">
            
                <li><a href="../../online-seminars/ux-real-world/index.php">UX in the Real World</a></li>
            
                <li><a href="../../online-seminars/how-change-your-corporate-culture/index.php">How to Change Your Corporate Culture to be User First</a></li>
            
            </ul>
          </div>
          
        
      </div>
    </div>
  </footer>
</article>

        </div>

      

    
  </div>
  <div class="row">
    
      <div class="column small-12 show-for-small-down l-navrail announcements">
        
          
        
      </div>
    
    
  </div>

    </div>
    




<footer id="page-footer">
    <div class="row hide-for-print">
        <div class="column small-12 medium-6">
            <section class="footer-popular">
                
                <h1>Popular Topics</h1>
                <div style="float: left;  width: 15em; display: block;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/applications/index.php">Application Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/e-commerce/index.php">E-Commerce Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/eyetracking/index.php">Eyetracking </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/information-architecture/index.php">Information Architecture </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/intranets/index.php">Intranet Design </a></li>
	<li><a href="../../topic/interaction-design/index.php">Interaction Design </a></li>
</ul>
</div>

<div style="float: left;  width: 15em; display: block; list-style: none;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/mobile-and-tablet-design/index.php">Mobile and Tablet Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/credibility-persuasion/index.php">Persuasive Design</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/research-methods/index.php">Research Methods</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/user-testing/index.php">User Testing </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/web-usability/index.php">Web Usability </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/writing-web/index.php">Web Writing &amp; Content Strategy </a></li>
</ul>
</div>

<p>&nbsp;</p>

                
            </section>
        </div>

        <div class="column small-12 medium-6">
            <div class="row">
                <div class="column small-6">
                    <section class="footer-about">
                        <h1>Follow Us</h1>
                        <ul>
	<li><a href="../subscribe/index.php">Subscribe to our weekly UX newsletter</a>,&nbsp;Jakob Nielsen&#39;s Alertbox</li>
	<li>Visit Don Norman&#39;s website, <a href="http://www.jnd.org/"> www.jnd.org </a></li>
	<li>Visit Bruce Tognazzini&#39;s website, <a href="http://www.asktog.com/"> www.asktog.com </a></li>
	<li style="padding-top: 6px;"><a class="twitter-follow-button" data-show-count="false" data-size="large" href="https://twitter.com/nngroup"><img alt="Follow @nngroup on Twitter" height="20" src="https://media.nngroup.com/media/editor/2016/12/30/twitter-follow-button.png" style="width: 117px; height: 20px;" width="117" /></a></li>
	<li style="padding-top: 6px;"><img alt="" height="14" src="https://media.nngroup.com/media/editor/2016/07/26/feed-icon-14x14.png" style="vertical-align:text-top;width:14px;margin-right:.25em;" width="14" /><a href="../../feed/rss/index.php">Subscribe to our RSS feed</a></li>
</ul>

                    </section>
                </div>

                <div class="column small-6">
                    <section class="footer-about right-column">
                        <h1>About
</h1>
                        <ul class="no-bullet">
	<li><a href="../../about/contact/index.php">Contact information</a></li>
	<li><a href="../../about/index.php">About Us</a></li>
	<li><a href="../../about/why-nng/index.php">Why NN/g? </a></li>
	<li><a href="../../return-policy/index.php">Return Policy</a></li>
</ul>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="copyright column small-12">
            <p><a href="../../copyright-and-reprint-info/index.php">Copyright</a> &copy; 1998-2017 Nielsen Norman Group, All Rights Reserved.</p>
        </div>
    </div>
</footer>


    


        <script src="https://media.nngroup.com/static/vendor/fastclick/js/fastclick.min.js"></script>
        <script src="https://media.nngroup.com/static/vendor/foundation/js/foundation.min.js"></script>
        <script src="http://media.nngroup.com/static/js/app.js"></script>
        <script src="http://media.nngroup.com/static/js/scroll-to-fixed.js"></script><script>var externalLinks = document.querySelectorAll('a[href^="http"]');for (var i = externalLinks.length-1; i >= 0; i--) {externalLinks[i].addEventListener("click", function(event) { alert("External links are disabled for this test."); event.preventDefault(); }, false);}var mailLinks = document.querySelectorAll('a[href^="mailto"]');for (var i = mailLinks.length-1; i >= 0; i--) {mailLinks[i].addEventListener("click", function(event) { alert("Email links are disabled for this test."); event.preventDefault(); }, false);}var externalForms = document.querySelectorAll('form');for (var i = externalForms.length-1; i >= 0; i--) {externalForms[i].addEventListener("submit", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false); externalForms[i].addEventListener("click", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false);}</script>

        <script>
            
            $(function() {
                $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
                $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
            });
        </script>

        
<script src="https://media.nngroup.com/static/js/articles.js"></script>


        
        <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1072699960;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072699960/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
    </body>

<!-- Mirrored from www.nngroup.com/articles/seamless-cross-channel/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 14:54:49 GMT -->
</html>
