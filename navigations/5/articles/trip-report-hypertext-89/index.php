<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->





    
<!-- Mirrored from www.nngroup.com/articles/trip-report-hypertext-89/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:14:36 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1026.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"beacon":"bam.nr-data.net","queueTime":9,"applicationTime":1094,"agent":"","licenseKey":"1a30ef9931","errorBeacon":"bam.nr-data.net","applicationID":"2048992","transactionName":"NFxSMRBUX0EDUkIPDA0WdhAMVkVbDV8ZBxEXUFMJB0YfRAtUQRVZIktEDAFZVHYHRVcPD01eVRE="}</script>
        <title>Hypertext&#39;89 Trip Report: Article by Jakob Nielsen</title><meta property="og:title" content="Hypertext&#39;89 Trip Report: Article by Jakob Nielsen" />
  
        
        <meta name="description" content="Jakob Nielsen&#39;s trip report from the ACM Hypertext&#39;89 conference. Includes summary of Meyrowitz&#39; discussion of open integrating hypertext and the extent to which the Memex vision has been realized so far.">
        <meta property="og:description" content="Jakob Nielsen&#39;s trip report from the ACM Hypertext&#39;89 conference. Includes summary of Meyrowitz&#39; discussion of open integrating hypertext and the extent to which the Memex vision has been realized so far." />
        
  
        
	
        
        <meta name="keywords" content="history of hypertext, Hypertext89, ACM Hypertext&#39;89 conference, Norman Meyrowitz,  Memex, Vannevar Bush, integrated open linking, FRESS, HyperCard, Intermedia, NoteCards, information retrieval, IR">
        

        

        <meta name="robots" content="noarchive">
        <meta name="viewport" content="width=device-width">

        <link rel="search" type="application/opensearchdescription+xml" title="NN/g Search" href="../../opensearch.xml" />
        <link rel="shortcut icon" href="http://media.nngroup.com/static/img/favicon.ico" />

        <link rel="stylesheet" href="https://media.nngroup.com/static/vendor/font-awesome/css/font-awesome.min.css">

        
        <link rel="stylesheet" href="https://media.nngroup.com/static/css/7232a960f1df.css" type="text/css" />
        <link rel="stylesheet" href="http://fast.fonts.com/t/1.css?apiType=css&amp;projectid=c5846260-9def-46ec-9407-0f1349bd2d64" type="text/css">

        <script src="https://media.nngroup.com/static/vendor/modernizr/js/modernizr.min.js"></script>

        <script type="text/javascript" src="https://media.nngroup.com/static/js/3283be5def74.js"></script>
        

        

        
        
        <!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=229034,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('http://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->


<!-- Google Analytics Tracking Code for http://nngroup.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23132681-1', 'nngroup.com');
  ga('require', 'linkid', 'https://www.nngroup.com/articles/trip-report-hypertext-89/linkid.js');
  ga('require', 'displayfeatures');

// Optimizely Universal Analytics Integration
 window.optimizely = window.optimizely || [];
 window.optimizely.push("activateUniversalAnalytics");

  ga('send', 'pageview');

</script>
<script>
/**
* Function to track a click on an outbound link in Google Analytics.
*/
function trackOutboundLink(label, url) {
    if(window.ga && ga.loaded) {
        ga('send', 'event', 'outbound', 'click', label, {'hitCallback': function(){document.location = url;}
        });
    } else {
        document.location = url;
    }
}
/**
* Function to track a click on a link in the article announcement area.
*/
$('#articleAnnouncement a').on('click', function(){
  ga('send', 'event', 'link', 'click', 'articleAnnouncement');
});
</script>

<script type="text/javascript">
 setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2421.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



<!-- Hotjar Tracking Code for http://nngroup.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:121605,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

        <script src="https://media.nngroup.com/static/js/analytics/track_outbound_link.js"></script>
        
        
        
    </head>
    <body class="location-articles articles-detail">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
    <header id="page-header">
        <div id="cart" class="shopping-cart mobile-cart show-for-small-down"></div>
        <div class="row">
            <div id="masthead" class="masthead small-12 medium-7 column">

                <!-- The button to toggles menu on small screen -->

                <h1><a href="../../index.php"><img src="http://media.nngroup.com/static/img/logo-wordmark.png" width="74" height="30"> Nielsen <span class="norman">Norman</span> Group</a></h1>
                <p class="tagline show-for-medium-up">Evidence-Based User Experience Research, Training, and Consulting</p>

                <!-- The button to toggles form search on small screen -->


                <div style="clear: right;width:  100%;height: 1px;"></div><?php define('ROOT_PATH',$_SERVER["DOCUMENT_ROOT"] . '/apps/navigation_styles/navigations/5'); ?><?php include(ROOT_PATH)."/navigation.php"; ?>

                <!-- Drop down menu on small screen -->
                <div id="hideNav" class="show-for-small-down">
                  
                  <ul class="menu-content">
                    <li><a href="../../index.php">Home</a></li>
                    <li class="has-submenu">
                      <a>Training</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../training/index.php">Overview</a></li>
                        
<li id="nav-ux-conference"><a href="../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../online-seminars/index.php"><span>Online Seminars</span></a></li>

                      </ul>
                    </li>
                    <li class="has-submenu">
                      <a>Consulting</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
  <li><a href="../../consulting/index.php">Overview</a></li>
  
  <li><a href="../../consulting/benchmarking/index.php">Benchmarking</a></li>
  
  <li><a href="../../consulting/expert-design-review/index.php">Expert Design Review</a></li>
  
  <li><a href="../../consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
  
  <li><a href="../../consulting/usability-testing/index.php">Usability Testing </a></li>
  
  <li><a href="../../consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
  
  <li><a href="../../in-house-training/index.php">In-House Training</a></li>
</ul>

                    </li>

                    <li><a href="../../reports/index.php">Reports</a></li>
                    <li><a href="../index.php">Articles</a></li>
                    <li class="has-submenu"><a>About NN/G</a><i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../about/index.php">Overview</a></li>
                        <li><a href="../../people/index.php">People</a></li>
                        <li><a href="../../about/why-nng/index.php">Why NN/g?</a></li>
                        <li><a href="../../about/contact/index.php">Contact</a></li>
                        <li><a href="../../news/index.php">News</a></li>
                        <li><a href="../../about/history/index.php">History</a></li>
                        <li><a href="../../books/index.php">Books</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                </div>

                <hr class="header-divider show-for-small-only"/>


            </div>
       </div>

        <div id="hideNav">            <nav class="primary-nav small-12 column">
                <ul>
                    <li><a class="navitem" id="nav-home" href="../../index.php">Home</a></li>
                    <li><a class="navitem" id="nav-training" href="../../training/index.php">Training</a></li>
                    <li><a class="navitem" id="nav-consulting" href="../../consulting/index.php">Consulting</a></li>
                    <li><a class="navitem" id="nav-reports" href="../../reports/index.php">Reports</a></li>
                    <li><a class="navitem" id="nav-articles" href="../index.php">Articles</a></li>
                    <li><a class="navitem" id="nav-about" href="../../about/index.php">About NN/G</a></li>
                </ul>
            </nav>
            <nav class="secondary-nav small-12 medium-12 large-12 columns">
                
                
            </nav>
        </div>
    </header>
    <div class="l-content"><span id="preventClose" style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776; Menu</span>
    
  

  

  <div class="row">
    
      
      <section class="small-12 medium-3 columns collapsable-content l-navrail articles-left-column">
        <h1 class="show-for-small-down collapse-link mobile-browse-header">Browse by Topic and Author</h1>
          <div class="row collapsable">

            
            <div class="small-6 medium-12 columns">
              <section id="article-popular-topics" style="display: none;">
                
                <h1>Topics</h1>
                <ul id="article-popular-topics">
                    
                    <li>
                        
                        <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/intranets/all/index.php">Intranets</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/user-testing/all/index.php">User Testing</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                        
                    </li>
                    
                    <li><a href="#" id="article-see-all-topics"><strong>See all topics&hellip;</strong></a></li>
                </ul>
                
              </section>
              <section id="article-all-topics" style="display: none;">
                
                <h1>
                  <span class="show-for-medium-up">All Article Topics</span>
                  <span class="show-for-small-down">All Topics</span>
                  (<a href="#" id="article-hide-all-topics">hide</a>)
                </h1>
                <ul>
                  
                  <li>
                    
                    <a href="../../topic/accessibility/all/index.php">Accessibility</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/agile/all/index.php">Agile</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/analytics-and-metrics/all/index.php">Analytics &amp; Metrics</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/applications/all/index.php">Application Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/b2b-websites/all/index.php">B2B Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/behavior-patterns/all/index.php">Behavior Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/branding/all/index.php">Branding</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/content-strategy/all/index.php">Content Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/corporate-websites/all/index.php">Corporate Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/design-patterns/all/index.php">Design Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-design-process/all/index.php">Design Process</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/email/all/index.php">Email</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/eyetracking/all/index.php">Eyetracking</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/heuristic-evaluation/all/index.php">Heuristic Evaluation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/human-computer-interaction/all/index.php">Human Computer Interaction</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-ideation/all/index.php">Ideation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/information-architecture/all/index.php">Information Architecture</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/interaction-design/all/index.php">Interaction Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/international-users/all/index.php">International Users</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/intranets/all/index.php">Intranets</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-management/all/index.php">Management</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/navigation/all/index.php">Navigation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/non-profit-websites/all/index.php">Non-Profit Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/personas/all/index.php">Personas</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/persuasive-design/all/index.php">Persuasive Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/prototyping/all/index.php">Prototyping</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/psychology-and-ux/all/index.php">Psychology and UX</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/research-methods/all/index.php">Research Methods</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/search/all/index.php">Search</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/social-media/all/index.php">Social Media</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/strategy/all/index.php">Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/user-testing/all/index.php">User Testing</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-teams/all/index.php">UX Teams</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/visual-design/all/index.php">Visual Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/writing-web/all/index.php">Writing for the Web</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/young-users/all/index.php">Young Users</a>
                    
                  </li>
                  
                </ul>
                
              </section>
            </div>
            <script>
              /* Determine which topics list will be displayed: the full or the
               * partial.  We are inlining this close to the list itself to
               * prevent rendering delays on the client
               */
              var topic_list = $.cookie("article-topics");

              var popular_topics = $('#article-popular-topics');
              var all_topics = $('#article-all-topics');

              if(topic_list == 1) {
                  popular_topics.hide();
                  all_topics.show();
              } else {
                  popular_topics.show();
                  all_topics.hide();
              }
            </script>
            

            
            <div class="small-6 medium-12 columns">
              
              <section id="article-initial-authors">
                  <h1>Author</h1>
                  <ul>
                    
                    <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                    
                    <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                    
                    <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                    
                    <li><a href="#" onClick="$('#article-initial-authors').hide(); $('#article-all-authors').show();"><strong>See all authors&hellip;</strong></a></li>
                  </ul>
              </section>
              <section id="article-all-authors" style="display: none;">
                <h1>
                  All Authors
                  (<a href="#" id="article-hide-all-authors">hide</a>)
                </h1>
                <ul>
                  
                  <li><a href="../author/raluca-budiu/index.php">Raluca Budiu</a></li>
                  
                  <li><a href="../author/patty-caya/index.php">Patty Caya</a></li>
                  
                  <li><a href="../author/susan-farrell/index.php">Susan Farrell</a></li>
                  
                  <li><a href="../author/therese-fessenden/index.php">Therese Fessenden</a></li>
                  
                  <li><a href="../author/kim-flaherty/index.php">Kim Flaherty</a></li>
                  
                  <li><a href="../author/sarah-gibbons/index.php">Sarah Gibbons</a></li>
                  
                  <li><a href="../author/aurora-harley/index.php">Aurora Harley</a></li>
                  
                  <li><a href="../author/kate-kaplan/index.php">Kate Kaplan</a></li>
                  
                  <li><a href="../author/page-laubheimer/index.php">Page Laubheimer</a></li>
                  
                  <li><a href="../author/angela-li/index.php">Angie Li</a></li>
                  
                  <li><a href="../author/hoa-loranger/index.php">Hoa Loranger</a></li>
                  
                  <li><a href="../author/kate-meyer/index.php">Kate Meyer</a></li>
                  
                  <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                  
                  <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                  
                  <li><a href="../author/kara-pernice/index.php">Kara Pernice</a></li>
                  
                  <li><a href="../author/christian-rohrer/index.php">Christian Rohrer</a></li>
                  
                  <li><a href="../author/amy-schade/index.php">Amy Schade</a></li>
                  
                  <li><a href="../author/katie-sherwin/index.php">Katie Sherwin</a></li>
                  
                  <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                  
                  <li><a href="../author/kathryn-whitenton/index.php">Kathryn Whitenton</a></li>
                  
                </ul>
              </section>
              
            </div>
            
          </div>

          
          <section class="hide-for-small-down">
            
              
              <h1>Recent Articles</h1>
              
              <ul>
                  
                  <li><a href="../seamless-cross-channel/index.php">Seamlessness in the Omnichannel User Experience</a></li>
                  
                  <li><a href="../brand-intention-interpretation/index.php">Experience Design: Bridging Brand Intention and Brand Interpretation</a></li>
                  
                  <li><a href="../eyetracking-tasks-efficient-scanning/index.php">Scanning Patterns on the Web Are Optimized for the Current Task</a></li>
                  
                  <li><a href="../flat-design-best-practices/index.php">Flat-Design Best Practices</a></li>
                  
                  <li><a href="../ux-user-stories/index.php">Accounting for UX Work with User Stories in Agile Projects</a></li>
                  
                  <li><a href="../index.php"><strong>See all articles&hellip;</strong></a></li>
              </ul>
              
            
          </section>
          

          
          
          <section class="hide-for-small-down">
            <h1>Popular Articles</h1>
            
            <ul>
                
                <li><a href="../usability-101-introduction-to-usability/index.php">Usability 101: Introduction to Usability</a></li>
                
                <li><a href="../top-10-mistakes-web-design/index.php">Top 10 Mistakes in Web Design</a></li>
                
                <li><a href="../how-users-read-on-the-web/index.php">How Users Read on the Web</a></li>
                
                <li><a href="../f-shaped-pattern-reading-web-content/index.php">F-Shaped Pattern For Reading Web Content</a></li>
                
                <li><a href="../ten-usability-heuristics/index.php">10 Usability Heuristics for User Interface Design</a></li>
                
                <li><a href="../intranet-design-2016/index.php">10 Best Intranets of 2016</a></li>
                
                <li><a href="../which-ux-research-methods/index.php">When to Use Which User-Experience Research Methods</a></li>
                
                <li><a href="../response-times-3-important-limits/index.php">Response Times: The 3 Important Limits</a></li>
                
                <li><a href="../why-you-only-need-to-test-with-5-users/index.php">Why You Only Need to Test with 5 Users</a></li>
                
                <li><a href="../page-fold-manifesto/index.php">The Fold Manifesto: Why the Page Fold Still Matters</a></li>
                
            </ul>
          </section>
          
          

          
            <section class="hide-for-small-down">
              
            </section>
          

      </section>
      

      
        <div class="small-12 medium-9 columns content-body">
          




<article class="l-subsection alertbox-article-content article-detail-bl">
  
  <header>
    <div class="l-subsection">
      <h1>Hypertext&#39;89 Trip Report</h1>
      <div class="author-meta">by
        
          
            
            <a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a>
            
          
        on  April 1, 1990
        
      </div>

      

      
      <div class="topic-links">
        Topics:
        
          <ul>

  <li><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction</a></li>

</ul>
        
      </div>
      
    </div>
  </header>
  

  
  
  

  
  <section id="article-body"><p>It may still be possible to <a href="http://www.amazon.com/exec/obidos/ISBN=0897913396/useitcomusableinA/"> buy the proceedings online</a>.</p>

<p>Pittsburgh, PA, 5-8 November 1989<br/>
<em>by <a href="../../people/jakob-nielsen/index.php"> Jakob Nielsen </a> </em></p>

<p>At the first hypertext conference, <a href="../trip-report-hypertext-87/index.php"> Hypertext'87</a>, there was some talk that we would soon stop having special hypertext conferences, just as we do not have special conferences about, say word processors. Future conferences would be focused on various application domains and might every now and then include papers on the use of a hypertext system. On the other hand there are still conferences about e.g. databases even though they have been one of the most commercially successful and widespread applications of computers. It may be too soon to call the final outcome, but right now it seems that hypertext conferences are proliferating (almost too much, in fact).</p>

<p>In any case, the Hypertext'89 conference was a success with a large number of papers submitted and high quality for those papers that got accepted. The conference attracted 650 participants, which was fewer than I had expected but still quite good for a second conference in a new field.</p>

<h2>Integration with the World</h2>

<p>The main theme of the conference was <strong> integrating hypertext </strong> with the rest of the world. Until now, hypertext systems have mostly been monolithic stand-alone systems with no connections to the rest of the user's computational environment or to alternative ways of accessing information. The only reason this has been acceptable is that hypertext provides such wonderful new facilities that users have been overwhelmed by the freedom and flexibility offered by hypertext navigation. It is actually possible to live within the bounds of a hypertext system and not suffer too much because hypertext is flexible enough to structure information in any way the user wants.</p>

<p>In the long term, however, users also want to do something with the information they browse in a hypertext and this is when hypertext systems will need to be integrated with the rest of the world. This conference presented new integrational trends in four areas (discussed further below):</p>

<ul>
	<li>computational hypertext</li>
	<li>information retrieval</li>
	<li>hypertext as a system service</li>
	<li>hypertext as file system interfaces</li>
</ul>

<p>Talking about integration, Bob Glushko from Search Technology presented a paper on the issues in integrating multi-document hypertext, claiming that cross-document linking would not always make sense. Considering that links are the Holy Grail of hypertext, this position was somewhat controversial, but then Glushko likes being controversial. His example was to consider the following four documents: 1) the classified telephone directory (yellow pages), 2) a software reference manual, 3) a thesaurus, and 4) a street map. Each of these would make a good hypertext document in its own right but it would be no good to have a hypertext system integrating the yellow pages and your software manual. Of these four documents, combining the yellow pages and the street map would make the most sense but there could also be some benefit from combining the yellow pages and the thesaurus. The latter example was not symmetric, however, since the thesaurus would help in the use of the yellow pages while having the yellow pages available would not help a user with a specific interest in using the thesaurus. Glushko's conclusion was that the two documents needed to have a certain degree of <strong> complementarity </strong> for a hypertext combination to be useful. There should be some overlap between the documents but not a complete overlap.</p>

<p>Given that a multi-document hypertext might be desirable, the next issue is how much to integrate the documents. Glushko's experience was that usability was enhanced by the use of the explicit structure of each document so he advocated keeping the identity of each document and avoiding a complete integration. Their approach was to let the user select one document as the primary focus of browsing and then have links to related information in other documents appear as cross-references. In their system, users do not switch context when following a cross-reference unless they make an explicit choice to do so.</p>

<h2>Even the Dog has Hypertext (Meyrowitz)</h2>

<p>Norman Meyrowitz from Brown University is one of the main developers of Intermedia and gave a very good keynote address entitled "Hypertext-does it reduce cholesterol, too?" He introduced the topic by a parody of the current "hyper hype" where every computer product seems to get hypertext added, just like some food products get added fiber. Meyrowitz asked if hypertext was getting to be the <strong> oat bran of computing</strong>. He hoped that we would not miss the real advantage of hypertext among all this superficial excitement.</p>

<p>Meyrowitz' vision for hypertext was an intelligent <strong> infrastructure </strong> to link together information at our fingertips in an information environment. This in contrast to the currently prevailing desktop metaphor for user interfaces where users move among objects manually. Any selection by the content of objects is haphazard and is at best done by the name of the object. For example it can be quite hard to find the appropriate file on a large harddisk. Instead it would be possible to integrate hypertext with the basic operating system and use hypertext links to facilitate moving among the user's units of information (whether files or smaller "chunks" of data).</p>

<p>When the user transfers data by a copy-paste operation, a temporary link is formed between the source document and the destination document but hypertext could make these links persistent-even across applications if the basic linking mechanism was a system hypertext service. Some limited support for cross-application hypertext links is starting to appear in e.g. some of Microsoft's applications and in version 7 of the Macintosh operating system, but Meyrowitz would like to see a true shift to navigational linking as the extension of the traditional cut-copy-paste mechanism for moving data. His own hypertext system, Intermedia, is in fact built on the principle of providing a standardized linking protocol for other applications to use, and this has proven quite useful in extending Intermedia with new specialized packages. Even so, the link service is internal to applications running under Intermedia and not integrated with the complete computing environment.</p>

<p>There will be many different media types needed in future hypermedia documents. Not even the Intermedia people will be able to produce good enough editors and utilities for all of them so Meyrowitz felt the need for third party developers. Similar problems have been felt for traditional integrated business software where <strong> closed </strong> integration rarely satisfies users. The solution has been <strong> open </strong> integration based on system services like the Macintosh clipboard which Meyrowitz would like to see extended to a "linkboard" to handle hypertext anchors with names, keywords, and attributes. Furthermore, Meyrowitz wanted computer systems to offer a set of standard building blocks for hypertext integration like dictionaries, glossaries, thesauri, spelling correctors, and linguistic tools like morphology. Some of this already exists like the Intermedia dictionary and the Perseus morphology tools, but only the NeXT machine's Webster's Dictionary comes close to being a true system service utilized by external applications.</p>

<p>Most fields have <strong> too little vision </strong> but Meyrowitz warned us that the hypertext field may have too much: We can all smell the future so well that we may forget to build it. He would like to see major projects building actual hypertext documents with interesting content instead of the current proliferation of one-shot proof-of-concept projects. Also it was a problem that too much funding in our field was for short term projects like "port Intermedia to system X in six months" instead of more long term projects. In contrast, there were currently plans in the US for almost two billion dollars government funding for a supercomputer network without any guarantee that it would be able to support hypermedia needs.</p>

<h3>We Cannot Build the Memex</h3>

<p>Besides talking about the need to have hypertext as a system service and integrating it with computer file systems, Meyrowitz also spent some of his keynote address comparing <strong> Vannevar Bush</strong>'s original <strong> Memex </strong> proposal from 1945 with current hypertext capabilities. In general, it turns out that we are still far from being able to support Bush's vision even if we consider frontline computer research capabilities which have not been integrated with hypertext yet.</p>

<p>Besides the main desktop Memex, Bush also envisaged portable extensions of his hypertext system in the form of a wireless device in communication with the home system. It would be possible to construct such a device on the basis of cellular telephone technology combined with modems but the bandwidth would not be large enough to transmit high-resolution images. For input, Bush described voice recording, handwriting, and scanning. None of these are currently in common use for personal computers. <strong> Scanning </strong> is possible but still expensive and have cumbersome software, both with regard to optical character recognition and adjustment of scanned images to look well on a computer screen. <strong> Handwriting recognition </strong> has been demonstrated to some extent and we seem to be within a few years of a breakthrough. <strong> Voice recognition </strong> is still not possible in the general case but voice applications like voice mail and voice response systems are starting to appear. For integration with hypertext, however, we do need the ability to have voice input transcribed to a permanent record.</p>

<p>Also, Bush described a portable input device in the form of a walnut-sized camera worn on the users forehead. This would be possible with current digital camera technology but it is not likely to see widespread use.</p>

<p>Even if some of the input devices suggested by Bush might become available in hypertext systems sometime in the future, we are still not up to handling the storage needs for these media. For example a user with the walnut-sized head-mounted camera might snap 5,000 photos per day for a storage need of about 10 Gigabytes. This translates to about 20 writable optical disks per day at a current cost of $5,000 per day. Meyrowitz suggested that CD-ROMs might not be the most appropriate storage technology for the future but we have still not found a really cheap mass storage device. Somebody else remarked that CD-ROMs are almost always either much too big or much too small for the amount of data one has for any given application.</p>

<p>The Memex vision remains to be realized even in its more social and cultural aspects. Bush had described a Memex user as buying most hypertext material from outside sources for merging with the user's home Memex base but there is almost no current hypertext data for general use available for sale. And in most cases it would not be possible to integrate it with the user's own hypertext materials anyway.</p>

<h2>Demos: Many Features, Little Content</h2>

<p>The Hypertext'89 conference had a nice demo room with lots of activity every evening. Unfortunately I did not get to see all that many demos myself because I was busy most of the time showing my own demo of a hypertext interface to the Unix network news. Demonstrations were run on a continuous basis and allowed people to drift between the demos in small groups. The advantage of this format was that it was flexible and allowed more interaction between the demo presenters and the audience than demos set up in an auditorium. Also, it was often possible to get hands-on experience with the systems. The disadvantage (besides taking up too much of the presenters' time) was that it was very hard to see the more popular demos because no screen projection was being used. Maybe the hypertext conferences have grown too large for this kind of very informal demos.</p>

<p>In any case, I had already seen most of the demos at earlier visits to various research centers so I did not miss all that much. For me the most interesting demo might have been the least advanced of them all: David Durand showing the <strong> FRESS </strong> system (File Retrieval and Editing System) developed at Brown University in 1971 as a successor to the original Hypertext Editing System (the world's first hypertext system). The demo ran on a Macintosh simulating the old vector graphics scope used in 1971. The Mac was connected by a modem to the IBM 370 mainframe at Brown University which was still able to run the original old code, thus showing the advantage of having IBM maintain the same architecture all these years.</p>

<p>There was some talk about getting the actual hypertext documents off the system before FRESS finally died. For example they had an interesting set of annotated poems originally used to teach poetry at Brown University in a very early field test of hypertext. It would probably not be all that hard to convert them to another hypertext format and this should certainly be done for the historical interest. It is amazing how little respect we have for the history of technology and how few resources are devoted to saving early technology for future generations. Consider how we condemn those Romans who tore down classical monuments during the Middle Ages in order to build new palaces from the marble. We are in effect doing the same when we use our resources exclusively for the production of new software and not on saving the most important artifacts and software in the history of technology.</p>

<p>The demos were mostly oriented towards systems and features instead of content. The Perseus Project did show an interesting set of documents oriented towards the study of classical Greek culture but otherwise there were very few really well prepared materials shown at the demos. It would have been interesting to see more actual sets of documents or data in hypertext available for people to buy. Mark Bernstein from <a href="http://www.eastgate.com/"> Eastgate Systems</a> did circulate a call for hypertexts to be published by their new publications program, the <strong> Eastgate Press</strong>. They plan to publish about four new titles per year of fiction as well as non-fiction. Bernstein showed some interesting initial works developed in <cite> Hypergate: The Election of 1912 </cite> and <cite> The Flag Was Still There </cite> (about the period 1808-1815). In yet another example of the integration trend, these hypertexts integrated the hypertext documents with simulations of the historical events discussed in the text.</p>

<p>At one of the panel sessions, Ben Shneiderman said that he too was surprised by not seeing more ambitious projects and people doing larger hypertexts. He would like to have some beautiful hypertext documents to show people what we can do. Shneiderman recalled Engelbart's and van Dam's stories at the Hypertext'87 conference about the lack of acceptance of their pioneering hypertext work in the 1960s but he had thought that we were above that now. The transfer of technology from the laboratory to everyday use often takes much longer than one would expect.</p>

<p>Another observation is that <strong> flashy multimedia systems were conspicuously absent </strong> from the conference which was dominated by more pragmatic, text oriented systems and applications, (not just at the demos but even more so at the paper sessions). This does not mean that the conference was dominated by command-line user interfaces. On the contrary, almost everybody used graphical user interfaces and many had features like graphical overview diagrams. There were just very few examples of the use of video and sound in hypertext systems.</p>

<p>With respect to <strong> hardware platforms</strong>, this conference was not nearly as dominated by the Macintosh as the European Hypertext'2 conference had been a few months earlier. There were certainly some Mac systems at the conference (e.g. Intermedia and the Perseus Project) but they did not nearly constitute a majority. The reason for this difference is definitely not that Apple might have had a larger market share in Europe than in the United States (the opposite is true). It is more likely to be due to the smaller scale of funding in Europe: The entry costs for getting into hypertext are small on the Macintosh since everybody has one already and can develop systems in HyperCard on their own without too much programming effort. Many US research and development projects have rejected that approach in favor of getting larger workstations and using larger groups of programmers to develop their own software.</p>

<h2>Confessions of the Hypertext Designers</h2>

<p>An interesting panel session had representatives of many of the main hypertext systems describe the shortcomings of their designs. <strong> Norman Meyrowitz </strong> started by describing <strong> Intermedia as a monolithic system</strong>. It only works with applications written within the system, making it impossible to integrate outside applications. He liked their web concept, but sometimes wanted to compare or superimpose two or more webs at the same time (Intermedia's webs are basically files of links for a given set of hypertext nodes and different users will frequently have different webs over the same nodes). This was impossible because of Intermedia's basic assumption of having a single active web of links. Furthermore, there are no filtering mechanisms for the links in a web and the single link- <strong> icon </strong> makes it impossible to see what will happen when the link is activated or what the destination will be like. A final problem was the lack of a print linearization method, making it impossible to print out a hypertext.</p>

<p><strong>Greg Kimberly </strong> from Apple represented <strong> HyperCard </strong> on the panel. His first comment was that HyperCard is not hypertext even though there is some overlap between the two. The inspiration for HyperCard was Bill Atkinson's old rolodex program. He was very fond of BitBlt and found that he could throw graphics up quickly. Furthermore, Dan Winkler added the idea of programmability in the form of the <strong> HyperTalk </strong> language (partly inspired by Lisp).</p>

<p>They had originally considered the marketing slogan "HyperCard-You Figure it Out!" and people have indeed done many different things with HyperCard. Kimberly claimed that the <strong> missing </strong> features were a deliberate feature of HyperCard. For example, it goes to great lengths to avoid abstractions and does not have abstract links. This feature makes it difficult to do things like overview maps. They also avoided things which would not fit on the Mac Plus computer such as object oriented graphics and color.</p>

<p>Challenges for future versions of HyperCard included factoring documents into cards and coming up with methods to take a document form it into cards. The Electronic Whole Earth Catalog was a good example of a match between the document and cards because of the brief nature of most of its elements. Furthermore, there was a need for version control as well as for managing automatic links and getting some kind of high-level control.</p>

<p><strong>Amy Pearl </strong> from Sun mentioned that the goal of the <strong> Sun Link Service </strong> was to support consistent connections. They built an open architecture where the links are managed by the link server while the node content is managed by the individual applications. Another goal was putting minimum constraints on the user interface of the various tools. The service is not general enough, however, and lacks support for inclusion links (automatically updated hot links). It also lacks link types, node and link attributes, and a browser. A more marketing-related problem is that the Sun Link Service depends on the application to provide any functionality. The Link Service by itself cannot be used for anything. <span class="updatecomment"> (Note added 1997: Amy Pearl is now the HotJava project lead.) </span></p>

<p><strong>Don McCracken </strong> from Knowledge Systems classified the problems of <strong> KMS </strong> in two categories: Some problems were wrongs of omission and could in principle be corrected in future version. This category included weak searching and indexing, lack of bidirectional links and embedded links, and the external links not being hot enough. McCracken felt that even the latest crop of workstations was still too slow for some of the things one would want to do in a hypertext system.</p>

<p>Other problems were wrongs of commission and were done "wrong" on purpose. These problems are therefore much harder to correct since they involve design tradeoffs. Even though they may seem wrong in isolation, McCracken felt that they might still be right from a more global design perspective. This category included KMS having its own idiosyncratic user interface with a strange big cursor. But because the interface is very tightly coupled with the data model of KMS, they cannot just rip it out and replace it with a standard user interface. Another wrong of commission is the lack of a simplified system for readers. They do not see any reason to design a limited annotation facility for readers since the best way to annotate is to have the full KMS features available. The only exception might be for a museum application or in similar environments.</p>

<p>On perhaps a slightly more commercial note, McCracken noted that the KMS user interface seemed so transparent that people do not understand why they need 100,000 limes of code to support it. In that regard, KMS may be "too simple for its own good" (or at least for the vendor's ability to charge a large sum of money for it).</p>

<p>Using McCracken's classification, <strong> Frank Halasz </strong> from Xerox PARC referred to his paper on <strong> NoteCards </strong> from the Hypertext'87 conference as being about the "wrongs of omission." He had discussed how to expand NoteCards to handle the kind of tasks people were bringing to NoteCards. Now he wanted to want to talk about things he did wrong because he did not know the right way to do it then. After having looked at other systems and the use of NoteCards over the years, he now believed that he knew how to do it.</p>

<p>NoteCards was built almost overnight because Interlisp was so powerful. The power of Interlisp also allowed people to extend NoteCards easily. But having NoteCards run under Interlisp also meant that it never got out to other people because so few had Interlisp available. Also, they never had to think about the open system issues because Interlisp was a large monolithic environment. The lack of openness have resulted in having people implement new applications which have been hard to integrate in NoteCards. So Interlisp encouraged bad design at the same time as it allowed them to get it done at all. Halasz would have preferred having used a general database to support Notecards since they could have saved hours of time if only they had used somebody else's database for storage, compression, etc.</p>

<p>Talking about databases, Halasz felt that the hypertext community ought to borrow the concept of <strong> schemas </strong> from the database community for talking about the conceptual structure of information compared to the implementation structure. In NoteCards, every node (card) has a type, but there are really two intermixed kinds of types: implementation types (text, graphics, video, etc.) and user oriented representation types such as position card, issue card, etc. These two type hierarchies should be orthogonal and that had actually been supported in the original design of NoteCards. Unfortunately, the concept of two different type hierarchies was taken out because they felt that it would be too difficult. Instead it has turned out that users have had to build incredibly complicated type hierarchies to get both kinds of types right.</p>

<p><strong>Links </strong> are second-class citizens in NoteCards. Only the cards have a full object-oriented type hierachy with inheritance while the typing of the links is restricted to plain named labels without any behavior. This is an asymmetric model and Halasz now felt that links are as important as nodes. It was also a mistake to implement links as embedded objects instead of the persistent selections (anchors) in Intermedia. Halasz complained that the same mistake has recenely been repeated in the CMU Andrew system.</p>

<p>The NoteCards programmer's language was an add-on. Halasz preferred the design of the new Emacs editor where a basic extension language is implemented by a small kernel and the rest of the system is implemented in that language. A major difficulty had been to modify the design of NoteCards from the original single-user system to the collaborative system wanted by many users.</p>

<p>Halasz finally mentioned the lack of search and query facilities in NoteCards as well as the need for filtering mechanisms. In a new design, he felt that some form of <strong> indexing </strong> should be a first class concept.</p>

<p><strong>Ben Shneiderman </strong> emphasized the different goals and applications for the various hypertext systems. His <strong> Hyperties </strong> system was developed for use in museums. In that application, there will be many more readers than authors, leading them to have a separate authoring interface. A problem with this design was the need for author-like facilities like annotations and the ability to add new links also in the browsing interface.</p>

<p>A lot of Hyperties limitations are due to the IBM PC world, such as being stuck with the text orientation and having a small screen. On the other hand, they have aimed for wide dissemination, and that is an advantage of IBM platform. For example, they have already sold 4,000 copies of their Hypertext Hands-On! book with its Hyperties diskettes. Also, ACM has sold 1,800 copies of the Hyperties version of Hypertext on Hypertext.</p>

<p><strong>Multiple authors </strong> are a problem in projects like a New York museum with twelve authors. They need better facilities for indexing large number of articles automatically and for importing existing text automatically. These <strong> conversion </strong> features are not yet part of the commercial Hyperties system even though they have been demonstrated as research projects. There are also problems with exporting information from the hypertext to a traditional text file (e.g. for printing). One can easily export a single article, but methods for exporting an entire web are missing. For the Hypertext Hands-On! book the text and hypertext versions were produced in two processes and the authors had to add all the cross-references to the paper-version by hand instead of having the system do so automatically.</p>

<p>A person from the audience asked why most systems still use WYSIWYG and emulate paper, since we are supposed to be moving away from paper. Halasz' answer was that it takes time for new forms of expression to develop, but he referred to two papers at the conference: People from the design firm Fitch RichardsonSmith showed some new typographic design ideas for hypertext cues. And Cathy Marshall from Xerox PARC showed interfaces with big arrows dynamically pointing at interesting information and looking different from traditional documents.</p>

<p>Shneiderman said that he would like to know how to make hypertext less paper-like, but that they are only now finding out what to do with the first deviation of using colored words which do something when you touch them. Kimberly's answer was having things move on the screen or having simulations etc. He emphasized that we need to give people something more than paper considering that all studies show that reading speed is 30% slower from screens.</p>

<h2>Domain-Specific Applications with Hypertext</h2>

<p>I would probably have given my award for best presentation to Frank Halasz if his analysis of the conceptual shortcomings of NoteCards had been given as a full paper instead of being crammed into an 8 minute panel statement. Norman Meyrowitz is another candidate for best presentation, but of course invited speakers don't get these awards, even if they are only Nielsen's ratings.</p>

<p>So I name <strong> Laura De Young </strong> best Hypertext'89 presenter for her talk on applications of hypertext in the <strong> auditing </strong> domain. She discussed current work at the Price Waterhouse Technology Centre aimed at building a specialized hypertext system to support auditors. De Young was able to describe this perhaps inherently somewhat boring domain with such zest that I got all fired up on having discovered a new application for hypertext.</p>

<p>The auditor's job is to look at a business carefully enough to determine whether the company has the same financial status as they claim they have. This is done by having a team of 5-8 people go to a client and gather evidence by going over the client's accounts. As the auditors go through these auditing procedures, they write down every piece of evidence and make sure to have references back to the origin of the evidence. A lot of documents are gathered and created and they include lots of references. De Young said that these "Audit Working Papers" are the best example she has ever seen of a manually constructed hypertext structure and she showed some interesting slides of heavily annotated and cross-referenced sample pages. It is so critical that these references are right that the people creating them sometimes need to take personal responsibility for them by initialing each reference.</p>

<p>De Young emphasized that her domain is real and has a real potential payoff: Studies have shown that approximately thirty percent of the time spent on an audit is dedicated to producing, relating, and reviewing the "Audit Working Papers." This does not even take the time spent obtaining the information into account. Therefore a good hypertext system for auditing support has the potential to reduce the time needed to conduct an effective audit. And therefore they also get real support from the company for their research.</p>

<p>On the other hand, they would also need to handle large sets of data. For example an audit of Shell Oil Australia had generated about 150 kg of paper (300 pounds) taking up six file cabinets (and that is not even the largest subsidiary of Shell Oil). So the hypertext techniques need to <strong> scale </strong> from the prototype to any potential production level system.</p>

<p>The Price Waterhouse prototype was not implemented in one of the standard hypertext systems but was programmed from the ground up in Common Lisp. They were using typed link and automatic link generation as well as the creation of paths to make it possible to follow a certain concept throughout the audit.</p>

<p>Special "review notes" can be attached to any of the other documents to point out potential problems in the client's papers. These notes were not just traditional annotation links but really drive the audit process. They can do a computation on the basis of the hypertext links to get an overview of how many review notes remain to be resolved, thus helping manage the auditing process.</p>

<p>Of course a complete auditing hypertext system should be integrated with documents created outside the system. They are currently able to open any document created by other applications like a spreadsheet or word processor and they can also scan in hard copy documents. But their hypertext access is limited to linking to entire documents and they do need more detailed access. One solution may be to use overlays and link to specific locations on the page.</p>

<p>A completely different application domain was discussed by <strong> John Schnase </strong> from Texas A&amp;M University. Schnase is a biologist in addition to being a computer scientist and had written a <strong> biological modelling </strong> paper in KMS taking advantage of its computational hypertext features.</p>

<p>The biology research addressed how an animal spends its time and energy. Schnase wanted to develop energy budgets for Cassin's Sparrow and to build computer simulations of that organism. The research combined field studies of the bird's activities and theoretical work on the computer.</p>

<p>The complete hypertext had about 1500-2000 KMS frames and contained a data subnet with the actual data collected in the field, an article subnet with Schnase's paper, a program subnet with the energy simulation, and a communication subnet with the necessary KMS action language statements to allows printing and sharing of information.</p>

<p>Schnase did his writing on the left half of the Sun screen since KMS allows you to print out that half while keeping the right half for hypertext links and related material which should not be a part of a printout. So his screen design was partly determined by his need to print out the final paper for publication in a traditional journal.</p>

<p>For the programs implementing the energy simulation, the first approach was to use the built-in KMS action language programs. This language has special instructions for actions on abstractions in the hypertext (e.g. a command for "create a new hypertext node"). These action language programs were too slow and not general enough so another approach was necessary. The second approach was C programs to act on KMS's low-level encoding of the nodes as data in Unix files.</p>

<p>The third approach turned out to be the most appropriate and involved a combination of the two first approaches. First the KMS action language was used to export the information needed for a particular computation to a generic ASCII file and then a C program was used for the actual simulations. Then the action language was used again to reimport the results into the hypertext. It was cumbersome to access the operating system, but it was important to be able to do so.</p>

<p>Schnase concluded that hypertext provides an integrated personal environment for scientific knowledge work. It also supports community scholarship among scientists by its distributed nature over local networks (important while research project is under way) as well as by email and the possibility to send an entire research setup to a colleague, having the hypertext and associated data and programs form a "virtual laboratory". This can substantially change the nature of dialogues in science: You don't just share results but the entire <strong> context </strong> in which the results were developed.</p>

<p>The third applications paper was on the use of hypertext in a <strong> law </strong> office. This example did not excite me quite as much as the other two, probably because I have been thinking of the legal domain as a natural application of hypertext for a long time. Law books are full of annotations and cross references. <strong> Bob Glushko </strong> took advantage of his good connections in the legal community and gave an example in his conference tutorial of a page from a law journal with footnotes taking up two thirds of the page and having references to other footnotes.</p>

<p>But even though the theoretical opportunities for hypertext in the legal domain are quite familiar, it is of course still interesting to learn about a practical implementation in a real law office office. <strong> Elise Yoder </strong> from Knowledge Workshop presented an example of the use of the KMS-based HyperLex system at the law firm of Reed Smith Shaw &amp; McClay in Pittsburgh to support intellectual property and <strong> patent </strong> law.</p>

<p>The HyperLex hypertext structure contains a general CSCW-like subnet with a group bulletin board, calendars, etc. and an office automation subnet with nodes for each client and links to correspondence and legal documents related to that client. It also contains nodes for the actual legal documents like patents and lawsuits. The patent nodes again have links to nodes representing the filing history, the prior art (earlier patents in the same area), etc.</p>

<p>One important use of the hypertext system is to search for the relevant prior art nodes in the database to see whether there is something in earlier patents that would conflict with a new potential patent.</p>

<p>They also build up hypertext networks for the litigation documents in trials. This application really needs a remote access capability through the phone system for attorneys in other cities or away from the office.</p>

<p>Yoder emphasized that they are trading off power for generality. It is very important in the law office to be able to hide information to give the attorneys the ability to take in info at a glance. They then also sometimes need to go into more depth with certain issues, and this can be offered through hypertext links to supporting material.</p>

<p>A person from the audience noted that the most important work during certain litigation was the building of the database of information. For example in the CDC antitrust case against IBM, the CDC people had built a very well organized database and there is a rumor that part of the settlement agreement was that CDC should destroy their database after having given IBM a copy. This implied that one would have to consider the possibility for a requirement to make hypertext links public and/or to hand them over to others. Traditionally one could at most risk having to make the documents themselves available and a large enough mass (and mess) of un-linked documents would discourage most opponents from digging deeply enough to find embarrassing material.</p>

<h2>Information Retrieval</h2>

<p>The conference had several sessions on information retrieval and showed a major trend towards integrating these more sophisticated searching capabilities with the basic hypertext framework. <strong> Michael Lesk </strong> from Bellcore discussed methods for query in huge hypertext networks, taking his examples from a book catalog containing 800,000 items. In his experience, people tend to type queries with single words only, and we should get them to type entire phrases to get more material to work with in the search. He was also working at giving users a graphical display to show them the classification of their query results.</p>

<p>For example, Lesk utilized the classification of the books in the two major library classification systems, Dewey and Library of Congress. In theory there should be a one-to-one between these two classifications but in reality there is not-either because of differences in the two taxonomies of world knowledge or simply because two different librarians have served as catalogers in the two systems. This makes it possible to show users a two-dimensional overview diagram of the result of their query by using each of the classification systems for one of the axes and showing either dots or book names in the diagram for each hit.</p>

<p>While Lesk's queries were based on the entry of keywords using overview diagrams for output, the <strong> GraphLog </strong> approach of <strong> Mariano Consens </strong> and <strong> Alberto Mendelzon </strong> from the University of Toronto was to get users to specify queries in terms of network structures.</p>

<p>One of their examples was the desire to find circular arguments in a NoteCards hypertext. This would involve finding a path that by following only support-links (links claiming that a given argument is supported by another argument) gets you back to the original node. This is a structure query where the user is looking for a structure in the hypertext graph and cannot be done with purely content-based search.</p>

<p>Consens and Mendelzon's solution was to design the GraphLog query language allowing users to specify query graphs as graph patterns consisting of a regular expression of links.</p>

<p>Another example another example was a network operation to link each notecard to its most reliable supporting argument according to confidence ratings supplied by the author. These virtual links can be either frozen snapshots or views recomputed whenever the author changes the hypertext.</p>

<p>These examples led me to consider the possibility for defining a set of standard GraphLog queries for debugging a hypertext or for heuristic interface evaluation pointing out nodes which would bear further usability evaluation.</p>

<p><strong>Mark Frisse </strong> from the Washington University School of Medicine gave an update on the <strong> dynamic medical handbook </strong> project previously presented at Hypertext'87. They now have several different versions implemented in HyperCard, the NeXT Digital Librarian and their own Lisp prototypes. They are also considering the potentials for a true <strong> medical </strong> workstation with the ability to display X-ray pictures etc.</p>

<p>In Frisse's 1987 paper, the main hypothesis was that propagating query scores along the hypertext links would increase the user's sense of context. Unfortunately they had found that their medical users could not work with traditional complicated query languages. Therefore he was now looking at having an index space structured to allow <strong> automatic inference </strong> to help users in their navigation.</p>

<p>Frisse did not like traditional flat indexes with words ordered alphabetically. This type of index simply leads to a flat information space just like having a set of unrelated papers on your desktop. Instead he was looking at a hierarchically ordered set of index terms from the National Library of Medicine. In true hypertext works, the index would of course be a network and one would have a computational engine operating on the index spaces.</p>

<p>They currently have a complete hypertext book where the chapters are indexed by hand. The next step will be to take the Unified Medical Language Thesaurus (a 30,000 node network of words) and use it for the inference methods.</p>

<h2>Interchange and Standards</h2>

<p>A major session on interchange standards for hypertext was started by <strong> Frank Halasz </strong> with a plea for the use of model-based interchange. He believed that the interchange of hypertext requires a common language for describing hypertext content and structure. And such a language must be based on a model of what hypertext is. Interchange standards will be more successful if they are explicitly derived from a model. Halasz warned against the trend in many other current projects to start from the standard and trying to add a the model later.</p>

<p>One attempt to articulate the important abstractions in current hypertext systems was the <strong> Dexter hypertext reference model </strong> developed by the "Dexter Group" of thirteen prominent hypertext designers. The group is named after the inn in New Hampshire where they had the first meeting.</p>

<p>The Dexter model has a layered look and includes a runtime layer (presentation of the hypertext), a hypertext layer, and a component layer (the content inside the nodes). The hypertext interchange standard focus on the hypertext layer's definition of the node and link structures unique for hypertext while the component layer it is the responsibility of other standards like ODA etc. With respect to the user interfaces of the runtime layer, Halasz felt that there was no hope for a standard at all. Later in the session, Rob Akscyn gave an analogy with the <strong> telephone system</strong>. It has a good interchange standard allowing you to call any other telephone in the world but that has not removed the cultural and language differences between the people of the world.</p>

<p>The hardest work in defining the model came at the interfaces between the main layers. For example <strong> anchoring </strong> is really an interface between the hypertext network structure (two nodes are connected) and the internals of the atomic components (the link is anchored in a specific sub-part of the node's content). Their solution was to use indirect links from a kind of table in the node to the place in the node you link to. In this way you can have anchored links without having the hypertext layer knowing about the internal addressing within the components. This does mean that the components need to contain not only the node contents but also all anchors within it in the table.</p>

<p><strong>Jeremy Bornstein </strong> from Apple demoed the preliminary result of a translation of a NoteCards document to HyperCard using the <strong> Dexter Hypertext Interchange Format, DHIF</strong>. The resulting HyperCard stack had a decidedly unimpressive user interface compared to both the standard NoteCards interface and to some of the better HyperCard stacks around. This could be because Bornstein concentrated on getting the actual hypertext transfer to work and not on the user interface of the result. It was certainly impressive to get a hypertext interchange to work. But it could also be because hypertext documents need to be written for their specific presentation environment to result in a good reading user interface. It remains to be seen whether interchange systems can automatically construct pleasing hypertext documents in the destination format or whether a human designer is needed to clean up the result.</p>

<p>One very fundamental distinction between hypertext systems is the card-based versus the scrolling windows. This was a severe problem in converting between HyperCard and NoteCards. Also the HyperCard scripts were difficult to translate-currently they just loose the script properties of HyperCard and translate them to dumb links.</p>

<p><strong>Victor Riley </strong> from Brown University said that Intermedia plans to follow the Dexter model soon. They are also looking at other standards under development such as the <strong> Apple Hypertext Interchange Format</strong>, HIP and the CGA sponsored standard, X3V1.8M.</p>

<p>Riley complained that most of these standards have no consistent understanding of the basic hypertext objects. Objects are not unique across systems, leading to problems. For example, if you copy an object from one system to another, change it, and copy it back: Is it still the same object?</p>

<p><strong>Tim Oren</strong>, from Apple mentioned a project called <strong> MIFF </strong> to define a non-proprietary format for hypertext interchange and storage for the Macintosh. It will go down to the byte level for file interchange, and it is being done in collaboration with Apple's developers.</p>

<p>MIFF is neutral to policy choices of e.g. linking methods and how entities are arranged among files (e.g. one huge file versus many small). They want to maintain as many <strong> neutralities </strong> as possible. When people in the hypertext field can still argue about some of the issues after so many years, Oren felt that Apple had better keep neutrality with respect to these issues and accommodate both views.</p>

<p>Apple was supporting two efforts at the same time because they do see the need for a public standard for e.g. the large customers like the government where a multi-vendor environment is required. Public standards are important for the long term health of the hypertext industry. But they also need a Mac-only standard very soon since their users are further along the learning curve: They have been using HyperCard for two years now and are discovering its limits.</p>

<p><strong>Rob Akscyn</strong>, from Knowledge Systems (KMS) felt that it was too early to actually standardize, but that we should start working on the conceptual differences. It will take a lot of time to end up with a good enough model to form the basis for a standard.</p>

<p>Akscyn's company is very interested in dynamic interaction between different systems in the form of links to documents stored in other systems. They take an incremental approach and start with the ability to read in a single node on demand from another system. They would also like to have write-access to that other system, but he felt that it was a research topic to come up with a method for doing so.</p>

<p>Akscyn mentioned several practical fundamental difficulties in hypertext interchange. With respect to node types and sizes it may be easy to go from HyperCard to KMS but hard to go the other way since the KMS cards (based on the Sun screen) are so much larger than the HyperCard cards. Also the computational hypertext aspects will be hard to interchange. For example, the HyperCard script language is object oriented while the KMS action language is a traditional Algol-like block-oriented language. Translating between two such languages is a Ph.D.-type project.</p>

<p><strong>Lawrence Welch </strong> from the National Institute for Standards and Technology, NIST presented a long list of relevant standards with respect to node content. Also, they were sponsoring the first "official" workshop on hypertext standards in January 1990. It seems that the responsibility for hypertext standards has now shifted from a self-established group of pioneers to the official bureaucracy.</p>

<h2>Lessons from the <cite> Communications of the ACM </cite> Project</h2>

<p>The Communications of the ACM had a special issue on hypertext in July 1988. The text of this issue was later converted to hypertext form in many different hypertext systems in a project called <cite> Hypertext on Hypertext </cite> . A panel at the conference gathered the designers of the various versions for a perspective on the conversion project. This has been one of the few successful hypertext publishing ventures so far. As of now, there have been 2500 copies sold and almost everybody raised a hand when <strong> Bernard Rous </strong> from the ACM asked the audience how many had seen at least one version of the Hypertext on Hypertext.</p>

<p>The <strong> KMS </strong> version was presented by <strong> Elise Yoder</strong>. It had 520 nodes structured as far as possible according to the original hierarchical structure of the articles in sections and subsections. One of their chief editorial contributions was an index of topics. They also built a combined bibliography. The whole process turned out to be much more time consuming that they thought it would be. They did not have any automatic facilities for anything, not even for creating the more routine links. The effort was one person in six weeks and the most time consuming activity was adding "value-added" links between articles.</p>

<p><strong>Nicole Yankelovich </strong> presented the <strong> HyperCard </strong> version done at Brown University's IRIS institute. They normally work in their own Intermedia system so they undertook this project to learn about HyperCard. Therefore they had to learn the tool at the same time as they were developing the hypertext. The overall structure had 11 interconnected stacks with an average of 40 cards each. The total number of physical nodes was 424 cards but they only considered the 11 stacks as their conceptual nodes.</p>

<p>The <strong> Hyperties </strong> version presented by <strong> Ben Shneiderman </strong> from the University of Maryland had 307 nodes. He added more information to the package than just the original articles such as e.g. the full IRIS hypertext bibliography and those sections of my trip report from Hypertext'87 dealing with those talks later rewritten for the Communications.</p>

<p>They had chosen to use CGA as their graphics platform to ensure the universality of the Hyperties version. But in retrospect he regretted this because the graphics had so low quality in some cases that they had to add a disclaimer saying that the figure was better in the original printed version. This was in spite of their having to redraw all figures to the CGA medium.</p>

<p>The total development effort for the Hyperties version was two people half time for 6 weeks.</p>

<p>I asked the panel members what they would have done differently if they had had more time. Yankelovich would have added more additional meaningful information in addition to the original set of articles. Also, she would have had a HyperCard programmer get things to work in the way she really wanted, such as building a better navigation aid. Shneiderman would have added better content-oriented links, and Yoder would have liked an extra round with the original authors to get feedback on the conversion since they got a lot out of the single round they had.</p>

<p>The papers from the CACM hypertext special issue may not have been the ideal set for a hypertext: It was a fairly small set and the papers had been selected for the journal exactly because of their diversity, so there may not have been so many interesting links between them. This led Bob Glushko to ask how a hypertext version of the full proceedings of the Hypertext'89 conference would be different. Nicole Yankelovich's answer was that it would not be that different but since the scale is much bigger, there might be more <strong> value added</strong>. Indeed some preparations for a hypertext proceedings has already been made: The authors submitted their papers in machine-readable form and the audience was asked to provide lists of potential links between the papers.</p>

<h2>Missing Aspects of Hypertext</h2>

<p>At the closing panel at the Hypertext'87 conference, <strong> Frank Halasz </strong> listed five domains/applications which he had not seen at that conference but would like to see in the future:</p>

<ul>
	<li>Libraries</li>
	<li>Publishers</li>
	<li>Lawyers</li>
	<li>Lower education</li>
	<li>Industrial training</li>
</ul>

<p>Now, two years further down the path we did see a paper on the use of hypertext for lawyers but the other four groups were mostly missing. There were several papers on information retrieval but they were mostly focused on how to use methods developed for searching huge bibliographic databases in searching hypertext structures. Nobody really talked about how to use hypertext in regular libraries (but some work is being done such as e.g. the Danish Book House project).</p>

<p>Publishers are still extremely conservative with respect to publishing hypertexts. Shneiderman said that it had been a struggle to get his Hypertext Hands-On! book into bookstores because of the disks in the back. Bookstores are nervous about the disks since it has usually been the responsibility of the store if books were damaged. Addison-Wesley has recently changed their policy and will accept damaged disks back and replace them instead of putting the responsibility on the bookstore.</p>

<p><strong>Greg Kimberly </strong> also complained that is it is difficult to get hypertexts out. Software dealers do not want to deal with $30 items and bookshops do not know what electronic books are. Therefore he felt that many of the more interesting hypertexts to come out in the near future will be at universities because they do have the distribution channels. The <strong> lack of hypertext publishing </strong> certainly seemed to be the second theme at this conference (in addition to the more positive theme of integrating hypertext with the rest of the world mentioned above). Maybe the problem will solve itself as hypertext systems do become more integrated (and thus more widely used) and as interchange standards appear (making it cheaper to produce hypertext documents for larger markets).</p>

<p>Finally, with respect to the two <strong> non-university types of learning</strong>, there were not very many examples at the conference. But this is probably more related to the type of people who attend scientific conferences than the true status of the field. For example, <strong> Harry McMahon </strong> and <strong> Bill O'Neill </strong> from the University of Ulster showed a very nice example of having pupils in elementary school construct interactive fiction at the U.K. Hypertext'2 conference, and <strong> Per-Olof Nerbrant </strong> from Ericsson Telecom showed a system at the first Swedish Multimedia conference for communicating a customer-oriented service attitude by interlinking corporate policy statements and videos of customer experiences.</p>
</section>
  

  
  <section>
    
        
    
  </section>
  
  
  
  
  

  


  <section>
    <br />
    <p class="share"><b>Share <span class="hide-for-small-down">this article</span>:</b> <a href="https://twitter.com/intent/tweet?url=http://www.nngroup.com/articles/trip-report-hypertext-89/&amp;text=Hypertext'89%20Trip%20Report&amp;via=nngroup">Twitter</a> | <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=http://www.nngroup.com/articles/trip-report-hypertext-89/&amp;title=Hypertext'89%20Trip%20Report&amp;source=Nielsen%20Norman%20Group">LinkedIn</a> | <a href="https://plus.google.com/share?url=http://www.nngroup.com/articles/trip-report-hypertext-89/">Google+</a> | <a href="mailto:?subject=NN/g Article: Hypertext&#39;89 Trip Report&amp;body=http://www.nngroup.com/articles/trip-report-hypertext-89/">Email</a></p>
    <hr />
  </section>

  <footer class="l-subsection">
    
    
    <div class="learn-more">
      <h2>Learn More</h2>
      <p><a href="../subscribe/index.php">Subscribe to the weekly newsletter</a> to get notified about future articles.</p>
      </div>
    
    

    <div class="row show-for-small-down related-content">
      
      
      

      
      
        <div class="column small-12 topic-links">All articles about:
          <ul>
            
            <li><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction</a></li>
            
            <li><a href="../../topic/hypertext/index.php">hypertext</a></li>
            
          </ul>
        </div>
      
      

      
      
        <div class="column small-12">
          <h3>UX Conference Training Courses</h3>
           <ul class="no-bullet">
    
        <li><a href="../../courses/ux-basic-training/index.php">UX Basic Training</a></li>
    
        <li><a href="../../courses/hci/index.php">User Interface Principles Every Designer Must Know</a></li>
    
        <li><a href="../../courses/interaction-design-3-day-course/index.php">Interaction Design: 3-Day Course</a></li>
    
</ul>
        </div>
      
      

      
      
      

      
      
      
    </div>

    <div class="hide-for-small-down row related-content">
      <div class="medium-6 columns">
        
        
          <div>
            <h3>Articles</h3>
            <ul class="no-bullet">
              
                
                <li><a href="../embarrassment/index.php">Computer-Assisted Embarrassment</a></li>
                
              
                
                <li><a href="../enhancement/index.php">The Role of Enhancement in Web Design</a></li>
                
              
                
                <li><a href="../trip-report-chi-89/index.php">CHI&#39;89 Trip Report</a></li>
                
              
                
                <li><a href="../trip-report-hyper-hyper-89/index.php">HyperHyper&#39;89 Trip Report</a></li>
                
              
                
                <li><a href="../trip-report-hypertext-87/index.php">Hypertext&#39;87 Trip Report</a></li>
                
              
            </ul>
          </div>
        
        

        

      </div>
      <div class="medium-6 columns">
        
          
          <div>
            <h3>UX Conference Training Courses</h3>
             <ul class="no-bullet">
    
        <li><a href="../../courses/ux-basic-training/index.php">UX Basic Training</a></li>
    
        <li><a href="../../courses/hci/index.php">User Interface Principles Every Designer Must Know</a></li>
    
        <li><a href="../../courses/interaction-design-3-day-course/index.php">Interaction Design: 3-Day Course</a></li>
    
</ul>
          </div>
          
        
        
      </div>
    </div>
  </footer>
</article>

        </div>

      

    
  </div>
  <div class="row">
    
      <div class="column small-12 show-for-small-down l-navrail announcements">
        
          
        
      </div>
    
    
  </div>

    </div>
    




<footer id="page-footer">
    <div class="row hide-for-print">
        <div class="column small-12 medium-6">
            <section class="footer-popular">
                
                <h1>Popular Topics</h1>
                <div style="float: left;  width: 15em; display: block;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/applications/index.php">Application Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/e-commerce/index.php">E-Commerce Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/eyetracking/index.php">Eyetracking </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/information-architecture/index.php">Information Architecture </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/intranets/index.php">Intranet Design </a></li>
	<li><a href="../../topic/interaction-design/index.php">Interaction Design </a></li>
</ul>
</div>

<div style="float: left;  width: 15em; display: block; list-style: none;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/mobile-and-tablet-design/index.php">Mobile and Tablet Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/credibility-persuasion/index.php">Persuasive Design</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/research-methods/index.php">Research Methods</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/user-testing/index.php">User Testing </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/web-usability/index.php">Web Usability </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/writing-web/index.php">Web Writing &amp; Content Strategy </a></li>
</ul>
</div>

<p>&nbsp;</p>

                
            </section>
        </div>

        <div class="column small-12 medium-6">
            <div class="row">
                <div class="column small-6">
                    <section class="footer-about">
                        <h1>Follow Us</h1>
                        <ul>
	<li><a href="../subscribe/index.php">Subscribe to our weekly UX newsletter</a>,&nbsp;Jakob Nielsen&#39;s Alertbox</li>
	<li>Visit Don Norman&#39;s website, <a href="http://www.jnd.org/"> www.jnd.org </a></li>
	<li>Visit Bruce Tognazzini&#39;s website, <a href="http://www.asktog.com/"> www.asktog.com </a></li>
	<li style="padding-top: 6px;"><a class="twitter-follow-button" data-show-count="false" data-size="large" href="https://twitter.com/nngroup"><img alt="Follow @nngroup on Twitter" height="20" src="https://media.nngroup.com/media/editor/2016/12/30/twitter-follow-button.png" style="width: 117px; height: 20px;" width="117" /></a></li>
	<li style="padding-top: 6px;"><img alt="" height="14" src="https://media.nngroup.com/media/editor/2016/07/26/feed-icon-14x14.png" style="vertical-align:text-top;width:14px;margin-right:.25em;" width="14" /><a href="../../feed/rss/index.php">Subscribe to our RSS feed</a></li>
</ul>

                    </section>
                </div>

                <div class="column small-6">
                    <section class="footer-about right-column">
                        <h1>About
</h1>
                        <ul class="no-bullet">
	<li><a href="../../about/contact/index.php">Contact information</a></li>
	<li><a href="../../about/index.php">About Us</a></li>
	<li><a href="../../about/why-nng/index.php">Why NN/g? </a></li>
	<li><a href="../../return-policy/index.php">Return Policy</a></li>
</ul>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="copyright column small-12">
            <p><a href="../../copyright-and-reprint-info/index.php">Copyright</a> &copy; 1998-2017 Nielsen Norman Group, All Rights Reserved.</p>
        </div>
    </div>
</footer>


    


        <script src="https://media.nngroup.com/static/vendor/fastclick/js/fastclick.min.js"></script>
        <script src="https://media.nngroup.com/static/vendor/foundation/js/foundation.min.js"></script>
        <script src="http://media.nngroup.com/static/js/app.js"></script>
        <script src="http://media.nngroup.com/static/js/scroll-to-fixed.js"></script><script>var externalLinks = document.querySelectorAll('a[href^="http"]');for (var i = externalLinks.length-1; i >= 0; i--) {externalLinks[i].addEventListener("click", function(event) { alert("External links are disabled for this test."); event.preventDefault(); }, false);}var mailLinks = document.querySelectorAll('a[href^="mailto"]');for (var i = mailLinks.length-1; i >= 0; i--) {mailLinks[i].addEventListener("click", function(event) { alert("Email links are disabled for this test."); event.preventDefault(); }, false);}var externalForms = document.querySelectorAll('form');for (var i = externalForms.length-1; i >= 0; i--) {externalForms[i].addEventListener("submit", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false); externalForms[i].addEventListener("click", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false);}</script>

        <script>
            
            $(function() {
                $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
                $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
            });
        </script>

        
<script src="https://media.nngroup.com/static/js/articles.js"></script>


        
        <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1072699960;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072699960/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
    </body>

<!-- Mirrored from www.nngroup.com/articles/trip-report-hypertext-89/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:14:36 GMT -->
</html>
