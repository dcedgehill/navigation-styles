<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->





    
<!-- Mirrored from www.nngroup.com/articles/direct-manipulation-analysis/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:14:00 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1026.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"licenseKey":"1a30ef9931","errorBeacon":"bam.nr-data.net","applicationID":"2048992","transactionName":"NFxSMRBUX0EDUkIPDA0WdhAMVkVbDV8ZBxEXUFMJB0YfRAtUQRVZIktEDAFZVHYHRVcPD01eVRE=","beacon":"bam.nr-data.net","queueTime":0,"applicationTime":453,"agent":""}</script>
        <title>Layered Interaction Analysis of Direct Manipulation (Jakob Nielsen)</title><meta property="og:title" content="Layered Interaction Analysis of Direct Manipulation (Jakob Nielsen)" />
  
        
        <meta name="description" content="Direct manipulation analyzed according to a detailed layered interaction model, showing that it has quite different effects on the dialogue on the different levels. In particular, the &quot;no errors&quot; claim may be true at the syntax level but not at several of the levels above or below that level. ">
        <meta property="og:description" content="Direct manipulation analyzed according to a detailed layered interaction model, showing that it has quite different effects on the dialogue on the different levels. In particular, the &quot;no errors&quot; claim may be true at the syntax level but not at several of the levels above or below that level. " />
        
  
        
	
        

        

        <meta name="robots" content="noarchive">
        <meta name="viewport" content="width=device-width">

        <link rel="search" type="application/opensearchdescription+xml" title="NN/g Search" href="../../opensearch.xml" />
        <link rel="shortcut icon" href="http://media.nngroup.com/static/img/favicon.ico" />

        <link rel="stylesheet" href="https://media.nngroup.com/static/vendor/font-awesome/css/font-awesome.min.css">

        
        <link rel="stylesheet" href="https://media.nngroup.com/static/css/7232a960f1df.css" type="text/css" />
        <link rel="stylesheet" href="http://fast.fonts.com/t/1.css?apiType=css&amp;projectid=c5846260-9def-46ec-9407-0f1349bd2d64" type="text/css">

        <script src="https://media.nngroup.com/static/vendor/modernizr/js/modernizr.min.js"></script>

        <script type="text/javascript" src="https://media.nngroup.com/static/js/3283be5def74.js"></script>
        

        

        
        
        <!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=229034,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('http://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->


<!-- Google Analytics Tracking Code for http://nngroup.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23132681-1', 'nngroup.com');
  ga('require', 'linkid', 'https://www.nngroup.com/articles/direct-manipulation-analysis/linkid.js');
  ga('require', 'displayfeatures');

// Optimizely Universal Analytics Integration
 window.optimizely = window.optimizely || [];
 window.optimizely.push("activateUniversalAnalytics");

  ga('send', 'pageview');

</script>
<script>
/**
* Function to track a click on an outbound link in Google Analytics.
*/
function trackOutboundLink(label, url) {
    if(window.ga && ga.loaded) {
        ga('send', 'event', 'outbound', 'click', label, {'hitCallback': function(){document.location = url;}
        });
    } else {
        document.location = url;
    }
}
/**
* Function to track a click on a link in the article announcement area.
*/
$('#articleAnnouncement a').on('click', function(){
  ga('send', 'event', 'link', 'click', 'articleAnnouncement');
});
</script>

<script type="text/javascript">
 setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2421.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



<!-- Hotjar Tracking Code for http://nngroup.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:121605,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

        <script src="https://media.nngroup.com/static/js/analytics/track_outbound_link.js"></script>
        
        
        
    </head>
    <body class="location-articles articles-detail">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
    <header id="page-header">
        <div id="cart" class="shopping-cart mobile-cart show-for-small-down"></div>
        <div class="row">
            <div id="masthead" class="masthead small-12 medium-7 column">

                <!-- The button to toggles menu on small screen -->

                <h1><a href="../../index.php"><img src="http://media.nngroup.com/static/img/logo-wordmark.png" width="74" height="30"> Nielsen <span class="norman">Norman</span> Group</a></h1>
                <p class="tagline show-for-medium-up">Evidence-Based User Experience Research, Training, and Consulting</p>

                <!-- The button to toggles form search on small screen -->


                <div style="clear: right;width:  100%;height: 1px;"></div><?php define('ROOT_PATH',$_SERVER["DOCUMENT_ROOT"] . '/apps/navigation_styles/navigations/5'); ?><?php include(ROOT_PATH)."/navigation.php"; ?>

                <!-- Drop down menu on small screen -->
                <div id="hideNav" class="show-for-small-down">
                  
                  <ul class="menu-content">
                    <li><a href="../../index.php">Home</a></li>
                    <li class="has-submenu">
                      <a>Training</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../training/index.php">Overview</a></li>
                        
<li id="nav-ux-conference"><a href="../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../online-seminars/index.php"><span>Online Seminars</span></a></li>

                      </ul>
                    </li>
                    <li class="has-submenu">
                      <a>Consulting</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
  <li><a href="../../consulting/index.php">Overview</a></li>
  
  <li><a href="../../consulting/benchmarking/index.php">Benchmarking</a></li>
  
  <li><a href="../../consulting/expert-design-review/index.php">Expert Design Review</a></li>
  
  <li><a href="../../consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
  
  <li><a href="../../consulting/usability-testing/index.php">Usability Testing </a></li>
  
  <li><a href="../../consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
  
  <li><a href="../../in-house-training/index.php">In-House Training</a></li>
</ul>

                    </li>

                    <li><a href="../../reports/index.php">Reports</a></li>
                    <li><a href="../index.php">Articles</a></li>
                    <li class="has-submenu"><a>About NN/G</a><i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../about/index.php">Overview</a></li>
                        <li><a href="../../people/index.php">People</a></li>
                        <li><a href="../../about/why-nng/index.php">Why NN/g?</a></li>
                        <li><a href="../../about/contact/index.php">Contact</a></li>
                        <li><a href="../../news/index.php">News</a></li>
                        <li><a href="../../about/history/index.php">History</a></li>
                        <li><a href="../../books/index.php">Books</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                </div>

                <hr class="header-divider show-for-small-only"/>


            </div>
       </div>

        <div id="hideNav">            <nav class="primary-nav small-12 column">
                <ul>
                    <li><a class="navitem" id="nav-home" href="../../index.php">Home</a></li>
                    <li><a class="navitem" id="nav-training" href="../../training/index.php">Training</a></li>
                    <li><a class="navitem" id="nav-consulting" href="../../consulting/index.php">Consulting</a></li>
                    <li><a class="navitem" id="nav-reports" href="../../reports/index.php">Reports</a></li>
                    <li><a class="navitem" id="nav-articles" href="../index.php">Articles</a></li>
                    <li><a class="navitem" id="nav-about" href="../../about/index.php">About NN/G</a></li>
                </ul>
            </nav>
            <nav class="secondary-nav small-12 medium-12 large-12 columns">
                
                
            </nav>
        </div>
    </header>
    <div class="l-content"><span id="preventClose" style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776; Menu</span>
    
  

  

  <div class="row">
    
      
      <section class="small-12 medium-3 columns collapsable-content l-navrail articles-left-column">
        <h1 class="show-for-small-down collapse-link mobile-browse-header">Browse by Topic and Author</h1>
          <div class="row collapsable">

            
            <div class="small-6 medium-12 columns">
              <section id="article-popular-topics" style="display: none;">
                
                <h1>Topics</h1>
                <ul id="article-popular-topics">
                    
                    <li>
                        
                        <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/intranets/all/index.php">Intranets</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/user-testing/all/index.php">User Testing</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                        
                    </li>
                    
                    <li><a href="#" id="article-see-all-topics"><strong>See all topics&hellip;</strong></a></li>
                </ul>
                
              </section>
              <section id="article-all-topics" style="display: none;">
                
                <h1>
                  <span class="show-for-medium-up">All Article Topics</span>
                  <span class="show-for-small-down">All Topics</span>
                  (<a href="#" id="article-hide-all-topics">hide</a>)
                </h1>
                <ul>
                  
                  <li>
                    
                    <a href="../../topic/accessibility/all/index.php">Accessibility</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/agile/all/index.php">Agile</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/analytics-and-metrics/all/index.php">Analytics &amp; Metrics</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/applications/all/index.php">Application Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/b2b-websites/all/index.php">B2B Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/behavior-patterns/all/index.php">Behavior Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/branding/all/index.php">Branding</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/content-strategy/all/index.php">Content Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/corporate-websites/all/index.php">Corporate Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/design-patterns/all/index.php">Design Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-design-process/all/index.php">Design Process</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/email/all/index.php">Email</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/eyetracking/all/index.php">Eyetracking</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/heuristic-evaluation/all/index.php">Heuristic Evaluation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/human-computer-interaction/all/index.php">Human Computer Interaction</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-ideation/all/index.php">Ideation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/information-architecture/all/index.php">Information Architecture</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/interaction-design/all/index.php">Interaction Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/international-users/all/index.php">International Users</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/intranets/all/index.php">Intranets</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-management/all/index.php">Management</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/navigation/all/index.php">Navigation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/non-profit-websites/all/index.php">Non-Profit Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/personas/all/index.php">Personas</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/persuasive-design/all/index.php">Persuasive Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/prototyping/all/index.php">Prototyping</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/psychology-and-ux/all/index.php">Psychology and UX</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/research-methods/all/index.php">Research Methods</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/search/all/index.php">Search</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/social-media/all/index.php">Social Media</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/strategy/all/index.php">Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/user-testing/all/index.php">User Testing</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-teams/all/index.php">UX Teams</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/visual-design/all/index.php">Visual Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/writing-web/all/index.php">Writing for the Web</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/young-users/all/index.php">Young Users</a>
                    
                  </li>
                  
                </ul>
                
              </section>
            </div>
            <script>
              /* Determine which topics list will be displayed: the full or the
               * partial.  We are inlining this close to the list itself to
               * prevent rendering delays on the client
               */
              var topic_list = $.cookie("article-topics");

              var popular_topics = $('#article-popular-topics');
              var all_topics = $('#article-all-topics');

              if(topic_list == 1) {
                  popular_topics.hide();
                  all_topics.show();
              } else {
                  popular_topics.show();
                  all_topics.hide();
              }
            </script>
            

            
            <div class="small-6 medium-12 columns">
              
              <section id="article-initial-authors">
                  <h1>Author</h1>
                  <ul>
                    
                    <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                    
                    <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                    
                    <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                    
                    <li><a href="#" onClick="$('#article-initial-authors').hide(); $('#article-all-authors').show();"><strong>See all authors&hellip;</strong></a></li>
                  </ul>
              </section>
              <section id="article-all-authors" style="display: none;">
                <h1>
                  All Authors
                  (<a href="#" id="article-hide-all-authors">hide</a>)
                </h1>
                <ul>
                  
                  <li><a href="../author/raluca-budiu/index.php">Raluca Budiu</a></li>
                  
                  <li><a href="../author/patty-caya/index.php">Patty Caya</a></li>
                  
                  <li><a href="../author/susan-farrell/index.php">Susan Farrell</a></li>
                  
                  <li><a href="../author/therese-fessenden/index.php">Therese Fessenden</a></li>
                  
                  <li><a href="../author/kim-flaherty/index.php">Kim Flaherty</a></li>
                  
                  <li><a href="../author/sarah-gibbons/index.php">Sarah Gibbons</a></li>
                  
                  <li><a href="../author/aurora-harley/index.php">Aurora Harley</a></li>
                  
                  <li><a href="../author/kate-kaplan/index.php">Kate Kaplan</a></li>
                  
                  <li><a href="../author/page-laubheimer/index.php">Page Laubheimer</a></li>
                  
                  <li><a href="../author/angela-li/index.php">Angie Li</a></li>
                  
                  <li><a href="../author/hoa-loranger/index.php">Hoa Loranger</a></li>
                  
                  <li><a href="../author/kate-meyer/index.php">Kate Meyer</a></li>
                  
                  <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                  
                  <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                  
                  <li><a href="../author/kara-pernice/index.php">Kara Pernice</a></li>
                  
                  <li><a href="../author/christian-rohrer/index.php">Christian Rohrer</a></li>
                  
                  <li><a href="../author/amy-schade/index.php">Amy Schade</a></li>
                  
                  <li><a href="../author/katie-sherwin/index.php">Katie Sherwin</a></li>
                  
                  <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                  
                  <li><a href="../author/kathryn-whitenton/index.php">Kathryn Whitenton</a></li>
                  
                </ul>
              </section>
              
            </div>
            
          </div>

          
          <section class="hide-for-small-down">
            
              
              <h1>Recent Articles</h1>
              
              <ul>
                  
                  <li><a href="../seamless-cross-channel/index.php">Seamlessness in the Omnichannel User Experience</a></li>
                  
                  <li><a href="../brand-intention-interpretation/index.php">Experience Design: Bridging Brand Intention and Brand Interpretation</a></li>
                  
                  <li><a href="../eyetracking-tasks-efficient-scanning/index.php">Scanning Patterns on the Web Are Optimized for the Current Task</a></li>
                  
                  <li><a href="../flat-design-best-practices/index.php">Flat-Design Best Practices</a></li>
                  
                  <li><a href="../ux-user-stories/index.php">Accounting for UX Work with User Stories in Agile Projects</a></li>
                  
                  <li><a href="../index.php"><strong>See all articles&hellip;</strong></a></li>
              </ul>
              
            
          </section>
          

          
          
          <section class="hide-for-small-down">
            <h1>Popular Articles</h1>
            
            <ul>
                
                <li><a href="../usability-101-introduction-to-usability/index.php">Usability 101: Introduction to Usability</a></li>
                
                <li><a href="../top-10-mistakes-web-design/index.php">Top 10 Mistakes in Web Design</a></li>
                
                <li><a href="../how-users-read-on-the-web/index.php">How Users Read on the Web</a></li>
                
                <li><a href="../f-shaped-pattern-reading-web-content/index.php">F-Shaped Pattern For Reading Web Content</a></li>
                
                <li><a href="../ten-usability-heuristics/index.php">10 Usability Heuristics for User Interface Design</a></li>
                
                <li><a href="../intranet-design-2016/index.php">10 Best Intranets of 2016</a></li>
                
                <li><a href="../which-ux-research-methods/index.php">When to Use Which User-Experience Research Methods</a></li>
                
                <li><a href="../response-times-3-important-limits/index.php">Response Times: The 3 Important Limits</a></li>
                
                <li><a href="../why-you-only-need-to-test-with-5-users/index.php">Why You Only Need to Test with 5 Users</a></li>
                
                <li><a href="../page-fold-manifesto/index.php">The Fold Manifesto: Why the Page Fold Still Matters</a></li>
                
            </ul>
          </section>
          
          

          
            <section class="hide-for-small-down">
              
            </section>
          

      </section>
      

      
        <div class="small-12 medium-9 columns content-body">
          




<article class="l-subsection alertbox-article-content article-detail-bl">
  
  <header>
    <div class="l-subsection">
      <h1>A Layered Interaction Analysis of Direct Manipulation</h1>
      <div class="author-meta">by
        
          
            
            <a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a>
            
          
        on  January 1, 1992
        
      </div>

      

      
      <div class="topic-links">
        Topics:
        
          <ul>

  <li><a href="../../topic/applications/index.php">Application Design</a></li>

  <li><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction</a></li>

</ul>
        
      </div>
      
    </div>
  </header>
  

  
  
  <section>
    <p class="lede"><strong>Summary:</strong> The concept of direct manipulation is usually viewed as a single characteristic of a class of interaction styles. Here, direct manipulation is analyzed according to a detailed layered interaction model, showing that it has quite different effects on the dialogue on the different levels. </p>
    <br />
  </section>
  
  

  
  <section id="article-body"><h2>1. Introduction</h2>

<p><a href="../direct-manipulation/index.php">Direct manipulation</a> (Shneiderman 1982a) has become a popular concept in the user interface field for describing a certain class of interaction styles. Initially, the term was used to characterize entire applications in a strict classification scheme where a system was seen as either having a direct manipulation interface or belonging to some other category of interfaces like natural language, menu-driven, or form fill-in. This usage lead Wixon and Good (1987) to point out the apparent paradox of having several early "canonical" direct manipulation applications make heavy use of menus and form fill-in (dialog boxes). (See Tognazzini (1989) for a discussion of the role of canonical applications in establishing consistency in new user interfaces.) Hutchins et al. (1986) provided an analysis of the concept in terms of semantic and articulatory forms of directness with respect to both execution (input) and evaluation (output) of the dialogue. This analysis was a major step towards realizing that the degree of directness in an interface should be seen as lying on a continuum.</p>

<p>In this paper, direct manipulation and other notions of dialogue directness are analyzed according to a layered interaction model. This kind of model identifies a number of levels on which an interaction takes place, using the lower levels to support the communication on the upper levels. This again means that it becomes possible to talk more precisely about directness in terms of the levels of the interaction model.</p>

<p>The Nielsen virtual protocol model (Nielsen 1986) is used for the analyses in this paper, since it is specifically intended for detailed analysis of dialogue techniques. A short summary of this model is shown in Table 1. Several other layered interaction models exist and could be used with similar results. Nielsen (1986) shows a comparison between the levels in virtual protocol model and the levels in three other popular layered models, and Taylor (1988a; 1988b) presents a general discussion of layered models.</p>

<p> </p>

<table border="1">
	<caption valign="bottom"><strong>Table 1. </strong> A summary of the seven levels in the virtual protocol model (Nielsen 1986) with examples from a traditional text editor and an editor with a more modern graphical user interface.</caption>
	<tbody>
		<tr>
			<th>Level Number</th>
			<th>Name of Layer</th>
			<th>Exchanged unit of Information</th>
			<th>Text Example</th>
			<th>GUI Example</th>
		</tr>
		<tr>
			<td>7</td>
			<td>Goal</td>
			<td>Real world concepts, external to computer</td>
			<td colspan="2">Remove section of my letter</td>
		</tr>
		<tr>
			<td>6</td>
			<td>Task</td>
			<td>Computer-oriented objects/actions</td>
			<td colspan="2">Delete 6 lines of edited text</td>
		</tr>
		<tr>
			<td>5</td>
			<td>Semantics</td>
			<td>Concrete objects, specific operations</td>
			<td>Delete line no 27</td>
			<td>Delete selected lines</td>
		</tr>
		<tr>
			<td>4</td>
			<td>Syntax</td>
			<td>Sentences (1 or 2 dimensional sequences or layouts) of tokens</td>
			<td>DELETE 27</td>
			<td>Click to the left of the first line; while holding down the shift-key, click to the right of last line; select CUT in menu</td>
		</tr>
		<tr>
			<td>3</td>
			<td>Lexical</td>
			<td>Tokens: smallest info-carrying units</td>
			<td>DELETE</td>
			<td>Click at left of first line</td>
		</tr>
		<tr>
		</tr>
		<tr>
			<td>2</td>
			<td>Alphabetic</td>
			<td>Lexemes: primitive symbols</td>
			<td>D</td>
			<td>Click at (345,120)</td>
		</tr>
		<tr>
			<td>1</td>
			<td>Physical</td>
			<td>"Hard I/O", light, sound, movement</td>
			<td>Press D-key</td>
			<td>Press mouse button</td>
		</tr>
	</tbody>
</table>

<p>Most of the examples in this paper involve the Apple Macintosh Finder (operating system user interface). This is neither because the Finder has a badly designed user interface nor because it is the only possible example, but simply because it is so widely used that most readers should be able to try out the examples in a dynamic interaction if they want to. In some sense, the Macintosh Finder serves as the canonical example of direct manipulation interfaces in the same way as the standard Unix user interface often serves as the canonical example of command line based systems.</p>

<h2>2. "No Errors"</h2>

<p>It is frequently claimed that direct manipulation interfaces reduce the need for error messages and this property was one of the main advantages listed in Shneiderman's original article defining the term (Shneiderman 1982a).</p>

<p>As a matter of fact, errors frequently do occur when using direct manipulation systems but in many cases the error messages are pretty poor, maybe because the designers have believed the "no errors" claim. As an example, users of the Macintosh Finder file system interface very often get the error message "An Application could not be found for this Document." This error message succeeds in breaking almost all the standard principles for message design (Shneiderman 1982b): It uses computer-oriented terminology rather than user-oriented terminology, it is general rather than specific, and it is not constructive. So it is no surprise that I have fairly often observed users (including in one case a user with a full year's Macintosh experience) having severe problems with this error message. A more specific message would tell the user the type of the document in question and what application it is intended for. If the document was of a type that could be opened by several applications (typically plain ASCII text or a bitmapped image), the message could also be constructive and inform the user if another application was available which would allow users to work with the specified document.</p>

<p> </p>

<center>
<p><img alt="" src="http://media.nngroup.com/media/editor/2012/10/30/dirman_1.gif" style="width: 176px; height: 160px;"/></p>

<p><br/>
<strong>Figure 1. </strong> Correct operation for "discard document foo" on the Macintosh: The icon representing the document file "foo" is dragged from the window containing "foo" to the Trash icon.</p>
</center>

<p>For several other examples of errors in a direct manipulation dialogue, consider the case of deleting a file in the Macintosh Finder by dragging its icon to the Trash icon. The correct operation is shown in Figure 1. It is impossible to make syntax errors in this dialogue, since any movement of the icon is legal and has some meaning.</p>

<p> </p>

<table align="center" border="0">
	<tbody>
		<tr>
			<td><img alt="Dragging" src="http://media.nngroup.com/media/editor/2012/10/30/dirman_2a.gif" style="width: 166px; height: 146px;"/></td>
			<td><img alt="Dragging to the wrong pixel" src="http://media.nngroup.com/media/editor/2012/10/30/dirman_2b.gif" style="width: 174px; height: 157px;"/></td>
		</tr>
		<tr>
			<td><strong>Figure 2a. </strong><br/>
			Lexical level error: "Foo" is dragged to the Backup icon instead of to the Trash icon.</td>
			<td><strong>Figure 2b. </strong><br/>
			Alphabetic level error: "Foo" is released at a pair of screen coordinates which are not inside the Trash icon.</td>
		</tr>
	</tbody>
</table>

<p>Figure 2a shows an error I have frequently made myself. The file icon is dragged to the icon representing backups, and not to the Trash icon as intended. This mistake constitutes a lexical level error since the syntax of dragging an icon on top of another icon has been correctly specified. It just happens that the second operand of the command has been given an erroneous value. Further analysis shows that the problem is due to a "capture error" (Norman 1983) because the two commands "delete file" and "backup file" start out almost identically by a gesture sweeping the file icon towards the bottom right of the screen. It happened that I had positioned by backup icon next to the Trash icon and that I used backup much more frequently than I used delete. After having observed the frequency of my error and having completed this analysis of the dialogue, my solution was simply to move the backup icon to the lower left part of the screen instead of the lower right. After this change, the gestures for backup and delete start out so differently that I have never since made the error of confusing the two commands.</p>

<p>Figure 2b shows a frequently observed error where the user has moved the document icon to a position just outside the Trash icon. Since most of the outline icon overlaps the Trash icon, the novice user may think that the Trash has been indicated as the destination for the document, but in actual fact, the cursor's "hot spot" is outside the Trash icon and therefore indicates another destination for the document. When the user lets go of the mouse button, the document will not be discarded but will be moved to a new location between the Backup and Trash icons. This is an alphabetic level error because the user is erroneously specifying a point on the screen which is outside the desired region. The experienced user may note the lack of lexical feedback since the Trash icon has not highlighted, thus indicating that it has not been specified, but this absence of feedback is unfortunately easy to miss.</p>

<p>Payne (1990) has also frequently observed users missing the Trash icon. His suggestion for a redesigned interface is to increase the feedback when users do specify the Trash icon as a destination for an icon being moved. He proposes having the outline icon disappear from the screen when it is over the Trash, thus giving a preview of the result of releasing the mouse button. This feedback action would give users information on both the syntax level ("you will have completed an entire command specification if you let go of the mouse button now") as well as the semantic level ("the action specified by this command will be to delete the document"). This increased feedback suggested by Payne might therefore alleviate the problems novice Macintosh users have been observed to have (Nielsen 1987) because of the generic nature of icon movements, since different feedback could be given depending on the chosen destination. For example, it would be possible to distinguish between the <code> move </code> operation started by dragging the document icon to a folder icon from same disk as the document and the <code> copy </code> operation started if the folder was on a different disk. Additional possibilities for enhanced feedback includes the use of sound effects to illustrate the user's actions (Gaver 1989).</p>

<p>For the specific alphabetic level error discussed here, it would probably be better, however, to redesign the lower levels of the dialogue. My suggestion would be either to allow an icon to be specified as the destination symbol for a command if there was more than 50% overlap between it and the icon being dragged, or to implement a "snap-to" attraction between destination icons and the cursor. The level of analysis presented in this paper is not sufficient to decide for sure whether one of these solutions to the icon trashing problem would be better than Payne's solution. The protocol model can only identify problems and suggest possible improvements. It would still be necessary to perform empirical tests in cases like this where there are several competing solutions.</p>

<p>In addition to the lexical and alphabetic errors discussed above, it is even possible to make physical level errors in direct manipulation interfaces. A common example is the manipulation of scrolling lists where the scrolling speed is too fast. When the program runs on a computer with the speed of the original Macintosh Plus, this scrolling works quite nicely, and the user can easily make any desired page number appear in the visible part of the list. Unfortunately, the implementation of the scrolling speed does not take variations in the computer's execution speed into account. When the program is run on the four times faster Macintosh SE/30, the list simply scrolls four times as fast as it did on the Macintosh Plus. This scrolling speed almost always make users overshoot because they hold down the mouse button too long, thus making an error on the physical level of the dialogue.</p>

<p>Finally, errors can also occur on the higher levels of the dialogue. It will often be impossible to avoid goal or task level errors, but in many cases, the computer can help the user in the case of semantic errors. As an example, consider the dialogue specifying the file to be opened by an application. In a traditional, character-based interface, this task would be realized by a command somewhat like<br/>
<code>open foo.txt </code></p>

<p>If the file foo.txt was a text file and the application required a graphics file, this command would constitute an error on the semantic level of the dialogue even though it was syntactically correct. Therefore, the user would get some kind of error message ranging from "ILLEGAL FILE" to "The file 'foo.txt' is a text file and cannot be opened since [Name-of-Application] only supports graphics files"-depending on the attention to usability engineering during the design of the application. In any case, assuming that the user actually wanted to open foo.txt in the current application, the text-based interface did allow the user to express that semantic intention. This action immediately resulted in an error message, thus indicating the user's mistake and hopefully clarifying the situation.</p>

<p>Many modern user interfaces like the Macintosh prevent the user from getting these error messages because they only allow the specification of semantically valid actions. In these systems, users open files by selecting them from a menu listing only those files that are currently legal operands for the open command. Since other files are not on the list, the user cannot click on them, and the interface avoids the "illegal file" message completely. Assume again that a user was in a graphics application and wanted to open the text file foo.txt. Often, the indirect feedback of not listing the file name in the menu will not be sufficient to make users realize their semantic error. Instead, users may conclude that the desired file must have been stored elsewhere in the file system, leading them on a major hunt through their various subdirectories.</p>

<p>Exactly this kind of problem occurred for a user who was the manager of the computer-human interaction group at the National Defence Research Center in a Western European country and was thus no novice user. He was returning to his computer after having been interrupted for slightly more than an hour because of my visit and he wanted to save his work. The Save command was grayed out, however, leading him to exclaim "Save! ... Why won't it Save? How can I get it to Save?" He tried various options without getting the computer's permission to save until he finally remembered that he had actually already saved his work when he was originally interrupted an hour earlier. This user was quite frustrated because it was impossible for him to perform his intended operation and because the computer could provide no feedback in this break-down of a protocol on the task level. An alternative design would have allowed the user to activate even the illegal Save command and would then have provided a simple feedback message somewhat like, "You have made no changes to [Name-of-File] since the last time you saved it."</p>

<p>In situations like those outlined above, having error messages may actually help the user-at least if they are worded according to established human factors principles. Taking the layered protocol view of human-computer interaction implies that negative acknowledgements (error messages) are better than the absence of communication that can easily result from a "no errors" design philosophy.</p>

<h2>3. Directness and Indirectness</h2>

<p>The very term "direct manipulation" implies some kind of directness in the user interface. Again, the layered protocol analysis indicates some modifications to this supposed advantage. As a matter of fact, it is possible for a direct manipulation interface to feel very indirect to the user on some levels of the dialogue.</p>

<p>For example, setting a tabulator in one desktop publishing program requires the following series of steps:</p>

<ol>
	<li>Call up the tabulator dialog box (shown in Figure 3) by using the pull-down menu or pressing a function key.</li>
	<li>Since the dialog box appears independently of the text it controls, the user will have to position the dialog box until it is next to the text. Otherwise it will be very hard to compare the tab setting with the text layout. It may also be necessary to scroll the tab ruler if the text line is wider than the dialog box.</li>
	<li>Actually change the tab setting by dragging the tab indicator.</li>
	<li>Click OK.</li>
	<li>Since the text does not reformat during step 3, the user will now have to observe the result of changing the tab setting to see whether the intended layout has been achieved. If not, it is necessary to start over with step 1.</li>
</ol>

<center>
<p><img alt="Complex GUI syntax" src="http://media.nngroup.com/media/editor/2012/10/30/dirman_3.gif" style="width: 431px; height: 118px;"/></p>

<p><br/>
<strong>Figure 3. </strong> Setting the tabulator marker for the text "18 %" in an early version of the desktop publishing program ReadySetGo. The dialog box containing the tab marker can be moved and scrolled independently of the text it controls.</p>
</center>

<p>Each of these steps is actually quite direct and relies on direct manipulation interaction techniques. For example, step 2 is achieved by dragging the dialog box to the desired location, and step 3 involves grabbing the tab marker with the mouse and directly moving it to the desired new setting. In spite of this directness on the lexical level of the dialogue, the complete syntax for the operation turns out to feel very indirect.</p>

<p>Another example of indirectness is based on a complaint by a user (Westland 1988) who reviewed the upgrade of a drawing program in a user group magazine: To perform the task of drawing a rectangle filled with text in the earlier version, one could just draw the rectangle and then immediately start typing from the keyboard. The program would interpret the character input as text which was intended to go into the rectangle. In the revised version one has to first draw the rectangle, then select the "text" mode and use it to define a text region inside the rectangle, and only then type the characters. This later dialogue is more modular and conforms to the general interaction techniques used in the interface. But the user complained about the loss of directness for this frequent special case which now involves two extra steps: 1) select "text", and 2) define input region. The directness in the earlier version was achieved by having a temporal mode for special-case interpretation of keyboard input: If typing occurred as the next action immediately after the drawing of a rectangle, the text went into the rectangle.</p>

<p>Directness sometimes come at the cost of introducing additional complexity in the dialogue. For example, consider the action of pressing down the mouse button while the cursor is over the name of an icon in the Macintosh Finder and then moving the mouse while holding down the mouse button. If the icon had not previously been selected (highlighted), this action will result in moving the icon. But if the icon was the currently selected object, the same action will result in the selection of part of the icon name. A single gesture on the physical level of the dialogue thus has two different interpretations, depending on state of the interface. This modedness of the interaction is necessary to achieve two types of directness: The editing of icon names can be achieved directly by selecting part of the name and typing in a replacement text, thus achieving a direct mapping between the input and output languages for the operation. At the same time, grabbing hold of an unselected icon to move it seems to be a very frequent operation during use of the Macintosh Finder. Therefore, the requirements for precision at the physical level and alphabetical levels of the dialogue are decreased by allowing the user to achieve the lexical level effect of indicating the icon as the operand for the move operation by pointing to any location within the icon or its name.</p>

<p>A final example of directness cannot be achieved by traditional forms of direct manipulation. Assume that a user of a window system has a large and a small window and that the large window is on top, completely hiding the small window. The user now wants to bring the small window to the front and make it visible, but since it is hidden, there is no way for the user to directly manipulate the small window with the mouse. The standard way to perform this task is to first move the large window off to the side to make the small window visible. The user then grabs the small window and moves it to a third location to make room for moving the large window back to its original position. And the user can then finally move the small window back to its original location where it will now be on the top because it was the last object to be selected. This solution is obviously very indirect.</p>

<p>A faster way to solve the problem is by having a special command to bring the large window to the back, thus implicitly bringing the small window to the front. This solution involves an indirect mapping between the task level and the semantic level of the dialogue, as the user will have to realize a task expressed in terms of one object by a semantic operation expressed in terms of another object.</p>

<p>In contrast, the most direct solution to the problem would involve a menu listing all the windows and allow the user to bring a window to the front by choosing its name from the menu.</p>

<h2>4. Direct Mappings</h2>

<p>As shown in the previous section, interaction styles that are normally considered as direct manipulation can actually be quite indirect from the user's perspective, and there are also situations where directness is better achieved by alternative interaction styles such as menu listings.</p>

<p> </p>

<center>
<p><img alt="Layered communication" src="http://media.nngroup.com/media/editor/2012/10/30/dirman_4.gif" style="width: 248px; height: 153px;"/></p>

<p><br/>
<strong>Figure 4. </strong> The communication principle in the layered protocol model. A communication on level <em> i </em> of the model (indicated by the gray arrow) is realized by an exchange of information on level <em> i-1 </em> . Both the user and the computer will have to translate between the two levels as indicated by the vertical arrows. The units of information exchanged at each level of the model are listed in Table 1.</p>
</center>

<p>The principle of direct mappings between dialogue levels seems to provide a better way to conceptualize directness in interactive systems. As shown in Figure 4, the layered protocol model involves several transformations between different units of information and we would want those transformations to be as direct mappings as possible to make it easy for the user to move between the levels. The idea of direct mappings is similar to the mathematical notion of isomorphisms; there is a one-to-one function between the two levels and the relationships between the units of information are the same on the two levels and are preserved by the function. For the analysis of dialogues, however, it does not seem possible to use the strict mathematical definition, so the term "direct mapping" is used in a slightly looser sense to indicate a feel of close and direct relation between two interaction levels.</p>

<p>As an example of a direct mapping, consider the operation of deleting six lines in a text editor. In a modern display editor, the user's input syntax would involve selecting the six lines and activating the delete command, utilizing a direct mapping from the user's knowledge of what lines to delete and the user's specification of those lines as an operand to the delete command. The system output would involve the removal of the selected lines from the display, thus making the mapping between the user input and the system output extremely direct following the principle of stimulus-response compatibility (John et al. 1985). In a traditional, line-oriented editor, the user input to realize this task would be something like DELETE 27-32 and the result would be an unchanged display on the syntax level even though the lines would be removed at the semantic level. This is indirect with respect to both the mappings just considered.</p>

<p>The directness of mappings is not always a clear-cut property of a dialogue element. For example, I recently designed a hypertext system (Nielsen 1990) with a front page screen depicting a book cover. This illustration served two functions in the user interface. First, it was an output language element intended to indicate that the screen was the front of a hypertext, thus making it easy for users to recognize the screen and its status when they returned to it after some period of reading. Second, the illustration was intended as the control element for getting into the hypertext proper. By clicking on the book cover as an input action, the user would "open the book" and start reading. It turned out that the illustration served the first purpose very well but failed at the second. In other words, the mapping between the lexical element of a book cover and the semantic notion of "beginning of information" was close enough to be immediately understood. But the mapping between the lexical book cover picture and the semantic notion of opening was too indirect, since a book cover suggest being pulled up, while a mouse button is pushed down during a click. Therefore, the physical level action needed to realize the lexical level element of activating the book illustration interfered with its interpretation as part of the input language. This example shows that we still do not know enough about user interface design to rely on a purely mathematical definition of isomorphisms between the levels of the dialogue.</p>

<p>Even though the directness of a mapping cannot be measured exactly, the concept of direct mappings can still be used to gain a better understanding of several forms of directness in user interfaces. In the rest of this section, direct mappings are used as a unifying principle in the discussion of direct manipulation, transparency, WYSIWYG, immediate command specification, articulatory directness, and computational appliances.</p>

<h3>Direct Manipulation</h3>

<p>Direct manipulation may be defined as a direct mapping between the semantic level of the dialogue and the syntactic level. The syntax of operations should correspond to a metaphor of the semantic change in the data and the screen representation of objects should mirror their internal state. The classic example is the file deletion shown in Figure 1. Moving an icon around on the screen corresponds to moving the file in the file system. When the icon is dropped into the Trash icon, the file icon disappears, and (in some implementations) the Trash icon changes to look stuffed.</p>

<p>According to the direct mapping view, direct manipulation is not just related to the dialogue's input language. The output language is also of interest for determining the degree of directness in the mapping between the semantic and the syntactic level of the dialogue. For example, consider a multi-font text editor where the user is about to type some text to be inserted at a point between two existing characters, say, A and B. In many systems, this system state would look something like A|B where the vertical bar | is a blinking insertion cursor. When the user starts typing, the font of the new text could be that of A, that of B, that of the previous text between A and B (if the user had just removed some such text), or it could even be something altogether different (if the user had just issued some font changing command). Most text editors provide no visible indication of this semantic state, leaving the user to guess. In most cases, this lack of information does not matter since A, B, and the new text will all have the same font. Otherwise, the user will have to start typing to learn what font will be used, leading to a small degree of indirectness: There is no way to "directly manipulate" (or learn) the font state at the insertion mark since it represents an empty object.</p>

<h3>Transparency</h3>

<p>Transparency (Maass 1983) is a direct mapping between goal level and the semantic level. What you want to do and what you can do corresponds, and it is not necessary to think about transforming real world objects and operations to computer-oriented objects and actions. The term itself suggests the "disappearance" of the computer-oriented concepts in the dialogue.</p>

<p>A typical example of the lack of transparency is the complicated installation procedures involved in transferring certain large applications from the floppy disks on which they are sold to the user's hard disk. The user's goal is to move the application to the hard disk and to run it, but it is sometimes necessary for the user to follow long and complicated instructions on how to establish various subdirectories and to copy files from the floppies in a specific order. These computer-oriented concepts and actions are completely unnecessary from the user's goal perspective.</p>

<h3>WYSIWYG</h3>

<p>To achieve WYSIWYG (what you see is what you get), there should be a direct mapping between the goal state (what you want) and the syntax level (what you see). Remember that the syntax level contains the two-dimensional layout of the dialogue elements on the screen. In a WYSIWYG interface, the screen representation of objects mirrors the real world, and the syntax of operations correspond to the desired real world action.</p>

<p>As an example, most mouse-based interfaces have a two-cursor problem (Brooks 1988) where one cursor is used to point and another cursor indicates where text input will appear. Users often confuse these two cursors because of the lack of WYSIWYG in the interface. The pointing cursor that tracks the user's mouse movements is the user's focus of attention ("what you see") in the syntax, but the input ("what you get") changes the output product according to the location of the insertion mark. One possible solution to the two-cursor problem is to follow a strict WYSIWYG interpretation of a single cursor where input appears wherever the pointer happens to be when the user hits the keyboard (Akscyn et al. 1988).</p>

<p>Changing the headers in one early graphical-interface word processor required the user to open a special window for the header. The actual editing followed direct manipulation principles as defined here since there was a direct mapping between the syntax for changing the header and the semantic change in the stored header information. But the interaction technique was not WYSIWYG since the user could not easily translate between the syntax for changing the header and the goal of making the header look a specific way in relation to the rest of the page. For a true WYSIWYG editing of headers, it becomes necessary to make them editable in the main window together with the main sequence of text in the file.</p>

<h3>Immediate Command Specification</h3>

<p>Immediate command specification involves a direct mapping between the syntactic level and the alphabetic level to allow users to package an entire command in a single, atomic input action. Typical examples are function keys in traditional interfaces and the double click shortcuts in many graphical user interfaces. It is also possible to allow the individual user to build "buttons" (MacLean et al. 1990) that run a complex sequence of operations when they are activated.</p>

<p> </p>

<center>
<p><img alt="Change bar dialog box" src="http://media.nngroup.com/media/editor/2012/10/30/dirman_5.gif" style="width: 193px; height: 151px;"/></p>

<p><br/>
<strong>Figure 5. </strong> Interface control for specifying borders surrounding the text in a word processor. Here, the user has specified that a wide bar is to appear in the left margin.</p>
</center>

<p>Figure 5 shows a control for specifying text borders in a word processor with a graphical user interface. This interface has both direct manipulation, WYSIWYG, and transparency aspects. The WYSIWYG property comes from the direct correspondance between the position of the borders on the miniature page and their position on the screen and the final printout. The syntax for placing the border lines on the miniature uses a direct manipulation technique corresponding directly to the semantics: Clicking first on the fat line in the palette and then on the left margin of the miniature page. Finally, the semantics of having a wide bar in the left margin correspond directly to the goal of getting a fat line in the margin because the interface has been simplified</p>

<p>A user who often wanted a wide bar to appear in the left margin could construct a button (perhaps labelled "Make Change Bar") that would have the same effect as calling up the control in Figure 5 and modifying it as shown. This hypothetical interface would be an immediate command specification. As this example shows, macros or other user-constructed customizations will often be necessary for immediate command specification unless the interface designer has conducted a very detailed task analysis.</p>

<h3>Articulatory Directness</h3>

<p>Articulatory directness involves a direct mapping between the alphabetic level and the physical level to ensure that the primitive input actions correspond to the user's physical actions. The classic example is the input unit of moving the cursor to a specific set of screen coordinates, (x,y). The highest articulatory directness is achieved by touching a touch screen at (x,y) since there is practically no transformation between the two levels. A slight degree of indirectness is involved in using a mouse since it is not moved in the same plane as the screen coordinates being specified. Even so, moving the mouse still has higher articulatory directness than combining a set of activations of the arrow keys.</p>

<p>In many visual interfaces, input actions sometimes involve not just the specification of a single point but the specification of a curve. When the curve is used to drag an icon, the mouse has a higher articulatory directness than a stylus, whereas the stylus has a higher articular directness than the mouse when it comes to handwriting input because of the different requirements for the preciseness of the curve.</p>

<h3>Computational Appliances</h3>

<p>The computational appliance principle involves a direct mapping all the way from the goal level to the physical level. The computer as tool, where you do not really "interact" with the computer as such-you just do your work. The computational appliance idea is really to achieve the ultimate directness.</p>

<p>An example of a measurement of the extent to which a computer served as a computational appliance, is the study by Haas (1989) of users revising a text either in a computer editor or using pen and paper. She had test subjects think aloud while they edited the text and she then counted the proportion of their utterances referring to the editing medium instead of the writing task. In the pen and paper condition, only 3% of the utterances referred to the medium, while 14% of the utterances in the computer condition did so. In the ideal case, users would be able to focus completely on their primary task and just deal directly with it without having to think about the computer at all. Haas' study indicates that we still have some way to go to achieve this goal.</p>

<p>In the computational appliance mapping, there is a direct connection between the user's input and the achieved goal, and the computer's output is interpreted as a signal for the real world goal state instead of signs or symbols of the computer's state (Rasmussen 1983). With current computers, the computational appliance mapping is of course easiest to achieve for text editing where the input and output equipment on modern workstations almost completely match the goal of producing sheets of paper with letters on them.</p>

<h2>5. Conclusions</h2>

<p>Analyzing the concept of direct manipulation according to a detailed layered interaction model indicates several modifications to a simplistic view-especially with respect to error situations where usability often is better served by explicit error handling. Furthermore, it seems that a more general notion of directness is more relevant for usability than the traditional concept of direct manipulation, since some direct manipulation designs can feel very indirect. The direct mapping principle presented here accounts for several other common interaction concepts such as WYSIWYG in addition to direct manipulation.</p>

<p> </p>

<center>
<p><img alt="Layered communication" src="http://media.nngroup.com/media/editor/2012/10/30/dirman_6.gif" style="width: 310px; height: 327px;"/><br/>
<strong>Figure 6. </strong> Summary of the direct mappings discussed in section 4. For simplicity, the figure only shows those interaction levels actually involved in these mappings, but similar mappings could be defined between the rest of the interaction levels.</p>
</center>

<p>Summary of the direct mappings discussed in section 4. For simplicity, the figure only shows those interaction levels actually involved in these mappings, but similar mappings could be defined between the rest of the interaction levels. Previous work has shown that experienced users find the concept of "user friendliness" more closely related to whether an application is pleasant to work with than to whether it is easy to learn (Nielsen 1989). Even though it is not really known what makes an interface pleasant, it is likely that the various forms of directness contribute to pleasantness. Another likely contributing factor is good-looking graphic design. Further research is required to address this issue.</p>

<h3>Acknowledgements</h3>

<p>The author would like to thank Saul Greenberg, Robert Jacob, and John Schnizlein for helpful comments on earlier versions of the manuscript.</p>

<h2>References</h2>

<ol>
	<li>Akscyn, R., McCracken, D., and Yoder, E. (1988). KMS: A distributed hypermedia system for managing knowledge in organizations. Communications of the ACM 31, 820-835.</li>
	<li>Brooks, F.P., Jr. (1988). Grasping reality through illusion-Interactive graphics serving science. Proceedings ACM CHI'88 (Washington, DC, 15-19 May), 1-11.</li>
	<li>Gaver, W.W. (1989). The SonicFinder: An interface that uses auditory icons. Human-Computer Interaction, 4, 67-94.</li>
	<li>Haas, C. (1989). Does the medium make a difference? Two studies of writing with pen and paper and with computers. Human-Computer Interaction, 4, 149-169.</li>
	<li>Hutchins, E.L., Hollan, J.D., and Norman, D.A. (1986). Direct manipulation interfaces. In: D.A. Norman and S.W. Draper (eds.): User Centered System Design: New Perspectives on Human-Computer Interaction, Hillsdale, NJ: Lawrence Erlbaum Associates, 87-124.</li>
	<li>John, B.E., Rosenbloom, P.S., and Newell, A. (1985). A theory of stimulus-response compatibility applied to human-computer interaction, Proceedings ACM CHI'85 (San Francisco, CA, 14-18 April), 213-219.</li>
	<li>Maass, S. (1983). Why systems transparency? In: T.R. Green, S.J. Payne, and G.C. van der Veer (eds.): The Psychology of Computer Use. London: Academic Press, 19-28.</li>
	<li>MacLean, A., Carter, K., Lovstrand, L., and Moran, T. (1990). User-tailorable systems: Pressing the issues with buttons. Proceedings ACM CHI'90 (Seattle, WA, 1-5 April), 175-182.</li>
	<li>Nielsen, J. (1986). A virtual protocol model for computer-human interaction. International Journal of Man-Machine Studies, 24, 301-312.</li>
	<li>Nielsen, J. (1987). A user interface case study of the Macintosh. In: G. Salvendy (ed.): Cognitive Engineering in the Design of Human-Computer Interaction and Expert Systems. Amsterdam: Elsevier Science Publishers, 241-248.</li>
	<li>Nielsen, J. (1989). What do users really want? International Journal of Human-Computer Interaction, 1, 137-147.</li>
	<li>Nielsen, J. (1990). The art of navigating through hypertext. Communications of the ACM, 33, 296-310.</li>
	<li>Norman, D.A. (1983). Design rules based on analyses of human error. Communications of the ACM, 26, 254-258.</li>
	<li>Payne, S.J. (1990). Looking HCI in the I. Proceedings IFIP INTERACT'90 Conf. (Cambridge, U.K., 27-31 August), 185-192.</li>
	<li>Rasmussen, J. (1983). Skills, rules, and knowledge: Signals, signs, and symbols, and other distinctions in human performance models. IEEE Transactions on Systems, Man, and Cybernetics, 13, 257-264.</li>
	<li>Shneiderman, B. (1982a). The future of interactive systems and the emergence of direct manipulation. Behaviour and Information Technology, 1, 237-256.</li>
	<li>Shneiderman (1982b). Designing computer systems messages. Communications of the ACM, 25, 610-611.</li>
	<li>Taylor, M.M. (1988a). Layered protocols for computer-human dialogue I: Principles. International Journal of Man-Machine Studies, 28, 175-218.</li>
	<li>Taylor, M.M. (1988b). Layered protocols for computer-human dialogue II: Some practical issues. International Journal of Man-Machine Studies, 28, 219-257.</li>
	<li>Tognazzini, B. (1989). Achieving consistency for the Macintosh. In: J. Nielsen (ed.): Coordinating User Interfaces for Consistency, San Diego, CA: Academic Press, 57-73.</li>
	<li>Westland, M.J. (1988). 10 steps forward, 2 steps back, The Active Window (Boston Computer Society), 5 (August), 36-40.</li>
	<li>Wixon, D. and Good, M. (1987). Interface style and eclecticism: Moving beyond categorical approaches. Proceedings Human Factors Society 31st Annual Meeting, 571-575.</li>
</ol>
</section>
  

  
  <section>
    
        
    
  </section>
  
  
  
  
  

  


  <section>
    <br />
    <p class="share"><b>Share <span class="hide-for-small-down">this article</span>:</b> <a href="https://twitter.com/intent/tweet?url=http://www.nngroup.com/articles/direct-manipulation-analysis/&amp;text=A%20Layered%20Interaction%20Analysis%20of%20Direct%20Manipulation&amp;via=nngroup">Twitter</a> | <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=http://www.nngroup.com/articles/direct-manipulation-analysis/&amp;title=A%20Layered%20Interaction%20Analysis%20of%20Direct%20Manipulation&amp;source=Nielsen%20Norman%20Group">LinkedIn</a> | <a href="https://plus.google.com/share?url=http://www.nngroup.com/articles/direct-manipulation-analysis/">Google+</a> | <a href="mailto:?subject=NN/g Article: A Layered Interaction Analysis of Direct Manipulation&amp;body=http://www.nngroup.com/articles/direct-manipulation-analysis/">Email</a></p>
    <hr />
  </section>

  <footer class="l-subsection">
    
    
    <div class="learn-more">
      <h2>Learn More</h2>
      <p><a href="../subscribe/index.php">Subscribe to the weekly newsletter</a> to get notified about future articles.</p>
      </div>
    
    

    <div class="row show-for-small-down related-content">
      
      
      

      
      
        <div class="column small-12 topic-links">All articles about:
          <ul>
            
            <li><a href="../../topic/applications/index.php">Application Design</a></li>
            
            <li><a href="../../topic/direct-manipulation/index.php">direct manipulation</a></li>
            
            <li><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction</a></li>
            
          </ul>
        </div>
      
      

      
      
        <div class="column small-12">
          <h3>UX Conference Training Courses</h3>
           <ul class="no-bullet">
    
        <li><a href="../../courses/ux-basic-training/index.php">UX Basic Training</a></li>
    
        <li><a href="../../courses/hci/index.php">User Interface Principles Every Designer Must Know</a></li>
    
        <li><a href="../../courses/interaction-design-3-day-course/index.php">Interaction Design: 3-Day Course</a></li>
    
        <li><a href="../../courses/application-ux/index.php">Application Design for Web and Desktop</a></li>
    
        <li><a href="../../courses/emotional-design/index.php">Emotional Design</a></li>
    
</ul>
        </div>
      
      

      
      
        <div class="small-12 column">
          <h3>Research Reports</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../reports/best-applications-2/index.php">Application Design Showcase: 2012</a></li>
              
            
              
                <li><a href="../../reports/enterprise-mobile-showcase/index.php">Mobile Intranets and Enterprise Apps</a></li>
              
            
              
                <li><a href="../../reports/website-tools-and-applications-flash/index.php">Website Tools and Applications with Flash</a></li>
              
            
              
                <li><a href="../../reports/customization-features/index.php">Customization Features Done Correctly for the Right Reasons</a></li>
              
            
              
                <li><a href="../../reports/best-applications-1/index.php">Application Design Showcase: 2008</a></li>
              
            
          </ul>
        </div>
      
      

      
      
        <div class="small-12 column">
          <h3>Online Seminars</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../online-seminars/ux-big-data/index.php">UX for Big Data</a></li>
              
            
          </ul>
        </div>
      
      
    </div>

    <div class="hide-for-small-down row related-content">
      <div class="medium-6 columns">
        
        
          <div>
            <h3>Articles</h3>
            <ul class="no-bullet">
              
                
                <li><a href="../direct-manipulation/index.php">Direct Manipulation: Definition</a></li>
                
              
                
                <li><a href="../gui-slider-controls/index.php">Slider Design: Rules of Thumb</a></li>
                
              
                
                <li><a href="../customization/index.php">7 Tips for Successful Customization</a></li>
                
              
                
                <li><a href="../customization-personalization/index.php">Customization vs. Personalization in the User Experience</a></li>
                
              
                
                <li><a href="../top-intranet-trends/index.php">Top 10 Intranet Trends of 2016</a></li>
                
              
            </ul>
          </div>
        
        

        
          
          <div>
            <h3>Research Reports</h3>
            <ul class="no-bullet">
              
                
                  <li><a href="../../reports/best-applications-2/index.php">Application Design Showcase: 2012</a></li>
                
              
                
                  <li><a href="../../reports/enterprise-mobile-showcase/index.php">Mobile Intranets and Enterprise Apps</a></li>
                
              
                
                  <li><a href="../../reports/website-tools-and-applications-flash/index.php">Website Tools and Applications with Flash</a></li>
                
              
                
                  <li><a href="../../reports/customization-features/index.php">Customization Features Done Correctly for the Right Reasons</a></li>
                
              
                
                  <li><a href="../../reports/best-applications-1/index.php">Application Design Showcase: 2008</a></li>
                
              
            </ul>
          </div>
          
        

      </div>
      <div class="medium-6 columns">
        
          
          <div>
            <h3>UX Conference Training Courses</h3>
             <ul class="no-bullet">
    
        <li><a href="../../courses/ux-basic-training/index.php">UX Basic Training</a></li>
    
        <li><a href="../../courses/hci/index.php">User Interface Principles Every Designer Must Know</a></li>
    
        <li><a href="../../courses/interaction-design-3-day-course/index.php">Interaction Design: 3-Day Course</a></li>
    
        <li><a href="../../courses/application-ux/index.php">Application Design for Web and Desktop</a></li>
    
        <li><a href="../../courses/emotional-design/index.php">Emotional Design</a></li>
    
</ul>
          </div>
          
        
        
          
          <div>
            <h3>Online Seminars</h3>
            <ul class="no-bullet">
            
                <li><a href="../../online-seminars/ux-big-data/index.php">UX for Big Data</a></li>
            
            </ul>
          </div>
          
        
      </div>
    </div>
  </footer>
</article>

        </div>

      

    
  </div>
  <div class="row">
    
      <div class="column small-12 show-for-small-down l-navrail announcements">
        
          
        
      </div>
    
    
  </div>

    </div>
    




<footer id="page-footer">
    <div class="row hide-for-print">
        <div class="column small-12 medium-6">
            <section class="footer-popular">
                
                <h1>Popular Topics</h1>
                <div style="float: left;  width: 15em; display: block;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/applications/index.php">Application Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/e-commerce/index.php">E-Commerce Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/eyetracking/index.php">Eyetracking </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/information-architecture/index.php">Information Architecture </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/intranets/index.php">Intranet Design </a></li>
	<li><a href="../../topic/interaction-design/index.php">Interaction Design </a></li>
</ul>
</div>

<div style="float: left;  width: 15em; display: block; list-style: none;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/mobile-and-tablet-design/index.php">Mobile and Tablet Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/credibility-persuasion/index.php">Persuasive Design</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/research-methods/index.php">Research Methods</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/user-testing/index.php">User Testing </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/web-usability/index.php">Web Usability </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/writing-web/index.php">Web Writing &amp; Content Strategy </a></li>
</ul>
</div>

<p>&nbsp;</p>

                
            </section>
        </div>

        <div class="column small-12 medium-6">
            <div class="row">
                <div class="column small-6">
                    <section class="footer-about">
                        <h1>Follow Us</h1>
                        <ul>
	<li><a href="../subscribe/index.php">Subscribe to our weekly UX newsletter</a>,&nbsp;Jakob Nielsen&#39;s Alertbox</li>
	<li>Visit Don Norman&#39;s website, <a href="http://www.jnd.org/"> www.jnd.org </a></li>
	<li>Visit Bruce Tognazzini&#39;s website, <a href="http://www.asktog.com/"> www.asktog.com </a></li>
	<li style="padding-top: 6px;"><a class="twitter-follow-button" data-show-count="false" data-size="large" href="https://twitter.com/nngroup"><img alt="Follow @nngroup on Twitter" height="20" src="https://media.nngroup.com/media/editor/2016/12/30/twitter-follow-button.png" style="width: 117px; height: 20px;" width="117" /></a></li>
	<li style="padding-top: 6px;"><img alt="" height="14" src="https://media.nngroup.com/media/editor/2016/07/26/feed-icon-14x14.png" style="vertical-align:text-top;width:14px;margin-right:.25em;" width="14" /><a href="../../feed/rss/index.php">Subscribe to our RSS feed</a></li>
</ul>

                    </section>
                </div>

                <div class="column small-6">
                    <section class="footer-about right-column">
                        <h1>About
</h1>
                        <ul class="no-bullet">
	<li><a href="../../about/contact/index.php">Contact information</a></li>
	<li><a href="../../about/index.php">About Us</a></li>
	<li><a href="../../about/why-nng/index.php">Why NN/g? </a></li>
	<li><a href="../../return-policy/index.php">Return Policy</a></li>
</ul>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="copyright column small-12">
            <p><a href="../../copyright-and-reprint-info/index.php">Copyright</a> &copy; 1998-2017 Nielsen Norman Group, All Rights Reserved.</p>
        </div>
    </div>
</footer>


    


        <script src="https://media.nngroup.com/static/vendor/fastclick/js/fastclick.min.js"></script>
        <script src="https://media.nngroup.com/static/vendor/foundation/js/foundation.min.js"></script>
        <script src="http://media.nngroup.com/static/js/app.js"></script>
        <script src="http://media.nngroup.com/static/js/scroll-to-fixed.js"></script><script>var externalLinks = document.querySelectorAll('a[href^="http"]');for (var i = externalLinks.length-1; i >= 0; i--) {externalLinks[i].addEventListener("click", function(event) { alert("External links are disabled for this test."); event.preventDefault(); }, false);}var mailLinks = document.querySelectorAll('a[href^="mailto"]');for (var i = mailLinks.length-1; i >= 0; i--) {mailLinks[i].addEventListener("click", function(event) { alert("Email links are disabled for this test."); event.preventDefault(); }, false);}var externalForms = document.querySelectorAll('form');for (var i = externalForms.length-1; i >= 0; i--) {externalForms[i].addEventListener("submit", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false); externalForms[i].addEventListener("click", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false);}</script>

        <script>
            
            $(function() {
                $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
                $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
            });
        </script>

        
<script src="https://media.nngroup.com/static/js/articles.js"></script>


        
        <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1072699960;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072699960/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
    </body>

<!-- Mirrored from www.nngroup.com/articles/direct-manipulation-analysis/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:14:00 GMT -->
</html>
