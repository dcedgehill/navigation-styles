<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->





    
<!-- Mirrored from www.nngroup.com/articles/collecting-feedback-from-users-of-an-archive-reader-challenge/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:14:58 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1026.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"beacon":"bam.nr-data.net","queueTime":3,"applicationTime":925,"agent":"","licenseKey":"1a30ef9931","errorBeacon":"bam.nr-data.net","applicationID":"2048992","transactionName":"NFxSMRBUX0EDUkIPDA0WdhAMVkVbDV8ZBxEXUFMJB0YfRAtUQRVZIktEDAFZVHYHRVcPD01eVRE="}</script>
        <title>Collecting Feedback From Users of an Archive (Reader Challenge)</title><meta property="og:title" content="Collecting Feedback From Users of an Archive (Reader Challenge)" />
  
        
        <meta name="description" content="How to collect usability data from site users, using a historical archive as the case study. Keep surveys simple, collect data from real-world usage, and get feedback from friends of the site.">
        <meta property="og:description" content="How to collect usability data from site users, using a historical archive as the case study. Keep surveys simple, collect data from real-world usage, and get feedback from friends of the site." />
        
  
        
	
        
        <meta name="keywords" content="user feedback, surveys, survey forms, field data, friends of the site user group">
        

        

        <meta name="robots" content="noarchive">
        <meta name="viewport" content="width=device-width">

        <link rel="search" type="application/opensearchdescription+xml" title="NN/g Search" href="../../opensearch.xml" />
        <link rel="shortcut icon" href="http://media.nngroup.com/static/img/favicon.ico" />

        <link rel="stylesheet" href="https://media.nngroup.com/static/vendor/font-awesome/css/font-awesome.min.css">

        
        <link rel="stylesheet" href="https://media.nngroup.com/static/css/7232a960f1df.css" type="text/css" />
        <link rel="stylesheet" href="http://fast.fonts.com/t/1.css?apiType=css&amp;projectid=c5846260-9def-46ec-9407-0f1349bd2d64" type="text/css">

        <script src="https://media.nngroup.com/static/vendor/modernizr/js/modernizr.min.js"></script>

        <script type="text/javascript" src="https://media.nngroup.com/static/js/3283be5def74.js"></script>
        

        

        
        
        <!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=229034,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('http://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->


<!-- Google Analytics Tracking Code for http://nngroup.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23132681-1', 'nngroup.com');
  ga('require', 'linkid', 'https://www.nngroup.com/articles/collecting-feedback-from-users-of-an-archive-reader-challenge/linkid.js');
  ga('require', 'displayfeatures');

// Optimizely Universal Analytics Integration
 window.optimizely = window.optimizely || [];
 window.optimizely.push("activateUniversalAnalytics");

  ga('send', 'pageview');

</script>
<script>
/**
* Function to track a click on an outbound link in Google Analytics.
*/
function trackOutboundLink(label, url) {
    if(window.ga && ga.loaded) {
        ga('send', 'event', 'outbound', 'click', label, {'hitCallback': function(){document.location = url;}
        });
    } else {
        document.location = url;
    }
}
/**
* Function to track a click on a link in the article announcement area.
*/
$('#articleAnnouncement a').on('click', function(){
  ga('send', 'event', 'link', 'click', 'articleAnnouncement');
});
</script>

<script type="text/javascript">
 setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2421.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



<!-- Hotjar Tracking Code for http://nngroup.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:121605,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

        <script src="https://media.nngroup.com/static/js/analytics/track_outbound_link.js"></script>
        
        
        
    </head>
    <body class="location-articles articles-detail">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
    <header id="page-header">
        <div id="cart" class="shopping-cart mobile-cart show-for-small-down"></div>
        <div class="row">
            <div id="masthead" class="masthead small-12 medium-7 column">

                <!-- The button to toggles menu on small screen -->

                <h1><a href="../../index.php"><img src="http://media.nngroup.com/static/img/logo-wordmark.png" width="74" height="30"> Nielsen <span class="norman">Norman</span> Group</a></h1>
                <p class="tagline show-for-medium-up">Evidence-Based User Experience Research, Training, and Consulting</p>

                <!-- The button to toggles form search on small screen -->


                <div style="clear: right;width:  100%;height: 1px;"></div><?php define('ROOT_PATH',$_SERVER["DOCUMENT_ROOT"] . '/apps/navigation_styles/navigations/5'); ?><?php include(ROOT_PATH)."/navigation.php"; ?>

                <!-- Drop down menu on small screen -->
                <div id="hideNav" class="show-for-small-down">
                  
                  <ul class="menu-content">
                    <li><a href="../../index.php">Home</a></li>
                    <li class="has-submenu">
                      <a>Training</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../training/index.php">Overview</a></li>
                        
<li id="nav-ux-conference"><a href="../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../online-seminars/index.php"><span>Online Seminars</span></a></li>

                      </ul>
                    </li>
                    <li class="has-submenu">
                      <a>Consulting</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
  <li><a href="../../consulting/index.php">Overview</a></li>
  
  <li><a href="../../consulting/benchmarking/index.php">Benchmarking</a></li>
  
  <li><a href="../../consulting/expert-design-review/index.php">Expert Design Review</a></li>
  
  <li><a href="../../consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
  
  <li><a href="../../consulting/usability-testing/index.php">Usability Testing </a></li>
  
  <li><a href="../../consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
  
  <li><a href="../../in-house-training/index.php">In-House Training</a></li>
</ul>

                    </li>

                    <li><a href="../../reports/index.php">Reports</a></li>
                    <li><a href="../index.php">Articles</a></li>
                    <li class="has-submenu"><a>About NN/G</a><i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../about/index.php">Overview</a></li>
                        <li><a href="../../people/index.php">People</a></li>
                        <li><a href="../../about/why-nng/index.php">Why NN/g?</a></li>
                        <li><a href="../../about/contact/index.php">Contact</a></li>
                        <li><a href="../../news/index.php">News</a></li>
                        <li><a href="../../about/history/index.php">History</a></li>
                        <li><a href="../../books/index.php">Books</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                </div>

                <hr class="header-divider show-for-small-only"/>


            </div>
       </div>

        <div id="hideNav">            <nav class="primary-nav small-12 column">
                <ul>
                    <li><a class="navitem" id="nav-home" href="../../index.php">Home</a></li>
                    <li><a class="navitem" id="nav-training" href="../../training/index.php">Training</a></li>
                    <li><a class="navitem" id="nav-consulting" href="../../consulting/index.php">Consulting</a></li>
                    <li><a class="navitem" id="nav-reports" href="../../reports/index.php">Reports</a></li>
                    <li><a class="navitem" id="nav-articles" href="../index.php">Articles</a></li>
                    <li><a class="navitem" id="nav-about" href="../../about/index.php">About NN/G</a></li>
                </ul>
            </nav>
            <nav class="secondary-nav small-12 medium-12 large-12 columns">
                
                
            </nav>
        </div>
    </header>
    <div class="l-content"><span id="preventClose" style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776; Menu</span>
    
  

  

  <div class="row">
    
      
      <section class="small-12 medium-3 columns collapsable-content l-navrail articles-left-column">
        <h1 class="show-for-small-down collapse-link mobile-browse-header">Browse by Topic and Author</h1>
          <div class="row collapsable">

            
            <div class="small-6 medium-12 columns">
              <section id="article-popular-topics" style="display: none;">
                
                <h1>Topics</h1>
                <ul id="article-popular-topics">
                    
                    <li>
                        
                        <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/intranets/all/index.php">Intranets</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/user-testing/all/index.php">User Testing</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                        
                    </li>
                    
                    <li><a href="#" id="article-see-all-topics"><strong>See all topics&hellip;</strong></a></li>
                </ul>
                
              </section>
              <section id="article-all-topics" style="display: none;">
                
                <h1>
                  <span class="show-for-medium-up">All Article Topics</span>
                  <span class="show-for-small-down">All Topics</span>
                  (<a href="#" id="article-hide-all-topics">hide</a>)
                </h1>
                <ul>
                  
                  <li>
                    
                    <a href="../../topic/accessibility/all/index.php">Accessibility</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/agile/all/index.php">Agile</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/analytics-and-metrics/all/index.php">Analytics &amp; Metrics</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/applications/all/index.php">Application Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/b2b-websites/all/index.php">B2B Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/behavior-patterns/all/index.php">Behavior Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/branding/all/index.php">Branding</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/content-strategy/all/index.php">Content Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/corporate-websites/all/index.php">Corporate Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/design-patterns/all/index.php">Design Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-design-process/all/index.php">Design Process</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/email/all/index.php">Email</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/eyetracking/all/index.php">Eyetracking</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/heuristic-evaluation/all/index.php">Heuristic Evaluation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/human-computer-interaction/all/index.php">Human Computer Interaction</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-ideation/all/index.php">Ideation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/information-architecture/all/index.php">Information Architecture</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/interaction-design/all/index.php">Interaction Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/international-users/all/index.php">International Users</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/intranets/all/index.php">Intranets</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-management/all/index.php">Management</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/navigation/all/index.php">Navigation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/non-profit-websites/all/index.php">Non-Profit Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/personas/all/index.php">Personas</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/persuasive-design/all/index.php">Persuasive Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/prototyping/all/index.php">Prototyping</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/psychology-and-ux/all/index.php">Psychology and UX</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/research-methods/all/index.php">Research Methods</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/search/all/index.php">Search</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/social-media/all/index.php">Social Media</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/strategy/all/index.php">Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/user-testing/all/index.php">User Testing</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-teams/all/index.php">UX Teams</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/visual-design/all/index.php">Visual Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/writing-web/all/index.php">Writing for the Web</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/young-users/all/index.php">Young Users</a>
                    
                  </li>
                  
                </ul>
                
              </section>
            </div>
            <script>
              /* Determine which topics list will be displayed: the full or the
               * partial.  We are inlining this close to the list itself to
               * prevent rendering delays on the client
               */
              var topic_list = $.cookie("article-topics");

              var popular_topics = $('#article-popular-topics');
              var all_topics = $('#article-all-topics');

              if(topic_list == 1) {
                  popular_topics.hide();
                  all_topics.show();
              } else {
                  popular_topics.show();
                  all_topics.hide();
              }
            </script>
            

            
            <div class="small-6 medium-12 columns">
              
              <section id="article-initial-authors">
                  <h1>Author</h1>
                  <ul>
                    
                    <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                    
                    <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                    
                    <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                    
                    <li><a href="#" onClick="$('#article-initial-authors').hide(); $('#article-all-authors').show();"><strong>See all authors&hellip;</strong></a></li>
                  </ul>
              </section>
              <section id="article-all-authors" style="display: none;">
                <h1>
                  All Authors
                  (<a href="#" id="article-hide-all-authors">hide</a>)
                </h1>
                <ul>
                  
                  <li><a href="../author/raluca-budiu/index.php">Raluca Budiu</a></li>
                  
                  <li><a href="../author/patty-caya/index.php">Patty Caya</a></li>
                  
                  <li><a href="../author/susan-farrell/index.php">Susan Farrell</a></li>
                  
                  <li><a href="../author/therese-fessenden/index.php">Therese Fessenden</a></li>
                  
                  <li><a href="../author/kim-flaherty/index.php">Kim Flaherty</a></li>
                  
                  <li><a href="../author/sarah-gibbons/index.php">Sarah Gibbons</a></li>
                  
                  <li><a href="../author/aurora-harley/index.php">Aurora Harley</a></li>
                  
                  <li><a href="../author/kate-kaplan/index.php">Kate Kaplan</a></li>
                  
                  <li><a href="../author/page-laubheimer/index.php">Page Laubheimer</a></li>
                  
                  <li><a href="../author/angela-li/index.php">Angie Li</a></li>
                  
                  <li><a href="../author/hoa-loranger/index.php">Hoa Loranger</a></li>
                  
                  <li><a href="../author/kate-meyer/index.php">Kate Meyer</a></li>
                  
                  <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                  
                  <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                  
                  <li><a href="../author/kara-pernice/index.php">Kara Pernice</a></li>
                  
                  <li><a href="../author/christian-rohrer/index.php">Christian Rohrer</a></li>
                  
                  <li><a href="../author/amy-schade/index.php">Amy Schade</a></li>
                  
                  <li><a href="../author/katie-sherwin/index.php">Katie Sherwin</a></li>
                  
                  <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                  
                  <li><a href="../author/kathryn-whitenton/index.php">Kathryn Whitenton</a></li>
                  
                </ul>
              </section>
              
            </div>
            
          </div>

          
          <section class="hide-for-small-down">
            
              
              <h1>Recent Articles</h1>
              
              <ul>
                  
                  <li><a href="../seamless-cross-channel/index.php">Seamlessness in the Omnichannel User Experience</a></li>
                  
                  <li><a href="../brand-intention-interpretation/index.php">Experience Design: Bridging Brand Intention and Brand Interpretation</a></li>
                  
                  <li><a href="../eyetracking-tasks-efficient-scanning/index.php">Scanning Patterns on the Web Are Optimized for the Current Task</a></li>
                  
                  <li><a href="../flat-design-best-practices/index.php">Flat-Design Best Practices</a></li>
                  
                  <li><a href="../ux-user-stories/index.php">Accounting for UX Work with User Stories in Agile Projects</a></li>
                  
                  <li><a href="../index.php"><strong>See all articles&hellip;</strong></a></li>
              </ul>
              
            
          </section>
          

          
          
          <section class="hide-for-small-down">
            <h1>Popular Articles</h1>
            
            <ul>
                
                <li><a href="../usability-101-introduction-to-usability/index.php">Usability 101: Introduction to Usability</a></li>
                
                <li><a href="../top-10-mistakes-web-design/index.php">Top 10 Mistakes in Web Design</a></li>
                
                <li><a href="../how-users-read-on-the-web/index.php">How Users Read on the Web</a></li>
                
                <li><a href="../f-shaped-pattern-reading-web-content/index.php">F-Shaped Pattern For Reading Web Content</a></li>
                
                <li><a href="../ten-usability-heuristics/index.php">10 Usability Heuristics for User Interface Design</a></li>
                
                <li><a href="../intranet-design-2016/index.php">10 Best Intranets of 2016</a></li>
                
                <li><a href="../which-ux-research-methods/index.php">When to Use Which User-Experience Research Methods</a></li>
                
                <li><a href="../response-times-3-important-limits/index.php">Response Times: The 3 Important Limits</a></li>
                
                <li><a href="../why-you-only-need-to-test-with-5-users/index.php">Why You Only Need to Test with 5 Users</a></li>
                
                <li><a href="../page-fold-manifesto/index.php">The Fold Manifesto: Why the Page Fold Still Matters</a></li>
                
            </ul>
          </section>
          
          

          
            <section class="hide-for-small-down">
              
            </section>
          

      </section>
      

      
        <div class="small-12 medium-9 columns content-body">
          




<article class="l-subsection alertbox-article-content article-detail-bl">
  
  <header>
    <div class="l-subsection">
      <h1>Collecting Feedback From Users of an Archive (Reader Challenge)</h1>
      <div class="author-meta">by
        
          
            
            <a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a>
            
          
        on  January 10, 1999
        
      </div>

      

      
      <div class="topic-links">
        Topics:
        
          <ul>

  <li><a href="../../topic/research-methods/index.php">Research Methods</a></li>

</ul>
        
      </div>
      
    </div>
  </header>
  

  
  
  

  
  <section id="article-body"><p>
	The collective brainpower of the Internet is an awesome beast that used to manifest itself on Usenet newsgroups. Most of these groups have degenerated into spam, flames, and newbie ignorance. The Web has not yet evolved good ways of utilizing this power, since most so-called " <a class="old" href="../community-is-dead-long-live-mega-collaboration/index.php" title="Alertbox Aug. 1997: Community is dead"> community </a>  " sites are equally degenerate.</p>
<p>
	I have always been impressed by the Web knowledge exhibited by the Alertbox readers, so as an experiment in collective brainpower, I decided to collect solutions to a specific Web usability problem from my readers. The <a class="new" href="../original-problem-statement-archivist-needs-user-feedback/index.php" title="Alertbox sidebar: Original Problem Statement - Archivist Needs User 
Feedback"> full problem statement </a>  is available as a sidebar, but briefly the question was <strong> how to get feedback from users to determine usability and future design directions for a large Web archive of historical documents </strong> .</p>
<p>
	I received a large number of great suggestions and analyses, so even though a website has a larger "news hole" than a print publication, I have still had to be very selective. Here are the best solutions.</p>
<h2>
	The Survey Form</h2>
<p>
	<strong>Dave Koelle </strong> from Raytheon Systems Company writes:</p>
<blockquote>
	The primary goal of users who will access the Pepper Library will be to access historical documents; additional <strong> indirect work, such as responding to a survey, may be glossed over or ignored </strong> . This is one of the reasons why collaborative filtering technologies, such as Firefly, were not as successful as anticipated.
	<p>
		Therefore, I think the survey should be as simple as possible, while giving the user the option to add more information. Perhaps the most important question (maybe, "How usable did you find this site?") should be a multiple choice question, or a scale of 1 through 10. Asking for an actual typed response requires too much work on the user's behalf. There should, however, be an optional textbox for the user to volunteer feedback (and this may be your most useful feedback). I think this arrangement will appeal to users, thereby providing you with the most responses.</p>
</blockquote>
<p>
	 </p>
<p>
	<strong>Jenna Burrell </strong> from Cornell University writes:</p>
<blockquote>
	I run a web site which provides links to educational resources for high school students. I've had a user survey which has gone through several rewrites since I set it up in 1995.
	<p>
		What I've found most useful based on the survey responses I've received is to <strong> ask what topic the user was looking for </strong> and whether or not they were able to find the desired information. Often users will suggest a topic I hadn't thought of including. Sometimes they will list a topic that I do have information on and then note that they were not able to find it. This helps me to rethink the organization of that specific subject, the keywords I used in the description of the relevant links, and where I placed the relevant links the user wasn't able to find.</p>
	<p>
		I also think it's important to include a textbox that allows users to fill in whatever questions or comments they have. It allows users to describe any problems they encountered and oftentimes it has helped me to think of new questions I want to ask in the survey.</p>
	<p>
		And, most importantly, <a class="new" href="../keep-online-surveys-short/index.php" title="Alertbox: Keep Online Surveys Short"> keep the survey short </a> , or no one will want to answer it.</p>
</blockquote>
<p>
	 </p>
<p>
	<strong>Jonathan Spencer </strong> from <a class="out" href="http://www.cyberartisans.com/" title="Company homepage"> CyberArtisans </a>  writes:</p>
<blockquote>
	Your survey must balance the desire to get the most information from each user with the tendency of most users not to fill out a form they don't have to fill out. To that end, I would suggest the following:
	<ul>
		<li>
			If you want a lot of information, offer your users something in return for filling out the survey form. Maybe a copy of the usability study, a printed copy of one of the documents or photographs, or something you feel comfortable distributing that you also think would be attractive to a user.</li>
		<li>
			You might want to make the survey in several stages, each stage offering some "goodie" in return, or maybe require that the user fill out all 3 or 4 stages to get the goodie.</li>
		<li>
			You might also create several sets of questions and provide them randomly, so that if someone happens to be sitting next to someone filling out the form, when they take the survey they will encounter different questions, thus forcing them to think rather than filling in what they remember the first person doing.</li>
		<li>
			In any case, the survey should not be long, so the user can fill it out quickly and move on. You will get many more responses.</li>
		<li>
			Arrange the site so the user fills out the survey immediately after using the site -- perhaps the user will encounter the survey after selecting the desired titles or abstracts and submitting the request.</li>
		<li>
			If you don't want to offer goodies, then you should make the survey very short, just a very few questions that the user can deal with quickly. Again, it can be done in stages, especially if your users tend to come back repeatedly.</li>
	</ul>
</blockquote>
<h2>
	Very Small Surveys</h2>
<p>
	<strong>John Arnold </strong> from Lex Vehicle Leasing writes:</p>
<blockquote>
	When it comes to gathering feedback I find that the times when I bother to respond to a question are the times when it least interrupts my information gathering.
	<p>
		I dislike the method Microsoft use on their support website - they have a question at the bottom of the page asking "did this page give you the information you were looking for?". The question is large and seems to be given as much importance as the page content. As an information seeker I'm far more interested in the content than in filling in a survey. Worse still, giving a response to their question takes you to another page - one without the original page content. Thus I lose the information I came for. Unacceptable.</p>
	<p>
		On the other hand, the idea of <strong> just asking a single question </strong> is good. No intrusive, privacy bashing questions about name, address, sex, income etc. Just find out what you really want to know "Did this site perform?".</p>
	<p>
		ZDNet and MSNBC among others often include a small survey question in a side bar on their pages. These really work for two reasons. Firstly, filling them in doesn't lose you the article information - the destination page includes the article again. Secondly, there is a reward for answering the question - you get to see the latest results. The second point is important. If there is a reward, however small, for answering the question then users will respond.</p>
	<p>
		Most important of all, though, is that the user came for information. Take that off their screen at your peril!</p>
	<p>
		Just as a quick technical solution - how about asking the question in a pop-up window? Give the results/reward in the same window. I am NOT proposing that each page load should automatically pop-up a small question window - that's REALLY annoying. There should be a button to open the question window and the results/reward page should have a clear CLOSE button to get rid of it again.</p>
</blockquote>
<h2>
	Don't Be Boring; Get Feedback on Live Alternatives</h2>
<p>
	Michael "Mac" McCarthy, President of Web Publishing Inc. writes:</p>
<blockquote>
	The biggest problem with reader surveys is that <strong> readers don't know what they want </strong> , can't articulate what they want, and articulate things different from what they actually do want based on how they later act when you give them what they thought they wanted.
	<p>
		Give people <strong> specific choices </strong> to look at, especially live choices, and see how they act, is a good way. We made a dramatic change in how the home page of <a class="out" href="http://www.javaworld.com/" title="Magazine home page"> JavaWorld </a>  was organized, and tested it by offering the new version to visitors to the old version: " <strong> Test our new home page and tell us what you think! </strong> " They clicked through to the new designed, then filled out a form saying whether they liked it or not; <strong> 94% said they preferred it strongly </strong> -- a forceful enough response to overwhelm our reservations.</p>
	<p>
		<strong>Long surveys aren't the problem, boring surveys are </strong> . For various reasons we tend to ask generalized questions, in boring academic phrasings, with dry "objective" choices. The solution is not to hide the length of the survey with multi-page surveys ("Next..."), which just makes you feel like you're on a treadmill.</p>
	<p>
		One solution is to at least sometimes <strong> ask an interesting question </strong> and give interesting choices. Be human, too.</p>
	<ul>
		<li>
			"I thought this story was.... a) great! b) pretty good; c) whatever; d) yawn; e) a waste of electrons; f) n.a. - not my kind of story, so who am I to judge? g) other (____)."</li>
		<li>
			Or give the background so I know why it matters: "We're stuck on a redesign -- give us a hand, will you?...</li>
		<li>
			"We're considering a redesign that would create three columns on the screen: The middle column would have the article, the left would contain navigation and house ads, the right column paid advertising. This would obviously squeeze the text into a narrower column than it's in now. Some of us think this great -- shorter scan lines supposedly make this stuff easier to read whether on the screen or printed out. Others think it makes our (already long) stories seem REALLY too long, especially when you print it out. Help us figure out which design works better by checking these two samples and then telling us how you like it."</li>
	</ul>
	<p>
		 </p>
	<p>
		We've gotten <strong> better response to folksy quizzes than formalistic ones </strong> -- and more essays in reply to open-ended questions, and the answers seem more useful. The worst thing about most surveys is once you've digested the responses and your most common reaction is "That's interesting - I wonder what they meant by <em> that </em> answer??"</p>
</blockquote>
<h2>
	Beyond Simple Surveys</h2>
<p>
	<strong>Susan Druding </strong> , webmaster of the <a class="out" href="http://www.fsm-a.org/" title="Archive homepage"> Free Speech Movement Archives </a>  writes:</p>
<blockquote>
	I'm not sure that [server log statistics are] the right approach to use. Until people access the information online you won't really know what their usage will be. Having people accessing the real material is the only way to see what they really focus on. I would suggest that you gear what you put on line and in what order on the basis of <strong> what people most frequently request in person in your real archives at Florida State </strong> . I think you would do better to study the access levels for the information that most seems to interest people and put that up first in descending order of requests at your Claude Pepper site.
	<p>
		I would strongly recommend that you set up some sort of "C. Pepper Archives Newsletter" on your site as soon as you open the site. Assure people that by signing up they will ONLY hear news of your archives and site and that you will not share the list with others. People DO sign up for these newsletters! You will be building a base of interested people to whom you can send site news and to whom you can send SHORT surveys or direct them to a SHORT online survey when you have one ready. We have a newsletter set up on the FSM-A site and receive a few a week. I have another Web site I do (on the art of <a class="out" href="http://quilting.miningco.com/" title="Mining Company 
Quilting site"> Quilting </a> ) and I've had an immensely good response setting up a newsletter. I have thousands who have signed up. I keep it short, no advertising and just focus on a few topics in each.</p>
</blockquote>
<p>
	 </p>
<p>
	<strong>Daniel Schwabe </strong> from PUC-Rio in Brazil writes:</p>
<blockquote>
	First, select users that could be considered representative of you target audience, if at all possible.
	<p>
		Then, ask the users to describe, as briefly as possible, the <strong> tasks they are trying to perform </strong> while accessing the collection. Together with the tasks, they should describe the navigation steps and interface operations (when not trivial) they took to achieve their goals. Then ask them to evaluate this sequence (adequate, inadequate), including both aspects. For those who feel the path is not adequate in some sense, or the interface inappropriate, they should give a brief explanation why. If they want, they may suggest an alternative they feel would be better suited for their task.</p>
	<p>
		The results of this survey would give you a feel for how well the intended set of tasks (as reported by the users) is supported by the site. There are several things that can be done with this result; we have developed a method whereby one can take these scenarios descriptions and actually synthesize a navigation design for the site. But I believe this would be already outside the scope of the original request.</p>
</blockquote>
<p>
	 </p>
<p>
	<strong>Adam C. Engst </strong> from <a class="out" href="http://www.tidbits.com/"> TidBITS </a>  writes:</p>
<blockquote>
	An on-the-spot survey regarding the <strong> usability of each section of the site </strong> might be useful. In essence, provide a means for users to rank each section of the site <em> within that section </em> rather than requiring them to complete a survey that's external to the content of the site. That takes advantage of the information being fresh in the users' minds, plus lowers the barriers to starting yet another task (the external survey). Obviously, the questions would have to be few in number and short, but I could also imagine an option for users to continue on with a more complete survey if they so desired.</blockquote>
<p>
	 </p>
<p>
	<strong>Ivan Handler </strong> from Automated Concepts, Inc. writes:</p>
<blockquote>
	I specialize in deploying "Knowledge Management" systems to industry. So far, these are primarily variations on document management which is really the service you are offering. In my experience with users both on and off the web, the most difficult issue to bridge is getting usable feedback from users. The primary reason for this is that when a user is accessing the document archive, it is because they have work to do. They get some kind of reward for their work (even if it is not getting screamed at , or not getting screamed at as much), what do they get by taking the time to fill out a "usability" questionnaire?
	<p>
		At least in a business, you will get negative feedback and plenty of it if the system is unusable, since then people will not be able to do their work. A public access web site will more often get ignored rather than getting actual feedback. While positive feedback is nice, the negative feedback is important, those are the users that will probably not come back unless something is done.</p>
	<p>
		My suggestion is based on the following ideas:</p>
	<ul>
		<li>
			make it very easy to provide feedback,</li>
		<li>
			provide some kind of feedback to the user (beside the automated thank you notes currently in vogue)</li>
		<li>
			demonstrate to the users that their input matters by showing how it actually provokes change</li>
		<li>
			quantify this system by measuring the number of different users who provide feedback and the number of actual changes that are made to your site as a result.</li>
	</ul>
	<p>
		A way to do this is to provide a simple suggestion box form that has a link on each page. Let the users tell you in simple or not so simple terms how you could make it easier for them to improve the site. Allow them to identify themselves with their email address. Then have an improvements page where you can document what has been changed recently, what is coming down the pike, what is being considered and what was rejected and why. I think that putting the suggestions related to a change is also a good idea (after you make sure that the user is open to the idea, or at least will allow the suggestion to be published anonymously). Also, when you actually consider a suggestion and decide to act on it or not, then send a note to the user.</p>
	<p>
		This kind of mechanism will take some time to have a noticeable effect. On the other hand, people who really care about this site will be drawn in to the site through this mechanism. They will spread the word and you should not only get more hits, you should get more involvement.</p>
	<p>
		Tracking the number of suggestions per time period over time should give you a nice increasing curve (unless the site is so spectacular that nobody has any suggestions, unlikely in my mind). While a nice academic study may allow the designer to publish a paper, I do not see it having much useful affect on your site.</p>
</blockquote>
<h2>
	A "Friends of the Site" User Group</h2>
<p>
	<strong>Bob Fabian </strong> from <a class="out" href="http://www.rfabian.com/" title="Company homepage"> Robert Fabian Associates </a>  writes:</p>
<blockquote>
	An alternative [to server stats] would be to use selective voting by users of the collection. An on-line "Friends of the Claude Pepper Library" could be established. Members should be chosen to represent the communities the Library is dedicated to serving. These "Friends" would have special access to the Library, and be encouraged to vote for those portions of the collection that should be first digitized.
	<p>
		The "Friends" would answer the question: How can the limited budget for digitizing be used to deliver the maximum value for the communities served by the Library?</p>
	<p>
		That's the question that the Library should be asking. And the on-line "Friends of the Claude Pepper Library" would provide compelling answers to that question. They would also be the natural reference group from which to get "usability" feedback. They would be an obvious source for suggested improvements. And they could be used to review alternate site designs.</p>
</blockquote>
<h2>
	User Registration or Profiles</h2>
<p>
	<strong>Linda G. Marsh </strong> from Compaq Computer Corporation writes:</p>
<blockquote>
	The best way to gather qualitative feedback is to ask your users, and, in order to do so, you must know who they are. Once you know who they are, you can ask them a few simple questions, such as:
	<ul>
		<li>
			Why did you access our site?</li>
		<li>
			Did you find what you needed?</li>
		<li>
			What did you like about the site? (This is important. You don't want to fix something that's working.)</li>
		<li>
			What did you dislike about the site?</li>
		<li>
			How would you improve the site?</li>
	</ul>
	<p>
		To satisfy my client's request to know who was using a Web-based course, we implemented a simple log in procedure. The procedure asked for a unique identifying number (e.g., employee badge, student ID or social security number) and the user's first and last name. Logging in was voluntary and users were allowed to bypass the login procedure if they wanted to. The log in procedure also explained why we were gathering data.</p>
	<p>
		On subsequent log in attempts, users only entered the identifying number. We didn't do any user validation, but interestingly, most users entered their correct names and identifying numbers. Relatively few chose to bypass the log in procedure and few entered bogus ID's. Once we had the list of users, we contacted them individually to solicit input about the course.</p>
	<p>
		You won't need to contact many users to learn what works about your site and what doesn't. After you've gathered enough data, you can discontinue the log in process.</p>
	<p>
		The above paradigm is simple, can be implemented quickly, and will give you the data you need in a short time.</p>
	<p>
		If you include a feedback option in your Web site, you probably won't get much information. Feedback pages are the equivalent of background noise on the Web.</p>
</blockquote>
<h2>
	Sidebars</h2>
<p>
	In addition to these suggestions for the problem of collecting user feedback, I also received many insightful comments on related issues. The best are available as sidebars to avoid making the main article even longer:</p>
<ul>
	<li>
		advice on the underlying design problem of <a class="new" href="../advice-designing-vast-web-archives/index.php" title="Alertbox sidebar: Web design for archives"> putting large archives on the Web </a></li>
	<li>
		report from a similar case study: collecting <a class="new" href="../collecting-user-feedback-from-a-beta-test-site/index.php" title="Alertbox sidebar: case study experience"> feedback from users of a commercial hotel site </a></li>
</ul>
<p>
	 </p>
</section>
  

  
  <section>
    
        
    
  </section>
  
  
  
  
  

  


  <section>
    <br />
    <p class="share"><b>Share <span class="hide-for-small-down">this article</span>:</b> <a href="https://twitter.com/intent/tweet?url=http://www.nngroup.com/articles/collecting-feedback-from-users-of-an-archive-reader-challenge/&amp;text=Collecting%20Feedback%20From%20Users%20of%20an%20Archive%20(Reader%20Challenge)&amp;via=nngroup">Twitter</a> | <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=http://www.nngroup.com/articles/collecting-feedback-from-users-of-an-archive-reader-challenge/&amp;title=Collecting%20Feedback%20From%20Users%20of%20an%20Archive%20(Reader%20Challenge)&amp;source=Nielsen%20Norman%20Group">LinkedIn</a> | <a href="https://plus.google.com/share?url=http://www.nngroup.com/articles/collecting-feedback-from-users-of-an-archive-reader-challenge/">Google+</a> | <a href="mailto:?subject=NN/g Article: Collecting Feedback From Users of an Archive (Reader Challenge)&amp;body=http://www.nngroup.com/articles/collecting-feedback-from-users-of-an-archive-reader-challenge/">Email</a></p>
    <hr />
  </section>

  <footer class="l-subsection">
    
    
    <div class="learn-more">
      <h2>Learn More</h2>
      <p><a href="../subscribe/index.php">Subscribe to the weekly newsletter</a> to get notified about future articles.</p>
      </div>
    
    

    <div class="row show-for-small-down related-content">
      
      
      

      
      
        <div class="column small-12 topic-links">All articles about:
          <ul>
            
            <li><a href="../../topic/research-methods/index.php">Research Methods</a></li>
            
          </ul>
        </div>
      
      

      
      
        <div class="column small-12">
          <h3>UX Conference Training Courses</h3>
           <ul class="no-bullet">
    
        <li><a href="../../courses/journey-mapping/index.php">Journey Mapping to Understand Customer Needs</a></li>
    
        <li><a href="../../courses/measuring-ux/index.php">Measuring User Experience</a></li>
    
        <li><a href="../../courses/analytics-and-user-experience/index.php">Analytics and User Experience</a></li>
    
        <li><a href="../../courses/usability-testing/index.php">Usability Testing</a></li>
    
        <li><a href="../../courses/personas/index.php">Personas: Turn User Data Into User-Centered Design</a></li>
    
</ul>
        </div>
      
      

      
      
        <div class="small-12 column">
          <h3>Research Reports</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../reports/user-experience-careers/index.php">User Experience Careers</a></li>
              
            
          </ul>
        </div>
      
      

      
      
      
    </div>

    <div class="hide-for-small-down row related-content">
      <div class="medium-6 columns">
        
        
          <div>
            <h3>Articles</h3>
            <ul class="no-bullet">
              
                
                <li><a href="../tips-user-research-field/index.php">27 Tips and Tricks for Conducting Successful User Research in the Field</a></li>
                
              
                
                <li><a href="../field-studies/index.php">Field Studies</a></li>
                
              
                
                <li><a href="../qualitative-surveys/index.php">28 Tips for Creating Great Qualitative Surveys</a></li>
                
              
                
                <li><a href="../open-ended-questions/index.php">Open-Ended vs. Closed-Ended Questions in User Research</a></li>
                
              
                
                <li><a href="../pilot-testing/index.php">Pilot Testing: Getting It Right (Before) the First Time</a></li>
                
              
            </ul>
          </div>
        
        

        
          
          <div>
            <h3>Research Reports</h3>
            <ul class="no-bullet">
              
                
                  <li><a href="../../reports/user-experience-careers/index.php">User Experience Careers</a></li>
                
              
            </ul>
          </div>
          
        

      </div>
      <div class="medium-6 columns">
        
          
          <div>
            <h3>UX Conference Training Courses</h3>
             <ul class="no-bullet">
    
        <li><a href="../../courses/journey-mapping/index.php">Journey Mapping to Understand Customer Needs</a></li>
    
        <li><a href="../../courses/measuring-ux/index.php">Measuring User Experience</a></li>
    
        <li><a href="../../courses/analytics-and-user-experience/index.php">Analytics and User Experience</a></li>
    
        <li><a href="../../courses/usability-testing/index.php">Usability Testing</a></li>
    
        <li><a href="../../courses/personas/index.php">Personas: Turn User Data Into User-Centered Design</a></li>
    
</ul>
          </div>
          
        
        
      </div>
    </div>
  </footer>
</article>

        </div>

      

    
  </div>
  <div class="row">
    
      <div class="column small-12 show-for-small-down l-navrail announcements">
        
          
        
      </div>
    
    
  </div>

    </div>
    




<footer id="page-footer">
    <div class="row hide-for-print">
        <div class="column small-12 medium-6">
            <section class="footer-popular">
                
                <h1>Popular Topics</h1>
                <div style="float: left;  width: 15em; display: block;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/applications/index.php">Application Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/e-commerce/index.php">E-Commerce Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/eyetracking/index.php">Eyetracking </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/information-architecture/index.php">Information Architecture </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/intranets/index.php">Intranet Design </a></li>
	<li><a href="../../topic/interaction-design/index.php">Interaction Design </a></li>
</ul>
</div>

<div style="float: left;  width: 15em; display: block; list-style: none;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/mobile-and-tablet-design/index.php">Mobile and Tablet Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/credibility-persuasion/index.php">Persuasive Design</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/research-methods/index.php">Research Methods</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/user-testing/index.php">User Testing </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/web-usability/index.php">Web Usability </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/writing-web/index.php">Web Writing &amp; Content Strategy </a></li>
</ul>
</div>

<p>&nbsp;</p>

                
            </section>
        </div>

        <div class="column small-12 medium-6">
            <div class="row">
                <div class="column small-6">
                    <section class="footer-about">
                        <h1>Follow Us</h1>
                        <ul>
	<li><a href="../subscribe/index.php">Subscribe to our weekly UX newsletter</a>,&nbsp;Jakob Nielsen&#39;s Alertbox</li>
	<li>Visit Don Norman&#39;s website, <a href="http://www.jnd.org/"> www.jnd.org </a></li>
	<li>Visit Bruce Tognazzini&#39;s website, <a href="http://www.asktog.com/"> www.asktog.com </a></li>
	<li style="padding-top: 6px;"><a class="twitter-follow-button" data-show-count="false" data-size="large" href="https://twitter.com/nngroup"><img alt="Follow @nngroup on Twitter" height="20" src="https://media.nngroup.com/media/editor/2016/12/30/twitter-follow-button.png" style="width: 117px; height: 20px;" width="117" /></a></li>
	<li style="padding-top: 6px;"><img alt="" height="14" src="https://media.nngroup.com/media/editor/2016/07/26/feed-icon-14x14.png" style="vertical-align:text-top;width:14px;margin-right:.25em;" width="14" /><a href="../../feed/rss/index.php">Subscribe to our RSS feed</a></li>
</ul>

                    </section>
                </div>

                <div class="column small-6">
                    <section class="footer-about right-column">
                        <h1>About
</h1>
                        <ul class="no-bullet">
	<li><a href="../../about/contact/index.php">Contact information</a></li>
	<li><a href="../../about/index.php">About Us</a></li>
	<li><a href="../../about/why-nng/index.php">Why NN/g? </a></li>
	<li><a href="../../return-policy/index.php">Return Policy</a></li>
</ul>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="copyright column small-12">
            <p><a href="../../copyright-and-reprint-info/index.php">Copyright</a> &copy; 1998-2017 Nielsen Norman Group, All Rights Reserved.</p>
        </div>
    </div>
</footer>


    


        <script src="https://media.nngroup.com/static/vendor/fastclick/js/fastclick.min.js"></script>
        <script src="https://media.nngroup.com/static/vendor/foundation/js/foundation.min.js"></script>
        <script src="http://media.nngroup.com/static/js/app.js"></script>
        <script src="http://media.nngroup.com/static/js/scroll-to-fixed.js"></script><script>var externalLinks = document.querySelectorAll('a[href^="http"]');for (var i = externalLinks.length-1; i >= 0; i--) {externalLinks[i].addEventListener("click", function(event) { alert("External links are disabled for this test."); event.preventDefault(); }, false);}var mailLinks = document.querySelectorAll('a[href^="mailto"]');for (var i = mailLinks.length-1; i >= 0; i--) {mailLinks[i].addEventListener("click", function(event) { alert("Email links are disabled for this test."); event.preventDefault(); }, false);}var externalForms = document.querySelectorAll('form');for (var i = externalForms.length-1; i >= 0; i--) {externalForms[i].addEventListener("submit", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false); externalForms[i].addEventListener("click", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false);}</script>

        <script>
            
            $(function() {
                $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
                $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
            });
        </script>

        
<script src="https://media.nngroup.com/static/js/articles.js"></script>


        
        <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1072699960;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072699960/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
    </body>

<!-- Mirrored from www.nngroup.com/articles/collecting-feedback-from-users-of-an-archive-reader-challenge/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:14:58 GMT -->
</html>
