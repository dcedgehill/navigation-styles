<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->





    
<!-- Mirrored from www.nngroup.com/articles/10-best-intranets-of-2008/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:13:56 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1026.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"licenseKey":"1a30ef9931","queueTime":3,"beacon":"bam.nr-data.net","errorBeacon":"bam.nr-data.net","applicationID":"2048992","transactionName":"NFxSMRBUX0EDUkIPDA0WdhAMVkVbDV8ZBxEXUFMJB0YfRAtUQRVZIktEDAFZVHYHRVcPD01eVRE=","applicationTime":291,"agent":""}</script>
        <title>10 Best Intranets of 2008</title><meta property="og:title" content="10 Best Intranets of 2008" />
  
        
        <meta name="description" content="Consistent design and integrated IA are becoming standard on good intranets. This year&#39;s winners focused on productivity tools, employee self-service, access to knowledgeable people (as opposed to &#39;knowledge management&#39;), and better-presented company news.">
        <meta property="og:description" content="Consistent design and integrated IA are becoming standard on good intranets. This year&#39;s winners focused on productivity tools, employee self-service, access to knowledgeable people (as opposed to &#39;knowledge management&#39;), and better-presented company news." />
        
  
        
	
        
        <meta name="keywords" content="intranet usability, design awards, Bank of America, BofA, Bankinter, Spain, Barnes &amp; Noble, British Airways, Campbell Soup Company, Coldwell Banker Real Estate Corporation, IKEA North America Service, Ministry of Transport, New Zealand, New South Wales Department of Primary Industries, Australia, SAP, Germany, Windows SQL Server, SharePoint, Google Search Appliance, Google Maps, Red Hat Linux, Lotus Notes, Lotus Domino, Oracle databases, Apache, Documentum, IBM WebSphere, Java 2 Enterprise Edition, J2EE, video, intranet branding, intranet name, naming, news, editorial review, editors, social distance, employee directory search, staff directory, staff directories, knowledge management, productivity, page views, ROI, personalization, executives, single sign-on, alerts">
        

        

        <meta name="robots" content="noarchive">
        <meta name="viewport" content="width=device-width">

        <link rel="search" type="application/opensearchdescription+xml" title="NN/g Search" href="../../opensearch.xml" />
        <link rel="shortcut icon" href="http://media.nngroup.com/static/img/favicon.ico" />

        <link rel="stylesheet" href="https://media.nngroup.com/static/vendor/font-awesome/css/font-awesome.min.css">

        
        <link rel="stylesheet" href="https://media.nngroup.com/static/css/7232a960f1df.css" type="text/css" />
        <link rel="stylesheet" href="http://fast.fonts.com/t/1.css?apiType=css&amp;projectid=c5846260-9def-46ec-9407-0f1349bd2d64" type="text/css">

        <script src="https://media.nngroup.com/static/vendor/modernizr/js/modernizr.min.js"></script>

        <script type="text/javascript" src="https://media.nngroup.com/static/js/3283be5def74.js"></script>
        

        

        
        
        <!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=229034,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('http://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->


<!-- Google Analytics Tracking Code for http://nngroup.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23132681-1', 'nngroup.com');
  ga('require', 'linkid', 'https://www.nngroup.com/articles/10-best-intranets-of-2008/linkid.js');
  ga('require', 'displayfeatures');

// Optimizely Universal Analytics Integration
 window.optimizely = window.optimizely || [];
 window.optimizely.push("activateUniversalAnalytics");

  ga('send', 'pageview');

</script>
<script>
/**
* Function to track a click on an outbound link in Google Analytics.
*/
function trackOutboundLink(label, url) {
    if(window.ga && ga.loaded) {
        ga('send', 'event', 'outbound', 'click', label, {'hitCallback': function(){document.location = url;}
        });
    } else {
        document.location = url;
    }
}
/**
* Function to track a click on a link in the article announcement area.
*/
$('#articleAnnouncement a').on('click', function(){
  ga('send', 'event', 'link', 'click', 'articleAnnouncement');
});
</script>

<script type="text/javascript">
 setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2421.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



<!-- Hotjar Tracking Code for http://nngroup.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:121605,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

        <script src="https://media.nngroup.com/static/js/analytics/track_outbound_link.js"></script>
        
        
        
    </head>
    <body class="location-articles articles-detail">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
    <header id="page-header">
        <div id="cart" class="shopping-cart mobile-cart show-for-small-down"></div>
        <div class="row">
            <div id="masthead" class="masthead small-12 medium-7 column">

                <!-- The button to toggles menu on small screen -->

                <h1><a href="../../index.php"><img src="http://media.nngroup.com/static/img/logo-wordmark.png" width="74" height="30"> Nielsen <span class="norman">Norman</span> Group</a></h1>
                <p class="tagline show-for-medium-up">Evidence-Based User Experience Research, Training, and Consulting</p>

                <!-- The button to toggles form search on small screen -->


                <div style="clear: right;width:  100%;height: 1px;"></div><?php define('ROOT_PATH',$_SERVER["DOCUMENT_ROOT"] . '/apps/navigation_styles/navigations/5'); ?><?php include(ROOT_PATH)."/navigation.php"; ?>

                <!-- Drop down menu on small screen -->
                <div id="hideNav" class="show-for-small-down">
                  
                  <ul class="menu-content">
                    <li><a href="../../index.php">Home</a></li>
                    <li class="has-submenu">
                      <a>Training</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../training/index.php">Overview</a></li>
                        
<li id="nav-ux-conference"><a href="../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../online-seminars/index.php"><span>Online Seminars</span></a></li>

                      </ul>
                    </li>
                    <li class="has-submenu">
                      <a>Consulting</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
  <li><a href="../../consulting/index.php">Overview</a></li>
  
  <li><a href="../../consulting/benchmarking/index.php">Benchmarking</a></li>
  
  <li><a href="../../consulting/expert-design-review/index.php">Expert Design Review</a></li>
  
  <li><a href="../../consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
  
  <li><a href="../../consulting/usability-testing/index.php">Usability Testing </a></li>
  
  <li><a href="../../consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
  
  <li><a href="../../in-house-training/index.php">In-House Training</a></li>
</ul>

                    </li>

                    <li><a href="../../reports/index.php">Reports</a></li>
                    <li><a href="../index.php">Articles</a></li>
                    <li class="has-submenu"><a>About NN/G</a><i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../about/index.php">Overview</a></li>
                        <li><a href="../../people/index.php">People</a></li>
                        <li><a href="../../about/why-nng/index.php">Why NN/g?</a></li>
                        <li><a href="../../about/contact/index.php">Contact</a></li>
                        <li><a href="../../news/index.php">News</a></li>
                        <li><a href="../../about/history/index.php">History</a></li>
                        <li><a href="../../books/index.php">Books</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                </div>

                <hr class="header-divider show-for-small-only"/>


            </div>
       </div>

        <div id="hideNav">            <nav class="primary-nav small-12 column">
                <ul>
                    <li><a class="navitem" id="nav-home" href="../../index.php">Home</a></li>
                    <li><a class="navitem" id="nav-training" href="../../training/index.php">Training</a></li>
                    <li><a class="navitem" id="nav-consulting" href="../../consulting/index.php">Consulting</a></li>
                    <li><a class="navitem" id="nav-reports" href="../../reports/index.php">Reports</a></li>
                    <li><a class="navitem" id="nav-articles" href="../index.php">Articles</a></li>
                    <li><a class="navitem" id="nav-about" href="../../about/index.php">About NN/G</a></li>
                </ul>
            </nav>
            <nav class="secondary-nav small-12 medium-12 large-12 columns">
                
                
            </nav>
        </div>
    </header>
    <div class="l-content"><span id="preventClose" style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776; Menu</span>
    
  

  

  <div class="row">
    
      
      <section class="small-12 medium-3 columns collapsable-content l-navrail articles-left-column">
        <h1 class="show-for-small-down collapse-link mobile-browse-header">Browse by Topic and Author</h1>
          <div class="row collapsable">

            
            <div class="small-6 medium-12 columns">
              <section id="article-popular-topics" style="display: none;">
                
                <h1>Topics</h1>
                <ul id="article-popular-topics">
                    
                    <li>
                        
                        <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/intranets/all/index.php">Intranets</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/user-testing/all/index.php">User Testing</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                        
                    </li>
                    
                    <li><a href="#" id="article-see-all-topics"><strong>See all topics&hellip;</strong></a></li>
                </ul>
                
              </section>
              <section id="article-all-topics" style="display: none;">
                
                <h1>
                  <span class="show-for-medium-up">All Article Topics</span>
                  <span class="show-for-small-down">All Topics</span>
                  (<a href="#" id="article-hide-all-topics">hide</a>)
                </h1>
                <ul>
                  
                  <li>
                    
                    <a href="../../topic/accessibility/all/index.php">Accessibility</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/agile/all/index.php">Agile</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/analytics-and-metrics/all/index.php">Analytics &amp; Metrics</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/applications/all/index.php">Application Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/b2b-websites/all/index.php">B2B Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/behavior-patterns/all/index.php">Behavior Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/branding/all/index.php">Branding</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/content-strategy/all/index.php">Content Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/corporate-websites/all/index.php">Corporate Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/design-patterns/all/index.php">Design Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-design-process/all/index.php">Design Process</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/email/all/index.php">Email</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/eyetracking/all/index.php">Eyetracking</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/heuristic-evaluation/all/index.php">Heuristic Evaluation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/human-computer-interaction/all/index.php">Human Computer Interaction</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-ideation/all/index.php">Ideation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/information-architecture/all/index.php">Information Architecture</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/interaction-design/all/index.php">Interaction Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/international-users/all/index.php">International Users</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/intranets/all/index.php">Intranets</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-management/all/index.php">Management</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/navigation/all/index.php">Navigation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/non-profit-websites/all/index.php">Non-Profit Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/personas/all/index.php">Personas</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/persuasive-design/all/index.php">Persuasive Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/prototyping/all/index.php">Prototyping</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/psychology-and-ux/all/index.php">Psychology and UX</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/research-methods/all/index.php">Research Methods</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/search/all/index.php">Search</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/social-media/all/index.php">Social Media</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/strategy/all/index.php">Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/user-testing/all/index.php">User Testing</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-teams/all/index.php">UX Teams</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/visual-design/all/index.php">Visual Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/writing-web/all/index.php">Writing for the Web</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/young-users/all/index.php">Young Users</a>
                    
                  </li>
                  
                </ul>
                
              </section>
            </div>
            <script>
              /* Determine which topics list will be displayed: the full or the
               * partial.  We are inlining this close to the list itself to
               * prevent rendering delays on the client
               */
              var topic_list = $.cookie("article-topics");

              var popular_topics = $('#article-popular-topics');
              var all_topics = $('#article-all-topics');

              if(topic_list == 1) {
                  popular_topics.hide();
                  all_topics.show();
              } else {
                  popular_topics.show();
                  all_topics.hide();
              }
            </script>
            

            
            <div class="small-6 medium-12 columns">
              
              <section id="article-initial-authors">
                  <h1>Author</h1>
                  <ul>
                    
                    <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                    
                    <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                    
                    <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                    
                    <li><a href="#" onClick="$('#article-initial-authors').hide(); $('#article-all-authors').show();"><strong>See all authors&hellip;</strong></a></li>
                  </ul>
              </section>
              <section id="article-all-authors" style="display: none;">
                <h1>
                  All Authors
                  (<a href="#" id="article-hide-all-authors">hide</a>)
                </h1>
                <ul>
                  
                  <li><a href="../author/raluca-budiu/index.php">Raluca Budiu</a></li>
                  
                  <li><a href="../author/patty-caya/index.php">Patty Caya</a></li>
                  
                  <li><a href="../author/susan-farrell/index.php">Susan Farrell</a></li>
                  
                  <li><a href="../author/therese-fessenden/index.php">Therese Fessenden</a></li>
                  
                  <li><a href="../author/kim-flaherty/index.php">Kim Flaherty</a></li>
                  
                  <li><a href="../author/sarah-gibbons/index.php">Sarah Gibbons</a></li>
                  
                  <li><a href="../author/aurora-harley/index.php">Aurora Harley</a></li>
                  
                  <li><a href="../author/kate-kaplan/index.php">Kate Kaplan</a></li>
                  
                  <li><a href="../author/page-laubheimer/index.php">Page Laubheimer</a></li>
                  
                  <li><a href="../author/angela-li/index.php">Angie Li</a></li>
                  
                  <li><a href="../author/hoa-loranger/index.php">Hoa Loranger</a></li>
                  
                  <li><a href="../author/kate-meyer/index.php">Kate Meyer</a></li>
                  
                  <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                  
                  <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                  
                  <li><a href="../author/kara-pernice/index.php">Kara Pernice</a></li>
                  
                  <li><a href="../author/christian-rohrer/index.php">Christian Rohrer</a></li>
                  
                  <li><a href="../author/amy-schade/index.php">Amy Schade</a></li>
                  
                  <li><a href="../author/katie-sherwin/index.php">Katie Sherwin</a></li>
                  
                  <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                  
                  <li><a href="../author/kathryn-whitenton/index.php">Kathryn Whitenton</a></li>
                  
                </ul>
              </section>
              
            </div>
            
          </div>

          
          <section class="hide-for-small-down">
            
              
              <h1>Recent Articles</h1>
              
              <ul>
                  
                  <li><a href="../seamless-cross-channel/index.php">Seamlessness in the Omnichannel User Experience</a></li>
                  
                  <li><a href="../brand-intention-interpretation/index.php">Experience Design: Bridging Brand Intention and Brand Interpretation</a></li>
                  
                  <li><a href="../eyetracking-tasks-efficient-scanning/index.php">Scanning Patterns on the Web Are Optimized for the Current Task</a></li>
                  
                  <li><a href="../flat-design-best-practices/index.php">Flat-Design Best Practices</a></li>
                  
                  <li><a href="../ux-user-stories/index.php">Accounting for UX Work with User Stories in Agile Projects</a></li>
                  
                  <li><a href="../index.php"><strong>See all articles&hellip;</strong></a></li>
              </ul>
              
            
          </section>
          

          
          
          <section class="hide-for-small-down">
            <h1>Popular Articles</h1>
            
            <ul>
                
                <li><a href="../usability-101-introduction-to-usability/index.php">Usability 101: Introduction to Usability</a></li>
                
                <li><a href="../top-10-mistakes-web-design/index.php">Top 10 Mistakes in Web Design</a></li>
                
                <li><a href="../how-users-read-on-the-web/index.php">How Users Read on the Web</a></li>
                
                <li><a href="../f-shaped-pattern-reading-web-content/index.php">F-Shaped Pattern For Reading Web Content</a></li>
                
                <li><a href="../ten-usability-heuristics/index.php">10 Usability Heuristics for User Interface Design</a></li>
                
                <li><a href="../intranet-design-2016/index.php">10 Best Intranets of 2016</a></li>
                
                <li><a href="../which-ux-research-methods/index.php">When to Use Which User-Experience Research Methods</a></li>
                
                <li><a href="../response-times-3-important-limits/index.php">Response Times: The 3 Important Limits</a></li>
                
                <li><a href="../why-you-only-need-to-test-with-5-users/index.php">Why You Only Need to Test with 5 Users</a></li>
                
                <li><a href="../page-fold-manifesto/index.php">The Fold Manifesto: Why the Page Fold Still Matters</a></li>
                
            </ul>
          </section>
          
          

          
            <section class="hide-for-small-down">
              
            </section>
          

      </section>
      

      
        <div class="small-12 medium-9 columns content-body">
          




<article class="l-subsection alertbox-article-content article-detail-bl">
  
  <header>
    <div class="l-subsection">
      <h1>10 Best Intranets of 2008</h1>
      <div class="author-meta">by
        
          
            
            <a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a>
            
          
        on  January 7, 2008
        
      </div>

      

      
      <div class="topic-links">
        Topics:
        
          <ul>

  <li><a href="../../topic/intranets/index.php">Intranets</a></li>

</ul>
        
      </div>
      
    </div>
  </header>
  

  
  
  <section>
    <p class="lede"><strong>Summary:</strong> Consistent design and integrated IA are becoming standard on good intranets. This year&#39;s winners focused on productivity tools, employee self-service, access to knowledgeable people (as opposed to &#39;knowledge management&#39;), and better-presented company news.</p>
    <br />
  </section>
  
  

  
  <section id="article-body"><p>
	The winners of the award for <strong> 10 best-designed intranets for 2008 </strong> are:</p>
<ul>
	<li>
		Bank of America, US</li>
	<li>
		Bankinter S.A., Spain</li>
	<li>
		Barnes &amp; Noble, US</li>
	<li>
		British Airways, UK</li>
	<li>
		Campbell Soup Company, US</li>
	<li>
		Coldwell Banker Real Estate Corporation, US</li>
	<li>
		IKEA North America Service, LLC, US</li>
	<li>
		Ministry of Transport, New Zealand</li>
	<li>
		New South Wales Department of Primary Industries, Australia</li>
	<li>
		SAP AG, Germany</li>
</ul>
<p>
	Most of the winning designs are traditional, company-wide intranets, but IKEA won for its regional intranet covering North America. Also, Coldwell Banker's intranet works somewhat like an extranet: it connects 3,800 independently owned and operated residential and commercial real estate offices, while appearing to users as a local office intranet rather than a corporate intranet.</p>
<p>
	Half of the winners are from the US, closely matching the nation's long-term performance average of 53%. The remaining five winners hail from five different countries. The southern hemisphere is strongly represented this year, including the first-ever winner from New Zealand. Australia has had many winners over the years, as have the UK and Germany. Spain seems to be an up-and-coming country in terms of quality intranets, collecting its third award this year (earlier awards went to Amadeus Global Travel Distribution in <a class="old" href="../ten-best-intranets-of-2003/index.php" title="Alertbox: 10 Best Intranets of 2003"> 2003</a> and Banco Español de Crédito [Banesto] in <a class="old" href="../10-best-intranets-of-2005/index.php" title="Alertbox: 10 Best Intranets of 2005"> 2005</a>).</p>
<p>
	In terms of industry sectors, the <a class="old" href="../../reports/best-financial-sector-intranets/index.php" title="Nielsen Norman Group report: Great Financial Sector Intranet Designs, 14 Case Studies of Award-Winning Intranets from Banks, Insurance Companies, Brokerage Companies, and Other Financial Service Organizations, Reprinted from the Intranet Design Annuals, 2001-2007"> financial sector</a> is strongly represented with three winners (two banks and one real estate company). This, too, follows tradition: earlier design annuals have typically had a disproportional number of winners from the financial sector ( <a class="old" href="../10-best-intranets-of-2007/index.php" title="Alertbox: 10 Best Intranets of 2007"> 2007</a> was an aberration, with only one financial winner, JPMorgan Chase).</p>
<p>
	Financial institutions probably have disproportionally good intranets for two reasons:</p>
<ul>
	<li>
		The companies tend to be big and have a lot of money resting on optimal performance. They therefore invest more heavily in IT than companies in most other sectors.</li>
	<li>
		The companies typically have a long tradition — often going back a decade or more — of taking usability seriously. After all, home banking is doomed unless the user experience is exceptionally approachable. Similarly, the financial sector deals in complex transactions, and training costs for internal applications can skyrocket if the design team fails to truly understand the needs of users in both local workgroups and remote branches. Intranets clearly benefit from the financial sector's above-average attention to user-centered design.</li>
</ul>
<p>
	Usually, the <a class="old" href="../../reports/best-technology-sector-intranets/index.php" title="Nielsen Norman Group report: Great Technology Sector Intranet Designs, 13 Case Studies of Award-Winning Intranets from Computer Companies, Independent Software Vendors (ISV), Value-Added Retailers (VAR), Technology Outsourcing Providers, Telecommunications Services, and Other High-Tech Organizations, Reprinted from the Intranet Design Annuals, 2001-2007"> technology sector</a> also produces many winners, since — obviously — its companies tend to have above-average sophistication in using technology. This year, however, the only technology winner is SAP.</p>
<p>
	Of course, it might be increasingly unreasonable to view technology companies as more sophisticated than other industries in the use of technology. This year, for example, the <a class="old" href="../../reports/best-retail-sector-intranets/index.php" title="Nielsen Norman Group report: Great Retail Sector Intranet Designs, 4 Case Studies of Award-Winning Intranets from Retail and E-Commerce Companies, Reprinted from the Intranet Design Annuals, 2002-2007"> retail sector</a> shines with two winners, beating its average performance as it becomes ever more tech-driven. Barnes &amp; Noble might be a bookstore, for example, but it's also a leading e-commerce site. And, as the case study of the B&amp;N intranet shows, the company certainly uses technology to the max.</p>
<p>
	As with the previous two years, most of this year's winners are big companies, employing an average of <strong> 50,000 employees</strong>. Still, this year's list includes the first small organization since 2005: New Zealand's Ministry of Transport, which has only 200 intranet users. Once again, we have proof that size isn't everything, and that a small but well-focused effort can produce a great intranet.</p>
<h2>
	Company News</h2>
<p>
	It's not exactly new for intranets to offer company or industry news. But this year, companies seem to be taking news much more seriously: most winning intranets give it <strong> major homepage real estate</strong>, and many invest significant resources in editing and maintaining their news areas.</p>
<p>
	The Campbell Soup Company has an interesting approach to news. Its intranet features an area across the top of the homepage with cells that represent the main business units and corporate functions, letting all users browse each unit's news. While this would be too much functionality for a smaller company's intranet, it can help manage the complexities of bigger companies — especially ones like Campbell that contain multiple strong brands.</p>
<p>
	Barnes &amp; Noble spends almost all of its homepage on various news areas. Among them is a large central area, <em> Barnes &amp; Noble Today</em>, that features stories from different stores and regions and helps build community among the widely dispersed bookstore staff. A smaller area lists <em> Store Alerts </em> that contain practical information, ranging from when a runaway bestseller will be in stock to recommendations for the gift fixture in bookstore cafés. The intranet team is committed to posting the <em> Store Alerts </em> by a certain time each day, which is critical to its success. News that isn't new gets old quickly, which is why a B&amp;N-like commitment to intranet news is necessary for it to truly attract traffic and interest employees.</p>
<p>
	Intranet <strong> multimedia </strong> use has been growing steadily during the last few years, and reached a new high this year. SAP dedicates an entire homepage section and intranet section to <em> SAP TV </em> , with videos on topics ranging from the SAP Cup soccer finals to doing business in Russia.</p>
<h2>
	Increasing Quality and Polish</h2>
<p>
	As with news, many of the winning intranets' other key features are also old favorites. The big distinction is that such features keep getting better and better. The quality level is high, which is appropriate given how many employees use basic intranet features on a daily basis. The productivity gains from polishing the user experience are well worth the cost of going beyond the first design that comes to mind.</p>
<p>
	Take, for example, the <strong> staff directory</strong>. This is a feature of virtually all intranets, and its <a class="new" href="../../reports/intranet-people-information/index.php" title="Nielsen Norman Group report: Intranet Usability Guidelines, vol. 5, Corporate Information, Teams, Departments, and People"> basic design guidelines</a> are well known. To go beyond the basics, Coldwell Banker emphasizes <strong> finding employees by geography </strong> — an enhancement that completely makes sense for a real estate company. Coldwell Banker's employee finder also includes a special <em> Referral </em> feature to help agents find colleagues for referral purposes. The directory's structure of tabs and labels emerged from usability research that revealed how company users think about seeking out their colleagues.</p>
<p>
	Most of the winning intranets have strong support for <strong> single sign-on </strong> , which we know from our <a class="new" href="../../reports/intranet/portals/index.php" title="Nielsen Norman Group report: Usability of Intranet Portals, A Report from the Trenches - Experiences From Real-Life Portal Projects"> studies of intranet portals</a> is a strong determinant of employee satisfaction and productivity — and yet something that's hard to achieve and rarely works as well as promised. This is definitely an area where the extra work to get it right improves the experience (and productivity) of users every day.</p>
<p>
	In general, <strong> integration </strong> was a strong theme: the impetus for many a winning redesign was to create a single <a class="old" href="../intranet-information-architecture-ia/index.php" title="Alertbox: Intranet IA"> intranet information architecture (IA)</a> with a consistent navigation scheme and consistent user experience. Such redesigns typically replaced hundreds of individual sites that lacked unified navigation and presented a highly inconsistent user experience. This chaotic state remains the norm on many intranets, which suffer reduced usability and lowered employee productivity as a result. In contrast, the new intranets are invariably based on <strong> content management systems </strong> with special interfaces designed to increase <strong> usability for content providers</strong>. The CMS interfaces also strongly emphasize the importance of sticking to a few templates with a standardized design.</p>
<p>
	In an effort to preserve their autonomy, individual departments sometimes resist the move toward a single, unified intranet design. The Campbell Soup Company provides a striking counter-example here. The company's employees are strongly attached to the specific brand they work for, which would seemingly doom any attempts to unify the intranet. However, the design team wisely provided ways for the intranet design to reinforce users' brand connections through customization and "skinning" (changing the appearance to reflect, for example, branded color schemes).</p>
<p>
	At Coldwell Banker, the company's franchise model resulted in the introduction of "co-mingling" rules in the CMS to blend corporate-level content with content from the local (franchise or office) level. This approach requires some added features, such as an extra CMS field for tagging the importance of each piece of corporate content so it could be appropriately prioritized relative to local content. However, the approach also creates a vastly more consistent (and thus productive) user experience than the free-for-all that's found in many other companies.</p>
<p>
	Updated content is a major contributor to intranet quality. The Department of Primary Industries brings two related trends together here. First, it increases usability for content providers to the limit, providing a tool that lets all users submit news items. This, of course, ties in with the second trend — to prioritize company news — because editors need a steady stream of news items from all organizational areas if they're to keep the news fresh and thus engaging.</p>
<h2>
	Productivity Focus</h2>
<p>
	Many of the most important features on the winning intranets directly support everyday work. At British Airways, <strong> employee self service </strong> is the intranet's main focus and the team backs it with a profusion of tools.</p>
<p>
	While advanced applications can boost productivity, smaller tools sometimes do the trick. For example, at the Department of Primary Industries, employees (such as mine safety inspectors) often need a department vehicle for offsite assignments. Historically, employees booked vehicles by calling their location's receptionist, who took the information over the phone and entered the details into a fleet management system. Now, a <strong> simple, one-page form </strong> serves the same purpose, saving time and increasing booking accuracy. (The same intranet also contains an advanced application for geospatial visualization, helping employees manage emergencies like floods, droughts, and exotic disease outbreaks. Thus proving the point that sometimes high tech is appropriate, and sometime it isn't.)</p>
<p>
	SAP's intranet offers an interesting twist on the productivity focus, providing special <strong> personalized pages for the company's Executive Board members</strong>. Top-level executives are busy, and their time is expensive, yet they're often among the least-trained users as their duties are focused elsewhere. It thus makes perfect sense to dedicate special attention to improving usability for this small but important group of users.</p>
<h2>
	Knowledge Management</h2>
<p>
	In previous years, "knowledge management" was a much-discussed buzzword for intranets. This year, most of the winners emphasized the same goals as the knowledge management movement, but with a rather different approach: they recognized that <strong> knowledge resides with people</strong>. As a result, many designs focused on improving access to the people who have the needed knowledge.</p>
<p>
	Bankinter probably had the most impressive expertise finder, with a profusion of graphical tools for visualizing the distribution and location of knowledge across employees.</p>
<h2>
	Diverse Technology Platforms</h2>
<p>
	The 10 winners used a total of <strong> 41 different products </strong> for their intranet technology platforms. As with every year, we again conclude that intranet technology is an unsettled field with no clear winner.</p>
<p>
	The <strong> most-used products </strong> were SharePoint and the Google Search Appliance. Other frequently used products were Red Hat Linux, Lotus Notes and Domino, and Oracle databases.</p>
<p>
	No single product made the list of most-used products for all of the four most recent Design Annuals (2005-2008). This simple fact reinforces the point that intranet platforms still have a long way to go. That said, the following products made the most-used lists <strong> more than once during this four-year period: </strong></p>
<ul>
	<li>
		<strong>3 of 4 </strong> years: Google Search Appliance, Microsoft SQL Server</li>
	<li>
		<strong>2 of 4 </strong> years: Apache, Documentum, IBM WebSphere, Java 2 Enterprise Edition (J2EE), Lotus Notes and Domino, Oracle databases, SharePoint</li>
</ul>
<p>
	In addition to these widely used intranet technologies, we constantly see new ones applied. For example, the Ministry of Transport is already using Microsoft's Silverlight technology to add interactivity to one of its intranet areas.</p>
<h2>
	Intranet Branding Gets a Lighter Touch</h2>
<p>
	In previous years, slightly more than half of the winning intranets have been branded to the extent that they had a separate name. A slightly smaller number of intranets have had no name, but were simply referred to as "the intranet" or some such.</p>
<p>
	This year's winners again include generic intranets and several traditionally branded intranets, with names like Discover, InSite, and Flagscape. However, we also have a prominent showing for a third option: give the intranet <strong> a name, but one that's not a strong brand </strong> in itself. Such names can borrow strength from the organization's main brand and typically include a plainspoken description of the intranet's function. These names include Employee Self Service, my Campbell, Coldwell Banker Works, and US Retail Inside.</p>
<p>
	Whether or not an intranet is branded, it needs <a class="new" href="../../reports/intranet-access/index.php" title="Nielsen Norman Group report: Intranet Usability Guidelines, vol. 2,  Address, Access, Homepage, Personalization, and Promotion"> internal marketing</a> to familiarize employees with its many features and new areas (particularly if it's a big intranet). Several winners this year had attractive and effective ways of promoting intranet features.</p>
<h2>
	Intranet Trends</h2>
<p>
	In addition to the issues discussed above, the following trends were apparent this year:</p>
<ul>
	<li>
		Increased <strong> personalization </strong></li>
	<li>
		<strong>Integration of information sources</strong>, often resulting in a single "one-stop shopping" page</li>
	<li>
		Emphasis on <strong> mission-critical applications </strong> and information (such as sales targets)</li>
	<li>
		Improved event and project <strong> calendars </strong></li>
	<li>
		Special sections to help orient <strong> new employees </strong></li>
	<li>
		Prominent display of <strong> stock quotes </strong> and other financial information</li>
	<li>
		<strong>Integration of external and company news</strong>, often in the form of customizable feeds</li>
	<li>
		Integration of <strong> alerts </strong> with the main intranet to inform users of important messages</li>
	<li>
		Redesigned and improved <strong> search </strong> features, which often went from horrible to good and generated ecstatic user feedback</li>
</ul>
<h2>
	ROI for Intranet Usability</h2>
<p>
	Once again, the winning teams focused their efforts on producing a great intranet and not on justifying their work by <a class="new" href="../../reports/usability-return-on-investment-roi/index.php" title="Nielsen Norman Group report: Usability Return on Investment"> measuring the return-on-investment (ROI)</a>. This makes sense in companies where the intranet team has the required executive support, and such support is certainly necessary in the long term. But teams in less ideal circumstances might still need ROI data, and at this point it's thin on the ground.</p>
<p>
	Bank of America collected detailed measurements of the <strong> time required to navigate </strong> from the homepage to 11 different intranet destinations. After the redesign, the average time <strong> decreased from 43.6 seconds to 21.7 seconds</strong>, cutting the navigation time in half. This corresponds to a <strong> 101% increase in post-redesign productivity</strong>, because users can get slightly more than twice as much done in the same time.</p>
<p>
	In our <a class="old" href="../intranet-usability-huge-advances/index.php" title="Alertbox: Intranet Usability Shows Huge Advances"> research testing a large number of intranets</a>, we found that the productivity gains from a major improvement in intranet usability were likely to be 72% on average. How does this research result square with the 101% increase measured at Bank of America? The difference in findings is easily resolved with two observations:</p>
<ul>
	<li>
		The 72% productivity increase is an <em> average</em>; some redesigns will experience smaller improvements, others higher.</li>
	<li>
		Bank of America has an award-winning intranet, so it stands to reason that its ROI will be higher than average.</li>
</ul>
<p>
	In addition to measuring ROI for intranet redesigns, it's also worth setting <strong> measurable goals </strong> in advance for what you want to achieve. British Airways is a good example, with goals such as getting a 75% increase in online training days and eventually having all staff travel booked online. In total, the BA redesign achieved <strong> cost savings of £55 million</strong>. But even before the results were known, it was obvious that the intranet project was big enough and had enough potential to be worth taking seriously.</p>
<p>
	At Campbell, the number of intranet <strong> visits per day increased by 727% </strong> after the redesign, but the number of actual <strong> pages viewed per visit decreased from 9.12 to 1.43 </strong> . As a result, the grand total number of <strong> page views only increased by 30%</strong>. Overly simplistic use of Web analytics might focus on this latter number and conclude that the redesign had been only a modest success. On the contrary, it's wildly successful, as the two other statistics show:</p>
<ul>
	<li>
		Employees get many <strong> more things done </strong> with the intranet, as shown by the 727% increase in visits.</li>
	<li>
		Employees are much more <strong> efficient each time they visit </strong> the intranet, corresponding to increased task productivity. Ideally, productivity is measured as time on task, which directly translates into the amount of work done per hour. We can't quite claim that the decrease in pages per visit corresponds to a 538% productivity increase because people might spend more time on each page. Still, there's no doubt that intranet efficiency — and thus employee productivity — increased immensely after the redesign consolidated, on personalized homepages, the information that users need.</li>
</ul>
<h2>
	Full Report</h2>
<p>
	<a class="new" href="../../reports/10-best-intranets-2008/index.php" title="Nielsen Norman Group report">362-page Intranet Design Annual with 212 screenshots</a> of the 10 winners for 2008 is available for download.</p>
<p>
	See also: <a class="new" href="../intranet-design/index.php" title="Nielsen Norman Group: report purchase and download page"> This year's Intranet Design Annual </a></p>
</section>
  

  
  <section>
    
        
    
  </section>
  
  
  
  
  

  


  <section>
    <br />
    <p class="share"><b>Share <span class="hide-for-small-down">this article</span>:</b> <a href="https://twitter.com/intent/tweet?url=http://www.nngroup.com/articles/10-best-intranets-of-2008/&amp;text=10%20Best%20Intranets%20of%202008&amp;via=nngroup">Twitter</a> | <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=http://www.nngroup.com/articles/10-best-intranets-of-2008/&amp;title=10%20Best%20Intranets%20of%202008&amp;source=Nielsen%20Norman%20Group">LinkedIn</a> | <a href="https://plus.google.com/share?url=http://www.nngroup.com/articles/10-best-intranets-of-2008/">Google+</a> | <a href="mailto:?subject=NN/g Article: 10 Best Intranets of 2008&amp;body=http://www.nngroup.com/articles/10-best-intranets-of-2008/">Email</a></p>
    <hr />
  </section>

  <footer class="l-subsection">
    
    
    <div class="learn-more">
      <h2>Learn More</h2>
      <p><a href="../subscribe/index.php">Subscribe to the weekly newsletter</a> to get notified about future articles.</p>
      </div>
    
    

    <div class="row show-for-small-down related-content">
      
      
      

      
      
        <div class="column small-12 topic-links">All articles about:
          <ul>
            
            <li><a href="../../topic/intranets/index.php">Intranets</a></li>
            
          </ul>
        </div>
      
      

      
      
      

      
      
        <div class="small-12 column">
          <h3>Research Reports</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../reports/intranet-design-annual/index.php">2017 Intranet Design Annual</a></li>
              
            
              
                <li><a href="../../reports/intranet-usability-guidelines/index.php">Intranet Usability Guidelines: Findings from User Testing of 42 Intranets</a></li>
              
            
              
                <li><a href="../../reports/intranet-portals-experiences-real-life-projects/index.php">Intranet Portals: UX Design Experience from Real-Life Projects</a></li>
              
            
              
                <li><a href="../../reports/intranet-information-architecture-design-methods/index.php">Intranet Information Architecture Design Methods and Case Studies</a></li>
              
            
              
                <li><a href="../../reports/best-merged-intranets/index.php">Mergers and Acquisitions, and the Resulting Intranets 2007-2016</a></li>
              
            
          </ul>
        </div>
      
      

      
      
        <div class="small-12 column">
          <h3>Online Seminars</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../online-seminars/improving-intranet-content/index.php">Improving Intranet Content</a></li>
              
            
              
                <li><a href="../../online-seminars/intranet-users-find/index.php">Helping Intranet Users Find What They Need</a></li>
              
            
          </ul>
        </div>
      
      
    </div>

    <div class="hide-for-small-down row related-content">
      <div class="medium-6 columns">
        
        
          <div>
            <h3>Articles</h3>
            <ul class="no-bullet">
              
                
                <li><a href="../intranet-design/index.php">10 Best Intranets of 2017</a></li>
                
              
                
                <li><a href="../top-intranet-design-mistakes/index.php">The Top Enduring Intranet-Design Mistakes: 7 Deadly Sins</a></li>
                
              
                
                <li><a href="../top-intranet-trends/index.php">Top 10 Intranet Trends of 2016</a></li>
                
              
                
                <li><a href="../intranet-content-authors/index.php">3 Ways to Inspire Intranet Content Authors</a></li>
                
              
                
                <li><a href="../sharepoint-intranet-ux/index.php">Design a Brilliant SharePoint Intranet</a></li>
                
              
            </ul>
          </div>
        
        

        
          
          <div>
            <h3>Research Reports</h3>
            <ul class="no-bullet">
              
                
                  <li><a href="../../reports/intranet-design-annual/index.php">2017 Intranet Design Annual</a></li>
                
              
                
                  <li><a href="../../reports/intranet-usability-guidelines/index.php">Intranet Usability Guidelines: Findings from User Testing of 42 Intranets</a></li>
                
              
                
                  <li><a href="../../reports/intranet-portals-experiences-real-life-projects/index.php">Intranet Portals: UX Design Experience from Real-Life Projects</a></li>
                
              
                
                  <li><a href="../../reports/intranet-information-architecture-design-methods/index.php">Intranet Information Architecture Design Methods and Case Studies</a></li>
                
              
                
                  <li><a href="../../reports/best-merged-intranets/index.php">Mergers and Acquisitions, and the Resulting Intranets 2007-2016</a></li>
                
              
            </ul>
          </div>
          
        

      </div>
      <div class="medium-6 columns">
        
        
          
          <div>
            <h3>Online Seminars</h3>
            <ul class="no-bullet">
            
                <li><a href="../../online-seminars/improving-intranet-content/index.php">Improving Intranet Content</a></li>
            
                <li><a href="../../online-seminars/intranet-users-find/index.php">Helping Intranet Users Find What They Need</a></li>
            
            </ul>
          </div>
          
        
      </div>
    </div>
  </footer>
</article>

        </div>

      

    
  </div>
  <div class="row">
    
      <div class="column small-12 show-for-small-down l-navrail announcements">
        
          
        
      </div>
    
    
  </div>

    </div>
    




<footer id="page-footer">
    <div class="row hide-for-print">
        <div class="column small-12 medium-6">
            <section class="footer-popular">
                
                <h1>Popular Topics</h1>
                <div style="float: left;  width: 15em; display: block;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/applications/index.php">Application Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/e-commerce/index.php">E-Commerce Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/eyetracking/index.php">Eyetracking </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/information-architecture/index.php">Information Architecture </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/intranets/index.php">Intranet Design </a></li>
	<li><a href="../../topic/interaction-design/index.php">Interaction Design </a></li>
</ul>
</div>

<div style="float: left;  width: 15em; display: block; list-style: none;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/mobile-and-tablet-design/index.php">Mobile and Tablet Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/credibility-persuasion/index.php">Persuasive Design</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/research-methods/index.php">Research Methods</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/user-testing/index.php">User Testing </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/web-usability/index.php">Web Usability </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/writing-web/index.php">Web Writing &amp; Content Strategy </a></li>
</ul>
</div>

<p>&nbsp;</p>

                
            </section>
        </div>

        <div class="column small-12 medium-6">
            <div class="row">
                <div class="column small-6">
                    <section class="footer-about">
                        <h1>Follow Us</h1>
                        <ul>
	<li><a href="../subscribe/index.php">Subscribe to our weekly UX newsletter</a>,&nbsp;Jakob Nielsen&#39;s Alertbox</li>
	<li>Visit Don Norman&#39;s website, <a href="http://www.jnd.org/"> www.jnd.org </a></li>
	<li>Visit Bruce Tognazzini&#39;s website, <a href="http://www.asktog.com/"> www.asktog.com </a></li>
	<li style="padding-top: 6px;"><a class="twitter-follow-button" data-show-count="false" data-size="large" href="https://twitter.com/nngroup"><img alt="Follow @nngroup on Twitter" height="20" src="https://media.nngroup.com/media/editor/2016/12/30/twitter-follow-button.png" style="width: 117px; height: 20px;" width="117" /></a></li>
	<li style="padding-top: 6px;"><img alt="" height="14" src="https://media.nngroup.com/media/editor/2016/07/26/feed-icon-14x14.png" style="vertical-align:text-top;width:14px;margin-right:.25em;" width="14" /><a href="../../feed/rss/index.php">Subscribe to our RSS feed</a></li>
</ul>

                    </section>
                </div>

                <div class="column small-6">
                    <section class="footer-about right-column">
                        <h1>About
</h1>
                        <ul class="no-bullet">
	<li><a href="../../about/contact/index.php">Contact information</a></li>
	<li><a href="../../about/index.php">About Us</a></li>
	<li><a href="../../about/why-nng/index.php">Why NN/g? </a></li>
	<li><a href="../../return-policy/index.php">Return Policy</a></li>
</ul>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="copyright column small-12">
            <p><a href="../../copyright-and-reprint-info/index.php">Copyright</a> &copy; 1998-2017 Nielsen Norman Group, All Rights Reserved.</p>
        </div>
    </div>
</footer>


    


        <script src="https://media.nngroup.com/static/vendor/fastclick/js/fastclick.min.js"></script>
        <script src="https://media.nngroup.com/static/vendor/foundation/js/foundation.min.js"></script>
        <script src="http://media.nngroup.com/static/js/app.js"></script>
        <script src="http://media.nngroup.com/static/js/scroll-to-fixed.js"></script><script>var externalLinks = document.querySelectorAll('a[href^="http"]');for (var i = externalLinks.length-1; i >= 0; i--) {externalLinks[i].addEventListener("click", function(event) { alert("External links are disabled for this test."); event.preventDefault(); }, false);}var mailLinks = document.querySelectorAll('a[href^="mailto"]');for (var i = mailLinks.length-1; i >= 0; i--) {mailLinks[i].addEventListener("click", function(event) { alert("Email links are disabled for this test."); event.preventDefault(); }, false);}var externalForms = document.querySelectorAll('form');for (var i = externalForms.length-1; i >= 0; i--) {externalForms[i].addEventListener("submit", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false); externalForms[i].addEventListener("click", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false);}</script>

        <script>
            
            $(function() {
                $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
                $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
            });
        </script>

        
<script src="https://media.nngroup.com/static/js/articles.js"></script>


        
        <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1072699960;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072699960/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
    </body>

<!-- Mirrored from www.nngroup.com/articles/10-best-intranets-of-2008/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:13:57 GMT -->
</html>
