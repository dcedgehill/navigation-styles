<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->





    
<!-- Mirrored from www.nngroup.com/articles/intranet-usability-huge-advances/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:14:04 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1026.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"beacon":"bam.nr-data.net","queueTime":4,"applicationTime":829,"agent":"","licenseKey":"1a30ef9931","errorBeacon":"bam.nr-data.net","applicationID":"2048992","transactionName":"NFxSMRBUX0EDUkIPDA0WdhAMVkVbDV8ZBxEXUFMJB0YfRAtUQRVZIktEDAFZVHYHRVcPD01eVRE="}</script>
        <title>Intranet Usability Shows Huge Advances</title><meta property="og:title" content="Intranet Usability Shows Huge Advances" />
  
        
        <meta name="description" content="Measured usability improved by 44% compared to our last large-scale intranet study. The new research identified 5 times the previous number of intranet design guidelines.">
        <meta property="og:description" content="Measured usability improved by 44% compared to our last large-scale intranet study. The new research identified 5 times the previous number of intranet design guidelines." />
        
  
        
	
        
        <meta name="keywords" content="intranet usability, intranet metrics, benchmarks, usability metrics, intranet design guidelines, trends, ROI">
        

        

        <meta name="robots" content="noarchive">
        <meta name="viewport" content="width=device-width">

        <link rel="search" type="application/opensearchdescription+xml" title="NN/g Search" href="../../opensearch.xml" />
        <link rel="shortcut icon" href="http://media.nngroup.com/static/img/favicon.ico" />

        <link rel="stylesheet" href="https://media.nngroup.com/static/vendor/font-awesome/css/font-awesome.min.css">

        
        <link rel="stylesheet" href="https://media.nngroup.com/static/css/7232a960f1df.css" type="text/css" />
        <link rel="stylesheet" href="http://fast.fonts.com/t/1.css?apiType=css&amp;projectid=c5846260-9def-46ec-9407-0f1349bd2d64" type="text/css">

        <script src="https://media.nngroup.com/static/vendor/modernizr/js/modernizr.min.js"></script>

        <script type="text/javascript" src="https://media.nngroup.com/static/js/3283be5def74.js"></script>
        

        

        
        
        <!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=229034,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('http://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->


<!-- Google Analytics Tracking Code for http://nngroup.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23132681-1', 'nngroup.com');
  ga('require', 'linkid', 'https://www.nngroup.com/articles/intranet-usability-huge-advances/linkid.js');
  ga('require', 'displayfeatures');

// Optimizely Universal Analytics Integration
 window.optimizely = window.optimizely || [];
 window.optimizely.push("activateUniversalAnalytics");

  ga('send', 'pageview');

</script>
<script>
/**
* Function to track a click on an outbound link in Google Analytics.
*/
function trackOutboundLink(label, url) {
    if(window.ga && ga.loaded) {
        ga('send', 'event', 'outbound', 'click', label, {'hitCallback': function(){document.location = url;}
        });
    } else {
        document.location = url;
    }
}
/**
* Function to track a click on a link in the article announcement area.
*/
$('#articleAnnouncement a').on('click', function(){
  ga('send', 'event', 'link', 'click', 'articleAnnouncement');
});
</script>

<script type="text/javascript">
 setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2421.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



<!-- Hotjar Tracking Code for http://nngroup.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:121605,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

        <script src="https://media.nngroup.com/static/js/analytics/track_outbound_link.js"></script>
        
        
        
    </head>
    <body class="location-articles articles-detail">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
    <header id="page-header">
        <div id="cart" class="shopping-cart mobile-cart show-for-small-down"></div>
        <div class="row">
            <div id="masthead" class="masthead small-12 medium-7 column">

                <!-- The button to toggles menu on small screen -->

                <h1><a href="../../index.php"><img src="http://media.nngroup.com/static/img/logo-wordmark.png" width="74" height="30"> Nielsen <span class="norman">Norman</span> Group</a></h1>
                <p class="tagline show-for-medium-up">Evidence-Based User Experience Research, Training, and Consulting</p>

                <!-- The button to toggles form search on small screen -->


                <div style="clear: right;width:  100%;height: 1px;"></div><?php define('ROOT_PATH',$_SERVER["DOCUMENT_ROOT"] . '/apps/navigation_styles/navigations/5'); ?><?php include(ROOT_PATH)."/navigation.php"; ?>

                <!-- Drop down menu on small screen -->
                <div id="hideNav" class="show-for-small-down">
                  
                  <ul class="menu-content">
                    <li><a href="../../index.php">Home</a></li>
                    <li class="has-submenu">
                      <a>Training</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../training/index.php">Overview</a></li>
                        
<li id="nav-ux-conference"><a href="../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../online-seminars/index.php"><span>Online Seminars</span></a></li>

                      </ul>
                    </li>
                    <li class="has-submenu">
                      <a>Consulting</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
  <li><a href="../../consulting/index.php">Overview</a></li>
  
  <li><a href="../../consulting/benchmarking/index.php">Benchmarking</a></li>
  
  <li><a href="../../consulting/expert-design-review/index.php">Expert Design Review</a></li>
  
  <li><a href="../../consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
  
  <li><a href="../../consulting/usability-testing/index.php">Usability Testing </a></li>
  
  <li><a href="../../consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
  
  <li><a href="../../in-house-training/index.php">In-House Training</a></li>
</ul>

                    </li>

                    <li><a href="../../reports/index.php">Reports</a></li>
                    <li><a href="../index.php">Articles</a></li>
                    <li class="has-submenu"><a>About NN/G</a><i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../about/index.php">Overview</a></li>
                        <li><a href="../../people/index.php">People</a></li>
                        <li><a href="../../about/why-nng/index.php">Why NN/g?</a></li>
                        <li><a href="../../about/contact/index.php">Contact</a></li>
                        <li><a href="../../news/index.php">News</a></li>
                        <li><a href="../../about/history/index.php">History</a></li>
                        <li><a href="../../books/index.php">Books</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                </div>

                <hr class="header-divider show-for-small-only"/>


            </div>
       </div>

        <div id="hideNav">            <nav class="primary-nav small-12 column">
                <ul>
                    <li><a class="navitem" id="nav-home" href="../../index.php">Home</a></li>
                    <li><a class="navitem" id="nav-training" href="../../training/index.php">Training</a></li>
                    <li><a class="navitem" id="nav-consulting" href="../../consulting/index.php">Consulting</a></li>
                    <li><a class="navitem" id="nav-reports" href="../../reports/index.php">Reports</a></li>
                    <li><a class="navitem" id="nav-articles" href="../index.php">Articles</a></li>
                    <li><a class="navitem" id="nav-about" href="../../about/index.php">About NN/G</a></li>
                </ul>
            </nav>
            <nav class="secondary-nav small-12 medium-12 large-12 columns">
                
                
            </nav>
        </div>
    </header>
    <div class="l-content"><span id="preventClose" style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776; Menu</span>
    
  

  

  <div class="row">
    
      
      <section class="small-12 medium-3 columns collapsable-content l-navrail articles-left-column">
        <h1 class="show-for-small-down collapse-link mobile-browse-header">Browse by Topic and Author</h1>
          <div class="row collapsable">

            
            <div class="small-6 medium-12 columns">
              <section id="article-popular-topics" style="display: none;">
                
                <h1>Topics</h1>
                <ul id="article-popular-topics">
                    
                    <li>
                        
                        <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/intranets/all/index.php">Intranets</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/user-testing/all/index.php">User Testing</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                        
                    </li>
                    
                    <li><a href="#" id="article-see-all-topics"><strong>See all topics&hellip;</strong></a></li>
                </ul>
                
              </section>
              <section id="article-all-topics" style="display: none;">
                
                <h1>
                  <span class="show-for-medium-up">All Article Topics</span>
                  <span class="show-for-small-down">All Topics</span>
                  (<a href="#" id="article-hide-all-topics">hide</a>)
                </h1>
                <ul>
                  
                  <li>
                    
                    <a href="../../topic/accessibility/all/index.php">Accessibility</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/agile/all/index.php">Agile</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/analytics-and-metrics/all/index.php">Analytics &amp; Metrics</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/applications/all/index.php">Application Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/b2b-websites/all/index.php">B2B Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/behavior-patterns/all/index.php">Behavior Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/branding/all/index.php">Branding</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/content-strategy/all/index.php">Content Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/corporate-websites/all/index.php">Corporate Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/design-patterns/all/index.php">Design Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-design-process/all/index.php">Design Process</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/email/all/index.php">Email</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/eyetracking/all/index.php">Eyetracking</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/heuristic-evaluation/all/index.php">Heuristic Evaluation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/human-computer-interaction/all/index.php">Human Computer Interaction</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-ideation/all/index.php">Ideation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/information-architecture/all/index.php">Information Architecture</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/interaction-design/all/index.php">Interaction Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/international-users/all/index.php">International Users</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/intranets/all/index.php">Intranets</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-management/all/index.php">Management</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/navigation/all/index.php">Navigation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/non-profit-websites/all/index.php">Non-Profit Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/personas/all/index.php">Personas</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/persuasive-design/all/index.php">Persuasive Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/prototyping/all/index.php">Prototyping</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/psychology-and-ux/all/index.php">Psychology and UX</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/research-methods/all/index.php">Research Methods</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/search/all/index.php">Search</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/social-media/all/index.php">Social Media</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/strategy/all/index.php">Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/user-testing/all/index.php">User Testing</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-teams/all/index.php">UX Teams</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/visual-design/all/index.php">Visual Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/writing-web/all/index.php">Writing for the Web</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/young-users/all/index.php">Young Users</a>
                    
                  </li>
                  
                </ul>
                
              </section>
            </div>
            <script>
              /* Determine which topics list will be displayed: the full or the
               * partial.  We are inlining this close to the list itself to
               * prevent rendering delays on the client
               */
              var topic_list = $.cookie("article-topics");

              var popular_topics = $('#article-popular-topics');
              var all_topics = $('#article-all-topics');

              if(topic_list == 1) {
                  popular_topics.hide();
                  all_topics.show();
              } else {
                  popular_topics.show();
                  all_topics.hide();
              }
            </script>
            

            
            <div class="small-6 medium-12 columns">
              
              <section id="article-initial-authors">
                  <h1>Author</h1>
                  <ul>
                    
                    <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                    
                    <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                    
                    <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                    
                    <li><a href="#" onClick="$('#article-initial-authors').hide(); $('#article-all-authors').show();"><strong>See all authors&hellip;</strong></a></li>
                  </ul>
              </section>
              <section id="article-all-authors" style="display: none;">
                <h1>
                  All Authors
                  (<a href="#" id="article-hide-all-authors">hide</a>)
                </h1>
                <ul>
                  
                  <li><a href="../author/raluca-budiu/index.php">Raluca Budiu</a></li>
                  
                  <li><a href="../author/patty-caya/index.php">Patty Caya</a></li>
                  
                  <li><a href="../author/susan-farrell/index.php">Susan Farrell</a></li>
                  
                  <li><a href="../author/therese-fessenden/index.php">Therese Fessenden</a></li>
                  
                  <li><a href="../author/kim-flaherty/index.php">Kim Flaherty</a></li>
                  
                  <li><a href="../author/sarah-gibbons/index.php">Sarah Gibbons</a></li>
                  
                  <li><a href="../author/aurora-harley/index.php">Aurora Harley</a></li>
                  
                  <li><a href="../author/kate-kaplan/index.php">Kate Kaplan</a></li>
                  
                  <li><a href="../author/page-laubheimer/index.php">Page Laubheimer</a></li>
                  
                  <li><a href="../author/angela-li/index.php">Angie Li</a></li>
                  
                  <li><a href="../author/hoa-loranger/index.php">Hoa Loranger</a></li>
                  
                  <li><a href="../author/kate-meyer/index.php">Kate Meyer</a></li>
                  
                  <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                  
                  <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                  
                  <li><a href="../author/kara-pernice/index.php">Kara Pernice</a></li>
                  
                  <li><a href="../author/christian-rohrer/index.php">Christian Rohrer</a></li>
                  
                  <li><a href="../author/amy-schade/index.php">Amy Schade</a></li>
                  
                  <li><a href="../author/katie-sherwin/index.php">Katie Sherwin</a></li>
                  
                  <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                  
                  <li><a href="../author/kathryn-whitenton/index.php">Kathryn Whitenton</a></li>
                  
                </ul>
              </section>
              
            </div>
            
          </div>

          
          <section class="hide-for-small-down">
            
              
              <h1>Recent Articles</h1>
              
              <ul>
                  
                  <li><a href="../seamless-cross-channel/index.php">Seamlessness in the Omnichannel User Experience</a></li>
                  
                  <li><a href="../brand-intention-interpretation/index.php">Experience Design: Bridging Brand Intention and Brand Interpretation</a></li>
                  
                  <li><a href="../eyetracking-tasks-efficient-scanning/index.php">Scanning Patterns on the Web Are Optimized for the Current Task</a></li>
                  
                  <li><a href="../flat-design-best-practices/index.php">Flat-Design Best Practices</a></li>
                  
                  <li><a href="../ux-user-stories/index.php">Accounting for UX Work with User Stories in Agile Projects</a></li>
                  
                  <li><a href="../index.php"><strong>See all articles&hellip;</strong></a></li>
              </ul>
              
            
          </section>
          

          
          
          <section class="hide-for-small-down">
            <h1>Popular Articles</h1>
            
            <ul>
                
                <li><a href="../usability-101-introduction-to-usability/index.php">Usability 101: Introduction to Usability</a></li>
                
                <li><a href="../top-10-mistakes-web-design/index.php">Top 10 Mistakes in Web Design</a></li>
                
                <li><a href="../how-users-read-on-the-web/index.php">How Users Read on the Web</a></li>
                
                <li><a href="../f-shaped-pattern-reading-web-content/index.php">F-Shaped Pattern For Reading Web Content</a></li>
                
                <li><a href="../ten-usability-heuristics/index.php">10 Usability Heuristics for User Interface Design</a></li>
                
                <li><a href="../intranet-design-2016/index.php">10 Best Intranets of 2016</a></li>
                
                <li><a href="../which-ux-research-methods/index.php">When to Use Which User-Experience Research Methods</a></li>
                
                <li><a href="../response-times-3-important-limits/index.php">Response Times: The 3 Important Limits</a></li>
                
                <li><a href="../why-you-only-need-to-test-with-5-users/index.php">Why You Only Need to Test with 5 Users</a></li>
                
                <li><a href="../page-fold-manifesto/index.php">The Fold Manifesto: Why the Page Fold Still Matters</a></li>
                
            </ul>
          </section>
          
          

          
            <section class="hide-for-small-down">
              
            </section>
          

      </section>
      

      
        <div class="small-12 medium-9 columns content-body">
          




<article class="l-subsection alertbox-article-content article-detail-bl">
  
  <header>
    <div class="l-subsection">
      <h1>Intranet Usability Shows Huge Advances</h1>
      <div class="author-meta">by
        
          
            
            <a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a>
            
          
        on  October 9, 2007
        
      </div>

      

      
      <div class="topic-links">
        Topics:
        
          <ul>

  <li><a href="../../topic/intranets/index.php">Intranets</a></li>

</ul>
        
      </div>
      
    </div>
  </header>
  

  
  
  <section>
    <p class="lede"><strong>Summary:</strong> Measured usability improved by 44% compared to our last large-scale intranet study. The new research identified 5 times the previous number of intranet design guidelines.</p>
    <br />
  </section>
  
  

  
  <section id="article-body"><p>
	During the few years between our two rounds of intranet testing, the world experienced a dramatic improvement in intranet usability.</p>
<p>
	Our simplest usability metric is <a class="old" href="../success-rate-the-simplest-usability-metric/index.php" title="Alertbox: Success Rate, The Simplest Usability Metric"> success rate</a>, which measures whether users can complete their tasks with the user interface. In the <a class="old" href="../intranet-usability-the-trillion-dollar-question/index.php" title="Alertbox: Intranet Usability, The Trillion-Dollar Question"> first study</a>, the average success rate was <strong> 74%</strong>; in the second study, it was <strong> 80%</strong>. An increase from 74% to 80% might not seem very big, but that's because the first study's success rate was already fairly high, and thus it was hard to increase it further.</p>
<p>
	Our public Internet tests have shown dramatically lower success rates than what we've found on intranets. In our book, <cite> <a class="old" href="../../books/prioritizing-web-usability/index.php" title="Book by Jakob Nielsen and Hoa Loranger"> Prioritizing Web Usability</a></cite>, we present detailed results of a study in which we asked users to perform tasks on the Web without first taking them to any particular website. Such a task tests how people typically use the Web, and it had a success rate of only 60%.</p>
<p>
	Intranet success rates are typically <strong> about 33% higher than the Web </strong> for two reasons:</p>
<ul>
	<li>
		There is <strong> no doubt about where to go</strong>. The company's intranet is the company's intranet — users don't have to worry whether they've reached a fraudulent site or a low-credibility source the way they do on the Web. The first problem in using the Web is to find a good website for your current question; intranets don't present this obstacle. (Yes, users have to find their way around their intranet, but that's equivalent to <em> navigating </em> a website, not to <em> finding </em> a website and <em> judging </em> whether it's going to cheat you.)</li>
	<li>
		Employees <strong> experience with their company's intranet continually increases </strong> because there's only one site. In contrast, users <a class="old" href="../users-interleave-sites-and-genres/index.php" title="Alertbox: Users Interleave Sites and Genres"> flitter among websites</a> and don't spend much time on any given site. As a result, people rarely learn the intricacies of an individual website's interface and are thus more easily baffled.</li>
</ul>
<p>
	The success rate <strong> variability </strong> among intranets was substantially smaller in the second study than in the first. The standard deviation relative to the mean was <strong> 15% </strong> for the first study, but only <strong> 8% </strong> for the second study. In other words, according to this common variability measure, <strong> intranet variability was cut almost in half</strong>. This further indicates the <strong> benefits of intranet usability guidelines</strong>.</p>
<p>
	Our first study was conducted before any intranet guidelines were available. (Indeed, the purpose of this first research was to identify the first set of intranet usability guidelines.) Without such guidelines, designers had to rely solely on local observations of their own intranet and couldn't benefit from usability findings from other intranets. Given such scant usability knowledge, design decisions were highly variable.</p>
<p>
	In contrast, we conducted the second study after we'd published general intranet usability guidelines in the report from our first round of research. Working from this broader knowledge base smoothed over the differences in the information available within each company. Of course, there was still some variability left because the resources allocated to intranet design teams varied and different design decisions had different effects.</p>
<p>
	Overall, however, <strong> the more we know about intranet usability, the less intranet design is a matter of guesswork</strong>. Hopefully, the publication of our new report edition, with much more detailed findings and additional guidelines, will further reduce the variability in intranet quality.</p>
<h2>
	Intranet User Research</h2>
<p>
	We conducted two rounds of international research into intranet usability, running <strong> 28 user tests with employees at 27 companies </strong> (we tested one company's intranet twice: once at its U.S. headquarters, and once in another country to collect information about international usability for multinational intranets).</p>
<p>
	We conducted our tests in the U.S. (20 tests), Europe (6 tests), Asia (1 test), and Canada (1 test). Our <a class="old" href="../intranet-usability-the-trillion-dollar-question/index.php" title="Alertbox: Intranet Usability, The Trillion-Dollar Question"> first round of research</a> in 2002 focused on traditional lab-based usability testing. The second round of research repeated most of the lab-based test tasks from Study 1, added new tasks to account for recent changes in intranet use, and also included field studies.</p>
<p>
	In all cases, we conducted the user research on-site at the company or organization whose intranet was being studied, using representative employees as the participants. In the <strong> lab-based sessions </strong> , we asked users to perform a series of standardized tasks that spanned a range of common intranet use cases. Assigning prepared tasks allowed us to compare usability performance across intranets and to assess how usability had changed during the years between Study 1 and Study 2.</p>
<p>
	For the <strong> field study sessions </strong> , we observed representative employees as they went about their normal work. We interfered as little as possible and didn't assign them any specific tasks. During these sessions, people at different companies obviously did very different things with the intranets, making formal comparisons difficult. But field studies are still invaluable; they show us a wider range of behaviors than we see in the laboratory and let us assess the impact of changing contexts and other ethnographic variables.</p>
<h2>
	Increased Knowledge about Intranet Usability</h2>
<p>
	The following table compares the <a class="new" href="../../reports/intranet/guidelines/index.php" title="Nielsen Norman Group report series: list of the 10 reports and their tables of content"> reports with intranet usability guidelines</a> that we published as a result of our two rounds of research:</p>
<table align="center" style="border-style: solid; border-color: #cccccc; border-width: 1px; border-collapse: collapse;">
	<tbody>
		<tr>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left">
				 </td>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: center">
				<strong>First Edition </strong></td>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: center">
				<strong>Second Edition </strong></td>
		</tr>
		<tr>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left">
				<strong>Intranets </strong> tested</td>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: center">
				14</td>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: center">
				27 (*)</td>
		</tr>
		<tr>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left">
				<strong>Pages </strong> in the report</td>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: center">
				231</td>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: center">
				1,160</td>
		</tr>
		<tr>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left">
				Design <strong> guidelines </strong></td>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: center">
				111</td>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: center">
				614</td>
		</tr>
		<tr>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left">
				<strong>Screenshots </strong></td>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: center">
				164</td>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: center">
				701</td>
		</tr>
	</tbody>
</table>
<p>
	(*) One company's intranet was tested in two countries to collect additional information about international usability of a multinational intranet. Thus, we have data from 28 tests at 27 organizations. [Update: note that the <a href="../../reports/intranet-usability-guidelines/index.php">3rd edition of intranet research</a> now covers <strong>42</strong> intranets.]</p>
<p>
	Considering that we only doubled our research, why do we have <strong> 5 times more usability findings </strong> in the report's second edition? Several reasons:</p>
<ul>
	<li>
		In addition to the 28 intranet tests that we can publicly discuss, we've studied many more intranets for our consulting clients. We've also <strong> analyzed more than 1,000 intranet designs </strong> as part of our series of Intranet Design Annuals. While the specifics of these additional intranets must remain confidential, we draw upon the many general lessons gained from studying them.</li>
	<li>
		Study 1 was the <strong> first project in history </strong> to systematically test intranet usability across many intranets with the goal of deriving generalized usability guidelines. Because of the lack of historical precedence, this initial research round was more difficult to plan and perform, and we had to focus on identifying the biggest intranet usability issues. In contrast, we can now <strong> build on a much firmer foundation</strong>, making it easier for us to identify and document a broader range of issues. Having the conceptual framework in place makes it easier to see things in a research study.</li>
	<li>
		For Study 2, the only thing we had to do in relation to Study 1's findings was to check whether these issues still constituted usability problems. This left most of the <strong> project resources available to identify new issues</strong>.</li>
	<li>
		We added <strong> field studies </strong> in Study 2. Although less structured than lab testing, field studies give broader insights into users' behavior and contextual needs.</li>
	<li>
		Intranets now have many <strong> more components and features </strong> than they did during our first study. With more features to study, there are more usability issues to be found.</li>
	<li>
		Intranets now have better usability. Users can therefore <strong> progress further and attempt more advanced behaviors</strong>. All for the good, but as people try to accomplish more, they encounter additional usability issues that were not as prominent in the past when users got stumped by more blatant usability problems.</li>
</ul>
<h2>
	Improvements in Intranet Usability Metrics</h2>
<p>
	Out of the 18 common intranet tasks we tested, 11 were repeated from Study 1 to Study 2. This fact lets us estimate the change in user productivity across the studies.</p>
<p>
	The following chart shows the trends for intranets at three usability levels:</p>
<ul>
	<li>
		<strong>Q1 </strong> (the first quartile): the task time that separates the best 25% of intranets from the worst 75%. This number is an estimate of the time needed to perform the tasks on an intranet with good usability.</li>
	<li>
		<strong>Median </strong> : the task time at which 50% of intranets are better and 50% are worse. This number is an estimate of the time needed to perform the tasks on an intranet with average usability.</li>
	<li>
		<strong>Q3 </strong> (the third quartile): the task time that separates the best 75% of intranets from the worst 25%. This number is an estimate of the time needed to perform the tasks on an intranet with poor usability.</li>
</ul>
<div style=" width: 600px; margin-left: auto; margin-right: auto; ">
	<img alt="Chart with 3 trend lines of the changes from Study 1 to Study 2: Good, average, and bad intranets all improved." height="366" src="http://media.nngroup.com/media/editor/alertbox/intranet-usability-metrics-trends.gif" width="527"/><br/>
	<em>The number of hours an average employee would spend each year performing the 11 common intranet tasks that we tested in both rounds of our user research. </em>
	<ul style="margin-top: 0.2ex;">
		<li>
			<em>The upper line (<strong>red</strong>) shows task times for the <strong> worst (slowest) 25% </strong> of intranets, </em></li>
		<li>
			<em>the middle line (<strong>blue</strong>) shows task times for an <strong> average intranet </strong> , and </em></li>
		<li>
			<em>the lower line (<strong>green</strong>) shows task times for the <strong> best (fastest) 25% </strong> of intranets </em></li>
	</ul>
</div>
<p>
	Two conclusions are clear from this chart:</p>
<ul>
	<li>
		Intranet usability has increased substantially, leading to <strong> faster task performance </strong> and thus higher employee productivity. The hours people save from faster intranet use is time they have available to do their "real" work. For the median intranet, <strong> productivity increased by 44%</strong>.</li>
	<li>
		The most <strong> dramatic improvements </strong> have come to <strong> intranets with poor usability </strong> , where productivity increased by <strong> 69%</strong>. Intranets with good usability have also improved, but not nearly as much as the poor intranets.</li>
</ul>
<h2>
	Does Intranet Usability Still Pay?</h2>
<p>
	Now that intranets are easier to use, do companies still need to invest in even better intranet usability? One way of considering this question is to look at the cost of the time employees spend using the intranet.</p>
<p>
	For the 18 common intranet tasks we tested in Study 2, we can compute the cost of the employees' time by multiplying the number of hours per year a user spends doing the tasks with the cost of keeping that user employed. Each company can perform the calculation using its own salary levels and overhead costs, but for the sake of argument, we'll use $30 per hour, which corresponds to the average hourly salary of a white-collar employee in the U.S. loaded with an additional 50% in overhead.</p>
<p>
	The following table shows the <strong> annual costs of the time spent </strong> performing our 18 intranet tasks in a company with 10,000 intranet users:</p>
<table align="center" style="border-style: solid; border-color: #cccccc; border-width: 1px; border-collapse: collapse;">
	<tbody>
		<tr>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left">
				<strong>Good </strong> intranet usability (Q1)</td>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: center">
				<strong>$7.5 M/year </strong></td>
		</tr>
		<tr>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left">
				<strong>Average </strong> intranet usability (median)</td>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: center">
				<strong>$9.9 M/year </strong></td>
		</tr>
		<tr>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left">
				<strong>Poor </strong> intranet usability (Q3)</td>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: center">
				<strong>$12.9 M/year </strong></td>
		</tr>
	</tbody>
</table>
<p>
	Thus, a company with poor intranet usability would <strong> save $3 million per year </strong> if it improved its intranet usability to an average level. And a company with average intranet usability would save $2.4 million per year if it improved its intranet to the usability level found in the best 25%.</p>
<p>
	These numbers only estimate the productivity gains from the common intranet tasks we tested. In addition, most companies have their own special, mission-critical intranet tasks that by definition can't be tested across organizations, but which usually have much more impact on overall employee performance than do the common intranet tasks. Thus, in most companies, the total productivity gains from improving intranet usability will be <strong> many times higher </strong> than these estimates.</p>
<p>
	In Study 1, we tested 16 common intranet tasks. <a class="old" href="../intranet-usability-the-trillion-dollar-question/index.php" title="Alertbox: Intranet Usability, The Trillion-Dollar Question"> Back then, we estimated</a> the following productivity gains from improving intranet usability in a company with 10,000 employees: going from bad to average, $10 M/year; going from average to good, $5 M/year.</p>
<p>
	Because there was some difference in the tasks tested in Study 1 and Study 2, the productivity savings are not directly comparable, but the overall picture is certainly clear: the productivity gains from improving intranet usability are much smaller now than they used to be.</p>
<p>
	This should come as no surprise — we've already eradicated the worst mistakes in intranet usability, so <strong> we've already collected most of the gains </strong> from the proverbial low-hanging fruit.</p>
<p>
	The downside of the huge intranet usability improvements in recent years is thus that there's <strong> not quite the same potential </strong> for future gains.</p>
<p>
	Should we throw in the towel and stabilize the world's intranets at their current level of usability? Not at all.</p>
<p>
	There are three reasons to advocate <strong> continued work </strong> on improving intranet usability:</p>
<ul>
	<li>
		We estimate <strong> productivity gains of $2-3 million per year </strong> for common intranet tasks, and several times that amount for company-specific tasks. In comparison, a project to improve intranet usability typically costs less than half a million for the 10,000-employee company we're using as our example. Bigger companies will have bigger expenses, but they'll also realize proportionally bigger productivity gains. A company with 100,000 employees will get 10 times the benefits from intranet improvements, but will probably need only around 4 times as big a budget to realize the gains (that is, $20-30 M return on a $2 M investment).</li>
	<li>
		Our discussion of "poor intranet usability" looks at the data from the Q3 performance level, representing the cut-off between the best 75% and the worst 25%. But many companies with poor intranet usability are <strong> far below this Q3 </strong> level, particularly if they've ignored intranet usability until now. The good news is that they still have correspondingly huge potential gains once they start a usability project.</li>
	<li>
		Our estimate of "good intranet usability" looks purely at current best practices, not at the theoretical optimal design. There's no reason to believe that our Q1 numbers represent the final word in intranet usability. <strong> Further improvements </strong> can be had if companies continue to push the state of the art in intranet design. Our experiences from the Intranet Design Annuals make us very optimistic on this front, because every year we see that the very best intranets become even better.</li>
</ul>
<p>
	Thus, there is every reason to believe that intranet usability projects will <strong> continue to show <a class="old" href="../usability-roi-declining-but-still-strong/index.php" title="Alertbox: Return on Investment for Usability"> good ROI</a></strong>, even if the improvements won't be as huge as they were for a company's first usability project.</p>
<h2>
	Better, But Not Good</h2>
<p>
	Intranet usability has improved substantially, yes. But is it good enough? No. We started out at an extreme low, with intranets being the impoverished cousins of websites: no company investment in design and usability, pure chaos in navigation and IA. Things are indeed better now — 77% of intranet teams say they get adequate management support. But at most companies, the intranet user experience is still nowhere near what it needs to be to maximize employee productivity.</p>
<h2>
	Full Report With Newer Research</h2>
<p>
	This article reports the findings from our 2nd round of research. Note that the <a href="../../reports/intranet-usability-guidelines/index.php" title="Intranet User Experience Design Guidelines: Findings from User Testing of 42 Intranets, report series">research report on intranet usability</a> has since been updated with newer research findings from additional studies.</p>
</section>
  

  
  <section>
    
        
    
  </section>
  
  
  
  
  

  


  <section>
    <br />
    <p class="share"><b>Share <span class="hide-for-small-down">this article</span>:</b> <a href="https://twitter.com/intent/tweet?url=http://www.nngroup.com/articles/intranet-usability-huge-advances/&amp;text=Intranet%20Usability%20Shows%20Huge%20Advances&amp;via=nngroup">Twitter</a> | <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=http://www.nngroup.com/articles/intranet-usability-huge-advances/&amp;title=Intranet%20Usability%20Shows%20Huge%20Advances&amp;source=Nielsen%20Norman%20Group">LinkedIn</a> | <a href="https://plus.google.com/share?url=http://www.nngroup.com/articles/intranet-usability-huge-advances/">Google+</a> | <a href="mailto:?subject=NN/g Article: Intranet Usability Shows Huge Advances&amp;body=http://www.nngroup.com/articles/intranet-usability-huge-advances/">Email</a></p>
    <hr />
  </section>

  <footer class="l-subsection">
    
    
    <div class="learn-more">
      <h2>Learn More</h2>
      <p><a href="../subscribe/index.php">Subscribe to the weekly newsletter</a> to get notified about future articles.</p>
      </div>
    
    

    <div class="row show-for-small-down related-content">
      
      
      

      
      
        <div class="column small-12 topic-links">All articles about:
          <ul>
            
            <li><a href="../../topic/intranets/index.php">Intranets</a></li>
            
          </ul>
        </div>
      
      

      
      
      

      
      
        <div class="small-12 column">
          <h3>Research Reports</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../reports/intranet-design-annual/index.php">2017 Intranet Design Annual</a></li>
              
            
              
                <li><a href="../../reports/intranet-usability-guidelines/index.php">Intranet Usability Guidelines: Findings from User Testing of 42 Intranets</a></li>
              
            
              
                <li><a href="../../reports/intranet-portals-experiences-real-life-projects/index.php">Intranet Portals: UX Design Experience from Real-Life Projects</a></li>
              
            
              
                <li><a href="../../reports/intranet-information-architecture-design-methods/index.php">Intranet Information Architecture Design Methods and Case Studies</a></li>
              
            
              
                <li><a href="../../reports/best-merged-intranets/index.php">Mergers and Acquisitions, and the Resulting Intranets 2007-2016</a></li>
              
            
          </ul>
        </div>
      
      

      
      
        <div class="small-12 column">
          <h3>Online Seminars</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../online-seminars/improving-intranet-content/index.php">Improving Intranet Content</a></li>
              
            
              
                <li><a href="../../online-seminars/intranet-users-find/index.php">Helping Intranet Users Find What They Need</a></li>
              
            
          </ul>
        </div>
      
      
    </div>

    <div class="hide-for-small-down row related-content">
      <div class="medium-6 columns">
        
        
          <div>
            <h3>Articles</h3>
            <ul class="no-bullet">
              
                
                <li><a href="../intranet-design/index.php">10 Best Intranets of 2017</a></li>
                
              
                
                <li><a href="../top-intranet-design-mistakes/index.php">The Top Enduring Intranet-Design Mistakes: 7 Deadly Sins</a></li>
                
              
                
                <li><a href="../top-intranet-trends/index.php">Top 10 Intranet Trends of 2016</a></li>
                
              
                
                <li><a href="../intranet-content-authors/index.php">3 Ways to Inspire Intranet Content Authors</a></li>
                
              
                
                <li><a href="../sharepoint-intranet-ux/index.php">Design a Brilliant SharePoint Intranet</a></li>
                
              
            </ul>
          </div>
        
        

        
          
          <div>
            <h3>Research Reports</h3>
            <ul class="no-bullet">
              
                
                  <li><a href="../../reports/intranet-design-annual/index.php">2017 Intranet Design Annual</a></li>
                
              
                
                  <li><a href="../../reports/intranet-usability-guidelines/index.php">Intranet Usability Guidelines: Findings from User Testing of 42 Intranets</a></li>
                
              
                
                  <li><a href="../../reports/intranet-portals-experiences-real-life-projects/index.php">Intranet Portals: UX Design Experience from Real-Life Projects</a></li>
                
              
                
                  <li><a href="../../reports/intranet-information-architecture-design-methods/index.php">Intranet Information Architecture Design Methods and Case Studies</a></li>
                
              
                
                  <li><a href="../../reports/best-merged-intranets/index.php">Mergers and Acquisitions, and the Resulting Intranets 2007-2016</a></li>
                
              
            </ul>
          </div>
          
        

      </div>
      <div class="medium-6 columns">
        
        
          
          <div>
            <h3>Online Seminars</h3>
            <ul class="no-bullet">
            
                <li><a href="../../online-seminars/improving-intranet-content/index.php">Improving Intranet Content</a></li>
            
                <li><a href="../../online-seminars/intranet-users-find/index.php">Helping Intranet Users Find What They Need</a></li>
            
            </ul>
          </div>
          
        
      </div>
    </div>
  </footer>
</article>

        </div>

      

    
  </div>
  <div class="row">
    
      <div class="column small-12 show-for-small-down l-navrail announcements">
        
          
        
      </div>
    
    
  </div>

    </div>
    




<footer id="page-footer">
    <div class="row hide-for-print">
        <div class="column small-12 medium-6">
            <section class="footer-popular">
                
                <h1>Popular Topics</h1>
                <div style="float: left;  width: 15em; display: block;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/applications/index.php">Application Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/e-commerce/index.php">E-Commerce Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/eyetracking/index.php">Eyetracking </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/information-architecture/index.php">Information Architecture </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/intranets/index.php">Intranet Design </a></li>
	<li><a href="../../topic/interaction-design/index.php">Interaction Design </a></li>
</ul>
</div>

<div style="float: left;  width: 15em; display: block; list-style: none;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/mobile-and-tablet-design/index.php">Mobile and Tablet Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/credibility-persuasion/index.php">Persuasive Design</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/research-methods/index.php">Research Methods</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/user-testing/index.php">User Testing </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/web-usability/index.php">Web Usability </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/writing-web/index.php">Web Writing &amp; Content Strategy </a></li>
</ul>
</div>

<p>&nbsp;</p>

                
            </section>
        </div>

        <div class="column small-12 medium-6">
            <div class="row">
                <div class="column small-6">
                    <section class="footer-about">
                        <h1>Follow Us</h1>
                        <ul>
	<li><a href="../subscribe/index.php">Subscribe to our weekly UX newsletter</a>,&nbsp;Jakob Nielsen&#39;s Alertbox</li>
	<li>Visit Don Norman&#39;s website, <a href="http://www.jnd.org/"> www.jnd.org </a></li>
	<li>Visit Bruce Tognazzini&#39;s website, <a href="http://www.asktog.com/"> www.asktog.com </a></li>
	<li style="padding-top: 6px;"><a class="twitter-follow-button" data-show-count="false" data-size="large" href="https://twitter.com/nngroup"><img alt="Follow @nngroup on Twitter" height="20" src="https://media.nngroup.com/media/editor/2016/12/30/twitter-follow-button.png" style="width: 117px; height: 20px;" width="117" /></a></li>
	<li style="padding-top: 6px;"><img alt="" height="14" src="https://media.nngroup.com/media/editor/2016/07/26/feed-icon-14x14.png" style="vertical-align:text-top;width:14px;margin-right:.25em;" width="14" /><a href="../../feed/rss/index.php">Subscribe to our RSS feed</a></li>
</ul>

                    </section>
                </div>

                <div class="column small-6">
                    <section class="footer-about right-column">
                        <h1>About
</h1>
                        <ul class="no-bullet">
	<li><a href="../../about/contact/index.php">Contact information</a></li>
	<li><a href="../../about/index.php">About Us</a></li>
	<li><a href="../../about/why-nng/index.php">Why NN/g? </a></li>
	<li><a href="../../return-policy/index.php">Return Policy</a></li>
</ul>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="copyright column small-12">
            <p><a href="../../copyright-and-reprint-info/index.php">Copyright</a> &copy; 1998-2017 Nielsen Norman Group, All Rights Reserved.</p>
        </div>
    </div>
</footer>


    


        <script src="https://media.nngroup.com/static/vendor/fastclick/js/fastclick.min.js"></script>
        <script src="https://media.nngroup.com/static/vendor/foundation/js/foundation.min.js"></script>
        <script src="http://media.nngroup.com/static/js/app.js"></script>
        <script src="http://media.nngroup.com/static/js/scroll-to-fixed.js"></script><script>var externalLinks = document.querySelectorAll('a[href^="http"]');for (var i = externalLinks.length-1; i >= 0; i--) {externalLinks[i].addEventListener("click", function(event) { alert("External links are disabled for this test."); event.preventDefault(); }, false);}var mailLinks = document.querySelectorAll('a[href^="mailto"]');for (var i = mailLinks.length-1; i >= 0; i--) {mailLinks[i].addEventListener("click", function(event) { alert("Email links are disabled for this test."); event.preventDefault(); }, false);}var externalForms = document.querySelectorAll('form');for (var i = externalForms.length-1; i >= 0; i--) {externalForms[i].addEventListener("submit", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false); externalForms[i].addEventListener("click", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false);}</script>

        <script>
            
            $(function() {
                $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
                $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
            });
        </script>

        
<script src="https://media.nngroup.com/static/js/articles.js"></script>


        
        <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1072699960;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072699960/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
    </body>

<!-- Mirrored from www.nngroup.com/articles/intranet-usability-huge-advances/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:14:04 GMT -->
</html>
