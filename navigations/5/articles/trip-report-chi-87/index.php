<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->





    
<!-- Mirrored from www.nngroup.com/articles/trip-report-chi-87/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:14:37 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1026.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"beacon":"bam.nr-data.net","queueTime":2,"applicationTime":1301,"agent":"","licenseKey":"1a30ef9931","errorBeacon":"bam.nr-data.net","applicationID":"2048992","transactionName":"NFxSMRBUX0EDUkIPDA0WdhAMVkVbDV8ZBxEXUFMJB0YfRAtUQRVZIktEDAFZVHYHRVcPD01eVRE="}</script>
        <title>CHI&#39;87 Trip Report: Article by Jakob Nielsen</title><meta property="og:title" content="CHI&#39;87 Trip Report: Article by Jakob Nielsen" />
  
        
        <meta name="description" content="Jakob Nielsen&#39;s Trip Report from the 1987 Joint Computer-Human Interaction + Graphics Interface Conferences (CHI+GI &#39;87)">
        <meta property="og:description" content="Jakob Nielsen&#39;s Trip Report from the 1987 Joint Computer-Human Interaction + Graphics Interface Conferences (CHI+GI &#39;87)" />
        
  
        
	
        

        

        <meta name="robots" content="noarchive">
        <meta name="viewport" content="width=device-width">

        <link rel="search" type="application/opensearchdescription+xml" title="NN/g Search" href="../../opensearch.xml" />
        <link rel="shortcut icon" href="http://media.nngroup.com/static/img/favicon.ico" />

        <link rel="stylesheet" href="https://media.nngroup.com/static/vendor/font-awesome/css/font-awesome.min.css">

        
        <link rel="stylesheet" href="https://media.nngroup.com/static/css/7232a960f1df.css" type="text/css" />
        <link rel="stylesheet" href="http://fast.fonts.com/t/1.css?apiType=css&amp;projectid=c5846260-9def-46ec-9407-0f1349bd2d64" type="text/css">

        <script src="https://media.nngroup.com/static/vendor/modernizr/js/modernizr.min.js"></script>

        <script type="text/javascript" src="https://media.nngroup.com/static/js/3283be5def74.js"></script>
        

        

        
        
        <!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=229034,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('http://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->


<!-- Google Analytics Tracking Code for http://nngroup.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23132681-1', 'nngroup.com');
  ga('require', 'linkid', 'https://www.nngroup.com/articles/trip-report-chi-87/linkid.js');
  ga('require', 'displayfeatures');

// Optimizely Universal Analytics Integration
 window.optimizely = window.optimizely || [];
 window.optimizely.push("activateUniversalAnalytics");

  ga('send', 'pageview');

</script>
<script>
/**
* Function to track a click on an outbound link in Google Analytics.
*/
function trackOutboundLink(label, url) {
    if(window.ga && ga.loaded) {
        ga('send', 'event', 'outbound', 'click', label, {'hitCallback': function(){document.location = url;}
        });
    } else {
        document.location = url;
    }
}
/**
* Function to track a click on a link in the article announcement area.
*/
$('#articleAnnouncement a').on('click', function(){
  ga('send', 'event', 'link', 'click', 'articleAnnouncement');
});
</script>

<script type="text/javascript">
 setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2421.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



<!-- Hotjar Tracking Code for http://nngroup.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:121605,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

        <script src="https://media.nngroup.com/static/js/analytics/track_outbound_link.js"></script>
        
        
        
    </head>
    <body class="location-articles articles-detail">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
    <header id="page-header">
        <div id="cart" class="shopping-cart mobile-cart show-for-small-down"></div>
        <div class="row">
            <div id="masthead" class="masthead small-12 medium-7 column">

                <!-- The button to toggles menu on small screen -->

                <h1><a href="../../index.php"><img src="http://media.nngroup.com/static/img/logo-wordmark.png" width="74" height="30"> Nielsen <span class="norman">Norman</span> Group</a></h1>
                <p class="tagline show-for-medium-up">Evidence-Based User Experience Research, Training, and Consulting</p>

                <!-- The button to toggles form search on small screen -->


                <div style="clear: right;width:  100%;height: 1px;"></div><?php define('ROOT_PATH',$_SERVER["DOCUMENT_ROOT"] . '/apps/navigation_styles/navigations/5'); ?><?php include(ROOT_PATH)."/navigation.php"; ?>

                <!-- Drop down menu on small screen -->
                <div id="hideNav" class="show-for-small-down">
                  
                  <ul class="menu-content">
                    <li><a href="../../index.php">Home</a></li>
                    <li class="has-submenu">
                      <a>Training</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../training/index.php">Overview</a></li>
                        
<li id="nav-ux-conference"><a href="../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../online-seminars/index.php"><span>Online Seminars</span></a></li>

                      </ul>
                    </li>
                    <li class="has-submenu">
                      <a>Consulting</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
  <li><a href="../../consulting/index.php">Overview</a></li>
  
  <li><a href="../../consulting/benchmarking/index.php">Benchmarking</a></li>
  
  <li><a href="../../consulting/expert-design-review/index.php">Expert Design Review</a></li>
  
  <li><a href="../../consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
  
  <li><a href="../../consulting/usability-testing/index.php">Usability Testing </a></li>
  
  <li><a href="../../consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
  
  <li><a href="../../in-house-training/index.php">In-House Training</a></li>
</ul>

                    </li>

                    <li><a href="../../reports/index.php">Reports</a></li>
                    <li><a href="../index.php">Articles</a></li>
                    <li class="has-submenu"><a>About NN/G</a><i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../about/index.php">Overview</a></li>
                        <li><a href="../../people/index.php">People</a></li>
                        <li><a href="../../about/why-nng/index.php">Why NN/g?</a></li>
                        <li><a href="../../about/contact/index.php">Contact</a></li>
                        <li><a href="../../news/index.php">News</a></li>
                        <li><a href="../../about/history/index.php">History</a></li>
                        <li><a href="../../books/index.php">Books</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                </div>

                <hr class="header-divider show-for-small-only"/>


            </div>
       </div>

        <div id="hideNav">            <nav class="primary-nav small-12 column">
                <ul>
                    <li><a class="navitem" id="nav-home" href="../../index.php">Home</a></li>
                    <li><a class="navitem" id="nav-training" href="../../training/index.php">Training</a></li>
                    <li><a class="navitem" id="nav-consulting" href="../../consulting/index.php">Consulting</a></li>
                    <li><a class="navitem" id="nav-reports" href="../../reports/index.php">Reports</a></li>
                    <li><a class="navitem" id="nav-articles" href="../index.php">Articles</a></li>
                    <li><a class="navitem" id="nav-about" href="../../about/index.php">About NN/G</a></li>
                </ul>
            </nav>
            <nav class="secondary-nav small-12 medium-12 large-12 columns">
                
                
            </nav>
        </div>
    </header>
    <div class="l-content"><span id="preventClose" style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776; Menu</span>
    
  

  

  <div class="row">
    
      
      <section class="small-12 medium-3 columns collapsable-content l-navrail articles-left-column">
        <h1 class="show-for-small-down collapse-link mobile-browse-header">Browse by Topic and Author</h1>
          <div class="row collapsable">

            
            <div class="small-6 medium-12 columns">
              <section id="article-popular-topics" style="display: none;">
                
                <h1>Topics</h1>
                <ul id="article-popular-topics">
                    
                    <li>
                        
                        <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/intranets/all/index.php">Intranets</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/user-testing/all/index.php">User Testing</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                        
                    </li>
                    
                    <li><a href="#" id="article-see-all-topics"><strong>See all topics&hellip;</strong></a></li>
                </ul>
                
              </section>
              <section id="article-all-topics" style="display: none;">
                
                <h1>
                  <span class="show-for-medium-up">All Article Topics</span>
                  <span class="show-for-small-down">All Topics</span>
                  (<a href="#" id="article-hide-all-topics">hide</a>)
                </h1>
                <ul>
                  
                  <li>
                    
                    <a href="../../topic/accessibility/all/index.php">Accessibility</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/agile/all/index.php">Agile</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/analytics-and-metrics/all/index.php">Analytics &amp; Metrics</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/applications/all/index.php">Application Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/b2b-websites/all/index.php">B2B Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/behavior-patterns/all/index.php">Behavior Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/branding/all/index.php">Branding</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/content-strategy/all/index.php">Content Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/corporate-websites/all/index.php">Corporate Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/design-patterns/all/index.php">Design Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-design-process/all/index.php">Design Process</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/email/all/index.php">Email</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/eyetracking/all/index.php">Eyetracking</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/heuristic-evaluation/all/index.php">Heuristic Evaluation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/human-computer-interaction/all/index.php">Human Computer Interaction</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-ideation/all/index.php">Ideation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/information-architecture/all/index.php">Information Architecture</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/interaction-design/all/index.php">Interaction Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/international-users/all/index.php">International Users</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/intranets/all/index.php">Intranets</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-management/all/index.php">Management</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/navigation/all/index.php">Navigation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/non-profit-websites/all/index.php">Non-Profit Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/personas/all/index.php">Personas</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/persuasive-design/all/index.php">Persuasive Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/prototyping/all/index.php">Prototyping</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/psychology-and-ux/all/index.php">Psychology and UX</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/research-methods/all/index.php">Research Methods</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/search/all/index.php">Search</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/social-media/all/index.php">Social Media</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/strategy/all/index.php">Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/user-testing/all/index.php">User Testing</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-teams/all/index.php">UX Teams</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/visual-design/all/index.php">Visual Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/writing-web/all/index.php">Writing for the Web</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/young-users/all/index.php">Young Users</a>
                    
                  </li>
                  
                </ul>
                
              </section>
            </div>
            <script>
              /* Determine which topics list will be displayed: the full or the
               * partial.  We are inlining this close to the list itself to
               * prevent rendering delays on the client
               */
              var topic_list = $.cookie("article-topics");

              var popular_topics = $('#article-popular-topics');
              var all_topics = $('#article-all-topics');

              if(topic_list == 1) {
                  popular_topics.hide();
                  all_topics.show();
              } else {
                  popular_topics.show();
                  all_topics.hide();
              }
            </script>
            

            
            <div class="small-6 medium-12 columns">
              
              <section id="article-initial-authors">
                  <h1>Author</h1>
                  <ul>
                    
                    <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                    
                    <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                    
                    <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                    
                    <li><a href="#" onClick="$('#article-initial-authors').hide(); $('#article-all-authors').show();"><strong>See all authors&hellip;</strong></a></li>
                  </ul>
              </section>
              <section id="article-all-authors" style="display: none;">
                <h1>
                  All Authors
                  (<a href="#" id="article-hide-all-authors">hide</a>)
                </h1>
                <ul>
                  
                  <li><a href="../author/raluca-budiu/index.php">Raluca Budiu</a></li>
                  
                  <li><a href="../author/patty-caya/index.php">Patty Caya</a></li>
                  
                  <li><a href="../author/susan-farrell/index.php">Susan Farrell</a></li>
                  
                  <li><a href="../author/therese-fessenden/index.php">Therese Fessenden</a></li>
                  
                  <li><a href="../author/kim-flaherty/index.php">Kim Flaherty</a></li>
                  
                  <li><a href="../author/sarah-gibbons/index.php">Sarah Gibbons</a></li>
                  
                  <li><a href="../author/aurora-harley/index.php">Aurora Harley</a></li>
                  
                  <li><a href="../author/kate-kaplan/index.php">Kate Kaplan</a></li>
                  
                  <li><a href="../author/page-laubheimer/index.php">Page Laubheimer</a></li>
                  
                  <li><a href="../author/angela-li/index.php">Angie Li</a></li>
                  
                  <li><a href="../author/hoa-loranger/index.php">Hoa Loranger</a></li>
                  
                  <li><a href="../author/kate-meyer/index.php">Kate Meyer</a></li>
                  
                  <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                  
                  <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                  
                  <li><a href="../author/kara-pernice/index.php">Kara Pernice</a></li>
                  
                  <li><a href="../author/christian-rohrer/index.php">Christian Rohrer</a></li>
                  
                  <li><a href="../author/amy-schade/index.php">Amy Schade</a></li>
                  
                  <li><a href="../author/katie-sherwin/index.php">Katie Sherwin</a></li>
                  
                  <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                  
                  <li><a href="../author/kathryn-whitenton/index.php">Kathryn Whitenton</a></li>
                  
                </ul>
              </section>
              
            </div>
            
          </div>

          
          <section class="hide-for-small-down">
            
              
              <h1>Recent Articles</h1>
              
              <ul>
                  
                  <li><a href="../seamless-cross-channel/index.php">Seamlessness in the Omnichannel User Experience</a></li>
                  
                  <li><a href="../brand-intention-interpretation/index.php">Experience Design: Bridging Brand Intention and Brand Interpretation</a></li>
                  
                  <li><a href="../eyetracking-tasks-efficient-scanning/index.php">Scanning Patterns on the Web Are Optimized for the Current Task</a></li>
                  
                  <li><a href="../flat-design-best-practices/index.php">Flat-Design Best Practices</a></li>
                  
                  <li><a href="../ux-user-stories/index.php">Accounting for UX Work with User Stories in Agile Projects</a></li>
                  
                  <li><a href="../index.php"><strong>See all articles&hellip;</strong></a></li>
              </ul>
              
            
          </section>
          

          
          
          <section class="hide-for-small-down">
            <h1>Popular Articles</h1>
            
            <ul>
                
                <li><a href="../usability-101-introduction-to-usability/index.php">Usability 101: Introduction to Usability</a></li>
                
                <li><a href="../top-10-mistakes-web-design/index.php">Top 10 Mistakes in Web Design</a></li>
                
                <li><a href="../how-users-read-on-the-web/index.php">How Users Read on the Web</a></li>
                
                <li><a href="../f-shaped-pattern-reading-web-content/index.php">F-Shaped Pattern For Reading Web Content</a></li>
                
                <li><a href="../ten-usability-heuristics/index.php">10 Usability Heuristics for User Interface Design</a></li>
                
                <li><a href="../intranet-design-2016/index.php">10 Best Intranets of 2016</a></li>
                
                <li><a href="../which-ux-research-methods/index.php">When to Use Which User-Experience Research Methods</a></li>
                
                <li><a href="../response-times-3-important-limits/index.php">Response Times: The 3 Important Limits</a></li>
                
                <li><a href="../why-you-only-need-to-test-with-5-users/index.php">Why You Only Need to Test with 5 Users</a></li>
                
                <li><a href="../page-fold-manifesto/index.php">The Fold Manifesto: Why the Page Fold Still Matters</a></li>
                
            </ul>
          </section>
          
          

          
            <section class="hide-for-small-down">
              
            </section>
          

      </section>
      

      
        <div class="small-12 medium-9 columns content-body">
          




<article class="l-subsection alertbox-article-content article-detail-bl">
  
  <header>
    <div class="l-subsection">
      <h1>CHI+GI&#39;87 Trip Report</h1>
      <div class="author-meta">by
        
          
            
            <a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a>
            
          
        on  December 31, 1987
        
      </div>

      

      
      <div class="topic-links">
        Topics:
        
          <ul>

  <li><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction</a></li>

</ul>
        
      </div>
      
    </div>
  </header>
  

  
  
  <section>
    <p class="lede"><strong>Summary:</strong> Joint Computer-Human Interaction + Graphics Interface Conferences; Toronto, Canada, 5-9 April 1987</p>
    <br />
  </section>
  
  

  
  <section id="article-body"><p><em>The conference proceedings can be <a href="http://www.amazon.com/exec/obidos/ISBN=0897911806/useitcomusableinA/"> bought online</a>. </em></p>

<p>Bill Buxton (University of Toronto) opened the conference by stressing that computer-human interaction work is a critical resource for improvements and that we are going to have significant impact on society-if only we let society at large know what we are working on. I could add that it is simply amazing how many people participate in the public debate about computers and subjects such as computer education on the basis of their experience with obsolete, "user-hostile" systems. Buxton warned that we have to realize that computer fobia is real and that we have to work to prevent it from happening and destroying the positive impact of modern user interfaces. We have to remember that we are not just designing computers - <strong> we are designing quality of life</strong>. So we have to strive for excellence and as an example, Buxton showed a video film from Pixar which was the second computer-generated film ever to be nominated for an Oscar. This was an extremely cute film of two animated Luxo lamps, and the technical quality was so good that one person (who came late and did not hear Buxton's introduction to the film) later asked me whether the pictures had been computer generated or were photographed.</p>

<h2>It Fits Like a Glove</h2>

<p>-of course it does; it <em> is </em> a glove: <strong> DataGlove </strong> from VPL Research was probably the most innovative interaction device shown at the conference. You put it on like a normal glove and it then acts as an input device that senses finger movements and hand position (3 coordinates) and orientation (3 coordinates) in real time. They even experiment with providing tactile feedback in the glove so that it would feel different to bend your fingers depending on whether you were "grasping" a data object with the glove.</p>

<p>The DataGlove had three applications at the moment. The first was to substitute for medical measurements of finger-movement ability of people who have certain diseases. Measuring how much people can flex their fingers is currently done with fairly expensive and awkward equipment. Instead the patients can just put on a DataGlove and let the computer make the measurements.</p>

<p>The second application was probably the most interesting: Direct manipulation of 3-dimensional objects. They had a 3-D CAD program running on the Macintosh where the user could use the DataGlove to grasp a 3-D object (e.g. a box) and move and rotate it by moving and rotating the hand. You would then let go of the object by spreading your fingers, and the object would be in its new position. The 2-D computer display showed a projection of the simulated 3-D space in which the DataGlove operated and the position and rotation of the user's hand was represented by a (somewhat sinister) skeleton hand on the screen. The real advantage of this system is that introduces as direct a manipulation as possible without 3-D holographic projection of the data space. In current system using a mouse or other 2-D locater device, 3-D operations are somewhat indirect.</p>

<p>Finally, the third application was to use hand- and finger-motion tracking as an input device. The demo showed a finger-painting system where you painted on the screen by moving your finger. The screen was cleared by spreading your fingers. This was not all that impressive since it was somewhat like the Videoplace system.</p>

<h2>Noobie: A Furry Computer to Play With</h2>

<p>One of the most talked-about events at the conference was the demonstration and exhibition of Noobie-The Furry Computer by Allison Druin from the MIT Media Lab. Noobie is a computer intended for small children who don't know who to use a keyboard-and who may not even like to use a mouse. The "input device" is simply a huge (somewhat larger than a person) <strong> plush animal </strong> (designed using help from the Muppet Show). The user (child) sits in the lap of the animal or crawls all over it. When the user squeezes some part of the animal, it acts as input to the computer which responds accordingly. At the moment the feedback consists of a drawing of another animal on the computer screen looking somewhat like the physical stuffed animal. But the computer animated animal can change its parts (e.g. from hands to claws) and the parts of the computer animal will cycle through its different when the user squeezes the corresponding parts of the stuffed animal.</p>

<p>The problem with this approach is that it probably does not teach the child algorithmic thinking or any general problem solving. The advantage is that the computer feels very responsive since you only get immediate feedback and no delayed programming effects. It is also an open question whether Noobie would motivate children for extended use. Certainly the children (of all ages) present during the demonstration had great fun. It could be used to generate positive feelings towards computers and of course it could probably also be used for more advanced interaction techniques. Certainly the concept of having the entire animal as the input device for a computer is both novel and challenging.</p>

<p class="updatecomment">(UPDATE: Now, in 1997, we are starting to see commercial products of the type pioneered by Noobie; e.g., Microsoft Barney. Unfortunately, these products are not as innovative and interactive as Druin's prototype from ten years ago.)</p>

<p>Noobie is implemented using a Macintosh which is buried inside the stuffed animal. The screen is visible in its stomach. The squeezing input is implemented with sensors throughout the animal which are connected to the Macintosh as though each sensor was a key on the keyboard. This has the advantage that the Macintosh does not have to be modified but it also means that a squeeze is simply sensed as an on-off action (i.e. not pressure-sensitively).</p>

<h2>Looking Good, Informing Great (Edward Tufte)</h2>

<p>Edward Tufte from Yale University gave the opening speech on information design and statistical graphics. The talk was rather similar to Tufte's book <a href="http://www.amazon.com/exec/obidos/ISBN=096139210X/useitcomusableinA/"> The Visual Display of Quantitative Information</a> which I had already read (and can highly recommend). Tufte advocated simplicity of design. He wanted richness and complexity of the contents and substance of a graph but no "self-advertising design".</p>

<p>In some sense I feel that Tufte is too much of a fanatic in his opposition to fancy graphics (made by computer or otherwise). Often it is a good criterion to maximize information contents in graphs, but attention-getting "business graphics" surely also has a place. Sometimes you simply get in a good mood by seeing a flashy graph. But of course there is no excuse for misleading graphics where you e.g. use the size of two-dimensional objects to depict changes in a one-dimensional variable (as in the figure where B is two times as large a number as A but looks 4 times as big).</p>

<p>On the other hand, Tufte was not so worried about the classical problem How to Lie with Statistics since he felt that our "lie detectors" are probably better for graphics than they are for words. The preoccupation with showing the Truth has sidetracked the design of statistical graphs-e.g., the principle of always showing the 0-point on the y axis may lead to less informative graphs.</p>

<p>The basic challenge of graph design is escape the 2-D Flatland of paper and the computer screen to represent the multi-D richness of our complex world. We need to add dimensionality. One approach advocated by Tufte is micro-macro reading-the ability to read a graph from the macro view to grasp the total picture and then to look at selected details from a micro view. As an example, Tufte distributed a reprint of a tourist map of Manhattan: One of these maps where all buildings are drawn as they look rather than as colored squares. The macro view of the map gave you an idea of the New York skyline (you could never mistake this kind of map of Manhattan with a similar map of Copenhagen) while the micro view allowed you to identify your own office window or your favorite news stand.</p>

<h2>Psychology as Input to System Design (Tom Landauer)</h2>

<p>Tom Landauer from Bell Communications Research (Bellcore) is one of the grandfathers of CHI Research and gave a keynote talk entitled <strong> Psychology as a Mother of Invention </strong> about the possibility for using psychological research results to drive the design of new products (rather than the traditional method of starting with the design and then-two weeks before the release date-rushing through a few evaluative studies). Landauer wanted to move our focus from that of usability (of systems whose functionality of already chosen) to usefulness. In other words: What computer products do we need in the first place. We should take responsibility for inventing functionality to help people solve problems.</p>

<p>The first solution to the interface problem has been to use the extra CPU-power available in modern computers to be more generous with help and other interface features. ("<strong>Burn cycles, not people</strong>" a quote from Daniel Nachbar). We also have to get away from the egocentric intuitive fallacy of believing that everybody else can see what you see and that they therefore will be able to use your "obvious" interface design with no problems. These changes are taking place, and examples were presented in other talks at the conference. We are also getting design guidelines as a distillation of our user interface experience.</p>

<p>So now we know how to solve a number of user interface problems. The next step according to Landauer is to make computers useful to do things people could not do before: Focus on providing service to the user rather than just on the surface of the system in the form of the user interface. We should do synthesis by analysis since once you know what the (real) problem is, the solution is normally easy. Landauer presented the following three methods to achieve this goal:</p>

<ol>
	<li><strong>Failure analysis</strong>. Look at what goes wrong at the moment. As an example Landauer discussed library catalog lookup where people get the wrong result half of the time. If you just ask people, they say that the catalog is OK since they can't imagine how it could be done better. But their studies showed that the reason for the many wrong results was that people look up under other words than the one used in the index. This is because there is so little agreement in naming: There is only 10-20% probability that two people will use the same name for the same thing. So the solution was to introduce <strong> unlimited aliasing </strong> by letting things be known by as many names as people want. This is prohibitive with index cards (store 20 copies of each card) but trivial with computers (just add pointers).</li>
	<li><strong>Individual differences </strong> analysis. We can look at what kind of people have trouble and what kind of people have success at doing some task. Then we can improve the tools such that everybody can be successful.</li>
	<li><strong>Time profiling</strong>. We can look at how people spend their time and then focus our attention on the important things to change. In one system they studied, users spent 53% of their time on error tasks and only 47% on error-free tasks (i.e. productive work). So in this system it would be important to either reduce the number of errors or to speed up error recovery significantly.</li>
</ol>

<p>Landauer wanted us to invent new tools for thought to augment our cognitive ability. One such example is the spreadsheet but there are very few other examples. The best examples of cognitive tools are arithmetic and reading.writing. The technology for these two tools are marks on pieces of paper, so they are "hand tools for the mind." We should now get power tools for the mind.</p>

<p>Computer tools for reading has not been any good as reading from computer displays until now has been slower than reading from paper. Maybe better displays will alleviate this problem. We are also getting new reading tools such as hypertext which has been discussed for many years but has not seen much use until now. The problem here is that we lack an analysis of what people really want/need to read.</p>

<h2>Methods</h2>

<p>An important precondition to being able to impact design is to have the right working methods. Judith Olson (University of Michigan) was the discussant at the paper session on Methodological issues and presented the following list of tools for researchers and designers in computer-human interaction:</p>

<ol>
	<li>Test usability</li>
	<li>Analyze usability</li>
	<li>Generate designs</li>
	<li>Generate functionality</li>
</ol>

<p>One tool (for analyzing usability) was presented by Wendy Kellogg (IBM Yorktown): Scaling techniques for analyzing users' conceptual models of systems. Users were asked to sort cards with the names of different parts of a document formatting system in piles according to their perceived similarity. Not surprisingly, there was considerable difference between expert and novice users in their models of the system. But we may not have expected Kellogg and Breen to find 23% of the system concepts misdefined by the expert users. This may partly have been because of a weakness in their experiment since they defined the reference model of the system from the manual instead of by testing the original designers of the system. Anyway, their result must have some implications for how to write the next version of the manual (either try to get users to acquire the "correct" model or redefine the way the system is explained if the expert users' model is actually OK).</p>

<p>Another tool (mostly for internal use among CHI researchers) was the scenario method presented by Phil Barnard. Barnard views a scenario as an idealized, simplified description of a specific instance of user-computer interaction: An established phenomenon in human factors which is exemplified in a was that allows researchers to take the scenario into account when discussing new models (e.g. cognitive models). A proposed model must be able to account for the events in the scenario by some sensible explanation. This is a way of saving researchers lots of time in going through experimental tests of their new models: First try the encapsulated essence of previous experiments and see if your model can stomach them. As Barnard put it: "We can turbocharge the tortoise of science" (referring to the keynote talk by Allen Newell at CHI'85 who said that the race is between the tortoise of cumulative science and the hare of intuitive design).</p>

<p>What we are now waiting for, is an annotated catalog of good interaction scenarios to really speed up our respective tortoises. It even seems to me that this method could be of use to practitioners too (maybe it will not speed up the hare-but it could get it to run in the right direction): It is well known that examples are much more easy to understand than abstract guidelines, so maybe a good set of interaction scenarios could show designers some of the things to take into account.</p>

<h2>The Social Dimension Hostage (Rob Kling)</h2>

<p>It seems that every user interface conference (CHI, INTERACT, etc.) has to have one keynote speaker who acts as a hostage to the organizational and social impact interests. Once we have gotten this talk over with, we can go back to being techno freaks and look at neat menu systems.</p>

<p>This year Rob Kling from the University of California had this ungrateful job. He discussed the familiar automobile metaphor for user interfaces and noted that in the world of real cars, it is not enough to optimize the individual car. Even if he owned a Porsche, he could still not get to work much faster if the freeway was jammed because of other people. And this problem would not even be solved if everybody drove Porsches. Cars operate in a world of rules and regulations: As soon as we leave our own driveway, we see streetsigns.</p>

<p>As a conclusion to his discussion of the car metaphor, Kling recommended that we use urban design as our metaphor for building computer systems rather than architecture which is the most common current metaphor. On the other hand, Kling did not want the audience to conclude that he felt that good user interfaces were not useful. The point was simply that they are not sufficient since computerization is socially mediated and takes place in an interaction among many people and groups. An example of this is the ZOG computer system at the USS Carl Vinson: It was a failure in real use because the system assumed that all Navy personnel was committed to total data validity-and some were not.</p>

<p>Another concrete example where the social context determines the user interface is the use of text processing in university departments. The secretaries view is as a "text factory" and don't want unneeded complexity such as in UNIX and TEX while the faculty members (especially in a computer science department) want flexible and programmable systems.</p>

<h2>Real World Design</h2>

<p>Very à propos Kling's talk, a group from Uppsala University in Sweden had earlier at the conference given presented a paper entitled "The Interface is Often Not the Problem." They had studied the users of a database system which performed poorly. Then they had redesigned the database user interface to be much better. And people still did not use the system! The explanation turned out to be that they lacked confidence in the validity of the results of database queries. Users were not able to judge whether the results were reasonable and they did not know whether their use of logic to formulate the queries was correct. In another situation the Uppsala group did solve a usability problem: They redesigned the organization of a health care unit into three smaller units so that the nurses using the computer system would be able to have personal knowledge of the situation of the patients and physicians.</p>

<p>Mary Beth Rosson (IBM Yorktown) presented the results of a study of design practice in the real world. It would be extremely useful to know more details about how what aspects of user interface design results in usable interfaces and what aspects are a waste of time (or possibly harmful). But unfortunately she did not have such clear-cut results. This is not surprising considering how hard a problem this is to study and considering how few other results are available. So this project represents a step forward from knowing very little to actually knowing a little bit.</p>

<p>They had interviewed 22 designers about their experience. It seemed that about half had used a phased design model while the other half had used an incremental design model of letting the system evolve during the design. There was very few cases of early user testing. Most empirical work had been done to evaluate designs rather than the possibly more important point of learning something about users to drive the design. Most designers expressed the following opinion about working with human factors people: "Yeah, it seems to be useful but it was too late..." Furthermore, many designers had difficulties separating the user interface and the functionality. For them, the user interface is "what the user does" and not just the form of the dialog.</p>

<h2>Intelligence Should Be in the User's Head</h2>

<p>I have often complained that too little human factors evaluation is being done of expert systems and so-called intelligent interfaces. Well, at least this year's CHI has a panel session entitled Intelligence in Interfaces so I sat down ready to hear all about how to use AI to achieve better interfaces. Actually, all panel participants agreed that the goal should not be to put intelligence into the system. Rather, the intelligence should reside in the user and the system should work as a tool. Tom Malone (MIT) used the slogan "<strong>naked systems</strong>" to signify not dressing up systems in fancy intelligent interfaces. Instead one should open up and expose the knowledge and reasoning in the system to the user so that users can use their own intelligence to do what the system cannot do.</p>

<p>Allan Cypfer (Intellicorp) would prefer that we keep intelligence out of the user interface and put it in the application itself. One example from Intellicorp was a structure editor for molecules: It knew about molecules in a way a simple graphics editor could not do and it would e.g. show 4 different views of the same molecule on the screen at the same time. The interaction techniques did not show any signs of intelligence but the system had semantic domain knowledge.</p>

<p>John Seely Brown (Xerox PARC) advocated three design goals:</p>

<ul>
	<li>Design for <strong> Guessing </strong> - the user should be able to say "let's try..."</li>
	<li>Design for <strong> Group Think </strong> - we should engineer the infrastructure to let each of us tap the mind of the group, thus facilitating learning</li>
	<li>Design for <strong> Bootstrapping </strong> - have self-explanatory systems that people can use in a lifetime learning situation in the workplace.</li>
</ul>

<p>Bob Neches (USC Information Sciences Institute) did suggest a knowledge based tool to help in the design process. He would force designers to use a common vocabulary by storing the definition of the different parts of the user interface in a knowledge base. Consistency between different parts of a user interface is certainly a big problem. Sometimes because designers want to use their own "good" ideas, but most often because they do not realize that a design decision in another part of the project will conflict with their part of the user interface.</p>

<p>Ben Shneiderman (University of Maryland) commented that the panelists had talked about intelligence in the design of interfaces but not about visible presentation of the actions available to the user. Many systems show the objects visibly on the screen but users still have to remember the commands (what you can do with the objects). Shneiderman thought that we could do better than just menus.</p>

<h2>Xerox Metaphors</h2>

<p>One of the paper sessions was officially entitled User Interface Metaphors but quickly became known as the Xerox Session as all three papers were by PARC staff. The session discussant, George W. Furnas said that his outside view of PARC was that their empirical work was OK but that their design work was <em> really great </em> . He suggested that the reasons were that they</p>

<ul>
	<li>have a tight coupling between computer science and psychology</li>
	<li>leverage <strong> cumulative design </strong> (instead of starting from scratch each time)</li>
	<li>perform careful observation and analysis of system use followed by hacking, hacking, hacking</li>
	<li>are very metaphorical.</li>
</ul>

<p>Randy Smith (now at SunLabs) got big laughs from describing the Xerox metaphor in his <strong> Alternate Reality Kit </strong> (ARK) where a button marked "Xerox" is used to make copies of objects. The ARK shows simulations on a graphical display and allows the user to interact very directly with the simulated objects. One example is a simulation of a solar system with small balls as "planets." The metaphor here is that the computer system works like everyday objects so that you can pick up a planet with the mouse (the cursor is shown as a drawing of a hand on the screen-it changes shape when you grab things) and you can throw a planet if you want to add speed to its orbit. People seem to learn this fairly quickly as it is a literal application of the metaphor (even though we don't go around making photocopies of the Moon everyday). It has of course been recognized for some time that metaphors also are harmful in that they limit more advanced features of a computer system. To overcome this problem, Smith introduces the concept of magic which is defined as those features which cannot be explained by the metaphor. Each element of magic in a design requires its own explanation and thus adds heavily to the learning time. An example of magic in the ARK is the law of gravity in the solar system simulation. The (abstract) concept of the constant of gravity is represented as a (physical) slider which can be used to increase or reduce the force of gravity.</p>

<p>Frank Halasz (who was with MCC during the conference but is now back working at PARC) presented the NoteCards system which was intended as a vehicle for studying the idea processing task. The research goal was to assist analysts of the CIA kind in writing better reports on the basis of lots of collected information. They used the following model of authoring: 1) Collecting sources from databases (computerized tools available) and from brainstorming (currently no tools). 2) Extracting ideas (currently no tools). 3) Analyzing and structuring ideas (currently no tools). 4) Organizing the presentation (tools: outlining processors). 5) Writing the document (tools: text editors). 6) Producing the document (tools: page layout programs).</p>

<p>NoteCards is intended to address the question of the missing tools. Since they did not have experience with existing tools for these tasks, they had to change the way the system worked as their understanding of the task changed. Currently NoteCards is used by about 70-100 users. Most users actually find NoteCards hard to use, first of all because playing with ideas is hard work! Other reasons are that most tasks really do not need the full power of computerized idea processing and that the current system has limitations such as screen space (not big enough to display the full graph of ideas). Another problem is that NoteCards is too general a system which does not include specialized support for special tasks such as legal reasoning. But to support the way a lawyer works, you have to now how a lawyer works! The current system can be tailored by the user but this is not good enough for non-programmers.</p>

<p>NoteCards requires the user to explicitly segment ideas into separate notecards which must be given a title and a classification. Often this is hard to do in the early stages where people do not yet know what they are analyzing. Another problem with the computerized representation of ideas is that they all look the same (typewritten cards). There are no coffee stains, paper clips, or other small reminders of the context in which the idea was generated.</p>

<p>Stu Card and Austin Henderson gave a collaborative talk in which they took turns presenting the theoretical issues and implementation issues of the <strong> Rooms </strong> concept. The general problem is that the metaphor of personal computer desktops is somewhat mistaken. One real desktop corresponds to 42 Macintosh screens. So a single display cannot hold all the information we want to use. Instead they introduced the concept of a room which is a full display holding certain windows which have been grouped for use in some task. When you shift task, you move (through a "door") to another room which has the windows useful for that other task.</p>

<p>This presentation showed a creative use of a videotape to accompany the talk itself. Interactive systems are often hard to describe using words only: You have to see them. So the video program is an important part8 of every user interface conference. More and more of the traditional paper presentations also include short video clips, but the Rooms presentation was illustrated throughout by a videotape instead of by overheads or slides. This of course meant that the speakers had to synchronize their talk with the tape, but they did that with no problems.</p>

<h2>The Politics of Human Factors</h2>

<p>Panels discussions are always good at CHI, and one of the best this year was the panel on the politics of human factors. The word "politics" was not meant as party politics but as company politics: How can we get computer companies to do what we want them to do?</p>

<p>Steve Boies of IBM found three problems with human factors people: 1) Many times behavioral people ignore economic constraints such as the need to market a product before it becomes obsolete. 2) Often their recommendations are difficult to implement compared with their importance. 3) Sometimes the quality of human factors recommendations is too low and opinions are confused with expertise. In one case the first human factors person to be consulted in a project recommended shifting a key from the right to the left on the keyboard. And later, a second human factors person recommended that the same key (which was now on the left) be shifted to the right. But in spite of these problems, Boies (who has a user interface background himself!) said that human factors was so important for computer companies that they should fund it at the same level as e.g. CMOS. Also, he suggested that cheap but good results could be gained from instrumenting systems to measure how they are used in real life.</p>

<p>Charles Grantham from Pacific Bell recommended that human factors should be placed at the corporate level in a company because this is the only place with the overall view required to balance all the tradeoffs. The individual project managers want their product out the door and are tempted to suboptimize user interfaces by choosing solutions which may work in their specific product but which destroys the commonalty between system thus impairing transfer of user learning. John Whiteside from DEC commented that the problem with this approach would be that the higher you are placed, the more thinly you are spread as you touch more and more projects. Grantham's answer was that the value of going to a strategic view of user interfaces would outweigh that disadvantage.</p>

<p>Dennis Wixon discussed the DEC experience with the concept of Usability Engineering Development Cycle which usually in their experience results in a <strong> 30% improvement in operationally defined usability </strong> indices and the solution of about 80% of the usability problems. They simply iterate user interface development and measure usability until reasonable goals are achieved. At some point they reach the point of diminishing returns, but they showed a nice curve of their experience with interface improvements as the result of their method. Two comments to this method was: 1) [Boies] A company like IBM will not switch technology from proven traditional systems unless at least 100% improvement can be shown. Otherwise it is just relatively insignificant tuning. 2) You cannot develop measurable usability specifications in the case where you try to get people to think of totally new systems.</p>

<p>Other comments were that human factors is a legitimate part of engineering and than you only do good engineering if you take human factors into account (Rubinstein, DEC) but that we just don't have the same track record of performance as other engineers at the moment to prove a steady level of improvements as the result of our human factors work (Boies, IBM).</p>

<p>A lot of discussion centered around how to trick management and/or developers into doing what we want: "<strong>whoever gives the best demo wins</strong>" - so give a really neat demo if you want to convince management to things your way. Let the developers feel collective ownership of the user interface solutions-the case where a user interface expert is the only person who "owns" the user interface will often end up as "you can write the HELP-messages;" Unless human factors is an item on the development team's agenda, it will be ignored.</p>

<p>Surprisingly, nobody mentioned the possibility of having the users put pressure on computer companies to develop usable products. It could be a real possibility that big customers got together with a team of user interface experts to develop requirement specifications for user interfaces as well as for other aspects of computer systems. It could also be a job for labor unions to require better user interfaces as part of their quest for better working conditions. Unfortunately, the major trade union for office workers in Denmark is currently focusing its efforts in the computer field on the non-issue of radiation from terminals. They will probably succeed in getting even lower low-level radiation in the workplace but at the cost of poorer working conditions as effort and money is spent on terminal radiation rather than on e.g. graphical workstations allowing a more varied work situation.</p>

<h2>Documentation Graphics</h2>

<p>Since I was chairing the Workshop on Classification of Dialog Techniques, I did not attend as many tutorials during this CHI as I normally like to do. At least some tutorials are good because they offer a chance to dwell at more length on a topic than is possible in a single paper or panel presentation. And the tutorial program at this CHI looked better than ever-partly because of a number of new tutorial speakers who may have been at the conference because it was also this year's Graphics Interface Conference.</p>

<p>I only attended the tutorial on documentation graphics. This is an area which is growing in importance as desktop publishing is spreading to more and more places. Just a few years ago, terms like font and kerning were only used by very few, specialized people and now they are part of the everyday language of both computer scientists and many ordinary users. Chuck Bigelow emphasized that there are very few artifacts in use that are the same today as they were 500 years ago-except letters. So Bigelow wanted us to use the long tradition of typeface design and use-of course taking into account the present limitations of screen and output resolution. Sometimes only small improvements in technology can lead to quite large improvements in readability. One example mentioned by Bigelow was that 100 dots per inch would be a significantly better screen resolution than the 75 or 80 dots per inch now common in most graphics displays.</p>

<p>Much of the tutorial was spent discussing the two page description languages, InterPress and PostScript. These languages are fairly hard to read (I don't think that the designers could have taken the Psychology of Programming tutorial!) which may not be a problem as long as they are only used as an internal representation which is shipped over the network from the computer to the printer. But now more and more applications are being marketed which require the user to write or edit PostScript programs directly. So maybe a new and more readable language is required for presenting a page description to the user. We can still use the standardized PostScript for transmission to the printer.</p>

<p>Several other, more technical issues were covered at this tutorial [i.e. graphics programming]. But one that I would like so single out was the AI-based illustration design system by Jock Mackinlay from Stanford University (now Xerox PARC). This is a system with at the moment 200 rules that knows about good layout and data presentation. Some fairly nice examples of output from the system were shown though it is still far from perfection (and of course is no good at "creative" design such as that done by good advertising agencies). Anyway this kind of system is promising in three ways: 1) It could be used for totally automatic generation of graphics in situations where the underlying data changes so rapidly that the intervention of a human designer is not feasible (e.g. foreign exchange rates). 2) It could be used to generate better first drafts of graphs than those suggested by present "business graphics" systems. A human designer could then refine the graphs if necessary but with far less work than is currently necessary. 3). The system could alleviate the problem of having people (such as myself!) design documents without a sufficient graphics background. We very often see awful designs of reports produced on desktop publishing equipment. If an AI system could design the layout (or critique a human designer), much would be gained.</p>

<h2>Comparing CHIs</h2>

<p>It is interesting to look back and compare the CHI 83, 85, 86, and 87 conferences all of which I have attended. Both I and several of my colleagues still think that CHI'83 was the best: The most exciting atmosphere and the most classical papers. Also, the keynote speeches were more memorable (both Don Norman, Pat Wright, and John Seely Brown) that those at recent CHIs, and they even continue to be of use as valuable references (this is mostly true of Wright's paper) since they are printed in the proceedings. Of course it must be admitted that it may be natural for the first "official" conference in a new field to contain an especially large proportion of classical papers, but even in the category of "interesting but not classical" work, there was still lots of good stuff at CHI'83.</p>

<p>CHI'85 on the other hand was not nearly as exciting with mostly predictable papers. Not too many really new ideas or methods were presented. Even the video tape program was rather dull. The enthusiasm (or at least my enthusiasm) was back at CHI'86 which made you feel part of a field with plenty of action. As Ben Shneiderman said in his keynote talk: We are not just fooling around in small labs just to write yet another paper but we are changing the way people work and relate to computers. Maybe there was a tendency to having each of several established research groups work in refining their kind of special methodology rather than having new and exciting methods come up.</p>

<p>This last tendency was even more true at CHI'87: Certain people work in certain set ways, and there are not too many surprises. Maybe this is a sign of a rapidly maturing field.</p>

<p>The quality of the demonstration program has changed a lot over the years. Maybe I liked the format in 85 best with demonstrations being given as real talks to a large audience: I still remember the cheers and thundering applause after the demonstration of the Pinball Construction Set. In 86 I did not have as much time for the demos but I still attended a few good ones (e.g. Javelin) shown using a video projector. Also, Myron Krueger's Videoplace game room was one of the aspects of CHI'86 that gave me a morale boost as a user interface researcher. This year I was too busy (literally running from one meeting to the next) to spend very much time on the system demonstrations which were somewhat uninspirational: Just a bunch of Suns etc. in a big room [some funny quotes at the Siemens exhibit, though]. On the other hand, this new format does offer more opportunities for audience participation during the demos which I enjoyed talking to Allison Druin, the creator of the furry computer mentioned above.</p>

<h2>Conference Theme</h2>

<p>It seems that the theme of this year's CHI was that we have to design the functionality of systems on the basis of human factors principles if we want usable systems. It is not enough to look at the more surface features of the dialog.</p>

<p>Last year, the theme was the change from looking only at the interaction between one user and one computer to looking at computer-supported cooperative work. I am not sure whether CHI'83 and '85 had any such themes running through a number of conference events. If any, it must have been the need for and success of empiricism at CHI'83 and the existence of rigorous research methods at CHI'85. But in any case, of course each conference has been so rich that a single theme cannot describe it in full. It just seems to me that every year there is one subject that underlies a disproportionately large share of conference presentations and informal discussions among participants.</p>

<h2>A Few Quotations</h2>

<ul>
	<li>John Seely Brown: "The unexpected is the one thing to expect" [when users interact with computers].</li>
	<li>Jack Carroll: "...using paper and ..... the thing you use to write with - My vocabulary is used up by now!" [towards the end of a day having given several talks and panel presentations, including some as last-moment stand-in].</li>
	<li>Peter Carstensen: "If text processing is the white laboratory rat of traditional computer-human interaction research, then it seems electronic mail is the white rat of computer-supported cooperative work research."</li>
</ul>
</section>
  

  
  <section>
    
        
    
  </section>
  
  
  
  
  

  


  <section>
    <br />
    <p class="share"><b>Share <span class="hide-for-small-down">this article</span>:</b> <a href="https://twitter.com/intent/tweet?url=http://www.nngroup.com/articles/trip-report-chi-87/&amp;text=CHI+GI'87%20Trip%20Report&amp;via=nngroup">Twitter</a> | <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=http://www.nngroup.com/articles/trip-report-chi-87/&amp;title=CHI+GI'87%20Trip%20Report&amp;source=Nielsen%20Norman%20Group">LinkedIn</a> | <a href="https://plus.google.com/share?url=http://www.nngroup.com/articles/trip-report-chi-87/">Google+</a> | <a href="mailto:?subject=NN/g Article: CHI+GI&#39;87 Trip Report&amp;body=http://www.nngroup.com/articles/trip-report-chi-87/">Email</a></p>
    <hr />
  </section>

  <footer class="l-subsection">
    
    
    <div class="learn-more">
      <h2>Learn More</h2>
      <p><a href="../subscribe/index.php">Subscribe to the weekly newsletter</a> to get notified about future articles.</p>
      </div>
    
    

    <div class="row show-for-small-down related-content">
      
      
      

      
      
        <div class="column small-12 topic-links">All articles about:
          <ul>
            
            <li><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction</a></li>
            
          </ul>
        </div>
      
      

      
      
        <div class="column small-12">
          <h3>UX Conference Training Courses</h3>
           <ul class="no-bullet">
    
        <li><a href="../../courses/ux-basic-training/index.php">UX Basic Training</a></li>
    
        <li><a href="../../courses/hci/index.php">User Interface Principles Every Designer Must Know</a></li>
    
        <li><a href="../../courses/interaction-design-3-day-course/index.php">Interaction Design: 3-Day Course</a></li>
    
</ul>
        </div>
      
      

      
      
      

      
      
      
    </div>

    <div class="hide-for-small-down row related-content">
      <div class="medium-6 columns">
        
        
          <div>
            <h3>Articles</h3>
            <ul class="no-bullet">
              
                
                <li><a href="../embarrassment/index.php">Computer-Assisted Embarrassment</a></li>
                
              
                
                <li><a href="../enhancement/index.php">The Role of Enhancement in Web Design</a></li>
                
              
                
                <li><a href="../lazy-users/index.php">Why Designers Think Users Are Lazy: 3 Human Behaviors</a></li>
                
              
                
                <li><a href="http://www.jnd.org/dn.mss/apples_products_are.php">Apple&#39;s products are getting harder to use because they ignore principles of design</a></li>
                
              
                
                <li><a href="../efficiency-vs-expectations/index.php">Don’t Prioritize Efficiency Over Expectations</a></li>
                
              
            </ul>
          </div>
        
        

        

      </div>
      <div class="medium-6 columns">
        
          
          <div>
            <h3>UX Conference Training Courses</h3>
             <ul class="no-bullet">
    
        <li><a href="../../courses/ux-basic-training/index.php">UX Basic Training</a></li>
    
        <li><a href="../../courses/hci/index.php">User Interface Principles Every Designer Must Know</a></li>
    
        <li><a href="../../courses/interaction-design-3-day-course/index.php">Interaction Design: 3-Day Course</a></li>
    
</ul>
          </div>
          
        
        
      </div>
    </div>
  </footer>
</article>

        </div>

      

    
  </div>
  <div class="row">
    
      <div class="column small-12 show-for-small-down l-navrail announcements">
        
          
        
      </div>
    
    
  </div>

    </div>
    




<footer id="page-footer">
    <div class="row hide-for-print">
        <div class="column small-12 medium-6">
            <section class="footer-popular">
                
                <h1>Popular Topics</h1>
                <div style="float: left;  width: 15em; display: block;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/applications/index.php">Application Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/e-commerce/index.php">E-Commerce Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/eyetracking/index.php">Eyetracking </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/information-architecture/index.php">Information Architecture </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/intranets/index.php">Intranet Design </a></li>
	<li><a href="../../topic/interaction-design/index.php">Interaction Design </a></li>
</ul>
</div>

<div style="float: left;  width: 15em; display: block; list-style: none;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/mobile-and-tablet-design/index.php">Mobile and Tablet Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/credibility-persuasion/index.php">Persuasive Design</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/research-methods/index.php">Research Methods</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/user-testing/index.php">User Testing </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/web-usability/index.php">Web Usability </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/writing-web/index.php">Web Writing &amp; Content Strategy </a></li>
</ul>
</div>

<p>&nbsp;</p>

                
            </section>
        </div>

        <div class="column small-12 medium-6">
            <div class="row">
                <div class="column small-6">
                    <section class="footer-about">
                        <h1>Follow Us</h1>
                        <ul>
	<li><a href="../subscribe/index.php">Subscribe to our weekly UX newsletter</a>,&nbsp;Jakob Nielsen&#39;s Alertbox</li>
	<li>Visit Don Norman&#39;s website, <a href="http://www.jnd.org/"> www.jnd.org </a></li>
	<li>Visit Bruce Tognazzini&#39;s website, <a href="http://www.asktog.com/"> www.asktog.com </a></li>
	<li style="padding-top: 6px;"><a class="twitter-follow-button" data-show-count="false" data-size="large" href="https://twitter.com/nngroup"><img alt="Follow @nngroup on Twitter" height="20" src="https://media.nngroup.com/media/editor/2016/12/30/twitter-follow-button.png" style="width: 117px; height: 20px;" width="117" /></a></li>
	<li style="padding-top: 6px;"><img alt="" height="14" src="https://media.nngroup.com/media/editor/2016/07/26/feed-icon-14x14.png" style="vertical-align:text-top;width:14px;margin-right:.25em;" width="14" /><a href="../../feed/rss/index.php">Subscribe to our RSS feed</a></li>
</ul>

                    </section>
                </div>

                <div class="column small-6">
                    <section class="footer-about right-column">
                        <h1>About
</h1>
                        <ul class="no-bullet">
	<li><a href="../../about/contact/index.php">Contact information</a></li>
	<li><a href="../../about/index.php">About Us</a></li>
	<li><a href="../../about/why-nng/index.php">Why NN/g? </a></li>
	<li><a href="../../return-policy/index.php">Return Policy</a></li>
</ul>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="copyright column small-12">
            <p><a href="../../copyright-and-reprint-info/index.php">Copyright</a> &copy; 1998-2017 Nielsen Norman Group, All Rights Reserved.</p>
        </div>
    </div>
</footer>


    


        <script src="https://media.nngroup.com/static/vendor/fastclick/js/fastclick.min.js"></script>
        <script src="https://media.nngroup.com/static/vendor/foundation/js/foundation.min.js"></script>
        <script src="http://media.nngroup.com/static/js/app.js"></script>
        <script src="http://media.nngroup.com/static/js/scroll-to-fixed.js"></script><script>var externalLinks = document.querySelectorAll('a[href^="http"]');for (var i = externalLinks.length-1; i >= 0; i--) {externalLinks[i].addEventListener("click", function(event) { alert("External links are disabled for this test."); event.preventDefault(); }, false);}var mailLinks = document.querySelectorAll('a[href^="mailto"]');for (var i = mailLinks.length-1; i >= 0; i--) {mailLinks[i].addEventListener("click", function(event) { alert("Email links are disabled for this test."); event.preventDefault(); }, false);}var externalForms = document.querySelectorAll('form');for (var i = externalForms.length-1; i >= 0; i--) {externalForms[i].addEventListener("submit", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false); externalForms[i].addEventListener("click", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false);}</script>

        <script>
            
            $(function() {
                $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
                $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
            });
        </script>

        
<script src="https://media.nngroup.com/static/js/articles.js"></script>


        
        <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1072699960;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072699960/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
    </body>

<!-- Mirrored from www.nngroup.com/articles/trip-report-chi-87/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:14:37 GMT -->
</html>
