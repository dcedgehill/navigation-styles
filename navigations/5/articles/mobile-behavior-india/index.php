<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->





    
<!-- Mirrored from www.nngroup.com/articles/mobile-behavior-india/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:07:57 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1026.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"beacon":"bam.nr-data.net","queueTime":2,"applicationTime":1805,"agent":"","licenseKey":"1a30ef9931","errorBeacon":"bam.nr-data.net","applicationID":"2048992","transactionName":"NFxSMRBUX0EDUkIPDA0WdhAMVkVbDV8ZBxEXUFMJB0YfRAtUQRVZIktEDAFZVHYHRVcPD01eVRE="}</script>
        <title>Mobile User Behavior in India</title><meta property="og:title" content="Mobile User Behavior in India" />
  
        
        <meta name="description" content="Mobile users in India are concerned with storage and data consumption, share devices, use app lockers for privacy, and log in with one-time passwords.">
        <meta property="og:description" content="Mobile users in India are concerned with storage and data consumption, share devices, use app lockers for privacy, and log in with one-time passwords." />
        
  
        
	
        
        <meta name="keywords" content="mobile, UX, user behavior, India, developing countries, usability. privacy, app locker, ">
        

        

        <meta name="robots" content="noarchive">
        <meta name="viewport" content="width=device-width">

        <link rel="search" type="application/opensearchdescription+xml" title="NN/g Search" href="../../opensearch.xml" />
        <link rel="shortcut icon" href="http://media.nngroup.com/static/img/favicon.ico" />

        <link rel="stylesheet" href="https://media.nngroup.com/static/vendor/font-awesome/css/font-awesome.min.css">

        
        <link rel="stylesheet" href="https://media.nngroup.com/static/css/7232a960f1df.css" type="text/css" />
        <link rel="stylesheet" href="http://fast.fonts.com/t/1.css?apiType=css&amp;projectid=c5846260-9def-46ec-9407-0f1349bd2d64" type="text/css">

        <script src="https://media.nngroup.com/static/vendor/modernizr/js/modernizr.min.js"></script>

        <script type="text/javascript" src="https://media.nngroup.com/static/js/3283be5def74.js"></script>
        

        

        
        
        <!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=229034,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('http://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->


<!-- Google Analytics Tracking Code for http://nngroup.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23132681-1', 'nngroup.com');
  ga('require', 'linkid', 'https://www.nngroup.com/articles/mobile-behavior-india/linkid.js');
  ga('require', 'displayfeatures');

// Optimizely Universal Analytics Integration
 window.optimizely = window.optimizely || [];
 window.optimizely.push("activateUniversalAnalytics");

  ga('send', 'pageview');

</script>
<script>
/**
* Function to track a click on an outbound link in Google Analytics.
*/
function trackOutboundLink(label, url) {
    if(window.ga && ga.loaded) {
        ga('send', 'event', 'outbound', 'click', label, {'hitCallback': function(){document.location = url;}
        });
    } else {
        document.location = url;
    }
}
/**
* Function to track a click on a link in the article announcement area.
*/
$('#articleAnnouncement a').on('click', function(){
  ga('send', 'event', 'link', 'click', 'articleAnnouncement');
});
</script>

<script type="text/javascript">
 setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2421.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



<!-- Hotjar Tracking Code for http://nngroup.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:121605,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

        <script src="https://media.nngroup.com/static/js/analytics/track_outbound_link.js"></script>
        
        
        
    </head>
    <body class="location-articles articles-detail">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
    <header id="page-header">
        <div id="cart" class="shopping-cart mobile-cart show-for-small-down"></div>
        <div class="row">
            <div id="masthead" class="masthead small-12 medium-7 column">

                <!-- The button to toggles menu on small screen -->

                <h1><a href="../../index.php"><img src="http://media.nngroup.com/static/img/logo-wordmark.png" width="74" height="30"> Nielsen <span class="norman">Norman</span> Group</a></h1>
                <p class="tagline show-for-medium-up">Evidence-Based User Experience Research, Training, and Consulting</p>

                <!-- The button to toggles form search on small screen -->


                <div style="clear: right;width:  100%;height: 1px;"></div><?php define('ROOT_PATH',$_SERVER["DOCUMENT_ROOT"] . '/apps/navigation_styles/navigations/5'); ?><?php include(ROOT_PATH)."/navigation.php"; ?>

                <!-- Drop down menu on small screen -->
                <div id="hideNav" class="show-for-small-down">
                  
                  <ul class="menu-content">
                    <li><a href="../../index.php">Home</a></li>
                    <li class="has-submenu">
                      <a>Training</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../training/index.php">Overview</a></li>
                        
<li id="nav-ux-conference"><a href="../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../online-seminars/index.php"><span>Online Seminars</span></a></li>

                      </ul>
                    </li>
                    <li class="has-submenu">
                      <a>Consulting</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
  <li><a href="../../consulting/index.php">Overview</a></li>
  
  <li><a href="../../consulting/benchmarking/index.php">Benchmarking</a></li>
  
  <li><a href="../../consulting/expert-design-review/index.php">Expert Design Review</a></li>
  
  <li><a href="../../consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
  
  <li><a href="../../consulting/usability-testing/index.php">Usability Testing </a></li>
  
  <li><a href="../../consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
  
  <li><a href="../../in-house-training/index.php">In-House Training</a></li>
</ul>

                    </li>

                    <li><a href="../../reports/index.php">Reports</a></li>
                    <li><a href="../index.php">Articles</a></li>
                    <li class="has-submenu"><a>About NN/G</a><i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../about/index.php">Overview</a></li>
                        <li><a href="../../people/index.php">People</a></li>
                        <li><a href="../../about/why-nng/index.php">Why NN/g?</a></li>
                        <li><a href="../../about/contact/index.php">Contact</a></li>
                        <li><a href="../../news/index.php">News</a></li>
                        <li><a href="../../about/history/index.php">History</a></li>
                        <li><a href="../../books/index.php">Books</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                </div>

                <hr class="header-divider show-for-small-only"/>


            </div>
       </div>

        <div id="hideNav">            <nav class="primary-nav small-12 column">
                <ul>
                    <li><a class="navitem" id="nav-home" href="../../index.php">Home</a></li>
                    <li><a class="navitem" id="nav-training" href="../../training/index.php">Training</a></li>
                    <li><a class="navitem" id="nav-consulting" href="../../consulting/index.php">Consulting</a></li>
                    <li><a class="navitem" id="nav-reports" href="../../reports/index.php">Reports</a></li>
                    <li><a class="navitem" id="nav-articles" href="../index.php">Articles</a></li>
                    <li><a class="navitem" id="nav-about" href="../../about/index.php">About NN/G</a></li>
                </ul>
            </nav>
            <nav class="secondary-nav small-12 medium-12 large-12 columns">
                
                
            </nav>
        </div>
    </header>
    <div class="l-content"><span id="preventClose" style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776; Menu</span>
    
  

  

  <div class="row">
    
      
      <section class="small-12 medium-3 columns collapsable-content l-navrail articles-left-column">
        <h1 class="show-for-small-down collapse-link mobile-browse-header">Browse by Topic and Author</h1>
          <div class="row collapsable">

            
            <div class="small-6 medium-12 columns">
              <section id="article-popular-topics" style="display: none;">
                
                <h1>Topics</h1>
                <ul id="article-popular-topics">
                    
                    <li>
                        
                        <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/intranets/all/index.php">Intranets</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/user-testing/all/index.php">User Testing</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                        
                    </li>
                    
                    <li><a href="#" id="article-see-all-topics"><strong>See all topics&hellip;</strong></a></li>
                </ul>
                
              </section>
              <section id="article-all-topics" style="display: none;">
                
                <h1>
                  <span class="show-for-medium-up">All Article Topics</span>
                  <span class="show-for-small-down">All Topics</span>
                  (<a href="#" id="article-hide-all-topics">hide</a>)
                </h1>
                <ul>
                  
                  <li>
                    
                    <a href="../../topic/accessibility/all/index.php">Accessibility</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/agile/all/index.php">Agile</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/analytics-and-metrics/all/index.php">Analytics &amp; Metrics</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/applications/all/index.php">Application Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/b2b-websites/all/index.php">B2B Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/behavior-patterns/all/index.php">Behavior Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/branding/all/index.php">Branding</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/content-strategy/all/index.php">Content Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/corporate-websites/all/index.php">Corporate Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/design-patterns/all/index.php">Design Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-design-process/all/index.php">Design Process</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/email/all/index.php">Email</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/eyetracking/all/index.php">Eyetracking</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/heuristic-evaluation/all/index.php">Heuristic Evaluation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/human-computer-interaction/all/index.php">Human Computer Interaction</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-ideation/all/index.php">Ideation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/information-architecture/all/index.php">Information Architecture</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/interaction-design/all/index.php">Interaction Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/international-users/all/index.php">International Users</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/intranets/all/index.php">Intranets</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-management/all/index.php">Management</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/navigation/all/index.php">Navigation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/non-profit-websites/all/index.php">Non-Profit Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/personas/all/index.php">Personas</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/persuasive-design/all/index.php">Persuasive Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/prototyping/all/index.php">Prototyping</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/psychology-and-ux/all/index.php">Psychology and UX</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/research-methods/all/index.php">Research Methods</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/search/all/index.php">Search</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/social-media/all/index.php">Social Media</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/strategy/all/index.php">Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/user-testing/all/index.php">User Testing</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-teams/all/index.php">UX Teams</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/visual-design/all/index.php">Visual Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/writing-web/all/index.php">Writing for the Web</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/young-users/all/index.php">Young Users</a>
                    
                  </li>
                  
                </ul>
                
              </section>
            </div>
            <script>
              /* Determine which topics list will be displayed: the full or the
               * partial.  We are inlining this close to the list itself to
               * prevent rendering delays on the client
               */
              var topic_list = $.cookie("article-topics");

              var popular_topics = $('#article-popular-topics');
              var all_topics = $('#article-all-topics');

              if(topic_list == 1) {
                  popular_topics.hide();
                  all_topics.show();
              } else {
                  popular_topics.show();
                  all_topics.hide();
              }
            </script>
            

            
            <div class="small-6 medium-12 columns">
              
              <section id="article-initial-authors">
                  <h1>Author</h1>
                  <ul>
                    
                    <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                    
                    <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                    
                    <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                    
                    <li><a href="#" onClick="$('#article-initial-authors').hide(); $('#article-all-authors').show();"><strong>See all authors&hellip;</strong></a></li>
                  </ul>
              </section>
              <section id="article-all-authors" style="display: none;">
                <h1>
                  All Authors
                  (<a href="#" id="article-hide-all-authors">hide</a>)
                </h1>
                <ul>
                  
                  <li><a href="../author/raluca-budiu/index.php">Raluca Budiu</a></li>
                  
                  <li><a href="../author/patty-caya/index.php">Patty Caya</a></li>
                  
                  <li><a href="../author/susan-farrell/index.php">Susan Farrell</a></li>
                  
                  <li><a href="../author/therese-fessenden/index.php">Therese Fessenden</a></li>
                  
                  <li><a href="../author/kim-flaherty/index.php">Kim Flaherty</a></li>
                  
                  <li><a href="../author/sarah-gibbons/index.php">Sarah Gibbons</a></li>
                  
                  <li><a href="../author/aurora-harley/index.php">Aurora Harley</a></li>
                  
                  <li><a href="../author/kate-kaplan/index.php">Kate Kaplan</a></li>
                  
                  <li><a href="../author/page-laubheimer/index.php">Page Laubheimer</a></li>
                  
                  <li><a href="../author/angela-li/index.php">Angie Li</a></li>
                  
                  <li><a href="../author/hoa-loranger/index.php">Hoa Loranger</a></li>
                  
                  <li><a href="../author/kate-meyer/index.php">Kate Meyer</a></li>
                  
                  <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                  
                  <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                  
                  <li><a href="../author/kara-pernice/index.php">Kara Pernice</a></li>
                  
                  <li><a href="../author/christian-rohrer/index.php">Christian Rohrer</a></li>
                  
                  <li><a href="../author/amy-schade/index.php">Amy Schade</a></li>
                  
                  <li><a href="../author/katie-sherwin/index.php">Katie Sherwin</a></li>
                  
                  <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                  
                  <li><a href="../author/kathryn-whitenton/index.php">Kathryn Whitenton</a></li>
                  
                </ul>
              </section>
              
            </div>
            
          </div>

          
          <section class="hide-for-small-down">
            
              
              <h1>Recent Articles</h1>
              
              <ul>
                  
                  <li><a href="../seamless-cross-channel/index.php">Seamlessness in the Omnichannel User Experience</a></li>
                  
                  <li><a href="../brand-intention-interpretation/index.php">Experience Design: Bridging Brand Intention and Brand Interpretation</a></li>
                  
                  <li><a href="../eyetracking-tasks-efficient-scanning/index.php">Scanning Patterns on the Web Are Optimized for the Current Task</a></li>
                  
                  <li><a href="../flat-design-best-practices/index.php">Flat-Design Best Practices</a></li>
                  
                  <li><a href="../ux-user-stories/index.php">Accounting for UX Work with User Stories in Agile Projects</a></li>
                  
                  <li><a href="../index.php"><strong>See all articles&hellip;</strong></a></li>
              </ul>
              
            
          </section>
          

          
          
          <section class="hide-for-small-down">
            <h1>Popular Articles</h1>
            
            <ul>
                
                <li><a href="../usability-101-introduction-to-usability/index.php">Usability 101: Introduction to Usability</a></li>
                
                <li><a href="../top-10-mistakes-web-design/index.php">Top 10 Mistakes in Web Design</a></li>
                
                <li><a href="../how-users-read-on-the-web/index.php">How Users Read on the Web</a></li>
                
                <li><a href="../f-shaped-pattern-reading-web-content/index.php">F-Shaped Pattern For Reading Web Content</a></li>
                
                <li><a href="../ten-usability-heuristics/index.php">10 Usability Heuristics for User Interface Design</a></li>
                
                <li><a href="../intranet-design-2016/index.php">10 Best Intranets of 2016</a></li>
                
                <li><a href="../which-ux-research-methods/index.php">When to Use Which User-Experience Research Methods</a></li>
                
                <li><a href="../response-times-3-important-limits/index.php">Response Times: The 3 Important Limits</a></li>
                
                <li><a href="../why-you-only-need-to-test-with-5-users/index.php">Why You Only Need to Test with 5 Users</a></li>
                
                <li><a href="../page-fold-manifesto/index.php">The Fold Manifesto: Why the Page Fold Still Matters</a></li>
                
            </ul>
          </section>
          
          

          
            <section class="hide-for-small-down">
              
            </section>
          

      </section>
      

      
        <div class="small-12 medium-9 columns content-body">
          




<article class="l-subsection alertbox-article-content article-detail-bl">
  
  <header>
    <div class="l-subsection">
      <h1>Mobile User Behavior in India</h1>
      <div class="author-meta">by
        
          
            
            <a href="../author/samyukta-sherugar/index.php">Samyukta Sherugar</a>
            
            and
            
            <a href="../author/raluca-budiu/index.php">Raluca Budiu</a>
            
          
        on  September 4, 2016
        
      </div>

      

      
      <div class="topic-links">
        Topics:
        
          <ul>

  <li><a href="../../topic/international-users/index.php">International Users</a></li>

  <li><a href="../../topic/mobile-and-tablet-design/index.php">Mobile &amp; Tablet</a></li>

</ul>
        
      </div>
      
    </div>
  </header>
  

  
  
  <section>
    <p class="lede"><strong>Summary:</strong> Mobile users in India are concerned with storage and data consumption, share devices, use app lockers for privacy, and log in with one-time passwords.</p>
    <br />
  </section>
  
  

  
  <section id="article-body"><p>Do mobile-device users behave differently, depending on whether they have a history of using desktop computers before they got a smartphone? We can’t answer this question from user research in developed countries, because most mainstream users have experience with both types of device: for example, only 10% of US adults rely on their smartphone as their <em>only</em> way to access the internet at home, according to a <a href="http://www.pewinternet.org/2015/04/01/us-smartphone-use-in-2015/">Pew Internet Survey</a>. This tiny group of people must differ from mainstream users in many ways, so it does not offer a fruitful way of studying desktop heritage.</p>

<p>In contrast, in developing countries, we often encounter the phenomenon of technology  “leapfrogging,” in which users bypass older technologies and directly adopt newer ones. Specifically, we know that many mainstream users in India use smartphones despite never having used a desktop computer.</p>

<p>To better understand mobile behavior in emerging markets, we conducted a mobile-usability study in India. The study involved 10 users, split into two user groups (each with 5 people):</p>

<ol>
	<li><strong>Mobile-only</strong> users, who accessed the internet mostly through their phones and who had never used desktop computers before.</li>
	<li><strong>Desktop-</strong><strong>heritage</strong> users, who had had significant experience using the internet on a desktop before using a smartphone and who were frequently using a computer to access the internet, in addition to their mobile device.</li>
</ol>

<p>Our study had two main goals: (1) to uncover differences between mobile-only and desktop-heritage users, and (2) to investigate mobile features and behaviors unique to countries such as India.</p>

<h2>Desktop Habits Carry Over          </h2>

<p>Our testing in the Western world shows that, in general, users rarely bookmark web pages on their mobile phones — possibly because bookmarking is a buried feature in most mobile browsers. Overall, we found a relatively low incidence of bookmarking in India as well, but we observed that <strong>bookmarking was more frequent among the desktop-heritage users compared to mobile-only users</strong>. These participants seemed to have carried over their bookmarking habits from the desktop browser to their mobile one.</p>

<p>However, none of the mobile-only users reported using bookmarks on their phones. Some were even unaware they could bookmark websites, and many mentioned that, instead of bookmarking a site, they would just redo the same search again and rely on the history suggestions to recover a previously visited page.</p>

<h2>Concerns About Storage and Data Consumption</h2>

<p>Many of our study participants had inexpensive Android phones with low internal memory (~512MB) and low internal storage (8–16 GB). Additionally, mobile data plans are usually limited in India, and unmonitored use can get expensive. Performance, storage space, and data costs heavily influenced user behavior.</p>

<p>While these issues affected both mobile-only and desktop-heritage users, <strong>desktop-heritage participants seemed to care less about data consumption</strong>, possibly because their more data-intensive browsing was done on their computers.</p>

<p>Users addressed the problem of limited storage by <strong>restricting the number of native apps</strong> installed on their phones. As a result, many users used <a href="../mobile-native-apps/index.php">websites or web apps instead of native apps</a> for their mobile use. <strong>Desktop-heritage and mobile-only users differed in how they decided which native apps to install.</strong></p>

<p>Desktop-heritage users tended to download native apps only for a limited number of <strong>frequent tasks</strong>. (This same finding holds true of users in other parts of the world, but to a less extreme degree: apps are for activities performed often, and mobile-web use is reserved for occasional needs.)</p>

<figure class="caption"><img alt="" border="0" height="622" src="https://media.nngroup.com/media/editor/2016/08/16/1-insufficient_storage.png" width="350"/>
<figcaption><em>One of the users encountered an insufficient-storage issue when she tried to download the Zomato app. While she was making space on her phone for it, she said: </em>“I used to have this app but because my phone ran out of memory I just use the website whenever I need it.”</figcaption>
</figure>

<p>In contrast, <strong>mobile-only users also considered the opportunity cost of not installing an app;</strong> in other words, they were also influenced by the availability and quality of a service in the browser. For example, one of our study participants chose not to install the Facebook native app, although she did use Facebook quite frequently; instead she used Facebook in the browser, because she felt that its mobile site was good enough. However, she did download MiniMovie, an application for creating slideshows, which she was using only occasionally, because, even if she had been able to find a slideshow web app, the experience would not have been as good.</p>

<p>Possibly as a result of their prior experience with the desktop environment, <strong>desktop-heritage users did more task management than mobile-only users: </strong>to address the problem of limited phone memory, many engaged in systematic killing of background apps by swiping them away as often as possible “to prevent the phone from crashing.”</p>

<p><strong>Mobile-only users</strong> had a different solution that tackled both the low memory and the need for data efficiency: <strong>they used lightweight browsers and services that supported off-network sharing of apps and other files among users. </strong>Browsers such as UC Browser and Opera Mini use data-compression technologies and cloud services (that determine the closest server from which to download data) to reduce the page load and minimize the data consumption.  One user explained that [Opera Mini]<em> </em><em>“is less data consuming. Chrome consumes more data. My RAM is too low so I didn't download the Facebook app.</em>”</p>

<figure class="caption"><img alt="" border="0" height="622" src="https://media.nngroup.com/media/editor/2016/08/16/2-facebook-uc-browser.png" width="350"/>
<figcaption><em>Lightweight browsers’ features allowed users to receive notifications from services such as Facebook without having to install the native app. Above is the Facebook login page as seen in the UC Browser. One user explained, </em>“Because it is faster and it consumes less data, I have the Facebook [web] app in UC Browser only. Whatever notification I will get in Facebook, it will automatically show in the mobile screen [notification bar]. Like any friends’ birthdays or if I get any messages.”</figcaption>
</figure>

<p>Users also took advantage of the ability to “save” apps to an SD card and thus expand their phone’s memory. Some even used the browser to download compressed versions of the apps on the SD card, and then expanded and installed those apps locally, thus minimizing data consumption:</p>

<p><em>“If you use UC Browser, you can save apps [i.e., bookmarks to web apps] to the SD card. I can then install it without the net. I never keep too many apps on my phone.</em>”</p>

<p>Services such as SHAREit were used to transfer apps and files between phones located in close proximity without using the internet. In this way, apps could be downloaded from a friend instead of using precious data to do so over the network.</p>

<figure class="caption"><img alt="" border="0" height="610" src="https://media.nngroup.com/media/editor/2016/08/16/3-share-it.png" width="700"/>
<figcaption><em>(Left) The SHAREit app allowed users to transfer files to a nearby phone without using the network. (Right) The files that could be transferred included downloaded apps. One user noted that he uses SHAREit:</em> “for sending and receiving apps... Sometimes when there's no network, I can use SHAREit. And SHAREit consumes less time. If you download on the network, it takes time. SHAREit is faster. I also use it for sharing media files.”</figcaption>
</figure>

<p>Because Indian users are so picky with the apps they choose to download on their phones, companies devise special incentives to convince people that their apps do provide enough added value. Many websites advertise special promotions that are available only in the corresponding app in order to entice users to download the app.</p>

<figure class="caption"><img alt="" border="0" height="622" src="https://media.nngroup.com/media/editor/2016/08/16/4-app-offers.png" width="350"/>
<figcaption><em>Companies such as Lenskart attempted to convince users to download apps by offering special promotions or features available only in the app.</em></figcaption>
</figure>

<p>Other companies such as Flipkart have embraced the users’ needs and provided strong web apps that can work offline and whose user experience is comparable to that of native apps. (Such mobile web apps are sometimes called “progressive.”)</p>

<p>In this world of expensive data and limited storage, bloatware installed on the phone by device manufacturers is particularly hurtful. Smartphones often come with a large number of useless preinstalled apps that cannot be deleted. Our study participants resented these default apps that burdened the phone’s already cramped storage space.</p>

<figure class="caption"><img alt="" border="0" height="649" src="https://media.nngroup.com/media/editor/2016/08/16/5-preinstalled-apps.png" width="700"/>
<figcaption><em>Out of the 17 Google apps preinstalled on one user’s phone, she only used 3: Chrome, Gmail, and YouTube. (Stills from usability-testing video.)</em></figcaption>
</figure>

<h2>Device Sharing and App Lockers</h2>

<p>Shared access to a phone is common in India. Often, there are fewer smartphones than people in the household, and family members share the device at some points in the day. Further, due to cultural differences in the notion of privacy, it is acceptable to borrow a friend or relative’s phone to browse through the picture gallery or to make a call. In these situations it becomes important to have a privacy-control feature that limits access to sensitive information such as text messages, contacts, or Facebook.  </p>

<p>In our study, the most frequent solution to this problem was the use of <strong>app lockers. </strong>App lockers are applications that, once installed, enable users to completely or partially “lock” certain other sensitive apps and allow access only if the user enters a pin or pattern. App lockers can also be configured to hide sensitive data within an application — for example, they may hide a subset of the pictures in the phone’s gallery.</p>

<figure class="caption"><img alt="" border="0" height="610" src="https://media.nngroup.com/media/editor/2016/08/16/6-applocker.png" width="700"/>
<figcaption><em>​(Left) NQ Vault provided both tapp-locking and data-hiding features. (Right)  A password screen controlled access to a locked app. One of our users who used this app explained,</em> “I use Vault for locking. I specially use it to lock the WhatsApp and [Facebook] Messenger...to maintain privacy. So if I open the app I will first have to enter password. Even in notifications it will hide the message...I can also hide the pictures. If any private pictures are there.”</figcaption>
</figure>

<p>App lockers are somewhat of a paradox. In our usability studies, we often see people complaining about <a href="../login-walls/index.php">login walls</a>, yet here’s an instance where they voluntarily raise one to prevent unwanted intrusions into personal information in a world where sharing devices is common. </p>

<h2>One-Time Passwords (OTP) for Registration and Login</h2>

<p>Not every mobile user in India will have an email or a Facebook account, but, by definition, all mobile users will have a phone where they can receive text messages. So apps and websites in India provide the ability to register with a phone number. When the user enters a phone number, the site will text to that phone a numeric code serving as a one-time password (OTP). Users can enter that OTP to register or log in to the site. Often they won’t even have to type the OTP: the site will grab the OTP from the received text message and will enter it into the login screen.  </p>

<figure class="caption"><img alt="" border="0" height="622" src="https://media.nngroup.com/media/editor/2016/08/16/7-otp.png" width="700"/>
<figcaption><em>(Left) Users could enter a phone number to register on the Swiggy app. (Right) Once the number was entered, the user was texted an OTP. For future logins, the user could use either a password entry or OTP verification.</em></figcaption>
</figure>

<p>OTP is also used to facilitate login and registration on the desktop, provided that the user has a mobile phone nearby. The site sends the OTP to the phone, and the user can copy that code on the computer. (This is an example of a <a href="../customer-journeys-omnichannel/index.php">good omnichannel experience</a>, in which users of one channel take advantage of the capabilities of another available channel.)</p>

<figure class="caption"><img alt="" border="0" height="424" src="https://media.nngroup.com/media/editor/2016/08/16/8-otp-website.png" width="700"/>
<figcaption><em>Quickr.com, a site for buying and selling preowned products, offered an email-login option and an OTP option. During each login, a new OTP was generated. No need for remembering passwords!</em></figcaption>
</figure>

<p>Note that the OTP also provides a reasonable solution to the notoriously hard problem of <a href="../passwords-memory/index.php">remembering passwords</a>. It also saves users the effort of typing passwords, which is especially difficult on mobile — regular passwords often involve a combination of special, lower- and upper-case characters that are rarely available on the same mobile keyboard.</p>

<h2>Mobile Phones for Payments:  Cash and Online Wallets</h2>

<p>In India, not everybody has a credit card or a bank account. Mobile phones serve as a substitute for people without these financial instruments.</p>

<figure class="caption"><img alt="" border="0" height="593" src="https://media.nngroup.com/media/editor/2016/08/16/last-payment.png" width="350"/>
<figcaption><em>ICICI Bank allowed users to send money to others through a text message.</em></figcaption>
</figure>

<p>Users <strong>can send money to others via a text message</strong> that contains transaction and authentication details. The receiver then uses those details to withdraw cash from an eligible ATM machine; no bank account or ATM card is needed for this transaction.</p>

<p>Some payment-portal applications like Freecharge allow users to <strong>deposit money into an online wallet</strong> and use it to make payments in physical stores. In Freecharge, this can be done using the <em>Pay Merchant</em> feature:  The user enters a registered mobile number and an app-provided PIN into the point-of-service (POS) machine of the vendor. At the time of payment, the user only needs a mobile phone; no cash nor card.</p>

<h2>Conclusion</h2>

<p>Our study of mobile use in India found few differences in behavior and concerns between mobile-only participants, who relied solely on their phone to access the internet, and desktop-heritage participants, who used both a phone and a computer. While desktop-heritage users were more likely to transfer behaviors learned on the desktop (e.g., bookmarks, managing multitasking) to mobile, both types of users were concerned with storage and data consumption and limited the number of native apps installed on their phones. Desktop-heritage users decided which native apps to install mainly based on expected frequency of use, but mobile-only users also considered the opportunity cost of not installing an app — that is, how likely they were to find the same functionality and experience on the web. Both desktop-heritage and mobile-only participants engaged in phone sharing and used app lockers to protect sensitive information from people who occasionally borrowed their phones.</p>

<p>Our international studies tend to find <a href="../cultural-nuances/index.php">cultural nuances as opposed to major cultural differences</a> in user behavior: people are the same everywhere, and the biggest usability issues stem from the gap between human thinking and computer features which is much greater than any differences between people from different countries.</p>

<p>Unlike earlier studies, this study with mobile users in India apparently departs from the norm: common Indian solutions that we report in this article (e.g., use of app lockers or lite browsers) are almost never seen in our mobile-usability studies with Western audiences. However, these solutions address behaviors encountered everywhere: many of us have occasionally handed our devices to someone else to take a picture or read a headline — just long enough for an <a href="../embarrassment/index.php">embarrassing</a> notification to pop up on the screen. And although in developed countries only a relatively small percentage of users resort to using lite browsers, people do care about their phone storage capability and install applications only if they expect to use them frequently.</p>

<p>Studying and understanding these problems and their solutions in India not only gives us insight into catering to international mobile audiences in developing countries, but it also teaches us how to design better products for a subset of our user demographics (e.g., those who rely on their phone to use the internet) and gives us ideas for solving general-audience problems such as keeping our information safe from accidental, but embarrassing disclosures, and dealing with the difficulties of remembering and typing passwords on the go.</p>
</section>
  

  
  <section>
    
        
    
  </section>
  
  
  
  
  

  


  <section>
    <br />
    <p class="share"><b>Share <span class="hide-for-small-down">this article</span>:</b> <a href="https://twitter.com/intent/tweet?url=http://www.nngroup.com/articles/mobile-behavior-india/&amp;text=Mobile%20User%20Behavior%20in%20India&amp;via=nngroup">Twitter</a> | <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=http://www.nngroup.com/articles/mobile-behavior-india/&amp;title=Mobile%20User%20Behavior%20in%20India&amp;source=Nielsen%20Norman%20Group">LinkedIn</a> | <a href="https://plus.google.com/share?url=http://www.nngroup.com/articles/mobile-behavior-india/">Google+</a> | <a href="mailto:?subject=NN/g Article: Mobile User Behavior in India&amp;body=http://www.nngroup.com/articles/mobile-behavior-india/">Email</a></p>
    <hr />
  </section>

  <footer class="l-subsection">
    
    
    <div class="learn-more">
      <h2>Learn More</h2>
      <p><a href="../subscribe/index.php">Subscribe to the weekly newsletter</a> to get notified about future articles.</p>
      </div>
    
    

    <div class="row show-for-small-down related-content">
      
      
      

      
      
        <div class="column small-12 topic-links">All articles about:
          <ul>
            
            <li><a href="../../topic/international-users/index.php">International Users</a></li>
            
            <li><a href="../../topic/mobile-and-tablet-design/index.php">Mobile &amp; Tablet</a></li>
            
          </ul>
        </div>
      
      

      
      
        <div class="column small-12">
          <h3>UX Conference Training Courses</h3>
           <ul class="no-bullet">
    
        <li><a href="../../courses/cross-channel-user-experience/index.php">Omnichannel Journeys and Customer Experience</a></li>
    
        <li><a href="../../courses/usability-mobile-websites-apps/index.php">Mobile User Experience</a></li>
    
        <li><a href="../../courses/scaling-responsive-design/index.php">Scaling User Interfaces</a></li>
    
        <li>Visual Design for Mobile and Tablet: <a href="../../courses/vis-mob-1/index.php">Day 1</a> and <a href="../../courses/vis-mob-2/index.php">Day 2</a></li>
    
</ul>
        </div>
      
      

      
      
        <div class="small-12 column">
          <h3>Research Reports</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../reports/tablets/index.php">Tablet Website and Application UX</a></li>
              
            
              
                <li><a href="../../reports/mobile-website-and-application-usability/index.php">User Experience for Mobile Applications and Websites</a></li>
              
            
              
                <li><a href="../../reports/enterprise-mobile-showcase/index.php">Mobile Intranets and Enterprise Apps</a></li>
              
            
              
                <li><a href="../../reports/wap-usability/index.php">WAP Usability</a></li>
              
            
              
                <li><a href="../../reports/ipad-app-and-website-usability/index.php">iPad App and Website Usability</a></li>
              
            
          </ul>
        </div>
      
      

      
      
        <div class="small-12 column">
          <h3>Online Seminars</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../online-seminars/every-word-count/index.php">Making Every Word Count</a></li>
              
            
              
                <li><a href="../../online-seminars/mobile-user-testing/index.php">Mobile User Testing</a></li>
              
            
          </ul>
        </div>
      
      
    </div>

    <div class="hide-for-small-down row related-content">
      <div class="medium-6 columns">
        
        
          <div>
            <h3>Articles</h3>
            <ul class="no-bullet">
              
                
                <li><a href="../wechat-integrated-ux/index.php">WeChat: China’s Integrated Internet User Experience</a></li>
                
              
                
                <li><a href="../cultural-nuances/index.php">Cultural Nuances Impact User Experience: Why We Test with International Audiences</a></li>
                
              
                
                <li><a href="../mobile-content/index.php">Reading Content on Mobile Devices</a></li>
                
              
                
                <li><a href="../wechat-qr-shake/index.php">Scan and Shake: A Lesson in Technology Adoption from China’s WeChat</a></li>
                
              
                
                <li><a href="../why-country-sites-are-so-bad/index.php">Why Country Sites Are So Bad</a></li>
                
              
            </ul>
          </div>
        
        

        
          
          <div>
            <h3>Research Reports</h3>
            <ul class="no-bullet">
              
                
                  <li><a href="../../reports/tablets/index.php">Tablet Website and Application UX</a></li>
                
              
                
                  <li><a href="../../reports/mobile-website-and-application-usability/index.php">User Experience for Mobile Applications and Websites</a></li>
                
              
                
                  <li><a href="../../reports/enterprise-mobile-showcase/index.php">Mobile Intranets and Enterprise Apps</a></li>
                
              
                
                  <li><a href="../../reports/wap-usability/index.php">WAP Usability</a></li>
                
              
                
                  <li><a href="../../reports/ipad-app-and-website-usability/index.php">iPad App and Website Usability</a></li>
                
              
            </ul>
          </div>
          
        

      </div>
      <div class="medium-6 columns">
        
          
          <div>
            <h3>UX Conference Training Courses</h3>
             <ul class="no-bullet">
    
        <li><a href="../../courses/cross-channel-user-experience/index.php">Omnichannel Journeys and Customer Experience</a></li>
    
        <li><a href="../../courses/usability-mobile-websites-apps/index.php">Mobile User Experience</a></li>
    
        <li><a href="../../courses/scaling-responsive-design/index.php">Scaling User Interfaces</a></li>
    
        <li>Visual Design for Mobile and Tablet: <a href="../../courses/vis-mob-1/index.php">Day 1</a> and <a href="../../courses/vis-mob-2/index.php">Day 2</a></li>
    
</ul>
          </div>
          
        
        
          
          <div>
            <h3>Online Seminars</h3>
            <ul class="no-bullet">
            
                <li><a href="../../online-seminars/every-word-count/index.php">Making Every Word Count</a></li>
            
                <li><a href="../../online-seminars/mobile-user-testing/index.php">Mobile User Testing</a></li>
            
            </ul>
          </div>
          
        
      </div>
    </div>
  </footer>
</article>

        </div>

      

    
  </div>
  <div class="row">
    
      <div class="column small-12 show-for-small-down l-navrail announcements">
        
          
        
      </div>
    
    
  </div>

    </div>
    




<footer id="page-footer">
    <div class="row hide-for-print">
        <div class="column small-12 medium-6">
            <section class="footer-popular">
                
                <h1>Popular Topics</h1>
                <div style="float: left;  width: 15em; display: block;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/applications/index.php">Application Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/e-commerce/index.php">E-Commerce Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/eyetracking/index.php">Eyetracking </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/information-architecture/index.php">Information Architecture </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/intranets/index.php">Intranet Design </a></li>
	<li><a href="../../topic/interaction-design/index.php">Interaction Design </a></li>
</ul>
</div>

<div style="float: left;  width: 15em; display: block; list-style: none;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/mobile-and-tablet-design/index.php">Mobile and Tablet Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/credibility-persuasion/index.php">Persuasive Design</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/research-methods/index.php">Research Methods</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/user-testing/index.php">User Testing </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/web-usability/index.php">Web Usability </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/writing-web/index.php">Web Writing &amp; Content Strategy </a></li>
</ul>
</div>

<p>&nbsp;</p>

                
            </section>
        </div>

        <div class="column small-12 medium-6">
            <div class="row">
                <div class="column small-6">
                    <section class="footer-about">
                        <h1>Follow Us</h1>
                        <ul>
	<li><a href="../subscribe/index.php">Subscribe to our weekly UX newsletter</a>,&nbsp;Jakob Nielsen&#39;s Alertbox</li>
	<li>Visit Don Norman&#39;s website, <a href="http://www.jnd.org/"> www.jnd.org </a></li>
	<li>Visit Bruce Tognazzini&#39;s website, <a href="http://www.asktog.com/"> www.asktog.com </a></li>
	<li style="padding-top: 6px;"><a class="twitter-follow-button" data-show-count="false" data-size="large" href="https://twitter.com/nngroup"><img alt="Follow @nngroup on Twitter" height="20" src="https://media.nngroup.com/media/editor/2016/12/30/twitter-follow-button.png" style="width: 117px; height: 20px;" width="117" /></a></li>
	<li style="padding-top: 6px;"><img alt="" height="14" src="https://media.nngroup.com/media/editor/2016/07/26/feed-icon-14x14.png" style="vertical-align:text-top;width:14px;margin-right:.25em;" width="14" /><a href="../../feed/rss/index.php">Subscribe to our RSS feed</a></li>
</ul>

                    </section>
                </div>

                <div class="column small-6">
                    <section class="footer-about right-column">
                        <h1>About
</h1>
                        <ul class="no-bullet">
	<li><a href="../../about/contact/index.php">Contact information</a></li>
	<li><a href="../../about/index.php">About Us</a></li>
	<li><a href="../../about/why-nng/index.php">Why NN/g? </a></li>
	<li><a href="../../return-policy/index.php">Return Policy</a></li>
</ul>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="copyright column small-12">
            <p><a href="../../copyright-and-reprint-info/index.php">Copyright</a> &copy; 1998-2017 Nielsen Norman Group, All Rights Reserved.</p>
        </div>
    </div>
</footer>


    


        <script src="https://media.nngroup.com/static/vendor/fastclick/js/fastclick.min.js"></script>
        <script src="https://media.nngroup.com/static/vendor/foundation/js/foundation.min.js"></script>
        <script src="http://media.nngroup.com/static/js/app.js"></script>
        <script src="http://media.nngroup.com/static/js/scroll-to-fixed.js"></script><script>var externalLinks = document.querySelectorAll('a[href^="http"]');for (var i = externalLinks.length-1; i >= 0; i--) {externalLinks[i].addEventListener("click", function(event) { alert("External links are disabled for this test."); event.preventDefault(); }, false);}var mailLinks = document.querySelectorAll('a[href^="mailto"]');for (var i = mailLinks.length-1; i >= 0; i--) {mailLinks[i].addEventListener("click", function(event) { alert("Email links are disabled for this test."); event.preventDefault(); }, false);}var externalForms = document.querySelectorAll('form');for (var i = externalForms.length-1; i >= 0; i--) {externalForms[i].addEventListener("submit", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false); externalForms[i].addEventListener("click", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false);}</script>

        <script>
            
            $(function() {
                $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
                $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
            });
        </script>

        
<script src="https://media.nngroup.com/static/js/articles.js"></script>


        
        <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1072699960;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072699960/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
    </body>

<!-- Mirrored from www.nngroup.com/articles/mobile-behavior-india/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:07:57 GMT -->
</html>
