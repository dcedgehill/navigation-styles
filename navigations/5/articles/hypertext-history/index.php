<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->





    
<!-- Mirrored from www.nngroup.com/articles/hypertext-history/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:13:20 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1026.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"licenseKey":"1a30ef9931","queueTime":6,"beacon":"bam.nr-data.net","errorBeacon":"bam.nr-data.net","applicationID":"2048992","transactionName":"NFxSMRBUX0EDUkIPDA0WdhAMVkVbDV8ZBxEXUFMJB0YfRAtUQRVZIktEDAFZVHYHRVcPD01eVRE=","applicationTime":464,"agent":""}</script>
        <title>History of Hypertext: Article by Jakob Nielsen</title><meta property="og:title" content="History of Hypertext: Article by Jakob Nielsen" />
  
        
        <meta name="description" content="Chapter 3 from Jakob Nielsen&#39;s book, Multimedia and Hypertext, describes the major milestones for hypertext, the internet, and the world wide web, including Vannevar Bush&#39;s Memex and Doug Engelbart&#39;s landmark demo of the online system (NLS.)">
        <meta property="og:description" content="Chapter 3 from Jakob Nielsen&#39;s book, Multimedia and Hypertext, describes the major milestones for hypertext, the internet, and the world wide web, including Vannevar Bush&#39;s Memex and Doug Engelbart&#39;s landmark demo of the online system (NLS.)" />
        
  
        
	
        
        <meta name="keywords" content="hypertext, vannevar bush, doug engelbart, memex">
        

        

        <meta name="robots" content="noarchive">
        <meta name="viewport" content="width=device-width">

        <link rel="search" type="application/opensearchdescription+xml" title="NN/g Search" href="../../opensearch.xml" />
        <link rel="shortcut icon" href="http://media.nngroup.com/static/img/favicon.ico" />

        <link rel="stylesheet" href="https://media.nngroup.com/static/vendor/font-awesome/css/font-awesome.min.css">

        
        <link rel="stylesheet" href="https://media.nngroup.com/static/css/7232a960f1df.css" type="text/css" />
        <link rel="stylesheet" href="http://fast.fonts.com/t/1.css?apiType=css&amp;projectid=c5846260-9def-46ec-9407-0f1349bd2d64" type="text/css">

        <script src="https://media.nngroup.com/static/vendor/modernizr/js/modernizr.min.js"></script>

        <script type="text/javascript" src="https://media.nngroup.com/static/js/3283be5def74.js"></script>
        

        

        
        
        <!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=229034,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('http://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->


<!-- Google Analytics Tracking Code for http://nngroup.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23132681-1', 'nngroup.com');
  ga('require', 'linkid', 'https://www.nngroup.com/articles/hypertext-history/linkid.js');
  ga('require', 'displayfeatures');

// Optimizely Universal Analytics Integration
 window.optimizely = window.optimizely || [];
 window.optimizely.push("activateUniversalAnalytics");

  ga('send', 'pageview');

</script>
<script>
/**
* Function to track a click on an outbound link in Google Analytics.
*/
function trackOutboundLink(label, url) {
    if(window.ga && ga.loaded) {
        ga('send', 'event', 'outbound', 'click', label, {'hitCallback': function(){document.location = url;}
        });
    } else {
        document.location = url;
    }
}
/**
* Function to track a click on a link in the article announcement area.
*/
$('#articleAnnouncement a').on('click', function(){
  ga('send', 'event', 'link', 'click', 'articleAnnouncement');
});
</script>

<script type="text/javascript">
 setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2421.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



<!-- Hotjar Tracking Code for http://nngroup.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:121605,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

        <script src="https://media.nngroup.com/static/js/analytics/track_outbound_link.js"></script>
        
        
        
    </head>
    <body class="location-articles articles-detail">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
    <header id="page-header">
        <div id="cart" class="shopping-cart mobile-cart show-for-small-down"></div>
        <div class="row">
            <div id="masthead" class="masthead small-12 medium-7 column">

                <!-- The button to toggles menu on small screen -->

                <h1><a href="../../index.php"><img src="http://media.nngroup.com/static/img/logo-wordmark.png" width="74" height="30"> Nielsen <span class="norman">Norman</span> Group</a></h1>
                <p class="tagline show-for-medium-up">Evidence-Based User Experience Research, Training, and Consulting</p>

                <!-- The button to toggles form search on small screen -->


                <div style="clear: right;width:  100%;height: 1px;"></div><?php define('ROOT_PATH',$_SERVER["DOCUMENT_ROOT"] . '/apps/navigation_styles/navigations/5'); ?><?php include(ROOT_PATH)."/navigation.php"; ?>

                <!-- Drop down menu on small screen -->
                <div id="hideNav" class="show-for-small-down">
                  
                  <ul class="menu-content">
                    <li><a href="../../index.php">Home</a></li>
                    <li class="has-submenu">
                      <a>Training</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../training/index.php">Overview</a></li>
                        
<li id="nav-ux-conference"><a href="../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../online-seminars/index.php"><span>Online Seminars</span></a></li>

                      </ul>
                    </li>
                    <li class="has-submenu">
                      <a>Consulting</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
  <li><a href="../../consulting/index.php">Overview</a></li>
  
  <li><a href="../../consulting/benchmarking/index.php">Benchmarking</a></li>
  
  <li><a href="../../consulting/expert-design-review/index.php">Expert Design Review</a></li>
  
  <li><a href="../../consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
  
  <li><a href="../../consulting/usability-testing/index.php">Usability Testing </a></li>
  
  <li><a href="../../consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
  
  <li><a href="../../in-house-training/index.php">In-House Training</a></li>
</ul>

                    </li>

                    <li><a href="../../reports/index.php">Reports</a></li>
                    <li><a href="../index.php">Articles</a></li>
                    <li class="has-submenu"><a>About NN/G</a><i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../about/index.php">Overview</a></li>
                        <li><a href="../../people/index.php">People</a></li>
                        <li><a href="../../about/why-nng/index.php">Why NN/g?</a></li>
                        <li><a href="../../about/contact/index.php">Contact</a></li>
                        <li><a href="../../news/index.php">News</a></li>
                        <li><a href="../../about/history/index.php">History</a></li>
                        <li><a href="../../books/index.php">Books</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                </div>

                <hr class="header-divider show-for-small-only"/>


            </div>
       </div>

        <div id="hideNav">            <nav class="primary-nav small-12 column">
                <ul>
                    <li><a class="navitem" id="nav-home" href="../../index.php">Home</a></li>
                    <li><a class="navitem" id="nav-training" href="../../training/index.php">Training</a></li>
                    <li><a class="navitem" id="nav-consulting" href="../../consulting/index.php">Consulting</a></li>
                    <li><a class="navitem" id="nav-reports" href="../../reports/index.php">Reports</a></li>
                    <li><a class="navitem" id="nav-articles" href="../index.php">Articles</a></li>
                    <li><a class="navitem" id="nav-about" href="../../about/index.php">About NN/G</a></li>
                </ul>
            </nav>
            <nav class="secondary-nav small-12 medium-12 large-12 columns">
                
                
            </nav>
        </div>
    </header>
    <div class="l-content"><span id="preventClose" style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776; Menu</span>
    
  

  

  <div class="row">
    
      
      <section class="small-12 medium-3 columns collapsable-content l-navrail articles-left-column">
        <h1 class="show-for-small-down collapse-link mobile-browse-header">Browse by Topic and Author</h1>
          <div class="row collapsable">

            
            <div class="small-6 medium-12 columns">
              <section id="article-popular-topics" style="display: none;">
                
                <h1>Topics</h1>
                <ul id="article-popular-topics">
                    
                    <li>
                        
                        <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/intranets/all/index.php">Intranets</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/user-testing/all/index.php">User Testing</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                        
                    </li>
                    
                    <li><a href="#" id="article-see-all-topics"><strong>See all topics&hellip;</strong></a></li>
                </ul>
                
              </section>
              <section id="article-all-topics" style="display: none;">
                
                <h1>
                  <span class="show-for-medium-up">All Article Topics</span>
                  <span class="show-for-small-down">All Topics</span>
                  (<a href="#" id="article-hide-all-topics">hide</a>)
                </h1>
                <ul>
                  
                  <li>
                    
                    <a href="../../topic/accessibility/all/index.php">Accessibility</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/agile/all/index.php">Agile</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/analytics-and-metrics/all/index.php">Analytics &amp; Metrics</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/applications/all/index.php">Application Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/b2b-websites/all/index.php">B2B Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/behavior-patterns/all/index.php">Behavior Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/branding/all/index.php">Branding</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/content-strategy/all/index.php">Content Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/corporate-websites/all/index.php">Corporate Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/design-patterns/all/index.php">Design Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-design-process/all/index.php">Design Process</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/email/all/index.php">Email</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/eyetracking/all/index.php">Eyetracking</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/heuristic-evaluation/all/index.php">Heuristic Evaluation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/human-computer-interaction/all/index.php">Human Computer Interaction</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-ideation/all/index.php">Ideation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/information-architecture/all/index.php">Information Architecture</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/interaction-design/all/index.php">Interaction Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/international-users/all/index.php">International Users</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/intranets/all/index.php">Intranets</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-management/all/index.php">Management</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/navigation/all/index.php">Navigation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/non-profit-websites/all/index.php">Non-Profit Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/personas/all/index.php">Personas</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/persuasive-design/all/index.php">Persuasive Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/prototyping/all/index.php">Prototyping</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/psychology-and-ux/all/index.php">Psychology and UX</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/research-methods/all/index.php">Research Methods</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/search/all/index.php">Search</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/social-media/all/index.php">Social Media</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/strategy/all/index.php">Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/user-testing/all/index.php">User Testing</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-teams/all/index.php">UX Teams</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/visual-design/all/index.php">Visual Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/writing-web/all/index.php">Writing for the Web</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/young-users/all/index.php">Young Users</a>
                    
                  </li>
                  
                </ul>
                
              </section>
            </div>
            <script>
              /* Determine which topics list will be displayed: the full or the
               * partial.  We are inlining this close to the list itself to
               * prevent rendering delays on the client
               */
              var topic_list = $.cookie("article-topics");

              var popular_topics = $('#article-popular-topics');
              var all_topics = $('#article-all-topics');

              if(topic_list == 1) {
                  popular_topics.hide();
                  all_topics.show();
              } else {
                  popular_topics.show();
                  all_topics.hide();
              }
            </script>
            

            
            <div class="small-6 medium-12 columns">
              
              <section id="article-initial-authors">
                  <h1>Author</h1>
                  <ul>
                    
                    <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                    
                    <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                    
                    <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                    
                    <li><a href="#" onClick="$('#article-initial-authors').hide(); $('#article-all-authors').show();"><strong>See all authors&hellip;</strong></a></li>
                  </ul>
              </section>
              <section id="article-all-authors" style="display: none;">
                <h1>
                  All Authors
                  (<a href="#" id="article-hide-all-authors">hide</a>)
                </h1>
                <ul>
                  
                  <li><a href="../author/raluca-budiu/index.php">Raluca Budiu</a></li>
                  
                  <li><a href="../author/patty-caya/index.php">Patty Caya</a></li>
                  
                  <li><a href="../author/susan-farrell/index.php">Susan Farrell</a></li>
                  
                  <li><a href="../author/therese-fessenden/index.php">Therese Fessenden</a></li>
                  
                  <li><a href="../author/kim-flaherty/index.php">Kim Flaherty</a></li>
                  
                  <li><a href="../author/sarah-gibbons/index.php">Sarah Gibbons</a></li>
                  
                  <li><a href="../author/aurora-harley/index.php">Aurora Harley</a></li>
                  
                  <li><a href="../author/kate-kaplan/index.php">Kate Kaplan</a></li>
                  
                  <li><a href="../author/page-laubheimer/index.php">Page Laubheimer</a></li>
                  
                  <li><a href="../author/angela-li/index.php">Angie Li</a></li>
                  
                  <li><a href="../author/hoa-loranger/index.php">Hoa Loranger</a></li>
                  
                  <li><a href="../author/kate-meyer/index.php">Kate Meyer</a></li>
                  
                  <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                  
                  <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                  
                  <li><a href="../author/kara-pernice/index.php">Kara Pernice</a></li>
                  
                  <li><a href="../author/christian-rohrer/index.php">Christian Rohrer</a></li>
                  
                  <li><a href="../author/amy-schade/index.php">Amy Schade</a></li>
                  
                  <li><a href="../author/katie-sherwin/index.php">Katie Sherwin</a></li>
                  
                  <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                  
                  <li><a href="../author/kathryn-whitenton/index.php">Kathryn Whitenton</a></li>
                  
                </ul>
              </section>
              
            </div>
            
          </div>

          
          <section class="hide-for-small-down">
            
              
              <h1>Recent Articles</h1>
              
              <ul>
                  
                  <li><a href="../seamless-cross-channel/index.php">Seamlessness in the Omnichannel User Experience</a></li>
                  
                  <li><a href="../brand-intention-interpretation/index.php">Experience Design: Bridging Brand Intention and Brand Interpretation</a></li>
                  
                  <li><a href="../eyetracking-tasks-efficient-scanning/index.php">Scanning Patterns on the Web Are Optimized for the Current Task</a></li>
                  
                  <li><a href="../flat-design-best-practices/index.php">Flat-Design Best Practices</a></li>
                  
                  <li><a href="../ux-user-stories/index.php">Accounting for UX Work with User Stories in Agile Projects</a></li>
                  
                  <li><a href="../index.php"><strong>See all articles&hellip;</strong></a></li>
              </ul>
              
            
          </section>
          

          
          
          <section class="hide-for-small-down">
            <h1>Popular Articles</h1>
            
            <ul>
                
                <li><a href="../usability-101-introduction-to-usability/index.php">Usability 101: Introduction to Usability</a></li>
                
                <li><a href="../top-10-mistakes-web-design/index.php">Top 10 Mistakes in Web Design</a></li>
                
                <li><a href="../how-users-read-on-the-web/index.php">How Users Read on the Web</a></li>
                
                <li><a href="../f-shaped-pattern-reading-web-content/index.php">F-Shaped Pattern For Reading Web Content</a></li>
                
                <li><a href="../ten-usability-heuristics/index.php">10 Usability Heuristics for User Interface Design</a></li>
                
                <li><a href="../intranet-design-2016/index.php">10 Best Intranets of 2016</a></li>
                
                <li><a href="../which-ux-research-methods/index.php">When to Use Which User-Experience Research Methods</a></li>
                
                <li><a href="../response-times-3-important-limits/index.php">Response Times: The 3 Important Limits</a></li>
                
                <li><a href="../why-you-only-need-to-test-with-5-users/index.php">Why You Only Need to Test with 5 Users</a></li>
                
                <li><a href="../page-fold-manifesto/index.php">The Fold Manifesto: Why the Page Fold Still Matters</a></li>
                
            </ul>
          </section>
          
          

          
            <section class="hide-for-small-down">
              
            </section>
          

      </section>
      

      
        <div class="small-12 medium-9 columns content-body">
          




<article class="l-subsection alertbox-article-content article-detail-bl">
  
  <header>
    <div class="l-subsection">
      <h1>The History of Hypertext</h1>
      <div class="author-meta">by
        
          
            
            <a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a>
            
          
        on  February 1, 1995
        
      </div>

      

      
      <div class="topic-links">
        Topics:
        
          <ul>

  <li><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction</a></li>

</ul>
        
      </div>
      
    </div>
  </header>
  

  
  
  <section>
    <p class="lede"><strong>Summary:</strong> Hypertext has a surprisingly rich history compared to most phenomena in the personal computer industry, especially considering that most people had not heard of it until a few years ago. I have been to talks at major conferences where the speakers were ignorant of any hypertext developments preceding the introduction of the WWW. Table 3.1 gives an overview of the history of hypertext; the major events are discussed in more detail in this chapter.</p>
    <br />
  </section>
  
  

  
  <section id="article-body"><p><em>This is chapter 3 from Jakob Nielsen's book <a href="../../books/multimedia-and-hypertext/index.php"> Multimedia and Hypertext: The Internet and Beyond</a>, Morgan Kaufmann Publishers, 1995. (For full literature references, please see the bibliography in the book.) </em></p>

<h2>Memex (1945)</h2>

<p>Vannevar Bush (1890–1974) is normally considered the "grandfather" of hypertext, since he proposed a system we would now describe as a hypertext system as long ago as 1945. This system, the Memex ("memory extender"), was never implemented, however, but was only described in theory in Bush's papers.</p>

<p>Bush actually developed some of his ideas for the Memex in 1932 and 1933 and finally wrote a draft paper on it in 1939. For various reasons [Nyce and Kahn 1989, 1991] this manuscript was not published until 1945, when it appeared in the <cite> Atlantic Monthly </cite> under the title " <a href="http://www.theatlantic.com/doc/194507/bush" title="Full text of article, at The Atlantic Monthly"> As We May Think</a>."</p>

<p>Bush described the Memex as "a sort of mechanized private file and library" and as "a device in which an individual stores his books, records, and communications, and which is mechanized so that it may be consulted with exceeding speed and flexibility." The Memex would store this information on microfilm, which would be kept in the user's desk. This desk was intended to have several microfilm projection positions to enable the user to compare different microfilms, in a manner very similar to the windows that became popular on personal computers more than forty years later.</p>

<p>The Memex would have a scanner to enable the user to input new material, and it would also allow the user to make handwritten marginal notes and comments. But Bush envisaged that</p>

<blockquote>most of the Memex contents are purchased on microfilm ready for insertion. Books of all sorts, pictures, current periodicals, newspapers, are thus obtained and dropped into place. Business correspondence takes the same path.</blockquote>

<p> </p>

<table style="border: solid 1px gray; padding: 1ex;">
	<caption><em><strong>Table 3.1. </strong> Overview of the history of hypertext. </em></caption>
	<tbody>
		<tr>
			<td><strong>1945 </strong> <a href="http://www.amazon.com/exec/obidos/ISBN=0262740222/useitcomusableinA/"> Vannevar Bush</a> proposes <em> <a href="http://www.press.umich.edu/jep/works/vbush/"> Memex</a></em><br/>
			<strong>1965 </strong> <a href="http://www.xanadu.com.au/"> Ted Nelson</a> coins the word " <em> hypertext </em> " (later elaborated in his pioneering book Literary Machines)<br/>
			<strong>1967 </strong> The <em> Hypertext Editing System </em> and FRESS, Brown University, <a href="http://www.cs.brown.edu/people/avd/"> Andy van Dam</a><br/>
			<strong>1968 </strong> Doug <a href="http://www.dougenglebart.org/"> Engelbart </a> demo of <em> NLS </em> system at FJCC<br/>
			<strong>1975 </strong> <em> ZOG </em> (now KMS): CMU<br/>
			<strong>1978 </strong> <em> Aspen Movie Map </em> , first hypermedia videodisk, Andy Lippman, MIT Architecture Machine Group (now <a href="http://www.media.mit.edu/"> Media Lab</a>)<br/>
			<strong>1984 </strong> Filevision from Telos; limited hypermedia database widely available for the Macintosh<br/>
			<strong>1985 </strong> <em> Symbolics Document Examiner </em> , Janet Walker<br/>
			<strong>1985 </strong> <em> Intermedia </em> , Brown University, Norman Meyrowitz<br/>
			<strong>1986 </strong> OWL introduces <em> Guide </em> , first widely available hypertext<br/>
			<strong>1987 </strong> <a href="http://www.apple.com/"> Apple</a> introduces <em> HyperCard </em> , Bill Atkinson<br/>
			<strong>1987 </strong> <em><a href="../trip-report-hypertext-87/index.php"> Hypertext'87</a> </em>first major conference on hypertext<br/>
			<strong>1991 </strong> <em> <a href="http://www.w3.org/WWW/"> World Wide Web</a></em>at <a href="http://www.cern.ch/"> CERN</a> becomes first global hypertext, Tim Berners-Lee<br/>
			<strong>1992 </strong> <cite> New York Times Book Review </cite> cover story on hypertext fiction<br/>
			<strong>1993 </strong> <em> <a href="http://www.ncsa.uiuc.edu/SDG/Software/Mosaic/NCSAMosaicHome.php"> Mosaic</a> </em>anointed Internet killer app, National Center for Supercomputing Applications<br/>
			<strong>1993 </strong> <em> <a href="http://www.amazon.com/exec/obidos/ISBN=1559402652/useitcomusableinA/"> A Hard Day's Night</a> </em>becomes the first full-length feature film in hypermedia (originally for Macintosh; now also <a href="http://www.amazon.com/exec/obidos/ISBN=1559404914/useitcomusableinA/"> available for Windows</a>)<br/>
			<strong>1993 </strong> Hypermedia encyclopedias sell more copies than print encyclopedias<br/>
			<strong>1995 </strong> Netscape Corp. gains market value of almost $3B on first day of stock market trading (1998: AOL buys Netscape for $4B)</td>
		</tr>
	</tbody>
</table>

<p>Actually we have not yet reached the state of hypertext development where there is a significant amount of preprocessed information for sale that can be integrated with a user's existing hypertext structure.</p>

<p>The main reason Vannevar Bush developed his proposal for the Memex was that he was worried about the explosion of scientific information which made it impossible even for specialists to follow developments in a discipline. Of course, this situation is much worse now, but even in 1945 Bush discussed the need to allow people to find information more easily than was possible on paper. After having described his various ideas for microfilm and projection equipment, he stated that</p>

<blockquote>All this is conventional, except for the projection forward of present-day mechanisms and gadgetry. It affords an immediate step, however, to associative indexing, the basic idea of which is a provision whereby any item may be caused at will to select immediately and automatically another. This is the essential feature of the memex. The process of tying two items together is the important thing.</blockquote>

<p>Hypertext, in other words!</p>

<p>In addition to the establishment of individual links, Bush wanted the Memex to support the building of trails through the material in the form of a set of links that would combine information of relevance for a specific perspective on a specific topic. He even forecast the establishment of a new profession of "trail blazers,""who find delight in the task of establishing useful trails through the enormous mass of the common record." In current terminology, these trail blazers would be people who add value to published collections of text and other information by providing a web of hypertext links to supplement the basic information. But since we do not even have a market for basic hypertexts yet, we unfortunately have to do without professional trail blazers. Amateur trail blazers have come into existence in recent years in the form of people who list WWW sites they find interesting on their home page.</p>

<p>The building of trails would also be an activity for the ordinary Memex user, and using his microfilm ideas, Bush assumed that such a user might want to photograph a whole trail for friends to put in their Memexes. Again we should note that current technology is not up to Bush's vision, since it is almost impossible to transfer selected subsets of a hypertext structure to another hypertext, especially if the two hypertexts are based on different systems.</p>

<p>Vannevar Bush was a famous scientist in his days and was the science advisor to President Roosevelt during the Second World War, when science-based issues like inventing nuclear weapons were of great importance. After "As We May Think" ran in the <cite> Atlantic Monthly </cite> , it caused considerable discussion, and both <cite> Time </cite> and <cite> Life </cite> ran stories on the Memex. <cite> Life </cite> even had an artist draw up illustrations of how the Memex would look and a scenario of its projection positions as the user was completing a link. Doug Engelbart, who later became a pioneer in the development of interactive computing and invented the mouse, got part of his inspiration by reading Bush's article while waiting for a ship home from the Philippines in 1945.</p>

<p>In spite of all this early interest surrounding the Memex it never got built. As hinted above, our current computer technology is still not able to support Bush's vision in its entirety [Meyrowitz 1989b]. We do have computers with most of the Memex functionality but they are based on a completely different technology from the microfilm discussed by Bush.</p>

<p>It is interesting to recall that Bush was one of the pioneering scientists in the development of computer hardware and was famous for such inventions as the MIT Differential Analyzer in 1931. Alan Kay from Apple has suggested that the areas about which we know most may be those where we are most <em> in </em> accurate in predicting the future, since we see all the problems inherent in them. Therefore Bush could gladly dream about impossible advances in microfilm technology but he would have been reluctant to publish an article about personal computing since he "knew" that computers were huge things costing millions of dollars.</p>

<h2>Augment/NLS (1962–1976)</h2>

<p>After Bush's article from 1945, nothing much happened in the hypertext field for twenty years. People were busy improving computers to the point where it would be feasible to use them interactively, but they were so expensive that most funding agencies viewed as completely irresponsible the suggestion that computer resources should be wasted on nonnumeric tasks such as text processing.</p>

<p>In spite of this attitude, Doug Engelbart started work in 1962 on his Augment project, developing computer tools to augment human capabilities and productivity. This project was the first major work in areas like office automation and text processing; in fact the entire project was much more ambitious and broad in scope than the productivity tools we currently enjoy in the professional work environment. The project was conducted at SRI (Stanford Research Institute) with a staff that grew to 45 people.</p>

<p>One part of the Augment project was NLS (for oN-Line System), which had several hypertext features even though it was not developed as a hypertext system. (The reason for the strange acronym was to distinguish the name from that of the oFf-Line System, FLS.) During the Augment project, the researchers stored all their papers, reports, and memos in a shared "journal" facility that enabled them to include cross-references to other work in their own writings. This journal grew to over 100,000 items and is still unique as a hypertext structure for support of real work over an extended time.</p>

<p>In 1968 Engelbart gave a demo of NLS at a special session of the 1968 Fall Joint Computer Conference. Giving this first public demo of many of the basic ideas in interactive computing was something of gamble for the group. Engelbart had to use much of his grant money to obtain special video projectors, run microwave transmission lines between his lab and the conference center, and get other kinds of specialized hardware built, and he would have been in big trouble if the demo had failed. But it worked, and in retrospect spending the money was the right decision; many people have said that it was that demo that got them fired up about inventing interactive computing.</p>

<p>In spite of the successful demo, the government dropped its research support of Engelbart in 1975 at a time when he had more or less invented half the concepts of modern computing. (After the Augment project was as good as terminated, several people from Engelbart's staff went on to Xerox PARC and helped invent many of the second half of the concepts of modern computing.) Augment continued as an office automation service but was not really developed further. Engelbart himself is still pushing his original augmentation ideas and a few years ago started the "Bootstrap Project," located at Stanford University.</p>

<h2>Xanadu (1965)</h2>

<p>The actual word "hypertext" was coined by Ted Nelson in 1965. Nelson was an early hypertext pioneer with his Xanadu system, which he has been developing ever since. Parts of Xanadu do work and have been a product from the Xanadu Operating Company since 1990.</p>

<p>The Xanadu <em> vision </em> has never been implemented, however, and probably never will be (at least not in the foreseeable future). The basic Xanadu idea is that of a repository for everything that anybody has ever written and thereby of a truly universal hypertext. Nelson views hypertext as a literary medium; and he believes that "everything is deeply intertwingled" and therefore has to be online together. Nelson's main book on hypertext is actually entitled <cite> Literary Machines </cite> . Robert.; Glushko [1989b], in contrast, believes that multidocument hypertext is only called for in comparatively few cases where users have explicit tasks that require the combination of information.</p>

<p>If Nelson's vision of having all the world's literature in a single hypertext system is to be fulfilled, it will obviously be impossible to rely on local storage of information in the user's own personal computer. Indeed, Nelson's Xanadu design is based on a combination of back end and local databases, which would enable fast response for most hypertext access since the information used the most by individual users would still be stored on their local computers. Whenever the user activates a link to more exotic information, the front end computer transparently links to the back end repository through the network and retrieves the information.</p>

<p>In Xanadu it is possible to address any substring of any document from any other document. In combination with the distributed storage of information, this capability means that Xanadu includes a scheme for giving a unique address to every single byte in the world if there should be a need for it.</p>

<p>Furthermore, the full Xanadu system will never delete any text, not even when new versions are added to the system, because other readers may have added links to the previous version of the text. This permanent record of all versions makes it possible for other documents to link either with the current version of a document or with a specific version. Frequently one will want to link with the most up-to-date material, as when referring to census statistics or weather forecasts, but in more polemic documents one may want to ensure a reference to a specific version of a position one is arguing against.</p>

<p>The reader of a document linked to a specific version of another document will always have the option of asking the system to display the most current version. This "temporal scrolling" can also be used to show how documents have looked in previous versions and can be useful, for instance for version management of software development.</p>

<p>Nelson does realize that this scheme means that billions of new bytes will have to be added to Xanadu every day without the hope of freeing storage by deleting old documents. His comment is, "So what...?" and a reference to the fact that the current load on the telephone system would have been impossible under the traditional technology of human operators connecting every call. The history of computer technology until now does give reason for some optimism with respect to being able to support the Xanadu vision some time in the future.</p>

<p>When everything is online in a single system and when everybody may link with everybody else, there will be tremendous copyright problems if the traditional view of copyright is maintained [Samuelson and Glushko 1991]. Nelson's answer is to abolish the traditional copyright to the extent that information placed in Xanadu will always be available to everybody. This principle may be feasible; the system would still keep track of original authorship and provide royalties to the original author based on the number of bytes seen by each reader.</p>

<p>Publishing an anthology would be a simple matter of creating a new document with some explanatory and combining text and with links to the original documents by other authors who would not need to be contacted for permission. Because of the royalty, everybody would be financially motivated to allow other people to link with their work, since it will be through the links that readers discover material worth reading. Even so, some authors might fear being quoted out of context or having their work misrepresented by other authors. This problem is taken care of in theory in Xanadu because the reader always has the option of asking for the complete text of any document being quoted by a link. In practice, many readers will probably not bother looking at the full text of linked documents, so one might need a mechanism for allowing authors to flag links to their work with an attribute indicating that they believe that the link is misleading.</p>

<p>During some of his early work on Xanadu, Ted Nelson was associated with Brown University (Providence, RI). Since then he has mostly been an independent visionary and author, though he was with Autodesk, Inc. for some time.</p>

<h2>Hypertext Editing System (1967) and FRESS (1968)</h2>

<p>Even though Xanadu was not even partly implemented until recently, hypertext systems were built at Brown University in the 1960s under the leadership of Andries van Dam. The Hypertext Editing System built in 1967 was the world's first working hypertext system. It ran in a 128K memory partition on a small IBM/360 mainframe and was funded by an IBM research contract.</p>

<p>After the Hypertext Editing System was finished as a research project at Brown University, IBM sold it to the Houston Manned Spacecraft Center, where it was actually used to produce documentation for the Apollo missions.</p>

<p>The second hypertext system was FRESS (File Retrieval and Editing System), which was done at Brown University in 1968 as a follow-up to the Hypertext Editing System and was also implemented on an IBM mainframe. Because of this extremely stable platform, it was actually possible to run a demonstration of this code, more than twenty years old, at the 1989 ACM Hypertext conference.</p>

<p>Both these early hypertext systems had the basic hypertext functionality of linking and jumping to other documents, but most of their user interface was text-based and required indirect user specification of the jumps.</p>

<p>Brown University has been a major player in the hypertext field ever since, with its most prominent effort being the development of the Intermedia system (discussed further later in this chapter).</p>

<h2>Aspen Movie Map (1978)</h2>

<p>Probably the first hyper <em> media </em> system was the <cite> Aspen Movie Map </cite> developed by Andrew Lippman and colleagues at the MIT Architecture Machine Group (which has now merged with other MIT groups to form the Media Lab). Aspen was a surrogate travel application that allowed the user to take a simulated "drive" through the city of Aspen on a computer screen.</p>

<p>The Aspen system was implemented with a set of videodisks containing photographs of all the streets of the city of Aspen, Colorado. Filming was done by mounting four cameras aimed at 90° intervals on a truck that was driven through all the city streets, each camera taking a frame every ten feet (three meters). The hypermedia aspects of the system come from accessing these pictures not as a traditional database ("show me 149 Main Street") but as a linked set of information.</p>

<p>Each photograph was linked to the other relevant photographs a person would see by continuing straight ahead, backing up, or moving to the left or to the right. The user navigated the information space by using a joystick to indicate the desired direction of movement, and the system retrieved the relevant next picture. The resulting feeling was that of driving through the city and being able to turn at will at any intersection. The videodisk player could in theory display the photos as quickly as one frame per 33 millisecond, which would correspond to driving through the streets at 200 mph (330 km/h). To achieve a better simulation of driving, the actual system was slowed down to display successive photos with a speed depending on the user's wishes, but no faster than ten frames/sec., corresponding to a speed of 68 mph (110 km/h).</p>

<p>It was also possible for the user to stop in front of a building and "walk" inside, since many of the buildings in Aspen had been filmed for the videodisk. As a final control, the user could select the time of year for the drive by a "season knob," since the entire town was recorded in both fall and winter. This concept is somewhat related to the "temporal scrolling" in the Xanadu system described above. But the Aspen season knob is probably easier to understand for users because it relates directly to a well-known concept from the real world even though it provides a functionality that would be impossible in the real world. Figures 3.1 and 3.2 show the use of a similar feature in the more recent Ecodisc system. The Ecodisc is an instructional hypertext for learning about ecology by allowing the user to move about a lake and observe its varied habitats [Nielsen 1990e].</p>

<p>The Aspen system used two monitors for its interface, but in a more natural way than the traditional two-screen solution discussed in Chapter 7. One monitor was a regular vertical screen and showed the street images filmed from the truck. This provided users with an immersive view of the city and made them feel as if they had entered into the environment. The second screen was horizontal and placed in front of the immersive screen. Used to show a street map, it provided the user with an overview of the environment. The user could point to a spot on the map and jump directly to it instead of having to navigate through the streets. The overview map also provided "landmarks" by highlighting the two main streets of the city. This two-screen solution allowed users easily to understand their position relative to these two main streets.</p>

<p>One reason for the availability of funding to build surrogate travel applications in the late 1970s was the successful liberation of hostages from the Entebbe airport by Israeli troops. Even though these soldiers had never been to Uganda before, they were able to carry through their mission extremely well because they had practiced in a full-scale mockup of the airport that had been built in Israel. Computerized surrogate travel systems might make it possible to train for similar missions in the future without actually having to build entire mockup cities.</p>

<p>It is also possible to imagine that surrogate travel systems like Aspen might be used on a routine basis in the future not just by commando soldiers training for a mission but also by tourists planning their vacations. In the near future, however, the main use of surrogate travel will probably be for educational use; the Palenque system described further in Chapter 4 is a good example.</p>

<p>The Aspen system itself was not really an "application" in the sense that it actually helped anybody accomplish anything. But it was far ahead of its time and of great historical significance in showing the way for future applications. Even now, almost twenty years after the Aspen project was completed, it still stands as one of the more sophisticated hypermedia systems ever built.</p>

<p>As a follow-up to Aspen, the MIT Architecture Machine Group built a more practically oriented system using hypermedia technology to integrate video and computer data. This project was called the <cite> Movie Manual </cite> and involved car and bicycle repair manuals. It is discussed further in Chapter 4.</p>

<p>The <cite> Movie Manual </cite> could use either a regular touch-sensitive computer display or it could project its image on an entire wall in a media room. It had a picture of a car as the table of contents and allowed the user to point to the area that needed repair. The <cite> Movie Manual </cite> would then show its instructions in a mixture of video, annotated images, and ordinary text, allowing the user to customize the screen layout by making the video window larger or smaller. The user could stop the video or play it faster, slower, or backward.</p>

<p><img alt="Screenshot from Ecodisk, showing the lake in summer" src="http://media.nngroup.com/media/editor/2012/10/31/ecodisk-summer.png" style="width: 512px; height: 342px;"/><br/>
<em><strong>Figure 3.1. </strong> View of an area at a lake during the summer from the Ecodisc system. The user can "turn" to look in another direction by clicking the radio buttons in the inner part of the compass rose and can "move" to another location by clicking the arrows in the outer part of the compass rose. Clicking on the snow crystal activates movement in time to the winter view in Figure 3.2, as an example of temporal navigation. Copyright © 1990 by ESM, Ltd., reprinted by permission. </em></p>

<p><img alt="Screenshot from Ecodisk, showing the lake in winter" src="http://media.nngroup.com/media/editor/2012/10/31/ecodisk-winter.png" style="width: 512px; height: 342px;"/><br/>
<em><strong>Figure 3.2. </strong> Winter view of the same part of the Ecodisc lake as that shown in Figure 3.1. Unfortunately the two photographs are not perfectly aligned, indicating the need for extreme precision when recording the camera positions and angles in the production of hypermedia with multiple views of the same scenes. Copyright © 1990 by ESM, Ltd., reprinted by permission. </em></p>

<h2>KMS (1983)</h2>

<p>KMS probably has the distinction of being the oldest among the currently popular hypertext systems since it is a direct descendant of the ZOG research system developed at Carnegie Mellon University with some development as early as 1972 and as a full-scale project from 1975 [Robertson et al. 1981]. The word ZOG does not mean anything but was chosen because it "is short, easily pronounced and easily remembered." At first, ZOG ran on mainframe computers; it was then moved to PERQ workstations, 28 of which were installed on the aircraft carrier USS <em> Carl Vinson </em> in 1983 for a field test of such applications as a maintenance manual for weapons elevators.</p>

<p>KMS is an abbreviation for Knowledge Management System and has been a commercial product since 1983. It runs on Unix workstations and has been used for a large number of applications. KMS is designed to manage fairly large hypertexts with many tens of thousands of nodes and has been designed from the start to work across local area networks.</p>

<p>KMS has a very simple data structure based on a single type of node called the frame. A frame can take over the entire workstation screen, but normally the screen is split into two frames, each of which is about as big as a letter-sized page of paper. Users cannot mix small and large nodes and cannot have more than two nodes on the screen at the same time. This might seem limiting at first but proponents of KMS claim that it is much better to use the hypertext navigation mechanism to change the contents of the display than to have to use window management operations to find the desired information among many overlapping windows.</p>

<p>KMS has been optimized for speed of navigation, so the destination frame will normally be displayed "instantaneously" as the user clicks the mouse on an anchor. The time to display a new frame is actually about a half-second, and the designers of KMS claim that there is no real benefit to being faster than that. They tried an experimental system to change the display in 0.05 seconds, but that was so fast that users had trouble noticing whether or not the screen had changed.</p>

<p>If an item on the screen is not linked to another node, then clicking on it will generate an empty frame, making node and link creation seem like a special form of navigation to the user. It is also possible for a click on an item to run a small program written in the special KMS action language. This language is not quite as general as the integrated InterLisp in NoteCards, but it still allows the user to customize KMS for many special applications. See for example the discussion in Chapter 4 of the use of KMS to support the research of a biologist.</p>

<p>KMS does not provide an overview diagram but instead relies on fast navigation and a hierarchical structure of the nodes. Links across the hierarchy are prefixed with an "@" to let users know that they are moving to another part of the information space. Two additional facilities to help users navigate are the landmark status of a special "home" frame, which is directly accessible from any location, and the special ease and global availability of backtracking to the previous node by single-clicking the mouse as long as it points to empty space on the screen.</p>

<h2>Hyperties (1983)</h2>

<p>Hyperties was started as a research project by Ben Shneiderman [Shneiderman 1987b] at the University of Maryland around 1983. It was originally called TIES as an abbreviation for The Electronic Encyclopedia System, but since that name was trademarked by somebody else, the name was changed to Hyperties to indicate the use of hypertext concepts in the system.</p>

<p>Since 1987 Hyperties has been available as a commercial product on standard PCs from Cognetics Corporation. Research continues at the University of Maryland, where a workstation version has been implemented on Sun workstations.</p>

<p>One of the interesting aspects of the commercial version of Hyperties is that it works with the plain text screen shown in Figure 3.3. It is thus suited for DOS users. Hyperties also works with the main graphics formats on PCs and PS/2s and can display color images if the screen can handle them.</p>

<p>The interaction techniques in Hyperties are extremely simple and allow the interface to be operated without a mouse. Some of the text on the screen is highlighted and the user can activate those anchors either by clicking on them with a mouse, touching if a touch screen is available, or simply by using the arrow keys to move the cursor until it is over the text and then hitting ENTER. Hyperties uses the arrow keys in a special manner called "jump keys," which causes the cursor to jump in a single step directly to the next active anchor in the direction of the arrow. This way of using arrow keys has been optimized for hypertext where there are normally only a few areas on the screen that the user can point to and the use of keys has been measured to be slightly faster than the mouse (see Chapter 6).</p>

<p>In the example in Figure 3.3, the user is activating the string "Xerox PARC," which is indicated by inverse video. In the color version of Hyperties it is possible for the user to edit a preference file to determine other types of feedback for selections such as the use of contrasting color.</p>

<p><img alt="Screenshot from Hyperties (enlarged fonts)" src="http://media.nngroup.com/media/editor/2012/10/31/hyperties-screenshot.gif" style="width: 910px; height: 604px;"/><br/>
<em><strong>Figure 3.3 </strong> . An example of a Hyperties screen as it typically looks on a text-only screen on a plain vanilla DOS machine. </em></p>

<p>Instead of taking the user directly to the destination node as almost all other hypertext systems do, Hyperties at first lets the user stay at the same navigational location and displays only a small "definition" at the bottom of the screen. This definition provides the user with a prospective view of what would happen if the link were indeed followed to its destination and it allows the user to see the information in the context of the anchor point. In many cases just seeing the definition is enough. Otherwise the user can of course choose to complete the link.</p>

<p>A Hyperties link points to an entire "article," which may consist of several pages. Users following the link will always be taken to the first page of the article and will have to page through it themselves. This set-up is in contrast to the KMS model, where a link always points to a single page, and to the Intermedia model where a link points to a specific text string within an article. The advantage of the Hyperties model is that authors do not need to specify destinations very precisely. They just indicate the name of the article they want to link to, and the authoring system completes the link.</p>

<p>The same text phrase will always point to the same article in Hyperties, which again simplifies the authoring interface but makes the system less flexible. Many applications call for having different destinations, depending on the context or perhaps on the system's model of the user's level of expertise.</p>

<p>Many of the design choices in Hyperties follow from the original emphasis on applications like museum information systems. These applications need a very simple reading interface without advanced facilities like overview diagrams (which cannot be supported on plain DOS machines anyway). Furthermore, the writers of the hypertexts were museum curators and historians who are mostly not very motivated for learning complex high-technology solutions, so the similarity of the Hyperties authoring facilities to traditional text processing was well suited for the initial users. Now Hyperties is being used for a much wider spectrum of applications.</p>

<p>The commercial version of Hyperties uses a full-screen user interface as shown in Figure 3.3, whereas the research system on the Sun uses a two-frame approach similar to that of KMS.</p>

<h2>NoteCards (1985)</h2>

<p>NoteCards may be the most famous of the original hypertext research systems because its design was been especially well documented [Halasz et al. 1987]. It was designed at Xerox PARC and is now available as a commercial product.</p>

<p>Originally, NoteCards ran only on the Xerox family of D-machines. These computers are fairly specialized Lisp machines and not in very widespread use outside the research world. Therefore the commercial version of NoteCards was ported to general workstations like the Sun.</p>

<p>One reason for implementing NoteCards on the Xerox Lisp machines was that they provided the powerful InterLisp programming environment. InterLisp made it easy to program a complex system like NoteCards, and it also gave users the option to customize NoteCards to their own special needs since it is fully integrated with the Lisp system. Users who know Lisp can in principle change any aspect of NoteCards and they can implement specialized card types as mentioned below.</p>

<p>NoteCards was built on the four basic kinds of objects shown in Figure 3.4:</p>

<p><img alt="Simplified view of a NoteCards screen" src="http://media.nngroup.com/media/editor/2012/10/31/notecards-ui-objects.gif" style="width: 600px; height: 261px;"/><br/>
<em><strong>Figure 3.4. </strong> The general layout of a NoteCards screen with the four basic objects: notecards, a link, FileBoxes, and a browser card. </em></p>

<ul>
	<li>Each node is a single <strong> notecard </strong> that can be opened as a window on the screen. These cards are not really "cards" in the HyperCard sense of having a fixed size but are really standard resizeable windows. Users can have as many notecards open on the screen as they want but quickly risk facing the "messy desktop" problem if they open too many. The notecards can have different types depending on the data they contain. The simplest card types are plain text or graphics but there are at least 50 specialized types of cards for individual applications that need special data structures. For example a legal application might need notecards containing forms for court decisions with fields for the standard units of information (defendant, plaintiff, etc.).</li>
	<li>The <strong> links </strong> are typed connections between cards. Links can be displayed as a small link icon as in Figure 3.4 or they can be shown as a box with the title of their destination card. Users open the destination card in a new window on the screen by clicking on the link icon with the mouse. The link type is a label chosen by the user to specify the relation between the departure card and the destination card for the link. To continue the legal example, lawyers might want one type of link to court decisions supporting their own position and another type of link to decisions that refute their position.</li>
	<li>The third kind of object is the <strong> browser </strong> card, which contains a structural overview diagram of the notecards and links. As shown in Figure 3.4, the different link types are indicated by different line patterns in the browser, thus giving the user an indication of the connection among the nodes. The browser card is an active overview diagram and allows users to edit the underlying hypertext nodes and links by carrying out operations on the boxes and lines in the browser. The user can also go to a card by clicking on the box representing it. The layout of the browser card is computed by system and therefore reflects the changing structure of the hypertext as users add or delete nodes and links.</li>
	<li>The fourth kind of object is the <strong> FileBox </strong> , which is used for hierarchical nesting of notecards. Each notecard is listed in exactly one FileBox. Actually, the FileBox is a special-purpose notecard, so FileBoxes can contain other FileBoxes and it is possible to construct links from other cards to a FileBox.</li>
</ul>

<p>In one case users customized NoteCards so extensively that the result may be said to be a new system. The Instructional Design Environment (IDE) developed at Xerox PARC [Jordan et al. 1989] is built on top of NoteCards but provides a new user interface to help courseware developers construct hypertext structures semi-automatically. IDE supports structure accelerators that speed up hypertext construction by allowing the user to generate an entire set of nodes and links from a template with a single action.</p>

<p>The standard version of NoteCards has been used for several years both within Xerox and at customer locations. One of the interesting early empirical studies of the actual use of NoteCards was a longitudinal study [Monty and Moran 1986] of a history graduate student who used the system to write a research paper over a period of seven months. This user did not use links across the FileBox hierarchy very much, but that result may not be generalized to other users. The important aspect of the study is that it investigated the behavior of the test subject for an extended period of time and observed the use of the system for a fairly large task.</p>

<h2>Symbolics Document Examiner (1985)</h2>

<p>The early hypertext systems can best be classified as proof-of-concept systems showing that hypertext was not just a wild idea but could actually be implemented on computers. Even though some systems, like Engelbart's NLS and the early Brown University systems, were used for real work, that use was mostly in-house at the same institutions where the systems were designed.</p>

<p>In contrast, the Symbolics Document Examiner [Walker 1987] was designed as a real product for users of the Symbolics workstations. The project started in 1982 and shipped in 1985, making it the first hypertext system to see real-world use. The Document Examiner was a hypertext interface to the online documentation for the Symbolics workstation, and people got it and used it because it was the best way to get information about the Symbolics, not because it was a hypertext system as such.</p>

<p>The Symbolics manual also existed in an 8,000-page printed version. This information was represented in a 10,000 nodes hypertext with 23,000 links taking up a total of ten megabytes storage space. This hypertext would still be considered fairly large today and was possible in 1985 only because the Symbolics workstation was a very powerful personal computer. To produce all this hypertext, the technical writers at Symbolics used a special writing interface called Concordia, which is discussed further in Chapter 11.</p>

<p>The information in the 8,000-page manual was modularized according to an analysis of the users' probable information needs. The basic principle was to have a node for any piece of information that a user might want.</p>

<p>Furthermore, the design goal for the user interface was to be as simple as possible and not scare users off. Since hypertext was not yet a popular concept in 1985, this goal meant using a book metaphor for the interface instead of trying to get users to use network-based navigation principles. The information was divided into "chapters" and "sections" and had a table of contents. Furthermore, users could insert "bookmarks" at nodes they wanted to return to later.</p>

<p>To assess the usability of the Symbolics Document Examiner, the designers conducted a survey of 24 users. Two of them did prefer the printed version of the manual, but half used only the hypertext version and eight had not even taken off the shrinkwrap of the printed manual [Walker et al. 1989]. These users were engineers and they were using advanced artificial intelligence workstations, so they might have been more motivated to use high-technology solutions than ordinary users are.</p>

<h2>Intermedia (1985)</h2>

<p>Intermedia was a highly integrated hypertext environment developed at Brown University over several years [Yankelovich et al. 1988a; Haan et al. 1992]. It ran on the Macintosh but unfortunately only under Apple's version of the Unix operating system. Since most Macintosh buyers do not want to touch Unix, that choice of operating system severely restricted the practical utility of Intermedia and may have been a cause of its eventual failure.</p>

<p>Intermedia was based on the scrolling window model, like Guide and NoteCards, but otherwise it followed a different philosophy from the other systems discussed in this chapter. The core of Intermedia was a linking protocol defining the way other applications should link to and from Intermedia documents [Meyrowitz 1986]. It was possible to write new specialized hypertext applications and have them integrated into the existing Intermedia framework, since all the existing Intermedia applications would already know how to interact with the new one [Haan et al. 1992].</p>

<p>The links in Intermedia were highly based on the idea of connecting two anchors rather than two nodes. The links were bidirectional so that there was no difference between departure anchors and destination anchors. When a user activated a link from one of its anchors, the system would open a window with the document containing the other anchor and scroll that window until the anchor became visible. Thus Intermedia authors were encouraged to construct fairly long documents, since they could easily link to specific points in the documents.</p>

<p>Intermedia had two kinds of overview diagram as shown in Figure 3.5. The web view was constructed automatically by the system, and overview documents like the Mitosis OV document in the figure were constructed manually by the author using a drawing package and only by convention have a common layout with the name of the topic in the center and related concepts in a circle around it.</p>

<p><img alt="Simplified view of a NoteCards screen" src="http://media.nngroup.com/media/editor/2012/10/31/intermedia-web-view.png" style="width: 540px; height: 398px;"/><br/>
<em><strong>Figure 3.5. </strong> An Intermedia web view. The InterDraw document called Mitosis OV is open. Each arrow icon in the overview diagram indicates the existence of one or more links. These connections are dynamically represented in the "Cell Motility: Web View" document. The web view is individual to each user and is saved from session to session. One of its functions is to provide the user with a path showing which documents he or she has opened, when they were opened, and how the document was reached (by following a link, opening the document from the desktop, and so on). The figure also illustrates another function of the web view: For the current document (the document most recently activated), the web view provides users with a map of where they can go next, thus allowing them to preview links and follow only those that they want to see. Copyright © 1989 by Brown University, reprinted with permission. </em></p>

<p>A typical Intermedia hypertext for a given course would contain many such overview documents, one for each of the central concepts in the course material.</p>

<p>Intermedia was designed for educational use on the university level and was used to teach several courses in both humanities and natural sciences. There is no reason why it could not be used for many of the other hypertext applications listed in Chapter 4, but the educational origin has had some impact on the design. For example, the Intermedia model assumes that several users (i.e., students) will access the same set of hypertext documents (i.e., course readings) and make their own annotations and new links. Therefore Intermedia stores separate files with links for each user in the form of so-called webs. Figure 3.6 shows the creation of a link in Intermedia. When the user has selected the other anchor for the link (for example the event listed under 1879 in the InterVal timeline) and has activated the "Complete Link" command, the new link will be added to the user's web.</p>

<p><img alt="Simplified view of a NoteCards screen" src="http://media.nngroup.com/media/editor/2012/10/31/intermedia-create-link.png" style="width: 576px; height: 480px;"/><br/>
<em><strong>Figure 3.6. </strong> To create a link in Intermedia, the user may select any portion of a document and choose the "Start Link" command. The link creation interface was modeled after the Macintosh cut/copy/paste paradigm; thus, the user may perform any number of intermediate actions and the link will remain pending until the user selects the other anchor for the link and activates the "Complete Link" command. Copyright © 1989 by Brown University, reprinted with permission. </em></p>

<p>Unfortunately, the funding agencies that had been supporting the development of Intermedia decided to discontinue funding the project in 1991, so even though Intermedia was the most promising educational hypertext system in the early 1990s, it does not exist anymore.</p>

<h2>Guide (1986)</h2>

<p>Guide was the first popular commercial hypertext system [Brown 1987] when it was released for the Macintosh in 1986. Soon thereafter it was also released for the IBM PC, and was the first hypertext system that was available on both platforms. The user interface looked exactly the same on the two computers. Recent versions of Guide have been restricted to the Windows platform.</p>

<p>Peter Brown started Guide as a research project at the University of Kent in the U.K. in 1982, and he had the first version running on PERQ workstations in 1983. In 1984 the company Office Workstations Ltd. (OWL) got interested in the program and decided to release it as a commercial product. They made several changes to the prototype, including some that were necessary to get the user interface to conform to the Macintosh user interface.</p>

<p>Peter Brown continues to conduct research in hypertext using the Unix version of Guide that is maintained at the university [Brown 1992]. It is also used for some consulting projects in industry. If nothing else is stated, my use of the term "Guide" will refer to the commercial version on the IBM PC and the Macintosh and not to the Unix workstation version, since there are several differences between them.</p>

<p>Guide is similar to NoteCards in being based on scrolling text windows instead of fixed frames. But whereas the links in NoteCards refer to other cards, links in Guide often just scroll the window to a new position to reveal a destination contained within a single file. Link anchors are associated with text strings and move over the screen as the user scrolls or edits the text. This approach is in contrast to, say, HyperCard, where anchors are fixed graphic regions on the screen. Guide does include support for graphic links, but they seem somewhat less natural to work with in the Guide user interface, and graphics have to be imported from external drawing programs.</p>

<p>Guide supported three different forms for hypertext link: Replacements, pop-ups, and jumps.</p>

<p>The <strong> replacement buttons </strong> were used for in-line expansion of the text of the anchor to a new and normally larger text in a concept that is sometimes called stretchtext. (The "stretchtext" term is probably due to Ted Nelson. Similar concepts were found in Augment and several early text editors at Xerox PARC)</p>

<p>Replacement buttons formed a hierarchical structure of text and were useful for representing text in the manner of a traditional textbook with chapters, sections, and subsections. Typically, the initial display would show all the chapter headings and users would then expand the one chapter in which they were interested by replacing the chapter heading with the list of sections in the chapter. They could then further replace the section that interested them the most with its list of subsections, and so on. While making these replacements, the user continuously had the other chapter headings available (perhaps by scrolling the window a little) and thereby preserved context. The reverse action of a replacement was to close the expanded text and have it re-replaced with the original text.</p>

<p>The replacement button existed in a variation called <strong> inquiry replacement</strong>, which was used to list several options and have the user choose one. When the user clicked on a replacement button that was part of an inquiry, that button would expand, and the other buttons in the inquiry would be removed from the screen until the user closed the expansion again. This interface was useful for multiple-choice type applications, like a repair manual where the user was asked to click on the type of equipment that needs repair. The explanation for the selected type was expanded and the other, irrelevant types would be hidden.</p>

<p>The second type of hypertext was small pop-up windows provided by clicking <strong> note buttons </strong> as shown in Figure 3.7. This facility was useful for footnote-type annotations, which are closely connected to the information in the main window. The pop-up was displayed only as long as the user held the mouse button down over the note button, implying that the "backtrack" command consisted simply of letting the mouse button go. This type of user interface is sometimes called a "spring loaded mode" because users are in the mode only as long as they continue to activate a dialogue element that will revert to normal as soon as it is released. The pop-ups are modes, nevertheless, since they make it impossible for the user to perform other actions (e.g., making a copy of the text in the pop-up window) as long as they are displayed.</p>

<p><img alt="Screenshot from Guide" src="http://media.nngroup.com/media/editor/2012/10/31/guide-screenshot.png" style="width: 512px; height: 324px;"/><br/>
<em><strong>Figure 3.7. </strong> A typical Guide screen where the user is pressing down the mouse button over the anchor for a pop-up note, which is temporarily displayed in the small window at the top right of the screen. </em></p>

<p>The third form for hypertext in Guide was the <strong> reference button</strong>, which was used to jump to another location in the hypertext. To get back to the departure point, users had to click a special backtrack icon.</p>

<p>The three different kinds of hypertext in Guide were revealed to the user by changing the shape of the cursor, as shown in Figure 3.8 One might have imagined that this fairly extensive set of different types of hypertext in a single small system would confuse users, but our field studies [Nielsen and Lyngbæk 1990] showed that users had no problems distinguishing among the three kinds of button.</p>

<p><img alt="The 4 cursor shapes in Guide" src="http://media.nngroup.com/media/editor/2012/10/31/guide-cursor-shapes.gif" style="width: 400px; height: 134px;"/><br/>
<em><strong>Figure 3.8. </strong> Guide used varying cursor shapes to indicate the type of hypertext action available to the user. </em></p>

<p>As further discussed in Chapter 10, we also found that users liked the note button for pop-ups best and that the reference button for jumps got the worst ratings. It is interesting to consider that the reference button is exactly the feature that was not included in the "cleanly designed" research prototype of Guide but was added for the commercial release [Brown 1987]. It is of course impossible to say from our data whether the reference button was rated relatively poorly because it was not integrated nicely into the overall design or because gotos are just harmful in general.</p>

<p>Version 2 of Guide introduced a fourth type of button called the command button, which executes a script in the special-purpose Genesis language when clicked. Genesis was not a general programming language like HyperCard's HyperTalk, however, and was typically only used to access a videodisk to play a specified set of frames.</p>

<h2>HyperCard (1987)</h2>

<p>It is important to note that the designer of HyperCard, Bill Atkinson, has admitted that it was not really a hypertext product from the beginning. He originally built HyperCard as a graphic programming environment and many of the applications built into HyperCard actually have nothing to do with hypertext. Even so, HyperCard was probably the most famous hypertext product in the world in the late 1980s.</p>

<p>There are several reasons for HyperCard's popularity. A very pragmatic one is that it was bundled free with every Macintosh sold by Apple from 1987 to 1992. You could not beat that price, and the fact that it came automatically with the machine also meant that it was introduced to a large number of people who would otherwise never have dreamt of getting a hypertext system. Even after Apple started selling HyperCard as a traditional product, they still supplied a HyperCard reader for free with every Macintosh sold, meaning that HyperCard developers were ensured of their market.</p>

<p>The second reason for HyperCard's popularity is that it includes a general programming language called HyperTalk, which is fairly easy to learn. My experiments indicate that people with some previous programming experience can learn HyperTalk programming in as little as two days [Nielsen et al. 1991a]. Furthermore, this programming language is quite powerful with respect to prototyping graphic user interfaces. It is not very well suited for implementing larger software systems needing maintenance over periods of several years, however.</p>

<p>HyperCard is a good match for many of the innovative things people want to experiment with in the hypertext field. It is easy to learn, it can produce aesthetically pleasing screen designs, and it allows fast prototyping of new design ideas. One of my own hypertext systems, described in Chapter 2, was implemented in HyperCard. HyperTalk makes HyperCard well suited for experiments with computational hypertext where information is generated at read-time under program control.</p>

<p>As the name implies, HyperCard is strongly based on the card metaphor. It is a frame-based system like KMS but mostly based on a much smaller frame size. Most HyperCard stacks are restricted to the size of the original small Macintosh screen even if the user has a larger screen. This is to make sure that all HyperCard designs will run on all Macintosh machines, thereby ensuring a reasonably wide distribution for HyperCard products. Version 1 of HyperCard enforced the card size restriction without exceptions, but the newer version 2 has made it possible to take advantage of larger screens.</p>

<p>The basic node object in HyperCard is the card, and a collection of cards is called a stack. The main hypertext support is the ability to construct rectangular buttons on the screen and associate a HyperTalk program with them. This program will often just contain a single line of code written by the user in the form of a <code> goto </code> statement to achieve a hypertext jump. Buttons are normally activated when the user clicks on them, but one of the flexible aspects of HyperCard is that it allows actions to be taken also in the case of other events, such as when the cursor enters the rectangular region, or even when a specified time period has passed without any user activity.</p>

<p>The main advantage of the HyperCard approach of implementing hypertext jumps as program language statements is that links do not need to be hardwired. Anything you can compute can be used as the destination for a link.</p>

<p>In addition to the basic jumps to other cards, HyperCard can at least simulate pop-ups like the ones in Guide by the use of special <code> show </code> and <code> hide </code> commands. The designer can determine that a specific text field should normally be hidden from the user but that it will be made visible when the user clicks some button. The end result of these manipulations will be very similar to the Guide pop-ups.</p>

<p>HyperCard does have one serious problem compared to Guide, however, and that is the question of having hypertext anchors associated with text strings. In Guide these "sticky buttons" are the standard, allowing users to edit the text as much as they like and still keep their hypertext links so long as they do not delete the anchor strings. In HyperCard, an anchor is normally associated with a text string by placing the rectangular region of a button at the same location of the screen as the text string. But this anchoring method means big trouble if the user ever edits the text, since it is sure to change the physical location of the anchor string on the screen.</p>

<p><img alt="Sample screenshot from HyperCard" src="http://media.nngroup.com/media/editor/2012/10/31/hypercard-sample-screen.gif" style="width: 511px; height: 342px;"/><br/>
<em><strong>Figure 3.9. </strong> An example of a screen implemented in HyperCard. Figure 3.10 gives a general idea of how this design was implemented. </em></p>

<p>Figure 3.10 gives a simplified view of how I implemented the hypertext design from Figure 3.9 in HyperCard. First the general graphic design of the nodes was drawn as a background object that would be inherited by all the nodes in its class. This design included the picture of a book and the global overview diagram (since it would be unchanged for all nodes). The background design also included an empty placeholder field for the text to be added in the individual nodes.</p>

<p><img alt="Layers of objects in a HyperCard card" src="http://media.nngroup.com/media/editor/2012/10/31/structure-hypercard-objects.gif" style="width: 800px; height: 719px;"/><br/>
<em><strong>Figure 3.10. </strong> A simplified view of the HyperCard implementation of the hypertext design in Figure 3.9. The background level contains graphics that are common for several nodes, whereas the foreground level contains the text and graphics that are specific for the individual node. Finally, the designer has placed several buttons on top of the text and graphics. </em></p>

<p>For each individual node I then added a foreground layer with the text of the node and some graphics. The foreground graphics included the local overview diagram (since it would be different from node to node) and the heavy rectangles used to highlight the current location in the local and global overview diagrams. Since HyperCard displays all the levels as a single image on the screen, following the same principle as when an animation artist photographs a pile of acetates, the user would never know that the visual appearance of the global overview diagram was created by a combination of a fixed background image and a changing foreground rectangle.</p>

<p>Finally, I added a set of buttons to each individual node to achieve the hypertext links. Some of these buttons were for the local overview diagram and were placed over the corresponding graphics, whereas other buttons were anchors associated with text strings in the foreground layer and had to be carefully positioned over the relevant text. Actually, the complete screen contains even more buttons since there are also some global buttons that are common for all nodes and are therefore placed in the background level. They are not shown specifically here.</p>

<p>HyperCard has several competitors, including SuperCard, Plus, and MetaCard. SuperCard has integrated facilities for dealing with color and several variable-sized windows at the same time and also allows object-oriented graphics of non-rectangular shapes to act as buttons. Plus is available both for the Macintosh and for the IBM PC (under Microsoft Windows as well as OS/2), affording cross-platform compatibility of its file format. MetaCard runs on workstations using X Windows, thus expanding the range of platforms on which the basic HyperCard type of hypertext can be used. Several other limitations have not been addressed by these competing products, however.</p>

<p>Some of these unsolved problems are not all that conceptually difficult, and one could imagine that HyperCard would address them in a possible version 3. This is true of the missing sticky buttons and the slow execution speed of HyperTalk programs. Other problems are harder to address since they conflict with the basic nature of HyperCard. These include issues such as changing the programming language to be completely object-oriented and more maintainable and designing advanced hypertext features or multiuser access.</p>

<p>Interestingly, HyperCard's early success was not just due to its conceptual structure or the power of the underlying system. A major reason many people started authoring their own HyperCard stacks was the inclusion of a "construction kit" of graphical user interface elements with the basic system. I can't begin to count the times I have seen people using the picture of a man thinking in front of a computer that was included with the original HyperCard. More important, instead of just providing square boxes for buttons and hoping that people would fill them in with their own icons, HyperCard shipped with a large collection of pointing hands, turning arrows, and other appropriate designs that could be used as building blocks for new user interfaces. The attractiveness of these sample and template materials made many of the early HyperCard stacks look pretty good and helped build critical mass. I would definitely advise developers of future systems to include plenty of GUI widgets and pre-designed graphics.</p>

<h2>Hypertext Grows Up</h2>

<p>Symbolics Document Examiner was an example of hypertext meeting the real world since it saw real use by real customers. But the Symbolics was a fairly specialized artificial intelligence workstation and was very expensive when the Document Examiner was first introduced. So even though it counts as the first real-world use of hypertext, it was not a widely distributed and known system.</p>

<p>Several hypertext systems were announced in 1985 and saw widespread use in the late 1980s and early 1990s, including NoteCards from Xerox and Intermedia from Brown University.</p>

<p>In contrast, when Office Workstations Limited (OWL) introduced Guide in 1986, it was as a commercial product. Guide was the first widely available hypertext to run on ordinary personal computers of the type people have in their homes or offices. To some extent the release of Guide could be said to mark the transition of hypertext from an exotic research concept to a "real world" computer technique for use in actual applications.</p>

<p>The final step to "realworldness" came when Apple introduced HyperCard in 1987. A nice product in its own right, its real significance was to be found in the marketing concept of giving away the program (or later a reader) for free with every Macintosh sold after 1987.</p>

<p>An event that really marked the graduation of hypertext from a pet project of a few fanatics to widespread popularity was the first ACM conference on hypertext, <a href="../trip-report-hypertext-87/index.php" title="Jakob Nielsen's trip report from the Hypertext 1987 conference"> Hypertext'87</a>, held at the University of North Carolina on November 13–15, 1987. Almost everybody who had been active in the hypertext field was there, all the way from the original pioneers (except Vannevar Bush) to this author. Unfortunately the conference organizers had completely underestimated the growing interest in hypertext and had to turn away about half of the 500 people who wanted to attend the conference. Even so, we were crammed into two auditoriums that were connected by video transmission, and people had to sit on the floor. For those people who were lucky enough to get in, this was a great conference with plenty of opportunity to meet everybody in the field and to see the richness of ongoing hypertext research and development.</p>

<p>History repeated itself when the first open conference on hypertext in Europe was held in 1989. This was the <a href="../trip-report-hypertext-2/index.php" title="Jakob Nielsen's trip report from the Hypertext'2 conference"> Hypertext'2 conference </a>  in York in the U.K. on June 29–30, 1989. The reason this conference was called Hypertext'2 was that there had been a first, closed conference in Aberdeen the year before. Again the organizers had underestimated the growth of the field and had facilities to accommodate only 250 people. But 500 wanted to come, so half had to be turned away.</p>

<p>The year 1989 also saw the birth of the first scientific journal devoted to hypertext, <cite> Hypermedia </cite> , published by Taylor Graham. It is discussed further in the bibliography.</p>

<p>In the mid-1990s, hypermedia systems came to the attention of the larger public through the proliferation of CD-ROMs. For example, the first full-length feature film in hypermedia form was shipped on a CD-ROM in 1993 when the Voyager Company released the Beatles film <cite> A Hard Day's Night </cite> . In 1993, compression technology was still primitive enough to make this something of a feat, and the only reason it was possible is that <cite> A Hard Day's Night </cite> is a rather short film and was filmed in black-and-white. Figure 3.11 shows a screen from this hypermedia production. The Voyager Company released a large number of other titles throughout the 1990s and proved that it had become possible to launch a successful publishing company by concentrating on shipping hypertext.</p>

<p><img alt="Screenshot from playing A Hard Day's Night" src="http://media.nngroup.com/media/editor/2012/10/31/a-hard-days-night.png" style="width: 583px; height: 444px;"/><br/>
<em><strong>Figure 3.11. </strong> Screen from the CD-ROM edition of the Beatles film <strong> <cite> A Hard Day's Night</cite></strong>. The film itself is playing in the upper left window and the rest of the screen updates to show the part of the original script that corresponds to the scene currently playing. Pop-up controls allow the user to move directly to various scenes or songs and to view related films. Copyright © 1964 by Proscenium Films, 1993 by The Voyager Company, reprinted by permission. </em></p>

<p>A final event in the mid-1990s was the extremely rapid growth of hypertext on the Internet, spearheaded by the specification of the World Wide Web by Tim Berners-Lee and colleagues at CERN (the European Center for Nuclear Physics Research in Geneva, Switzerland). Almost immediately after its introduction by the National Center for Supercomputing Applications (NCSA) in January 1993, Mosaic became the most popular browser for the WWW and the growth of Internet hypertext accelerated even more. See Chapter 7 for a more extended treatment of hypertext over the Internet.</p>

<p>It is interesting to contemplate the fact that Mosaic and the WWW more or less succeeded in establishing a universal hypertext system in just three years, even though Ted Nelson could not get his Xanadu system accepted in thirty years of trying. One major reason for this difference is doubtless that the WWW projects were paid for by the taxpayers (the European taxpayers in the case of CERN and the American taxpayers in the case of NCSA). It always makes it easier to sell a product when the cost is $0. Even so, there are also other reasons why WWW succeeded where Xanadu failed. The most important differences are the open systems nature of the WWW and its ability to be backwards compatible with legacy data.</p>

<p>The WWW designers compromised and designed their system to work with the Internet through open standards with capabilities matching the kind of data that was available on the net at the time of the launch. These compromises ensured the success of the WWW but also hampered its ability to provide all the features one would ideally want in a hypertext system. The specification of the WWW's underlying hypertext markup language (HTML) has been through three versions in the first four years after the introduction of the system and it is still not ideal. There is no doubt that this reliance on iterative design and evolutionary change is better than waiting for the revolution that never comes. After all, if the choice is between perfection and nothing, then nothing wins every time. We should be grateful to the WWW designers for offering us a third choice.</p>

<p>In conclusion, we can say that hypertext was conceived in 1945, born in the 1960s, slowly nurtured in the 1970s, and finally entered the real world in the 1980s with an especially rapid growth after 1985, culminating in a fully established field during 1989. We now have several real-world systems that anybody can buy in their local computer store (or get for free bundled with their computer); we have successful conferences and a journal; and most important of all, we have many examples of actual use of hypertext for real projects. These examples are the subject of the next chapter.</p>
</section>
  

  
  <section>
    
        
    
  </section>
  
  
  
  
  

  


  <section>
    <br />
    <p class="share"><b>Share <span class="hide-for-small-down">this article</span>:</b> <a href="https://twitter.com/intent/tweet?url=http://www.nngroup.com/articles/hypertext-history/&amp;text=The%20History%20of%20Hypertext&amp;via=nngroup">Twitter</a> | <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=http://www.nngroup.com/articles/hypertext-history/&amp;title=The%20History%20of%20Hypertext&amp;source=Nielsen%20Norman%20Group">LinkedIn</a> | <a href="https://plus.google.com/share?url=http://www.nngroup.com/articles/hypertext-history/">Google+</a> | <a href="mailto:?subject=NN/g Article: The History of Hypertext&amp;body=http://www.nngroup.com/articles/hypertext-history/">Email</a></p>
    <hr />
  </section>

  <footer class="l-subsection">
    
    
    <div class="learn-more">
      <h2>Learn More</h2>
      <p><a href="../subscribe/index.php">Subscribe to the weekly newsletter</a> to get notified about future articles.</p>
      </div>
    
    

    <div class="row show-for-small-down related-content">
      
      
      

      
      
        <div class="column small-12 topic-links">All articles about:
          <ul>
            
            <li><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction</a></li>
            
            <li><a href="../../topic/hypertext/index.php">hypertext</a></li>
            
            <li><a href="../../topic/technology/index.php">new technologies</a></li>
            
          </ul>
        </div>
      
      

      
      
        <div class="column small-12">
          <h3>UX Conference Training Courses</h3>
           <ul class="no-bullet">
    
        <li><a href="../../courses/ux-basic-training/index.php">UX Basic Training</a></li>
    
        <li><a href="../../courses/hci/index.php">User Interface Principles Every Designer Must Know</a></li>
    
        <li><a href="../../courses/emerging-patterns-web-design/index.php">Emerging Patterns for Web Design</a></li>
    
        <li><a href="../../courses/interaction-design-3-day-course/index.php">Interaction Design: 3-Day Course</a></li>
    
</ul>
        </div>
      
      

      
      
      

      
      
        <div class="small-12 column">
          <h3>Online Seminars</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../online-seminars/beyond-screens/index.php">Beyond Screens</a></li>
              
            
          </ul>
        </div>
      
      
    </div>

    <div class="hide-for-small-down row related-content">
      <div class="medium-6 columns">
        
        
          <div>
            <h3>Articles</h3>
            <ul class="no-bullet">
              
                
                <li><a href="../embarrassment/index.php">Computer-Assisted Embarrassment</a></li>
                
              
                
                <li><a href="../wechat-integrated-ux/index.php">WeChat: China’s Integrated Internet User Experience</a></li>
                
              
                
                <li><a href="../voice-interaction-ux/index.php">Voice Interaction UX: Brave New World...Same Old Story</a></li>
                
              
                
                <li><a href="../enhancement/index.php">The Role of Enhancement in Web Design</a></li>
                
              
                
                <li><a href="../human-body-touch-input/index.php">The Human Body as Touchscreen Replacement</a></li>
                
              
            </ul>
          </div>
        
        

        

      </div>
      <div class="medium-6 columns">
        
          
          <div>
            <h3>UX Conference Training Courses</h3>
             <ul class="no-bullet">
    
        <li><a href="../../courses/ux-basic-training/index.php">UX Basic Training</a></li>
    
        <li><a href="../../courses/hci/index.php">User Interface Principles Every Designer Must Know</a></li>
    
        <li><a href="../../courses/emerging-patterns-web-design/index.php">Emerging Patterns for Web Design</a></li>
    
        <li><a href="../../courses/interaction-design-3-day-course/index.php">Interaction Design: 3-Day Course</a></li>
    
</ul>
          </div>
          
        
        
          
          <div>
            <h3>Online Seminars</h3>
            <ul class="no-bullet">
            
                <li><a href="../../online-seminars/beyond-screens/index.php">Beyond Screens</a></li>
            
            </ul>
          </div>
          
        
      </div>
    </div>
  </footer>
</article>

        </div>

      

    
  </div>
  <div class="row">
    
      <div class="column small-12 show-for-small-down l-navrail announcements">
        
          
        
      </div>
    
    
  </div>

    </div>
    




<footer id="page-footer">
    <div class="row hide-for-print">
        <div class="column small-12 medium-6">
            <section class="footer-popular">
                
                <h1>Popular Topics</h1>
                <div style="float: left;  width: 15em; display: block;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/applications/index.php">Application Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/e-commerce/index.php">E-Commerce Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/eyetracking/index.php">Eyetracking </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/information-architecture/index.php">Information Architecture </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/intranets/index.php">Intranet Design </a></li>
	<li><a href="../../topic/interaction-design/index.php">Interaction Design </a></li>
</ul>
</div>

<div style="float: left;  width: 15em; display: block; list-style: none;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/mobile-and-tablet-design/index.php">Mobile and Tablet Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/credibility-persuasion/index.php">Persuasive Design</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/research-methods/index.php">Research Methods</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/user-testing/index.php">User Testing </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/web-usability/index.php">Web Usability </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/writing-web/index.php">Web Writing &amp; Content Strategy </a></li>
</ul>
</div>

<p>&nbsp;</p>

                
            </section>
        </div>

        <div class="column small-12 medium-6">
            <div class="row">
                <div class="column small-6">
                    <section class="footer-about">
                        <h1>Follow Us</h1>
                        <ul>
	<li><a href="../subscribe/index.php">Subscribe to our weekly UX newsletter</a>,&nbsp;Jakob Nielsen&#39;s Alertbox</li>
	<li>Visit Don Norman&#39;s website, <a href="http://www.jnd.org/"> www.jnd.org </a></li>
	<li>Visit Bruce Tognazzini&#39;s website, <a href="http://www.asktog.com/"> www.asktog.com </a></li>
	<li style="padding-top: 6px;"><a class="twitter-follow-button" data-show-count="false" data-size="large" href="https://twitter.com/nngroup"><img alt="Follow @nngroup on Twitter" height="20" src="https://media.nngroup.com/media/editor/2016/12/30/twitter-follow-button.png" style="width: 117px; height: 20px;" width="117" /></a></li>
	<li style="padding-top: 6px;"><img alt="" height="14" src="https://media.nngroup.com/media/editor/2016/07/26/feed-icon-14x14.png" style="vertical-align:text-top;width:14px;margin-right:.25em;" width="14" /><a href="../../feed/rss/index.php">Subscribe to our RSS feed</a></li>
</ul>

                    </section>
                </div>

                <div class="column small-6">
                    <section class="footer-about right-column">
                        <h1>About
</h1>
                        <ul class="no-bullet">
	<li><a href="../../about/contact/index.php">Contact information</a></li>
	<li><a href="../../about/index.php">About Us</a></li>
	<li><a href="../../about/why-nng/index.php">Why NN/g? </a></li>
	<li><a href="../../return-policy/index.php">Return Policy</a></li>
</ul>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="copyright column small-12">
            <p><a href="../../copyright-and-reprint-info/index.php">Copyright</a> &copy; 1998-2017 Nielsen Norman Group, All Rights Reserved.</p>
        </div>
    </div>
</footer>


    


        <script src="https://media.nngroup.com/static/vendor/fastclick/js/fastclick.min.js"></script>
        <script src="https://media.nngroup.com/static/vendor/foundation/js/foundation.min.js"></script>
        <script src="http://media.nngroup.com/static/js/app.js"></script>
        <script src="http://media.nngroup.com/static/js/scroll-to-fixed.js"></script><script>var externalLinks = document.querySelectorAll('a[href^="http"]');for (var i = externalLinks.length-1; i >= 0; i--) {externalLinks[i].addEventListener("click", function(event) { alert("External links are disabled for this test."); event.preventDefault(); }, false);}var mailLinks = document.querySelectorAll('a[href^="mailto"]');for (var i = mailLinks.length-1; i >= 0; i--) {mailLinks[i].addEventListener("click", function(event) { alert("Email links are disabled for this test."); event.preventDefault(); }, false);}var externalForms = document.querySelectorAll('form');for (var i = externalForms.length-1; i >= 0; i--) {externalForms[i].addEventListener("submit", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false); externalForms[i].addEventListener("click", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false);}</script>

        <script>
            
            $(function() {
                $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
                $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
            });
        </script>

        
<script src="https://media.nngroup.com/static/js/articles.js"></script>


        
        <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1072699960;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072699960/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
    </body>

<!-- Mirrored from www.nngroup.com/articles/hypertext-history/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:13:20 GMT -->
</html>
