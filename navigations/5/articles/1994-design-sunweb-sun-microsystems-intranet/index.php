<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->





    
<!-- Mirrored from www.nngroup.com/articles/1994-design-sunweb-sun-microsystems-intranet/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:14:37 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1026.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"licenseKey":"1a30ef9931","queueTime":0,"beacon":"bam.nr-data.net","errorBeacon":"bam.nr-data.net","applicationID":"2048992","transactionName":"NFxSMRBUX0EDUkIPDA0WdhAMVkVbDV8ZBxEXUFMJB0YfRAtUQRVZIktEDAFZVHYHRVcPD01eVRE=","applicationTime":730,"agent":""}</script>
        <title>1994 Design of SunWeb: Sun Microsystems&#39; Intranet</title><meta property="og:title" content="1994 Design of SunWeb: Sun Microsystems&#39; Intranet" />
  
        
        <meta name="description" content="Paper by Jakob Nielsen, describing the methods used to design the user interface and overall structure of the intranet pages for Sun Microsystems in 1994. ">
        <meta property="og:description" content="Paper by Jakob Nielsen, describing the methods used to design the user interface and overall structure of the intranet pages for Sun Microsystems in 1994. " />
        
  
        
	
        

        

        <meta name="robots" content="noarchive">
        <meta name="viewport" content="width=device-width">

        <link rel="search" type="application/opensearchdescription+xml" title="NN/g Search" href="../../opensearch.xml" />
        <link rel="shortcut icon" href="http://media.nngroup.com/static/img/favicon.ico" />

        <link rel="stylesheet" href="https://media.nngroup.com/static/vendor/font-awesome/css/font-awesome.min.css">

        
        <link rel="stylesheet" href="https://media.nngroup.com/static/css/7232a960f1df.css" type="text/css" />
        <link rel="stylesheet" href="http://fast.fonts.com/t/1.css?apiType=css&amp;projectid=c5846260-9def-46ec-9407-0f1349bd2d64" type="text/css">

        <script src="https://media.nngroup.com/static/vendor/modernizr/js/modernizr.min.js"></script>

        <script type="text/javascript" src="https://media.nngroup.com/static/js/3283be5def74.js"></script>
        

        

        
        
        <!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=229034,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('http://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->


<!-- Google Analytics Tracking Code for http://nngroup.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23132681-1', 'nngroup.com');
  ga('require', 'linkid', 'https://www.nngroup.com/articles/1994-design-sunweb-sun-microsystems-intranet/linkid.js');
  ga('require', 'displayfeatures');

// Optimizely Universal Analytics Integration
 window.optimizely = window.optimizely || [];
 window.optimizely.push("activateUniversalAnalytics");

  ga('send', 'pageview');

</script>
<script>
/**
* Function to track a click on an outbound link in Google Analytics.
*/
function trackOutboundLink(label, url) {
    if(window.ga && ga.loaded) {
        ga('send', 'event', 'outbound', 'click', label, {'hitCallback': function(){document.location = url;}
        });
    } else {
        document.location = url;
    }
}
/**
* Function to track a click on a link in the article announcement area.
*/
$('#articleAnnouncement a').on('click', function(){
  ga('send', 'event', 'link', 'click', 'articleAnnouncement');
});
</script>

<script type="text/javascript">
 setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2421.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



<!-- Hotjar Tracking Code for http://nngroup.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:121605,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

        <script src="https://media.nngroup.com/static/js/analytics/track_outbound_link.js"></script>
        
        
        
    </head>
    <body class="location-articles articles-detail">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
    <header id="page-header">
        <div id="cart" class="shopping-cart mobile-cart show-for-small-down"></div>
        <div class="row">
            <div id="masthead" class="masthead small-12 medium-7 column">

                <!-- The button to toggles menu on small screen -->

                <h1><a href="../../index.php"><img src="http://media.nngroup.com/static/img/logo-wordmark.png" width="74" height="30"> Nielsen <span class="norman">Norman</span> Group</a></h1>
                <p class="tagline show-for-medium-up">Evidence-Based User Experience Research, Training, and Consulting</p>

                <!-- The button to toggles form search on small screen -->


                <div style="clear: right;width:  100%;height: 1px;"></div><?php define('ROOT_PATH',$_SERVER["DOCUMENT_ROOT"] . '/apps/navigation_styles/navigations/5'); ?><?php include(ROOT_PATH)."/navigation.php"; ?>

                <!-- Drop down menu on small screen -->
                <div id="hideNav" class="show-for-small-down">
                  
                  <ul class="menu-content">
                    <li><a href="../../index.php">Home</a></li>
                    <li class="has-submenu">
                      <a>Training</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../training/index.php">Overview</a></li>
                        
<li id="nav-ux-conference"><a href="../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../online-seminars/index.php"><span>Online Seminars</span></a></li>

                      </ul>
                    </li>
                    <li class="has-submenu">
                      <a>Consulting</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
  <li><a href="../../consulting/index.php">Overview</a></li>
  
  <li><a href="../../consulting/benchmarking/index.php">Benchmarking</a></li>
  
  <li><a href="../../consulting/expert-design-review/index.php">Expert Design Review</a></li>
  
  <li><a href="../../consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
  
  <li><a href="../../consulting/usability-testing/index.php">Usability Testing </a></li>
  
  <li><a href="../../consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
  
  <li><a href="../../in-house-training/index.php">In-House Training</a></li>
</ul>

                    </li>

                    <li><a href="../../reports/index.php">Reports</a></li>
                    <li><a href="../index.php">Articles</a></li>
                    <li class="has-submenu"><a>About NN/G</a><i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../about/index.php">Overview</a></li>
                        <li><a href="../../people/index.php">People</a></li>
                        <li><a href="../../about/why-nng/index.php">Why NN/g?</a></li>
                        <li><a href="../../about/contact/index.php">Contact</a></li>
                        <li><a href="../../news/index.php">News</a></li>
                        <li><a href="../../about/history/index.php">History</a></li>
                        <li><a href="../../books/index.php">Books</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                </div>

                <hr class="header-divider show-for-small-only"/>


            </div>
       </div>

        <div id="hideNav">            <nav class="primary-nav small-12 column">
                <ul>
                    <li><a class="navitem" id="nav-home" href="../../index.php">Home</a></li>
                    <li><a class="navitem" id="nav-training" href="../../training/index.php">Training</a></li>
                    <li><a class="navitem" id="nav-consulting" href="../../consulting/index.php">Consulting</a></li>
                    <li><a class="navitem" id="nav-reports" href="../../reports/index.php">Reports</a></li>
                    <li><a class="navitem" id="nav-articles" href="../index.php">Articles</a></li>
                    <li><a class="navitem" id="nav-about" href="../../about/index.php">About NN/G</a></li>
                </ul>
            </nav>
            <nav class="secondary-nav small-12 medium-12 large-12 columns">
                
                
            </nav>
        </div>
    </header>
    <div class="l-content"><span id="preventClose" style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776; Menu</span>
    
  

  

  <div class="row">
    
      
      <section class="small-12 medium-3 columns collapsable-content l-navrail articles-left-column">
        <h1 class="show-for-small-down collapse-link mobile-browse-header">Browse by Topic and Author</h1>
          <div class="row collapsable">

            
            <div class="small-6 medium-12 columns">
              <section id="article-popular-topics" style="display: none;">
                
                <h1>Topics</h1>
                <ul id="article-popular-topics">
                    
                    <li>
                        
                        <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/intranets/all/index.php">Intranets</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/user-testing/all/index.php">User Testing</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                        
                    </li>
                    
                    <li><a href="#" id="article-see-all-topics"><strong>See all topics&hellip;</strong></a></li>
                </ul>
                
              </section>
              <section id="article-all-topics" style="display: none;">
                
                <h1>
                  <span class="show-for-medium-up">All Article Topics</span>
                  <span class="show-for-small-down">All Topics</span>
                  (<a href="#" id="article-hide-all-topics">hide</a>)
                </h1>
                <ul>
                  
                  <li>
                    
                    <a href="../../topic/accessibility/all/index.php">Accessibility</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/agile/all/index.php">Agile</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/analytics-and-metrics/all/index.php">Analytics &amp; Metrics</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/applications/all/index.php">Application Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/b2b-websites/all/index.php">B2B Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/behavior-patterns/all/index.php">Behavior Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/branding/all/index.php">Branding</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/content-strategy/all/index.php">Content Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/corporate-websites/all/index.php">Corporate Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/design-patterns/all/index.php">Design Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-design-process/all/index.php">Design Process</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/email/all/index.php">Email</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/eyetracking/all/index.php">Eyetracking</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/heuristic-evaluation/all/index.php">Heuristic Evaluation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/human-computer-interaction/all/index.php">Human Computer Interaction</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-ideation/all/index.php">Ideation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/information-architecture/all/index.php">Information Architecture</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/interaction-design/all/index.php">Interaction Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/international-users/all/index.php">International Users</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/intranets/all/index.php">Intranets</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-management/all/index.php">Management</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/navigation/all/index.php">Navigation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/non-profit-websites/all/index.php">Non-Profit Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/personas/all/index.php">Personas</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/persuasive-design/all/index.php">Persuasive Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/prototyping/all/index.php">Prototyping</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/psychology-and-ux/all/index.php">Psychology and UX</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/research-methods/all/index.php">Research Methods</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/search/all/index.php">Search</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/social-media/all/index.php">Social Media</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/strategy/all/index.php">Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/user-testing/all/index.php">User Testing</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-teams/all/index.php">UX Teams</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/visual-design/all/index.php">Visual Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/writing-web/all/index.php">Writing for the Web</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/young-users/all/index.php">Young Users</a>
                    
                  </li>
                  
                </ul>
                
              </section>
            </div>
            <script>
              /* Determine which topics list will be displayed: the full or the
               * partial.  We are inlining this close to the list itself to
               * prevent rendering delays on the client
               */
              var topic_list = $.cookie("article-topics");

              var popular_topics = $('#article-popular-topics');
              var all_topics = $('#article-all-topics');

              if(topic_list == 1) {
                  popular_topics.hide();
                  all_topics.show();
              } else {
                  popular_topics.show();
                  all_topics.hide();
              }
            </script>
            

            
            <div class="small-6 medium-12 columns">
              
              <section id="article-initial-authors">
                  <h1>Author</h1>
                  <ul>
                    
                    <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                    
                    <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                    
                    <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                    
                    <li><a href="#" onClick="$('#article-initial-authors').hide(); $('#article-all-authors').show();"><strong>See all authors&hellip;</strong></a></li>
                  </ul>
              </section>
              <section id="article-all-authors" style="display: none;">
                <h1>
                  All Authors
                  (<a href="#" id="article-hide-all-authors">hide</a>)
                </h1>
                <ul>
                  
                  <li><a href="../author/raluca-budiu/index.php">Raluca Budiu</a></li>
                  
                  <li><a href="../author/patty-caya/index.php">Patty Caya</a></li>
                  
                  <li><a href="../author/susan-farrell/index.php">Susan Farrell</a></li>
                  
                  <li><a href="../author/therese-fessenden/index.php">Therese Fessenden</a></li>
                  
                  <li><a href="../author/kim-flaherty/index.php">Kim Flaherty</a></li>
                  
                  <li><a href="../author/sarah-gibbons/index.php">Sarah Gibbons</a></li>
                  
                  <li><a href="../author/aurora-harley/index.php">Aurora Harley</a></li>
                  
                  <li><a href="../author/kate-kaplan/index.php">Kate Kaplan</a></li>
                  
                  <li><a href="../author/page-laubheimer/index.php">Page Laubheimer</a></li>
                  
                  <li><a href="../author/angela-li/index.php">Angie Li</a></li>
                  
                  <li><a href="../author/hoa-loranger/index.php">Hoa Loranger</a></li>
                  
                  <li><a href="../author/kate-meyer/index.php">Kate Meyer</a></li>
                  
                  <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                  
                  <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                  
                  <li><a href="../author/kara-pernice/index.php">Kara Pernice</a></li>
                  
                  <li><a href="../author/christian-rohrer/index.php">Christian Rohrer</a></li>
                  
                  <li><a href="../author/amy-schade/index.php">Amy Schade</a></li>
                  
                  <li><a href="../author/katie-sherwin/index.php">Katie Sherwin</a></li>
                  
                  <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                  
                  <li><a href="../author/kathryn-whitenton/index.php">Kathryn Whitenton</a></li>
                  
                </ul>
              </section>
              
            </div>
            
          </div>

          
          <section class="hide-for-small-down">
            
              
              <h1>Recent Articles</h1>
              
              <ul>
                  
                  <li><a href="../seamless-cross-channel/index.php">Seamlessness in the Omnichannel User Experience</a></li>
                  
                  <li><a href="../brand-intention-interpretation/index.php">Experience Design: Bridging Brand Intention and Brand Interpretation</a></li>
                  
                  <li><a href="../eyetracking-tasks-efficient-scanning/index.php">Scanning Patterns on the Web Are Optimized for the Current Task</a></li>
                  
                  <li><a href="../flat-design-best-practices/index.php">Flat-Design Best Practices</a></li>
                  
                  <li><a href="../ux-user-stories/index.php">Accounting for UX Work with User Stories in Agile Projects</a></li>
                  
                  <li><a href="../index.php"><strong>See all articles&hellip;</strong></a></li>
              </ul>
              
            
          </section>
          

          
          
          <section class="hide-for-small-down">
            <h1>Popular Articles</h1>
            
            <ul>
                
                <li><a href="../usability-101-introduction-to-usability/index.php">Usability 101: Introduction to Usability</a></li>
                
                <li><a href="../top-10-mistakes-web-design/index.php">Top 10 Mistakes in Web Design</a></li>
                
                <li><a href="../how-users-read-on-the-web/index.php">How Users Read on the Web</a></li>
                
                <li><a href="../f-shaped-pattern-reading-web-content/index.php">F-Shaped Pattern For Reading Web Content</a></li>
                
                <li><a href="../ten-usability-heuristics/index.php">10 Usability Heuristics for User Interface Design</a></li>
                
                <li><a href="../intranet-design-2016/index.php">10 Best Intranets of 2016</a></li>
                
                <li><a href="../which-ux-research-methods/index.php">When to Use Which User-Experience Research Methods</a></li>
                
                <li><a href="../response-times-3-important-limits/index.php">Response Times: The 3 Important Limits</a></li>
                
                <li><a href="../why-you-only-need-to-test-with-5-users/index.php">Why You Only Need to Test with 5 Users</a></li>
                
                <li><a href="../page-fold-manifesto/index.php">The Fold Manifesto: Why the Page Fold Still Matters</a></li>
                
            </ul>
          </section>
          
          

          
            <section class="hide-for-small-down">
              
            </section>
          

      </section>
      

      
        <div class="small-12 medium-9 columns content-body">
          




<article class="l-subsection alertbox-article-content article-detail-bl">
  
  <header>
    <div class="l-subsection">
      <h1>1994 Design of SunWeb: Sun Microsystems&#39; Intranet</h1>
      <div class="author-meta">by
        
          
            
            <a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a>
            
          
        on  December 31, 1994
        
      </div>

      

      
      <div class="topic-links">
        Topics:
        
          <ul>

  <li><a href="../../topic/information-architecture/index.php">Information Architecture</a></li>

  <li><a href="../../topic/intranets/index.php">Intranets</a></li>

  <li><a href="../../topic/navigation/index.php">Navigation</a></li>

</ul>
        
      </div>
      
    </div>
  </header>
  

  
  
  <section>
    <p class="lede"><strong>Summary:</strong> This paper presents the methods used to design the user interface and overall structure of the internal web pages (intranet) for Sun Microsystems. Sun had an extensive set of information available on the WWW with the home page as the access point, but also wanted to provide employees access to internal information that could not be made available to the Internet at large.

Much of the user interface work was done in a few weeks with four usability studies completed in a single week: card sorting to discover the users&#39; view of the information space, an icon intuitiveness study, and two tests of the actual interface.</p>
    <br />
  </section>
  
  

  
  <section id="article-body"><p><strong>Note added May 1995: </strong> This report was written for traditional print publication and is thus not a paragon of hypertext design. Also, the design described in this report is clearly "last year's style" on the Web with its profusion of icons and options on the home page. The design is appropriate for its intended use (a corporate-wide internal information system) but should not be emulated for Web sites intended to present a company's image on the Internet.<br/>
<strong>Added 2002</strong>: For more recent information about intranet design and usability, see Nielsen Norman Group's <a href="../../reports/topic/intranets/index.php" title="Listing of reports with links to immediate download"> usability studies on intranets and Intranet Design Annuals</a>.</p>

<h2>Background</h2>

<p>Sun Microsystems is a leading vendor of computer workstations and system software with a large number of branches all over the world. The company has large amounts of internal information that would be of interest to employees but which currently exists in inconsistent formats and in locations that are difficult for new employees to discover. Sun had earlier produced a set of World Wide Web pages with external information for access through the Internet and that system had been favorably received both inside and outside the company, and it was a natural idea to follow up with an internal Web service, especially given that all Sun employees use networked workstations.</p>

<p>For security purposes, it was desirable to have separate visual identities for the internal and external pages (making it clear to users and authors when information could be accessed by outsiders). Also, the type of information available through the internal web was in many ways different in nature from the external information, meaning that a different design would be appropriate. Therefore, a separate SunWeb project was initiated in order to design Sun's internal Web.</p>

<p>SunWeb was designed during the summer of 1994, a period where the number of Web servers on the Internet grew at a rate of about 10,000% per year (doubling every two months). We suspect that the growth of internal Web servers inside Sun's firewall was even faster in this period. Many Sun employees had already built home pages for groups and individuals before the start of the SunWeb project and many more were coming online during the project. As another indication of the growth of the Web inside Sun, the SunWeb server was accessed by 2,649 different employees during a 34-day period of testing where it had not even been announced yet. Thus, work on SunWeb had to be completed within a few months if we were to have any hope of achieving a consistent user interface and not just glom together a set of disparate designs.</p>

<h2>Usability Plan</h2>

<p>Because of the need to deliver a design quickly, we decided to rely on "discount usability engineering" [Nielsen 1994] to ensure the usability of the design. Discount methods are deliberately informal and rely less on statistics and more on the interface engineer's ability to observe users and interpret results. A major benefit of these methods is that they are fast, and since we were not developing, say, an aircraft cockpit or other interfaces with lives at stake, we were willing to risk the possible downside of not finding every last usability problem in our designs.</p>

<p>We were able to conduct four usability studies over a period of two weeks during which we tested the interface at various stages of design. We used the following sequence of studies:</p>

<ol>
	<li>Card sorting to discover categories (4 users)</li>
	<li>Icon intuitiveness testing (4 users)</li>
	<li>Card distribution to icons (4 users)</li>
	<li>Thinking aloud walkthrough of page mock-up (3 users)</li>
</ol>

<p>The participants in the last two tests were also asked for ratings of icon aesthetics. Note that we used different users in each of the four studies in order to avoid any learning effects.</p>

<h3>Card Sorting</h3>

<p><a href="../card-sorting-how-many-users-to-test/index.php">Card sorting</a> is a common usability technique that is often used to discover users' mental model of an information space. A typical application is to get ideas for menu structures by asking users to sort cards with the command names: commands that get sorted together often should probably be in the same menu. A downside of the method is that users may not always have optimal models, and card sorting (or other similarity measures) is often used to assess the difference between the way novice and expert users understand a system.</p>

<p>Before our card sorting study, the SunWeb development group had brainstormed about possible information services to be provided over the system, resulting in a list of 51 types of information. We wrote the name of each of these services on a 3-by-4 inch notecard. In a few cases we also wrote a one-line explanation on the card.</p>

<p>Before each user entered the lab, the cards were scattered around the desk in random order. The users were asked to sit down at a table and sort the cards into piles according to similarity. Users were encouraged not to produce too small or too large piles but they were not requested to place any specific number of cards in each pile. After a user had sorted the cards into piles of similar cards, the user was asked to group the piles into larger groups that seemed to belong together and to invent a name for each group. These names were written on Post-It notes and placed on the table next to the groups. The users typically completed the entire process in about 30 minutes, though some took about 40 minutes.</p>

<p>The data from this study was lists of named groups of cards with subgroups corresponding to the original piles. Based on this information, it is possible to calculate similarity ratings for the various concepts by giving a pair of concepts one similarity point for each time both concepts occur in the same larger cluster and two points for each time they occur in the same pile. This similarity matrix can then be fed to one of the many standard statistics packages for a cluster analysis. It is also possible to use other statistical techniques such as multidimensional scaling.</p>

<p>We actually computed a statistical cluster analysis at the end of the project, but we initially did not have time to code the data appropriately, so our design was based on "data eyeballing" and not on formal statistics. Given our discount usability engineering approach with only four users, the statistical methods are not that reliable anyway but, as it turned out, the statistical cluster analysis was very similar to that we had constructed manually. For our manual clustering, we worked bottom- up and initially grouped concepts that most users had sorted together. We then expanded these small groups into larger clusters by adding concepts that some users had sorted with most of the concepts in the group if the grouping made sense to us. This subjective interpretation of the data is dubious if the objective "truth" is desired, but in our case we were after a coherent design, so we felt justified in applying our judgment in those cases where the user data was too sparse for a clear conclusion to be drawn on the basis of the numbers. Eyeballing the data took one person about two hours and resulted in a list of recommended groups of features as well as initial names for the groups. The names were often inspired by the labels provided by the users, but due to the verbal disagreement phenomenon [Furnas et al. 1987], the users obviously had not used exactly the same terms, so we also had to apply our judgment in choosing appropriate names.</p>

<h3>Icon Intuitiveness</h3>

<p>Based on the results from the card sorting study, we defined fifteen first-level groupings of the information in SunWeb and designed icons for each of them for use on the home page. The icons would be presented with labels to enhance users' ability to understand. Even so, we wanted to make the icons themselves as understandable as possible, and in order to achieve this goal we conducted an icon intuitiveness study where four users were shown the icons without labels and asked to tell us what they thought each icon was supposed to represent.</p>

<table border="1">
	<caption align="bottom"><strong>Table 1. </strong> <em> Results of icon intuitiveness study with four users (some users gave more than one interpretation of some icons). </em></caption>
	<tbody>
		<tr valign="middle">
			<td><img alt="" src="http://media.nngroup.com/media/editor/2012/10/31/icon_globe.gif" style="width: 76px; height: 48px;"/></td>
			<td><em>Intended Meaning: </em> Geographic view of the company (branch offices in different locations).</td>
			<td><em>Test Users' Interpretations: </em> World, global view, planet, the world, Earth.</td>
		</tr>
		<tr valign="middle">
			<td><img alt="Money bag icon" src="http://media.nngroup.com/media/editor/2012/10/31/icon_benefits.gif" style="width: 76px; height: 48px;"/></td>
			<td><em>Intended Meaning: </em> Benefits.</td>
			<td><em>Test Users' Interpretations: </em> Health field, money, health care is expensive, Clinton's health plan, hospital, don't know, benefits.</td>
		</tr>
		<tr valign="middle">
			<td><img alt="TV icon" src="http://media.nngroup.com/media/editor/2012/10/31/icon_tv.gif" style="width: 76px; height: 48px;"/></td>
			<td><em>Intended Meaning: </em> Public relations (TV with commercial).</td>
			<td><em>Test Users' Interpretations: </em> TV set, video, TV, TV, TV.</td>
		</tr>
		<tr valign="middle">
			<td><img alt="computer and disc icon" src="http://media.nngroup.com/media/editor/2012/10/31/icon_productcatalog.gif" style="width: 76px; height: 48px;"/></td>
			<td><em>Intended Meaning: </em> Product catalog.</td>
			<td><em>Test Users' Interpretations: </em> System oriented, disk, CD, Computer, CD-ROM, CD-ROM.</td>
		</tr>
		<tr valign="middle">
			<td><img alt="briefcase icon" src="http://media.nngroup.com/media/editor/2012/10/31/icon_toolbox1.gif" style="width: 76px; height: 48px;"/></td>
			<td><em>Intended Meaning: </em> Specialized tools (toolbox).</td>
			<td><em>Test Users' Interpretations: </em> Briefcase, personal info, briefcase, toolbox, briefcase.</td>
		</tr>
		<tr valign="middle">
			<td><img alt="bulletin board icon" src="http://media.nngroup.com/media/editor/2012/10/31/icon_bulletinboard.gif" style="width: 76px; height: 48px;"/></td>
			<td><em>Intended Meaning: </em> What's new (bulletin board).</td>
			<td><em>Test Users' Interpretations: </em> Bulletin board, bulletin board, bulletin board, laundry.</td>
		</tr>
		<tr valign="middle">
			<td><img alt="globe with lines overlay icon" src="http://media.nngroup.com/media/editor/2012/10/31/icon_www1.gif" style="width: 76px; height: 48px;"/></td>
			<td><em>Intended Meaning: </em> World Wide Web.</td>
			<td><em>Test Users' Interpretations: </em> Networking on a world scale, map, location, dimensions of the planet, networking around the world, geography, global.</td>
		</tr>
	</tbody>
</table>

<p>Table 1 shows the results of the icon intuitiveness testing for some of our initial icons. Some icons passed the test easily, with most users either guessing the intended meaning or at least guessing something that was very close and would not be misleading in the context of the full system. For example, only one user explicitly used the word "benefits" to describe our "employee benefits" icon, but descriptions like "hospital" and "Clinton's health plan" actually show that people got the general idea. In cases like this, we were satisfied that users would understand the icon when it was combined with its label in the full system, so we did not feel a need to change it, especially given that we had planned several additional user tests that would reveal any hidden problems. A minor adjustment was made to reduce the emphasis on the money bag.</p>

<p>In other cases, users did not get exactly the correct meaning of an icon but we decided to keep it anyway. The product catalog was one such example, because the CD-ROM component of the icon was so visually powerful that most users focused on it and not on the group of system components. The underlying problem is that software is an abstract concept that is difficult to visualize. In another icon we tried to show "software" by a code listing but that also proved difficult for users to recognize. Our final decision was to keep the product catalog icon since users did recognize its components. In the final design (see Figure 4), we made the workstation screen more visually prominent by giving it a more saturated color.</p>

<p>With respect to the "what's new" bulletin board, one user claimed that it looked like laundry. Even so, three of the four users recognized the icon immediately and we decided to keep it. When conducting user testing with a small number of users it is important not to let results from individual users influence one's decisions unduly. Seen from one perspective, 25% of our sample had trouble with the icon, but seen from another (and more appropriate) perspective, we happened to have asked a person who did not recognize the icon. When designing based on small samples of test users, one has to apply judgment based on general usability principles and one's experience with interaction principles since the data itself is too sparse to use without interpretation.</p>

<p>The toolbox icon was seen by most users as a briefcase. Since briefcases have completely different connotations, this icon would have been misleading to users and we decided to redesign the it by adding a monkey wrench to emphasize the "tool" aspect. Finally, the World Wide Web icon was reasonable: some users got it almost exactly right by guessing "networking on a world scale" or some such, but many of the interpretations had to do with geography or the globe. Since we already had a globe icon to signify geography, these erroneous interpretations of the WWW icon were problematic and we decided to redesign with more emphasis on the network. We also replaced the globe with a flat map to distinguish the WWW icon further from the geography icon.</p>

<h3>Card Distribution to Icons (a Variant of Closed Card Sorting)</h3>

<p>After a redesign of those icons that tested poorly in the intuitiveness test, we proceeded to test whether users would associate the correct concepts with each of the groups we had defined. For this test, we mocked up a table in our usability laboratory as a large-scale version of the eventual home page design, as shown in Figure 1. We had printed out large magnifications of the icons on a color printer and placed them on the desk in an approximation of the layout we had planned for the SunWeb home page. Even though the desk was several times larger than a typical workstation window, we only printed the icons at 200% magnification since this size (four times the area of screen icons) were large enough to be seen from a distance, whereas larger icons seemed strange. We tested the icons both with and without labels, but we would recommend including the labels in future testing since we did not learn that much more from removing the labels than we had already learned in the icon intuitiveness study.</p>

<p><img alt="Usability Lab photo" src="http://media.nngroup.com/media/editor/2012/10/31/user_testing.gif" style="width: 499px; height: 418px;"/></p>

<p><strong>Figure 1. </strong> <em> Card distribution to icons in the usability laboratory. In preparation for the test, the icons were printed out in 200% magnification on a color printer and Post-It tape was used to divide the table into areas for each icon. For the test, the user is given a pile of cards and asked to place each card in the area with the most appropriate icon. An observer is in the room with the user to give instructions and take notes. Other observers can watch though the one-way mirror and the session is also videotaped in case further analysis is necessary later.<br/>
[Note added <strong> 2013</strong>: Why does this photo look so bad? Because it was digitized in 1994 and we needed to keep it within a "safe" color space of 50 colors to avoid flicker on the lower-end range of Sun workstations at the time.] </em></p>

<p>We used Post-It(TM) tape to divide the desk into areas for each icon, and each of test user was then asked to distribute the cards among these areas, with each card going to the (labelled) icon most appropriate for it. At the end of the card distribution test, the users were asked to comment on the aesthetics of the icons and to list the icons they liked the most and disliked the most. Figure 1 shows the lab setup during the card distribution test. The card distribution tests took about fifteen minutes per user.</p>

<h3>Thinking Aloud Page Walkthrough</h3>

<p>For our final usability test, we printed out a magnified color screendump of our design for the SunWeb home page. We wanted to test a <a href="../../reports/paper-prototyping-training-video/index.php" title="Nielsen Norman Group: Paper Prototyping, A How-To Video (32 minute film)"> paper version</a> instead of a screen version to avoid the problem of users clicking on buttons that at that time had no effect. The test users were asked to point to each button and think aloud what information they thought would be accessed through that button. At the end of the page walkthrough, the users were asked to comment on the aesthetics of the icons and to list the icons they liked the most and disliked the most.</p>

<p>The card distribution and page walkthrough studies revealed several usability problems in our revised design, leading to additional design changes before we could release the SunWeb user interface. One of these new usability problems is discussed in further detail in the following section. In retrospect, though, we feel that we learned the most from the first two studies, the card sorting and the icon intuitiveness study. In general, we will always recommend thinking aloud studies where users perform tasks with the full user interface, but in the special case where the command structure and interaction techniques are determined by the World Wide Web viewer it was more important to study the information structure that we could actually influence.</p>

<p>Asking users for subjective ratings of icon aesthetics proved to be useful. Even though people have different taste and liked and disliked different icons, there were two icons that were singled out for criticism by most users: a blackboard we had used to represent education and the TV icon we had used to represent sales, marketing, and PR. Users had no trouble recognizing the TV (see Table 1) and most users easily understood that it represented promotional materials (though only a few of them recognized it as showing a "Sun TV commercial," which was one of the services that we planned to make available through SunWeb's multimedia features). Users just did not like it.</p>

<p>Since subjective satisfaction is at least as important as task performance for a system like SunWeb where we want to encourage people to browse and learn more about their company, we removed these most hated icons and replaced them with more attractive ones. The blackboard was replaced with a graduation cap and a diploma to represent education, and the TV set was replaced with a spotlight to represent promotional materials. Even though the American-style graduation cap is problematic from an international use perspective (not all countries use these caps), we decided to use it for this system because it is intended for internal use in Sun which is a company where most employees understand English and basic aspects of American culture.</p>

<h2>Iterative Design</h2>

<p>The SunWeb user interface was developed by iterative design, meaning that new versions of the interface were designed each time we discovered a weakness in its usability. We often had to go through a large number of revisions since our "fixes" to the user interface sometimes turned out not to work. An example is shown in Figure 2. Actually, Figure 2 only shows the five main iterations that were tested with users. In total, we designed twenty versions of the icon: seven tool metaphor icons, nine shopping metaphor icons (including shopping carts and a grocery shelf), and four chest icons. In order to produce our many different designs, ideas were gleaned from a thesaurus, a visual dictionary, and catalogs of international signs and symbols.</p>

<p><img alt="briefcase icon" src="http://media.nngroup.com/media/editor/2012/10/31/icon_toolbox1_.gif" style="width: 76px; height: 48px;"/> <img alt="toolbox with wrench icon" src="http://media.nngroup.com/media/editor/2012/10/31/icon_toolbox2.gif" style="width: 76px; height: 48px;"/> <img alt="storefront icon" src="http://media.nngroup.com/media/editor/2012/10/31/icon_storefront1.gif" style="width: 76px; height: 48px;"/> <img alt="alternative storefront icon" src="http://media.nngroup.com/media/editor/2012/10/31/icon_storefront2.gif" style="width: 76px; height: 48px;"/> <img alt="chest icon" src="http://media.nngroup.com/media/editor/2012/10/31/icon_chest.gif" style="width: 76px; height: 48px;"/></p>

<p><strong>Figure 2. </strong> <em> Five iterations of the icon to represent a group of special-purpose applications. The first two represent the toolbox metaphor, the next two are "application stores," and the last is the "application chest." </em></p>

<p>As mentioned earlier, the initial toolbox icon was interpreted as a briefcase by most users, so we opened it up and added a monkey wrench. This redesign worked and users in our card distribution study and page walkthroughs had no problems and users sorted a very large numbers of cards onto this icon with the comment that <cite> "oh, this is a tool." </cite> Essentially, almost any concept that represented an executable program was considered a tool. An example is the expense report application which should have been grouped with the travel icon but was often placed in the toolbox.</p>

<p>In order to use a weaker metaphor for the special-purpose applications, we next tried a shopping metaphor with an icon showing a storefront (the middle icon in Figure 2). When we conducted an icon intuitiveness study and showed this icon to a user, he immediately said <cite> "this is a circuit board." </cite> This user happened to be an engineer, but since we do have a very large number of engineers in the user population for SunWeb, we decided to take this comment seriously and redesigned the icon. This is an example where our judgment as user interface specialists was to rely on a result from a single user since we felt that this user's problems would be frequent in real use of the system.</p>

<p>We tried several alternative storefront and other shopping icons (see the fourth icon in Figure 2) before realizing that a successful shopping icon would interfere with one of our other interface elements: the "product catalog" icon. Therefore, we dropped the application store as the metaphor and we finally settled on the "application chest" icon shown as the last icon in Figure 2.</p>

<h2>Unified Design</h2>

<p>To make SunWeb both usable and aesthetically pleasing, basic visual design techniques were applied to graphic elements. A consistent visual design also provided the necessary orientation cues and navigational controls critically needed by users. Applying the same techniques consistently provided users with a predictable location for information and controls on every page of SunWeb. This reinforced user expectations across the entire system of SunWeb, thus enhancing satisfaction and usability.</p>

<p><img alt="Screenshot of two levels of navigation bars" src="http://media.nngroup.com/media/editor/2012/10/31/sunweb_header.gif" style="width: 468px; height: 224px;"/></p>

<p><strong>Figure 3. </strong> <em> SunWeb main homepage masthead (top) and second level masthead (below). </em></p>

<p>A systematic design solution was needed to provide page identification and access to essential controls, and this was accomplished by developing "banners" (see Figure 3). Present at the top of every SunWeb page, the banners provide a category of essential global controls, including Search, Overview, and Help (C). The vast scope of information included in SunWeb required a Search engine as a prerequisite from the initial functional specification stage. An Overview in the form of a comprehensive content list was also deemed necessary. These buttons were rendered in a somewhat subdued grey, so as not to compete or overpower the other elements on the page. The subdued uniformity also visually separates functionality from the more colorful category icons found on the home page. The SunWeb identifier mark (A) is consistently located on the left half of the banner. This logotype area provides SunWeb the necessary visual distinction from the "look" of Suns' external public server. This area also functions as the link mechanism back to the main SunWeb homepage from anywhere in the system, and is reinforced by a descriptive label. On second level banners, the paging buttons (D) are highlighted when available to allow users paging capability through a series of information, for example, the homepages of members of a certain engineering team, or pages in a product catalog. This avoids the back-next selection cycle, a tedious routine for users if numerous categories of information are available. The 15 different categories of SunWeb information represented by icons are integral second level banner components (B). These icons embedded in the banner function as direct links back to that particular category homepage. In SunWeb, these 15 categories may be thought of as major "hubs," as found in airline route systems, and provide the major arteries through the information space. The ability for users to quickly return to these central homepages was a crucial navigational feature. By clicking on the icon, the user will immediately link to that category homepage, no matter how removed the user may currently be from the second level. The banner icons also function as location cues for user position within SunWeb.</p>

<p>In order to facilitate consistency among contributors to SunWeb, banner components were placed in a central repository, and usage encouraged by the development team. Although not mandated to adopt the banner gif files or adhere to a particular design, many content contributors felt it necessary to "fit-in", and wished to appear consistent with the rest of the top-level SunWeb environment. In this instance, a suitable design solution "given away" provided the initial momentum for widespread exposure and adoption. Banner specification drawings helped to clarify the design, and insured proper implementation. These drawings were distributed in hard copy during the development phase meetings to the various content providers.</p>

<p>As with the icons, the SunWeb banners relied on a minimal color palette in order to reduce the possibility of color-map flashing and unpredictable results. This also would reserve more color space for the content providers, who may feel it necessary to include an undetermined amount of continuous tone photographs, color illustrations and charts, etc., into their SunWeb pages. Subsets from a 64 color palette were adopted for all graphic elements in SunWeb. This would insure hue saturation, intensity, and value consistency across components.</p>

<p><img alt="Screenshot of final intranet homepage launched 1994" src="http://media.nngroup.com/media/editor/2012/10/31/sunweb_final_homepage.gif" style="width: 564px; height: 518px;"/></p>

<p><strong>Figure 4. </strong> <em> The final home page design for SunWeb. Icons sharing design attributes, such as frontal orientation and color palette, insures a visual continuity across the image ensemble. Each icon occupies a similar amount of space within the button graphic, so as to equalize image density. Pre-defined colors are used throughout the icon imagery which represents the 15 major categories of information provided by SunWeb. This provides the necessary visual contrast with the global icon controls in the SunWeb banner, which visually remain monochromatic. Grouping and placement of the categories on the page reflects a hierarchy based on related topics and information precedence. Higher priority information is placed at the top, while categories of lesser importance towards the bottom. It is interesting to note that first iteration of the homepage design presented each icon as a separate graphic ismap. However, due to the length of time required by Mosaic to load each individual image, one at a time, the homepage was redesigned as one large ismap containing all button categories. This dramatically reduced access time for users. Textual labels in the smallest font available was positioned below the ismap (not shown), in case users turn off the load picture functionality. A design should always take into account users who are concerned with speed of access, and therefore prefer not to load any of the graphic gif files. </em></p>

<h2>References</h2>

<p>Furnas, G. W., Landauer, T. K., Gomez, L. M., and Dumais, S. T. (1987). The vocabulary problem in human-system communication. <em> Communications of the ACM </em> <strong> 30 </strong> , 11 (November), 964-971.</p>

<p>Mullet, Kevin, and Sano, Darrell (1994). <a href="http://www.amazon.com/exec/obidos/ISBN=0133033899/ref=nosim/useitcomusableinA/"> <em> Designing Visual Interfaces: Communication Oriented Techniques</em></a>. Sun Microsystems Press/Prentice Hall, New Jersey.</p>

<p>Nielsen, J. (1994). <em> <a href="../../books/usability-engineering/index.php"> Usability Engineering</a></em>, paperback edition. Academic Press, Boston.</p>
</section>
  

  
  <section>
    
        
    
  </section>
  
  
  
  
  

  


  <section>
    <br />
    <p class="share"><b>Share <span class="hide-for-small-down">this article</span>:</b> <a href="https://twitter.com/intent/tweet?url=http://www.nngroup.com/articles/1994-design-sunweb-sun-microsystems-intranet/&amp;text=1994%20Design%20of%20SunWeb:%20Sun%20Microsystems'%20Intranet&amp;via=nngroup">Twitter</a> | <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=http://www.nngroup.com/articles/1994-design-sunweb-sun-microsystems-intranet/&amp;title=1994%20Design%20of%20SunWeb:%20Sun%20Microsystems'%20Intranet&amp;source=Nielsen%20Norman%20Group">LinkedIn</a> | <a href="https://plus.google.com/share?url=http://www.nngroup.com/articles/1994-design-sunweb-sun-microsystems-intranet/">Google+</a> | <a href="mailto:?subject=NN/g Article: 1994 Design of SunWeb: Sun Microsystems&#39; Intranet&amp;body=http://www.nngroup.com/articles/1994-design-sunweb-sun-microsystems-intranet/">Email</a></p>
    <hr />
  </section>

  <footer class="l-subsection">
    
    
    <div class="learn-more">
      <h2>Learn More</h2>
      <p><a href="../subscribe/index.php">Subscribe to the weekly newsletter</a> to get notified about future articles.</p>
      </div>
    
    

    <div class="row show-for-small-down related-content">
      
      
      

      
      
        <div class="column small-12 topic-links">All articles about:
          <ul>
            
            <li><a href="../../topic/information-architecture/index.php">Information Architecture</a></li>
            
            <li><a href="../../topic/intranets/index.php">Intranets</a></li>
            
            <li><a href="../../topic/navigation/index.php">Navigation</a></li>
            
          </ul>
        </div>
      
      

      
      
        <div class="column small-12">
          <h3>UX Conference Training Courses</h3>
           <ul class="no-bullet">
    
        <li><a href="../../courses/information-architecture/index.php">Information Architecture</a></li>
    
        <li><a href="../../courses/navigation-design/index.php">Navigation Design</a></li>
    
</ul>
        </div>
      
      

      
      
        <div class="small-12 column">
          <h3>Research Reports</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../reports/intranet-design-annual/index.php">2017 Intranet Design Annual</a></li>
              
            
              
                <li><a href="../../reports/intranet-usability-guidelines/index.php">Intranet Usability Guidelines: Findings from User Testing of 42 Intranets</a></li>
              
            
              
                <li><a href="../../reports/intranet-navigation-layout-and-text/index.php">Vol. 07: Navigation and Page Layout</a></li>
              
            
              
                <li><a href="../../reports/intranet-portals-experiences-real-life-projects/index.php">Intranet Portals: UX Design Experience from Real-Life Projects</a></li>
              
            
              
                <li><a href="../../reports/intranet-information-architecture-design-methods/index.php">Intranet Information Architecture Design Methods and Case Studies</a></li>
              
            
          </ul>
        </div>
      
      

      
      
        <div class="small-12 column">
          <h3>Online Seminars</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../online-seminars/improving-intranet-content/index.php">Improving Intranet Content</a></li>
              
            
              
                <li><a href="../../online-seminars/intranet-users-find/index.php">Helping Intranet Users Find What They Need</a></li>
              
            
          </ul>
        </div>
      
      
    </div>

    <div class="hide-for-small-down row related-content">
      <div class="medium-6 columns">
        
        
          <div>
            <h3>Articles</h3>
            <ul class="no-bullet">
              
                
                <li><a href="../ia-warning-signs-analytics/index.php">5 Information Architecture Warning Signs in Your Analytics Reports</a></li>
                
              
                
                <li><a href="../ia-questions-navigation-menus/index.php">Top 3 IA Questions about Navigation Menus</a></li>
                
              
                
                <li><a href="../fixing-bad-intranet-navigation/index.php">Bad Intranet Navigation Labels: 3 Workarounds</a></li>
                
              
                
                <li><a href="../intranet-information-architecture-ia/index.php">Intranet Information Architecture (IA) Trends</a></li>
                
              
                
                <li><a href="../navigation-you-are-here/index.php">Navigation: You Are Here</a></li>
                
              
            </ul>
          </div>
        
        

        
          
          <div>
            <h3>Research Reports</h3>
            <ul class="no-bullet">
              
                
                  <li><a href="../../reports/intranet-design-annual/index.php">2017 Intranet Design Annual</a></li>
                
              
                
                  <li><a href="../../reports/intranet-usability-guidelines/index.php">Intranet Usability Guidelines: Findings from User Testing of 42 Intranets</a></li>
                
              
                
                  <li><a href="../../reports/intranet-navigation-layout-and-text/index.php">Vol. 07: Navigation and Page Layout</a></li>
                
              
                
                  <li><a href="../../reports/intranet-portals-experiences-real-life-projects/index.php">Intranet Portals: UX Design Experience from Real-Life Projects</a></li>
                
              
                
                  <li><a href="../../reports/intranet-information-architecture-design-methods/index.php">Intranet Information Architecture Design Methods and Case Studies</a></li>
                
              
            </ul>
          </div>
          
        

      </div>
      <div class="medium-6 columns">
        
          
          <div>
            <h3>UX Conference Training Courses</h3>
             <ul class="no-bullet">
    
        <li><a href="../../courses/information-architecture/index.php">Information Architecture</a></li>
    
        <li><a href="../../courses/navigation-design/index.php">Navigation Design</a></li>
    
</ul>
          </div>
          
        
        
          
          <div>
            <h3>Online Seminars</h3>
            <ul class="no-bullet">
            
                <li><a href="../../online-seminars/improving-intranet-content/index.php">Improving Intranet Content</a></li>
            
                <li><a href="../../online-seminars/intranet-users-find/index.php">Helping Intranet Users Find What They Need</a></li>
            
            </ul>
          </div>
          
        
      </div>
    </div>
  </footer>
</article>

        </div>

      

    
  </div>
  <div class="row">
    
      <div class="column small-12 show-for-small-down l-navrail announcements">
        
          
        
      </div>
    
    
  </div>

    </div>
    




<footer id="page-footer">
    <div class="row hide-for-print">
        <div class="column small-12 medium-6">
            <section class="footer-popular">
                
                <h1>Popular Topics</h1>
                <div style="float: left;  width: 15em; display: block;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/applications/index.php">Application Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/e-commerce/index.php">E-Commerce Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/eyetracking/index.php">Eyetracking </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/information-architecture/index.php">Information Architecture </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/intranets/index.php">Intranet Design </a></li>
	<li><a href="../../topic/interaction-design/index.php">Interaction Design </a></li>
</ul>
</div>

<div style="float: left;  width: 15em; display: block; list-style: none;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/mobile-and-tablet-design/index.php">Mobile and Tablet Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/credibility-persuasion/index.php">Persuasive Design</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/research-methods/index.php">Research Methods</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/user-testing/index.php">User Testing </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/web-usability/index.php">Web Usability </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/writing-web/index.php">Web Writing &amp; Content Strategy </a></li>
</ul>
</div>

<p>&nbsp;</p>

                
            </section>
        </div>

        <div class="column small-12 medium-6">
            <div class="row">
                <div class="column small-6">
                    <section class="footer-about">
                        <h1>Follow Us</h1>
                        <ul>
	<li><a href="../subscribe/index.php">Subscribe to our weekly UX newsletter</a>,&nbsp;Jakob Nielsen&#39;s Alertbox</li>
	<li>Visit Don Norman&#39;s website, <a href="http://www.jnd.org/"> www.jnd.org </a></li>
	<li>Visit Bruce Tognazzini&#39;s website, <a href="http://www.asktog.com/"> www.asktog.com </a></li>
	<li style="padding-top: 6px;"><a class="twitter-follow-button" data-show-count="false" data-size="large" href="https://twitter.com/nngroup"><img alt="Follow @nngroup on Twitter" height="20" src="https://media.nngroup.com/media/editor/2016/12/30/twitter-follow-button.png" style="width: 117px; height: 20px;" width="117" /></a></li>
	<li style="padding-top: 6px;"><img alt="" height="14" src="https://media.nngroup.com/media/editor/2016/07/26/feed-icon-14x14.png" style="vertical-align:text-top;width:14px;margin-right:.25em;" width="14" /><a href="../../feed/rss/index.php">Subscribe to our RSS feed</a></li>
</ul>

                    </section>
                </div>

                <div class="column small-6">
                    <section class="footer-about right-column">
                        <h1>About
</h1>
                        <ul class="no-bullet">
	<li><a href="../../about/contact/index.php">Contact information</a></li>
	<li><a href="../../about/index.php">About Us</a></li>
	<li><a href="../../about/why-nng/index.php">Why NN/g? </a></li>
	<li><a href="../../return-policy/index.php">Return Policy</a></li>
</ul>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="copyright column small-12">
            <p><a href="../../copyright-and-reprint-info/index.php">Copyright</a> &copy; 1998-2017 Nielsen Norman Group, All Rights Reserved.</p>
        </div>
    </div>
</footer>


    


        <script src="https://media.nngroup.com/static/vendor/fastclick/js/fastclick.min.js"></script>
        <script src="https://media.nngroup.com/static/vendor/foundation/js/foundation.min.js"></script>
        <script src="http://media.nngroup.com/static/js/app.js"></script>
        <script src="http://media.nngroup.com/static/js/scroll-to-fixed.js"></script><script>var externalLinks = document.querySelectorAll('a[href^="http"]');for (var i = externalLinks.length-1; i >= 0; i--) {externalLinks[i].addEventListener("click", function(event) { alert("External links are disabled for this test."); event.preventDefault(); }, false);}var mailLinks = document.querySelectorAll('a[href^="mailto"]');for (var i = mailLinks.length-1; i >= 0; i--) {mailLinks[i].addEventListener("click", function(event) { alert("Email links are disabled for this test."); event.preventDefault(); }, false);}var externalForms = document.querySelectorAll('form');for (var i = externalForms.length-1; i >= 0; i--) {externalForms[i].addEventListener("submit", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false); externalForms[i].addEventListener("click", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false);}</script>

        <script>
            
            $(function() {
                $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
                $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
            });
        </script>

        
<script src="https://media.nngroup.com/static/js/articles.js"></script>


        
        <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1072699960;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072699960/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
    </body>

<!-- Mirrored from www.nngroup.com/articles/1994-design-sunweb-sun-microsystems-intranet/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:14:37 GMT -->
</html>
