<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->





    
<!-- Mirrored from www.nngroup.com/articles/medical-usability/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:14:20 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1026.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"licenseKey":"1a30ef9931","errorBeacon":"bam.nr-data.net","applicationID":"2048992","transactionName":"NFxSMRBUX0EDUkIPDA0WdhAMVkVbDV8ZBxEXUFMJB0YfRAtUQRVZIktEDAFZVHYHRVcPD01eVRE=","beacon":"bam.nr-data.net","queueTime":5,"applicationTime":693,"agent":""}</script>
        <title>Medical Usability: How to Kill Patients Through Bad Design</title><meta property="og:title" content="Medical Usability: How to Kill Patients Through Bad Design" />
  
        
        <meta name="description" content="A field study identified 22 ways that automated hospital systems can result in the wrong medication being dispensed to patients: mainly classic usability problems that have been known for decades.">
        <meta property="og:description" content="A field study identified 22 ways that automated hospital systems can result in the wrong medication being dispensed to patients: mainly classic usability problems that have been known for decades." />
        
  
        
	
        
        <meta name="keywords" content="domain-specific design, hospitals, field studies, university websites, default value, short term memory load, workflow">
        

        

        <meta name="robots" content="noarchive">
        <meta name="viewport" content="width=device-width">

        <link rel="search" type="application/opensearchdescription+xml" title="NN/g Search" href="../../opensearch.xml" />
        <link rel="shortcut icon" href="http://media.nngroup.com/static/img/favicon.ico" />

        <link rel="stylesheet" href="https://media.nngroup.com/static/vendor/font-awesome/css/font-awesome.min.css">

        
        <link rel="stylesheet" href="https://media.nngroup.com/static/css/7232a960f1df.css" type="text/css" />
        <link rel="stylesheet" href="http://fast.fonts.com/t/1.css?apiType=css&amp;projectid=c5846260-9def-46ec-9407-0f1349bd2d64" type="text/css">

        <script src="https://media.nngroup.com/static/vendor/modernizr/js/modernizr.min.js"></script>

        <script type="text/javascript" src="https://media.nngroup.com/static/js/3283be5def74.js"></script>
        

        

        
        
        <!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=229034,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('http://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->


<!-- Google Analytics Tracking Code for http://nngroup.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23132681-1', 'nngroup.com');
  ga('require', 'linkid', 'https://www.nngroup.com/articles/medical-usability/linkid.js');
  ga('require', 'displayfeatures');

// Optimizely Universal Analytics Integration
 window.optimizely = window.optimizely || [];
 window.optimizely.push("activateUniversalAnalytics");

  ga('send', 'pageview');

</script>
<script>
/**
* Function to track a click on an outbound link in Google Analytics.
*/
function trackOutboundLink(label, url) {
    if(window.ga && ga.loaded) {
        ga('send', 'event', 'outbound', 'click', label, {'hitCallback': function(){document.location = url;}
        });
    } else {
        document.location = url;
    }
}
/**
* Function to track a click on a link in the article announcement area.
*/
$('#articleAnnouncement a').on('click', function(){
  ga('send', 'event', 'link', 'click', 'articleAnnouncement');
});
</script>

<script type="text/javascript">
 setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2421.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



<!-- Hotjar Tracking Code for http://nngroup.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:121605,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

        <script src="https://media.nngroup.com/static/js/analytics/track_outbound_link.js"></script>
        
        
        
    </head>
    <body class="location-articles articles-detail">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
    <header id="page-header">
        <div id="cart" class="shopping-cart mobile-cart show-for-small-down"></div>
        <div class="row">
            <div id="masthead" class="masthead small-12 medium-7 column">

                <!-- The button to toggles menu on small screen -->

                <h1><a href="../../index.php"><img src="http://media.nngroup.com/static/img/logo-wordmark.png" width="74" height="30"> Nielsen <span class="norman">Norman</span> Group</a></h1>
                <p class="tagline show-for-medium-up">Evidence-Based User Experience Research, Training, and Consulting</p>

                <!-- The button to toggles form search on small screen -->


                <div style="clear: right;width:  100%;height: 1px;"></div><?php define('ROOT_PATH',$_SERVER["DOCUMENT_ROOT"] . '/apps/navigation_styles/navigations/5'); ?><?php include(ROOT_PATH)."/navigation.php"; ?>

                <!-- Drop down menu on small screen -->
                <div id="hideNav" class="show-for-small-down">
                  
                  <ul class="menu-content">
                    <li><a href="../../index.php">Home</a></li>
                    <li class="has-submenu">
                      <a>Training</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../training/index.php">Overview</a></li>
                        
<li id="nav-ux-conference"><a href="../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../online-seminars/index.php"><span>Online Seminars</span></a></li>

                      </ul>
                    </li>
                    <li class="has-submenu">
                      <a>Consulting</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
  <li><a href="../../consulting/index.php">Overview</a></li>
  
  <li><a href="../../consulting/benchmarking/index.php">Benchmarking</a></li>
  
  <li><a href="../../consulting/expert-design-review/index.php">Expert Design Review</a></li>
  
  <li><a href="../../consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
  
  <li><a href="../../consulting/usability-testing/index.php">Usability Testing </a></li>
  
  <li><a href="../../consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
  
  <li><a href="../../in-house-training/index.php">In-House Training</a></li>
</ul>

                    </li>

                    <li><a href="../../reports/index.php">Reports</a></li>
                    <li><a href="../index.php">Articles</a></li>
                    <li class="has-submenu"><a>About NN/G</a><i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../about/index.php">Overview</a></li>
                        <li><a href="../../people/index.php">People</a></li>
                        <li><a href="../../about/why-nng/index.php">Why NN/g?</a></li>
                        <li><a href="../../about/contact/index.php">Contact</a></li>
                        <li><a href="../../news/index.php">News</a></li>
                        <li><a href="../../about/history/index.php">History</a></li>
                        <li><a href="../../books/index.php">Books</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                </div>

                <hr class="header-divider show-for-small-only"/>


            </div>
       </div>

        <div id="hideNav">            <nav class="primary-nav small-12 column">
                <ul>
                    <li><a class="navitem" id="nav-home" href="../../index.php">Home</a></li>
                    <li><a class="navitem" id="nav-training" href="../../training/index.php">Training</a></li>
                    <li><a class="navitem" id="nav-consulting" href="../../consulting/index.php">Consulting</a></li>
                    <li><a class="navitem" id="nav-reports" href="../../reports/index.php">Reports</a></li>
                    <li><a class="navitem" id="nav-articles" href="../index.php">Articles</a></li>
                    <li><a class="navitem" id="nav-about" href="../../about/index.php">About NN/G</a></li>
                </ul>
            </nav>
            <nav class="secondary-nav small-12 medium-12 large-12 columns">
                
                
            </nav>
        </div>
    </header>
    <div class="l-content"><span id="preventClose" style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776; Menu</span>
    
  

  

  <div class="row">
    
      
      <section class="small-12 medium-3 columns collapsable-content l-navrail articles-left-column">
        <h1 class="show-for-small-down collapse-link mobile-browse-header">Browse by Topic and Author</h1>
          <div class="row collapsable">

            
            <div class="small-6 medium-12 columns">
              <section id="article-popular-topics" style="display: none;">
                
                <h1>Topics</h1>
                <ul id="article-popular-topics">
                    
                    <li>
                        
                        <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/intranets/all/index.php">Intranets</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/user-testing/all/index.php">User Testing</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                        
                    </li>
                    
                    <li><a href="#" id="article-see-all-topics"><strong>See all topics&hellip;</strong></a></li>
                </ul>
                
              </section>
              <section id="article-all-topics" style="display: none;">
                
                <h1>
                  <span class="show-for-medium-up">All Article Topics</span>
                  <span class="show-for-small-down">All Topics</span>
                  (<a href="#" id="article-hide-all-topics">hide</a>)
                </h1>
                <ul>
                  
                  <li>
                    
                    <a href="../../topic/accessibility/all/index.php">Accessibility</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/agile/all/index.php">Agile</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/analytics-and-metrics/all/index.php">Analytics &amp; Metrics</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/applications/all/index.php">Application Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/b2b-websites/all/index.php">B2B Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/behavior-patterns/all/index.php">Behavior Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/branding/all/index.php">Branding</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/content-strategy/all/index.php">Content Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/corporate-websites/all/index.php">Corporate Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/design-patterns/all/index.php">Design Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-design-process/all/index.php">Design Process</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/email/all/index.php">Email</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/eyetracking/all/index.php">Eyetracking</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/heuristic-evaluation/all/index.php">Heuristic Evaluation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/human-computer-interaction/all/index.php">Human Computer Interaction</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-ideation/all/index.php">Ideation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/information-architecture/all/index.php">Information Architecture</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/interaction-design/all/index.php">Interaction Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/international-users/all/index.php">International Users</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/intranets/all/index.php">Intranets</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-management/all/index.php">Management</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/navigation/all/index.php">Navigation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/non-profit-websites/all/index.php">Non-Profit Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/personas/all/index.php">Personas</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/persuasive-design/all/index.php">Persuasive Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/prototyping/all/index.php">Prototyping</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/psychology-and-ux/all/index.php">Psychology and UX</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/research-methods/all/index.php">Research Methods</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/search/all/index.php">Search</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/social-media/all/index.php">Social Media</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/strategy/all/index.php">Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/user-testing/all/index.php">User Testing</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-teams/all/index.php">UX Teams</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/visual-design/all/index.php">Visual Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/writing-web/all/index.php">Writing for the Web</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/young-users/all/index.php">Young Users</a>
                    
                  </li>
                  
                </ul>
                
              </section>
            </div>
            <script>
              /* Determine which topics list will be displayed: the full or the
               * partial.  We are inlining this close to the list itself to
               * prevent rendering delays on the client
               */
              var topic_list = $.cookie("article-topics");

              var popular_topics = $('#article-popular-topics');
              var all_topics = $('#article-all-topics');

              if(topic_list == 1) {
                  popular_topics.hide();
                  all_topics.show();
              } else {
                  popular_topics.show();
                  all_topics.hide();
              }
            </script>
            

            
            <div class="small-6 medium-12 columns">
              
              <section id="article-initial-authors">
                  <h1>Author</h1>
                  <ul>
                    
                    <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                    
                    <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                    
                    <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                    
                    <li><a href="#" onClick="$('#article-initial-authors').hide(); $('#article-all-authors').show();"><strong>See all authors&hellip;</strong></a></li>
                  </ul>
              </section>
              <section id="article-all-authors" style="display: none;">
                <h1>
                  All Authors
                  (<a href="#" id="article-hide-all-authors">hide</a>)
                </h1>
                <ul>
                  
                  <li><a href="../author/raluca-budiu/index.php">Raluca Budiu</a></li>
                  
                  <li><a href="../author/patty-caya/index.php">Patty Caya</a></li>
                  
                  <li><a href="../author/susan-farrell/index.php">Susan Farrell</a></li>
                  
                  <li><a href="../author/therese-fessenden/index.php">Therese Fessenden</a></li>
                  
                  <li><a href="../author/kim-flaherty/index.php">Kim Flaherty</a></li>
                  
                  <li><a href="../author/sarah-gibbons/index.php">Sarah Gibbons</a></li>
                  
                  <li><a href="../author/aurora-harley/index.php">Aurora Harley</a></li>
                  
                  <li><a href="../author/kate-kaplan/index.php">Kate Kaplan</a></li>
                  
                  <li><a href="../author/page-laubheimer/index.php">Page Laubheimer</a></li>
                  
                  <li><a href="../author/angela-li/index.php">Angie Li</a></li>
                  
                  <li><a href="../author/hoa-loranger/index.php">Hoa Loranger</a></li>
                  
                  <li><a href="../author/kate-meyer/index.php">Kate Meyer</a></li>
                  
                  <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                  
                  <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                  
                  <li><a href="../author/kara-pernice/index.php">Kara Pernice</a></li>
                  
                  <li><a href="../author/christian-rohrer/index.php">Christian Rohrer</a></li>
                  
                  <li><a href="../author/amy-schade/index.php">Amy Schade</a></li>
                  
                  <li><a href="../author/katie-sherwin/index.php">Katie Sherwin</a></li>
                  
                  <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                  
                  <li><a href="../author/kathryn-whitenton/index.php">Kathryn Whitenton</a></li>
                  
                </ul>
              </section>
              
            </div>
            
          </div>

          
          <section class="hide-for-small-down">
            
              
              <h1>Recent Articles</h1>
              
              <ul>
                  
                  <li><a href="../seamless-cross-channel/index.php">Seamlessness in the Omnichannel User Experience</a></li>
                  
                  <li><a href="../brand-intention-interpretation/index.php">Experience Design: Bridging Brand Intention and Brand Interpretation</a></li>
                  
                  <li><a href="../eyetracking-tasks-efficient-scanning/index.php">Scanning Patterns on the Web Are Optimized for the Current Task</a></li>
                  
                  <li><a href="../flat-design-best-practices/index.php">Flat-Design Best Practices</a></li>
                  
                  <li><a href="../ux-user-stories/index.php">Accounting for UX Work with User Stories in Agile Projects</a></li>
                  
                  <li><a href="../index.php"><strong>See all articles&hellip;</strong></a></li>
              </ul>
              
            
          </section>
          

          
          
          <section class="hide-for-small-down">
            <h1>Popular Articles</h1>
            
            <ul>
                
                <li><a href="../usability-101-introduction-to-usability/index.php">Usability 101: Introduction to Usability</a></li>
                
                <li><a href="../top-10-mistakes-web-design/index.php">Top 10 Mistakes in Web Design</a></li>
                
                <li><a href="../how-users-read-on-the-web/index.php">How Users Read on the Web</a></li>
                
                <li><a href="../f-shaped-pattern-reading-web-content/index.php">F-Shaped Pattern For Reading Web Content</a></li>
                
                <li><a href="../ten-usability-heuristics/index.php">10 Usability Heuristics for User Interface Design</a></li>
                
                <li><a href="../intranet-design-2016/index.php">10 Best Intranets of 2016</a></li>
                
                <li><a href="../which-ux-research-methods/index.php">When to Use Which User-Experience Research Methods</a></li>
                
                <li><a href="../response-times-3-important-limits/index.php">Response Times: The 3 Important Limits</a></li>
                
                <li><a href="../why-you-only-need-to-test-with-5-users/index.php">Why You Only Need to Test with 5 Users</a></li>
                
                <li><a href="../page-fold-manifesto/index.php">The Fold Manifesto: Why the Page Fold Still Matters</a></li>
                
            </ul>
          </section>
          
          

          
            <section class="hide-for-small-down">
              
            </section>
          

      </section>
      

      
        <div class="small-12 medium-9 columns content-body">
          




<article class="l-subsection alertbox-article-content article-detail-bl">
  
  <header>
    <div class="l-subsection">
      <h1>Medical Usability: How to Kill Patients Through Bad Design</h1>
      <div class="author-meta">by
        
          
            
            <a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a>
            
          
        on  April 11, 2005
        
      </div>

      

      
      <div class="topic-links">
        Topics:
        
          <ul>

  <li><a href="../../topic/applications/index.php">Application Design</a></li>

</ul>
        
      </div>
      
    </div>
  </header>
  

  
  
  <section>
    <p class="lede"><strong>Summary:</strong> A field study identified 22 ways that automated hospital systems can result in the wrong medication being dispensed to patients. Most of these flaws are classic usability problems that have been understood for decades.</p>
    <br />
  </section>
  
  

  
  <section id="article-body"><p>
	Usability is often a matter of life or death. In a fighter plane's user interface, for example, taking a second off the time required to operate targeting-and-firing systems offers pilots a dramatic edge in dog-fights.</p>
<p>
	The most striking example of how bad design can kill comes from <a class="old" href="../why-consumer-products-have-bad-ux/index.php" title="Alertbox: Why Consumer Products Have Inferior User Experience"> in-car user interfaces</a>: thousands of deaths per year are related to drivers being distracted by overly complex designs. Conversely, good automotive design can save lives. As an example, take my new Lexus LS430's slightly nagging navigation system, which tells you far in advance whether the freeway exit you need will be to the left or the right. This feature gives you plenty of time to change lanes, rather than having to wait until the last moment, which is when you typically spot the road sign. (The number of people killed due to poor sign usability must be astounding.)</p>
<p>
	Medical systems have also provided many well-documented killer designs, such as the radiation machines that fried six patients because of complex and misleading operator consoles. What's less known is that usability problems in the medical sector's good old-fashioned office automation systems can harm patients just as seriously as machines used for treatment.</p>
<h2>
	Field Study in a Hospital</h2>
<p>
	In a recent <a class="out" href="http://jama.ama-assn.org/cgi/content/abstract/293/10/1197" title="JAMA, March 2005: Role of Computerized Physician Order Entry Systems in Facilitating Medication Errors"> <cite> Journal of the American Medical Association </cite> paper</a>, Ross Koppel and colleagues reported on a field study of a <strong> hospital's order-entry system</strong>, which physicians use to specify patient medications. The study identified twenty-two ways in which the system caused patients to get the wrong medicine. Most of these issues are usability problems. I'll briefly discuss the ones of general interest here.</p>
<p>
	<strong>Misleading Default Values. </strong> The system screens listed dosages based on the medication units available through the hospital's pharmacy. When hospital staff members prescribed infrequently used medications, they often relied on the listed unit as being a typical dose, even though that's not the true meaning of the numbers. If a medication is usually prescribed in 20 or 30 mg doses, for example, the pharmacy might stock 10 mg pills so it can cover both dosage needs and avoid overstocking a rare medication. In this case, users might prescribe 10 mg, even though 20 or 30 would be more appropriate. The solution here is simple: Each screen should list the typical prescription as a guidance. Years of usability studies in many domains have shown that users tend to assume that the given default or example values are applicable to their own situations.</p>
<p>
	<strong>New Commands Not Checked Against Previous Ones. </strong> When doctors changed the dosage of a patient's medication, they often entered the new dose without canceling the old one. As a result, the patients received the sum of the old and new doses. This common type of user error is equivalent to a banking interface error, where you specify payment of the same amount to the same recipient twice in one day. Many bank websites will catch these errors and ask you to double-check so you don't pay the same bill twice. In general, if users are doing something they've already done, the system should ask whether both operations should remain in effect or whether the new command should overrule the old one.</p>
<p>
	<strong>Poor Readability. </strong> Because patient names appeared in a small font that was difficult to read, it was easy for users to select the wrong patient. The problem was compounded by the fact that names were listed alphabetically rather than grouped by hospital areas, which meant that users looking for a specific patient saw many similar names. Also, in individual patient records, the patient's name didn't appear on all screens, reducing the probability that users would discover the error before reaching a critical point in the interaction.</p>
<p>
	<strong>Memory Overload. </strong> At times, users had to review up to twenty screens to see all of a patient's medications. The well-known limits on human short-term memory make it impossible to remember everything across that many screens. In a survey, 72% of staff reported that they were often uncertain about medications and dosages because of the difficulties in reviewing a patient's total medications. Humans are notoriously poor at remembering exact information, and minimizing users' memory load has long been one of computing's <a class="old" href="../ten-usability-heuristics/index.php" title="Jakob Nielsen: Ten Usability Heuristics From 1994"> top-ten usability heuristics</a>. Facts should be restated when and where they're needed rather than requiring users to remember things from one screen to the next (let alone twenty screens down the road).</p>
<p>
	<strong>Date Description Errors. </strong> The interface let users specify medications for "tomorrow." When surgeries were late in the day and users entered such orders after midnight, patients would miss a day's medication.</p>
<p>
	<strong>Overly Complicated Workflow. </strong> Many aspects of the system required users to go through numerous screens that conflicted with hospital workflow. As a result, the system wasn't always used as intended. Nurses, for example, kept a separate set of paper records that they entered into the system at the end of the shift. This both increased the risk of errors and prevented the system from reflecting real-time information about the medications each patient had received. In general, whenever you see users resorting to sticky notes or other paper-based workarounds, you know you have a failed UI.</p>
<h2>
	Methodology Weaknesses</h2>
<p>
	To supplement their field observation of actual user behavior, the researchers administered a survey that asked hospital staff how often various errors had occurred during the previous three months. Unfortunately, the <strong> paper relies overly much on this self-reported data </strong> in estimating the impact of the usability problems. It's well known that people have a hard time remembering what they do with computers. Valid data comes from <a class="old" href="../first-rule-of-usability-dont-listen-to-users/index.php" title="Alertbox: First Rule of Usability? Don't Listen to Users"> what people do, not what they say</a>.</p>
<p>
	When it comes to user errors caused by bad design, there's a further problem as well: If the interface fails to provide adequate feedback, users might not even realize that they've committed an error. With medication errors in particular, it's also quite possible that hospital staff might tend to minimize the extent to which patients get the wrong medication -- even when a survey guarantees anonymity.</p>
<p>
	I would have much preferred error-frequency estimates based on actual observations, rather than fallible human memory and possibly biased survey answers. Still, the survey indicated that many of the errors reportedly occurred at least weekly. If anything, the true error rate is probably higher than the self-reported estimates in the survey.</p>
<p>
	It's great to see usability branching out beyond its origins and being researched in a clinical epidemiology department. It's less great to observe methodological weaknesses that stem from studying usability issues without the benefit of the last twenty-five years' experience with usability research. Of the paper's sixty references, 92% are from medical journals and the like. Only five of the sixty references are from the human factors literature. And, despite the fact that the study related to software design, none of the five references are from leading journals, conferences, books, or thinkers in human-computer interaction.</p>
<p>
	Hospital systems offer just one example of the usability problems that proliferate in domain-specific systems. Such systems rarely get as much public exposure and analysis as websites do. Vendors often think that having domain experts on staff means that their software will work in the field. But the way people are supposed to work in theory never matches reality. The more specialized the system, the more you need user research to ensure success. From physicians to firefighters, if you don't observe real users and test your designs with them, you are guaranteed a plethora of usability problems.</p>
<h2>
	Locating the Paper: More Usability Trials</h2>
<p>
	I'm not a regular reader of the <cite> Journal of the American Medical Association</cite>; I discovered the study through an <a class="out" href="http://www.nytimes.com/2005/03/09/technology/09compute.php?ex=1268110800&amp;en=402b792d748c99a2&amp;ei=5088&amp;partner=rssnyt" title="New York Times March 9, 2005: 'Doctors' Journal Says Computing Is No Panacea'"> article in <cite> The New York Times</cite></a>. Unfortunately, getting from the newspaper to the paper it referenced was a trying ordeal.</p>
<p>
	I never cease to be surprised at the <strong> miserable usability of university websites</strong>. The Web was invented to disseminate academic papers, but it's almost impossible to find research results on academic websites.</p>
<p>
	In this case, I didn't know the paper's title, as it wasn't reported in the newspaper. I did have the lead author's name, so I searched for it and was promptly led to a faculty homepage at the University of Pennsylvania. Unfortunately, this page was useless, as are most faculty member homepages. The most recent entry on the "selected publications" list was from 2002. The professor's main research interest was presented in <a class="old" href="../marginalia-of-web-design/index.php" title="Alertbox: Marginalia of Web Design"> colored text</a>, offering a strong perceived affordance of clickability. Nevertheless, it offered no link. The biography page offered no further information about the professor's research either. It did link to his full curriculum vitae (in PDF, oh woe), but it hadn't been updated since March 2003 and also had no links.</p>
<p>
	Looking for the author failed to produce any information about the research. What about the academic institution responsible for the project? The newspaper handily provided the department's full name, making for an easy search. The top search result was the correct one, but the page title -- CCEB -- had almost no <a class="old" href="../wrong-information-scent-costs-sales/index.php" title="Alertbox: Deceivingly Strong Information Scent Costs Sales"> information scent</a>. Further probing revealed that CCEB stands for "Center for Clinical Epidemiology and Biostatistics." With an entire line available to spell out their names, you'd think organizations would want to help poor outside users by doing so.</p>
<p>
	But this was far from the worst problem. Sadly, the university has almost no idea of how to <a class="old" href="../../reports/pr-websites/index.php" title="Nielsen Norman Group: Designing Websites to Maximize Press Relations: 75 Guidelines from Usability Studies with Journalists"> use the Web for PR</a>. On a day when the "CCEB" was featured on the front page of <cite> The New York Times </cite> ' business section, the department's latest <em> News </em> page entry was ten months old.</p>
<p>
	Where the University of Pennsylvania failed miserably, the American Medical Association performed wonderfully: a search for <cite> Journal of the American Medical Association </cite> retrieved the journal's website as the first hit. The JAMA homepage offered a direct link to the article I wanted, in keeping with the <a class="old" href="../113-design-guidelines-homepage-usability/index.php" title="Alertbox: Top Ten Guidelines for Homepage Usability"> homepage guideline</a> to feature high-priority content.</p>
<p>
	JAMA's main navbar also had a prominent link to <em> Past issues </em> (unfortunately presented in low-contrast colors and ALL CAPS text). This link led to an archive that included the current issue. This is quite helpful for users -- like myself -- who don't realize their target content is actually in the current issue.</p>
<p>
	All that said, JAMA's website had plenty of usability problems, including a proliferation of undifferentiated <em> More </em> links that simultaneously hurt the homepage's <a class="old" href="../how-users-read-on-the-web/index.php" title="Alertbox: How Users Read on the Web"> scannability </a> for sighted users, reduced <a class="old" href="../../topic/accessibility/all/index.php" title="Nielsen Norman Group: 75 Best Practices to Improve Usability for Users With Disabilities, Based on Studies With People Who Use Assistive Technology"> accessibility</a> for blind users, and prevented search engines from associating destination pages with meaningful keywords from the anchor text.</p>
<p>
	Mainly, though, JAMA did its job on the Web. Once I changed my strategy and searched for the paper's publisher, rather than the author and his academic institution, it took me about a minute to go from a major search engine to retrieving the paper's full text on the JAMA site.</p>
<p>
	The fact that academic websites are so miserable to use is surely a contributing factor to the isolating and narrowing effect of current research practices. If outsiders could more easily connect with research results in other disciplines -- where they don't know the scientists personally -- we might see more cross-fertilization and growth in our shared knowledge base. Indeed, a unified, worldwide hypertext system was the Web's founding motivation.</p>
<h3>
	Reference</h3>
<p>
	Koppel, R., Metlay. J.P., Cohen, A., Abaluck, B., Localio, A.R., Kimmel, S.E., and Strom B.L. (2005): " <a class="out" href="http://jama.ama-assn.org/cgi/content/full/293/10/1197" title="Journal of the American Medical Association: full text of paper"> Role of Computerized Physician Order Entry Systems in Facilitating Medication Errors</a>," <cite> Journal of the American Medical Association </cite> Vol. <strong> 293 </strong> No. 10 (March 9), 1197-1203.</p>
</section>
  

  
  <section>
    
        
    
  </section>
  
  
  
  
  

  


  <section>
    <br />
    <p class="share"><b>Share <span class="hide-for-small-down">this article</span>:</b> <a href="https://twitter.com/intent/tweet?url=http://www.nngroup.com/articles/medical-usability/&amp;text=Medical%20Usability:%20How%20to%20Kill%20Patients%20Through%20Bad%20Design&amp;via=nngroup">Twitter</a> | <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=http://www.nngroup.com/articles/medical-usability/&amp;title=Medical%20Usability:%20How%20to%20Kill%20Patients%20Through%20Bad%20Design&amp;source=Nielsen%20Norman%20Group">LinkedIn</a> | <a href="https://plus.google.com/share?url=http://www.nngroup.com/articles/medical-usability/">Google+</a> | <a href="mailto:?subject=NN/g Article: Medical Usability: How to Kill Patients Through Bad Design&amp;body=http://www.nngroup.com/articles/medical-usability/">Email</a></p>
    <hr />
  </section>

  <footer class="l-subsection">
    
    
    <div class="learn-more">
      <h2>Learn More</h2>
      <p><a href="../subscribe/index.php">Subscribe to the weekly newsletter</a> to get notified about future articles.</p>
      </div>
    
    

    <div class="row show-for-small-down related-content">
      
      
      

      
      
        <div class="column small-12 topic-links">All articles about:
          <ul>
            
            <li><a href="../../topic/applications/index.php">Application Design</a></li>
            
          </ul>
        </div>
      
      

      
      
        <div class="column small-12">
          <h3>UX Conference Training Courses</h3>
           <ul class="no-bullet">
    
        <li><a href="../../courses/application-ux/index.php">Application Design for Web and Desktop</a></li>
    
        <li><a href="../../courses/emotional-design/index.php">Emotional Design</a></li>
    
</ul>
        </div>
      
      

      
      
        <div class="small-12 column">
          <h3>Research Reports</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../reports/best-applications-2/index.php">Application Design Showcase: 2012</a></li>
              
            
              
                <li><a href="../../reports/enterprise-mobile-showcase/index.php">Mobile Intranets and Enterprise Apps</a></li>
              
            
              
                <li><a href="../../reports/website-tools-and-applications-flash/index.php">Website Tools and Applications with Flash</a></li>
              
            
              
                <li><a href="../../reports/customization-features/index.php">Customization Features Done Correctly for the Right Reasons</a></li>
              
            
              
                <li><a href="../../reports/best-applications-1/index.php">Application Design Showcase: 2008</a></li>
              
            
          </ul>
        </div>
      
      

      
      
        <div class="small-12 column">
          <h3>Online Seminars</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../online-seminars/ux-big-data/index.php">UX for Big Data</a></li>
              
            
          </ul>
        </div>
      
      
    </div>

    <div class="hide-for-small-down row related-content">
      <div class="medium-6 columns">
        
        
          <div>
            <h3>Articles</h3>
            <ul class="no-bullet">
              
                
                <li><a href="../tabs-used-right/index.php">Tabs, Used Right</a></li>
                
              
                
                <li><a href="../indicators-validations-notifications/index.php">Indicators, Validations, and Notifications: Pick the Correct Communication Option</a></li>
                
              
                
                <li><a href="../passwords-memory/index.php">Help People Create Passwords That They Can Actually Remember</a></li>
                
              
                
                <li><a href="../progress-indicators/index.php">Progress Indicators Make a Slow System Less Insufferable</a></li>
                
              
                
                <li><a href="../radio-buttons-default-selection/index.php">Radio Buttons: Select One by Default or Leave All Unselected?</a></li>
                
              
            </ul>
          </div>
        
        

        
          
          <div>
            <h3>Research Reports</h3>
            <ul class="no-bullet">
              
                
                  <li><a href="../../reports/best-applications-2/index.php">Application Design Showcase: 2012</a></li>
                
              
                
                  <li><a href="../../reports/enterprise-mobile-showcase/index.php">Mobile Intranets and Enterprise Apps</a></li>
                
              
                
                  <li><a href="../../reports/website-tools-and-applications-flash/index.php">Website Tools and Applications with Flash</a></li>
                
              
                
                  <li><a href="../../reports/customization-features/index.php">Customization Features Done Correctly for the Right Reasons</a></li>
                
              
                
                  <li><a href="../../reports/best-applications-1/index.php">Application Design Showcase: 2008</a></li>
                
              
            </ul>
          </div>
          
        

      </div>
      <div class="medium-6 columns">
        
          
          <div>
            <h3>UX Conference Training Courses</h3>
             <ul class="no-bullet">
    
        <li><a href="../../courses/application-ux/index.php">Application Design for Web and Desktop</a></li>
    
        <li><a href="../../courses/emotional-design/index.php">Emotional Design</a></li>
    
</ul>
          </div>
          
        
        
          
          <div>
            <h3>Online Seminars</h3>
            <ul class="no-bullet">
            
                <li><a href="../../online-seminars/ux-big-data/index.php">UX for Big Data</a></li>
            
            </ul>
          </div>
          
        
      </div>
    </div>
  </footer>
</article>

        </div>

      

    
  </div>
  <div class="row">
    
      <div class="column small-12 show-for-small-down l-navrail announcements">
        
          
        
      </div>
    
    
  </div>

    </div>
    




<footer id="page-footer">
    <div class="row hide-for-print">
        <div class="column small-12 medium-6">
            <section class="footer-popular">
                
                <h1>Popular Topics</h1>
                <div style="float: left;  width: 15em; display: block;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/applications/index.php">Application Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/e-commerce/index.php">E-Commerce Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/eyetracking/index.php">Eyetracking </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/information-architecture/index.php">Information Architecture </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/intranets/index.php">Intranet Design </a></li>
	<li><a href="../../topic/interaction-design/index.php">Interaction Design </a></li>
</ul>
</div>

<div style="float: left;  width: 15em; display: block; list-style: none;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/mobile-and-tablet-design/index.php">Mobile and Tablet Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/credibility-persuasion/index.php">Persuasive Design</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/research-methods/index.php">Research Methods</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/user-testing/index.php">User Testing </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/web-usability/index.php">Web Usability </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/writing-web/index.php">Web Writing &amp; Content Strategy </a></li>
</ul>
</div>

<p>&nbsp;</p>

                
            </section>
        </div>

        <div class="column small-12 medium-6">
            <div class="row">
                <div class="column small-6">
                    <section class="footer-about">
                        <h1>Follow Us</h1>
                        <ul>
	<li><a href="../subscribe/index.php">Subscribe to our weekly UX newsletter</a>,&nbsp;Jakob Nielsen&#39;s Alertbox</li>
	<li>Visit Don Norman&#39;s website, <a href="http://www.jnd.org/"> www.jnd.org </a></li>
	<li>Visit Bruce Tognazzini&#39;s website, <a href="http://www.asktog.com/"> www.asktog.com </a></li>
	<li style="padding-top: 6px;"><a class="twitter-follow-button" data-show-count="false" data-size="large" href="https://twitter.com/nngroup"><img alt="Follow @nngroup on Twitter" height="20" src="https://media.nngroup.com/media/editor/2016/12/30/twitter-follow-button.png" style="width: 117px; height: 20px;" width="117" /></a></li>
	<li style="padding-top: 6px;"><img alt="" height="14" src="https://media.nngroup.com/media/editor/2016/07/26/feed-icon-14x14.png" style="vertical-align:text-top;width:14px;margin-right:.25em;" width="14" /><a href="../../feed/rss/index.php">Subscribe to our RSS feed</a></li>
</ul>

                    </section>
                </div>

                <div class="column small-6">
                    <section class="footer-about right-column">
                        <h1>About
</h1>
                        <ul class="no-bullet">
	<li><a href="../../about/contact/index.php">Contact information</a></li>
	<li><a href="../../about/index.php">About Us</a></li>
	<li><a href="../../about/why-nng/index.php">Why NN/g? </a></li>
	<li><a href="../../return-policy/index.php">Return Policy</a></li>
</ul>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="copyright column small-12">
            <p><a href="../../copyright-and-reprint-info/index.php">Copyright</a> &copy; 1998-2017 Nielsen Norman Group, All Rights Reserved.</p>
        </div>
    </div>
</footer>


    


        <script src="https://media.nngroup.com/static/vendor/fastclick/js/fastclick.min.js"></script>
        <script src="https://media.nngroup.com/static/vendor/foundation/js/foundation.min.js"></script>
        <script src="http://media.nngroup.com/static/js/app.js"></script>
        <script src="http://media.nngroup.com/static/js/scroll-to-fixed.js"></script><script>var externalLinks = document.querySelectorAll('a[href^="http"]');for (var i = externalLinks.length-1; i >= 0; i--) {externalLinks[i].addEventListener("click", function(event) { alert("External links are disabled for this test."); event.preventDefault(); }, false);}var mailLinks = document.querySelectorAll('a[href^="mailto"]');for (var i = mailLinks.length-1; i >= 0; i--) {mailLinks[i].addEventListener("click", function(event) { alert("Email links are disabled for this test."); event.preventDefault(); }, false);}var externalForms = document.querySelectorAll('form');for (var i = externalForms.length-1; i >= 0; i--) {externalForms[i].addEventListener("submit", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false); externalForms[i].addEventListener("click", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false);}</script>

        <script>
            
            $(function() {
                $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
                $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
            });
        </script>

        
<script src="https://media.nngroup.com/static/js/articles.js"></script>


        
        <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1072699960;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072699960/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
    </body>

<!-- Mirrored from www.nngroup.com/articles/medical-usability/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:14:20 GMT -->
</html>
