<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->





    
<!-- Mirrored from www.nngroup.com/articles/coping-information-overload/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:13:20 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1026.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"beacon":"bam.nr-data.net","queueTime":0,"applicationTime":934,"agent":"","licenseKey":"1a30ef9931","errorBeacon":"bam.nr-data.net","applicationID":"2048992","transactionName":"NFxSMRBUX0EDUkIPDA0WdhAMVkVbDV8ZBxEXUFMJB0YfRAtUQRVZIktEDAFZVHYHRVcPD01eVRE="}</script>
        <title>Coping with Information Overload: Article by Jakob Nielsen</title><meta property="og:title" content="Coping with Information Overload: Article by Jakob Nielsen" />
  
        
        <meta name="description" content="Survey of methods used in various research systems for reducing information overload.">
        <meta property="og:description" content="Survey of methods used in various research systems for reducing information overload." />
        
  
        
	
        

        

        <meta name="robots" content="noarchive">
        <meta name="viewport" content="width=device-width">

        <link rel="search" type="application/opensearchdescription+xml" title="NN/g Search" href="../../opensearch.xml" />
        <link rel="shortcut icon" href="http://media.nngroup.com/static/img/favicon.ico" />

        <link rel="stylesheet" href="https://media.nngroup.com/static/vendor/font-awesome/css/font-awesome.min.css">

        
        <link rel="stylesheet" href="https://media.nngroup.com/static/css/7232a960f1df.css" type="text/css" />
        <link rel="stylesheet" href="http://fast.fonts.com/t/1.css?apiType=css&amp;projectid=c5846260-9def-46ec-9407-0f1349bd2d64" type="text/css">

        <script src="https://media.nngroup.com/static/vendor/modernizr/js/modernizr.min.js"></script>

        <script type="text/javascript" src="https://media.nngroup.com/static/js/3283be5def74.js"></script>
        

        

        
        
        <!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=229034,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('http://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->


<!-- Google Analytics Tracking Code for http://nngroup.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23132681-1', 'nngroup.com');
  ga('require', 'linkid', 'https://www.nngroup.com/articles/coping-information-overload/linkid.js');
  ga('require', 'displayfeatures');

// Optimizely Universal Analytics Integration
 window.optimizely = window.optimizely || [];
 window.optimizely.push("activateUniversalAnalytics");

  ga('send', 'pageview');

</script>
<script>
/**
* Function to track a click on an outbound link in Google Analytics.
*/
function trackOutboundLink(label, url) {
    if(window.ga && ga.loaded) {
        ga('send', 'event', 'outbound', 'click', label, {'hitCallback': function(){document.location = url;}
        });
    } else {
        document.location = url;
    }
}
/**
* Function to track a click on a link in the article announcement area.
*/
$('#articleAnnouncement a').on('click', function(){
  ga('send', 'event', 'link', 'click', 'articleAnnouncement');
});
</script>

<script type="text/javascript">
 setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2421.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



<!-- Hotjar Tracking Code for http://nngroup.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:121605,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

        <script src="https://media.nngroup.com/static/js/analytics/track_outbound_link.js"></script>
        
        
        
    </head>
    <body class="location-articles articles-detail">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
    <header id="page-header">
        <div id="cart" class="shopping-cart mobile-cart show-for-small-down"></div>
        <div class="row">
            <div id="masthead" class="masthead small-12 medium-7 column">

                <!-- The button to toggles menu on small screen -->

                <h1><a href="../../index.php"><img src="http://media.nngroup.com/static/img/logo-wordmark.png" width="74" height="30"> Nielsen <span class="norman">Norman</span> Group</a></h1>
                <p class="tagline show-for-medium-up">Evidence-Based User Experience Research, Training, and Consulting</p>

                <!-- The button to toggles form search on small screen -->


                <div style="clear: right;width:  100%;height: 1px;"></div><?php define('ROOT_PATH',$_SERVER["DOCUMENT_ROOT"] . '/apps/navigation_styles/navigations/5'); ?><?php include(ROOT_PATH)."/navigation.php"; ?>

                <!-- Drop down menu on small screen -->
                <div id="hideNav" class="show-for-small-down">
                  
                  <ul class="menu-content">
                    <li><a href="../../index.php">Home</a></li>
                    <li class="has-submenu">
                      <a>Training</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../training/index.php">Overview</a></li>
                        
<li id="nav-ux-conference"><a href="../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../online-seminars/index.php"><span>Online Seminars</span></a></li>

                      </ul>
                    </li>
                    <li class="has-submenu">
                      <a>Consulting</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
  <li><a href="../../consulting/index.php">Overview</a></li>
  
  <li><a href="../../consulting/benchmarking/index.php">Benchmarking</a></li>
  
  <li><a href="../../consulting/expert-design-review/index.php">Expert Design Review</a></li>
  
  <li><a href="../../consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
  
  <li><a href="../../consulting/usability-testing/index.php">Usability Testing </a></li>
  
  <li><a href="../../consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
  
  <li><a href="../../in-house-training/index.php">In-House Training</a></li>
</ul>

                    </li>

                    <li><a href="../../reports/index.php">Reports</a></li>
                    <li><a href="../index.php">Articles</a></li>
                    <li class="has-submenu"><a>About NN/G</a><i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../about/index.php">Overview</a></li>
                        <li><a href="../../people/index.php">People</a></li>
                        <li><a href="../../about/why-nng/index.php">Why NN/g?</a></li>
                        <li><a href="../../about/contact/index.php">Contact</a></li>
                        <li><a href="../../news/index.php">News</a></li>
                        <li><a href="../../about/history/index.php">History</a></li>
                        <li><a href="../../books/index.php">Books</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                </div>

                <hr class="header-divider show-for-small-only"/>


            </div>
       </div>

        <div id="hideNav">            <nav class="primary-nav small-12 column">
                <ul>
                    <li><a class="navitem" id="nav-home" href="../../index.php">Home</a></li>
                    <li><a class="navitem" id="nav-training" href="../../training/index.php">Training</a></li>
                    <li><a class="navitem" id="nav-consulting" href="../../consulting/index.php">Consulting</a></li>
                    <li><a class="navitem" id="nav-reports" href="../../reports/index.php">Reports</a></li>
                    <li><a class="navitem" id="nav-articles" href="../index.php">Articles</a></li>
                    <li><a class="navitem" id="nav-about" href="../../about/index.php">About NN/G</a></li>
                </ul>
            </nav>
            <nav class="secondary-nav small-12 medium-12 large-12 columns">
                
                
            </nav>
        </div>
    </header>
    <div class="l-content"><span id="preventClose" style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776; Menu</span>
    
  

  

  <div class="row">
    
      
      <section class="small-12 medium-3 columns collapsable-content l-navrail articles-left-column">
        <h1 class="show-for-small-down collapse-link mobile-browse-header">Browse by Topic and Author</h1>
          <div class="row collapsable">

            
            <div class="small-6 medium-12 columns">
              <section id="article-popular-topics" style="display: none;">
                
                <h1>Topics</h1>
                <ul id="article-popular-topics">
                    
                    <li>
                        
                        <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/intranets/all/index.php">Intranets</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/user-testing/all/index.php">User Testing</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                        
                    </li>
                    
                    <li><a href="#" id="article-see-all-topics"><strong>See all topics&hellip;</strong></a></li>
                </ul>
                
              </section>
              <section id="article-all-topics" style="display: none;">
                
                <h1>
                  <span class="show-for-medium-up">All Article Topics</span>
                  <span class="show-for-small-down">All Topics</span>
                  (<a href="#" id="article-hide-all-topics">hide</a>)
                </h1>
                <ul>
                  
                  <li>
                    
                    <a href="../../topic/accessibility/all/index.php">Accessibility</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/agile/all/index.php">Agile</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/analytics-and-metrics/all/index.php">Analytics &amp; Metrics</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/applications/all/index.php">Application Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/b2b-websites/all/index.php">B2B Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/behavior-patterns/all/index.php">Behavior Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/branding/all/index.php">Branding</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/content-strategy/all/index.php">Content Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/corporate-websites/all/index.php">Corporate Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/design-patterns/all/index.php">Design Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-design-process/all/index.php">Design Process</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/email/all/index.php">Email</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/eyetracking/all/index.php">Eyetracking</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/heuristic-evaluation/all/index.php">Heuristic Evaluation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/human-computer-interaction/all/index.php">Human Computer Interaction</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-ideation/all/index.php">Ideation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/information-architecture/all/index.php">Information Architecture</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/interaction-design/all/index.php">Interaction Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/international-users/all/index.php">International Users</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/intranets/all/index.php">Intranets</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-management/all/index.php">Management</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/navigation/all/index.php">Navigation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/non-profit-websites/all/index.php">Non-Profit Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/personas/all/index.php">Personas</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/persuasive-design/all/index.php">Persuasive Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/prototyping/all/index.php">Prototyping</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/psychology-and-ux/all/index.php">Psychology and UX</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/research-methods/all/index.php">Research Methods</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/search/all/index.php">Search</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/social-media/all/index.php">Social Media</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/strategy/all/index.php">Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/user-testing/all/index.php">User Testing</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-teams/all/index.php">UX Teams</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/visual-design/all/index.php">Visual Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/writing-web/all/index.php">Writing for the Web</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/young-users/all/index.php">Young Users</a>
                    
                  </li>
                  
                </ul>
                
              </section>
            </div>
            <script>
              /* Determine which topics list will be displayed: the full or the
               * partial.  We are inlining this close to the list itself to
               * prevent rendering delays on the client
               */
              var topic_list = $.cookie("article-topics");

              var popular_topics = $('#article-popular-topics');
              var all_topics = $('#article-all-topics');

              if(topic_list == 1) {
                  popular_topics.hide();
                  all_topics.show();
              } else {
                  popular_topics.show();
                  all_topics.hide();
              }
            </script>
            

            
            <div class="small-6 medium-12 columns">
              
              <section id="article-initial-authors">
                  <h1>Author</h1>
                  <ul>
                    
                    <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                    
                    <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                    
                    <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                    
                    <li><a href="#" onClick="$('#article-initial-authors').hide(); $('#article-all-authors').show();"><strong>See all authors&hellip;</strong></a></li>
                  </ul>
              </section>
              <section id="article-all-authors" style="display: none;">
                <h1>
                  All Authors
                  (<a href="#" id="article-hide-all-authors">hide</a>)
                </h1>
                <ul>
                  
                  <li><a href="../author/raluca-budiu/index.php">Raluca Budiu</a></li>
                  
                  <li><a href="../author/patty-caya/index.php">Patty Caya</a></li>
                  
                  <li><a href="../author/susan-farrell/index.php">Susan Farrell</a></li>
                  
                  <li><a href="../author/therese-fessenden/index.php">Therese Fessenden</a></li>
                  
                  <li><a href="../author/kim-flaherty/index.php">Kim Flaherty</a></li>
                  
                  <li><a href="../author/sarah-gibbons/index.php">Sarah Gibbons</a></li>
                  
                  <li><a href="../author/aurora-harley/index.php">Aurora Harley</a></li>
                  
                  <li><a href="../author/kate-kaplan/index.php">Kate Kaplan</a></li>
                  
                  <li><a href="../author/page-laubheimer/index.php">Page Laubheimer</a></li>
                  
                  <li><a href="../author/angela-li/index.php">Angie Li</a></li>
                  
                  <li><a href="../author/hoa-loranger/index.php">Hoa Loranger</a></li>
                  
                  <li><a href="../author/kate-meyer/index.php">Kate Meyer</a></li>
                  
                  <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                  
                  <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                  
                  <li><a href="../author/kara-pernice/index.php">Kara Pernice</a></li>
                  
                  <li><a href="../author/christian-rohrer/index.php">Christian Rohrer</a></li>
                  
                  <li><a href="../author/amy-schade/index.php">Amy Schade</a></li>
                  
                  <li><a href="../author/katie-sherwin/index.php">Katie Sherwin</a></li>
                  
                  <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                  
                  <li><a href="../author/kathryn-whitenton/index.php">Kathryn Whitenton</a></li>
                  
                </ul>
              </section>
              
            </div>
            
          </div>

          
          <section class="hide-for-small-down">
            
              
              <h1>Recent Articles</h1>
              
              <ul>
                  
                  <li><a href="../seamless-cross-channel/index.php">Seamlessness in the Omnichannel User Experience</a></li>
                  
                  <li><a href="../brand-intention-interpretation/index.php">Experience Design: Bridging Brand Intention and Brand Interpretation</a></li>
                  
                  <li><a href="../eyetracking-tasks-efficient-scanning/index.php">Scanning Patterns on the Web Are Optimized for the Current Task</a></li>
                  
                  <li><a href="../flat-design-best-practices/index.php">Flat-Design Best Practices</a></li>
                  
                  <li><a href="../ux-user-stories/index.php">Accounting for UX Work with User Stories in Agile Projects</a></li>
                  
                  <li><a href="../index.php"><strong>See all articles&hellip;</strong></a></li>
              </ul>
              
            
          </section>
          

          
          
          <section class="hide-for-small-down">
            <h1>Popular Articles</h1>
            
            <ul>
                
                <li><a href="../usability-101-introduction-to-usability/index.php">Usability 101: Introduction to Usability</a></li>
                
                <li><a href="../top-10-mistakes-web-design/index.php">Top 10 Mistakes in Web Design</a></li>
                
                <li><a href="../how-users-read-on-the-web/index.php">How Users Read on the Web</a></li>
                
                <li><a href="../f-shaped-pattern-reading-web-content/index.php">F-Shaped Pattern For Reading Web Content</a></li>
                
                <li><a href="../ten-usability-heuristics/index.php">10 Usability Heuristics for User Interface Design</a></li>
                
                <li><a href="../intranet-design-2016/index.php">10 Best Intranets of 2016</a></li>
                
                <li><a href="../which-ux-research-methods/index.php">When to Use Which User-Experience Research Methods</a></li>
                
                <li><a href="../response-times-3-important-limits/index.php">Response Times: The 3 Important Limits</a></li>
                
                <li><a href="../why-you-only-need-to-test-with-5-users/index.php">Why You Only Need to Test with 5 Users</a></li>
                
                <li><a href="../page-fold-manifesto/index.php">The Fold Manifesto: Why the Page Fold Still Matters</a></li>
                
            </ul>
          </section>
          
          

          
            <section class="hide-for-small-down">
              
            </section>
          

      </section>
      

      
        <div class="small-12 medium-9 columns content-body">
          




<article class="l-subsection alertbox-article-content article-detail-bl">
  
  <header>
    <div class="l-subsection">
      <h1>Coping with Information Overload</h1>
      <div class="author-meta">by
        
          
            
            <a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a>
            
          
        on  February 1, 1995
        
      </div>

      

      
      <div class="topic-links">
        Topics:
        
          <ul>

  <li><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction</a></li>

</ul>
        
      </div>
      
    </div>
  </header>
  

  
  
  <section>
    <p class="lede"><strong>Summary:</strong> Chapter 8 from Jakob Nielsen&#39;s book, Multimedia and Hypertext: The Internet and Beyond, explores a variety of information retrieval strategies for dealing with the ever-increasing volume of information on the internet. </p>
    <br />
  </section>
  
  

  
  <section id="article-body"><p><em>This is chapter 8 from Jakob Nielsen's book <a href="../../books/multimedia-and-hypertext/index.php"> Multimedia and Hypertext: The Internet and Beyond</a>, Morgan Kaufmann Publishers, 1995. (For full literature references, please see the bibliography in the book.) </em></p>

<p>Michael Lesk once wrote a paper called "What To Do When There's Too Much Information" [Lesk 1989]. Lesk was dealing with a hypertext system with 800,000 objects which is certainly larger than most current systems, but future systems will have to deal with at least that many objects and possibly more. Consider that the number of objects available over the WWW was probably at least two million in the beginning of 1995 and that the Library of Congress holds more than 100 million publications. On the WWW, the millions of objects are not registered in any single place, so no single user interface has to deal with that many objects, but in return, the user has no way of truly taking advantage of the full amount of information because it is not being managed and presented in any way.</p>

<p><img alt="Column chart showing papers published by year, 1980-1993" src="http://media.nngroup.com/media/editor/2012/10/31/growth_usability_papers.png" style="width: 585px; height: 291px; "/><br/>
<strong>Figure 8.1. </strong> <em> Chart showing the growth in number of entries per year in the HCIbib database of the human-computer interaction literature. </em></p>

<p><a href="../kill-the-53-day-meme/index.php">The Internet is about doubling every year</a>. The amount of data transmitted over the Usenet netnews is growing by about 181% per year according to statistics from UUnet (a major netnews hub). The actual number of articles transmitted is "only" growing by 132%, and the discrepancy between these two numbers can probably be explained by the growing popularity of transmitting long messages with executable programs and digitized images.</p>

<p>At the end of 1994, about 100,000 netnews articles were posted per day, and 48% of the total number of bytes transmitted were due to articles that were 16 kB or more. In the beginning of 1994, only 40,000 articles were posted per day, and only 33% of the transmitted data were due to articles that were 16 kB or more.</p>

<p>Also, the number of newsgroups to which these messages get posted is growing by 52% per year, meaning that individual newsgroups do not see quite as rapid growth. Even so, individual newsgroups do grow. For example, alt.hypertext had an annual growth rate of 77% in number of messages posted from 1992 to 1994 and comp.human-factors had an annual growth rate of 92% in number of messages posted. These growth rates are largely due to the increase in readership that follows from the annual doubling of the Internet.</p>

<p>I subscribe to an email mailing list of Danes living abroad, and the statistics from this group are a good example of the increasing information overload on the Internet. The number of words of email traffic sent to the group has been growing by 170% per year from about 16,000 words in 1990 to almost 900,000 words in 1994. The number of distribution list members grew by 118% per year over the same period. The must faster growth in traffic than in the number of people is probably related to the fact that the number of potential interactions (person A commenting on person B's postings) grows by the square of the number of members.</p>

<p>Rapidly growing amounts of information can also be found outside electronic systems. According to the January 1990 issue of the journal <cite> Mathematical Review </cite> , the number of mathematical papers published annually has grown from 840 a year in 1870 to about 50,000 in 1989, with an accelerating growth that has doubled the number of papers every ten years since World War 2. In general, the number of scientific papers across all fields have been doubling every 10-15 years for the last two centuries [Price 1956, as cited in Odlyzko 1995]. In fact, the number of paper research publications has grown so large that many scientists have given up keeping up with all of the literature, even in their own highly specialized fields, and many journals now have the reputation of being "write-only," meaning that they are not being read very much. The <cite> Science Citation Index </cite> has found that more than half of the papers published in research journals are never cited by anybody else, and even though that does not prove that nobody read those papers that were not cited, it certainly means that nobody found them of particular value.</p>

<p>Gary Perlman maintains the "HCIbib" online bibliography of the human-computer interaction literature at the Ohio State University. The number of new articles in the HCIbib database has grown by 30% per year as shown in Figure 8.1. A 30% annual growth rate corresponds to 1,300% growth every ten years, which is much more than the doubling of the research literature per decade seen in more established fields like mathematics.</p>

<p>Even though the growth rates differ among disciplines there is no doubt that the research literature is growing fast enough to present scientists with a major information overload problem. Research publications are edited with a view toward keeping down the number of publications by weeding out less interesting submissions. Unedited publications like the netnews or the Danish email list grow much faster and quickly reach the point where many people have to stop reading them.</p>

<p>There are many different ways of calculating the economic value of information: one can consider the cost needed to produce the information, or one can consider what it can sell for. In a world with information overload, one also needs to consider the negative value of information in terms of the resources spent reading or pondering it. If somebody sends an email message to all the employees of a company with a staff of 10,000 then the cost to the company of the time spent on the message can be anywhere from $1,000 (if everybody immediately discards the message) to $15,000 (if everybody reads it). A steady increase in the amount of information risks acting as a time sink that can prevent people from ever getting any real work done.</p>

<p>Fortunately, it is possible to deal with large amounts of information. As an example, Table 8.1 shows an estimate of the amount of information in a Sunday issue of <cite> The New York Times </cite> . (Printing the daily and Sunday editions of The New York Times required 301,000 metric tons of newsprint in 1993.) The Sunday paper does include huge amounts of information, and it is sometimes said that a single Sunday <cite> Times </cite> has more information than the average villager would get in a lifetime during the Middle Ages. In fact, I am sure that old-time villagers encountered lots of information when farming the fields since it takes many megabytes to accurately represent data about weather and growth patterns. But if we only consider official "news" in the form of words or images reporting on world events, edicts from the King or Pope, and similar types of newspaper-like information the comparison may in fact be correct.</p>

<p>The estimates of data content in Table 8.1 were made under the following assumptions: Each full page of text is about 31 kB. Each page is about 262 square inches (0.17 m <sup> 2 </sup> ). Each page of images is about 1.6 MB of uncompressed data, given that about 10% of the images are in color and that the print resolution is approximately equivalent to 72 pixels per inch, in 8 bits grayscale or 24 bit color. Each page of display ads is about 30% empty space, 40% images, and 30% text, corresponding to 0.6 MB of image data and 9 kB of text.</p>

<p>In total, the sample Sunday <cite> New York Times </cite> contained 7.5 MB of text data and 177 MB of image data. (For sake of comparison, this entire book contains about 1.0 MB of text and 13 MB of image data.) People can get this much information in the door every week and still have time for other activities on Sundays. Admittedly, it takes a long time to read every word and study every image in the Sunday <cite> Times </cite> , but then people don't do that. Instead, every reader selects some parts of the paper that is of interest to that individual and skips the rest. It is feasible to get many times more information delivered than one wants because of the fairly cheap distribution mechanism. And it is feasible to skip the most of the paper because it has been designed to make it easy for readers to find information of interest to them.</p>

<p>In the future, one of the most promising approaches to hypertext journalism is the delivery of individualized electronic newspapers. Since all components of a modern newspaper are edited online, it is possible to replace the delivery of a huge printout with online access to exactly those stories that interest the individual reader. An online newspaper would also deliver the latest version of all stories as of the exact time the reader asked for them.</p>

<table>
	<caption align="bottom"><strong>Table 8.1. </strong> <em> The sections in the Sunday <cite> New York Times </cite> , January 9, 1994. In addition to total page count, the table lists the proportion of the pages that was devoted to editorial text and illustrations and to display advertising and text and classified ads. These proportions have been calculated relative to the amount of space taken up by each category of information. The page count excludes several advertising supplements without editorial content that cannot be considered part of the newspaper proper. </em></caption>
	<tbody>
		<tr>
			<th align="left">Section Number and Title</th>
			<th>Pages</th>
			<th>Editorial Text</th>
			<th>Editorial Illustrations</th>
			<th>Display Ads</th>
			<th>Text Ads</th>
		</tr>
		<tr>
		</tr>
		<tr>
			<td>1. News</td>
			<td align="center">32</td>
			<td align="center">37%</td>
			<td align="center">13%</td>
			<td align="center">47%</td>
			<td align="center">3%</td>
		</tr>
		<tr>
		</tr>
		<tr>
			<td>2. Arts &amp; Leisure</td>
			<td align="center">40</td>
			<td align="center">24%</td>
			<td align="center">15%</td>
			<td align="center">60%</td>
			<td align="center">1%</td>
		</tr>
		<tr>
		</tr>
		<tr>
			<td>3. Business</td>
			<td align="center">42</td>
			<td align="center">39%</td>
			<td align="center">7%</td>
			<td align="center">30%</td>
			<td align="center">26%</td>
		</tr>
		<tr>
		</tr>
		<tr>
			<td>4. The Week in Review</td>
			<td align="center">22</td>
			<td align="center">24%</td>
			<td align="center">14%</td>
			<td align="center">20%</td>
			<td align="center">42%</td>
		</tr>
		<tr>
		</tr>
		<tr>
			<td>4A. Education Life (actually 52 half-size pages)</td>
			<td align="center">26</td>
			<td align="center">23%</td>
			<td align="center">15%</td>
			<td align="center">58%</td>
			<td align="center">3%</td>
		</tr>
		<tr>
		</tr>
		<tr>
			<td>5. Travel</td>
			<td align="center">38</td>
			<td align="center">16%</td>
			<td align="center">13%</td>
			<td align="center">62%</td>
			<td align="center">9%</td>
		</tr>
		<tr>
		</tr>
		<tr>
			<td>6. The New York Times Magazine (actually 68 half-size pages)</td>
			<td align="center">34</td>
			<td align="center">32%</td>
			<td align="center">26%</td>
			<td align="center">33%</td>
			<td align="center">9%</td>
		</tr>
		<tr>
		</tr>
		<tr>
			<td>7. Book Review (actually 32 half-size pages)</td>
			<td align="center">16</td>
			<td align="center">54%</td>
			<td align="center">9%</td>
			<td align="center">38%</td>
			<td align="center">1%</td>
		</tr>
		<tr>
		</tr>
		<tr>
			<td>8. SportsSunday</td>
			<td align="center">24</td>
			<td align="center">31%</td>
			<td align="center">14%</td>
			<td align="center">39%</td>
			<td align="center">16%</td>
		</tr>
		<tr>
		</tr>
		<tr>
			<td>9. Styles of the Times</td>
			<td align="center">10</td>
			<td align="center">35%</td>
			<td align="center">40%</td>
			<td align="center">27%</td>
			<td align="center">1%</td>
		</tr>
		<tr>
		</tr>
		<tr>
			<td>10. Real Estate</td>
			<td align="center">42</td>
			<td align="center">7%</td>
			<td align="center">8%</td>
			<td align="center">34%</td>
			<td align="center">51%</td>
		</tr>
		<tr>
		</tr>
		<tr>
			<td>11. Help Wanted</td>
			<td align="center">42</td>
			<td align="center">0%</td>
			<td align="center">0%</td>
			<td align="center">13%</td>
			<td align="center">87%</td>
		</tr>
		<tr>
		</tr>
		<tr>
			<td>12. Television listings (actually 56 quarter-size pages)</td>
			<td align="center">14</td>
			<td align="center">75%</td>
			<td align="center">7%</td>
			<td align="center">18%</td>
			<td align="center">0%</td>
		</tr>
		<tr>
		</tr>
		<tr>
			<td>13. New Jersey Weekly (distributed to suburban subscribers as a replacement for the City Weekly that was distributed in New York City)</td>
			<td align="center">24</td>
			<td align="center">26%</td>
			<td align="center">11%</td>
			<td align="center">62%</td>
			<td align="center">2%</td>
		</tr>
		<tr style="background-color: #dddddd">
			<td><strong>Total for the entire Sunday paper </strong></td>
			<td align="center">406</td>
			<td align="center">26%</td>
			<td align="center">12%</td>
			<td align="center">40%</td>
			<td align="center">&gt;22%</td>
		</tr>
		<tr>
			<td><strong>Equivalent number of full pages </strong></td>
			<td align="center">406</td>
			<td align="center">105</td>
			<td align="center">50</td>
			<td align="center">161</td>
			<td align="center">91</td>
		</tr>
		<tr>
			<td><strong>Information in Megabytes </strong></td>
			<td> </td>
			<td align="center">3.2</td>
			<td align="center">77</td>
			<td align="center">102</td>
			<td align="center">&gt;2.8</td>
		</tr>
	</tbody>
</table>

<p>Figures 8.2 and 8.3 show an example of an individualized electronic newspaper developed at GMD in Germany [A. Haake et al. 1994]. The newspaper interface, designed by Klaus Reichenberger, can automatically lay out the current stories that match the user's stated interests, resulting in interesting and appealing displays that invite further exploration and reading. As can be seen from comparing Figures 8.2 and 8.3, different sets of stories can be assembled for readers with different interests.</p>

<p><img alt="Experimental personalization system from 1992" src="http://media.nngroup.com/media/editor/2012/10/31/gmd_individualized_electronic_newspaper.png" style="width: 1152px; height: 900px; "/><br/>
<strong>Figure 8.2. </strong> <em> The experimental individualized electronic newspaper (IEN) from GMD in Germany showing a page customized for a reader with an interest in science. Compare with Figure 8.3 showing a page from the same newspaper customized for a reader with an interest in sport. Copyright © 1992 by Klaus Reichenberger and GMD-IPSI, reprinted by permission. </em></p>

<p>Utilizing hypermedia linking, each part of the electronic newspaper can follow the well-established principle from printed newspaper with front pages and cover stories for each of the main sections of the newspaper.</p>

<p>Current attempts at putting newspaper stories online on services like America Online use much more boring menu interfaces where the user gets very little information about the stories before having to decide which ones to read. There is no doubt that better systems for automated layout (like the ones shown in the figures here) will be necessary for online newspapers to have a chance of competing with printed ones that are based on hundreds of years of typographical and editorial experience.</p>

<p><img alt="Same system as in previous figure, but personalized for sports" src="http://media.nngroup.com/media/editor/2012/10/31/sports_page.png" style="width: 1152px; height: 900px; "/><br/>
<strong>Figure 8.3. </strong> <em> The experimental individualized electronic newspaper (IEN) from GMD in Germany showing a page customized for a reader with an interest in sport. Copyright © 1992 by Klaus Reichenberger and GMD-IPSI, reprinted by permission. </em></p>

<p>There are three main approaches to addressing information overload. The first (and often the most successful) is good user interface design and good editorial preparation of the data, resulting in an ability for the user to rapidly skim the information and pick out the exact pieces that interest him or her. Paper newspapers like The New York Times exemplify this solution to the information overload problem. If I am really busy one day, I can just scan the front page of the newspaper and know that I have not missed being informed about any really important event.</p>

<p>The two other solutions are information retrieval and information filtering [Belkin and Croft 1992]. The difference between the two is that retrieval is normally done actively by the user in specific cases where the user is looking for a certain piece of information, whereas filtering is done continuously in cases where the user wants to be kept informed about certain events. For example, a typical retrieval task would be to find the name of the president of IBM and a typical filtering task would be to be informed every time IBM announced a new workstation but not when it announces a new mainframe or PC.</p>

<h2>Information Retrieval</h2>

<p>A search for information in a hypertext might be performed purely by navigation, but it should also be possible for the user to have the computer find things through various query mechanisms. Navigation is best for information spaces that are small enough to be covered exhaustively and familiar enough to the users to let them find their way around. Many information spaces in real life are unfortunately large and unfamiliar and require the use of queries to find information.</p>

<p>The simplest query principle is the full text search which finds the occurrences of words specified by the user. Some hypertext systems simply take the user to the first occurrence of the search term, but it is much better to display a menu of the hits first as shown in the example from Intermedia in Figure 8.4. The problem with jumping directly to the first term occurrence is that the user has no way of knowing how many other hits are in the hypertext. The general usability principle of letting the user know what is going on leads to a requirement for an overview, even in the case of query results. Figure 8.5 shows the search method from Storyspace which provides a list of all the nodes with hits without indicating the number of times the search terms were found in each nodes. Storyspace has a preview facility which the user can activate by clicking "View current text" to quickly see the beginning of the various nodes before deciding where to jump.</p>

<p><img alt="Screenshot of full-text search feature from 1989" src="http://media.nngroup.com/media/editor/2012/10/31/intermedia_search.gif" style="width: 564px; height: 360px; "/><br/>
<strong>Figure 8.4. </strong> <em> Intermedia's full-text interface allows users to search the entire Intermedia database to find every occurrence of the specified text in all documents, regardless of type. The list of retrieved documents can be sorted according to five different criteria. Clicking on the document name in the list will allow the user to view information about the document. Double-clicking on the document name will open the document. Copyright © 1989 by Brown University, reprinted with permission. </em></p>

<p>Normally search is done in stages where the user first specifies the query and then has to wait for the system to return the set of found objects. With faster computers it is becoming possible to perform dynamic queries where the users manipulate sliders or other controls to specify desired search values and get immediate feedback from the system as they do so. In one study the subjects were able to find information in a database 119% faster (i.e., in less than half the time) when they were given dynamic feedback as they constructed their query than when they did not get any feedback until after they had submitted a complete query to the system [Ahlberg et al. 1992].</p>

<p>Figure 8.6 shows the use of dynamic queries in the FilmFinder from the University of Maryland [Ahlberg and Shneiderman 1994]. The user can specify that only films of a certain running length are of interest by moving the range selector slider, and the display will update in real time while the user moves the slider, making it very clear whether reasonable values are being specified. The overview diagram in the FilmFinder is a so-called starfield display where each of the retrieved objects is shown as a "star" in a two-dimensional scatterplot. The two dimensions of the scatterplot can be chosen by the user to represent particularly meaningful object attributes, and a third dimension can be used to color-code the dots (in the figure, genre like Sci-Fi or Western was the attribute used to color-code the films). Note in Figure 8.6 how zooming and panning the scatterplot in effect is the same as specifying query intervals for the attributes represented by the diagram axes.</p>

<p><img alt="Screenshot of full-text search from 1992" src="http://media.nngroup.com/media/editor/2012/10/31/storyspace_search.gif" style="width: 981px; height: 711px; "/><br/>
<strong>Figure 8.5. </strong> <em> Searching for the name "Beer" in the Storyspace version of the Dickens Web [Landow and Kahn 1992]. Copyright © 1992-94 by Paul Kahn, George P. Landow, and Brown University, reprinted by permission. </em></p>

<p>Figure 8.6 also illustrates the output-as-input interaction technique. When the user has found an interesting film (here <cite> Murder on the Orient Express </cite> ), the user can click on the dot representing that object and link to a box with more detailed information about the film. The user can then link further by taking this query output as input for the next query: in our example, the user has chosen Sean Connery's name as a search term and transferred it from the initial search result to a new query specification to see only films starring Sean Connery.</p>

<p><img alt="Screenshot of dynamic query" src="http://media.nngroup.com/media/editor/2012/10/31/filmfinder_dynamic_query.png" style="width: 996px; height: 710px; "/><br/>
<strong>Figure 8.6. </strong> <em> The Maryland FilmFinder uses dynamic queries to allow users to search for films with various attributes. Here, the user has specified a search for films starring Sean Connery with a running length between 60 and 269 minutes. The user has furthermore used the zoom control sliders for the x and y axes to display only the part of the diagram with films after 1960 that are rated more than 4 in popularity. Copyright © 1993 by University of Maryland Human-Computer Interaction Lab., reprinted by permission. </em></p>

<p>Even though most query systems perform text searches or select objects based on numeric attribute values, it is also possible to search on other types of media. Since humans are very visually oriented, they often rely on images to remember things, and image-based searchers might well be a very useful supplement to text and attribute-based search. Unfortunately, current computer capabilities in the pattern recognition area are very limited, and computers cannot really understand pictures well enough to deal with them as well as with text. Therefore, the traditional way to search image databases has been the one shown in Figure 8.7, where each picture has to be annotated with a text caption or keywords for search purposes.</p>

<p><img alt="Screenshot of WAIS image search" src="http://media.nngroup.com/media/editor/2012/10/31/wais_image_search.png" style="width: 570px; height: 364px; "/><br/>
<strong>Figure 8.7. </strong> <em> Screendump from a session where the user has connected to a WAIS (wide area information service) server with photographs from the Smithsonian Institution to search for images of the Greek goddess Athena. The system uses a full text search in the caption to retrieve the image. Copyright © 1992 by the Smithsonian Institution, reprinted by permission. </em></p>

<p>Searching the captions is a much better way to find a picture than flipping through thousands of photos but it does not work in all cases. In the example in Figure 8.7 it might have been the case that the user wanted a coin with the picture of a woman or that the user remembered approximately how the coin looked but not exactly what it represented.</p>

<p><img alt="Screenshot of Japanese image search system from 1993" src="http://media.nngroup.com/media/editor/2012/10/31/image_search.png" style="width: 1279px; height: 1023px; "/><br/>
<strong>Figure 8.8. </strong> <em> Hypermedia navigation by image retrieval. This experimental system shows a tourist guide to Paris where the user has asked the system to show all other images that look somewhat like the photo of the Eiffel Tower in the upper right window. In the middle of the screen, the system displays miniatures of its pictures of tall thin things, and the user has selected some of these images for full-scale display. The images again have hypertext links to the maps and to textual descriptions of the sights of Paris. Copyright © 1993 by NEC Corporation, reprinted by permission. </em></p>

<p>Some experimental systems have been developed that allow computers to deal with image understanding in a rudimentary manner. For example, Figure 8.8 shows a system that understands the general shape of the major objects depicted in an image [Hirata et al. 1993]. In order to find pictures, the user can either sketch the approximate composition of the image or select an existing picture and use it to link to more of the same.</p>

<p><img alt="Screenshot of FSN navigation" src="http://media.nngroup.com/media/editor/2012/10/31/fsn1.png" style="width: 863px; height: 869px; "/><br/>
<strong>Figure 8.9. </strong> <em> Screen from Joel Tesler's FSN file system navigator [Fairchild 1993]. Here, the user has performed a search on the file system to find files that are larger than one million bytes and older than one hundred days. Copyright © 1994 by Silicon Graphics, Inc., reprinted by permission. </em></p>

<p> </p>

<p><img alt="Close-up view of FSN" src="http://media.nngroup.com/media/editor/2012/10/31/fsn2.png" style="width: 865px; height: 869px; "/><br/>
<strong>Figure 8.10. </strong> <em> Revised view of the file system from Figure 8.9. Here, the user has used FSN's three-dimensional navigation system to move in to a close-up of one of the directories in order to get a better view of the individual files. Copyright © 1994 by Silicon Graphics, Inc., reprinted by permission. </em></p>

<p>A very promising way of showing search results is to integrate them with the overview diagram by highlighting those nodes that contain "hits." Hits indicate the number of the user's search terms that can be found in the node. It is possible to use more advanced query facilities and also add to the hit score if words are found which are synonyms or otherwise related to the search terms.</p>

<p>Figures 8.9 and 8.10 show how the FSN system highlights search hits in a three-dimensional overview of an information space. Movie aficionados will be interested in knowing that FSN (pronounced "fusion") was the system used in the film <cite> Jurassic Park </cite> in the memorable example of product placement where a child sees a workstation and happily declares "This is Unix; I can use that" (and saves the day by rapt navigation of the FSN interface). Most of the time, though, the goal of FSN is not to fight dinosaurs but to manage large file systems.</p>

<p>SuperBook [Egan et al. 1989] annotates the names of nodes with the number of hits to allow users to see not just where there is something of interest but also how much there is. It would be possible to use this type of search result to construct fisheye views since the number of hits in a given region of the information space would indicate how interesting that region must be to the user.</p>

<p>One can also use more sophisticated methods from the field of information retrieval. This brief section cannot do justice to that field, which is an active research area in its own right, so the interested reader should read a good textbook like Salton's Automatic Text Processing [1989] or at least a full survey article like [Bärtschi 1985].</p>

<p>Information retrieval can be integrated with hypertext navigation to deliver powerful means of finding information. Figure 8.11 shows a hypertext system [Andersen et al. 1989] for reading the Usenet network news, which is a world-wide bulletin board system with a huge number of messages about various computer-related topics. Since there are far too many nodes in the system to rely on manually constructed links, we use a full text similarity rating calculated by counting the overlap in vocabulary between any two nodes. A list of the articles that are rated as the most similar to the current article is displayed when the user clicks on the "similarity" button.</p>

<p>In a case where we have a hypertext available in which the links have already been constructed, we should be able to utilize the information inherent in the linking structure to perform more semantically meaningful searches than just plain full text searches. This step is possible because a hypertext can be considered as a "belief network" to the extent that if two nodes are linked, then we "believe" that their contents are related in some way.</p>

<p><img alt="Screenshot from the HyperNews interface to read the network news, including relevancy ratings as link annotations" src="http://media.nngroup.com/media/editor/2012/10/31/hypernews.gif" style="width: 542px; height: 361px; "/><br/>
<strong>Figure 8.11. </strong> <em> A screen from the HyperNews system showing pie icons rating the links to other articles. </em></p>

<p>Thus if a node matches a search, then we should also assign a higher score for the other nodes it is linked to since our "belief" that the connected nodes are related justifies the propagation of scores among them. One way of calculating this score is by assigning the final search result for a node as the sum of the number of hits in the node itself (called the intrinsic score) and some weighted average of the scores for the nodes it is linked to (called the extrinsic score). As a simple example, we could assign the final query score as the intrinsic score plus half the extrinsic score.</p>

<p>In the example in Figure 8.12, we see that the central node ends up getting the highest query score even though it does not contain any of the search terms (as can be seen from the fact that it has an intrinsic score of zero). This is because the central node sits in the middle of a lot of information related to the user's query and is therefore probably also highly relevant.</p>

<p>In addition to just finding information, query mechanisms can also be used to filter the hypertext so that only relevant links are made active and only relevant nodes are shown in overview diagrams. Even though the "raw" hypertext may be large and confusing, the filtered hypertext can still be easy to navigate. Such a combination of query methods to select a subset of the hypertext and traditional navigation to look at the information might be the best of both worlds if done right.</p>

<p><img alt="Example of how to propagate relevancy ratings along the links of a hypertext graph." src="http://media.nngroup.com/media/editor/2012/10/31/propagating_relevancy_along_links.png" style="width: 657px; height: 252px; "/><br/>
<strong>Figure 8.12. </strong> <em> An example of a calculation of query scores as a combination of intrinsic scores (how well the individual node itself matches the user's query) and extrinsic scores (how well the nodes it is linked to match the query). Here we have used a rule that gives a node a search score equal to its intrinsic score plus half its extrinsic score (the sum of the scores of the nodes it is linked to). </em></p>

<h2>Human Editing</h2>

<p>Despite much work on automated ways of reducing information overload, the most promising approaches will probably be the ones that rely on human judgment to some extent. Some authorities on the human factors of information believe that it is impossible to achieve sufficiently usable information filtering without having a human in the loop somewhere to make individual judgments as to the quality and relevance of each information object. As long as computers are not intelligent enough to be able to actually understand the content of the information they are processing, they will never be able to provide true quality ratings. In fact, perfect information filtering is likely to be an "AI complete problem" in the sense that solving it will be equivalent to solving the complete set of intelligent computing problems. (Maybe some day we will achieve sufficiently good artificial intelligence to allow the computer to understand the content of information objects, but quality judgments seem to be beyond the scope of AI for the next many years. Whether it is impossible or just currently infeasible to get computers to rate quality is a philosophical discussion that is fairly irrelevant for anybody wanting to ship product during the next ten or twenty years since the two positions are equivalent within this time horizon.) An alternative approach to reducing information overload is the time-honored approach of an editor with a firm hand.</p>

<p><img alt="Screenshot from the Ziff-Davis Interchange service in 1994" src="http://media.nngroup.com/media/editor/2012/10/31/interchange_service_column.png" style="width: 648px; height: 488px; "/><br/>
<strong>Figure 8.13. </strong> <em> Edited column on the Interchange service with hypertext links to other articles that are available on the service. Copyright © 1994 by Interchange Network Company, reprinted by permission. </em></p>

<p>Figure 8.13 shows an example of an edited column from the Interchange online service [Perkins 1995]. The human editor has selected a number of articles which she believes will be of relevance to readers with an interest in the stated topic of the column (shopping tips for computer buyers). She annotates the links with her own comments and then provides a link to the source material. Note how the personality of the editor is emphasized by showing her picture and by the style of her writing. In fact, it is likely that the information explosion will increase users' desire to feel that they are in touch with real humans and not just with some mass of Internet data. In his book <a href="http://www.amazon.com/exec/obidos/ISBN=0380704374/ref=nosim/useitcomusableinA/"> Megatrends</a>, John Naisbitt talked about a trend he called High Tech-High Touch, and the edited column in Figure 8.13 is an example of this trend. In a series of user tests I conducted of various WWW sites, people were invariably thrilled about a page with a picture of Microsoft's webmaster standing in front of their server: users enjoyed seeing the guy who was brining them the information as opposed to having it come from a faceless bureaucracy.</p>

<p>Note how the "Deals &amp; Steals" column in Figure 8.13 has a hypertext link to the "Buyer's Advisor Discussion" where the users can add their comments to the topics discussed in the column. By offering this facility, Interchange makes it easier for users to find comments by other users on topics they are interested in, since these comments will often be linked to specific columns or articles. A Bellcore research project called ephemeral interest groups had very similar goals [Brothers et al. 1992]. In the ephemeral interest group system, people could indicate their interest in a topic by "joining" postings to a bulletin board system in order to get sent follow-up messages to the postings. Every single message posted was a potential seed for an ephemeral interest group, and the groups only lived for as long as members posted additional follow-ups. This scheme was very successful in increasing the value of the messages seen by any individual participant: on a 1-5 scale (where 1 indicated completely irrelevant material and 5 indicated very relevant material), users rated messages sent to them by the ephemeral interest group system as 3.9 on the average, whereas the same messages only received a 2.7 rating when we tried sending them to a control group.</p>

<p>Editing can also be done collaboratively where a group of users build up information structures to help each other. Figure 8.14 shows a sample information digest constructed at Lotus Development Corporation [Maltz and Ehrlich 1995]. Because people who work in the same organization know and trust each other, they can assume that information recommended by their colleagues will be of much higher-than-average relevance to them. Information digests can exist on the corporate net for a variety of topics, and employees who find interesting information on the Internet or elsewhere can add the information with an annotation and a hypertext link.</p>

<p><img alt="Screenshot of information digest in Lotus Notes" src="http://media.nngroup.com/media/editor/2012/10/31/lotus_information_digest.png" style="width: 605px; height: 388px; "/><br/>
<strong>Figure 8.14. </strong> <em> An Information Digest written in Lotus Notes by Kate Ehrlich at Lotus Development Corp. Copyright © 1994 by Kate Ehrlich, reprinted by permission. </em></p>

<p>Maltz and Ehrlich refer to the system shown in Figure 8.14 as "active filtering" in contrast to the passive filtering discussed in the following section where there is no direct connection between a person casting a vote for some information and the readers who come later and filter the documents based on these votes. In the "active" filtering approach, there is an intent on the part of the person who finds some information to share it with his or her colleagues. The person finding the information may even recommend the information for particular colleagues who are known to have an interest in a particular area, and the Lotus information digest system has a feature for sending announcements of new and interesting information to specific individuals in addition to adding the information to a digest for public consumption.</p>

<h2>Interest Voting and Readwear</h2>

<p>Even though human editing is ideal, there are many cases where it is infeasible to rely solely on individual editors. The two main problems with human editors are that nobody can cover the full extent of information available on the Internet and other rich sources and that any individual reader only has a partial degree of agreement with the judgment of any individual editor.</p>

<p><img alt="Screenshot of GroupLens" src="http://media.nngroup.com/media/editor/2012/10/31/grouplens.png" style="width: 564px; height: 364px; "/><br/>
<strong>Figure 8.15. </strong> <em> Reading a netnews article with a modified front end in GroupLens. The user can click on one of the five ratings buttons with the mouse, or type a number from 1 to 5 on the keyboard. Copyright © 1994 by Paul Resnick, Neophytos Iacovou, Mitesh Suchak, Peter Bergstrom, and John Riedl, reprinted by permission. </em></p>

<p>An alternative approach is to rely on aggregate judgments by a larger group of editors. Figures 8.15 and 8.16 show an approach to reducing information overload in netnews suggested by Paul Resnick and colleagues from MIT and the University of Minnesota. Their GroupLens system [Resnick et al. 1994] collects quality ratings from anybody who happens to read a netnews article. These ratings are forwarded to special servers called Better Bit Bureaus from which they are made available to future readers. As Figure 8.16 shows, having ratings available can help people decide what articles to choose from a menu.</p>

<p>So far, quality ratings are only used in research projects like GroupLens. If they become more widely used, one could imagine several different ways of using them. The simplest approach would be to collect ratings from anybody who happens to read an article and just compute its quality as the mean of the individual ratings. Given that netnews was estimated to have seven million readers in 1994, sufficient ratings to form a reliable mean should be available in a few minutes no matter when an article is posted, especially considering that the readers are scattered in all timezones around the world. A major problem with this approach is that it is not clear that you share interests with a group of geeks in New Zealand or whoever happens to be the first to rate an article. It is likely that the early ratings will come to dominate the quality score for an article since very few other users will bother reading an article once it has gathered a string of poor ratings.</p>

<p><img alt="Quality ratings are visualized as bars next to links" src="http://media.nngroup.com/media/editor/2012/10/31/quality_ratings.png" style="width: 611px; height: 159px; "/><br/>
<strong>Figure 8.16. </strong> <em> Quality rating scores integrated into a netnews reading interface. The bars represent quality grades as retrieved from the Better Bit Bureau server. Copyright © 1994 by Paul Resnick, Neophytos Iacovou, Mitesh Suchak, Peter Bergstrom, and John Riedl, reprinted by permission. </em></p>

<p>A second potential problem with world-wide ratings is the potential for "ratings wars" where people would stuff the ballot box so to speak and seek out postings from their friends or enemies and rate articles without really having read the content. Indeed, netnews articles can be processed automatically as proven, e.g., by the Norwegian hacker who released a "cancelbot" on the Internet to hunt down and erase messages from a certain pair of lawyers who had "spammed" inappropriate ads for their office to thousands of unrelated newsgroups (and who do not deserve further publicity by having their name mentioned here).</p>

<p>An alternative approach to quality voting would be to collect only votes from people within your own organization. The downside of doing so would be that more people would have to read irrelevant articles before it was determined that they were irrelevant, but in return the ratings would be more directed toward the specific needs of the individual organization. It would also be possible to collect votes world-wide but only count the ones from people who in some way had been deemed responsible or who were known to have the same taste as the user who wanted to review the quality ratings.</p>

<p>Hill et al. [1995] experimented with interest voting where the users actively had to indicate their interest in each of 500 films. The system collected votes from 291 people who provided more than 55,000 ratings and then constructed a statistical model of which users had similar interests to which other users. In other words, instead of assuming that everybody had the same taste, the system recognized that people are different and that the same film may be a favorite of some people while being hated by others. The trick is to find a group of other people who share taste with the user for whom the system is trying to find relevant information. Luckily, if the system can aggregate information across a large enough population (e.g., the users of the Internet), there will always be others who have similar tastes, no matter how eclectic you may think you are. Hill et al.'s system was fairly successful in generating recommendations of new films for the users, with a correlation of 0.62 between the system's prediction of how well people would like a film and the actual rating given by the users. In comparison, the correlation between ratings from nationally-known movie critics and the users' own ratings was only 0.22. In other words, one can find much better information objects for users if one knows their taste and ratings of other objects than if one just rely on ratings of the intrinsic quality of the objects.</p>

<p>No matter how quality votes are gathered and distributed they have the distinct disadvantage that they require active decisions on the part of the users. In Figure 8.15, the user has to make up his or her mind as to what rating on a five-point scale to assign to the article, after which the user has to move the mouse to the chosen button to enter the vote. People might be motivated to vote on things that particularly upset them as being bad or that they find exceptionally good, but it will be difficult to get people to spend the time to rate everything they read. Just consider the many publications that have reader reply cards where you are asked to rate all articles in a given issue. How many of these cards have you sent back? And how often did you provide ratings for all the articles?</p>

<p><img alt="Screenshot of HyperTED" src="http://media.nngroup.com/media/editor/2012/10/31/hyperted.png" style="width: 356px; height: 231px; "/><br/>
<strong>Figure 8.17. </strong> <em> The link dialog box from HyperTED. The system keeps track of how often each link has been followed and displays this count when the user selects a link. Copyright © 1994 by Adrian Vanzyl, Monash University Medical Informatics, reprinted by permission. </em></p>

<p>An alternative to voting is to rely on information that is gathered unobtrusively by the computer in the background as the user goes about his or her normal activities. This approach is called readwear [Hill et al. 1992] because the notion is that reading things on the computer will cause them to be "worn" in the same way as a physical book is worn by repeated reading. In fact, a book that has been used a lot will often open by itself to the pages that have been read the most even if the user has not left a bookmark. Figure 8.17 shows an example of readwear in the HyperTED system: the system keeps track of how many times each link has been followed, and users can use this information to help decide which of several links they want to follow themselves.</p>

<p>In the case of netnews, readwear might be collected by an instrumented system that recorded how long each user spent reading each article. The assumption might be that articles that people spent a long time on would be those that had some inherent quality. Of course there is no guarantee that this is true every time. For example, a user might spend a long time looking at something that was upsetting or potentially false—or the user may just have left the window open while taking a phone call. On the average, though, it does seem plausible that people would invest their time wisely and spend the most time on the best information, and a study of eight users reading 8,000 netnews messages found a strong correlation between the time the users spent reading each message and their subjective rating of the message [Morita and Shinoda 1994].</p>

<h2>The <em> n </em> of 2 <em> n </em> Approach</h2>

<p>It is possible to combine automated and human methods for information filtering. Susan Dumais.; and I have experimented with doing so for assigning submitted conference papers to members of the review committee [Dumais and Nielsen 1992]. Our method is called " <em> n </em> of 2 <em> n </em> " and involves having the computer pick about twice as many papers for each reviewer as that person actually is asked to review. The reviewer thus uses his or her individual judgment to pick n information objects from a selection of 2 <em> n </em> objects presented by the system. By adding this last element of human judgment, any wildly wrong guesses by the computer are removed from the final set of information objects actually read by the user.</p>

<p>The " <em> n </em> of 2 <em> n </em> " method can be used for many other applications than conference paper reviews. It requires just one additional attribute of the information objects: it should be possible for the human user to pick the most relevant <em> n </em> objects in substantially less time than it would take to read all 2 <em> n </em> information objects. In the case of conference submissions, it is usually possible for a reviewer to read the abstract in much less time than it would take to read the full paper and it is usually possible to assess the topic of a paper from its abstract, so conference papers are ideal for the " <em> n </em> of 2 <em> n </em> " method.</p>

<p>The assignment of submitted manuscripts to reviewers is a common task in the scientific community and is an important part of the duties of journal editors, conference program chairs, and research councils. Finding reviewers for journal submissions and some types of grant proposals can normally be done for a small number of submissions at a time and at a more or less leisurely pace. For conference submissions and other forms of grant proposals, however, the reviews and review assignments must be completed under severe time pressure, with a very large number of submissions arriving near the announced deadline, making it difficult to plan the review assignments much in advance.</p>

<p>These dual problems of large volume and limited time make the assignment of submitted manuscripts to reviewers a complicated job that has traditionally been handled by a single person (or at most a few people) under quite stressful conditions. Also, manual review assignment is only possible if the person doing the assignments (typically the program chair for the conference) knows all the members of the review committee and their respective areas of expertise. As some conferences grow in scope with respect to number of submissions and reviewers as well as the number of sub-domains of their fields, it would be desirable to develop automated means of assigning the submitted manuscripts to appropriate members of the review committee.</p>

<p>The actual application of assigning manuscripts to reviewers involves two further considerations in addition to the matching of manuscripts and reviewers. First, one needs to guard against conflicts of interest by not assigning any reviewers their own papers or those of close colleagues. Second, there is a need to balance the review assignments to ensure that no single reviewer is overworked just because that person happens to be an appropriate choice for many papers, and that each paper gets assigned a certain minimum number of reviewers. All these constraints can be expressed as linear inequalities that can be handled by a linear programming package, so the entire review assignment can be done automatically.</p>

<p>Dumais and I tried automatic assignment of manuscripts to reviewers for the Hypertext'91 conference as a pilot project where a human program chair made the final assignments. We also did actual automated assignments for the INTERCHI'93 and CHI'94 conferences where we were papers co-chairs ourselves. Hypertext'91 was a fairly small conference with 117 manuscripts submitted and 25 members of the review committee. INTERCHI'93 and CHI'94 were larger conferences with 330 and 263 submitted papers, respectively, and 307 and 276 members of the review committee, respectively.</p>

<p>For Hypertext'91 the members of the review committee had been asked to review an average of 26 papers each by the program chair. The program chair had received help from our automated method and thus the actual review assignments could not be seen as representative of the work of an unaided human. We simulated the result of purely human review assignments by asking three other hypertext experts to manually assign papers to reviewers. The human experts assigned an average of 28 papers to each reviewer, achieving a mean rated relevance of 3.6 on a 1-5 scale where 5 was best. (Relevance ratings were gathered by asking each reviewer to rate how closely each manuscript matched his or her expertise as a reviewer: The rating scale was 1=how did I get this one?; 2=I'm following it, sort of; 3=somewhat relevant; 4=good match; and 5=right up my alley.) In comparison, our automated <em> n </em> of 2 <em> n </em> method achieved a mean rated relevance of 3.8 on the same scale, thus doing slightly better.</p>

<p>For INTERCHI'93 each reviewer was sent ten papers and was asked to review five of them. In order to make sure that each paper was read by at least some reviewers, we had pre-assigned three papers to each reviewer, meaning that the reviewer had the freedom to pick two additional manuscripts from the remaining seven that had not been pre-assigned. On a 1-5 scale (with 5 best), the reviewers rated the relevance of the papers they ended up reviewing as 4.1 on the average. For CHI'94 each reviewer was sent eleven papers and actually reviewed seven, and the mean relevance rating was 3.9 on the 1-5 scale. (Note that even though we refer to our approach as " <em> n </em> of 2 <em> n </em> " it also works when people can choose some other proportion of the selected information object (e.g., 7 of 11).)</p>

<p>For the INTERCHI'93 and CHI'94 conferences we do not have data from simulated human manuscript assignments because the job was too large to be done when it was not absolutely necessary. However, data exists from the very similar CHI'92 conference where a human program chair made the review assignments with help from several experts in different subfields. For the 1992 conference, the mean rated relevance of the manuscripts sent to the reviewers was 4.1 on the 1-5 scale. This is exactly the same as the relevance achieved by our automated method for INTERCHI'93 and slightly better than the result of the automated method for CHI'94.</p>

<p>An interesting aspect of the automated review assignment compared with manual assignment is that some of the more famous reviewers expressed satisfaction with getting papers that were more in line with their current interests than they were used to. Normally, such famous people continue getting papers in areas for which they are famous for many years after they have stopped working in those areas, simply because human committee chairs think, "Oh, a paper on XX, that must be just the thing for Dr. YY." With automated assignment, all the computer knows is what the reviewers told it themselves when defining their interest profile, so they mainly get papers in the areas they specifically indicated as their current interests.</p>
</section>
  

  
  <section>
    
        
    
  </section>
  
  
  
  
  

  


  <section>
    <br />
    <p class="share"><b>Share <span class="hide-for-small-down">this article</span>:</b> <a href="https://twitter.com/intent/tweet?url=http://www.nngroup.com/articles/coping-information-overload/&amp;text=Coping%20with%20Information%20Overload&amp;via=nngroup">Twitter</a> | <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=http://www.nngroup.com/articles/coping-information-overload/&amp;title=Coping%20with%20Information%20Overload&amp;source=Nielsen%20Norman%20Group">LinkedIn</a> | <a href="https://plus.google.com/share?url=http://www.nngroup.com/articles/coping-information-overload/">Google+</a> | <a href="mailto:?subject=NN/g Article: Coping with Information Overload&amp;body=http://www.nngroup.com/articles/coping-information-overload/">Email</a></p>
    <hr />
  </section>

  <footer class="l-subsection">
    
    
    <div class="learn-more">
      <h2>Learn More</h2>
      <p><a href="../subscribe/index.php">Subscribe to the weekly newsletter</a> to get notified about future articles.</p>
      </div>
    
    

    <div class="row show-for-small-down related-content">
      
      
      

      
      
        <div class="column small-12 topic-links">All articles about:
          <ul>
            
            <li><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction</a></li>
            
            <li><a href="../../topic/technology/index.php">new technologies</a></li>
            
          </ul>
        </div>
      
      

      
      
        <div class="column small-12">
          <h3>UX Conference Training Courses</h3>
           <ul class="no-bullet">
    
        <li><a href="../../courses/ux-basic-training/index.php">UX Basic Training</a></li>
    
        <li><a href="../../courses/hci/index.php">User Interface Principles Every Designer Must Know</a></li>
    
        <li><a href="../../courses/emerging-patterns-web-design/index.php">Emerging Patterns for Web Design</a></li>
    
        <li><a href="../../courses/interaction-design-3-day-course/index.php">Interaction Design: 3-Day Course</a></li>
    
</ul>
        </div>
      
      

      
      
      

      
      
        <div class="small-12 column">
          <h3>Online Seminars</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../online-seminars/beyond-screens/index.php">Beyond Screens</a></li>
              
            
          </ul>
        </div>
      
      
    </div>

    <div class="hide-for-small-down row related-content">
      <div class="medium-6 columns">
        
        
          <div>
            <h3>Articles</h3>
            <ul class="no-bullet">
              
                
                <li><a href="../embarrassment/index.php">Computer-Assisted Embarrassment</a></li>
                
              
                
                <li><a href="../enhancement/index.php">The Role of Enhancement in Web Design</a></li>
                
              
                
                <li><a href="../voice-interaction-ux/index.php">Voice Interaction UX: Brave New World...Same Old Story</a></li>
                
              
                
                <li><a href="../human-body-touch-input/index.php">The Human Body as Touchscreen Replacement</a></li>
                
              
                
                <li><a href="../kinect-gestural-ui-first-impressions/index.php">Kinect Gestural UI: First Impressions</a></li>
                
              
            </ul>
          </div>
        
        

        

      </div>
      <div class="medium-6 columns">
        
          
          <div>
            <h3>UX Conference Training Courses</h3>
             <ul class="no-bullet">
    
        <li><a href="../../courses/ux-basic-training/index.php">UX Basic Training</a></li>
    
        <li><a href="../../courses/hci/index.php">User Interface Principles Every Designer Must Know</a></li>
    
        <li><a href="../../courses/emerging-patterns-web-design/index.php">Emerging Patterns for Web Design</a></li>
    
        <li><a href="../../courses/interaction-design-3-day-course/index.php">Interaction Design: 3-Day Course</a></li>
    
</ul>
          </div>
          
        
        
          
          <div>
            <h3>Online Seminars</h3>
            <ul class="no-bullet">
            
                <li><a href="../../online-seminars/beyond-screens/index.php">Beyond Screens</a></li>
            
            </ul>
          </div>
          
        
      </div>
    </div>
  </footer>
</article>

        </div>

      

    
  </div>
  <div class="row">
    
      <div class="column small-12 show-for-small-down l-navrail announcements">
        
          
        
      </div>
    
    
  </div>

    </div>
    




<footer id="page-footer">
    <div class="row hide-for-print">
        <div class="column small-12 medium-6">
            <section class="footer-popular">
                
                <h1>Popular Topics</h1>
                <div style="float: left;  width: 15em; display: block;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/applications/index.php">Application Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/e-commerce/index.php">E-Commerce Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/eyetracking/index.php">Eyetracking </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/information-architecture/index.php">Information Architecture </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/intranets/index.php">Intranet Design </a></li>
	<li><a href="../../topic/interaction-design/index.php">Interaction Design </a></li>
</ul>
</div>

<div style="float: left;  width: 15em; display: block; list-style: none;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/mobile-and-tablet-design/index.php">Mobile and Tablet Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/credibility-persuasion/index.php">Persuasive Design</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/research-methods/index.php">Research Methods</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/user-testing/index.php">User Testing </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/web-usability/index.php">Web Usability </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/writing-web/index.php">Web Writing &amp; Content Strategy </a></li>
</ul>
</div>

<p>&nbsp;</p>

                
            </section>
        </div>

        <div class="column small-12 medium-6">
            <div class="row">
                <div class="column small-6">
                    <section class="footer-about">
                        <h1>Follow Us</h1>
                        <ul>
	<li><a href="../subscribe/index.php">Subscribe to our weekly UX newsletter</a>,&nbsp;Jakob Nielsen&#39;s Alertbox</li>
	<li>Visit Don Norman&#39;s website, <a href="http://www.jnd.org/"> www.jnd.org </a></li>
	<li>Visit Bruce Tognazzini&#39;s website, <a href="http://www.asktog.com/"> www.asktog.com </a></li>
	<li style="padding-top: 6px;"><a class="twitter-follow-button" data-show-count="false" data-size="large" href="https://twitter.com/nngroup"><img alt="Follow @nngroup on Twitter" height="20" src="https://media.nngroup.com/media/editor/2016/12/30/twitter-follow-button.png" style="width: 117px; height: 20px;" width="117" /></a></li>
	<li style="padding-top: 6px;"><img alt="" height="14" src="https://media.nngroup.com/media/editor/2016/07/26/feed-icon-14x14.png" style="vertical-align:text-top;width:14px;margin-right:.25em;" width="14" /><a href="../../feed/rss/index.php">Subscribe to our RSS feed</a></li>
</ul>

                    </section>
                </div>

                <div class="column small-6">
                    <section class="footer-about right-column">
                        <h1>About
</h1>
                        <ul class="no-bullet">
	<li><a href="../../about/contact/index.php">Contact information</a></li>
	<li><a href="../../about/index.php">About Us</a></li>
	<li><a href="../../about/why-nng/index.php">Why NN/g? </a></li>
	<li><a href="../../return-policy/index.php">Return Policy</a></li>
</ul>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="copyright column small-12">
            <p><a href="../../copyright-and-reprint-info/index.php">Copyright</a> &copy; 1998-2017 Nielsen Norman Group, All Rights Reserved.</p>
        </div>
    </div>
</footer>


    


        <script src="https://media.nngroup.com/static/vendor/fastclick/js/fastclick.min.js"></script>
        <script src="https://media.nngroup.com/static/vendor/foundation/js/foundation.min.js"></script>
        <script src="http://media.nngroup.com/static/js/app.js"></script>
        <script src="http://media.nngroup.com/static/js/scroll-to-fixed.js"></script><script>var externalLinks = document.querySelectorAll('a[href^="http"]');for (var i = externalLinks.length-1; i >= 0; i--) {externalLinks[i].addEventListener("click", function(event) { alert("External links are disabled for this test."); event.preventDefault(); }, false);}var mailLinks = document.querySelectorAll('a[href^="mailto"]');for (var i = mailLinks.length-1; i >= 0; i--) {mailLinks[i].addEventListener("click", function(event) { alert("Email links are disabled for this test."); event.preventDefault(); }, false);}var externalForms = document.querySelectorAll('form');for (var i = externalForms.length-1; i >= 0; i--) {externalForms[i].addEventListener("submit", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false); externalForms[i].addEventListener("click", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false);}</script>

        <script>
            
            $(function() {
                $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
                $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
            });
        </script>

        
<script src="https://media.nngroup.com/static/js/articles.js"></script>


        
        <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1072699960;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072699960/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
    </body>

<!-- Mirrored from www.nngroup.com/articles/coping-information-overload/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:13:20 GMT -->
</html>
