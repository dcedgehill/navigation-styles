<!doctype html>
<html id="doc" class="no-js">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="/apps/navigation_styles/app/app.js"></script>
    <script>
        window.onclick = function() { addCoordinates() };
        window.addEventListener("touchstart", function() { addCoordinates() }, false);
    </script>
    <link href="/apps/navigation_styles/navigations/5/assets/style.css" rel="stylesheet"/>
    <script type="text/javascript" src="/apps/navigation_styles/navigations/5/assets/scripts.js"></script>
</head>
<body>
<div id="myNav" class="overlay">
    <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
    <div class="overlay-content">
        <a href="/apps/navigation_styles/navigations/5/index.php">Home</a>
        <a href="/apps/navigation_styles/navigations/5/reports/index.php">Reports</a>
        <a href="/apps/navigation_styles/navigations/5/articles/index.php">Articles</a>
        <a href="/apps/navigation_styles/navigations/5/training/index.php">Training</a>
        <a href="/apps/navigation_styles/navigations/5/consulting/index.php">Consulting</a>
        <a href="/apps/navigation_styles/navigations/5/about/index.php">About NN/G</a>
    </div>
</div>
</body>
</html>