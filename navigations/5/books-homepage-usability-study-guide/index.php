<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->





    
<!-- Mirrored from www.nngroup.com/books-homepage-usability-study-guide/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:13:19 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1026.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"licenseKey":"1a30ef9931","errorBeacon":"bam.nr-data.net","applicationID":"2048992","transactionName":"NFxSMRBUX0EDUkIPDA0WdhAMVkVbDV8ZFgIEXFIMFkYfRAtUQRVZM1hXADRcVEVMVlMS","beacon":"bam.nr-data.net","queueTime":1,"applicationTime":128,"agent":""}</script>
        <title>Study Guide for &#39;Homepage Usability&#39; Book</title><meta property="og:title" content="Study Guide for &#39;Homepage Usability&#39; Book" />
  
        
        <meta name="description" content="Four exercises for use together with the book Homepage Usability by Jakob Nielsen. Intended for corporate reading groups and individual study.">
        <meta property="og:description" content="Four exercises for use together with the book Homepage Usability by Jakob Nielsen. Intended for corporate reading groups and individual study." />
        
  
        
	
        

        

        <meta name="robots" content="noarchive">
        <meta name="viewport" content="width=device-width">

        <link rel="search" type="application/opensearchdescription+xml" title="NN/g Search" href="../opensearch.xml" />
        <link rel="shortcut icon" href="http://media.nngroup.com/static/img/favicon.ico" />

        <link rel="stylesheet" href="https://media.nngroup.com/static/vendor/font-awesome/css/font-awesome.min.css">

        
        <link rel="stylesheet" href="https://media.nngroup.com/static/css/7232a960f1df.css" type="text/css" />
        <link rel="stylesheet" href="http://fast.fonts.com/t/1.css?apiType=css&amp;projectid=c5846260-9def-46ec-9407-0f1349bd2d64" type="text/css">

        <script src="https://media.nngroup.com/static/vendor/modernizr/js/modernizr.min.js"></script>

        <script type="text/javascript" src="https://media.nngroup.com/static/js/3283be5def74.js"></script>
        

        

        
        
        <!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=229034,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('http://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->


<!-- Google Analytics Tracking Code for http://nngroup.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23132681-1', 'nngroup.com');
  ga('require', 'linkid', 'https://www.nngroup.com/books-homepage-usability-study-guide/linkid.js');
  ga('require', 'displayfeatures');

// Optimizely Universal Analytics Integration
 window.optimizely = window.optimizely || [];
 window.optimizely.push("activateUniversalAnalytics");

  ga('send', 'pageview');

</script>
<script>
/**
* Function to track a click on an outbound link in Google Analytics.
*/
function trackOutboundLink(label, url) {
    if(window.ga && ga.loaded) {
        ga('send', 'event', 'outbound', 'click', label, {'hitCallback': function(){document.location = url;}
        });
    } else {
        document.location = url;
    }
}
/**
* Function to track a click on a link in the article announcement area.
*/
$('#articleAnnouncement a').on('click', function(){
  ga('send', 'event', 'link', 'click', 'articleAnnouncement');
});
</script>

<script type="text/javascript">
 setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2421.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



<!-- Hotjar Tracking Code for http://nngroup.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:121605,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

        <script src="https://media.nngroup.com/static/js/analytics/track_outbound_link.js"></script>
        
        
        
    </head>
    <body class="location-about">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
    <header id="page-header">
        <div id="cart" class="shopping-cart mobile-cart show-for-small-down"></div>
        <div class="row">
            <div id="masthead" class="masthead small-12 medium-7 column">

                <!-- The button to toggles menu on small screen -->

                <h1><a href="../index.php"><img src="http://media.nngroup.com/static/img/logo-wordmark.png" width="74" height="30"> Nielsen <span class="norman">Norman</span> Group</a></h1>
                <p class="tagline show-for-medium-up">Evidence-Based User Experience Research, Training, and Consulting</p>

                <!-- The button to toggles form search on small screen -->


                <div style="clear: right;width:  100%;height: 1px;"></div><?php define('ROOT_PATH',$_SERVER["DOCUMENT_ROOT"] . '/apps/navigation_styles/navigations/5'); ?><?php include(ROOT_PATH)."/navigation.php"; ?>

                <!-- Drop down menu on small screen -->
                <div id="hideNav" class="show-for-small-down">
                  
                  <ul class="menu-content">
                    <li><a href="../index.php">Home</a></li>
                    <li class="has-submenu">
                      <a>Training</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../training/index.php">Overview</a></li>
                        
<li id="nav-ux-conference"><a href="../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../online-seminars/index.php"><span>Online Seminars</span></a></li>

                      </ul>
                    </li>
                    <li class="has-submenu">
                      <a>Consulting</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
  <li><a href="../consulting/index.php">Overview</a></li>
  
  <li><a href="../consulting/benchmarking/index.php">Benchmarking</a></li>
  
  <li><a href="../consulting/expert-design-review/index.php">Expert Design Review</a></li>
  
  <li><a href="../consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
  
  <li><a href="../consulting/usability-testing/index.php">Usability Testing </a></li>
  
  <li><a href="../consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
  
  <li><a href="../in-house-training/index.php">In-House Training</a></li>
</ul>

                    </li>

                    <li><a href="../reports/index.php">Reports</a></li>
                    <li><a href="../articles/index.php">Articles</a></li>
                    <li class="has-submenu"><a>About NN/G</a><i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../about/index.php">Overview</a></li>
                        <li><a href="../people/index.php">People</a></li>
                        <li><a href="../about/why-nng/index.php">Why NN/g?</a></li>
                        <li><a href="../about/contact/index.php">Contact</a></li>
                        <li><a href="../news/index.php">News</a></li>
                        <li><a href="../about/history/index.php">History</a></li>
                        <li><a href="../books/index.php">Books</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                </div>

                <hr class="header-divider show-for-small-only"/>


            </div>
       </div>

        <div id="hideNav">            <nav class="primary-nav small-12 column">
                <ul>
                    <li><a class="navitem" id="nav-home" href="../index.php">Home</a></li>
                    <li><a class="navitem" id="nav-training" href="../training/index.php">Training</a></li>
                    <li><a class="navitem" id="nav-consulting" href="../consulting/index.php">Consulting</a></li>
                    <li><a class="navitem" id="nav-reports" href="../reports/index.php">Reports</a></li>
                    <li><a class="navitem" id="nav-articles" href="../articles/index.php">Articles</a></li>
                    <li><a class="navitem" id="nav-about" href="../about/index.php">About NN/G</a></li>
                </ul>
            </nav>
            <nav class="secondary-nav small-12 medium-12 large-12 columns">
                
                
            </nav>
        </div>
    </header>
    <div class="l-content"><span id="preventClose" style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776; Menu</span>
    
<div class="row">
    <div class="small-12 column l-subsection">
        <h1>
 Study Guide for the Homepage Usability Book
</h1>
<p>
 Many companies run reading groups or study groups about usability or web design, where team members meet to discuss the lessons from books like
 <a href="../books/homepage-usability/index.php">
  Homepage Usability: 50 Websites Deconstructed
 </a>
 . The following four exercises will work for home study (i.e., just yourself), but they are mainly targeted at design teams that want to apply the lessons from the book to their own project.
 <br />
 The page numbers in the following refer to the English edition of the book. If you have a translated edition, the guidelines and statistics recommendations may be located on different pages.
</p>
<p>
 <span style="font-size : large;">
  Exercise 1: Evaluate a Redesigned Homepage
 </span>
</p>
<p style="margin-left: 40px;">
 BBC released a redesign of their homepage after we published the book. Compare the redesigned homepage with the original picture and commentary in Homepage Usability and evaluate how many usability problems BBC solved in the new design.
</p>
<p style="margin-left: 40px;">
 Calculate two scores: the percentage of good features retained and the percentage of usability problems fixed in the design.
</p>
<p style="margin-left: 40px;">
 <strong>
  Instructions
 </strong>
</p>
<ol>
 <li>
  Compare the following redesigned homepage for the BBC website to the BBC homepage and commentary in the
  <a href="../books/homepage-usability/index.php">
   book
  </a>
  .
 </li>
 <li>
  For each
  <strong>
   positive design element
  </strong>
  that the redesign
  <strong>
   retains
  </strong>
  , score the design one point towards the positive percentage. If they keep only part of the positive feature, count it as half a point. See &quot;Tips&quot; below for list of positive comments.
 </li>
 <li>
  For each
  <strong>
   usability problem
  </strong>
  that the redesign
  <strong>
   fixes
  </strong>
  , score one point toward the usability issues fixed. If they only partially fixed the problem, count half a point towards the total. See &quot;Tips&quot; below for the list of usability issues.
 </li>
 <li>
  Add up the points for positive features retained in the design. Divide the total by 8 (total possible points). This gives you the percentage of good features kept in the design.
 </li>
 <li>
  Add up the points for usability problems fixed in the redesign. Divide the total by 28 (total possible usability issues). This gives you the percentage of usability problems fixed in the design.
 </li>
 <li>
  Finally, compare your scores with our solution. (Please do not follow this link until after you have scored the redesign on your own.)
 </li>
</ol>
<p style="margin-left: 40px;">
 <strong>
  Tips
 </strong>
</p>
<p style="margin-left: 40px;">
 Some comments have both negative and positive remarks, as outlined below:
</p>
<ul>
 <li style="margin-bottom: 0.5em">
  <strong>
   Positive aspects. 8 possible points for keeping these design goodies in the redesign:
  </strong>
  <br />
  Window title, red numbered comments 1, 8, 9, 17, 19, 23, and 24.
 </li>
 <li>
  <strong>
   Usability problems. 28 possible points for fixing these design flaws in the redesign:
  </strong>
  <br />
  Window title, tagline, red numbered comments 2, 3, 4, 5, 6 (2 points possible), 7, 8, 10, 11, 12, 13, 14, 15, 16, 18, 20, 21, 22, 23, 24, 25, 26, 27, 28, and 29.
 </li>
</ul>
<p style="margin-left: 40px;">
 The numbers refer to the red &quot;bubbles&quot; in the analysis of the old www.bbc.co.uk homepage in
 <cite>
  Homepage Usability
 </cite>
 (shown on pages 84 and 86 in the English edition).
</p>
<p>
 &nbsp;
</p>
<p style="margin-left: 40px;">
 <strong>
  Screenshot of the redesigned BBC Homepage
 </strong>
</p>
<p style="margin-left: 40px;">
 Please compute your scores based on the following screen capture, which we used in deriving our solution. Of course, you can also look at the live
 <a href="http://www.bbc.co.uk/">
  www.bbc.co.uk
 </a>
 site, but you should score the screenshot below, in order to best compare your score with our results.
</p>
<p style="margin-left: 40px;">
 <img alt="Screenshot of the redesigned BBC homepage" src="http://media.nngroup.com.s3.amazonaws.com/media/editor/2012/11/27/bbc_new.gif" style="width: 772px; height: 1075px;" />
</p>
<p style="margin-left: 40px;">
 <a href="../books-homepage-usability-study-guide-solution/index.php">
  Solution to Exercise 1
 </a>
 (please do not peek - don&#39;t follow this link until after you have scored the new BBC page)
</p>
<p style="margin-left: 40px;">
 Even though the BBC changed their design, the original homepage analysis in the book is still of interest. Many of the other websites represented in the book are probably going to take advantage of getting free consulting and will probably revise their homepages as well. That is OK, because the goal of the homepage analyses was not to provide recommendations to 50 design teams. When we write design recommendations, we do so in a different style. The goal of the book is to learn about homepage design by taking a detailed look at a set of case studies. So it&#39;s just an additional benefit when readers get the opportunity to compare the before and after versions of the pages, and thus see which of the usability problems have been fixed.
</p>
<p>
 &nbsp;
</p>
<p>
 <span style="font-size : large;">
  Exercise 2. Use guidelines to evaluate other homepages
 </span>
</p>
<ol>
 <li>
  Choose a homepage (other than your own) to use for this exercise. You might want to try evaluating a competitor&#39;s homepage, a site with similar business goals, or a homepage from a popular website that interests you.
 </li>
 <li>
  Take a screenshot of the homepage, to ensure that you evaluate the same design for all of the guidelines. Capturing the page is necessary, because many sites update their content frequently.
 </li>
 <li>
  Evaluate whether or not the homepage follows the homepage guidelines (pages 10 to 35 in the English edition). For each guideline, give the site one point if it follows the guideline, or half a point if it only partially follows the guideline.
 </li>
 <li>
  Add up all of the points.
 </li>
 <li>
  Divide the total points by 113 (the total number of guidelines). This will give you the percentage of the guidelines that the homepage follows.
  <br />
  &nbsp;
 </li>
</ol>
<p style="margin-left: 40px;">
 <strong>
  Tips
 </strong>
</p>
<ul>
 <li>
  Although you should use your screenshot of the homepage for the exercise, you&#39;ll probably need to have the live site available, in order to check some links or functionality.
 </li>
 <li>
  Limit your scoring to the homepage, even if you need to go to other pages in the design in order to evaluate it. For example, a site might have a section with information about the company, but not have any link to it on the homepage, and so shouldn&#39;t get any points for that guideline.
 </li>
 <li>
  It may be that some of the guidelines don&#39;t apply to the homepage you are evaluating. For example, the guidelines for presenting stock quotes would not apply to a page without stock quotes (for example, a homepage for a government agency). If you are certain that a certain guideline doesn&#39;t apply, then you should not add a score for that guideline. In such cases, you would use a smaller number than 113 as the divisor in step 5: you would divide by the number of guidelines scored, not counting those that do not apply.
 </li>
 <li>
  Reading groups can do this exercise in a variety of ways. Groups can evaluate the sites together, but we recommend that each member do the exercise independently, and then get together to compare and discuss the scores and rationale as a group activity.
 </li>
</ul>
<p>
 &nbsp;
</p>
<p>
 <span style="font-size : large;">
  Exercise 3. Evaluate your own homepage&#39;s guidelines compliance
 </span>
</p>
<p style="margin-left: 40px;">
 Use the procedure in Exercise 2 to evaluate how closely your company&#39;s own homepage follows the homepage guidelines.
</p>
<p>
 &nbsp;
</p>
<p>
 <span style="font-size : large;">
  Exercise 4. Evaluate how close you are to the &quot;recommended homepage design&quot;
 </span>
</p>
<ol>
 <li>
  Evaluate how many of the recommendations your homepage follows from the table at the end of the statistics chapter (pages 52 and 53 in the English edition), using the following procedure:
 </li>
 <li>
  For each recommendation in the table that your site follows, give yourself points equal to the number of stars in the &quot;Strength of Recommendation&quot; column. For example, if you have your logo in the upper left corner, you get three points.
 </li>
 <li>
  Add up all of the points.
 </li>
 <li>
  Divide the total points by 85 (highest possible score). This will give you an estimate of how close your homepage is to the ideal design, in terms of what percentage of the recommendations you follow.
 </li>
</ol>
<p>
 &nbsp;
</p>

    </div>
</div>

    </div>
    




<footer id="page-footer">
    <div class="row hide-for-print">
        <div class="column small-12 medium-6">
            <section class="footer-popular">
                
                <h1>Popular Topics</h1>
                <div style="float: left;  width: 15em; display: block;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../topic/applications/index.php">Application Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../topic/e-commerce/index.php">E-Commerce Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../topic/eyetracking/index.php">Eyetracking </a></li>
	<li style="padding-bottom: .5em;"><a href="../topic/human-computer-interaction/index.php">Human Computer Interaction </a></li>
	<li style="padding-bottom: .5em;"><a href="../topic/information-architecture/index.php">Information Architecture </a></li>
	<li style="padding-bottom: .5em;"><a href="../topic/intranets/index.php">Intranet Design </a></li>
	<li><a href="../topic/interaction-design/index.php">Interaction Design </a></li>
</ul>
</div>

<div style="float: left;  width: 15em; display: block; list-style: none;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../topic/mobile-and-tablet-design/index.php">Mobile and Tablet Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../topic/credibility-persuasion/index.php">Persuasive Design</a></li>
	<li style="padding-bottom: .5em;"><a href="../topic/research-methods/index.php">Research Methods</a></li>
	<li style="padding-bottom: .5em;"><a href="../topic/user-testing/index.php">User Testing </a></li>
	<li style="padding-bottom: .5em;"><a href="../topic/web-usability/index.php">Web Usability </a></li>
	<li style="padding-bottom: .5em;"><a href="../topic/writing-web/index.php">Web Writing &amp; Content Strategy </a></li>
</ul>
</div>

<p>&nbsp;</p>

                
            </section>
        </div>

        <div class="column small-12 medium-6">
            <div class="row">
                <div class="column small-6">
                    <section class="footer-about">
                        <h1>Follow Us</h1>
                        <ul>
	<li><a href="../articles/subscribe/index.php">Subscribe to our weekly UX newsletter</a>,&nbsp;Jakob Nielsen&#39;s Alertbox</li>
	<li>Visit Don Norman&#39;s website, <a href="http://www.jnd.org/"> www.jnd.org </a></li>
	<li>Visit Bruce Tognazzini&#39;s website, <a href="http://www.asktog.com/"> www.asktog.com </a></li>
	<li style="padding-top: 6px;"><a class="twitter-follow-button" data-show-count="false" data-size="large" href="https://twitter.com/nngroup"><img alt="Follow @nngroup on Twitter" height="20" src="https://media.nngroup.com/media/editor/2016/12/30/twitter-follow-button.png" style="width: 117px; height: 20px;" width="117" /></a></li>
	<li style="padding-top: 6px;"><img alt="" height="14" src="https://media.nngroup.com/media/editor/2016/07/26/feed-icon-14x14.png" style="vertical-align:text-top;width:14px;margin-right:.25em;" width="14" /><a href="../feed/rss/index.php">Subscribe to our RSS feed</a></li>
</ul>

                    </section>
                </div>

                <div class="column small-6">
                    <section class="footer-about right-column">
                        <h1>About
</h1>
                        <ul class="no-bullet">
	<li><a href="../about/contact/index.php">Contact information</a></li>
	<li><a href="../about/index.php">About Us</a></li>
	<li><a href="../about/why-nng/index.php">Why NN/g? </a></li>
	<li><a href="../return-policy/index.php">Return Policy</a></li>
</ul>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="copyright column small-12">
            <p><a href="../copyright-and-reprint-info/index.php">Copyright</a> &copy; 1998-2017 Nielsen Norman Group, All Rights Reserved.</p>
        </div>
    </div>
</footer>


    


        <script src="https://media.nngroup.com/static/vendor/fastclick/js/fastclick.min.js"></script>
        <script src="https://media.nngroup.com/static/vendor/foundation/js/foundation.min.js"></script>
        <script src="http://media.nngroup.com/static/js/app.js"></script>
        <script src="http://media.nngroup.com/static/js/scroll-to-fixed.js"></script><script>var externalLinks = document.querySelectorAll('a[href^="http"]');for (var i = externalLinks.length-1; i >= 0; i--) {externalLinks[i].addEventListener("click", function(event) { alert("External links are disabled for this test."); event.preventDefault(); }, false);}var mailLinks = document.querySelectorAll('a[href^="mailto"]');for (var i = mailLinks.length-1; i >= 0; i--) {mailLinks[i].addEventListener("click", function(event) { alert("Email links are disabled for this test."); event.preventDefault(); }, false);}var externalForms = document.querySelectorAll('form');for (var i = externalForms.length-1; i >= 0; i--) {externalForms[i].addEventListener("submit", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false); externalForms[i].addEventListener("click", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false);}</script>

        <script>
            
            $(function() {
                $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
                $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
            });
        </script>

        


        
        <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1072699960;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072699960/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
    </body>

<!-- Mirrored from www.nngroup.com/books-homepage-usability-study-guide/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:13:19 GMT -->
</html>
