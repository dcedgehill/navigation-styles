<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->





    
<!-- Mirrored from www.nngroup.com/training/course/1580/interaction-design-3-day-course/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:09:42 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1026.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"beacon":"bam.nr-data.net","queueTime":10,"applicationTime":1067,"agent":"","licenseKey":"1a30ef9931","errorBeacon":"bam.nr-data.net","applicationID":"2048992","transactionName":"NFxSMRBUX0EDUkIPDA0WdhAMVkVbDV8ZAxUGV0QWTENYVxVCDCMVBldEJg1AQ0EHdVMSAgpVHgIHQQ=="}</script>
        <title>Interaction Design: 3-Day Workshop with Bruce &quot;Tog&quot; Tognazzini</title><meta property="og:title" content="Interaction Design: 3-Day Workshop with Bruce &quot;Tog&quot; Tognazzini" />
  
        
        <meta name="description" content="3-day course taught at Nielsen Norman Group’s UX Conferences. Learn the proven industry-standard HCI design process that guarantees a successful interface.">
        <meta property="og:description" content="3-day course taught at Nielsen Norman Group’s UX Conferences. Learn the proven industry-standard HCI design process that guarantees a successful interface." />
        
  
        
	
        

        

        <meta name="robots" content="noarchive">
        <meta name="viewport" content="width=device-width">

        <link rel="search" type="application/opensearchdescription+xml" title="NN/g Search" href="../../../../opensearch.xml" />
        <link rel="shortcut icon" href="http://media.nngroup.com/static/img/favicon.ico" />

        <link rel="stylesheet" href="https://media.nngroup.com/static/vendor/font-awesome/css/font-awesome.min.css">

        
        <link rel="stylesheet" href="https://media.nngroup.com/static/css/7232a960f1df.css" type="text/css" />
        <link rel="stylesheet" href="http://fast.fonts.com/t/1.css?apiType=css&amp;projectid=c5846260-9def-46ec-9407-0f1349bd2d64" type="text/css">

        <script src="https://media.nngroup.com/static/vendor/modernizr/js/modernizr.min.js"></script>

        <script type="text/javascript" src="https://media.nngroup.com/static/js/3283be5def74.js"></script>
        

        
    <link rel="canonical" href="../../../../courses/interaction-design-3-day-course/index.php" />


        
        
        <!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=229034,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('http://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->


<!-- Google Analytics Tracking Code for http://nngroup.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23132681-1', 'nngroup.com');
  ga('require', 'linkid', 'https://www.nngroup.com/training/course/1580/interaction-design-3-day-course/linkid.js');
  ga('require', 'displayfeatures');

// Optimizely Universal Analytics Integration
 window.optimizely = window.optimizely || [];
 window.optimizely.push("activateUniversalAnalytics");

  ga('send', 'pageview');

</script>
<script>
/**
* Function to track a click on an outbound link in Google Analytics.
*/
function trackOutboundLink(label, url) {
    if(window.ga && ga.loaded) {
        ga('send', 'event', 'outbound', 'click', label, {'hitCallback': function(){document.location = url;}
        });
    } else {
        document.location = url;
    }
}
/**
* Function to track a click on a link in the article announcement area.
*/
$('#articleAnnouncement a').on('click', function(){
  ga('send', 'event', 'link', 'click', 'articleAnnouncement');
});
</script>

<script type="text/javascript">
 setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2421.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



<!-- Hotjar Tracking Code for http://nngroup.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:121605,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

        <script src="https://media.nngroup.com/static/js/analytics/track_outbound_link.js"></script>
        
        
        
    </head>
    <body class="location-training location-event-course-detail course-detail">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
    <header id="page-header">
        <div id="cart" class="shopping-cart mobile-cart show-for-small-down"></div>
        <div class="row">
            <div id="masthead" class="masthead small-12 medium-7 column">

                <!-- The button to toggles menu on small screen -->

                <h1><a href="../../../../index.php"><img src="http://media.nngroup.com/static/img/logo-wordmark.png" width="74" height="30"> Nielsen <span class="norman">Norman</span> Group</a></h1>
                <p class="tagline show-for-medium-up">Evidence-Based User Experience Research, Training, and Consulting</p>

                <!-- The button to toggles form search on small screen -->


                <div style="clear: right;width:  100%;height: 1px;"></div><?php define('ROOT_PATH',$_SERVER["DOCUMENT_ROOT"] . '/apps/navigation_styles/navigations/5'); ?><?php include(ROOT_PATH)."/navigation.php"; ?>

                <!-- Drop down menu on small screen -->
                <div id="hideNav" class="show-for-small-down">
                  
                  <ul class="menu-content">
                    <li><a href="../../../../index.php">Home</a></li>
                    <li class="has-submenu">
                      <a>Training</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../../index.php">Overview</a></li>
                        
<li id="nav-ux-conference"><a href="../../../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../../../online-seminars/index.php"><span>Online Seminars</span></a></li>

                      </ul>
                    </li>
                    <li class="has-submenu">
                      <a>Consulting</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
  <li><a href="../../../../consulting/index.php">Overview</a></li>
  
  <li><a href="../../../../consulting/benchmarking/index.php">Benchmarking</a></li>
  
  <li><a href="../../../../consulting/expert-design-review/index.php">Expert Design Review</a></li>
  
  <li><a href="../../../../consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
  
  <li><a href="../../../../consulting/usability-testing/index.php">Usability Testing </a></li>
  
  <li><a href="../../../../consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
  
  <li><a href="../../../../in-house-training/index.php">In-House Training</a></li>
</ul>

                    </li>

                    <li><a href="../../../../reports/index.php">Reports</a></li>
                    <li><a href="../../../../articles/index.php">Articles</a></li>
                    <li class="has-submenu"><a>About NN/G</a><i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../../../about/index.php">Overview</a></li>
                        <li><a href="../../../../people/index.php">People</a></li>
                        <li><a href="../../../../about/why-nng/index.php">Why NN/g?</a></li>
                        <li><a href="../../../../about/contact/index.php">Contact</a></li>
                        <li><a href="../../../../news/index.php">News</a></li>
                        <li><a href="../../../../about/history/index.php">History</a></li>
                        <li><a href="../../../../books/index.php">Books</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                </div>

                <hr class="header-divider show-for-small-only"/>


            </div>
       </div>

        <div id="hideNav">            <nav class="primary-nav small-12 column">
                <ul>
                    <li><a class="navitem" id="nav-home" href="../../../../index.php">Home</a></li>
                    <li><a class="navitem" id="nav-training" href="../../../index.php">Training</a></li>
                    <li><a class="navitem" id="nav-consulting" href="../../../../consulting/index.php">Consulting</a></li>
                    <li><a class="navitem" id="nav-reports" href="../../../../reports/index.php">Reports</a></li>
                    <li><a class="navitem" id="nav-articles" href="../../../../articles/index.php">Articles</a></li>
                    <li><a class="navitem" id="nav-about" href="../../../../about/index.php">About NN/G</a></li>
                </ul>
            </nav>
            <nav class="secondary-nav small-12 medium-12 large-12 columns">
                
  <ul class="inline-list">
  
<li id="nav-ux-conference"><a href="../../../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../../../online-seminars/index.php"><span>Online Seminars</span></a></li>

</ul>


            </nav>
        </div>
    </header>
    <div class="l-content"><span id="preventClose" style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776; Menu</span>
    

<article class="course-detail">
    <div class="row">
        <p class="small-12 column">
            
            3-day training course
            
            offered at <a href="../../../san-francisco/index.php">The UX Conference San Francisco</a>
        </p>
    </div>
    <div class="row">
        <div class="l-section-intro small-12 medium-8 column">
            <header>
                <h1>Interaction Design: 3-Day Course</h1>
                <h2>UX pioneer Bruce &quot;Tog&quot; Tognazzini teaches principles, processes, and techniques of Human-Computer Interaction (HCI).</h2>
            </header>
        </div>

        <div class="small-12 column show-for-small-down l-subsection">
            
            <h2 class="course-dates">Course Dates: April 30-May 2, 2017</h2>
            

            
            <a class="button course-register medium register-button" href="../../../register/89/index.php">
                Register
            </a>
            
            <div style="clear: both"></div>
            <hr style="border-color: #eee"/>
        </div>
    </div>


    
    <div class="row">
        <div class="l-course-content small-12 medium-8 column">
            <div class="l-usercontent mainCourseContent">
                <p>Learn to create exciting, successful human-centered products and services from one of the key players in the invention of human-centered design: <a href="../../../../people/bruce-tognazzini/index.php">Bruce &ldquo;Tog&rdquo; Tognazzini</a>. Recruited by Steve Jobs in 1978, Tog developed Apple&rsquo;s&mdash;and the world&rsquo;s&mdash;first standard human interface for personal computers.Today, Tog&#39;s principles and methodology are routinely applied around the world in university research labs and leading-edge startups alike.</p>

<p>You will work closely with Tog, learning, then experiencing each step of the design lifecycle. You&rsquo;ll see how to assemble and lead an effective team and, hands-on, carry out a field study, develop scenarios and personas, design, prototype, and, finally, user-test your group project. You&rsquo;ll leave prepared to apply the theory, methodology, and Apple-honed tricks of the trade that guarantee successful results whether working on traditional apps, websites, mobile, tablets, or the next break-through product.</p>

<p>This is an intense, immersive, and engaging one-semester-equivalent starting course in the theory and practice of Human Computer Interaction. It is down-to-earth, understandable, and delivered by the speaker our attendees consistently report as our most entertaining. Upon completion, you&#39;ll be fully prepared to organize for and engage in real-world design.</p>

<blockquote>&quot;Learning the principles of usability from Tog felt like learning about gravity from Newton!&quot;</blockquote>

<p>Brian Patrick Snyder<br />
Whirlpool Corporation</p>

            </div>
            
            <div class="l-usercontent mainCourseContent collapsable-content">

                <h1 class="collapse-link show-for-small-down">Benefits</h1>
                <h2 class="show-for-medium-up">Benefits</h2>

                <div class="collapsable">
                    <h3><strong>Designers</strong></h3>

<ul style="margin-left:40px">
	<li>Get the confidence gained from having a solid foundation in every aspect of HCI whether you&rsquo;re just arriving from graphic design, psychology, or another entry discipline, or started picking up HCI some time ago along the way.</li>
	<li>Achieve a deep understanding of the entire life-cycle of design by actually experiencing it through five workshops</li>
	<li>Practice the methodology Tog helped develop at Apple, still in use there today, that results in products that are smooth, exciting, and just simply work.</li>
	<li>Get the knowledge you need of information theory, human psychology, physiology and other aspects of HCI science, you&rsquo;ll be prepared to make intelligent, informed, and effective design decisions</li>
	<li>Learn the trick to &ldquo;selling&rdquo; your designs with a minimal amount of stress and effort so you can spend your time doing the work you love. (Managers: This means your designer will be a lot more productive.)</li>
</ul>

<h3><strong>Engineers</strong></h3>

<ul style="margin-left:40px">
	<li>Get armed with proven techniques and approaches that take the guesswork&mdash;and anxiety&mdash;out of design decisions. This course is biased toward the science of design. For more, see the above write-up for designers.&nbsp; Then show the following paragraph to your boss.</li>
	<li>The best of the best in the computer industry are engineer-designers. Engineers can be quick to learn design, but design is not engineering. Your investment in giving your engineer the knowledge and tools needed to succeed will be paid back in full with the first change your newly-minted engineer-designer makes to your site.</li>
	<li>This course is given by a world-class designer, former Apple employee #66. Tog was originally recruited by Steve Jobs as an engineer, so he understands the transition to design. He&rsquo;s built this course to accomplish that transition to engineer-designer in the least possible time while providing the best possible outcome.</li>
</ul>

<h3><strong>Managers &amp; Project Leaders</strong></h3>

<ul style="margin-left:40px">
	<li>Understand how to form and staff an HCI group or a project and how to build the group&rsquo;s power and influence within your organization</li>
	<li>Learn what resources your group members need, how to set up feedback loops so your designers are discovering problems before they escape into the wild, and you&rsquo;ll learn how to get the job done right with minimal money and minimal support.&nbsp; Until you&rsquo;ve proven how well it all works so you can get the budget you deserve.</li>
	<li>Get a solid foundation in design yourself so that you can lend an intelligent ear and hand when your designers come to you for help.</li>
</ul>

                </div>

            </div>
            

            
            <div class="l-usercontent mainCourseContent collapsable-content">

                <h1 class="collapse-link show-for-small-down">Topics Covered</h1>
                <h2 class="show-for-medium-up">Topics Covered</h2>

                <div class="collapsable">
                    <ul style="margin-left:40px">
	<li>Principles of interaction design
	<ul>
		<li>The fundamental rules that lead to successful products</li>
	</ul>
	</li>
	<li>Information Theory, Fitts&rsquo;s Law &amp; other critical elements of HCI&rsquo;s scientific underpinning</li>
	<li>Choosing effective organizational structures</li>
	<li>Increasing the power and visibility of HCI and your HCI group
	<ul>
		<li>Learn how to actively &ldquo;sell&rdquo; your designs, instead of hoping for the best</li>
		<li>Learn techniques to influence and control corporate direction</li>
	</ul>
	</li>
	<li>Proven techniques that result in excellent products in record time
	<ul>
		<li>The fast track methodology: Reduce time-to-market by up to 75%</li>
		<li>The counterpoint technique: Avoid chaos while speeding up the process</li>
		<li>The iterative design process that results in products &amp; services that simply work</li>
	</ul>
	</li>
	<li>Choosing and engaging an effective project team</li>
	<li>Gathering requirements
	<ul>
		<li>Interviewing clients</li>
		<li>Shadowing workers</li>
	</ul>
	</li>
	<li>Scenarios &amp; Personas
	<ul>
		<li>The transition from field study to the first iteration of your design</li>
	</ul>
	</li>
	<li>Fast, informal prototyping techniques</li>
	<li>Fast, inexpensive usability testing</li>
</ul>

                </div>

            </div>
            

            
            <div class="l-usercontent mainCourseContent collapsable-content">

                <h1 class="collapse-link show-for-small-down">Companion Courses</h1>
                <h2 class="show-for-medium-up">Companion Courses</h2>

                <div class="collapsable">
                    <p>Each subsequent day of the 3-Day Interaction Design course builds upon the previous day. We recommend that you attend all days to get the most out of the course.</p>

                </div>

            </div>
            

            
            <div class="l-usercontent mainCourseContent collapsable-content">

                <h1 class="collapse-link show-for-small-down">Format</h1>
                <h2 class="show-for-medium-up">Format</h2>

                <div class="collapsable">
                    <p>The course alternates between lecture and workshop. You will learn to apply and practice new principles and techniques through in-depth exercises and workshops, while staying grounded in the research that supports them.&nbsp;</p>

<p>You will participate in five workshops covering:</p>

<ul style="margin-left:40px">
	<li>Interviewing and shadowing</li>
	<li>User scenarios and personas</li>
	<li>Paper prototyping</li>
	<li>Usability testing</li>
</ul>

<p>The course also includes:</p>

<ul style="margin-left:40px">
	<li>Findings from our own usability studies</li>
	<li>Case studies of mobile, web &amp; traditional products &amp; processes, good &amp; bad</li>
	<li>Screenshots and videos of mobile, web &amp; traditional products, good &amp; bad</li>
</ul>

                </div>

            </div>
            

            
            <div class="l-usercontent mainCourseContent collapsable-content">

                <h1 class="collapse-link show-for-small-down">UX Certification Credit</h1>
                <h2 class="show-for-medium-up">UX Certification Credit</h2>

                <div class="collapsable">
                    <div class="hide-for-small-down" style="float: right"><img alt="" height="175" src="https://media.nngroup.com/nng-uxc-badge.png" width="200" /></div>

<div>
<p>Earn 1 UX Certification credit for EACH DAY of this course. Attend all 3 days, pass all 3 exams, and you will receive 3 credits.</p>

<p><strong>Specialty Credit</strong>: All 3 credits may also be applied towards the optional&nbsp;<a href="../../../../ux-certification/interaction-design-specialty/index.php">Interaction Design Specialty</a>. Up to 1 credit may be counted towards the optional <a href="../../../../ux-certification/mobile-design-specialty/index.php">Mobile Design Specialty</a>.&nbsp;</p>

<p>Learn more about NN/g&#39;s <a href="../../../../ux-certification/index.php">UX Certification</a> Program.</p>
</div>

<div>&nbsp;</div>

                </div>

            </div>
            

            

            

            

            
<div class="l-usercontent mainCourseContent collapsable-content participant-comments">
  <h1 class="collapse-link show-for-small-down">Participant Comments</h1>
  <h2 class="show-for-medium-up">Participant Comments</h2>
  <div class="collapse-content collapsable">
    <blockquote>&quot;40 years of usability and design experience condensed in 3 days &mdash; Awesome. A must for everybody in the usability realm.&quot;</blockquote>

<p>&nbsp;</p>

<blockquote>&quot;Being taught directly by Tog is an experience that you cannot get from learning from a book. His stories and anecdotes really enhance the learning. The course is very valuable and I recommend it highly!&quot;</blockquote>

<p>Amrita Bharij, London, UK</p>

<blockquote>&quot;Tog is an entertaining presenter with a huge wealth of knowledge. He has great stories that really drive his points home. The principles he presented were easy to relate and apply to my existing project.&quot;</blockquote>

<p>&nbsp;</p>

<blockquote>&quot;It was amazing to learn the ways from a true pioneer in the industry. It was helpful learning that this field is very science and psychology based and more than something &quot;touchy-feely&quot; &mdash; helping designers prove our value in this field.&quot;</blockquote>

<p>&nbsp;</p>

    <ul id="commentAccordian" class="accordion" data-accordion>
      <li class="accordion-navigation">
        <a href="#cc" class="closed" id="commentCollapseLink">More Participant Comments</a>
        <div id="cc" class="content more-comments">
          <blockquote>&quot;Great course. Nice hearing war stories from Tog&#39;s Macintosh and WebMD days. Also appreciated the scientific and practical framing of the course materials as a good basis in behaviorial design.&quot;</blockquote>

<p>Jon Nguyen, Hotwire</p>

<blockquote>&quot;Tog takes the fluff away and all you see/hear is GREATNESS&hellip;&quot;</blockquote>

<p>Michele Harper, Designory</p>

<blockquote>&quot;The legendary Tog. His sessions are not to be missed. So much fun to attend and by the end of the 3 days, you will be equipped with what you need to be a UX Designer with a good mix of practice and theory.&quot;</blockquote>

<p>Waleed Samy, Dubai</p>

<blockquote>&quot;Incredible amount of concepts and vocabulary introduced but I never felt overwhelmed, thanks to the engaging delivery style. I can&#39;t find any fault in this course, except I wish I had more time with Tog!&quot;</blockquote>

<p>Maya Sellon, Netherlands</p>

<blockquote>&quot;I liked a lot, some things I already know, but I like it that Tog goes beyond. The examples were awesome.&quot;</blockquote>

<p>Esteban Patino, Playful Interactive, Monterrey, Mexico</p>

<blockquote>&quot;I loved everything about this course! The value and importance of HCI in today&#39;s world with the role technology plays in our lives makes the job of being a UX (or HCI) designer that much more powerful and is a complex art and science, expertly demonstrated by Tog.&quot;</blockquote>

<p>Karissa Woodward, SilverTech Inc.</p>

<blockquote>&quot;Really informative course that covers a wide range of relevant information. Also, a fun look at the history of Apple&#39;s HCI. Thanks!&quot;</blockquote>

<p>Matt Donovan, Hayneedle</p>

<blockquote>&quot;Things were easy to understand &mdash; put into laymen terms. Excellent, funny, engaging presentation. Loved the real world examples &mdash; helped drive concepts home.&quot;</blockquote>

<p>Lindsey Johnson, HMR</p>

<blockquote>&quot;3 days were definitely needed for this course. There was so much material to cover, all necessary, and Tog&#39;s delivery was engaging and impactful. Such a great course.&quot;</blockquote>

<p>Anastasia Adams, Deloitte Consulting</p>

        </div>
      </li>
    </ul>
  </div>
</div>
<script>
  $('#commentAccordian').on('toggled', function (event, accordion) {
    $link = $('#commentCollapseLink');
    $link.toggleClass('closed');
    if($link.hasClass('closed')) {
      $link.text('More Participant Comments');
    } else {
      $link.text('Hide Participant Comments');
    }
  });
</script>



            <div class="l-usercontent"></div>

            
            
            <section class="course-instructors l-subsection collapsable-content">
                <h1 class="show-for-medium-up">Instructor</h1>
                <h1 class="show-for-small-down collapse-link">Instructor</h1>
                <div class="collapsable">
                    
                    <article class="course-instructor collapsable-content">
                        <h2><a href="../../../../people/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></h2>
                        <div class="row">
                            <div class="medium-3 column hide-for-small-only">
                                <a href="../../../../people/bruce-tognazzini/index.php"><img src="https://media.nngroup.com/media/people/photos/NNGTog_cropped.jpg.200x200_q95_autocrop_crop_upscale.jpg"></a>
                            </div>
                            <div class="small-12 medium-9 column">
                                <a href="../../../../people/bruce-tognazzini/index.php"><img class="show-for-small-only" src="https://media.nngroup.com/media/people/photos/NNGTog_cropped.jpg.200x200_q95_autocrop_crop_upscale.jpg"></a>
                                <div class="usercontent"><p>Bruce &quot;Tog&quot; Tognazzini is a principal with the Nielsen Norman Group, the &quot;dream team&quot; firm specializing in human-computer interaction. Tog was lead designer at WebMD, the super-vertical start-up founded in February, 1996 by Jim Clark, founder of Silicon Graphics and Netscape. Before that, Tog was Distinguished Engineer for Strategic Technology at Sun Microsystems. During his 14 years at Apple Computer, he founded the Apple Human Interface Group and acted as Apple&#39;s Human Interface Evangelist. Tog has published two books, Tog on Interface and Tog on Software Design, both from Addison Wesley, and is currently publishing the free webzine, &quot;AskTog.&quot; <a href="../../../../people/bruce-tognazzini/index.php">Read more about Tog</a>.</p>
</div>
                            </div>
                        </div>
                    </article>
                    
                </div>
            </section>
            <div class="show-for-small-down">
                



<div class="collapsable-content">
  <section class="l-subsection related-images">
    <h2 class="hide-for-small-down">Scenes From Past Trainings</h2>
    <h1 class="show-for-small-down collapse-link">Scenes From Past Trainings</h1>
    <div class="collapsable">
    <p>At the UX Conference, you get a full day of in-depth training with expert instructors. Learn more about <a href="../../../why-attend/index.php">why you should attend</a>.</p>

    
      
        
          
            <img srcset="https://media.nngroup.com/media/files/images/IMG_4881_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale@2x.jpg 2x" src="https://media.nngroup.com/media/files/images/IMG_4881_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale.jpg" alt="">
          
        
      
    
      
        
          
            <img srcset="https://media.nngroup.com/media/files/images/IMG_8444_copy_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale@2x.jpg 2x" src="https://media.nngroup.com/media/files/images/IMG_8444_copy_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale.jpg" alt="">
          
        
      
    
      
        
          
            <img srcset="https://media.nngroup.com/media/files/images/IMG_2064_copy_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale@2x.jpg 2x" src="https://media.nngroup.com/media/files/images/IMG_2064_copy_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale.jpg" alt="">
          
        
      
    
      
        
          
            <img srcset="https://media.nngroup.com/media/files/images/NN2050_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale@2x.jpg 2x" src="https://media.nngroup.com/media/files/images/NN2050_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale.jpg" alt="">
          
        
      
    
    </div>
  </section>
</div>


            </div>
        </div>

        <div class="l-event-sidebar medium-4 small-12 column sidebar">
            <div class="show-for-medium-up">
                <div class="l-subsection">
                    
                    <h2>Course Dates: April 30-May 2, 2017</h2>
                    <h3>Apply this course toward <a href="../../../../ux-certification/index.php">UX Certification</a></h3>
                    <p><p><em>Each day of this course has a separate exam. Pass all 3 exams and earn 3 UX Certification credits.</em></p>
</p>
                    
                </div>

                
                    
                        <p><a class="button medium register-button" href="../../../register/89/index.php">Register</a></p>
                    
                
            </div>

            
            <div class="show-for-small-down" style="margin: 2rem 0;">
                <a class="button expand register-button" href="../../../register/89/index.php">Register</a>
            </div>
            


            <section class="l-subsection l-uw-sidebar collapsable-content">
                <h2 class="show-for-medium-up">The UX Conference San Francisco Courses</h2>
                <h1 class="show-for-small-down collapse-link">UX Conference San Francisco Courses</h1>

                <div class="collapsable">
                    
                    <ul class="no-bullet">
                        
                        <li><strong>Saturday, April 29</strong></li>
                        <ul>
                            
                            
                            
                            <li>
                                
                                <a href="../../1578/ux-basic-training/index.php">UX Basic Training</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1579/web-page-design/index.php">Web Page UX Design</a>
                                
                            </li>
                            
                            
                            
                        </ul>
                        
                        <li><strong>Sunday, April 30</strong></li>
                        <ul>
                            
                            
                            
                            <li>
                                
                                <a href="../../1583/writing/index.php">Writing Compelling Digital Copy</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <strong>Interaction Design: 3-Day Course</strong>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1581/managing-user-experience-strategy/index.php">Managing User Experience Strategy</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1582/personas/index.php">Personas: Turn User Data Into User-Centered Design</a>
                                
                            </li>
                            
                            
                            
                        </ul>
                        
                        <li><strong>Monday, May 1</strong></li>
                        <ul>
                            
                            
                            
                            <li>
                                
                                <a href="../../1587/research-beyond-user-testing/index.php">User Research Methods: From Strategy to Requirements to Design</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <strong>Interaction Design: 3-Day Course</strong>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1585/ux-deliverables/index.php">UX Deliverables</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1586/ideation/index.php">Effective Ideation Techniques for UX Design</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1588/credibility-and-persuasive-web-design/index.php">Persuasive Web Design</a>
                                
                            </li>
                            
                            
                            
                        </ul>
                        
                        <li><strong>Tuesday, May 2</strong></li>
                        <ul>
                            
                            
                            
                            <li>
                                
                                <a href="../../1592/facilitating-ux-workshops/index.php">Facilitating UX Workshops</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1593/human-mind/index.php">The Human Mind and Usability</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1591/information-architecture/index.php">Information Architecture</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1590/usability-mobile-websites-apps/index.php">Mobile User Experience</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <strong>Interaction Design: 3-Day Course</strong>
                                
                            </li>
                            
                            
                            
                        </ul>
                        
                        <li><strong>Wednesday, May 3</strong></li>
                        <ul>
                            
                            
                            
                            <li>
                                
                                <a href="../../1595/scaling-responsive-design/index.php">Scaling User Interfaces</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1594/cross-channel-user-experience/index.php">Omnichannel Journeys and Customer Experience</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1596/leading-ux-teams/index.php">Leading Highly-Effective UX Teams </a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1597/journey-mapping/index.php">Journey Mapping to Understand Customer Needs</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1598/measuring-ux/index.php">Measuring User Experience</a>
                                
                            </li>
                            
                            
                            
                        </ul>
                        
                        <li><strong>Thursday, May 4</strong></li>
                        <ul>
                            
                            
                            
                            <li>
                                
                                <a href="../../1602/design-tradeoffs/index.php">Design Tradeoffs and UX Decision Frameworks</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1599/application-ux/index.php">Application Design for Web and Desktop</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1603/stakeholder-ux-approval/index.php">Engaging Stakeholders to Build Buy-In</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1600/hci/index.php">User Interface Principles Every Designer Must Know</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1601/usability-testing/index.php">Usability Testing</a>
                                
                            </li>
                            
                            
                            
                        </ul>
                        
                        <li><strong>Friday, May 5</strong></li>
                        <ul>
                            
                            
                            
                            <li>
                                
                                <a href="../../1606/analytics-and-user-experience/index.php">Analytics and User Experience</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1605/translating-brand/index.php">Translating Brand into User Interactions</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1607/lean-ux-and-agile/index.php">Lean UX and Agile</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1604/emerging-patterns-web-design/index.php">Emerging Patterns for Web Design</a>
                                
                            </li>
                            
                            
                            
                        </ul>
                        
                    </ul>
                </div>
            </section>

            <div class="hide-for-small-down">
                



<div class="collapsable-content">
  <section class="l-subsection related-images">
    <h2 class="hide-for-small-down">Scenes From Past Trainings</h2>
    <h1 class="show-for-small-down collapse-link">Scenes From Past Trainings</h1>
    <div class="collapsable">
    <p>At the UX Conference, you get a full day of in-depth training with expert instructors. Learn more about <a href="../../../why-attend/index.php">why you should attend</a>.</p>

    
      
        
          
            <img srcset="https://media.nngroup.com/media/files/images/IMG_4881_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale@2x.jpg 2x" src="https://media.nngroup.com/media/files/images/IMG_4881_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale.jpg" alt="">
          
        
      
    
      
        
          
            <img srcset="https://media.nngroup.com/media/files/images/IMG_8444_copy_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale@2x.jpg 2x" src="https://media.nngroup.com/media/files/images/IMG_8444_copy_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale.jpg" alt="">
          
        
      
    
      
        
          
            <img srcset="https://media.nngroup.com/media/files/images/IMG_2064_copy_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale@2x.jpg 2x" src="https://media.nngroup.com/media/files/images/IMG_2064_copy_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale.jpg" alt="">
          
        
      
    
      
        
          
            <img srcset="https://media.nngroup.com/media/files/images/NN2050_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale@2x.jpg 2x" src="https://media.nngroup.com/media/files/images/NN2050_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale.jpg" alt="">
          
        
      
    
    </div>
  </section>
</div>


            </div>

            <p class="other-cities"><a href="../../../../courses/interaction-design-3-day-course/index.php">Other cities</a> where Interaction Design: 3-Day Course is offered.</p>

        </div>
    </div>
</article>

    </div>
    




<footer id="page-footer">
    <div class="row hide-for-print">
        <div class="column small-12 medium-6">
            <section class="footer-popular">
                
                <h1>Popular Topics</h1>
                <div style="float: left;  width: 15em; display: block;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../../../topic/applications/index.php">Application Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../../../topic/e-commerce/index.php">E-Commerce Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../../../topic/eyetracking/index.php">Eyetracking </a></li>
	<li style="padding-bottom: .5em;"><a href="../../../../topic/human-computer-interaction/index.php">Human Computer Interaction </a></li>
	<li style="padding-bottom: .5em;"><a href="../../../../topic/information-architecture/index.php">Information Architecture </a></li>
	<li style="padding-bottom: .5em;"><a href="../../../../topic/intranets/index.php">Intranet Design </a></li>
	<li><a href="../../../../topic/interaction-design/index.php">Interaction Design </a></li>
</ul>
</div>

<div style="float: left;  width: 15em; display: block; list-style: none;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../../../topic/mobile-and-tablet-design/index.php">Mobile and Tablet Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../../../topic/credibility-persuasion/index.php">Persuasive Design</a></li>
	<li style="padding-bottom: .5em;"><a href="../../../../topic/research-methods/index.php">Research Methods</a></li>
	<li style="padding-bottom: .5em;"><a href="../../../../topic/user-testing/index.php">User Testing </a></li>
	<li style="padding-bottom: .5em;"><a href="../../../../topic/web-usability/index.php">Web Usability </a></li>
	<li style="padding-bottom: .5em;"><a href="../../../../topic/writing-web/index.php">Web Writing &amp; Content Strategy </a></li>
</ul>
</div>

<p>&nbsp;</p>

                
            </section>
        </div>

        <div class="column small-12 medium-6">
            <div class="row">
                <div class="column small-6">
                    <section class="footer-about">
                        <h1>Follow Us</h1>
                        <ul>
	<li><a href="../../../../articles/subscribe/index.php">Subscribe to our weekly UX newsletter</a>,&nbsp;Jakob Nielsen&#39;s Alertbox</li>
	<li>Visit Don Norman&#39;s website, <a href="http://www.jnd.org/"> www.jnd.org </a></li>
	<li>Visit Bruce Tognazzini&#39;s website, <a href="http://www.asktog.com/"> www.asktog.com </a></li>
	<li style="padding-top: 6px;"><a class="twitter-follow-button" data-show-count="false" data-size="large" href="https://twitter.com/nngroup"><img alt="Follow @nngroup on Twitter" height="20" src="https://media.nngroup.com/media/editor/2016/12/30/twitter-follow-button.png" style="width: 117px; height: 20px;" width="117" /></a></li>
	<li style="padding-top: 6px;"><img alt="" height="14" src="https://media.nngroup.com/media/editor/2016/07/26/feed-icon-14x14.png" style="vertical-align:text-top;width:14px;margin-right:.25em;" width="14" /><a href="../../../../feed/rss/index.php">Subscribe to our RSS feed</a></li>
</ul>

                    </section>
                </div>

                <div class="column small-6">
                    <section class="footer-about right-column">
                        <h1>About
</h1>
                        <ul class="no-bullet">
	<li><a href="../../../../about/contact/index.php">Contact information</a></li>
	<li><a href="../../../../about/index.php">About Us</a></li>
	<li><a href="../../../../about/why-nng/index.php">Why NN/g? </a></li>
	<li><a href="../../../../return-policy/index.php">Return Policy</a></li>
</ul>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="copyright column small-12">
            <p><a href="../../../../copyright-and-reprint-info/index.php">Copyright</a> &copy; 1998-2017 Nielsen Norman Group, All Rights Reserved.</p>
        </div>
    </div>
</footer>


    


        <script src="https://media.nngroup.com/static/vendor/fastclick/js/fastclick.min.js"></script>
        <script src="https://media.nngroup.com/static/vendor/foundation/js/foundation.min.js"></script>
        <script src="http://media.nngroup.com/static/js/app.js"></script>
        <script src="http://media.nngroup.com/static/js/scroll-to-fixed.js"></script><script>var externalLinks = document.querySelectorAll('a[href^="http"]');for (var i = externalLinks.length-1; i >= 0; i--) {externalLinks[i].addEventListener("click", function(event) { alert("External links are disabled for this test."); event.preventDefault(); }, false);}var mailLinks = document.querySelectorAll('a[href^="mailto"]');for (var i = mailLinks.length-1; i >= 0; i--) {mailLinks[i].addEventListener("click", function(event) { alert("Email links are disabled for this test."); event.preventDefault(); }, false);}var externalForms = document.querySelectorAll('form');for (var i = externalForms.length-1; i >= 0; i--) {externalForms[i].addEventListener("submit", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false); externalForms[i].addEventListener("click", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false);}</script>

        <script>
            
            $(function() {
                $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
                $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
            });
        </script>

        
    

    <script>
        $(function() {
            var toggle = $("#collapse-toggle");
            toggle.on("click", function() {
                $(".collapsable").toggleClass("collapsed");
                toggle.toggleClass("collapsed");
                return(false);
            })
        });
    </script>


        
        <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1072699960;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072699960/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
    </body>

<!-- Mirrored from www.nngroup.com/training/course/1580/interaction-design-3-day-course/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:09:42 GMT -->
</html>
