<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->





    
<!-- Mirrored from www.nngroup.com/books/multimedia-and-hypertext/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:02:43 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1026.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"licenseKey":"1a30ef9931","errorBeacon":"bam.nr-data.net","applicationID":"2048992","transactionName":"NFxSMRBUX0EDUkIPDA0WdhAMVkVbDV8ZBAwMUkNLFFxURRELdAkMCH1VEQNcXRwFVEI=","beacon":"bam.nr-data.net","queueTime":2,"applicationTime":318,"agent":""}</script>
        <title>Multimedia and Hypertext (Book by Jakob Nielsen)</title><meta property="og:title" content="Multimedia and Hypertext (Book by Jakob Nielsen)" />
  
        
        <meta name="description" content="Jakob Nielsen&#39;s book about hypertext and the internet, including examples of hypermedia systems, strategies for overcoming information overload, usability issues for hypertext, and the use of HTML  HTML (hypertext markup language) and the World Wide Web with interfaces such as Mosaic and Netscape.
">
        <meta property="og:description" content="Jakob Nielsen&#39;s book about hypertext and the internet, including examples of hypermedia systems, strategies for overcoming information overload, usability issues for hypertext, and the use of HTML  HTML (hypertext markup language) and the World Wide Web with interfaces such as Mosaic and Netscape.
" />
        
  
        
	
        

        

        <meta name="robots" content="noarchive">
        <meta name="viewport" content="width=device-width">

        <link rel="search" type="application/opensearchdescription+xml" title="NN/g Search" href="../../opensearch.xml" />
        <link rel="shortcut icon" href="http://media.nngroup.com/static/img/favicon.ico" />

        <link rel="stylesheet" href="https://media.nngroup.com/static/vendor/font-awesome/css/font-awesome.min.css">

        
        <link rel="stylesheet" href="https://media.nngroup.com/static/css/7232a960f1df.css" type="text/css" />
        <link rel="stylesheet" href="http://fast.fonts.com/t/1.css?apiType=css&amp;projectid=c5846260-9def-46ec-9407-0f1349bd2d64" type="text/css">

        <script src="https://media.nngroup.com/static/vendor/modernizr/js/modernizr.min.js"></script>

        <script type="text/javascript" src="https://media.nngroup.com/static/js/3283be5def74.js"></script>
        

        

        
        
        <!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=229034,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('http://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->


<!-- Google Analytics Tracking Code for http://nngroup.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23132681-1', 'nngroup.com');
  ga('require', 'linkid', 'https://www.nngroup.com/books/multimedia-and-hypertext/linkid.js');
  ga('require', 'displayfeatures');

// Optimizely Universal Analytics Integration
 window.optimizely = window.optimizely || [];
 window.optimizely.push("activateUniversalAnalytics");

  ga('send', 'pageview');

</script>
<script>
/**
* Function to track a click on an outbound link in Google Analytics.
*/
function trackOutboundLink(label, url) {
    if(window.ga && ga.loaded) {
        ga('send', 'event', 'outbound', 'click', label, {'hitCallback': function(){document.location = url;}
        });
    } else {
        document.location = url;
    }
}
/**
* Function to track a click on a link in the article announcement area.
*/
$('#articleAnnouncement a').on('click', function(){
  ga('send', 'event', 'link', 'click', 'articleAnnouncement');
});
</script>

<script type="text/javascript">
 setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2421.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



<!-- Hotjar Tracking Code for http://nngroup.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:121605,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

        <script src="https://media.nngroup.com/static/js/analytics/track_outbound_link.js"></script>
        
        
        
    </head>
    <body class="location-about location-books book-detail">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
    <header id="page-header">
        <div id="cart" class="shopping-cart mobile-cart show-for-small-down"></div>
        <div class="row">
            <div id="masthead" class="masthead small-12 medium-7 column">

                <!-- The button to toggles menu on small screen -->

                <h1><a href="../../index.php"><img src="http://media.nngroup.com/static/img/logo-wordmark.png" width="74" height="30"> Nielsen <span class="norman">Norman</span> Group</a></h1>
                <p class="tagline show-for-medium-up">Evidence-Based User Experience Research, Training, and Consulting</p>

                <!-- The button to toggles form search on small screen -->


                <div style="clear: right;width:  100%;height: 1px;"></div><?php define('ROOT_PATH',$_SERVER["DOCUMENT_ROOT"] . '/apps/navigation_styles/navigations/5'); ?><?php include(ROOT_PATH)."/navigation.php"; ?>

                <!-- Drop down menu on small screen -->
                <div id="hideNav" class="show-for-small-down">
                  
                  <ul class="menu-content">
                    <li><a href="../../index.php">Home</a></li>
                    <li class="has-submenu">
                      <a>Training</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../training/index.php">Overview</a></li>
                        
<li id="nav-ux-conference"><a href="../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../online-seminars/index.php"><span>Online Seminars</span></a></li>

                      </ul>
                    </li>
                    <li class="has-submenu">
                      <a>Consulting</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
  <li><a href="../../consulting/index.php">Overview</a></li>
  
  <li><a href="../../consulting/benchmarking/index.php">Benchmarking</a></li>
  
  <li><a href="../../consulting/expert-design-review/index.php">Expert Design Review</a></li>
  
  <li><a href="../../consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
  
  <li><a href="../../consulting/usability-testing/index.php">Usability Testing </a></li>
  
  <li><a href="../../consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
  
  <li><a href="../../in-house-training/index.php">In-House Training</a></li>
</ul>

                    </li>

                    <li><a href="../../reports/index.php">Reports</a></li>
                    <li><a href="../../articles/index.php">Articles</a></li>
                    <li class="has-submenu"><a>About NN/G</a><i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../about/index.php">Overview</a></li>
                        <li><a href="../../people/index.php">People</a></li>
                        <li><a href="../../about/why-nng/index.php">Why NN/g?</a></li>
                        <li><a href="../../about/contact/index.php">Contact</a></li>
                        <li><a href="../../news/index.php">News</a></li>
                        <li><a href="../../about/history/index.php">History</a></li>
                        <li><a href="../index.php">Books</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                </div>

                <hr class="header-divider show-for-small-only"/>


            </div>
       </div>

        <div id="hideNav">            <nav class="primary-nav small-12 column">
                <ul>
                    <li><a class="navitem" id="nav-home" href="../../index.php">Home</a></li>
                    <li><a class="navitem" id="nav-training" href="../../training/index.php">Training</a></li>
                    <li><a class="navitem" id="nav-consulting" href="../../consulting/index.php">Consulting</a></li>
                    <li><a class="navitem" id="nav-reports" href="../../reports/index.php">Reports</a></li>
                    <li><a class="navitem" id="nav-articles" href="../../articles/index.php">Articles</a></li>
                    <li><a class="navitem" id="nav-about" href="../../about/index.php">About NN/G</a></li>
                </ul>
            </nav>
            <nav class="secondary-nav small-12 medium-12 large-12 columns">
                
<ul class="inline-list">
    <li id="nav-people"><a href="../../people/index.php"><span>People</span></a></li>
    <li id="nav-whynng"><a href="../../about/why-nng/index.php"><span>Why NN/g?</span></a></li>
    <li id="nav-contact"><a href="../../about/contact/index.php"><span>Contact</span></a></li>
    <li id="nav-news"><a href="../../news/index.php"><span>News</span></a></li>
    <li id="nav-history"><a href="../../about/history/index.php"><span>History</span></a></li>
    <li id="nav-books"><a href="../index.php"><span>Books</span></a></li>
</ul>

            </nav>
        </div>
    </header>
    <div class="l-content"><span id="preventClose" style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776; Menu</span>
    
<div class="row">
  <div class="medium-2 columns hide-for-small-down">
    <img class="book-cover" src="https://media.nngroup.com/media/publications/books/multimedia-hypertext-cover.png.300x400_q95_autocrop_crop_upscale.png">
  </div>
  <div class="small-12 medium-6 columns l-subsection">
    <h1>Multimedia and Hypertext: The Internet and Beyond</h1>

    <img class="show-for-small-down book-cover" src="https://media.nngroup.com/media/publications/books/multimedia-hypertext-cover.png.300x400_q95_autocrop_crop_upscale.png">
    <h2>
<a href="../../people/jakob-nielsen/index.php">Jakob Nielsen</a>
, 1995</h2>

    

    <p>
	The 2nd edition of the best-selling <a href="../hypertext-and-hypermedia/index.php"> HyperText and HyperMedia</a>, this book takes hypertext a step further&mdash;to the Internet. Based on a sound conceptual foundation in hypertext theory, the book includes richly illustrated examples of a wide variety of new hypermedia systems, a range of strategies for overcoming information overload, an explanation of copyright issues for users and developers, usability issues for hypertext, and long and short term predictions for the future of the field.</p>
<p>
	Published by Morgan Kaufmann, San Francisco; originally published by AP Professional, Boston, MA.&nbsp; <strong> ISBN 0-12-518408-5</strong>.</p>
<h2>
	Table of Contents</h2>
<ul style="margin-left: 40px;">
	<li>
		Preface</li>
	<li>
		A Multitude of Hypertext and Multimedia</li>
	<li>
		Acknowledgments</li>
</ul>
<ol>
	<li>
		Defining Hypertext, Hypermedia, and Multimedia
		<ul>
			<li>
				Narrower Definitions of Hypertext</li>
			<li>
				Hypermedia: Multimedia Hypertext</li>
			<li>
				Hypertext and Regular Computer Applications</li>
			<li>
				The Hype about Hypertext</li>
		</ul>
	</li>
	<li>
		An Example of a Hypertext System</li>
	<li>
		<a href="../../articles/hypertext-history/index.php">The History of Hypertext </a>
		<ul>
			<li>
				Memex (1945)</li>
			<li>
				Augment/NLS (1962-1976)</li>
			<li>
				Xanadu (1965)</li>
			<li>
				Hypertext Editing System (1967) and FRESS (1968)</li>
			<li>
				Aspen Movie Map (1978)</li>
			<li>
				KMS (1983)</li>
			<li>
				Hyperties (1983)</li>
			<li>
				NoteCards (1985)</li>
			<li>
				Symbolics Document Examiner (1985)</li>
			<li>
				Intermedia (1985)</li>
			<li>
				Guide (1986)</li>
			<li>
				HyperCard (1987)</li>
			<li>
				Hypertext Grows Up</li>
		</ul>
	</li>
	<li>
		Applications of Hypertext
		<ul>
			<li>
				Computer Applications</li>
			<li>
				Business Applications</li>
			<li>
				Intellectual Applications</li>
			<li>
				Educational Applications</li>
			<li>
				Entertainment and Leisure Applications</li>
		</ul>
	</li>
	<li>
		<a href="../../articles/architectural-component-hypertext-systems/index.php">The Architecture of Hypertext Systems </a>
		<ul>
			<li>
				Nodes</li>
			<li>
				Links</li>
			<li>
				Hypertext Engines</li>
			<li>
				Open Hypertext</li>
			<li>
				Integrating Hypertext Ideas into Other Environments</li>
		</ul>
	</li>
	<li>
		Hardware Support for Hypertext
		<ul>
			<li>
				Problems with the Computer Screen</li>
			<li>
				Pointing Devices</li>
			<li>
				Can Text-Only Computers Be Used for Hypertext?</li>
			<li>
				CD-ROM as a Storage Device</li>
		</ul>
	</li>
	<li>
		Hypertext on the Internet
		<ul>
			<li>
				Accessing Hypertext Through the Internet</li>
			<li>
				The World Wide Web and Mosaic</li>
			<li>
				HTML</li>
			<li>
				Hyper-G and Harmony</li>
			<li>
				Half-Dead Hypertext and the Electronic Business Card</li>
		</ul>
	</li>
	<li>
		<a href="../../articles/coping-information-overload/index.php">Coping with Information Overload </a>
		<ul>
			<li>
				Information Retrieval</li>
			<li>
				Human Editing</li>
			<li>
				Interest Voting and Readwear</li>
			<li>
				The n of 2n Approach</li>
		</ul>
	</li>
	<li>
		<a href="../../articles/navigating-large-information-spaces/index.php">Navigating Large Information Spaces </a>
		<ul>
			<li>
				Overview Diagrams</li>
			<li>
				Navigational Dimensions and Metaphors</li>
		</ul>
	</li>
	<li>
		Hypertext Usability
		<ul>
			<li>
				Usability Parameters for Hypertext</li>
			<li>
				Survey of Benchmark Research</li>
			<li>
				Non-Benchmark Studies</li>
			<li>
				The Larger Picture</li>
		</ul>
	</li>
	<li>
		Multimedia Authoring
		<ul>
			<li>
				Usability for Authors</li>
			<li>
				Separate Interfaces for Writers</li>
			<li>
				Cooperative Authoring</li>
			<li>
				The Authority of the Author</li>
		</ul>
	</li>
	<li>
		Repurposing Existing Content
		<ul>
			<li>
				Conversion</li>
			<li>
				The Manual of Medical Therapeutics</li>
			<li>
				Oxford English Dictionary</li>
		</ul>
	</li>
	<li>
		<a href="../../articles/hypertext-future/index.php">The Future of Multimedia and Hypertext </a>
		<ul>
			<li>
				What Happened to the Predictions from my Earlier Book?</li>
			<li>
				Short-Term Future: Three to Five Years</li>
			<li>
				Medium-Term Future: Five to Ten Years</li>
			<li>
				Long-Term Future: Ten to Twenty Years</li>
			<li>
				Hypermedia Publishing: Monopoly or Anarchy?</li>
			<li>
				A Summary: Hypertext versus the Competition</li>
		</ul>
	</li>
</ol>
<ul>
	<li>
		Appendix: Annotated Bibliography
		<ul>
			<li>
				Surveys</li>
			<li>
				Societies</li>
			<li>
				Compendium</li>
			<li>
				Conferences</li>
			<li>
				Journals and Magazines</li>
			<li>
				Videotapes</li>
			<li>
				Books about the Internet</li>
			<li>
				Classics</li>
			<li>
				Alphabetical Listing of Papers and Books</li>
			<li>
				Far Out Stuff</li>
		</ul>
	</li>
	<li>
		Index</li>
</ul>
<p>
	<em>This book is the second edition of my hypertext book. The first edition was published under the title <a href="../hypertext-and-hypermedia/index.php"> Hypertext and Hypermedia</a>. </em></p>
<h2>
	Translations:</h2>
<p>
	German (Vieweg Verlag, ISBN 3-528-05525-1):</p>
<p>
	&nbsp; <img alt="German book cover" src="http://media.nngroup.com.s3.amazonaws.com/media/editor/2012/11/14/mmhtcover_german.jpg" style="width: 200px; height: 298px;" /></p>
<p>
	Japanese (Tokyo Denki University Press, ISBN 4-501-53510-5):</p>
<p>
	<img alt="Japanese book cover" src="http://media.nngroup.com.s3.amazonaws.com/media/editor/2012/11/14/mmhtcover_japanese.jpg" style="width: 200px; height: 251px;" /></p>


    <div class="show-for-small-down purchase-from">
      <hr>
      
  <h2>Purchase from</h2>
  
    <p><a href="http://www.amazon.com/dp/0125184085?tag=useitcomusablein">Amazon.com</a></p>
  
  
    
    
      <ul class="no-bullet">
        <li><strong>UK/Europe</strong></li>
        
          <li><a href="http://www.amazon.co.uk/dp/0125184085?tag=useit-21">Buy from Amazon.co.uk</a></li>
        
      </ul>
    
  


    </div>
    <div class="related">
      


  <hr>



  <h3>Related Reports</h3>
  <ul class="no-bullet">
    
      <li><a href="../../reports/b2b-websites-usability/index.php">B2B Website Usability for Converting Users into Leads and Customers</a></li>
    
      <li><a href="../../reports/strategic-design-faqs/index.php">Strategic Design for Frequently Asked Questions</a></li>
    
  </ul>



  <h3>Related Courses</h3>
   <ul class="no-bullet">
    
        <li><a href="../../courses/ux-basic-training/index.php">UX Basic Training</a></li>
    
        <li><a href="../../courses/human-mind/index.php">The Human Mind and Usability</a></li>
    
        <li><a href="../../courses/emerging-patterns-web-design/index.php">Emerging Patterns for Web Design</a></li>
    
        <li><a href="../../courses/web-page-design/index.php">Web Page UX Design</a></li>
    
</ul>



  <h3>Articles</h3>
  <ul class="no-bullet">
    
      <li><a href="../../articles/menu-design/index.php">Menu Design: Checklist of 15 UX Guidelines to Help Users </a></li>
    
      <li><a href="../../articles/flat-design/index.php">Flat Design: Its Origins, Its Problems, and Why Flat 2.0 Is Better for Users</a></li>
    
      <li><a href="../../articles/page-fold-manifesto/index.php">The Fold Manifesto: Why the Page Fold Still Matters</a></li>
    
      <li><a href="../../articles/scaling-user-interfaces/index.php">Scaling User Interfaces: An Information-Processing Approach to Multi-Device Design</a></li>
    
      <li><a href="../../articles/top-10-mistakes-web-design/index.php">Top 10 Mistakes in Web Design</a></li>
    
  </ul>


    </div>
  </div>

  <div class="small-12 medium-4 columns hide-for-small-down l-subsection">
    
  <h2>Purchase from</h2>
  
    <p><a href="http://www.amazon.com/dp/0125184085?tag=useitcomusablein">Amazon.com</a></p>
  
  
    
    
      <ul class="no-bullet">
        <li><strong>UK/Europe</strong></li>
        
          <li><a href="http://www.amazon.co.uk/dp/0125184085?tag=useit-21">Buy from Amazon.co.uk</a></li>
        
      </ul>
    
  


  </div>

</div>

    </div>
    




<footer id="page-footer">
    <div class="row hide-for-print">
        <div class="column small-12 medium-6">
            <section class="footer-popular">
                
                <h1>Popular Topics</h1>
                <div style="float: left;  width: 15em; display: block;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/applications/index.php">Application Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/e-commerce/index.php">E-Commerce Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/eyetracking/index.php">Eyetracking </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/information-architecture/index.php">Information Architecture </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/intranets/index.php">Intranet Design </a></li>
	<li><a href="../../topic/interaction-design/index.php">Interaction Design </a></li>
</ul>
</div>

<div style="float: left;  width: 15em; display: block; list-style: none;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/mobile-and-tablet-design/index.php">Mobile and Tablet Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/credibility-persuasion/index.php">Persuasive Design</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/research-methods/index.php">Research Methods</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/user-testing/index.php">User Testing </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/web-usability/index.php">Web Usability </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/writing-web/index.php">Web Writing &amp; Content Strategy </a></li>
</ul>
</div>

<p>&nbsp;</p>

                
            </section>
        </div>

        <div class="column small-12 medium-6">
            <div class="row">
                <div class="column small-6">
                    <section class="footer-about">
                        <h1>Follow Us</h1>
                        <ul>
	<li><a href="../../articles/subscribe/index.php">Subscribe to our weekly UX newsletter</a>,&nbsp;Jakob Nielsen&#39;s Alertbox</li>
	<li>Visit Don Norman&#39;s website, <a href="http://www.jnd.org/"> www.jnd.org </a></li>
	<li>Visit Bruce Tognazzini&#39;s website, <a href="http://www.asktog.com/"> www.asktog.com </a></li>
	<li style="padding-top: 6px;"><a class="twitter-follow-button" data-show-count="false" data-size="large" href="https://twitter.com/nngroup"><img alt="Follow @nngroup on Twitter" height="20" src="https://media.nngroup.com/media/editor/2016/12/30/twitter-follow-button.png" style="width: 117px; height: 20px;" width="117" /></a></li>
	<li style="padding-top: 6px;"><img alt="" height="14" src="https://media.nngroup.com/media/editor/2016/07/26/feed-icon-14x14.png" style="vertical-align:text-top;width:14px;margin-right:.25em;" width="14" /><a href="../../feed/rss/index.php">Subscribe to our RSS feed</a></li>
</ul>

                    </section>
                </div>

                <div class="column small-6">
                    <section class="footer-about right-column">
                        <h1>About
</h1>
                        <ul class="no-bullet">
	<li><a href="../../about/contact/index.php">Contact information</a></li>
	<li><a href="../../about/index.php">About Us</a></li>
	<li><a href="../../about/why-nng/index.php">Why NN/g? </a></li>
	<li><a href="../../return-policy/index.php">Return Policy</a></li>
</ul>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="copyright column small-12">
            <p><a href="../../copyright-and-reprint-info/index.php">Copyright</a> &copy; 1998-2017 Nielsen Norman Group, All Rights Reserved.</p>
        </div>
    </div>
</footer>


    


        <script src="https://media.nngroup.com/static/vendor/fastclick/js/fastclick.min.js"></script>
        <script src="https://media.nngroup.com/static/vendor/foundation/js/foundation.min.js"></script>
        <script src="http://media.nngroup.com/static/js/app.js"></script>
        <script src="http://media.nngroup.com/static/js/scroll-to-fixed.js"></script><script>var externalLinks = document.querySelectorAll('a[href^="http"]');for (var i = externalLinks.length-1; i >= 0; i--) {externalLinks[i].addEventListener("click", function(event) { alert("External links are disabled for this test."); event.preventDefault(); }, false);}var mailLinks = document.querySelectorAll('a[href^="mailto"]');for (var i = mailLinks.length-1; i >= 0; i--) {mailLinks[i].addEventListener("click", function(event) { alert("Email links are disabled for this test."); event.preventDefault(); }, false);}var externalForms = document.querySelectorAll('form');for (var i = externalForms.length-1; i >= 0; i--) {externalForms[i].addEventListener("submit", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false); externalForms[i].addEventListener("click", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false);}</script>

        <script>
            
            $(function() {
                $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
                $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
            });
        </script>

        


        
        <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1072699960;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072699960/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
    </body>

<!-- Mirrored from www.nngroup.com/books/multimedia-and-hypertext/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:02:43 GMT -->
</html>
