<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->





    
<!-- Mirrored from www.nngroup.com/news/item/2014-intranet-design-awards/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:13:15 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1026.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"licenseKey":"1a30ef9931","errorBeacon":"bam.nr-data.net","applicationID":"2048992","transactionName":"NFxSMRBUX0EDUkIPDA0WdhAMVkVbDV8ZCAYUSh4TC1BGQVh/UxEQJ1xEBAtZH1UHRQ==","beacon":"bam.nr-data.net","queueTime":7,"applicationTime":69,"agent":""}</script>
        <title>2014 Intranet Design Award Winners</title><meta property="og:title" content="2014 Intranet Design Award Winners" />
  
        
        <meta name="description" content="2014 Intranet Design Annual winners announced">
        <meta property="og:description" content="2014 Intranet Design Annual winners announced" />
        
  
        
	
        

        

        <meta name="robots" content="noarchive">
        <meta name="viewport" content="width=device-width">

        <link rel="search" type="application/opensearchdescription+xml" title="NN/g Search" href="../../../opensearch.xml" />
        <link rel="shortcut icon" href="http://media.nngroup.com/static/img/favicon.ico" />

        <link rel="stylesheet" href="https://media.nngroup.com/static/vendor/font-awesome/css/font-awesome.min.css">

        
        <link rel="stylesheet" href="https://media.nngroup.com/static/css/7232a960f1df.css" type="text/css" />
        <link rel="stylesheet" href="http://fast.fonts.com/t/1.css?apiType=css&amp;projectid=c5846260-9def-46ec-9407-0f1349bd2d64" type="text/css">

        <script src="https://media.nngroup.com/static/vendor/modernizr/js/modernizr.min.js"></script>

        <script type="text/javascript" src="https://media.nngroup.com/static/js/3283be5def74.js"></script>
        

        

        
        
        <!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=229034,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('http://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->


<!-- Google Analytics Tracking Code for http://nngroup.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23132681-1', 'nngroup.com');
  ga('require', 'linkid', 'https://www.nngroup.com/news/item/2014-intranet-design-awards/linkid.js');
  ga('require', 'displayfeatures');

// Optimizely Universal Analytics Integration
 window.optimizely = window.optimizely || [];
 window.optimizely.push("activateUniversalAnalytics");

  ga('send', 'pageview');

</script>
<script>
/**
* Function to track a click on an outbound link in Google Analytics.
*/
function trackOutboundLink(label, url) {
    if(window.ga && ga.loaded) {
        ga('send', 'event', 'outbound', 'click', label, {'hitCallback': function(){document.location = url;}
        });
    } else {
        document.location = url;
    }
}
/**
* Function to track a click on a link in the article announcement area.
*/
$('#articleAnnouncement a').on('click', function(){
  ga('send', 'event', 'link', 'click', 'articleAnnouncement');
});
</script>

<script type="text/javascript">
 setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2421.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



<!-- Hotjar Tracking Code for http://nngroup.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:121605,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

        <script src="https://media.nngroup.com/static/js/analytics/track_outbound_link.js"></script>
        
        
        
    </head>
    <body class="location-about location-news">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
    <header id="page-header">
        <div id="cart" class="shopping-cart mobile-cart show-for-small-down"></div>
        <div class="row">
            <div id="masthead" class="masthead small-12 medium-7 column">

                <!-- The button to toggles menu on small screen -->

                <h1><a href="../../../index.php"><img src="http://media.nngroup.com/static/img/logo-wordmark.png" width="74" height="30"> Nielsen <span class="norman">Norman</span> Group</a></h1>
                <p class="tagline show-for-medium-up">Evidence-Based User Experience Research, Training, and Consulting</p>

                <!-- The button to toggles form search on small screen -->


                <div style="clear: right;width:  100%;height: 1px;"></div><?php define('ROOT_PATH',$_SERVER["DOCUMENT_ROOT"] . '/apps/navigation_styles/navigations/5'); ?><?php include(ROOT_PATH)."/navigation.php"; ?>

                <!-- Drop down menu on small screen -->
                <div id="hideNav" class="show-for-small-down">
                  
                  <ul class="menu-content">
                    <li><a href="../../../index.php">Home</a></li>
                    <li class="has-submenu">
                      <a>Training</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../../training/index.php">Overview</a></li>
                        
<li id="nav-ux-conference"><a href="../../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../../online-seminars/index.php"><span>Online Seminars</span></a></li>

                      </ul>
                    </li>
                    <li class="has-submenu">
                      <a>Consulting</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
  <li><a href="../../../consulting/index.php">Overview</a></li>
  
  <li><a href="../../../consulting/benchmarking/index.php">Benchmarking</a></li>
  
  <li><a href="../../../consulting/expert-design-review/index.php">Expert Design Review</a></li>
  
  <li><a href="../../../consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
  
  <li><a href="../../../consulting/usability-testing/index.php">Usability Testing </a></li>
  
  <li><a href="../../../consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
  
  <li><a href="../../../in-house-training/index.php">In-House Training</a></li>
</ul>

                    </li>

                    <li><a href="../../../reports/index.php">Reports</a></li>
                    <li><a href="../../../articles/index.php">Articles</a></li>
                    <li class="has-submenu"><a>About NN/G</a><i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../../about/index.php">Overview</a></li>
                        <li><a href="../../../people/index.php">People</a></li>
                        <li><a href="../../../about/why-nng/index.php">Why NN/g?</a></li>
                        <li><a href="../../../about/contact/index.php">Contact</a></li>
                        <li><a href="../../index.php">News</a></li>
                        <li><a href="../../../about/history/index.php">History</a></li>
                        <li><a href="../../../books/index.php">Books</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                </div>

                <hr class="header-divider show-for-small-only"/>


            </div>
       </div>

        <div id="hideNav">            <nav class="primary-nav small-12 column">
                <ul>
                    <li><a class="navitem" id="nav-home" href="../../../index.php">Home</a></li>
                    <li><a class="navitem" id="nav-training" href="../../../training/index.php">Training</a></li>
                    <li><a class="navitem" id="nav-consulting" href="../../../consulting/index.php">Consulting</a></li>
                    <li><a class="navitem" id="nav-reports" href="../../../reports/index.php">Reports</a></li>
                    <li><a class="navitem" id="nav-articles" href="../../../articles/index.php">Articles</a></li>
                    <li><a class="navitem" id="nav-about" href="../../../about/index.php">About NN/G</a></li>
                </ul>
            </nav>
            <nav class="secondary-nav small-12 medium-12 large-12 columns">
                
<ul class="inline-list">
    <li id="nav-people"><a href="../../../people/index.php"><span>People</span></a></li>
    <li id="nav-whynng"><a href="../../../about/why-nng/index.php"><span>Why NN/g?</span></a></li>
    <li id="nav-contact"><a href="../../../about/contact/index.php"><span>Contact</span></a></li>
    <li id="nav-news"><a href="../../index.php"><span>News</span></a></li>
    <li id="nav-history"><a href="../../../about/history/index.php"><span>History</span></a></li>
    <li id="nav-books"><a href="../../../books/index.php"><span>Books</span></a></li>
</ul>

            </nav>
        </div>
    </header>
    <div class="l-content"><span id="preventClose" style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776; Menu</span>
    


<div class="row">
    <div class="small-12 medium-4 large-4 columns offset-by-eight">
    </div>
</div>
<div class="row">
    <div class="small-12 medium-3 large-3 columns l-navrail news-rail hide-for-small-only">
        <h1><a href="../../index.php">All News Types</a></h1>
        <ul class="no-bullet">
            
                
                    <li><a href="../../type/announcements/index.php">Announcements (52)</a></li>
                
            
                
                    <li><a href="../../type/press-mentions/index.php">Interviews &amp; Press (225)</a></li>
                
            
        </ul>
    </div>
    <div class="small-12 medium-9 large-9 columns">
        <article class="news_detail">
            <div class="l-section-intro">
                <header>
                    <h1>NN/g News</h1>
                    <h2>2014 Intranet Design Award Winners</h2>
                    <p>January 5, 2014</p>
                </header>
            </div>
            <div class="l-subsection">
                <h2>Congratulations to the 2014 Intranet Design Annual Award winners!</h2>

<h2>Abt Associates, Inc.</h2>

<p>The Abt Associates intranet team set the goal to &ldquo;create a smooth, seamless, and personalized experience for users that makes them feel welcome; encourage them to contribute insights to foster continuous learning; and, with mobile technology, enable them to access everything, no matter their location.&rdquo; The team&rsquo;s &ldquo;virtual hallway&rdquo; was a giant endeavor that works tirelessly to connect employees around the world &mdash; effectively, thoroughly, and equally.</p>

<p><img alt="Abt Associates AGI team photo" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2013/12/18/01a_Abt_Team.png" style="width: 460px; height: 314px; border-width: 1px; border-style: solid;" /></p>

<p><strong>The Abt Associates AGI team</strong> (top row, left to right): Mary Harper, Mary Maguire, Mark Spranca, Albina Shekhtman, Cheryl Fries, and Cara Capizzi; (middle row, left to right): Frank Divita, Jessica Erbacher, Deborah Dangay, Danielle Hunt, Mauricio Poodts, and Beth Williams; (bottom row, left to right): Vanessa Martin, Allison Jung, and Rob Grimmett.&nbsp;</p>

<p><img alt="Brightstarr team photo" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2013/12/18/01b_Abt_Brightstarr_team.png" style="width: 460px; height: 343px; border-width: 1px; border-style: solid;" /></p>

<p><strong>Brightstarr team members</strong> (top row, left to right): Emma Pinkerton, Kunaal Kapoor, and Kip Wagner; (bottom row, left to right); Kanwal Khipple, Glen Chambers, and Juan Larios.</p>

<h2>Air New Zealand Limited</h2>

<p>With a content refresh and an overhaul of the information architecture, the Air New Zealand team tackled the challenge of a cluttered and disorganized intranet. A bold design and the use of photographs of employees throughout the site add to the users&rsquo; experience.</p>

<p><img alt="Air New Zealand Limited team photo" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2013/12/18/03_Air_New_Zealand_team.png" style="width: 460px; height: 296px; border-width: 1px; border-style: solid;" /></p>

<p><strong>Air New Zealand team members</strong> (left to right): Ian Lang, Tristan Chan, Stacey Olsen, Andrea Perez, Tess O&rsquo;Connor, and Ryan Mears; (absent from photo): Maarten Nieuwland and Shane Burfield-Mills.</p>

<h2>Allianz Australia&nbsp;</h2>

<p>With thorough planning, the best navigational elements, and a well-defined use of screen real estate, the Allianz Australia intranet, collabor8, makes it a breeze for employees to find what they need and get up to speed with their work and the latest happenings at the organization and around the world.&nbsp;</p>

<p><img alt="Allianz Australia intranet team photo" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2013/12/18/04_Allianz_team.png" style="width: 460px; height: 303px; border-width: 1px; border-style: solid;" /></p>

<p><strong>Allianz Australia team members</strong>&nbsp;(left to right): Patrick Brownsberger, Ida Tamabadlbo, Zoe Salonitides, Mike Brown, and Greg Loundar.</p>

<h2>AMP&nbsp;</h2>

<p>With an activity stream on the homepage and collaborative spaces integrated into the main intranet, AMP&rsquo;s intranet, The Hub, invites active participation as it updates employees about the latest information. The team worked with employees throughout the design and development process to integrate user feedback and also make employees feel like they were a part of the site&rsquo;s development from the beginning.&nbsp;</p>

<p><img alt="AMP intranet team photo" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2013/12/18/02_AMP_team.png" style="width: 460px; height: 389px; border-width: 1px; border-style: solid;" /></p>

<p><strong>The AMP intranet team</strong> (front row, left to right): Jenny Thai, Octavia Maddox, Steve Dawson, Linda Le, and Henry Wijaya; (back row, left to right): Doug Wolfson, Darren Walker, David Wall, Frank Arraiza, Wendy Chan, and Pramit Punnilethu.</p>

<h2>International Monetary Fund</h2>

<p>With a streamlined design focused on moving employees to information quickly, the IMF intranet presents information in an easy to use way. The creation of an Intranet Council to sustain the gains made by the new site and manage its growth shows that the team planned for the future in building the new site.&nbsp;</p>

<p><img alt="IMF intranet team photo" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2013/12/18/05_IMF_team.png" style="width: 460px; height: 324px; border-width: 1px; border-style: solid;" /></p>

<p><strong>The IMF intranet team</strong> (left to right): Camilla Andersen, Graham Dwyer, Bego&ntilde;a Nu&ntilde;ez Allue, Hari Maddineni, Aissata Sidibe, H&eacute;l&egrave;ne Faur&egrave;s, Jamie Colucci (Threespot), Rajitha Devineni, Joshua Sampson, Deb Reilly, Shishir Bhandari, Phil Gosier (Threespot), Vijay Challa, Scott Merker, Hallie Wilfert (Threespot), and Archana Kumar. Missing from photo: Sonia Dwyer, Padraic Hughes, Vera Rhoads, Anna Rappoport (Threespot), James Early (Threespot), and Paul Zolandz (Threespot).</p>

<h2>Mayo Clinic</h2>

<p>Persona-driven UX development helped the Mayo Clinic intranet team understand and focus on the various types of people for which they were designing and derive this great intranet, which makes everything searchable and findable. Mayo Clinic designers used constraints to their advantage to create a system that exceeds expectations and makes it possible for physicians, scientists, students, and staff alike to find all the information they need.</p>

<p><img alt="Mayo Clinic intranet team photo" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2013/12/18/06_Mayo_cinic_team.png" style="width: 460px; height: 245px; border-width: 1px; border-style: solid;" /></p>

<p><strong>The Mayo Clinic intranet team</strong> (back row, left to right): Josh Brule, Mary Uhlir, Ryan Hegge, Jane Jacobs, Richard Hurt Jr., Sumathi Jayakumar, Donna Blade, Jodie Bartz, and Nik Coates; (front row, left to right): Jonathan Schelander-Pugh, EleAnn Mulholland, Dawn Daehn, Monty Flinsch, Craig Hobson, Terry Smoley, Gianna LaPin, John Schultz, and Mark McGlinch; (inset): Deborah Grover.</p>

<h2>National Geographic Society</h2>

<p>Connections are key on the National Geographic Society website. The intranet team built a custom social tool, ng+, to encourage communication and collaboration across the organization. It also paid attention to details throughout the site to make engagement and participation easy. An emphasis on visuals takes advantage of and reflects the organization&rsquo;s rich history.</p>

<p><img alt="National Geographic Society intranet team photo" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2013/12/18/07_National_Geographic_team.png" style="width: 460px; height: 614px; border-width: 1px; border-style: solid;" /></p>

<p><strong>The National Geographic intranet team</strong> (front row, left to right): Drew Engelson (Celerity), Megan Seldon (National Geographic), Keelin Vaccaro (National Geographic), Christy Solberg (National Geographic), Susana Esparza (Celerity), and Kenneth Yu (Celerity); (back row, left to right): Elisabeth Beller (Celerity), Russ Little (National Geographic), Beshoy Louka (Celerity), Mark Hill (Baker Hill), John Dymond (Celerity), Dan Baker (National Geographic), and Jason Kolaitis (Celerity).</p>

<h2>Ooredoo</h2>

<p>Ooredoo designers have made an excellent case study for highly successful social sharing on an intranet. &ldquo;Share and share alike&rdquo; could be the dictum for the Buzz portal. Ooredoo&rsquo;s intranet connects thousands of employees by exploiting social sharing features and offering robust language support. With a simple UI and targeted encouragement, Ooredoo&rsquo;s intranet fosters a culture of sharing across the organization.</p>

<p><img alt="Ooredoo intranet team photo" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2013/12/18/08a_Ooredoo_team.png" style="width: 460px; height: 690px; border-width: 1px; border-style: solid;" /></p>

<p><strong>Ooredoo Group intranet team members</strong> Caroline Lewis and Sid Ahmed Seghouani.</p>

<p><img alt="IT Worx intranet team photo" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2013/12/18/08b_Ooredoo_ITWorx_team.png" style="width: 460px; height: 239px; border-width: 1px; border-style: solid;" /></p>

<p><strong>ITWORX team members </strong>(top row, left to right): Amira Samir, Nahla Gaber, Nahla Ghoneim, Michael Milad, Ahmed Yehia, Hoda Mira, and Sara El Khoudary; (bottom row, left to right): Abdel Rahman Hazem, Karim Ahmed, Hossam AbdelSalam, Ahmed Kassem, and Ahmed Mostafa.</p>

<h2>triptic</h2>

<p>The triptic intranet, Iris, is a breath of fresh air. She is disarming, funny, smart, modern, and available when you need her. Through the use of offbeat images, whimsical icons, bright colors, and simple text, Iris draws you in without crossing the line and being off-putting. The Iris homepage recognizes logged-in employees individually and serves them desired information &mdash; such as a personal calendar and social following activity alongside the news and a poll widget &mdash; helping keep the employee&rsquo;s finger on the organization&rsquo;s pulse.&nbsp;</p>

<p><img alt="TripTic intranet team photo" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2013/12/18/09_triptic_team.png" style="width: 460px; height: 312px; border-width: 1px; border-style: solid;" /></p>

<p><strong>The triptic team</strong> (left to right, standing): Tim Didderi&euml;ns, Frank Kemps, Anne Blommaert, Martijn Wernaert, Onno Marsman, Lieke Huenges Wajer, Willem van Berlo, Tatiana Morokko, and Jos Rouw; (left to right, sitting): Roel Knapen, Rick Cuijpers, and Arthur Turksma.</p>

<h2>WellPoint</h2>

<p>Using iterative design and focusing on change management, the Executive and Employee Communications team and the Intranet Strategy and Development team at WellPoint&rsquo;s Medicaid business unit combined forces to create an intranet that informs employees and enhances communications through thoughtful design.</p>

<p><img alt="WellPoint intranet team photo" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2013/12/18/10_Wellpoint_team.png" style="width: 460px; height: 373px; border-width: 1px; border-style: solid;" /></p>

<p><strong>The WellPoint team</strong> (top to bottom, left to right): Peter Lobred, Jamisson Fowler, Thomas Caudron, Jason Marlowitz, Peggy Callaway, Nealy Gihan, Katie Landry, Leila Roche, Daniel Whitehead, Rebecca Lambert, Arlen Vargas, Brendan Mcgarrett, Melissa Carter, Anthony Francisco, Jaclyn Payne, Brian King, Andrew Flowe, Susanna Cagle, and Kaitlin Kelly.</p>

<p><a href="../../../reports/10-best-intranets-2014/index.php">2014 Intranet Design Annual details and purchase information</a></p>

            </div>
        </article>
    </div>
</div>


    </div>
    




<footer id="page-footer">
    <div class="row hide-for-print">
        <div class="column small-12 medium-6">
            <section class="footer-popular">
                
                <h1>Popular Topics</h1>
                <div style="float: left;  width: 15em; display: block;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../../topic/applications/index.php">Application Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../../topic/e-commerce/index.php">E-Commerce Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../../topic/eyetracking/index.php">Eyetracking </a></li>
	<li style="padding-bottom: .5em;"><a href="../../../topic/human-computer-interaction/index.php">Human Computer Interaction </a></li>
	<li style="padding-bottom: .5em;"><a href="../../../topic/information-architecture/index.php">Information Architecture </a></li>
	<li style="padding-bottom: .5em;"><a href="../../../topic/intranets/index.php">Intranet Design </a></li>
	<li><a href="../../../topic/interaction-design/index.php">Interaction Design </a></li>
</ul>
</div>

<div style="float: left;  width: 15em; display: block; list-style: none;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../../topic/mobile-and-tablet-design/index.php">Mobile and Tablet Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../../topic/credibility-persuasion/index.php">Persuasive Design</a></li>
	<li style="padding-bottom: .5em;"><a href="../../../topic/research-methods/index.php">Research Methods</a></li>
	<li style="padding-bottom: .5em;"><a href="../../../topic/user-testing/index.php">User Testing </a></li>
	<li style="padding-bottom: .5em;"><a href="../../../topic/web-usability/index.php">Web Usability </a></li>
	<li style="padding-bottom: .5em;"><a href="../../../topic/writing-web/index.php">Web Writing &amp; Content Strategy </a></li>
</ul>
</div>

<p>&nbsp;</p>

                
            </section>
        </div>

        <div class="column small-12 medium-6">
            <div class="row">
                <div class="column small-6">
                    <section class="footer-about">
                        <h1>Follow Us</h1>
                        <ul>
	<li><a href="../../../articles/subscribe/index.php">Subscribe to our weekly UX newsletter</a>,&nbsp;Jakob Nielsen&#39;s Alertbox</li>
	<li>Visit Don Norman&#39;s website, <a href="http://www.jnd.org/"> www.jnd.org </a></li>
	<li>Visit Bruce Tognazzini&#39;s website, <a href="http://www.asktog.com/"> www.asktog.com </a></li>
	<li style="padding-top: 6px;"><a class="twitter-follow-button" data-show-count="false" data-size="large" href="https://twitter.com/nngroup"><img alt="Follow @nngroup on Twitter" height="20" src="https://media.nngroup.com/media/editor/2016/12/30/twitter-follow-button.png" style="width: 117px; height: 20px;" width="117" /></a></li>
	<li style="padding-top: 6px;"><img alt="" height="14" src="https://media.nngroup.com/media/editor/2016/07/26/feed-icon-14x14.png" style="vertical-align:text-top;width:14px;margin-right:.25em;" width="14" /><a href="../../../feed/rss/index.php">Subscribe to our RSS feed</a></li>
</ul>

                    </section>
                </div>

                <div class="column small-6">
                    <section class="footer-about right-column">
                        <h1>About
</h1>
                        <ul class="no-bullet">
	<li><a href="../../../about/contact/index.php">Contact information</a></li>
	<li><a href="../../../about/index.php">About Us</a></li>
	<li><a href="../../../about/why-nng/index.php">Why NN/g? </a></li>
	<li><a href="../../../return-policy/index.php">Return Policy</a></li>
</ul>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="copyright column small-12">
            <p><a href="../../../copyright-and-reprint-info/index.php">Copyright</a> &copy; 1998-2017 Nielsen Norman Group, All Rights Reserved.</p>
        </div>
    </div>
</footer>


    


        <script src="https://media.nngroup.com/static/vendor/fastclick/js/fastclick.min.js"></script>
        <script src="https://media.nngroup.com/static/vendor/foundation/js/foundation.min.js"></script>
        <script src="http://media.nngroup.com/static/js/app.js"></script>
        <script src="http://media.nngroup.com/static/js/scroll-to-fixed.js"></script><script>var externalLinks = document.querySelectorAll('a[href^="http"]');for (var i = externalLinks.length-1; i >= 0; i--) {externalLinks[i].addEventListener("click", function(event) { alert("External links are disabled for this test."); event.preventDefault(); }, false);}var mailLinks = document.querySelectorAll('a[href^="mailto"]');for (var i = mailLinks.length-1; i >= 0; i--) {mailLinks[i].addEventListener("click", function(event) { alert("Email links are disabled for this test."); event.preventDefault(); }, false);}var externalForms = document.querySelectorAll('form');for (var i = externalForms.length-1; i >= 0; i--) {externalForms[i].addEventListener("submit", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false); externalForms[i].addEventListener("click", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false);}</script>

        <script>
            
            $(function() {
                $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
                $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
            });
        </script>

        


        
        <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1072699960;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072699960/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
    </body>

<!-- Mirrored from www.nngroup.com/news/item/2014-intranet-design-awards/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:13:15 GMT -->
</html>
