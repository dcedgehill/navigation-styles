<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->





    
<!-- Mirrored from www.nngroup.com/news/item/design-challenges-cultural-world-tour-hong-kong/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:13:19 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1026.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"licenseKey":"1a30ef9931","queueTime":3,"beacon":"bam.nr-data.net","errorBeacon":"bam.nr-data.net","applicationID":"2048992","transactionName":"NFxSMRBUX0EDUkIPDA0WdhAMVkVbDV8ZCAYUSh4TC1BGQVh/UxEQJ1xEBAtZH1UHRQ==","applicationTime":68,"agent":""}</script>
        <title>World Tour Hong Kong | Nielsen Norman Group</title><meta property="og:title" content="World Tour Hong Kong | Nielsen Norman Group" />
  
        
        <meta name="description" content="Interviews with User Experience World Tour attendees at the Hong Kong conference.">
        <meta property="og:description" content="Interviews with User Experience World Tour attendees at the Hong Kong conference." />
        
  
        
	
        
        <meta name="keywords" content="Usability, design">
        

        

        <meta name="robots" content="noarchive">
        <meta name="viewport" content="width=device-width">

        <link rel="search" type="application/opensearchdescription+xml" title="NN/g Search" href="../../../opensearch.xml" />
        <link rel="shortcut icon" href="http://media.nngroup.com/static/img/favicon.ico" />

        <link rel="stylesheet" href="https://media.nngroup.com/static/vendor/font-awesome/css/font-awesome.min.css">

        
        <link rel="stylesheet" href="https://media.nngroup.com/static/css/7232a960f1df.css" type="text/css" />
        <link rel="stylesheet" href="http://fast.fonts.com/t/1.css?apiType=css&amp;projectid=c5846260-9def-46ec-9407-0f1349bd2d64" type="text/css">

        <script src="https://media.nngroup.com/static/vendor/modernizr/js/modernizr.min.js"></script>

        <script type="text/javascript" src="https://media.nngroup.com/static/js/3283be5def74.js"></script>
        

        

        
        
        <!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=229034,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('http://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->


<!-- Google Analytics Tracking Code for http://nngroup.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23132681-1', 'nngroup.com');
  ga('require', 'linkid', 'https://www.nngroup.com/news/item/design-challenges-cultural-world-tour-hong-kong/linkid.js');
  ga('require', 'displayfeatures');

// Optimizely Universal Analytics Integration
 window.optimizely = window.optimizely || [];
 window.optimizely.push("activateUniversalAnalytics");

  ga('send', 'pageview');

</script>
<script>
/**
* Function to track a click on an outbound link in Google Analytics.
*/
function trackOutboundLink(label, url) {
    if(window.ga && ga.loaded) {
        ga('send', 'event', 'outbound', 'click', label, {'hitCallback': function(){document.location = url;}
        });
    } else {
        document.location = url;
    }
}
/**
* Function to track a click on a link in the article announcement area.
*/
$('#articleAnnouncement a').on('click', function(){
  ga('send', 'event', 'link', 'click', 'articleAnnouncement');
});
</script>

<script type="text/javascript">
 setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2421.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



<!-- Hotjar Tracking Code for http://nngroup.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:121605,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

        <script src="https://media.nngroup.com/static/js/analytics/track_outbound_link.js"></script>
        
        
        
    </head>
    <body class="location-about location-news">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
    <header id="page-header">
        <div id="cart" class="shopping-cart mobile-cart show-for-small-down"></div>
        <div class="row">
            <div id="masthead" class="masthead small-12 medium-7 column">

                <!-- The button to toggles menu on small screen -->

                <h1><a href="../../../index.php"><img src="http://media.nngroup.com/static/img/logo-wordmark.png" width="74" height="30"> Nielsen <span class="norman">Norman</span> Group</a></h1>
                <p class="tagline show-for-medium-up">Evidence-Based User Experience Research, Training, and Consulting</p>

                <!-- The button to toggles form search on small screen -->


                <div style="clear: right;width:  100%;height: 1px;"></div><?php define('ROOT_PATH',$_SERVER["DOCUMENT_ROOT"] . '/apps/navigation_styles/navigations/5'); ?><?php include(ROOT_PATH)."/navigation.php"; ?>

                <!-- Drop down menu on small screen -->
                <div id="hideNav" class="show-for-small-down">
                  
                  <ul class="menu-content">
                    <li><a href="../../../index.php">Home</a></li>
                    <li class="has-submenu">
                      <a>Training</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../../training/index.php">Overview</a></li>
                        
<li id="nav-ux-conference"><a href="../../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../../online-seminars/index.php"><span>Online Seminars</span></a></li>

                      </ul>
                    </li>
                    <li class="has-submenu">
                      <a>Consulting</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
  <li><a href="../../../consulting/index.php">Overview</a></li>
  
  <li><a href="../../../consulting/benchmarking/index.php">Benchmarking</a></li>
  
  <li><a href="../../../consulting/expert-design-review/index.php">Expert Design Review</a></li>
  
  <li><a href="../../../consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
  
  <li><a href="../../../consulting/usability-testing/index.php">Usability Testing </a></li>
  
  <li><a href="../../../consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
  
  <li><a href="../../../in-house-training/index.php">In-House Training</a></li>
</ul>

                    </li>

                    <li><a href="../../../reports/index.php">Reports</a></li>
                    <li><a href="../../../articles/index.php">Articles</a></li>
                    <li class="has-submenu"><a>About NN/G</a><i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../../about/index.php">Overview</a></li>
                        <li><a href="../../../people/index.php">People</a></li>
                        <li><a href="../../../about/why-nng/index.php">Why NN/g?</a></li>
                        <li><a href="../../../about/contact/index.php">Contact</a></li>
                        <li><a href="../../index.php">News</a></li>
                        <li><a href="../../../about/history/index.php">History</a></li>
                        <li><a href="../../../books/index.php">Books</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                </div>

                <hr class="header-divider show-for-small-only"/>


            </div>
       </div>

        <div id="hideNav">            <nav class="primary-nav small-12 column">
                <ul>
                    <li><a class="navitem" id="nav-home" href="../../../index.php">Home</a></li>
                    <li><a class="navitem" id="nav-training" href="../../../training/index.php">Training</a></li>
                    <li><a class="navitem" id="nav-consulting" href="../../../consulting/index.php">Consulting</a></li>
                    <li><a class="navitem" id="nav-reports" href="../../../reports/index.php">Reports</a></li>
                    <li><a class="navitem" id="nav-articles" href="../../../articles/index.php">Articles</a></li>
                    <li><a class="navitem" id="nav-about" href="../../../about/index.php">About NN/G</a></li>
                </ul>
            </nav>
            <nav class="secondary-nav small-12 medium-12 large-12 columns">
                
<ul class="inline-list">
    <li id="nav-people"><a href="../../../people/index.php"><span>People</span></a></li>
    <li id="nav-whynng"><a href="../../../about/why-nng/index.php"><span>Why NN/g?</span></a></li>
    <li id="nav-contact"><a href="../../../about/contact/index.php"><span>Contact</span></a></li>
    <li id="nav-news"><a href="../../index.php"><span>News</span></a></li>
    <li id="nav-history"><a href="../../../about/history/index.php"><span>History</span></a></li>
    <li id="nav-books"><a href="../../../books/index.php"><span>Books</span></a></li>
</ul>

            </nav>
        </div>
    </header>
    <div class="l-content"><span id="preventClose" style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776; Menu</span>
    


<div class="row">
    <div class="small-12 medium-4 large-4 columns offset-by-eight">
    </div>
</div>
<div class="row">
    <div class="small-12 medium-3 large-3 columns l-navrail news-rail hide-for-small-only">
        <h1><a href="../../index.php">All News Types</a></h1>
        <ul class="no-bullet">
            
                
                    <li><a href="../../type/announcements/index.php">Announcements (52)</a></li>
                
            
                
                    <li><a href="../../type/press-mentions/index.php">Interviews &amp; Press (225)</a></li>
                
            
        </ul>
    </div>
    <div class="small-12 medium-9 large-9 columns">
        <article class="news_detail">
            <div class="l-section-intro">
                <header>
                    <h1>NN/g News</h1>
                    <h2>Hong Kong UX Conference: Design and Usability Challenges in a Cultural Portal</h2>
                    <p>February 22, 2001</p>
                </header>
            </div>
            <div class="l-subsection">
                <p>
	<i>By Marie Tahir </i></p>
<table border="0" cellspacing="6">
	<tbody>
		<tr>
			<td>
				<img alt="Jakob Nielsen meeting with attendees after his talk" src="http://media.nngroup.com.s3.amazonaws.com/media/editor/2012/11/30/hk-jakobandattendees.jpg" style="border-width: 0px; border-style: solid; width: 235px; height: 150px;" /></td>
			<td>
				<img alt="Tog speaking at the Main Event" src="http://media.nngroup.com.s3.amazonaws.com/media/editor/2012/11/30/hk-togexplains.jpg" style="border-width: 0px; border-style: solid; width: 200px; height: 150px;" /></td>
		</tr>
		<tr>
			<td valign="top">
				<em>Jakob Nielsen meets with attendees<br />
				after his talk </em></td>
			<td valign="top">
				<em>Tog speaking at the Main Event<br />
				in Hong Kong</em></td>
		</tr>
	</tbody>
</table>
<p>
	HONG KONG, China, February 22, 2001. Hong Kong&#39;s diversity makes for design and usability challenges that extend beyond website localization issues. Attendees at the Hong Kong World Tour event told us that interacting with so many different cultures means <strong> localizing your communication style</strong>, as well as your product. Usability consultants and designers described how they must use different tactics to motivate clients and colleagues with different cultural backgrounds to adopt user-centered research and design methodology.</p>
<p>
	We had a lively lunch discussion one day on these different tactics for different cultures. Attendees said that <strong> Hong Kong </strong> clients tend to be &quot;instruction based, and you need to get management mandate, give clear tasks--[the clients] don&#39;t do anything without manager&#39;s approval.&quot; <strong> Australians </strong> are &quot;very individualistic,&quot; and although on the one hand they&#39;re very social and fun loving, it&#39;s hard to get them to follow standards or to change their opinions. One evening you can be out buying them beers, but when it comes to trying to negotiate design changes, attendees reported meeting resistance. Attendees said finds they make most progress with <strong> Malaysian </strong> clients, who are &quot;very accommodating&quot; to change, as well as with clients from the <strong> Philippines</strong>, who &quot;deal well with value-added service-if you position yourself as someone there to help them, and be very understanding to their issues.&quot; In <strong> Taiwan</strong>, attendees said they need to spend more time with clients and build relationships. Taiwanese clients respond better to suggestions for change from people they know &quot;They will do it, because we have a relationship.&quot; In <strong> India</strong>, &quot;It&#39;s all about respect. You need to show [Indian clients] respect to get things done.&quot;</p>
<table align="right" border="0">
	<tbody>
		<tr>
			<td>
				<img alt="Photo of Daniel Szuc, Apogee" src="http://media.nngroup.com.s3.amazonaws.com/media/editor/2012/11/30/hk-danielszuc.jpg" style="border-width: 0px; border-style: solid; width: 75px; height: 100px;" /></td>
		</tr>
		<tr>
			<td>
				<em>Daniel Szuc,<br />
				<small>Apogee </small> </em></td>
		</tr>
	</tbody>
</table>
<p>
	Several attendees mentioned that <strong> Hong Kong is an especially fast-paced, deadline driven city</strong>. Daniel Szuc, IT Manager of Apogee Communications Internet Division, told me, &quot;The speed to get stuff done goes against quality. That&#39;s why I&#39;m implementing [usability practices] at design phase&hellip; I implement by stealth.&quot; Daniel said that the extreme time pressure, coupled with a loose infrastructure and lack of user-centered standards and processes means things get done there in an ad-hoc way. Daniel is teaching the staff at his company and his clients how to integrate user-centered design processes.</p>
<table align="left" border="0">
	<tbody>
		<tr>
			<td>
				<a href="http://nngroup.com.s3.amazonaws.com/media/editor/2012/11/30/hk-joanlui.jpg"><img alt="Photo Joan Lui, Lemon" src="http://media.nngroup.com.s3.amazonaws.com/media/editor/2012/11/30/hk-joanlui_.jpg" style="border-width: 0px; border-style: solid; width: 75px; height: 100px;" /> </a></td>
		</tr>
		<tr>
			<td>
				<em>Joan Lui,<br />
				<small>Lemon </small> </em></td>
		</tr>
	</tbody>
</table>
<p>
	Joan Lui, Deputy Director of Interface Developer at Lemon, a design company, agreed with Daniel. &quot;Right now, usability is not a main issue in Hong Kong,&quot; she said. She told me that her company usually starts by doing a review of the client&#39;s site to identify issues, and then they bring users in to test the redesign: &quot;When the clients come to us, they know they have a problem, then we need to test [afterwards] to make sure the redesign is good.&quot; Joan says one of her most difficult challenges is convincing her clients to make changes like getting rid of confusing technical terms on their sites.</p>
<table align="right" border="0">
	<tbody>
		<tr>
			<td>
				<img alt="Photo of Pierre Lavigne, ewatchfactory" src="http://media.nngroup.com.s3.amazonaws.com/media/editor/2012/11/30/hk-pierrelavigne.jpg" style="border-width: 0px; border-style: solid; width: 75px; height: 100px;" /></td>
		</tr>
		<tr>
			<td>
				<em>Pierre Lavigne,<br />
				<small>ewatchfactory </small> </em></td>
		</tr>
	</tbody>
</table>
<p>
	Pierre Lavigne, CTO of ewatchfactory, talked to me about the <strong> pressures of customizing for clients&#39; sites in very short timeframes</strong>. Ewatchfactory produces custom watches for their customers&#39; websites. Users can go to a website like CBS&#39;s site for the popular <strong> American </strong> television show &quot;Survivor,&quot; and create a custom wristwatch that includes elements from the television show. Other clients include a <strong> Mexican </strong> children&#39;s television show, &quot;Burundis,&quot; and the Sierra Club. Pierre says they usually only have one week to design their portion of the website, which lets users design their custom watch: &quot;each time it&#39;s new. We have to adapt the interface each time to the look and culture of the site we will stick with.&quot; Pierre said his biggest need is to identify a &quot;common basis for each site&hellip; a general interface for everybody&quot; that he can then customize for each site. Until recently, he thought this was possible to do within house and without user input: &quot;I was waiting before for feedback on the site once it&#39;s out there&hellip; I thought we had the resources to do it right, but I&#39;ve changed my mind.&quot; Pierre is a recent devotee of Jakob Nielsen, and was excited to be at the conference to get new ideas on how to incorporate user feedback into their design process.</p>
<table align="left" border="0">
	<tbody>
		<tr>
			<td>
				<img alt="Photo of Yan Sham-Shakleton, Asia Online" src="http://media.nngroup.com.s3.amazonaws.com/media/editor/2012/11/30/hk-yanshamshackleton.jpg" style="border-width: 0px; border-style: solid; width: 75px; height: 100px;" /></td>
		</tr>
		<tr>
			<td>
				<em>Yan Sham-<br />
				Shackleton,<br />
				<small>Asia Online </small> </em></td>
		</tr>
	</tbody>
</table>
<p>
	Yan Sham Shackleton, who is managing the redesign of Asia Online&#39;s website, faces the challenge of trying to get a &quot;cohesive web presence&quot; from a website that has 35 separate products and 10 country sites. Yan says she has to work with <strong> branches of her company that were acquired as separate companies</strong>, and she finds this compounds cultural differences and makes it difficult to be an agent of change: &quot;People are resistant to change if they&#39;ve been bought.&quot; Yan also mentioned that she&#39;s most successful communicating with her clients by phone, rather than impersonal email. She thinks this is a gender issue, since she has a name that could be male or female and says she gets misinterpreted in emails. For example, clients don&#39;t mind more frequent communication from a woman, but if they think she&#39;s a man, they &quot;think she&#39;s an annoying guy.&quot;</p>
<table align="right" border="0">
	<tbody>
		<tr>
			<td>
				<img alt="Photo of Linda Cheng" src="http://media.nngroup.com.s3.amazonaws.com/media/editor/2012/11/30/hk-lindacheng.jpg" style="border-width: 0px; border-style: solid; width: 75px; height: 100px;" /></td>
		</tr>
		<tr>
			<td>
				<em>Linda Cheng </em></td>
		</tr>
	</tbody>
</table>
<p>
	Some design challenges are the same the world over. When I asked some attendees to tell me their <strong> one wish for Web design</strong>, Daniel Szuc instantly responded, &quot; <strong> no more pop-up windows </strong> ... if there&#39;s a user reason, it&#39;s okay, but if it&#39;s just to do cool Javascript, it&#39;s not.&quot; &quot;APUWs,&quot; laughingly agreed Linda Cheng, from Hong Kong, &quot;Annoying Pop Up Windows!&quot;</p>
<p>
	&nbsp;</p>
<table border="0" cellspacing="6">
	<tbody>
		<tr>
			<td>
				<img alt="Brenda Laurel considers a question from Yan Sham-Shakleton" src="http://media.nngroup.com.s3.amazonaws.com/media/editor/2012/11/30/hk-brendaandattendees.jpg" style="border-width: 0px; border-style: solid; width: 220px; height: 150px;" /></td>
		</tr>
		<tr>
			<td valign="top">
				<em>Brenda Laurel considers a question<br />
				from Yan Sham-Shackleton</em></td>
		</tr>
	</tbody>
</table>
<h3>
	Press Coverage of Hong Kong Conference</h3>
<p>
	<strong>In English </strong></p>
<ul>
	<li>
		<a href="http://appledaily.atnext.com/adotpl/ado_article.cfm?main_section_id=15307&amp;showdate=20010327&amp;article_id=292830"><cite>New </cite> </a></li>
	<li>
		<a href="http://www.gorillasia.com/tc/readarticle?id=2158&amp;page=1"><cite>Gorilla Asia </cite> </a></li>
	<li>
		<a href="http://asia.cnn.com/2001/WORLD/asiapcf/east/02/23/web.usability/index.php" title="Web usability guru pitches better design in Asia"><cite>CNN Asia </cite> </a> <!-- &lt;/li&gt;
				the following link is broken, the article is still available in the archives, but it now costs to view.  I'm commenting this link away for now.  &lt;li&gt;&lt;a href="http://technology.scmp.com/internet/ZZZ6YQ2SNJC.php" title="Regional sites need to please users"&gt;&lt;cite&gt;South China Morning Post&lt;/cite&gt;&lt;/a&gt; (access requires free registration)--></li>
</ul>
<p>
	<strong>In Chinese </strong></p>
<ul>
	<li>
		<a href="http://hongkong.cnet.com/business/eachstory.asp?story_id=200102280078&amp;pageno=1"><cite>CNET </cite> </a></li>
</ul>

            </div>
        </article>
    </div>
</div>


    </div>
    




<footer id="page-footer">
    <div class="row hide-for-print">
        <div class="column small-12 medium-6">
            <section class="footer-popular">
                
                <h1>Popular Topics</h1>
                <div style="float: left;  width: 15em; display: block;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../../topic/applications/index.php">Application Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../../topic/e-commerce/index.php">E-Commerce Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../../topic/eyetracking/index.php">Eyetracking </a></li>
	<li style="padding-bottom: .5em;"><a href="../../../topic/human-computer-interaction/index.php">Human Computer Interaction </a></li>
	<li style="padding-bottom: .5em;"><a href="../../../topic/information-architecture/index.php">Information Architecture </a></li>
	<li style="padding-bottom: .5em;"><a href="../../../topic/intranets/index.php">Intranet Design </a></li>
	<li><a href="../../../topic/interaction-design/index.php">Interaction Design </a></li>
</ul>
</div>

<div style="float: left;  width: 15em; display: block; list-style: none;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../../topic/mobile-and-tablet-design/index.php">Mobile and Tablet Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../../topic/credibility-persuasion/index.php">Persuasive Design</a></li>
	<li style="padding-bottom: .5em;"><a href="../../../topic/research-methods/index.php">Research Methods</a></li>
	<li style="padding-bottom: .5em;"><a href="../../../topic/user-testing/index.php">User Testing </a></li>
	<li style="padding-bottom: .5em;"><a href="../../../topic/web-usability/index.php">Web Usability </a></li>
	<li style="padding-bottom: .5em;"><a href="../../../topic/writing-web/index.php">Web Writing &amp; Content Strategy </a></li>
</ul>
</div>

<p>&nbsp;</p>

                
            </section>
        </div>

        <div class="column small-12 medium-6">
            <div class="row">
                <div class="column small-6">
                    <section class="footer-about">
                        <h1>Follow Us</h1>
                        <ul>
	<li><a href="../../../articles/subscribe/index.php">Subscribe to our weekly UX newsletter</a>,&nbsp;Jakob Nielsen&#39;s Alertbox</li>
	<li>Visit Don Norman&#39;s website, <a href="http://www.jnd.org/"> www.jnd.org </a></li>
	<li>Visit Bruce Tognazzini&#39;s website, <a href="http://www.asktog.com/"> www.asktog.com </a></li>
	<li style="padding-top: 6px;"><a class="twitter-follow-button" data-show-count="false" data-size="large" href="https://twitter.com/nngroup"><img alt="Follow @nngroup on Twitter" height="20" src="https://media.nngroup.com/media/editor/2016/12/30/twitter-follow-button.png" style="width: 117px; height: 20px;" width="117" /></a></li>
	<li style="padding-top: 6px;"><img alt="" height="14" src="https://media.nngroup.com/media/editor/2016/07/26/feed-icon-14x14.png" style="vertical-align:text-top;width:14px;margin-right:.25em;" width="14" /><a href="../../../feed/rss/index.php">Subscribe to our RSS feed</a></li>
</ul>

                    </section>
                </div>

                <div class="column small-6">
                    <section class="footer-about right-column">
                        <h1>About
</h1>
                        <ul class="no-bullet">
	<li><a href="../../../about/contact/index.php">Contact information</a></li>
	<li><a href="../../../about/index.php">About Us</a></li>
	<li><a href="../../../about/why-nng/index.php">Why NN/g? </a></li>
	<li><a href="../../../return-policy/index.php">Return Policy</a></li>
</ul>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="copyright column small-12">
            <p><a href="../../../copyright-and-reprint-info/index.php">Copyright</a> &copy; 1998-2017 Nielsen Norman Group, All Rights Reserved.</p>
        </div>
    </div>
</footer>


    


        <script src="https://media.nngroup.com/static/vendor/fastclick/js/fastclick.min.js"></script>
        <script src="https://media.nngroup.com/static/vendor/foundation/js/foundation.min.js"></script>
        <script src="http://media.nngroup.com/static/js/app.js"></script>
        <script src="http://media.nngroup.com/static/js/scroll-to-fixed.js"></script><script>var externalLinks = document.querySelectorAll('a[href^="http"]');for (var i = externalLinks.length-1; i >= 0; i--) {externalLinks[i].addEventListener("click", function(event) { alert("External links are disabled for this test."); event.preventDefault(); }, false);}var mailLinks = document.querySelectorAll('a[href^="mailto"]');for (var i = mailLinks.length-1; i >= 0; i--) {mailLinks[i].addEventListener("click", function(event) { alert("Email links are disabled for this test."); event.preventDefault(); }, false);}var externalForms = document.querySelectorAll('form');for (var i = externalForms.length-1; i >= 0; i--) {externalForms[i].addEventListener("submit", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false); externalForms[i].addEventListener("click", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false);}</script>

        <script>
            
            $(function() {
                $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
                $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
            });
        </script>

        


        
        <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1072699960;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072699960/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
    </body>

<!-- Mirrored from www.nngroup.com/news/item/design-challenges-cultural-world-tour-hong-kong/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:13:19 GMT -->
</html>
