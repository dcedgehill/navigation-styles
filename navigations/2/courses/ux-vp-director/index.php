<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->





    
<!-- Mirrored from www.nngroup.com/courses/ux-vp-director/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:10:30 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1026.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"beacon":"bam.nr-data.net","queueTime":15,"applicationTime":1660,"agent":"","licenseKey":"1a30ef9931","errorBeacon":"bam.nr-data.net","applicationID":"2048992","transactionName":"NFxSMRBUX0EDUkIPDA0WdhAMVkVbDV8ZBQwWS0MAERtHWwdGRVwgDExCFgdxVEYDWFpIBAZN"}</script>
        <title>The UX VP/Director: Full-Day Course at the NNG Conference</title><meta property="og:title" content="The UX VP/Director: Full-Day Course at the NNG Conference" />
  
        
        <meta name="description" content="Working successfully with executives requires a different, concise, business-focuses articulation of and commitment to user experience. UX executives can drive business objectives and deliver on corporate goals.">
        <meta property="og:description" content="Working successfully with executives requires a different, concise, business-focuses articulation of and commitment to user experience. UX executives can drive business objectives and deliver on corporate goals." />
        
  
        
	
        

        

        <meta name="robots" content="noarchive">
        <meta name="viewport" content="width=device-width">

        <link rel="search" type="application/opensearchdescription+xml" title="NN/g Search" href="../../opensearch.xml" />
        <link rel="shortcut icon" href="http://media.nngroup.com/static/img/favicon.ico" />

        <link rel="stylesheet" href="https://media.nngroup.com/static/vendor/font-awesome/css/font-awesome.min.css">

        
        <link rel="stylesheet" href="https://media.nngroup.com/static/css/7232a960f1df.css" type="text/css" />
        <link rel="stylesheet" href="http://fast.fonts.com/t/1.css?apiType=css&amp;projectid=c5846260-9def-46ec-9407-0f1349bd2d64" type="text/css">

        <script src="https://media.nngroup.com/static/vendor/modernizr/js/modernizr.min.js"></script>

        <script type="text/javascript" src="https://media.nngroup.com/static/js/3283be5def74.js"></script>
        

        

        
        
        <!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=229034,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('http://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->


<!-- Google Analytics Tracking Code for http://nngroup.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23132681-1', 'nngroup.com');
  ga('require', 'linkid', 'https://www.nngroup.com/courses/ux-vp-director/linkid.js');
  ga('require', 'displayfeatures');

// Optimizely Universal Analytics Integration
 window.optimizely = window.optimizely || [];
 window.optimizely.push("activateUniversalAnalytics");

  ga('send', 'pageview');

</script>
<script>
/**
* Function to track a click on an outbound link in Google Analytics.
*/
function trackOutboundLink(label, url) {
    if(window.ga && ga.loaded) {
        ga('send', 'event', 'outbound', 'click', label, {'hitCallback': function(){document.location = url;}
        });
    } else {
        document.location = url;
    }
}
/**
* Function to track a click on a link in the article announcement area.
*/
$('#articleAnnouncement a').on('click', function(){
  ga('send', 'event', 'link', 'click', 'articleAnnouncement');
});
</script>

<script type="text/javascript">
 setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2421.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



<!-- Hotjar Tracking Code for http://nngroup.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:121605,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

        <script src="https://media.nngroup.com/static/js/analytics/track_outbound_link.js"></script>
        
        
        
    </head>
    <body class="location-training location-course-detail course-detail">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
    <header id="page-header">
        <div id="cart" class="shopping-cart mobile-cart show-for-small-down"></div>
        <div class="row">
            <div id="masthead" class="masthead small-12 medium-7 column">

                <!-- The button to toggles menu on small screen -->

                <h1><a href="../../index.php"><img src="http://media.nngroup.com/static/img/logo-wordmark.png" width="74" height="30"> Nielsen <span class="norman">Norman</span> Group</a></h1>
                <p class="tagline show-for-medium-up">Evidence-Based User Experience Research, Training, and Consulting</p>

                <!-- The button to toggles form search on small screen -->


                <div style="clear: right;width:  100%;height: 1px;"></div><?php define('ROOT_PATH',$_SERVER["DOCUMENT_ROOT"] . '/apps/navigation_styles/navigations/2'); ?><?php include(ROOT_PATH)."/navigation.php"; ?>

                <!-- Drop down menu on small screen -->
                <div id="hideNav" class="show-for-small-down">
                  
                  <ul class="menu-content">
                    <li><a href="../../index.php">Home</a></li>
                    <li class="has-submenu">
                      <a>Training</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../training/index.php">Overview</a></li>
                        
<li id="nav-ux-conference"><a href="../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../online-seminars/index.php"><span>Online Seminars</span></a></li>

                      </ul>
                    </li>
                    <li class="has-submenu">
                      <a>Consulting</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
  <li><a href="../../consulting/index.php">Overview</a></li>
  
  <li><a href="../../consulting/benchmarking/index.php">Benchmarking</a></li>
  
  <li><a href="../../consulting/expert-design-review/index.php">Expert Design Review</a></li>
  
  <li><a href="../../consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
  
  <li><a href="../../consulting/usability-testing/index.php">Usability Testing </a></li>
  
  <li><a href="../../consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
  
  <li><a href="../../in-house-training/index.php">In-House Training</a></li>
</ul>

                    </li>

                    <li><a href="../../reports/index.php">Reports</a></li>
                    <li><a href="../../articles/index.php">Articles</a></li>
                    <li class="has-submenu"><a>About NN/G</a><i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../about/index.php">Overview</a></li>
                        <li><a href="../../people/index.php">People</a></li>
                        <li><a href="../../about/why-nng/index.php">Why NN/g?</a></li>
                        <li><a href="../../about/contact/index.php">Contact</a></li>
                        <li><a href="../../news/index.php">News</a></li>
                        <li><a href="../../about/history/index.php">History</a></li>
                        <li><a href="../../books/index.php">Books</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                </div>

                <hr class="header-divider show-for-small-only"/>


            </div>
       </div>

        <div id="hideNav">            <nav class="primary-nav small-12 column">
                <ul>
                    <li><a class="navitem" id="nav-home" href="../../index.php">Home</a></li>
                    <li><a class="navitem" id="nav-training" href="../../training/index.php">Training</a></li>
                    <li><a class="navitem" id="nav-consulting" href="../../consulting/index.php">Consulting</a></li>
                    <li><a class="navitem" id="nav-reports" href="../../reports/index.php">Reports</a></li>
                    <li><a class="navitem" id="nav-articles" href="../../articles/index.php">Articles</a></li>
                    <li><a class="navitem" id="nav-about" href="../../about/index.php">About NN/G</a></li>
                </ul>
            </nav>
            <nav class="secondary-nav small-12 medium-12 large-12 columns">
                
  <ul class="inline-list">
  
<li id="nav-ux-conference"><a href="../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../online-seminars/index.php"><span>Online Seminars</span></a></li>

</ul>


            </nav>
        </div>
    </header>
    <div id="main" class="l-content" onclick="closeNav(event)"><span id="preventClose" style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776; Menu</span>
    

<article class="course-detail">
    <div class="row">
        
        <p class="small-12 column">
            
            Full day training course
            
        </p>
    </div>
    <div class="row">
        <div class="l-section-intro small-12 medium-8 end column">
            <header>
                <h1>The UX VP/Director</h1>
                <h2>Leading UX at the Executive Level</h2>
            </header>
             <div class="usercontent">
                
            </div>
           
        </div>
    </div>

    <div class="row">
        <div class="l-course-content small-12 medium-8 column">
            <div class="l-usercontent mainCourseContent">
                <p>For years, User Experience Professionals (UXP&rsquo;s) have wanted a seat at the leadership table. Now it&rsquo;s happening more frequently. But many UX VP&rsquo;s and directors are illl equipped to influence, communicate, and succeed at the executive level.&nbsp;</p>

<p>Working successfully with executives requires a different, concise, business-focuses articulation of and commitment to user experience. UX executives can drive business objectives and deliver on corporate goals.</p>

<blockquote>&quot;Nancy presented strategies for becoming a VP/Director of UX in a holistic, engaging and practical matter. This is one of the most fulfilling training sessions that I have attended.&quot;</blockquote>

<p>Amy Lindsey<br />
Winston-Salem</p>

            </div>
            
                <div class="l-usercontent mainCourseContent collapsable-content">

                    <h1 class="collapse-link show-for-small-down">Who Should Attend</h1>
                    <h2 class="show-for-medium-up">Who Should Attend</h2>

                    <div class="collapsable">
                        <p>This course is designed to help UX professionals, managers, and executives engage at the highest levels of effectiveness and influence. It is designed for people who are already UX executives and people striving to be UX executives, including the following:</p>

<ul>
	<li>UX VPs, directors, and managers with a background in:
	<ul>
		<li>Interaction design</li>
		<li>Visual design</li>
		<li>User research</li>
		<li>Content strategy</li>
	</ul>
	</li>
	<li>Non-UX managers, directors, and VPs tasked with leading user experience strategies in areas of:
	<ul>
		<li>Marketing and brand management</li>
		<li>Engineering and development</li>
		<li>Customer care</li>
		<li>Product management</li>
	</ul>
	</li>
	<li>UX and non-UX professionals who need to or are striving to become UX executives</li>
</ul>

                    </div>

                </div>
            

            
            <div class="l-usercontent mainCourseContent collapsable-content">

                <h1 class="collapse-link show-for-small-down">What You&#39;ll Learn</h1>
                <h2 class="show-for-medium-up">What You&#39;ll Learn</h2>

                <div class="collapsable">
                    <ul>
	<li>Communicate UX in ways executives understand, appreciate, and support</li>
	<li>Strengthen your business acumen and UX rationale</li>
	<li>Assess and navigate corporate cultures</li>
	<li>Secure champions and build key partnerships</li>
	<li>Succeed as an &ldquo;agent of change&rdquo; at the highest levels of the organization</li>
	<li>Avoid common mistakes UX professionals make in managing expectations, delivering results, and building credibility at the executive level</li>
</ul>

                </div>

            </div>
            

            
            <div class="l-usercontent mainCourseContent collapsable-content">

                <h1 class="collapse-link show-for-small-down">Topics Covered</h1>
                <h2 class="show-for-medium-up">Topics Covered</h2>

                <div class="collapsable">
                    <ul>
	<li>The role of UX directors and VPs across different industries and companies</li>
	<li>UX executives as catalysts for the creation of user-centered organizations</li>
	<li>Emerging UX success measures and key performance indicators, (KPIs), that guide successful UX companies</li>
	<li>Best practices for communicating UX at the executive level</li>
	<li>Tips for becoming a UX executive</li>
</ul>

                </div>

            </div>
            

            
            <div class="l-usercontent mainCourseContent collapsable-content">

                <h1 class="collapse-link show-for-small-down">Format</h1>
                <h2 class="show-for-medium-up">Format</h2>

                <div class="collapsable">
                    <p>This course has been designed to help attendees learn, analyze, assess, and create plans specific to their organization and situation. Short periods of lecture will set the context for hands-on exercises, planning, and group discussions.&nbsp;</p>

<p>By the end of the class, you will have your own unique UX executive &ldquo;game plan.&rdquo;</p>

                </div>

            </div>
            

            

            

            

            
              



<div class="row certification-info">
  <section class="l-subsection collapsable-content small-12 medium-8 columns">
    <h1 class="collapse-link show-for-small-down">UX Certification Credit</h1>
    <h2 class="hide-for-small-down">UX Certification Credit</h2>
    <div class="collapsable">
      <p>
      Attending this course and passing the exam earns <strong>1 UX Certification credit</strong>, which also counts towards the optional <a href="../../ux-certification/ux-management-specialty/index.php">UX Management Specialty</a>.
      </p>

      

      <p>
      Learn more about NN/g's <a href="../../ux-certification/index.php">UX Certification</a> Program.
      </p>
    </div>
  </section>
  <div class="hide-for-small-down medium-4 columns badge">
    <img alt="UX Certification Badge from Nielsen Norman Group" src="https://media.nngroup.com/nng-uxc-badge.png">
  </div>
</div>

            

            
<div class="l-usercontent mainCourseContent collapsable-content participant-comments">
  <h1 class="collapse-link show-for-small-down">Participant Comments</h1>
  <h2 class="show-for-medium-up">Participant Comments</h2>
  <div class="collapse-content collapsable">
    <blockquote>&quot;The course simultaneously empowered me to bring change to my organization and prepared me for the challenges I will face.&quot;</blockquote>

<p>Libby Maurer, Widen Enterprises</p>

<blockquote>&quot;If you are wondering or struggling with how to influence your organization at a higher level, this class will show you some powerful strategies. Highly recommend.&quot;</blockquote>

<p>Marlene Mussler-Wright, NIC/egov/Access Idaho</p>

<blockquote>&quot;Too many take-aways to list. Love the PDF links to resources. I feel like I could leave today and be an agent of change. Validation that I&#39;ve chosen the right career&nbsp; &#39;experience&#39;.&quot;</blockquote>

<p>Toni Pastorino, SteelBrick</p>

<blockquote>&quot;Nancy has such vast experiences in the field. Loads of information she is happy to share, and advice on comments made in class with interest and great energy.&quot;</blockquote>

<p>Solara Crawford, RallyApp</p>

    <ul id="commentAccordian" class="accordion" data-accordion>
      <li class="accordion-navigation">
        <a href="#cc" class="closed" id="commentCollapseLink">More Participant Comments</a>
        <div id="cc" class="content more-comments">
          <blockquote>&quot;I&#39;ve been looking for 2 years now for a course that would cover what my current challenges are within the UI/UX space. Nancy is such an engaging apeaker. I grew so much from what she shared. Thank you for sharing your real world experiences.&quot;</blockquote>

<p>&nbsp;</p>

<blockquote>&quot;I would say it&#39;s jampacked with tons of helpful leadership tools and strategies &mdash; so many takeaways!&quot;</blockquote>

<p>Chelsey Delaney, PPFA</p>

<blockquote>&quot;Nancy is like the sea turtle in <em>Finding Nemo</em>. She is wise in her experience and willing to share to make us better fish dads&hellip;.er&hellip;.UX leaders.&quot;</blockquote>

<p>Chris Kutt, DevFacto</p>

<blockquote>&quot;Great course with excellent real life examples &mdash; would definitely recommend to anyone looking to educate and persuade people about UX.&quot;</blockquote>

<p>Venetia Casely, Money Advice Service,</p>

<blockquote>&quot;Loved it! I only wish I had her charismatic &amp; enthusiastic presentation skills.&quot;</blockquote>

<p>Jim Bull, Phoenix Contact</p>

<blockquote>&quot;Great to see our UCD tools put under the lens of leadership &mdash; I can use what I already know in new ways.&quot;</blockquote>

<p>Len Beasley, Triage</p>

<blockquote>&quot;I just wished I did this course before. So many questions were answered!&quot;</blockquote>

<p>Luciana Terceiro, S&atilde;o Paulo, Brazil</p>

<blockquote>&quot;Extremely useful and insightful. My company asked me to evaluate this as well and I&#39;ll definitely be recommending it. Thank you!&quot;</blockquote>

<p>Jon Anderson, Appirio</p>

        </div>
      </li>
    </ul>
  </div>
</div>
<script>
  $('#commentAccordian').on('toggled', function (event, accordion) {
    $link = $('#commentCollapseLink');
    $link.toggleClass('closed');
    if($link.hasClass('closed')) {
      $link.text('More Participant Comments');
    } else {
      $link.text('Hide Participant Comments');
    }
  });
</script>



            <div class="l-usercontent">
                
            </div>
            <div class="usercontent">
                
            </div>
            
            <section class="course-instructors l-subsection collapsable-content">
                <h1 class="show-for-medium-up">Instructor</h1>
                <h1 class="show-for-small-down collapse-link">Instructor</h1>
                <div class="collapsable">
                    
                    <article class="course-instructor ">
                        <h2>Nancy Dickenson</h2>
                        <div class="row">
                            <div class="small-12 medium-3 column">
                                <img src="https://media.nngroup.com/media/people/photos/IMG_8763_copy-retouched-luminous-square-800.jpg.200x200_q95_autocrop_crop_upscale.jpg">
                            </div>
                            <div class="small-12 medium-9 column">
                                <div class="usercontent"><p>Nancy Dickenson has been a leading user experience expert for over 25 years. Nancy began her career at Apple Computer in 1988. She went on to manage, lead, and build award winning, user-centered, product design and research teams at ABCNews, HarperCollins, Scient, eBay, and FirstGiving. As the head of eBay&#39;s Global User Experience Design and Research Team, Nancy spent 5 years leading over 100 user experience professionals. Nancy was an early employee of Scient Corporation where she founded and built the Customer Experience Division &mdash; a group of over 450 branding, graphic design, content, user interface, and user research employees in 13 offices worldwide.</p>

<p>Nancy founded Dickenson, LLc in 2010.&nbsp; Since then, Nancy has created successful user-centered strategies and workshops for user experience teams worldwide.&nbsp; She also provides individual coaching for UX executives.&nbsp; Recent clients include: Intuit, eBay, PayPal, Wells Fargo, MasterCard, and Financial Engines.</p>

<p>Nancy is also Executive In Residence for Bentley University&rsquo;s Human Factor and Information Design Graduate Program.</p>
</div>
                            </div>
                        </div>
                    </article>
                    
                </div>
            </section>
        </div>

        <div class="l-event-sidebar small-12 medium-4 l-subsection column sidebar">
          <div class="collapsable-content">
            <h1 class="show-for-small-down collapse-link">Course Dates</h1>
            <div class="l-subsection course-scheduled collapsable">
              <div  style="display: table">
                <h2 class="show-for-medium-up">Course Dates:</h2>
                <ul class="no-bullet ">
                  
                  
                    
                    <li><a href="../../training/london/index.php">London</a> March 25, 2017</li>
                    
                    
                    
                    <li><a href="../../training/washington-dc/index.php">Washington DC</a> May 14, 2017</li>
                    
                    
                    
                    <li><a href="../../training/vancouver/index.php">Vancouver</a> August 13, 2017</li>
                    
                    
                  <li><a href="../../in-house-training/index.php">In-House Training Options</a></li>
                </ul>
              </div>
            </div>
          </div>

          <div class="show-for-small-down">
            



<div class="collapsable-content">
  <section class="l-subsection related-images">
    <h2 class="hide-for-small-down">Scenes From Past Trainings</h2>
    <h1 class="show-for-small-down collapse-link">Scenes From Past Trainings</h1>
    <div class="collapsable">
    <p>At the UX Conference, you get a full day of in-depth training with expert instructors. Learn more about <a href="../../training/why-attend/index.php">why you should attend</a>.</p>

    
      
        
          
            <img srcset="https://media.nngroup.com/media/files/images/IMG_1090_copy_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale@2x.jpg 2x" src="https://media.nngroup.com/media/files/images/IMG_1090_copy_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale.jpg" alt="">
          
        
      
    
      
        
          
            <img srcset="https://media.nngroup.com/media/files/images/IMG_7582_copy_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale@2x.jpg 2x" src="https://media.nngroup.com/media/files/images/IMG_7582_copy_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale.jpg" alt="">
          
        
      
    
      
        
          
            <img srcset="https://media.nngroup.com/media/files/images/IMG_8464_copy_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale@2x.jpg 2x" src="https://media.nngroup.com/media/files/images/IMG_8464_copy_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale.jpg" alt="">
          
        
      
    
      
        
          
            <img srcset="https://media.nngroup.com/media/files/images/IMG_4730_copy_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale@2x.jpg 2x" src="https://media.nngroup.com/media/files/images/IMG_4730_copy_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale.jpg" alt="">
          
        
      
    
    </div>
  </section>
</div>


          </div>

            <hr class="show-for-small-down"/>
            <div class="collapsable-content">
                <div class="l-subsection course-categories">
                    <h2 class="show-for-medium-up">Browse other courses by category:</h2>
                    <h1 class="hide-for-medium-up collapse-link">Browse Other Courses by Category</h1>
                    <div class="collapsable">
                        <ul class="no-bullet">
                            
                            <li><a href="../topic/agile/index.php">Agile</a></li>
                            
                            <li><a href="../topic/analytics-and-metrics/index.php">Analytics &amp; Metrics</a></li>
                            
                            <li><a href="../topic/applications/index.php">Application Design</a></li>
                            
                            <li><a href="../topic/content-strategy/index.php">Content Strategy</a></li>
                            
                            <li><a href="../topic/design-patterns/index.php">Design Patterns</a></li>
                            
                            <li><a href="../topic/ux-design-process/index.php">Design Process</a></li>
                            
                            <li><a href="../topic/e-commerce/index.php">E-commerce</a></li>
                            
                            <li><a href="../topic/human-computer-interaction/index.php">Human Computer Interaction</a></li>
                            
                            <li><a href="../topic/ux-ideation/index.php">Ideation</a></li>
                            
                            <li><a href="../topic/information-architecture/index.php">Information Architecture</a></li>
                            
                            <li><a href="../topic/interaction-design/index.php">Interaction Design</a></li>
                            
                            <li><a href="../topic/ux-management/index.php">Management</a></li>
                            
                            <li><a href="../topic/mobile-and-tablet-design/index.php">Mobile &amp; Tablet</a></li>
                            
                            <li><a href="../topic/navigation/index.php">Navigation</a></li>
                            
                            <li><a href="../topic/persuasive-design/index.php">Persuasive Design</a></li>
                            
                            <li><a href="../topic/prototyping/index.php">Prototyping</a></li>
                            
                            <li><a href="../topic/psychology-and-ux/index.php">Psychology and UX</a></li>
                            
                            <li><a href="../topic/research-methods/index.php">Research Methods</a></li>
                            
                            <li><a href="../topic/social-media/index.php">Social Media</a></li>
                            
                            <li><a href="../topic/strategy/index.php">Strategy</a></li>
                            
                            <li><a href="../topic/user-testing/index.php">User Testing</a></li>
                            
                            <li><a href="../topic/ux-teams/index.php">UX Teams</a></li>
                            
                            <li><a href="../topic/visual-design/index.php">Visual Design</a></li>
                            
                            <li><a href="../topic/web-usability/index.php">Web Usability</a></li>
                            
                            <li><a href="../topic/writing-web/index.php">Writing for the Web</a></li>
                            
                        </ul>
                        <h2>Recommended Courses for:</h2>
                        <ul class="no-bullet">
                            
                            <li><a href="../topic/managers/index.php">For Managers</a></li>
                            
                            <li><a href="../topic/new-user-experience/index.php">New to User Experience</a></li>
                            
                            <li><a href="../topic/refresher-ux-pros/index.php">Refresher for UX Pros</a></li>
                            
                            <li><a href="../topic/transitioning-another-discipline/index.php">Transitioning From Another Discipline</a></li>
                            
                        </ul>
                    </div>
                </div>
                <div class="l-navrail">
                    



                </div>
                <div class=" hide-for-small-down l-subsection">
                    
                    

                    
                </div>

                <div class="hide-for-small-down">
                    



<div class="collapsable-content">
  <section class="l-subsection related-images">
    <h2 class="hide-for-small-down">Scenes From Past Trainings</h2>
    <h1 class="show-for-small-down collapse-link">Scenes From Past Trainings</h1>
    <div class="collapsable">
    <p>At the UX Conference, you get a full day of in-depth training with expert instructors. Learn more about <a href="../../training/why-attend/index.php">why you should attend</a>.</p>

    
      
        
          
            <img srcset="https://media.nngroup.com/media/files/images/IMG_1090_copy_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale@2x.jpg 2x" src="https://media.nngroup.com/media/files/images/IMG_1090_copy_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale.jpg" alt="">
          
        
      
    
      
        
          
            <img srcset="https://media.nngroup.com/media/files/images/IMG_7582_copy_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale@2x.jpg 2x" src="https://media.nngroup.com/media/files/images/IMG_7582_copy_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale.jpg" alt="">
          
        
      
    
      
        
          
            <img srcset="https://media.nngroup.com/media/files/images/IMG_8464_copy_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale@2x.jpg 2x" src="https://media.nngroup.com/media/files/images/IMG_8464_copy_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale.jpg" alt="">
          
        
      
    
      
        
          
            <img srcset="https://media.nngroup.com/media/files/images/IMG_4730_copy_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale@2x.jpg 2x" src="https://media.nngroup.com/media/files/images/IMG_4730_copy_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale.jpg" alt="">
          
        
      
    
    </div>
  </section>
</div>


                </div>
            </div>
        </div>
    </div>

    <div class="row hide-for-small-down related-content">
      
      
        <section class="l-subsection related-courses medium-6 column">
          <h1>Related Courses</h1>
           <ul class="no-bullet">
    
        <li><a href="../ux-basic-training/index.php">UX Basic Training</a></li>
    
        <li><a href="../journey-mapping/index.php">Journey Mapping to Understand Customer Needs</a></li>
    
        <li><a href="../facilitating-ux-workshops/index.php">Facilitating UX Workshops</a></li>
    
        <li><a href="../analytics-and-user-experience/index.php">Analytics and User Experience</a></li>
    
        <li><a href="../ux-deliverables/index.php">UX Deliverables</a></li>
    
</ul>
        </section>
      
      

      
      
        <section class="l-subsection related-articles medium-6 column">
        <h1>Related Articles</h1>
        <ul class="no-bullet">
          
            
            <li><a href="../../articles/game-user-research/index.php">Games User Research: What’s Different?</a></li>
            
          
            
            <li><a href="../../articles/ux-stories/index.php">UX Stories Communicate Designs </a></li>
            
          
            
            <li><a href="../../articles/usability-roi-declining-but-still-strong/index.php">Usability ROI Declining, But Still Strong</a></li>
            
          
            
            <li><a href="../../articles/ux-maturity-stages-1-4/index.php">Corporate UX Maturity: Stages 1-4</a></li>
            
          
            
            <li><a href="../../articles/return-on-investment-for-usability/index.php">Return on Investment for Usability</a></li>
            
          
        </ul>
        </section>
      
      

    </div>
</article>

    </div>
    




<footer id="page-footer">
    <div class="row hide-for-print">
        <div class="column small-12 medium-6">
            <section class="footer-popular">
                
                <h1>Popular Topics</h1>
                <div style="float: left;  width: 15em; display: block;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/applications/index.php">Application Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/e-commerce/index.php">E-Commerce Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/eyetracking/index.php">Eyetracking </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/information-architecture/index.php">Information Architecture </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/intranets/index.php">Intranet Design </a></li>
	<li><a href="../../topic/interaction-design/index.php">Interaction Design </a></li>
</ul>
</div>

<div style="float: left;  width: 15em; display: block; list-style: none;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/mobile-and-tablet-design/index.php">Mobile and Tablet Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/credibility-persuasion/index.php">Persuasive Design</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/research-methods/index.php">Research Methods</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/user-testing/index.php">User Testing </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/web-usability/index.php">Web Usability </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/writing-web/index.php">Web Writing &amp; Content Strategy </a></li>
</ul>
</div>

<p>&nbsp;</p>

                
            </section>
        </div>

        <div class="column small-12 medium-6">
            <div class="row">
                <div class="column small-6">
                    <section class="footer-about">
                        <h1>Follow Us</h1>
                        <ul>
	<li><a href="../../articles/subscribe/index.php">Subscribe to our weekly UX newsletter</a>,&nbsp;Jakob Nielsen&#39;s Alertbox</li>
	<li>Visit Don Norman&#39;s website, <a href="http://www.jnd.org/"> www.jnd.org </a></li>
	<li>Visit Bruce Tognazzini&#39;s website, <a href="http://www.asktog.com/"> www.asktog.com </a></li>
	<li style="padding-top: 6px;"><a class="twitter-follow-button" data-show-count="false" data-size="large" href="https://twitter.com/nngroup"><img alt="Follow @nngroup on Twitter" height="20" src="https://media.nngroup.com/media/editor/2016/12/30/twitter-follow-button.png" style="width: 117px; height: 20px;" width="117" /></a></li>
	<li style="padding-top: 6px;"><img alt="" height="14" src="https://media.nngroup.com/media/editor/2016/07/26/feed-icon-14x14.png" style="vertical-align:text-top;width:14px;margin-right:.25em;" width="14" /><a href="../../feed/rss/index.php">Subscribe to our RSS feed</a></li>
</ul>

                    </section>
                </div>

                <div class="column small-6">
                    <section class="footer-about right-column">
                        <h1>About
</h1>
                        <ul class="no-bullet">
	<li><a href="../../about/contact/index.php">Contact information</a></li>
	<li><a href="../../about/index.php">About Us</a></li>
	<li><a href="../../about/why-nng/index.php">Why NN/g? </a></li>
	<li><a href="../../return-policy/index.php">Return Policy</a></li>
</ul>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="copyright column small-12">
            <p><a href="../../copyright-and-reprint-info/index.php">Copyright</a> &copy; 1998-2017 Nielsen Norman Group, All Rights Reserved.</p>
        </div>
    </div>
</footer>


    


        <script src="https://media.nngroup.com/static/vendor/fastclick/js/fastclick.min.js"></script>
        <script src="https://media.nngroup.com/static/vendor/foundation/js/foundation.min.js"></script>
        <script src="http://media.nngroup.com/static/js/app.js"></script>
        <script src="http://media.nngroup.com/static/js/scroll-to-fixed.js"></script><script>var externalLinks = document.querySelectorAll('a[href^="http"]');for (var i = externalLinks.length-1; i >= 0; i--) {externalLinks[i].addEventListener("click", function(event) { alert("External links are disabled for this test."); event.preventDefault(); }, false);}var mailLinks = document.querySelectorAll('a[href^="mailto"]');for (var i = mailLinks.length-1; i >= 0; i--) {mailLinks[i].addEventListener("click", function(event) { alert("Email links are disabled for this test."); event.preventDefault(); }, false);}var externalForms = document.querySelectorAll('form');for (var i = externalForms.length-1; i >= 0; i--) {externalForms[i].addEventListener("submit", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false); externalForms[i].addEventListener("click", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false);}</script>

        <script>
            
            $(function() {
                $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
                $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
            });
        </script>

        


        
        <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1072699960;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072699960/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
    </body>

<!-- Mirrored from www.nngroup.com/courses/ux-vp-director/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:10:30 GMT -->
</html>
