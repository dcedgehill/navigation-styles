<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->





    
<!-- Mirrored from www.nngroup.com/articles/readers-comments-on-predictions-1999/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:16:10 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1026.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"beacon":"bam.nr-data.net","queueTime":7,"applicationTime":479,"agent":"","licenseKey":"1a30ef9931","errorBeacon":"bam.nr-data.net","applicationID":"2048992","transactionName":"NFxSMRBUX0EDUkIPDA0WdhAMVkVbDV8ZBxEXUFMJB0YfRAtUQRVZIktEDAFZVHYHRVcPD01eVRE="}</script>
        <title>Readers&#39; Comments on Predictions for 1999</title><meta property="og:title" content="Readers&#39; Comments on Predictions for 1999" />
  
        
        <meta name="description" content="Some readers like human service reps. 
What is the best patent strategy?
Y2K may hit the Web in unexpected ways.">
        <meta property="og:description" content="Some readers like human service reps. 
What is the best patent strategy?
Y2K may hit the Web in unexpected ways." />
        
  
        
	
        
        <meta name="keywords" content="human service representatives, customer 
service, Y2K, Perl, date notations, popularity, feedback look, site 
access statistics, unbiased recommendations, biased reviews, reputation 
manager, patents, futurism, invention, creativity, Jay Walker, Walker Digital">
        

        

        <meta name="robots" content="noarchive">
        <meta name="viewport" content="width=device-width">

        <link rel="search" type="application/opensearchdescription+xml" title="NN/g Search" href="../../opensearch.xml" />
        <link rel="shortcut icon" href="http://media.nngroup.com/static/img/favicon.ico" />

        <link rel="stylesheet" href="https://media.nngroup.com/static/vendor/font-awesome/css/font-awesome.min.css">

        
        <link rel="stylesheet" href="https://media.nngroup.com/static/css/7232a960f1df.css" type="text/css" />
        <link rel="stylesheet" href="http://fast.fonts.com/t/1.css?apiType=css&amp;projectid=c5846260-9def-46ec-9407-0f1349bd2d64" type="text/css">

        <script src="https://media.nngroup.com/static/vendor/modernizr/js/modernizr.min.js"></script>

        <script type="text/javascript" src="https://media.nngroup.com/static/js/3283be5def74.js"></script>
        

        

        
        
        <!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=229034,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('http://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->


<!-- Google Analytics Tracking Code for http://nngroup.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23132681-1', 'nngroup.com');
  ga('require', 'linkid', 'https://www.nngroup.com/articles/readers-comments-on-predictions-1999/linkid.js');
  ga('require', 'displayfeatures');

// Optimizely Universal Analytics Integration
 window.optimizely = window.optimizely || [];
 window.optimizely.push("activateUniversalAnalytics");

  ga('send', 'pageview');

</script>
<script>
/**
* Function to track a click on an outbound link in Google Analytics.
*/
function trackOutboundLink(label, url) {
    if(window.ga && ga.loaded) {
        ga('send', 'event', 'outbound', 'click', label, {'hitCallback': function(){document.location = url;}
        });
    } else {
        document.location = url;
    }
}
/**
* Function to track a click on a link in the article announcement area.
*/
$('#articleAnnouncement a').on('click', function(){
  ga('send', 'event', 'link', 'click', 'articleAnnouncement');
});
</script>

<script type="text/javascript">
 setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2421.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



<!-- Hotjar Tracking Code for http://nngroup.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:121605,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

        <script src="https://media.nngroup.com/static/js/analytics/track_outbound_link.js"></script>
        
        
        
    </head>
    <body class="location-articles articles-detail">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
    <header id="page-header">
        <div id="cart" class="shopping-cart mobile-cart show-for-small-down"></div>
        <div class="row">
            <div id="masthead" class="masthead small-12 medium-7 column">

                <!-- The button to toggles menu on small screen -->

                <h1><a href="../../index.php"><img src="http://media.nngroup.com/static/img/logo-wordmark.png" width="74" height="30"> Nielsen <span class="norman">Norman</span> Group</a></h1>
                <p class="tagline show-for-medium-up">Evidence-Based User Experience Research, Training, and Consulting</p>

                <!-- The button to toggles form search on small screen -->


                <div style="clear: right;width:  100%;height: 1px;"></div><?php define('ROOT_PATH',$_SERVER["DOCUMENT_ROOT"] . '/apps/navigation_styles/navigations/2'); ?><?php include(ROOT_PATH)."/navigation.php"; ?>

                <!-- Drop down menu on small screen -->
                <div id="hideNav" class="show-for-small-down">
                  
                  <ul class="menu-content">
                    <li><a href="../../index.php">Home</a></li>
                    <li class="has-submenu">
                      <a>Training</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../training/index.php">Overview</a></li>
                        
<li id="nav-ux-conference"><a href="../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../online-seminars/index.php"><span>Online Seminars</span></a></li>

                      </ul>
                    </li>
                    <li class="has-submenu">
                      <a>Consulting</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
  <li><a href="../../consulting/index.php">Overview</a></li>
  
  <li><a href="../../consulting/benchmarking/index.php">Benchmarking</a></li>
  
  <li><a href="../../consulting/expert-design-review/index.php">Expert Design Review</a></li>
  
  <li><a href="../../consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
  
  <li><a href="../../consulting/usability-testing/index.php">Usability Testing </a></li>
  
  <li><a href="../../consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
  
  <li><a href="../../in-house-training/index.php">In-House Training</a></li>
</ul>

                    </li>

                    <li><a href="../../reports/index.php">Reports</a></li>
                    <li><a href="../index.php">Articles</a></li>
                    <li class="has-submenu"><a>About NN/G</a><i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../about/index.php">Overview</a></li>
                        <li><a href="../../people/index.php">People</a></li>
                        <li><a href="../../about/why-nng/index.php">Why NN/g?</a></li>
                        <li><a href="../../about/contact/index.php">Contact</a></li>
                        <li><a href="../../news/index.php">News</a></li>
                        <li><a href="../../about/history/index.php">History</a></li>
                        <li><a href="../../books/index.php">Books</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                </div>

                <hr class="header-divider show-for-small-only"/>


            </div>
       </div>

        <div id="hideNav">            <nav class="primary-nav small-12 column">
                <ul>
                    <li><a class="navitem" id="nav-home" href="../../index.php">Home</a></li>
                    <li><a class="navitem" id="nav-training" href="../../training/index.php">Training</a></li>
                    <li><a class="navitem" id="nav-consulting" href="../../consulting/index.php">Consulting</a></li>
                    <li><a class="navitem" id="nav-reports" href="../../reports/index.php">Reports</a></li>
                    <li><a class="navitem" id="nav-articles" href="../index.php">Articles</a></li>
                    <li><a class="navitem" id="nav-about" href="../../about/index.php">About NN/G</a></li>
                </ul>
            </nav>
            <nav class="secondary-nav small-12 medium-12 large-12 columns">
                
                
            </nav>
        </div>
    </header>
    <div id="main" class="l-content" onclick="closeNav(event)"><span id="preventClose" style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776; Menu</span>
    
  

  

  <div class="row">
    
      
      <section class="small-12 medium-3 columns collapsable-content l-navrail articles-left-column">
        <h1 class="show-for-small-down collapse-link mobile-browse-header">Browse by Topic and Author</h1>
          <div class="row collapsable">

            
            <div class="small-6 medium-12 columns">
              <section id="article-popular-topics" style="display: none;">
                
                <h1>Topics</h1>
                <ul id="article-popular-topics">
                    
                    <li>
                        
                        <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/intranets/all/index.php">Intranets</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/user-testing/all/index.php">User Testing</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                        
                    </li>
                    
                    <li><a href="#" id="article-see-all-topics"><strong>See all topics&hellip;</strong></a></li>
                </ul>
                
              </section>
              <section id="article-all-topics" style="display: none;">
                
                <h1>
                  <span class="show-for-medium-up">All Article Topics</span>
                  <span class="show-for-small-down">All Topics</span>
                  (<a href="#" id="article-hide-all-topics">hide</a>)
                </h1>
                <ul>
                  
                  <li>
                    
                    <a href="../../topic/accessibility/all/index.php">Accessibility</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/agile/all/index.php">Agile</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/analytics-and-metrics/all/index.php">Analytics &amp; Metrics</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/applications/all/index.php">Application Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/b2b-websites/all/index.php">B2B Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/behavior-patterns/all/index.php">Behavior Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/branding/all/index.php">Branding</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/content-strategy/all/index.php">Content Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/corporate-websites/all/index.php">Corporate Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/design-patterns/all/index.php">Design Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-design-process/all/index.php">Design Process</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/email/all/index.php">Email</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/eyetracking/all/index.php">Eyetracking</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/heuristic-evaluation/all/index.php">Heuristic Evaluation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/human-computer-interaction/all/index.php">Human Computer Interaction</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-ideation/all/index.php">Ideation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/information-architecture/all/index.php">Information Architecture</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/interaction-design/all/index.php">Interaction Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/international-users/all/index.php">International Users</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/intranets/all/index.php">Intranets</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-management/all/index.php">Management</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/navigation/all/index.php">Navigation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/non-profit-websites/all/index.php">Non-Profit Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/personas/all/index.php">Personas</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/persuasive-design/all/index.php">Persuasive Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/prototyping/all/index.php">Prototyping</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/psychology-and-ux/all/index.php">Psychology and UX</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/research-methods/all/index.php">Research Methods</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/search/all/index.php">Search</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/social-media/all/index.php">Social Media</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/strategy/all/index.php">Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/user-testing/all/index.php">User Testing</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-teams/all/index.php">UX Teams</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/visual-design/all/index.php">Visual Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/writing-web/all/index.php">Writing for the Web</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/young-users/all/index.php">Young Users</a>
                    
                  </li>
                  
                </ul>
                
              </section>
            </div>
            <script>
              /* Determine which topics list will be displayed: the full or the
               * partial.  We are inlining this close to the list itself to
               * prevent rendering delays on the client
               */
              var topic_list = $.cookie("article-topics");

              var popular_topics = $('#article-popular-topics');
              var all_topics = $('#article-all-topics');

              if(topic_list == 1) {
                  popular_topics.hide();
                  all_topics.show();
              } else {
                  popular_topics.show();
                  all_topics.hide();
              }
            </script>
            

            
            <div class="small-6 medium-12 columns">
              
              <section id="article-initial-authors">
                  <h1>Author</h1>
                  <ul>
                    
                    <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                    
                    <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                    
                    <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                    
                    <li><a href="#" onClick="$('#article-initial-authors').hide(); $('#article-all-authors').show();"><strong>See all authors&hellip;</strong></a></li>
                  </ul>
              </section>
              <section id="article-all-authors" style="display: none;">
                <h1>
                  All Authors
                  (<a href="#" id="article-hide-all-authors">hide</a>)
                </h1>
                <ul>
                  
                  <li><a href="../author/raluca-budiu/index.php">Raluca Budiu</a></li>
                  
                  <li><a href="../author/patty-caya/index.php">Patty Caya</a></li>
                  
                  <li><a href="../author/susan-farrell/index.php">Susan Farrell</a></li>
                  
                  <li><a href="../author/therese-fessenden/index.php">Therese Fessenden</a></li>
                  
                  <li><a href="../author/kim-flaherty/index.php">Kim Flaherty</a></li>
                  
                  <li><a href="../author/sarah-gibbons/index.php">Sarah Gibbons</a></li>
                  
                  <li><a href="../author/aurora-harley/index.php">Aurora Harley</a></li>
                  
                  <li><a href="../author/kate-kaplan/index.php">Kate Kaplan</a></li>
                  
                  <li><a href="../author/page-laubheimer/index.php">Page Laubheimer</a></li>
                  
                  <li><a href="../author/angela-li/index.php">Angie Li</a></li>
                  
                  <li><a href="../author/hoa-loranger/index.php">Hoa Loranger</a></li>
                  
                  <li><a href="../author/kate-meyer/index.php">Kate Meyer</a></li>
                  
                  <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                  
                  <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                  
                  <li><a href="../author/kara-pernice/index.php">Kara Pernice</a></li>
                  
                  <li><a href="../author/christian-rohrer/index.php">Christian Rohrer</a></li>
                  
                  <li><a href="../author/amy-schade/index.php">Amy Schade</a></li>
                  
                  <li><a href="../author/katie-sherwin/index.php">Katie Sherwin</a></li>
                  
                  <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                  
                  <li><a href="../author/kathryn-whitenton/index.php">Kathryn Whitenton</a></li>
                  
                </ul>
              </section>
              
            </div>
            
          </div>

          
          <section class="hide-for-small-down">
            
              
              <h1>Recent Articles</h1>
              
              <ul>
                  
                  <li><a href="../seamless-cross-channel/index.php">Seamlessness in the Omnichannel User Experience</a></li>
                  
                  <li><a href="../brand-intention-interpretation/index.php">Experience Design: Bridging Brand Intention and Brand Interpretation</a></li>
                  
                  <li><a href="../eyetracking-tasks-efficient-scanning/index.php">Scanning Patterns on the Web Are Optimized for the Current Task</a></li>
                  
                  <li><a href="../flat-design-best-practices/index.php">Flat-Design Best Practices</a></li>
                  
                  <li><a href="../ux-user-stories/index.php">Accounting for UX Work with User Stories in Agile Projects</a></li>
                  
                  <li><a href="../index.php"><strong>See all articles&hellip;</strong></a></li>
              </ul>
              
            
          </section>
          

          
          
          <section class="hide-for-small-down">
            <h1>Popular Articles</h1>
            
            <ul>
                
                <li><a href="../usability-101-introduction-to-usability/index.php">Usability 101: Introduction to Usability</a></li>
                
                <li><a href="../top-10-mistakes-web-design/index.php">Top 10 Mistakes in Web Design</a></li>
                
                <li><a href="../how-users-read-on-the-web/index.php">How Users Read on the Web</a></li>
                
                <li><a href="../f-shaped-pattern-reading-web-content/index.php">F-Shaped Pattern For Reading Web Content</a></li>
                
                <li><a href="../ten-usability-heuristics/index.php">10 Usability Heuristics for User Interface Design</a></li>
                
                <li><a href="../intranet-design-2016/index.php">10 Best Intranets of 2016</a></li>
                
                <li><a href="../which-ux-research-methods/index.php">When to Use Which User-Experience Research Methods</a></li>
                
                <li><a href="../response-times-3-important-limits/index.php">Response Times: The 3 Important Limits</a></li>
                
                <li><a href="../why-you-only-need-to-test-with-5-users/index.php">Why You Only Need to Test with 5 Users</a></li>
                
                <li><a href="../page-fold-manifesto/index.php">The Fold Manifesto: Why the Page Fold Still Matters</a></li>
                
            </ul>
          </section>
          
          

          
            <section class="hide-for-small-down">
              
            </section>
          

      </section>
      

      
        <div class="small-12 medium-9 columns content-body">
          




<article class="l-subsection alertbox-article-content article-detail-bl">
  
  <header>
    <div class="l-subsection">
      <h1>Readers&#39; Comments on Predictions for 1999</h1>
      <div class="author-meta">by
        
          
            
            <a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a>
            
          
        on  December 27, 1998
        
      </div>

      

      
      <div class="topic-links">
        Topics:
        
          <ul>

</ul>
        
      </div>
      
    </div>
  </header>
  

  
  
  

  
  <section id="article-body"><p>
 <em>
  Sidebar to
  <a href="../../people/jakob-nielsen/index.php" title="Author biography">
   Jakob Nielsen
  </a>
  &#39;s column with predictions for the
  <a href="../predictions-for-the-web-in-1999/index.php">
   Web in 1999
  </a>
  .
 </em>
</p>
<p>
 I have received some interesting comments on my December Alertbox with predictions for future Web trends.
</p>
<h2>
 The Web Is Boring
</h2>
<p>
 <em>
  Dan Shafer, the Editorial Director of CNET&#39;s
  <a class="out" href="http://www.builder.com/" title="Service home page">
   Builder.com
  </a>
  site writes:
 </em>
</p>
<blockquote>
 Interesting stuff. Limiting yourself to just a handful of predictions jeopardizes your percentage chance of being right but helps focus your audience&#39;s attention on those issues you consider important. I like that. Too many top 10 lists floating around anyway.
 <p>
  While I essentially agree with most of what you predict, I would take issue on two points.
 </p>
 <p>
  First, as to the
  <strong>
   deployment of rich media content
  </strong>
  . I think it is essential to the long-term health and well-being of the Web that we find ways of making such content available and accessible via the bandwidth people have. I know that the Web isn&#39;t like TV but many people who aren&#39;t yet on the Web -- and probably the majority of those who are -- don&#39;t know that or don&#39;t appreciate it.
  <strong>
   &quot;Boring&quot; is a word I hear used a lot to describe today&#39;s Web
  </strong>
  and when I probe the meaning of that term, I usually find that the speaker is comparing it to television or, more generously perhaps, to other software applications he is accustomed to seeing and using.
 </p>
 <p>
  While indiscriminate use of multimedia isn&#39;t any more sound an idea than the profligate use of graphics in general (a point where you and I surely agree), I
  <em>
   do
  </em>
  think that we&#39;ll see a significant increase in the number of sites who make use of rich media in 1999. Not all of those uses will be appropriate and some will definitely be annoying, but I predict that a number of hugely successful commercial sites will find ways to make rich media a differentiator next year.
 </p>
 <p>
  The other point at which I disagree is on the
  <strong>
   nearness in time of the portable, wireless, and small-format Web access device
  </strong>
  . While I do think that portable and wireless access will become much more broadly accessible during the year, I
  <em>
   don&#39;t
  </em>
  think the small-format prediction will come true quite that soon. The primary reason I disagree is that we have a serious chicken-egg problem here.
  <strong>
   The vast majority of content on the Web today won&#39;t scale well to the small device.
  </strong>
  That means that the publishers of that content must revise its format to accommodate the new form factors or that a lot of new content will have to be written to take them into account. I think it is highly unlikely that the latter will take place, so these devices will end up being initially used only to access sites that prepare content specifically for them.
 </p>
 <p>
  Ultimately, I see this as a server problem. Rather than expecting each Web site to create content for all these multifarious formats, the devices themselves will need to incorporate small Web servers (or similar technology) that filters incoming content and adjusts its format to suit the capabilities of the device. If we don&#39;t find a way to do this intelligently -- with JINI, for example -- then we are going to fragment the Web into dozens, perhaps hundreds of tiny pieces. Nobody wins if that happens.
 </p>
</blockquote>
<p>
 <em>
  Jakob&#39;s reply:
 </em>
 People may say that text-dominated websites are boring, but then they would also say that newspapers are boring. (And yes, I know that many people don&#39;t read newspapers: but most of them probably do read crime novels or other text-only media.) Text is a very vivid medium - the human brain is a very powerful illustration tool: read
 <a class="out" href="http://www.amazon.com/exec/obidos/ISBN=0140445927/useitcomusableinA/" title="My favorite translation of The Illiad - book page at Amazon.com">
  Homer
 </a>
 and you imagine the blood-stained broken bronze helmets littering the battlefields of Troy 3000 years ago.
</p>
<p>
 The comparisons to TV and software are very different:
</p>
<ul>
 <li>
  The TV comparison is a complaint about poor production values.
 </li>
 <li>
  The software comparison is a complaint about lack of interaction, engagement, and accomplishment. It is in fact more fun to use something that gives you a sense of having done something.
 </li>
</ul>
<p>
 I would
 <em>
  like
 </em>
 to see more bandwidth and more multimedia, but wishing for it doesn&#39;t make it happen. For the next few years, I think we are relegated to a low-bandwidth Web and we would do better if we attacked the comparison to engaging software than the comparison to entertaining television.
</p>
<p>
 I agree with the chicken-and-egg problem for mobile Web content. I hope that the solution can be something that doesn&#39;t fragment the Web, but you are probably right that the initial solution will be special-purpose content. Over the long term, your proposal for server-based solutions will hopefully come true. Scalable content will become ever more important as the diversity of Web-access devices increases.
</p>
<h2>
 Human Help and Unbiased Recommendations
</h2>
<p>
 <em>
  <a class="out" href="http://www.htc.net/~joegrant/" title="Personal home page">
   Joe Grant
  </a>
  from Tripos, Inc. writes:
 </em>
</p>
<blockquote>
 Your article predicting web trends for 1999 included the quote, &quot;it is easier to train a computer to give good service than to train thousands of human service reps.&quot; When you have a chance in a future article, please offer more explanation of this... at this point, I still prefer the humans if I can reach them quickly.
 <p>
  And thanks for also reviewing your predictions from a year ago! One trend I&#39;ve observed on the Web is more honesty. Communication mediated via the Internet (as opposed, for example, to &quot;mediated via television&quot;) require more openness. For one thing, if anyone tries to ignore past mistakes or bluff their way past them, it&#39;s too easy to have someone slap back a URL or email from the past! I love the
  <a class="out" href="http://www.fool.com/" title="Motley Fool home page">
   www.fool.com
  </a>
  site for this reason... they make fun of their bad choices in stocks, and invite criticism. (They even had an article recently entitled &quot;Uncompromising Honesty&quot; concerning their internal company policies.)
 </p>
 <p>
  BTW, one item on my wish list for the Web in 1999 is for better means to find unbiased recommendations and choices. I often turn to the web to find a review of software and find it difficult to find a list of choice with reasonably unbiased recommendations and reviews. The desire for unbiased recommendations concerns a number of entities: any merchandise sold, organizations, treatments for health, methods for handling kids, etc.
 </p>
</blockquote>
<p>
 <em>
  Jakob&#39;s reply:
 </em>
 You prefer human service reps,
 <em>
  if
 </em>
 you can reach them quickly and
 <em>
  if
 </em>
 they actually provide good help. But realistically, most companies cannot afford to staff call centers with sufficiently many operators to handle peak loads without long waits. Also, the operators never know your personal situation well and often don&#39;t even know their own products well since they support a range of stuff.
</p>
<p>
 Truly great personalized human service exists, of course, but mainly in luxury establishments that can afford to invest in the necessary quality and quantity of operators. The Web turns the tables and makes quality service affordable in many more cases since the cost of developing a high-quality automated service can be amortized over a large number of users. The problem with human service is that it doesn&#39;t scale: if you get ten times as many customers, you need more than ten times as large a service staff (the staff goes up more than the use because of management overhead).
</p>
<p>
 I fully agree with your desire for unbiased recommendations. This is one reason I am so eager to see
 <a class="old" href="../the-case-for-micropayments/index.php" title="Alertbox Jan. 1998: The case for micropayments">
  micropayments
 </a>
 succeed: when the user is the paying customer, then the site will be motivated to offer trustworthy reviews and recommendations. In general, I think there is a great business opportunity on the Web for offering
 <a class="out" href="../the-reputation-manager/index.php" title="Alertbox Feb. 1998: The Reputation Manager">
  reputation management services
 </a>
 that would keep track of the quality of other sites and services and advise users where to go.
</p>
<p>
 <em>
  Colleen Kehoe from Georgia Tech follows up:
 </em>
</p>
<blockquote>
 Joe Grant&#39;s comments asking for unbiased recommendations reminded me of a site I came across recently:
 <a class="out" href="http://www.bizrate.com/">
  BizRate
 </a>
 It&#39;s not exactly what Grant was asking for - it rates merchants rather than products - but I thought it was in that same spirit.
 <p>
  I had just placed an order with
  <a class="out" href="http://www.etoys.com/">
   eToys
  </a>
  (which I was really impressed with) and at the end of ordering, I had the option to answer a short survey from BizRate about the order I had just placed. Talk about being in the right place at the right time -- at no other moment would I have been more motivated (either by a good or bad experience) to answer the survey than just as I completed the order. It also raised my confidence in etoys that they would participate in this kind of ratings program. Another nice touch from BizRate was showing me how others had rated the site and then returning me to etoys where I had left off. Finally, just today (3 weeks later) I received an email from BizRate asking me to complete a short survey on how the rest of the order went -- did I receive what I ordered, what it what I expected, etc.? Very nice follow up on the original survey and very short, as they had promised.
 </p>
</blockquote>
<h2>
 Year 2000: More Problems Than We May Think
</h2>
<p>
 <em>
  Ben Tilly from
  <a class="out" href="http://www.trepp.com/" title="Company home page">
   The Trepp Group
  </a>
  writes:
 </em>
</p>
<blockquote>
 You claim that 2 digit date-fields only matter if programs do computations with them.
 <p>
  Not so.
 </p>
 <p>
  I have already been struggling with the issues that they pose for various infrastructure automations. Allow me to demonstrate in Perl.
 </p>
 <p>
  Suppose that we want to produce your
  <code>
   981227.php
  </code>
  filename automatically. A typical code-snippet would be
 </p>
 <p>
  <code>
   ($yy, $mm, $dd) = (localtime)[5, 4, 3];
   <br />
   $mm++;
   <br />
   $mm = &quot;0$mm&quot; if $mm &lt; 10;
   <br />
   $dd = &quot;0$dd&quot; if $dd &lt; 10;
   <br />
   $file_name = &quot;$yy$mm$dd.php&quot;;
  </code>
 </p>
 <p>
  Suppose that we want to go out and automatically find that URL. A typical code-snippet would look for files that match the pattern
  <code>
   /^\d{6}\.php$/.
  </code>
  <em>
   (6 digits and
   <code>
    .php
   </code>
   )
  </em>
  Seems fine.
 </p>
 <p>
  In 2000 the first program will produce a URL of
  <code>
   1000101.php
  </code>
  and the second program will not find it.
 </p>
 <p>
  Both solutions are equally valid, but a lot of automatic FTP posts and downloads are going to find that one used the one convention and the other used the other, and there is no good solution. Unless you ask you have no way of knowing which one someone is about to use. Sure, you can work around the issue, but not many programmers do.
 </p>
 <p>
  Even worse off are people who assume their site is safe because they use a 4 digit number. This does not mean that you are safe. If that number is generated automatically, there is an excellent chance that you are about to go from 1999 to 19100. The reason is that they got a 4-digit year by &quot;19$yy&quot; instead of adding 1900.
 </p>
 <p>
  And the problem is not just Perl. Perl&#39;s behavior is inherited from C, and C is the ancestor of a lot of other languages.
 </p>
</blockquote>
<p>
 <em>
  Jakob&#39;s reply:
 </em>
 I stand corrected. In general, I am sure there are plenty of such Y2K bugs hiding in places where you would least expect them.
</p>
<p>
 I think the Web is pretty robust and will handle Y2K relatively well. But the network nature of the Web will haunt us: since the Web
 <em>
  connects
 </em>
 systems together, even compliant systems may be impacted by bad data received from non-compliant systems. Even if most large sites fix their systems, the Web may still have problems due to less diligent services. My own site was almost brought to its knees a few months ago by somebody in India who downloaded the same file about ten thousand times within a few minutes. Being connected to people with bugs is almost as bad as having bugs yourself. The Internet as a whole will probably continue running, but segments may be taken down.
</p>
<h2>
 What Good are Patents for the Fast-Moving Web?
</h2>
<p>
 <em>
  Jay Virdee of the
  <a class="out" href="http://www.bhrc.co.uk/" title="Company home page">
   British Hotel Reservation Centre
  </a>
  writes:
 </em>
</p>
<blockquote>
 I am at a loss to follow your argument - &quot;Web Patent Bonanza&quot;. Surely the Internet is still evolving rapidly from a business model and user interface perspective and I find it difficult to conceive how a patent would offer any kind of protection on both counts. Furthermore policing and enforcing such patents would be an enormous if not impossible task given the nature of the Internet. In the 3-4 years time it takes for the patent to make its way through the Patent Office, the Internet will surely be a different beast from the one the patent is designed to protect.
</blockquote>
<p>
 <em>
  Jakob&#39;s reply:
 </em>
 You are right that it doesn&#39;t make much sense to patent a current &quot;hot&quot; idea: long before the patent issues, we will all be off to doing something else. So, for example, there is little value in having patents on the various tricks to do animated Web pages that were in use before animated GIFs infected the Web.
</p>
<p>
 This only proves the need to be
 <strong>
  extremely far-sighted in the patent game
 </strong>
 . A patent is good for twenty years, so the
 <strong>
  best inventions are those that may not see common use for about ten years
 </strong>
 : far enough out that it is possible to patent fundamental inventions rather than small twists; yet early enough that there are many years left in which to collect royalties.
</p>
<p>
 Jay Walker from Walker Digital described his patent strategy in a recent interview: the first part of the article is the usual boring stuff about how much money he may make on his IPO, but scroll to the bottom of the page and start reading with the last three paragraphs (and continue with the &quot;next page&quot; button). Walkers basic approach to patents is to
 <strong>
  start by analyzing unmet user needs and then patent a way to satisfy these needs
 </strong>
 . This is the opposite approach of traditional labs like IBM Research which starts from the technology end: they ask what they might be able to invent given their research interests.
</p>
<p>
 Walker&#39;s approach is exactly the same as my own. If you start with current technology, then you are never going to be able to push out into the part of the design space where the good patents lie. Walker is very proud that his 25-person research team is filing two patents per week (whereas the hugely larger Lucent Technologies only files 15 patent applications per week). I am actually not that impressed: during a year when Bruce Tognazzini and I had a contest to see who would be Sun Microsystem&#39;s most prolific inventor, we each filed a patent per week on the average. Of course, having a creativity contest with Tog is a doomed endeavor, but the point is that it is possible to invent a huge number of technology improvements if you base the project in future user needs and not in the technology itself.
</p>
<p>
 You don&#39;t need to police the entire Internet and crack down on every single infringement. Just go for the big ones: that&#39;s where the royalties lie. Or use your patents as a trading leverage when you want to get free access to use some other company&#39;s patent. If you have enough patents, then this other company will surely infringe on at least one: now you have them and can get their inventions for free.
</p>
<h2>
 Self-Referential Feedback When Calling Pages &quot;Popular&quot;
</h2>
<p>
 <em>
  Commenting on the notion of Self-Optimizing Content in the
  <a class="new" href="../predictions-for-1998-revisited/index.php" title="Alertbox sidebar: Predictions for 1998 revisited at year-end">
   sidebar
  </a>
  , Eric Scheid from
  <a class="out" href="http://www.ironclad.net.au/" title="Company home page in Australia">
   Ironclad Networks Pty Limited
  </a>
  writes:
 </em>
</p>
<blockquote>
 I wonder a little regarding the self-referential feedback that is happening here. For example, you have listed certain pages on your site as &quot;most popular&quot; and prominently positioned links for same, but how many of those hits are because of the prominent positioning? If you were to prominently position a page which is actually not-so-popular but refer to it as &quot;a popular page&quot;, by how much would the hits increase?
 <p>
  To a limited degree you could determine some extent of this effect by examining your logs to see how many of those hits were referred by the front page compare to normal links which may be scattered about your site. That would still be inexact since you don&#39;t know whether that front-page referrer hit was because it was highly recommended by group consensus, or because of the inherent interesting nature of the link itself -- how to discriminate between the context of the mention vs the subject of the mention?
 </p>
 <p>
  In a similar vein, there are other mis-uses of hit-stats. The most popular seems to be &quot;we don&#39;t get many hits from earlier browsers, therefore we don&#39;t need to design for them&quot;. In this instance the proper measure of &quot;many hits&quot; would be the first-hit per visitor, not the subsequent trolling-through-site hits.
 </p>
</blockquote>
<p>
 <em>
  Jakob&#39;s reply:
 </em>
 There is no doubt that a feedback loop starts as soon as you denote something as popular:
 <em>
  calling
 </em>
 it popular
 <em>
  makes
 </em>
 it popular.
</p>
<p>
 So to be &quot;fair&quot;, continued presence on a most-popular pages list should be determined after modifying the site access statistics to downplay hits generated due to the &quot;most popular&quot; status. For a high-traffic site, it would be possible to estimate the effect of the popularity highlighting by letting a very small percentage of users see pages that did not have any indication of popularity. Doing so would make it possible to compare the clickthrough rates gathered by an article when it is highlighted as &quot;popular&quot; as opposed to when it is simply listed on an equal footing with other articles.
</p>
<p>
 Even though popularity statistics have an inherent weakness due to this feedback bias, I remain a firm believer in their utility. Once a site becomes very big, it becomes necessary to prioritize the information and allow easy access to the most popular elements. Even if popularity cannot be determined exactly it can still help users. Just remember the feedback loop and be prepared to downgrade items even if they continue to attract a fair number of hits.
</p>
<h2>
 Early Readers Don&#39;t Discriminate Consciously
</h2>
<p>
 <em>
  Jason R. Fink from
  <a class="out" href="http://www.yyz.net/iit/" title="Company home page">
   I2T
  </a>
  writes:
 </em>
</p>
<blockquote>
 I was noting the &quot;standards on the rise&quot; prediction (no surprise) which is, of course, great. One of the Flame wars that ensued at
 <a class="out" href="http://www.webstandards.org/">
  the Web Standards Project
 </a>
 was about showing compliant pages and informing the general public. I felt (as I do about a great many things) if someone chooses to spend their time trying to educate those who may not wish to be educated, feel free. I also agree with the other side of the argument that companies and developers (although for hypertext only technology I prefer to call them
 <em>
  authors
 </em>
 ) need to be conscience and aware of standards as well.
 <p>
  It has been my limited experience that creating a valid and well written &quot;webspace&quot; will attract the average as well as &quot;discriminating&quot; reader. Along with this of course I provide professional background information and links to other more informative sites to bolster the validity of actual content and the design philosophy.
 </p>
 <p>
  The proof was quite literally in the pudding. When I began the arduous task of building the site (a long time ago on a server far away) I was totally clueless and received very few visits or subscribers. This was because I wrote fluff, regurgitated links and knew nothing about HTML. I got a perverbial slam when I requested a Usability Engineer look over my site. As is typical of my personality, I got mad and learned basic HTML (and have gone onto learn some other neat stuff like XML and SGML).
 </p>
 <p>
  When I began to write about aspects of technology I actually had experience with (Name servers, DHCP, Operating Systems etc.) I began to draw a crowd. It is my belief that I drew in more readers not just because the content was better but my actual HTML was validated and there was a means for readers to discover that.
 </p>
 <p>
  On one last note, the comparison of Web to ANYTHING is invalid. I recommend Terry Sullivan&#39;s Essay on
  <a class="out" href="http://www.pantos.org/atw/perspectives/">
   <span style="display: none;">
    &nbsp;
   </span>
   Metaphors for the Web
   <span style="display: none;">
    &nbsp;
   </span>
  </a>
  . If a user prefers pictures over words (I like the little quip about crime novels) then I won&#39;t be the least bit offended if they don&#39;t visit my site and am sure you are not sweating bullets over that audience either.
 </p>
</blockquote>
<p>
 <em>
  Jakob&#39;s reply:
 </em>
 I totally agree with the point about writing about something you know and care about. No design can compensate for lack of insight or value in the content. Once you have good content, it furthermore needs to be presented in a way that will work in all browsers and not just the one you happen to have on your own computer.
</p>
<h2>
 Benefits of Open Software
</h2>
<p>
 <em>
  David Jao from MIT writes:
 </em>
</p>
<blockquote>
 You mentioned that the next Netscape browser promises to be 100% standards compliant. I am surprised that you did not mention in passing the reason for this improved standards support. When Netscape released the source code for their browser on March 31, 1998, the ensuing open development effort engaging hundreds of volunteer programmers all over the world all but ensured that their next browser would be standards compliant.
 <strong>
  Open projects promote open standards
 </strong>
 , and frankly, anything less than full standards support in an open project would have been completely unacceptable.
 <p>
  Anyone connected to the internet today has got to be aware of the revolution that is happening with the rise of open source, collaboratively developed free software. Full standards compliance is but one of the myriad benefits that will result from this new software development paradigm.
 </p>
</blockquote>
</section>
  

  
  <section>
    
        
    
  </section>
  
  
  
  
  

  


  <section>
    <br />
    <p class="share"><b>Share <span class="hide-for-small-down">this article</span>:</b> <a href="https://twitter.com/intent/tweet?url=http://www.nngroup.com/articles/readers-comments-on-predictions-1999/&amp;text=Readers'%20Comments%20on%20Predictions%20for%201999&amp;via=nngroup">Twitter</a> | <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=http://www.nngroup.com/articles/readers-comments-on-predictions-1999/&amp;title=Readers'%20Comments%20on%20Predictions%20for%201999&amp;source=Nielsen%20Norman%20Group">LinkedIn</a> | <a href="https://plus.google.com/share?url=http://www.nngroup.com/articles/readers-comments-on-predictions-1999/">Google+</a> | <a href="mailto:?subject=NN/g Article: Readers&#39; Comments on Predictions for 1999&amp;body=http://www.nngroup.com/articles/readers-comments-on-predictions-1999/">Email</a></p>
    <hr />
  </section>

  <footer class="l-subsection">
    
    
    

    <div class="row show-for-small-down related-content">
      
      
      

      
      
      

      
      
      

      
      
      

      
      
      
    </div>

    <div class="hide-for-small-down row related-content">
      <div class="medium-6 columns">
        
        
        

        

      </div>
      <div class="medium-6 columns">
        
        
      </div>
    </div>
  </footer>
</article>

        </div>

      

    
  </div>
  <div class="row">
    
      <div class="column small-12 show-for-small-down l-navrail announcements">
        
          
        
      </div>
    
    
  </div>

    </div>
    




<footer id="page-footer">
    <div class="row hide-for-print">
        <div class="column small-12 medium-6">
            <section class="footer-popular">
                
                <h1>Popular Topics</h1>
                <div style="float: left;  width: 15em; display: block;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/applications/index.php">Application Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/e-commerce/index.php">E-Commerce Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/eyetracking/index.php">Eyetracking </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/information-architecture/index.php">Information Architecture </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/intranets/index.php">Intranet Design </a></li>
	<li><a href="../../topic/interaction-design/index.php">Interaction Design </a></li>
</ul>
</div>

<div style="float: left;  width: 15em; display: block; list-style: none;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/mobile-and-tablet-design/index.php">Mobile and Tablet Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/credibility-persuasion/index.php">Persuasive Design</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/research-methods/index.php">Research Methods</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/user-testing/index.php">User Testing </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/web-usability/index.php">Web Usability </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/writing-web/index.php">Web Writing &amp; Content Strategy </a></li>
</ul>
</div>

<p>&nbsp;</p>

                
            </section>
        </div>

        <div class="column small-12 medium-6">
            <div class="row">
                <div class="column small-6">
                    <section class="footer-about">
                        <h1>Follow Us</h1>
                        <ul>
	<li><a href="../subscribe/index.php">Subscribe to our weekly UX newsletter</a>,&nbsp;Jakob Nielsen&#39;s Alertbox</li>
	<li>Visit Don Norman&#39;s website, <a href="http://www.jnd.org/"> www.jnd.org </a></li>
	<li>Visit Bruce Tognazzini&#39;s website, <a href="http://www.asktog.com/"> www.asktog.com </a></li>
	<li style="padding-top: 6px;"><a class="twitter-follow-button" data-show-count="false" data-size="large" href="https://twitter.com/nngroup"><img alt="Follow @nngroup on Twitter" height="20" src="https://media.nngroup.com/media/editor/2016/12/30/twitter-follow-button.png" style="width: 117px; height: 20px;" width="117" /></a></li>
	<li style="padding-top: 6px;"><img alt="" height="14" src="https://media.nngroup.com/media/editor/2016/07/26/feed-icon-14x14.png" style="vertical-align:text-top;width:14px;margin-right:.25em;" width="14" /><a href="../../feed/rss/index.php">Subscribe to our RSS feed</a></li>
</ul>

                    </section>
                </div>

                <div class="column small-6">
                    <section class="footer-about right-column">
                        <h1>About
</h1>
                        <ul class="no-bullet">
	<li><a href="../../about/contact/index.php">Contact information</a></li>
	<li><a href="../../about/index.php">About Us</a></li>
	<li><a href="../../about/why-nng/index.php">Why NN/g? </a></li>
	<li><a href="../../return-policy/index.php">Return Policy</a></li>
</ul>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="copyright column small-12">
            <p><a href="../../copyright-and-reprint-info/index.php">Copyright</a> &copy; 1998-2017 Nielsen Norman Group, All Rights Reserved.</p>
        </div>
    </div>
</footer>


    


        <script src="https://media.nngroup.com/static/vendor/fastclick/js/fastclick.min.js"></script>
        <script src="https://media.nngroup.com/static/vendor/foundation/js/foundation.min.js"></script>
        <script src="http://media.nngroup.com/static/js/app.js"></script>
        <script src="http://media.nngroup.com/static/js/scroll-to-fixed.js"></script><script>var externalLinks = document.querySelectorAll('a[href^="http"]');for (var i = externalLinks.length-1; i >= 0; i--) {externalLinks[i].addEventListener("click", function(event) { alert("External links are disabled for this test."); event.preventDefault(); }, false);}var mailLinks = document.querySelectorAll('a[href^="mailto"]');for (var i = mailLinks.length-1; i >= 0; i--) {mailLinks[i].addEventListener("click", function(event) { alert("Email links are disabled for this test."); event.preventDefault(); }, false);}var externalForms = document.querySelectorAll('form');for (var i = externalForms.length-1; i >= 0; i--) {externalForms[i].addEventListener("submit", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false); externalForms[i].addEventListener("click", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false);}</script>

        <script>
            
            $(function() {
                $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
                $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
            });
        </script>

        
<script src="https://media.nngroup.com/static/js/articles.js"></script>


        
        <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1072699960;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072699960/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
    </body>

<!-- Mirrored from www.nngroup.com/articles/readers-comments-on-predictions-1999/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:16:10 GMT -->
</html>
