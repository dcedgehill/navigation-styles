<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->





    
<!-- Mirrored from www.nngroup.com/articles/power-law-learning/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:06:19 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1026.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"licenseKey":"1a30ef9931","queueTime":4,"beacon":"bam.nr-data.net","errorBeacon":"bam.nr-data.net","applicationID":"2048992","transactionName":"NFxSMRBUX0EDUkIPDA0WdhAMVkVbDV8ZBxEXUFMJB0YfRAtUQRVZIktEDAFZVHYHRVcPD01eVRE=","applicationTime":511,"agent":""}</script>
        <title>The Power Law of Learning: Consistency vs. Innovation in User Interfaces</title><meta property="og:title" content="The Power Law of Learning: Consistency vs. Innovation in User Interfaces" />
  
        
        <meta name="description" content="Across many tasks, learning curves show an initial learning period, followed by a plateau of optimal efficiency.">
        <meta property="og:description" content="Across many tasks, learning curves show an initial learning period, followed by a plateau of optimal efficiency." />
        
  
        
	
        
        <meta name="keywords" content="learning, power law of learning, consistency, learning curve, learning experiment, usability, conventions, UI design">
        

        

        <meta name="robots" content="noarchive">
        <meta name="viewport" content="width=device-width">

        <link rel="search" type="application/opensearchdescription+xml" title="NN/g Search" href="../../opensearch.xml" />
        <link rel="shortcut icon" href="http://media.nngroup.com/static/img/favicon.ico" />

        <link rel="stylesheet" href="https://media.nngroup.com/static/vendor/font-awesome/css/font-awesome.min.css">

        
        <link rel="stylesheet" href="https://media.nngroup.com/static/css/7232a960f1df.css" type="text/css" />
        <link rel="stylesheet" href="http://fast.fonts.com/t/1.css?apiType=css&amp;projectid=c5846260-9def-46ec-9407-0f1349bd2d64" type="text/css">

        <script src="https://media.nngroup.com/static/vendor/modernizr/js/modernizr.min.js"></script>

        <script type="text/javascript" src="https://media.nngroup.com/static/js/3283be5def74.js"></script>
        

        

        
        
        <!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=229034,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('http://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->


<!-- Google Analytics Tracking Code for http://nngroup.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23132681-1', 'nngroup.com');
  ga('require', 'linkid', 'https://www.nngroup.com/articles/power-law-learning/linkid.js');
  ga('require', 'displayfeatures');

// Optimizely Universal Analytics Integration
 window.optimizely = window.optimizely || [];
 window.optimizely.push("activateUniversalAnalytics");

  ga('send', 'pageview');

</script>
<script>
/**
* Function to track a click on an outbound link in Google Analytics.
*/
function trackOutboundLink(label, url) {
    if(window.ga && ga.loaded) {
        ga('send', 'event', 'outbound', 'click', label, {'hitCallback': function(){document.location = url;}
        });
    } else {
        document.location = url;
    }
}
/**
* Function to track a click on a link in the article announcement area.
*/
$('#articleAnnouncement a').on('click', function(){
  ga('send', 'event', 'link', 'click', 'articleAnnouncement');
});
</script>

<script type="text/javascript">
 setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2421.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



<!-- Hotjar Tracking Code for http://nngroup.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:121605,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

        <script src="https://media.nngroup.com/static/js/analytics/track_outbound_link.js"></script>
        
        
        
    </head>
    <body class="location-articles articles-detail">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
    <header id="page-header">
        <div id="cart" class="shopping-cart mobile-cart show-for-small-down"></div>
        <div class="row">
            <div id="masthead" class="masthead small-12 medium-7 column">

                <!-- The button to toggles menu on small screen -->

                <h1><a href="../../index.php"><img src="http://media.nngroup.com/static/img/logo-wordmark.png" width="74" height="30"> Nielsen <span class="norman">Norman</span> Group</a></h1>
                <p class="tagline show-for-medium-up">Evidence-Based User Experience Research, Training, and Consulting</p>

                <!-- The button to toggles form search on small screen -->


                <div style="clear: right;width:  100%;height: 1px;"></div><?php define('ROOT_PATH',$_SERVER["DOCUMENT_ROOT"] . '/apps/navigation_styles/navigations/2'); ?><?php include(ROOT_PATH)."/navigation.php"; ?>

                <!-- Drop down menu on small screen -->
                <div id="hideNav" class="show-for-small-down">
                  
                  <ul class="menu-content">
                    <li><a href="../../index.php">Home</a></li>
                    <li class="has-submenu">
                      <a>Training</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../training/index.php">Overview</a></li>
                        
<li id="nav-ux-conference"><a href="../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../online-seminars/index.php"><span>Online Seminars</span></a></li>

                      </ul>
                    </li>
                    <li class="has-submenu">
                      <a>Consulting</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
  <li><a href="../../consulting/index.php">Overview</a></li>
  
  <li><a href="../../consulting/benchmarking/index.php">Benchmarking</a></li>
  
  <li><a href="../../consulting/expert-design-review/index.php">Expert Design Review</a></li>
  
  <li><a href="../../consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
  
  <li><a href="../../consulting/usability-testing/index.php">Usability Testing </a></li>
  
  <li><a href="../../consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
  
  <li><a href="../../in-house-training/index.php">In-House Training</a></li>
</ul>

                    </li>

                    <li><a href="../../reports/index.php">Reports</a></li>
                    <li><a href="../index.php">Articles</a></li>
                    <li class="has-submenu"><a>About NN/G</a><i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../about/index.php">Overview</a></li>
                        <li><a href="../../people/index.php">People</a></li>
                        <li><a href="../../about/why-nng/index.php">Why NN/g?</a></li>
                        <li><a href="../../about/contact/index.php">Contact</a></li>
                        <li><a href="../../news/index.php">News</a></li>
                        <li><a href="../../about/history/index.php">History</a></li>
                        <li><a href="../../books/index.php">Books</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                </div>

                <hr class="header-divider show-for-small-only"/>


            </div>
       </div>

        <div id="hideNav">            <nav class="primary-nav small-12 column">
                <ul>
                    <li><a class="navitem" id="nav-home" href="../../index.php">Home</a></li>
                    <li><a class="navitem" id="nav-training" href="../../training/index.php">Training</a></li>
                    <li><a class="navitem" id="nav-consulting" href="../../consulting/index.php">Consulting</a></li>
                    <li><a class="navitem" id="nav-reports" href="../../reports/index.php">Reports</a></li>
                    <li><a class="navitem" id="nav-articles" href="../index.php">Articles</a></li>
                    <li><a class="navitem" id="nav-about" href="../../about/index.php">About NN/G</a></li>
                </ul>
            </nav>
            <nav class="secondary-nav small-12 medium-12 large-12 columns">
                
                
            </nav>
        </div>
    </header>
    <div id="main" class="l-content" onclick="closeNav(event)"><span id="preventClose" style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776; Menu</span>
    
  

  

  <div class="row">
    
      
      <section class="small-12 medium-3 columns collapsable-content l-navrail articles-left-column">
        <h1 class="show-for-small-down collapse-link mobile-browse-header">Browse by Topic and Author</h1>
          <div class="row collapsable">

            
            <div class="small-6 medium-12 columns">
              <section id="article-popular-topics" style="display: none;">
                
                <h1>Topics</h1>
                <ul id="article-popular-topics">
                    
                    <li>
                        
                        <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/intranets/all/index.php">Intranets</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/user-testing/all/index.php">User Testing</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                        
                    </li>
                    
                    <li><a href="#" id="article-see-all-topics"><strong>See all topics&hellip;</strong></a></li>
                </ul>
                
              </section>
              <section id="article-all-topics" style="display: none;">
                
                <h1>
                  <span class="show-for-medium-up">All Article Topics</span>
                  <span class="show-for-small-down">All Topics</span>
                  (<a href="#" id="article-hide-all-topics">hide</a>)
                </h1>
                <ul>
                  
                  <li>
                    
                    <a href="../../topic/accessibility/all/index.php">Accessibility</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/agile/all/index.php">Agile</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/analytics-and-metrics/all/index.php">Analytics &amp; Metrics</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/applications/all/index.php">Application Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/b2b-websites/all/index.php">B2B Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/behavior-patterns/all/index.php">Behavior Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/branding/all/index.php">Branding</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/content-strategy/all/index.php">Content Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/corporate-websites/all/index.php">Corporate Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/design-patterns/all/index.php">Design Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-design-process/all/index.php">Design Process</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/email/all/index.php">Email</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/eyetracking/all/index.php">Eyetracking</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/heuristic-evaluation/all/index.php">Heuristic Evaluation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/human-computer-interaction/all/index.php">Human Computer Interaction</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-ideation/all/index.php">Ideation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/information-architecture/all/index.php">Information Architecture</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/interaction-design/all/index.php">Interaction Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/international-users/all/index.php">International Users</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/intranets/all/index.php">Intranets</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-management/all/index.php">Management</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/navigation/all/index.php">Navigation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/non-profit-websites/all/index.php">Non-Profit Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/personas/all/index.php">Personas</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/persuasive-design/all/index.php">Persuasive Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/prototyping/all/index.php">Prototyping</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/psychology-and-ux/all/index.php">Psychology and UX</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/research-methods/all/index.php">Research Methods</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/search/all/index.php">Search</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/social-media/all/index.php">Social Media</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/strategy/all/index.php">Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/user-testing/all/index.php">User Testing</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-teams/all/index.php">UX Teams</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/visual-design/all/index.php">Visual Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/writing-web/all/index.php">Writing for the Web</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/young-users/all/index.php">Young Users</a>
                    
                  </li>
                  
                </ul>
                
              </section>
            </div>
            <script>
              /* Determine which topics list will be displayed: the full or the
               * partial.  We are inlining this close to the list itself to
               * prevent rendering delays on the client
               */
              var topic_list = $.cookie("article-topics");

              var popular_topics = $('#article-popular-topics');
              var all_topics = $('#article-all-topics');

              if(topic_list == 1) {
                  popular_topics.hide();
                  all_topics.show();
              } else {
                  popular_topics.show();
                  all_topics.hide();
              }
            </script>
            

            
            <div class="small-6 medium-12 columns">
              
              <section id="article-initial-authors">
                  <h1>Author</h1>
                  <ul>
                    
                    <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                    
                    <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                    
                    <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                    
                    <li><a href="#" onClick="$('#article-initial-authors').hide(); $('#article-all-authors').show();"><strong>See all authors&hellip;</strong></a></li>
                  </ul>
              </section>
              <section id="article-all-authors" style="display: none;">
                <h1>
                  All Authors
                  (<a href="#" id="article-hide-all-authors">hide</a>)
                </h1>
                <ul>
                  
                  <li><a href="../author/raluca-budiu/index.php">Raluca Budiu</a></li>
                  
                  <li><a href="../author/patty-caya/index.php">Patty Caya</a></li>
                  
                  <li><a href="../author/susan-farrell/index.php">Susan Farrell</a></li>
                  
                  <li><a href="../author/therese-fessenden/index.php">Therese Fessenden</a></li>
                  
                  <li><a href="../author/kim-flaherty/index.php">Kim Flaherty</a></li>
                  
                  <li><a href="../author/sarah-gibbons/index.php">Sarah Gibbons</a></li>
                  
                  <li><a href="../author/aurora-harley/index.php">Aurora Harley</a></li>
                  
                  <li><a href="../author/kate-kaplan/index.php">Kate Kaplan</a></li>
                  
                  <li><a href="../author/page-laubheimer/index.php">Page Laubheimer</a></li>
                  
                  <li><a href="../author/angela-li/index.php">Angie Li</a></li>
                  
                  <li><a href="../author/hoa-loranger/index.php">Hoa Loranger</a></li>
                  
                  <li><a href="../author/kate-meyer/index.php">Kate Meyer</a></li>
                  
                  <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                  
                  <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                  
                  <li><a href="../author/kara-pernice/index.php">Kara Pernice</a></li>
                  
                  <li><a href="../author/christian-rohrer/index.php">Christian Rohrer</a></li>
                  
                  <li><a href="../author/amy-schade/index.php">Amy Schade</a></li>
                  
                  <li><a href="../author/katie-sherwin/index.php">Katie Sherwin</a></li>
                  
                  <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                  
                  <li><a href="../author/kathryn-whitenton/index.php">Kathryn Whitenton</a></li>
                  
                </ul>
              </section>
              
            </div>
            
          </div>

          
          <section class="hide-for-small-down">
            
              
              <h1>Recent Articles</h1>
              
              <ul>
                  
                  <li><a href="../seamless-cross-channel/index.php">Seamlessness in the Omnichannel User Experience</a></li>
                  
                  <li><a href="../brand-intention-interpretation/index.php">Experience Design: Bridging Brand Intention and Brand Interpretation</a></li>
                  
                  <li><a href="../eyetracking-tasks-efficient-scanning/index.php">Scanning Patterns on the Web Are Optimized for the Current Task</a></li>
                  
                  <li><a href="../flat-design-best-practices/index.php">Flat-Design Best Practices</a></li>
                  
                  <li><a href="../ux-user-stories/index.php">Accounting for UX Work with User Stories in Agile Projects</a></li>
                  
                  <li><a href="../index.php"><strong>See all articles&hellip;</strong></a></li>
              </ul>
              
            
          </section>
          

          
          
          <section class="hide-for-small-down">
            <h1>Popular Articles</h1>
            
            <ul>
                
                <li><a href="../usability-101-introduction-to-usability/index.php">Usability 101: Introduction to Usability</a></li>
                
                <li><a href="../top-10-mistakes-web-design/index.php">Top 10 Mistakes in Web Design</a></li>
                
                <li><a href="../how-users-read-on-the-web/index.php">How Users Read on the Web</a></li>
                
                <li><a href="../f-shaped-pattern-reading-web-content/index.php">F-Shaped Pattern For Reading Web Content</a></li>
                
                <li><a href="../ten-usability-heuristics/index.php">10 Usability Heuristics for User Interface Design</a></li>
                
                <li><a href="../intranet-design-2016/index.php">10 Best Intranets of 2016</a></li>
                
                <li><a href="../which-ux-research-methods/index.php">When to Use Which User-Experience Research Methods</a></li>
                
                <li><a href="../response-times-3-important-limits/index.php">Response Times: The 3 Important Limits</a></li>
                
                <li><a href="../why-you-only-need-to-test-with-5-users/index.php">Why You Only Need to Test with 5 Users</a></li>
                
                <li><a href="../page-fold-manifesto/index.php">The Fold Manifesto: Why the Page Fold Still Matters</a></li>
                
            </ul>
          </section>
          
          

          
            <section class="hide-for-small-down">
              
            </section>
          

      </section>
      

      
        <div class="small-12 medium-9 columns content-body">
          




<article class="l-subsection alertbox-article-content article-detail-bl">
  
  <header>
    <div class="l-subsection">
      <h1>The Power Law of Learning: Consistency vs. Innovation in User Interfaces </h1>
      <div class="author-meta">by
        
          
            
            <a href="../author/raluca-budiu/index.php">Raluca Budiu</a>
            
          
        on  October 30, 2016
        
      </div>

      

      
      <div class="topic-links">
        Topics:
        
          <ul>

  <li><a href="../../topic/psychology-and-ux/index.php">Psychology and UX</a></li>

</ul>
        
      </div>
      
    </div>
  </header>
  

  
  
  <section>
    <p class="lede"><strong>Summary:</strong> Across many tasks, learning curves show an initial learning period, followed by a plateau of optimal efficiency. New interfaces compete with much practiced, old ones that have already reached this plateau.</p>
    <br />
  </section>
  
  

  
  <section id="article-body"><p>In response to one of our recent articles on centered logos vs. left-aligned logos, one reader tweeted: “Every @NNgroup newsletter, summarized: Do things exactly the way every other website already does them, or your users will be confused.”</p>

<p><img alt="" height="121" src="https://media.nngroup.com/media/editor/2016/09/26/tweet.png" width="600"/></p>

<p>Although obviously a hyperbole (we publish articles on a wide variety of topics), the tweet does identify a big theme in many of our articles and one of the main principles of user experience: <strong>consistency</strong>. Consistency is one of the original <a href="../ten-usability-heuristics/index.php">10 usability heuristics</a> and is a corollary of <a href="../do-interface-standards-stifle-design-creativity/index.php">Jakob’s law of Internet use</a>. As disappointing as it may be for designers to have to follow the same beaten path, we preach consistency again and again for reasons deeply rooted in basic human behavior. In this article, we explain the most fundamental of these reasons: the power law of learning.</p>

<h2>Learning Studies and Learning Curves</h2>

<p>The best way to measure how people learn a task or an interface is by running a <strong>learning experiment</strong>. In a learning experiment, people come into the lab and do the same task <em>multiple times</em>. Each time the person does the task, the experimenter records one or more quantitative metrics (usually, the time it takes to do that task and the number of errors). If the measures get better, people have learned from their previous experience, and the numbers show how much. The repetitions of the same tasks may or may not be separated by different activities, and sometimes participants are even sent home between two measurements, and asked to come back after a day, a week, or a month.</p>

<p>One of the first rigorous learning experiments was described by Hermann Ebbinghaus in 1885 in his book on human memory. Since then, many other learning experiments have been reported in the psychology, human-factors, and HCI literature. All these experiments show that “practice makes perfect:” when people do the same task over and over again, they get better and faster. The chart below shows the results from one such study by David Ahlstrom and colleagues, who were investigating <a href="../expandable-menus/index.php">pie menus</a> and comparing them with other types of menus.</p>

<figure class="caption"><img alt="" height="417" src="https://media.nngroup.com/media/editor/2016/09/26/chart1.PNG" width="700"/>
<figcaption><em>Ahlstrom et al.’s study had participants interact with the same menu interface for 8 different practice blocks — in each block participants selected the same 6 items in the menu, and the obtained selection times were averaged to get the mean time for that block. The learning curve shows that the mean selection time decreases with practice.</em></figcaption>
</figure>

<p>This type of graph that plots the results from a learning experiment is a learning curve.<strong>A learning curve</strong> describes how a specific quantitative measure of the same human behavior changes as a function of time. In the menu experiment, the measure of interest is the task time — the mean time to select an option inside the menu. But the measure can vary from one learning experiment to another: it can be any metric that you’d expect to change as a result of learning. For example, if we were interested in UX education, we may ask people with the same background to facilitate a user test once, and measure how many facilitation errors they make. We would give them feedback on those errors, and then we would ask them to come a second time to facilitate a user test. After a few such repetitions, we would plot the average number of errors made for each test. That graphic of errors over time would represent a learning curve.</p>

<h2>The Power Law of Learning</h2>

<p>In the 1980s, Allen Newell, a famous Carnegie Mellon cognitive scientist, analyzed reaction times for a variety of tasks reported in learning experiments and he noted that the learning curves obtained in all these studies had a very similar shape: that of a so-called <strong>power law</strong>. Power laws have a nice mathematical property: when you plot them in log-log scale, you obtain a straight line.</p>

<figure class="caption"><img alt="" height="416" src="https://media.nngroup.com/media/editor/2016/09/26/chart2-log-pie.PNG" width="700"/>
<figcaption><em>The learning curve in Ahlstrom’s menu experiment is described by a power law; when plotted in log–log scale, it is well approximated by a straight line.</em></figcaption>
</figure>

<p style="margin-left:.5in;">Definition: <strong>The power law of learning</strong> says that (1) the time it takes to perform a task decreases with the number of repetitions of that task; and (2) the decrease follows the shape of a power law.</p>

<p>Newell focused primarily on time as the quantitative measure of learning, but there is evidence that the power law holds for other measures as well.</p>

<h2>Analyzing a Learning Curve</h2>

<p>Although learning curves can be described by power laws, they won’t be described by the <em>same</em> power law.</p>

<p>Let’s assume that we were interested in analyzing the learnability of three different interfaces A, B, and C for the same task (e.g., answering a customer query in a call center). For each design, we ask participants to complete the task in different, repeated trials and we measure the task time for every trial. Then we plot the average task time corresponding to each repetition and we obtain three learning curves like the ones in the figure below.</p>

<figure class="caption"><img alt="" height="578" src="https://media.nngroup.com/media/editor/2016/09/26/chart3.PNG" width="700"/>
<figcaption><em>Three learning curves for three different interfaces</em></figcaption>
</figure>

<p>Notice that in the first trial participants take roughly the same amount of time with all designs. But by the second repetition, design A is much faster than designs B or C. By the 3<sup>rd</sup> repetition design A speeds up even more, and after the 4<sup>th</sup> repetition the reaction times reach a plateau: the curve flattens out and the users have learned the interface as much as possible. There are no more improvements to be expected, and extra repetitions will only decrease the reaction time insignificantly. We can say that, with design A, <strong>learning is saturated after the 4<sup>th</sup> repetition </strong>(or that 4 is the saturation point for design A)<strong>.</strong></p>

<p>The learning curve for design C also flattens, but the plateau is reached later, by approximately the 10<sup>th</sup> or 11<sup>th</sup> repetition. So design C requires more practice to stabilize the performance. In other words, it takes people more trials to learn how to use design C than design A.</p>

<p>Moreover, design A exhibits more improvement: the difference between the highest and the lowest points on the learning curve is approximately 20s (22 for repetition 1 and 2 for repetition 15), whereas for design C this difference is approximately 19s. So with design C participants don’t speed up as much as they do with design A.</p>

<p>Design B also reaches saturation later than design A (approximately by repetition 11), but the improvement is bigger. More importantly, the expected task time after the interface has been learned is lower for design B than for design A (1s vs. 2s). In other words, design B takes longer to learn than design A, but once it has been learned, people are faster at using it.</p>

<p>The choice between A and C is easy: A is better in every way, with an earlier saturation point, a greater speedup, and a superior task time once the interface has been learned. But the choice between A and B depends on whether in real life users will be exposed to enough repetitions to reach the saturation plateau. If, for instance, you expect people to use the interface every day as part of their work, then it makes sense to go with design B, because in the long run it will save more time. (During the first week of use, A will be better, but halfway through the second work week B becomes better, and then it stays better forever.) However, if your users will use the design occasionally, with large intervals of time between two different sessions, then design A will be better because it will help people learn the interface faster.</p>

<p>Let’s consider two enterprise software examples:</p>

<ul>
	<li>The employee directory: assuming that most people use it once a day, we should prefer a user interface like design B for this type of application.</li>
	<li>Reclaiming value-added taxes for foreign travel in expense reporting: we should prefer a user interface like design A if most people go on, say, at most one business trip abroad each year.</li>
</ul>

<p>The ratio between the improvement and the saturation point indicates the <em>slope of the learning curve</em>: if the curve drops a lot and fast, then it means that the interface is highly learnable. If the curve drops only a little, or it takes many trials to reach the saturation point, the interface is less learnable. So the term “steep learning curve” is actually a misnomer — in reality, steep learning curves are good. They mean that the improvement is substantial and that it happens fast.</p>

<h2>Memory and the Power Law of Learning</h2>

<p>Romans used to say that “repetition is the mother of learning” — the more we rehearse a piece of information, the more likely we’ll be to remember it. Not only that, but we’ll also be faster at retrieving it from memory. When applied to human memory, the power law of learning says that the time it takes to retrieve a piece of information from memory depends on how much we’ve used that information in the past, and this dependence follows a power law. Thus, we are fluent with concepts and patterns that we use every day in our work, yet high-school math (such as the definition of logarithms) or other facts that we don’t often encounter are hard to remember, because we haven’t used them often enough.</p>

<figure class="caption"><img alt="" height="414" src="https://media.nngroup.com/media/editor/2016/09/26/chart4-memory.PNG" width="700"/>
<figcaption><em>In an experiment reported by Peter Pirolli and John R. Anderson (1985), the time it took participants to recognize facts that they had studied decreased with the number of days they had practiced those facts. The curve follows a power law and reaches a saturation level approximately around day 12.</em></figcaption>
</figure>

<p>Items that are practiced a lot acquire <a href="../recognition-and-recall/index.php">a high activation in our memory</a> and are retrieved faster. Whenever we’re trying to solve a problem or recall a piece of information, the first things that come to mind are those items in our memory that have a raised activation. Let’s say you want to navigate to the homepage of a site. You may have encountered multiple solutions to this problem in the past — for example, clicking the logo or clicking a <em>Home </em>link. All these solutions will compete in a “race” in your memory and you will select the one that gets to the finish line first. But, based on the power law of learning, the one that wins the race is the one that’s been practiced most often. Of course, if the first winning solution doesn’t work, people will try the next best. But they will also start feeling annoyed and perceive the problem as harder, and the site as less usable.</p>

<p>The key implications of this research are as follows:</p>

<ul>
	<li>The power law of learning is real: it’s been proven in countless experiments during a very long period (the 19<sup>th</sup>, 20<sup>th</sup>, and 21<sup>st</sup> centuries). It’s the way the human brain works, and no degree of wishful thinking or new gadgets will change this. Design for it.</li>
	<li>Learning is not a dichotomy (as a simplistic model might have assumed), where either you know something or you don’t. The more something is practiced, and the more recently it was practiced, the better it’s known.</li>
	<li>Just showing users a tutorial or help screen isn’t enough to make them learn something well.</li>
	<li>Doing something often is the way to strong learning.</li>
</ul>

<h2>Consistency = Boring, Old Interfaces?</h2>

<p>We’ve seen that every repetition helps users practice a concept or an action. So, by being consistent with other sites, you’re giving users one more repetition of a highly common UI element, and you’re also reaping the benefits of practice on all these other websites. Remember Jakob’s law: your users spend most of their time on other websites.</p>

<figure class="caption"><img alt="" height="452" src="https://media.nngroup.com/media/editor/2016/09/26/chart5.PNG" width="700"/>
<figcaption><em>Learning curves for two different interfaces: when the new interface is introduced, the old one is already at saturation level (repetition 1 for the new interface corresponds to repetition 5 for the old one). It’s going to take a lot more time and good will for the user to put up with the new suboptimal interface than to continue using the old one.</em></figcaption>
</figure>

<p>As shown in the graph above, when you are creating a new design pattern that goes against an old, familiar one (e.g., logo on the <a href="../logo-placement-brand-recall/index.php">right</a> of the page instead of on the left, <a href="../horizontal-scrolling/index.php">horizontal scrolling</a> instead of vertical scrolling on desktop, <a href="../mobile-first-not-mobile-only/index.php">hamburger menu instead of a navigation bar</a> on desktop), the learning curve for the new pattern will be in the steep, high part of the first repetitions, while the learning curve for the competing old alternative will have already reached saturation. It’s going to take more than a few repetitions for your new design to also reach saturation and perhaps prove better than the competing one. Unless your users are captive and you can force them to practice, chances are that they will give up and go elsewhere instead of putting up with a harder to use design: <a href="../fresh-vs-familiar-aggressive-redesign/index.php">users hate change</a>.</p>

<p>So that means there’s no hope for innovation, right? We are doomed to have the search box in the top right corner, the <a href="../centered-logos/index.php">logo on the left</a>, and the navigation in a bar?</p>

<p>Any type of innovation will incur a cost for users and for designers. For users, because it will be a new pattern that they must learn and that takes them on an untrodden, slow path. For designers, because they must provide extra scaffolds such as contextual <a href="../mobile-instructional-overlay/index.php">tips</a> and <a href="../progressive-disclosure/index.php">progressive disclosure</a> to help users navigate on the new path. The cost of implementing these tools can be significant. Think twice at what you are trying to achieve — is it worth departing from the well-beaten path? Does it make sense to innovate or will you be just as well served by a traditional design?</p>

<p>It also means that innovation is easier push when you have a captive audience or when the <a href="../perceived-value/index.php">perceived value</a> of a <a href="../brand-experience-ux/index.php">brand</a> is a lot bigger than the cost of using a new design pattern. That’s why traditionally, big companies with a large user base (think Apple and iOS or Google and Android, to a lesser extent) can afford to innovate — because people who are already using these platforms will have to put up with the new interface (especially if the company is pushing updates aggressively, like Apple does with iOS, or the innovation happens in an enterprise, where users don’t have a choice to go back to an older version of the interface).</p>

<p>It’s also easier to innovate if your users will experience the new interface very often, perhaps several times a day. That means that people will get faster to the saturation part of the learning curve because they will have quite a few opportunities to practice. (Yet, if the saturation point is too far in the future, people may actually never get there. <a href="../windows-8-disappointing-usability/index.php">Windows 8</a> is a live proof of that: Microsoft ended up changing the design instead of waiting for users to reach the saturation plateau.)</p>

<p>Innovation can also happen if designers adopt it en masse and create a new standard. If all websites rebelled tomorrow and started placing the logo in the top right corner, then users would get the repetitions needed to reach saturation relatively fast, everywhere. Usually, this process takes time, but it did happen with design elements such as the swipe-to-delete gesture in iOS or the <a href="../hamburger-menus/index.php">hamburger menu on mobile</a>.</p>

<p>We can make a simple decision tree for whether to introduce a deviant user interface in cases where a conventional design is already well established:</p>

<ol>
	<li>Will the new design perform <em>much</em> better than the old, once users have “descended” the learning curve? If not, don’t even try.</li>
	<li>Is it credible that users will be willing to try the new design again and again, until they have learned it well enough to realize those long-term benefits? If people are likely to give up (e.g., leave a website for a competing, familiar design), then don’t introduce the new design.</li>
	<li>Can you speed up learning, either by exposing users to the new design more often or by making it easier to learn? If yes, you will increase the proportion of users who will be willing to embrace the new design.</li>
</ol>

<p>So yes, consistency is the curse of innovation in design. If you’re convinced that, once your users will have learned the interface, they will save time over the status quo, then it can be worth trying. But remember that the path to innovation is circuitous and costly, and if your users won’t have many learning opportunities, they may never reach that optimal-performance plateau accessible only after learning has happened.</p>

<p>Learn more about human memory and the power law of learning in our seminar on <a href="file:///C:/Users/Raluca/AppData/Local/Microsoft/Windows/INetCache/Content.Outlook/ZP50FNW6/The Human Mind and Usability">The Human Mind and Usability</a>.</p>

<h2>References</h2>

<p>David Ahlstrom, Andy Cockburn, Carl Gutwin, Pourang Irani (2010). Why It’s Quick to Be Square: Modelling New and Existing Hierarchical Menu Designs. CHI 2010.</p>

<p>Hermann Ebbinghaus, (1885). <a href="http://psy.ed.asu.edu/~classics/Ebbinghaus/index.htm"><em>Memory: A contribution to experimental psychology</em></a><em>.</em> New York: Dover.</p>

<p>Allen Newell, Paul Rosenbloom (1980). <a href="http://repository.cmu.edu/cgi/viewcontent.cgi?article=3420&amp;context=compsci"><em>Mechanisms of skill acquisition and the law of practice</em></a><em>. </em>Technical Report. School of Computer Science, Carnegie Mellon University.</p>

<p>Peter Pirolli, John R. Anderson, J. R. (1985). <a href="http://act-r.psy.cmu.edu/wordpress/wp-content/uploads/2012/12/283pirolli-1985-practice.pdf">The role of practice in fact retrieval</a>. <em>Journal of Experimental Psychology: Learning, Memory, &amp; Cognition</em>, 11, 136-153.</p>
</section>
  

  
  <section>
    
        
    
  </section>
  
  
  
  
  

  


  <section>
    <br />
    <p class="share"><b>Share <span class="hide-for-small-down">this article</span>:</b> <a href="https://twitter.com/intent/tweet?url=http://www.nngroup.com/articles/power-law-learning/&amp;text=The%20Power%20Law%20of%20Learning:%20Consistency%20vs.%20Innovation%20in%20User%20Interfaces%20&amp;via=nngroup">Twitter</a> | <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=http://www.nngroup.com/articles/power-law-learning/&amp;title=The%20Power%20Law%20of%20Learning:%20Consistency%20vs.%20Innovation%20in%20User%20Interfaces%20&amp;source=Nielsen%20Norman%20Group">LinkedIn</a> | <a href="https://plus.google.com/share?url=http://www.nngroup.com/articles/power-law-learning/">Google+</a> | <a href="mailto:?subject=NN/g Article: The Power Law of Learning: Consistency vs. Innovation in User Interfaces &amp;body=http://www.nngroup.com/articles/power-law-learning/">Email</a></p>
    <hr />
  </section>

  <footer class="l-subsection">
    
    
    <div class="learn-more">
      <h2>Learn More</h2>
      <p><a href="../subscribe/index.php">Subscribe to the weekly newsletter</a> to get notified about future articles.</p>
      </div>
    
    

    <div class="row show-for-small-down related-content">
      
      
      

      
      
        <div class="column small-12 topic-links">All articles about:
          <ul>
            
            <li><a href="../../topic/learning/index.php">learning</a></li>
            
            <li><a href="../../topic/memory/index.php">memory</a></li>
            
            <li><a href="../../topic/psychology-and-ux/index.php">Psychology and UX</a></li>
            
          </ul>
        </div>
      
      

      
      
        <div class="column small-12">
          <h3>UX Conference Training Courses</h3>
           <ul class="no-bullet">
    
        <li><a href="../../courses/human-mind/index.php">The Human Mind and Usability</a></li>
    
        <li><a href="../../courses/credibility-and-persuasive-web-design/index.php">Persuasive Web Design</a></li>
    
        <li><a href="../../courses/website-design-lessons-social-psychology/index.php">Website Design Lessons from Social Psychology</a></li>
    
</ul>
        </div>
      
      

      
      
        <div class="small-12 column">
          <h3>Research Reports</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../reports/user-experience-careers/index.php">User Experience Careers</a></li>
              
            
          </ul>
        </div>
      
      

      
      
      
    </div>

    <div class="hide-for-small-down row related-content">
      <div class="medium-6 columns">
        
        
          <div>
            <h3>Articles</h3>
            <ul class="no-bullet">
              
                
                <li><a href="../priming/index.php">Priming and User Interfaces</a></li>
                
              
                
                <li><a href="../password-creation/index.php">Password Creation: 3 Ways To Make It Easier</a></li>
                
              
                
                <li><a href="../negativity-bias-ux/index.php">The Negativity Bias in User Experience</a></li>
                
              
                
                <li><a href="../short-term-memory-and-web-usability/index.php">Short-Term Memory and Web Usability</a></li>
                
              
                
                <li><a href="../computer-skill-levels/index.php">The Distribution of Users’ Computer Skills: Worse Than You Think</a></li>
                
              
            </ul>
          </div>
        
        

        
          
          <div>
            <h3>Research Reports</h3>
            <ul class="no-bullet">
              
                
                  <li><a href="../../reports/user-experience-careers/index.php">User Experience Careers</a></li>
                
              
            </ul>
          </div>
          
        

      </div>
      <div class="medium-6 columns">
        
          
          <div>
            <h3>UX Conference Training Courses</h3>
             <ul class="no-bullet">
    
        <li><a href="../../courses/human-mind/index.php">The Human Mind and Usability</a></li>
    
        <li><a href="../../courses/credibility-and-persuasive-web-design/index.php">Persuasive Web Design</a></li>
    
        <li><a href="../../courses/website-design-lessons-social-psychology/index.php">Website Design Lessons from Social Psychology</a></li>
    
</ul>
          </div>
          
        
        
      </div>
    </div>
  </footer>
</article>

        </div>

      

    
  </div>
  <div class="row">
    
      <div class="column small-12 show-for-small-down l-navrail announcements">
        
          
        
      </div>
    
    
  </div>

    </div>
    




<footer id="page-footer">
    <div class="row hide-for-print">
        <div class="column small-12 medium-6">
            <section class="footer-popular">
                
                <h1>Popular Topics</h1>
                <div style="float: left;  width: 15em; display: block;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/applications/index.php">Application Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/e-commerce/index.php">E-Commerce Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/eyetracking/index.php">Eyetracking </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/information-architecture/index.php">Information Architecture </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/intranets/index.php">Intranet Design </a></li>
	<li><a href="../../topic/interaction-design/index.php">Interaction Design </a></li>
</ul>
</div>

<div style="float: left;  width: 15em; display: block; list-style: none;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/mobile-and-tablet-design/index.php">Mobile and Tablet Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/credibility-persuasion/index.php">Persuasive Design</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/research-methods/index.php">Research Methods</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/user-testing/index.php">User Testing </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/web-usability/index.php">Web Usability </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/writing-web/index.php">Web Writing &amp; Content Strategy </a></li>
</ul>
</div>

<p>&nbsp;</p>

                
            </section>
        </div>

        <div class="column small-12 medium-6">
            <div class="row">
                <div class="column small-6">
                    <section class="footer-about">
                        <h1>Follow Us</h1>
                        <ul>
	<li><a href="../subscribe/index.php">Subscribe to our weekly UX newsletter</a>,&nbsp;Jakob Nielsen&#39;s Alertbox</li>
	<li>Visit Don Norman&#39;s website, <a href="http://www.jnd.org/"> www.jnd.org </a></li>
	<li>Visit Bruce Tognazzini&#39;s website, <a href="http://www.asktog.com/"> www.asktog.com </a></li>
	<li style="padding-top: 6px;"><a class="twitter-follow-button" data-show-count="false" data-size="large" href="https://twitter.com/nngroup"><img alt="Follow @nngroup on Twitter" height="20" src="https://media.nngroup.com/media/editor/2016/12/30/twitter-follow-button.png" style="width: 117px; height: 20px;" width="117" /></a></li>
	<li style="padding-top: 6px;"><img alt="" height="14" src="https://media.nngroup.com/media/editor/2016/07/26/feed-icon-14x14.png" style="vertical-align:text-top;width:14px;margin-right:.25em;" width="14" /><a href="../../feed/rss/index.php">Subscribe to our RSS feed</a></li>
</ul>

                    </section>
                </div>

                <div class="column small-6">
                    <section class="footer-about right-column">
                        <h1>About
</h1>
                        <ul class="no-bullet">
	<li><a href="../../about/contact/index.php">Contact information</a></li>
	<li><a href="../../about/index.php">About Us</a></li>
	<li><a href="../../about/why-nng/index.php">Why NN/g? </a></li>
	<li><a href="../../return-policy/index.php">Return Policy</a></li>
</ul>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="copyright column small-12">
            <p><a href="../../copyright-and-reprint-info/index.php">Copyright</a> &copy; 1998-2017 Nielsen Norman Group, All Rights Reserved.</p>
        </div>
    </div>
</footer>


    


        <script src="https://media.nngroup.com/static/vendor/fastclick/js/fastclick.min.js"></script>
        <script src="https://media.nngroup.com/static/vendor/foundation/js/foundation.min.js"></script>
        <script src="http://media.nngroup.com/static/js/app.js"></script>
        <script src="http://media.nngroup.com/static/js/scroll-to-fixed.js"></script><script>var externalLinks = document.querySelectorAll('a[href^="http"]');for (var i = externalLinks.length-1; i >= 0; i--) {externalLinks[i].addEventListener("click", function(event) { alert("External links are disabled for this test."); event.preventDefault(); }, false);}var mailLinks = document.querySelectorAll('a[href^="mailto"]');for (var i = mailLinks.length-1; i >= 0; i--) {mailLinks[i].addEventListener("click", function(event) { alert("Email links are disabled for this test."); event.preventDefault(); }, false);}var externalForms = document.querySelectorAll('form');for (var i = externalForms.length-1; i >= 0; i--) {externalForms[i].addEventListener("submit", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false); externalForms[i].addEventListener("click", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false);}</script>

        <script>
            
            $(function() {
                $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
                $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
            });
        </script>

        
<script src="https://media.nngroup.com/static/js/articles.js"></script>


        
        <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1072699960;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072699960/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
    </body>

<!-- Mirrored from www.nngroup.com/articles/power-law-learning/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:06:19 GMT -->
</html>
