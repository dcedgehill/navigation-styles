<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->





    
<!-- Mirrored from www.nngroup.com/articles/1994-web-usability-report/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:15:01 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1026.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"beacon":"bam.nr-data.net","queueTime":2,"applicationTime":615,"agent":"","licenseKey":"1a30ef9931","errorBeacon":"bam.nr-data.net","applicationID":"2048992","transactionName":"NFxSMRBUX0EDUkIPDA0WdhAMVkVbDV8ZBxEXUFMJB0YfRAtUQRVZIktEDAFZVHYHRVcPD01eVRE="}</script>
        <title>1994 Web Usability Study: Article by Jakob Nielsen</title><meta property="og:title" content="1994 Web Usability Study: Article by Jakob Nielsen" />
  
        
        <meta name="description" content="Paper by Jakob Nielsen summarizing usability results conducted in 1994 on the web sites of Hewlett-Packard, IBM, Microsoft, Sun Microsystems, and Time Warner; the report includes screen captures of several famous early websites and because it is one of the first formal usability studies of the Web. ">
        <meta property="og:description" content="Paper by Jakob Nielsen summarizing usability results conducted in 1994 on the web sites of Hewlett-Packard, IBM, Microsoft, Sun Microsystems, and Time Warner; the report includes screen captures of several famous early websites and because it is one of the first formal usability studies of the Web. " />
        
  
        
	
        

        

        <meta name="robots" content="noarchive">
        <meta name="viewport" content="width=device-width">

        <link rel="search" type="application/opensearchdescription+xml" title="NN/g Search" href="../../opensearch.xml" />
        <link rel="shortcut icon" href="http://media.nngroup.com/static/img/favicon.ico" />

        <link rel="stylesheet" href="https://media.nngroup.com/static/vendor/font-awesome/css/font-awesome.min.css">

        
        <link rel="stylesheet" href="https://media.nngroup.com/static/css/7232a960f1df.css" type="text/css" />
        <link rel="stylesheet" href="http://fast.fonts.com/t/1.css?apiType=css&amp;projectid=c5846260-9def-46ec-9407-0f1349bd2d64" type="text/css">

        <script src="https://media.nngroup.com/static/vendor/modernizr/js/modernizr.min.js"></script>

        <script type="text/javascript" src="https://media.nngroup.com/static/js/3283be5def74.js"></script>
        

        

        
        
        <!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=229034,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('http://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->


<!-- Google Analytics Tracking Code for http://nngroup.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23132681-1', 'nngroup.com');
  ga('require', 'linkid', 'https://www.nngroup.com/articles/1994-web-usability-report/linkid.js');
  ga('require', 'displayfeatures');

// Optimizely Universal Analytics Integration
 window.optimizely = window.optimizely || [];
 window.optimizely.push("activateUniversalAnalytics");

  ga('send', 'pageview');

</script>
<script>
/**
* Function to track a click on an outbound link in Google Analytics.
*/
function trackOutboundLink(label, url) {
    if(window.ga && ga.loaded) {
        ga('send', 'event', 'outbound', 'click', label, {'hitCallback': function(){document.location = url;}
        });
    } else {
        document.location = url;
    }
}
/**
* Function to track a click on a link in the article announcement area.
*/
$('#articleAnnouncement a').on('click', function(){
  ga('send', 'event', 'link', 'click', 'articleAnnouncement');
});
</script>

<script type="text/javascript">
 setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2421.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



<!-- Hotjar Tracking Code for http://nngroup.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:121605,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

        <script src="https://media.nngroup.com/static/js/analytics/track_outbound_link.js"></script>
        
        
        
    </head>
    <body class="location-articles articles-detail">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
    <header id="page-header">
        <div id="cart" class="shopping-cart mobile-cart show-for-small-down"></div>
        <div class="row">
            <div id="masthead" class="masthead small-12 medium-7 column">

                <!-- The button to toggles menu on small screen -->

                <h1><a href="../../index.php"><img src="http://media.nngroup.com/static/img/logo-wordmark.png" width="74" height="30"> Nielsen <span class="norman">Norman</span> Group</a></h1>
                <p class="tagline show-for-medium-up">Evidence-Based User Experience Research, Training, and Consulting</p>

                <!-- The button to toggles form search on small screen -->


                <div style="clear: right;width:  100%;height: 1px;"></div><?php define('ROOT_PATH',$_SERVER["DOCUMENT_ROOT"] . '/apps/navigation_styles/navigations/2'); ?><?php include(ROOT_PATH)."/navigation.php"; ?>

                <!-- Drop down menu on small screen -->
                <div id="hideNav" class="show-for-small-down">
                  
                  <ul class="menu-content">
                    <li><a href="../../index.php">Home</a></li>
                    <li class="has-submenu">
                      <a>Training</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../training/index.php">Overview</a></li>
                        
<li id="nav-ux-conference"><a href="../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../online-seminars/index.php"><span>Online Seminars</span></a></li>

                      </ul>
                    </li>
                    <li class="has-submenu">
                      <a>Consulting</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
  <li><a href="../../consulting/index.php">Overview</a></li>
  
  <li><a href="../../consulting/benchmarking/index.php">Benchmarking</a></li>
  
  <li><a href="../../consulting/expert-design-review/index.php">Expert Design Review</a></li>
  
  <li><a href="../../consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
  
  <li><a href="../../consulting/usability-testing/index.php">Usability Testing </a></li>
  
  <li><a href="../../consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
  
  <li><a href="../../in-house-training/index.php">In-House Training</a></li>
</ul>

                    </li>

                    <li><a href="../../reports/index.php">Reports</a></li>
                    <li><a href="../index.php">Articles</a></li>
                    <li class="has-submenu"><a>About NN/G</a><i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../about/index.php">Overview</a></li>
                        <li><a href="../../people/index.php">People</a></li>
                        <li><a href="../../about/why-nng/index.php">Why NN/g?</a></li>
                        <li><a href="../../about/contact/index.php">Contact</a></li>
                        <li><a href="../../news/index.php">News</a></li>
                        <li><a href="../../about/history/index.php">History</a></li>
                        <li><a href="../../books/index.php">Books</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                </div>

                <hr class="header-divider show-for-small-only"/>


            </div>
       </div>

        <div id="hideNav">            <nav class="primary-nav small-12 column">
                <ul>
                    <li><a class="navitem" id="nav-home" href="../../index.php">Home</a></li>
                    <li><a class="navitem" id="nav-training" href="../../training/index.php">Training</a></li>
                    <li><a class="navitem" id="nav-consulting" href="../../consulting/index.php">Consulting</a></li>
                    <li><a class="navitem" id="nav-reports" href="../../reports/index.php">Reports</a></li>
                    <li><a class="navitem" id="nav-articles" href="../index.php">Articles</a></li>
                    <li><a class="navitem" id="nav-about" href="../../about/index.php">About NN/G</a></li>
                </ul>
            </nav>
            <nav class="secondary-nav small-12 medium-12 large-12 columns">
                
                
            </nav>
        </div>
    </header>
    <div id="main" class="l-content" onclick="closeNav(event)"><span id="preventClose" style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776; Menu</span>
    
  

  

  <div class="row">
    
      
      <section class="small-12 medium-3 columns collapsable-content l-navrail articles-left-column">
        <h1 class="show-for-small-down collapse-link mobile-browse-header">Browse by Topic and Author</h1>
          <div class="row collapsable">

            
            <div class="small-6 medium-12 columns">
              <section id="article-popular-topics" style="display: none;">
                
                <h1>Topics</h1>
                <ul id="article-popular-topics">
                    
                    <li>
                        
                        <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/intranets/all/index.php">Intranets</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/user-testing/all/index.php">User Testing</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                        
                    </li>
                    
                    <li><a href="#" id="article-see-all-topics"><strong>See all topics&hellip;</strong></a></li>
                </ul>
                
              </section>
              <section id="article-all-topics" style="display: none;">
                
                <h1>
                  <span class="show-for-medium-up">All Article Topics</span>
                  <span class="show-for-small-down">All Topics</span>
                  (<a href="#" id="article-hide-all-topics">hide</a>)
                </h1>
                <ul>
                  
                  <li>
                    
                    <a href="../../topic/accessibility/all/index.php">Accessibility</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/agile/all/index.php">Agile</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/analytics-and-metrics/all/index.php">Analytics &amp; Metrics</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/applications/all/index.php">Application Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/b2b-websites/all/index.php">B2B Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/behavior-patterns/all/index.php">Behavior Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/branding/all/index.php">Branding</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/content-strategy/all/index.php">Content Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/corporate-websites/all/index.php">Corporate Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/design-patterns/all/index.php">Design Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-design-process/all/index.php">Design Process</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/email/all/index.php">Email</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/eyetracking/all/index.php">Eyetracking</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/heuristic-evaluation/all/index.php">Heuristic Evaluation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/human-computer-interaction/all/index.php">Human Computer Interaction</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-ideation/all/index.php">Ideation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/information-architecture/all/index.php">Information Architecture</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/interaction-design/all/index.php">Interaction Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/international-users/all/index.php">International Users</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/intranets/all/index.php">Intranets</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-management/all/index.php">Management</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/navigation/all/index.php">Navigation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/non-profit-websites/all/index.php">Non-Profit Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/personas/all/index.php">Personas</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/persuasive-design/all/index.php">Persuasive Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/prototyping/all/index.php">Prototyping</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/psychology-and-ux/all/index.php">Psychology and UX</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/research-methods/all/index.php">Research Methods</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/search/all/index.php">Search</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/social-media/all/index.php">Social Media</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/strategy/all/index.php">Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/user-testing/all/index.php">User Testing</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-teams/all/index.php">UX Teams</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/visual-design/all/index.php">Visual Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/writing-web/all/index.php">Writing for the Web</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/young-users/all/index.php">Young Users</a>
                    
                  </li>
                  
                </ul>
                
              </section>
            </div>
            <script>
              /* Determine which topics list will be displayed: the full or the
               * partial.  We are inlining this close to the list itself to
               * prevent rendering delays on the client
               */
              var topic_list = $.cookie("article-topics");

              var popular_topics = $('#article-popular-topics');
              var all_topics = $('#article-all-topics');

              if(topic_list == 1) {
                  popular_topics.hide();
                  all_topics.show();
              } else {
                  popular_topics.show();
                  all_topics.hide();
              }
            </script>
            

            
            <div class="small-6 medium-12 columns">
              
              <section id="article-initial-authors">
                  <h1>Author</h1>
                  <ul>
                    
                    <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                    
                    <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                    
                    <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                    
                    <li><a href="#" onClick="$('#article-initial-authors').hide(); $('#article-all-authors').show();"><strong>See all authors&hellip;</strong></a></li>
                  </ul>
              </section>
              <section id="article-all-authors" style="display: none;">
                <h1>
                  All Authors
                  (<a href="#" id="article-hide-all-authors">hide</a>)
                </h1>
                <ul>
                  
                  <li><a href="../author/raluca-budiu/index.php">Raluca Budiu</a></li>
                  
                  <li><a href="../author/patty-caya/index.php">Patty Caya</a></li>
                  
                  <li><a href="../author/susan-farrell/index.php">Susan Farrell</a></li>
                  
                  <li><a href="../author/therese-fessenden/index.php">Therese Fessenden</a></li>
                  
                  <li><a href="../author/kim-flaherty/index.php">Kim Flaherty</a></li>
                  
                  <li><a href="../author/sarah-gibbons/index.php">Sarah Gibbons</a></li>
                  
                  <li><a href="../author/aurora-harley/index.php">Aurora Harley</a></li>
                  
                  <li><a href="../author/kate-kaplan/index.php">Kate Kaplan</a></li>
                  
                  <li><a href="../author/page-laubheimer/index.php">Page Laubheimer</a></li>
                  
                  <li><a href="../author/angela-li/index.php">Angie Li</a></li>
                  
                  <li><a href="../author/hoa-loranger/index.php">Hoa Loranger</a></li>
                  
                  <li><a href="../author/kate-meyer/index.php">Kate Meyer</a></li>
                  
                  <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                  
                  <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                  
                  <li><a href="../author/kara-pernice/index.php">Kara Pernice</a></li>
                  
                  <li><a href="../author/christian-rohrer/index.php">Christian Rohrer</a></li>
                  
                  <li><a href="../author/amy-schade/index.php">Amy Schade</a></li>
                  
                  <li><a href="../author/katie-sherwin/index.php">Katie Sherwin</a></li>
                  
                  <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                  
                  <li><a href="../author/kathryn-whitenton/index.php">Kathryn Whitenton</a></li>
                  
                </ul>
              </section>
              
            </div>
            
          </div>

          
          <section class="hide-for-small-down">
            
              
              <h1>Recent Articles</h1>
              
              <ul>
                  
                  <li><a href="../seamless-cross-channel/index.php">Seamlessness in the Omnichannel User Experience</a></li>
                  
                  <li><a href="../brand-intention-interpretation/index.php">Experience Design: Bridging Brand Intention and Brand Interpretation</a></li>
                  
                  <li><a href="../eyetracking-tasks-efficient-scanning/index.php">Scanning Patterns on the Web Are Optimized for the Current Task</a></li>
                  
                  <li><a href="../flat-design-best-practices/index.php">Flat-Design Best Practices</a></li>
                  
                  <li><a href="../ux-user-stories/index.php">Accounting for UX Work with User Stories in Agile Projects</a></li>
                  
                  <li><a href="../index.php"><strong>See all articles&hellip;</strong></a></li>
              </ul>
              
            
          </section>
          

          
          
          <section class="hide-for-small-down">
            <h1>Popular Articles</h1>
            
            <ul>
                
                <li><a href="../usability-101-introduction-to-usability/index.php">Usability 101: Introduction to Usability</a></li>
                
                <li><a href="../top-10-mistakes-web-design/index.php">Top 10 Mistakes in Web Design</a></li>
                
                <li><a href="../how-users-read-on-the-web/index.php">How Users Read on the Web</a></li>
                
                <li><a href="../f-shaped-pattern-reading-web-content/index.php">F-Shaped Pattern For Reading Web Content</a></li>
                
                <li><a href="../ten-usability-heuristics/index.php">10 Usability Heuristics for User Interface Design</a></li>
                
                <li><a href="../intranet-design-2016/index.php">10 Best Intranets of 2016</a></li>
                
                <li><a href="../which-ux-research-methods/index.php">When to Use Which User-Experience Research Methods</a></li>
                
                <li><a href="../response-times-3-important-limits/index.php">Response Times: The 3 Important Limits</a></li>
                
                <li><a href="../why-you-only-need-to-test-with-5-users/index.php">Why You Only Need to Test with 5 Users</a></li>
                
                <li><a href="../page-fold-manifesto/index.php">The Fold Manifesto: Why the Page Fold Still Matters</a></li>
                
            </ul>
          </section>
          
          

          
            <section class="hide-for-small-down">
              
            </section>
          

      </section>
      

      
        <div class="small-12 medium-9 columns content-body">
          




<article class="l-subsection alertbox-article-content article-detail-bl">
  
  <header>
    <div class="l-subsection">
      <h1>Report From a 1994 Web Usability Study</h1>
      <div class="author-meta">by
        
          
            
            <a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a>
            
          
        on  December 18, 1994
        
      </div>

      

      
      <div class="topic-links">
        Topics:
        
          <ul>

  <li><a href="../../topic/user-testing/index.php">User Testing</a></li>

</ul>
        
      </div>
      
    </div>
  </header>
  

  
  
  <section>
    <p class="lede"><strong>Summary:</strong> This reports summarizes results of a usability study of several Web sites I conducted in the beginning of December 1994. </p>
    <br />
  </section>
  
  

  
  <section id="article-body"><html><body><p>In this study, users were observed as they browsed the Web sites of Hewlett-Packard, IBM, Microsoft, Sun Microsystems, and Time Warner. The report has only been very lightly edited and thus represents my thinking about Web usability in 1994. In fact, the report was originally written for distribution to the rest of the Web team on paper since we were not heavy intranet users in 1994, despite having designed SunWeb a few months before this study.</p>
<p>The report is of some historical interest, both because it includes screen captures of several famous early websites and because it is one of the first formal usability studies of the Web. In fact, it is remarkable how well the findings and conclusions in this report hold up in the light of the current state of the Web. People often talk about how the Web changes on "Internet time", but usability issues seem to change much more slowly since they stem from human capabilities and interests.</p>
<h2>Method</h2>
<p>Three external participants were tested: an MIS director, a programmer, and a systems administrator. All were employed with technically oriented companies in the Silicon Valley area, all had extensive Unix experience, and all were highly technically competent. One participant was a current Sun user, one used another version of Unix on the Intel platform, and one was currently a Windows user but had been a Sun user for seven years. All participants had used the WWW extensively before the test. Thus, the participants were very advanced users and one would expect that less sophisticated users would have many more problems in using the WWW than described here. In other words, this test investigated a best-case situation.</p>
<p>Each participant was tested for 60-90 minutes. During the test, the participants visited 2-4 WWW sites from a list of sites prepared by the experimenter. For each site, the participants were first asked to give their initial impression of the home page, after which they were allowed to explore the site freely. After about ten minutes of exploratory browsing, the participants were given a directed task that asked them to find some specific information that was available on the site. The sites and the directed tasks were:</p>
<table border="1" cols="2" width="100%">
<tbody>
<tr>
<th>Site</th>
<th>Directed Task</th>
</tr>
<tr>
<td>Hewlett-Packard</td>
<td>You want to see the photo of the garage in which the company was founded</td>
</tr>
<tr>
<td>IBM</td>
<td>You want to know how many hard drives you can put into a PC Server 50</td>
</tr>
<tr>
<td>Microsoft</td>
<td>You are interested in knowing how Windows'95 will support access to the Internet</td>
</tr>
<tr>
<td>Sun Microsystems</td>
<td>You are interested in knowing more about the Spring Distributed Operating System Project (a project trying out alternatives to Unix)</td>
</tr>
<tr>
<td>Time-Warner</td>
<td>You want to read the profile of the co-creator of Mosaic. His first name is Marc and his last name starts with an A, but you can't remember the exact spelling</td>
</tr>
</tbody>
</table>
<p>The users were tested using NCSA Mosaic version 2.4 for X Windows running on a SPARC 10 workstation with a megapixel 256-color display. The workstation had an indirect connection to the Internet through a firewall with transfer rates of approximately 10 kilobytes per second. This is about five times faster than a modem and about the same as ISDN and thus represents best-case performance for home users, though on the slow side of what one would experience from an office computer that was directly connected to the Internet. When using the Sun WWW pages, the users accessed an internal mirror server to which the workstation had a direct network connection, and transfer rates were much faster for the Sun pages (around 65 kB/s) than for the pages retrieved from the Internet through the firewall. The users commented several times that access to the Sun pages was faster than their normal experience and that access to the non-Sun pages seemed a little slow. Thus, we can assume that the network speeds used in this study were approximately (though not exactly) the same as those normally experienced by the users.</p>
<h2>The Sites</h2>
<p>We obviously wanted to test Sun's own existing web pages since the previous release of a product is the first prototype of its own replacement. Four additional sites were selected for the study based on discussions in the www.sun.com redesign project. Project participants were asked to recommend good web sites that had interesting design with an emphasis on sites with a magazine or news-oriented approach to WWW information. We believed that frequent information updates are an emerging trend and would be important for attracting repeat traffic to our WWW site. IBM was selected because it used a magazine format for its home page with a monthly cover story. Time Warner was selected because of its highly news-oriented design. HP was chosen because of its clean and highly structured design, and Microsoft was chosen due to its status in the computer industry.</p>
<p><img alt="HotWired's homepage in 1994" src="http://media.nngroup.com/media/editor/2012/10/29/1994_hotwired.gif" style="width: 480px; height: 322px;"/></p>
<p><br/>
Many other sites were considered and were rejected either because they were similar to the ones chosen or because we were sure that we did not want to emulate their designs. For example, the home page for HotWired used icons and labels that were essentially impossible to understand. Such a design may be appropriate if "hipness" is the essential quality to be communicated, but I felt that our design should involve qualities like ease of use and powerful computers. One possibility would be to follow the example set by eWorld and design the corporate Web page to mirror the graphical user interface of our desktop software, but we were not aware of the eWorld Web site when the comparative usability test was planned, so this option was not tested, except for the observation that users liked a similar design feature at Microsoft's site.</p>
<p><img alt="Microsoft page for Windows'95 in 1994" src="http://media.nngroup.com/media/editor/2012/10/29/1994_ms_windows95.gif" style="width: 596px; height: 365px;"/></p>
<h2>Findings</h2>
<p>The study was performed using the <a href="../web-discount-usability/index.php"> discount usability engineering</a> approach where a small number of users are tested. Due to this method, statistical data analysis is not appropriate and the findings reported here are qualitative in nature.</p>
<h3>Page Complexity</h3>
<p><img alt="Time-Warner's homepage in 1994" src="http://media.nngroup.com/media/editor/2012/10/29/1994_pathfinder.gif" style="width: 511px; height: 705px;"/></p>
<p>Users were often overwhelmed by the amount of information on the pages. All users found the Time Warner page too complex. One user commented that the Sun home page "contains to much stuff that I will just ignore anything that is not clear." As an example of something that was not clear and would be ignored, this user pointed to the SunSITE entry.</p>
<p><img alt="Sun's homepage in 1994" src="http://media.nngroup.com/media/editor/2012/10/29/1994_sun.gif" style="width: 614px; height: 672px;"/></p>
<p>On the Sun home page, all users clicked on the Santa Claus icon first due to its prominent position at the top left corner of the page. Also, several users expressed a liking for the untraditional icon design with the button graphically protruding from the image. All users overlooked the row of flat buttons on top of the Santa button. Several users were actively looking for a "new at Sun" button and a search feature but did not find them. The design used for the Sun home page makes the button bar look too much like captions or headers for the top row of much more prominent icons.</p>
<p><img alt="Product listing from Hewlett-Packard's 1994 website" src="http://media.nngroup.com/media/editor/2012/10/29/1994_hp_productlist.gif" style="width: 358px; height: 533px;"/></p>
<p>All users complained when they retrieved pages with a screen or more of unstructured text. They plainly did not want to read much, and at best scanned the text for important or relevant paragraphs. The users liked information that was presented in lists that were easily scannable, especially when icons were used to indicate the different parts of the list (see the figure from Hewlett-Packard's site). Also, users liked the use of a "New" indicator to highlight new information in a list and the use of horizontal rules (&lt;HR&gt;) to partition a page. Long pages (more than a screen) were only deemed acceptable when users could quickly decide to ignore most of them and focus on the relevant parts (e.g., a list of Microsoft networking products organized by operating system).</p>
<p>In one case, a user retrieved a page that was very similar to one he had seen before and wondered whether the two pages were in fact identical. This incident indicates a potential problem with representing the same information multiple times with slight variations.</p>
<h3>Search</h3>
<p>Observations regarding search in this study confirmed the results from more quantitative studies conducted as part of the AnswerBook redesign project: users typed very short search strings (normally one or two words) and sometimes overlooked the item they were trying to find even when it was included on a list of search results. The main conclusion from these observations is that one cannot rely on search as a mechanism for leading users to the information they need.</p>
<p>Users also sometimes scoped their search incorrectly: in one case, a user was looking for information on the Sun server and came across the Catalyst project page, which included a search form for finding information in these pages. The user happily used the Catalyst search in the belief that it searched the entire Sun server and not just the Catalyst database. The conclusion from this observation is not to offer search features that are limited to a subset of the information on a sever. If scoped search is needed anyway for some reason, it should be made very clear to the users what information is being searched and what it not being searched and there should be a direct hyperlink to the global search page. Also, users sometimes identified the need for search after they had traversed several links and were deep in the hierarchy. They expressed a desire to be able to start a search from their current location and not have to navigate back to the home page first.</p>
<h3>Navigation and Metaphors</h3>
<p>During this study, users were mostly very good at knowing what site they were currently visiting, so they did not seem to get lost in hyperspace at the macro level of knowing what company's information they were browsing. One user complained about the inclusion of information about third party products on Sun's server. He felt that he was browsing Sun's web to get information about Sun's products and that he would access those other companies' sites if he wanted information about them. A possible solution to this problem would be to make third-party pages more visibly different from Sun's own pages or to eliminate the information itself from Sun's server and only provide links to the other companies servers.</p>
<p>On the Time Warner page, one user was surprised when the "Virtual Garden" button pointed to information for home gardeners. The user had expected it to be a list of links to other interesting Web sites to visit. Users may have become so accustomed to the heavy use of metaphors in many WWW designs (and systems like Bob and Magic Link) that they expect design elements to be metaphorical rather than literal. The lesson for user interface design is to consider not just the first-level interpretation of proposed design elements but also whether they could be misconstrued as inappropriate metaphors.</p>
<p>In a few cases, users retrieved information that caused a PostScript viewer to be launched without warning. The users complained that the WWW page had not warned them that the link would download a PostScript document rather than jumping to another hypertext screen and they also did not want to read the PostScript document (as mentioned above, the users disliked long texts). Even though the users disliked links to PostScript files that were displayed in a separate viewer, they did like the possibility of using FTP to retrieve additional information, executables, patches, or other non-hypertext files.</p>
<h3>Overview Diagrams</h3>
<p><img alt="Hewlett-Packard navigation screen in 1994" src="http://media.nngroup.com/media/editor/2012/10/29/1994_hp_navigation.gif" style="width: 532px; height: 404px;"/></p>
<p>Even though users understood what sites they were visiting, they often got lost within sites, and they several times expressed a wish for overview diagrams. Hewlett-Packard's design was particularly good at informing users about their current location in the information space.</p>
<p><img alt="Microsoft's homepage in 1994" src="http://media.nngroup.com/media/editor/2012/10/29/1994_microsoft.gif" style="width: 531px; height: 439px;"/></p>
<p>Users consistently praised screens that provided overviews of large information spaces. For example, they liked the HP navigation screens, Microsoft's home page with its extensive listing of server content, and the overview page for Windows'95 (see figure at the beginning of this report). Some users did complain that the text in the Microsoft home page overview was too small.</p>
<p>Several users who were getting lost coped by analyzing the file names with full directory paths given for HTML files to understand the server hierarchy and their current location. Unix experts may be capable of doing so, but general users would need a simpler mechanism for understanding the information space.</p>
<h3>Thumbnails</h3>
<p><img alt="Complex thumbnails from IBM's site in 1994" src="http://media.nngroup.com/media/editor/2012/10/29/1994_ibm_thumbnails.gif" style="width: 574px; height: 446px;"/></p>
<p>Users several times complained about small thumbnail pictures where too much photographic detail was shown in too little space to be clearly visible. The figure shows a page with several overly complex thumbnails from IBM. Another example from the IBM server, below, shows how a small picture can be perfectly acceptable provided it shows a fairly simple object without much detail. In general, users appreciated when the system was giving them advance notice before they decided to retrieve a large file (e.g., a big image).</p>
<p><img alt="Simple thumbnail from IBM's site in 1994" src="http://media.nngroup.com/media/editor/2012/10/29/1994_ibm_pcserver.gif" style="width: 262px; height: 489px;"/></p>
<p>Broadly speaking, it seemed that users disliked 1x1 inch thumbnails but liked 1x2 or 2x2 inch thumbnails. To communicate graphical information in a one-square-inch space one should thus use icons and not thumbnails. The general conclusion regarding thumbnails, though, is that they should communicate some information to the user and have a clean and uncluttered appearance. Thus, the exact size of a thumbnail should be a function of the complexity of the original image. If it is impossible to represent an image clearly with a thumbnail then it will be better to use a textual description.</p>
<h3>Incomplete Sites</h3>
<p>Users distinctly disliked seeing "under construction" markers. As one user put it, "either the information is there or it is not; don't waste my time with information you are not giving me." Users were particularly aggravated when they had linked to a page only to find that it was under construction. One user said "At least give me something for going to the page; don't put it out there if it is not working." Users were less upset when currently inactive hypertext markers had construction signs that could be seen before activating the link.</p>
<p>Users had little patience for server error messages. One user said that he might try retrieving the page once more, knowing that servers sometimes got overloaded, but if he got a second error message from the same server then he would never visit it again: he would assume that it was too unreliable and/or too poorly maintained to be worth his time.</p>
<p><img alt="" src="http://media.nngroup.com/media/editor/2012/10/29/1994_ibm.gif" style="width: 558px; height: 648px;"/></p>
<p>Users were also very disappointed when they came across evidence that a server was not being kept up to date. For example, several sites listed talks or conferences that had already taken place as if they were still upcoming. In related comments, users praised the way some information on the Sun and Microsoft servers was labeled with dates to indicate how current it was. Users also liked the use of the month name to indicate the currency of the information on IBM's home page.</p>
<h3>The Human Touch</h3>
<p><img alt="IBM's homepage for November 1994" src="http://media.nngroup.com/media/editor/2012/10/29/1994_ms_webmaster.jpg" style="width: 447px; height: 325px;"/></p>
<p>All users liked Microsoft's picture of their Webmaster in front of the server. They felt that it was nice to know that they were communicating with a service supported by an actual person and not a faceless entity. The users also liked being able to read the technical specifications for Microsoft's server, again from the perspective that the information was not just coming in over the wire, but was coming from somewhere. Of course, server specs may be less interesting to less technically oriented users than the ones tested in this study.</p>
<h3>Two-Way Communication</h3>
<p>The users liked the ability to request further information about products by filling in a form. One user mentioned that he would often use a Web site to get a general idea about a company's products but that he would then fill in a form asking to get contacted with more information about the specific products he was interested in buying. He was less motivated for having to search the Web site himself to find this detailed information. This user was particularly interested in finding lists of contact telephone numbers and email addresses for the various products.</p>
<p>The users also liked the forms that were provided at several sites to allow them to give feedback to the Webmaster. This observation may also be related to the "human touch" phenomenon mentioned above since having the form makes the users feel more in contact with the actual people who maintain the server as opposed to being just consumers of a flow of information.</p>
<p>Users did not mind online surveys and questionnaires as long as they were at most one page long. Users frequently complained when they came across pages with questionnaire forms that did not fit on the screen. Users expressed a wish to have as much information as possible filled in for them. For example, a user who had already filled in his name and address on one questionnaire wanted the computer to automatically insert that information in another questionnaire he retrieved from the same server a few minutes later. One user wanted to be able to enter his email address to subscribe to notices about updates for a certain page that he found particularly interesting.</p>
<h3>Mirror Servers</h3>
<p>One problem related to mirror servers was encountered during the test: one user had initially accessed Sun's WWW site through the internal mirror. From there, he had linked to an outside site which happened to have a link back to Sun. The user followed this link, but since the outside link pointed to the outside version of Sun's Web site (the "original" copy of the site), all further cross-reference links to other Sun pages now pointed to the outside copies. This change in addressing caused a usability problem because the Web viewer did not display appropriate breadcrumbs on the link anchors. Normally, Mosaic shows anchors for links that the user has already followed in a different color, but since even those links now pointed to new URLs, they were displayed in the color reserved for unexplored links. This confused the user who remembered having followed some of the links earlier in the session.</p>
<p>This usability problem cannot be solved in the design of any individual server but must await a change to URNs or some other addressing scheme that takes mirror servers into account.</p>
<h3>Parallel Sessions</h3>
<p>All users opened two Mosaic windows and kept separate sessions going in parallel. Most of the time, these two sessions visited the same Web site, but explored different parts of its information space. The users would typically start a retrieval operation in one window and, while waiting, turn their attention to the other to study the information it contained.</p>
<p>Two conclusions can be drawn from the users' extensive user of parallel sessions: WWW users are very impatient and do not like waiting for information to be retrieved over the Internet. As far as possible, one should avoid having them wait (since their attention will wander).</p>
<p>One cannot assume a one-to-one correspondence between the stream of WWW requests received from a user and that user's hypertext browsing, since two or more browsing sessions may be interleaved.</p>
<h3>Integration with Rest of World</h3>
<p>One user who had read a page about the speed of PhotoShop on Sun computers, wanted to get a demo of PhotoShop from Sun's WWW server (or have a link to Adobe's server with a demo). Another user mentioned that he normally requested demo CD-ROMs for products before buying them.</p>
<p>One user had read in the newspaper that Sun had information about the Rolling Stones online and searched for that information during the study. Knowing about certain information from paper-based sources is one way that users can get prompted to look for it, and the URL should be given in those printed sources as far as possible. Some users mentioned that they would not want to spend time during the day to read extensive information but that they might print it out or email it to themselves to read later. With current technology, most printouts will be restricted to grayscale graphics and much email will be restricted to ASCII text, so the information should be designed to be useful in those formats.</p>
<h3>Color Graphics</h3>
<p><img alt="Microsoft's 'about the server' page in 1994" src="http://media.nngroup.com/media/editor/2012/10/29/1994_hp.gif" style="width: 536px; height: 481px;"/></p>
<p>Users sometimes had problems with flashing colormaps, leading to a recommendation to minimize the number of colors used in bitmaps. Also, several users expressed an appreciation for graphics with a small number of colors (e.g., the HP home page) because they looked clean and felt like they did not waste a lot of bandwidth (and user time) when they were transmitted. It is possible that the move to screens with larger color spaces and the use of faster networks will change this preference for less-colorful images, but for now, it seemed prevalent.</p>
<h2>Conclusions</h2>
<p>Users had low tolerance for anything that did not work, was too complicated, or that they did not like. They often made comments like "if this was not a test I would be out of here" or stated that they would not want to visit a site again after a quite small number of problems. With non-WWW user interfaces, the technically oriented users in this study would normally persist for some time in trying to figure out how to use the system, but with the WWW, there are so many sites out there that users have zero patience. Thus, the demands for good usability are probably higher for WWW user interfaces than for normal user interfaces, even though the designers' options are fewer. "Under construction" signs should be avoided and the server should always provide a response within a few seconds. If the requested information cannot be provided, a meaningful error message should be given instead.</p>
<p>I found that users wanted search and that global search mechanisms should be globally available. Even so, users were poor at specifying search strings and they often overlooked relevant hits. Thus, we cannot rely on search as the main navigation feature. Navigational structure and overviews are necessary to avoid user confusion and should be provided both in the large (server structure and location) and in the small (structure for the individual pages with iconic markers for the various types of information). Users liked the feeling of being part of a two-way communication with a site staffed by real humans and not just the recipients of a stream of bytes coming in over the net. Care should be taken to provide a "high-touch" feeling in addition to the "high-tech" image of a WWW server.</p></section>
  

  
  <section>
    
        
    
  </section>
  
  
  
  
  

  


  <section>
    <br />
    <p class="share"><b>Share <span class="hide-for-small-down">this article</span>:</b> <a href="https://twitter.com/intent/tweet?url=http://www.nngroup.com/articles/1994-web-usability-report/&amp;text=Report%20From%20a%201994%20Web%20Usability%20Study&amp;via=nngroup">Twitter</a> | <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=http://www.nngroup.com/articles/1994-web-usability-report/&amp;title=Report%20From%20a%201994%20Web%20Usability%20Study&amp;source=Nielsen%20Norman%20Group">LinkedIn</a> | <a href="https://plus.google.com/share?url=http://www.nngroup.com/articles/1994-web-usability-report/">Google+</a> | <a href="mailto:?subject=NN/g Article: Report From a 1994 Web Usability Study&amp;body=http://www.nngroup.com/articles/1994-web-usability-report/">Email</a></p>
    <hr />
  </section>

  <footer class="l-subsection">
    
    
    <div class="learn-more">
      <h2>Learn More</h2>
      <p><a href="../subscribe/index.php">Subscribe to the weekly newsletter</a> to get notified about future articles.</p>
      </div>
    
    

    <div class="row show-for-small-down related-content">
      
      
      

      
      
        <div class="column small-12 topic-links">All articles about:
          <ul>
            
            <li><a href="../../topic/user-testing/index.php">User Testing</a></li>
            
          </ul>
        </div>
      
      

      
      
        <div class="column small-12">
          <h3>UX Conference Training Courses</h3>
           <ul class="no-bullet">
    
        <li><a href="../../courses/ux-basic-training/index.php">UX Basic Training</a></li>
    
        <li><a href="../../courses/usability-testing/index.php">Usability Testing</a></li>
    
        <li><a href="../../courses/one-person-ux-team/index.php">The One-Person UX Team Tool Box</a></li>
    
        <li><a href="../../courses/wireframing-and-prototyping/index.php">Wireframing and Prototyping</a></li>
    
</ul>
        </div>
      
      

      
      
        <div class="small-12 column">
          <h3>Research Reports</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../reports/paper-prototyping-training-video/index.php">Paper Prototyping Training Video</a></li>
              
            
              
                <li><a href="../../reports/how-to-conduct-eyetracking-studies/index.php">How to Conduct Eyetracking Studies</a></li>
              
            
              
                <li><a href="../../reports/how-to-recruit-participants-usability-studies/index.php">How to Recruit Participants for Usability Studies</a></li>
              
            
              
            
              
                <li><a href="../../reports/how-to-conduct-usability-studies/index.php">How to Conduct Usability Studies</a></li>
              
            
          </ul>
        </div>
      
      

      
      
        <div class="small-12 column">
          <h3>Online Seminars</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../online-seminars/mobile-user-testing/index.php">Mobile User Testing</a></li>
              
            
          </ul>
        </div>
      
      
    </div>

    <div class="hide-for-small-down row related-content">
      <div class="medium-6 columns">
        
        
          <div>
            <h3>Articles</h3>
            <ul class="no-bullet">
              
                
                <li><a href="../pm-research-plan/index.php">Project Management for User Research: The Plan</a></li>
                
              
                
                <li><a href="../observer-guidelines/index.php">Observer Guidelines for Usability Research</a></li>
                
              
                
                <li><a href="../employees-user-test/index.php">Employees as Usability-Test Participants</a></li>
                
              
                
                <li><a href="../team-members-user-test/index.php">Team Members Behaving Badly During Usability Tests</a></li>
                
              
                
                <li><a href="../usability-test-checklist/index.php">Checklist for Planning Usability Studies</a></li>
                
              
            </ul>
          </div>
        
        

        
          
          <div>
            <h3>Research Reports</h3>
            <ul class="no-bullet">
              
                
                  <li><a href="../../reports/paper-prototyping-training-video/index.php">Paper Prototyping Training Video</a></li>
                
              
                
                  <li><a href="../../reports/how-to-conduct-eyetracking-studies/index.php">How to Conduct Eyetracking Studies</a></li>
                
              
                
                  <li><a href="../../reports/how-to-recruit-participants-usability-studies/index.php">How to Recruit Participants for Usability Studies</a></li>
                
              
                
              
                
                  <li><a href="../../reports/how-to-conduct-usability-studies/index.php">How to Conduct Usability Studies</a></li>
                
              
            </ul>
          </div>
          
        

      </div>
      <div class="medium-6 columns">
        
          
          <div>
            <h3>UX Conference Training Courses</h3>
             <ul class="no-bullet">
    
        <li><a href="../../courses/ux-basic-training/index.php">UX Basic Training</a></li>
    
        <li><a href="../../courses/usability-testing/index.php">Usability Testing</a></li>
    
        <li><a href="../../courses/one-person-ux-team/index.php">The One-Person UX Team Tool Box</a></li>
    
        <li><a href="../../courses/wireframing-and-prototyping/index.php">Wireframing and Prototyping</a></li>
    
</ul>
          </div>
          
        
        
          
          <div>
            <h3>Online Seminars</h3>
            <ul class="no-bullet">
            
                <li><a href="../../online-seminars/mobile-user-testing/index.php">Mobile User Testing</a></li>
            
            </ul>
          </div>
          
        
      </div>
    </div>
  </footer>
</article>

        </div>

      

    
  </div>
  <div class="row">
    
      <div class="column small-12 show-for-small-down l-navrail announcements">
        
          
        
      </div>
    
    
  </div>

    </div>
    




<footer id="page-footer">
    <div class="row hide-for-print">
        <div class="column small-12 medium-6">
            <section class="footer-popular">
                
                <h1>Popular Topics</h1>
                <div style="float: left;  width: 15em; display: block;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/applications/index.php">Application Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/e-commerce/index.php">E-Commerce Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/eyetracking/index.php">Eyetracking </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/information-architecture/index.php">Information Architecture </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/intranets/index.php">Intranet Design </a></li>
	<li><a href="../../topic/interaction-design/index.php">Interaction Design </a></li>
</ul>
</div>

<div style="float: left;  width: 15em; display: block; list-style: none;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/mobile-and-tablet-design/index.php">Mobile and Tablet Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/credibility-persuasion/index.php">Persuasive Design</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/research-methods/index.php">Research Methods</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/user-testing/index.php">User Testing </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/web-usability/index.php">Web Usability </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/writing-web/index.php">Web Writing &amp; Content Strategy </a></li>
</ul>
</div>

<p>&nbsp;</p>

                
            </section>
        </div>

        <div class="column small-12 medium-6">
            <div class="row">
                <div class="column small-6">
                    <section class="footer-about">
                        <h1>Follow Us</h1>
                        <ul>
	<li><a href="../subscribe/index.php">Subscribe to our weekly UX newsletter</a>,&nbsp;Jakob Nielsen&#39;s Alertbox</li>
	<li>Visit Don Norman&#39;s website, <a href="http://www.jnd.org/"> www.jnd.org </a></li>
	<li>Visit Bruce Tognazzini&#39;s website, <a href="http://www.asktog.com/"> www.asktog.com </a></li>
	<li style="padding-top: 6px;"><a class="twitter-follow-button" data-show-count="false" data-size="large" href="https://twitter.com/nngroup"><img alt="Follow @nngroup on Twitter" height="20" src="https://media.nngroup.com/media/editor/2016/12/30/twitter-follow-button.png" style="width: 117px; height: 20px;" width="117" /></a></li>
	<li style="padding-top: 6px;"><img alt="" height="14" src="https://media.nngroup.com/media/editor/2016/07/26/feed-icon-14x14.png" style="vertical-align:text-top;width:14px;margin-right:.25em;" width="14" /><a href="../../feed/rss/index.php">Subscribe to our RSS feed</a></li>
</ul>

                    </section>
                </div>

                <div class="column small-6">
                    <section class="footer-about right-column">
                        <h1>About
</h1>
                        <ul class="no-bullet">
	<li><a href="../../about/contact/index.php">Contact information</a></li>
	<li><a href="../../about/index.php">About Us</a></li>
	<li><a href="../../about/why-nng/index.php">Why NN/g? </a></li>
	<li><a href="../../return-policy/index.php">Return Policy</a></li>
</ul>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="copyright column small-12">
            <p><a href="../../copyright-and-reprint-info/index.php">Copyright</a> &copy; 1998-2017 Nielsen Norman Group, All Rights Reserved.</p>
        </div>
    </div>
</footer>


    


        <script src="https://media.nngroup.com/static/vendor/fastclick/js/fastclick.min.js"></script>
        <script src="https://media.nngroup.com/static/vendor/foundation/js/foundation.min.js"></script>
        <script src="http://media.nngroup.com/static/js/app.js"></script>
        <script src="http://media.nngroup.com/static/js/scroll-to-fixed.js"></script><script>var externalLinks = document.querySelectorAll('a[href^="http"]');for (var i = externalLinks.length-1; i >= 0; i--) {externalLinks[i].addEventListener("click", function(event) { alert("External links are disabled for this test."); event.preventDefault(); }, false);}var mailLinks = document.querySelectorAll('a[href^="mailto"]');for (var i = mailLinks.length-1; i >= 0; i--) {mailLinks[i].addEventListener("click", function(event) { alert("Email links are disabled for this test."); event.preventDefault(); }, false);}var externalForms = document.querySelectorAll('form');for (var i = externalForms.length-1; i >= 0; i--) {externalForms[i].addEventListener("submit", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false); externalForms[i].addEventListener("click", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false);}</script>

        <script>
            
            $(function() {
                $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
                $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
            });
        </script>

        
<script src="https://media.nngroup.com/static/js/articles.js"></script>


        
        <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1072699960;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072699960/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
    </body>

<!-- Mirrored from www.nngroup.com/articles/1994-web-usability-report/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:15:01 GMT -->
</html>
