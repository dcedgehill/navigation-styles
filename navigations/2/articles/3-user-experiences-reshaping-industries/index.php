<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->





    
<!-- Mirrored from www.nngroup.com/articles/3-user-experiences-reshaping-industries/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:01:28 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1026.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"beacon":"bam.nr-data.net","queueTime":4,"applicationTime":343,"agent":"","licenseKey":"1a30ef9931","errorBeacon":"bam.nr-data.net","applicationID":"2048992","transactionName":"NFxSMRBUX0EDUkIPDA0WdhAMVkVbDV8ZBxEXUFMJB0YfRAtUQRVZIktEDAFZVHYHRVcPD01eVRE="}</script>
        <title>Beyond Usability: 3 User Experiences Reshaping Their Industries </title><meta property="og:title" content="Beyond Usability: 3 User Experiences Reshaping Their Industries " />
  
        
        <meta name="description" content="Rent the Runway, Uber, and Airbnb challenge the traditional experiences of their industries through deep design, not just screen-UI design. ">
        <meta property="og:description" content="Rent the Runway, Uber, and Airbnb challenge the traditional experiences of their industries through deep design, not just screen-UI design. " />
        
  
        
	
        
        <meta name="keywords" content="sharing economy, experience design, service design, overall user experience, changing industries, omnichannel, omnichannel touchpoints, dynamic social proof, transparent design">
        

        

        <meta name="robots" content="noarchive">
        <meta name="viewport" content="width=device-width">

        <link rel="search" type="application/opensearchdescription+xml" title="NN/g Search" href="../../opensearch.xml" />
        <link rel="shortcut icon" href="http://media.nngroup.com/static/img/favicon.ico" />

        <link rel="stylesheet" href="https://media.nngroup.com/static/vendor/font-awesome/css/font-awesome.min.css">

        
        <link rel="stylesheet" href="https://media.nngroup.com/static/css/7232a960f1df.css" type="text/css" />
        <link rel="stylesheet" href="http://fast.fonts.com/t/1.css?apiType=css&amp;projectid=c5846260-9def-46ec-9407-0f1349bd2d64" type="text/css">

        <script src="https://media.nngroup.com/static/vendor/modernizr/js/modernizr.min.js"></script>

        <script type="text/javascript" src="https://media.nngroup.com/static/js/3283be5def74.js"></script>
        

        

        
        
        <!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=229034,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('http://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->


<!-- Google Analytics Tracking Code for http://nngroup.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23132681-1', 'nngroup.com');
  ga('require', 'linkid', 'https://www.nngroup.com/articles/3-user-experiences-reshaping-industries/linkid.js');
  ga('require', 'displayfeatures');

// Optimizely Universal Analytics Integration
 window.optimizely = window.optimizely || [];
 window.optimizely.push("activateUniversalAnalytics");

  ga('send', 'pageview');

</script>
<script>
/**
* Function to track a click on an outbound link in Google Analytics.
*/
function trackOutboundLink(label, url) {
    if(window.ga && ga.loaded) {
        ga('send', 'event', 'outbound', 'click', label, {'hitCallback': function(){document.location = url;}
        });
    } else {
        document.location = url;
    }
}
/**
* Function to track a click on a link in the article announcement area.
*/
$('#articleAnnouncement a').on('click', function(){
  ga('send', 'event', 'link', 'click', 'articleAnnouncement');
});
</script>

<script type="text/javascript">
 setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2421.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



<!-- Hotjar Tracking Code for http://nngroup.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:121605,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

        <script src="https://media.nngroup.com/static/js/analytics/track_outbound_link.js"></script>
        
        
        
    </head>
    <body class="location-articles articles-detail">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
    <header id="page-header">
        <div id="cart" class="shopping-cart mobile-cart show-for-small-down"></div>
        <div class="row">
            <div id="masthead" class="masthead small-12 medium-7 column">

                <!-- The button to toggles menu on small screen -->

                <h1><a href="../../index.php"><img src="http://media.nngroup.com/static/img/logo-wordmark.png" width="74" height="30"> Nielsen <span class="norman">Norman</span> Group</a></h1>
                <p class="tagline show-for-medium-up">Evidence-Based User Experience Research, Training, and Consulting</p>

                <!-- The button to toggles form search on small screen -->


                <div style="clear: right;width:  100%;height: 1px;"></div><?php define('ROOT_PATH',$_SERVER["DOCUMENT_ROOT"] . '/apps/navigation_styles/navigations/2'); ?><?php include(ROOT_PATH)."/navigation.php"; ?>

                <!-- Drop down menu on small screen -->
                <div id="hideNav" class="show-for-small-down">
                  
                  <ul class="menu-content">
                    <li><a href="../../index.php">Home</a></li>
                    <li class="has-submenu">
                      <a>Training</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../training/index.php">Overview</a></li>
                        
<li id="nav-ux-conference"><a href="../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../online-seminars/index.php"><span>Online Seminars</span></a></li>

                      </ul>
                    </li>
                    <li class="has-submenu">
                      <a>Consulting</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
  <li><a href="../../consulting/index.php">Overview</a></li>
  
  <li><a href="../../consulting/benchmarking/index.php">Benchmarking</a></li>
  
  <li><a href="../../consulting/expert-design-review/index.php">Expert Design Review</a></li>
  
  <li><a href="../../consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
  
  <li><a href="../../consulting/usability-testing/index.php">Usability Testing </a></li>
  
  <li><a href="../../consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
  
  <li><a href="../../in-house-training/index.php">In-House Training</a></li>
</ul>

                    </li>

                    <li><a href="../../reports/index.php">Reports</a></li>
                    <li><a href="../index.php">Articles</a></li>
                    <li class="has-submenu"><a>About NN/G</a><i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../about/index.php">Overview</a></li>
                        <li><a href="../../people/index.php">People</a></li>
                        <li><a href="../../about/why-nng/index.php">Why NN/g?</a></li>
                        <li><a href="../../about/contact/index.php">Contact</a></li>
                        <li><a href="../../news/index.php">News</a></li>
                        <li><a href="../../about/history/index.php">History</a></li>
                        <li><a href="../../books/index.php">Books</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                </div>

                <hr class="header-divider show-for-small-only"/>


            </div>
       </div>

        <div id="hideNav">            <nav class="primary-nav small-12 column">
                <ul>
                    <li><a class="navitem" id="nav-home" href="../../index.php">Home</a></li>
                    <li><a class="navitem" id="nav-training" href="../../training/index.php">Training</a></li>
                    <li><a class="navitem" id="nav-consulting" href="../../consulting/index.php">Consulting</a></li>
                    <li><a class="navitem" id="nav-reports" href="../../reports/index.php">Reports</a></li>
                    <li><a class="navitem" id="nav-articles" href="../index.php">Articles</a></li>
                    <li><a class="navitem" id="nav-about" href="../../about/index.php">About NN/G</a></li>
                </ul>
            </nav>
            <nav class="secondary-nav small-12 medium-12 large-12 columns">
                
                
            </nav>
        </div>
    </header>
    <div id="main" class="l-content" onclick="closeNav(event)"><span id="preventClose" style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776; Menu</span>
    
  

  

  <div class="row">
    
      
      <section class="small-12 medium-3 columns collapsable-content l-navrail articles-left-column">
        <h1 class="show-for-small-down collapse-link mobile-browse-header">Browse by Topic and Author</h1>
          <div class="row collapsable">

            
            <div class="small-6 medium-12 columns">
              <section id="article-popular-topics" style="display: none;">
                
                <h1>Topics</h1>
                <ul id="article-popular-topics">
                    
                    <li>
                        
                        <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/intranets/all/index.php">Intranets</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/user-testing/all/index.php">User Testing</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                        
                    </li>
                    
                    <li><a href="#" id="article-see-all-topics"><strong>See all topics&hellip;</strong></a></li>
                </ul>
                
              </section>
              <section id="article-all-topics" style="display: none;">
                
                <h1>
                  <span class="show-for-medium-up">All Article Topics</span>
                  <span class="show-for-small-down">All Topics</span>
                  (<a href="#" id="article-hide-all-topics">hide</a>)
                </h1>
                <ul>
                  
                  <li>
                    
                    <a href="../../topic/accessibility/all/index.php">Accessibility</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/agile/all/index.php">Agile</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/analytics-and-metrics/all/index.php">Analytics &amp; Metrics</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/applications/all/index.php">Application Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/b2b-websites/all/index.php">B2B Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/behavior-patterns/all/index.php">Behavior Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/branding/all/index.php">Branding</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/content-strategy/all/index.php">Content Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/corporate-websites/all/index.php">Corporate Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/design-patterns/all/index.php">Design Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-design-process/all/index.php">Design Process</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/email/all/index.php">Email</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/eyetracking/all/index.php">Eyetracking</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/heuristic-evaluation/all/index.php">Heuristic Evaluation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/human-computer-interaction/all/index.php">Human Computer Interaction</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-ideation/all/index.php">Ideation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/information-architecture/all/index.php">Information Architecture</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/interaction-design/all/index.php">Interaction Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/international-users/all/index.php">International Users</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/intranets/all/index.php">Intranets</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-management/all/index.php">Management</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/navigation/all/index.php">Navigation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/non-profit-websites/all/index.php">Non-Profit Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/personas/all/index.php">Personas</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/persuasive-design/all/index.php">Persuasive Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/prototyping/all/index.php">Prototyping</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/psychology-and-ux/all/index.php">Psychology and UX</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/research-methods/all/index.php">Research Methods</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/search/all/index.php">Search</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/social-media/all/index.php">Social Media</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/strategy/all/index.php">Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/user-testing/all/index.php">User Testing</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-teams/all/index.php">UX Teams</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/visual-design/all/index.php">Visual Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/writing-web/all/index.php">Writing for the Web</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/young-users/all/index.php">Young Users</a>
                    
                  </li>
                  
                </ul>
                
              </section>
            </div>
            <script>
              /* Determine which topics list will be displayed: the full or the
               * partial.  We are inlining this close to the list itself to
               * prevent rendering delays on the client
               */
              var topic_list = $.cookie("article-topics");

              var popular_topics = $('#article-popular-topics');
              var all_topics = $('#article-all-topics');

              if(topic_list == 1) {
                  popular_topics.hide();
                  all_topics.show();
              } else {
                  popular_topics.show();
                  all_topics.hide();
              }
            </script>
            

            
            <div class="small-6 medium-12 columns">
              
              <section id="article-initial-authors">
                  <h1>Author</h1>
                  <ul>
                    
                    <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                    
                    <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                    
                    <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                    
                    <li><a href="#" onClick="$('#article-initial-authors').hide(); $('#article-all-authors').show();"><strong>See all authors&hellip;</strong></a></li>
                  </ul>
              </section>
              <section id="article-all-authors" style="display: none;">
                <h1>
                  All Authors
                  (<a href="#" id="article-hide-all-authors">hide</a>)
                </h1>
                <ul>
                  
                  <li><a href="../author/raluca-budiu/index.php">Raluca Budiu</a></li>
                  
                  <li><a href="../author/patty-caya/index.php">Patty Caya</a></li>
                  
                  <li><a href="../author/susan-farrell/index.php">Susan Farrell</a></li>
                  
                  <li><a href="../author/therese-fessenden/index.php">Therese Fessenden</a></li>
                  
                  <li><a href="../author/kim-flaherty/index.php">Kim Flaherty</a></li>
                  
                  <li><a href="../author/sarah-gibbons/index.php">Sarah Gibbons</a></li>
                  
                  <li><a href="../author/aurora-harley/index.php">Aurora Harley</a></li>
                  
                  <li><a href="../author/kate-kaplan/index.php">Kate Kaplan</a></li>
                  
                  <li><a href="../author/page-laubheimer/index.php">Page Laubheimer</a></li>
                  
                  <li><a href="../author/angela-li/index.php">Angie Li</a></li>
                  
                  <li><a href="../author/hoa-loranger/index.php">Hoa Loranger</a></li>
                  
                  <li><a href="../author/kate-meyer/index.php">Kate Meyer</a></li>
                  
                  <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                  
                  <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                  
                  <li><a href="../author/kara-pernice/index.php">Kara Pernice</a></li>
                  
                  <li><a href="../author/christian-rohrer/index.php">Christian Rohrer</a></li>
                  
                  <li><a href="../author/amy-schade/index.php">Amy Schade</a></li>
                  
                  <li><a href="../author/katie-sherwin/index.php">Katie Sherwin</a></li>
                  
                  <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                  
                  <li><a href="../author/kathryn-whitenton/index.php">Kathryn Whitenton</a></li>
                  
                </ul>
              </section>
              
            </div>
            
          </div>

          
          <section class="hide-for-small-down">
            
              
              <h1>Recent Articles</h1>
              
              <ul>
                  
                  <li><a href="../seamless-cross-channel/index.php">Seamlessness in the Omnichannel User Experience</a></li>
                  
                  <li><a href="../brand-intention-interpretation/index.php">Experience Design: Bridging Brand Intention and Brand Interpretation</a></li>
                  
                  <li><a href="../eyetracking-tasks-efficient-scanning/index.php">Scanning Patterns on the Web Are Optimized for the Current Task</a></li>
                  
                  <li><a href="../flat-design-best-practices/index.php">Flat-Design Best Practices</a></li>
                  
                  <li><a href="../ux-user-stories/index.php">Accounting for UX Work with User Stories in Agile Projects</a></li>
                  
                  <li><a href="../index.php"><strong>See all articles&hellip;</strong></a></li>
              </ul>
              
            
          </section>
          

          
          
          <section class="hide-for-small-down">
            <h1>Popular Articles</h1>
            
            <ul>
                
                <li><a href="../usability-101-introduction-to-usability/index.php">Usability 101: Introduction to Usability</a></li>
                
                <li><a href="../top-10-mistakes-web-design/index.php">Top 10 Mistakes in Web Design</a></li>
                
                <li><a href="../how-users-read-on-the-web/index.php">How Users Read on the Web</a></li>
                
                <li><a href="../f-shaped-pattern-reading-web-content/index.php">F-Shaped Pattern For Reading Web Content</a></li>
                
                <li><a href="../ten-usability-heuristics/index.php">10 Usability Heuristics for User Interface Design</a></li>
                
                <li><a href="../intranet-design-2016/index.php">10 Best Intranets of 2016</a></li>
                
                <li><a href="../which-ux-research-methods/index.php">When to Use Which User-Experience Research Methods</a></li>
                
                <li><a href="../response-times-3-important-limits/index.php">Response Times: The 3 Important Limits</a></li>
                
                <li><a href="../why-you-only-need-to-test-with-5-users/index.php">Why You Only Need to Test with 5 Users</a></li>
                
                <li><a href="../page-fold-manifesto/index.php">The Fold Manifesto: Why the Page Fold Still Matters</a></li>
                
            </ul>
          </section>
          
          

          
            <section class="hide-for-small-down">
              
            </section>
          

      </section>
      

      
        <div class="small-12 medium-9 columns content-body">
          




<article class="l-subsection alertbox-article-content article-detail-bl">
  
  <header>
    <div class="l-subsection">
      <h1>Beyond Usability: 3 User Experiences Reshaping Their Industries </h1>
      <div class="author-meta">by
        
          
            
            <a href="../author/sarah-gibbons/index.php">Sarah Gibbons</a>
            
          
        on  February 19, 2017
        
      </div>

      

      
      <div class="topic-links">
        Topics:
        
          <ul>

  <li><a href="../../topic/branding/index.php">Branding</a></li>

  <li><a href="../../topic/applications/index.php">Application Design</a></li>

</ul>
        
      </div>
      
    </div>
  </header>
  

  
  
  <section>
    <p class="lede"><strong>Summary:</strong> Rent the Runway, Uber, and Airbnb challenge the traditional experiences of their industries through deep design, not just screen-UI design. </p>
    <br />
  </section>
  
  

  
  <section id="article-body"><p>We live in a world of new services. We are eating via Seamless or Blue Apron, streaming music on Spotify through Alexa, calling cars on Uber and Lyft. We Airbnb our homes; Netflix invades our TVs as we swipe on Tinder.</p>

<p>All these services all have several things in common:</p>

<ul>
	<li><strong>Usefulness</strong>: they answer a previously unmet user need.</li>
	<li><strong>Usability</strong>: their digital UIs are easy to use.</li>
	<li><strong>Overall user experience</strong>: the quality of these user experiences extends far beyond the traditional “UX” touchpoints of simply a website and mobile app. (Thus, when I say ‘<a href="../definition-user-experience/index.php">user experience</a>’ in this article, I am referring to the whole experience — not specific interfaces, layouts, colors, copy, or dropdowns; it’s the decades-old NN/g definition of UX come alive.)</li>
</ul>

<p>In no particular order, below are 3 user experiences that have not just disrupted their traditional markets, but in most cases, are actively creating new ones. We analyze their usefulness, usability, and overall user experience.</p>

<h2>1. Rent the Runway — Popularizing a “Catch-and-Release” Wardrobe</h2>

<p>It used to be that you would buy two or three nice outfits to have in your wardrobe and would rotate them for special events. But in today’s era, with social media like Instagram, Facebook, and Snapchat, all your friends will see what you wear, whether they go to the same event or not. And even worse, if they forgot what you wore to last year‘s Christmas party, Instagram is there to remind them. Everyone sees pictures from each event, fostering the social pressure not to be caught in the same thing twice. This is the basis of Rent the Runway, an online service that rents designer dresses and accessories for a tenth of their retail price.</p>

<p>While the concept of Rent the Runway is enough to shake up the retail industry, it is the convenient, seamless start-to-finish user experience that keeps it growing (and keeps me a repeat customer). Those looking to rent are met with a simple landing page to get them started.</p>

<div style="text-align:center">
<figure class="caption" style="display:inline-block"><img alt="Rent The Runway Homepage" height="760" src="https://media.nngroup.com/media/editor/2017/02/10/screen-shot-2017-01-04-at-103640-am.png" width="1060"/>
<figcaption><em>Rent the Runway supports users by getting right to the task at hand: an outfit, in the correct size, on a certain date, for a specific event. In a few seconds, users are browsing the inventory available for their event’s date, in their size.</em></figcaption>
</figure>
</div>

<div style="text-align:center">
<figure class="caption" style="display:inline-block"><img alt="Rent The Runway Mobile Experience" height="927" src="https://media.nngroup.com/media/editor/2017/02/10/renttherunway-02.png" width="1060"/>
<figcaption><em>Beyond filtering for a specific event, customers can narrow down the inventory by color, size, price, and availability. When considering a specific dress, users can read reviews and see pictures of other renters in the same dress.</em></figcaption>
</figure>
</div>

<h3>Usefulness</h3>

<p>Unless you have an unlimited clothes budget, it’s unrealistic (and wasteful) to wear a new outfit for every single occasion, yet social media pressures us into it. Rent a Runway solves this tension elegantly by recycling (expensive) clothes across a large set of women. It’s an example of the sharing economy at work and, for a significant subset of the population, it solves a real need.</p>

<h3>Usability</h3>

<p>The Rent the Runway website and apps support well the task of selecting an outfit for your next event.</p>

<ul>
	<li>The site <a href="../mobile-ux/index.php"><strong>prioritizes the essential</strong></a> tasks that are core to the experience. Searching for outfits is the main user task, and it is supported with a variety of filters. On mobile, Rent the Runway’s does a good job prioritizing important actions like switching dress categories and reserving a dress. Further, the app is <strong><a href="../mobile-ux/index.php">designed for interruptions</a></strong> by allowing the user to “heart” dresses in order to get back to them when they are available or appropriate for an event.</li>
	<li><strong>High-quality images and details, as well as reviews and photos from real women and availability indicators</strong> aid in the decision-making moment. The site provides as much information as needed to help users find the right dress for them.</li>
</ul>

<h3>Overall user experience</h3>

<p>Renting a dress easily online is not going to cut it if said dress arrives in a sorry condition, the size is not quite right and you need to quickly fumble for a replacement, or if the nice dresses are always rented out to other people. Well-thought details make the overall Rent the Runway experience smooth and seamless.</p>

<ul>
	<li><strong>A free backup size</strong> of the same dress is sent to you, in case the normal, expected size does not fit. The dress arrives in a reusable garment bag, with return shipping labels. The customer can wear the dress, then send it right back in the same bag, without having to worry about dry cleaning. The process is convenient and hassle free.</li>
	<li><strong>Algorithms forecast demand</strong> for an item based on customer reviews and popularity patterns; popular dresses end up with overnight return labels, while less popular ones are assigned a three-day shipping speed.</li>
</ul>

<p style="margin-left: 40px;">At first, you might think that such an innovation in the field of reverse logistics should not be discussed in a UX article, but many pieces of the user experience are improved by optimizing this backend operation. A traditional UI-only solution to the problem of high demand would be to clearly indicate in the interface what items are in stock and display the expected availability date for out-of-stock items. This solution is one that we recommend in our <a href="../../reports/ecommerce-user-experience/index.php">e-commerce design guidelines</a>. But, beyond the digital user experience, the overall customer experience is improved further by reducing the probability that any individual user see that out-of-stock indicator. Thus, returning the most-requested dresses to the warehouse faster allows the next customer to access the high-demand inventory, further helping the user bypass the frustration experienced when a desired item is unavailable for the needed dates.</p>

<ul>
	<li><a href="../available-cross-channel/index.php"><strong>Availability of features on a variety of platforms</strong></a> allows Rent the Runway’s customers to order new items fast, even on the go. This is especially important for Rent the Runway’s Unlimited plan clients, who pay a monthly fee to have three rotating items at all times. Thus, the sooner Unlimited users can pick new items, regardless of device or location, the more value they get out of their subscriptions. Though not completely free of hiccups, this unique <a href="../customer-journeys-omnichannel/index.php">omnichannel experience</a> initiates a viable migration from ownership to that of subscribing and sharing.</li>
</ul>

<h2>2. Airbnb — Shaking Up the Spare Room</h2>

<p>Why are we willing to sleep in strangers’ beds, or conversely, let strangers into our homes? We do so because Airbnb has established a trusted, secure, and easy to use customer experience. Similar to Rent the Runway, Airbnb is a player in the growing “sharing” economy. The peer-to-peer online marketplace for accommodations enables users to list or rent short-term lodging in residential properties at a cost set by the property owner.</p>

<div style="text-align:center">
<figure class="caption" style="display:inline-block"><img alt="Airbnb Cross Device" height="466" src="https://media.nngroup.com/media/editor/2017/02/10/airbnb-02.png" width="1060"/>
<figcaption><em>Airbnb’s crossdevice experience enables users to search, find, read reviews, and book accommodations.</em></figcaption>
</figure>
</div>

<h3>Usefullness</h3>

<p>Decent hotel rooms in popular destinations can be quite expensive, and many travelers like to book a house to save money, experience the local way of life, have the convenience of more space and a kitchen, or simply go off the well-trodden tourist track and connect with local communities.</p>

<p>Airbnb makes it easy to satisfy the needs of these travelers by connecting them with owners who want to make an extra dollar by renting out their property. Before Airbnb, sites such as Home Away and property-management agencies used to connect owners with interested renters, but these were focused mostly on rental properties. Airbnb allowed everybody with a room to spare to rent it out to interested parties.</p>

<p>But renting a room in a stranger’s house carries risks for both the renter and the owner. To mitigate potential safety and trust issues, a strong <strong>two-way rating system</strong>, where both hosts and guests review and rate each other, is one of the main reasons for which the site is so popular.</p>

<h3>Usability</h3>

<p>Airbnb’s desktop and mobile site, as well as its apps, are not perfect, yet their usability is good enough to allow most users to complete their tasks.</p>

<ul>
	<li><strong>Flexible filters</strong> let users significantly scope their search (room type, price, size, amenities, property type, host language, and keywords).</li>
	<li><strong>Images, a standard detailed description of the property, and user reviews</strong> allow prospective renters to form an accurate representation of the rentals and avoid unpleasant surprises.</li>
	<li>The <a href="../social-proof-ux/index.php"><strong>dynamic social proof</strong></a> gives users a better grasp on how popular an accommodation or experience may be, and, consequently, on how soon they may need to book in order to insure availability.</li>
	<li><strong>The availability information</strong> is usually listed and updated in real time, which eliminates the extra step of contacting the owner to check if the property is still free for the desired dates. This information also reassures users that they haven’t wasted time browsing listings that are already booked.</li>
	<li><strong>The booking process</strong> is relatively smooth and allows people to use credit cards to pay right away. The easy, secure, Airbnb-mediated payment allows people to feel safe and also eliminates the extra step of contacting the owner to establish payment logistics (a step that used to be the norm on the old-fashioned rental-listing sites).</li>
</ul>

<div style="text-align:center">
<figure class="caption" style="display:inline-block"><img alt="Airbnb Dynamic Social Proof" height="629" src="https://media.nngroup.com/media/editor/2017/02/10/screen-shot-2017-01-06-at-34451-am.png" width="1060"/>
<figcaption><em>Airbnb’s desktop site uses social proof (“561 people are also looking at this experience.”) to build trust and also convince users to book faster.</em></figcaption>
</figure>
</div>

<div style="text-align:center">
<figure class="caption" style="display:inline-block"><img alt="Airbnb iPad" height="754" src="https://media.nngroup.com/media/editor/2017/02/10/airbnbipad.png" width="566"/>
<figcaption><em>The usability of the Airbnb iPad app is not perfect (e.g., the Amenities section does not have a title, and some of the icons are cryptic), but it is good enough to allow most users to complete the basic tasks of browsing and booking a property.</em></figcaption>
</figure>
</div>

<h3>Overall user experience</h3>

<p>Omnichannel touchpoints enable users to stay up to date on bookings and in contact with hosts via email and text integration without opening an Airbnb interface. This becomes tremendously powerful when traveling abroad or in low bandwidth areas where data is expensive or hard to come by.</p>

<h2>3. Uber — “Let’s Uber”</h2>

<p>Pre Uber we were all plagued with standing on the corner, possibly in the rain, belongings in hand while attempting to wave down one of the few free taxis. (The worse the rain, the fewer the taxis — that’s a law of nature.) An abundance of unknowns adding further anxiety: how much is it going to cost, how long will I have to wait, is it safe, do I have enough cash, or even, will the driver try to cheat me by charging extra?</p>

<h3>Usefulness</h3>

<p>When they call a regular cab, riders have no control — hence, all the anxieties listed above. With Uber, they suddenly get all the control they need: once they order an “uber”, they see how far away the car is and how much they have to wait, they know in advance how much the ride will cost, there is no cash involved, and they can even pick the car type and a well-rated driver. And, if for any reason, they are not satisfied with their “uber”, they can express their dislike by providing a poor rating.</p>

<p>From the drivers’ perspective, Uber not only gives them the opportunity to use their cars as a source of income, but it also provides tools for maximizing that income — for example, it shows real-time information about the demand across a city.</p>

<h3>Usability</h3>

<p>Unlike our other examples (which can be used on a variety of channels and devices), Uber’s primary functionality is accessed through its mobile apps. Their usability has been significant enough to sustain loyal customers from the beginning.</p>

<ul>
	<li><strong>Upfront information </strong>about the trip makes you aware of your fare and wait time before you even hop in the car. (And should you want to impress your date by riding in a BMW, the UberSelect cars are an icon away and clearly show you how much higher the fare would be.) In the latest update, Uber <strong>streamlined car choices</strong> by grouping offerings into categories, so riders can first select high-level options such as Economy, Premium, or Extra Seats, and then select the level of service within that option (e.g., under Extra Seats they could pick uberXL or SUV).</li>
</ul>

<div style="text-align:center">
<figure class="caption" style="display:inline-block"><img alt="Uber Upfront Information" height="926" src="https://media.nngroup.com/media/editor/2017/02/10/uber-02.png" width="1060"/>
<figcaption><em>The user has an estimated time of arrival as well as pricing for each car tier. The user can see call the car immediately, or schedule it for the future.</em></figcaption>
</figure>
</div>

<p style="margin-left: 40px;"> </p>

<ul>
	<li><strong>A concise</strong> in-context followup presents the user with a a confirmation screen with the trip summary, cost, and option to rate the driver upon finishing the trip.</li>
	<li><strong>Suggestions to routine destinations</strong> minimize cognitive load and <a href="../interaction-cost-definition/index.php">interaction cost</a>. Frequent riders are provided shortcut buttons representing their “favorite” or “likely” places.</li>
</ul>

<h3>Overall user experience</h3>

<p>Though the app is the primary touchpoint for customers, it is only one puzzle piece of a much larger ecosystem making up the Uber user experience.</p>

<ul>
	<li><strong>The two-way rating system</strong> holds both the Uber driver and the passenger accountable for bad good behavior. Thus, the rating system establishes trust and keeps the whole experience in check. It also hints at basic gamification principles — in order to keep your rating, or “score,” high you have to be a decent human being. This rating system <strong>establishes an inherent sense of transparency and safety</strong>. Because both parties, driver and passenger, are “introduced” to one another prior to pick up, there is a connection before the passenger even sets foot in the car.</li>
	<li><strong>Riders can easily recognize the car that is supposed to pick them up</strong> because the application provides the car’s make and license plate.</li>
	<li><strong>Payment is handled automatically by the app</strong>, which makes the entire experience quicker (and simpler overall) than an interaction with a cab driver. Plus, you don’t have to worry about asking for a receipt and then losing it in the rush to catch your plane.</li>
</ul>

<h2>Conclusion</h2>

<p>The above user experiences were all able to <strong>identify a user need</strong> and to address it by building a digital product that was not only <strong>usable</strong>, but it ensured <strong>a great overall user experience</strong>. It just so happened that they all addressed that need by taking advantage of the growing sharing economy. However, the same approach can be applied to other types of solutions, whether involving new or legacy products. Start by holistically breaking apart each <a href="../channels-devices-touchpoints/index.php">touchpoint</a> in the <a href="../customer-journey-mapping/index.php">user’s journey</a>. Where are the pain points? What interactions are currently transactional and could be improved?</p>

<p>Take Uber for example. While the experience is hugely popular and generally pain free, there is room for enhancement — especially for large groups travelling together. Currently, a group must problem-solve in order to figure out how many “ubers” to call and and how many passengers can fit in each of them. Who is ordering the cars? Who is riding with whom? When is the next car arriving? These were pain points for my group of twelve just this past weekend.</p>

<p>Imagine however, that Uber coordinated this process: What if, instead one member requesting cars for the whole group, Uber would divide the group and call the appropriate number of cars? This simple, but meaningful shift would likely change the angst created in large groups, allowing them to focus on stress-free time together.</p>

<p>What can we take away from these 3 experiences? In order to create truly resonating products, ones that answer real, unmet needs — focus equally on the design your users see and on the transparent, intangible factors that the experience is built on. As Joe Soprano puts it, “Good design is obvious. Great design is transparent.”</p>

<p>For more examples, see also our full-day course on <a href="../../courses/brand-experience/index.php">how to communicate brand values in an interactive experience</a>.</p>

<p> </p>

<p><em>Disclaimer: Nielsen Norman Group and the author of the article received no compensation or free services from Rent the Runway, Airbnb, and Uber.</em></p>
</section>
  

  
  <section>
    
        
    
  </section>
  
  
  
  
  

  


  <section>
    <br />
    <p class="share"><b>Share <span class="hide-for-small-down">this article</span>:</b> <a href="https://twitter.com/intent/tweet?url=http://www.nngroup.com/articles/3-user-experiences-reshaping-industries/&amp;text=Beyond%20Usability:%203%20User%20Experiences%20Reshaping%20Their%20Industries%20&amp;via=nngroup">Twitter</a> | <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=http://www.nngroup.com/articles/3-user-experiences-reshaping-industries/&amp;title=Beyond%20Usability:%203%20User%20Experiences%20Reshaping%20Their%20Industries%20&amp;source=Nielsen%20Norman%20Group">LinkedIn</a> | <a href="https://plus.google.com/share?url=http://www.nngroup.com/articles/3-user-experiences-reshaping-industries/">Google+</a> | <a href="mailto:?subject=NN/g Article: Beyond Usability: 3 User Experiences Reshaping Their Industries &amp;body=http://www.nngroup.com/articles/3-user-experiences-reshaping-industries/">Email</a></p>
    <hr />
  </section>

  <footer class="l-subsection">
    
    
    <div class="learn-more">
      <h2>Learn More</h2>
      <p><a href="../subscribe/index.php">Subscribe to the weekly newsletter</a> to get notified about future articles.</p>
      </div>
    
    

    <div class="row show-for-small-down related-content">
      
      
      

      
      
        <div class="column small-12 topic-links">All articles about:
          <ul>
            
            <li><a href="../../topic/applications/index.php">Application Design</a></li>
            
            <li><a href="../../topic/branding/index.php">Branding</a></li>
            
            <li><a href="../../topic/case-studies/index.php">case studies</a></li>
            
            <li><a href="../../topic/customer-journeys/index.php">Customer Journeys</a></li>
            
            <li><a href="../../topic/emotional-design/index.php">emotional design</a></li>
            
            <li><a href="../../topic/service-design/index.php">Service Design</a></li>
            
          </ul>
        </div>
      
      

      
      
        <div class="column small-12">
          <h3>UX Conference Training Courses</h3>
           <ul class="no-bullet">
    
        <li><a href="../../courses/journey-mapping/index.php">Journey Mapping to Understand Customer Needs</a></li>
    
        <li><a href="../../courses/cross-channel-user-experience/index.php">Omnichannel Journeys and Customer Experience</a></li>
    
        <li><a href="../../courses/credibility-and-persuasive-web-design/index.php">Persuasive Web Design</a></li>
    
        <li><a href="../../courses/content-strategy/index.php">Content Strategy</a></li>
    
        <li><a href="../../courses/application-ux/index.php">Application Design for Web and Desktop</a></li>
    
</ul>
        </div>
      
      

      
      
        <div class="small-12 column">
          <h3>Research Reports</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../reports/best-applications-2/index.php">Application Design Showcase: 2012</a></li>
              
            
              
                <li><a href="../../reports/enterprise-mobile-showcase/index.php">Mobile Intranets and Enterprise Apps</a></li>
              
            
              
                <li><a href="../../reports/website-tools-and-applications-flash/index.php">Website Tools and Applications with Flash</a></li>
              
            
              
                <li><a href="../../reports/customization-features/index.php">Customization Features Done Correctly for the Right Reasons</a></li>
              
            
              
                <li><a href="../../reports/best-applications-1/index.php">Application Design Showcase: 2008</a></li>
              
            
          </ul>
        </div>
      
      

      
      
        <div class="small-12 column">
          <h3>Online Seminars</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../online-seminars/ux-big-data/index.php">UX for Big Data</a></li>
              
            
              
                <li><a href="../../online-seminars/ux-real-world/index.php">UX in the Real World</a></li>
              
            
          </ul>
        </div>
      
      
    </div>

    <div class="hide-for-small-down row related-content">
      <div class="medium-6 columns">
        
        
          <div>
            <h3>Articles</h3>
            <ul class="no-bullet">
              
                
                <li><a href="../channels-devices-touchpoints/index.php">How Channels, Devices, and Touchpoints Impact the Customer Journey</a></li>
                
              
                
                <li><a href="../journey-mapping-ux-practitioners/index.php">Journey Mapping in Real Life: A Survey of UX Practitioners</a></li>
                
              
                
                <li><a href="../customer-journey-mapping/index.php">When and How to Create Customer Journey Maps</a></li>
                
              
                
                <li><a href="../perceived-value/index.php">Perceived Value in User Interfaces</a></li>
                
              
                
                <li><a href="../wireflows/index.php">Wireflows: A UX Deliverable for Workflows and Apps</a></li>
                
              
            </ul>
          </div>
        
        

        
          
          <div>
            <h3>Research Reports</h3>
            <ul class="no-bullet">
              
                
                  <li><a href="../../reports/best-applications-2/index.php">Application Design Showcase: 2012</a></li>
                
              
                
                  <li><a href="../../reports/enterprise-mobile-showcase/index.php">Mobile Intranets and Enterprise Apps</a></li>
                
              
                
                  <li><a href="../../reports/website-tools-and-applications-flash/index.php">Website Tools and Applications with Flash</a></li>
                
              
                
                  <li><a href="../../reports/customization-features/index.php">Customization Features Done Correctly for the Right Reasons</a></li>
                
              
                
                  <li><a href="../../reports/best-applications-1/index.php">Application Design Showcase: 2008</a></li>
                
              
            </ul>
          </div>
          
        

      </div>
      <div class="medium-6 columns">
        
          
          <div>
            <h3>UX Conference Training Courses</h3>
             <ul class="no-bullet">
    
        <li><a href="../../courses/journey-mapping/index.php">Journey Mapping to Understand Customer Needs</a></li>
    
        <li><a href="../../courses/cross-channel-user-experience/index.php">Omnichannel Journeys and Customer Experience</a></li>
    
        <li><a href="../../courses/credibility-and-persuasive-web-design/index.php">Persuasive Web Design</a></li>
    
        <li><a href="../../courses/content-strategy/index.php">Content Strategy</a></li>
    
        <li><a href="../../courses/application-ux/index.php">Application Design for Web and Desktop</a></li>
    
</ul>
          </div>
          
        
        
          
          <div>
            <h3>Online Seminars</h3>
            <ul class="no-bullet">
            
                <li><a href="../../online-seminars/ux-big-data/index.php">UX for Big Data</a></li>
            
                <li><a href="../../online-seminars/ux-real-world/index.php">UX in the Real World</a></li>
            
            </ul>
          </div>
          
        
      </div>
    </div>
  </footer>
</article>

        </div>

      

    
  </div>
  <div class="row">
    
      <div class="column small-12 show-for-small-down l-navrail announcements">
        
          
        
      </div>
    
    
  </div>

    </div>
    




<footer id="page-footer">
    <div class="row hide-for-print">
        <div class="column small-12 medium-6">
            <section class="footer-popular">
                
                <h1>Popular Topics</h1>
                <div style="float: left;  width: 15em; display: block;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/applications/index.php">Application Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/e-commerce/index.php">E-Commerce Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/eyetracking/index.php">Eyetracking </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/information-architecture/index.php">Information Architecture </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/intranets/index.php">Intranet Design </a></li>
	<li><a href="../../topic/interaction-design/index.php">Interaction Design </a></li>
</ul>
</div>

<div style="float: left;  width: 15em; display: block; list-style: none;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/mobile-and-tablet-design/index.php">Mobile and Tablet Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/credibility-persuasion/index.php">Persuasive Design</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/research-methods/index.php">Research Methods</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/user-testing/index.php">User Testing </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/web-usability/index.php">Web Usability </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/writing-web/index.php">Web Writing &amp; Content Strategy </a></li>
</ul>
</div>

<p>&nbsp;</p>

                
            </section>
        </div>

        <div class="column small-12 medium-6">
            <div class="row">
                <div class="column small-6">
                    <section class="footer-about">
                        <h1>Follow Us</h1>
                        <ul>
	<li><a href="../subscribe/index.php">Subscribe to our weekly UX newsletter</a>,&nbsp;Jakob Nielsen&#39;s Alertbox</li>
	<li>Visit Don Norman&#39;s website, <a href="http://www.jnd.org/"> www.jnd.org </a></li>
	<li>Visit Bruce Tognazzini&#39;s website, <a href="http://www.asktog.com/"> www.asktog.com </a></li>
	<li style="padding-top: 6px;"><a class="twitter-follow-button" data-show-count="false" data-size="large" href="https://twitter.com/nngroup"><img alt="Follow @nngroup on Twitter" height="20" src="https://media.nngroup.com/media/editor/2016/12/30/twitter-follow-button.png" style="width: 117px; height: 20px;" width="117" /></a></li>
	<li style="padding-top: 6px;"><img alt="" height="14" src="https://media.nngroup.com/media/editor/2016/07/26/feed-icon-14x14.png" style="vertical-align:text-top;width:14px;margin-right:.25em;" width="14" /><a href="../../feed/rss/index.php">Subscribe to our RSS feed</a></li>
</ul>

                    </section>
                </div>

                <div class="column small-6">
                    <section class="footer-about right-column">
                        <h1>About
</h1>
                        <ul class="no-bullet">
	<li><a href="../../about/contact/index.php">Contact information</a></li>
	<li><a href="../../about/index.php">About Us</a></li>
	<li><a href="../../about/why-nng/index.php">Why NN/g? </a></li>
	<li><a href="../../return-policy/index.php">Return Policy</a></li>
</ul>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="copyright column small-12">
            <p><a href="../../copyright-and-reprint-info/index.php">Copyright</a> &copy; 1998-2017 Nielsen Norman Group, All Rights Reserved.</p>
        </div>
    </div>
</footer>


    


        <script src="https://media.nngroup.com/static/vendor/fastclick/js/fastclick.min.js"></script>
        <script src="https://media.nngroup.com/static/vendor/foundation/js/foundation.min.js"></script>
        <script src="http://media.nngroup.com/static/js/app.js"></script>
        <script src="http://media.nngroup.com/static/js/scroll-to-fixed.js"></script><script>var externalLinks = document.querySelectorAll('a[href^="http"]');for (var i = externalLinks.length-1; i >= 0; i--) {externalLinks[i].addEventListener("click", function(event) { alert("External links are disabled for this test."); event.preventDefault(); }, false);}var mailLinks = document.querySelectorAll('a[href^="mailto"]');for (var i = mailLinks.length-1; i >= 0; i--) {mailLinks[i].addEventListener("click", function(event) { alert("Email links are disabled for this test."); event.preventDefault(); }, false);}var externalForms = document.querySelectorAll('form');for (var i = externalForms.length-1; i >= 0; i--) {externalForms[i].addEventListener("submit", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false); externalForms[i].addEventListener("click", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false);}</script>

        <script>
            
            $(function() {
                $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
                $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
            });
        </script>

        
<script src="https://media.nngroup.com/static/js/articles.js"></script>


        
        <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1072699960;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072699960/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
    </body>

<!-- Mirrored from www.nngroup.com/articles/3-user-experiences-reshaping-industries/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:01:29 GMT -->
</html>
