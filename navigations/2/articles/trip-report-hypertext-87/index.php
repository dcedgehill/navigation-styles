<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->





    
<!-- Mirrored from www.nngroup.com/articles/trip-report-hypertext-87/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:14:37 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1026.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"licenseKey":"1a30ef9931","queueTime":0,"beacon":"bam.nr-data.net","errorBeacon":"bam.nr-data.net","applicationID":"2048992","transactionName":"NFxSMRBUX0EDUkIPDA0WdhAMVkVbDV8ZBxEXUFMJB0YfRAtUQRVZIktEDAFZVHYHRVcPD01eVRE=","applicationTime":433,"agent":""}</script>
        <title>Hypertext&#39;87 Trip Report</title><meta property="og:title" content="Hypertext&#39;87 Trip Report" />
  
        
        <meta name="description" content="Jakob Nielsen&#39;s trip report for the 1987 Hypertext conference.">
        <meta property="og:description" content="Jakob Nielsen&#39;s trip report for the 1987 Hypertext conference." />
        
  
        
	
        

        

        <meta name="robots" content="noarchive">
        <meta name="viewport" content="width=device-width">

        <link rel="search" type="application/opensearchdescription+xml" title="NN/g Search" href="../../opensearch.xml" />
        <link rel="shortcut icon" href="http://media.nngroup.com/static/img/favicon.ico" />

        <link rel="stylesheet" href="https://media.nngroup.com/static/vendor/font-awesome/css/font-awesome.min.css">

        
        <link rel="stylesheet" href="https://media.nngroup.com/static/css/7232a960f1df.css" type="text/css" />
        <link rel="stylesheet" href="http://fast.fonts.com/t/1.css?apiType=css&amp;projectid=c5846260-9def-46ec-9407-0f1349bd2d64" type="text/css">

        <script src="https://media.nngroup.com/static/vendor/modernizr/js/modernizr.min.js"></script>

        <script type="text/javascript" src="https://media.nngroup.com/static/js/3283be5def74.js"></script>
        

        

        
        
        <!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=229034,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('http://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->


<!-- Google Analytics Tracking Code for http://nngroup.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23132681-1', 'nngroup.com');
  ga('require', 'linkid', 'https://www.nngroup.com/articles/trip-report-hypertext-87/linkid.js');
  ga('require', 'displayfeatures');

// Optimizely Universal Analytics Integration
 window.optimizely = window.optimizely || [];
 window.optimizely.push("activateUniversalAnalytics");

  ga('send', 'pageview');

</script>
<script>
/**
* Function to track a click on an outbound link in Google Analytics.
*/
function trackOutboundLink(label, url) {
    if(window.ga && ga.loaded) {
        ga('send', 'event', 'outbound', 'click', label, {'hitCallback': function(){document.location = url;}
        });
    } else {
        document.location = url;
    }
}
/**
* Function to track a click on a link in the article announcement area.
*/
$('#articleAnnouncement a').on('click', function(){
  ga('send', 'event', 'link', 'click', 'articleAnnouncement');
});
</script>

<script type="text/javascript">
 setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2421.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



<!-- Hotjar Tracking Code for http://nngroup.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:121605,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

        <script src="https://media.nngroup.com/static/js/analytics/track_outbound_link.js"></script>
        
        
        
    </head>
    <body class="location-articles articles-detail">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
    <header id="page-header">
        <div id="cart" class="shopping-cart mobile-cart show-for-small-down"></div>
        <div class="row">
            <div id="masthead" class="masthead small-12 medium-7 column">

                <!-- The button to toggles menu on small screen -->

                <h1><a href="../../index.php"><img src="http://media.nngroup.com/static/img/logo-wordmark.png" width="74" height="30"> Nielsen <span class="norman">Norman</span> Group</a></h1>
                <p class="tagline show-for-medium-up">Evidence-Based User Experience Research, Training, and Consulting</p>

                <!-- The button to toggles form search on small screen -->


                <div style="clear: right;width:  100%;height: 1px;"></div><?php define('ROOT_PATH',$_SERVER["DOCUMENT_ROOT"] . '/apps/navigation_styles/navigations/2'); ?><?php include(ROOT_PATH)."/navigation.php"; ?>

                <!-- Drop down menu on small screen -->
                <div id="hideNav" class="show-for-small-down">
                  
                  <ul class="menu-content">
                    <li><a href="../../index.php">Home</a></li>
                    <li class="has-submenu">
                      <a>Training</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../training/index.php">Overview</a></li>
                        
<li id="nav-ux-conference"><a href="../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../online-seminars/index.php"><span>Online Seminars</span></a></li>

                      </ul>
                    </li>
                    <li class="has-submenu">
                      <a>Consulting</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
  <li><a href="../../consulting/index.php">Overview</a></li>
  
  <li><a href="../../consulting/benchmarking/index.php">Benchmarking</a></li>
  
  <li><a href="../../consulting/expert-design-review/index.php">Expert Design Review</a></li>
  
  <li><a href="../../consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
  
  <li><a href="../../consulting/usability-testing/index.php">Usability Testing </a></li>
  
  <li><a href="../../consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
  
  <li><a href="../../in-house-training/index.php">In-House Training</a></li>
</ul>

                    </li>

                    <li><a href="../../reports/index.php">Reports</a></li>
                    <li><a href="../index.php">Articles</a></li>
                    <li class="has-submenu"><a>About NN/G</a><i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../about/index.php">Overview</a></li>
                        <li><a href="../../people/index.php">People</a></li>
                        <li><a href="../../about/why-nng/index.php">Why NN/g?</a></li>
                        <li><a href="../../about/contact/index.php">Contact</a></li>
                        <li><a href="../../news/index.php">News</a></li>
                        <li><a href="../../about/history/index.php">History</a></li>
                        <li><a href="../../books/index.php">Books</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                </div>

                <hr class="header-divider show-for-small-only"/>


            </div>
       </div>

        <div id="hideNav">            <nav class="primary-nav small-12 column">
                <ul>
                    <li><a class="navitem" id="nav-home" href="../../index.php">Home</a></li>
                    <li><a class="navitem" id="nav-training" href="../../training/index.php">Training</a></li>
                    <li><a class="navitem" id="nav-consulting" href="../../consulting/index.php">Consulting</a></li>
                    <li><a class="navitem" id="nav-reports" href="../../reports/index.php">Reports</a></li>
                    <li><a class="navitem" id="nav-articles" href="../index.php">Articles</a></li>
                    <li><a class="navitem" id="nav-about" href="../../about/index.php">About NN/G</a></li>
                </ul>
            </nav>
            <nav class="secondary-nav small-12 medium-12 large-12 columns">
                
                
            </nav>
        </div>
    </header>
    <div id="main" class="l-content" onclick="closeNav(event)"><span id="preventClose" style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776; Menu</span>
    
  

  

  <div class="row">
    
      
      <section class="small-12 medium-3 columns collapsable-content l-navrail articles-left-column">
        <h1 class="show-for-small-down collapse-link mobile-browse-header">Browse by Topic and Author</h1>
          <div class="row collapsable">

            
            <div class="small-6 medium-12 columns">
              <section id="article-popular-topics" style="display: none;">
                
                <h1>Topics</h1>
                <ul id="article-popular-topics">
                    
                    <li>
                        
                        <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/intranets/all/index.php">Intranets</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/user-testing/all/index.php">User Testing</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                        
                    </li>
                    
                    <li><a href="#" id="article-see-all-topics"><strong>See all topics&hellip;</strong></a></li>
                </ul>
                
              </section>
              <section id="article-all-topics" style="display: none;">
                
                <h1>
                  <span class="show-for-medium-up">All Article Topics</span>
                  <span class="show-for-small-down">All Topics</span>
                  (<a href="#" id="article-hide-all-topics">hide</a>)
                </h1>
                <ul>
                  
                  <li>
                    
                    <a href="../../topic/accessibility/all/index.php">Accessibility</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/agile/all/index.php">Agile</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/analytics-and-metrics/all/index.php">Analytics &amp; Metrics</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/applications/all/index.php">Application Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/b2b-websites/all/index.php">B2B Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/behavior-patterns/all/index.php">Behavior Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/branding/all/index.php">Branding</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/content-strategy/all/index.php">Content Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/corporate-websites/all/index.php">Corporate Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/design-patterns/all/index.php">Design Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-design-process/all/index.php">Design Process</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/email/all/index.php">Email</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/eyetracking/all/index.php">Eyetracking</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/heuristic-evaluation/all/index.php">Heuristic Evaluation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/human-computer-interaction/all/index.php">Human Computer Interaction</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-ideation/all/index.php">Ideation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/information-architecture/all/index.php">Information Architecture</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/interaction-design/all/index.php">Interaction Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/international-users/all/index.php">International Users</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/intranets/all/index.php">Intranets</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-management/all/index.php">Management</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/navigation/all/index.php">Navigation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/non-profit-websites/all/index.php">Non-Profit Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/personas/all/index.php">Personas</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/persuasive-design/all/index.php">Persuasive Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/prototyping/all/index.php">Prototyping</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/psychology-and-ux/all/index.php">Psychology and UX</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/research-methods/all/index.php">Research Methods</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/search/all/index.php">Search</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/social-media/all/index.php">Social Media</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/strategy/all/index.php">Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/user-testing/all/index.php">User Testing</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-teams/all/index.php">UX Teams</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/visual-design/all/index.php">Visual Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/writing-web/all/index.php">Writing for the Web</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/young-users/all/index.php">Young Users</a>
                    
                  </li>
                  
                </ul>
                
              </section>
            </div>
            <script>
              /* Determine which topics list will be displayed: the full or the
               * partial.  We are inlining this close to the list itself to
               * prevent rendering delays on the client
               */
              var topic_list = $.cookie("article-topics");

              var popular_topics = $('#article-popular-topics');
              var all_topics = $('#article-all-topics');

              if(topic_list == 1) {
                  popular_topics.hide();
                  all_topics.show();
              } else {
                  popular_topics.show();
                  all_topics.hide();
              }
            </script>
            

            
            <div class="small-6 medium-12 columns">
              
              <section id="article-initial-authors">
                  <h1>Author</h1>
                  <ul>
                    
                    <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                    
                    <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                    
                    <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                    
                    <li><a href="#" onClick="$('#article-initial-authors').hide(); $('#article-all-authors').show();"><strong>See all authors&hellip;</strong></a></li>
                  </ul>
              </section>
              <section id="article-all-authors" style="display: none;">
                <h1>
                  All Authors
                  (<a href="#" id="article-hide-all-authors">hide</a>)
                </h1>
                <ul>
                  
                  <li><a href="../author/raluca-budiu/index.php">Raluca Budiu</a></li>
                  
                  <li><a href="../author/patty-caya/index.php">Patty Caya</a></li>
                  
                  <li><a href="../author/susan-farrell/index.php">Susan Farrell</a></li>
                  
                  <li><a href="../author/therese-fessenden/index.php">Therese Fessenden</a></li>
                  
                  <li><a href="../author/kim-flaherty/index.php">Kim Flaherty</a></li>
                  
                  <li><a href="../author/sarah-gibbons/index.php">Sarah Gibbons</a></li>
                  
                  <li><a href="../author/aurora-harley/index.php">Aurora Harley</a></li>
                  
                  <li><a href="../author/kate-kaplan/index.php">Kate Kaplan</a></li>
                  
                  <li><a href="../author/page-laubheimer/index.php">Page Laubheimer</a></li>
                  
                  <li><a href="../author/angela-li/index.php">Angie Li</a></li>
                  
                  <li><a href="../author/hoa-loranger/index.php">Hoa Loranger</a></li>
                  
                  <li><a href="../author/kate-meyer/index.php">Kate Meyer</a></li>
                  
                  <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                  
                  <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                  
                  <li><a href="../author/kara-pernice/index.php">Kara Pernice</a></li>
                  
                  <li><a href="../author/christian-rohrer/index.php">Christian Rohrer</a></li>
                  
                  <li><a href="../author/amy-schade/index.php">Amy Schade</a></li>
                  
                  <li><a href="../author/katie-sherwin/index.php">Katie Sherwin</a></li>
                  
                  <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                  
                  <li><a href="../author/kathryn-whitenton/index.php">Kathryn Whitenton</a></li>
                  
                </ul>
              </section>
              
            </div>
            
          </div>

          
          <section class="hide-for-small-down">
            
              
              <h1>Recent Articles</h1>
              
              <ul>
                  
                  <li><a href="../seamless-cross-channel/index.php">Seamlessness in the Omnichannel User Experience</a></li>
                  
                  <li><a href="../brand-intention-interpretation/index.php">Experience Design: Bridging Brand Intention and Brand Interpretation</a></li>
                  
                  <li><a href="../eyetracking-tasks-efficient-scanning/index.php">Scanning Patterns on the Web Are Optimized for the Current Task</a></li>
                  
                  <li><a href="../flat-design-best-practices/index.php">Flat-Design Best Practices</a></li>
                  
                  <li><a href="../ux-user-stories/index.php">Accounting for UX Work with User Stories in Agile Projects</a></li>
                  
                  <li><a href="../index.php"><strong>See all articles&hellip;</strong></a></li>
              </ul>
              
            
          </section>
          

          
          
          <section class="hide-for-small-down">
            <h1>Popular Articles</h1>
            
            <ul>
                
                <li><a href="../usability-101-introduction-to-usability/index.php">Usability 101: Introduction to Usability</a></li>
                
                <li><a href="../top-10-mistakes-web-design/index.php">Top 10 Mistakes in Web Design</a></li>
                
                <li><a href="../how-users-read-on-the-web/index.php">How Users Read on the Web</a></li>
                
                <li><a href="../f-shaped-pattern-reading-web-content/index.php">F-Shaped Pattern For Reading Web Content</a></li>
                
                <li><a href="../ten-usability-heuristics/index.php">10 Usability Heuristics for User Interface Design</a></li>
                
                <li><a href="../intranet-design-2016/index.php">10 Best Intranets of 2016</a></li>
                
                <li><a href="../which-ux-research-methods/index.php">When to Use Which User-Experience Research Methods</a></li>
                
                <li><a href="../response-times-3-important-limits/index.php">Response Times: The 3 Important Limits</a></li>
                
                <li><a href="../why-you-only-need-to-test-with-5-users/index.php">Why You Only Need to Test with 5 Users</a></li>
                
                <li><a href="../page-fold-manifesto/index.php">The Fold Manifesto: Why the Page Fold Still Matters</a></li>
                
            </ul>
          </section>
          
          

          
            <section class="hide-for-small-down">
              
            </section>
          

      </section>
      

      
        <div class="small-12 medium-9 columns content-body">
          




<article class="l-subsection alertbox-article-content article-detail-bl">
  
  <header>
    <div class="l-subsection">
      <h1>Hypertext&#39;87 Trip Report</h1>
      <div class="author-meta">by
        
          
            
            <a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a>
            
          
        on  April 1, 1988
        
      </div>

      

      
      <div class="topic-links">
        Topics:
        
          <ul>

  <li><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction</a></li>

</ul>
        
      </div>
      
    </div>
  </header>
  

  
  
  

  
  <section id="article-body"><p><strong>Note added 1995: </strong> This document was witten in 1987 and reflects the state of the art and my thinking at that time. <strong> Definitely a pre-Web report! </strong> I am making it available again to allow you to reflect on the extent to which the issues facing the WWW are similar to or different from the issues discussed in 1987.</p>

<p><br/>
The report was originally published in the <cite> ACM SIGCHI Bulletin </cite> 19, 4 (April 1988), pp. 27-35.</p>

<p>For more thorough coverage of hypertext, including the work at Hypertext'87 as well as both earlier and later research, please see my book <a href="../../books/multimedia-and-hypertext/index.php"> Multimedia and Hypertext: The Internet and Beyond</a>.</p>

<p>HyperTEXT'87 was the <strong> first large-scale meeting devoted to the hypertext concept</strong>. Before the workshop, hypertext had been considered a somewhat esoteric concept of interest to a few fanatics only. So just in case some readers don't know what hypertext is, let me start by defining it:</p>

<h3>Definition</h3>

<p>"Hypertext" is non-sequentially linked pieces of text or other information. If the focus of such a system or document is on non-textual types<sup><a href="#fn1">(1)</a> </sup>of information, the term hypermedia is often used instead. In traditional printed documents, practically the only such link supported is the footnote, so hypertext is often referred to as "the generalized footnote."</p>

<p>The things which we can link to or from are called nodes, and the whole system will form a network of nodes interconnected with links. Links may be typed and/or have attributes, and they may be one or bi-directional. The user accesses the information in the nodes by navigating the links. Frank Halasz would add to the definition that this navigation should be aided by a structural overview (which, say, HyperCard does not have!).</p>

<p>Hypertext has seen considerable interest recently because of some new commercial hypertext products such as Guide from OWL and HyperCard from Apple running on popular personal computers such as Macintosh and IBM. When the workshop was originally planned in the fall of 1986, the planners were not sure whether they would be able to get a large enough set of papers and participants. But they had ended up having 500 people compete for the 200 seats at the workshop. At the end of the workshop, Ted Nelson compared it to the first SIGGRAPH conference on interactive computer graphics. It had also had a small and cosy feel but now it was a gigantic conference with 30,000 people. Nelson felt that hypertext would also grow to be an industry.</p>

<h2>Classifying Hypertext Systems (Frank Halasz)</h2>

<p>Frank Halasz from MCC gave the last talk at the workshop. He and the organizing committee should be criticized for not making it the first talk <em> and </em> the last talk: Part of the talk was a very good survey of what hypertext really is and a classification of current systems. This material could have filled a whole talk with no problems but was presented with such speed that it left the audience breathless. It would also have made a good platform for the discussions during the conference if it had been presented at the beginning instead of at the end.</p>

<p>Hypertext systems can be divided into on the one hand the "original" generation of Memex [Vannevar Bush], NLS/Augment [Engelbart], Xanadu [Ted Nelson], etc. and on the other hand the "current" generation consisting of e.g.</p>

<ul>
	<li>Research systems: Intermedia [Brown University], NoteCards [Xerox]</li>
	<li>PC Product systems: Guide [OWL], HyperCard [Apple]</li>
	<li>Workstation products: Document Examiner [Symbolics].</li>
</ul>

<p>Further dimensions for classifying hypertext systems are:</p>

<ul>
	<li><strong>Scope of the user target </strong>

	<ul>
		<li>Single user: Guide, HyperCard, the original NoteCards</li>
		<li>Work group/CSCW: Intermedia, NoteCards recent version</li>
		<li>Corporate division: Augment, ZOG [Carnegie Mellon]</li>
		<li>Whole World: Xanadu</li>
	</ul>
	</li>
	<li><strong>Browsing vs. authoring </strong>
	<ul>
		<li>Focus on information presentation: Document Examiner, Hyperties [Shneiderman]</li>
		<li>Focus on network creation and manipulation: Augment, Neptune [Tektronix], NoteCards</li>
	</ul>
	</li>
	<li><strong>Task specificity </strong>
	<ul>
		<li>General: Guide, HyperCard, Neptune, ZOG</li>
		<li>Some task inclination: Augment, NoteCards, Intermedia</li>
		<li>Task specific: Document Examiner</li>
	</ul>
	</li>
</ul>

<p>The second part of Halasz' talk was also filled with enough material to be worth a full talk. It discussed the unresolved issues in hypertext research. This was a good way to end the workshop with a look towards the future. Indeed, several people had commented that we would soon stop having special hypertext conferences as it would cease to be an interesting problem in itself. This is the traditional argument against computer science: "But we don't have a typewriter science or international conferences on pencil design, do we?" Halasz showed that there are enough unresolved problems to keep research going for some time yet.</p>

<p>Halasz' seven issues were:</p>

<ol>
	<li>How to do search and query. Basically, hypertext is navigation and the current methods for navigating the information space works fine for small spaces up to about 2000 nodes. But navigation breaks down as a search method in large or unfamiliar networks. It is not good enough if you have to click at 500 links to get at what you want, and the risk of going down the garden path grows too large. The solution should be some kind of query-based retrieval of nodes to complement navigation. We would like to have a content search where each node or link acts as an entity. A simple database model would just do queries on the basis of the content of the entities without taking advantage of the network structure of the hypertext, while a more advanced structure search on the other hand would do so.</li>
	<li>Composition of the basic nodes and links to higher structures. Halasz viewed this as the missing concept in current hypertext systems. We have no mechanisms for treating structures of nodes and links (subnets) as structures <a href="#fn2"> <sup> (2) </sup> </a> in their own rights. Many systems do add ad hoc mechanisms such as the file boxes in NoteCards which may be used for hierarchically nesting nodes but the real solution would be to add composition to the basic hypertext model on par with the nodes and links. An issue in doing so would be whether a node could be included in several composites and if so, whether links to that node refer to the node per se or to the node as located in a specific composite?</li>
	<li>Virtual structures are a concept intended to deal with the fact that the users' conceptual structures tend to change faster than their hypertext representations. Current experience with NoteCards suggests the presence of the premature organization problem where users organize their knowledge in hypertext structures on the basis of an early understanding of the problem. When their view of the domain changes, it involves a lot of work to reorganize the hypertext structures. Instead, virtual structures would allow nodes, links, and compositions to be specified by descriptions called by name (instead of hardwired). It would then be possible to change the definitions of those descriptions and have the structure of the hypertext change accordingly. Some support for this concept already exists, e.g. in commands as "return to previous node." which are virtual in that their actual meaning changes according to the user's present access of the information space.</li>
	<li>Using computation to change hypertext networks from being passive to being active. Current systems do not actively process the information included in them to guide the user's navigation. Examples which do exist include the use of NoteCards together with a computational engine to drive a computer-aided-instruction system and the HyperTalk language in HyperCard.</li>
	<li>Versioning. Some hypertext systems such as Neptune and Intermedia do include some support for different versions of their information space. This is of course especially important in the use of hypertext to support software development but it could also see more use in traditional hypertext applications.</li>
	<li>Support of collaborative work CSCW, which is the topic of a separate series of Computer-Supported Cooperative Work conferences. <a href="#fn3"> <sup> (3) </sup> </a> Most current hypertext systems are inherently single user applications. If the case of concurrent multi-user access to the same hypertext network we would face the traditional data base problems of locking and notification as well as more human factors issues such as maintaining mutual intelligibility of a network being changed by several users.</li>
	<li>Extensibility and tailorability. Current hypertext systems tend to be suitable for a given range of tasks and style of use and to require additional programming and considerable expertise for any other use. Solutions could include being able to create new behaviors by-example, but it is in general not clear how we can provide users with the option to change user interfaces to their taste and needs.</li>
</ol>

<p>In summary, Halasz felt that items 1-3 on his list were the most important. In the long term he would assume that hypertext would become a standard inside computer systems and that all information in computers would become network/link based.</p>

<p> </p>

<h2>Is Text Data ? (Andy van Dam)</h2>

<p>Andy van Dam (Brown University) gave the opening speech giving his perspectives on hypertext from a historical point of view. They had invented the first hypertext system at Brown in 1967 -- i.e. 20 years<sup><a href="#fn4">(4)</a> </sup>ago. The University of North Carolina had been the first place outside Brown itself to use the system so this made it fitting to have this workshop at UNC. First, van Dam wanted to acknowledge two trailblazers in the hypertext field, Doug Engelbart and Ted Nelson (see the next sections). Engelbart invented outlining/idea processing and did office automation before the word even existed, while Nelson coined the word "hypertext" itself and tried to put some early discipline into the links and associations in hypertext with his concept of gradually expanding stretchtext.</p>

<p>The original hypertext editing system ran in an 128 K machine timeshared with other users on a computer which was slower than a Macintosh. The original sponsor was IBM but they could not tell them at that time that they were using the expensive graphics terminals just for displaying words. Later IBM did accept the idea of displaying words interactively on a graphics terminal and sold the system to NASA which used if for writing documentation for Apollo. <a href="#fn5"> <sup> (5) </sup> </a> In the late 1960s van Dam et al. also made the rounds to magazines such as Time to try to get them interested in hypertext systems but it was beyond their imagination.</p>

<p>A later project at Brown was FRESS which was intended to be device independent of I/O media and not to have size limitations on anything.<a href="#fn6"><sup>(6) </sup> </a> Additionally, they would not have the size of something impact its performance. The system included bidirectional links: Not just goto, but also come from. To the best of van Dam's knowledge, FRESS was also the first system to have undo. van Dam really wanted unlimited undo, since one level is not enough (but it is a lot better than zero). For some time, they worked on text evolution: Recording all the changes to all parts if the text so that the complete editing history would be documented. But the complexity of doing so made them drop it.</p>

<p>During this period, van Dam had some serious arguments with the vice president of the university about whether their programs should be allowed on the computer. If it was there, people would use it which would subvert the real purpose of computing according to the VP: to produce numbers not words. One more example of the limited foresight of people in charge. You can always get research grants to work on yesterday's problems and sometimes even today's problems. But visionaries have a hard time<sup><a href="#fn7">(7)</a> </sup>as also pointed out by Engelbart in his talk.</p>

<p>As a comment to this, Jef Raskin who used to be at Apple, said that he had wanted to include lover case letters in the design of the Apple II but had been told that "we only need upper case on personal computers as they will only be used for playing games and writing BASIC programs."</p>

<p>In spite of his problems, van Dam continued his research on online text systems and at some time had his system used for a poetry<sup><a href="#fn8">(8)</a> </sup>course. The students would sign up for an hour on the one and only graphics workstation to read poems and write their own interpretations, comments, and annotations. Afterwards the students would use the common database of comments to compare notes and end up with a richer structure than their individual work.</p>

<p>On the basis of his extensive work on online text systems and hypertext during 20 years, van Dam presented a list of 8 key areas to look more closely at:</p>

<ol>
	<li>The glue (infrastructure) in hypertext may be good, but what should be at the nodes -- one could have not just text but e.g. three dimensional animation under user control. This is wide open.</li>
	<li>The "docuverse" [a Ted Nelson-word] is the most interesting, but we are building "docu-islands" in the form of isolated, not cross-linked information structures. Systems are closed, incompatible, and without the possibility for data transfer. Instead linking information between systems should be part of an open system conforming to a standard.<a href="#fn9"><sup>(9) </sup> </a></li>
	<li>Since only toy systems have been built, we do not know if the hypertext principles currently in use will scale up to handle real world documentation systems. As an example, the number of pages needed to document a fighter aircraft has grown practically exponentially from one war to the next as shown in the following graph:
	<p> </p>

	<center><img alt="number of pages documentation per generation of fighters" src="http://media.nngroup.com/media/editor/2012/11/01/doccurve.gif" style="width: 192px; height: 190px;"/></center>

	<p> </p>
	</li>
	<li>The navigation problem of getting lost<sup><a href="#fn10">(10)</a> </sup>in hyperspace. Hypertext gives us a goto link which we know from software engineering gives "spaghetti." van Dam noted that it could be that we have also discovered the equivalent of if-then-else in the form of hierarchies, but we also need new forms of flow of control in structures that users recognize.</li>
	<li>We also need a notion of semantics of nodes instead of just pure syntax, so the system needs to know what is in it (AI needed).</li>
	<li>We would want wall-sized displays as well as portable displays.</li>
	<li>We need to handle the socio-economical problems such as intellectual property rights. At the moment traditional copyright laws<sup><a href="#fn11">(11)</a> </sup>don't know how to handle electronic documents which may partly include or reference other documents, and the politicians certainly don't know either so there is not hope of an early solution. Another problem mentioned later by Ted Nelson is that freedom to publish hypertext may mean that we loose current distinctions between more or less important published papers unless we define certain hypertext structures as e.g. an "official" version of some prestigious journal.</li>
</ol>

<h2>The Father of HyperText (Ted Nelson)</h2>

<p>The coiner of the very word "hypertext," Ted Nelson from Xanadu gave a somewhat rambling talk on some of his hypertext ideas. It was illustrated with a colorful collection of slides from previous talks -- some going back to 1970. He wanted to see computers used for the generalized handling of ideas and said that the way personal computing is currently happening is plain wrong. People are drowning in small files with incomprehensible names and incompatible software.</p>

<p>When writing with current computers, we need to do "double maintenance" to keep the printouts and file consistent when making changes to one or the other. The Macintosh is just what Nelson calls a paper simulator and not an online solution. Instead we need to build a world where we can share paper-less information in the same way we can share paper: Books are always compatible!</p>

<p>Nelson related how he became interested in hypertext: As a student he became a serious note-taker and took notes all the time. He wanted to be able to see the original context of the note as well as its current use side by side, so he started hypertext as a student project and has been working on it ever since. In 1967 he gave his system the name Xanadu and by now it is actually working (we saw a demo at the conference).</p>

<p>By now, the real difference between Nelson and most other hypertext proponents is that he still argues for the universal hypertext which is to contain all literature in the world with interlinked references. To do this, he has invented an addressing scheme called tumblers which has the potential to give an unique address to every byte in all documents in the world. Of course such an open, universal hypertext system should expect to accumulate 100 Mbytes of info every hour and this may seem unrealistic at the present moment. But Nelson reminded us that it had also seemed unrealistic to have several 100 millions of telephones all over the world, all able to call each other.</p>

<h2>Pioneers have Arrows in their Backs (Doug Engelbart)</h2>

<p>Doug Engelbart who is now with the MacDonnel-Douglas Corp. did a lot of pioneering work back in the 1960s: He invented the mouse and developed a lot of word processing and hypertext concepts in his Augment system. The name Augment comes from the goal of the project which was to augment human capabilities. Many computer systems are fairly unsophisticated in augmenting human capabilities (Engelbart mentioned Apple as being in that category) but they have large markets. On the other hand Engelbart wanted to develop a system which was very sophisticated and which would significantly augment the capabilities of its users.</p>

<p>This augmentation process takes place in two systems according to Engelbart: The human system of culture, organizations, work procedures, skills, knowledge, and training on the one hand and the tool system of media, views, manipulation, retrieval, computation, and communication of data. Most designers aim at improving the tool system to automate an unchanged human system. But to truly augment the performance of human capabilities, Engelbart felt that we also needed to change the human system to reflect the new tools.</p>

<p>The Augment system [originally called NLS<a href="#fn12"><sup>(12)</sup></a>] is still being used and now has about 100,000 articles stored online after 17 years of use by a total of about 2000 people. It runs on a group of DEC-20's but they have not gotten the funding to move it to more modern hardware. Engelbart gave a demo of Augment using a PC linked to the system via a modem and ran a small conference with Jim Norton who was sitting in his office in Montreal. This was a fairly nice demo, but the really striking aspect of the demo was that it was very similar to the breakthrough demo given by Engelbart at the Fall Joint Computer Conference in 1968. Engelbart was one of the most brilliant pioneers the computer field has seen, but his fate apparently has been that of getting arrows in his back in the form of reduced funding just as he had shown the feasibility of a system which was truly many years ahead of its time.</p>

<h2>Practical Experience with hypertext</h2>

<p>Not much practical experience exists at the moment. Most results presented under this heading really came from studies of research systems and not from actual everyday field use outside of research labs and university settings. But one of my mottos is that "data is data" (knowing something is better than knowing nothing). Even so, there is a real need for more empirical studies of how hypertext is really used and what the usability issues are.</p>

<p>Randy Trigg and Peggy Irish from Xerox PARC discussed the experiences of writers in NoteCards. They video taped 20 people writing in NoteCards in a naturalistic setting. One thing they noted was that many writers felt it necessary to move out of NoteCards at some point. E.g. a Master's student had to get his thesis out as a plain text file to submit to the university. But of course, most of the time they observed people actually using the system in different ways. In many cases, writers had fairly unstructured notes, sometimes in the form of "whiteboard" cards where they could put notes they otherwise did not know where to put. When organizing the material, writers sometimes used multiple overlapping organizations of the same notes; e.g. one organized according to subjects covered and one organized according to the structure of the final paper being written.</p>

<p>References and bibliographies (one of the prime targets for hypertexting) were done in several different ways. Some authors just had all their references lumped together, and others constructed elaborate structures. One reason for this is the different number of references<sup><a href="#fn13">(13)</a> </sup>used by different authors. Finally, it was noted that the writers needed a way to modify the contents of some final document and then automatically have the source cards updated (a reverse hot link).</p>

<p>Davida Charney from Pennsylvania State University was planning a study of the reading strategies used by hypertext readers. These readers face the problem of loss of discourse cues. Traditional text which contains many such cues, ranging from genres (e.g. research paper vs. science fiction novel) over text-level schemas (e.g. the division of a research report into introduction, methods, results, conclusion, and references) to sequencing ("there are three reasons for..., 1..., 2..., 3..."), paragraphing and cohesive ties ("on the contrary..." etc.) showing how the previous relates to the next.</p>

<p>These cues are lost<sup><a href="#fn14">(14)</a> </sup>when moving to a hypertext system which drops the reader in the middle of a new node in the same way no matter which node was the previous one. Also, in hypertext the burden of deciding when to read what has been moved from the writer to the reader even though structuring the material is one of the most important functions of an author.</p>

<p>On the basis of knowledge of how people read traditional texts, Charney conjectured that domain knowledge would have an important impact on reading performance in hypertext. Novice readers can be mislead by superficial relationships and may stop reading too soon (before they have found all the necessary information).</p>

<p>Charney suggested the following ways in which hypertext designers may help readers: Design reading strategies which 1) depend on or reveal the underlying structures of the information space, 2) depend on precedence, and 3) have repetition or consistent patterns.</p>

<p>Janet Walker from Symbolics presented the Document Examiner which is one of the few hypertext systems to see real commercial use. It is used for the online manuals for the Symbolics workstation and had two goals: To be a better tool for the technical writers and to allow easier access for customers. They set out to solve those two practical problems and not specifically to build a hypertext system as such even though that is what they ended up having.</p>

<p>Also, readers should not have to work as hard as writers, so they designed two separate user interfaces to the system. The one for the writers is called Concordia while the Document Examiner is the readers' interface. Currently the system has 10,000 nodes and 23,000 explicit links between nodes as well as 100,000 implicit links. Each node has an average size of 1 K, so the total system has 10 M of info. <a href="#fn15"> <sup> (15) </sup> </a> The information was modularized by conducting an analysis of the information needs of users: If they would need or want to look up some specific issue then they made a module for that info.</p>

<p>The goals in the design of the user interface were as follows: 1) Keep the user's model of what is in the system as simple as possible, so don't use a network based navigation model. 2) Learn from how people use paper-based documentation and provide the same capabilities. 3) In addition, exploit what is unique about the computer.</p>

<p>Good things about paper-based documentation are:</p>

<ul>
	<li>One can turn directly to a known location.</li>
	<li>One can look up things in the index (but there is a naming problem when many things have the same name -- e.g., "introduction" is no good if there are 1000 of them).</li>
	<li>Bookmarks may be put in.</li>
	<li>Annotations<sup><a href="#fn16">(16)</a> </sup>are possible and present at the same location as the original material, yet are noticeably different.</li>
	<li>One can find material by position.</li>
</ul>

<p>On the other hand, computers can do the following things which paper cannot:</p>

<ul>
	<li>Full indexing of everything.</li>
	<li>Dynamic cross-referencing.</li>
	<li>Different views of the same info.</li>
	<li>One can quickly find out what is not there if you ask for it and get a nil answer.</li>
</ul>

<p>The Document Examiner project started 1982 and it was first shipped to users April 1985. The local engineering staff at Symbolics by now prefer the system over the paper version: About half of their engineers had not even taken the new manuals out of the shrink-wrap. I asked whether they had considered totally getting rid of the printed manuals and was told that they probably would do so but that it still was some years away. Also, of the 24 engineers surveyed, 2 said that they did prefer the printed version.</p>

<p>They have monitored the use of the system for a year and collected 68,000 data points, 43,000 of which are from "real users." It turns out that searching and context commands account for 40 % of the use.</p>

<h2>Tradition and Creative Writing: Hypertext in the Humanities</h2>

<p>A true multi-media version of this trip report would have started this section with a digitized clip of Zero Mostel singing Tradition! from Fiddler on the Roof. Certainly, Gregory Crane from the Classics Department at Harvard University stressed this concept enough to warrant the song. As many others at this conference he had the problem that his department did not view it as really relevant research to work with computers and text. But where Andy van Dam had that problem 20 years ago, Crane still has it. The problem is that hypertext does not give much short-term benefit compared with traditional ways of doing things.</p>

<p>Otherwise, the classics are an obvious field for use of hypertext, since they basically comment and annotate the same small basic set of primary literature over and over again. Scholarly texts make heavy use of references to these source documents. For example, a single page of Walter Burkert's <em> Greek Religion </em> contains pointers to more than two dozen source texts which the interested reader would ideally need to have available to get full benefit from studying Burkert's interpretations. In the Perseus Project, Crane and others are entering approximately 100 Mbytes of text about the classical Greek world into a hypertext structure which will be distributed on a CD-ROM or similar optical media. A big problem here is the automatic generation of links: Nobody is going to manually produce links between every word and the corresponding Greek-English dictionary. Also, it should be possible to view the hypertext structure as a generalized, abstract document which can be moved automatically to the next generation of hypertext system.</p>

<p>Another talk on the use of hypertext in the humanities was by Jay Bolter from the University of North Carolina on the Storyspace system. It is implemented on the Macintosh and is intended as a vehicle for creative writing of interactive fiction. Interactive fiction has existed for some time in the form of adventure games, even the simples of which can be viewed as a hypertext structure as the computer presents a different text as the result of reader/player action. Other movements have also tried to break down the traditional structure of text, e.g. the DADAists.</p>

<p>The reader's experience of interactive fiction is dependent on how it is accessed. For example, the length of the individual episodes presented by the computer determines the rhythm of the story: For how long is the reader a conventional reader of sequential text compared to reaching branching points. Storyspace has two modes: A structure editor showing the links between episodes and a reader mode with a more limited view of just the text without structure.</p>

<p>Bolter gave references to several works by the contemporary Argentinian writer Jorge Luis Borges which may inspire hypertext workers: "Ficciones," "An Examination of the Work of Herbert Quain," and "April March." Borges' "The Garden of Forking Paths" has been converted into a Storyspace structure of 100 episodes and 300 links. When reading this kind of interactive hypertext fiction, the synthesis of many readings is what will give the readers the full appreciation of the work. It may not be the case that all the branches should be plot-related, another possibility could be multiple narrators describing the same events [as in the classic film Rashomon by Kurosawa].</p>

<p>An interesting question from the audience was what would happen to the "authority" of the author if readers can decide how to move through fiction. In answering this question, Bolter noted that our notion of "author" has been colored by 500 years of experience with the printing press defining the role of the author compared with earlier traditions of e.g. telling stories around the camp fire.</p>

<p>George Landow of the English Department at Brown University discussed what he called the rhetoric of hypertext or in other words, the need for certain conventions for the links between nodes. hypertext preconditions the users to expect significant relations between files and a user becomes very disappointed<sup><a href="#fn17">(17)</a> </sup>if a link is not rewarding, and this disappointment may even lead to hostility towards the system.</p>

<p>Landow suggested links with labels or whose spatial proximity indicate their probably destination. Also, we could have menus of links related to some starting place. Another of Landow's suggestions for a style guide for hypertext structures is that most text nodes should be at most one screen full (bigger texts should be broken up). A comment from the audience was that HyperCard has a set of icons which seem to have relatively well-defined meanings.</p>

<h2>HyperCard</h2>

<p>HyperCard from Apple was one of the most discussed pieces of software at the workshop. The general consensus seemed to be that HC was not really hypertext because of its limited possibilities for associating links with words.<sup><a href="#fn18">(18)</a> </sup>As noted above, Frank Halasz criticized HyperCard for not having a structural overview or a browser to help users navigate stacks.</p>

<p>Andy van Dam in his opening speech called HC "beautifully engineered in spite of its many flaws" and suggested that it would "enculture" the computer community. It is simple enough to be widely used and is already emerging as somewhat of a cult<sup><a href="#fn19">(19)</a> </sup>phenomenon. On the other hand, Jef Raskin said that HyperCard is only cheap and popular for the software itself. To run it, you need an expensive computer in the form of a Macintosh, so in reality it is "yuppie-text."</p>

<p>HyperCard was presented reasonably impressively in a demo by Mike Liebhold from Apple and it was also used by many of the other information structures shown in the lobby outside the lecture halls. One of the most interesting of those was the LaserCards system from Optical Data Corp. It would control a videodisk player from HyperCard showing e.g. satellite photos of weather formations and areas of geological interest. The by now almost "traditional" feature of getting to a photo of an area by clicking on its location on a map was of course supported. Other nice features were the "tours" of customized lessons through the information space -- almost the "trail blazing" of Vannevar Bush come to life.</p>

<p>One disappointment in relation to HyperCard is that its primary author and designer, Bill Atkinson did not attend the workshop. Instead the demo was given by Liebhold who is working on transferring the <em> Whole Earth Catalog </em> into a 17 Mbyte HyperCard stack.</p>

<h2>Hypertext = Hype ? (Jef Raskin)</h2>

<p>Jef Raskin from the company Information Appliance, which designed the Canon Cat computer, felt that hypertext was "one part inspiration and nine parts hyperbole" so he had taken the role of Devil's advocate at the workshop. He raised several problems with hypertext: If you trust your hypertext system too much, you will be faced with the "missing link" problem when it actually does not include some essential reference. Alternatively, links could conceivably be added as jokes or by vandals so that the structure ended up looking like New York City subway cars. Prepackaged (non-universal) hypertext would avoid those problems, but would then just like books have the problem that only some of them were any good.</p>

<p>Even if links are not added by vandals, they could still end up overwhelming the reader in an open system which has been accumulating links added by many people over the years. If readers are presented with hundreds of possible links<sup><a href="#fn20">(20)</a> </sup>from each node, the system might as well not have had any links as it is very unlikely that any given reader will follow the specific link needed by that reader.</p>

<p>According to Raskin, too many people working on hypertext have concentrated on mechanisms instead of on the user interface. It is necessary to look at the entire spectrum of interaction and to do continuous user testing. He was surprised to see articles on hypertext that start out assuming the use of a mouse since his tests during the design of the Canon Cat indicates that keyboard-based interaction is faster. This computer was demoed at the workshop: It is a small system which is mainly intended to perform one task (wordprocessing): It is an <em> "Information Appliance" </em> [the name of Raskin's company].</p>

<h2>Conference Ergonomics</h2>

<p>This was more of a conference than a workshop. Most of the time was allocated to traditional paper presentations and only very little to discussion groups. In some sense this was OK as the paper presentations turned out to be more successful than the discussion groups. The fairly small number of participants still encouraged a workshop feel to the event as did the large number of Mac II's and other computers set up in the lobby.</p>

<p>A fairly nice feature was the small number of parallel sessions which didn't make you feel that you were missing out on most of the papers and which substantially increased the probability that the people you were talking with had attended somewhat the same presentations as yourself. Even so, it was of course not possible to attend everything. One talk I missed was Ben Shneiderman's presentation of his Hyperties system. This was actually on purpose: Not because I expected Shneiderman to give a bad talk (quite on the contrary) but for two other reasons. I had heard him give a talk on the same system just a few months before at the HCI Intl.'87 conference in Honolulu and I had tried the system myself<sup><a href="#fn21">(21)</a> </sup>in New York during a field installation at the International Center of Photography. I simply went to the museum to see the photography exhibits and discovered that Hyperties was part of one of them. This is the way most people will discover hypertext: Not as something interesting in itself but embedded in a larger context which is what really interests them.</p>

<p> </p>

<hr/>
<p>See Also: Report from the following conference, <a href="../trip-report-hypertext-89/index.php"> Hypertext'89 </a></p>

<hr/>
<h2>Footnotes</h2>

<p>(Due to the lack of pop-up notes in most WWW browsers, these footnotes have been placed at the bottom of this document even though they were designed to be read on the same page as the body text they refer to.)</p>

<p class="footnote"><a name="fn1"> <em> Footnote 1. </em> </a> For example graphics, sound, moving images from videodisks, executable programs.</p>

<p class="footnote"><a name="fn2"> <em> Footnote 2. </em> </a> Cf. the discussion by van Dam of GOTO vs. other control structures.</p>

<p class="footnote"><a name="fn3"> <em> Footnote 3. </em> </a> See my trip report from CSCW'86, <em> ACM SIGCHI Bulletin </em> <strong> 19</strong>, 1 (July 1987), pp. 54-61.</p>

<p class="footnote"><a name="fn4"> <em> Footnote 4. </em> </a> This 20 year time lack between laboratory invention and commercial realization seems to be quite typical of many breakthroughs in computer science which is not moving so quickly as some may think. There was also about 20 years between Engelbart's invention of the mouse in 1963 and the use of it on a popular PC (Lisa) in 1983. Cf. also the comment by Tom Landauer that a rule of thumb at Bell Labs was that it would take 15 years from Lab concept to Real World application (cited in my CHI'86 trip report, <em> IFIP INTERACT Newsletter </em> No. <strong> 17</strong>).</p>

<p class="footnote"><a name="fn5"> <em> Footnote 5. </em> </a> The space program -- not the computer.</p>

<p class="footnote"><a name="fn6"> <em> Footnote 6. </em> </a> Good idea. Consider, say, the file names on the Macintosh which may be 31 characters. At first this seemed a big improvement over the 8 or so characters in many other operating systems. But after some time users often hit that limit when they take advantage of the fact that they only have to type a name once (afterwards they can click on the name instead of typing it).</p>

<p class="footnote"><a name="fn7"> <em> Footnote 7. </em> </a> Cf. the comment by Jens Rasmussen at the INTERACT'87 conference that cognitive engineering research fell between the borders drawn up by traditional research councils so that the best research support came from sources such as NATO, see my trip report in <em> ACM SIGCHI Bulletin </em> <strong> 19</strong>, 4 (April 1988), pp. 36-42.</p>

<p class="footnote"><a name="fn8"> <em> Footnote 8. </em> </a> See the description of this course from the English Department's point of view: James V. Catano: "Poetry and computers: Experimenting with the communal text", <em> Computers and the Humanities </em> <strong> 13 </strong> (1979), pp. 269-275.</p>

<p class="footnote"><a name="fn9"> <em> Footnote 9. </em> </a> Towards the end of the workshop, Norman Meyrowitz (Brown University) reported from the working group on standards for hypertext. They had felt that a full standard would be premature but that we should get a standard for data and structure interchange soon.</p>

<p class="footnote"><a name="fn10"> <em> Footnote 10. </em> </a> Getting lost seems to be a worse user interface problem than navigation itself. In a small field study I did of a Guide document, users found the reference button (which is the one that really moves you to a different place in the structure) significantly less easy to understand and less of a usability/readability improvement than the note button and the replacement button [about a difference of one whole point for each question on a 1-5 Likert scale].</p>

<p class="footnote"><a name="fn11"> <em> Footnote 11. </em> </a> As another example I can mention the problems I have had with an earlier report which I only published as a hypertext document. According to the law in Denmark, all published material is to be deposited in the National Library so that every citizen can borrow it for free through the public library system. I could of course have deposited the diskette, but this would have been against at least the spirit of the law as most libraries or citizens would not have had a compatible computer system. So I ended up with printing out as good a version of the hypertext network as I could and depositing that together with the diskette.</p>

<p class="footnote"><a name="fn12"> <em> Footnote 12. </em> </a> NLS = oN Line System.</p>

<p class="footnote"><a name="fn13"> <em> Footnote 13. </em> </a> Use of between 10 and 150 bibliography entries were observed.</p>

<p class="footnote"><a name="fn14"> <em> Footnote 14. </em> </a> A comment from the audience was however, that hypertext is actually more structured than some of the information people face -- e.g. all the articles in all the journals in the library. So one should not just compare hypertext with a highly organized and structured textbook.</p>

<p class="footnote"><a name="fn15"> <em> Footnote 15. </em> </a> The printed version has 8,000 pages.</p>

<p class="footnote"><a name="fn16"> <em> Footnote 16. </em> </a> Annotations are one of the features not yet included in the Document Examiner. One reason for this is the difficulty with helping users maintain their notes across different releases of the documentation.</p>

<p class="footnote"><a name="fn17"> <em> Footnote 17. </em> </a> This is in correspondence with my own results from a field study of hypertext: Readers commented that they were frustrated by clicking on things they wanted to know more about and then not always actually getting information. This really ties in with the comment by Gregory Crane about the need to automatically generate a lot of links: Hypertext structures need to be very rich to live up to reader expectations. In my case, the problem was that my hypertext report did not have that many links because I had written it myself. In Landow's case, there may also be a problem if there is a link but to the "wrong" info, so we also need types or other prospective views on the links.</p>

<p class="footnote"><a name="fn18"> <em> Footnote 18. </em> </a> HyperCard will only link whole cards -- i.e. big chunks of text.</p>

<p class="footnote"><a name="fn19"> <em> Footnote 19. </em> </a> About half a year after the introduction of HyperCard, the Berkeley Macintosh User Group already had about 225 HC stacks [i.e. hypermedia structures] in its disk library.</p>

<p class="footnote"><a name="fn20"> <em> Footnote 20. </em> </a> I took part in the working group on the disorientation problem which concluded that it would be important with filters for links to avoid presenting them all to users. Also, following a link is not necessarily either/or: Perhaps one can follow a link partly using some principle of progressive disclosure of what will happen if one goes further down a link. This is e.g. done in in Hyperties where each link first shows a single line definition of the node reachable further down the link.</p>

<p class="footnote"><a name="fn21"> <em> Footnote 21. </em> </a> See the discussion in my article in the <cite> ACM SIGCHI Bulletin </cite> <strong> 19</strong>, 1 (July 1987), pp. 54-61.</p>
</section>
  

  
  <section>
    
        
    
  </section>
  
  
  
  
  

  


  <section>
    <br />
    <p class="share"><b>Share <span class="hide-for-small-down">this article</span>:</b> <a href="https://twitter.com/intent/tweet?url=http://www.nngroup.com/articles/trip-report-hypertext-87/&amp;text=Hypertext'87%20Trip%20Report&amp;via=nngroup">Twitter</a> | <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=http://www.nngroup.com/articles/trip-report-hypertext-87/&amp;title=Hypertext'87%20Trip%20Report&amp;source=Nielsen%20Norman%20Group">LinkedIn</a> | <a href="https://plus.google.com/share?url=http://www.nngroup.com/articles/trip-report-hypertext-87/">Google+</a> | <a href="mailto:?subject=NN/g Article: Hypertext&#39;87 Trip Report&amp;body=http://www.nngroup.com/articles/trip-report-hypertext-87/">Email</a></p>
    <hr />
  </section>

  <footer class="l-subsection">
    
    
    <div class="learn-more">
      <h2>Learn More</h2>
      <p><a href="../subscribe/index.php">Subscribe to the weekly newsletter</a> to get notified about future articles.</p>
      </div>
    
    

    <div class="row show-for-small-down related-content">
      
      
      

      
      
        <div class="column small-12 topic-links">All articles about:
          <ul>
            
            <li><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction</a></li>
            
            <li><a href="../../topic/hypertext/index.php">hypertext</a></li>
            
          </ul>
        </div>
      
      

      
      
        <div class="column small-12">
          <h3>UX Conference Training Courses</h3>
           <ul class="no-bullet">
    
        <li><a href="../../courses/ux-basic-training/index.php">UX Basic Training</a></li>
    
        <li><a href="../../courses/hci/index.php">User Interface Principles Every Designer Must Know</a></li>
    
        <li><a href="../../courses/interaction-design-3-day-course/index.php">Interaction Design: 3-Day Course</a></li>
    
</ul>
        </div>
      
      

      
      
      

      
      
      
    </div>

    <div class="hide-for-small-down row related-content">
      <div class="medium-6 columns">
        
        
          <div>
            <h3>Articles</h3>
            <ul class="no-bullet">
              
                
                <li><a href="../embarrassment/index.php">Computer-Assisted Embarrassment</a></li>
                
              
                
                <li><a href="../enhancement/index.php">The Role of Enhancement in Web Design</a></li>
                
              
                
                <li><a href="../trip-report-hypertext-89/index.php">Hypertext&#39;89 Trip Report</a></li>
                
              
                
                <li><a href="../trip-report-chi-89/index.php">CHI&#39;89 Trip Report</a></li>
                
              
                
                <li><a href="../trip-report-hyper-hyper-89/index.php">HyperHyper&#39;89 Trip Report</a></li>
                
              
            </ul>
          </div>
        
        

        

      </div>
      <div class="medium-6 columns">
        
          
          <div>
            <h3>UX Conference Training Courses</h3>
             <ul class="no-bullet">
    
        <li><a href="../../courses/ux-basic-training/index.php">UX Basic Training</a></li>
    
        <li><a href="../../courses/hci/index.php">User Interface Principles Every Designer Must Know</a></li>
    
        <li><a href="../../courses/interaction-design-3-day-course/index.php">Interaction Design: 3-Day Course</a></li>
    
</ul>
          </div>
          
        
        
      </div>
    </div>
  </footer>
</article>

        </div>

      

    
  </div>
  <div class="row">
    
      <div class="column small-12 show-for-small-down l-navrail announcements">
        
          
        
      </div>
    
    
  </div>

    </div>
    




<footer id="page-footer">
    <div class="row hide-for-print">
        <div class="column small-12 medium-6">
            <section class="footer-popular">
                
                <h1>Popular Topics</h1>
                <div style="float: left;  width: 15em; display: block;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/applications/index.php">Application Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/e-commerce/index.php">E-Commerce Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/eyetracking/index.php">Eyetracking </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/information-architecture/index.php">Information Architecture </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/intranets/index.php">Intranet Design </a></li>
	<li><a href="../../topic/interaction-design/index.php">Interaction Design </a></li>
</ul>
</div>

<div style="float: left;  width: 15em; display: block; list-style: none;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/mobile-and-tablet-design/index.php">Mobile and Tablet Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/credibility-persuasion/index.php">Persuasive Design</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/research-methods/index.php">Research Methods</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/user-testing/index.php">User Testing </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/web-usability/index.php">Web Usability </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/writing-web/index.php">Web Writing &amp; Content Strategy </a></li>
</ul>
</div>

<p>&nbsp;</p>

                
            </section>
        </div>

        <div class="column small-12 medium-6">
            <div class="row">
                <div class="column small-6">
                    <section class="footer-about">
                        <h1>Follow Us</h1>
                        <ul>
	<li><a href="../subscribe/index.php">Subscribe to our weekly UX newsletter</a>,&nbsp;Jakob Nielsen&#39;s Alertbox</li>
	<li>Visit Don Norman&#39;s website, <a href="http://www.jnd.org/"> www.jnd.org </a></li>
	<li>Visit Bruce Tognazzini&#39;s website, <a href="http://www.asktog.com/"> www.asktog.com </a></li>
	<li style="padding-top: 6px;"><a class="twitter-follow-button" data-show-count="false" data-size="large" href="https://twitter.com/nngroup"><img alt="Follow @nngroup on Twitter" height="20" src="https://media.nngroup.com/media/editor/2016/12/30/twitter-follow-button.png" style="width: 117px; height: 20px;" width="117" /></a></li>
	<li style="padding-top: 6px;"><img alt="" height="14" src="https://media.nngroup.com/media/editor/2016/07/26/feed-icon-14x14.png" style="vertical-align:text-top;width:14px;margin-right:.25em;" width="14" /><a href="../../feed/rss/index.php">Subscribe to our RSS feed</a></li>
</ul>

                    </section>
                </div>

                <div class="column small-6">
                    <section class="footer-about right-column">
                        <h1>About
</h1>
                        <ul class="no-bullet">
	<li><a href="../../about/contact/index.php">Contact information</a></li>
	<li><a href="../../about/index.php">About Us</a></li>
	<li><a href="../../about/why-nng/index.php">Why NN/g? </a></li>
	<li><a href="../../return-policy/index.php">Return Policy</a></li>
</ul>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="copyright column small-12">
            <p><a href="../../copyright-and-reprint-info/index.php">Copyright</a> &copy; 1998-2017 Nielsen Norman Group, All Rights Reserved.</p>
        </div>
    </div>
</footer>


    


        <script src="https://media.nngroup.com/static/vendor/fastclick/js/fastclick.min.js"></script>
        <script src="https://media.nngroup.com/static/vendor/foundation/js/foundation.min.js"></script>
        <script src="http://media.nngroup.com/static/js/app.js"></script>
        <script src="http://media.nngroup.com/static/js/scroll-to-fixed.js"></script><script>var externalLinks = document.querySelectorAll('a[href^="http"]');for (var i = externalLinks.length-1; i >= 0; i--) {externalLinks[i].addEventListener("click", function(event) { alert("External links are disabled for this test."); event.preventDefault(); }, false);}var mailLinks = document.querySelectorAll('a[href^="mailto"]');for (var i = mailLinks.length-1; i >= 0; i--) {mailLinks[i].addEventListener("click", function(event) { alert("Email links are disabled for this test."); event.preventDefault(); }, false);}var externalForms = document.querySelectorAll('form');for (var i = externalForms.length-1; i >= 0; i--) {externalForms[i].addEventListener("submit", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false); externalForms[i].addEventListener("click", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false);}</script>

        <script>
            
            $(function() {
                $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
                $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
            });
        </script>

        
<script src="https://media.nngroup.com/static/js/articles.js"></script>


        
        <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1072699960;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072699960/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
    </body>

<!-- Mirrored from www.nngroup.com/articles/trip-report-hypertext-87/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:14:37 GMT -->
</html>
