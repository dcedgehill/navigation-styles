<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->





    
<!-- Mirrored from www.nngroup.com/articles/parallel-design/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:14:00 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1026.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"beacon":"bam.nr-data.net","queueTime":6,"applicationTime":839,"agent":"","licenseKey":"1a30ef9931","errorBeacon":"bam.nr-data.net","applicationID":"2048992","transactionName":"NFxSMRBUX0EDUkIPDA0WdhAMVkVbDV8ZBxEXUFMJB0YfRAtUQRVZIktEDAFZVHYHRVcPD01eVRE="}</script>
        <title>Parallel Design and Testing by Jakob Nielsen and Jan Maurits Faber</title><meta property="og:title" content="Parallel Design and Testing by Jakob Nielsen and Jan Maurits Faber" />
  
        
        <meta name="description" content="How parallel design (a usability engineering method where multiple designers independently of each other design suggested user interfaces) can improve usability in the finished product. ">
        <meta property="og:description" content="How parallel design (a usability engineering method where multiple designers independently of each other design suggested user interfaces) can improve usability in the finished product. " />
        
  
        
	
        

        

        <meta name="robots" content="noarchive">
        <meta name="viewport" content="width=device-width">

        <link rel="search" type="application/opensearchdescription+xml" title="NN/g Search" href="../../opensearch.xml" />
        <link rel="shortcut icon" href="http://media.nngroup.com/static/img/favicon.ico" />

        <link rel="stylesheet" href="https://media.nngroup.com/static/vendor/font-awesome/css/font-awesome.min.css">

        
        <link rel="stylesheet" href="https://media.nngroup.com/static/css/7232a960f1df.css" type="text/css" />
        <link rel="stylesheet" href="http://fast.fonts.com/t/1.css?apiType=css&amp;projectid=c5846260-9def-46ec-9407-0f1349bd2d64" type="text/css">

        <script src="https://media.nngroup.com/static/vendor/modernizr/js/modernizr.min.js"></script>

        <script type="text/javascript" src="https://media.nngroup.com/static/js/3283be5def74.js"></script>
        

        

        
        
        <!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=229034,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('http://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->


<!-- Google Analytics Tracking Code for http://nngroup.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23132681-1', 'nngroup.com');
  ga('require', 'linkid', 'https://www.nngroup.com/articles/parallel-design/linkid.js');
  ga('require', 'displayfeatures');

// Optimizely Universal Analytics Integration
 window.optimizely = window.optimizely || [];
 window.optimizely.push("activateUniversalAnalytics");

  ga('send', 'pageview');

</script>
<script>
/**
* Function to track a click on an outbound link in Google Analytics.
*/
function trackOutboundLink(label, url) {
    if(window.ga && ga.loaded) {
        ga('send', 'event', 'outbound', 'click', label, {'hitCallback': function(){document.location = url;}
        });
    } else {
        document.location = url;
    }
}
/**
* Function to track a click on a link in the article announcement area.
*/
$('#articleAnnouncement a').on('click', function(){
  ga('send', 'event', 'link', 'click', 'articleAnnouncement');
});
</script>

<script type="text/javascript">
 setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2421.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



<!-- Hotjar Tracking Code for http://nngroup.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:121605,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

        <script src="https://media.nngroup.com/static/js/analytics/track_outbound_link.js"></script>
        
        
        
    </head>
    <body class="location-articles articles-detail">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
    <header id="page-header">
        <div id="cart" class="shopping-cart mobile-cart show-for-small-down"></div>
        <div class="row">
            <div id="masthead" class="masthead small-12 medium-7 column">

                <!-- The button to toggles menu on small screen -->

                <h1><a href="../../index.php"><img src="http://media.nngroup.com/static/img/logo-wordmark.png" width="74" height="30"> Nielsen <span class="norman">Norman</span> Group</a></h1>
                <p class="tagline show-for-medium-up">Evidence-Based User Experience Research, Training, and Consulting</p>

                <!-- The button to toggles form search on small screen -->


                <div style="clear: right;width:  100%;height: 1px;"></div><?php define('ROOT_PATH',$_SERVER["DOCUMENT_ROOT"] . '/apps/navigation_styles/navigations/2'); ?><?php include(ROOT_PATH)."/navigation.php"; ?>

                <!-- Drop down menu on small screen -->
                <div id="hideNav" class="show-for-small-down">
                  
                  <ul class="menu-content">
                    <li><a href="../../index.php">Home</a></li>
                    <li class="has-submenu">
                      <a>Training</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../training/index.php">Overview</a></li>
                        
<li id="nav-ux-conference"><a href="../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../online-seminars/index.php"><span>Online Seminars</span></a></li>

                      </ul>
                    </li>
                    <li class="has-submenu">
                      <a>Consulting</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
  <li><a href="../../consulting/index.php">Overview</a></li>
  
  <li><a href="../../consulting/benchmarking/index.php">Benchmarking</a></li>
  
  <li><a href="../../consulting/expert-design-review/index.php">Expert Design Review</a></li>
  
  <li><a href="../../consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
  
  <li><a href="../../consulting/usability-testing/index.php">Usability Testing </a></li>
  
  <li><a href="../../consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
  
  <li><a href="../../in-house-training/index.php">In-House Training</a></li>
</ul>

                    </li>

                    <li><a href="../../reports/index.php">Reports</a></li>
                    <li><a href="../index.php">Articles</a></li>
                    <li class="has-submenu"><a>About NN/G</a><i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../about/index.php">Overview</a></li>
                        <li><a href="../../people/index.php">People</a></li>
                        <li><a href="../../about/why-nng/index.php">Why NN/g?</a></li>
                        <li><a href="../../about/contact/index.php">Contact</a></li>
                        <li><a href="../../news/index.php">News</a></li>
                        <li><a href="../../about/history/index.php">History</a></li>
                        <li><a href="../../books/index.php">Books</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                </div>

                <hr class="header-divider show-for-small-only"/>


            </div>
       </div>

        <div id="hideNav">            <nav class="primary-nav small-12 column">
                <ul>
                    <li><a class="navitem" id="nav-home" href="../../index.php">Home</a></li>
                    <li><a class="navitem" id="nav-training" href="../../training/index.php">Training</a></li>
                    <li><a class="navitem" id="nav-consulting" href="../../consulting/index.php">Consulting</a></li>
                    <li><a class="navitem" id="nav-reports" href="../../reports/index.php">Reports</a></li>
                    <li><a class="navitem" id="nav-articles" href="../index.php">Articles</a></li>
                    <li><a class="navitem" id="nav-about" href="../../about/index.php">About NN/G</a></li>
                </ul>
            </nav>
            <nav class="secondary-nav small-12 medium-12 large-12 columns">
                
                
            </nav>
        </div>
    </header>
    <div id="main" class="l-content" onclick="closeNav(event)"><span id="preventClose" style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776; Menu</span>
    
  

  

  <div class="row">
    
      
      <section class="small-12 medium-3 columns collapsable-content l-navrail articles-left-column">
        <h1 class="show-for-small-down collapse-link mobile-browse-header">Browse by Topic and Author</h1>
          <div class="row collapsable">

            
            <div class="small-6 medium-12 columns">
              <section id="article-popular-topics" style="display: none;">
                
                <h1>Topics</h1>
                <ul id="article-popular-topics">
                    
                    <li>
                        
                        <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/intranets/all/index.php">Intranets</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/user-testing/all/index.php">User Testing</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                        
                    </li>
                    
                    <li><a href="#" id="article-see-all-topics"><strong>See all topics&hellip;</strong></a></li>
                </ul>
                
              </section>
              <section id="article-all-topics" style="display: none;">
                
                <h1>
                  <span class="show-for-medium-up">All Article Topics</span>
                  <span class="show-for-small-down">All Topics</span>
                  (<a href="#" id="article-hide-all-topics">hide</a>)
                </h1>
                <ul>
                  
                  <li>
                    
                    <a href="../../topic/accessibility/all/index.php">Accessibility</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/agile/all/index.php">Agile</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/analytics-and-metrics/all/index.php">Analytics &amp; Metrics</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/applications/all/index.php">Application Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/b2b-websites/all/index.php">B2B Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/behavior-patterns/all/index.php">Behavior Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/branding/all/index.php">Branding</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/content-strategy/all/index.php">Content Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/corporate-websites/all/index.php">Corporate Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/design-patterns/all/index.php">Design Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-design-process/all/index.php">Design Process</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/email/all/index.php">Email</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/eyetracking/all/index.php">Eyetracking</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/heuristic-evaluation/all/index.php">Heuristic Evaluation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/human-computer-interaction/all/index.php">Human Computer Interaction</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-ideation/all/index.php">Ideation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/information-architecture/all/index.php">Information Architecture</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/interaction-design/all/index.php">Interaction Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/international-users/all/index.php">International Users</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/intranets/all/index.php">Intranets</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-management/all/index.php">Management</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/navigation/all/index.php">Navigation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/non-profit-websites/all/index.php">Non-Profit Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/personas/all/index.php">Personas</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/persuasive-design/all/index.php">Persuasive Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/prototyping/all/index.php">Prototyping</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/psychology-and-ux/all/index.php">Psychology and UX</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/research-methods/all/index.php">Research Methods</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/search/all/index.php">Search</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/social-media/all/index.php">Social Media</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/strategy/all/index.php">Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/user-testing/all/index.php">User Testing</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-teams/all/index.php">UX Teams</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/visual-design/all/index.php">Visual Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/writing-web/all/index.php">Writing for the Web</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/young-users/all/index.php">Young Users</a>
                    
                  </li>
                  
                </ul>
                
              </section>
            </div>
            <script>
              /* Determine which topics list will be displayed: the full or the
               * partial.  We are inlining this close to the list itself to
               * prevent rendering delays on the client
               */
              var topic_list = $.cookie("article-topics");

              var popular_topics = $('#article-popular-topics');
              var all_topics = $('#article-all-topics');

              if(topic_list == 1) {
                  popular_topics.hide();
                  all_topics.show();
              } else {
                  popular_topics.show();
                  all_topics.hide();
              }
            </script>
            

            
            <div class="small-6 medium-12 columns">
              
              <section id="article-initial-authors">
                  <h1>Author</h1>
                  <ul>
                    
                    <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                    
                    <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                    
                    <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                    
                    <li><a href="#" onClick="$('#article-initial-authors').hide(); $('#article-all-authors').show();"><strong>See all authors&hellip;</strong></a></li>
                  </ul>
              </section>
              <section id="article-all-authors" style="display: none;">
                <h1>
                  All Authors
                  (<a href="#" id="article-hide-all-authors">hide</a>)
                </h1>
                <ul>
                  
                  <li><a href="../author/raluca-budiu/index.php">Raluca Budiu</a></li>
                  
                  <li><a href="../author/patty-caya/index.php">Patty Caya</a></li>
                  
                  <li><a href="../author/susan-farrell/index.php">Susan Farrell</a></li>
                  
                  <li><a href="../author/therese-fessenden/index.php">Therese Fessenden</a></li>
                  
                  <li><a href="../author/kim-flaherty/index.php">Kim Flaherty</a></li>
                  
                  <li><a href="../author/sarah-gibbons/index.php">Sarah Gibbons</a></li>
                  
                  <li><a href="../author/aurora-harley/index.php">Aurora Harley</a></li>
                  
                  <li><a href="../author/kate-kaplan/index.php">Kate Kaplan</a></li>
                  
                  <li><a href="../author/page-laubheimer/index.php">Page Laubheimer</a></li>
                  
                  <li><a href="../author/angela-li/index.php">Angie Li</a></li>
                  
                  <li><a href="../author/hoa-loranger/index.php">Hoa Loranger</a></li>
                  
                  <li><a href="../author/kate-meyer/index.php">Kate Meyer</a></li>
                  
                  <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                  
                  <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                  
                  <li><a href="../author/kara-pernice/index.php">Kara Pernice</a></li>
                  
                  <li><a href="../author/christian-rohrer/index.php">Christian Rohrer</a></li>
                  
                  <li><a href="../author/amy-schade/index.php">Amy Schade</a></li>
                  
                  <li><a href="../author/katie-sherwin/index.php">Katie Sherwin</a></li>
                  
                  <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                  
                  <li><a href="../author/kathryn-whitenton/index.php">Kathryn Whitenton</a></li>
                  
                </ul>
              </section>
              
            </div>
            
          </div>

          
          <section class="hide-for-small-down">
            
              
              <h1>Recent Articles</h1>
              
              <ul>
                  
                  <li><a href="../seamless-cross-channel/index.php">Seamlessness in the Omnichannel User Experience</a></li>
                  
                  <li><a href="../brand-intention-interpretation/index.php">Experience Design: Bridging Brand Intention and Brand Interpretation</a></li>
                  
                  <li><a href="../eyetracking-tasks-efficient-scanning/index.php">Scanning Patterns on the Web Are Optimized for the Current Task</a></li>
                  
                  <li><a href="../flat-design-best-practices/index.php">Flat-Design Best Practices</a></li>
                  
                  <li><a href="../ux-user-stories/index.php">Accounting for UX Work with User Stories in Agile Projects</a></li>
                  
                  <li><a href="../index.php"><strong>See all articles&hellip;</strong></a></li>
              </ul>
              
            
          </section>
          

          
          
          <section class="hide-for-small-down">
            <h1>Popular Articles</h1>
            
            <ul>
                
                <li><a href="../usability-101-introduction-to-usability/index.php">Usability 101: Introduction to Usability</a></li>
                
                <li><a href="../top-10-mistakes-web-design/index.php">Top 10 Mistakes in Web Design</a></li>
                
                <li><a href="../how-users-read-on-the-web/index.php">How Users Read on the Web</a></li>
                
                <li><a href="../f-shaped-pattern-reading-web-content/index.php">F-Shaped Pattern For Reading Web Content</a></li>
                
                <li><a href="../ten-usability-heuristics/index.php">10 Usability Heuristics for User Interface Design</a></li>
                
                <li><a href="../intranet-design-2016/index.php">10 Best Intranets of 2016</a></li>
                
                <li><a href="../which-ux-research-methods/index.php">When to Use Which User-Experience Research Methods</a></li>
                
                <li><a href="../response-times-3-important-limits/index.php">Response Times: The 3 Important Limits</a></li>
                
                <li><a href="../why-you-only-need-to-test-with-5-users/index.php">Why You Only Need to Test with 5 Users</a></li>
                
                <li><a href="../page-fold-manifesto/index.php">The Fold Manifesto: Why the Page Fold Still Matters</a></li>
                
            </ul>
          </section>
          
          

          
            <section class="hide-for-small-down">
              
            </section>
          

      </section>
      

      
        <div class="small-12 medium-9 columns content-body">
          




<article class="l-subsection alertbox-article-content article-detail-bl">
  
  <header>
    <div class="l-subsection">
      <h1>Improving System Usability Through Parallel Design</h1>
      <div class="author-meta">by
        
          
            
            <a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a>
            
          
        on  February 1, 1996
        
      </div>

      

      
      <div class="topic-links">
        Topics:
        
          <ul>

  <li><a href="../../topic/interaction-design/index.php">Interaction Design</a></li>

</ul>
        
      </div>
      
    </div>
  </header>
  

  
  
  <section>
    <p class="lede"><strong>Summary:</strong> In parallel design multiple designers independently of each other design suggest user interfaces. These interfaces are then merged to a unified design. In a case study, measured usability from version 1 to version 2 was improved by 18% when using traditional iterative design and by 70% when using parallel design.</p>
    <br />
  </section>
  
  

  
  <section id="article-body"><p>
	Paper by <a href="../../people/jakob-nielsen/index.php" title="Author biography"> Jakob Nielsen</a> and Jan Maurits Faber  <br/>
	<em>Originally published in <cite> IEEE Computer </cite> <strong> Vol. 29 </strong> , No. 2 (February 1996), pp. 29-35. </em></p>
<h3>
	Introduction</h3>
<p>
	Companies are shortening software project schedules to introduce products more rapidly than competitors. At the same time, customers are demanding higher usability. Unfortunately, the goals of increased usability and decreased development time conflict with traditional usability engineering approaches.</p>
<p>
	The design process always requires several rounds where interfaces confront users' needs and capabilities and are modified accordingly. This approach is called <a href="../iterative-design/index.php" title="Jakob Nielsen: case studies of four iterative design projects"> iterative design</a>. <sup> [Ref. 1] </sup> Our experience indicates the need for at least two iterations, yielding three versions, before the product is good enough for release. However, three or more iterations are better.</p>
<p>
	Unfortunately, testing and redesigning take time, thus delaying product release. Because major delays are intolerable, much effort has gone into improving user interface design efficiency, prototyping, and evaluation. This has led to a cheaper and faster approach called <a href="../guerrilla-hci/index.php"> discount usability engineering</a>. <sup> [Ref. 2] </sup> However, even discount usability engineering relies on traditional linear iteration. To yield final designs faster, we want parts of the usability engineering life cycle to take place at the same time, in a process we call parallel user interface design.</p>
<h2>
	Parallel Design vs. Iterative Design</h2>
<p>
	Figure 1 shows the parallel design project model. Both iterative and parallel design are based on a version 0 concept, which describes generally what the interface is supposed to do. Parallel design speeds up time-to-market by exploring design alternatives simultaneously. The initial parallel design versions, denoted 1.a, 1.b, 1.c, and 1.d in Figure 1, do not have to be fully implemented. Merged version 2, based on each designer's best ideas, is the first to be implemented and tested.</p>
<p>
	<img alt="Parallel design process, starting at the bottom and working up for improved designs" src="http://media.nngroup.com/media/editor/2012/10/30/fig1.gif" style="width: 500px; height: 352px; "/></p>
<p>
	<br/>
	<strong>Figure 1. </strong> <em> Recommended parallel design project model. Note that this article's case study followed Figure 3's slightly different project model. </em></p>
<p>
	Each merged design element can be the best of the parallel versions' corresponding elements or a synthesis of several parallel versions' elements. The merging designer can also introduce new elements, although it is probably better to postpone the introduction of most new elements until the iterative part of the parallel process, which begins with version 3.</p>
<p>
	A weakness of parallel design is the waste of resources when several designers do the same work, even though some design ideas will not be used. Because of this, we do not recommend final polishing of parallel design versions. A skilled designer can often produce reasonable design descriptions in less than a day. For very large and complicated systems, more time is needed. But it may be possible to concentrate initial parallel design efforts on a subset of the total system to explore alternatives without fully designing the interface.</p>
<p>
	Parallel design's basic trade-off is that more staff work is invested up front in order to spend less time exploring design revisions. Therefore, parallel design is best suited for projects where reduced time-to-market is essential and makes the up-front investment acceptable. For projects where cost savings are more important than early release, traditional iterative design is preferable.</p>
<p>
	Some readers may object to parallel design, calling it design by committee. Indeed, as far back as the year 959, the Viking Olaf the Peacock said, "I want only the shrewdest to decide; in my opinion the council of fools is all the more dangerous the more of them there are." <sup> [Ref. 3] </sup> To make parallel design work best, designers should work independently and promote their ideas without the type of compromises that lead to the camel that is a horse designed by committee. The merged design can draw upon their best ideas but should not be beholden to any designer nor necessarily include ideas from all designs.</p>
<h2>
	Case Study</h2>
<p>
	In our previous projects, we found that parallel designs were excellent for exploring design options. <sup> [Ref. 4] </sup> However, we did not collect metrics for the resulting designs' usability or the method's impact on shipment schedule or on the need for future revisions. Therefore, we conducted a study that implemented all designs and subjected them to traditional, controlled-usability measurements, even though this involved considerable resource expenditure that could be justified only for research purposes.</p>
<p>
	The case study concerned screen-based user interfaces to advanced telephone services like call forwarding, where incoming calls are routed to another telephone, and call waiting, where you are notified if somebody calls while you are on the line. Instead of using a screen-based telephone, we simulated this type of telephone on a personal computer, as shown in Figure 2. Using simulation was more flexible than using the telephone network and allowed faster implementation of alternative designs.</p>
<p>
	<img alt="Picture of a smartphone" src="http://media.nngroup.com/media/editor/2012/10/30/fig2.gif" style="width: 386px; height: 556px; "/><br/>
	 </p>
<p>
	<strong>Figure 2. </strong> <em> Software mock-up of a screen-based advanced telephone service interface. The user would operate this interface by clicking on button images with a mouse. In this example, the user has selected call forwarding from the main menu and is entering the number to which calls should be forwarded. </em></p>
<p>
	Our software mock-up was patterned after realistic telephone hardware, using a small set of buttons and an alphanumeric screen with 16 40-character lines. Of course, test users could not place calls using the simulated telephone, but that was not a problem because our project concerned only features used to dial and set up calls, not the calls themselves. Also, we could easily use a computer to generate a log file with exact user-action timings.</p>
<p>
	For our parallel design project, we first defined a functional specification for the interface, which was easy, as our functionality was that offered by the telephone network. For most projects, though, task analysis and customer field visits would be needed for the specification stage. We defined a set of 16 user test tasks. One task was, "You are waiting for an important business call from 758-2307 or 758-2257. However, you are going to your neighbor's house for a while. Arrange it so that calls from either business number will go to your neighbor's house, but other calls will ring at your house. Your neighbor's phone number is 201-4232." We defined measured usability to be a function of how well our test users performed these tasks.</p>
<h2>
	Parallel Design Stage</h2>
<p>
	Figure 3 shows our project model. Four user-interface designers took part in our parallel design stage. We chose this number of designers based on earlier experience with projects where three designers were used with great success <sup> [Ref. 4] </sup> but where more designers would have improved results. We do not claim that four is the optimal number of designers for all projects.</p>
<p>
	<img alt="Design process for this research study (not recommended for commercial projects)." src="http://media.nngroup.com/media/editor/2012/10/30/fig3.gif" style="width: 500px; height: 311px; "/></p>
<p>
	<br/>
	<strong>Figure 3. </strong> <em> The project model used in the case study. For practical projects, we recommend the model in Figure 1. </em></p>
<p>
	Our four designers worked independently and had access only to their own designs. Each designer was first given the desired functionality's specification and the equipment's limitations. Each then designed a user interface and described it to a research assistant, who implemented it. To save time, the designers provided their specifications in a mixture of natural language and hand-drawn screen layouts. To reduce the possibility that the research assistant misinterpreted the designer's intentions, the assistant showed the designer the interface's running version. Designers were asked not to engage in iterative design at this stage but only to identify and correct disparities between the implemented design and their original intentions.</p>
<p>
	<the> <img alt="Four different version one designs" src="http://media.nngroup.com/media/editor/2012/10/30/fig4.gif" style="width: 600px; height: 565px; "/> </the></p>
<p>
	<the><br/>
	<strong>Figure 4. </strong> <em> The initial screens from each of the four parallel designs. Top: versions 1.a and 1.b. Bottom: versions 1.c and 1.d. For the user tests, each screen was displayed as part of the simulated telephone interface in Figure 2. </em> </the></p>
<h3>
	User Testing</h3>
<p>
	Each version was tested with 10 people who were timed performing the 16 test tasks. To keep performance from being affected by skills learned during earlier tests, rather than version usability, no person was used more than once. The designers were then given only their own test results and were told to modify their designs. Thus, their redesigns, versions 2.a-d, correspond to traditional iterative design and were produced only to compare iterative and parallel design.</p>
<p>
	We measured the time it took to perform the 16 test tasks and the number of times the user completed a task incorrectly and was told to try again. Task time is the classic usability metric. The number of errors was especially important to us because people generally use telephones without assistance.</p>
<p>
	To assess relative usability changes, we normalized the raw test results so that the overall usability score derived from the average task time and average error rate for all four designs would be 100. Improvements yielded higher scores. Overall usability was calculated as the geometric mean of the normalized scores for task time and number of errors, because this emphasized improved and reduced usability equally.</p>
<p>
	Test results for the initial parallel versions and their iterative design revisions are shown in Table 1. For the four designs, the usability improvement after one iteration ranged from 12 to 29 percent and averaged 18 percent. This was less than expected from our earlier survey, <sup> [Ref. 1] </sup> where iterative design improvement averaged 38 percent per iteration. However, the results were still within expected values, given that two projects in the earlier survey had improvements of 17 and 19 percent per iteration.</p>
<p>
	 </p>
<table border="1">
	<caption align="bottom">
		<strong>Table 1. </strong> <em> Measured usability for the four initial parallel designs and for the redesigned versions produced by the initial designers. </em></caption>
	<tbody>
		<tr>
			<th align="LEFT" valign="top">
				<font color="purple">Version </font></th>
			<th align="CENTER" valign="top">
				<font color="purple"><b>Task time in minutes </b> </font></th>
			<th align="CENTER" valign="top">
				<font color="purple"><b>Normalized task time </b> </font></th>
			<th align="CENTER" valign="top">
				<font color="purple"><b>Errors per task </b> </font></th>
			<th align="CENTER" valign="top">
				<font color="purple"><b>Normalized errors </b> </font></th>
			<th align="CENTER" valign="top">
				<font color="purple"><b>Overall usability </b> </font></th>
		</tr>
		<tr>
			<td align="LEFT" valign="top">
				<font color="purple">1.a </font></td>
			<td align="CENTER" valign="top">
				<font color="purple">27 </font></td>
			<td align="CENTER" valign="top">
				<font color="purple">106 </font></td>
			<td align="CENTER" valign="top">
				<font color="purple">1.4 </font></td>
			<td align="CENTER" valign="top">
				<font color="purple">176 </font></td>
			<td align="CENTER" valign="top">
				<font color="purple"><b>136 </b> </font></td>
		</tr>
		<tr>
			<td align="LEFT" valign="top">
				<font color="purple">1.b </font></td>
			<td align="CENTER" valign="top">
				<font color="purple">41 </font></td>
			<td align="CENTER" valign="top">
				<font color="purple">70 </font></td>
			<td align="CENTER" valign="top">
				<font color="purple">3.3 </font></td>
			<td align="CENTER" valign="top">
				<font color="purple">75 </font></td>
			<td align="CENTER" valign="top">
				<font color="purple"><b>73 </b> </font></td>
		</tr>
		<tr>
			<td align="LEFT" valign="top">
				<font color="purple">1.c </font></td>
			<td align="CENTER" valign="top">
				<font color="purple">25 </font></td>
			<td align="CENTER" valign="top">
				<font color="purple">116 </font></td>
			<td align="CENTER" valign="top">
				<font color="purple">2.8 </font></td>
			<td align="CENTER" valign="top">
				<font color="purple">88 </font></td>
			<td align="CENTER" valign="top">
				<font color="purple"><b>101 </b> </font></td>
		</tr>
		<tr>
			<td align="LEFT" valign="top">
				<font color="purple">1.d </font></td>
			<td align="CENTER" valign="top">
				<font color="purple">26 </font></td>
			<td align="CENTER" valign="top">
				<font color="purple">110 </font></td>
			<td align="CENTER" valign="top">
				<font color="purple">2.7 </font></td>
			<td align="CENTER" valign="top">
				<font color="purple">91 </font></td>
			<td align="CENTER" valign="top">
				<font color="purple"><b>100 </b> </font></td>
		</tr>
		<tr style="background-color: #ccffff">
			<td align="LEFT" valign="top">
				<font color="purple">1 average </font></td>
			<td align="CENTER" valign="top">
				<font color="purple">29 </font></td>
			<td align="CENTER" valign="top">
				<font color="purple">99 </font></td>
			<td align="CENTER" valign="top">
				<font color="purple">2.4 </font></td>
			<td align="CENTER" valign="top">
				<font color="purple">101 </font></td>
			<td align="CENTER" valign="top">
				<font color="purple"><b>100 </b> </font></td>
		</tr>
		<tr>
			<td align="LEFT" valign="top">
				<font color="purple">2.a </font></td>
			<td align="CENTER" valign="top">
				<font color="purple">24 </font></td>
			<td align="CENTER" valign="top">
				<font color="purple">118 </font></td>
			<td align="CENTER" valign="top">
				<font color="purple">1.2 </font></td>
			<td align="CENTER" valign="top">
				<font color="purple">205 </font></td>
			<td align="CENTER" valign="top">
				<font color="purple"><b>156 </b> </font></td>
		</tr>
		<tr>
			<td align="LEFT" valign="top">
				<font color="purple">2.b </font></td>
			<td align="CENTER" valign="top">
				<font color="purple">35 </font></td>
			<td align="CENTER" valign="top">
				<font color="purple">82 </font></td>
			<td align="CENTER" valign="top">
				<font color="purple">2.3 </font></td>
			<td align="CENTER" valign="top">
				<font color="purple">107 </font></td>
			<td align="CENTER" valign="top">
				<font color="purple"><b>94 </b> </font></td>
		</tr>
		<tr>
			<td align="LEFT" valign="top">
				<font color="purple">2.c </font></td>
			<td align="CENTER" valign="top">
				<font color="purple">25 </font></td>
			<td align="CENTER" valign="top">
				<font color="purple">116 </font></td>
			<td align="CENTER" valign="top">
				<font color="purple">2.0 </font></td>
			<td align="CENTER" valign="top">
				<font color="purple">123 </font></td>
			<td align="CENTER" valign="top">
				<font color="purple"><b>119 </b> </font></td>
		</tr>
		<tr>
			<td align="LEFT" valign="top">
				<font color="purple">2.d </font></td>
			<td align="CENTER" valign="top">
				<font color="purple">30 </font></td>
			<td align="CENTER" valign="top">
				<font color="purple">97 </font></td>
			<td align="CENTER" valign="top">
				<font color="purple">1.9 </font></td>
			<td align="CENTER" valign="top">
				<font color="purple">129 </font></td>
			<td align="CENTER" valign="top">
				<font color="purple"><b>112 </b> </font></td>
		</tr>
		<tr style="background-color: #ccffff">
			<td align="LEFT" valign="top">
				<font color="purple">2 average </font></td>
			<td align="CENTER" valign="top">
				<font color="purple">28 </font></td>
			<td align="CENTER" valign="top">
				<font color="purple">102 </font></td>
			<td align="CENTER" valign="top">
				<font color="purple">1.8 </font></td>
			<td align="CENTER" valign="top">
				<font color="purple">137 </font></td>
			<td align="CENTER" valign="top">
				<font color="purple"><b>118 </b> </font></td>
		</tr>
	</tbody>
</table>
<p>
	Figure 5 shows the measured usability of the original designs and their respective redesigns connected by arrows. The longer arrows indicate greater improvements. We used a logarithmic scale to give equal prominence to relative improvements, the most reasonable indication of redesign skill. Thus, all redesigns that reduced user errors by the same percentage would have equally long arrows.</p>
<p>
	<img alt="Improvements in measured usability from one round to the next" src="http://media.nngroup.com/media/editor/2012/10/30/fig5.gif" style="width: 700px; height: 680px; "/><br/>
	<strong>Figure 5. </strong> <em> Diagram showing the various designs' measured usability. Each circle indicates one design, and the placement of the circle shows that design's task time (how fast users were at using it) on the x-axis and its error rate on the y-axis (how many errors people made). Arrows connect iterative revisions of a design from one version to the next. Merged versions 2.x and 2.y were based on all four initial parallel versions, 1.a-d. </em></p>
<p>
	Optimal usability is in Figure 5's lower left corner, and most of the arrows point there, indicating task time and error rate improvements. The main exception is version 2.d, where the error rate dropped substantially but the task time increased slightly, because the design's help system was too extensive. Figure 5 shows it was easier to reduce user error rates than to speed up task performance, which is consistent with our experience from other development projects.</p>
<h2>
	Merged Design Stage</h2>
<p>
	Versions 2.x and 2.y represent the unifying step of the parallel design process. They are shown in Figure 5 with their subsequent iterative redesigns, versions 3.x and 3.y, and in Figure 6.</p>
<p>
	<img alt="Merged Designs" src="http://media.nngroup.com/media/editor/2012/10/30/fig6.gif" style="width: 600px; height: 265px; "/><br/>
	<strong>Figure 6. </strong> <em> Merged designs based on the initial parallel versions shown in Figure 4. Left: version 2.x, produced by a designer who was shown the initial designs but not their user test results. Right: version 2.y, produced by a designer who was shown the initial designs and their user test results. </em></p>
<p>
	The designers of these merged versions did not participate in producing the original versions, but they were experienced user-interface professionals on about the same level as the four original designers. In other projects, one would often choose the most senior original designer to produce the merged design to avoid having to teach yet another designer about the project. A potential problem, though, is that some designers may become too enamored of their own ideas to fully appreciate competing designs. Under these circumstances, it is best to use senior designers and ask them to guard against bias.</p>
<p>
	The two designers who produced our merged versions worked independently and were not shown each other's designs. Normally, we would use only a single designer in this step, but we used two to gather additional research data. Version 2.x's designer had access to the four original parallel designs, versions 1.a-d, but was not given user test results. Version 2.y's designer had the four original designs and the user test results. Neither was shown the original designers' redesigns, since those were done only for research purposes and were not part of the parallel design process.</p>
<p>
	We did not give the designers of versions 2.x and 2.y any of the design rationale used in the initial versions, so we could keep their work as independent as possible for evaluation and comparison purposes. Normally, one would expect the designers of the merged versions to talk to the original designers. We expect that the availability of design rationale would help the designers of the merged versions make better decisions and make parallel design's relative performance even better than in our project. However, this would also entail additional time for discussions, meetings, and report writing.</p>
<h3>
	User Testing</h3>
<p>
	The two merged designs were subjected to the same user tests as the earlier versions. The designers then produced redesigns, and the resulting versions 3.x and 3.y were subjected to a final round of testing. The test results for versions 2.x, 2.y, 3.x, and 3.y are in Table 2. Versions 3.x and 3.y had very high measured usability and were on average 2.5 times better than the first versions. It is likely that further improvements could have been achieved by additional iterations, but we did not have resources for that.</p>
<p>
	 </p>
<table border="1">
	<caption align="bottom">
		<strong>Table 2. </strong> <em> Measured usability for the two merged designs that were based on the original four parallel designs and for the redesigns of the merged versions. </em></caption>
	<tbody>
		<tr>
			<th align="CENTER" valign="top">
				<font color="purple">Version </font></th>
			<th align="CENTER" valign="top">
				<font color="purple">Task time in minutes </font></th>
			<th align="CENTER" valign="top">
				<font color="purple">Normalized task time </font></th>
			<th align="CENTER" valign="top">
				<font color="purple">Errors per task </font></th>
			<th align="CENTER" valign="top">
				<font color="purple">Normalized errors </font></th>
			<th>
				<font color="purple">Overall usability </font></th>
		</tr>
		<tr>
			<td align="LEFT" valign="top">
				<font color="purple">2.x </font></td>
			<td align="CENTER" valign="top">
				<font color="purple">19 </font></td>
			<td align="CENTER" valign="top">
				<font color="purple">152 </font></td>
			<td align="CENTER" valign="top">
				<font color="purple">0.9 </font></td>
			<td align="CENTER" valign="top">
				<font color="purple">274 </font></td>
			<td align="CENTER" valign="top">
				<font color="purple"><b>204 </b> </font></td>
		</tr>
		<tr>
			<td align="LEFT" valign="top">
				<font color="purple">2.y </font></td>
			<td align="CENTER" valign="top">
				<font color="purple">23 </font></td>
			<td align="CENTER" valign="top">
				<font color="purple">124 </font></td>
			<td align="CENTER" valign="top">
				<font color="purple">1.5 </font></td>
			<td align="CENTER" valign="top">
				<font color="purple">164 </font></td>
			<td align="CENTER" valign="top">
				<font color="purple"><b>143 </b> </font></td>
		</tr>
		<tr style="background-color: #ccffff">
			<td align="LEFT" valign="top">
				<font color="purple">2 average </font></td>
			<td align="CENTER" valign="top">
				<font color="purple">21 </font></td>
			<td align="CENTER" valign="top">
				<font color="purple">137 </font></td>
			<td align="CENTER" valign="top">
				<font color="purple">1.2 </font></td>
			<td align="CENTER" valign="top">
				<font color="purple">212 </font></td>
			<td align="CENTER" valign="top">
				<font color="purple"><b>170 </b> </font></td>
		</tr>
		<tr>
			<td align="LEFT" valign="top">
				<font color="purple">3.x </font></td>
			<td align="CENTER" valign="top">
				<font color="purple">18 </font></td>
			<td align="CENTER" valign="top">
				<font color="purple">159 </font></td>
			<td align="CENTER" valign="top">
				<font color="purple">0.5 </font></td>
			<td align="CENTER" valign="top">
				<font color="purple">493 </font></td>
			<td align="CENTER" valign="top">
				<font color="purple"><b>280 </b> </font></td>
		</tr>
		<tr>
			<td align="LEFT" valign="top">
				<font color="purple">3.y </font></td>
			<td align="CENTER" valign="top">
				<font color="purple">20 </font></td>
			<td align="CENTER" valign="top">
				<font color="purple">147 </font></td>
			<td align="CENTER" valign="top">
				<font color="purple">0.7 </font></td>
			<td align="CENTER" valign="top">
				<font color="purple">352 </font></td>
			<td align="CENTER" valign="top">
				<font color="purple"><b>228 </b> </font></td>
		</tr>
		<tr style="background-color: #ccffff">
			<td align="LEFT" valign="top">
				<font color="purple">3 average </font></td>
			<td align="CENTER" valign="top">
				<font color="purple">19 </font></td>
			<td align="CENTER" valign="top">
				<font color="purple">153 </font></td>
			<td align="CENTER" valign="top">
				<font color="purple">0.6 </font></td>
			<td align="CENTER" valign="top">
				<font color="purple">417 </font></td>
			<td align="CENTER" valign="top">
				<font color="purple"><b>252 </b> </font></td>
		</tr>
	</tbody>
</table>
<p>
	Version 2.x had higher measured usability than version 2.y, even though only version 2.y's designer had user test data. However, we do not have enough data to conclude that usability tests are unnecessary for the parallel process' merging step.</p>
<p>
	Note that the designer who moved from version 2.y to 3.y had access to the initial parallel versions' test data and achieved substantial measured usability improvements. This designer compared version 2.y's test results with those from the four original versions and looked closely at the original designs that tested better for specific tasks. This let the designer target productive redesign areas and avoid a time-consuming reading of all four usability test reports.</p>
<h2>
	Comparing Test Results</h2>
<p>
	Tables 1 and 2 show that version 2's average overall usability was 118 when designed by traditional iterative design and 170 when designed by merging four earlier parallel designs. Thus, parallel design was 44 percent better. During the next, iterative phase of the parallel design process, the measured usability improvement going from version 2.x to 3.x and from version 3.x to 3.y averaged 48 percent. This is in the expected range for improvements by iterative design, based on prior experience. <sup> [Ref. 1] </sup> Thus, parallel design did not sacrifice its improvement potential in later stages to achieve its usability boost in the merging stage.</p>
<p>
	Another question is whether it's better to simply pick the single best design out of the original options and proceed with that one design and not bother with the "losing" designs. In our case study, version 1.a was the "winner" among the first-round designs (at a usability score of 136). Iterating solely on this design generates a usability score of 156 (for version 2.a), which is lower than the average score for the merged designs (170 points, or 9% better).</p>
<p>
	Since our system mirrored the existing telephone system, we can compare our designs' usability with that of the existing system. Earlier studies <sup> [Ref. 5] </sup> showed that people had an average success rate of 61 percent the first time they used one of our system's 10 services on a traditional telephone, 84 percent when they used our parallel versions (1.a-d), and 95 percent when they used our merged versions (2.x and 2.y).</p>
<p>
	The earlier study found that people who used these services for the first time on a screen-based telephone, instead of a traditional telephone, had a success rate of 75 percent. Since the screen-based telephone used in the earlier study had a much smaller screen (16 × 7 characters) than the one called for in our design (40 × 16 characters), it is not surprising that its measured usability was poorer than that found for our initial designs.</p>
<p>
	We conclude that we had good initial parallel designs. They performed better than traditional telephones and screen-based telephones. Therefore, improvement by the merged versions cannot be attributed to initial design inferiority.</p>
<h2>
	Independent Iterative Design</h2>
<p>
	Even though usability improved significantly, one might hypothesize that this was due not to the parallel design method but to the fresh designers who worked on the merged versions. First, we do not believe these designers were better than the four parallel version designers. Also, we tried a project that introduced fresh designers at each stage, using a method we call independent iterative design. The results were miserable.</p>
<p>
	The new designers in this project not only fixed the previous design's usability problems but also introduced new ideas. This caused a phenomenon we call design thrashing, where the design never became stable or polished. Each time the design changed direction, new problems were introduced, causing the overall design's measured usability to be no better than that of its predecessor. Thus, it is unlikely that our parallel design project's improvements were due simply to using fresh designers for the merged versions.</p>
<h2>
	Cost Accounting</h2>
<p>
	<strong>Table 3 </strong> shows cost estimates for the parallel design process' various stages. Fixed costs related to project start-up remain the same no matter how many versions are designed and tested, but variable costs are incurred for each version. The variable costs are larger for a first (1.a-d) or merged (2.x-y) version than for a revision (2.a-d and 3.x-y) because the designer, and subsequently the programmer, spend more time on initial design than on revision.</p>
<table border="1">
	<caption align="bottom">
		<strong>Table 3. </strong> <em> Parallel design project cost estimates, based on costs of $30 per hour for a research assistant (RA), $125 per hour for a usability professional (UP), and a flat fee of $20 for a user. </em></caption>
	<tbody>
		<tr style="background-color: #ccffff">
			<th align="LEFT" valign="top">
				<font color="purple">Fixed cost of project start-up </font></th>
			<th align="RIGHT" valign="top">
				<font color="purple">Cost </font></th>
		</tr>
		<tr>
			<td align="LEFT" valign="top">
				<font color="purple">Learning domain, writing specifications: 160 hours RA </font></td>
			<td align="RIGHT" valign="top">
				<font color="purple">$4,800 </font></td>
		</tr>
		<tr>
			<td align="LEFT" valign="top">
				<font color="purple">Constructing test tasks, preparing pilot tests: 40 hours RA </font></td>
			<td align="RIGHT" valign="top">
				<font color="purple">$1,200 </font></td>
		</tr>
		<tr>
			<td align="LEFT" valign="top">
				<font color="purple">Pilot testing, 3 hours RA + 3 users </font></td>
			<td align="RIGHT" valign="top">
				<font color="purple">$150 </font></td>
		</tr>
		<tr>
			<td align="LEFT" valign="top">
				<font color="purple"><b>Total </b> </font></td>
			<td align="RIGHT" valign="top">
				<font color="purple"><b>$6,150 </b> </font></td>
		</tr>
		<tr style="background-color: #ccffff">
			<td align="LEFT" valign="top">
				<font color="purple"><b>Variable cost per parallel or merged version </b> </font></td>
			<td align="RIGHT" valign="top">
				 </td>
		</tr>
		<tr>
			<td align="LEFT" valign="top">
				<font color="purple">Design session, confirming implementation: 9 hours RA + UP </font></td>
			<td align="RIGHT" valign="top">
				<font color="purple">$1,395 </font></td>
		</tr>
		<tr>
			<td align="LEFT" valign="top">
				<font color="purple">Programming: 17 hours RA </font></td>
			<td align="RIGHT" valign="top">
				<font color="purple">$510 </font></td>
		</tr>
		<tr>
			<td align="LEFT" valign="top">
				<font color="purple">User testing: 20 hours RA + 10 users </font></td>
			<td align="RIGHT" valign="top">
				<font color="purple">$800 </font></td>
		</tr>
		<tr>
			<td align="LEFT" valign="top">
				<font color="purple">Writing test report: 6 hours RA </font></td>
			<td align="RIGHT" valign="top">
				<font color="purple">$180 </font></td>
		</tr>
		<tr>
			<td align="LEFT" valign="top">
				<font color="purple"><b>Total </b> </font></td>
			<td align="RIGHT" valign="top">
				<font color="purple"><b>$2,885 </b> </font></td>
		</tr>
		<tr style="background-color: #ccffff">
			<td align="LEFT" valign="top">
				<font color="purple"><b>Variable cost per iterative design version </b> </font></td>
			<td align="RIGHT" valign="top">
				 </td>
		</tr>
		<tr>
			<td align="LEFT" valign="top">
				<font color="purple">Redesign session, confirming implementation: 5 hours RA + UP </font></td>
			<td align="RIGHT" valign="top">
				<font color="purple">$775 </font></td>
		</tr>
		<tr>
			<td align="LEFT" valign="top">
				<font color="purple">Programming: 9 hours RA </font></td>
			<td align="RIGHT" valign="top">
				<font color="purple">$270 </font></td>
		</tr>
		<tr>
			<td align="LEFT" valign="top">
				<font color="purple">User testing: 20 hours RA + 10 users </font></td>
			<td align="RIGHT" valign="top">
				<font color="purple">$800 </font></td>
		</tr>
		<tr>
			<td align="LEFT" valign="top">
				<font color="purple">Writing test report: 6 hours RA </font></td>
			<td align="RIGHT" valign="top">
				<font color="purple">$180 </font></td>
		</tr>
		<tr>
			<td align="LEFT" valign="top">
				<font color="purple"><b>Total </b> </font></td>
			<td align="RIGHT" valign="top">
				<font color="purple"><b>$2,025 </b> </font></td>
		</tr>
	</tbody>
</table>
<p>
	 </p>
<div style="border: solid thin #999999; background-color: #eeeeee; padding: 1ex">
	Note added 2006: The numbers in the table were the ones that applied to phone company projects in 1996. Sadly, costs have gone up some for commercial usability projects over the last ten years. In particular, the <a href="../recruiting-test-participants-for-usability-studies/index.php" title="Alertbox: Recruiting Test Participants for Usability Studies"> cost per test user</a> is closer to $200 when you test a more targeted audience, as is typical for website research. These cost increases don't change our general conclusion, but do be prepared to pay a good deal more than indicated in the table.</div>
<p>
	Our overall costs were slightly lower than the estimates for earlier iterative design projects, where the estimated fixed start-up costs ranged from $7,000 to $16,500 and the estimated variable costs for each version after the first ranged from $2,800 to $7,000. Although we are better at controlling costs now, they were lower primarily because this study's interface was smaller and simpler than the interfaces we usually work with.</p>
<p>
	There is no doubt that parallel design is more expensive than standard iterative design. If we eliminate the cost of the additional versions we tested only for research purposes, we can calculate the two project models' costs as follows, for a design ending after version 3:</p>
<ul>
	<li>
		Traditional iterative design: $6,150 fixed costs + $2,885 variable costs for one initial version + $4,050 variable costs for two subsequent versions = $13,085.</li>
	<li>
		Parallel design: $6,150 fixed costs + $11,540 variable costs for four initial parallel versions + $2,885 variable costs for one merged version + $2,025 variable costs for one subsequent version = $22,600.</li>
</ul>
<p>
	In our project, parallel design was 73 percent more expensive than iterative design. We still recommend parallel design because it achieves major usability improvements very fast. We don't know how usability would have improved if we had used iterative design and extended the iterative process. If we assume usability improvements of 18 percent per iteration, which was the average for our four initial designers, version 3's usability level would have been 139. This compares very poorly with the measured usability of 252 for the parallel process' version 3. An average of 6.6 versions, costing $20,375 and considerable time, would have been required for standard iterative design to reach a usability level of 252.</p>
<h2>
	Diversified Parallel Design</h2>
<p>
	In a variant called diversified parallel design, each parallel-stage designer optimizes an interface for one specific platform or user group without considering other platforms or user groups that will have to be supported. For example, one designer can design a novice user interface, while another designs an expert user interface. This allows both designers to explore the design space more fully than if each had to work on an all-encompassing design. This also provides a refined set of ideas from which the eventual unified design will support all intended platforms and users.</p>
<h2>
	Deviations From Recommended Practice</h2>
<p>
	We had at least ten people take each test, even though we would normally recommend using no more than five, because <a href="../why-you-only-need-to-test-with-5-users/index.php" title="Alertbox: Why You Only Need to Test With 5 Users"> almost all interface usability problems can be found with 5 tests</a>. However, our project required many test users to get reasonably tight confidence intervals on our usability measures.</p>
<p>
	Our test tasks and system functionality were fixed for all versions. Some development projects require constant specifications, but the product functionality definition normally changes as one learns more about product use. We had to keep our test tasks and product specifications constant to get comparable results from the testing of the various versions.</p>
<p>
	Our parallel design phase deviated from what we would recommend. First, we would not ask each parallel designer to redesign without seeing other designers' versions. Thus, versions 2.a-d were produced only for research purposes. Generally, the merged version would be the only version 2. Second, we implemented all versions as complete running programs because we wanted to know exactly how long it took users to perform realistic tasks. It might normally be sufficient for exploration purposes to implement most early versions as low-fidelity prototypes.</p>
<h2>
	Conclusion</h2>
<p>
	Our case study supports the value of parallel design. It reflects one possible use of the method, which would probably be even more successful for traditional graphical user interfaces with more design space.</p>
<p>
	Parallel design is more expensive than traditional iterative design, but it explores the design space in less time. In our case study, the improvement in measured usability from version 1 to 2 was 18 percent with traditional iterative design and 70 percent with parallel design. Since parallel design is more expensive, we cannot recommend it for all projects, but it has value when time-to-market is of the essence.</p>
<h2>
	Acknowledgments</h2>
<p>
	This work was done while the authors were with Bellcore's Applied Research area. The authors thank Sheila Borack for substantial help in scheduling subjects and for other matters related to running experiments. We thank Richard Herring for letting us reuse his task descriptions for user testing of advanced telephone services. We also benefited from George Furnas' helpful comments on a previous version of this article. Finally, we especially thank the six Bellcore user-interface designers who spent significant time designing and redesigning our interface. Since these people were, in some sense, the real subjects for our study, ethical considerations prevent us from listing their names. But they know who they are.</p>
<h2>
	References</h2>
<ol>
	<li>
		J. Nielsen, " <a href="../iterative-design/index.php"> Iterative User-Interface Design</a>," <cite> IEEE Computer, </cite> Vol. 26, No. 11, Nov. 1993, pp. 32-41.</li>
	<li>
		J. Nielsen, <a href="../../books/usability-engineering/index.php"> Usability Engineering,</a> Academic Press, Boston, 1994.</li>
	<li>
		M. Magnusson and H. Palsson, (translators), <cite> Laxdaela Saga, </cite> Penguin Books, London, 1969, p. 90. A translation of an anonymous Icelandic handwritten manuscript from about 1245.</li>
	<li>
		J. Nielsen et al., "Comparative Design Review: An Exercise in Parallel Design," <cite> Proc. ACM INTERCHI'93 Conf., </cite> Assoc. for Computing Machinery, New York, 1993, pp. 414-417.</li>
	<li>
		R.D. Herring, J.A. List, and E.A. Youngs, "Screen-Assisted Telephony and Voice Service Usability," <cite> Proc. 14th Int'l. Symp. on Human Factors in Telecommunications, </cite> 14th Int'l. Symp. on Human Factors in Telecommunications, Darmstadt, Germany, 1993.</li>
</ol>
</section>
  

  
  <section>
    
        
    
  </section>
  
  
  
  
  

  


  <section>
    <br />
    <p class="share"><b>Share <span class="hide-for-small-down">this article</span>:</b> <a href="https://twitter.com/intent/tweet?url=http://www.nngroup.com/articles/parallel-design/&amp;text=Improving%20System%20Usability%20Through%20Parallel%20Design&amp;via=nngroup">Twitter</a> | <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=http://www.nngroup.com/articles/parallel-design/&amp;title=Improving%20System%20Usability%20Through%20Parallel%20Design&amp;source=Nielsen%20Norman%20Group">LinkedIn</a> | <a href="https://plus.google.com/share?url=http://www.nngroup.com/articles/parallel-design/">Google+</a> | <a href="mailto:?subject=NN/g Article: Improving System Usability Through Parallel Design&amp;body=http://www.nngroup.com/articles/parallel-design/">Email</a></p>
    <hr />
  </section>

  <footer class="l-subsection">
    
    
    <div class="learn-more">
      <h2>Learn More</h2>
      <p><a href="../subscribe/index.php">Subscribe to the weekly newsletter</a> to get notified about future articles.</p>
      </div>
    
    

    <div class="row show-for-small-down related-content">
      
      
      

      
      
        <div class="column small-12 topic-links">All articles about:
          <ul>
            
            <li><a href="../../topic/interaction-design/index.php">Interaction Design</a></li>
            
          </ul>
        </div>
      
      

      
      
        <div class="column small-12">
          <h3>UX Conference Training Courses</h3>
           <ul class="no-bullet">
    
        <li><a href="../../courses/facilitating-ux-workshops/index.php">Facilitating UX Workshops</a></li>
    
        <li><a href="../../courses/ideation/index.php">Effective Ideation Techniques for UX Design</a></li>
    
        <li><a href="../../courses/generating-big-ideas/index.php">Generating Big Ideas with Design Thinking</a></li>
    
        <li><a href="../../courses/cross-channel-user-experience/index.php">Omnichannel Journeys and Customer Experience</a></li>
    
        <li><a href="../../courses/interaction-design-3-day-course/index.php">Interaction Design: 3-Day Course</a></li>
    
</ul>
        </div>
      
      

      
      
        <div class="small-12 column">
          <h3>Research Reports</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../reports/website-tools-and-applications-flash/index.php">Website Tools and Applications with Flash</a></li>
              
            
          </ul>
        </div>
      
      

      
      
        <div class="small-12 column">
          <h3>Online Seminars</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../online-seminars/informal-sketching/index.php">Informal Sketching Techniques</a></li>
              
            
          </ul>
        </div>
      
      
    </div>

    <div class="hide-for-small-down row related-content">
      <div class="medium-6 columns">
        
        
          <div>
            <h3>Articles</h3>
            <ul class="no-bullet">
              
                
                <li><a href="../customization/index.php">7 Tips for Successful Customization</a></li>
                
              
                
                <li><a href="../customization-personalization/index.php">Customization vs. Personalization in the User Experience</a></li>
                
              
                
                <li><a href="http://asktog.com/atc/principles-of-interaction-design/">First Principles of Interaction Design (Revised &amp; Expanded)</a></li>
                
              
                
                <li><a href="http://www.jnd.org/dn.mss/suggested_readings_f.php">Suggested Readings From Design of Everyday Things, Revised edition (at jnd.org)</a></li>
                
              
                
                <li><a href="http://asktog.com/atc/providing-predictable-targets">Providing Predictable Targets (at asktog.com)</a></li>
                
              
            </ul>
          </div>
        
        

        
          
          <div>
            <h3>Research Reports</h3>
            <ul class="no-bullet">
              
                
                  <li><a href="../../reports/website-tools-and-applications-flash/index.php">Website Tools and Applications with Flash</a></li>
                
              
            </ul>
          </div>
          
        

      </div>
      <div class="medium-6 columns">
        
          
          <div>
            <h3>UX Conference Training Courses</h3>
             <ul class="no-bullet">
    
        <li><a href="../../courses/facilitating-ux-workshops/index.php">Facilitating UX Workshops</a></li>
    
        <li><a href="../../courses/ideation/index.php">Effective Ideation Techniques for UX Design</a></li>
    
        <li><a href="../../courses/generating-big-ideas/index.php">Generating Big Ideas with Design Thinking</a></li>
    
        <li><a href="../../courses/cross-channel-user-experience/index.php">Omnichannel Journeys and Customer Experience</a></li>
    
        <li><a href="../../courses/interaction-design-3-day-course/index.php">Interaction Design: 3-Day Course</a></li>
    
</ul>
          </div>
          
        
        
          
          <div>
            <h3>Online Seminars</h3>
            <ul class="no-bullet">
            
                <li><a href="../../online-seminars/informal-sketching/index.php">Informal Sketching Techniques</a></li>
            
            </ul>
          </div>
          
        
      </div>
    </div>
  </footer>
</article>

        </div>

      

    
  </div>
  <div class="row">
    
      <div class="column small-12 show-for-small-down l-navrail announcements">
        
          
        
      </div>
    
    
  </div>

    </div>
    




<footer id="page-footer">
    <div class="row hide-for-print">
        <div class="column small-12 medium-6">
            <section class="footer-popular">
                
                <h1>Popular Topics</h1>
                <div style="float: left;  width: 15em; display: block;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/applications/index.php">Application Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/e-commerce/index.php">E-Commerce Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/eyetracking/index.php">Eyetracking </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/information-architecture/index.php">Information Architecture </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/intranets/index.php">Intranet Design </a></li>
	<li><a href="../../topic/interaction-design/index.php">Interaction Design </a></li>
</ul>
</div>

<div style="float: left;  width: 15em; display: block; list-style: none;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/mobile-and-tablet-design/index.php">Mobile and Tablet Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/credibility-persuasion/index.php">Persuasive Design</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/research-methods/index.php">Research Methods</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/user-testing/index.php">User Testing </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/web-usability/index.php">Web Usability </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/writing-web/index.php">Web Writing &amp; Content Strategy </a></li>
</ul>
</div>

<p>&nbsp;</p>

                
            </section>
        </div>

        <div class="column small-12 medium-6">
            <div class="row">
                <div class="column small-6">
                    <section class="footer-about">
                        <h1>Follow Us</h1>
                        <ul>
	<li><a href="../subscribe/index.php">Subscribe to our weekly UX newsletter</a>,&nbsp;Jakob Nielsen&#39;s Alertbox</li>
	<li>Visit Don Norman&#39;s website, <a href="http://www.jnd.org/"> www.jnd.org </a></li>
	<li>Visit Bruce Tognazzini&#39;s website, <a href="http://www.asktog.com/"> www.asktog.com </a></li>
	<li style="padding-top: 6px;"><a class="twitter-follow-button" data-show-count="false" data-size="large" href="https://twitter.com/nngroup"><img alt="Follow @nngroup on Twitter" height="20" src="https://media.nngroup.com/media/editor/2016/12/30/twitter-follow-button.png" style="width: 117px; height: 20px;" width="117" /></a></li>
	<li style="padding-top: 6px;"><img alt="" height="14" src="https://media.nngroup.com/media/editor/2016/07/26/feed-icon-14x14.png" style="vertical-align:text-top;width:14px;margin-right:.25em;" width="14" /><a href="../../feed/rss/index.php">Subscribe to our RSS feed</a></li>
</ul>

                    </section>
                </div>

                <div class="column small-6">
                    <section class="footer-about right-column">
                        <h1>About
</h1>
                        <ul class="no-bullet">
	<li><a href="../../about/contact/index.php">Contact information</a></li>
	<li><a href="../../about/index.php">About Us</a></li>
	<li><a href="../../about/why-nng/index.php">Why NN/g? </a></li>
	<li><a href="../../return-policy/index.php">Return Policy</a></li>
</ul>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="copyright column small-12">
            <p><a href="../../copyright-and-reprint-info/index.php">Copyright</a> &copy; 1998-2017 Nielsen Norman Group, All Rights Reserved.</p>
        </div>
    </div>
</footer>


    


        <script src="https://media.nngroup.com/static/vendor/fastclick/js/fastclick.min.js"></script>
        <script src="https://media.nngroup.com/static/vendor/foundation/js/foundation.min.js"></script>
        <script src="http://media.nngroup.com/static/js/app.js"></script>
        <script src="http://media.nngroup.com/static/js/scroll-to-fixed.js"></script><script>var externalLinks = document.querySelectorAll('a[href^="http"]');for (var i = externalLinks.length-1; i >= 0; i--) {externalLinks[i].addEventListener("click", function(event) { alert("External links are disabled for this test."); event.preventDefault(); }, false);}var mailLinks = document.querySelectorAll('a[href^="mailto"]');for (var i = mailLinks.length-1; i >= 0; i--) {mailLinks[i].addEventListener("click", function(event) { alert("Email links are disabled for this test."); event.preventDefault(); }, false);}var externalForms = document.querySelectorAll('form');for (var i = externalForms.length-1; i >= 0; i--) {externalForms[i].addEventListener("submit", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false); externalForms[i].addEventListener("click", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false);}</script>

        <script>
            
            $(function() {
                $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
                $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
            });
        </script>

        
<script src="https://media.nngroup.com/static/js/articles.js"></script>


        
        <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1072699960;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072699960/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
    </body>

<!-- Mirrored from www.nngroup.com/articles/parallel-design/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:14:00 GMT -->
</html>
