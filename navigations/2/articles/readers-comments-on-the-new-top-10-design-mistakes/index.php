<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->





    
<!-- Mirrored from www.nngroup.com/articles/readers-comments-on-the-new-top-10-design-mistakes/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:15:47 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1026.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"licenseKey":"1a30ef9931","errorBeacon":"bam.nr-data.net","applicationID":"2048992","transactionName":"NFxSMRBUX0EDUkIPDA0WdhAMVkVbDV8ZBxEXUFMJB0YfRAtUQRVZIktEDAFZVHYHRVcPD01eVRE=","beacon":"bam.nr-data.net","queueTime":5,"applicationTime":500,"agent":""}</script>
        <title>Readers&#39; Comments on the new Top-10 Design Mistakes</title><meta property="og:title" content="Readers&#39; Comments on the new Top-10 Design Mistakes" />
  
        
        <meta name="description" content="Even more design mistakes that annoy readers. Usability problems in Web advertising.">
        <meta property="og:description" content="Even more design mistakes that annoy readers. Usability problems in Web advertising." />
        
  
        
	
        
        <meta name="keywords" content="&quot;banner blockers, advertising banners,
Siemens WebWasher, deceptive advertisement, links, URL, URLs,
trans-Atlantic bandwidth, caching, document formats, non-standard file format,
PDF, .doc, complex tables">
        

        

        <meta name="robots" content="noarchive">
        <meta name="viewport" content="width=device-width">

        <link rel="search" type="application/opensearchdescription+xml" title="NN/g Search" href="../../opensearch.xml" />
        <link rel="shortcut icon" href="http://media.nngroup.com/static/img/favicon.ico" />

        <link rel="stylesheet" href="https://media.nngroup.com/static/vendor/font-awesome/css/font-awesome.min.css">

        
        <link rel="stylesheet" href="https://media.nngroup.com/static/css/7232a960f1df.css" type="text/css" />
        <link rel="stylesheet" href="http://fast.fonts.com/t/1.css?apiType=css&amp;projectid=c5846260-9def-46ec-9407-0f1349bd2d64" type="text/css">

        <script src="https://media.nngroup.com/static/vendor/modernizr/js/modernizr.min.js"></script>

        <script type="text/javascript" src="https://media.nngroup.com/static/js/3283be5def74.js"></script>
        

        

        
        
        <!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=229034,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('http://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->


<!-- Google Analytics Tracking Code for http://nngroup.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23132681-1', 'nngroup.com');
  ga('require', 'linkid', 'https://www.nngroup.com/articles/readers-comments-on-the-new-top-10-design-mistakes/linkid.js');
  ga('require', 'displayfeatures');

// Optimizely Universal Analytics Integration
 window.optimizely = window.optimizely || [];
 window.optimizely.push("activateUniversalAnalytics");

  ga('send', 'pageview');

</script>
<script>
/**
* Function to track a click on an outbound link in Google Analytics.
*/
function trackOutboundLink(label, url) {
    if(window.ga && ga.loaded) {
        ga('send', 'event', 'outbound', 'click', label, {'hitCallback': function(){document.location = url;}
        });
    } else {
        document.location = url;
    }
}
/**
* Function to track a click on a link in the article announcement area.
*/
$('#articleAnnouncement a').on('click', function(){
  ga('send', 'event', 'link', 'click', 'articleAnnouncement');
});
</script>

<script type="text/javascript">
 setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2421.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



<!-- Hotjar Tracking Code for http://nngroup.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:121605,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

        <script src="https://media.nngroup.com/static/js/analytics/track_outbound_link.js"></script>
        
        
        
    </head>
    <body class="location-articles articles-detail">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
    <header id="page-header">
        <div id="cart" class="shopping-cart mobile-cart show-for-small-down"></div>
        <div class="row">
            <div id="masthead" class="masthead small-12 medium-7 column">

                <!-- The button to toggles menu on small screen -->

                <h1><a href="../../index.php"><img src="http://media.nngroup.com/static/img/logo-wordmark.png" width="74" height="30"> Nielsen <span class="norman">Norman</span> Group</a></h1>
                <p class="tagline show-for-medium-up">Evidence-Based User Experience Research, Training, and Consulting</p>

                <!-- The button to toggles form search on small screen -->


                <div style="clear: right;width:  100%;height: 1px;"></div><?php define('ROOT_PATH',$_SERVER["DOCUMENT_ROOT"] . '/apps/navigation_styles/navigations/2'); ?><?php include(ROOT_PATH)."/navigation.php"; ?>

                <!-- Drop down menu on small screen -->
                <div id="hideNav" class="show-for-small-down">
                  
                  <ul class="menu-content">
                    <li><a href="../../index.php">Home</a></li>
                    <li class="has-submenu">
                      <a>Training</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../training/index.php">Overview</a></li>
                        
<li id="nav-ux-conference"><a href="../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../online-seminars/index.php"><span>Online Seminars</span></a></li>

                      </ul>
                    </li>
                    <li class="has-submenu">
                      <a>Consulting</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
  <li><a href="../../consulting/index.php">Overview</a></li>
  
  <li><a href="../../consulting/benchmarking/index.php">Benchmarking</a></li>
  
  <li><a href="../../consulting/expert-design-review/index.php">Expert Design Review</a></li>
  
  <li><a href="../../consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
  
  <li><a href="../../consulting/usability-testing/index.php">Usability Testing </a></li>
  
  <li><a href="../../consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
  
  <li><a href="../../in-house-training/index.php">In-House Training</a></li>
</ul>

                    </li>

                    <li><a href="../../reports/index.php">Reports</a></li>
                    <li><a href="../index.php">Articles</a></li>
                    <li class="has-submenu"><a>About NN/G</a><i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../about/index.php">Overview</a></li>
                        <li><a href="../../people/index.php">People</a></li>
                        <li><a href="../../about/why-nng/index.php">Why NN/g?</a></li>
                        <li><a href="../../about/contact/index.php">Contact</a></li>
                        <li><a href="../../news/index.php">News</a></li>
                        <li><a href="../../about/history/index.php">History</a></li>
                        <li><a href="../../books/index.php">Books</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                </div>

                <hr class="header-divider show-for-small-only"/>


            </div>
       </div>

        <div id="hideNav">            <nav class="primary-nav small-12 column">
                <ul>
                    <li><a class="navitem" id="nav-home" href="../../index.php">Home</a></li>
                    <li><a class="navitem" id="nav-training" href="../../training/index.php">Training</a></li>
                    <li><a class="navitem" id="nav-consulting" href="../../consulting/index.php">Consulting</a></li>
                    <li><a class="navitem" id="nav-reports" href="../../reports/index.php">Reports</a></li>
                    <li><a class="navitem" id="nav-articles" href="../index.php">Articles</a></li>
                    <li><a class="navitem" id="nav-about" href="../../about/index.php">About NN/G</a></li>
                </ul>
            </nav>
            <nav class="secondary-nav small-12 medium-12 large-12 columns">
                
                
            </nav>
        </div>
    </header>
    <div id="main" class="l-content" onclick="closeNav(event)"><span id="preventClose" style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776; Menu</span>
    
  

  

  <div class="row">
    
      
      <section class="small-12 medium-3 columns collapsable-content l-navrail articles-left-column">
        <h1 class="show-for-small-down collapse-link mobile-browse-header">Browse by Topic and Author</h1>
          <div class="row collapsable">

            
            <div class="small-6 medium-12 columns">
              <section id="article-popular-topics" style="display: none;">
                
                <h1>Topics</h1>
                <ul id="article-popular-topics">
                    
                    <li>
                        
                        <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/intranets/all/index.php">Intranets</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/user-testing/all/index.php">User Testing</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                        
                    </li>
                    
                    <li><a href="#" id="article-see-all-topics"><strong>See all topics&hellip;</strong></a></li>
                </ul>
                
              </section>
              <section id="article-all-topics" style="display: none;">
                
                <h1>
                  <span class="show-for-medium-up">All Article Topics</span>
                  <span class="show-for-small-down">All Topics</span>
                  (<a href="#" id="article-hide-all-topics">hide</a>)
                </h1>
                <ul>
                  
                  <li>
                    
                    <a href="../../topic/accessibility/all/index.php">Accessibility</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/agile/all/index.php">Agile</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/analytics-and-metrics/all/index.php">Analytics &amp; Metrics</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/applications/all/index.php">Application Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/b2b-websites/all/index.php">B2B Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/behavior-patterns/all/index.php">Behavior Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/branding/all/index.php">Branding</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/content-strategy/all/index.php">Content Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/corporate-websites/all/index.php">Corporate Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/design-patterns/all/index.php">Design Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-design-process/all/index.php">Design Process</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/email/all/index.php">Email</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/eyetracking/all/index.php">Eyetracking</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/heuristic-evaluation/all/index.php">Heuristic Evaluation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/human-computer-interaction/all/index.php">Human Computer Interaction</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-ideation/all/index.php">Ideation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/information-architecture/all/index.php">Information Architecture</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/interaction-design/all/index.php">Interaction Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/international-users/all/index.php">International Users</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/intranets/all/index.php">Intranets</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-management/all/index.php">Management</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/navigation/all/index.php">Navigation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/non-profit-websites/all/index.php">Non-Profit Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/personas/all/index.php">Personas</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/persuasive-design/all/index.php">Persuasive Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/prototyping/all/index.php">Prototyping</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/psychology-and-ux/all/index.php">Psychology and UX</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/research-methods/all/index.php">Research Methods</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/search/all/index.php">Search</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/social-media/all/index.php">Social Media</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/strategy/all/index.php">Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/user-testing/all/index.php">User Testing</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-teams/all/index.php">UX Teams</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/visual-design/all/index.php">Visual Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/writing-web/all/index.php">Writing for the Web</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/young-users/all/index.php">Young Users</a>
                    
                  </li>
                  
                </ul>
                
              </section>
            </div>
            <script>
              /* Determine which topics list will be displayed: the full or the
               * partial.  We are inlining this close to the list itself to
               * prevent rendering delays on the client
               */
              var topic_list = $.cookie("article-topics");

              var popular_topics = $('#article-popular-topics');
              var all_topics = $('#article-all-topics');

              if(topic_list == 1) {
                  popular_topics.hide();
                  all_topics.show();
              } else {
                  popular_topics.show();
                  all_topics.hide();
              }
            </script>
            

            
            <div class="small-6 medium-12 columns">
              
              <section id="article-initial-authors">
                  <h1>Author</h1>
                  <ul>
                    
                    <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                    
                    <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                    
                    <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                    
                    <li><a href="#" onClick="$('#article-initial-authors').hide(); $('#article-all-authors').show();"><strong>See all authors&hellip;</strong></a></li>
                  </ul>
              </section>
              <section id="article-all-authors" style="display: none;">
                <h1>
                  All Authors
                  (<a href="#" id="article-hide-all-authors">hide</a>)
                </h1>
                <ul>
                  
                  <li><a href="../author/raluca-budiu/index.php">Raluca Budiu</a></li>
                  
                  <li><a href="../author/patty-caya/index.php">Patty Caya</a></li>
                  
                  <li><a href="../author/susan-farrell/index.php">Susan Farrell</a></li>
                  
                  <li><a href="../author/therese-fessenden/index.php">Therese Fessenden</a></li>
                  
                  <li><a href="../author/kim-flaherty/index.php">Kim Flaherty</a></li>
                  
                  <li><a href="../author/sarah-gibbons/index.php">Sarah Gibbons</a></li>
                  
                  <li><a href="../author/aurora-harley/index.php">Aurora Harley</a></li>
                  
                  <li><a href="../author/kate-kaplan/index.php">Kate Kaplan</a></li>
                  
                  <li><a href="../author/page-laubheimer/index.php">Page Laubheimer</a></li>
                  
                  <li><a href="../author/angela-li/index.php">Angie Li</a></li>
                  
                  <li><a href="../author/hoa-loranger/index.php">Hoa Loranger</a></li>
                  
                  <li><a href="../author/kate-meyer/index.php">Kate Meyer</a></li>
                  
                  <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                  
                  <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                  
                  <li><a href="../author/kara-pernice/index.php">Kara Pernice</a></li>
                  
                  <li><a href="../author/christian-rohrer/index.php">Christian Rohrer</a></li>
                  
                  <li><a href="../author/amy-schade/index.php">Amy Schade</a></li>
                  
                  <li><a href="../author/katie-sherwin/index.php">Katie Sherwin</a></li>
                  
                  <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                  
                  <li><a href="../author/kathryn-whitenton/index.php">Kathryn Whitenton</a></li>
                  
                </ul>
              </section>
              
            </div>
            
          </div>

          
          <section class="hide-for-small-down">
            
              
              <h1>Recent Articles</h1>
              
              <ul>
                  
                  <li><a href="../seamless-cross-channel/index.php">Seamlessness in the Omnichannel User Experience</a></li>
                  
                  <li><a href="../brand-intention-interpretation/index.php">Experience Design: Bridging Brand Intention and Brand Interpretation</a></li>
                  
                  <li><a href="../eyetracking-tasks-efficient-scanning/index.php">Scanning Patterns on the Web Are Optimized for the Current Task</a></li>
                  
                  <li><a href="../flat-design-best-practices/index.php">Flat-Design Best Practices</a></li>
                  
                  <li><a href="../ux-user-stories/index.php">Accounting for UX Work with User Stories in Agile Projects</a></li>
                  
                  <li><a href="../index.php"><strong>See all articles&hellip;</strong></a></li>
              </ul>
              
            
          </section>
          

          
          
          <section class="hide-for-small-down">
            <h1>Popular Articles</h1>
            
            <ul>
                
                <li><a href="../usability-101-introduction-to-usability/index.php">Usability 101: Introduction to Usability</a></li>
                
                <li><a href="../top-10-mistakes-web-design/index.php">Top 10 Mistakes in Web Design</a></li>
                
                <li><a href="../how-users-read-on-the-web/index.php">How Users Read on the Web</a></li>
                
                <li><a href="../f-shaped-pattern-reading-web-content/index.php">F-Shaped Pattern For Reading Web Content</a></li>
                
                <li><a href="../ten-usability-heuristics/index.php">10 Usability Heuristics for User Interface Design</a></li>
                
                <li><a href="../intranet-design-2016/index.php">10 Best Intranets of 2016</a></li>
                
                <li><a href="../which-ux-research-methods/index.php">When to Use Which User-Experience Research Methods</a></li>
                
                <li><a href="../response-times-3-important-limits/index.php">Response Times: The 3 Important Limits</a></li>
                
                <li><a href="../why-you-only-need-to-test-with-5-users/index.php">Why You Only Need to Test with 5 Users</a></li>
                
                <li><a href="../page-fold-manifesto/index.php">The Fold Manifesto: Why the Page Fold Still Matters</a></li>
                
            </ul>
          </section>
          
          

          
            <section class="hide-for-small-down">
              
            </section>
          

      </section>
      

      
        <div class="small-12 medium-9 columns content-body">
          




<article class="l-subsection alertbox-article-content article-detail-bl">
  
  <header>
    <div class="l-subsection">
      <h1>Readers&#39; Comments on the new Top-10 Design Mistakes</h1>
      <div class="author-meta">by
        
          
            
            <a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a>
            
          
        on  May 31, 1999
        
      </div>

      

      
      <div class="topic-links">
        Topics:
        
          <ul>

</ul>
        
      </div>
      
    </div>
  </header>
  

  
  
  

  
  <section id="article-body"><p>
 <em>
  Sidebar to
  <a href="../../people/jakob-nielsen/index.php" title="Author biography">
   Jakob Nielsen
  </a>
  &#39;s column on
  <a href="../the-top-ten-web-design-mistakes-of-1999/index.php">
   The Top Ten New Mistakes of Web Design
  </a>
  .
 </em>
</p>
<h2>
 Even More New Web Design Problems
</h2>
<p>
 Readers pointed out several new design mistakes that should also be avoided.
</p>
<h3>
 Downplaying Why Users Come To Your Site
</h3>
<p>
 <em>
  John Brewer from
  <a class="out" href="http://www.jera.com/" title="Company home page">
   Jera Design
  </a>
  writes:
 </em>
</p>
<blockquote>
 It&#39;s a mistake to bury your site&#39;s core competency in buzzwords. Typically, this involves redesigning your site to look &quot;sort of like Yahoo&quot;
 <strong>
  at the expense of whatever it was that brought people to your site in the first place
 </strong>
 .
 <p>
  Probably the most egregious example of this is www.netscape.com. (Exercise: figure out what to click on Netscape&#39;s home page to get tech support for Netscape Navigator!)
 </p>
 <p>
  DejaNews (oops, Deja.com) has relegated its prime purpose (trolling for old USENET postings) to a box in the upper right corner of the screen. It&#39;s not actually called USENET or netnews, either. It&#39;s called &quot;Discussions&quot;.
 </p>
 <p>
  And recently, eGroups, which provides free mailing lists and list archives, now no longer takes me directly to my home page. Instead, I have to wade through a page that describes all their other services. (And the link I click to get to my home page is shaped like an ad banner, an example of mistake #10).
 </p>
</blockquote>
<p>
 <em>
  Jakob&#39;s reply:
 </em>
 Many sites apparently think it is not sufficiently glamorous to support the reasons they became popular in the first place. If users like you for any reason, then it is better to improve that core competency and make it into an even greater strength than it is to hide it and try to force users to do something else. Most sites have great problems finding something that users want to do in this new medium. If you have something good, then please don&#39;t let it go.
</p>
<h3>
 Audio is Intrusive
</h3>
<p>
 <em>
  Alistair Nicholson, Principal Consultant with e-strategists, writes:
 </em>
</p>
<blockquote>
 Those designers that play a sound file without choice immediately a page is displayed lose traffic from every worker in an office situation with an advanced workstation with speakers. If my computer starts playing music, those around me are likely to assume that I am just playing games. At the very least, it lowers the credibility of the information, and in the increasingly common cubicle &quot;farms&quot; is disruptive to those around me. I immediately back out of such a site, and then consider if it seemed so valuable that I should adjust my sound level and re-enter, or just go somewhere else. The situation is quite different when a click is required to fire a sound file off, and I can make a choice.
</blockquote>
<p>
 <em>
  Jakob&#39;s reply:
 </em>
 Auditory interfaces differ from visual ones in many ways, including the fact that the ears are &quot;always on,&quot; whereas the eyes may be directed elsewhere (not at the screen). This property makes sounds good for alarms and for subtly communicating progress on background tasks. Sound waves also have much further reach than screen images which can be used to interface with users who are in a different part of the room than the computer. All good things that should be employed more in user interfaces.
</p>
<p>
 But these same fundamentals of sound also make it highly
 <strong>
  annoying and intrusive when played in a multi-person environment
 </strong>
 . The current use of sound files attached to Web pages typically has very little communicative value and I advise designers to take Alistair Nicholson&#39;s comments to heart and think of the poor cubicle-dwellers.
</p>
<h3>
 Splash Screens
</h3>
<p>
 <em>
  Mike Garrison (mikegarrison@alum.mit.edu) writes:
 </em>
</p>
<blockquote>
 I think that splash pages may not be one of the top-10 web mistakes, but they are probably the top useless web fashion of the past year or two.
 <p>
  Why?
 </p>
 <ol>
  <li>
   No one wants to have to access it every time, so getting to it really annoys anyone who is not a first time user.
  </li>
  <li>
   But for the first time user, it adds a useless step between them and whatever brought them to the site in the first place. So it really annoys them too.
  </li>
  <li>
   Most new users will come via a search engine anyway, so they&#39;ll probably miss the splash page.
  </li>
  <li>
   If you make it the default highest page in the server (eg. http://www.useit.com/ ) then when people try to find your home page by chopping off a URL, they get the useless splash page instead.
  </li>
  <li>
   They ruin the back button. (Your #1 new mistake.)
  </li>
 </ol>
 I especially hate it when a page I have bookmarked (say: //blah.com/ ) gets moved to some URL like //blah.com/content and my bookmark suddenly starts taking me to a splash page. Then I have to edit my bookmark so that it will take me to the real home page.
</blockquote>
<p>
 <em>
  Jakob&#39;s reply:
 </em>
 I agree: splash pages are useless and annoying. In general, every time you see a splash page, the reaction is &quot;oh no, here comes a site that will be slow and difficult to use and that doesn&#39;t respect my time.&quot;
</p>
<p>
 Splash pages are a sure sign of bad Web design.
</p>
<h3>
 Pages With No URL
</h3>
<p>
 <em>
  Andy Breeding from Compaq&#39;s Information &amp; Research Services writes:
 </em>
</p>
<blockquote>
 I would like to suggest another problem with newer sites: no URL access to specific pages. Everything is script driven and there is no way to bookmark a subsection or page within the site -- you can only go the &quot;home&quot; URL -- as that is the only URL that is ever visible.
</blockquote>
<p>
 <em>
  Jakob&#39;s reply:
 </em>
 Linking is the foundation of the Web and every object on a system needs to be accessible through a persistent URL that will keep working. URLs are needed for:
</p>
<ul>
 <li>
  bookmarks
 </li>
 <li>
  emailing to other users
 </li>
 <li>
  links from other sites
 </li>
 <li>
  search engines
 </li>
</ul>
<p>
 - all good ways of providing customer service online.
</p>
<h3>
 Mis-Named Bookmarks
</h3>
<p>
 <em>
  Nicole Burton from Sprint writes:
 </em>
</p>
<blockquote>
 I enjoyed your Top Ten New Mistakes column and wanted to add a new discovery of mine regarding opening new browser windows.
 <p>
  Hotmail, owned by MS, &quot;upgraded&quot; to open a new window when you click on a web link contained in an E-mail. But after you go to the site, if you like it and bookmark it, the bookmark shows up as &quot;Hotmail.com!&quot; The bookmark takes you the site you bookmarked, but in order to make it useful, I will have to go in and edit the bookmarks to be meaningful; a not-too-user-friendly process in Netscape 4. Didn&#39;t know if you knew this but it drives me bats. I&#39;m about ready to give Hotmail up as my E-mail service.
 </p>
</blockquote>
<p>
 <em>
  Jakob&#39;s reply:
 </em>
 Bookmarks are one of the most important navigation features on the Web, so retaining good bookmarks is essential for usability. One of the main reasons frames are still bad despite improvements in browsers is that they usually mess up the user&#39;s bookmarks.
</p>
<p>
 Many site authors still don&#39;t appreciate the need to write meaningful page titles that can serve as useful bookmarks. But it is positively obnoxious for another site to take over and interfere with the bookmarks.
</p>
<h3>
 Linking to Non-Standard Files Without Warning
</h3>
<p>
 <em>
  A reader who prefers to remain anonymous writes:
 </em>
</p>
<blockquote>
 Have you already addressed the practice of linking to .doc/.pdf files vs .php files? This is something we&#39;re asked to do with increasing frequency - &quot;just link to the .doc file, don&#39;t bother to convert it&quot; - seems to me there are some big usability issues here but others see only the savings in developer&#39;s and editor&#39;s time in not converting to .php........
</blockquote>
<p>
 <em>
  Jakob&#39;s reply:
 </em>
 You always need to provide an
 <strong>
  explicit warning when linking to files in any other format than HTML
 </strong>
 for at least two reasons:
</p>
<ul>
 <li>
  the user may not have the necessary software installed and it is very impolite to make them wait (these files are usually bigger than simple HTML) for something they can&#39;t use
 </li>
 <li>
  the non-standard file format will cause a non-standard behavior: for example, a PDF file will start up Acrobat (if the user has it installed, of course), thus giving users a nasty surprise in terms of an even longer wait and the appearance of new and different interface controls
 </li>
</ul>
<p>
 Even with a warning, non-standard files have lower usability than HTML files because the older file formats were designed for different purposes than online reading. For example, Microsoft Word is an authoring tool for writing big reports and not an optimal reader (not that Internet Explorer is optimal, but at least it&#39;s better). And PDF defines pages for printing on a laserprinter and not a scrolling canvas for reading on a computer screen.
</p>
<p>
 Sometimes, it can be an advantage that non-HTML file formats are optimized for different goals than online reading. For example, a form that needs to be printed will look much better in PDF than in HTML. And a table of numbers that people can use for running their own &quot;what-if&quot; analyses will often be better distributed as an Excel file than as an HTML file.
</p>
<h3>
 Good Overseas Response Times Require Caching
</h3>
<p>
 <em>
  <a class="out" href="http://home.pages.de/~naddy/" title="Personal home page">
   Christian Weisgerber
  </a>
  writes:
 </em>
</p>
<blockquote>
 Individual experience may vary depending on the part of the world you live at and the network infrastructure there, but I don&#39;t feel that server performance itself is the much of a problem in this context. What is? All those dynamically generated pages can&#39;t be cached. Elaborate proxy hierarchies are turned useless. Those documents must be pulled from the original server each and every time. The folks there can set up a big server park to handle the requests instantly but they can&#39;t do anything about choke points elsewhere. Many
 <strong>
  transatlantic links are
  <em>
   saturated
  </em>
 </strong>
 . It doesn&#39;t matter whether the server takes 0.01s, 0.1s, 1s, or even 10s to answer the request, when you&#39;re getting the data at 100 bytes/s. Which is just all too common.
 <p>
  I appreciate the possibilities offered by dynamically created pages, but as an author it is important to realize that you may force your users to pull data through the eye of a needle from the other side of the globe, data that might otherwise be available in a snap from a nearby proxy cache.
 </p>
</blockquote>
<p>
 <em>
  Jakob&#39;s reply:
 </em>
 Good point: even a tiny page may be horribly delayed when it crosses the big pond. Thus, there are great benefits from allowing pages to be cached on proxies and other intermediate servers.
</p>
<h3>
 Need Contact Info on All Pages
</h3>
<p>
 <em>
  Adrian Midgley, General Practitioner in Exeter, UK, writes:
 </em>
</p>
<blockquote>
 I think the author&#39;s name and a means to contact them directly should be on each page. The reason is that I recognise how hard it is to get any feedback/capture any info from a site dealing with a generally unsophisticated audience.
 <p>
  Perhaps this varies from place to place. Using the title attribute in the mailto: link helps, of course.
 </p>
</blockquote>
<p>
 <em>
  Jakob&#39;s reply:
 </em>
 It is true that it is sometimes hard to contact a website, whether to get customer service or to provide feedback. I am not sure that the solution requires a contact link on every single page, but the site definitely needs to be structured such that it is very easy to find the page that provides contact info.
</p>
<h3>
 Keyboard Efficiency
</h3>
<p>
 <em>
  David Sky from IBM Canada writes:
 </em>
</p>
<blockquote>
 Many web designs don&#39;t take into account one simple fact: Many of us use the keyboard as much as possible. Yes, this even applies to the web.
 <p>
  I encounter many log in screens on the web (the bank, e-mail, news services, etc...). A good site design lets me click on the user ID, then hit tab once to jump to the password, then one more tab to jump to the &#39;Login&#39; button. I press enter, and off I go.
 </p>
 <p>
  A poorly designed log in screen has some extra links in between each field, each requiring an extra tab. These are usually links that are central to the task at hand, such as &quot;Forgot your Password?&quot; or &quot;Special Upgrade Offer&quot;.
 </p>
 <p>
  Here is an example of a quick, easy to use log in screen: www.gtemail.net.
 </p>
 <p>
  Take a look at the following for a poorly designed log in screen: www.deja.com.
 </p>
 <p>
  There are two additional links, and a check box. You can&#39;t even see the &quot;Login&quot; button without scrolling the window down.
 </p>
</blockquote>
<p>
 <em>
  Jakob&#39;s reply:
 </em>
 Efficiency for the experienced is an important usability criterion. Any extra step is annoying when you just want to get moving. Also note that the ability to easily manipulate a page from the keyboard is crucial for
 <a class="old" href="../accessible-design-for-users-with-disabilities/index.php" title="Alertbox June 1996: Accessible Design for Users With Disabilities">
  users with disabilities
 </a>
 , particularly for users who can&#39;t use a mouse.
</p>
<h3>
 Complex Tables
</h3>
<p>
 <em>
  <a class="out" href="http://www.phphelp.com/~liam/" title="Personal home page">
   Liam Quinn
  </a>
  of the
  <a class="out" href="http://www.phphelp.com/">
   Web Design Group
  </a>
  writes:
 </em>
</p>
<blockquote>
 I&#39;d just like to add another pet peeve of mine that closely mimics Mistake #9, Slow Server Response Times.
 <p>
  Many pages these days put all their content in one huge HTML TABLE. Almost all browsers will show nothing before loading the entire TABLE, so usually I&#39;m stuck staring at a blank page for 10 seconds (assuming I haven&#39;t left, which I often do). In many cases, the page isn&#39;t completely blank--the authors kindly leave the ad banners outside of the TABLE.
 </p>
 <p>
  Notable offenders are
  <a class="out" href="http://slashdot.org/">
   slashdot
  </a>
  and
  <a class="out" href="http://freshmeat.net/">
   freshmeat
  </a>
  .
 </p>
 <p>
  I find the one huge TABLE problem to be worse than slow servers or large graphics. Yet the TABLE problem receives comparatively little attention.
 </p>
</blockquote>
<h2>
 Problems with Web Advertising
</h2>
<h3>
 Slow Ads Don&#39;t Get Seen
</h3>
<p>
 <em>
  Bill Hovingh, a Web application developer with the Presbyterian Church (USA) website, writes:
 </em>
</p>
<blockquote>
 Since you get read a fair amount, maybe people might pay attention if you pointed out that banner ads which are (a) located at the top of a page which (b) has a complicated, table-based layout and (c) are served up by extremely-high-traffic servers will (d) cause the
 <strong>
  rendering of the entire page to have to wait for the single image
 </strong>
 -- and the wait can get excruciating if condition (c) is particularly acute.
 <p>
  Sites with this problem: Wired, with their image server static.wired.com sometimes bogged down; sites with ads coming from adforce.imgis.com (only on bad &#39;Net days, though); sometimes the NYTimes.
 </p>
 <p>
  Kind of defeats the purpose if your ad takes so long to load that the user doesn&#39;t stick around for even the first page-view, doesn&#39;t it?
 </p>
</blockquote>
<p>
 <em>
  Jakob&#39;s reply:
 </em>
 Agree, agree. Slow advertising servers are definitely self-defeating. Overly complex encoding of page layouts are always a problem, but they are particularly bad if they cause the rendering to be delayed. Response times are bad enough on the Web already due to the download times. At least allow the browser to render the page progressively rather than having to wait for everything.
</p>
<h3>
 Ads That Pretend to be Dialog Boxes
</h3>
<p>
 <em>
  John G. Tyler writes:
 </em>
</p>
<blockquote>
 Perhaps you could base a future Alertbox on the insidious problem of
 <strong>
  visually deceptive advertisements
 </strong>
 in web design, where advertisers fool users by displaying what appear to be user-control dialog elements but which really are links to their voracious marketing engines. Your number 10 this week &quot;Anything that looks like Advertising&quot; is closely related. But even more troubling is advertising that entices by offering bogus controls (i.e., combo-box pulldowns, text edit/entry fields, &quot;Help&quot; buttons, etc.)
 <p>
  These advertisers should be barred from qualifying for usability design awards. Furthermore, major content providers (i.e., AOL) should establish guidelines to refuse to host content designed to mislead.
 </p>
</blockquote>
<p>
 <em>
  Jakob&#39;s reply:
 </em>
 Yes, ads that masquerade as dialog boxes or other useful user interface elements are deceptive and probably unethical. But they are also self-defeating: sure, you can trick the user into clicking on an ad in the belief that it is a dialog box, but that user&#39;s first reaction upon arriving at your site will be one of disgust - and an immediate click on the
 <em>
  Back
 </em>
 button.
</p>
<p>
 Deceptive ads can increase click-through, but they will not increase sales. On the contrary, they will incite users to dislike and distrust the advertiser. All current marketing literature promotes the concept of customer relationships and the necessity of looking at the long-time value of customers who give you repeat business. It is beyond me why anybody would think that it is a good way to start a long-term relationship by cheating the customer.
</p>
<h3>
 Banner Blockers May Kill Your Graphics
</h3>
<p>
 Not only do
 <em>
  users
 </em>
 ignore any page element that looks like an advertisement, new
 <em>
  software
 </em>
 may do so as well and
 <strong>
  prevent the browser from ever downloading a graphic that looks like an ad
 </strong>
 .
</p>
<p>
 So-called banner blockers like Siemens&#39; free
 <a class="out" href="http://www.webwasher.com/">
  WebWasher
 </a>
 are easy to install (in fact, WebWasher has great usability, even if it does require users to change one obscure setting in one of the Windows control panels). Once installed, the banner-blocking software inspects every page while it is being downloaded and simply stops the browser from ever requesting any image files (or Java, scripts, pop-up windows, or other technologies) that it thinks is an advertisement. Unfortunately, the software doesn&#39;t have perfect artificial intelligence, so it could well block legitimate content that looked sufficiently similar to current advertising formats.
</p>
<h2>
 Defining Pop-Up Windows
</h2>
<p>
 <em>
  Jay Virdee, Operations Director for the
  <a class="out" href="http://www.bhrc.co.uk/">
   British Hotel Reservation Centre
  </a>
  , writes:
 </em>
</p>
<blockquote>
 Perhaps the terminology used needs better definition. What is the difference between a new browser window and a pop-up as described in mistake #10? From a technical perspective a pop-up is a new browser window where the navigation, address bar, etc, could be disabled. The same could be done to a new browser window i.e. disable navigation bar etc.
 <p>
  We use pop-up/new browser windows extensively to provide context sensitive help and it works very well - once the user has read the help, they can click on a close button and can carry on with the rest of the transaction. We use it primarily to keep the user in the flow of a transaction. If we followed the convention of taking the user to a help page, the user will fall out of the transaction for a fairly trivial reason.
 </p>
 <p>
  If done correctly and in context I believe pop-up windows can aid smooth navigation and can be helpful to users. In our particular case, once D-HTML becomes widely supported, it could be an alternative to pop-up windows.
 </p>
</blockquote>
<p>
 <em>
  Jakob&#39;s reply:
 </em>
 To me, a pop-up is a window that is intended as a
 <em>
  supplement
 </em>
 to a primary browser window. Thus, it is typically smaller and it does not contain a &quot;full&quot; page of info. In contrast, the &quot;new browser windows&quot; I warned against as mistake #2 are intended to stand on their own and contain a full page.
</p>
<p>
 I actually agree that pop-ups can be useful; I simply observe that users often close the pop-ups without even looking at their content. Thus, I currently warn against using them for anything essential.
</p>
<p>
 Help may in fact be a great example of an appropriate pop-up: the text should definitely be short and it is best to be able to see the help without changing or obscuring the original context.
</p>
<h2>
 Do Biographies and Archives Matter for Transactional Sites?
</h2>
<p>
 <em>
  Jay Virdee, Operations Director for the
  <a class="out" href="http://www.bhrc.co.uk/">
   British Hotel Reservation Centre
  </a>
  , writes:
 </em>
</p>
<blockquote>
 Regarding mistakes #4, Lack of Biographies, and #5, Lack of Archives:
 <p>
  I&#39;m not sure whether the above two apply to a
  <em>
   majority
  </em>
  of popular websites. In most transactional websites, I don&#39;t think the above matter - the user is purpose driven e.g. buying a car. Would they need access to biographies or archives from the website - probably not. Just going through the websites that you used for &quot;Who commits the Top-10 mistakes of Web Design?&quot;, I imagine only one or two would benefit from this. Avoiding the above mistakes would however be fundamental to the design of websites carrying news or educational content.
 </p>
</blockquote>
<p>
 <em>
  Jakob&#39;s reply:
 </em>
 You just need to interpret &quot;archives&quot; slightly differently for a transactional site. It should be possible for users to revisit their transaction history and see an archive of previous things they have bought.
</p>
<p>
 Even a traditional archive of old articles may be useful sometimes if the site carries descriptive content. Say, &quot;toy of the week&quot; for a site that sells toys. You return to the site a few weeks later and want to buy a toy that you remember seeing featured.
</p>
<p>
 Biographies also apply, though to a smaller degree than on content sites. Many descriptive articles would benefit from by-lines: in the example with a featured toy, the description could be written by one or two toy reviewers who could establish a reputation among the loyal customers.
</p>
<h2>
 Artistic Sites May Differ From Useful Sites
</h2>
<p>
 <em>
  Dave Scott writes:
 </em>
</p>
<blockquote>
 I have an education in 2D graphic design, as well as several years of experience as a web designer. Currently I design web sites for the Environmental Protection Agency.
 <p>
  I agree with many of your opinions, such as frames being generally a bad thing. I also agree that web advertising no longer works. I think there are three main reasons people use the Web:
 </p>
 <ul>
  <li>
   To gather information
  </li>
  <li>
   To buy things
  </li>
  <li>
   For entertainment
  </li>
 </ul>
 Sometimes these all blend together, but I think we need to remember that there are many people exploring the web simply as entertainment. They aren&#39;t afraid of being surprised. In fact, they
 <em>
  want
 </em>
 to be surprised. They
 <em>
  want
 </em>
 fancy web pages that are as engaging to look at as they are to read.
 <p>
  Of course this is no excuse for bad design or pages with 50 megs of graphics, but I believe most web sites can get away with breaking a few of your web design rules. I expect a site for Microsoft or CNN to look different from an artist&#39;s web site or a band&#39;s web site. There is room in web design for creative, beautiful web pages. The Internet is more than just a huge library of text, connected with links. It is a visual experience.
 </p>
</blockquote>
<p>
 <em>
  Jakob&#39;s reply:
 </em>
 It is true that some sites are a form of expression in themselves and don&#39;t have any other purpose. Such sites can deviate from the usability rules without causing too much damage to their users since the users (almost by definition) won&#39;t be trying to accomplish anything.
</p>
<p>
 But any site where you want the user to be able to get something done needs to make usability a high priority. Even an entertainment site usually needs to get the users to the (entertaining) content as quickly as possible without letting the navigation or other user interface elements intrude on the experience. And the more standardized the interaction, the less users need to spend cognitive resources on it.
</p>
</section>
  

  
  <section>
    
        
    
  </section>
  
  
  
  
  

  


  <section>
    <br />
    <p class="share"><b>Share <span class="hide-for-small-down">this article</span>:</b> <a href="https://twitter.com/intent/tweet?url=http://www.nngroup.com/articles/readers-comments-on-the-new-top-10-design-mistakes/&amp;text=Readers'%20Comments%20on%20the%20new%20Top-10%20Design%20Mistakes&amp;via=nngroup">Twitter</a> | <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=http://www.nngroup.com/articles/readers-comments-on-the-new-top-10-design-mistakes/&amp;title=Readers'%20Comments%20on%20the%20new%20Top-10%20Design%20Mistakes&amp;source=Nielsen%20Norman%20Group">LinkedIn</a> | <a href="https://plus.google.com/share?url=http://www.nngroup.com/articles/readers-comments-on-the-new-top-10-design-mistakes/">Google+</a> | <a href="mailto:?subject=NN/g Article: Readers&#39; Comments on the new Top-10 Design Mistakes&amp;body=http://www.nngroup.com/articles/readers-comments-on-the-new-top-10-design-mistakes/">Email</a></p>
    <hr />
  </section>

  <footer class="l-subsection">
    
    
    

    <div class="row show-for-small-down related-content">
      
      
      

      
      
      

      
      
      

      
      
      

      
      
      
    </div>

    <div class="hide-for-small-down row related-content">
      <div class="medium-6 columns">
        
        
        

        

      </div>
      <div class="medium-6 columns">
        
        
      </div>
    </div>
  </footer>
</article>

        </div>

      

    
  </div>
  <div class="row">
    
      <div class="column small-12 show-for-small-down l-navrail announcements">
        
          
        
      </div>
    
    
  </div>

    </div>
    




<footer id="page-footer">
    <div class="row hide-for-print">
        <div class="column small-12 medium-6">
            <section class="footer-popular">
                
                <h1>Popular Topics</h1>
                <div style="float: left;  width: 15em; display: block;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/applications/index.php">Application Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/e-commerce/index.php">E-Commerce Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/eyetracking/index.php">Eyetracking </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/information-architecture/index.php">Information Architecture </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/intranets/index.php">Intranet Design </a></li>
	<li><a href="../../topic/interaction-design/index.php">Interaction Design </a></li>
</ul>
</div>

<div style="float: left;  width: 15em; display: block; list-style: none;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/mobile-and-tablet-design/index.php">Mobile and Tablet Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/credibility-persuasion/index.php">Persuasive Design</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/research-methods/index.php">Research Methods</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/user-testing/index.php">User Testing </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/web-usability/index.php">Web Usability </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/writing-web/index.php">Web Writing &amp; Content Strategy </a></li>
</ul>
</div>

<p>&nbsp;</p>

                
            </section>
        </div>

        <div class="column small-12 medium-6">
            <div class="row">
                <div class="column small-6">
                    <section class="footer-about">
                        <h1>Follow Us</h1>
                        <ul>
	<li><a href="../subscribe/index.php">Subscribe to our weekly UX newsletter</a>,&nbsp;Jakob Nielsen&#39;s Alertbox</li>
	<li>Visit Don Norman&#39;s website, <a href="http://www.jnd.org/"> www.jnd.org </a></li>
	<li>Visit Bruce Tognazzini&#39;s website, <a href="http://www.asktog.com/"> www.asktog.com </a></li>
	<li style="padding-top: 6px;"><a class="twitter-follow-button" data-show-count="false" data-size="large" href="https://twitter.com/nngroup"><img alt="Follow @nngroup on Twitter" height="20" src="https://media.nngroup.com/media/editor/2016/12/30/twitter-follow-button.png" style="width: 117px; height: 20px;" width="117" /></a></li>
	<li style="padding-top: 6px;"><img alt="" height="14" src="https://media.nngroup.com/media/editor/2016/07/26/feed-icon-14x14.png" style="vertical-align:text-top;width:14px;margin-right:.25em;" width="14" /><a href="../../feed/rss/index.php">Subscribe to our RSS feed</a></li>
</ul>

                    </section>
                </div>

                <div class="column small-6">
                    <section class="footer-about right-column">
                        <h1>About
</h1>
                        <ul class="no-bullet">
	<li><a href="../../about/contact/index.php">Contact information</a></li>
	<li><a href="../../about/index.php">About Us</a></li>
	<li><a href="../../about/why-nng/index.php">Why NN/g? </a></li>
	<li><a href="../../return-policy/index.php">Return Policy</a></li>
</ul>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="copyright column small-12">
            <p><a href="../../copyright-and-reprint-info/index.php">Copyright</a> &copy; 1998-2017 Nielsen Norman Group, All Rights Reserved.</p>
        </div>
    </div>
</footer>


    


        <script src="https://media.nngroup.com/static/vendor/fastclick/js/fastclick.min.js"></script>
        <script src="https://media.nngroup.com/static/vendor/foundation/js/foundation.min.js"></script>
        <script src="http://media.nngroup.com/static/js/app.js"></script>
        <script src="http://media.nngroup.com/static/js/scroll-to-fixed.js"></script><script>var externalLinks = document.querySelectorAll('a[href^="http"]');for (var i = externalLinks.length-1; i >= 0; i--) {externalLinks[i].addEventListener("click", function(event) { alert("External links are disabled for this test."); event.preventDefault(); }, false);}var mailLinks = document.querySelectorAll('a[href^="mailto"]');for (var i = mailLinks.length-1; i >= 0; i--) {mailLinks[i].addEventListener("click", function(event) { alert("Email links are disabled for this test."); event.preventDefault(); }, false);}var externalForms = document.querySelectorAll('form');for (var i = externalForms.length-1; i >= 0; i--) {externalForms[i].addEventListener("submit", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false); externalForms[i].addEventListener("click", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false);}</script>

        <script>
            
            $(function() {
                $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
                $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
            });
        </script>

        
<script src="https://media.nngroup.com/static/js/articles.js"></script>


        
        <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1072699960;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072699960/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
    </body>

<!-- Mirrored from www.nngroup.com/articles/readers-comments-on-the-new-top-10-design-mistakes/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:15:47 GMT -->
</html>
