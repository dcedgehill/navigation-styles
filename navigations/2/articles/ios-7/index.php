<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->





    
<!-- Mirrored from www.nngroup.com/articles/ios-7/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:13:10 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1026.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"licenseKey":"1a30ef9931","errorBeacon":"bam.nr-data.net","applicationID":"2048992","transactionName":"NFxSMRBUX0EDUkIPDA0WdhAMVkVbDV8ZBxEXUFMJB0YfRAtUQRVZIktEDAFZVHYHRVcPD01eVRE=","beacon":"bam.nr-data.net","queueTime":2,"applicationTime":580,"agent":""}</script>
        <title>iOS 7 User-Experience Appraisal</title><meta property="og:title" content="iOS 7 User-Experience Appraisal" />
  
        
        <meta name="description" content="Flat design hides calls to action, and swiping around the edges can interfere with carousels and scrolling.">
        <meta property="og:description" content="Flat design hides calls to action, and swiping around the edges can interfere with carousels and scrolling." />
        
  
        
	
        
        <meta name="keywords" content="mobile, tablet, usability, user experience, swipe ambiguity, iOS, flat design, swipe ambiguity, human-computer interaction, change">
        

        

        <meta name="robots" content="noarchive">
        <meta name="viewport" content="width=device-width">

        <link rel="search" type="application/opensearchdescription+xml" title="NN/g Search" href="../../opensearch.xml" />
        <link rel="shortcut icon" href="http://media.nngroup.com/static/img/favicon.ico" />

        <link rel="stylesheet" href="https://media.nngroup.com/static/vendor/font-awesome/css/font-awesome.min.css">

        
        <link rel="stylesheet" href="https://media.nngroup.com/static/css/7232a960f1df.css" type="text/css" />
        <link rel="stylesheet" href="http://fast.fonts.com/t/1.css?apiType=css&amp;projectid=c5846260-9def-46ec-9407-0f1349bd2d64" type="text/css">

        <script src="https://media.nngroup.com/static/vendor/modernizr/js/modernizr.min.js"></script>

        <script type="text/javascript" src="https://media.nngroup.com/static/js/3283be5def74.js"></script>
        

        

        
        
        <!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=229034,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('http://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->


<!-- Google Analytics Tracking Code for http://nngroup.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23132681-1', 'nngroup.com');
  ga('require', 'linkid', 'https://www.nngroup.com/articles/ios-7/linkid.js');
  ga('require', 'displayfeatures');

// Optimizely Universal Analytics Integration
 window.optimizely = window.optimizely || [];
 window.optimizely.push("activateUniversalAnalytics");

  ga('send', 'pageview');

</script>
<script>
/**
* Function to track a click on an outbound link in Google Analytics.
*/
function trackOutboundLink(label, url) {
    if(window.ga && ga.loaded) {
        ga('send', 'event', 'outbound', 'click', label, {'hitCallback': function(){document.location = url;}
        });
    } else {
        document.location = url;
    }
}
/**
* Function to track a click on a link in the article announcement area.
*/
$('#articleAnnouncement a').on('click', function(){
  ga('send', 'event', 'link', 'click', 'articleAnnouncement');
});
</script>

<script type="text/javascript">
 setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2421.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



<!-- Hotjar Tracking Code for http://nngroup.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:121605,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

        <script src="https://media.nngroup.com/static/js/analytics/track_outbound_link.js"></script>
        
        
        
    </head>
    <body class="location-articles articles-detail">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
    <header id="page-header">
        <div id="cart" class="shopping-cart mobile-cart show-for-small-down"></div>
        <div class="row">
            <div id="masthead" class="masthead small-12 medium-7 column">

                <!-- The button to toggles menu on small screen -->

                <h1><a href="../../index.php"><img src="http://media.nngroup.com/static/img/logo-wordmark.png" width="74" height="30"> Nielsen <span class="norman">Norman</span> Group</a></h1>
                <p class="tagline show-for-medium-up">Evidence-Based User Experience Research, Training, and Consulting</p>

                <!-- The button to toggles form search on small screen -->


                <div style="clear: right;width:  100%;height: 1px;"></div><?php define('ROOT_PATH',$_SERVER["DOCUMENT_ROOT"] . '/apps/navigation_styles/navigations/2'); ?><?php include(ROOT_PATH)."/navigation.php"; ?>

                <!-- Drop down menu on small screen -->
                <div id="hideNav" class="show-for-small-down">
                  
                  <ul class="menu-content">
                    <li><a href="../../index.php">Home</a></li>
                    <li class="has-submenu">
                      <a>Training</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../training/index.php">Overview</a></li>
                        
<li id="nav-ux-conference"><a href="../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../online-seminars/index.php"><span>Online Seminars</span></a></li>

                      </ul>
                    </li>
                    <li class="has-submenu">
                      <a>Consulting</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
  <li><a href="../../consulting/index.php">Overview</a></li>
  
  <li><a href="../../consulting/benchmarking/index.php">Benchmarking</a></li>
  
  <li><a href="../../consulting/expert-design-review/index.php">Expert Design Review</a></li>
  
  <li><a href="../../consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
  
  <li><a href="../../consulting/usability-testing/index.php">Usability Testing </a></li>
  
  <li><a href="../../consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
  
  <li><a href="../../in-house-training/index.php">In-House Training</a></li>
</ul>

                    </li>

                    <li><a href="../../reports/index.php">Reports</a></li>
                    <li><a href="../index.php">Articles</a></li>
                    <li class="has-submenu"><a>About NN/G</a><i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../about/index.php">Overview</a></li>
                        <li><a href="../../people/index.php">People</a></li>
                        <li><a href="../../about/why-nng/index.php">Why NN/g?</a></li>
                        <li><a href="../../about/contact/index.php">Contact</a></li>
                        <li><a href="../../news/index.php">News</a></li>
                        <li><a href="../../about/history/index.php">History</a></li>
                        <li><a href="../../books/index.php">Books</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                </div>

                <hr class="header-divider show-for-small-only"/>


            </div>
       </div>

        <div id="hideNav">            <nav class="primary-nav small-12 column">
                <ul>
                    <li><a class="navitem" id="nav-home" href="../../index.php">Home</a></li>
                    <li><a class="navitem" id="nav-training" href="../../training/index.php">Training</a></li>
                    <li><a class="navitem" id="nav-consulting" href="../../consulting/index.php">Consulting</a></li>
                    <li><a class="navitem" id="nav-reports" href="../../reports/index.php">Reports</a></li>
                    <li><a class="navitem" id="nav-articles" href="../index.php">Articles</a></li>
                    <li><a class="navitem" id="nav-about" href="../../about/index.php">About NN/G</a></li>
                </ul>
            </nav>
            <nav class="secondary-nav small-12 medium-12 large-12 columns">
                
                
            </nav>
        </div>
    </header>
    <div id="main" class="l-content" onclick="closeNav(event)"><span id="preventClose" style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776; Menu</span>
    
  

  

  <div class="row">
    
      
      <section class="small-12 medium-3 columns collapsable-content l-navrail articles-left-column">
        <h1 class="show-for-small-down collapse-link mobile-browse-header">Browse by Topic and Author</h1>
          <div class="row collapsable">

            
            <div class="small-6 medium-12 columns">
              <section id="article-popular-topics" style="display: none;">
                
                <h1>Topics</h1>
                <ul id="article-popular-topics">
                    
                    <li>
                        
                        <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/intranets/all/index.php">Intranets</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/user-testing/all/index.php">User Testing</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                        
                    </li>
                    
                    <li><a href="#" id="article-see-all-topics"><strong>See all topics&hellip;</strong></a></li>
                </ul>
                
              </section>
              <section id="article-all-topics" style="display: none;">
                
                <h1>
                  <span class="show-for-medium-up">All Article Topics</span>
                  <span class="show-for-small-down">All Topics</span>
                  (<a href="#" id="article-hide-all-topics">hide</a>)
                </h1>
                <ul>
                  
                  <li>
                    
                    <a href="../../topic/accessibility/all/index.php">Accessibility</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/agile/all/index.php">Agile</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/analytics-and-metrics/all/index.php">Analytics &amp; Metrics</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/applications/all/index.php">Application Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/b2b-websites/all/index.php">B2B Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/behavior-patterns/all/index.php">Behavior Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/branding/all/index.php">Branding</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/content-strategy/all/index.php">Content Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/corporate-websites/all/index.php">Corporate Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/design-patterns/all/index.php">Design Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-design-process/all/index.php">Design Process</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/email/all/index.php">Email</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/eyetracking/all/index.php">Eyetracking</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/heuristic-evaluation/all/index.php">Heuristic Evaluation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/human-computer-interaction/all/index.php">Human Computer Interaction</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-ideation/all/index.php">Ideation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/information-architecture/all/index.php">Information Architecture</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/interaction-design/all/index.php">Interaction Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/international-users/all/index.php">International Users</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/intranets/all/index.php">Intranets</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-management/all/index.php">Management</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/navigation/all/index.php">Navigation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/non-profit-websites/all/index.php">Non-Profit Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/personas/all/index.php">Personas</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/persuasive-design/all/index.php">Persuasive Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/prototyping/all/index.php">Prototyping</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/psychology-and-ux/all/index.php">Psychology and UX</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/research-methods/all/index.php">Research Methods</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/search/all/index.php">Search</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/social-media/all/index.php">Social Media</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/strategy/all/index.php">Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/user-testing/all/index.php">User Testing</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-teams/all/index.php">UX Teams</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/visual-design/all/index.php">Visual Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/writing-web/all/index.php">Writing for the Web</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/young-users/all/index.php">Young Users</a>
                    
                  </li>
                  
                </ul>
                
              </section>
            </div>
            <script>
              /* Determine which topics list will be displayed: the full or the
               * partial.  We are inlining this close to the list itself to
               * prevent rendering delays on the client
               */
              var topic_list = $.cookie("article-topics");

              var popular_topics = $('#article-popular-topics');
              var all_topics = $('#article-all-topics');

              if(topic_list == 1) {
                  popular_topics.hide();
                  all_topics.show();
              } else {
                  popular_topics.show();
                  all_topics.hide();
              }
            </script>
            

            
            <div class="small-6 medium-12 columns">
              
              <section id="article-initial-authors">
                  <h1>Author</h1>
                  <ul>
                    
                    <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                    
                    <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                    
                    <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                    
                    <li><a href="#" onClick="$('#article-initial-authors').hide(); $('#article-all-authors').show();"><strong>See all authors&hellip;</strong></a></li>
                  </ul>
              </section>
              <section id="article-all-authors" style="display: none;">
                <h1>
                  All Authors
                  (<a href="#" id="article-hide-all-authors">hide</a>)
                </h1>
                <ul>
                  
                  <li><a href="../author/raluca-budiu/index.php">Raluca Budiu</a></li>
                  
                  <li><a href="../author/patty-caya/index.php">Patty Caya</a></li>
                  
                  <li><a href="../author/susan-farrell/index.php">Susan Farrell</a></li>
                  
                  <li><a href="../author/therese-fessenden/index.php">Therese Fessenden</a></li>
                  
                  <li><a href="../author/kim-flaherty/index.php">Kim Flaherty</a></li>
                  
                  <li><a href="../author/sarah-gibbons/index.php">Sarah Gibbons</a></li>
                  
                  <li><a href="../author/aurora-harley/index.php">Aurora Harley</a></li>
                  
                  <li><a href="../author/kate-kaplan/index.php">Kate Kaplan</a></li>
                  
                  <li><a href="../author/page-laubheimer/index.php">Page Laubheimer</a></li>
                  
                  <li><a href="../author/angela-li/index.php">Angie Li</a></li>
                  
                  <li><a href="../author/hoa-loranger/index.php">Hoa Loranger</a></li>
                  
                  <li><a href="../author/kate-meyer/index.php">Kate Meyer</a></li>
                  
                  <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                  
                  <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                  
                  <li><a href="../author/kara-pernice/index.php">Kara Pernice</a></li>
                  
                  <li><a href="../author/christian-rohrer/index.php">Christian Rohrer</a></li>
                  
                  <li><a href="../author/amy-schade/index.php">Amy Schade</a></li>
                  
                  <li><a href="../author/katie-sherwin/index.php">Katie Sherwin</a></li>
                  
                  <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                  
                  <li><a href="../author/kathryn-whitenton/index.php">Kathryn Whitenton</a></li>
                  
                </ul>
              </section>
              
            </div>
            
          </div>

          
          <section class="hide-for-small-down">
            
              
              <h1>Recent Articles</h1>
              
              <ul>
                  
                  <li><a href="../seamless-cross-channel/index.php">Seamlessness in the Omnichannel User Experience</a></li>
                  
                  <li><a href="../brand-intention-interpretation/index.php">Experience Design: Bridging Brand Intention and Brand Interpretation</a></li>
                  
                  <li><a href="../eyetracking-tasks-efficient-scanning/index.php">Scanning Patterns on the Web Are Optimized for the Current Task</a></li>
                  
                  <li><a href="../flat-design-best-practices/index.php">Flat-Design Best Practices</a></li>
                  
                  <li><a href="../ux-user-stories/index.php">Accounting for UX Work with User Stories in Agile Projects</a></li>
                  
                  <li><a href="../index.php"><strong>See all articles&hellip;</strong></a></li>
              </ul>
              
            
          </section>
          

          
          
          <section class="hide-for-small-down">
            <h1>Popular Articles</h1>
            
            <ul>
                
                <li><a href="../usability-101-introduction-to-usability/index.php">Usability 101: Introduction to Usability</a></li>
                
                <li><a href="../top-10-mistakes-web-design/index.php">Top 10 Mistakes in Web Design</a></li>
                
                <li><a href="../how-users-read-on-the-web/index.php">How Users Read on the Web</a></li>
                
                <li><a href="../f-shaped-pattern-reading-web-content/index.php">F-Shaped Pattern For Reading Web Content</a></li>
                
                <li><a href="../ten-usability-heuristics/index.php">10 Usability Heuristics for User Interface Design</a></li>
                
                <li><a href="../intranet-design-2016/index.php">10 Best Intranets of 2016</a></li>
                
                <li><a href="../which-ux-research-methods/index.php">When to Use Which User-Experience Research Methods</a></li>
                
                <li><a href="../response-times-3-important-limits/index.php">Response Times: The 3 Important Limits</a></li>
                
                <li><a href="../why-you-only-need-to-test-with-5-users/index.php">Why You Only Need to Test with 5 Users</a></li>
                
                <li><a href="../page-fold-manifesto/index.php">The Fold Manifesto: Why the Page Fold Still Matters</a></li>
                
            </ul>
          </section>
          
          

          
            <section class="hide-for-small-down">
              
            </section>
          

      </section>
      

      
        <div class="small-12 medium-9 columns content-body">
          




<article class="l-subsection alertbox-article-content article-detail-bl">
  
  <header>
    <div class="l-subsection">
      <h1>iOS 7 User-Experience Appraisal</h1>
      <div class="author-meta">by
        
          
            
            <a href="../author/raluca-budiu/index.php">Raluca Budiu</a>
            
          
        on  October 12, 2013
        
      </div>

      

      
      <div class="topic-links">
        Topics:
        
          <ul>

  <li><a href="../../topic/mobile-and-tablet-design/index.php">Mobile &amp; Tablet</a></li>

</ul>
        
      </div>
      
    </div>
  </header>
  

  
  
  <section>
    <p class="lede"><strong>Summary:</strong> Flat design hides calls to action, and swiping around the edges can interfere with carousels and scrolling.</p>
    <br />
  </section>
  
  

  
  <section id="article-body"><p>iOS 7, Apple’s operating system for their tablets and mobile devices, moves away from the skeuomorphic design that characterized earlier versions of iOS. The new look is drastically different from the previous operating-system iterations and it boldly parts with some of the conventions that Apple had worked hard to establish over the past 8 years. But is the new design really better? Whether you like the new look or not, some of the new features are welcome usability improvements, whereas others are likely to cause pain.</p>

<h2>Buttons and Flat Design</h2>

<p>Let’s start with the elephant in the room: the <a href="../flat-design/index.php"><b> flat design</b></a>. What makes iOS 7 look so different from its previous incarnations is the decision to move the <a href="../browser-and-gui-chrome/index.php">chrome</a> into the backstage and leave the spotlight to the content. While this decision reflects the well-publicized mobile precept that says to prioritize content over chrome, it can also cause confusion.</p>

<p>Buttons and interface widgets, when present, need to be easily <b>distinguishable from content</b>. They need to have good affordances that invite users to action. In the absence of strong signifiers, they can get ignored, and users may find themselves lost and disoriented.</p>

<p>Flat design is by no means Apple’s invention. In fact, if anything, it seems to be quite fashionable in the mobile world, with Android and especially Windows 8 toying with it at variable extent. Back in 2012, when Windows 8 came around, we noted that <a href="../windows-8-disappointing-usability/index.php">users had difficulty distinguishing content from chrome</a>: people missed important buttons on the screen because they looked too much like plain text.</p>

<p>That being said, flat design is not necessarily hopeless. 3D is just one of the many cues that can invite users to tap. Other cues are shadows, coloring the links differently (<a href="../guidelines-for-visualizing-links/index.php">as on the web</a>), or even the placement of those links on the page.</p>

<p>So far, in Apple’s apps, these cues do a good enough job of signaling tappability. Many of the apps that we’ve seen so far are decent; there are clear differences between what can be pressed and what cannot. Some of the cues rely on users’ previous knowledge of iOS and the web.</p>

<p>For instance, when configuring an email account on the <i>Mail</i> page under <i>Settings</i>, there are several tappability cues:</p>

<ul>
	<li>Blue color in the navigation bar (&lt;<i>Mail </i>in the top left corner) – this takes advantage of the previous web knowledge (blue is a link on the web) and iOS knowledge (items in the navigation bars are tappable)</li>
	<li>Arrow in a table view (next to <i>Mail Days to Sync</i>) – this also relies on previous iOS knowledge</li>
	<li>Toggle switches that look like sliders that can be moved</li>
	<li>Red color, text centering, and position at the bottom of the page for <i>Delete Account</i> – these are all weaker cues, but they all reinforce each other</li>
</ul>

<p> </p>

<div style="text-align: center;"><img alt="Mail settings" src="http://media.nngroup.com/media/editor/2013/10/07/image001.png" style="width: 350px; height: 525px; border-width: 1px; border-style: solid;"/></div>

<div style="text-align: center;"><em>Mail page under Settings in iOS 7</em></div>

<p> </p>

<p>As this list shows, even in a single screenshot, the colors used to indicate actionable text can vary. The inconsistency makes this screen harder to use, but worse, it makes other screens harder as well, because it reduces learnability.</p>

<p>When Apple or Google or Microsoft introduce a new look for their mobile operating system, they are also responsible for <strong>design guidelines</strong> to help app creators replicate that look. Those guidelines should be robust — it shouldn’t be easy to misinterpret them in a way that makes the designs unusable. Unfortunately, flat design is prone to such misinterpretations.</p>

<p>Whether app designers will know how to create the right tap affordances in the absence of proper buttons still remains to be seen. So far it looks like some apps (for instance, <i>The New York Times</i>) are proceeding cautiously, choosing to use borders and other cues to make sure that users know where to tap.</p>

<p> </p>

<div style="text-align: center;"><em><img alt="NYTimes app" src="http://media.nngroup.com/media/editor/2013/10/07/image002.png" style="width: 350px; height: 525px; border-width: 1px; border-style: solid;"/></em></div>

<div style="text-align: center;"><em>The New York Times for iPhone</em></div>

<p> </p>

<p>Others, such as <i>Mobile Inspect </i>(an app for used-car dealers) fully embrace the lack of buttons and generate harder-to-use designs:</p>

<p> </p>

<div style="text-align: center;"><img alt="Mobile Inspect for iPhone" src="http://media.nngroup.com/media/editor/2013/10/07/image003.png" style="width: 350px; height: 523px;"/></div>

<div style="text-align: center;"><em>Mobile Inspect for iPhone</em></div>

<p> </p>

<p>In <i>Mobile Inspect</i>’s<i> </i> design, it’s hard to say what the call to action is and what the user is supposed to do on the page. Of course, if the user reads carefully all the text on the page (<a href="../how-little-do-users-read/index.php">which they usually don’t</a>), paying attention to things like verb tense (<i>Selected Category </i>versus <i>Select a Record Type and Click Continue</i>), he will figure it out eventually. But we know from countless hours of testing that people don’t like to think hard, read thoroughly, solve puzzles, and make inferences while using a mobile app: the <a href="../interaction-cost-definition/index.php">interaction cost</a> is just too high, and most mobile sessions are short and prone to interruptions.</p>

<p>In <i>Mobile Inspect</i> we can also see the new picker introduced in iOS 7:</p>

<p> </p>

<div style="text-align: center;"><img alt="iOS 7 Picker as used by Mobile Inspect" src="http://media.nngroup.com/media/editor/2013/10/07/image004.png" style="width: 350px; height: 525px; border-width: 1px; border-style: solid;"/></div>

<div style="text-align: center;"><em>The iOS 7 picker as used by Mobile Inspect for iPhone</em></div>

<p> </p>

<p>The older- iOS picker was already quite impractical — because it only used half the screen, it made a poor choice for long lists:</p>

<p> </p>

<div style="text-align: center;"><img alt="iOS6 picker in Lufthansa" src="http://media.nngroup.com/media/editor/2013/10/07/image005.png" style="width: 350px; height: 525px;"/></div>

<div style="text-align: center;"><em>The iOS 6 picker as used by Lufthansa for iPhone</em></div>

<p> </p>

<p>The new design makes the picker even smaller by using a <b> focus-plus-context visualization </b>(discussed in <a href="../../courses/hci/index.php">our course on human-computer interaction</a>): only three items are clearly visible. The others appear distorted and in lighter font; they are harder to read, although perhaps not unreadable. The new picker doesn’t have any clear usability advantages, except for the “cool” design.(Focus-plus-context visualizations usually make sense when an item’s neighbors are usually more relevant than items that are farther away in the same list. For instance, in a calendar, you could imagine that time slots around an event are more relevant to the event than time slots far away. For most dropdowns, the items in the list are equally relevant irrespective of their position in the list.)</p>

<h2>Swipe Ambiguity</h2>

<p><b>Swipe ambiguity</b> means that swiping in different places on the screen leads to different results. <a href="../ipad-usability-year-one/index.php">We first discovered swipe ambiguity</a><b> </b>when we tested iPad apps that made use of swipe for turning pages. Later on, in its Windows 8 tablets, <a href="../windows-8-disappointing-usability/index.php">Microsoft incorporated swipe ambiguity at the operating –system level</a> when it decided to use swiping (1) to expose controls, and (2) for navigation.</p>

<p>iOS 7 has also embraced swipe ambiguity. Swiping near the left, bottom, or upper edges of the screen can cause problems if the swipe is not precisely executed. Let’s talk about each of these swipe gestures.</p>

<ol>
	<li><b>Bottom edge: Control Center.</b> Swiping on the bottom edge of the screen exposes the<em> Control Center</em>, a place where some frequently used functions of the phone are grouped together. The idea of a quickly accessible <em>Control Center</em> is laudable, since people won’t need to navigate through the iPhone settings to put their phone in airplane mode or turn on the Wi-Fi. However, the placement interferes with a very common gesture on touch screens: scrolling down to see more content.

	<p>Here is an example. If, in <em>Safari</em>, the user were reading an article like the one on the left in the image below, she may accidentally trigger the control center while trying to scroll down:</p>

	<p> </p>

	<div style="text-align: center;"><em><img alt="" src="http://media.nngroup.com/media/editor/2013/10/07/bottomscroll.png" style="width: 700px; height: 412px;"/></em></div>

	<div style="text-align: center;"><em>Scrolling vertically on a page can accidental trigger the Control Center.</em></div>

	<p> </p>

	<p>Now, this functionality can be disabled in the iOS <em>Settings</em> by turning off the access to the control screen. But we know from previous testing that <a href="../the-power-of-defaults/index.php">people rarely take the time to alter a default</a>. (And a CHI 2007 study from Michigan State University showed that even tech-savvy users such as <em>Slashdot</em> readers rarely change the default interface.)</p>
	</li>
	<li><b>Top edge: <em>Spotlight Search</em> and notifications.</b> In the previous iOS versions the <em>Spotlight Search</em> (Apple’s global device search) was accessible by swiping on left edge of the first page of the device home screen. Users who had several app pages needed to navigate to the first one and then swipe on the left edge to invoke the search. In iOS 7, this interaction cost has been reduced:now users can access the <em>Spotlight Search</em> on any of the home-screen pages by swiping down somewhere below the status bar. This feature comes at the price of swipe ambiguity.
	<p>Indeed, users who want to search must be careful not to touch the top edge; if they did start their swipe gesture in the status bar, the <em>Notification Center</em> would appear instead of the <em>Spotlight Search</em>:</p>

	<p> </p>

	<div style="text-align: center;"><em><img alt="Swiping for the Spotlight Search can trigger the Notification Center" src="http://media.nngroup.com/media/editor/2013/10/08/search_.png" style="width: 700px; height: 840px;"/></em></div>

	<div style="text-align: center;"><em>Swiping for the Spotlight Search can accidentally trigger the Notification Center.</em></div>

	<p> </p>
	</li>
	<li>
	<p><b>Left edge: Safari. </b>Another example of swipe ambiguity comes from <em>Safari.</em> In the newer <em>Safari</em>, swiping on the left edge of the screen takes the user back to the previous page. (Swiping on the left also means “up” or “back” in several other Apple apps – it looks Apple is trying to compensate for the lack of a <i>Back </i>button by using swipe as back consistently.)</p>

	<p>Because of swipe as back, any webpage that contains a carousel can get into trouble in the new <em>Safari</em>: swiping the carousel back and forth (a fairly standard touch behavior) may take the users to the previous webpage instead of the previous image in the carousel.</p>

	<p>Apple itself is not exempt from the swipe-ambiguity trap —Apple.com has a big carousel on the front page:</p>

	<p> </p>

	<div style="text-align: center;"><img alt="The back as swipe interferes with carousels" src="http://media.nngroup.com/media/editor/2013/10/07/image008.png" style="width: 500px; height: 667px;"/></div>

	<div style="text-align: center;"><i>Swiping back to the first image of the carousel can accidentally lead outside the Apple site to the previous site that was visited.</i></div>

	<p> </p>
	</li>
</ol>

<p>With swiping as back Apple takes one more step into the realm of gesture-based interfaces. On touch screens, their allure is that they can take the place of interface widgets and free up screen space for content. Back in 2010 <a href="../ipad-usability-first-findings/index.php">the first iPad interfaces tried to take advantage of gestures, but the result mostly confused users</a>.  New gestures were hard to memorize and discover, and sometimes even hard to replicate.</p>

<p>With the gestures embedded in its apps, Apple took the right approach most of the time: because gestures are fairly undiscoverable, they were assigned to nonessential, power-user features (for instance, shake to undo) or there were other ways to achieve the same effect. Thus, in the <em>Mail</em> app, users could (and still can) swipe to delete, or could take a detour and use the <i>Edit</i> button. Similarly, with the new iOS 7, they can swipe to go back to the previous page or they can use other visible controls (in <em>Safari</em> — the back arrow; in <em>Mail</em>, <em>Settings</em>, <em>Contacts,</em> and <em>Notes</em> — the back control in the navigation bar). Although we normally do not advocate redundancy in interfaces, gestural interfaces are one notable exception: because <a href="http://www.jnd.org/dn.mss/gestural_interfaces_a_step_backwards_in_usability_6.php"> gestures have low affordance, discoverability, and memorability</a>, some users never use them. That can be ok for more advanced features (such as the <em>Spotlight Search</em> or the <em>Control Center</em>), but it is not ok for basic interface functions such as navigating back to the previous page. In those cases, a button can save users hours of frustration.</p>

<h2>Everything Else</h2>

<p>We mentioned two issues that we think will give trouble to users in the iOS 7. There are other features that benefit the users, and we enumerate them here.</p>

<p><b>Hiding the controls in Safari.</b> Web pages gain a few more pixels of content because the browser controls disappear shortly after the user navigates to a new page if she moves down the page, indicating that she wants to read it. Once the user starts scrolling up towards the top of the page, the controls reappear. This is good: Apple took the right route by not hiding controls altogether from the beginning; subtle cues such as scrolling up and down are rightly interpreted.</p>

<p><b>Unlimited number of files in a folder. </b> Gone are the days when people were forced to have three <em>Games</em> folders to host the many games apps they installed. Users won’t have to remember which game folder contained a specific game. Of course, now they will have to deal with finding that game within that unique folder. (But menu research shows that breadth is generally better than depth, so overall we think it’s a change for the better.)</p>

<p><b>Multitasking. </b> Before iOS 7, one of the few differences that we observed between Android and iOS users was that Android owners were a lot more concerned with battery life and task management. Now the door has opened for iOS users to experience the same worries. (Apple says that their smart multitasking will protect the battery.)</p>

<p>On the plus side, apps can now update in the background and users won’t need to wait for the data to refresh when they open their favorite app.</p>

<p><b>Global font-size control. </b> People can change the font size for all the apps that support dynamic font-size adjustment. This is a great feature, <a href="../usability-for-senior-citizens/index.php"> particularly for middle-aged or older users</a> (provided that users will get to it and that apps will allow the adjustment – again, not many of us like to fiddle with the defaults).</p>

<p><b><em>Settings</em> </b>is more streamlined and easier to navigate. <i> Do not disturb</i> no longer lives in two different places (under <em>Notifications</em> and in the main <em>Settings</em>). And we have high hopes for a redesign change for the<em> Wi-Fi</em> page that previously caused many of our users some hassle:</p>

<p> </p>

<div style="text-align: center;"><img alt="Wifi Settings" src="http://media.nngroup.com/media/editor/2013/10/07/image009.png" style="width: 650px; height: 480px;"/></div>

<div style="text-align: center;"><em>Old (left) and new (right) designs for the iOS Wi-Fi Settings.</em></div>

<p> </p>

<p>When people wanted to select a network, in iOS 6 (left screenshot above) they would often tap the blue arrow on the same line, accidentally getting to the advanced Wi-Fi<em> </em>settings. Now the arrow has been replaced with a circled <i> i</i> , an icon usually associated with help or extra information. Presumably this change will make users more aware of the double functionality present in the table rows under <i> Choose a Network</i> —tap the circled <i> i</i> to get to the advanced screen, tap elsewhere in the row to select the network.</p>

<h2>Change Is Bad</h2>

<p>In this article we focused on interaction design, not visual style. However, both are components of the user experience, together with the writing:</p>

<ul>
	<li><b>Visual style </b>is how the system <b>looks. </b></li>
	<li><b>Interaction </b> is how the system <b>feels.</b></li>
	<li><a href="../../courses/topic/content-strategy/index.php"><b>Content strategy</b></a> is how the system <b>sounds</b> (or speaks).</li>
</ul>

<p>Other sites have spilled many words over the new visual style in iOS 7. Most of these changes don’t matter for usability — like the new look or not; you’re going to use the screens the same.</p>

<p>However, one visual change does have negative usability implications: the <b> new icon designs</b>. Just as people have gotten used to looking for certain features under certain icons, all the icons sport a new look. Even just changing the background color of an icon is bad, because users would have gotten used to scanning the screen for “the red square” or something like that. Changing the actual content of an icon is even worse, because it destroys users’ ability to recognize it.</p>

<p>Apple has <b> demolished millions of hours </b> of user learning by changing the icons.</p>

<p> </p>

<div style="text-align:
      center;"><i><img alt="" src="http://media.nngroup.com/media/editor/2013/10/07/photo-icon-ios7-vs-6.png" style="width: 200px; height: 96px;"/></i></div>

<div style="text-align:
      center;"><i>If you’re used to looking for a flower to represent “Photos,” the visual scanning for this app will be drastically slowed now that the icon looks like a kaleidoscope instead.</i></div>

<p> </p>

<p>While definitely bad for usability, the icon changes are not a full-blown catastrophe because the visuals are supplemented by labels which don’t seem to have changed. So users can still find, say, the photo icon, even if they can’t recognize it anymore and thus have to spend more time looking for it.</p>

<p><a href="../fresh-vs-familiar-aggressive-redesign/index.php">Changing something people have gotten used to is bad in itself</a>. It can be worth doing anyway if the new design is so much better that the long-term usability benefits outweigh the short-term penalty of relearning. Are these new icons that much better than the old ones? Probably not. But users will learn the new icons within the first month or so of upgrading, after which usability will have been restored. Unless Apple makes another radical icon change in iOS 8.</p>

<h2>Is iOS 7 Fatally Flawed?</h2>

<p>The short answer is no. Simply because there is no such thing as fatally flawed designs: we can always learn from mistakes. What’s surprising is that we don’t learn from someone else’s mistakes: Apple ignored some of the hurdles that Microsoft experienced with flat design and swipe ambiguity in Windows 8. We still have to see whether Apple’s strong design guidelines will protect most app designers from not getting lost in the flat 2D world. Early experience with applications redesigned for iOS 7 is fairly negative: several have worse usability than their iOS 6 versions.</p>

<h3>Reference</h3>

<p>Lampe, C., Johnston, E. and Resnick, P. (2007): “Follow the Reader: Filtering Comments on Slashdot.” <em>Proceedings CHI 07 conference.</em></p>
</section>
  

  
  <section>
    
        
    
  </section>
  
  
  
  
  

  


  <section>
    <br />
    <p class="share"><b>Share <span class="hide-for-small-down">this article</span>:</b> <a href="https://twitter.com/intent/tweet?url=http://www.nngroup.com/articles/ios-7/&amp;text=iOS%207%20User-Experience%20Appraisal&amp;via=nngroup">Twitter</a> | <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=http://www.nngroup.com/articles/ios-7/&amp;title=iOS%207%20User-Experience%20Appraisal&amp;source=Nielsen%20Norman%20Group">LinkedIn</a> | <a href="https://plus.google.com/share?url=http://www.nngroup.com/articles/ios-7/">Google+</a> | <a href="mailto:?subject=NN/g Article: iOS 7 User-Experience Appraisal&amp;body=http://www.nngroup.com/articles/ios-7/">Email</a></p>
    <hr />
  </section>

  <footer class="l-subsection">
    
    
    <div class="learn-more">
      <h2>Learn More</h2>
      <p><a href="../subscribe/index.php">Subscribe to the weekly newsletter</a> to get notified about future articles.</p>
      </div>
    
    

    <div class="row show-for-small-down related-content">
      
      
      

      
      
        <div class="column small-12 topic-links">All articles about:
          <ul>
            
            <li><a href="../../topic/ios/index.php">ios</a></li>
            
            <li><a href="../../topic/mobile-and-tablet-design/index.php">Mobile &amp; Tablet</a></li>
            
          </ul>
        </div>
      
      

      
      
        <div class="column small-12">
          <h3>UX Conference Training Courses</h3>
           <ul class="no-bullet">
    
        <li><a href="../../courses/cross-channel-user-experience/index.php">Omnichannel Journeys and Customer Experience</a></li>
    
        <li><a href="../../courses/usability-mobile-websites-apps/index.php">Mobile User Experience</a></li>
    
        <li><a href="../../courses/scaling-responsive-design/index.php">Scaling User Interfaces</a></li>
    
        <li>Visual Design for Mobile and Tablet: <a href="../../courses/vis-mob-1/index.php">Day 1</a> and <a href="../../courses/vis-mob-2/index.php">Day 2</a></li>
    
</ul>
        </div>
      
      

      
      
        <div class="small-12 column">
          <h3>Research Reports</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../reports/tablets/index.php">Tablet Website and Application UX</a></li>
              
            
              
                <li><a href="../../reports/mobile-website-and-application-usability/index.php">User Experience for Mobile Applications and Websites</a></li>
              
            
              
                <li><a href="../../reports/enterprise-mobile-showcase/index.php">Mobile Intranets and Enterprise Apps</a></li>
              
            
              
                <li><a href="../../reports/wap-usability/index.php">WAP Usability</a></li>
              
            
              
                <li><a href="../../reports/ipad-app-and-website-usability/index.php">iPad App and Website Usability</a></li>
              
            
          </ul>
        </div>
      
      

      
      
        <div class="small-12 column">
          <h3>Online Seminars</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../online-seminars/every-word-count/index.php">Making Every Word Count</a></li>
              
            
              
                <li><a href="../../online-seminars/mobile-user-testing/index.php">Mobile User Testing</a></li>
              
            
          </ul>
        </div>
      
      
    </div>

    <div class="hide-for-small-down row related-content">
      <div class="medium-6 columns">
        
        
          <div>
            <h3>Articles</h3>
            <ul class="no-bullet">
              
                
                <li><a href="../ios-9-back-to-app-button/index.php">iOS 9 App Switching and the Back-to-App Button</a></li>
                
              
                
                <li><a href="../4-ios-rules-break/index.php">4 iOS Rules to Break </a></li>
                
              
                
                <li><a href="../mobile-content/index.php">Reading Content on Mobile Devices</a></li>
                
              
                
                <li><a href="../mobile-behavior-india/index.php">Mobile User Behavior in India</a></li>
                
              
                
                <li><a href="../mobile-navigation-patterns/index.php">Basic Patterns for Mobile Navigation: A Primer</a></li>
                
              
            </ul>
          </div>
        
        

        
          
          <div>
            <h3>Research Reports</h3>
            <ul class="no-bullet">
              
                
                  <li><a href="../../reports/tablets/index.php">Tablet Website and Application UX</a></li>
                
              
                
                  <li><a href="../../reports/mobile-website-and-application-usability/index.php">User Experience for Mobile Applications and Websites</a></li>
                
              
                
                  <li><a href="../../reports/enterprise-mobile-showcase/index.php">Mobile Intranets and Enterprise Apps</a></li>
                
              
                
                  <li><a href="../../reports/wap-usability/index.php">WAP Usability</a></li>
                
              
                
                  <li><a href="../../reports/ipad-app-and-website-usability/index.php">iPad App and Website Usability</a></li>
                
              
            </ul>
          </div>
          
        

      </div>
      <div class="medium-6 columns">
        
          
          <div>
            <h3>UX Conference Training Courses</h3>
             <ul class="no-bullet">
    
        <li><a href="../../courses/cross-channel-user-experience/index.php">Omnichannel Journeys and Customer Experience</a></li>
    
        <li><a href="../../courses/usability-mobile-websites-apps/index.php">Mobile User Experience</a></li>
    
        <li><a href="../../courses/scaling-responsive-design/index.php">Scaling User Interfaces</a></li>
    
        <li>Visual Design for Mobile and Tablet: <a href="../../courses/vis-mob-1/index.php">Day 1</a> and <a href="../../courses/vis-mob-2/index.php">Day 2</a></li>
    
</ul>
          </div>
          
        
        
          
          <div>
            <h3>Online Seminars</h3>
            <ul class="no-bullet">
            
                <li><a href="../../online-seminars/every-word-count/index.php">Making Every Word Count</a></li>
            
                <li><a href="../../online-seminars/mobile-user-testing/index.php">Mobile User Testing</a></li>
            
            </ul>
          </div>
          
        
      </div>
    </div>
  </footer>
</article>

        </div>

      

    
  </div>
  <div class="row">
    
      <div class="column small-12 show-for-small-down l-navrail announcements">
        
          
        
      </div>
    
    
  </div>

    </div>
    




<footer id="page-footer">
    <div class="row hide-for-print">
        <div class="column small-12 medium-6">
            <section class="footer-popular">
                
                <h1>Popular Topics</h1>
                <div style="float: left;  width: 15em; display: block;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/applications/index.php">Application Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/e-commerce/index.php">E-Commerce Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/eyetracking/index.php">Eyetracking </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/information-architecture/index.php">Information Architecture </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/intranets/index.php">Intranet Design </a></li>
	<li><a href="../../topic/interaction-design/index.php">Interaction Design </a></li>
</ul>
</div>

<div style="float: left;  width: 15em; display: block; list-style: none;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/mobile-and-tablet-design/index.php">Mobile and Tablet Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/credibility-persuasion/index.php">Persuasive Design</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/research-methods/index.php">Research Methods</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/user-testing/index.php">User Testing </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/web-usability/index.php">Web Usability </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/writing-web/index.php">Web Writing &amp; Content Strategy </a></li>
</ul>
</div>

<p>&nbsp;</p>

                
            </section>
        </div>

        <div class="column small-12 medium-6">
            <div class="row">
                <div class="column small-6">
                    <section class="footer-about">
                        <h1>Follow Us</h1>
                        <ul>
	<li><a href="../subscribe/index.php">Subscribe to our weekly UX newsletter</a>,&nbsp;Jakob Nielsen&#39;s Alertbox</li>
	<li>Visit Don Norman&#39;s website, <a href="http://www.jnd.org/"> www.jnd.org </a></li>
	<li>Visit Bruce Tognazzini&#39;s website, <a href="http://www.asktog.com/"> www.asktog.com </a></li>
	<li style="padding-top: 6px;"><a class="twitter-follow-button" data-show-count="false" data-size="large" href="https://twitter.com/nngroup"><img alt="Follow @nngroup on Twitter" height="20" src="https://media.nngroup.com/media/editor/2016/12/30/twitter-follow-button.png" style="width: 117px; height: 20px;" width="117" /></a></li>
	<li style="padding-top: 6px;"><img alt="" height="14" src="https://media.nngroup.com/media/editor/2016/07/26/feed-icon-14x14.png" style="vertical-align:text-top;width:14px;margin-right:.25em;" width="14" /><a href="../../feed/rss/index.php">Subscribe to our RSS feed</a></li>
</ul>

                    </section>
                </div>

                <div class="column small-6">
                    <section class="footer-about right-column">
                        <h1>About
</h1>
                        <ul class="no-bullet">
	<li><a href="../../about/contact/index.php">Contact information</a></li>
	<li><a href="../../about/index.php">About Us</a></li>
	<li><a href="../../about/why-nng/index.php">Why NN/g? </a></li>
	<li><a href="../../return-policy/index.php">Return Policy</a></li>
</ul>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="copyright column small-12">
            <p><a href="../../copyright-and-reprint-info/index.php">Copyright</a> &copy; 1998-2017 Nielsen Norman Group, All Rights Reserved.</p>
        </div>
    </div>
</footer>


    


        <script src="https://media.nngroup.com/static/vendor/fastclick/js/fastclick.min.js"></script>
        <script src="https://media.nngroup.com/static/vendor/foundation/js/foundation.min.js"></script>
        <script src="http://media.nngroup.com/static/js/app.js"></script>
        <script src="http://media.nngroup.com/static/js/scroll-to-fixed.js"></script><script>var externalLinks = document.querySelectorAll('a[href^="http"]');for (var i = externalLinks.length-1; i >= 0; i--) {externalLinks[i].addEventListener("click", function(event) { alert("External links are disabled for this test."); event.preventDefault(); }, false);}var mailLinks = document.querySelectorAll('a[href^="mailto"]');for (var i = mailLinks.length-1; i >= 0; i--) {mailLinks[i].addEventListener("click", function(event) { alert("Email links are disabled for this test."); event.preventDefault(); }, false);}var externalForms = document.querySelectorAll('form');for (var i = externalForms.length-1; i >= 0; i--) {externalForms[i].addEventListener("submit", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false); externalForms[i].addEventListener("click", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false);}</script>

        <script>
            
            $(function() {
                $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
                $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
            });
        </script>

        
<script src="https://media.nngroup.com/static/js/articles.js"></script>


        
        <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1072699960;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072699960/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
    </body>

<!-- Mirrored from www.nngroup.com/articles/ios-7/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:13:10 GMT -->
</html>
