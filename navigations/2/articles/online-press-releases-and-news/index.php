<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->





    
<!-- Mirrored from www.nngroup.com/articles/online-press-releases-and-news/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:13:13 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1026.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"beacon":"bam.nr-data.net","queueTime":8,"applicationTime":812,"agent":"","licenseKey":"1a30ef9931","errorBeacon":"bam.nr-data.net","applicationID":"2048992","transactionName":"NFxSMRBUX0EDUkIPDA0WdhAMVkVbDV8ZBxEXUFMJB0YfRAtUQRVZIktEDAFZVHYHRVcPD01eVRE="}</script>
        <title>Press Releases and News Items Online: Usability Guidelines</title><meta property="og:title" content="Press Releases and News Items Online: Usability Guidelines" />
  
        
        <meta name="description" content="21 design guidelines for presenting press releases and news listing on corporate websites. Based on usability studies with journalists.">
        <meta property="og:description" content="21 design guidelines for presenting press releases and news listing on corporate websites. Based on usability studies with journalists." />
        
  
        
	
        
        <meta name="keywords" content="PR, press releases, public relations, press release usability, PR design, press release design, PR UX, press releases user experience, press releases UX, journalists">
        

        

        <meta name="robots" content="noarchive">
        <meta name="viewport" content="width=device-width">

        <link rel="search" type="application/opensearchdescription+xml" title="NN/g Search" href="../../opensearch.xml" />
        <link rel="shortcut icon" href="http://media.nngroup.com/static/img/favicon.ico" />

        <link rel="stylesheet" href="https://media.nngroup.com/static/vendor/font-awesome/css/font-awesome.min.css">

        
        <link rel="stylesheet" href="https://media.nngroup.com/static/css/7232a960f1df.css" type="text/css" />
        <link rel="stylesheet" href="http://fast.fonts.com/t/1.css?apiType=css&amp;projectid=c5846260-9def-46ec-9407-0f1349bd2d64" type="text/css">

        <script src="https://media.nngroup.com/static/vendor/modernizr/js/modernizr.min.js"></script>

        <script type="text/javascript" src="https://media.nngroup.com/static/js/3283be5def74.js"></script>
        

        

        
        
        <!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=229034,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('http://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->


<!-- Google Analytics Tracking Code for http://nngroup.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23132681-1', 'nngroup.com');
  ga('require', 'linkid', 'https://www.nngroup.com/articles/online-press-releases-and-news/linkid.js');
  ga('require', 'displayfeatures');

// Optimizely Universal Analytics Integration
 window.optimizely = window.optimizely || [];
 window.optimizely.push("activateUniversalAnalytics");

  ga('send', 'pageview');

</script>
<script>
/**
* Function to track a click on an outbound link in Google Analytics.
*/
function trackOutboundLink(label, url) {
    if(window.ga && ga.loaded) {
        ga('send', 'event', 'outbound', 'click', label, {'hitCallback': function(){document.location = url;}
        });
    } else {
        document.location = url;
    }
}
/**
* Function to track a click on a link in the article announcement area.
*/
$('#articleAnnouncement a').on('click', function(){
  ga('send', 'event', 'link', 'click', 'articleAnnouncement');
});
</script>

<script type="text/javascript">
 setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2421.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



<!-- Hotjar Tracking Code for http://nngroup.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:121605,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

        <script src="https://media.nngroup.com/static/js/analytics/track_outbound_link.js"></script>
        
        
        
    </head>
    <body class="location-articles articles-detail">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
    <header id="page-header">
        <div id="cart" class="shopping-cart mobile-cart show-for-small-down"></div>
        <div class="row">
            <div id="masthead" class="masthead small-12 medium-7 column">

                <!-- The button to toggles menu on small screen -->

                <h1><a href="../../index.php"><img src="http://media.nngroup.com/static/img/logo-wordmark.png" width="74" height="30"> Nielsen <span class="norman">Norman</span> Group</a></h1>
                <p class="tagline show-for-medium-up">Evidence-Based User Experience Research, Training, and Consulting</p>

                <!-- The button to toggles form search on small screen -->


                <div style="clear: right;width:  100%;height: 1px;"></div><?php define('ROOT_PATH',$_SERVER["DOCUMENT_ROOT"] . '/apps/navigation_styles/navigations/2'); ?><?php include(ROOT_PATH)."/navigation.php"; ?>

                <!-- Drop down menu on small screen -->
                <div id="hideNav" class="show-for-small-down">
                  
                  <ul class="menu-content">
                    <li><a href="../../index.php">Home</a></li>
                    <li class="has-submenu">
                      <a>Training</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../training/index.php">Overview</a></li>
                        
<li id="nav-ux-conference"><a href="../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../online-seminars/index.php"><span>Online Seminars</span></a></li>

                      </ul>
                    </li>
                    <li class="has-submenu">
                      <a>Consulting</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
  <li><a href="../../consulting/index.php">Overview</a></li>
  
  <li><a href="../../consulting/benchmarking/index.php">Benchmarking</a></li>
  
  <li><a href="../../consulting/expert-design-review/index.php">Expert Design Review</a></li>
  
  <li><a href="../../consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
  
  <li><a href="../../consulting/usability-testing/index.php">Usability Testing </a></li>
  
  <li><a href="../../consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
  
  <li><a href="../../in-house-training/index.php">In-House Training</a></li>
</ul>

                    </li>

                    <li><a href="../../reports/index.php">Reports</a></li>
                    <li><a href="../index.php">Articles</a></li>
                    <li class="has-submenu"><a>About NN/G</a><i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../about/index.php">Overview</a></li>
                        <li><a href="../../people/index.php">People</a></li>
                        <li><a href="../../about/why-nng/index.php">Why NN/g?</a></li>
                        <li><a href="../../about/contact/index.php">Contact</a></li>
                        <li><a href="../../news/index.php">News</a></li>
                        <li><a href="../../about/history/index.php">History</a></li>
                        <li><a href="../../books/index.php">Books</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                </div>

                <hr class="header-divider show-for-small-only"/>


            </div>
       </div>

        <div id="hideNav">            <nav class="primary-nav small-12 column">
                <ul>
                    <li><a class="navitem" id="nav-home" href="../../index.php">Home</a></li>
                    <li><a class="navitem" id="nav-training" href="../../training/index.php">Training</a></li>
                    <li><a class="navitem" id="nav-consulting" href="../../consulting/index.php">Consulting</a></li>
                    <li><a class="navitem" id="nav-reports" href="../../reports/index.php">Reports</a></li>
                    <li><a class="navitem" id="nav-articles" href="../index.php">Articles</a></li>
                    <li><a class="navitem" id="nav-about" href="../../about/index.php">About NN/G</a></li>
                </ul>
            </nav>
            <nav class="secondary-nav small-12 medium-12 large-12 columns">
                
                
            </nav>
        </div>
    </header>
    <div id="main" class="l-content" onclick="closeNav(event)"><span id="preventClose" style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776; Menu</span>
    
  

  

  <div class="row">
    
      
      <section class="small-12 medium-3 columns collapsable-content l-navrail articles-left-column">
        <h1 class="show-for-small-down collapse-link mobile-browse-header">Browse by Topic and Author</h1>
          <div class="row collapsable">

            
            <div class="small-6 medium-12 columns">
              <section id="article-popular-topics" style="display: none;">
                
                <h1>Topics</h1>
                <ul id="article-popular-topics">
                    
                    <li>
                        
                        <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/intranets/all/index.php">Intranets</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/user-testing/all/index.php">User Testing</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                        
                    </li>
                    
                    <li><a href="#" id="article-see-all-topics"><strong>See all topics&hellip;</strong></a></li>
                </ul>
                
              </section>
              <section id="article-all-topics" style="display: none;">
                
                <h1>
                  <span class="show-for-medium-up">All Article Topics</span>
                  <span class="show-for-small-down">All Topics</span>
                  (<a href="#" id="article-hide-all-topics">hide</a>)
                </h1>
                <ul>
                  
                  <li>
                    
                    <a href="../../topic/accessibility/all/index.php">Accessibility</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/agile/all/index.php">Agile</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/analytics-and-metrics/all/index.php">Analytics &amp; Metrics</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/applications/all/index.php">Application Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/b2b-websites/all/index.php">B2B Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/behavior-patterns/all/index.php">Behavior Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/branding/all/index.php">Branding</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/content-strategy/all/index.php">Content Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/corporate-websites/all/index.php">Corporate Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/design-patterns/all/index.php">Design Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-design-process/all/index.php">Design Process</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/email/all/index.php">Email</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/eyetracking/all/index.php">Eyetracking</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/heuristic-evaluation/all/index.php">Heuristic Evaluation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/human-computer-interaction/all/index.php">Human Computer Interaction</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-ideation/all/index.php">Ideation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/information-architecture/all/index.php">Information Architecture</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/interaction-design/all/index.php">Interaction Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/international-users/all/index.php">International Users</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/intranets/all/index.php">Intranets</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-management/all/index.php">Management</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/navigation/all/index.php">Navigation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/non-profit-websites/all/index.php">Non-Profit Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/personas/all/index.php">Personas</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/persuasive-design/all/index.php">Persuasive Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/prototyping/all/index.php">Prototyping</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/psychology-and-ux/all/index.php">Psychology and UX</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/research-methods/all/index.php">Research Methods</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/search/all/index.php">Search</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/social-media/all/index.php">Social Media</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/strategy/all/index.php">Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/user-testing/all/index.php">User Testing</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-teams/all/index.php">UX Teams</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/visual-design/all/index.php">Visual Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/writing-web/all/index.php">Writing for the Web</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/young-users/all/index.php">Young Users</a>
                    
                  </li>
                  
                </ul>
                
              </section>
            </div>
            <script>
              /* Determine which topics list will be displayed: the full or the
               * partial.  We are inlining this close to the list itself to
               * prevent rendering delays on the client
               */
              var topic_list = $.cookie("article-topics");

              var popular_topics = $('#article-popular-topics');
              var all_topics = $('#article-all-topics');

              if(topic_list == 1) {
                  popular_topics.hide();
                  all_topics.show();
              } else {
                  popular_topics.show();
                  all_topics.hide();
              }
            </script>
            

            
            <div class="small-6 medium-12 columns">
              
              <section id="article-initial-authors">
                  <h1>Author</h1>
                  <ul>
                    
                    <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                    
                    <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                    
                    <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                    
                    <li><a href="#" onClick="$('#article-initial-authors').hide(); $('#article-all-authors').show();"><strong>See all authors&hellip;</strong></a></li>
                  </ul>
              </section>
              <section id="article-all-authors" style="display: none;">
                <h1>
                  All Authors
                  (<a href="#" id="article-hide-all-authors">hide</a>)
                </h1>
                <ul>
                  
                  <li><a href="../author/raluca-budiu/index.php">Raluca Budiu</a></li>
                  
                  <li><a href="../author/patty-caya/index.php">Patty Caya</a></li>
                  
                  <li><a href="../author/susan-farrell/index.php">Susan Farrell</a></li>
                  
                  <li><a href="../author/therese-fessenden/index.php">Therese Fessenden</a></li>
                  
                  <li><a href="../author/kim-flaherty/index.php">Kim Flaherty</a></li>
                  
                  <li><a href="../author/sarah-gibbons/index.php">Sarah Gibbons</a></li>
                  
                  <li><a href="../author/aurora-harley/index.php">Aurora Harley</a></li>
                  
                  <li><a href="../author/kate-kaplan/index.php">Kate Kaplan</a></li>
                  
                  <li><a href="../author/page-laubheimer/index.php">Page Laubheimer</a></li>
                  
                  <li><a href="../author/angela-li/index.php">Angie Li</a></li>
                  
                  <li><a href="../author/hoa-loranger/index.php">Hoa Loranger</a></li>
                  
                  <li><a href="../author/kate-meyer/index.php">Kate Meyer</a></li>
                  
                  <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                  
                  <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                  
                  <li><a href="../author/kara-pernice/index.php">Kara Pernice</a></li>
                  
                  <li><a href="../author/christian-rohrer/index.php">Christian Rohrer</a></li>
                  
                  <li><a href="../author/amy-schade/index.php">Amy Schade</a></li>
                  
                  <li><a href="../author/katie-sherwin/index.php">Katie Sherwin</a></li>
                  
                  <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                  
                  <li><a href="../author/kathryn-whitenton/index.php">Kathryn Whitenton</a></li>
                  
                </ul>
              </section>
              
            </div>
            
          </div>

          
          <section class="hide-for-small-down">
            
              
              <h1>Recent Articles</h1>
              
              <ul>
                  
                  <li><a href="../seamless-cross-channel/index.php">Seamlessness in the Omnichannel User Experience</a></li>
                  
                  <li><a href="../brand-intention-interpretation/index.php">Experience Design: Bridging Brand Intention and Brand Interpretation</a></li>
                  
                  <li><a href="../eyetracking-tasks-efficient-scanning/index.php">Scanning Patterns on the Web Are Optimized for the Current Task</a></li>
                  
                  <li><a href="../flat-design-best-practices/index.php">Flat-Design Best Practices</a></li>
                  
                  <li><a href="../ux-user-stories/index.php">Accounting for UX Work with User Stories in Agile Projects</a></li>
                  
                  <li><a href="../index.php"><strong>See all articles&hellip;</strong></a></li>
              </ul>
              
            
          </section>
          

          
          
          <section class="hide-for-small-down">
            <h1>Popular Articles</h1>
            
            <ul>
                
                <li><a href="../usability-101-introduction-to-usability/index.php">Usability 101: Introduction to Usability</a></li>
                
                <li><a href="../top-10-mistakes-web-design/index.php">Top 10 Mistakes in Web Design</a></li>
                
                <li><a href="../how-users-read-on-the-web/index.php">How Users Read on the Web</a></li>
                
                <li><a href="../f-shaped-pattern-reading-web-content/index.php">F-Shaped Pattern For Reading Web Content</a></li>
                
                <li><a href="../ten-usability-heuristics/index.php">10 Usability Heuristics for User Interface Design</a></li>
                
                <li><a href="../intranet-design-2016/index.php">10 Best Intranets of 2016</a></li>
                
                <li><a href="../which-ux-research-methods/index.php">When to Use Which User-Experience Research Methods</a></li>
                
                <li><a href="../response-times-3-important-limits/index.php">Response Times: The 3 Important Limits</a></li>
                
                <li><a href="../why-you-only-need-to-test-with-5-users/index.php">Why You Only Need to Test with 5 Users</a></li>
                
                <li><a href="../page-fold-manifesto/index.php">The Fold Manifesto: Why the Page Fold Still Matters</a></li>
                
            </ul>
          </section>
          
          

          
            <section class="hide-for-small-down">
              
            </section>
          

      </section>
      

      
        <div class="small-12 medium-9 columns content-body">
          




<article class="l-subsection alertbox-article-content article-detail-bl">
  
  <header>
    <div class="l-subsection">
      <h1>Press Releases and News Items Online: Usability Guidelines</h1>
      <div class="author-meta">by
        
          
            
            <a href="../author/hoa-loranger/index.php">Hoa Loranger</a>
            
            and
            
            <a href="../author/kara-pernice/index.php">Kara Pernice</a>
            
          
        on  January 19, 2009
        
      </div>

      

      
      <div class="topic-links">
        Topics:
        
          <ul>

  <li><a href="../../topic/content-strategy/index.php">Content Strategy</a></li>

  <li><a href="../../topic/corporate-websites/index.php">Corporate Websites</a></li>

  <li><a href="../../topic/writing-web/index.php">Writing for the Web</a></li>

</ul>
        
      </div>
      
    </div>
  </header>
  

  
  
  <section>
    <p class="lede"><strong>Summary:</strong> 21 design guidelines for presenting press releases and news listing on corporate websites. Based on usability studies with journalists.</p>
    <br />
  </section>
  
  

  
  <section id="article-body"><p style="background: yellow;">
	This article is excerpted from the report on <a href="../../reports/pr-websites/index.php">PR on Websites</a>. (The full report is available as a <strong>free download</strong>.)<br/>
	See also: <a href="../press-area-usability/index.php">summary of this research on how journalists use PR sites</a>.</p>
<h3>
	Guideline #20. Post press releases on your site.</h3>
<p>
	Journalists said they want, look for, and use press releases on corporate websites. Including them facilitates press coverage.</p>
<p>
	The main reasons journalist said they use press releases are to:</p>
<ul>
	<li>
		Learn about a product or event.</li>
	<li>
		Get the company’s spin or position on a topic.</li>
	<li>
		Find a contact name specific to a topic.</li>
</ul>
<p>
	When looking for Hemscott press releases, a journalist was surprised when the company website didn’t seem to have any.</p>
<p>
	<em>“Is it possible that this is the one company on the planet that doesn’t publish press releases? At this point, I’d call up the PR person and scream at them –  [but] I’d tell them it is not their personal fault.”</em></p>
<p>
	The journalists we studied had a lot to say about press releases:</p>
<ul>
	<li>
		<em>“I’d go check out the news releases now, to see what’s new.”</em></li>
	<li>
		<em>“I understand everyone’s hype about their product. The hype helps me understand the positioning of a product. It helps me understand how they are out selling to customers. [I ask] is this true, not true. ... I take everything with the grain of salt, until someone has tested it.”</em></li>
	<li>
		<em>“I almost never read them. Only if there is an event that I have to know the company’s take on it. If I need to know their angle, I will read it, or numbers from a deal or acquisition for stock. Otherwise I almost never use them.”</em></li>
	<li>
		<em>“I might look at the quick facts and see some of the info. About assets, funds, this is info. I would print out. I may not use it in the story, but just so I understand the company a little better. Sometimes I even get into the news center archive and see what they have. It can be very interesting especially if they changed their outlook but didn’t change the text.”</em></li>
	<li>
		<em>“I believe it’s what the company wants me to think. I am very skeptical about that. It’s useful to see what the company wants me to know. A lot of times the facts are right, but they place in a positive context for the company. It’s helpful to see how company wants us to see things.”</em></li>
	<li>
		<em>“Oh, it’s just boring press releases. You can never trust a press release.”</em></li>
	<li>
		<em>“You can’t say you don’t trust press releases, but it is not like the whole truth. But looking at the page you see how they look at it. There is some truth in it, but I still have to talk to people there. Check it out. ... There’s probably some truth in it. Why else would they spend forces saying it? Someone might check up on it. If it is not true, it would be much easier to keep quiet. As a journalist, of course, you are always suspicious.”</em></li>
	<li>
		<em>“I use them as a backgrounder. For instance, I’m doing a story now where the company sent [a release] to me. I would never take the quotes from the release. As far as getting random press releases, I never read them.”</em></li>
	<li>
		<em>“I do not read press releases, especially here where they have links to news clips available – I’d read that instead of their own. I trust other journalists more.”</em></li>
	<li>
		<em>“I guess with a lot of sites, they have really old press releases. ...”</em></li>
	<li>
		<em>“Seems like if there is more information about what a company is coming up with, it would be helpful.”</em></li>
	<li>
		<em>“The more controversial, the more I have to check it. Generally I would not just take the information. You never really write an article without talking to some people. Check the facts.”</em></li>
</ul>
<h3>
	Guideline #21. Show the most recent press releases on the main press page.</h3>
<p>
	When journalists navigate to the press section of your site, they expect to see press releases immediately. Don’t make users search for them or drill down any further to get the most recent press releases.<br/>
	 <br/>
	<img alt="Screenshot of Hyatt press room" src="http://media.nngroup.com/media/editor/2013/06/02/Hyatt-press-room.png" style="width: 800px; height: 451px;"/><br/>
	<em>The Hyatt site baffled a journalist because the main “Press Room” page didn’t show any press releases. She carefully studied the screen before realizing she had to click on the links on the left side of the screen to search for releases. This additional step creates confusion because people don’t expect it.</em></p>
<h3>
	Guideline #22. Feature links to news articles about your company.</h3>
<p>
	Journalists review news articles from multiple sources when covering companies. While press releases are helpful, journalists still want to verify the facts and get various perspectives. Typically, they’ll leave your site to do this.</p>
<p>
	However, in our study, when sites featured links to news items about the company, journalists often followed those links. Journalists may look upon your organization more favorably if you make their jobs easier. By offering resources, you ensure that journalists see the content while enhancing your credibility.</p>
<p>
	News listings should have the following items:</p>
<ul>
	<li>
		Title of article</li>
	<li>
		A blurb (summary of the article)</li>
	<li>
		Date of publication</li>
	<li>
		Name of publication</li>
</ul>
<p>
	Don’t paste the article on your site; linking to the actual publication gives you more credibility because it directs readers to the source. In Nielsen Norman Group’s <a href="../../reports/ecommerce-user-experience/index.php">study of e-commerce sites</a>, the research found that links to outside sources are a good way to generate trust with customers.</p>
<p>
	A journalist on the SeeItFirst site was suspicious of the news featured on the website because the titles were not clickable and there was no easy way to get the articles.<br/>
	<em>“Hey. Well that stinks. The articles should be here. I feel like I’ve been tricked. I would like to have seen the articles. Either they didn’t want to take the time to load the articles, or maybe there were some parts of the articles they didn’t want to talk about.”</em></p>
<p>
	<img alt="Screenshot of SeeItFirst news area" src="http://media.nngroup.com/media/editor/2013/06/02/SeeItFirst-news-listing.png" style="width: 766px; height: 575px;"/><br/>
	<em>The SeeItFirst site listed news, which is good. But the headings did not link to the actual news article, which is bad.</em></p>
<p>
	Tellme.com had very thorough coverage of news items. The site had logos for publications that featured the news stories, giving journalists quick information about existing coverage. The article title, date, and author were all right there in the list, which also helped journalists determine when the company was last written about, and by whom.</p>
<p>
	The best part was that the site linked to the actual articles in the various news publications, so journalists were not reading some abbreviated version that the PR department cut and annotated. Instead, the link went right to the source, which people appreciated. It also seemed to give Tellme a little more credibility, because they were willing to link to the actual article and let it speak for itself.</p>
<p>
	<br/>
	<img alt="Tellme news as tested in original research" src="http://media.nngroup.com/media/editor/2013/06/02/tellme-as-tested-news-items.png" style="width: 764px; height: 577px;"/><br/>
	<em>The Tellme website presented news items well. Each news article was properly cited and linked out to the source’s website.</em></p>
<p>
	<img alt="Tellme redesign after our usability study" src="http://media.nngroup.com/media/editor/2013/06/02/tellme-revised-news.png" style="width: 332px; height: 807px;"/></p>
<p>
	<em>The updated Tellme website no longer features the logos, but still links to the website of the original news source.</em><br/>
	<img alt="Peek.com news listing" src="http://media.nngroup.com/media/editor/2013/06/02/peek-news-listing.png" style="width: 458px; height: 819px;"/><br/>
	<em>The Peek website links news directly to the source. This is effective.</em></p>
<h3>
	Guideline #23. Make a distinction between press releases and news.</h3>
<p>
	Many companies make the mistake of labeling press releases as “News Releases” or “News.” Do not confuse corporate releases with external news items. Press releases are put out by the company and news is what’s written by others about the company.</p>
<p>
	When press releases were labeled “News Releases,” journalists usually, at least initially, thought they were actually pieces of news from other sources, not releases from the company. Links to press releases and lists of press releases should be labeled “Press Releases” not “news.”</p>
<p>
	When using the GlaxoSmithKline site, one user was looking right at the list of press releases, but because they were labeled “Latest News,” she didn’t realize it. When asked to find the latest press releases, she said:<br/>
	<em>“I looked in ‘Media Room.’ I guess ‘Media Room,’ what they are saying is, ‘This is the stuff we have had written about us.’”</em><br/>
	Then, looking at the “Latest News” list, she said:<br/>
	<em>“These are out-tears, or whatever you want to call them. When I think of ‘Media Room,’ I think it would have all the information you might possible need, for the media. But it doesn’t. This must be their press releases.”</em></p>
<p>
	Then she opened a release, and said:<br/>
	“<em>Oh, I guess these are press releases. Is this written by someone or... Oh I guess this is it. Even while looking at this, I thought these were things that were written about them. I saw the dates, and looks like these are headlines ... from like a newspaper or something like that. I would probably put the words ‘press release’ on it somewhere. It also says ‘Latest News’ [which] makes me feel like it is coming from the news. I would redo this whole page. I wouldn’t call it ‘Media Room.’ I wouldn’t put ‘Latest News.’ I would definitely spell it out and say ‘Press Releases.’”</em></p>
<p>
	<img alt="GSK latest news area" src="http://media.nngroup.com/media/editor/2013/06/02/gsk-latest-news.png" style="width: 800px; height: 572px;"/><br/>
	<em>The label name “Latest News” on the GlaxoSmithKline site made a user think they were news items, not press releases.</em></p>
<p>
	<img alt="BMW corporate news" src="http://media.nngroup.com/media/editor/2013/06/02/bmw-corporate-news.png" style="width: 584px; height: 502px;"/><br/>
	<em>The BMW Group website has a section called “Corporate News &gt; Current articles.” Site visitors can mistake this for news articles, especially when the content doesn’t look like press releases. In this example, the contact information, typically found at the bottom of the release, is missing.</em></p>
<p>
	<img alt="ACCOR news area" src="http://media.nngroup.com/media/editor/2013/06/02/accor-news-area.png" style="width: 777px; height: 591px;"/><br/>
	<em>Accor appropriately makes a distinction between “News” and “Press.” Incorporating news items under “Press” would be even better. It reduces the effort required to locate related topics.</em></p>
<h3>
	Guideline #24. Have a designated page (or section of the site) for press releases. Label this area “Press Releases.”</h3>
<p>
	At times, users were not sure if they were looking at a list of press releases, briefs, or news items. Make it easy for journalists to quickly tell when they are looking at a list of press releases. The “Press Releases” section should include multiple releases.</p>
<p>
	On the Benetton site, there were two press-related areas, one entitled “Press Releases,” and one entitled “Press Resources.” The names were too similar for users to differentiate between them.<br/>
	<em>“Seems to be kind of vague. I wonder if they could have used better terminology or combined with press releases...”</em></p>
<p>
	When using the Hemscott site, users were not sure if “RNS Announcements” were the same as press releases.<br/>
	<br/>
	<em><img alt="Hemscott RNS announcements" src="http://media.nngroup.com/media/editor/2013/06/02/Hemscott-rns-announcements.png" style="width: 800px; height: 577px;"/>The Hemscott site’s list of “RNS Announcements.” Users were not sure if these were the same as press releases.</em></p>
<p>
	<em><img alt="Merck press releases as tested in 2000" src="http://media.nngroup.com/media/editor/2013/06/02/merck-press-releases-2000.png" style="width: 791px; height: 558px;"/>On the Merck site, the “Press Releases” link opened up to a single old press release and didn’t offer an easy way to see other releases.</em></p>
<p>
	On the Merck site, the “Press Releases” link opened up to a single press release. Although it’s possible this was the most recent release, it doesn’t seem likely since the same one appeared from Dec. 10, 2000, to (at least) March 10, 2001. Also, this persistent press release had no navigator linking to other press releases.</p>
<ul>
	<li>
		<em>“You get one press release. This is a problem. The first thing we see is one press release from July 10 of last year. That pisses me off. This tells me they hadn’t thought about this or it’s some glitch.”</em></li>
	<li>
		<em>“There is one press release here. I find that sort of amazing. Tells me they are not being particularly open. ... Most companies will put up a PR page with links to all releases in the last year. I would think they would have hot-linked headlines. ... The better companies archive their press releases too.”</em></li>
</ul>
<p>
	<img alt="Merck financial news as tested in 2000" src="http://media.nngroup.com/media/editor/2013/06/02/merck-financial.png" style="width: 790px; height: 557px;"/></p>
<p>
	<em>The path to find other press releases on the Merck website was difficult to find. Seemingly the only way to get other press releases was to go to “About Merck &gt; Financial &gt; Press Releases.”</em></p>
<p>
	<img alt="Merck redesigned screenshiot" src="http://media.nngroup.com/media/editor/2013/06/02/merck-redesigned.png" style="width: 770px; height: 909px;"/><br/>
	<em>The updated Merck site loses points because it replaced the term “Press Releases” with the more ambiguous “News.” Who is this news from, the company or outsiders?</em><br/>
	 </p>
<p>
	<img alt="HotelExective.com news screenshot" src="http://media.nngroup.com/media/editor/2013/06/02/HotelExective-news.png" style="width: 800px; height: 904px;"/></p>
<p>
	<em>A user used the HotelExective.com website to do research for her story. But the site disappointed her when the article listings didn’t reveal dates. “The drawback to this one is it doesn’t show dates – I'm sure this is not for today. I don't know where the new news is. Some of these I read last week.”</em></p>
<p>
	<img alt="Hemscortt press releases" src="http://media.nngroup.com/media/editor/2013/06/02/hemscorr-investor-centre.png" style="width: 800px; height: 561px;"/><br/>
	<em>The Hemscott site’s press releases were ordered well, so that the most recent release was first in the list. The site also provided a short summary of each release, in addition to the title.</em></p>
<h3>
	Guideline #25. Feature dates on all press releases and articles.</h3>
<p>
	Dates are important for time-sensitive content. Readers need to know how current the information is before making the commitment to read or cite it.</p>
<h3>
	Guideline #26. Organize the list of press releases so that the most recent one is first in the list.</h3>
<p>
	People expect press releases to be listed chronologically, from most recent to oldest. Follow this standard to reduce confusion.</p>
<p>
	The Hemscott site ordered the press releases so that the most recent one appeared first in the list. This made it easy for journalists to get the latest company news. The site also provided a short summary of each release, in addition to the title, which was helpful.</p>
<p>
	<img alt="Quest press releases screenshot" src="http://media.nngroup.com/media/editor/2013/06/02/qwest-sorting.png" style="width: 772px; height: 820px;"/><br/>
	<em>The Qwest site ordered press releases appropriately, with the most recent first.</em></p>
<h3>
	Guideline #27. Feature dates in an international format.</h3>
<p>
	Consider international users when presenting data, especially when presenting dates of press releases and news releases.  Write out the month to avoid confusion: 9/1 means September 1 to Americans but January 9 to Europeans. Several times during our study, releases and news items appeared stale to people who didn’t remember or understand how European and U.S. date formats differ.</p>
<p>
	On the BMW site, the most recent press release was dated “9.1.01.” A U.S. journalist, testing the site in January, thought this was a September press release because of the date format.<br/>
	<em>“The last press release is from September. That’s not good.”</em></p>
<p>
	Similarly, on the Tellme site, one of the Danish journalists saw the most recent press release dated: 10-03-2000, which was October 3, 2000. He said:<br/>
	<em>“The latest is March this year. That’s a long time ago. Nothing has been happening for over six months? They don’t list them first. They list them weird. Oh wait, do you write month first? Then day? It’s a European thing.”</em></p>
<p>
	<img alt="Tellme date format" src="http://media.nngroup.com/media/editor/2013/06/02/tellme-date-format.png" style="width: 765px; height: 571px;"/><br/>
	<em>A list of press releases on the Tellme site. The date format confused international users.</em></p>
<p>
	<img alt="Tellme redesigned" src="http://media.nngroup.com/media/editor/2013/06/02/tellme-redesigned-dates.png" style="width: 800px; height: 991px;"/><br/>
	<em>The updated Tellme site does the right thing by listing dates in an international format.</em></p>
<h3>
	28. If your organization has many press releases, make it easy for people to find them by date and topic. Assign topic categories to each release.</h3>
<p>
	When faced with a long list of press releases, it became tedious for journalists to scroll through them. They often wanted to read the latest releases, so the default sorting should present the most recent releases first. But sometimes users knew they were looking for a particular type of information and had trouble finding it when they could not search the releases or reorder them by topic category.</p>
<p>
	The Tyco site offered a search function for releases, and the ability to sort releases by year. They also provided a feature that let users show only press releases in a particular category, such as “Electronics” or “Healthcare.” This was very useful.</p>
<p>
	The main issue with the implementation, however, was that the category drop-down list was too far from the search and year selection areas and could be easily missed. Also, once a user selected a topic, there was no status indication of the topic and the sorting. Even the drop-down menu itself defaulted back to “Please Select.”</p>
<p>
	<img alt="Tyco press releases" src="http://media.nngroup.com/media/editor/2013/06/02/tyco-press-releases.png" style="width: 800px; height: 505px;"/></p>
<p>
	<em>The Tyco site allowed users to find press releases by category and search, which was very nice. The main issue with the implementation was that the category dropdown list was too far from the search and year selection areas. Highly related items should be placed in close proximity to each other, in order to reinforce their relationship.</em></p>
<p>
	<img alt="Merck screenshot from 2008" src="http://media.nngroup.com/media/editor/2013/06/02/merck-press-releases-separation.png" style="width: 769px; height: 679px;"/><br/>
	<em>The Merck website separates press releases by major categories. This helps people who know the exact topic they want to explore. The downside is that press releases might be difficult to find if the topic falls under multiple categories. In this case, being able to search press releases would be nice. Also, press releases should be called “Press Releases,” not “News”.</em></p>
<p>
	<img alt="Quest press releases listign" src="http://media.nngroup.com/media/editor/2013/06/02/quest-releases-listing.png" style="width: 766px; height: 1087px;"/></p>
<p>
	<em>The Qwest website organizes press releases by quarter. This is only helpful for people who know the specific timeframe for what they are looking. Otherwise finding arrived press releases by search and topic is more helpful. Also, the list is long and difficult to scan.</em></p>
<p>
	<img alt="Starwood screenshot" src="http://media.nngroup.com/media/editor/2013/06/02/starwood-keywords.png" style="width: 800px; height: 538px;"/></p>
<p>
	<em>The Starwood website has a keyword search for press releases, making it convenient for journalists to find a specific release or topic. The site also features printer-friendly versions of press releases, which users want.</em></p>
<h3>
	Guideline #29. Press releases and news articles should be labeled with concise titles and summaries that describe the content well. Titles should start with a meaningful keyword.</h3>
<p>
	Users complained that they often encounter titles that are cryptic or unhelpful. Our studies show that people often avoid clicking on links that are vague. They won’t waste their effort on areas (or links) that are difficult to understand.</p>
<p>
	Avoid superfluous details. Short phrases are more inviting than long winded strands of text. Even if you think your titles were well written, still have a brief summary for each release. Some users read them first, before deciding whether to click.</p>
<p>
	Think about how your press release listing will appear on the page. If the titles start with the same word, say, your company name, then it’s difficult to scan the list. We don’t recommend having repetitive words for links because they look too similar and require extra effort to differentiate. Leave out the company name on the press release title because people already know they are on your website.</p>
<p>
	<img alt="AA gaze plot" src="http://media.nngroup.com/media/editor/2013/06/02/aa-pr-gaze-plot.png" style="width: 800px; height: 571px;"/><em>This gaze plot shows that this person mainly focuses on the first few words of titles. Starting each title with the right keywords is essential in attracting and keeping interest.</em></p>
<p>
	<img alt="JetBlue screenshot" src="http://media.nngroup.com/media/editor/2013/06/02/jetblue-pr-listing.png" style="width: 800px; height: 694px;"/></p>
<p>
	<em>The redundancy of these press listings make them difficult to scan. Starting each title with “JetBlue Airways” increases the cognitive effort required to differentiate between items.</em></p>
<p>
	<img alt="VW news area" src="http://media.nngroup.com/media/editor/2013/06/02/vw-news-area.png" style="width: 800px; height: 503px;"/><br/>
	<em>Unlike many other websites, the Volkswagen site has concise titles and blurbs for press releases.</em></p>
<h3>
	Guideline #30. Post new press releases (and other time-sensitive content) regularly and quickly.</h3>
<p>
	The importance of broadcast journalism has risen steadily in recent years and is likely to increase further, as print outlets struggle with economic realities.</p>
<p>
	Broadcast journalists have significantly different needs than their counterparts in print. For example, broadcasters need and expect statements and press releases posted to a website within minutes, rather than hours.</p>
<p>
	If a company went a few months between press releases, journalists wondered what was happening with the company. Outdated content diminishes credibility.</p>
<p>
	A journalist on the Hemscott site noticed that the latest press release was four months old, making her wonder about the legitimacy of the company.</p>
<p>
	<em>“Now, I think this is a very weird company because the latest press release is the second of July. It’s now November.”</em></p>
<p>
	Another journalist on the Hilton website noticed that the site delayed posting their corporate earnings conference calls. She expected them to be available on the site within a few hours.</p>
<p>
	<em>“They don't have their conference calls yet. They usually get that up pretty fast. They usually have these up within hours when they release their earnings.”</em></p>
<h3>
	Guideline #31. Press releases should be interesting and insightful.</h3>
<p>
	Don’t put out press releases unless you have something interesting to say. A press release is a good way to get attention and press coverage. However, this only works if your content is truly newsworthy. Many press releases are simply noise. Use press releases wisely. It’s a common misconception that more press releases are better. People don’t like being inundated with useless e-mail or information. If they get a constant stream of mundane releases from you, they are less likely to notice when something big actually happens. Don’t desensitize your audience. Hire good people who can write insightful and creative stories.</p>
<h3>
	Guideline #32. Archive all past press releases on the site, with release dates for each item.</h3>
<p>
	Journalists said they sometimes looked for press releases from past years. Archiving press releases on the site enables journalists to access content they need and shows that your organization is conscientious.</p>
<p>
	A journalist criticized a company for not making old press releases available online:</p>
<p>
	<em>“I don’t know why. Not in business? Nothing important? Nothing here on the arrival of the CEO.”</em></p>
<h3>
	Guideline #33. Make it simple to search press releases and archived press releases independent of the rest of the site.</h3>
<p>
	Journalists looked for ways to search only the press releases. In the “Search” section of this report, we recommend that a site’s search feature query the entire site, not just the section the user is in. However, searching press releases is an exception. If a journalist is looking for a specific release, or a specific topic in a press release, they should be able to search just the releases. The presentation of this press releases search should be very clear, and communicate that it is searching just the releases.</p>
<p>
	When using the Hemscott site, a user looked for a way to search press releases.<br/>
	<em>“I notice they don’t have a search button for the press releases, which would be a helpful thing. In fact they don’t have a search for the entire site.”</em></p>
<h3>
	Guideline #34. Design press releases for the Web. In the actual press release, clearly present the title, date, and press contact information.</h3>
<p>
	There are specific components and formatting guidelines that press releases typically follow. While these are reasonable and effective for print releases, this traditional formatting can be improved for Web presentation. The table below summarizes some recommendations for adapting press releases to the Web.</p>
<table align="center" style="border-style: solid; border-color: #cccccc; border-width: 1px; border-collapse: collapse; ">
	<caption>
		Usability Guidelines for Press Release Formatting</caption>
	<tbody>
		<tr>
			<th style=" border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left; background-color:  #003366; color: #ffffff; width: 20%;">
				 </th>
			<th style=" border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left; background-color:  #003366; color: #ffffff; width: 40%;">
				Print Press Releases: Traditional Format</th>
			<th style=" border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left; background-color:  #003366; color: #ffffff; width: 40%;">
				Web Press Releases: Recommended Format</th>
		</tr>
		<tr>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left; vertical-align: top;">
				Length</td>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left; vertical-align: top;">
				No longer than two 8 ½ × 11 pages</td>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left; vertical-align: top;">
				No longer than two scrollable pages</td>
		</tr>
		<tr>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left; vertical-align: top;">
				Text</td>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left; vertical-align: top;">
				Double-spaced</td>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left; vertical-align: top;">
				Small paragraphs. Headings and bold text to call out the main sections</td>
		</tr>
		<tr>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left; vertical-align: top;">
				Typeface</td>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left; vertical-align: top;">
				Common serif font (E.g., Times New Roman)</td>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left; vertical-align: top;">
				Common sans-serif font (e.g, Verdana)</td>
		</tr>
		<tr>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left; vertical-align: top;">
				Top of the first page</td>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left; vertical-align: top;">
				In all capital letters and underlined: “<u>FOR IMMEDIATE RELEASE</u>”</td>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left; vertical-align: top;">
				Date of release</td>
		</tr>
		<tr>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left; vertical-align: top;">
				Contact</td>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left; vertical-align: top;">
				<p>
					At the <strong>top</strong> of each page:</p>
				<ul>
					<li>
						Contact name</li>
					<li>
						Mailing address</li>
					<li>
						Personal e-mail address</li>
					<li>
						Telephone number</li>
					<li>
						Fax number</li>
				</ul>
			</td>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left; vertical-align: top;">
				<p>
					At the <strong>top and bottom </strong>of the page:</p>
				<ul>
					<li>
						Contact name</li>
					<li>
						Company name</li>
					<li>
						Personal e-mail address</li>
					<li>
						Telephone number</li>
					<li>
						(mayby) Fax number</li>
					<li>
						International contacts (names, addresses, telephone and fax numbers)</li>
					<li>
						Option for a printer-friendly format</li>
				</ul>
			</td>
		</tr>
		<tr>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left; vertical-align: top;">
				Page numbering</td>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left; vertical-align: top;">
				Yes</td>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left; vertical-align: top;">
				No</td>
		</tr>
		<tr>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left; vertical-align: top;">
				At the end</td>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left; vertical-align: top;">
				<ul>
					<li>
						Boilerplate (basic info about company)</li>
					<li>
						“End”, “-30-”, or “ ###” to indicate the end</li>
				</ul>
			</td>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left; vertical-align: top;">
				<ul>
					<li>
						Boilerplate (basic info about company)</li>
					<li>
						Links to related releases or news items</li>
					<li>
						Printer-friendly option</li>
				</ul>
			</td>
		</tr>
	</tbody>
</table>
<p>
	After finding a press release on the Hemscott site, a user said:<br/>
	<em>“I do have the PR contact now. This is often what I do – look for the press release and get the contact from there. ... It is really, really annoying to read a press release that has no date and no contacts. ... They could even link to the press contacts.”</em></p>
<p>
	<em><img alt="Sample press release screenshot" src="http://media.nngroup.com/media/editor/2013/06/02/gbc-press-release-example.png" style="width: 800px; height: 578px;"/>Journalists expect to find PR contact information at the end of press releases, as in this example.</em></p>
<p>
	The ING website does a good job of presenting press releases, as the following example demonstrates. The writing style is straightforward and concise, and the release contains the general components journalists need:</p>
<ul>
	<li>
		The bold title clearly marked at the top, so users knew that the link they clicked brought them to the right document.</li>
	<li>
		The date of the release is clearly shown.</li>
	<li>
		The press contact is clearly stated at the bottom and top of the release.</li>
	<li>
		The press contact information included the name of the firm, so journalists knew whether they were calling the company’s in-house PR department or an outsourced one.</li>
	<li>
		The press contact information included a person’s name, a telephone number and an e-mail address with the person’s actual name.</li>
</ul>
<p>
	<em><img alt="" src="http://media.nngroup.com/media/editor/2013/06/02/ing-press-example.png" style="width: 800px; height: 650px;"/>The ING website has well-written press releases that include general elements journalists need.</em></p>
<h3>
	Guideline #35.  Keep press releases concise.</h3>
<p>
	Make it easy for journalists to do their jobs. Keep press releases under two scrollable pages. In general, journalists said they disliked lengthy press releases, especially when they are poorly written. </p>
<p>
	A journalist was looking through press releases, which tended to be quite long, on the Qwest site. One was four pages long. She said:<br/>
	<em>“Press releases seem to go on and on and on and on. It is very difficult to find something in there.”</em></p>
<p>
	<img alt="Prudential screenshot" src="http://media.nngroup.com/media/editor/2013/06/02/prudential-press-example.png" style="width: 800px; height: 1431px;"/></p>
<p>
	<em>This press release on the Prudential website uses effective formatting techniques. The use of boldface, indents, and short sentences communicates the message well.</em></p>
<p>
	<img alt="Gaze plot from eyetracking" src="http://media.nngroup.com/media/editor/2013/06/02/continental-eyetracking-perss-release.png" style="width: 800px; height: 501px;"/></p>
<p>
	<em>This user is mainly fixated on the title and the first few paragraphs of the release, even though it’s relatively short.</em></p>
<p>
	<img alt="Gaze plot from eyetracking" src="http://media.nngroup.com/media/editor/2013/06/02/united-gaze-plot-press-release.png" style="width: 800px; height: 1213px;"/><br/>
	<em>Long press releases often don’t get read. Having a strong opening paragraph is essential. In this example, the journalist looked at the first paragraph to determine whether the release had what she needed and ignored the rest.</em></p>
<p>
	<img alt="Gaze pklot from eyetracking" src="http://media.nngroup.com/media/editor/2013/06/02/bloomberg-gaze-plot.png" style="width: 800px; height: 1660px;"/></p>
<p>
	<em>Long articles are not entirely bad, and sometimes welcomed. Our studies show that people tend to only scroll long pages if they feel that the information his highly related. People are more apt to scroll if there are sub-headings, so they can focus on the topics that matter. This journalist reviewed the first few paragraphs, saw that the article was relevant, and then quickly scrolled the rest of the page until she found something useful.</em></p>
<h3>
	Guideline #36. Be wary of using marketing hype, clichés and jargon. They usually don’t communicate anything useful and can damage the credibility of your organization.</h3>
<p>
	Marketese and blah-blah text are prevalent on many sites. Jargon, industry buzzwords and acronyms are repellant. At best, they have a strong tendency to confuse users. At worst, they make users mistrust the site.</p>
<p>
	The words you choose should be understood by most audiences. Minimize the marketing hype, especially in the corporate sections of your site. People who visit corporate sections are on a fact-finding mission. They are smart and can recognize smoke and mirrors (deceptive explanations or descriptions).</p>
<p>
	Don’t make people question your intentions or wonder what you’re saying. People want press releases that are concise and thought-provoking. Similarly, flowery or hyped language interferes with people’s goals, because it convolutes the message and tends not to communicate anything useful.</p>
<p>
	Avoid repeating the shorthand you use to communicate with the people in your organization. People who come to your site may have varying levels of knowledge about your industry or organization.</p>
<p>
	It’s a strong indication that that the site needs rewriting when people make fun of the content or roll their eyes in disgust. Keep your credibility intact by using a factual tone and leaving out the fluff. Don’t annoy people with your cleverness.</p>
<p>
	On the BMW site, the writing was very marketing-oriented. One user laughed when he read the words “benchmark for innovative mobility.”</p>
<p>
	<em>“That’s funny to me as someone who is sensitive to being marketed to by media in our lives. I have become somewhat immune to these pitches. The more someone testifies to the product the more I think it’s a lemon. BMW has a reputation for being a superlative car. That’s enough for me. But, they should still do it because maybe most people aren’t like me. ... Always good to assert the greatness of your product. It’s just the language that is used makes me laugh. ... I look at it as a clue as to how marketing wants to tip people. Usually it says more about who they think their market is.”</em></p>
<p>
	Another user read a section described as “mobility is life” on the BMW site.</p>
<p>
	<em>“Connected Drive. What does that mean? This is what I call ‘too clever by half.’“</em></p>
<p>
	Then she stuck fingers in her mouth as if to gag herself.</p>
<p>
	On the Philip Morris site, one user clicked a link labeled “Working to Make a Difference” and laughed.</p>
<p>
	<em>“This is really funny. I am very skeptical of this. Interesting that they are not doing anything with the ailments of smoking. ... Something like this just looks like they have a certain tax need to fill and they are giving away to get their quota. Funny.”</em></p>
<p>
	<img alt="Good press release example" src="http://media.nngroup.com/media/editor/2013/06/02/bofa-good-pr.png" style="width: 765px; height: 1254px;"/></p>
<p>
	<em>The Bank of America site has good press releases. The release in this example uses language that most audiences can understand. Note the long title and unusual word wrapping in the “Most Recent Press Release” area; people tend to ignore areas that are busy and difficult to scan.</em></p>
<h3>
	Guideline #37. If your press releases cover highly technical information, provide an easy way to get definitions or simple explanations.</h3>
<p>
	Some companies are very technical and write their press releases for journalists who are knowledgeable in the domain. But other journalists, who write about more mainstream topics, might also want to use the site and read press releases. Press releases can contain technical information, of course, but adding links to definitions and descriptions will help those journalists who don’t work in your particular field, and who might otherwise become lost.</p>
<p>
	If you must have proprietary words, technical terms or arcane category names, always explain their meaning. People tend to skip over meaningless words. If you want people to be interested in what you have to say, people must know what you are saying. People will not waste their time on content that appears unfruitful.</p>
<p>
	Remember, once press releases are published on the website, many people, not just journalists, read them. It’s common for people to use search engines to research a topic and be directed to a press release. Sometimes people don’t even know they’re reading a press release. In fact, many non-journalists don’t make a distinction between press releases and any other content on the site. To readers, information is information.</p>
<p>
	When looking at some complicated releases on the Tyco Electronics site, a user said:</p>
<p>
	<em>“I’d have to have a context for my brain before I got into these press releases. That’s why I went back, to see what industries they are in and the various products that they sell. Now I can come back to the press releases, and if I see something that says ‘New FASTON Receptacles,’ I have no idea what that means [but] that’s okay, once I have a context. I still don’t know what it means, but at least I have a context of what Tyco does. So I click on this. ... A little graphic like that is always helpful to journalists because, as you probably know, we are really real generalists. We don’t really know about anything we’re writing about. ... The more readable the information is for us, the better we can translate it for our readers. Now some of this stuff you can’t make really readable, like ‘New ways to eliminate costly insulation of electrical terminals.’ But this seems to be written fine.<br/>
	“So I might say in my article something like, for example, ‘Tyco recently found ways to...uhm, to...’ [sigh] How would I put this in English? ‘...to reduce the cost of insulating electrical terminals’ and then, ‘which are used in such things such as dishwashers, clothes washers, and other household appliances.’”</em></p>
<p>
	<br/>
	<img alt="Screenshot with complex terms" src="http://media.nngroup.com/media/editor/2013/06/02/press-releases-complex-terms.png" style="width: 450px; height: 324px;"/></p>
<p>
	<em>A list of press releases on the Tyco Electronics homepage. The listings contain technical terms that people don’t understand.</em></p>
<h3>
	Guideline #38. Press releases should emphasize how the topic affects consumers or the general public.</h3>
<p>
	After reading a press release, many journalists said that though they understood the information the release presented, they needed to research how the new product or change would affect their readers.</p>
<p>
	In their stories, they don’t want to just present facts from releases, they want to frame the information in such a way that readers will care about it. It would help if the releases got them started with some context information.</p>
<p>
	One user, after reading a press release on the Tyco Electronics site, said:<br/>
	<em>“Then what I would do, because it doesn’t seem to tell me what I’d want to know for my readers – I am assuming my readers are general readers as opposed to specialists in the industry – I call one of the PR people who is listed on the other page, I would ask them, say, ‘Well, tell me how this is going to affect a consumer. I’ve gotten the general information – it’s going to reduce the costs here – so tell me how this is going to affect somebody.’ Which they don’t necessarily have to put in the releases. Well, they could have put it in the release, but they didn’t necessarily have to.”</em></p>
<h3>
	Guideline #39. At the end of each release, include a short section with “boilerplate” information about the company, or offer a clear link to such information.</h3>
<p>
	When journalists could not easily find basic corporate information via the website, many looked in the press releases for “boilerplate” information about the company, which traditionally appears at the end of a press release. On the Web, whether to include this information in each release is somewhat debatable, as it clutters up all the releases. But, if someone found a release by searching on the Web or on the corporate site, having the basic corporate information would be beneficial to the journalist and the company. Alternatively, you could house this information on a separate page and link each release to it.</p>
<p>
	The boilerplate usually includes:</p>
<ul>
	<li>
		A few sentences that describe your company (overview)</li>
	<li>
		The company’s website address</li>
	<li>
		Telephone number</li>
</ul>
<p>
	One journalist said:<br/>
	<em>“While we are in the newsroom, something all journalists are looking for is boilerplate, which is normally stripped out of press releases [online] because it would be repeated. ... It would be nice to find the one paragraph.”</em></p>
<p>
	<img alt="Screenshot of boilerplate content at bottom of press release" src="http://media.nngroup.com/media/editor/2013/06/02/deutsche-bank-boilerplate.png" style="width: 562px; height: 494px;"/><br/>
	<em>The “boilerplate” at the end of each Deutsche Bank release helped people better understand the company.</em></p>
<h3>
	Guideline #40. At the end of a release, include links to related information.</h3>
<p>
	Offering related information at the right time shows you understand people’s needs. When done well, it makes your site more interesting and encourages page views.</p>
<p>
	<img alt=".S. Food and Drug Administration sample press release screenshot" src="http://media.nngroup.com/media/editor/2013/06/02/FDA-news.png" style="width: 800px; height: 1035px;"/><br/>
	<em>The U.S. Food and Drug Administration site does a good job of featuring links to related information on press releases.</em></p>
<p>
	(See separate guidelines for <a href="../email-press-releases-journalists/index.php">emailing press releases to journalists</a>.)</p>
<p style="background: yellow;">
	This article was excerpted from the report on <a href="../../reports/pr-websites/index.php">PR on Websites</a>. (The full report is available as a <strong>free download</strong>.)<br/>
	See also: <a href="../press-area-usability/index.php">summary of this research on how journalists use PR sites</a>.</p>
</section>
  

  
  <section>
    
        
    
  </section>
  
  
  
  
  

  


  <section>
    <br />
    <p class="share"><b>Share <span class="hide-for-small-down">this article</span>:</b> <a href="https://twitter.com/intent/tweet?url=http://www.nngroup.com/articles/online-press-releases-and-news/&amp;text=Press%20Releases%20and%20News%20Items%20Online:%20Usability%20Guidelines&amp;via=nngroup">Twitter</a> | <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=http://www.nngroup.com/articles/online-press-releases-and-news/&amp;title=Press%20Releases%20and%20News%20Items%20Online:%20Usability%20Guidelines&amp;source=Nielsen%20Norman%20Group">LinkedIn</a> | <a href="https://plus.google.com/share?url=http://www.nngroup.com/articles/online-press-releases-and-news/">Google+</a> | <a href="mailto:?subject=NN/g Article: Press Releases and News Items Online: Usability Guidelines&amp;body=http://www.nngroup.com/articles/online-press-releases-and-news/">Email</a></p>
    <hr />
  </section>

  <footer class="l-subsection">
    
    
    <div class="learn-more">
      <h2>Learn More</h2>
      <p><a href="../subscribe/index.php">Subscribe to the weekly newsletter</a> to get notified about future articles.</p>
      </div>
    
    

    <div class="row show-for-small-down related-content">
      
      
      

      
      
        <div class="column small-12 topic-links">All articles about:
          <ul>
            
            <li><a href="../../topic/content-strategy/index.php">Content Strategy</a></li>
            
            <li><a href="../../topic/corporate-websites/index.php">Corporate Websites</a></li>
            
            <li><a href="../../topic/writing-web/index.php">Writing for the Web</a></li>
            
          </ul>
        </div>
      
      

      
      
        <div class="column small-12">
          <h3>UX Conference Training Courses</h3>
           <ul class="no-bullet">
    
        <li><a href="../../courses/cross-channel-user-experience/index.php">Omnichannel Journeys and Customer Experience</a></li>
    
        <li><a href="../../courses/stakeholder-ux-approval/index.php">Engaging Stakeholders to Build Buy-In</a></li>
    
        <li><a href="../../courses/content-strategy/index.php">Content Strategy</a></li>
    
        <li><a href="../../courses/writing/index.php">Writing Compelling Digital Copy</a></li>
    
</ul>
        </div>
      
      

      
      
        <div class="small-12 column">
          <h3>Research Reports</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../reports/how-people-read-web-eyetracking-evidence/index.php">How People Read on the Web: The Eyetracking Evidence</a></li>
              
            
              
                <li><a href="../../reports/investor-relations-ir-corporate-websites/index.php">Investor Relations (IR) on Corporate Websites</a></li>
              
            
              
                <li><a href="../../reports/strategic-design-faqs/index.php">Strategic Design for Frequently Asked Questions</a></li>
              
            
              
                <li><a href="../../reports/social-media-user-experience/index.php">Social Media User Experience</a></li>
              
            
              
                <li><a href="../../reports/pr-websites/index.php">PR on Websites</a></li>
              
            
          </ul>
        </div>
      
      

      
      
        <div class="small-12 column">
          <h3>Online Seminars</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../online-seminars/improving-intranet-content/index.php">Improving Intranet Content</a></li>
              
            
              
                <li><a href="../../online-seminars/every-word-count/index.php">Making Every Word Count</a></li>
              
            
          </ul>
        </div>
      
      
    </div>

    <div class="hide-for-small-down row related-content">
      <div class="medium-6 columns">
        
        
          <div>
            <h3>Articles</h3>
            <ul class="no-bullet">
              
                
                <li><a href="../faq-ux-deconstructed/index.php">An FAQ’s User Experience Deconstructed</a></li>
                
              
                
                <li><a href="../faqs-deliver-value/index.php">FAQs Still Deliver Great Value</a></li>
                
              
                
                <li><a href="../intranet-content-authors/index.php">3 Ways to Inspire Intranet Content Authors</a></li>
                
              
                
                <li><a href="../corporate-blogs-front-page-structure/index.php">Corporate Blogs: Front Page Structure</a></li>
                
              
                
                <li><a href="../spoilers/index.php">What Spoilers Teach Us About Designing for Different User Tasks</a></li>
                
              
            </ul>
          </div>
        
        

        
          
          <div>
            <h3>Research Reports</h3>
            <ul class="no-bullet">
              
                
                  <li><a href="../../reports/how-people-read-web-eyetracking-evidence/index.php">How People Read on the Web: The Eyetracking Evidence</a></li>
                
              
                
                  <li><a href="../../reports/investor-relations-ir-corporate-websites/index.php">Investor Relations (IR) on Corporate Websites</a></li>
                
              
                
                  <li><a href="../../reports/strategic-design-faqs/index.php">Strategic Design for Frequently Asked Questions</a></li>
                
              
                
                  <li><a href="../../reports/social-media-user-experience/index.php">Social Media User Experience</a></li>
                
              
                
                  <li><a href="../../reports/pr-websites/index.php">PR on Websites</a></li>
                
              
            </ul>
          </div>
          
        

      </div>
      <div class="medium-6 columns">
        
          
          <div>
            <h3>UX Conference Training Courses</h3>
             <ul class="no-bullet">
    
        <li><a href="../../courses/cross-channel-user-experience/index.php">Omnichannel Journeys and Customer Experience</a></li>
    
        <li><a href="../../courses/stakeholder-ux-approval/index.php">Engaging Stakeholders to Build Buy-In</a></li>
    
        <li><a href="../../courses/content-strategy/index.php">Content Strategy</a></li>
    
        <li><a href="../../courses/writing/index.php">Writing Compelling Digital Copy</a></li>
    
</ul>
          </div>
          
        
        
          
          <div>
            <h3>Online Seminars</h3>
            <ul class="no-bullet">
            
                <li><a href="../../online-seminars/improving-intranet-content/index.php">Improving Intranet Content</a></li>
            
                <li><a href="../../online-seminars/every-word-count/index.php">Making Every Word Count</a></li>
            
            </ul>
          </div>
          
        
      </div>
    </div>
  </footer>
</article>

        </div>

      

    
  </div>
  <div class="row">
    
      <div class="column small-12 show-for-small-down l-navrail announcements">
        
          
        
      </div>
    
    
  </div>

    </div>
    




<footer id="page-footer">
    <div class="row hide-for-print">
        <div class="column small-12 medium-6">
            <section class="footer-popular">
                
                <h1>Popular Topics</h1>
                <div style="float: left;  width: 15em; display: block;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/applications/index.php">Application Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/e-commerce/index.php">E-Commerce Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/eyetracking/index.php">Eyetracking </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/information-architecture/index.php">Information Architecture </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/intranets/index.php">Intranet Design </a></li>
	<li><a href="../../topic/interaction-design/index.php">Interaction Design </a></li>
</ul>
</div>

<div style="float: left;  width: 15em; display: block; list-style: none;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/mobile-and-tablet-design/index.php">Mobile and Tablet Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/credibility-persuasion/index.php">Persuasive Design</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/research-methods/index.php">Research Methods</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/user-testing/index.php">User Testing </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/web-usability/index.php">Web Usability </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/writing-web/index.php">Web Writing &amp; Content Strategy </a></li>
</ul>
</div>

<p>&nbsp;</p>

                
            </section>
        </div>

        <div class="column small-12 medium-6">
            <div class="row">
                <div class="column small-6">
                    <section class="footer-about">
                        <h1>Follow Us</h1>
                        <ul>
	<li><a href="../subscribe/index.php">Subscribe to our weekly UX newsletter</a>,&nbsp;Jakob Nielsen&#39;s Alertbox</li>
	<li>Visit Don Norman&#39;s website, <a href="http://www.jnd.org/"> www.jnd.org </a></li>
	<li>Visit Bruce Tognazzini&#39;s website, <a href="http://www.asktog.com/"> www.asktog.com </a></li>
	<li style="padding-top: 6px;"><a class="twitter-follow-button" data-show-count="false" data-size="large" href="https://twitter.com/nngroup"><img alt="Follow @nngroup on Twitter" height="20" src="https://media.nngroup.com/media/editor/2016/12/30/twitter-follow-button.png" style="width: 117px; height: 20px;" width="117" /></a></li>
	<li style="padding-top: 6px;"><img alt="" height="14" src="https://media.nngroup.com/media/editor/2016/07/26/feed-icon-14x14.png" style="vertical-align:text-top;width:14px;margin-right:.25em;" width="14" /><a href="../../feed/rss/index.php">Subscribe to our RSS feed</a></li>
</ul>

                    </section>
                </div>

                <div class="column small-6">
                    <section class="footer-about right-column">
                        <h1>About
</h1>
                        <ul class="no-bullet">
	<li><a href="../../about/contact/index.php">Contact information</a></li>
	<li><a href="../../about/index.php">About Us</a></li>
	<li><a href="../../about/why-nng/index.php">Why NN/g? </a></li>
	<li><a href="../../return-policy/index.php">Return Policy</a></li>
</ul>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="copyright column small-12">
            <p><a href="../../copyright-and-reprint-info/index.php">Copyright</a> &copy; 1998-2017 Nielsen Norman Group, All Rights Reserved.</p>
        </div>
    </div>
</footer>


    


        <script src="https://media.nngroup.com/static/vendor/fastclick/js/fastclick.min.js"></script>
        <script src="https://media.nngroup.com/static/vendor/foundation/js/foundation.min.js"></script>
        <script src="http://media.nngroup.com/static/js/app.js"></script>
        <script src="http://media.nngroup.com/static/js/scroll-to-fixed.js"></script><script>var externalLinks = document.querySelectorAll('a[href^="http"]');for (var i = externalLinks.length-1; i >= 0; i--) {externalLinks[i].addEventListener("click", function(event) { alert("External links are disabled for this test."); event.preventDefault(); }, false);}var mailLinks = document.querySelectorAll('a[href^="mailto"]');for (var i = mailLinks.length-1; i >= 0; i--) {mailLinks[i].addEventListener("click", function(event) { alert("Email links are disabled for this test."); event.preventDefault(); }, false);}var externalForms = document.querySelectorAll('form');for (var i = externalForms.length-1; i >= 0; i--) {externalForms[i].addEventListener("submit", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false); externalForms[i].addEventListener("click", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false);}</script>

        <script>
            
            $(function() {
                $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
                $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
            });
        </script>

        
<script src="https://media.nngroup.com/static/js/articles.js"></script>


        
        <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1072699960;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072699960/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
    </body>

<!-- Mirrored from www.nngroup.com/articles/online-press-releases-and-news/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:13:13 GMT -->
</html>
