<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->





    
<!-- Mirrored from www.nngroup.com/articles/childrens-websites-usability-issues/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:13:48 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1026.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"licenseKey":"1a30ef9931","queueTime":0,"beacon":"bam.nr-data.net","errorBeacon":"bam.nr-data.net","applicationID":"2048992","transactionName":"NFxSMRBUX0EDUkIPDA0WdhAMVkVbDV8ZBxEXUFMJB0YfRAtUQRVZIktEDAFZVHYHRVcPD01eVRE=","applicationTime":328,"agent":""}</script>
        <title>Children&#39;s Websites: Usability Issues in Designing for Kids</title><meta property="og:title" content="Children&#39;s Websites: Usability Issues in Designing for Kids" />
  
        
        <meta name="description" content="New research with users aged 3-12 shows that older kids have gained substantial Web proficiency since our last studies, while younger kids still face many problems. Designing for children requires distinct usability approaches, including targeting content narrowly for different ages of kids.">
        <meta property="og:description" content="New research with users aged 3-12 shows that older kids have gained substantial Web proficiency since our last studies, while younger kids still face many problems. Designing for children requires distinct usability approaches, including targeting content narrowly for different ages of kids." />
        
  
        
	
        
        <meta name="keywords" content="children, child, kids, young users, young readers, reading, pre-readers, websites for kids, kids korner, kids corner, navigation, redundant navigation, learned path bias, mine sweeping, animation, games, scrolling, age-specific design">
        

        

        <meta name="robots" content="noarchive">
        <meta name="viewport" content="width=device-width">

        <link rel="search" type="application/opensearchdescription+xml" title="NN/g Search" href="../../opensearch.xml" />
        <link rel="shortcut icon" href="http://media.nngroup.com/static/img/favicon.ico" />

        <link rel="stylesheet" href="https://media.nngroup.com/static/vendor/font-awesome/css/font-awesome.min.css">

        
        <link rel="stylesheet" href="https://media.nngroup.com/static/css/7232a960f1df.css" type="text/css" />
        <link rel="stylesheet" href="http://fast.fonts.com/t/1.css?apiType=css&amp;projectid=c5846260-9def-46ec-9407-0f1349bd2d64" type="text/css">

        <script src="https://media.nngroup.com/static/vendor/modernizr/js/modernizr.min.js"></script>

        <script type="text/javascript" src="https://media.nngroup.com/static/js/3283be5def74.js"></script>
        

        

        
        
        <!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=229034,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('http://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->


<!-- Google Analytics Tracking Code for http://nngroup.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23132681-1', 'nngroup.com');
  ga('require', 'linkid', 'https://www.nngroup.com/articles/childrens-websites-usability-issues/linkid.js');
  ga('require', 'displayfeatures');

// Optimizely Universal Analytics Integration
 window.optimizely = window.optimizely || [];
 window.optimizely.push("activateUniversalAnalytics");

  ga('send', 'pageview');

</script>
<script>
/**
* Function to track a click on an outbound link in Google Analytics.
*/
function trackOutboundLink(label, url) {
    if(window.ga && ga.loaded) {
        ga('send', 'event', 'outbound', 'click', label, {'hitCallback': function(){document.location = url;}
        });
    } else {
        document.location = url;
    }
}
/**
* Function to track a click on a link in the article announcement area.
*/
$('#articleAnnouncement a').on('click', function(){
  ga('send', 'event', 'link', 'click', 'articleAnnouncement');
});
</script>

<script type="text/javascript">
 setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2421.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



<!-- Hotjar Tracking Code for http://nngroup.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:121605,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

        <script src="https://media.nngroup.com/static/js/analytics/track_outbound_link.js"></script>
        
        
        
    </head>
    <body class="location-articles articles-detail">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
    <header id="page-header">
        <div id="cart" class="shopping-cart mobile-cart show-for-small-down"></div>
        <div class="row">
            <div id="masthead" class="masthead small-12 medium-7 column">

                <!-- The button to toggles menu on small screen -->

                <h1><a href="../../index.php"><img src="http://media.nngroup.com/static/img/logo-wordmark.png" width="74" height="30"> Nielsen <span class="norman">Norman</span> Group</a></h1>
                <p class="tagline show-for-medium-up">Evidence-Based User Experience Research, Training, and Consulting</p>

                <!-- The button to toggles form search on small screen -->


                <div style="clear: right;width:  100%;height: 1px;"></div><?php define('ROOT_PATH',$_SERVER["DOCUMENT_ROOT"] . '/apps/navigation_styles/navigations/2'); ?><?php include(ROOT_PATH)."/navigation.php"; ?>

                <!-- Drop down menu on small screen -->
                <div id="hideNav" class="show-for-small-down">
                  
                  <ul class="menu-content">
                    <li><a href="../../index.php">Home</a></li>
                    <li class="has-submenu">
                      <a>Training</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../training/index.php">Overview</a></li>
                        
<li id="nav-ux-conference"><a href="../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../online-seminars/index.php"><span>Online Seminars</span></a></li>

                      </ul>
                    </li>
                    <li class="has-submenu">
                      <a>Consulting</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
  <li><a href="../../consulting/index.php">Overview</a></li>
  
  <li><a href="../../consulting/benchmarking/index.php">Benchmarking</a></li>
  
  <li><a href="../../consulting/expert-design-review/index.php">Expert Design Review</a></li>
  
  <li><a href="../../consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
  
  <li><a href="../../consulting/usability-testing/index.php">Usability Testing </a></li>
  
  <li><a href="../../consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
  
  <li><a href="../../in-house-training/index.php">In-House Training</a></li>
</ul>

                    </li>

                    <li><a href="../../reports/index.php">Reports</a></li>
                    <li><a href="../index.php">Articles</a></li>
                    <li class="has-submenu"><a>About NN/G</a><i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../about/index.php">Overview</a></li>
                        <li><a href="../../people/index.php">People</a></li>
                        <li><a href="../../about/why-nng/index.php">Why NN/g?</a></li>
                        <li><a href="../../about/contact/index.php">Contact</a></li>
                        <li><a href="../../news/index.php">News</a></li>
                        <li><a href="../../about/history/index.php">History</a></li>
                        <li><a href="../../books/index.php">Books</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                </div>

                <hr class="header-divider show-for-small-only"/>


            </div>
       </div>

        <div id="hideNav">            <nav class="primary-nav small-12 column">
                <ul>
                    <li><a class="navitem" id="nav-home" href="../../index.php">Home</a></li>
                    <li><a class="navitem" id="nav-training" href="../../training/index.php">Training</a></li>
                    <li><a class="navitem" id="nav-consulting" href="../../consulting/index.php">Consulting</a></li>
                    <li><a class="navitem" id="nav-reports" href="../../reports/index.php">Reports</a></li>
                    <li><a class="navitem" id="nav-articles" href="../index.php">Articles</a></li>
                    <li><a class="navitem" id="nav-about" href="../../about/index.php">About NN/G</a></li>
                </ul>
            </nav>
            <nav class="secondary-nav small-12 medium-12 large-12 columns">
                
                
            </nav>
        </div>
    </header>
    <div id="main" class="l-content" onclick="closeNav(event)"><span id="preventClose" style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776; Menu</span>
    
  

  

  <div class="row">
    
      
      <section class="small-12 medium-3 columns collapsable-content l-navrail articles-left-column">
        <h1 class="show-for-small-down collapse-link mobile-browse-header">Browse by Topic and Author</h1>
          <div class="row collapsable">

            
            <div class="small-6 medium-12 columns">
              <section id="article-popular-topics" style="display: none;">
                
                <h1>Topics</h1>
                <ul id="article-popular-topics">
                    
                    <li>
                        
                        <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/intranets/all/index.php">Intranets</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/user-testing/all/index.php">User Testing</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                        
                    </li>
                    
                    <li><a href="#" id="article-see-all-topics"><strong>See all topics&hellip;</strong></a></li>
                </ul>
                
              </section>
              <section id="article-all-topics" style="display: none;">
                
                <h1>
                  <span class="show-for-medium-up">All Article Topics</span>
                  <span class="show-for-small-down">All Topics</span>
                  (<a href="#" id="article-hide-all-topics">hide</a>)
                </h1>
                <ul>
                  
                  <li>
                    
                    <a href="../../topic/accessibility/all/index.php">Accessibility</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/agile/all/index.php">Agile</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/analytics-and-metrics/all/index.php">Analytics &amp; Metrics</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/applications/all/index.php">Application Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/b2b-websites/all/index.php">B2B Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/behavior-patterns/all/index.php">Behavior Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/branding/all/index.php">Branding</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/content-strategy/all/index.php">Content Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/corporate-websites/all/index.php">Corporate Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/design-patterns/all/index.php">Design Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-design-process/all/index.php">Design Process</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/email/all/index.php">Email</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/eyetracking/all/index.php">Eyetracking</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/heuristic-evaluation/all/index.php">Heuristic Evaluation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/human-computer-interaction/all/index.php">Human Computer Interaction</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-ideation/all/index.php">Ideation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/information-architecture/all/index.php">Information Architecture</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/interaction-design/all/index.php">Interaction Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/international-users/all/index.php">International Users</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/intranets/all/index.php">Intranets</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-management/all/index.php">Management</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/navigation/all/index.php">Navigation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/non-profit-websites/all/index.php">Non-Profit Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/personas/all/index.php">Personas</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/persuasive-design/all/index.php">Persuasive Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/prototyping/all/index.php">Prototyping</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/psychology-and-ux/all/index.php">Psychology and UX</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/research-methods/all/index.php">Research Methods</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/search/all/index.php">Search</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/social-media/all/index.php">Social Media</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/strategy/all/index.php">Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/user-testing/all/index.php">User Testing</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-teams/all/index.php">UX Teams</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/visual-design/all/index.php">Visual Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/writing-web/all/index.php">Writing for the Web</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/young-users/all/index.php">Young Users</a>
                    
                  </li>
                  
                </ul>
                
              </section>
            </div>
            <script>
              /* Determine which topics list will be displayed: the full or the
               * partial.  We are inlining this close to the list itself to
               * prevent rendering delays on the client
               */
              var topic_list = $.cookie("article-topics");

              var popular_topics = $('#article-popular-topics');
              var all_topics = $('#article-all-topics');

              if(topic_list == 1) {
                  popular_topics.hide();
                  all_topics.show();
              } else {
                  popular_topics.show();
                  all_topics.hide();
              }
            </script>
            

            
            <div class="small-6 medium-12 columns">
              
              <section id="article-initial-authors">
                  <h1>Author</h1>
                  <ul>
                    
                    <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                    
                    <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                    
                    <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                    
                    <li><a href="#" onClick="$('#article-initial-authors').hide(); $('#article-all-authors').show();"><strong>See all authors&hellip;</strong></a></li>
                  </ul>
              </section>
              <section id="article-all-authors" style="display: none;">
                <h1>
                  All Authors
                  (<a href="#" id="article-hide-all-authors">hide</a>)
                </h1>
                <ul>
                  
                  <li><a href="../author/raluca-budiu/index.php">Raluca Budiu</a></li>
                  
                  <li><a href="../author/patty-caya/index.php">Patty Caya</a></li>
                  
                  <li><a href="../author/susan-farrell/index.php">Susan Farrell</a></li>
                  
                  <li><a href="../author/therese-fessenden/index.php">Therese Fessenden</a></li>
                  
                  <li><a href="../author/kim-flaherty/index.php">Kim Flaherty</a></li>
                  
                  <li><a href="../author/sarah-gibbons/index.php">Sarah Gibbons</a></li>
                  
                  <li><a href="../author/aurora-harley/index.php">Aurora Harley</a></li>
                  
                  <li><a href="../author/kate-kaplan/index.php">Kate Kaplan</a></li>
                  
                  <li><a href="../author/page-laubheimer/index.php">Page Laubheimer</a></li>
                  
                  <li><a href="../author/angela-li/index.php">Angie Li</a></li>
                  
                  <li><a href="../author/hoa-loranger/index.php">Hoa Loranger</a></li>
                  
                  <li><a href="../author/kate-meyer/index.php">Kate Meyer</a></li>
                  
                  <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                  
                  <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                  
                  <li><a href="../author/kara-pernice/index.php">Kara Pernice</a></li>
                  
                  <li><a href="../author/christian-rohrer/index.php">Christian Rohrer</a></li>
                  
                  <li><a href="../author/amy-schade/index.php">Amy Schade</a></li>
                  
                  <li><a href="../author/katie-sherwin/index.php">Katie Sherwin</a></li>
                  
                  <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                  
                  <li><a href="../author/kathryn-whitenton/index.php">Kathryn Whitenton</a></li>
                  
                </ul>
              </section>
              
            </div>
            
          </div>

          
          <section class="hide-for-small-down">
            
              
              <h1>Recent Articles</h1>
              
              <ul>
                  
                  <li><a href="../seamless-cross-channel/index.php">Seamlessness in the Omnichannel User Experience</a></li>
                  
                  <li><a href="../brand-intention-interpretation/index.php">Experience Design: Bridging Brand Intention and Brand Interpretation</a></li>
                  
                  <li><a href="../eyetracking-tasks-efficient-scanning/index.php">Scanning Patterns on the Web Are Optimized for the Current Task</a></li>
                  
                  <li><a href="../flat-design-best-practices/index.php">Flat-Design Best Practices</a></li>
                  
                  <li><a href="../ux-user-stories/index.php">Accounting for UX Work with User Stories in Agile Projects</a></li>
                  
                  <li><a href="../index.php"><strong>See all articles&hellip;</strong></a></li>
              </ul>
              
            
          </section>
          

          
          
          <section class="hide-for-small-down">
            <h1>Popular Articles</h1>
            
            <ul>
                
                <li><a href="../usability-101-introduction-to-usability/index.php">Usability 101: Introduction to Usability</a></li>
                
                <li><a href="../top-10-mistakes-web-design/index.php">Top 10 Mistakes in Web Design</a></li>
                
                <li><a href="../how-users-read-on-the-web/index.php">How Users Read on the Web</a></li>
                
                <li><a href="../f-shaped-pattern-reading-web-content/index.php">F-Shaped Pattern For Reading Web Content</a></li>
                
                <li><a href="../ten-usability-heuristics/index.php">10 Usability Heuristics for User Interface Design</a></li>
                
                <li><a href="../intranet-design-2016/index.php">10 Best Intranets of 2016</a></li>
                
                <li><a href="../which-ux-research-methods/index.php">When to Use Which User-Experience Research Methods</a></li>
                
                <li><a href="../response-times-3-important-limits/index.php">Response Times: The 3 Important Limits</a></li>
                
                <li><a href="../why-you-only-need-to-test-with-5-users/index.php">Why You Only Need to Test with 5 Users</a></li>
                
                <li><a href="../page-fold-manifesto/index.php">The Fold Manifesto: Why the Page Fold Still Matters</a></li>
                
            </ul>
          </section>
          
          

          
            <section class="hide-for-small-down">
              
            </section>
          

      </section>
      

      
        <div class="small-12 medium-9 columns content-body">
          




<article class="l-subsection alertbox-article-content article-detail-bl">
  
  <header>
    <div class="l-subsection">
      <h1>Children&#39;s Websites: Usability Issues in Designing for Kids</h1>
      <div class="author-meta">by
        
          
            
            <a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a>
            
          
        on  September 13, 2010
        
      </div>

      

      
      <div class="topic-links">
        Topics:
        
          <ul>

  <li><a href="../../topic/young-users/index.php">Young Users</a></li>

  <li><a href="../../topic/behavior-patterns/index.php">Behavior Patterns</a></li>

</ul>
        
      </div>
      
    </div>
  </header>
  

  
  
  <section>
    <p class="lede"><strong>Summary:</strong> New research with users aged 3-12 shows that older kids have gained substantial Web proficiency since our last studies, while younger kids still face many problems. Designing for children requires distinct usability approaches, including targeting content narrowly for different ages of kids.</p>
    <br />
  </section>
  
  

  
  <section id="article-body"><p>Millions of children use the Internet, and millions more are coming online each year. Many websites specifically target children with educational or entertainment content, and mainstream websites often have specific "kids' corner" sections — either as a public service or to build brand loyalty from an early age.</p>

<p>Despite this growth in users and services, little is known about how children actually use websites or how to design sites that will be easy for them to use. Website design for kids is <strong> typically based purely on folklore </strong> about how kids supposedly behave — or, at best, on insights gleaned when designers observe their own children, who hardly represent average kids, typical Internet skills, or common knowledge about the Web.</p>

<p>To <strong> separate design myths from usability facts</strong>, we turn to empirical user research: observations of a broad range of children as they use a wide variety of websites.</p>

<p>This research covers <strong> users aged 3–12 years</strong>. (Guidelines for sites targeting 13- to 17-year-olds are available in a report from our separate <a class="new" href="../usability-of-websites-for-teenagers/index.php" title="Alertbox: Teenage Usability: Designing Teen-Targeted Websites (users aged 13-17)"> research with teenagers</a>.)</p>

<h2>User Studies</h2>

<p>We conducted <strong> two separate rounds of usability studies</strong>, testing a total of 90 children (41 girls and 49 boys):</p>

<ul>
	<li>Study 1 (9 years ago). In this study, we tested <strong> 27 sites with 55 children</strong>, aged 6–11. We conducted about a third of the study in Israel, and the rest in the United States.</li>
	<li>Study 2 (new research). In this study, we tested <strong> 29 sites with 35 children</strong>, aged 3–12 years. All of these user sessions were in the U.S.</li>
</ul>

<p>In Study 1, we conducted sessions in participants' homes, at schools, and in a usability lab. All of Study 2 sessions were run in a lab. We tested some users in friendship pairs, and other individually. Pair sessions worked best for 6- to 8-year-old users. In contrast, for children younger than 6 or older than 8, individual sessions were just as good (and are obviously cheaper, as we had to recruit only one user per session).</p>

<p>Although it can be difficult for shy or very young kids, we encouraged users to think out loud while they were using the sites. We told the children that <strong> they were the experts</strong>, and that we wanted them to teach us how kids use and think about websites. We then explained that, in order for us to learn, they had to explain what they were thinking at all times.</p>

<p>We mainly took the users to specific sites and gave them <strong> directed tasks </strong> that we'd prepared for each site. Often, these tasks differed according to the user's age and gender. For example, on lego.com, we asked girls in the 6- to 8-year-old range to find a horseback riding game, and asked 9- to 12-year-old boys to find information about the multiplayer game Lego Universe. At other times, we gave all users the same task. For example, on jitterbug.tv, we asked users to watch a video of the song, "The Wheels on the Bus." Finally, some sites were tested only by a targeted age group or gender. On crayola.com, for example, we tested only 3- to 5-year-old users, and asked them to draw and print a portrait of their best friend.</p>

<p>We mainly tested websites targeted at children, as well as the special "kids' corner" sections, where many mainstream websites offer content for kids. In Study 1, we also tested a few general websites targeted at grownup users to assess how kids use such sites. And, in Study 2, we tested some <strong> Web-wide tasks</strong>, asking users a general question and letting them find the answer on a site of their choosing. For example, we asked children aged 6–12 to find out how to say "thank you" in Japanese.</p>

<p>We tested 53 websites, covering a <strong> broad range of genres</strong>:</p>

<ul>
	<li>Games (e.g., Herman's Homepage, PoissonRouge.com)</li>
	<li>Media sites (e.g., Cartoon Network, Discovery Channel, PBS)</li>
	<li>Educational (e.g., Children's Discovery Museum of San Jose, Funbrain.com, Girl Scouts, National Geographic)</li>
	<li>Toys and other children's products (e.g., Barbie, Fisher-Price, the Harry Potter books, Hasbro, Lego)</li>
	<li>Other commercial sites (e.g., Belmont Bank, National Football League)</li>
	<li>Government (e.g., Jet Propulsion Laboratory, Library of Congress, United States Mint)</li>
</ul>

<h2>Changes over Time</h2>

<p>We conducted our two studies 9 years apart. <a class="old" href="../usability-guidelines-change/index.php" title="Alertbox:  Change vs. Stability in Web Usability Guidelines"> Most aspects of usability don't change much</a> in 9 years; when we test the same design questions repeatedly, we usually end up with the same findings. For example, guidelines on the best way to structure a menu or how many items a menu should include are typically the same year after year, because such user interface questions are determined more by the <a class="new" href="../../courses/human-mind/index.php" title="Nielsen Norman Group course: The Human Mind and Usability - How Your Customers Think"> human brain's characteristics and limitations</a> than by changing technologies. (Other design issues are more technologically determined. For example, people's approach to video on websites has changed substantially over the last decade.)</p>

<p>In addition to the static state of human psychology, usability findings tend to be stable because <strong> the user pool is stable</strong>. If we take a simplified view of the adult audience — say, anyone from 20 to 80 years of age — then 83% of users will be unchanged from one decade to the next. (In reality, most websites don't have an even age distribution among their customers, but the point remains: adult users in 10 years will be generally the same people who are using the Web today.)</p>

<p><strong>Children are a different story</strong>, and there are at least two reasons to expect that current usability findings might be different from those of 9 years ago:</p>

<ul>
	<li>There has been <strong> 100% turnover </strong> among the individuals in our 3- to 12-year-old range. Our youngest users from 9 years ago (when we tested kids aged 6–11) will be 15 years old now, and thus no longer within our research's target audience. We're dealing with a completely new generation of kids.</li>
	<li>Over the past decade, the amount of time children spend on a computer has tripled, according to Kaiser Family Foundation research. And, according to both our research and that of others, the best predictor of how children use websites is <strong> how much online practice they have</strong>.</li>
</ul>

<p>Taken together, these two observations imply major changes in what constitutes a usable site for kids today compared to 9 years ago.</p>

<p>Despite this reasonable case for expecting major changes, our second study actually confirmed most of the first study's guidelines. We did learn many new things, however, and the number of design guidelines increased from 70 to 130 — partly because we tested newer sites that do newer things, and partly because we extended the age group to include very young children (aged 3–5).</p>

<p>In one example, we observed literally the <strong> same usability problem again 9 years later</strong>: the Web pages for the <cite> Sesame Street </cite> TV series use a navigation bar with characters from the show. Unfortunately, in the UI, these icons serve a dual purpose as both navigation icons and features of a mini-game: as users mouse over them, the characters act as xylophone keys that play musical notes. Both 9 years ago and today, the xylophone feature distracted children from the navigation. And, as kids played tunes, they were disoriented by accidental clicking.</p>

<p>Although many usability guidelines are the same as 9 years ago, we did find one major change since the first study: <strong> children today are much more experienced </strong> in using computers and the Internet. As a result, they're not as subject to many of the prevalent, beginning-user problems we found in our first study. These days, kids are on computers almost as soon as they can sit up and move a mouse or tap a screen. It's now common for a 7-year-old kid to be a seasoned Internet user with several years' experience.</p>

<p>Such <strong> early exposure to computers </strong> was the reason we extended the age range covered by our research to include children aged 3–5.</p>

<p>The biggest change since Study 1 is that many of the behaviors we previously saw in the mid-range age group (6–8 years) are now more characteristic for the youngest users (3–5). In contrast, users who are 7 or older often exhibit <strong> fairly advanced behaviors </strong> . For example, now only the youngest kids have problems with scrolling. Children 9 years and older are more likely to scroll, and in fact do better with articles that are presented on one scrolling page rather than split into many small pages. (The ability to use scrolling Web pages is something we've seen in adult users <a class="old" href="../changes-in-web-usability-since-1994/index.php" title="Alertbox:  Changes in Web Usability Since 1994"> since 1997</a>, though people still <a class="old" href="../scrolling-and-attention/index.php" title="Alertbox:  Scrolling and Attention"> look more at content on the top part of the page</a> than at information lower down.)</p>

<p>Another change relates to reading. In the first study, many children were willing to read instructions before, say, starting a game. Now many kids behave more like adult users and <a class="old" href="../how-little-do-users-read/index.php" title="Alertbox:  How Little Do Users Read?"> refuse to read</a>. This reduced willingness to read seems related to experience: the more experience our users had, the less they read.</p>

<h2>Children vs. Adult Users</h2>

<p>The two big conclusions regarding Web usability for children are:</p>

<ul>
	<li><strong>Kids and adults are different</strong>, and kids need a design style that follows different usability guidelines.</li>
	<li>That said, many of the things that make sites easier for adults also make them easier for kids. Don't discard what you already know about usable design and how to simplify sites. In particular, <a class="old" href="../the-need-for-web-design-standards/index.php" title="Alertbox:  The Need for Web Design Standards"> comply with Web-wide UI conventions</a> and employ a consistent design within your site.</li>
</ul>

<p>The following table summarizes some of the main similarities and differences we've observed in user behavior between children (in this study) and adults (across many other studies).</p>

<table align="center" style="border-style: solid; border-color: #cccccc; border-width: 1px; border-collapse: collapse; max-width: 40em;">
	<tbody>
		<tr>
			<th style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left; background-color:  #003366; color: #ffffff; width: 30%;"> </th>
			<th style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: center; background-color:  #003366; color: #ffffff; width: 35%;">Children</th>
			<th style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: center; background-color:  #003366; color: #ffffff; width: 35%;">Adults</th>
		</tr>
		<tr>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left; vertical-align: middle;">Goal in visiting websites</td>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left; vertical-align: middle;">Entertainment</td>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left; vertical-align: middle;">Getting things done<br/>
			Communication/community</td>
		</tr>
		<tr>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left; vertical-align: middle;">First reactions</td>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left; vertical-align: middle;">Quick to judge site<br/>
			(and to leave if no good)</td>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left; vertical-align: middle;">Quick to judge site<br/>
			(and to leave if no good)</td>
		</tr>
		<tr>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left; vertical-align: middle;">Willingness to wait</td>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left; vertical-align: middle;">Want instant gratification</td>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left; vertical-align: middle;">Limited patience</td>
		</tr>
		<tr>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left; vertical-align: middle;">Following UI conventions</td>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left; vertical-align: middle;">Preferred</td>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left; vertical-align: middle;">Preferred</td>
		</tr>
		<tr>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left; vertical-align: middle;">User control</td>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left; vertical-align: middle;">Preferred</td>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left; vertical-align: middle;">Preferred</td>
		</tr>
		<tr>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left; vertical-align: middle;">Exploratory behavior</td>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left; vertical-align: middle;">Like to try many options<br/>
			Mine-sweeping the screen</td>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left; vertical-align: middle;">Stick to main path</td>
		</tr>
		<tr>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left; vertical-align: middle;">Multiple/redundant navigation</td>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left; vertical-align: middle;">Very confusing</td>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left; vertical-align: middle;">Slightly confusing</td>
		</tr>
		<tr>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left; vertical-align: middle;"><em>Back </em> button</td>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left; vertical-align: middle;">Not used (young kids)<br/>
			Relied on (older kids)</td>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left; vertical-align: middle;">Relied on</td>
		</tr>
		<tr>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left; vertical-align: middle;">Reading</td>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left; vertical-align: middle;">Not at all (youngest kids)<br/>
			Tentative (young kids)<br/>
			Scanning (older kids)</td>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left; vertical-align: middle;"><a class="old" href="../first-2-words-a-signal-for-scanning/index.php" title="Alertbox:  First 2 Words - A Signal for the Scanning Eye">Scanning </a></td>
		</tr>
		<tr>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left; vertical-align: middle;">Readability level</td>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left; vertical-align: middle;">Each user's grade level</td>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left; vertical-align: middle;"><a class="old" href="../writing-for-lower-literacy-users/index.php" title="Alertbox:  Lower-Literacy Users">8<sup>th </sup> to 10<sup>th </sup>grade text</a> for broad consumer audiences</td>
		</tr>
		<tr>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left; vertical-align: middle;">Real-life metaphors<br/>
			e.g., spatial navigation</td>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left; vertical-align: middle;">Very helpful for pre-readers</td>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left; vertical-align: middle;">Often distracting or too clunky for online UI</td>
		</tr>
		<tr>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left; vertical-align: middle;">Font size</td>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left; vertical-align: middle;">14 point (young kids)<br/>
			12 point (older kids)</td>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left; vertical-align: middle;">10 point<br/>
			(up to 14 point for seniors)</td>
		</tr>
		<tr>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left; vertical-align: middle;">Physical limitations</td>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left; vertical-align: middle;">Slow typists<br/>
			Poor mouse control</td>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left; vertical-align: middle;">None (unless <a class="old" href="../../topic/accessibility/index.php" title="Nielsen Norman Group report: Beyond ALT Text"> disabled</a>)</td>
		</tr>
		<tr>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left; vertical-align: middle;">Scrolling</td>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left; vertical-align: middle;">Avoid (young kids)<br/>
			Some (older kids)</td>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left; vertical-align: middle;">Some</td>
		</tr>
		<tr>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left; vertical-align: middle;">Animation and sound</td>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left; vertical-align: middle;">Liked</td>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left; vertical-align: middle;">Usually disliked</td>
		</tr>
		<tr>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left; vertical-align: middle;">Advertising and promotions</td>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left; vertical-align: middle;">Can't distinguish from real content</td>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left; vertical-align: middle;">Ads avoided ( <a class="old" href="../banner-blindness-old-and-new-findings/index.php" title="Alertbox:  Banner Blindness -Old and New Findings"> banner blindness</a>);<br/>
			promos viewed skeptically</td>
		</tr>
		<tr>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left; vertical-align: middle;">Disclosing private info</td>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left; vertical-align: middle;">Usually aware of issues: hesitant to enter info</td>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left; vertical-align: middle;">Often recklessly willing to give out personal info</td>
		</tr>
		<tr>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left; vertical-align: middle;">Age-targeted design</td>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left; vertical-align: middle;">Crucial, with very fine-grained distinctions between age groups</td>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left; vertical-align: middle;">Unimportant for most sites (except to <a class="old" href="../usability-for-senior-citizens/index.php" title="Alertbox: Usability for Senior Citizens"> accommodate seniors</a>)</td>
		</tr>
		<tr>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left; vertical-align: middle;">Search</td>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left; vertical-align: middle;">Bigger reliance on bookmarks than search, but older kids do search</td>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left; vertical-align: middle;"><a class="old" href="../search-engines-become-answer-engines/index.php" title="Alertbox: When Search Engines Become Answer Engines">Main entry point</a> to the Web</td>
		</tr>
	</tbody>
</table>

<p>Many of the basic rules for usable Web design are the same for children and adults, though often with differences in degree.</p>

<p>For example, we've had a long-standing guideline to avoid redundant navigation schemes for adult users. People get annoyed when they have to look for navigation in several different places. And it's confusing when pages have multiple links to the same destination, because users don't know whether the various links actually point to the same place or have slightly different meanings. This often forces adult users to waste time clicking on the "same" link several times, causing navigational disorientation.</p>

<p>Our seminar on <a class="new" href="../../courses/ia-2/index.php" title="Nielsen Norman Group: detailed tutorial description and course outline"> website navigation design</a> covers 25 different navigation techniques, with guidelines for when to use them. They all have their place. But the one guideline to rule them all is to avoid stuffing too many navigation schemes into a single design.</p>

<p>Although too much navigation is annoying and confusing for adults, it can be devastating for children.</p>

<p>Kids suffer from a <strong> learned path bias</strong>: they tend to <strong> reuse the same method they've used before </strong> to initiate an action. In our studies, we often saw kids who had been successful with a certain approach to a site stick determinedly to that approach over and over again, even as it failed them during subsequent tasks that required them to use a different navigation scheme.</p>

<h2>Age-Appropriate Design</h2>

<p>The biggest finding in both the new and old research is the need to <strong> target very narrow age groups </strong> when designing for children. Indeed, there's no such thing as "designing for children," defined as everybody aged 3–12. At a minimum, you must <strong> distinguish between young (3–5), mid-range (6–8), and older (9–12) children</strong>. Each group has different behaviors, and the users get substantially more web-savvy as they get older. And, those different needs range far beyond the obvious imperative to design differently for pre-readers, beginning readers, and moderately skilled readers.</p>

<p>We found that young users reacted negatively to content designed for kids that were even one school grade below or above their own level. Children are <strong> acutely aware of age differences</strong>: at one website, a 6-year-old said, <em> "This website is for babies, maybe 4 or 5 years old. You can tell because of the cartoons and trains." </em> (Although you might view both 5- and 6-year olds as "little kids," in the mind of a 6-year-old, the difference between them is vast.)</p>

<p>Finally, it's important to <strong> retain a consistent user experience </strong> rather than bounce users among pages targeting different age groups. In particular, by understanding what attracts children's attention, you can "bury" the links to service content for parents in places that kids are unlikely to click. Text-only footers worked well for this purpose.</p>

<h2>Advice for Parents and Educators</h2>

<p>We conducted this research in order to generate usability guidelines for companies, government agencies, and major non-profit organizations that want to design websites for children. Even so, some of our findings have personal <strong> implications for parents, teachers, and others </strong> who want to help individual children succeed on the Internet:</p>

<ul>
	<li>The main predictor of children's ability to use websites is <strong> their amount of prior experience</strong>. We also found that kids as young as 3 can use websites, as long as they're designed according to the guidelines for this very young audience. Together, these two findings lead to the advice to start your children on the Internet at an early age (while also setting limits; too much computer time isn't good for kids).</li>
	<li>Campaigns to sensitize children to <strong> the Internet's potential dangers </strong> and to teach them to be wary of submitting personal information are meeting with success. Keep up this good work.</li>
	<li>On a more negative note, kids still don't understand the Web's commercial nature and <strong> lack the skills needed to identify advertising </strong> and treat it differently than real content. We need much stronger efforts to teach children about these facts of new media.</li>
</ul>

<h3>Full Report</h3>

<p>The full <a href="../../reports/children-on-the-web/index.php"> report on our user research with children, with actionable website design guidelines</a> is available for download.</p>
</section>
  

  
  <section>
    
        
    
  </section>
  
  
  
  
  

  


  <section>
    <br />
    <p class="share"><b>Share <span class="hide-for-small-down">this article</span>:</b> <a href="https://twitter.com/intent/tweet?url=http://www.nngroup.com/articles/childrens-websites-usability-issues/&amp;text=Children's%20Websites:%20Usability%20Issues%20in%20Designing%20for%20Kids&amp;via=nngroup">Twitter</a> | <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=http://www.nngroup.com/articles/childrens-websites-usability-issues/&amp;title=Children's%20Websites:%20Usability%20Issues%20in%20Designing%20for%20Kids&amp;source=Nielsen%20Norman%20Group">LinkedIn</a> | <a href="https://plus.google.com/share?url=http://www.nngroup.com/articles/childrens-websites-usability-issues/">Google+</a> | <a href="mailto:?subject=NN/g Article: Children&#39;s Websites: Usability Issues in Designing for Kids&amp;body=http://www.nngroup.com/articles/childrens-websites-usability-issues/">Email</a></p>
    <hr />
  </section>

  <footer class="l-subsection">
    
    
    <div class="learn-more">
      <h2>Learn More</h2>
      <p><a href="../subscribe/index.php">Subscribe to the weekly newsletter</a> to get notified about future articles.</p>
      </div>
    
    

    <div class="row show-for-small-down related-content">
      
      
      

      
      
        <div class="column small-12 topic-links">All articles about:
          <ul>
            
            <li><a href="../../topic/behavior-patterns/index.php">Behavior Patterns</a></li>
            
            <li><a href="../../topic/young-users/index.php">Young Users</a></li>
            
          </ul>
        </div>
      
      

      
      
        <div class="column small-12">
          <h3>UX Conference Training Courses</h3>
           <ul class="no-bullet">
    
        <li><a href="../../courses/hci/index.php">User Interface Principles Every Designer Must Know</a></li>
    
        <li><a href="../../courses/web-page-design/index.php">Web Page UX Design</a></li>
    
        <li><a href="../../courses/emotional-design/index.php">Emotional Design</a></li>
    
</ul>
        </div>
      
      

      
      
        <div class="small-12 column">
          <h3>Research Reports</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../reports/designing-for-young-adults/index.php">Designing for Young Adults (Ages 18-25)</a></li>
              
            
              
                <li><a href="../../reports/teenagers-on-the-web/index.php">Teenagers (Ages 13-17) on the Web</a></li>
              
            
              
                <li><a href="../../reports/children-on-the-web/index.php">Children (Ages 3-12) on the Web</a></li>
              
            
              
                <li><a href="../../reports/university/index.php">University Websites</a></li>
              
            
          </ul>
        </div>
      
      

      
      
        <div class="small-12 column">
          <h3>Online Seminars</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../online-seminars/millennials-online/index.php">Millennials Online</a></li>
              
            
              
                <li><a href="../../online-seminars/university-websites/index.php">University Website UX Essentials</a></li>
              
            
          </ul>
        </div>
      
      
    </div>

    <div class="hide-for-small-down row related-content">
      <div class="medium-6 columns">
        
        
          <div>
            <h3>Articles</h3>
            <ul class="no-bullet">
              
                
                <li><a href="../college-students-on-the-web/index.php">College Students on the Web</a></li>
                
              
                
                <li><a href="../embarrassment/index.php">Computer-Assisted Embarrassment</a></li>
                
              
                
                <li><a href="../page-fold-manifesto/index.php">The Fold Manifesto: Why the Page Fold Still Matters</a></li>
                
              
                
                <li><a href="../negativity-bias-ux/index.php">The Negativity Bias in User Experience</a></li>
                
              
                
                <li><a href="../millennials-digital-natives/index.php">Millennials as Digital Natives: Myths and Realities</a></li>
                
              
            </ul>
          </div>
        
        

        
          
          <div>
            <h3>Research Reports</h3>
            <ul class="no-bullet">
              
                
                  <li><a href="../../reports/designing-for-young-adults/index.php">Designing for Young Adults (Ages 18-25)</a></li>
                
              
                
                  <li><a href="../../reports/teenagers-on-the-web/index.php">Teenagers (Ages 13-17) on the Web</a></li>
                
              
                
                  <li><a href="../../reports/children-on-the-web/index.php">Children (Ages 3-12) on the Web</a></li>
                
              
                
                  <li><a href="../../reports/university/index.php">University Websites</a></li>
                
              
            </ul>
          </div>
          
        

      </div>
      <div class="medium-6 columns">
        
          
          <div>
            <h3>UX Conference Training Courses</h3>
             <ul class="no-bullet">
    
        <li><a href="../../courses/hci/index.php">User Interface Principles Every Designer Must Know</a></li>
    
        <li><a href="../../courses/web-page-design/index.php">Web Page UX Design</a></li>
    
        <li><a href="../../courses/emotional-design/index.php">Emotional Design</a></li>
    
</ul>
          </div>
          
        
        
          
          <div>
            <h3>Online Seminars</h3>
            <ul class="no-bullet">
            
                <li><a href="../../online-seminars/millennials-online/index.php">Millennials Online</a></li>
            
                <li><a href="../../online-seminars/university-websites/index.php">University Website UX Essentials</a></li>
            
            </ul>
          </div>
          
        
      </div>
    </div>
  </footer>
</article>

        </div>

      

    
  </div>
  <div class="row">
    
      <div class="column small-12 show-for-small-down l-navrail announcements">
        
          
        
      </div>
    
    
  </div>

    </div>
    




<footer id="page-footer">
    <div class="row hide-for-print">
        <div class="column small-12 medium-6">
            <section class="footer-popular">
                
                <h1>Popular Topics</h1>
                <div style="float: left;  width: 15em; display: block;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/applications/index.php">Application Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/e-commerce/index.php">E-Commerce Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/eyetracking/index.php">Eyetracking </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/information-architecture/index.php">Information Architecture </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/intranets/index.php">Intranet Design </a></li>
	<li><a href="../../topic/interaction-design/index.php">Interaction Design </a></li>
</ul>
</div>

<div style="float: left;  width: 15em; display: block; list-style: none;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/mobile-and-tablet-design/index.php">Mobile and Tablet Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/credibility-persuasion/index.php">Persuasive Design</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/research-methods/index.php">Research Methods</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/user-testing/index.php">User Testing </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/web-usability/index.php">Web Usability </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/writing-web/index.php">Web Writing &amp; Content Strategy </a></li>
</ul>
</div>

<p>&nbsp;</p>

                
            </section>
        </div>

        <div class="column small-12 medium-6">
            <div class="row">
                <div class="column small-6">
                    <section class="footer-about">
                        <h1>Follow Us</h1>
                        <ul>
	<li><a href="../subscribe/index.php">Subscribe to our weekly UX newsletter</a>,&nbsp;Jakob Nielsen&#39;s Alertbox</li>
	<li>Visit Don Norman&#39;s website, <a href="http://www.jnd.org/"> www.jnd.org </a></li>
	<li>Visit Bruce Tognazzini&#39;s website, <a href="http://www.asktog.com/"> www.asktog.com </a></li>
	<li style="padding-top: 6px;"><a class="twitter-follow-button" data-show-count="false" data-size="large" href="https://twitter.com/nngroup"><img alt="Follow @nngroup on Twitter" height="20" src="https://media.nngroup.com/media/editor/2016/12/30/twitter-follow-button.png" style="width: 117px; height: 20px;" width="117" /></a></li>
	<li style="padding-top: 6px;"><img alt="" height="14" src="https://media.nngroup.com/media/editor/2016/07/26/feed-icon-14x14.png" style="vertical-align:text-top;width:14px;margin-right:.25em;" width="14" /><a href="../../feed/rss/index.php">Subscribe to our RSS feed</a></li>
</ul>

                    </section>
                </div>

                <div class="column small-6">
                    <section class="footer-about right-column">
                        <h1>About
</h1>
                        <ul class="no-bullet">
	<li><a href="../../about/contact/index.php">Contact information</a></li>
	<li><a href="../../about/index.php">About Us</a></li>
	<li><a href="../../about/why-nng/index.php">Why NN/g? </a></li>
	<li><a href="../../return-policy/index.php">Return Policy</a></li>
</ul>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="copyright column small-12">
            <p><a href="../../copyright-and-reprint-info/index.php">Copyright</a> &copy; 1998-2017 Nielsen Norman Group, All Rights Reserved.</p>
        </div>
    </div>
</footer>


    


        <script src="https://media.nngroup.com/static/vendor/fastclick/js/fastclick.min.js"></script>
        <script src="https://media.nngroup.com/static/vendor/foundation/js/foundation.min.js"></script>
        <script src="http://media.nngroup.com/static/js/app.js"></script>
        <script src="http://media.nngroup.com/static/js/scroll-to-fixed.js"></script><script>var externalLinks = document.querySelectorAll('a[href^="http"]');for (var i = externalLinks.length-1; i >= 0; i--) {externalLinks[i].addEventListener("click", function(event) { alert("External links are disabled for this test."); event.preventDefault(); }, false);}var mailLinks = document.querySelectorAll('a[href^="mailto"]');for (var i = mailLinks.length-1; i >= 0; i--) {mailLinks[i].addEventListener("click", function(event) { alert("Email links are disabled for this test."); event.preventDefault(); }, false);}var externalForms = document.querySelectorAll('form');for (var i = externalForms.length-1; i >= 0; i--) {externalForms[i].addEventListener("submit", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false); externalForms[i].addEventListener("click", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false);}</script>

        <script>
            
            $(function() {
                $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
                $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
            });
        </script>

        
<script src="https://media.nngroup.com/static/js/articles.js"></script>


        
        <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1072699960;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072699960/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
    </body>

<!-- Mirrored from www.nngroup.com/articles/childrens-websites-usability-issues/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:13:48 GMT -->
</html>
