<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->





    
<!-- Mirrored from www.nngroup.com/articles/usability-labs/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:14:36 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1026.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"licenseKey":"1a30ef9931","queueTime":5,"beacon":"bam.nr-data.net","errorBeacon":"bam.nr-data.net","applicationID":"2048992","transactionName":"NFxSMRBUX0EDUkIPDA0WdhAMVkVbDV8ZBxEXUFMJB0YfRAtUQRVZIktEDAFZVHYHRVcPD01eVRE=","applicationTime":412,"agent":""}</script>
        <title>Usability Labs Survey: Article by Jakob Nielsen</title><meta property="og:title" content="Usability Labs Survey: Article by Jakob Nielsen" />
  
        
        <meta name="description" content="This article summarizes statistics for the thirteen usability laboratories described in the papers in this special issue. It also gives an introduction to the main uses of usability laboratories in  usability engineering and surveys some of the issues related to practical use of user testing and CAUSE tools for computer-aided usability engineering.">
        <meta property="og:description" content="This article summarizes statistics for the thirteen usability laboratories described in the papers in this special issue. It also gives an introduction to the main uses of usability laboratories in  usability engineering and surveys some of the issues related to practical use of user testing and CAUSE tools for computer-aided usability engineering." />
        
  
        
	
        
        <meta name="keywords" content="Usability Laboratories, Usability Engineering, Discount Usability Engineering, User Testing, Formative 
Evaluation, Summative Evaluation, Staffing, Metrics, CAUSE, Computer-Aided Usability Engineering">
        

        

        <meta name="robots" content="noarchive">
        <meta name="viewport" content="width=device-width">

        <link rel="search" type="application/opensearchdescription+xml" title="NN/g Search" href="../../opensearch.xml" />
        <link rel="shortcut icon" href="http://media.nngroup.com/static/img/favicon.ico" />

        <link rel="stylesheet" href="https://media.nngroup.com/static/vendor/font-awesome/css/font-awesome.min.css">

        
        <link rel="stylesheet" href="https://media.nngroup.com/static/css/7232a960f1df.css" type="text/css" />
        <link rel="stylesheet" href="http://fast.fonts.com/t/1.css?apiType=css&amp;projectid=c5846260-9def-46ec-9407-0f1349bd2d64" type="text/css">

        <script src="https://media.nngroup.com/static/vendor/modernizr/js/modernizr.min.js"></script>

        <script type="text/javascript" src="https://media.nngroup.com/static/js/3283be5def74.js"></script>
        

        

        
        
        <!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=229034,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('http://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->


<!-- Google Analytics Tracking Code for http://nngroup.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23132681-1', 'nngroup.com');
  ga('require', 'linkid', 'https://www.nngroup.com/articles/usability-labs/linkid.js');
  ga('require', 'displayfeatures');

// Optimizely Universal Analytics Integration
 window.optimizely = window.optimizely || [];
 window.optimizely.push("activateUniversalAnalytics");

  ga('send', 'pageview');

</script>
<script>
/**
* Function to track a click on an outbound link in Google Analytics.
*/
function trackOutboundLink(label, url) {
    if(window.ga && ga.loaded) {
        ga('send', 'event', 'outbound', 'click', label, {'hitCallback': function(){document.location = url;}
        });
    } else {
        document.location = url;
    }
}
/**
* Function to track a click on a link in the article announcement area.
*/
$('#articleAnnouncement a').on('click', function(){
  ga('send', 'event', 'link', 'click', 'articleAnnouncement');
});
</script>

<script type="text/javascript">
 setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2421.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



<!-- Hotjar Tracking Code for http://nngroup.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:121605,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

        <script src="https://media.nngroup.com/static/js/analytics/track_outbound_link.js"></script>
        
        
        
    </head>
    <body class="location-articles articles-detail">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
    <header id="page-header">
        <div id="cart" class="shopping-cart mobile-cart show-for-small-down"></div>
        <div class="row">
            <div id="masthead" class="masthead small-12 medium-7 column">

                <!-- The button to toggles menu on small screen -->

                <h1><a href="../../index.php"><img src="http://media.nngroup.com/static/img/logo-wordmark.png" width="74" height="30"> Nielsen <span class="norman">Norman</span> Group</a></h1>
                <p class="tagline show-for-medium-up">Evidence-Based User Experience Research, Training, and Consulting</p>

                <!-- The button to toggles form search on small screen -->


                <div style="clear: right;width:  100%;height: 1px;"></div><?php define('ROOT_PATH',$_SERVER["DOCUMENT_ROOT"] . '/apps/navigation_styles/navigations/2'); ?><?php include(ROOT_PATH)."/navigation.php"; ?>

                <!-- Drop down menu on small screen -->
                <div id="hideNav" class="show-for-small-down">
                  
                  <ul class="menu-content">
                    <li><a href="../../index.php">Home</a></li>
                    <li class="has-submenu">
                      <a>Training</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../training/index.php">Overview</a></li>
                        
<li id="nav-ux-conference"><a href="../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../online-seminars/index.php"><span>Online Seminars</span></a></li>

                      </ul>
                    </li>
                    <li class="has-submenu">
                      <a>Consulting</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
  <li><a href="../../consulting/index.php">Overview</a></li>
  
  <li><a href="../../consulting/benchmarking/index.php">Benchmarking</a></li>
  
  <li><a href="../../consulting/expert-design-review/index.php">Expert Design Review</a></li>
  
  <li><a href="../../consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
  
  <li><a href="../../consulting/usability-testing/index.php">Usability Testing </a></li>
  
  <li><a href="../../consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
  
  <li><a href="../../in-house-training/index.php">In-House Training</a></li>
</ul>

                    </li>

                    <li><a href="../../reports/index.php">Reports</a></li>
                    <li><a href="../index.php">Articles</a></li>
                    <li class="has-submenu"><a>About NN/G</a><i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../about/index.php">Overview</a></li>
                        <li><a href="../../people/index.php">People</a></li>
                        <li><a href="../../about/why-nng/index.php">Why NN/g?</a></li>
                        <li><a href="../../about/contact/index.php">Contact</a></li>
                        <li><a href="../../news/index.php">News</a></li>
                        <li><a href="../../about/history/index.php">History</a></li>
                        <li><a href="../../books/index.php">Books</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                </div>

                <hr class="header-divider show-for-small-only"/>


            </div>
       </div>

        <div id="hideNav">            <nav class="primary-nav small-12 column">
                <ul>
                    <li><a class="navitem" id="nav-home" href="../../index.php">Home</a></li>
                    <li><a class="navitem" id="nav-training" href="../../training/index.php">Training</a></li>
                    <li><a class="navitem" id="nav-consulting" href="../../consulting/index.php">Consulting</a></li>
                    <li><a class="navitem" id="nav-reports" href="../../reports/index.php">Reports</a></li>
                    <li><a class="navitem" id="nav-articles" href="../index.php">Articles</a></li>
                    <li><a class="navitem" id="nav-about" href="../../about/index.php">About NN/G</a></li>
                </ul>
            </nav>
            <nav class="secondary-nav small-12 medium-12 large-12 columns">
                
                
            </nav>
        </div>
    </header>
    <div id="main" class="l-content" onclick="closeNav(event)"><span id="preventClose" style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776; Menu</span>
    
  

  

  <div class="row">
    
      
      <section class="small-12 medium-3 columns collapsable-content l-navrail articles-left-column">
        <h1 class="show-for-small-down collapse-link mobile-browse-header">Browse by Topic and Author</h1>
          <div class="row collapsable">

            
            <div class="small-6 medium-12 columns">
              <section id="article-popular-topics" style="display: none;">
                
                <h1>Topics</h1>
                <ul id="article-popular-topics">
                    
                    <li>
                        
                        <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/intranets/all/index.php">Intranets</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/user-testing/all/index.php">User Testing</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                        
                    </li>
                    
                    <li><a href="#" id="article-see-all-topics"><strong>See all topics&hellip;</strong></a></li>
                </ul>
                
              </section>
              <section id="article-all-topics" style="display: none;">
                
                <h1>
                  <span class="show-for-medium-up">All Article Topics</span>
                  <span class="show-for-small-down">All Topics</span>
                  (<a href="#" id="article-hide-all-topics">hide</a>)
                </h1>
                <ul>
                  
                  <li>
                    
                    <a href="../../topic/accessibility/all/index.php">Accessibility</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/agile/all/index.php">Agile</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/analytics-and-metrics/all/index.php">Analytics &amp; Metrics</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/applications/all/index.php">Application Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/b2b-websites/all/index.php">B2B Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/behavior-patterns/all/index.php">Behavior Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/branding/all/index.php">Branding</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/content-strategy/all/index.php">Content Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/corporate-websites/all/index.php">Corporate Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/design-patterns/all/index.php">Design Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-design-process/all/index.php">Design Process</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/email/all/index.php">Email</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/eyetracking/all/index.php">Eyetracking</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/heuristic-evaluation/all/index.php">Heuristic Evaluation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/human-computer-interaction/all/index.php">Human Computer Interaction</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-ideation/all/index.php">Ideation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/information-architecture/all/index.php">Information Architecture</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/interaction-design/all/index.php">Interaction Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/international-users/all/index.php">International Users</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/intranets/all/index.php">Intranets</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-management/all/index.php">Management</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/navigation/all/index.php">Navigation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/non-profit-websites/all/index.php">Non-Profit Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/personas/all/index.php">Personas</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/persuasive-design/all/index.php">Persuasive Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/prototyping/all/index.php">Prototyping</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/psychology-and-ux/all/index.php">Psychology and UX</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/research-methods/all/index.php">Research Methods</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/search/all/index.php">Search</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/social-media/all/index.php">Social Media</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/strategy/all/index.php">Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/user-testing/all/index.php">User Testing</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-teams/all/index.php">UX Teams</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/visual-design/all/index.php">Visual Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/writing-web/all/index.php">Writing for the Web</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/young-users/all/index.php">Young Users</a>
                    
                  </li>
                  
                </ul>
                
              </section>
            </div>
            <script>
              /* Determine which topics list will be displayed: the full or the
               * partial.  We are inlining this close to the list itself to
               * prevent rendering delays on the client
               */
              var topic_list = $.cookie("article-topics");

              var popular_topics = $('#article-popular-topics');
              var all_topics = $('#article-all-topics');

              if(topic_list == 1) {
                  popular_topics.hide();
                  all_topics.show();
              } else {
                  popular_topics.show();
                  all_topics.hide();
              }
            </script>
            

            
            <div class="small-6 medium-12 columns">
              
              <section id="article-initial-authors">
                  <h1>Author</h1>
                  <ul>
                    
                    <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                    
                    <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                    
                    <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                    
                    <li><a href="#" onClick="$('#article-initial-authors').hide(); $('#article-all-authors').show();"><strong>See all authors&hellip;</strong></a></li>
                  </ul>
              </section>
              <section id="article-all-authors" style="display: none;">
                <h1>
                  All Authors
                  (<a href="#" id="article-hide-all-authors">hide</a>)
                </h1>
                <ul>
                  
                  <li><a href="../author/raluca-budiu/index.php">Raluca Budiu</a></li>
                  
                  <li><a href="../author/patty-caya/index.php">Patty Caya</a></li>
                  
                  <li><a href="../author/susan-farrell/index.php">Susan Farrell</a></li>
                  
                  <li><a href="../author/therese-fessenden/index.php">Therese Fessenden</a></li>
                  
                  <li><a href="../author/kim-flaherty/index.php">Kim Flaherty</a></li>
                  
                  <li><a href="../author/sarah-gibbons/index.php">Sarah Gibbons</a></li>
                  
                  <li><a href="../author/aurora-harley/index.php">Aurora Harley</a></li>
                  
                  <li><a href="../author/kate-kaplan/index.php">Kate Kaplan</a></li>
                  
                  <li><a href="../author/page-laubheimer/index.php">Page Laubheimer</a></li>
                  
                  <li><a href="../author/angela-li/index.php">Angie Li</a></li>
                  
                  <li><a href="../author/hoa-loranger/index.php">Hoa Loranger</a></li>
                  
                  <li><a href="../author/kate-meyer/index.php">Kate Meyer</a></li>
                  
                  <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                  
                  <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                  
                  <li><a href="../author/kara-pernice/index.php">Kara Pernice</a></li>
                  
                  <li><a href="../author/christian-rohrer/index.php">Christian Rohrer</a></li>
                  
                  <li><a href="../author/amy-schade/index.php">Amy Schade</a></li>
                  
                  <li><a href="../author/katie-sherwin/index.php">Katie Sherwin</a></li>
                  
                  <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                  
                  <li><a href="../author/kathryn-whitenton/index.php">Kathryn Whitenton</a></li>
                  
                </ul>
              </section>
              
            </div>
            
          </div>

          
          <section class="hide-for-small-down">
            
              
              <h1>Recent Articles</h1>
              
              <ul>
                  
                  <li><a href="../seamless-cross-channel/index.php">Seamlessness in the Omnichannel User Experience</a></li>
                  
                  <li><a href="../brand-intention-interpretation/index.php">Experience Design: Bridging Brand Intention and Brand Interpretation</a></li>
                  
                  <li><a href="../eyetracking-tasks-efficient-scanning/index.php">Scanning Patterns on the Web Are Optimized for the Current Task</a></li>
                  
                  <li><a href="../flat-design-best-practices/index.php">Flat-Design Best Practices</a></li>
                  
                  <li><a href="../ux-user-stories/index.php">Accounting for UX Work with User Stories in Agile Projects</a></li>
                  
                  <li><a href="../index.php"><strong>See all articles&hellip;</strong></a></li>
              </ul>
              
            
          </section>
          

          
          
          <section class="hide-for-small-down">
            <h1>Popular Articles</h1>
            
            <ul>
                
                <li><a href="../usability-101-introduction-to-usability/index.php">Usability 101: Introduction to Usability</a></li>
                
                <li><a href="../top-10-mistakes-web-design/index.php">Top 10 Mistakes in Web Design</a></li>
                
                <li><a href="../how-users-read-on-the-web/index.php">How Users Read on the Web</a></li>
                
                <li><a href="../f-shaped-pattern-reading-web-content/index.php">F-Shaped Pattern For Reading Web Content</a></li>
                
                <li><a href="../ten-usability-heuristics/index.php">10 Usability Heuristics for User Interface Design</a></li>
                
                <li><a href="../intranet-design-2016/index.php">10 Best Intranets of 2016</a></li>
                
                <li><a href="../which-ux-research-methods/index.php">When to Use Which User-Experience Research Methods</a></li>
                
                <li><a href="../response-times-3-important-limits/index.php">Response Times: The 3 Important Limits</a></li>
                
                <li><a href="../why-you-only-need-to-test-with-5-users/index.php">Why You Only Need to Test with 5 Users</a></li>
                
                <li><a href="../page-fold-manifesto/index.php">The Fold Manifesto: Why the Page Fold Still Matters</a></li>
                
            </ul>
          </section>
          
          

          
            <section class="hide-for-small-down">
              
            </section>
          

      </section>
      

      
        <div class="small-12 medium-9 columns content-body">
          




<article class="l-subsection alertbox-article-content article-detail-bl">
  
  <header>
    <div class="l-subsection">
      <h1>Usability Laboratories: A 1994 Survey</h1>
      <div class="author-meta">by
        
          
            
            <a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a>
            
          
        on  February 1, 1994
        
      </div>

      

      
      <div class="topic-links">
        Topics:
        
          <ul>

  <li><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction</a></li>

  <li><a href="../../topic/research-methods/index.php">Research Methods</a></li>

  <li><a href="../../topic/user-testing/index.php">User Testing</a></li>

</ul>
        
      </div>
      
    </div>
  </header>
  

  
  
  <section>
    <p class="lede"><strong>Summary:</strong> This article provides a table with summary statistics for the thirteen usability laboratories described in the papers in this special issue. It also gives an introduction to the main uses of usability laboratories in  usability engineering and surveys some of the issues related to practical use of user testing and CAUSE tools for computer-aided usability engineering.</p>
    <br />
  </section>
  
  

  
  <section id="article-body"><html><body><p><em>Orignially published as: Nielsen, J. (1994). Usability laboratories. Behaviour &amp; Information Technology <strong>13</strong>, 1&amp;2, 3–8.</em></p>
<p><em><strong>Comment added December 1996: </strong> This article was written in 1994 to summarize a special issue on usability laboratories I edited for the journal Behaviour &amp; Information Technology. The special issue itself is well worth reading if you can get hold of it (many large technical libraries subscribe to the journal and should have the issue). The special issue on usability labs was published as a double issue: Behaviour &amp; Information Technology, vol. <strong> 13</strong>, nos. 1-2, January-April 1994.</em></p>
<p>Usability is playing a steadily more important role in software development. This can be seen in many ways, including the growing budgets for usability engineering. In 1971 Shackel estimated that a reasonable share for usability budgets for non-military systems was about 3% (Shackel 1971). Later, in 1989, a study by Wasserman (1989) of several corporations found that "many leading companies" allocated about 4-6% of their research and development staff to interface design and usability work. Finally, in January 1993, I surveyed 31 development projects and found that the median share of their budgets allocated to usability engineering was 6% (Nielsen 1993). Thus, usability budgets have been steadily growing. Other indications of the added emphasis on usability is the increasing number of personal computer trade press magazines that include usability measures in their reviews and the overwhelming response to the call for papers for this special issue of <em> Behaviour &amp; Information Technology </em> on usability laboratories</p>
<p>Assuming that a company has decided to improve the usability of its products, what should it do? Even though this is a special issue on usability laboratories, I am not sure that the answer is to build a usability lab right away. Even though usability laboratories are great resources for usability engineering groups, it is possible to get started with simpler usability methods that can be used immediately on current projects without having to wait for the lab to be built. See Nielsen (1993) for an overview of these methods, which are often referred to as "discount usability engineering." Almost always, the result of applying simple usability methods to current projects is a blinding insight that usability methods improve products substantially, making it hard to believe how anybody could ever develop user interfaces without usability engineering. Unfortunately, many people still do just that, and we need cheap and simple methods to enable them to overcome the initial hurdle of starting to use usability engineering.</p>
<p>Once a company has recognized the benefits of the systematic use of usability methods to improve its products, management often decides to make usability a permanent part of the development process and to establish resources to facilitate the use of usability methods by the various project teams. Two such typical usability resources are the usability group and the usability laboratory.</p>
<p>The staffing, management, and organizational placement of usability groups are important issues that must unfortunately be left unresolved here since virtually no research is available to resolve them. Suffice it to say that usability laboratories as a physical resource can be used by the usability groups almost no matter how they are organized. For example, one of the main schisms in the organizational placement of usability specialists is whether to centralize them in a single human factors department or to distribute them as specialized team members of the individual development projects. Some of the arguments in favor of centralized usability departments are that they are better at attracting talented usability staff because of their higher visibility; that they can nurture the special skills of usability specialists by providing an environment focused on usability issues where new techniques are discussed and developed; that they provide a clear management chain (with the ensuing career paths) for usability staff; that they can maintain corporate interface standards and serve as "interface police" to ensure consistency; and that they can take an objective view of the user interfaces they are evaluating since the interfaces come from outside departments. Some of the arguments in favor of distributed usability staff is that it is more satisfying for usability specialists to work for an extended time on a single product than to consult briefly on multiple products; that usability specialists on a product team are more likely to contribute to the design efforts throughout the lifecycle rather than just clean up the GUI; that some domains are so complicated that one needs to "live" with the product team for an extended period of time to be able to contribute; that usability specialists will only be taken seriously by developers if both groups are part of the same team; and that the communication channels will be shorter (leading to greater productivity) the less organizational distance there is between the produ cers and the consumers of usability knowledge. In spite of the clear distinction between the two types of organizations and the great need to know more about their relative advantages and disadvantages, no research results are currently available to assess what circumstances should lead one to prefer one organization of usability groups over the other.</p>
<p>Interestingly, even though a centralized usability group is the prime candidate to manage a usability lab, the existence of a corporate usability lab is not necessarily an argument in favor of a centralized usability department. It is possible to have a lab supported by a small number of dedicated support staff even while it is being used by usability specialists from a large number of distributed groups. As noted by Dayton, Tudor, and Root in their paper on Bellcore's user-centered-design support center in this issue, a shared usability lab can even serve as a gathering point to provide some of the cross-fertilization and educational benefits to distributed usability specialists that others get from a centralized department.</p>
<p>Usability laboratories are typically used for user testing as discussed in most papers and summarized in the next paper by Salzman and Rivers, "Smoke and mirrors: Setting the stage for a successful usability test." There is no doubt that user testing is the main justification for most usability laboratories and that user testing is one of the foundations of usability engineering. Once a usability lab is in place, however, it becomes a convenient resource for many other kinds of usability activities such as focus groups or task analysis. Palmiter, Lynch, Lewis, and Stempski discuss several such non-test forms of data collection in their paper on "Breaking away from the conventional usability lab," and Zirkler and Ballman describe ways of combining focus groups and traditional testing in their paper on "Usability testing in a competitive market." Usability laboratories are sometimes also used to record design sessions, though this is mostly done as part of research projects and not as part of practical development projects. One exception is the use of participatory design sessions using methods like PICTIVE (Muller, Wildman, and White 1993) where several users and designers sketch out interface designs using bits of colored paper that is moved around on a desk. With this design technique, much of the design information is never written down, so capturing the dynamics of the design session on video can provide a valuable record for later reference.</p>
<p>Usability laboratories can also be used for certain variants of heuristic evaluation (Nielsen 1994). Heuristic evaluation is based on having usability specialists or other evaluators inspect an interface to find usability problems that violate a list of established usability principles (the "heuristics") and does not normally require a special laboratory since the evaluators can work anywhere and are normally supposed to do so individually. Sometimes, however, one wants to have an observer present to log the usability problems discovered by the evaluators so that they do not need to spend time and effort on writing up a report. It may be valuable for this observer to have access to a video record of the evaluation session, and it may also sometimes be advantageous to have developers or other project representatives observe a heuristic evaluation session from behind the one-way mirror in a usability lab. In general, though, only a small minority of heuristic evaluation sessions take place in usability laboratories.</p>
<table align="center" style="border-style: solid; border-width: 1px; border-collapse: collapse; padding: 19px;">
<caption align="bottom"><strong>Table 1. </strong><br/>
<em>Overview of the usability laboratories discussed in this special issue. For each company, the usability laboratory described in this table is the one discussed in the paper in this special issue by authors from that company. For IBM, the lab is the one described by Fath et al., and for SunSoft, the lab is the one described by Rohn.<br/>
	Notes: The first number in the entry for "usability staff supporting vs. using the lab indicates" the headcount of the staff that is dedicated to keeping the lab up and running. The second number in this entry indicates the number of usability specialists who share the lab for their work, even though they may not all be using it full-time.<br/>
	One square meter is 10.76 square feet. </em></caption>
<tbody>
<tr>
<th style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">Company Name</th>
<th style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">Main Product</th>
<th style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">Other Labs in Company?</th>
<th style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">Date of First Usability Lab in Company</th>
<th style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">Floor Space of Typical Subject Room in Sq. Meters</th>
<th style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">Floor Space of Total Lab Area in Sq. Meters</th>
<th style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">Number of Rooms (Subject Rooms, Control Rooms, etc.)</th>
<th style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">Number of Cameras in Typical Subject Room</th>
<th style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">Scan Converter Used to Directly Tape Screen Image?</th>
<th style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">One-Way Mirror?</th>
<th style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">Usability Staff Supporting vs. Utilizing Lab</th>
</tr>
<tr>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: left">Ameritech</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">Communications service</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">No</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">1989</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">12.5</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">237</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">7</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">2</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">Yes</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">Yes</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">1 / 10</td>
</tr>
<tr>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: left">Bellcore</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">Telco software</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">Yes</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">1985</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">12.3</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">121</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">7</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">2</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">No</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">Yes</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">0.3 / 30</td>
</tr>
<tr>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: left">BT (British Telecom)</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">Telephone service</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">No</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">1988</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">40</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">96</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">3</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">3</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">Yes</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">Yes</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">0.5 / 70</td>
</tr>
<tr>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: left">IBM</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">Computer systems</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">Yes</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">1981</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">11.7</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">165</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">14</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">2</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">No</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">Yes</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">0.1 / 4</td>
</tr>
<tr>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: left">MAYA Design Group</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">Design consultants</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">No</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">1990</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">8.8</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">42.9</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">3</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">2</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">No</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">Yes</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">3 / 12</td>
</tr>
<tr>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: left">Microsoft Corp.</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">PC software</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">No</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">1989</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">10.8</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">181.3</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">19</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">2</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">Yes</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">Yes</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">4 / 22</td>
</tr>
<tr>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: left">NCR</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">Computer systems</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">Yes</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">1966</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">13.4</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">31.2</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">3</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">2</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">Yes</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">Yes</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">2 / 15</td>
</tr>
<tr>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: left">Philips, Corp. Design</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">Consumer electronics</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">Yes</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">1990</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">30</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">40</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">2</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">3</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">No</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">Yes</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">0.25 / 10</td>
</tr>
<tr>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: left">Philips, IPO</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">Consumer electronics</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">Yes</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">1990</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">9</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">35</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">3</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">2</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">No</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">No</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">1 / 25</td>
</tr>
<tr>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: left">SAP</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">Enterprise business apps</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">No</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">1992</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">37</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">63</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">2</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">1</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">No</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">Yes</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">2 / 12</td>
</tr>
<tr>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: left">SunSoft</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">Workstation software</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">No</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">1988</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">25.1</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">202.3</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">8</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">3</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">Yes</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">Yes</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">3.5 / 8</td>
</tr>
<tr>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: left">Symantec</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">PC software</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">No</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">1992</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">23.8</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">47.6</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">2</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">2</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">Yes</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">Yes</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">1 / 1</td>
</tr>
<tr>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: left">Taligent</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">PC operating systems</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">No</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">1992</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">13.4</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">26.8</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">2</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">2</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">No</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">Yes</td>
<td style="border-style: solid; border-width: 1px; padding: 2px; text-align: center">1 / 2</td>
</tr>
<tr class="summaryrow">
<td colspan="2" style="border-style: solid; border-width: 2px; padding: 2px; text-align: left"><strong>Mean </strong></td>
<td style="border-style: solid; border-width: 2px; padding: 2px; text-align: center">38%</td>
<td style="border-style: solid; border-width: 2px; padding: 2px; text-align: center">1987</td>
<td style="border-style: solid; border-width: 2px; padding: 2px; text-align: center">19.1</td>
<td style="border-style: solid; border-width: 2px; padding: 2px; text-align: center">99.2</td>
<td style="border-style: solid; border-width: 2px; padding: 2px; text-align: center">5.8</td>
<td style="border-style: solid; border-width: 2px; padding: 2px; text-align: center">2.2</td>
<td style="border-style: solid; border-width: 2px; padding: 2px; text-align: center">46%</td>
<td style="border-style: solid; border-width: 2px; padding: 2px; text-align: center">92%</td>
<td style="border-style: solid; border-width: 2px; padding: 2px; text-align: center">1.5 / 17.0</td>
</tr>
<tr class="summaryrow">
<td colspan="2" style="border-style: solid; border-width: 2px; padding: 2px; text-align: left"><strong>Median </strong></td>
<td style="border-style: solid; border-width: 2px; padding: 2px; text-align: center">No</td>
<td style="border-style: solid; border-width: 2px; padding: 2px; text-align: center">1989</td>
<td style="border-style: solid; border-width: 2px; padding: 2px; text-align: center">13.4</td>
<td style="border-style: solid; border-width: 2px; padding: 2px; text-align: center">63.8</td>
<td style="border-style: solid; border-width: 2px; padding: 2px; text-align: center">3</td>
<td style="border-style: solid; border-width: 2px; padding: 2px; text-align: center">2</td>
<td style="border-style: solid; border-width: 2px; padding: 2px; text-align: center">No</td>
<td style="border-style: solid; border-width: 2px; padding: 2px; text-align: center">Yes</td>
<td style="border-style: solid; border-width: 2px; padding: 2px; text-align: center">1 / 12</td>
</tr>
</tbody>
</table>
<hr/>
<p>The papers in this special issue describe thirteen usability laboratories that are summarized in Table 1. As noted in the table, 38% of the companies have other usability laboratories that are not represented in the table, so it should only be seen as representing a survey of usability labs and not as a complete listing of labs. It can be seen from the table that the defining characteristics of a usability laboratory seem to be video cameras (used in all the labs) and the one-way mirror (installed in 92% of the labs). The table also shows that usability laboratories are a fairly recent phenomenon, with the median year of the first usability lab in these companies being 1989. Of course, some companies have had usability laboratories for a long time. Also, we should note that companies in industries like aerospace and control room design have employed usability laboratories for many years even though they are not represented in the table. The papers in this issue are mostly from the computer and telecommunications industries, though de Vries, van Gelderen, and Brigham write about usability labs for consumer products at Philips. In general, the lessons described in the papers in this issue may apply most directly to the usability of information technology, but there is no reason to believe that most of the same methods could not be used for other types of products and services.</p>
<p>The table shows that the median usability laboratory has three rooms. Normally, the room distribution is a room for the test subject(s), a control room for the experimenter and other usability specialists involved in running the experiment and operating the recording and logging tools, and an "executive observation lounge" where additional staff can observe the test without interfering with either the subject or the experimenters. Sometimes additional rooms are used for waiting areas for subjects, and the larger labs also often have special video editing rooms to avoid occupying an entire test suite by using the control room facilities for editing rather than testing. The observers in the executive observation lounge may sometimes in fact be executives, but more commonly they are members of the development team who take the opportunity of the user test to get exposure to the users. Even though the video tape equipment in the labs are often used to produce very communicative highlight tapes of the most notable usability problems and colorful user utterances, there is still no substitute for observing a test live.</p>
<p>It can be seen from the table that some usability labs have a very large number of rooms. Often, this only means that multiple tests can be conducted in parallel, but sometimes larger facilities are used for experiments in computer-supported cooperative work, video conferencing, and other cases where multiple users have to be combined for a single test. Lund's paper on Ameritech's usability laboratory discusses this issue in further depth.</p>
<p>At this time of writing, only slightly less than half of the laboratories surveyed in the table used scan converters to make it possible to tape the computer screen image directly without having to point a camera at it. Scan converters have been somewhat expensive, but since they are dropping in price, their use can be expected to be more widespread in the future.</p>
<p>One of the major advantages of having a usability laboratory is that the incremental hurdle for user testing of a new product becomes fairly small since all the equipment is already in place in a dedicated room that is available for testing. This effect is important because of the compressed development schedules that often leave little time for delay. Thus, if usability testing can be done now and with minimal overhead, it will get done. Similarly, usability may get left out of a project if there is too much delay or effort involved before results become available. Because of this phenomenon, the support staff form a very important part of a usability laboratory in terms of keeping it up and running, stocked with supplies, and taking care of the practical details of recruiting and scheduling test users. In my opinion, the ratio of one support person to twelve usability specialists running tests that is shown as the median in Table 1 is actually too small. I believe that a higher number of support staff are well worth their cost in terms of more efficient usability work (which again leads to a larger amount of usability work being done).</p>
<p>The building of a usability laboratory involves a myriad of decisions and trade-offs. Most of the papers in this special issue touch upon several such issues, and the papers by Sazagari, Rohn, Uyeda, and Neugebauer and Spielmann are particularly detailed. The papers by Lucas and Fisher; Dayton, Tudor, and Root; Lund; and Blatt, Jacobson, and Miller take the detailed needs analysis one step further and describe how they redesigned their second-generation usability laboratories based on experience with older labs and using trusted user-centered design principles to find out what usability specialists really need in their lab.</p>
<p>A usability laboratory does not necessarily need to be a fixed facility in a given set of rooms constructed for the purpose. Szczur describes how she used a regular conference room at NASA as a low-budget usability lab by using part of the room for the subjects and part of the room for the observers. Several papers describe "portable usability labs" with video kits and other data logging equipment that can be brought to the field to allow testing, and Zirkler and Ballman emphasize the need to visit customer sites when assessing the usability of systems like their specialized databases for users in the legal and financial communities. Smilowitz, Darnell, and Benson compare standard usability testing in the lab with Beta testing done by having customers test the software at their own sites and report usability problems on their own without supervision by a usability specialist. Even though the Beta tests did have some limitations (notably, that they were restricted to the very last stages of the development lifecycle), they did provide a cheap source of additional usability data that should be considered as a supplement to the lab-based sources.</p>
<p>In addition to the building and equipment of the usability laboratory, an important issue is obviously the actual methods used in running experiments in the lab. Many papers discuss methodology issues, and Fath, Mann, and Holzman provide detailed coverage of five main phases used in evaluation sessions in IBM's Atlanta usability laboratory: designing the evaluation, preparing for it, conducting it, analyzing the data, and reporting the results. One of their conclusions is that as much of the data reduction process as possible should be automated. Usability testing generates huge masses of raw data, and any meaningful conclusions have to be based on analyses that summarize the test events in a comprehensible manner. Hoiem and Sullivan provide detailed information about the integrated set of usability data collection and analysis tools built for Microsoft's lab. In general, it is characteristic for the state of the art that almost all CAUSE tools are homemade for the individual labs. There are probably two reasons for this: First, we still do not know enough about what computerized tools usability professionals actually need, and we know even less about how to make such tools sufficiently usable and efficient. Second, the market is still fairly small, though it is constantly growing and may one day support a wider variety of commercial offerings.</p>
<p>A classic form of CAUSE tool used in many usability labs is the event logger, which is used by an observer to record occurrences of events of interest as the user progresses through the experiment. Typically, events are automatically timestamped (and often linked to a videotape record of the event), and the logger also records the type of event as classified by the human observer in real time -- possibly with a supplementary natural language annotation. In addition to human-coded event logs, usability labs often produce keystroke logs of all user input and activity logs of higher-level dialogue actions (like menu selections, error messages, etc.). These logs can be subjected to many different kinds of analysis, including the sequential data analysis techniques discussed in the paper by Coumo.</p>
<p>Much user testing is simply aimed at generating qualitative insights that are communicated through lists of usability problems and highlights videos showing striking cases of user frustration. Such insights are sufficient for most practical usability engineering applications where the goal is the improvement of a user interface through iterative design. Often, there is no real reason to expend resources of gathering statistically significant data on a user interface that is known to contain major usability problems and has to be changed anyway. By using a probabilistic model of the finding of usability problems and an economic model of project management, Nielsen and Landauer (1993) found that one often gets the optimal cost-benefit ratio by <a href="../why-you-only-need-to-test-with-5-users/index.php"> using between three and five test users for each round of user testing</a>.</p>
<p>User testing with the goal of learning about the design to improve its next iteration falls in the category of formative evaluation. Sometimes, one wants to do summative evaluation that is quantitative in nature and results in numbers that can be compared across products. One case where summative evaluation is desired is the comparative product reviews produced by personal computer magazines like the ones discussed in Bawa's paper. Another application of competitive testing is the selection of recommended (or prescribed) software for a big company where the benefits in terms of reduced training and support and increased productivity are sufficiently large to warrant the investment of considerable resources on choosing the most usable product from the available offerings on the market. To my knowledge, not many user organizations currently perform such competitive usability testing before major software purchases, though there are a few that do. Also, it is becoming fairly common for software vendors to commission third-party usability consultants to perform comparative studies and then advertise the results if their own product wins. Finally, summative evaluation is sometimes used for regular software development projects when one wants to investigate whether a revised product has achieved a sufficient improvement in usability over the previous version. The paper by Bevan and Macleod describes a comprehensive set of measurement tools and principles developed as part of the ESPRIT MUSiC project on usability metrics.</p>
<p>Even though usability metrics can be very elaborate (for example, Bevan and Macleod describe the measurement of heart rate variability as a way of assessing the user's mental effort over time), it is also possible to have a "discount usability engineering" approach to usability metrics. As an example, Molich's paper presents a technique for keeping track of the number of "user interface disasters" (certain kinds of serious usability problems experienced by at least two users) as a simple, yet quantifiable measure of interface quality.</p>
<p>As shown by the contrast between the full-blown usability metrics and the simplistic count of interface disasters, there are many different possible approaches to usability engineering (Nielsen 1993). It is important to realize that it is possible to achieve major improvements in usability even if one does not utilize all the most advanced techniques and even if one has a fairly primitive usability lab (or sets up a temporary lab in a conference room as described by Szczur). The single-most important decision in usability engineering is simply to do it! The best intentions of some day building the perfect lab will result in exactly zero improvement in current products, and if the choice is between perfection or doing nothing, nothing will win every time. Luckily, it is possible to start small and then grow a usability effort over time as management discovers the huge benefits one normally gets (Ehrlich and Rohn 1994). It is in the nature of things that this special issue mostly has papers on leading usability laboratories from companies that have a better-than-average approach to usability engineering. This does not mean that people in less fortunate companies should abandon usability, it only means that they have something to strive for as they start small and gradually expand their usability groups and usability labs.</p>
<h3>Acknowledgments</h3>
<p>The main review burden in selecting the papers was shouldered by the following reviewers who had been especially selected for this special issue: Tomas Berns (Nomos Management AB), Linda I. Borghesani (The MITRE Corporation), Joseph S. Dumas (American Institutes for Research), Tom G. Gough (University of Leeds), Lovie Ann Melkus (IBM Consulting Group), James R. Miller (Apple Computer, Inc.), Michele Morris (BNR Europe Ltd.), Kenneth Nemire (Interface Technologies), Markku I. Nurminen (University of Turku), Diane J. Schiano (Interval Research Corporation), Sylvia Sheppard (NASA Goddard Space Flight Center), Nancy Storch (Lawrence Livermore National Laboratory), Paulus-Hubert Vossen (Fraunhofer-Institut IAO), and David Ziedman (Philips Interactive Media of America). Additional ad-hoc reviews were provided by: Rita M. Bush (Bellcore), Tom Dayton (Bellcore), Arye R. Ephrath (Bellcore), Richard D. Herring (Bellcore), Arnold M. Lund (Ameritech), Michael J. Muller (U S WEST Advanced Technologies), and Estela M. Tice (Bellcore).</p>
<h2>References</h2>
<ul>
<li>Bias, R. G. and Mayhew, D. J. (Eds.) 1994, <em><a href="http://www.amazon.com/exec/obidos/ISBN=0120958104/ref=nosim/useitcomusableinA/"> Cost-Justifying Usability</a></em> (Academic Press, Boston, MA).</li>
<li>Ehrlich, K. and Rohn, J. 1994, Cost-justification of usability engineering: A vendor's perspective, in Bias, R.G., and Mayhew, D.J. (Eds.), <em><a href="http://www.amazon.com/exec/obidos/ISBN=0120958104/ref=nosim/useitcomusablein/">Cost-Justifying Usability</a></em> (Academic Press, Boston, MA).</li>
<li>Muller, M. J., Wildman, D. M., and White, E. A. 1993, "Equal opportunity" PD using PICTIVE. <em> Communications of the ACM </em> <strong> 36</strong>, 4, 64-66.</li>
<li>Nielsen, J. 1993, <em><a href="../../books/usability-engineering/index.php">Usability Engineering</a></em> (Academic Press, Boston, MA).</li>
<li>Nielsen, J. 1994, Heuristic evaluation, in Nielsen, J., and Mack, R. L. (Eds.), <a href="../../books/usability-inspection-methods/index.php"> <em> Usability Inspection Methods</em></a>. (John Wiley &amp; Sons, New York, NY), 25-64.</li>
<li>Nielsen, J. and Landauer, T. K. 1993, A mathematical model of the finding of usability problems, <em> Proceedings of the ACM INTERCHI'93 Conference </em> (Amsterdam, the Netherlands, April 24-29), 206-213.</li>
<li>Shackel, B. 1971, Human factors in the P.L.A. meat handling automation scheme. A case study and some conclusions. <em> International Journal of Production Research </em> <strong> 9</strong>, 1, 95-121.</li>
<li>Wasserman, A. S. 1989, Redesigning Xerox: A design strategy based on operability. In Klemmer, E. T. (Ed.), <a href="http://www.amazon.com/exec/obidos/ISBN=0893915599/useitcomusablein/"><em> Ergonomics: Harness the Power of Human Factors in Your Business</em></a>, Ablex, Norwood, NJ. 7-44.</li>
</ul></section>
  

  
  <section>
    
        
    
  </section>
  
  
  
  
  

  


  <section>
    <br />
    <p class="share"><b>Share <span class="hide-for-small-down">this article</span>:</b> <a href="https://twitter.com/intent/tweet?url=http://www.nngroup.com/articles/usability-labs/&amp;text=Usability%20Laboratories:%20A%201994%20Survey&amp;via=nngroup">Twitter</a> | <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=http://www.nngroup.com/articles/usability-labs/&amp;title=Usability%20Laboratories:%20A%201994%20Survey&amp;source=Nielsen%20Norman%20Group">LinkedIn</a> | <a href="https://plus.google.com/share?url=http://www.nngroup.com/articles/usability-labs/">Google+</a> | <a href="mailto:?subject=NN/g Article: Usability Laboratories: A 1994 Survey&amp;body=http://www.nngroup.com/articles/usability-labs/">Email</a></p>
    <hr />
  </section>

  <footer class="l-subsection">
    
    
    <div class="learn-more">
      <h2>Learn More</h2>
      <p><a href="../subscribe/index.php">Subscribe to the weekly newsletter</a> to get notified about future articles.</p>
      </div>
    
    

    <div class="row show-for-small-down related-content">
      
      
      

      
      
        <div class="column small-12 topic-links">All articles about:
          <ul>
            
            <li><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction</a></li>
            
            <li><a href="../../topic/research-methods/index.php">Research Methods</a></li>
            
            <li><a href="../../topic/user-testing/index.php">User Testing</a></li>
            
          </ul>
        </div>
      
      

      
      
        <div class="column small-12">
          <h3>UX Conference Training Courses</h3>
           <ul class="no-bullet">
    
        <li><a href="../../courses/ux-basic-training/index.php">UX Basic Training</a></li>
    
        <li><a href="../../courses/journey-mapping/index.php">Journey Mapping to Understand Customer Needs</a></li>
    
        <li><a href="../../courses/measuring-ux/index.php">Measuring User Experience</a></li>
    
        <li><a href="../../courses/analytics-and-user-experience/index.php">Analytics and User Experience</a></li>
    
        <li><a href="../../courses/hci/index.php">User Interface Principles Every Designer Must Know</a></li>
    
</ul>
        </div>
      
      

      
      
        <div class="small-12 column">
          <h3>Research Reports</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../reports/paper-prototyping-training-video/index.php">Paper Prototyping Training Video</a></li>
              
            
              
                <li><a href="../../reports/user-experience-careers/index.php">User Experience Careers</a></li>
              
            
              
                <li><a href="../../reports/how-to-conduct-eyetracking-studies/index.php">How to Conduct Eyetracking Studies</a></li>
              
            
              
                <li><a href="../../reports/how-to-recruit-participants-usability-studies/index.php">How to Recruit Participants for Usability Studies</a></li>
              
            
              
                <li><a href="../../reports/how-to-conduct-usability-studies-accessibility/index.php">How to Conduct Usability Studies for Accessibility</a></li>
              
            
          </ul>
        </div>
      
      

      
      
        <div class="small-12 column">
          <h3>Online Seminars</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../online-seminars/mobile-user-testing/index.php">Mobile User Testing</a></li>
              
            
          </ul>
        </div>
      
      
    </div>

    <div class="hide-for-small-down row related-content">
      <div class="medium-6 columns">
        
        
          <div>
            <h3>Articles</h3>
            <ul class="no-bullet">
              
                
                <li><a href="../embarrassment/index.php">Computer-Assisted Embarrassment</a></li>
                
              
                
                <li><a href="../enhancement/index.php">The Role of Enhancement in Web Design</a></li>
                
              
                
                <li><a href="../icon-testing/index.php">Usability Testing of Icons</a></li>
                
              
                
                <li><a href="../goal-composition/index.php">Goal Composition: Extending Task Analysis to Predict Things People May Want to Do</a></li>
                
              
                
                <li><a href="../lazy-users/index.php">Why Designers Think Users Are Lazy: 3 Human Behaviors</a></li>
                
              
            </ul>
          </div>
        
        

        
          
          <div>
            <h3>Research Reports</h3>
            <ul class="no-bullet">
              
                
                  <li><a href="../../reports/paper-prototyping-training-video/index.php">Paper Prototyping Training Video</a></li>
                
              
                
                  <li><a href="../../reports/user-experience-careers/index.php">User Experience Careers</a></li>
                
              
                
                  <li><a href="../../reports/how-to-conduct-eyetracking-studies/index.php">How to Conduct Eyetracking Studies</a></li>
                
              
                
                  <li><a href="../../reports/how-to-recruit-participants-usability-studies/index.php">How to Recruit Participants for Usability Studies</a></li>
                
              
                
                  <li><a href="../../reports/how-to-conduct-usability-studies-accessibility/index.php">How to Conduct Usability Studies for Accessibility</a></li>
                
              
            </ul>
          </div>
          
        

      </div>
      <div class="medium-6 columns">
        
          
          <div>
            <h3>UX Conference Training Courses</h3>
             <ul class="no-bullet">
    
        <li><a href="../../courses/ux-basic-training/index.php">UX Basic Training</a></li>
    
        <li><a href="../../courses/journey-mapping/index.php">Journey Mapping to Understand Customer Needs</a></li>
    
        <li><a href="../../courses/measuring-ux/index.php">Measuring User Experience</a></li>
    
        <li><a href="../../courses/analytics-and-user-experience/index.php">Analytics and User Experience</a></li>
    
        <li><a href="../../courses/hci/index.php">User Interface Principles Every Designer Must Know</a></li>
    
</ul>
          </div>
          
        
        
          
          <div>
            <h3>Online Seminars</h3>
            <ul class="no-bullet">
            
                <li><a href="../../online-seminars/mobile-user-testing/index.php">Mobile User Testing</a></li>
            
            </ul>
          </div>
          
        
      </div>
    </div>
  </footer>
</article>

        </div>

      

    
  </div>
  <div class="row">
    
      <div class="column small-12 show-for-small-down l-navrail announcements">
        
          
        
      </div>
    
    
  </div>

    </div>
    




<footer id="page-footer">
    <div class="row hide-for-print">
        <div class="column small-12 medium-6">
            <section class="footer-popular">
                
                <h1>Popular Topics</h1>
                <div style="float: left;  width: 15em; display: block;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/applications/index.php">Application Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/e-commerce/index.php">E-Commerce Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/eyetracking/index.php">Eyetracking </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/information-architecture/index.php">Information Architecture </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/intranets/index.php">Intranet Design </a></li>
	<li><a href="../../topic/interaction-design/index.php">Interaction Design </a></li>
</ul>
</div>

<div style="float: left;  width: 15em; display: block; list-style: none;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/mobile-and-tablet-design/index.php">Mobile and Tablet Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/credibility-persuasion/index.php">Persuasive Design</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/research-methods/index.php">Research Methods</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/user-testing/index.php">User Testing </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/web-usability/index.php">Web Usability </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/writing-web/index.php">Web Writing &amp; Content Strategy </a></li>
</ul>
</div>

<p>&nbsp;</p>

                
            </section>
        </div>

        <div class="column small-12 medium-6">
            <div class="row">
                <div class="column small-6">
                    <section class="footer-about">
                        <h1>Follow Us</h1>
                        <ul>
	<li><a href="../subscribe/index.php">Subscribe to our weekly UX newsletter</a>,&nbsp;Jakob Nielsen&#39;s Alertbox</li>
	<li>Visit Don Norman&#39;s website, <a href="http://www.jnd.org/"> www.jnd.org </a></li>
	<li>Visit Bruce Tognazzini&#39;s website, <a href="http://www.asktog.com/"> www.asktog.com </a></li>
	<li style="padding-top: 6px;"><a class="twitter-follow-button" data-show-count="false" data-size="large" href="https://twitter.com/nngroup"><img alt="Follow @nngroup on Twitter" height="20" src="https://media.nngroup.com/media/editor/2016/12/30/twitter-follow-button.png" style="width: 117px; height: 20px;" width="117" /></a></li>
	<li style="padding-top: 6px;"><img alt="" height="14" src="https://media.nngroup.com/media/editor/2016/07/26/feed-icon-14x14.png" style="vertical-align:text-top;width:14px;margin-right:.25em;" width="14" /><a href="../../feed/rss/index.php">Subscribe to our RSS feed</a></li>
</ul>

                    </section>
                </div>

                <div class="column small-6">
                    <section class="footer-about right-column">
                        <h1>About
</h1>
                        <ul class="no-bullet">
	<li><a href="../../about/contact/index.php">Contact information</a></li>
	<li><a href="../../about/index.php">About Us</a></li>
	<li><a href="../../about/why-nng/index.php">Why NN/g? </a></li>
	<li><a href="../../return-policy/index.php">Return Policy</a></li>
</ul>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="copyright column small-12">
            <p><a href="../../copyright-and-reprint-info/index.php">Copyright</a> &copy; 1998-2017 Nielsen Norman Group, All Rights Reserved.</p>
        </div>
    </div>
</footer>


    


        <script src="https://media.nngroup.com/static/vendor/fastclick/js/fastclick.min.js"></script>
        <script src="https://media.nngroup.com/static/vendor/foundation/js/foundation.min.js"></script>
        <script src="http://media.nngroup.com/static/js/app.js"></script>
        <script src="http://media.nngroup.com/static/js/scroll-to-fixed.js"></script><script>var externalLinks = document.querySelectorAll('a[href^="http"]');for (var i = externalLinks.length-1; i >= 0; i--) {externalLinks[i].addEventListener("click", function(event) { alert("External links are disabled for this test."); event.preventDefault(); }, false);}var mailLinks = document.querySelectorAll('a[href^="mailto"]');for (var i = mailLinks.length-1; i >= 0; i--) {mailLinks[i].addEventListener("click", function(event) { alert("Email links are disabled for this test."); event.preventDefault(); }, false);}var externalForms = document.querySelectorAll('form');for (var i = externalForms.length-1; i >= 0; i--) {externalForms[i].addEventListener("submit", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false); externalForms[i].addEventListener("click", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false);}</script>

        <script>
            
            $(function() {
                $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
                $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
            });
        </script>

        
<script src="https://media.nngroup.com/static/js/articles.js"></script>


        
        <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1072699960;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072699960/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
    </body>

<!-- Mirrored from www.nngroup.com/articles/usability-labs/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:14:36 GMT -->
</html>
