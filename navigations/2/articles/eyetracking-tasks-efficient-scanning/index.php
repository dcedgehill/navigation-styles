<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->





    
<!-- Mirrored from www.nngroup.com/articles/eyetracking-tasks-efficient-scanning/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 14:54:51 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1026.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"licenseKey":"1a30ef9931","queueTime":0,"beacon":"bam.nr-data.net","errorBeacon":"bam.nr-data.net","applicationID":"2048992","transactionName":"NFxSMRBUX0EDUkIPDA0WdhAMVkVbDV8ZBxEXUFMJB0YfRAtUQRVZIktEDAFZVHYHRVcPD01eVRE=","applicationTime":408,"agent":""}</script>
        <title>Scanning Patterns on the Web Are Optimized for the Current Task</title><meta property="og:title" content="Scanning Patterns on the Web Are Optimized for the Current Task" />
  
        
        <meta name="description" content="How users attend to information on a page depends on their tasks and goals, as confirmed by new eyetracking research. Good design promotes efficient scanning.">
        <meta property="og:description" content="How users attend to information on a page depends on their tasks and goals, as confirmed by new eyetracking research. Good design promotes efficient scanning." />
        
  
        
	
        
        <meta name="keywords" content="Yarbus, eyetracking, eye tracking,  eye–mind hypothesis, fixation, saccade, bebe, jetblue, the unexpected visitor, task, attend, quiz, reading patterns, layer cake, page layout, consistency, motivated reading, star ratings, price, images, thumbnail, free examination, eye movement, gaze plot, gazeplot, efficient, lay of the land, comparing items, compare, motivated reading, commitment, usability test, writing tasks, task writing, heatmap, heat map, least effort, minimum interaction cost">
        

        

        <meta name="robots" content="noarchive">
        <meta name="viewport" content="width=device-width">

        <link rel="search" type="application/opensearchdescription+xml" title="NN/g Search" href="../../opensearch.xml" />
        <link rel="shortcut icon" href="http://media.nngroup.com/static/img/favicon.ico" />

        <link rel="stylesheet" href="https://media.nngroup.com/static/vendor/font-awesome/css/font-awesome.min.css">

        
        <link rel="stylesheet" href="https://media.nngroup.com/static/css/7232a960f1df.css" type="text/css" />
        <link rel="stylesheet" href="http://fast.fonts.com/t/1.css?apiType=css&amp;projectid=c5846260-9def-46ec-9407-0f1349bd2d64" type="text/css">

        <script src="https://media.nngroup.com/static/vendor/modernizr/js/modernizr.min.js"></script>

        <script type="text/javascript" src="https://media.nngroup.com/static/js/3283be5def74.js"></script>
        

        

        
        
        <!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=229034,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('http://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->


<!-- Google Analytics Tracking Code for http://nngroup.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23132681-1', 'nngroup.com');
  ga('require', 'linkid', 'https://www.nngroup.com/articles/eyetracking-tasks-efficient-scanning/linkid.js');
  ga('require', 'displayfeatures');

// Optimizely Universal Analytics Integration
 window.optimizely = window.optimizely || [];
 window.optimizely.push("activateUniversalAnalytics");

  ga('send', 'pageview');

</script>
<script>
/**
* Function to track a click on an outbound link in Google Analytics.
*/
function trackOutboundLink(label, url) {
    if(window.ga && ga.loaded) {
        ga('send', 'event', 'outbound', 'click', label, {'hitCallback': function(){document.location = url;}
        });
    } else {
        document.location = url;
    }
}
/**
* Function to track a click on a link in the article announcement area.
*/
$('#articleAnnouncement a').on('click', function(){
  ga('send', 'event', 'link', 'click', 'articleAnnouncement');
});
</script>

<script type="text/javascript">
 setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2421.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



<!-- Hotjar Tracking Code for http://nngroup.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:121605,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

        <script src="https://media.nngroup.com/static/js/analytics/track_outbound_link.js"></script>
        
        
        
    </head>
    <body class="location-articles articles-detail">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
    <header id="page-header">
        <div id="cart" class="shopping-cart mobile-cart show-for-small-down"></div>
        <div class="row">
            <div id="masthead" class="masthead small-12 medium-7 column">

                <!-- The button to toggles menu on small screen -->

                <h1><a href="../../index.php"><img src="http://media.nngroup.com/static/img/logo-wordmark.png" width="74" height="30"> Nielsen <span class="norman">Norman</span> Group</a></h1>
                <p class="tagline show-for-medium-up">Evidence-Based User Experience Research, Training, and Consulting</p>

                <!-- The button to toggles form search on small screen -->


                <div style="clear: right;width:  100%;height: 1px;"></div><?php define('ROOT_PATH',$_SERVER["DOCUMENT_ROOT"] . '/apps/navigation_styles/navigations/2'); ?><?php include(ROOT_PATH)."/navigation.php"; ?>

                <!-- Drop down menu on small screen -->
                <div id="hideNav" class="show-for-small-down">
                  
                  <ul class="menu-content">
                    <li><a href="../../index.php">Home</a></li>
                    <li class="has-submenu">
                      <a>Training</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../training/index.php">Overview</a></li>
                        
<li id="nav-ux-conference"><a href="../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../online-seminars/index.php"><span>Online Seminars</span></a></li>

                      </ul>
                    </li>
                    <li class="has-submenu">
                      <a>Consulting</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
  <li><a href="../../consulting/index.php">Overview</a></li>
  
  <li><a href="../../consulting/benchmarking/index.php">Benchmarking</a></li>
  
  <li><a href="../../consulting/expert-design-review/index.php">Expert Design Review</a></li>
  
  <li><a href="../../consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
  
  <li><a href="../../consulting/usability-testing/index.php">Usability Testing </a></li>
  
  <li><a href="../../consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
  
  <li><a href="../../in-house-training/index.php">In-House Training</a></li>
</ul>

                    </li>

                    <li><a href="../../reports/index.php">Reports</a></li>
                    <li><a href="../index.php">Articles</a></li>
                    <li class="has-submenu"><a>About NN/G</a><i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../about/index.php">Overview</a></li>
                        <li><a href="../../people/index.php">People</a></li>
                        <li><a href="../../about/why-nng/index.php">Why NN/g?</a></li>
                        <li><a href="../../about/contact/index.php">Contact</a></li>
                        <li><a href="../../news/index.php">News</a></li>
                        <li><a href="../../about/history/index.php">History</a></li>
                        <li><a href="../../books/index.php">Books</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                </div>

                <hr class="header-divider show-for-small-only"/>


            </div>
       </div>

        <div id="hideNav">            <nav class="primary-nav small-12 column">
                <ul>
                    <li><a class="navitem" id="nav-home" href="../../index.php">Home</a></li>
                    <li><a class="navitem" id="nav-training" href="../../training/index.php">Training</a></li>
                    <li><a class="navitem" id="nav-consulting" href="../../consulting/index.php">Consulting</a></li>
                    <li><a class="navitem" id="nav-reports" href="../../reports/index.php">Reports</a></li>
                    <li><a class="navitem" id="nav-articles" href="../index.php">Articles</a></li>
                    <li><a class="navitem" id="nav-about" href="../../about/index.php">About NN/G</a></li>
                </ul>
            </nav>
            <nav class="secondary-nav small-12 medium-12 large-12 columns">
                
                
            </nav>
        </div>
    </header>
    <div id="main" class="l-content" onclick="closeNav(event)"><span id="preventClose" style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776; Menu</span>
    
  

  

  <div class="row">
    
      
      <section class="small-12 medium-3 columns collapsable-content l-navrail articles-left-column">
        <h1 class="show-for-small-down collapse-link mobile-browse-header">Browse by Topic and Author</h1>
          <div class="row collapsable">

            
            <div class="small-6 medium-12 columns">
              <section id="article-popular-topics" style="display: none;">
                
                <h1>Topics</h1>
                <ul id="article-popular-topics">
                    
                    <li>
                        
                        <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/intranets/all/index.php">Intranets</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/user-testing/all/index.php">User Testing</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                        
                    </li>
                    
                    <li><a href="#" id="article-see-all-topics"><strong>See all topics&hellip;</strong></a></li>
                </ul>
                
              </section>
              <section id="article-all-topics" style="display: none;">
                
                <h1>
                  <span class="show-for-medium-up">All Article Topics</span>
                  <span class="show-for-small-down">All Topics</span>
                  (<a href="#" id="article-hide-all-topics">hide</a>)
                </h1>
                <ul>
                  
                  <li>
                    
                    <a href="../../topic/accessibility/all/index.php">Accessibility</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/agile/all/index.php">Agile</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/analytics-and-metrics/all/index.php">Analytics &amp; Metrics</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/applications/all/index.php">Application Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/b2b-websites/all/index.php">B2B Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/behavior-patterns/all/index.php">Behavior Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/branding/all/index.php">Branding</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/content-strategy/all/index.php">Content Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/corporate-websites/all/index.php">Corporate Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/design-patterns/all/index.php">Design Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-design-process/all/index.php">Design Process</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/email/all/index.php">Email</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/eyetracking/all/index.php">Eyetracking</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/heuristic-evaluation/all/index.php">Heuristic Evaluation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/human-computer-interaction/all/index.php">Human Computer Interaction</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-ideation/all/index.php">Ideation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/information-architecture/all/index.php">Information Architecture</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/interaction-design/all/index.php">Interaction Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/international-users/all/index.php">International Users</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/intranets/all/index.php">Intranets</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-management/all/index.php">Management</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/navigation/all/index.php">Navigation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/non-profit-websites/all/index.php">Non-Profit Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/personas/all/index.php">Personas</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/persuasive-design/all/index.php">Persuasive Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/prototyping/all/index.php">Prototyping</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/psychology-and-ux/all/index.php">Psychology and UX</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/research-methods/all/index.php">Research Methods</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/search/all/index.php">Search</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/social-media/all/index.php">Social Media</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/strategy/all/index.php">Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/user-testing/all/index.php">User Testing</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-teams/all/index.php">UX Teams</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/visual-design/all/index.php">Visual Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/writing-web/all/index.php">Writing for the Web</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/young-users/all/index.php">Young Users</a>
                    
                  </li>
                  
                </ul>
                
              </section>
            </div>
            <script>
              /* Determine which topics list will be displayed: the full or the
               * partial.  We are inlining this close to the list itself to
               * prevent rendering delays on the client
               */
              var topic_list = $.cookie("article-topics");

              var popular_topics = $('#article-popular-topics');
              var all_topics = $('#article-all-topics');

              if(topic_list == 1) {
                  popular_topics.hide();
                  all_topics.show();
              } else {
                  popular_topics.show();
                  all_topics.hide();
              }
            </script>
            

            
            <div class="small-6 medium-12 columns">
              
              <section id="article-initial-authors">
                  <h1>Author</h1>
                  <ul>
                    
                    <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                    
                    <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                    
                    <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                    
                    <li><a href="#" onClick="$('#article-initial-authors').hide(); $('#article-all-authors').show();"><strong>See all authors&hellip;</strong></a></li>
                  </ul>
              </section>
              <section id="article-all-authors" style="display: none;">
                <h1>
                  All Authors
                  (<a href="#" id="article-hide-all-authors">hide</a>)
                </h1>
                <ul>
                  
                  <li><a href="../author/raluca-budiu/index.php">Raluca Budiu</a></li>
                  
                  <li><a href="../author/patty-caya/index.php">Patty Caya</a></li>
                  
                  <li><a href="../author/susan-farrell/index.php">Susan Farrell</a></li>
                  
                  <li><a href="../author/therese-fessenden/index.php">Therese Fessenden</a></li>
                  
                  <li><a href="../author/kim-flaherty/index.php">Kim Flaherty</a></li>
                  
                  <li><a href="../author/sarah-gibbons/index.php">Sarah Gibbons</a></li>
                  
                  <li><a href="../author/aurora-harley/index.php">Aurora Harley</a></li>
                  
                  <li><a href="../author/kate-kaplan/index.php">Kate Kaplan</a></li>
                  
                  <li><a href="../author/page-laubheimer/index.php">Page Laubheimer</a></li>
                  
                  <li><a href="../author/angela-li/index.php">Angie Li</a></li>
                  
                  <li><a href="../author/hoa-loranger/index.php">Hoa Loranger</a></li>
                  
                  <li><a href="../author/kate-meyer/index.php">Kate Meyer</a></li>
                  
                  <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                  
                  <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                  
                  <li><a href="../author/kara-pernice/index.php">Kara Pernice</a></li>
                  
                  <li><a href="../author/christian-rohrer/index.php">Christian Rohrer</a></li>
                  
                  <li><a href="../author/amy-schade/index.php">Amy Schade</a></li>
                  
                  <li><a href="../author/katie-sherwin/index.php">Katie Sherwin</a></li>
                  
                  <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                  
                  <li><a href="../author/kathryn-whitenton/index.php">Kathryn Whitenton</a></li>
                  
                </ul>
              </section>
              
            </div>
            
          </div>

          
          <section class="hide-for-small-down">
            
              
              <h1>Recent Articles</h1>
              
              <ul>
                  
                  <li><a href="../seamless-cross-channel/index.php">Seamlessness in the Omnichannel User Experience</a></li>
                  
                  <li><a href="../brand-intention-interpretation/index.php">Experience Design: Bridging Brand Intention and Brand Interpretation</a></li>
                  
                  <li><a href="index.php">Scanning Patterns on the Web Are Optimized for the Current Task</a></li>
                  
                  <li><a href="../flat-design-best-practices/index.php">Flat-Design Best Practices</a></li>
                  
                  <li><a href="../ux-user-stories/index.php">Accounting for UX Work with User Stories in Agile Projects</a></li>
                  
                  <li><a href="../index.php"><strong>See all articles&hellip;</strong></a></li>
              </ul>
              
            
          </section>
          

          
          
          <section class="hide-for-small-down">
            <h1>Popular Articles</h1>
            
            <ul>
                
                <li><a href="../usability-101-introduction-to-usability/index.php">Usability 101: Introduction to Usability</a></li>
                
                <li><a href="../top-10-mistakes-web-design/index.php">Top 10 Mistakes in Web Design</a></li>
                
                <li><a href="../how-users-read-on-the-web/index.php">How Users Read on the Web</a></li>
                
                <li><a href="../f-shaped-pattern-reading-web-content/index.php">F-Shaped Pattern For Reading Web Content</a></li>
                
                <li><a href="../ten-usability-heuristics/index.php">10 Usability Heuristics for User Interface Design</a></li>
                
                <li><a href="../intranet-design-2016/index.php">10 Best Intranets of 2016</a></li>
                
                <li><a href="../which-ux-research-methods/index.php">When to Use Which User-Experience Research Methods</a></li>
                
                <li><a href="../response-times-3-important-limits/index.php">Response Times: The 3 Important Limits</a></li>
                
                <li><a href="../why-you-only-need-to-test-with-5-users/index.php">Why You Only Need to Test with 5 Users</a></li>
                
                <li><a href="../page-fold-manifesto/index.php">The Fold Manifesto: Why the Page Fold Still Matters</a></li>
                
            </ul>
          </section>
          
          

          
            <section class="hide-for-small-down">
              
            </section>
          

      </section>
      

      
        <div class="small-12 medium-9 columns content-body">
          




<article class="l-subsection alertbox-article-content article-detail-bl">
  
  <header>
    <div class="l-subsection">
      <h1>Scanning Patterns on the Web Are Optimized for the Current Task</h1>
      <div class="author-meta">by
        
          
            
            <a href="../author/kara-pernice/index.php">Kara Pernice</a>
            
          
        on  March 19, 2017
        
      </div>

      

      
      <div class="topic-links">
        Topics:
        
          <ul>

  <li><a href="../../topic/eyetracking/index.php">Eyetracking</a></li>

  <li><a href="../../topic/behavior-patterns/index.php">Behavior Patterns</a></li>

  <li><a href="../../topic/user-testing/index.php">User Testing</a></li>

</ul>
        
      </div>
      
    </div>
  </header>
  

  
  
  <section>
    <p class="lede"><strong>Summary:</strong> How users attend to information on a page depends on their tasks and goals, as confirmed by new eyetracking research. Good design promotes efficient scanning. In usability studies, (biased) task formulation may tip users to discover features.</p>
    <br />
  </section>
  
  

  
  <section id="article-body"><p>In 1967, the Russian psychologist Alfred Lukyanovich Yarbus watched people as they looked at the same oil painting with different goals in mind. He noticed that the eye-gaze movements depended on the activity being performed and he concluded that people attended to those areas of the scene that were more likely to contain relevant information for the current task.</p>

<p>Our recent eyetracking studies build on Yarbus’s research, and reinforce the idea that <strong>tasks greatly impact user behavior</strong> on the web and therefore drastically change the outcome of eyetracking gazeplots and heatmaps. Better get the tasks right, or any eyetracking you do will be more misleading than helpful for driving your design.</p>

<h2>Methodology</h2>

<p>In usability eyetracking studies, we uncover the most helpful and realistic findings for improving a website when we <a href="../task-scenarios-usability-testing/index.php">allow people to click, search, and type</a> as much as they want, since this is how they normally use the web. In other words, the users have to try to <strong>actually do something</strong> realistic with the website or application, as opposed to being told to “just take a look at this page.” Doing something, of course, almost always requires users to move between different screens where they don’t know in advance which pages will be useful to them.</p>

<p>However, because this study’s goal was to determine how users examine the same webpage as they attempt different tasks on that page, we adjusted <a href="../../reports/how-to-conduct-eyetracking-studies/index.php">our recommended eyetracking methodology</a>: We gave users a task and, instead of allowing them to navigate by themselves to the page of interest, we took them to that page, allowed them to complete the task on the page, then closed the page for them. We examined a few different pages, including 2 pages that we will discuss in this article: a page displaying women’s dresses on Bebe.com, and a page displaying vacation packages on jetBlue.com.</p>

<figure class="caption"><img alt="" height="388" src="https://media.nngroup.com/media/editor/2017/03/08/p43-andrea-yarbus_bebe_0_cropped.png" width="720"/>
<figcaption><em>One page in our study included images of dresses on www.bebe.com.</em></figcaption>
</figure>

<p> </p>

<figure class="caption"><img alt="" height="1247" src="https://media.nngroup.com/media/editor/2017/03/08/p47-stacey-yarbus_jetblue_0.png" width="720"/>
<figcaption><em>Another page in our study included a list of vacation packages offered on www.jetBlue.com.</em></figcaption>
</figure>

<p>For each page, we gave users several different tasks. (The Bebe tasks will be described later.) These were the tasks given on the jetBlue page:</p>

<ol>
	<li>Take a look at the page. (Test facilitator closes the page after 8 seconds.)</li>
	<li>To which places do these getaways go?</li>
	<li>Which place looks the nicest to you?</li>
	<li>Which is the least expensive?</li>
	<li>Which is the highest rated?</li>
	<li>Imagine you were going to get a quiz about this page. Study the page enough that you feel you could pass the quiz.</li>
</ol>

<p>To be clear: the approach we used in this study is <em>not how we recommend that you test your website.</em> The goal of our research was to capture the effect of the various test tasks, not to improve the websites. If Bebe or jetBlue had hired us for a consulting job, we would have run very different studies.</p>

<h3>Gaze Plots</h3>

<p>In eyetracking studies, researchers record participants’ eye movements and track the order and duration of their gazes (or fixations). Then they aggregate the number of fixations, fixation durations, or sequence of fixations in graphical representations such as gaze plots and heatmaps. In this article we illustrate our findings using gaze plots.</p>

<p><strong>A gaze plot is an overlay superimposed on a static screenshot that demonstrates where one or more users looked on that page.</strong> The elements in a gaze plot include the following:</p>

<ul>
	<li><strong>Dots = fixations:</strong> In a gaze plot, each dot represents one gaze (or fixation). In other words, a dot says that the user looked at that spot on the screen (or closely around it). Most eyetracking usability studies operate under the <strong>eye–mind hypothesis — </strong>namely, if a user looked at an item, that item was attended and processed cognitively. That said, an attended item may still not be properly interpreted or remembered. It’s important to understand that the dots show the <em>only parts of the page that the user saw</em> sharply enough to read text or understand the details in images.</li>
	<li><strong>Larger dots = longer fixations: </strong>The size of the dot is (roughly) proportional with the duration of the corresponding fixation. Thus, larger dots represent a longer time spent looking at that location on the page. Long fixations signal that the user spent more time processing the corresponding item, because 1) she is interested in it, or 2) she is confused and has a hard time understanding what it means.</li>
	<li><strong>Numbers = order:</strong> The numbers within the dots represent the order in which the user looked at the items on the page.</li>
	<li><strong>Lines = saccades between fixations:</strong> Each dot (fixation) is connected to the fixation preceding it and to the one following it. Thus, the lines between dots make it easier to follow the eye movements (called saccades) and the sequence of the fixations. (The numbers provide enough information for determining the order, but, without lines, it becomes too challenging to hunt for the next fixation.) Because the eyes move extremely fast between fixations, the person is effectively blind during an eye movement and doesn’t register the visual landscape that the gaze speeds across.</li>
</ul>

<figure class="caption"><img alt="" height="1247" src="https://media.nngroup.com/media/editor/2017/03/08/p47-stacey-yarbus_jetblue_7_all-tasks.png" width="720"/>
<figcaption><em>This gaze plot from a study participant doing all the given tasks on jetBlue’s vacation-package page includes more than 440 fixations.</em></figcaption>
</figure>

<h2>Interpreting User’s Fixations on a Page</h2>

<p>The gaze plot of the jetblue.com page above includes more than 440 fixations on various page elements, ranging from vacation listings to UI components such as logo and navigation. Where did the user look and why?</p>

<p>We could come up with several reasonable interpretations, such as:</p>

<ul>
	<li>The participant looked in the area in the upper left a few times to see the <a href="../logo-placement-brand-recall/index.php">logo</a> and confirm the site she was on.</li>
	<li>She looked at the <a href="../ia-questions-navigation-menus/index.php">navigation</a> within the first few moments on the page to get a sense of what was offered on the site.</li>
	<li>Many, long fixations on each package name, image, rating, price, and short description possibly indicate that she is interested in all of them and trying to decide which to pick.</li>
	<li>After she looked at everything on the page, she checked the footer to see what else was offered on the site.</li>
</ul>

<p>Each of these possible interpretations are logical. But, because we know nothing about what the user was trying to achieve, these assumptions are unfounded. Simply zooming in on the top of the gaze plot shows that the fixation numbers are high and did not happen within the user’s first few moments on the site, demonstrating that the second interpretation is wrong. But that’s about all we can conclude from looking at the static image alone.</p>

<figure class="caption"><img alt="" height="140" src="https://media.nngroup.com/media/editor/2017/03/08/p47-stacey-yarbus_jetblue_7_all-tasks_zoom-top.png" width="720"/>
<figcaption><em>Fixations on the menu and logo happened relatively late in the task, after more than 200 fixations on other places on the page.</em></figcaption>
</figure>

<p><strong>Watching the user’s full eyetracking session, not just studying gaze plots, is the best way to uncover meaningful insights.</strong> Watching slow-motion replays enabled us to create more telling gaze plots that represent time segments on the page (as opposed to the entire visit on the page) related to the task the user was doing at the time. We’ll examine those in this article.</p>

<h2>Task and User Motivation Affect Eye Movements</h2>

<h3>Task 1: Free Examination (“Take a Look at the Page”)</h3>

<p>The first task was simply to look at the page. The task did not specify an amount of time, but the facilitator closed the page after 8 seconds. This task tested the concept of “free examination.” Free examination is rare in the real world — people usually visit websites for a reason. In usability testing, free-examination tasks should be reserved for special cases when you want to understand how users behave when they’re simply interested in your company or <a href="../brand-experience-ux/index.php">brand</a>. (Even then, people are usually trying to satisfy an information need — for example, to see<em> what’s new</em>, or<em> what the company does.)</em> The main reasons free-examination tasks are not usually recommended in usability testing are: 1) they are unrealistic; and 2) they cause users to study the page more carefully than they normally would, and, as a result, may bias their behavior in subsequent tasks.</p>

<p><strong>User behavior. </strong>In these first few seconds on the page, the user looked at the largest piece of text in the content area (the price of the first vacation), at the corresponding thumbnail, and then she moved her gaze to the upper left corner of the page. That spot is traditionally reserved for the company logo, but on jetBlue’s page the logo was pushed to the right. <a href="../centered-logos/index.php">Centered logos are unexpected and harder to locate</a>.</p>

<p>After not finding the logo, the user returned to the content area and began to scan the location names.</p>

<figure class="caption"><img alt="" height="374" src="https://media.nngroup.com/media/editor/2017/03/08/p47-stacey-yarbus_jetblue_1_take-a-look-at-the-page_8sec_cropped.png" width="720"/>
<figcaption><em>The gaze plot for the task “Take a look at the page.” The image is cropped to exclude areas where the user did not fixate.</em></figcaption>
</figure>

<h3>Task 2: Scan Section Titles (“To Which Places Do These Getaways Go?”)</h3>

<p>The second task was to find something more specific: the locations of the vacations. For this task and the ones that follow, we will describe the information on the page that was necessary to complete the task, the user’s behavior, and, if applicable, the design elements that supported the task or hindered it.</p>

<p><strong>Information needed to complete the task:</strong></p>

<ul>
	<li>the names of all the destinations</li>
	<li>the destination thumbnail images, which may have been a secondary source of information</li>
</ul>

<p><strong>User behavior. </strong>Once the user learned the look of the destination names and how much vertical space existed between them, she quickly adapted her scanning to extract the information needed for the task (i.e., destination names) as efficiently as possible, without wasting extra fixations. Although occasionally the user glanced at secondary elements, such as the thumbnail and the description, the majority of her gazes were directed at the destination names. Our participant was able to complete the task in <strong>38 fixations.</strong></p>

<p>The gaze plot that shows her eye movements exhibits the <em>layer cake </em>pattern of scanning, during which users scan to headings and subheadings but don’t read the information below these, usually because the headings contain enough information to either answer their question, or indicate that the text under the heading is not relevant for answering it. The physical scan path resembles the horizontal sheets found in a layer cake. (This and other scanning patterns are described in our <a href="../../reports/how-people-read-web-eyetracking-evidence/index.php">How People Read on the Web: The Eyetracking Evidence report</a>.) The scanning pattern in the gaze plot is an instance of <strong><em>efficient scanning: </em></strong>it is highly focused on the current task and ruthlessly ignores content unrelated to the user’s goal.</p>

<figure class="caption"><img alt="" height="2659" src="https://media.nngroup.com/media/editor/2017/03/08/p47-stacey-yarbus_jetblue_2_where-go_cropped.png" width="720"/>
<figcaption><em>The gaze plot for the task “To which places do these getaways go?”; The image is cropped to exclude areas where the user did not fixate.</em></figcaption>
</figure>

<p><strong>Design elements that support the task:</strong></p>

<ul>
	<li>consistent presentation of the different vacation packages</li>
	<li>large, bold text for location names, juxtaposed against the smaller description text</li>
	<li>vertical white space between vacation packages</li>
	<li>light, thin, subtle grey lines separating vacation packages</li>
	<li>consistent vertical spacing between different package names</li>
</ul>

<p>All these design elements allowed the user to quickly figure out how to find the most important piece of information needed to complete the task: the destination names.</p>

<h2>Task 3: Scan the Images (“Which Place Looks the Nicest to You?”)</h2>

<p>The third task involved gathering impressions about each destination.</p>

<p><strong>Information needed to complete the task:</strong></p>

<ul>
	<li>the thumbnail photographs</li>
	<li>the location name for the nicest place</li>
</ul>

<p><strong>User behavior. </strong>Again, the user quickly limited her scanning only to information that was needed to complete the task. Each thumbnail got 1–6 fixations, and some destination names (presumably of places that looked nice to her) also got fixations. When the user didn’t care for the photo — as with those for Grand Cayman, Charleston, and Fort Lauderdale — she didn’t bother reading the location name. And she scanned nothing else on the page. She was able to complete the task in <strong>37 fixations. </strong>(However, had the images been clearer, she likely would have been able to succeed with even fewer fixations.)</p>

<figure class="caption"><img alt="" height="3586" src="https://media.nngroup.com/media/editor/2017/03/08/p47-stacey-yarbus_jetblue_3_-look-nicest_cropped.png" width="720"/>
<figcaption><em>The gaze plot for the task “Which place looks the nicest to you?”: The image is cropped to exclude areas where the user did not fixate.</em></figcaption>
</figure>

<p><strong>Design elements that hindered the task:</strong></p>

<ul>
	<li><a href="../photos-as-web-content/index.php">photographs</a> that were too small given the amount of detail in them</li>
	<li>inconsistent thumbnail image types, with different subjects, camera angles, times of day, and level of detail</li>
</ul>

<h3>Task 4: Scan the Prices (“Which Is the Least Expensive?”)</h3>

<p>The second vacation package, Saint Maarten, cost $349 and was the least expensive.</p>

<p><strong>Information needed to see to complete the task:</strong></p>

<ul>
	<li>price for each location</li>
	<li>location name for the cheapest destination</li>
</ul>

<p><strong>User behavior. </strong>Our study participant confidently scanned all prices on the page, then she scrolled up and gazed at the name of the second destination in the list to find her answer. That is highly efficient scanning. She completed the task in <strong>28 fixations.</strong></p>

<figure class="caption"><img alt="" height="2476" src="https://media.nngroup.com/media/editor/2017/03/08/p47-stacey-yarbus_jetblue_4_least-expensive_cropped.png" width="720"/>
<figcaption><em>The gaze plot for the task “Which is the least expensive?”; The image is cropped to exclude areas where the user did not fixate.</em></figcaption>
</figure>

<p><strong>Design elements that supported the task:</strong></p>

<ul>
	<li>price size larger than all the text items in the content area</li>
	<li>whitespace surrounding prices</li>
	<li>short numbers</li>
	<li>bold font for prices</li>
	<li>same price position for each location in the list</li>
</ul>

<p>All these elements made the prices easy to locate.</p>

<h3>Task 5: Scan Details (“Which Is the Highest Rated?)</h3>

<p><strong>Information needed to complete the task:</strong></p>

<ul>
	<li>star ratings</li>
</ul>

<ul>
	<li>location name for the highest-rated destination</li>
</ul>

<p><strong>User behavior. </strong>Our participant optimized her scanning procedure along the way, as she learned about the structure of the page: she started by looking at all the stars in the ratings appearing early in the list, but, as the scan progressed, she understood that all locations got at least 4 stars, so she fixated on only the stars to the right. Finally, she determined that Charleston, the 7<sup>th</sup> entry, and Aruba, the last entry, each held a 5-star rating. She completed the task in <strong>36 fixations.</strong></p>

<figure class="caption"><img alt="" height="2623" src="https://media.nngroup.com/media/editor/2017/03/08/p47-stacey-yarbus_jetblue_5_highest-rated_cropped.png" width="720"/>
<figcaption><em>The gaze plot for the task “Which is the highest rated?”; The image was cropped to exclude areas where the user did not fixate.</em></figcaption>
</figure>

<p><strong>Design elements that supported the task:</strong></p>

<ul>
	<li>conventional pattern for ratings (stars)</li>
	<li>star icons easily distinguishable from the other items in the destination description</li>
	<li>consistent positioning of items across destinations</li>
</ul>

<p>All these made the stars easy to locate.</p>

<p><strong>Design elements that hindered the task:</strong></p>

<ul>
	<li>small stars</li>
	<li>little visual difference between dark blue and light blue stars, or between half-full stars and full stars</li>
</ul>

<p>These issues required the user to spend more time on each star rating to parse the details.</p>

<h3>Task 6: Motivated Reading (“Study the Page Enough That You Feel You Could Pass a Quiz About This Page”)</h3>

<p>This task required the user to pay attention to all the information presented on the page and attempt to memorize it so that it could be recalled later.</p>

<p><strong>Information needed to complete the task:</strong></p>

<ul>
	<li>everything on the page</li>
</ul>

<p><strong>User behavior. </strong>Our participant looked at navigational elements and at the content, and in fact she fixated many of these elements multiple times, presumably trying to memorize them. She used <strong>228 fixations</strong> to complete the task.</p>

<p>As we saw in the previous tasks, for the sake of efficiency, users <a href="../f-shaped-pattern-reading-web-content/index.php">scan web pages</a>, focusing only on the relevant bits of content. But when their motivation or engagement are high (or when all the content on the page is relevant, like in this task), they might read almost everything. This commitment can be simulated in a lab setting by telling people that they will be quizzed about the content of the page.</p>

<figure class="caption"><img alt="" height="2320" src="https://media.nngroup.com/media/editor/2017/03/08/p47-stacey-yarbus_jetblue_6_quiz_cropped.png" width="720"/>
<figcaption><em>The gaze plot for the task: “Imagine you were going to get a quiz about this page. Study the page enough that you feel you could pass the quiz”; The image is cropped to exclude areas where the user did not fixate.</em></figcaption>
</figure>

<h2>Test Your Eyetracking-Analysis Assumptions</h2>

<p>Our analysis of the gaze plots for the jetBlue tasks probably gave you some insights into how users adjust eye movements to respond to the task. Let’s see if you can apply these to a new set of data.</p>

<p>Below is a list of 3 tasks we asked people to try on <a href="http://www.bebe.com/">www.bebe.com</a>, and 3 gaze plots (labeled A through C) of segments from one user’s visit to the same page.</p>

<p>Your job: Match the task to the corresponding gaze plot.</p>

<ol>
	<li>Which dress is the prettiest?</li>
	<li>Estimate the average age of the models.</li>
	<li>What is the price range for the dresses?</li>
</ol>

<p> </p>

<p><strong>A.</strong></p>

<p><img alt="" height="414" src="https://media.nngroup.com/media/editor/2017/03/08/p43-andrea-yarbus_bebe_4_price-range_first-seconds_cropped.png" width="720"/></p>

<p> </p>

<p> </p>

<p><strong>B.</strong></p>

<p><img alt="" height="411" src="https://media.nngroup.com/media/editor/2017/03/08/p43-andrea-yarbus_bebe_3_average-age-models_first-seconds_cropped.png" width="720"/></p>

<p> </p>

<p> </p>

<p><strong>C.</strong></p>

<p><img alt="" height="388" src="https://media.nngroup.com/media/editor/2017/03/08/p43-andrea-yarbus_bebe_2_prettiest-dress_first-seconds_cropped.png" width="720"/></p>

<p> </p>

<p>The answers appear at the end of this article.</p>

<h2>What Did We Learn from This Study?</h2>

<p>The jetBlue examples and the Bebe quiz should have convinced you that Yarbus’s findings hold for web reading: Users fixate on those elements on the page that are relevant for their task. The <em>same</em> page will be processed <em>differently</em> by the same user when her goal changes.</p>

<p>And, more importantly, this behavior is another embodiment of the principle of least effort and minimum <a href="../interaction-cost-definition/index.php">interaction cost</a>: in performing an activity, people will try to be as efficient as possible and will avoid spending any unnecessary effort. Like our study participant, they will always try to find an optimum algorithm for getting the information that they need without working unnecessarily hard.</p>

<h2>Design for Efficient Scanning</h2>

<p>The 6 jetBlue tasks discussed above encapsulate 3 general scanning behaviors:</p>

<ol>
	<li>getting the lay of the land (task 1)</li>
	<li>comparing items (tasks 2–5)</li>
	<li>motivated reading (task 6)</li>
</ol>

<p>The first two are most common on the web, and indeed all our Bebe tasks are also <a href="../the-3cs-of-critical-web-use-collect-compare-choose/index.php">comparison tasks</a>. Designers should support users in their attempt to minimize the amount of effort involved in reading and extracting relevant information from a webpage. As we saw in the previous gaze plots, <strong>predictable, unambiguous patterns help users get to an optimal scanning algorithm fast and allow them to easily focus on the essential</strong>, while skipping those content elements that are unnecessary for their goals. Remember that sometimes too much variation or detail (like in the case of star ratings and photographs) can slow down the eye, make the task harder, and ultimately increase user frustration.</p>

<p>Here are some design recommendations for supporting efficient scanning of <a href="../list-entries/index.php">list pages</a>:</p>

<ol>
	<li>Be consistent with the position and layout of items in the list.</li>
	<li>Use <a href="../be-succinct-writing-for-the-web/index.php">short, recognizable words</a> and <a href="../web-writing-show-numbers-as-numerals/index.php">digits</a> when possible.</li>
	<li>Use large, bold text and white space surrounding it to attract the eye and showcase the most important information.</li>
	<li>Consider using <a href="../comparison-tables/index.php">comparison tables</a> to support this behavior.</li>
</ol>

<p>If displaying photos:</p>

<ol>
	<li value="5">Present consistent types of photos.</li>
	<li value="6">Present a level of detail that is recognizable given the size of the photos.</li>
</ol>

<p>If presenting star ratings:</p>

<ol>
	<li value="7">Consider showing a number with the star ratings.</li>
	<li value="8">Make the selected number of stars very simple to glean in one fixation. For example, use a saturated color filling with a thick outline for the selected stars, and white filling and a medium outline for the stars that are not selected. Use high color contrast between selected and not selected stars (and remember color blindness when choosing colors).</li>
</ol>

<h2>Tasks in Usability Studies Focus Test Participants on Particular Site Areas</h2>

<p>One of the greatest drawbacks of giving people tasks to perform in a usability test is that tasks affect behavior. Even the act of asking people to do an activity usually clues them that the activity is possible and that the answer exists somewhere on the site. If the users had not been given a task, they may have never discovered certain site features related to that task.</p>

<p>If tasks change how people look at and interact with a design, then why give a task at all in a usability test? Although giving tasks has some drawbacks, it also has many benefits:</p>

<ul>
	<li>Most important, if users don’t have a reason to use a website or application, then they would just flail around to no purpose, which is a completely unrealistic way of approaching most designs. It’s better if we present people with a purpose than to waste our time observing a style of use that doesn’t happen in real life and has no business value.</li>
	<li>If the tasks reflect our audience’s main user needs and goals, then it makes sense to ask study participants to perform those tasks in the lab, so we can optimize the design for those high-priority activities.</li>
	<li>Tasks allow teams to focus on sections of the design that are new or important for the business. Task-based studies enable us to fix issues before a design goes live, and learn from the research so we can become better at predicting which new designs will work for users and why.</li>
</ul>

<p>Clearly formulated tasks enable us to figure out what design elements work for which user goals. Still, it’s helpful to employ a variety of research methods in addition to task-based usability tests. Particularly useful are open-ended observation of users in their natural environment (e.g., <a href="../field-studies/index.php">field studies</a>, <a href="../../consulting-field-studies/index.php">contextual inquiry</a>) where they have their own reasons to use the computer or phone. But these research methods are not practical for all projects, budgets, development schedules, or design stages. If we did a field study every time we wanted to see users interact with a design, we could spend days watching people engage in site activities that are irrelevant for our current design project. And this method doesn’t work for sketches and prototypes that need to be tested early in the design lifecycle. Also, when researchers observe users in the field, they may not have a clear understanding of their goals and motivations.</p>

<p>When designing a usability study or analyzing user data, remember that tasks may change user behavior: they may tell users that a piece of information or a functionality are available in the design, leading them work harder than if they had not suspected that the task was doable.</p>

<p>(Learn much more about how to design good test tasks in our full-day <a href="../../courses/usability-testing/index.php">Usability Testing</a> course.)</p>

<h2>Eyetracking Requires Realistic Tasks</h2>

<p>While good test tasks are important for traditional usability testing, the tasks are essential for the validity of an eyetracking study. A heatmap of where users looked while performing a nonrealistic task will be misleading. Any design decision made on the basis of such data is more likely to hurt your business than to improve your metrics. Write good tasks for the most useful eyetracking studies.</p>

<p> </p>

<h2>Quiz Answer</h2>

<p>These are the answers to the quiz in this article:</p>

<ol>
	<li>Which dress is the prettiest? Image C.</li>
</ol>

<p>After a few initial fixations needed to locate the dresses on the page, the user looked directly at the dresses. Consistent size and layout of the dress images made it easy for the user to find the dresses. Unlike the jetBlue photos, the dress images are consistent and each shows a model standing in front of a plain light background.</p>

<ol>
	<li value="2">Estimate the average age of the models. Image B.</li>
</ol>

<p>Most of the fixations were on the faces of the models, which would be most telling of their age. But there were also a few fixations on the body, which can also be expressive of age. Again, the consistent pictures used in the design helped the user quickly locate the faces.</p>

<ol>
	<li value="3">What is the price range for the dresses? Image A</li>
</ol>

<p>The first 2 fixations were used to finding the way to the prices, then the user scanned only the text below the images, where the prices appear. The consistent positioning of the prices made the task easy. But, unlike the large jetBlue prices, the Bebe prices were smaller and positioned too close to the dress name, forcing the user to spend longer time or more fixations to register the information.</p>
</section>
  

  
  <section>
    
        
    
  </section>
  
  
  
  
  

  


  <section>
    <br />
    <p class="share"><b>Share <span class="hide-for-small-down">this article</span>:</b> <a href="https://twitter.com/intent/tweet?url=http://www.nngroup.com/articles/eyetracking-tasks-efficient-scanning/&amp;text=Scanning%20Patterns%20on%20the%20Web%20Are%20Optimized%20for%20the%20Current%20Task&amp;via=nngroup">Twitter</a> | <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=http://www.nngroup.com/articles/eyetracking-tasks-efficient-scanning/&amp;title=Scanning%20Patterns%20on%20the%20Web%20Are%20Optimized%20for%20the%20Current%20Task&amp;source=Nielsen%20Norman%20Group">LinkedIn</a> | <a href="https://plus.google.com/share?url=http://www.nngroup.com/articles/eyetracking-tasks-efficient-scanning/">Google+</a> | <a href="mailto:?subject=NN/g Article: Scanning Patterns on the Web Are Optimized for the Current Task&amp;body=http://www.nngroup.com/articles/eyetracking-tasks-efficient-scanning/">Email</a></p>
    <hr />
  </section>

  <footer class="l-subsection">
    
    
    <div class="learn-more">
      <h2>Learn More</h2>
      <p><a href="../subscribe/index.php">Subscribe to the weekly newsletter</a> to get notified about future articles.</p>
      </div>
    
    

    <div class="row show-for-small-down related-content">
      
      
      

      
      
        <div class="column small-12 topic-links">All articles about:
          <ul>
            
            <li><a href="../../topic/behavior-patterns/index.php">Behavior Patterns</a></li>
            
            <li><a href="../../topic/eyetracking/index.php">Eyetracking</a></li>
            
            <li><a href="../../topic/facilitating-research/index.php">facilitating research</a></li>
            
            <li><a href="../../topic/legibility/index.php">legibility</a></li>
            
            <li><a href="../../topic/tasks/index.php">tasks</a></li>
            
            <li><a href="../../topic/user-testing/index.php">User Testing</a></li>
            
          </ul>
        </div>
      
      

      
      
        <div class="column small-12">
          <h3>UX Conference Training Courses</h3>
           <ul class="no-bullet">
    
        <li><a href="../../courses/ux-basic-training/index.php">UX Basic Training</a></li>
    
        <li><a href="../../courses/hci/index.php">User Interface Principles Every Designer Must Know</a></li>
    
        <li><a href="../../courses/usability-testing/index.php">Usability Testing</a></li>
    
        <li><a href="../../courses/web-page-design/index.php">Web Page UX Design</a></li>
    
        <li><a href="../../courses/one-person-ux-team/index.php">The One-Person UX Team Tool Box</a></li>
    
</ul>
        </div>
      
      

      
      
        <div class="small-12 column">
          <h3>Research Reports</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../reports/how-people-read-web-eyetracking-evidence/index.php">How People Read on the Web: The Eyetracking Evidence</a></li>
              
            
              
                <li><a href="../../reports/paper-prototyping-training-video/index.php">Paper Prototyping Training Video</a></li>
              
            
              
                <li><a href="../../reports/how-to-conduct-eyetracking-studies/index.php">How to Conduct Eyetracking Studies</a></li>
              
            
              
                <li><a href="../../reports/how-to-recruit-participants-usability-studies/index.php">How to Recruit Participants for Usability Studies</a></li>
              
            
              
                <li><a href="../../reports/how-to-conduct-usability-studies-accessibility/index.php">How to Conduct Usability Studies for Accessibility</a></li>
              
            
          </ul>
        </div>
      
      

      
      
        <div class="small-12 column">
          <h3>Online Seminars</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../online-seminars/mobile-user-testing/index.php">Mobile User Testing</a></li>
              
            
          </ul>
        </div>
      
      
    </div>

    <div class="hide-for-small-down row related-content">
      <div class="medium-6 columns">
        
        
          <div>
            <h3>Articles</h3>
            <ul class="no-bullet">
              
                
                <li><a href="../website-response-times/index.php">Website Response Times</a></li>
                
              
                
                <li><a href="../horizontal-attention-leans-left/index.php">Horizontal Attention Leans Left</a></li>
                
              
                
                <li><a href="../fancy-formatting-looks-like-an-ad/index.php">Fancy Formatting, Fancy Words = Looks Like a Promotion = Ignored</a></li>
                
              
                
                <li><a href="../banner-blindness-old-and-new-findings/index.php">Banner Blindness: Old and New Findings</a></li>
                
              
                
                <li><a href="../web-writing-show-numbers-as-numerals/index.php">Show Numbers as Numerals When Writing for Online Readers</a></li>
                
              
            </ul>
          </div>
        
        

        
          
          <div>
            <h3>Research Reports</h3>
            <ul class="no-bullet">
              
                
                  <li><a href="../../reports/how-people-read-web-eyetracking-evidence/index.php">How People Read on the Web: The Eyetracking Evidence</a></li>
                
              
                
                  <li><a href="../../reports/paper-prototyping-training-video/index.php">Paper Prototyping Training Video</a></li>
                
              
                
                  <li><a href="../../reports/how-to-conduct-eyetracking-studies/index.php">How to Conduct Eyetracking Studies</a></li>
                
              
                
                  <li><a href="../../reports/how-to-recruit-participants-usability-studies/index.php">How to Recruit Participants for Usability Studies</a></li>
                
              
                
                  <li><a href="../../reports/how-to-conduct-usability-studies-accessibility/index.php">How to Conduct Usability Studies for Accessibility</a></li>
                
              
            </ul>
          </div>
          
        

      </div>
      <div class="medium-6 columns">
        
          
          <div>
            <h3>UX Conference Training Courses</h3>
             <ul class="no-bullet">
    
        <li><a href="../../courses/ux-basic-training/index.php">UX Basic Training</a></li>
    
        <li><a href="../../courses/hci/index.php">User Interface Principles Every Designer Must Know</a></li>
    
        <li><a href="../../courses/usability-testing/index.php">Usability Testing</a></li>
    
        <li><a href="../../courses/web-page-design/index.php">Web Page UX Design</a></li>
    
        <li><a href="../../courses/one-person-ux-team/index.php">The One-Person UX Team Tool Box</a></li>
    
</ul>
          </div>
          
        
        
          
          <div>
            <h3>Online Seminars</h3>
            <ul class="no-bullet">
            
                <li><a href="../../online-seminars/mobile-user-testing/index.php">Mobile User Testing</a></li>
            
            </ul>
          </div>
          
        
      </div>
    </div>
  </footer>
</article>

        </div>

      

    
  </div>
  <div class="row">
    
      <div class="column small-12 show-for-small-down l-navrail announcements">
        
          
        
      </div>
    
    
  </div>

    </div>
    




<footer id="page-footer">
    <div class="row hide-for-print">
        <div class="column small-12 medium-6">
            <section class="footer-popular">
                
                <h1>Popular Topics</h1>
                <div style="float: left;  width: 15em; display: block;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/applications/index.php">Application Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/e-commerce/index.php">E-Commerce Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/eyetracking/index.php">Eyetracking </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/information-architecture/index.php">Information Architecture </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/intranets/index.php">Intranet Design </a></li>
	<li><a href="../../topic/interaction-design/index.php">Interaction Design </a></li>
</ul>
</div>

<div style="float: left;  width: 15em; display: block; list-style: none;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/mobile-and-tablet-design/index.php">Mobile and Tablet Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/credibility-persuasion/index.php">Persuasive Design</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/research-methods/index.php">Research Methods</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/user-testing/index.php">User Testing </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/web-usability/index.php">Web Usability </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/writing-web/index.php">Web Writing &amp; Content Strategy </a></li>
</ul>
</div>

<p>&nbsp;</p>

                
            </section>
        </div>

        <div class="column small-12 medium-6">
            <div class="row">
                <div class="column small-6">
                    <section class="footer-about">
                        <h1>Follow Us</h1>
                        <ul>
	<li><a href="../subscribe/index.php">Subscribe to our weekly UX newsletter</a>,&nbsp;Jakob Nielsen&#39;s Alertbox</li>
	<li>Visit Don Norman&#39;s website, <a href="http://www.jnd.org/"> www.jnd.org </a></li>
	<li>Visit Bruce Tognazzini&#39;s website, <a href="http://www.asktog.com/"> www.asktog.com </a></li>
	<li style="padding-top: 6px;"><a class="twitter-follow-button" data-show-count="false" data-size="large" href="https://twitter.com/nngroup"><img alt="Follow @nngroup on Twitter" height="20" src="https://media.nngroup.com/media/editor/2016/12/30/twitter-follow-button.png" style="width: 117px; height: 20px;" width="117" /></a></li>
	<li style="padding-top: 6px;"><img alt="" height="14" src="https://media.nngroup.com/media/editor/2016/07/26/feed-icon-14x14.png" style="vertical-align:text-top;width:14px;margin-right:.25em;" width="14" /><a href="../../feed/rss/index.php">Subscribe to our RSS feed</a></li>
</ul>

                    </section>
                </div>

                <div class="column small-6">
                    <section class="footer-about right-column">
                        <h1>About
</h1>
                        <ul class="no-bullet">
	<li><a href="../../about/contact/index.php">Contact information</a></li>
	<li><a href="../../about/index.php">About Us</a></li>
	<li><a href="../../about/why-nng/index.php">Why NN/g? </a></li>
	<li><a href="../../return-policy/index.php">Return Policy</a></li>
</ul>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="copyright column small-12">
            <p><a href="../../copyright-and-reprint-info/index.php">Copyright</a> &copy; 1998-2017 Nielsen Norman Group, All Rights Reserved.</p>
        </div>
    </div>
</footer>


    


        <script src="https://media.nngroup.com/static/vendor/fastclick/js/fastclick.min.js"></script>
        <script src="https://media.nngroup.com/static/vendor/foundation/js/foundation.min.js"></script>
        <script src="http://media.nngroup.com/static/js/app.js"></script>
        <script src="http://media.nngroup.com/static/js/scroll-to-fixed.js"></script><script>var externalLinks = document.querySelectorAll('a[href^="http"]');for (var i = externalLinks.length-1; i >= 0; i--) {externalLinks[i].addEventListener("click", function(event) { alert("External links are disabled for this test."); event.preventDefault(); }, false);}var mailLinks = document.querySelectorAll('a[href^="mailto"]');for (var i = mailLinks.length-1; i >= 0; i--) {mailLinks[i].addEventListener("click", function(event) { alert("Email links are disabled for this test."); event.preventDefault(); }, false);}var externalForms = document.querySelectorAll('form');for (var i = externalForms.length-1; i >= 0; i--) {externalForms[i].addEventListener("submit", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false); externalForms[i].addEventListener("click", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false);}</script>

        <script>
            
            $(function() {
                $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
                $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
            });
        </script>

        
<script src="https://media.nngroup.com/static/js/articles.js"></script>


        
        <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1072699960;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072699960/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
    </body>

<!-- Mirrored from www.nngroup.com/articles/eyetracking-tasks-efficient-scanning/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 14:54:51 GMT -->
</html>
