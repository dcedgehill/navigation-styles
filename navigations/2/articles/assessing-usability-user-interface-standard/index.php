<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->





    
<!-- Mirrored from www.nngroup.com/articles/assessing-usability-user-interface-standard/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:14:20 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1026.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"beacon":"bam.nr-data.net","queueTime":0,"applicationTime":574,"agent":"","licenseKey":"1a30ef9931","errorBeacon":"bam.nr-data.net","applicationID":"2048992","transactionName":"NFxSMRBUX0EDUkIPDA0WdhAMVkVbDV8ZBxEXUFMJB0YfRAtUQRVZIktEDAFZVHYHRVcPD01eVRE="}</script>
        <title>Assessing the Usability of a User Interface Standard</title><meta property="og:title" content="Assessing the Usability of a User Interface Standard" />
  
        
        <meta name="description" content="This paper by Henrik Thovtrup and Jakob Nielsen illustrates how user interface standards can be hard to use for developers. Two experiments demonstrate that compliance with standards is limited in both laboratory and corporate settings. ">
        <meta property="og:description" content="This paper by Henrik Thovtrup and Jakob Nielsen illustrates how user interface standards can be hard to use for developers. Two experiments demonstrate that compliance with standards is limited in both laboratory and corporate settings. " />
        
  
        
	
        

        

        <meta name="robots" content="noarchive">
        <meta name="viewport" content="width=device-width">

        <link rel="search" type="application/opensearchdescription+xml" title="NN/g Search" href="../../opensearch.xml" />
        <link rel="shortcut icon" href="http://media.nngroup.com/static/img/favicon.ico" />

        <link rel="stylesheet" href="https://media.nngroup.com/static/vendor/font-awesome/css/font-awesome.min.css">

        
        <link rel="stylesheet" href="https://media.nngroup.com/static/css/7232a960f1df.css" type="text/css" />
        <link rel="stylesheet" href="http://fast.fonts.com/t/1.css?apiType=css&amp;projectid=c5846260-9def-46ec-9407-0f1349bd2d64" type="text/css">

        <script src="https://media.nngroup.com/static/vendor/modernizr/js/modernizr.min.js"></script>

        <script type="text/javascript" src="https://media.nngroup.com/static/js/3283be5def74.js"></script>
        

        

        
        
        <!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=229034,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('http://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->


<!-- Google Analytics Tracking Code for http://nngroup.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23132681-1', 'nngroup.com');
  ga('require', 'linkid', 'https://www.nngroup.com/articles/assessing-usability-user-interface-standard/linkid.js');
  ga('require', 'displayfeatures');

// Optimizely Universal Analytics Integration
 window.optimizely = window.optimizely || [];
 window.optimizely.push("activateUniversalAnalytics");

  ga('send', 'pageview');

</script>
<script>
/**
* Function to track a click on an outbound link in Google Analytics.
*/
function trackOutboundLink(label, url) {
    if(window.ga && ga.loaded) {
        ga('send', 'event', 'outbound', 'click', label, {'hitCallback': function(){document.location = url;}
        });
    } else {
        document.location = url;
    }
}
/**
* Function to track a click on a link in the article announcement area.
*/
$('#articleAnnouncement a').on('click', function(){
  ga('send', 'event', 'link', 'click', 'articleAnnouncement');
});
</script>

<script type="text/javascript">
 setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2421.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



<!-- Hotjar Tracking Code for http://nngroup.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:121605,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

        <script src="https://media.nngroup.com/static/js/analytics/track_outbound_link.js"></script>
        
        
        
    </head>
    <body class="location-articles articles-detail">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
    <header id="page-header">
        <div id="cart" class="shopping-cart mobile-cart show-for-small-down"></div>
        <div class="row">
            <div id="masthead" class="masthead small-12 medium-7 column">

                <!-- The button to toggles menu on small screen -->

                <h1><a href="../../index.php"><img src="http://media.nngroup.com/static/img/logo-wordmark.png" width="74" height="30"> Nielsen <span class="norman">Norman</span> Group</a></h1>
                <p class="tagline show-for-medium-up">Evidence-Based User Experience Research, Training, and Consulting</p>

                <!-- The button to toggles form search on small screen -->


                <div style="clear: right;width:  100%;height: 1px;"></div><?php define('ROOT_PATH',$_SERVER["DOCUMENT_ROOT"] . '/apps/navigation_styles/navigations/2'); ?><?php include(ROOT_PATH)."/navigation.php"; ?>

                <!-- Drop down menu on small screen -->
                <div id="hideNav" class="show-for-small-down">
                  
                  <ul class="menu-content">
                    <li><a href="../../index.php">Home</a></li>
                    <li class="has-submenu">
                      <a>Training</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../training/index.php">Overview</a></li>
                        
<li id="nav-ux-conference"><a href="../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../online-seminars/index.php"><span>Online Seminars</span></a></li>

                      </ul>
                    </li>
                    <li class="has-submenu">
                      <a>Consulting</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
  <li><a href="../../consulting/index.php">Overview</a></li>
  
  <li><a href="../../consulting/benchmarking/index.php">Benchmarking</a></li>
  
  <li><a href="../../consulting/expert-design-review/index.php">Expert Design Review</a></li>
  
  <li><a href="../../consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
  
  <li><a href="../../consulting/usability-testing/index.php">Usability Testing </a></li>
  
  <li><a href="../../consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
  
  <li><a href="../../in-house-training/index.php">In-House Training</a></li>
</ul>

                    </li>

                    <li><a href="../../reports/index.php">Reports</a></li>
                    <li><a href="../index.php">Articles</a></li>
                    <li class="has-submenu"><a>About NN/G</a><i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../about/index.php">Overview</a></li>
                        <li><a href="../../people/index.php">People</a></li>
                        <li><a href="../../about/why-nng/index.php">Why NN/g?</a></li>
                        <li><a href="../../about/contact/index.php">Contact</a></li>
                        <li><a href="../../news/index.php">News</a></li>
                        <li><a href="../../about/history/index.php">History</a></li>
                        <li><a href="../../books/index.php">Books</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                </div>

                <hr class="header-divider show-for-small-only"/>


            </div>
       </div>

        <div id="hideNav">            <nav class="primary-nav small-12 column">
                <ul>
                    <li><a class="navitem" id="nav-home" href="../../index.php">Home</a></li>
                    <li><a class="navitem" id="nav-training" href="../../training/index.php">Training</a></li>
                    <li><a class="navitem" id="nav-consulting" href="../../consulting/index.php">Consulting</a></li>
                    <li><a class="navitem" id="nav-reports" href="../../reports/index.php">Reports</a></li>
                    <li><a class="navitem" id="nav-articles" href="../index.php">Articles</a></li>
                    <li><a class="navitem" id="nav-about" href="../../about/index.php">About NN/G</a></li>
                </ul>
            </nav>
            <nav class="secondary-nav small-12 medium-12 large-12 columns">
                
                
            </nav>
        </div>
    </header>
    <div id="main" class="l-content" onclick="closeNav(event)"><span id="preventClose" style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776; Menu</span>
    
  

  

  <div class="row">
    
      
      <section class="small-12 medium-3 columns collapsable-content l-navrail articles-left-column">
        <h1 class="show-for-small-down collapse-link mobile-browse-header">Browse by Topic and Author</h1>
          <div class="row collapsable">

            
            <div class="small-6 medium-12 columns">
              <section id="article-popular-topics" style="display: none;">
                
                <h1>Topics</h1>
                <ul id="article-popular-topics">
                    
                    <li>
                        
                        <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/intranets/all/index.php">Intranets</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/user-testing/all/index.php">User Testing</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                        
                    </li>
                    
                    <li><a href="#" id="article-see-all-topics"><strong>See all topics&hellip;</strong></a></li>
                </ul>
                
              </section>
              <section id="article-all-topics" style="display: none;">
                
                <h1>
                  <span class="show-for-medium-up">All Article Topics</span>
                  <span class="show-for-small-down">All Topics</span>
                  (<a href="#" id="article-hide-all-topics">hide</a>)
                </h1>
                <ul>
                  
                  <li>
                    
                    <a href="../../topic/accessibility/all/index.php">Accessibility</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/agile/all/index.php">Agile</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/analytics-and-metrics/all/index.php">Analytics &amp; Metrics</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/applications/all/index.php">Application Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/b2b-websites/all/index.php">B2B Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/behavior-patterns/all/index.php">Behavior Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/branding/all/index.php">Branding</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/content-strategy/all/index.php">Content Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/corporate-websites/all/index.php">Corporate Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/design-patterns/all/index.php">Design Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-design-process/all/index.php">Design Process</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/email/all/index.php">Email</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/eyetracking/all/index.php">Eyetracking</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/heuristic-evaluation/all/index.php">Heuristic Evaluation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/human-computer-interaction/all/index.php">Human Computer Interaction</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-ideation/all/index.php">Ideation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/information-architecture/all/index.php">Information Architecture</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/interaction-design/all/index.php">Interaction Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/international-users/all/index.php">International Users</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/intranets/all/index.php">Intranets</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-management/all/index.php">Management</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/navigation/all/index.php">Navigation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/non-profit-websites/all/index.php">Non-Profit Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/personas/all/index.php">Personas</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/persuasive-design/all/index.php">Persuasive Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/prototyping/all/index.php">Prototyping</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/psychology-and-ux/all/index.php">Psychology and UX</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/research-methods/all/index.php">Research Methods</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/search/all/index.php">Search</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/social-media/all/index.php">Social Media</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/strategy/all/index.php">Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/user-testing/all/index.php">User Testing</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-teams/all/index.php">UX Teams</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/visual-design/all/index.php">Visual Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/writing-web/all/index.php">Writing for the Web</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/young-users/all/index.php">Young Users</a>
                    
                  </li>
                  
                </ul>
                
              </section>
            </div>
            <script>
              /* Determine which topics list will be displayed: the full or the
               * partial.  We are inlining this close to the list itself to
               * prevent rendering delays on the client
               */
              var topic_list = $.cookie("article-topics");

              var popular_topics = $('#article-popular-topics');
              var all_topics = $('#article-all-topics');

              if(topic_list == 1) {
                  popular_topics.hide();
                  all_topics.show();
              } else {
                  popular_topics.show();
                  all_topics.hide();
              }
            </script>
            

            
            <div class="small-6 medium-12 columns">
              
              <section id="article-initial-authors">
                  <h1>Author</h1>
                  <ul>
                    
                    <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                    
                    <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                    
                    <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                    
                    <li><a href="#" onClick="$('#article-initial-authors').hide(); $('#article-all-authors').show();"><strong>See all authors&hellip;</strong></a></li>
                  </ul>
              </section>
              <section id="article-all-authors" style="display: none;">
                <h1>
                  All Authors
                  (<a href="#" id="article-hide-all-authors">hide</a>)
                </h1>
                <ul>
                  
                  <li><a href="../author/raluca-budiu/index.php">Raluca Budiu</a></li>
                  
                  <li><a href="../author/patty-caya/index.php">Patty Caya</a></li>
                  
                  <li><a href="../author/susan-farrell/index.php">Susan Farrell</a></li>
                  
                  <li><a href="../author/therese-fessenden/index.php">Therese Fessenden</a></li>
                  
                  <li><a href="../author/kim-flaherty/index.php">Kim Flaherty</a></li>
                  
                  <li><a href="../author/sarah-gibbons/index.php">Sarah Gibbons</a></li>
                  
                  <li><a href="../author/aurora-harley/index.php">Aurora Harley</a></li>
                  
                  <li><a href="../author/kate-kaplan/index.php">Kate Kaplan</a></li>
                  
                  <li><a href="../author/page-laubheimer/index.php">Page Laubheimer</a></li>
                  
                  <li><a href="../author/angela-li/index.php">Angie Li</a></li>
                  
                  <li><a href="../author/hoa-loranger/index.php">Hoa Loranger</a></li>
                  
                  <li><a href="../author/kate-meyer/index.php">Kate Meyer</a></li>
                  
                  <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                  
                  <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                  
                  <li><a href="../author/kara-pernice/index.php">Kara Pernice</a></li>
                  
                  <li><a href="../author/christian-rohrer/index.php">Christian Rohrer</a></li>
                  
                  <li><a href="../author/amy-schade/index.php">Amy Schade</a></li>
                  
                  <li><a href="../author/katie-sherwin/index.php">Katie Sherwin</a></li>
                  
                  <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                  
                  <li><a href="../author/kathryn-whitenton/index.php">Kathryn Whitenton</a></li>
                  
                </ul>
              </section>
              
            </div>
            
          </div>

          
          <section class="hide-for-small-down">
            
              
              <h1>Recent Articles</h1>
              
              <ul>
                  
                  <li><a href="../seamless-cross-channel/index.php">Seamlessness in the Omnichannel User Experience</a></li>
                  
                  <li><a href="../brand-intention-interpretation/index.php">Experience Design: Bridging Brand Intention and Brand Interpretation</a></li>
                  
                  <li><a href="../eyetracking-tasks-efficient-scanning/index.php">Scanning Patterns on the Web Are Optimized for the Current Task</a></li>
                  
                  <li><a href="../flat-design-best-practices/index.php">Flat-Design Best Practices</a></li>
                  
                  <li><a href="../ux-user-stories/index.php">Accounting for UX Work with User Stories in Agile Projects</a></li>
                  
                  <li><a href="../index.php"><strong>See all articles&hellip;</strong></a></li>
              </ul>
              
            
          </section>
          

          
          
          <section class="hide-for-small-down">
            <h1>Popular Articles</h1>
            
            <ul>
                
                <li><a href="../usability-101-introduction-to-usability/index.php">Usability 101: Introduction to Usability</a></li>
                
                <li><a href="../top-10-mistakes-web-design/index.php">Top 10 Mistakes in Web Design</a></li>
                
                <li><a href="../how-users-read-on-the-web/index.php">How Users Read on the Web</a></li>
                
                <li><a href="../f-shaped-pattern-reading-web-content/index.php">F-Shaped Pattern For Reading Web Content</a></li>
                
                <li><a href="../ten-usability-heuristics/index.php">10 Usability Heuristics for User Interface Design</a></li>
                
                <li><a href="../intranet-design-2016/index.php">10 Best Intranets of 2016</a></li>
                
                <li><a href="../which-ux-research-methods/index.php">When to Use Which User-Experience Research Methods</a></li>
                
                <li><a href="../response-times-3-important-limits/index.php">Response Times: The 3 Important Limits</a></li>
                
                <li><a href="../why-you-only-need-to-test-with-5-users/index.php">Why You Only Need to Test with 5 Users</a></li>
                
                <li><a href="../page-fold-manifesto/index.php">The Fold Manifesto: Why the Page Fold Still Matters</a></li>
                
            </ul>
          </section>
          
          

          
            <section class="hide-for-small-down">
              
            </section>
          

      </section>
      

      
        <div class="small-12 medium-9 columns content-body">
          




<article class="l-subsection alertbox-article-content article-detail-bl">
  
  <header>
    <div class="l-subsection">
      <h1>Assessing the Usability of a User Interface Standard</h1>
      <div class="author-meta">by
        
          
            
            <a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a>
            
          
        on  April 28, 1991
        
      </div>

      

      
      <div class="topic-links">
        Topics:
        
          <ul>

  <li><a href="../../topic/ux-management/index.php">Management</a></li>

  <li><a href="../../topic/applications/index.php">Application Design</a></li>

</ul>
        
      </div>
      
    </div>
  </header>
  

  
  
  <section>
    <p class="lede"><strong>Summary:</strong> User interface standards can be hard to use for developers. In a laboratory experiment, 26 students achieved only 71% compliance with a two page standard; many violations were due to influence from previous experience with non-standard systems. In a study of a real company&#39;s standard, developers were only able to find 4 of 12 deviations in a sample system, and three real products broke between 32% and 55% of the mandatory rules in the standard. Designers were found to rely heavily on the examples in the standard and their experience with other user interfaces.</p>
    <br />
  </section>
  
  

  
  <section id="article-body"><p><em>Originally published as: </em><em style="font-size: 12px;">Thovtrup, H., and Nielsen, J. (1991). Assessing the usability of a user interface standard. Proc. ACM CHI'91 Conf. Human Factors in Computing Systems (New Orleans, LA, 28 April–2 May), 335–341.</em></p>

<h2>Introduction</h2>

<p>User interface standards have become the object of increasingly intense activities in recent years [Abernethy 1988; Holdaway and Bevan 1989], including work in the International Standards Organization (ISO) [Brooke et al. 1990] and the European Community [Stewart 1990]. Work is also going on in national standards organizations [Dzida 1989] and in several major computer companies [Berry 1988; Nielsen 1989b]. These activities are part of a general current interest in information processing standards [Berg and Schumny 1990] but are also based on the widely held feeling that consistency is one of the most important usability considerations [Nielsen 1989b]. Even though consistency is obviously not the only usability factor [Grudin 1989], there are still good reasons to strive to obtain it in balance with other usability considerations [Nielsen 1990b] in a usability engineering process [<a href="../../books/usability-engineering/index.php" title="'UsabilityEngineering' book summarypage">Nielsen 1994</a>], and such additional considerations are indeed also included in many current standards activities.</p>

<p>Given the potential future importance of usability standards, it seems reasonable to study the usability of the standards themselves to assess whether developers can actually apply the content of the documents. Not much research is available on this topic yet, but existing evidence does indicate the potential for "meta-usability problems" (usability problems in a usability document). Mosier and Smith [1986] report that only 58% of the users of a large collection of interface guidelines found the information they were looking for (an additional 36% "sometimes found it"). de Souza and Bevan [1990] had three designers design an interface using a draft of the ISO standard for menu interfaces and report that they violated 11% of the rules and had difficulties in interpreting 30% of the rules.</p>

<p>The draft standard was improved after the experiment, so the main lesson from this study is the need for <strong> usability testing of usability standards</strong>: the ability of designers to use and understand a standard can have more impact on interface quality then the rules specified in the standard. As with all system design, if the intended users (in this case, user interface designers) cannot use the system (in this case, a design standard) or have trouble doing so, the proper response is to redesign the system to make it more usable.</p>

<p>For a user interface standard to increase usability in the resulting products, two conditions have to be met: The standard must specify a usable interface, and the standard must be usable by developers so that they actually build the interface according to the specifications. As reported by Potter et al. [1990], a user interface may have usability problems even when an interface standard is followed without violations. The usability of the resulting interfaces is obviously extremely important for the development of the actual content of interface standards, but the present paper will concentrate on whether developers can use standards. First, we report on a small laboratory study, and the main part of the paper than reports on a field study of the use of a real standard.</p>

<h2>Laboratory Study of a Standard</h2>

<p>In one small experiment, 26 computer science students who were taking a user interface design class were asked to design an interface for a hypothetical company having a two page user interface standard. The standard described the use of several special function keys and the way the screen was partitioned into various fixed fields. In addition to the two page standard, the students were given a three page specification of a sample system that complied with the standard. The average degree of <strong> compliance in the resulting designs was rated at 71% </strong> by (subjectively) comparing them with a checklist of design elements specified in the standard.</p>

<p>These designers were presumably highly motivated to follow the standard since a part of their course grade was determined by their designs and they knew from previous lectures that compliance was to be considered a major usability consideration. Also, the standard was very small and easy to follow, making it almost impossible to overlook rules. Even so, the results show that the designs deviated substantially from the standard. The use of special keys deviated the most, possibly because the test standard specified a drastically different use of the keyboard than that used in most actual systems with which the students were familiar. For example, it specified the use of special "yes" and "no" keys to answer questions and did not use the traditional numbered function keys. It seemed that <strong> experience with running systems in everyday use influenced many of the designers more than the standards document.</strong> In fact, only 35% of the designs received a perfect score for compliance with the standard's use of special keys. The remaining 65% were at least partly influenced by the use of special keys in outside systems, and 15% of the designs were scored as having zero compliance on this point.</p>

<h2>The Data Company and Its Standard</h2>

<p>We studied the actual use of an in-house user interface standard at a medium sized Danish company which we will call <cite> The Data Company. </cite> This company is a mixture of a software house and a service bureau and supplies several software products to its customers. The software is mainly mainframe software for traditional, text-only terminals, and each system typically contains between 50 and 300 screens. Because of this fairly large number of screens in each product, consistency is a highly desirable usability attribute for The Data Company. Also, several customers use more than one product, further indicating the need for an interface standard. The Data Company uses incompatible mainframes from two major computer companies with one extremely large vendor supplying most of the machines and another very large vendor supplying a smaller number of machines. A single interface standard was also desirable as a way to smooth over the differences between these two environments.</p>

<table align="center" border="" cellspacing="1" hspace="12" vspace="12">
	<caption align="bottom"><strong>Table 1. </strong> <em> Pagecount of various user interface standards or guidelines (including front matter).<br/>
	Note that ISO 9241 will eventually have 19 parts. The pagecount given here is a projection on the basis of drafts of six of these parts, assuming that the remaining parts will have the same average pagecount as these parts. Also note that the complete set of ISO standards applicable to user interface design will include several additional documents such as the JTC1/SC18/WG9 work on "Specifications for objects, actions, operations, and applications" (50 page draft), "Cursor control" (13 page draft), and "Icons for use on screens" (34 page draft). See [Billingsley 1990a,b] for further lists of international standards. </em></caption>
	<tbody>
		<tr>
			<th>Standard or Guideline</th>
			<th>Pages</th>
		</tr>
		<tr>
			<td>DIN 66234 part 8 standard [1988] (note: <em> dense </em> pages)</td>
			<td align="RIGHT">6</td>
		</tr>
		<tr>
			<td>The Data Company's standard</td>
			<td align="RIGHT">57</td>
		</tr>
		<tr>
			<td>Apple Human Interface Guidelines [1987]</td>
			<td align="RIGHT">166</td>
		</tr>
		<tr>
			<td>Motif™ style guide [OSF 1990]</td>
			<td align="RIGHT">167</td>
		</tr>
		<tr>
			<td>"Advanced CUA" (for graphical interfaces) [IBM 1990a]</td>
			<td align="RIGHT">209</td>
		</tr>
		<tr>
			<td>"Basic CUA" (for traditional terminals) [IBM 1990b]</td>
			<td align="RIGHT">288</td>
		</tr>
		<tr>
			<td>Original CUA [IBM 1987]</td>
			<td align="RIGHT">340</td>
		</tr>
		<tr>
			<td>OPEN LOOK™ [Sun Microsystems 1990]</td>
			<td align="RIGHT">404</td>
		</tr>
		<tr>
			<td>Smith and Mosier [1986] guidelines</td>
			<td align="RIGHT">485</td>
		</tr>
		<tr>
			<td>ISO 9241 (mostly not published yet) - <em> estimated </em></td>
			<td align="RIGHT">532</td>
		</tr>
	</tbody>
</table>

<p>The Data Company released its interface standard in 1989 as a fairly small document of 57 pages (cf. Table 1). The standard was deliberately limited to dealing with text-only mainframe interfaces. Current plans call for the release of an additional standard for graphical interfaces in 1991.</p>

<p><img alt="Correct tab order grouping" src="http://media.nngroup.com/media/editor/2012/10/31/standards_rightwrong.gif" style="width: 600px; height: 217px;"/><br/>
<strong>Figure 1: </strong> <em> Example from The Data Company's standard of how to position input fields to utilize the left-to-right tabulating sequence of cursor movements on alphanumeric terminals. </em></p>

<p>After a small section giving recommendations for the process of constructing usable interfaces, the standard defines requirements for menu and command oriented systems. It defines standard function key assignments (F1=help, etc.) and a standard terminology (for example, "personal code" is to be used instead of terms like "userid"). The standard then describes methods to ensure that the user can control and customize the system (e.g., it must be possible to turn audible beeps off). A large part of the standard is devoted to a definition of proper screen design and layout, including the way input and output fields should be shown and labelled. Figure 1 shows an example used to illustrate rules about the tabulator sequencing used to move the cursor between fields. The standard further contains sections on color and highlighting, printouts, help and error messages, and security. The standard has several example screen dumps used to illustrate the rules. These examples are in Danish and are mostly difficult to translate. Figure 2 shows a translated version of one of the examples used in the standard to illustrate the navigation system through hierarchical menus. This example only shows the header area of the standard screen layout whereas most examples show full screen dumps and actual text (menus, field labels, sample user input, etc.).</p>

<p><img alt="Header area standard" src="http://media.nngroup.com/media/editor/2012/10/31/standards_headerarea.gif" style="width: 597px; height: 83px;"/><br/>
<strong>Figure 2: </strong> <em> Example (translated) from The Data Company's standard of the header area of a screen with the user's navigational location encoded in the upper left corner (the user is in the YHR system's MO area's BE subsystem's L subsubsystem's V screen. </em></p>

<p>A mixture of methods was used to assess the usability of the standard. One study was aimed at The Data Company's developers and their opinions about the standard and their ability to follow it. Another study looked at The Data Company's actual products and how closely they followed the standard.</p>

<h2>Developers' Subjective View of the Standard</h2>

<p>For the study of the developers, 15 developers from a total of seven different projects were visited and asked a number of questions using both a structured and a free-form interview. The participants were, unfortunately, selected with a bias towards developers with a higher than average interest in usability since participation in the study was voluntary. Indeed, 14 of the 15 participants indicated that they had taken a course in usability. This bias should be kept in mind in interpreting the results from the study.</p>

<p>In general, the developers had a very positive attitude toward the standard. As many as <strong> 73% could "fully agree" that The Data Company should have a user interface standard </strong> and nobody disagreed. The average agreement on a 1–5 scale was 4.7 (where 1="completely disagree" and 5="fully agree"). The developers also liked the actual content of the standard; the screen designs were rated 4.3 and the dialogue flow was rated 4.2 on a 1–5 scale with nobody using the negative ratings of 1 or 2. The developers even had a fairly positive opinion of the possibility for introducing an official national Danish standard to cover all vendors and software houses, giving a proposal to do so a rating of 3.6 on the 1–5 scale. When asked whether it would have been better for The Data Company to have adopted the standard from its main vendor instead of writing its own, the developers rated their agreement as 3.1 on the average on the 1–5 scale. The underlying distribution of attitudes towards vendor standards was bimodal, however, with a majority (53%) of developers having a neutral or partly positive attitude and a minority of 27% having strong negative feelings. In free-form interviews, some developers who developed for the minority vendor's machines claimed that the main vendor's standard was inappropriate for their platform because of differences such as varying keyboard design.</p>

<p>On the 1–5 scale, the developers only gave a rating of 1.9 to the suggestion that a set of recommendations would have been better than an actual standard, thus again expressing their favorable attitudes toward having standards. They did like the fact that the standard included some recommendations and "good advice" in addition to the formal requirements (rating this 4.7 on the 1–5 scale). During the free-form interview, one major reason mentioned for wanting a formal standard was that it helped minimize wasted time during project meetings. Prior to the introduction of the standard, a lot of time was spent arguing about minor interface design details whereas now it was possible to close such discussions rapidly by referring to the standard, thus making it possible to concentrate on higher-level matters.</p>

<p>Despite the mainly positive attitudes towards the standard, there were also negative points mentioned during the interviews. Even though 67% of the developers felt that the standard made it easier for them to design screens and develop the associated software, 20% disagreed with that statement. As many as 53% of the developers complained that they did not have sufficiently good programming tools to support the user interface requirements made by the standard.</p>

<p>The question about whether they would comply with the standard under all circumstances divided the developers into two camps, with 33% who would sometimes deviate from the standard. Of the 67% "loyalists," only 27% were "blind loyalists" who would follow the standard no matter what, whereas the other 40% could only partly agree that they would follow the standard under all circumstances. Considering the probable sampling bias of using volunteer developers with an interest in usability, it is likely that the true proportion of "deviators" is even larger for the non-sampled group of developers. One perspective on this result might be that a standard is only a true standard if it is followed closely under all circumstances and that one should therefore institute very strict quality assurance policies to enforce compliance. An alternative perspective, as argued by Tognazzini [1989], is that user interface standards have a somewhat different character than other computer standards and one should actually be allowed to deviate from them if one has very good reasons for doing so. In any case, the replies to this question certainly indicate the need for some way to minimize deviations since the developers do not feel much bound by the standard.</p>

<p>One main underlying reason for the tendency to deviate from the standard can be found in the answers to the question <strong> whether the standard restricted the creativity of system designers</strong>. All "loyalists" answered no to this question while all "deviators" answered yes, suggesting that better compliance with standards will follow if they can be made to seem less confining. Again, all 33% "deviators" answered yes to whether the standard precluded new design ideas, further indicating a rationale for their discontent. Even so, 20% of the 67% "loyalists" also answered yes to this question, and the average "loyalist" rating of agreement was 2.8 on the 1–5 scale (only a very slight disagreement—in fact, almost neutral). This indicates that limitations on the product may not be as influential as are limitations on the people as a motivation to break the standard.</p>

<p>Looking at the usability of the standard as a document, as many as <strong> 53% of the developers said that the rules in the standard were difficult to remember</strong>. Only 27% found the rules easy to remember, and 20% answered "don't know" to this question. 27% said that the standard was difficult to apply, 40% that it was easy to apply, and 33% had a neutral opinion on this issue.</p>

<p>Regarding the size of the document, 60% felt that the standard was too small, compared with 40% who felt that it was the appropriate size and nobody who felt that it was too large. As shown by Table 1, The Data Company's standard is indeed considerably smaller than most other standards. It does not follow, however, that a larger standard would actually be easier to apply or to remember. When it comes to traditional computer manuals, it is certainly often the case that users express a desire for larger and more complete manuals [Nielsen 1989a] even though smaller and more concise manuals are sometimes better for them [Carroll 1990].</p>

<h2>Measuring Developers' Ability to Use the Standard</h2>

<p>As reported above, many developers reported that the standard was difficult to remember and to apply. To measure the developers' actual abilities to use the standard, the same 15 developers were asked to apply the standard in a small test using a method similar to that used by Molich and Nielsen [1990]: <strong> Developers were presented with a concrete design in the form of screen dumps and were asked to list all its deviations </strong> from the standard. They were given 15 minutes to do so and were allowed to use the standard document as much as they wanted during the exercise. The test design contained four screens and represented a hypothetical system for the administration of a dog owners' tax. The design contained 12 deviations from the standard.</p>

<p>On the average, the developers only found 4.0 out of these 12 deviations. The top scorer was one developer who found 7 of the 12 deviations. This performance was surprisingly poor, especially considering the bias inherent in our sample of developers with above-average interest in usability. Table 2 lists the 12 deviations and the number of developers who found them. Three of the 15 developers had served as user interface coordinators for their projects. They found on average 5.7 as opposed to the 3.4 deviations found by those 12 developers who had not had the role of user interface coordinator. This difference (significant at <i> p </i> &lt;0.01) might be due to the coordinators' greater experience at looking at other people's screen design, but they still performed surprisingly poorly on the test task.</p>

<table align="center" border="" hspace="17" vspace="17">
	<caption align="bottom"><strong>Table 2. </strong> <em> Deviations from the standard in a four-screen test design. The deviations are sorted accord-ing to the number of the 15 developers who found each deviation. </em></caption>
	<tbody>
		<tr>
			<th>Deviation from the standard</th>
			<th>Developers finding the deviation</th>
		</tr>
		<tr>
			<td>1. Absence of the colon which is supposed to mark the start of a data display field</td>
			<td align="RIGHT">15</td>
		</tr>
		<tr>
			<td>2. No abbreviation given for menu options</td>
			<td align="RIGHT">13</td>
		</tr>
		<tr>
			<td>3. Wrong format for a date (hyphen used instead of slash as a separator between elements)</td>
			<td align="RIGHT">9</td>
		</tr>
		<tr>
			<td>4. Deviation from standard terminology ("personal identifier" instead of the proper "personal code")</td>
			<td align="RIGHT">5</td>
		</tr>
		<tr>
			<td>5. F2 used for "main menu" command instead of F12</td>
			<td align="RIGHT">4</td>
		</tr>
		<tr>
			<td>6. One screen did not have the required indication of navigational location in the menu hierarchy</td>
			<td align="RIGHT">4</td>
		</tr>
		<tr>
			<td>7. The sub-system for updating the database did not contain an explicit update command</td>
			<td align="RIGHT">4</td>
		</tr>
		<tr>
			<td>8. No measurement unit shown for the field giving the weight of the dog</td>
			<td align="RIGHT">2</td>
		</tr>
		<tr>
			<td>9. Grouping of fields not appropriate for the tabulator sequence on the screen</td>
			<td align="RIGHT">2</td>
		</tr>
		<tr>
			<td>10. Wrong format for a date (no leading zero given for month numbers &lt;10 on the first screen)</td>
			<td align="RIGHT">1</td>
		</tr>
		<tr>
			<td>11. The sub-system for updating the database did not contain an undo function</td>
			<td align="RIGHT">1</td>
		</tr>
		<tr>
			<td>12. One of the menus has no Quit command (it should be possible to quit directly from everywhere)</td>
			<td align="RIGHT">0</td>
		</tr>
	</tbody>
</table>

<p>In addition to being poor at finding the actual deviations in the test screens, many <strong> developers claimed that certain design details were deviations even though they were not</strong>. Only two of the 15 developers did not indicate such spurious deviations. The average number of spurious deviations per developer was 1.6. The two most common spurious deviations were the centering of the line listing the valid function keys (mentioned by 11) and the use of navigational abbreviations on the screens instead of full subsystem names (mentioned by 7). The standard actually does not state anything about the justification of the line listing the function keys but all the example screenshots in the document happen to show left justified lines. As will be further discussed below, the <strong> examples were more influential than the actual formal specifications </strong> in the standard. The use of navigational abbreviations was authorized by the standard but had not been used by any actual system at The Data Company yet. It seems that concrete, implemented product designs were more salient than the abstract standards document in defining the corporate interface style in the minds of the developers.</p>

<table align="center" border="" cellpadding="5" cellspacing="1" hspace="18" vspace="18">
	<caption align="bottom"><strong>Table 3. </strong> <em> Use of different parts of the standard during the experiment where 15 developers tried to find deviations in a sample design. The right column indicates the proportion of the total number of times the developers accessed the document. </em></caption>
	<tbody>
		<tr>
			<th>Part of the document</th>
			<th>% use</th>
		</tr>
		<tr>
			<td>Main document (the actual standard specification)</td>
			<td align="CENTER">25%</td>
		</tr>
		<tr>
			<td>Example screenshots</td>
			<td align="CENTER">21%</td>
		</tr>
		<tr>
			<td>List of function keys and their standard use</td>
			<td align="CENTER">21%</td>
		</tr>
		<tr>
			<td>Table of contents and index</td>
			<td align="CENTER">18%</td>
		</tr>
		<tr>
			<td>Word list of approved screen terms</td>
			<td align="CENTER">16%</td>
		</tr>
	</tbody>
</table>

<p>Table 3 shows the distribution of the developers' use of the standards document during the experiment. They made comparatively <strong> little use of the main document containing the actual specification and rules of the standard and instead relied on the examples and the concrete lists of approved function keys and screen terms</strong>. This corresponds well with results from studies of written instructions in general [LeFevre and Dixon 1986] showing that users often rely the most on examples. Also, when asked during the interview, 80% of the developers could "partly agree" that the examples were used more than the actual rules and specifications. The remaining 20% would "neither agree nor disagree" with this statement.</p>

<h2>Product Compliance</h2>

<p>Three of The Data Company's released products were carefully inspected by the authors to assess their compliance with the standard. The developers of the system were confronted with the list of violations and in no case disputed they were indeed in conflict with the standard. In order to carry out the inspection, it was necessary to develop a standardized checklist listing all the rules in the standard and dividing them into three groups: Mandatory rules, voluntary rules, and guidelines for the development process (as opposed to rules for the resulting product). The checklist had 22 mandatory rules, 6 voluntary rules, and 5 guidelines for the design process. Table 4 shows the results from this compliance test. Most of the deviations were judged to have fairly minor impact on the final usability of the product and mainly have the effect of reducing the feeling of "product family" across products.</p>

<p>Of the five guidelines for the development process (study the characteristics of the user population, have user representatives participate during the process, run thinking aloud tests of the interface, use iterative techniques such as <a href="../../reports/prototyping/index.php" title="Nielsen Norman Group: Paper Prototyping: A How-To Video (32 minute DVD)"> prototyping</a>, and have an appointed coordinator for the entire user interface), all three projects had omitted the thinking aloud test and had complied with the remaining four.</p>

<p>With respect to the mandatory rules, Table 4 shows a fairly <strong> large number of deviations from the standard</strong>, especially with respect to the number of rules broken (between a third and a half of the 22 rules). The present data are too limited for any firm conclusions but seem to indicate a possible trend towards having more rules broken in larger interfaces. Given that a larger system (especially one with more screens) presents more opportunities for the designers to make mistakes, it is somewhat surprising that this trend is not stronger.</p>

<p>When asked why their products deviated from the standard, the developers mostly claimed that they had chosen alternative design solutions because they had found them to be better than the one mandated by the standard. Two other frequent explanations were that the development tools did not allow compliance with the standard and that the developers had indeed planned compliance but had not yet had time to implement it. The first explanation is related to the "loyalist"/"deviator" discussion above, and the latter two explanations indicate the need for good development tools to make it easy to implement interfaces that follow the standard [Tognazzini 1989]. Further explanations were that the developers were not aware of the rule they had broken or that they had overlooked the deviation. In no case did it turn out that the developers had actively misinterpreted the standard and designed a specific deviating interface feature in the explicit belief that they were following a rule from the standard. This indicates that the individual parts of the standard are reasonably understandable - perhaps because the standard almost always contains elaborations of the rules and backs them up with a rationale.</p>

<table align="center" border="" cellpadding="5" cellspacing="1" hspace="12" vspace="12">
	<caption align="bottom"><strong>Table 4. </strong> <em> Results from investigating three systems' compliance with the standard. </em></caption>
	<tbody>
		<tr>
			<th>System</th>
			<th>1</th>
			<th>2</th>
			<th>3</th>
		</tr>
		<tr>
			<td>Number of screens</td>
			<td align="RIGHT">250</td>
			<td align="RIGHT">80</td>
			<td align="RIGHT">40</td>
		</tr>
		<tr>
			<td>Number of developers on project</td>
			<td align="RIGHT">32</td>
			<td align="RIGHT">5</td>
			<td align="RIGHT">4</td>
		</tr>
		<tr>
			<td>Deviations from mandatory rules</td>
			<td align="RIGHT">31</td>
			<td align="RIGHT">38</td>
			<td align="RIGHT">18</td>
		</tr>
		<tr>
			<td>Number of mandatory rules broken (of 22)</td>
			<td align="RIGHT">12</td>
			<td align="RIGHT">9</td>
			<td align="RIGHT">7</td>
		</tr>
		<tr>
			<td>Number of voluntary rules broken (of 6)</td>
			<td align="RIGHT">3</td>
			<td align="RIGHT">4</td>
			<td align="RIGHT">3</td>
		</tr>
		<tr>
			<td>Number of design process guidelines broken (of 5)</td>
			<td align="RIGHT">1</td>
			<td align="RIGHT">1</td>
			<td align="RIGHT">1</td>
		</tr>
	</tbody>
</table>

<h2>Conclusions</h2>

<p>The results presented above show that user interface standards are very likely to be violated. Even our highly motivated, biased sample of developers did not express completely blind loyalty towards the standard. They were not able to apply the standard very well, and the concrete designs we investigated did indeed contain many violations.</p>

<p>To <strong> increase the usability of user interface standards, we recommend </strong></p>

<ul>
	<li>having <strong> development tools or Web templates </strong> that support implementation of interfaces that follow the standard</li>
	<li>including many concrete <strong> examples </strong> of correctly designed interfaces</li>
	<li>making sure that all <strong> examples are 100% compliant </strong> with the standard (i.e., that they are correct in all details and not just with respect to the point they are supposed to illustrate)</li>
	<li><strong>complying with older standards </strong> as much as possible - otherwise changes should be highlighted and explained (since designers rely extensively on experience with older standards)</li>
</ul>

<p>These recommendations correspond fairly well with the advice Happ and Cohen [1989] derived from interviewing a group of developers. We would add a warning to make sure that the examples do not inadvertently misrepresent the standard since the examples can have as much impact on developers as the formal specifications. Similarly, one should be aware that existing systems also influence developers heavily. Therefore, if a deviation from previous design practice is desired, it will probably be necessary to state so explicitly and to provide a rationale for why the new interface style is better.</p>

<p>Furthermore, the standards document itself should be designed according to recognized principles for good document design and include good <strong> access mechanisms such as a thorough index and table of contents, word lists, glossaries, and other checklists </strong> (such as a list of function keys). Table 3 indicates heavy reliance on such access mechanisms. Possibly computerized access mechanism such as hypertext [Nielsen 1990a, especially pp. 50–52] could be used to good effect, especially since they would allow the display of dynamic, animated examples which will probably be important for the standardization of modern interaction techniques.</p>

<p>Finally, our personal experience from this work indicates that a checklist of specified design elements and rules helps tremendously during a conformance quality assurance review.</p>

<h3>Acknowledgements</h3>

<p>The authors would like to thank Rita Bush, Susan Dumais, Tom Landauer, Jim Williams, and the referees for helpful comments on earlier versions of the manuscript.</p>

<h2>References</h2>

<dir>
</dir>

<p>Abernethy, C.N. (1988). Human–computer interface standards: Origins, organizations and comment. In Oborne, D.J. (Ed.), <cite> International Review of Ergonomics </cite> <strong> 2 </strong> , 31–54.</p>

<p>Apple Computer (1987). <cite> Human Interface Guidelines: The Apple Desktop Interface, </cite> Addison-Wesley.</p>

<p>Berg, J.L., and Schumny, H. (Eds.) (1990). <cite> An Analysis of the Information Technology Standardization Process, </cite> Elsevier Science Publishers, Amsterdam, the Netherlands.</p>

<p>Berry, R.E. (1988). Common User Access - A consistent and usable human–computer interface for the SAA environment, <cite> IBM Systems Journal </cite> <strong> 27 </strong> , 3, 281–300.</p>

<p>Billingsley, P. (1990a). The standards factor: Committee updates, <cite> ACM SIGCHI Bulletin </cite> <strong> 21 </strong> , 4 (April), 16–19.</p>

<p>Billingsley, P. (1990b). The standards factor: Standards on the horizon, <cite> ACM SIGCHI Bulletin </cite> <strong> 22 </strong> , 2 (October), 10–12.</p>

<p>Brooke, J., Bevan, N., Brigham, F., Harker, S., and Youmans, D. (1990). Usability assurance and standardization—work in progress in ISO, <cite> Proceedings IFIP INTERACT'90 </cite> (Cambridge, U.K., 27–31 August), 357–361.</p>

<p>Carroll, J.M. (1990). <cite> The Nurnberg Funnel: Designing Minimalist Instruction for Practical Computer Skill, </cite> The MIT Press.</p>

<p>de Souza, F., and Bevan, N. (1990). The use of guidelines in menu interface design, <cite> Proceedings IFIP INTERACT'90 </cite> (Cambridge, U.K., 27–31 August), 435–440.</p>

<p>DIN (1988). <cite> Bildschirmarbeitsplätze: Grundsätze ergonomischer Dialoggestaltung </cite> (VDU work stations: Principles of ergonomic dialogue design, in German), Deutsches Institut für Normung <strong> DIN 66234, Teil 8</strong>.</p>

<p>Dzida, W. (1989). The development of ergonomic standards, <cite> ACM SIGCHI Bulletin </cite> <strong> 20</strong>, 3 (January), 35–43.</p>

<p>Furnas, G.W., Landauer, T.K., Gomez, L.M., and Dumais, S.T. (1987). The vocabulary problem in human–system communication, <cite> Communications of the ACM </cite> <strong> 30 </strong> , 11 (November), 964–971.</p>

<p>Grudin, J. (1989). The case against user interface consistency, <cite> Communications of the ACM </cite> <strong> 32 </strong> , 10 (October), 1164–1173.</p>

<p>Happ, A.J., and Cohen, K.C. (1989). Consistency in the user interface: Common User Access impact study, <cite> Technical Report </cite> <strong> TR54.513</strong>, IBM Entry Systems Division, Boca Raton, FL.</p>

<p>Holdaway, K., and Bevan, N. (1989). User system interaction standards, <cite> Computer Communications </cite> <strong> 12</strong>, 2 (April), 97–102.</p>

<p>IBM (1987). <cite> Systems Application Architecture: Common User Access: Panel Design and User Interaction, </cite> Document <strong> SC26-4351-0</strong>.</p>

<p>IBM (1990a). <cite> Systems Application Architecture: Common User Access: Advanced Interface Design Guide, </cite> Document <strong> SC26-4582-0</strong>.</p>

<p>IBM (1990b). <cite> Systems Application Architecture: Common User Access: </cite> <cite> Basic Interface Design Guide, </cite> Document <strong> SC26-4583-0</strong>.</p>

<p>LeFevre, J-A., and Dixon, P. (1986). Do written instructions need examples?, <cite> Cognition and Instruction </cite> <strong> 3 </strong> , 1, 1–30.</p>

<p>Molich, R., and Nielsen, J. (1990). Improving a human-computer dialogue, <cite> Communications of the ACM </cite> <strong> 33 </strong> , 3 (March), 338–348.</p>

<p>Mosier, J.N., and Smith, S.L. (1986). Application of guidelines for designing user interface software, <cite> Behaviour and Information Technology </cite> <strong> 5</strong>, 1 (January–March), 39–46.</p>

<p>Nielsen, J. (1989a). What do users really want?, <cite> International Journal of Human–Computer Interaction </cite> <strong> 1</strong>, 2, 137–147.</p>

<p>Nielsen, J. (Ed.) (1989b). <cite> <a href="../../books/coordinating-user-interfaces-for-consistency/index.php"> Coordinating User Interfaces for Consistency</a>, </cite> Academic Press, San Diego, CA.</p>

<p>Nielsen, J. (1990a). <cite> <a href="../../books/hypertext-and-hypermedia/index.php"> Hypertext and Hypermedia</a>, </cite> Academic Press, San Diego, CA.</p>

<p>Nielsen, J. (1990b). Traditional dialogue design applied to modern user interfaces, <cite> Communications of the ACM </cite> <strong> 33 </strong> , 10 (October), 109–118.</p>

<p>Nielsen, J. (1994). <cite> <a href="../../books/usability-engineering/index.php"> Usability Engineering</a>  </cite> (paperback edition), AP Professional, Boston, MA.</p>

<p>Open Software Foundation (1990). <cite> OSF/Motif Style Guide, </cite> Prentice Hall.</p>

<p>Potter, S.S., Cook, R.I., Woods, D.D., and McDonald, J.S. (1990). The role of human factors guidelines in designing usable systems: A case study of operating room equipment, <cite> Proc. Human Factors Society 34th Annual Meeting </cite> (Orlando, FL, 8–12 October), 392–395.</p>

<p>Scapin, D.L. (1990). Organizing human factors knowledge for the evaluation and design of interfaces, <cite> International Journal of Human–Computer Interaction </cite> <strong> 2 </strong> , 3, 203–229.</p>

<p>Smith, S.L., and Mosier, J.N. (1986). <cite> Design Guidelines for Designing User Interface Software </cite> . Technical Report MTR-10090 (August), The MITRE Corporation, Bedford, MA 01730, USA.</p>

<p>Stewart, T. (1990). SIOIS—Standard interfaces or interface standards, <cite> Proceedings IFIP INTERACT'90 </cite> (Cambridge, U.K., 27–31 August), xxix–xxxiv.</p>

<p>Sun Microsystems (1990). <cite> OPEN LOOK Graphical User Interface Application Style Guidelines, </cite> Addison-Wesley.</p>

<p>Tognazzini, B. (1989). Achieving consistency for the Macintosh. In Nielsen, J. (Ed.), <cite> <a href="../../books/coordinating-user-interfaces-for-consistency/index.php"> Coordinating User Interfaces for Consistency</a>, </cite> Academic Press, 57–73.</p>

<p>Williams, J.R. (1989). Guidelines for dialogue design. In Salvendy, G., and Smith, M.J. (Eds.), <cite> Designing and Using Human–Computer Interfaces and Knowledge Based Systems, </cite> Elsevier Science Publishers, 589–596.</p>
</section>
  

  
  <section>
    
        
    
  </section>
  
  
  
  
  

  


  <section>
    <br />
    <p class="share"><b>Share <span class="hide-for-small-down">this article</span>:</b> <a href="https://twitter.com/intent/tweet?url=http://www.nngroup.com/articles/assessing-usability-user-interface-standard/&amp;text=Assessing%20the%20Usability%20of%20a%20User%20Interface%20Standard&amp;via=nngroup">Twitter</a> | <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=http://www.nngroup.com/articles/assessing-usability-user-interface-standard/&amp;title=Assessing%20the%20Usability%20of%20a%20User%20Interface%20Standard&amp;source=Nielsen%20Norman%20Group">LinkedIn</a> | <a href="https://plus.google.com/share?url=http://www.nngroup.com/articles/assessing-usability-user-interface-standard/">Google+</a> | <a href="mailto:?subject=NN/g Article: Assessing the Usability of a User Interface Standard&amp;body=http://www.nngroup.com/articles/assessing-usability-user-interface-standard/">Email</a></p>
    <hr />
  </section>

  <footer class="l-subsection">
    
    
    <div class="learn-more">
      <h2>Learn More</h2>
      <p><a href="../subscribe/index.php">Subscribe to the weekly newsletter</a> to get notified about future articles.</p>
      </div>
    
    

    <div class="row show-for-small-down related-content">
      
      
      

      
      
        <div class="column small-12 topic-links">All articles about:
          <ul>
            
            <li><a href="../../topic/applications/index.php">Application Design</a></li>
            
            <li><a href="../../topic/ux-management/index.php">Management</a></li>
            
            <li><a href="../../topic/standards-conventions/index.php">standards &amp; conventions</a></li>
            
          </ul>
        </div>
      
      

      
      
        <div class="column small-12">
          <h3>UX Conference Training Courses</h3>
           <ul class="no-bullet">
    
        <li><a href="../../courses/facilitating-ux-workshops/index.php">Facilitating UX Workshops</a></li>
    
        <li><a href="../../courses/ux-deliverables/index.php">UX Deliverables</a></li>
    
        <li><a href="../../courses/hci/index.php">User Interface Principles Every Designer Must Know</a></li>
    
        <li><a href="../../courses/lean-ux-and-agile/index.php">Lean UX and Agile</a></li>
    
        <li><a href="../../courses/stakeholder-ux-approval/index.php">Engaging Stakeholders to Build Buy-In</a></li>
    
</ul>
        </div>
      
      

      
      
        <div class="small-12 column">
          <h3>Research Reports</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../reports/usability-return-on-investment-roi/index.php">Return on Investment (ROI) for Usability</a></li>
              
            
              
                <li><a href="../../reports/best-applications-2/index.php">Application Design Showcase: 2012</a></li>
              
            
              
                <li><a href="../../reports/agile-development-user-experience/index.php"> Effective Agile UX Product Development</a></li>
              
            
              
                <li><a href="../../reports/user-experience-careers/index.php">User Experience Careers</a></li>
              
            
              
                <li><a href="../../reports/enterprise-mobile-showcase/index.php">Mobile Intranets and Enterprise Apps</a></li>
              
            
          </ul>
        </div>
      
      

      
      
        <div class="small-12 column">
          <h3>Online Seminars</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../online-seminars/ux-big-data/index.php">UX for Big Data</a></li>
              
            
              
                <li><a href="../../online-seminars/how-change-your-corporate-culture/index.php">How to Change Your Corporate Culture to be User First</a></li>
              
            
          </ul>
        </div>
      
      
    </div>

    <div class="hide-for-small-down row related-content">
      <div class="medium-6 columns">
        
        
          <div>
            <h3>Articles</h3>
            <ul class="no-bullet">
              
                
                <li><a href="../game-user-research/index.php">Games User Research: What’s Different?</a></li>
                
              
                
                <li><a href="../link-list-color-on-intranets/index.php">Link List Color on Intranets</a></li>
                
              
                
                <li><a href="../usability-guidelines-change/index.php">Change vs. Stability in Web Usability Guidelines</a></li>
                
              
                
                <li><a href="../durability-of-usability-guidelines/index.php">Durability of Usability Guidelines</a></li>
                
              
                
                <li><a href="../the-need-for-web-design-standards/index.php">The Need for Web Design Standards</a></li>
                
              
            </ul>
          </div>
        
        

        
          
          <div>
            <h3>Research Reports</h3>
            <ul class="no-bullet">
              
                
                  <li><a href="../../reports/usability-return-on-investment-roi/index.php">Return on Investment (ROI) for Usability</a></li>
                
              
                
                  <li><a href="../../reports/best-applications-2/index.php">Application Design Showcase: 2012</a></li>
                
              
                
                  <li><a href="../../reports/agile-development-user-experience/index.php"> Effective Agile UX Product Development</a></li>
                
              
                
                  <li><a href="../../reports/user-experience-careers/index.php">User Experience Careers</a></li>
                
              
                
                  <li><a href="../../reports/enterprise-mobile-showcase/index.php">Mobile Intranets and Enterprise Apps</a></li>
                
              
            </ul>
          </div>
          
        

      </div>
      <div class="medium-6 columns">
        
          
          <div>
            <h3>UX Conference Training Courses</h3>
             <ul class="no-bullet">
    
        <li><a href="../../courses/facilitating-ux-workshops/index.php">Facilitating UX Workshops</a></li>
    
        <li><a href="../../courses/ux-deliverables/index.php">UX Deliverables</a></li>
    
        <li><a href="../../courses/hci/index.php">User Interface Principles Every Designer Must Know</a></li>
    
        <li><a href="../../courses/lean-ux-and-agile/index.php">Lean UX and Agile</a></li>
    
        <li><a href="../../courses/stakeholder-ux-approval/index.php">Engaging Stakeholders to Build Buy-In</a></li>
    
</ul>
          </div>
          
        
        
          
          <div>
            <h3>Online Seminars</h3>
            <ul class="no-bullet">
            
                <li><a href="../../online-seminars/ux-big-data/index.php">UX for Big Data</a></li>
            
                <li><a href="../../online-seminars/how-change-your-corporate-culture/index.php">How to Change Your Corporate Culture to be User First</a></li>
            
            </ul>
          </div>
          
        
      </div>
    </div>
  </footer>
</article>

        </div>

      

    
  </div>
  <div class="row">
    
      <div class="column small-12 show-for-small-down l-navrail announcements">
        
          
        
      </div>
    
    
  </div>

    </div>
    




<footer id="page-footer">
    <div class="row hide-for-print">
        <div class="column small-12 medium-6">
            <section class="footer-popular">
                
                <h1>Popular Topics</h1>
                <div style="float: left;  width: 15em; display: block;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/applications/index.php">Application Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/e-commerce/index.php">E-Commerce Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/eyetracking/index.php">Eyetracking </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/information-architecture/index.php">Information Architecture </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/intranets/index.php">Intranet Design </a></li>
	<li><a href="../../topic/interaction-design/index.php">Interaction Design </a></li>
</ul>
</div>

<div style="float: left;  width: 15em; display: block; list-style: none;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/mobile-and-tablet-design/index.php">Mobile and Tablet Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/credibility-persuasion/index.php">Persuasive Design</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/research-methods/index.php">Research Methods</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/user-testing/index.php">User Testing </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/web-usability/index.php">Web Usability </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/writing-web/index.php">Web Writing &amp; Content Strategy </a></li>
</ul>
</div>

<p>&nbsp;</p>

                
            </section>
        </div>

        <div class="column small-12 medium-6">
            <div class="row">
                <div class="column small-6">
                    <section class="footer-about">
                        <h1>Follow Us</h1>
                        <ul>
	<li><a href="../subscribe/index.php">Subscribe to our weekly UX newsletter</a>,&nbsp;Jakob Nielsen&#39;s Alertbox</li>
	<li>Visit Don Norman&#39;s website, <a href="http://www.jnd.org/"> www.jnd.org </a></li>
	<li>Visit Bruce Tognazzini&#39;s website, <a href="http://www.asktog.com/"> www.asktog.com </a></li>
	<li style="padding-top: 6px;"><a class="twitter-follow-button" data-show-count="false" data-size="large" href="https://twitter.com/nngroup"><img alt="Follow @nngroup on Twitter" height="20" src="https://media.nngroup.com/media/editor/2016/12/30/twitter-follow-button.png" style="width: 117px; height: 20px;" width="117" /></a></li>
	<li style="padding-top: 6px;"><img alt="" height="14" src="https://media.nngroup.com/media/editor/2016/07/26/feed-icon-14x14.png" style="vertical-align:text-top;width:14px;margin-right:.25em;" width="14" /><a href="../../feed/rss/index.php">Subscribe to our RSS feed</a></li>
</ul>

                    </section>
                </div>

                <div class="column small-6">
                    <section class="footer-about right-column">
                        <h1>About
</h1>
                        <ul class="no-bullet">
	<li><a href="../../about/contact/index.php">Contact information</a></li>
	<li><a href="../../about/index.php">About Us</a></li>
	<li><a href="../../about/why-nng/index.php">Why NN/g? </a></li>
	<li><a href="../../return-policy/index.php">Return Policy</a></li>
</ul>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="copyright column small-12">
            <p><a href="../../copyright-and-reprint-info/index.php">Copyright</a> &copy; 1998-2017 Nielsen Norman Group, All Rights Reserved.</p>
        </div>
    </div>
</footer>


    


        <script src="https://media.nngroup.com/static/vendor/fastclick/js/fastclick.min.js"></script>
        <script src="https://media.nngroup.com/static/vendor/foundation/js/foundation.min.js"></script>
        <script src="http://media.nngroup.com/static/js/app.js"></script>
        <script src="http://media.nngroup.com/static/js/scroll-to-fixed.js"></script><script>var externalLinks = document.querySelectorAll('a[href^="http"]');for (var i = externalLinks.length-1; i >= 0; i--) {externalLinks[i].addEventListener("click", function(event) { alert("External links are disabled for this test."); event.preventDefault(); }, false);}var mailLinks = document.querySelectorAll('a[href^="mailto"]');for (var i = mailLinks.length-1; i >= 0; i--) {mailLinks[i].addEventListener("click", function(event) { alert("Email links are disabled for this test."); event.preventDefault(); }, false);}var externalForms = document.querySelectorAll('form');for (var i = externalForms.length-1; i >= 0; i--) {externalForms[i].addEventListener("submit", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false); externalForms[i].addEventListener("click", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false);}</script>

        <script>
            
            $(function() {
                $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
                $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
            });
        </script>

        
<script src="https://media.nngroup.com/static/js/articles.js"></script>


        
        <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1072699960;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072699960/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
    </body>

<!-- Mirrored from www.nngroup.com/articles/assessing-usability-user-interface-standard/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:14:20 GMT -->
</html>
