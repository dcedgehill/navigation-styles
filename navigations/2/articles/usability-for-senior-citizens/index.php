<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->





    
<!-- Mirrored from www.nngroup.com/articles/usability-for-senior-citizens/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:12:31 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1026.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"beacon":"bam.nr-data.net","queueTime":5,"applicationTime":1944,"agent":"","licenseKey":"1a30ef9931","errorBeacon":"bam.nr-data.net","applicationID":"2048992","transactionName":"NFxSMRBUX0EDUkIPDA0WdhAMVkVbDV8ZBxEXUFMJB0YfRAtUQRVZIktEDAFZVHYHRVcPD01eVRE="}</script>
        <title>Usability for Senior Citizens</title><meta property="og:title" content="Usability for Senior Citizens" />
  
        
        <meta name="description" content="Users aged 65 and older are 43% slower at using websites than users aged 21–55. This is an improvement over previous studies, but designs must change to better accommodate aging users.">
        <meta property="og:description" content="Users aged 65 and older are 43% slower at using websites than users aged 21–55. This is an improvement over previous studies, but designs must change to better accommodate aging users." />
        
  
        
	
        
        <meta name="keywords" content="senior citizens, seniors, usability guidelines for designing for old users, elderly users, old users, old people, aging society, human aging process, age differences, error messages, forms, links, link colors, navigation, memory, eyesight, default font size, changing font sizes, readability, conceptual models, special interest sites, hobbies, hobby website, social networking, health information, medical sites, tabs, vision, dexterity, memory
">
        

        

        <meta name="robots" content="noarchive">
        <meta name="viewport" content="width=device-width">

        <link rel="search" type="application/opensearchdescription+xml" title="NN/g Search" href="../../opensearch.xml" />
        <link rel="shortcut icon" href="http://media.nngroup.com/static/img/favicon.ico" />

        <link rel="stylesheet" href="https://media.nngroup.com/static/vendor/font-awesome/css/font-awesome.min.css">

        
        <link rel="stylesheet" href="https://media.nngroup.com/static/css/7232a960f1df.css" type="text/css" />
        <link rel="stylesheet" href="http://fast.fonts.com/t/1.css?apiType=css&amp;projectid=c5846260-9def-46ec-9407-0f1349bd2d64" type="text/css">

        <script src="https://media.nngroup.com/static/vendor/modernizr/js/modernizr.min.js"></script>

        <script type="text/javascript" src="https://media.nngroup.com/static/js/3283be5def74.js"></script>
        

        

        
        
        <!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=229034,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('http://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->


<!-- Google Analytics Tracking Code for http://nngroup.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23132681-1', 'nngroup.com');
  ga('require', 'linkid', 'https://www.nngroup.com/articles/usability-for-senior-citizens/linkid.js');
  ga('require', 'displayfeatures');

// Optimizely Universal Analytics Integration
 window.optimizely = window.optimizely || [];
 window.optimizely.push("activateUniversalAnalytics");

  ga('send', 'pageview');

</script>
<script>
/**
* Function to track a click on an outbound link in Google Analytics.
*/
function trackOutboundLink(label, url) {
    if(window.ga && ga.loaded) {
        ga('send', 'event', 'outbound', 'click', label, {'hitCallback': function(){document.location = url;}
        });
    } else {
        document.location = url;
    }
}
/**
* Function to track a click on a link in the article announcement area.
*/
$('#articleAnnouncement a').on('click', function(){
  ga('send', 'event', 'link', 'click', 'articleAnnouncement');
});
</script>

<script type="text/javascript">
 setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2421.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



<!-- Hotjar Tracking Code for http://nngroup.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:121605,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

        <script src="https://media.nngroup.com/static/js/analytics/track_outbound_link.js"></script>
        
        
        
    </head>
    <body class="location-articles articles-detail">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
    <header id="page-header">
        <div id="cart" class="shopping-cart mobile-cart show-for-small-down"></div>
        <div class="row">
            <div id="masthead" class="masthead small-12 medium-7 column">

                <!-- The button to toggles menu on small screen -->

                <h1><a href="../../index.php"><img src="http://media.nngroup.com/static/img/logo-wordmark.png" width="74" height="30"> Nielsen <span class="norman">Norman</span> Group</a></h1>
                <p class="tagline show-for-medium-up">Evidence-Based User Experience Research, Training, and Consulting</p>

                <!-- The button to toggles form search on small screen -->


                <div style="clear: right;width:  100%;height: 1px;"></div><?php define('ROOT_PATH',$_SERVER["DOCUMENT_ROOT"] . '/apps/navigation_styles/navigations/2'); ?><?php include(ROOT_PATH)."/navigation.php"; ?>

                <!-- Drop down menu on small screen -->
                <div id="hideNav" class="show-for-small-down">
                  
                  <ul class="menu-content">
                    <li><a href="../../index.php">Home</a></li>
                    <li class="has-submenu">
                      <a>Training</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../training/index.php">Overview</a></li>
                        
<li id="nav-ux-conference"><a href="../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../online-seminars/index.php"><span>Online Seminars</span></a></li>

                      </ul>
                    </li>
                    <li class="has-submenu">
                      <a>Consulting</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
  <li><a href="../../consulting/index.php">Overview</a></li>
  
  <li><a href="../../consulting/benchmarking/index.php">Benchmarking</a></li>
  
  <li><a href="../../consulting/expert-design-review/index.php">Expert Design Review</a></li>
  
  <li><a href="../../consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
  
  <li><a href="../../consulting/usability-testing/index.php">Usability Testing </a></li>
  
  <li><a href="../../consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
  
  <li><a href="../../in-house-training/index.php">In-House Training</a></li>
</ul>

                    </li>

                    <li><a href="../../reports/index.php">Reports</a></li>
                    <li><a href="../index.php">Articles</a></li>
                    <li class="has-submenu"><a>About NN/G</a><i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../about/index.php">Overview</a></li>
                        <li><a href="../../people/index.php">People</a></li>
                        <li><a href="../../about/why-nng/index.php">Why NN/g?</a></li>
                        <li><a href="../../about/contact/index.php">Contact</a></li>
                        <li><a href="../../news/index.php">News</a></li>
                        <li><a href="../../about/history/index.php">History</a></li>
                        <li><a href="../../books/index.php">Books</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                </div>

                <hr class="header-divider show-for-small-only"/>


            </div>
       </div>

        <div id="hideNav">            <nav class="primary-nav small-12 column">
                <ul>
                    <li><a class="navitem" id="nav-home" href="../../index.php">Home</a></li>
                    <li><a class="navitem" id="nav-training" href="../../training/index.php">Training</a></li>
                    <li><a class="navitem" id="nav-consulting" href="../../consulting/index.php">Consulting</a></li>
                    <li><a class="navitem" id="nav-reports" href="../../reports/index.php">Reports</a></li>
                    <li><a class="navitem" id="nav-articles" href="../index.php">Articles</a></li>
                    <li><a class="navitem" id="nav-about" href="../../about/index.php">About NN/G</a></li>
                </ul>
            </nav>
            <nav class="secondary-nav small-12 medium-12 large-12 columns">
                
                
            </nav>
        </div>
    </header>
    <div id="main" class="l-content" onclick="closeNav(event)"><span id="preventClose" style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776; Menu</span>
    
  

  

  <div class="row">
    
      
      <section class="small-12 medium-3 columns collapsable-content l-navrail articles-left-column">
        <h1 class="show-for-small-down collapse-link mobile-browse-header">Browse by Topic and Author</h1>
          <div class="row collapsable">

            
            <div class="small-6 medium-12 columns">
              <section id="article-popular-topics" style="display: none;">
                
                <h1>Topics</h1>
                <ul id="article-popular-topics">
                    
                    <li>
                        
                        <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/intranets/all/index.php">Intranets</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/user-testing/all/index.php">User Testing</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                        
                    </li>
                    
                    <li><a href="#" id="article-see-all-topics"><strong>See all topics&hellip;</strong></a></li>
                </ul>
                
              </section>
              <section id="article-all-topics" style="display: none;">
                
                <h1>
                  <span class="show-for-medium-up">All Article Topics</span>
                  <span class="show-for-small-down">All Topics</span>
                  (<a href="#" id="article-hide-all-topics">hide</a>)
                </h1>
                <ul>
                  
                  <li>
                    
                    <a href="../../topic/accessibility/all/index.php">Accessibility</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/agile/all/index.php">Agile</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/analytics-and-metrics/all/index.php">Analytics &amp; Metrics</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/applications/all/index.php">Application Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/b2b-websites/all/index.php">B2B Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/behavior-patterns/all/index.php">Behavior Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/branding/all/index.php">Branding</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/content-strategy/all/index.php">Content Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/corporate-websites/all/index.php">Corporate Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/design-patterns/all/index.php">Design Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-design-process/all/index.php">Design Process</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/email/all/index.php">Email</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/eyetracking/all/index.php">Eyetracking</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/heuristic-evaluation/all/index.php">Heuristic Evaluation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/human-computer-interaction/all/index.php">Human Computer Interaction</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-ideation/all/index.php">Ideation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/information-architecture/all/index.php">Information Architecture</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/interaction-design/all/index.php">Interaction Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/international-users/all/index.php">International Users</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/intranets/all/index.php">Intranets</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-management/all/index.php">Management</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/navigation/all/index.php">Navigation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/non-profit-websites/all/index.php">Non-Profit Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/personas/all/index.php">Personas</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/persuasive-design/all/index.php">Persuasive Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/prototyping/all/index.php">Prototyping</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/psychology-and-ux/all/index.php">Psychology and UX</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/research-methods/all/index.php">Research Methods</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/search/all/index.php">Search</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/social-media/all/index.php">Social Media</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/strategy/all/index.php">Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/user-testing/all/index.php">User Testing</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-teams/all/index.php">UX Teams</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/visual-design/all/index.php">Visual Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/writing-web/all/index.php">Writing for the Web</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/young-users/all/index.php">Young Users</a>
                    
                  </li>
                  
                </ul>
                
              </section>
            </div>
            <script>
              /* Determine which topics list will be displayed: the full or the
               * partial.  We are inlining this close to the list itself to
               * prevent rendering delays on the client
               */
              var topic_list = $.cookie("article-topics");

              var popular_topics = $('#article-popular-topics');
              var all_topics = $('#article-all-topics');

              if(topic_list == 1) {
                  popular_topics.hide();
                  all_topics.show();
              } else {
                  popular_topics.show();
                  all_topics.hide();
              }
            </script>
            

            
            <div class="small-6 medium-12 columns">
              
              <section id="article-initial-authors">
                  <h1>Author</h1>
                  <ul>
                    
                    <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                    
                    <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                    
                    <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                    
                    <li><a href="#" onClick="$('#article-initial-authors').hide(); $('#article-all-authors').show();"><strong>See all authors&hellip;</strong></a></li>
                  </ul>
              </section>
              <section id="article-all-authors" style="display: none;">
                <h1>
                  All Authors
                  (<a href="#" id="article-hide-all-authors">hide</a>)
                </h1>
                <ul>
                  
                  <li><a href="../author/raluca-budiu/index.php">Raluca Budiu</a></li>
                  
                  <li><a href="../author/patty-caya/index.php">Patty Caya</a></li>
                  
                  <li><a href="../author/susan-farrell/index.php">Susan Farrell</a></li>
                  
                  <li><a href="../author/therese-fessenden/index.php">Therese Fessenden</a></li>
                  
                  <li><a href="../author/kim-flaherty/index.php">Kim Flaherty</a></li>
                  
                  <li><a href="../author/sarah-gibbons/index.php">Sarah Gibbons</a></li>
                  
                  <li><a href="../author/aurora-harley/index.php">Aurora Harley</a></li>
                  
                  <li><a href="../author/kate-kaplan/index.php">Kate Kaplan</a></li>
                  
                  <li><a href="../author/page-laubheimer/index.php">Page Laubheimer</a></li>
                  
                  <li><a href="../author/angela-li/index.php">Angie Li</a></li>
                  
                  <li><a href="../author/hoa-loranger/index.php">Hoa Loranger</a></li>
                  
                  <li><a href="../author/kate-meyer/index.php">Kate Meyer</a></li>
                  
                  <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                  
                  <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                  
                  <li><a href="../author/kara-pernice/index.php">Kara Pernice</a></li>
                  
                  <li><a href="../author/christian-rohrer/index.php">Christian Rohrer</a></li>
                  
                  <li><a href="../author/amy-schade/index.php">Amy Schade</a></li>
                  
                  <li><a href="../author/katie-sherwin/index.php">Katie Sherwin</a></li>
                  
                  <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                  
                  <li><a href="../author/kathryn-whitenton/index.php">Kathryn Whitenton</a></li>
                  
                </ul>
              </section>
              
            </div>
            
          </div>

          
          <section class="hide-for-small-down">
            
              
              <h1>Recent Articles</h1>
              
              <ul>
                  
                  <li><a href="../seamless-cross-channel/index.php">Seamlessness in the Omnichannel User Experience</a></li>
                  
                  <li><a href="../brand-intention-interpretation/index.php">Experience Design: Bridging Brand Intention and Brand Interpretation</a></li>
                  
                  <li><a href="../eyetracking-tasks-efficient-scanning/index.php">Scanning Patterns on the Web Are Optimized for the Current Task</a></li>
                  
                  <li><a href="../flat-design-best-practices/index.php">Flat-Design Best Practices</a></li>
                  
                  <li><a href="../ux-user-stories/index.php">Accounting for UX Work with User Stories in Agile Projects</a></li>
                  
                  <li><a href="../index.php"><strong>See all articles&hellip;</strong></a></li>
              </ul>
              
            
          </section>
          

          
          
          <section class="hide-for-small-down">
            <h1>Popular Articles</h1>
            
            <ul>
                
                <li><a href="../usability-101-introduction-to-usability/index.php">Usability 101: Introduction to Usability</a></li>
                
                <li><a href="../top-10-mistakes-web-design/index.php">Top 10 Mistakes in Web Design</a></li>
                
                <li><a href="../how-users-read-on-the-web/index.php">How Users Read on the Web</a></li>
                
                <li><a href="../f-shaped-pattern-reading-web-content/index.php">F-Shaped Pattern For Reading Web Content</a></li>
                
                <li><a href="../ten-usability-heuristics/index.php">10 Usability Heuristics for User Interface Design</a></li>
                
                <li><a href="../intranet-design-2016/index.php">10 Best Intranets of 2016</a></li>
                
                <li><a href="../which-ux-research-methods/index.php">When to Use Which User-Experience Research Methods</a></li>
                
                <li><a href="../response-times-3-important-limits/index.php">Response Times: The 3 Important Limits</a></li>
                
                <li><a href="../why-you-only-need-to-test-with-5-users/index.php">Why You Only Need to Test with 5 Users</a></li>
                
                <li><a href="../page-fold-manifesto/index.php">The Fold Manifesto: Why the Page Fold Still Matters</a></li>
                
            </ul>
          </section>
          
          

          
            <section class="hide-for-small-down">
              
            </section>
          

      </section>
      

      
        <div class="small-12 medium-9 columns content-body">
          




<article class="l-subsection alertbox-article-content article-detail-bl">
  
  <header>
    <div class="l-subsection">
      <h1>Seniors as Web Users</h1>
      <div class="author-meta">by
        
          
            
            <a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a>
            
          
        on  May 28, 2013
        
      </div>

      

      
      <div class="topic-links">
        Topics:
        
          <ul>

  <li><a href="../../topic/accessibility/index.php">Accessibility</a></li>

  <li><a href="../../topic/web-usability/index.php">Web Usability</a></li>

</ul>
        
      </div>
      
    </div>
  </header>
  

  
  
  <section>
    <p class="lede"><strong>Summary:</strong> Users aged 65 and older are 43% slower at using websites than users aged 21–55. This is an improvement over previous studies, but designs must change to better accommodate aging users.</p>
    <br />
  </section>
  
  

  
  <section id="article-body"><p>
	In rich countries, <strong>older users are the last Internet frontier </strong>as every other age group is already online in vast numbers.</p>
<p>
	In 2002, the United States had an estimated 4.2 million Internet users over the age of 65. As of 2012, this number had grown to <strong>19 million American seniors on the Internet</strong>,  for a <strong>growth rate of 16% per year</strong> throughout this decade.</p>
<p>
	In contrast, from 2004 to 2012, the number of U.S. Internet users aged 30–49 increased only 3% per year, from 58 million to 73 million.</p>
<p>
	The situation is similar in the U.K., where the Office for National Statistics  estimated the following change in Internet use during a one-year period:</p>
<table align="center" style="border-style: solid; border-color: #cccccc; border-width: 1px; border-collapse: collapse; ">
	<tbody>
		<tr>
			<th style=" border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left; background-color:  #003366; color: #ffffff; width: 25%;">
				 </th>
			<th style=" border-style: solid; border-width: 1px; padding: 0.5ex; text-align: center; background-color:  #003366; color: #ffffff; width: 25%;">
				End of 2011</th>
			<th style=" border-style: solid; border-width: 1px; padding: 0.5ex; text-align: center; background-color:  #003366; color: #ffffff; width: 25%;">
				End of 2012</th>
			<th style=" border-style: solid; border-width: 1px; padding: 0.5ex; text-align: center; background-color:  #003366; color: #ffffff; width: 25%;">
				Annualized<br/>
				Growth Rate</th>
		</tr>
		<tr>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left; vertical-align: top;">
				Aged 35–44</td>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: center; vertical-align: middle;">
				95.9%</td>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: center; vertical-align: middle;">
				96.9%</td>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: center; vertical-align: middle;">
				1%</td>
		</tr>
		<tr>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left; vertical-align: top;">
				Aged 65–74</td>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: center; vertical-align: middle;">
				59.8%</td>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: center; vertical-align: middle;">
				65.4%</td>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: center; vertical-align: middle;">
				9%</td>
		</tr>
	</tbody>
</table>
<p>
	Yes, there are more young users, but the number of older users is growing much more rapidly, driven by two factors:</p>
<ul>
	<li>
		an aging society and</li>
	<li>
		an ever-larger percentage of old people who go online.</li>
</ul>
<p>
	The <strong>business opportunities </strong>offered by elderly users is growing even faster than the demographics indicate for two reasons:</p>
<ul>
	<li>
		E-commerce purchases, online banking and brokerage, and the many other ways of extracting business value from users are all lagging indicators relative to users’ initial uptake of free services. Thus, many of the seniors who’ve gone online in recent years have yet to start spending money, but they’ll do so shortly.</li>
	<li>
		Our new user research shows that current websites still discriminate against seniors. By embracing web design that’s more suited for older users, sites can vastly expand the amount of business they generate from this population..</li>
</ul>
<h2>
	Research with Seniors Using Websites</h2>
<p>
	We’ve conducted two rounds of usability studies with a total of <strong>75 seniors</strong>:</p>
<ul>
	<li>
		Round 1: 11 years ago, we tested 17 websites with 44 seniors.</li>
	<li>
		Round 2: This year, we tested 29 sites with 31 seniors.</li>
</ul>
<p>
	To compare how websites treat seniors and mainstream users, each of the two research rounds also included a control group of 20 users aged 21–55.</p>
<p>
	Most user sessions were in the U.S., but we also tested seniors in Australia, Germany, Japan, and the U.K. to ensure the international applicability of our findings.</p>
<h2>
	Defining “Senior Citizen”</h2>
<p>
	We use a simple definition: “seniors” are users aged <strong>65 years</strong> or older. We had no upper end, though the oldest participant in our research was 89 years old.</p>
<p>
	Of course, this is a simplification. It’s not as if people change all their behaviors on their 65th birthday. The human aging process starts when you turn 20; people in their 40s already have sufficiently reduced eyesight to require somewhat <a href="../let-users-control-font-size/index.php" title="Let Users Control Font Size, article">larger font sizes</a> than eagle-eyed designers in their 20s.</p>
<p>
	In testing middle-aged users, we’ve found that <a href="../middle-aged-web-users/index.php" title="Middle-Aged Users' Declining Web Performance, article">between the ages of 25 and 60</a>, people's ability to use websites <strong>declines by 0.8% per year</strong>.</p>
<p>
	Thus, in one sense, we need to start thinking about older users’ problems long before they turn 65.</p>
<p>
	On the other hand, <strong>65 is too young</strong> to be truly considered “senior” in other contexts. For example, in Denmark, the retirement age is increasing to 69 years to avoid bankrupting the government-run pension system, which used to start paying out when people reached 65. Most rich countries have similar funding problems for their retirement schemes. Because life expectancies (happily) keep rising, it will become increasingly common for people to work until they’re at least 70.</p>
<p>
	Even in countries that retain a traditional retirement age of 65, many people don’t want to retire this soon. Older people are healthier than they used to be, and many like to stay active in the workplace. Given this, usability guidelines for “senior” users should be considered when designing <strong>intranets and enterprise application</strong>s, as many companies will have growing numbers of employees aged 65 and older.</p>
<h2>
	Clear Gains—and Room to Improve</h2>
<p>
	Both research rounds included <a href="../quantitative-studies-how-many-users/index.php" title="Quantitative Studies: How Many Users to Test?, article">quantitative testing</a>, where we collected four key <a href="../usability-metrics/index.php" title="Usability Metrics, article">usability metrics</a> for a set of tasks:</p>
<ul>
	<li>
		<a href="../success-rate-the-simplest-usability-metric/index.php" title="Success Rate: The Simplest Usability Metric, article"><strong>Success rate</strong></a>: Can people even complete the task?</li>
	<li>
		<strong>Time on task</strong>: How fast can people do the task? This measures efficiency.</li>
	<li>
		<strong>Error rate</strong>: How often do people do something wrong?</li>
	<li>
		<a href="../satisfaction-vs-performance-metrics/index.php" title="User Satisfaction vs. Performance Metrics, article"><strong>Subjective satisfaction</strong></a>: On a 1–7 scale, how much do people like the website?</li>
</ul>
<p>
	Here are the results, averaged across the test tasks:</p>
<table align="center" style="border-style: solid; border-color: #cccccc; border-width: 1px; border-collapse: collapse; ">
	<tbody>
		<tr>
			<th style=" border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left; background-color:  #003366; color: #ffffff; width: 25%; ">
				 </th>
			<th style=" border-style: solid; border-width: 1px; padding: 0.5ex; text-align: center; background-color:  #003366; color: #ffffff; width: 25%; ">
				Seniors<br/>
				(11 years ago)</th>
			<th style=" border-style: solid; border-width: 1px; padding: 0.5ex; text-align: center; background-color:  #003366; color: #ffffff; width: 25%; ">
				Seniors<br/>
				(now)</th>
			<th style=" border-style: solid; border-width: 1px; padding: 0.5ex; text-align: center; background-color:  #003366; color: #ffffff; width: 25%; ">
				Users Aged 21–55<br/>
				(now)</th>
		</tr>
		<tr>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left; vertical-align: top;">
				Success rate</td>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: center; vertical-align: middle;">
				52.5%</td>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: center; vertical-align: middle;">
				55.3%</td>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: center; vertical-align: middle;">
				74.5%</td>
		</tr>
		<tr>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left; vertical-align: top;">
				Time on task (min:sec)</td>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: center; vertical-align: middle;">
				9:58</td>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: center; vertical-align: middle;">
				7:49</td>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: center; vertical-align: middle;">
				5:28</td>
		</tr>
		<tr>
		</tr>
		<tr>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left; vertical-align: top;">
				Errors</td>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: center; vertical-align: middle;">
				4.6</td>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: center; vertical-align: middle;">
				2.4</td>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: center; vertical-align: middle;">
				1.1</td>
		</tr>
		<tr>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left; vertical-align: top;">
				Subjective rating<br/>
				(1–7, 7 best)</td>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: center; vertical-align: middle;">
				3.7</td>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: center; vertical-align: middle;">
				4.1</td>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: center; vertical-align: middle;">
				4.6</td>
		</tr>
	</tbody>
</table>
<p>
	On the first three metrics, the difference between seniors and the control group are significant at the <em>p</em>&lt;.002 level. The satisfaction ratings are different at the <em>p</em>&lt;.05 level.</p>
<p>
	Because faster times are better, all four usability metrics lead to the same conclusions:</p>
<ul>
	<li>
		Websites have become <strong>somewhat better for seniors </strong>over the past 11 years.</li>
	<li>
		Websites are still <strong>substantially harder to use for seniors </strong>than for younger users.</li>
</ul>
<p>
	Let’s translate these usability metrics into business terms: If you redesigned your website to give seniors the same user experience quality as younger users, you could expect to get <strong>35% more business</strong> from them, based purely on the higher success rate. (Most likely, usage would increase even more as tasks became faster, less error-prone, and more pleasant to perform.)</p>
<p>
	Why are seniors better at using websites now? There are two reasons; unfortunately, the data doesn’t allow us to estimate the relative contribution of each.</p>
<ul>
	<li>
		Websites are <strong>better designed </strong>than they used to be. Even though there’s still gross negligence of seniors’ needs, sites are now at least somewhat more accommodating.</li>
	<li>
		Seniors have gotten <strong>more skilled </strong>at using the web. Although we <a href="../user-skills-improving-only-slightly/index.php" title="User Skills Improving, But Only Slightly, article">don’t see much progress over time in the mainstream audience’s web skills</a>, seniors are a different matter. Unlike seniors a decade ago, today’s seniors are more likely to have learned how to use computers while they were still in the workplace. Seniors who learn from corporate training courses and colleagues are more likely to create solid <a href="../mental-models/index.php" title="Mental Models, article">mental models</a> than seniors who picked up their computer skills after retirement.</li>
</ul>
<p>
	Furthermore, Internet connections have gotten faster: In our first research study, 75% of seniors were still on dial-up connections, whereas all the seniors in the second study had some form of broadband connection. Although <a href="../website-response-times/index.php" title="Website Response Times, article">fast response times are important</a> for all users, they’re particularly crucial for seniors, who are more likely to forget things if tasks take too long.</p>
<h2>
	Human Aging and Web Use</h2>
<p>
	Time takes its toll as we get older. <em>It will happen to you</em>, too, which is one reason—besides business gains—that everybody should care about designing for seniors. In our second study, we collected simple measures of our two user groups on a few criteria:</p>
<table align="center" style="border-style: solid; border-color: #cccccc; border-width: 1px; border-collapse: collapse; ">
	<tbody>
		<tr>
			<th style=" border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left; background-color:  #003366; color: #ffffff; width: 33%; ">
				 </th>
			<th style=" border-style: solid; border-width: 1px; padding: 0.5ex; text-align: center; background-color:  #003366; color: #ffffff; width: 33%; ">
				Seniors (65 and Older)</th>
			<th style=" border-style: solid; border-width: 1px; padding: 0.5ex; text-align: center; background-color:  #003366; color: #ffffff; width: 34%; ">
				Users Aged 21–55</th>
		</tr>
		<tr>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left; vertical-align: top;">
				<strong>Vision</strong></td>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: center; vertical-align: middle;">
				82%</td>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: center; vertical-align: middle;">
				95%</td>
		</tr>
		<tr>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left; vertical-align: top;">
				<strong>Dexterity</strong></td>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: center; vertical-align: middle;">
				73%</td>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: center; vertical-align: middle;">
				95%</td>
		</tr>
		<tr>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: left; vertical-align: top;">
				<strong>Memory</strong></td>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: center; vertical-align: middle;">
				49%</td>
			<td style="border-style: solid; border-width: 1px; padding: 0.5ex; text-align: center; vertical-align: middle;">
				63%</td>
		</tr>
	</tbody>
</table>
<p>
	For vision and dexterity, 100% would indicate no problems using websites. For memory, the scores indicate the percentage of items introduced early in the test session that users could recall correctly at the session’s end.</p>
<p>
	It’s obviously important to recognize that even young people have physical and cognitive limitations. But, as the table clearly shows, these issues are much more severe for older users.</p>
<p>
	<strong>Hearing</strong> also declines with age, but we didn’t measure this because most mainstream websites can be used just fine without sound effects. We did observe cases of older users choosing to watch videos at extremely high audio volumes or even deciding to read information instead of watching a video.</p>
<h2>
	Design Issues: Readability, Clickability</h2>
<p>
	Reduced visual acuity is probably the best-known aging problem, and yet websites with <strong>tiny type</strong> are legion. Sites that target seniors should use at least 12-point fonts as the default. And all sites, whether or not they specifically target seniors, should let users <a href="../let-users-control-font-size/index.php" title="Let Users Control Font Size, article">increase text size as desired</a>—especially if the site’s default is a small font size.</p>
<p>
	<strong>Hypertext links</strong> are essential design components; using large text for them is especially important for two reasons: 1) to ensure readability, and 2) to make them more prominent targets for clicking. Also, you should avoid tightly clustered links; using white space to separate links decreases erroneous clicks and increases the speed at which users hit the correct link. This rule also applies to command buttons and other interaction objects, all of which should be reasonably large to facilitate easy clicking.</p>
<p>
	Pull-down menus, hierarchically walking menus, and other moving interface elements are problematic for seniors who are not always steady with the mouse. Better to use static user interface widgets and designs that don’t require pixel-perfect pointing.</p>
<h2>
	Behavioral Issues: Hesitation, Discouragement</h2>
<p>
	In our studies, 45% of seniors showed behaviors that indicated they were <strong>uncomfortable trying new things</strong> or hesitant to explore. For example, when they failed in their first attempt at a task, some seniors were hesitant to try alternate paths. One senior, while looking for the average temperature for Dallas, Texas in January, went to his favorite weather website. When he couldn’t find the information, he didn’t want to go to a different website. Instead, he gave up.</p>
<p>
	The younger users in our control group were twice as likely as older users to try more and different methods—such as site search, contextual help, or online chat—to find the answers to their questions or to complete tasks.</p>
<p>
	Conversely, seniors were almost <strong>twice as likely to give up</strong> on a task. Among all users who quit a task without completing it, seniors gave up 30 seconds before the younger users did.</p>
<p>
	When users had problems, <strong>seniors blamed themselves 90% of the time</strong>, compared to 58% of younger users. As I see it, almost 100% of the blame should fall on the websites and their designers, because most problems could have been avoided if they’d paid better attention to the <a href="../../reports/senior-citizens-on-the-web/index.php">usability guidelines for designing for seniors</a>.</p>
<p>
	Perhaps because of their reduced confidence, seniors were much more likely to turn to web-wide <strong>search engines</strong> like Google or Bing. These sites are familiar and welcoming ground, and seniors used search engines <strong>51% more</strong> than the younger users to complete tasks.</p>
<p>
	The other side of the hesitation coin is that seniors are slower and more methodical in performing tasks. In our studies, 95% of seniors were rated as methodical in their behaviors: for example, they were likely to think through each step or click and assess an entire page before moving forward. Only 35% of younger users exhibited such methodical behaviors. Sadly, the slower and more measured approach to computers didn’t gain seniors better results, as demonstrated by the study’s success scores.</p>
<h2>
	Offer Supportive (and Forgiving) Design</h2>
<p>
	When websites violate the guideline to use <a href="../change-the-color-of-visited-links/index.php" title="Change the Color of Visited Links, article">different colors to clearly distinguish between visited and unvisited links</a>, seniors easily lose track of where they’ve been. The same is true for all age groups: It's confusing when websites change the standard link colors, and it's particularly confusing when the same color is used for all links, regardless of whether you’ve visited the destination page. Seniors, however, have a harder time remembering which parts of a website they’ve visited and are thus more likely to waste time repeatedly returning to the same place.</p>
<p>
	Seniors also have a harder time using unforgiving search engines and forms. We saw them thwarted due to simple query typos, and punished for using hyphens or parentheses in telephone or credit card numbers.</p>
<p>
	Further, seniors often had problems reading error messages, either because the wording was obscure or imprecise, or the message’s placement on the page was easily overlooked among a profusion of other design elements. When seniors encounter <a href="../error-message-guidelines/index.php" title="Error Message Guidelines, article">error handling</a>, simplicity is even more important than usual: focus on the error, explain it clearly, and make it as easy as possible to fix. Website tasks should adapt to seniors and how they prefer to do things as much as possible. After decades of writing telephone numbers in a certain way, it’s not a very nice experience to come across a form that insists on a different format.</p>
<p>
	Seniors sometimes have trouble with the basic mechanics of using computers and the web. For example, 45% of seniors had <strong>trouble managing browser tabs</strong> and multiple windows. Other seniors, however, were web virtuosos and confidently used advanced features like <em>ctrl-F</em> to jump to keywords within the current page.</p>
<h2>
	Avoid Navigation Changes</h2>
<p>
	<a href="../fresh-vs-familiar-aggressive-redesign/index.php" title="Fresh vs. Familiar: How Aggressively to Redesign, article">All users detest change</a>, but drastic design changes hurt seniors the most. Half of the seniors in our study said they <strong>keep a list of steps</strong> and instructions about how to use websites they need or often visit. If such a website changes drastically, these notes might become invalid, and seniors could struggle to understand the new design.</p>
<p>
	One user went to a redesigned site and eventually abandoned it. She said, <em>“I give up. As I say, I hate it when they change a steady information piece.”</em></p>
<p>
	Obviously, websites can’t always stay the same forever. But it’s worth trying to maintain consistency in key task steps for as long as possible. Also, you can reduce the future need for major website restructuring by conducting extensive early usability research on <a href="../workflow-expectations/index.php" title="Workflow Expectations: Presenting Steps at the Right Time, article">workflow steps</a>, <a href="../../topic/information-architecture/index.php" title="IA guidelines">information architecture (IA)</a>, and other foundational aspects.</p>
<h2>
	Why Seniors Use the Web</h2>
<p>
	As I mentioned in the introduction, ever-more seniors are using the web, despite the usability challenges. Following are some of the <strong>main activities seniors do online</strong>, together with examples of specific tasks our users said they had tried:</p>
<ul>
	<li>
		<strong>Health</strong>: “When I was put on [a drug], no one explained it to me. One of the nurses gave me a book, but I went to Internet, too. Anything I have to take, I want to know what it is all about.”</li>
	<li>
		<strong>Travel</strong>: “I have used the Internet for travel—Travelocity. I don’t put my credit card on the machine. I telephone. I just don’t feel like having my credit card on the computer. I checked the weather, I checked the price for the airlines, I found a flight, and then I telephoned the airlines. I found the price on the telephone was cheaper than the computer.”</li>
	<li>
		<strong>Hobbies</strong>: “I check into some of the various TV shows, and tried for tickets for shows. I got into the Internet as far as the names of the shows and all, but I didn’t know how to fill out the forms.”</li>
	<li>
		<strong>News</strong>: “I pay for the <em>Wall Street Journal</em> online. I get the print copy but sometimes the print is missing information, and I can get more online. They update the information online more. And the <em>New York Times</em>, too. You can go back days as well.”</li>
	<li>
		<strong>Finance</strong>: “I log into my bank account every morning. And then I go to my various retirement accounts.”</li>
	<li>
		<strong>Shopping</strong>: “I buy clothes and books. I like Lands' End. They let you build a model. I was using Priceline.com to save on food. I knit, and I like Wooly Hill Farm to buy yarn.”</li>
	<li>
		<strong>Social</strong>: “I was trying to find relatives, and I discovered an old army buddy of mine and so we are emailing. I am trying to get an email address of a second cousin once removed.”</li>
</ul>
<p>
	As these examples show, many categories of websites can attract seniors, who form a rapidly growing and increasingly affluent target audience. Site designers simply need to make the extra effort to accommodate the human aging process and make websites easier and faster for older people to use. As one of our test participants noted, <em>“Contrary to what many people believe, older people don’t have vast amounts of time to waste.”</em> If sites are too difficult, seniors will go elsewhere—just like all other users do.</p>
<h2>
	Full Research Report</h2>
<p>
	The full <a href="../../reports/senior-citizens-on-the-web/index.php" title="Nielsen Norman Group report - Web Usability for Senior Citizens: 106 Design Guidelines Based on Usability Studies with People Age 65 and Older">usability report with design guidelines for targeting seniors</a> (users aged 65+) is available for download.</p>
</section>
  

  
  <section>
    
        
    
  </section>
  
  
  
  
  

  


  <section>
    <br />
    <p class="share"><b>Share <span class="hide-for-small-down">this article</span>:</b> <a href="https://twitter.com/intent/tweet?url=http://www.nngroup.com/articles/usability-for-senior-citizens/&amp;text=Seniors%20as%20Web%20Users&amp;via=nngroup">Twitter</a> | <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=http://www.nngroup.com/articles/usability-for-senior-citizens/&amp;title=Seniors%20as%20Web%20Users&amp;source=Nielsen%20Norman%20Group">LinkedIn</a> | <a href="https://plus.google.com/share?url=http://www.nngroup.com/articles/usability-for-senior-citizens/">Google+</a> | <a href="mailto:?subject=NN/g Article: Seniors as Web Users&amp;body=http://www.nngroup.com/articles/usability-for-senior-citizens/">Email</a></p>
    <hr />
  </section>

  <footer class="l-subsection">
    
    
    <div class="learn-more">
      <h2>Learn More</h2>
      <p><a href="../subscribe/index.php">Subscribe to the weekly newsletter</a> to get notified about future articles.</p>
      </div>
    
    

    <div class="row show-for-small-down related-content">
      
      
      

      
      
        <div class="column small-12 topic-links">All articles about:
          <ul>
            
            <li><a href="../../topic/accessibility/index.php">Accessibility</a></li>
            
            <li><a href="../../topic/web-usability/index.php">Web Usability</a></li>
            
          </ul>
        </div>
      
      

      
      
        <div class="column small-12">
          <h3>UX Conference Training Courses</h3>
           <ul class="no-bullet">
    
        <li><a href="../../courses/ux-basic-training/index.php">UX Basic Training</a></li>
    
        <li><a href="../../courses/human-mind/index.php">The Human Mind and Usability</a></li>
    
        <li><a href="../../courses/emerging-patterns-web-design/index.php">Emerging Patterns for Web Design</a></li>
    
        <li><a href="../../courses/web-page-design/index.php">Web Page UX Design</a></li>
    
</ul>
        </div>
      
      

      
      
        <div class="small-12 column">
          <h3>Research Reports</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../reports/senior-citizens-on-the-web/index.php">Senior Citizens (Ages 65 and older) on the Web</a></li>
              
            
              
                <li><a href="../../reports/b2b-websites-usability/index.php">B2B Website Usability for Converting Users into Leads and Customers</a></li>
              
            
              
                <li><a href="../../reports/strategic-design-faqs/index.php">Strategic Design for Frequently Asked Questions</a></li>
              
            
              
                <li><a href="../../reports/usability-guidelines-accessible-web-design/index.php">Usability Guidelines for Accessible Web Design</a></li>
              
            
              
                <li><a href="../../reports/how-to-conduct-usability-studies-accessibility/index.php">How to Conduct Usability Studies for Accessibility</a></li>
              
            
          </ul>
        </div>
      
      

      
      
        <div class="small-12 column">
          <h3>Online Seminars</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../online-seminars/fundamental-flaw-flat-design/index.php">The Fundamental Flaw in Flat Design</a></li>
              
            
              
                <li><a href="../../online-seminars/millennials-online/index.php">Millennials Online</a></li>
              
            
              
                <li><a href="../../online-seminars/making-accessibility-happen/index.php">Making Accessibility Happen</a></li>
              
            
              
                <li><a href="../../online-seminars/university-websites/index.php">University Website UX Essentials</a></li>
              
            
          </ul>
        </div>
      
      
    </div>

    <div class="hide-for-small-down row related-content">
      <div class="medium-6 columns">
        
        
          <div>
            <h3>Articles</h3>
            <ul class="no-bullet">
              
                
                <li><a href="../touchscreen-screen-readers/index.php">Screen Readers on Touchscreen Devices</a></li>
                
              
                
                <li><a href="../making-flash-usable-for-users-with-disabilities/index.php">Making Flash Usable for Users With Disabilities</a></li>
                
              
                
                <li><a href="../beyond-accessibility-treating-users-with-disabilities-as-people/index.php">Beyond Accessibility: Treating Users with Disabilities as People</a></li>
                
              
                
                <li><a href="../web-form-design/index.php">Website Forms Usability:  Top 10 Recommendations</a></li>
                
              
                
                <li><a href="../duplicate-links/index.php">The Same Link Twice on the Same Page: Do Duplicates Help or Hurt?</a></li>
                
              
            </ul>
          </div>
        
        

        
          
          <div>
            <h3>Research Reports</h3>
            <ul class="no-bullet">
              
                
                  <li><a href="../../reports/senior-citizens-on-the-web/index.php">Senior Citizens (Ages 65 and older) on the Web</a></li>
                
              
                
                  <li><a href="../../reports/b2b-websites-usability/index.php">B2B Website Usability for Converting Users into Leads and Customers</a></li>
                
              
                
                  <li><a href="../../reports/strategic-design-faqs/index.php">Strategic Design for Frequently Asked Questions</a></li>
                
              
                
                  <li><a href="../../reports/usability-guidelines-accessible-web-design/index.php">Usability Guidelines for Accessible Web Design</a></li>
                
              
                
                  <li><a href="../../reports/how-to-conduct-usability-studies-accessibility/index.php">How to Conduct Usability Studies for Accessibility</a></li>
                
              
            </ul>
          </div>
          
        

      </div>
      <div class="medium-6 columns">
        
          
          <div>
            <h3>UX Conference Training Courses</h3>
             <ul class="no-bullet">
    
        <li><a href="../../courses/ux-basic-training/index.php">UX Basic Training</a></li>
    
        <li><a href="../../courses/human-mind/index.php">The Human Mind and Usability</a></li>
    
        <li><a href="../../courses/emerging-patterns-web-design/index.php">Emerging Patterns for Web Design</a></li>
    
        <li><a href="../../courses/web-page-design/index.php">Web Page UX Design</a></li>
    
</ul>
          </div>
          
        
        
          
          <div>
            <h3>Online Seminars</h3>
            <ul class="no-bullet">
            
                <li><a href="../../online-seminars/fundamental-flaw-flat-design/index.php">The Fundamental Flaw in Flat Design</a></li>
            
                <li><a href="../../online-seminars/millennials-online/index.php">Millennials Online</a></li>
            
                <li><a href="../../online-seminars/making-accessibility-happen/index.php">Making Accessibility Happen</a></li>
            
                <li><a href="../../online-seminars/university-websites/index.php">University Website UX Essentials</a></li>
            
            </ul>
          </div>
          
        
      </div>
    </div>
  </footer>
</article>

        </div>

      

    
  </div>
  <div class="row">
    
      <div class="column small-12 show-for-small-down l-navrail announcements">
        
          
        
      </div>
    
    
  </div>

    </div>
    




<footer id="page-footer">
    <div class="row hide-for-print">
        <div class="column small-12 medium-6">
            <section class="footer-popular">
                
                <h1>Popular Topics</h1>
                <div style="float: left;  width: 15em; display: block;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/applications/index.php">Application Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/e-commerce/index.php">E-Commerce Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/eyetracking/index.php">Eyetracking </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/information-architecture/index.php">Information Architecture </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/intranets/index.php">Intranet Design </a></li>
	<li><a href="../../topic/interaction-design/index.php">Interaction Design </a></li>
</ul>
</div>

<div style="float: left;  width: 15em; display: block; list-style: none;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/mobile-and-tablet-design/index.php">Mobile and Tablet Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/credibility-persuasion/index.php">Persuasive Design</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/research-methods/index.php">Research Methods</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/user-testing/index.php">User Testing </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/web-usability/index.php">Web Usability </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/writing-web/index.php">Web Writing &amp; Content Strategy </a></li>
</ul>
</div>

<p>&nbsp;</p>

                
            </section>
        </div>

        <div class="column small-12 medium-6">
            <div class="row">
                <div class="column small-6">
                    <section class="footer-about">
                        <h1>Follow Us</h1>
                        <ul>
	<li><a href="../subscribe/index.php">Subscribe to our weekly UX newsletter</a>,&nbsp;Jakob Nielsen&#39;s Alertbox</li>
	<li>Visit Don Norman&#39;s website, <a href="http://www.jnd.org/"> www.jnd.org </a></li>
	<li>Visit Bruce Tognazzini&#39;s website, <a href="http://www.asktog.com/"> www.asktog.com </a></li>
	<li style="padding-top: 6px;"><a class="twitter-follow-button" data-show-count="false" data-size="large" href="https://twitter.com/nngroup"><img alt="Follow @nngroup on Twitter" height="20" src="https://media.nngroup.com/media/editor/2016/12/30/twitter-follow-button.png" style="width: 117px; height: 20px;" width="117" /></a></li>
	<li style="padding-top: 6px;"><img alt="" height="14" src="https://media.nngroup.com/media/editor/2016/07/26/feed-icon-14x14.png" style="vertical-align:text-top;width:14px;margin-right:.25em;" width="14" /><a href="../../feed/rss/index.php">Subscribe to our RSS feed</a></li>
</ul>

                    </section>
                </div>

                <div class="column small-6">
                    <section class="footer-about right-column">
                        <h1>About
</h1>
                        <ul class="no-bullet">
	<li><a href="../../about/contact/index.php">Contact information</a></li>
	<li><a href="../../about/index.php">About Us</a></li>
	<li><a href="../../about/why-nng/index.php">Why NN/g? </a></li>
	<li><a href="../../return-policy/index.php">Return Policy</a></li>
</ul>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="copyright column small-12">
            <p><a href="../../copyright-and-reprint-info/index.php">Copyright</a> &copy; 1998-2017 Nielsen Norman Group, All Rights Reserved.</p>
        </div>
    </div>
</footer>


    


        <script src="https://media.nngroup.com/static/vendor/fastclick/js/fastclick.min.js"></script>
        <script src="https://media.nngroup.com/static/vendor/foundation/js/foundation.min.js"></script>
        <script src="http://media.nngroup.com/static/js/app.js"></script>
        <script src="http://media.nngroup.com/static/js/scroll-to-fixed.js"></script><script>var externalLinks = document.querySelectorAll('a[href^="http"]');for (var i = externalLinks.length-1; i >= 0; i--) {externalLinks[i].addEventListener("click", function(event) { alert("External links are disabled for this test."); event.preventDefault(); }, false);}var mailLinks = document.querySelectorAll('a[href^="mailto"]');for (var i = mailLinks.length-1; i >= 0; i--) {mailLinks[i].addEventListener("click", function(event) { alert("Email links are disabled for this test."); event.preventDefault(); }, false);}var externalForms = document.querySelectorAll('form');for (var i = externalForms.length-1; i >= 0; i--) {externalForms[i].addEventListener("submit", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false); externalForms[i].addEventListener("click", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false);}</script>

        <script>
            
            $(function() {
                $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
                $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
            });
        </script>

        
<script src="https://media.nngroup.com/static/js/articles.js"></script>


        
        <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1072699960;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072699960/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
    </body>

<!-- Mirrored from www.nngroup.com/articles/usability-for-senior-citizens/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:12:31 GMT -->
</html>
