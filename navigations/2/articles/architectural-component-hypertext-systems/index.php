<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->





    
<!-- Mirrored from www.nngroup.com/articles/architectural-component-hypertext-systems/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:13:20 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1026.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"licenseKey":"1a30ef9931","queueTime":0,"beacon":"bam.nr-data.net","errorBeacon":"bam.nr-data.net","applicationID":"2048992","transactionName":"NFxSMRBUX0EDUkIPDA0WdhAMVkVbDV8ZBxEXUFMJB0YfRAtUQRVZIktEDAFZVHYHRVcPD01eVRE=","applicationTime":471,"agent":""}</script>
        <title>Theory of Hypertext Systems: Article by Jakob Nielsen</title><meta property="og:title" content="Theory of Hypertext Systems: Article by Jakob Nielsen" />
  
        
        <meta name="description" content="Chapter 5 from Jakob Nielsen&#39;s book, Multimedia and Hypertext, describes the three levels of a hypertext system: the Presentation level, the Hypertext Abstract Machine (HAM) level, and the Database level. ">
        <meta property="og:description" content="Chapter 5 from Jakob Nielsen&#39;s book, Multimedia and Hypertext, describes the three levels of a hypertext system: the Presentation level, the Hypertext Abstract Machine (HAM) level, and the Database level. " />
        
  
        
	
        

        

        <meta name="robots" content="noarchive">
        <meta name="viewport" content="width=device-width">

        <link rel="search" type="application/opensearchdescription+xml" title="NN/g Search" href="../../opensearch.xml" />
        <link rel="shortcut icon" href="http://media.nngroup.com/static/img/favicon.ico" />

        <link rel="stylesheet" href="https://media.nngroup.com/static/vendor/font-awesome/css/font-awesome.min.css">

        
        <link rel="stylesheet" href="https://media.nngroup.com/static/css/7232a960f1df.css" type="text/css" />
        <link rel="stylesheet" href="http://fast.fonts.com/t/1.css?apiType=css&amp;projectid=c5846260-9def-46ec-9407-0f1349bd2d64" type="text/css">

        <script src="https://media.nngroup.com/static/vendor/modernizr/js/modernizr.min.js"></script>

        <script type="text/javascript" src="https://media.nngroup.com/static/js/3283be5def74.js"></script>
        

        

        
        
        <!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=229034,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('http://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->


<!-- Google Analytics Tracking Code for http://nngroup.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23132681-1', 'nngroup.com');
  ga('require', 'linkid', 'https://www.nngroup.com/articles/architectural-component-hypertext-systems/linkid.js');
  ga('require', 'displayfeatures');

// Optimizely Universal Analytics Integration
 window.optimizely = window.optimizely || [];
 window.optimizely.push("activateUniversalAnalytics");

  ga('send', 'pageview');

</script>
<script>
/**
* Function to track a click on an outbound link in Google Analytics.
*/
function trackOutboundLink(label, url) {
    if(window.ga && ga.loaded) {
        ga('send', 'event', 'outbound', 'click', label, {'hitCallback': function(){document.location = url;}
        });
    } else {
        document.location = url;
    }
}
/**
* Function to track a click on a link in the article announcement area.
*/
$('#articleAnnouncement a').on('click', function(){
  ga('send', 'event', 'link', 'click', 'articleAnnouncement');
});
</script>

<script type="text/javascript">
 setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2421.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



<!-- Hotjar Tracking Code for http://nngroup.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:121605,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

        <script src="https://media.nngroup.com/static/js/analytics/track_outbound_link.js"></script>
        
        
        
    </head>
    <body class="location-articles articles-detail">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
    <header id="page-header">
        <div id="cart" class="shopping-cart mobile-cart show-for-small-down"></div>
        <div class="row">
            <div id="masthead" class="masthead small-12 medium-7 column">

                <!-- The button to toggles menu on small screen -->

                <h1><a href="../../index.php"><img src="http://media.nngroup.com/static/img/logo-wordmark.png" width="74" height="30"> Nielsen <span class="norman">Norman</span> Group</a></h1>
                <p class="tagline show-for-medium-up">Evidence-Based User Experience Research, Training, and Consulting</p>

                <!-- The button to toggles form search on small screen -->


                <div style="clear: right;width:  100%;height: 1px;"></div><?php define('ROOT_PATH',$_SERVER["DOCUMENT_ROOT"] . '/apps/navigation_styles/navigations/2'); ?><?php include(ROOT_PATH)."/navigation.php"; ?>

                <!-- Drop down menu on small screen -->
                <div id="hideNav" class="show-for-small-down">
                  
                  <ul class="menu-content">
                    <li><a href="../../index.php">Home</a></li>
                    <li class="has-submenu">
                      <a>Training</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../training/index.php">Overview</a></li>
                        
<li id="nav-ux-conference"><a href="../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../online-seminars/index.php"><span>Online Seminars</span></a></li>

                      </ul>
                    </li>
                    <li class="has-submenu">
                      <a>Consulting</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
  <li><a href="../../consulting/index.php">Overview</a></li>
  
  <li><a href="../../consulting/benchmarking/index.php">Benchmarking</a></li>
  
  <li><a href="../../consulting/expert-design-review/index.php">Expert Design Review</a></li>
  
  <li><a href="../../consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
  
  <li><a href="../../consulting/usability-testing/index.php">Usability Testing </a></li>
  
  <li><a href="../../consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
  
  <li><a href="../../in-house-training/index.php">In-House Training</a></li>
</ul>

                    </li>

                    <li><a href="../../reports/index.php">Reports</a></li>
                    <li><a href="../index.php">Articles</a></li>
                    <li class="has-submenu"><a>About NN/G</a><i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../about/index.php">Overview</a></li>
                        <li><a href="../../people/index.php">People</a></li>
                        <li><a href="../../about/why-nng/index.php">Why NN/g?</a></li>
                        <li><a href="../../about/contact/index.php">Contact</a></li>
                        <li><a href="../../news/index.php">News</a></li>
                        <li><a href="../../about/history/index.php">History</a></li>
                        <li><a href="../../books/index.php">Books</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                </div>

                <hr class="header-divider show-for-small-only"/>


            </div>
       </div>

        <div id="hideNav">            <nav class="primary-nav small-12 column">
                <ul>
                    <li><a class="navitem" id="nav-home" href="../../index.php">Home</a></li>
                    <li><a class="navitem" id="nav-training" href="../../training/index.php">Training</a></li>
                    <li><a class="navitem" id="nav-consulting" href="../../consulting/index.php">Consulting</a></li>
                    <li><a class="navitem" id="nav-reports" href="../../reports/index.php">Reports</a></li>
                    <li><a class="navitem" id="nav-articles" href="../index.php">Articles</a></li>
                    <li><a class="navitem" id="nav-about" href="../../about/index.php">About NN/G</a></li>
                </ul>
            </nav>
            <nav class="secondary-nav small-12 medium-12 large-12 columns">
                
                
            </nav>
        </div>
    </header>
    <div id="main" class="l-content" onclick="closeNav(event)"><span id="preventClose" style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776; Menu</span>
    
  

  

  <div class="row">
    
      
      <section class="small-12 medium-3 columns collapsable-content l-navrail articles-left-column">
        <h1 class="show-for-small-down collapse-link mobile-browse-header">Browse by Topic and Author</h1>
          <div class="row collapsable">

            
            <div class="small-6 medium-12 columns">
              <section id="article-popular-topics" style="display: none;">
                
                <h1>Topics</h1>
                <ul id="article-popular-topics">
                    
                    <li>
                        
                        <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/intranets/all/index.php">Intranets</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/user-testing/all/index.php">User Testing</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                        
                    </li>
                    
                    <li><a href="#" id="article-see-all-topics"><strong>See all topics&hellip;</strong></a></li>
                </ul>
                
              </section>
              <section id="article-all-topics" style="display: none;">
                
                <h1>
                  <span class="show-for-medium-up">All Article Topics</span>
                  <span class="show-for-small-down">All Topics</span>
                  (<a href="#" id="article-hide-all-topics">hide</a>)
                </h1>
                <ul>
                  
                  <li>
                    
                    <a href="../../topic/accessibility/all/index.php">Accessibility</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/agile/all/index.php">Agile</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/analytics-and-metrics/all/index.php">Analytics &amp; Metrics</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/applications/all/index.php">Application Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/b2b-websites/all/index.php">B2B Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/behavior-patterns/all/index.php">Behavior Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/branding/all/index.php">Branding</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/content-strategy/all/index.php">Content Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/corporate-websites/all/index.php">Corporate Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/design-patterns/all/index.php">Design Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-design-process/all/index.php">Design Process</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/email/all/index.php">Email</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/eyetracking/all/index.php">Eyetracking</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/heuristic-evaluation/all/index.php">Heuristic Evaluation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/human-computer-interaction/all/index.php">Human Computer Interaction</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-ideation/all/index.php">Ideation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/information-architecture/all/index.php">Information Architecture</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/interaction-design/all/index.php">Interaction Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/international-users/all/index.php">International Users</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/intranets/all/index.php">Intranets</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-management/all/index.php">Management</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/navigation/all/index.php">Navigation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/non-profit-websites/all/index.php">Non-Profit Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/personas/all/index.php">Personas</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/persuasive-design/all/index.php">Persuasive Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/prototyping/all/index.php">Prototyping</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/psychology-and-ux/all/index.php">Psychology and UX</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/research-methods/all/index.php">Research Methods</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/search/all/index.php">Search</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/social-media/all/index.php">Social Media</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/strategy/all/index.php">Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/user-testing/all/index.php">User Testing</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-teams/all/index.php">UX Teams</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/visual-design/all/index.php">Visual Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/writing-web/all/index.php">Writing for the Web</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/young-users/all/index.php">Young Users</a>
                    
                  </li>
                  
                </ul>
                
              </section>
            </div>
            <script>
              /* Determine which topics list will be displayed: the full or the
               * partial.  We are inlining this close to the list itself to
               * prevent rendering delays on the client
               */
              var topic_list = $.cookie("article-topics");

              var popular_topics = $('#article-popular-topics');
              var all_topics = $('#article-all-topics');

              if(topic_list == 1) {
                  popular_topics.hide();
                  all_topics.show();
              } else {
                  popular_topics.show();
                  all_topics.hide();
              }
            </script>
            

            
            <div class="small-6 medium-12 columns">
              
              <section id="article-initial-authors">
                  <h1>Author</h1>
                  <ul>
                    
                    <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                    
                    <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                    
                    <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                    
                    <li><a href="#" onClick="$('#article-initial-authors').hide(); $('#article-all-authors').show();"><strong>See all authors&hellip;</strong></a></li>
                  </ul>
              </section>
              <section id="article-all-authors" style="display: none;">
                <h1>
                  All Authors
                  (<a href="#" id="article-hide-all-authors">hide</a>)
                </h1>
                <ul>
                  
                  <li><a href="../author/raluca-budiu/index.php">Raluca Budiu</a></li>
                  
                  <li><a href="../author/patty-caya/index.php">Patty Caya</a></li>
                  
                  <li><a href="../author/susan-farrell/index.php">Susan Farrell</a></li>
                  
                  <li><a href="../author/therese-fessenden/index.php">Therese Fessenden</a></li>
                  
                  <li><a href="../author/kim-flaherty/index.php">Kim Flaherty</a></li>
                  
                  <li><a href="../author/sarah-gibbons/index.php">Sarah Gibbons</a></li>
                  
                  <li><a href="../author/aurora-harley/index.php">Aurora Harley</a></li>
                  
                  <li><a href="../author/kate-kaplan/index.php">Kate Kaplan</a></li>
                  
                  <li><a href="../author/page-laubheimer/index.php">Page Laubheimer</a></li>
                  
                  <li><a href="../author/angela-li/index.php">Angie Li</a></li>
                  
                  <li><a href="../author/hoa-loranger/index.php">Hoa Loranger</a></li>
                  
                  <li><a href="../author/kate-meyer/index.php">Kate Meyer</a></li>
                  
                  <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                  
                  <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                  
                  <li><a href="../author/kara-pernice/index.php">Kara Pernice</a></li>
                  
                  <li><a href="../author/christian-rohrer/index.php">Christian Rohrer</a></li>
                  
                  <li><a href="../author/amy-schade/index.php">Amy Schade</a></li>
                  
                  <li><a href="../author/katie-sherwin/index.php">Katie Sherwin</a></li>
                  
                  <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                  
                  <li><a href="../author/kathryn-whitenton/index.php">Kathryn Whitenton</a></li>
                  
                </ul>
              </section>
              
            </div>
            
          </div>

          
          <section class="hide-for-small-down">
            
              
              <h1>Recent Articles</h1>
              
              <ul>
                  
                  <li><a href="../seamless-cross-channel/index.php">Seamlessness in the Omnichannel User Experience</a></li>
                  
                  <li><a href="../brand-intention-interpretation/index.php">Experience Design: Bridging Brand Intention and Brand Interpretation</a></li>
                  
                  <li><a href="../eyetracking-tasks-efficient-scanning/index.php">Scanning Patterns on the Web Are Optimized for the Current Task</a></li>
                  
                  <li><a href="../flat-design-best-practices/index.php">Flat-Design Best Practices</a></li>
                  
                  <li><a href="../ux-user-stories/index.php">Accounting for UX Work with User Stories in Agile Projects</a></li>
                  
                  <li><a href="../index.php"><strong>See all articles&hellip;</strong></a></li>
              </ul>
              
            
          </section>
          

          
          
          <section class="hide-for-small-down">
            <h1>Popular Articles</h1>
            
            <ul>
                
                <li><a href="../usability-101-introduction-to-usability/index.php">Usability 101: Introduction to Usability</a></li>
                
                <li><a href="../top-10-mistakes-web-design/index.php">Top 10 Mistakes in Web Design</a></li>
                
                <li><a href="../how-users-read-on-the-web/index.php">How Users Read on the Web</a></li>
                
                <li><a href="../f-shaped-pattern-reading-web-content/index.php">F-Shaped Pattern For Reading Web Content</a></li>
                
                <li><a href="../ten-usability-heuristics/index.php">10 Usability Heuristics for User Interface Design</a></li>
                
                <li><a href="../intranet-design-2016/index.php">10 Best Intranets of 2016</a></li>
                
                <li><a href="../which-ux-research-methods/index.php">When to Use Which User-Experience Research Methods</a></li>
                
                <li><a href="../response-times-3-important-limits/index.php">Response Times: The 3 Important Limits</a></li>
                
                <li><a href="../why-you-only-need-to-test-with-5-users/index.php">Why You Only Need to Test with 5 Users</a></li>
                
                <li><a href="../page-fold-manifesto/index.php">The Fold Manifesto: Why the Page Fold Still Matters</a></li>
                
            </ul>
          </section>
          
          

          
            <section class="hide-for-small-down">
              
            </section>
          

      </section>
      

      
        <div class="small-12 medium-9 columns content-body">
          




<article class="l-subsection alertbox-article-content article-detail-bl">
  
  <header>
    <div class="l-subsection">
      <h1>Architectural Component of Hypertext Systems</h1>
      <div class="author-meta">by
        
          
            
            <a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a>
            
          
        on  January 1, 1995
        
      </div>

      

      
      <div class="topic-links">
        Topics:
        
          <ul>

  <li><a href="../../topic/navigation/index.php">Navigation</a></li>

</ul>
        
      </div>
      
    </div>
  </header>
  

  
  
  <section>
    <p class="lede"><strong>Summary:</strong> Hypertext systems include a Presentation level, a Hypertext Abstract Machine (HAM) level, and a Database level. The following sections describe each of the levels in further detail, starting at the bottom. </p>
    <br />
  </section>
  
  

  
  <section id="article-body"><p><em>This is chapter 5 from Jakob Nielsen's book <a href="../../books/multimedia-and-hypertext/index.php"> Multimedia and Hypertext: The Internet and Beyond</a>, Morgan Kaufmann Publishers, 1995. (For full literature references, please see the bibliography in the book.)</em></p>

<p>In theory one can distinguish three levels of a hypertext system [Campbell and Goodman 1988]:</p>

<ul>
	<li>Presentation level: user interface</li>
	<li>Hypertext Abstract Machine (HAM) level: nodes and links</li>
	<li>Database level: storage, shared data, and network access</li>
</ul>

<p>Actually, very few current hypertext systems follow this model in their internal structure; most are a more or less confused mix of features. Even so, the model shows interesting directions for the future and is important for standardization work. The following sections describe each of the levels in further detail, starting at the bottom.</p>

<h3>The Database Level</h3>

<p>The database level is at the bottom of the three-level model and deals with all the traditional issues of information storage that do not really have anything specifically to do with hypertext. It is necessary to store large amounts of information on various computer storage devices like hard disks, optical disks, etc., and it may be necessary to keep some of the information stored on remote servers accessed through a network. No matter how the information is stored it should be possible to retrieve a specified small chunk of it in a very short time. This sounds very much like a specification for a database, which is what it is.</p>

<p>Furthermore, the database level should handle other traditional database issues, like multiuser access to the information, and various security considerations, including backup. Ultimately it will be the database level's responsibility to enforce the access controls which may be defined at the upper levels of the architecture.</p>

<p>As far as the database level is concerned, the hypertext nodes and links are just data objects with no particular meaning. Each of them forms a unit that only one user can modify at the same time and that takes up so many bits of storage space. In real life, it may be advantageous for the database level to know a little bit more about its data objects in order to enable it to manage its storage space most efficiently and provide faster response. But in any case the hypertext field would do well in taking advantage of the extensive work and experience in the traditional database field for the design and implementation of the database level.</p>

<h3>The Hypertext Abstract Machine (HAM) Level</h3>

<p>The HAM sits in the middle of the sandwich between the database and user interface levels. This center is where the hypertext system determines the basic nature of its nodes and links and where it maintains the relation among them. See the discussion below about the range of design choices regarding nodes and links. The HAM would have knowledge of the form of the nodes and links and would know what attributes were related to each. For example, a node might have an "owner" attribute that specified the user who created it and who has to authorize updates, or it could have a version number. Links might be typed as in NoteCards, or they might be plain pointers as in Guide.</p>

<p>The HAM is the best candidate for standardization of import-export formats for hypertexts, since the database level has to be heavily machine dependent in its storage format and the user interface level is highly different from one hypertext system to the next. This leaves only the HAM, and since we do need the ability to transfer information from one hypertext system to the other, we have to come up with an interchange format at this level. One current example of a HAM-level standard is HyTime (the ISO standard for <em> hy </em> permedia/ <em> time </em> -based document structuring) [Newcomb et al. 1991; DeRose and Durand 1994].</p>

<p>Interchanging hypertexts is more difficult than simply interchanging the component data in the nodes, even though there are also problems with the less standardized data formats for non-ASCII information like graphics and video clips. The problem is that hypertext interchange also requires the transfer of linking information. It should be possible to transfer the basic links (i.e., the "A points to B" type information), but large parts of the linking information may be lost.</p>

<p><img alt="Screenshot of one-to-many links" src="http://media.nngroup.com/media/editor/2012/10/31/dickens_crossroads_interleaf.gif" style="width: 617px; height: 481px;"/><br/>
<strong>Figure 5.1. </strong> <em> So-called crossroads document used to represent one-to-many links in the WorldView version of the Dickens Web. Copyright © 1992–94 by Paul Kahn, George P. Landow, and Brown University, reprinted by permission. </em></p>

<p>For example, some hypertext systems like Intermedia have links that point to specific text strings in the destination node, whereas other systems like Hyperties only point to the destination node as a complete entity. A transfer of a hypertext from Intermedia to Hyperties would therefore lose important aspects of the linking information but should still be possible in principle. If we were to transfer a hypertext structure from Hyperties to Intermedia there would be no way to come up with a sensible substring for the destination anchor because the Hyperties author would not have considered that option when writing the hypertext. We would probably have to come up with a dummy anchor in the beginning of the destination node just to keep the Intermedia system happy. Of course if the transfer had been from Intermedia to Guide (which does have anchored link destinations), then we would want the transfer to keep the information about the destination substring so that it could be highlighted on arrival. The difficulty is that we would like to achieve both of these results with a single interchange format.</p>

<p>Kahn and Landow [1992] report on their experience in transferring the <cite> Dickens Web </cite> (a hypertext about Charles Dickens) from Intermedia where it was originally developed to two other hypertext systems: Eastgate System's Storyspace and Interleaf's WorldView. One of the major problems was that the "fat" one-to-many links in Intermedia could not be transferred to WorldView since it only had traditional one-to-one hypertext links. As an alternative, Kahn and Landow came up with "crossroads" documents like the one shown in Figure 5.1.</p>

<p>A crossroads documents serves as a waystation between the original departure anchor and the original destination anchors. The original link is replaced by a link from the departure anchor to the crossroads document as well as however many links it take to connect the crossroads document to all the original destination anchors. This example shows how awkward it can be to move hypertexts between systems if they do not share fundamental concepts as to the structure of the hypertext.</p>

<p>Work on the definition of hypertext interchange formats was initially started through informal meetings of the so-called Dexter Group, consisting of many of the designers of the early hypertext systems. (The Dexter Group is named after the inn in New Hampshire where it had its first meeting. The two meetings that resulted in the reference model were organized by Jan Walker and John Leggett and funded primarily by Digital Equipment Corporation and Texas A&amp;M University.) Since the hypertext standardization workshop in January 1990, work on hypertext reference models has continued through more formal activities at the U.S. National Institute of Standards and Technology.</p>

<p>This work has resulted in more detailed architectural models than the simple three-level model discussed here [Halasz and Schwartz 1990, 1994; Grønbæk and Trigg 1992, 1994] and also in some preliminary success in transferring hypertext information from NoteCards to HyperCard. Both NoteCards and HyperCard are frame-based hypertext engines, where the information is displayed on fixed cards. It would presumably be harder to transfer between hypertext systems with different architectures, such as, for example, a transfer from a window-based system like Guide with its long scrolling texts to a frame-based system. See the discussion of frame-based vs. window-based hypertext in the following section on Nodes. Figure 5.2 shows the relation between the Dexter model and the model used in this book.</p>

<p><img alt="Comparing two abstract models of the architectures of hypertext systems" src="http://media.nngroup.com/media/editor/2012/10/31/hypertext_abstract_machine_model_model.png" style="width: 612px; height: 399px;"/><br/>
<strong>Figure 5.2. </strong> <em> Comparing the Dexter reference model to the three-level model proposed by Campbell and Goodman [1988] and used in this book. There are some terminology differences, such as the use of the word "storage" in the Dexter model to refer to the representation of the abstract hypertext network, but otherwise the main difference is the explicit discussion in the Dexter model of the interfaces between the levels, especially with respect to anchors. </em></p>

<h3>The User Interface Level</h3>

<p>The user interface deals with the presentation of the information in the HAM, including such issues as what commands should be made available to the user, how to show nodes and links, and whether to include overview diagrams or not.</p>

<p>Let us assume that the HAM level of a hypertext defines the links as being typed. The user interface level might decide not to display that information at all to some novice users and to make typing information available only in an authoring mode. The very distinction between reading and writing is one of the basic user interface issues.</p>

<p>Let us now assume that the user interface level does want to display the link typing to the user. It might want to do so by changing the shape of the cursor, as Guide does (see Figure 3.8), or by having a special notation for various forms for anchors. It could also decide to display typing information in an overview diagram. If it had a color display available, it might choose to show each link type in a different color, whereas on a monochrome display it would have to use different representations, such as different line patterns (like NoteCards), small icons (like CM/1, see Figure 4.3), or by using words to label the lines.</p>

<p>Actually, this decision cannot be made at the user interface level in isolation without considering the likely form of the data in the HAM. If the hypertext will have only a few link types, then colors or line patterns are suitable choices, but human ability to understand and distinguish such an encoding is limited to about seven different values. Icons could support hypertexts with somewhat more link types, but a hypertext with hundreds of link types would probably require the use of the type names in the interface.</p>

<h2>Nodes</h2>

<p>Nodes are the fundamental unit of hypertext, but there is no agreement as to what really constitutes a "node." The main distinction is between frame-based systems and window-based systems.</p>

<p>Frames take up a specific amount of space on the computer screen no matter how much information they contain. Typical examples are the KMS frames and the HyperCard cards. Often the size of the frame is defined as the size of the computer screen, but that determination may not hold in all systems. Since the frame has a fixed size, the user may have to split a given amount of information over several frames if it cannot fit into one. The advantage of frames is that all user navigation takes place using whatever hypertext mechanisms are provided by the system.</p>

<p>In contrast, window-based systems require the user to use a scrolling mechanism in addition to the hypertext mechanisms to get the desired part of the node to show in the window. Because the system can display only a (potentially small) part of the node through the window at any given time, the node may be as large as needed, and the need for potential unnatural distribution of text over several nodes is eliminated. Guide and Intermedia are typical window-based systems.</p>

<p>A great disadvantage of window-based hypertexts is that the hypertext designer has no control over how the node will appear when the user reads it since it can be scrolled in many ways. The advantage is that windows may be of different size depending on the importance and nature of information they hold. One can imagine a window-based system that did away with scrolling and thus kept most of the advantages of both display formats.</p>

<p>The real world is not quite as simple as the clear distinction between frames and windows. HyperCard is mostly frame-based but includes the possibility for having scrolling text fields as part of a card. Hyperties uses a full-screen display without scrolling but instead requires the user to page back and forth through a sequence of screens in case the node is too big to fit on a single screen.</p>

<p>Most current hypertext systems provide fixed information in the nodes as written by the original author. In computational hypertext systems like KMS and HyperCard (with an embedded programming language) or NoteCards (with an interface to a programming language), it is possible to have computed nodes generated for the reader by the system. An example is a node with the current weather forecast retrieved from an online service like the American Prodigy or the French Minitel. Computing a node on the fly by executing a program obviously involves the risk of Trojan horses leading to virus infection or other problems. The problem is minimized when the program is executed on a remote server that is restricted to transmitting the resulting data to the user's computer. If the code has to execute on the user's computer precautions will need to be taken in the form of virus scanners. There are also efforts under way for constructing safe programming languages that can be interpreted inside a protective shell that ensures that no permanent damage can be done to the user's system.</p>

<p>If one has a certain amount of information to communicate, one issue is whether it should be split into many small nodes or kept in a rather small number of larger nodes. Kreitzberg and Shneiderman [1988] report on a small experiment to investigate this issue, wherein they split the same text into either 46 articles of between 4 and 83 lines or 5 articles of between 104 to 150 lines in Hyperties. The result was that users could answer questions significantly faster in the information base with many small nodes (125 sec. vs. 178 sec. per answer). One reason for this result is probably that Hyperties is one of the hypertext systems that links to the beginning of an article and not to the location within an article where the information of interest for the departure point is located. Because of this feature, Hyperties is most easily operated with small, focused nodes dealing with precisely one issue so that there can be no doubt about what part of the node a link points to.</p>

<h2>Links</h2>

<p>Links are the other fundamental unit of hypertext besides nodes. Links are almost always anchored at their departure point to provide the user with some explicit object to activate in order to follow the link. The result of activating the anchor is to follow the link to its destination node. Most often, this anchoring takes the form of "embedded menus" where part of the primary text or graphics does double duty as being both information in itself and being the link anchor. It is also possible to have the hypertext anchors listed as separate menus, but that somehow seems to reduce the "hypertext feel" of a design, and a study by Vora et al. [1994] found that users performed 26% faster when the anchors were part of the main text.</p>

<p>Most links are explicit in the sense that they have been defined by somebody as connecting the departure node with the destination node. Some systems also provide <em> implicit </em> links, which are not defined as such but follow from various properties of the information. A classic example of implicit links is the automatic glossary lookup possible in Intermedia (see Figure 4.15). The InterLex server provides a link from any word in any Intermedia document to the definition of that word in the dictionary, but it would obviously be ridiculous to have to store all these links explicitly. Only when the user requests the definition of a word does the system need to find the destination for the link.</p>

<p>The StrathTutor system [Kibby and Mayes 1989] provided another kind of implicit link. The hypertext author was not expected to provide links between nodes but was instead asked to define a set of relevant attributes for each node and for areas of interest in the node. These attributes were keywords taken from a predefined restricted vocabulary. The areas of interest were called "hotspots" and served a purpose similar to anchors in other hypertext systems. When the user activated a hotspot, the system would view the user's interests as being defined by the combination of the attributes (keywords) from the current node and the selected hotspot. StrathTutor therefore linked to a new node having the highest overlap between its own attributes and this set of attributes. Kibby and Mayes claimed that this form for distributed specification of hypertext connections was the only way one could manage the authoring of really big hypertexts.</p>

<p>The StrathTutor links were an example of computed links determined by the system while the reader is reading, instead of being statically determined in advance by the author. Another example of computed links is a link from a tourist guide like <cite> Glasgow Online </cite> to the train schedule, where the system could link to the listing for the next train out of Glasgow for each destination.</p>

<p>A hypertext link has two ends. Even if a link is not bidirectional there may still be a need to anchor it explicitly at the destination node. Most frame-based hypertext systems only have links that point to an entire node, but when the destination is large, it is an advantage for the user to have the system point out the relevant information more precisely. See for example how the Drexel Disk highlights Building 53 because the user jumped to the campus map in Figure 1.3 from a description of the repair facility (which is located in that building).</p>

<p>In general, a hypertext design should tell the user why the destination for a link was an interesting place to jump to by relating it to the point of departure and following a set of conventions for the "rhetoric of arrival" [Landow 1989a].</p>

<p>Given that the hypertext is based on explicit links, the next issue is whether or not to make the anchors especially prominent on the screen compared with the rest of the node. In a sparse hypertext, where maybe less than 10% of the information serves as anchors, it is probably a good idea to visually emphasize the anchors. This is just a special case of the general user interface guideline of letting the user know what options are available in a dialogue. In a rich hypertext, where almost everything is linked to something, the best advice would be to remove any special emphasis on the anchors. After all, if everything is highlighted, then nothing is really highlighted anyway.</p>

<p>It is possible to use the Guide method of providing feedback by changing the shape of the cursor when it is over an anchor (see Figure 3.8). But that method should still be supplemented with some visual indication of the location of the anchors since users will otherwise be reduced to playing mine sweeper with the mouse to discover the active areas of the screen.</p>

<p>Unfortunately the highlighting of anchors conflicts with the use of emphasis in the running text. Traditionally writers have used typographical notation like <i> italics </i> or <b> boldfaced </b> type to indicate various forms for emphasis or special purpose text like quotations, and we would like to keep these capabilities for hypertext authors. But many current hypertext systems use the same or similar notation to indicate hypertext anchors also. This can unfortunately be very confusing to users unless the author has used a style guide to provide consistent notation for anchors and running emphasis. One solution to this problem may be the invention of special typographical cues for hypertext links [Evenson et al. 1989] and the gradual emergence of conventions for hypertext notation.</p>

<p>Most current hypertext systems have plain links, which are just connections between two nodes (and possibly anchors). The advantage of that approach is of course the simplicity of both authoring and reading. There is nothing to do with links except to follow them, and that one action can be achieved by a click of the mouse.</p>

<p>Alternatively, a link can be tagged with keywords or semantic attributes such as the name of the creator or the date it was created. These tags allow one to reduce the complexity of a hypertext through filter queries like, "Show all links created after March 23, 1995" or "Hide all links by so-and-so" (if we think that that person's contributions are rubbish).</p>

<p>Links can also be typed to distinguish among different forms of relationship between nodes. Trigg [1983] presents a very elaborate taxonomy of 75 different link types, including abstraction, example, formalization, application, rewrite, simplification, refutation, support, and data.</p>

<p>In addition to the standard links connecting two nodes, some hypertext systems also have "super-links" to connect a larger number of nodes. There are several possibilities for dealing with having a single i.Anchors:Multiple destinations;anchor connected to several destinations. The two simplest options are either to show a menu of the links or to go to all the destinations at the same time. Intermedia uses the menu option and allows users to choose only a single destination. This approach requires good names for the links or destination nodes in order for users to be able to understand their options. Some users of NoteCards have implemented a "fat link" type that opens windows on the screen for all the destination nodes.</p>

<p>The alternative way to deal with multiple destinations would be to have the system choose for the user in some way. The choice could be based on the system's model of the user's needs or some other estimate of the best destination, or it could simply be random, as in the example discussed towards the end of Chapter 2.</p>

<p><img alt="Screenshot from CD-ROM" src="http://media.nngroup.com/media/editor/2012/10/31/kon-tiki.png" style="width: 649px; height: 486px;"/><br/>
<strong>Figure 5.3. </strong> <em> The Kon-Tiki system generates a video footnote by zooming the large video image to an icon. Copyright © 1993 by The Kon-Tiki Museum, reprinted by permission. </em></p>

<p>Link anchors present special problems for layered hypertext architectures like the model presented in the beginning of this chapter. In principle, links belong at the hypertext abstract machine level, but the location of the anchor in the node is dependent on the storage structure for the node media. In a text-only node, an anchor position can be described as a substring ("characters 25–37"), whereas an anchor in a film clip needs both substring information ("film frames 517–724") and a graphic location ("the rectangle [(10,10);(20,20)]"). Some dynamic anchors may be even harder to specify: Try encoding the anchors in a video of a football game to allow the user to click on a player at any time to link to that player's name and statistics. Therefore the actual anchoring of the link cannot be handled by the hypertext abstract machine. The solution in the Dexter model [Halasz and Schwartz 1990] is to define an explicit interface between the hypertext abstract machine (called "storage layer" in their model) and the database level (called "within-component layer" in their model) as shown in Figure 5.2. Anchors become indirect pointers and the anchoring interface provides a translation between anchor identifiers in the hypertext abstract machine and actual anchor values in the node data.</p>

<p>Gunnar Liestøl and Per Siljubergsåsen have designed a user interface for the Kon-Tiki Museum in Norway that uses an interesting way of indicating links in video materials [Liestøl 1994]. The museum has large amounts of films recorded in connection with Thor Heyerdahl's expeditions, and the design challenge was how to represent the links between the video clips and other information such as text, maps, and photographs.</p>

<p>To get a device pointing out of a video sequence, the designers borrowed a convention from the Macintosh user interface guidelines (Michael Arent [Vertelney et al. 1990] uses the term "interface slang" for this approach to borrowing elements of an established interaction vocabulary for other, though related, uses.): when documents are opened or closed on the Mac, the relationship between the document and the icon representing it is visualized by an animated rectangle zooming between the two positions. Similarly, the main video area in the Kon-Tiki system "throws off" miniature pictures that zoom down from the video to rest on the screen as "video footnotes" that can be clicked for further information about that part of the video.</p>

<h3>Annotations</h3>

<p>A special link type is the annotation link to a small, additional amount of information. The reading of an annotation typically takes the form of a temporary excursion from the primary material to which the reader returns after having finished with the annotation. Annotations are quite similar to footnotes in traditional text and can be implemented, for instance, as Guide pop-up windows that disappear as soon as the user releases the mouse button. Alternatively, as shown in Figure 5.4, annotations can be floating "stickies" (almost always shown in yellow windoids in homage to 3M's Post-It notes [Lucas and Schneider 1994; Scott 1994]), or they can be accessed through an icon.</p>

<p><img alt="Sticky-note annotation from DEC system." src="http://media.nngroup.com/media/editor/2012/10/31/dec_sticky_note.png" style="width: 640px; height: 481px;"/><br/>
<strong>Figure 5.4. </strong> <em> Use of sticky-notes to represent annotations. To add an annotation, the user tears off a sticky-note from the pile to the right and places it on the object that needs a comment. Every time this object is displayed in the future, the sticky-note will be there (until the user removes it). Copyright © 1994 by J. Ray Scott, Digital Equipment Corporation, reprinted by permission. </em></p>

<p>Hypertext writers can use annotations in the same way they would use footnotes in traditional text with the exception that hypertext annotations are less intrusive because they are not shown unless the reader asks for them. The most interesting use of annotations in hypertext is for the readers, however. Many hypertext systems allow readers to add new links to the primary material even if they do not always allow the reader to change the original nodes and links, and readers can use these facilities to customize the information space to their own needs. For example, readers of a hypertext medical handbook [Frisse 1988a] might want to supplement the generic description of a drug in the handbook with an annotation stating the brand name normally prescribed at their hospital.</p>

<p><img alt="Screenshot of InterNote annotation service" src="http://media.nngroup.com/media/editor/2012/10/31/internote.gif" style="width: 598px; height: 491px;"/><br/>
<strong>Figure 5.5. </strong> <em> The InterNote service within IRIS Intermedia provides a quick facility for annotating documents of any type. The top frame of the note window (right) contains the annotator's suggestion of a change in wording. The bottom frame explains the reason for the suggestion. The author of the "Microtubules Introduction" document may now choose to incorporate the change by selecting the marker at either end of the link that connects the note to the document and choosing the "Incorporate Annotation" command. © 1989 by Brown University, reprinted with permission. </em></p>

<p>One system that does provide an annotation facility is Intermedia, as shown in Figure 5.5. Other systems like Hyperties do not allow readers to annotate but reserve all options to change the information space for the authors. In the case of Hyperties, one reason for refusing readers the right to add information is that the system was originally designed for museum information systems where one may in fact not want random users to alter the content of the hypertext. Another reason was the desire for an extremely simple user interface to Hyperties: Having a smaller number of options available to the user means that there is less to learn.</p>

<p>Finally, users can annotate a hypertext by adding various forms of highlighting [Nielsen 1986], such as the use of various colors to emphasize text of special importance to a particular user [Irler and Barbieri 1990]. One product that supports highlighting is Folio. Even though individualized highlighting does not really constitute a form of link, information gathered from highlighting (possibly from multiple users) can be used in filtering the hypertext to show only important information or to search the hypertext for nodes that had previously been highlighted.</p>

<h2>Hypertext Engines</h2>

<p>Many hypertext systems are really hypertext engines that can display many different hypertext documents. Other hypertext systems are built specifically to display a single document and can therefore provide a much richer interaction with respect to the content of that particular document.</p>

<p>Besides the obvious advantage of not having to program a new application, the use of hypertext engines also has the advantage that they provide a user interface common to many documents. Users who already know how to use Guide, for example, can immediately start to read the next Guide document without any training.</p>

<p>Some hypertext systems like Guide and Hyperties are truly plain engines. The author just pours text into them and they take care of everything else. For example, a pop-up window in Guide always appears in the top right corner of the screen. The author does not have to make any user interface decisions except for a few low-level formatting details such as where to break paragraphs. Considering that most people are poor user interface designers, this may well be an advantage.</p>

<p>Other hypertext engines allow the hypertext designer to customize the user interface to a document within a certain framework. HyperCard is a prime example of such a system since it allows the designer to move fields around and add all sorts of background graphics. Even so, the designer is constrained by the basic HyperCard framework of being a frame-based system with a fixed size monochrome card. There are certain user interface facilities available in a kind of construction kit for the designer, but it is not possible to add new interaction techniques.</p>

<p>Actually it is possible to extend the framework of HyperCard, but only by leaving its built-in HyperTalk programming language behind and programming so-called external commands (XCMDs) in a traditional programming language like C. NoteCards is extensible because it is integrated with InterLisp, but it also provides several simpler possibilities for hypertext designers to customize their interfaces to the needs of their individual documents [Trigg et al. 1987].</p>

<p>HyperCard not only allows hypertext authors to customize the user interface of their hypertext documents, it requires them to do so. HyperCard has no default document design but in principle presents the author with a blank screen where it is necessary to define the placement of text fields before anything can be written.</p>

<p>Finally, some hypertext documents are implemented as specialized applications. These include the Drexel Disk (see Figure 1.3) and Palenque (see Chapter 4). These specialized applications can achieve an exact match between the hypertext system and the needs of the document. For example, for the children exploring the Mexican jungle, Palenque has special features in the form of a filmed television personality who pops up from time to time to introduce new discoveries. The designers of Palenque added this guide character to the interface after having interviewed children about how they would like to explore Maya ruins in the Mexican jungle. And they certainly did not want to do so alone.</p>

<h2>Open Hypertext</h2>

<p>Most hypertext researchers work with systems that are specifically designed as hypertext systems. Most <em> users </em> , on the other hand, work with systems that assist them in performing all kinds of tasks, from simple spreadsheet calculations to complex domain-specific work like analysis of seismic data from oil exploration sites. The conflict between these two perspectives on the world will definitely never be resolved by having the majority of users convert to using a hypertext tool and give up on their other software tools. A more promising approach is called open hypertext and is aimed at integrating hypertext capabilities with the rest of the user's software environment. The key notion is to let each application work with the data it is optimized for and have some way for the applications to communicate with a shared hypertext mechanism that handles the links across applications (and possibly even within applications).</p>

<p>Engelbart [1990] defines three levels of interoperability for open hypertext: interoperability in an individual's information space (e.g., linking from a phone list to a set of notes), interoperability in a group's information space (e.g., from one person's file space to a colleague's), and interoperability across groups (e.g., linking from the marketing organization to the manufacturing or product development organizations). The latter case is an example of interoperability across knowledge domains since the different groups are likely to use very different applications for their specialized work.</p>

<p>This section will mostly concern the first level of interoperability: the ability to link between information objects in different applications. The remaining two levels are in the nature of computer-supported cooperative work and involve networking and groupware software to a larger extent than they involve hypertext as such. Interoperability across group and companies can partly be supported by the World Wide Web and other Internet mechanisms discussed further in Chapter 7.</p>

<p>The typical method for supplying open hypertext is to remove the hypertext level from the applications and provide a single hypertext service for everything on the user's computer. The hypertext service will need to keep track of what information is moved where, and it can then communicate with the applications using message passing to indicate, for example, that they should open a certain document and scroll it to a certain location. The link information is typically stored in a database that is separate from the primary documents and is maintained by the hypertext service rather than the user's applications. The reasons for this separation is that the link information may exist in formats that are incompatible with some of the applications and that one does not want the user to have to see these internal data structures.</p>

<p>The Microcosm project [Davis et al. 1992, 1994; Hill and Hall 1994] defined three degrees of application-support for open hypertext: fully aware, partly aware, and unaware applications, where the term "aware" refers to the extent to which the application knows about hypertext and the possibility of links between its data and other applications. Figure 5.6 shows fully Microcosm-aware applications and the way Microcosm supports links between, say, a graphics application and a word processor. When a fully aware application opens a document, it sends a message to Microcosm to find out whether the document contains any links, and if so, it can display the anchors as buttons and make the user interface more appealing and intuitive to use. When the user activates an anchor, the fully aware application will know that a hypertext link should be followed, and it can ask the Microcosm service to do so. As the user edits the information in the source document, a fully aware application can inform Microcosm if link anchors have been added, moved, or deleted, and Microcosm can thus get the necessary information to maintain the link database.</p>

<p><img alt="Screenshot of Microcosm viewers" src="http://media.nngroup.com/media/editor/2012/10/31/fully_aware_microcosm.png" style="width: 781px; height: 448px;"/><br/>
<strong>Figure 5.6. </strong> <em> Fully aware Microcosm viewers. The image viewer shows part of a city map with super-imposed "buttons" on top of the bitmap data for display purposes. The text viewer is based on RTF (rich text format). These viewers have been launched as a result of the user selecting items from the available links box, which is showing links available from the source anchor "Abbey Green." Copyright © 1994 by the Microcosm Group, University of Southampton, U.K., reprinted by permission. </em></p>

<p>The main problem with fully aware applications is that they have to be customized for the particular open hypertext model employed by the user. In this case, Microcosm was used, but several other open hypertext systems exist (and more can be expected in the future), so it is not realistic to expect all applications to be modified to work with all hypertext services.</p>

<p><img alt="Screenshot of Microcosm in Microsoft Calendar" src="http://media.nngroup.com/media/editor/2012/10/31/microcosm_overlay.png" style="width: 426px; height: 277px;"/><br/>
<strong>Figure 5.7. </strong> <em> The Microcosm Universal Viewer running on top of the Microsoft Calendar program which is completely unaware of Microcosm. The Universal Viewer acts as a shim between Microcosm and unaware applications, displaying the menus and any relevant buttons on the application's title bar, and allows the user to make selections within the application for making or following links. Copyright © 1994 by the Microcosm Group, University of Southampton, U.K., reprinted by permission. </em></p>

<p>Partly aware viewers are those that have some kind of programmability that allows them to be extended with an action menu with the commands needed to interact with the hypertext service. Many popular applications, including Microsoft Word and most spreadsheet packages, come with a scripting facility (e.g., Visual Basic) that can be used for this purpose. The partly aware viewers typically do not allow anchors to be marked as buttons since they control their own user interface and the way they want to show their data on the screen. Thus, users are relegated to selecting objects in the application using its normal selection mechanism and then issuing hypertext commands such as "Follow Link" from the Microcosm menu.</p>

<p>In an unaware application, the hypertext commands cannot even be represented as menu commands but must be supplied through a modification of the window system or some other level of the underlying operating system. Figure 5.7 shows how the Microcosm Universal Viewer can attach itself to the title bar of a window and thus be accessed in the context of the application running in that window even though the application knows nothing about Microcosm. When the Microcosm service is activated through the menu on the titlebar, it reads the current selection in the window and compares it with its database of anchors or other hypertext objects, after which Microcosm can take the appropriate action (e.g., open another window with the destination of a link, even if that destination is in another application).</p>

<p>Applications that are partly aware or unaware of the hypertext service will not know to inform the hypertext service as the user edits information that may contain hypertext links. Therefore, the hypertext service will need some other mechanism to update its link database to ensure link integrity. This problem cannot be solved fully in all cases since users may have modified the document too much to allow the hypertext anchors to be located. Remember that the document itself is typically not allowed to contain any anchor markup in order to keep it compatible with the original application datastructures. The typical solution to the problem of the moving anchors is to remember the location of the anchor before the document was edited and then to search for a location as close as possible with the same text (or other data) as the anchor. Of course, if the user edited the anchor itself or if it was moved too much, then this approach will fail.</p>

<h2>Integrating Hypertext Ideas into Other Environments</h2>

<p>A final architectural observation is that hypertext does not really need to involve a full-fledged set of features. It is possible to utilize a smaller number of ideas from hypertext and integrate them into other computer systems without making them full hypertext systems [Frisse and Cousins 1992].</p>

<p><img alt="Screenshot of HyperView menu" src="http://media.nngroup.com/media/editor/2012/10/31/hyperview_pulldown_menu.png" style="width: 494px; height: 193px;"/><br/>
<strong>Figure 5.8. </strong> <em> A HyperView pop-up menu from the statistics program Data Desk Professional. The user is looking at a table containing an analysis of variance and has indicated a special interest in one of the variables. The pop-up menu now allows the user to open windows with additional statistical analyses and graphs that are especially relevant given this context. </em></p>

<p>For example, the statistics package Data Desk Professional from Odesta has a facility called HyperView which is shown in Figure 5.8. HyperViews allow the user direct access from one statistical analysis to a small number of other analyses that are relevant given the user's current context. Based on its knowledge of statistics, the program "knows" what other statistical analyses people normally want if they have an interest in the selected variable in the context given by the existing table.</p>

<p>The result of making a choice from the menu in Figure 5.8 is to jump to a new window containing the desired analysis or graph. Of course the system has to calculate the content of that window first so the real result is just to activate the statistics package with a given command and a given set of parameters. But to the user it <em> feels </em> like a hypertext-like navigation between connected windows. It is also a great practical advantage of the HyperView facility that it reduces the need for the user to find the correct command among the large set of statistical analyses available in the program and that it automatically specifies the correct parameters.</p>
</section>
  

  
  <section>
    
        
    
  </section>
  
  
  
  
  

  


  <section>
    <br />
    <p class="share"><b>Share <span class="hide-for-small-down">this article</span>:</b> <a href="https://twitter.com/intent/tweet?url=http://www.nngroup.com/articles/architectural-component-hypertext-systems/&amp;text=Architectural%20Component%20of%20Hypertext%20Systems&amp;via=nngroup">Twitter</a> | <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=http://www.nngroup.com/articles/architectural-component-hypertext-systems/&amp;title=Architectural%20Component%20of%20Hypertext%20Systems&amp;source=Nielsen%20Norman%20Group">LinkedIn</a> | <a href="https://plus.google.com/share?url=http://www.nngroup.com/articles/architectural-component-hypertext-systems/">Google+</a> | <a href="mailto:?subject=NN/g Article: Architectural Component of Hypertext Systems&amp;body=http://www.nngroup.com/articles/architectural-component-hypertext-systems/">Email</a></p>
    <hr />
  </section>

  <footer class="l-subsection">
    
    
    <div class="learn-more">
      <h2>Learn More</h2>
      <p><a href="../subscribe/index.php">Subscribe to the weekly newsletter</a> to get notified about future articles.</p>
      </div>
    
    

    <div class="row show-for-small-down related-content">
      
      
      

      
      
        <div class="column small-12 topic-links">All articles about:
          <ul>
            
            <li><a href="../../topic/hypertext/index.php">hypertext</a></li>
            
            <li><a href="../../topic/navigation/index.php">Navigation</a></li>
            
            <li><a href="../../topic/technology/index.php">new technologies</a></li>
            
          </ul>
        </div>
      
      

      
      
        <div class="column small-12">
          <h3>UX Conference Training Courses</h3>
           <ul class="no-bullet">
    
        <li><a href="../../courses/emerging-patterns-web-design/index.php">Emerging Patterns for Web Design</a></li>
    
        <li><a href="../../courses/information-architecture/index.php">Information Architecture</a></li>
    
        <li><a href="../../courses/navigation-design/index.php">Navigation Design</a></li>
    
</ul>
        </div>
      
      

      
      
        <div class="small-12 column">
          <h3>Research Reports</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../reports/intranet-navigation-layout-and-text/index.php">Vol. 07: Navigation and Page Layout</a></li>
              
            
              
                <li><a href="../../reports/intranet-information-architecture-design-methods/index.php">Intranet Information Architecture Design Methods and Case Studies</a></li>
              
            
              
                <li><a href="../../reports/university/index.php">University Websites</a></li>
              
            
          </ul>
        </div>
      
      

      
      
        <div class="small-12 column">
          <h3>Online Seminars</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../online-seminars/intranet-users-find/index.php">Helping Intranet Users Find What They Need</a></li>
              
            
              
                <li><a href="../../online-seminars/beyond-screens/index.php">Beyond Screens</a></li>
              
            
          </ul>
        </div>
      
      
    </div>

    <div class="hide-for-small-down row related-content">
      <div class="medium-6 columns">
        
        
          <div>
            <h3>Articles</h3>
            <ul class="no-bullet">
              
                
                <li><a href="../top-10-enduring/index.php">Top 10 Enduring Web-Design Mistakes</a></li>
                
              
                
                <li><a href="../visual-indicators-differentiators/index.php">Visual Indicators to Differentiate Items in a List</a></li>
                
              
                
                <li><a href="../tabs-used-right/index.php">Tabs, Used Right</a></li>
                
              
                
                <li><a href="../menu-design/index.php">Menu Design: Checklist of 15 UX Guidelines to Help Users </a></li>
                
              
                
                <li><a href="../trip-report-hypertext-2/index.php">Hypertext&#39;2 Trip Report</a></li>
                
              
            </ul>
          </div>
        
        

        
          
          <div>
            <h3>Research Reports</h3>
            <ul class="no-bullet">
              
                
                  <li><a href="../../reports/intranet-navigation-layout-and-text/index.php">Vol. 07: Navigation and Page Layout</a></li>
                
              
                
                  <li><a href="../../reports/intranet-information-architecture-design-methods/index.php">Intranet Information Architecture Design Methods and Case Studies</a></li>
                
              
                
                  <li><a href="../../reports/university/index.php">University Websites</a></li>
                
              
            </ul>
          </div>
          
        

      </div>
      <div class="medium-6 columns">
        
          
          <div>
            <h3>UX Conference Training Courses</h3>
             <ul class="no-bullet">
    
        <li><a href="../../courses/emerging-patterns-web-design/index.php">Emerging Patterns for Web Design</a></li>
    
        <li><a href="../../courses/information-architecture/index.php">Information Architecture</a></li>
    
        <li><a href="../../courses/navigation-design/index.php">Navigation Design</a></li>
    
</ul>
          </div>
          
        
        
          
          <div>
            <h3>Online Seminars</h3>
            <ul class="no-bullet">
            
                <li><a href="../../online-seminars/intranet-users-find/index.php">Helping Intranet Users Find What They Need</a></li>
            
                <li><a href="../../online-seminars/beyond-screens/index.php">Beyond Screens</a></li>
            
            </ul>
          </div>
          
        
      </div>
    </div>
  </footer>
</article>

        </div>

      

    
  </div>
  <div class="row">
    
      <div class="column small-12 show-for-small-down l-navrail announcements">
        
          
        
      </div>
    
    
  </div>

    </div>
    




<footer id="page-footer">
    <div class="row hide-for-print">
        <div class="column small-12 medium-6">
            <section class="footer-popular">
                
                <h1>Popular Topics</h1>
                <div style="float: left;  width: 15em; display: block;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/applications/index.php">Application Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/e-commerce/index.php">E-Commerce Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/eyetracking/index.php">Eyetracking </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/information-architecture/index.php">Information Architecture </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/intranets/index.php">Intranet Design </a></li>
	<li><a href="../../topic/interaction-design/index.php">Interaction Design </a></li>
</ul>
</div>

<div style="float: left;  width: 15em; display: block; list-style: none;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/mobile-and-tablet-design/index.php">Mobile and Tablet Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/credibility-persuasion/index.php">Persuasive Design</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/research-methods/index.php">Research Methods</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/user-testing/index.php">User Testing </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/web-usability/index.php">Web Usability </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/writing-web/index.php">Web Writing &amp; Content Strategy </a></li>
</ul>
</div>

<p>&nbsp;</p>

                
            </section>
        </div>

        <div class="column small-12 medium-6">
            <div class="row">
                <div class="column small-6">
                    <section class="footer-about">
                        <h1>Follow Us</h1>
                        <ul>
	<li><a href="../subscribe/index.php">Subscribe to our weekly UX newsletter</a>,&nbsp;Jakob Nielsen&#39;s Alertbox</li>
	<li>Visit Don Norman&#39;s website, <a href="http://www.jnd.org/"> www.jnd.org </a></li>
	<li>Visit Bruce Tognazzini&#39;s website, <a href="http://www.asktog.com/"> www.asktog.com </a></li>
	<li style="padding-top: 6px;"><a class="twitter-follow-button" data-show-count="false" data-size="large" href="https://twitter.com/nngroup"><img alt="Follow @nngroup on Twitter" height="20" src="https://media.nngroup.com/media/editor/2016/12/30/twitter-follow-button.png" style="width: 117px; height: 20px;" width="117" /></a></li>
	<li style="padding-top: 6px;"><img alt="" height="14" src="https://media.nngroup.com/media/editor/2016/07/26/feed-icon-14x14.png" style="vertical-align:text-top;width:14px;margin-right:.25em;" width="14" /><a href="../../feed/rss/index.php">Subscribe to our RSS feed</a></li>
</ul>

                    </section>
                </div>

                <div class="column small-6">
                    <section class="footer-about right-column">
                        <h1>About
</h1>
                        <ul class="no-bullet">
	<li><a href="../../about/contact/index.php">Contact information</a></li>
	<li><a href="../../about/index.php">About Us</a></li>
	<li><a href="../../about/why-nng/index.php">Why NN/g? </a></li>
	<li><a href="../../return-policy/index.php">Return Policy</a></li>
</ul>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="copyright column small-12">
            <p><a href="../../copyright-and-reprint-info/index.php">Copyright</a> &copy; 1998-2017 Nielsen Norman Group, All Rights Reserved.</p>
        </div>
    </div>
</footer>


    


        <script src="https://media.nngroup.com/static/vendor/fastclick/js/fastclick.min.js"></script>
        <script src="https://media.nngroup.com/static/vendor/foundation/js/foundation.min.js"></script>
        <script src="http://media.nngroup.com/static/js/app.js"></script>
        <script src="http://media.nngroup.com/static/js/scroll-to-fixed.js"></script><script>var externalLinks = document.querySelectorAll('a[href^="http"]');for (var i = externalLinks.length-1; i >= 0; i--) {externalLinks[i].addEventListener("click", function(event) { alert("External links are disabled for this test."); event.preventDefault(); }, false);}var mailLinks = document.querySelectorAll('a[href^="mailto"]');for (var i = mailLinks.length-1; i >= 0; i--) {mailLinks[i].addEventListener("click", function(event) { alert("Email links are disabled for this test."); event.preventDefault(); }, false);}var externalForms = document.querySelectorAll('form');for (var i = externalForms.length-1; i >= 0; i--) {externalForms[i].addEventListener("submit", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false); externalForms[i].addEventListener("click", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false);}</script>

        <script>
            
            $(function() {
                $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
                $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
            });
        </script>

        
<script src="https://media.nngroup.com/static/js/articles.js"></script>


        
        <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1072699960;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072699960/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
    </body>

<!-- Mirrored from www.nngroup.com/articles/architectural-component-hypertext-systems/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:13:20 GMT -->
</html>
