<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->





    
<!-- Mirrored from www.nngroup.com/articles/intranet-design-2012/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:13:13 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1026.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"licenseKey":"1a30ef9931","errorBeacon":"bam.nr-data.net","applicationID":"2048992","transactionName":"NFxSMRBUX0EDUkIPDA0WdhAMVkVbDV8ZBxEXUFMJB0YfRAtUQRVZIktEDAFZVHYHRVcPD01eVRE=","beacon":"bam.nr-data.net","queueTime":2,"applicationTime":434,"agent":""}</script>
        <title>10 Best Intranets of 2012</title><meta property="og:title" content="10 Best Intranets of 2012" />
  
        
        <meta name="description" content="Social networking and personalization rise to higher levels this year, while mobile intranets continue to cut their teeth. Also, smaller organizations get larger teams and better designs.">
        <meta property="og:description" content="Social networking and personalization rise to higher levels this year, while mobile intranets continue to cut their teeth. Also, smaller organizations get larger teams and better designs." />
        
  
        
	
        
        <meta name="keywords" content="Intranet Design Annual 2012, Intranet Design Annual, intranet usability, intranet UX, intranet user experience, intranet design, design awards, 2012, CenturyLink Business, Everything Everywhere, Genentech, LivePerson, Logica, MAN Diesel &amp; Turbo SE, NCR Corporation, Scotts Miracle-Gro Company, Skanska, Staples, Germany, Sweden, UK, German intranets, Swedish intranets, British intranets, social networking, personalization, mobile, mobile intranet design, mobile intranets, intranet team size, intranet budgets, intranet governance, ROI">
        

        

        <meta name="robots" content="noarchive">
        <meta name="viewport" content="width=device-width">

        <link rel="search" type="application/opensearchdescription+xml" title="NN/g Search" href="../../opensearch.xml" />
        <link rel="shortcut icon" href="http://media.nngroup.com/static/img/favicon.ico" />

        <link rel="stylesheet" href="https://media.nngroup.com/static/vendor/font-awesome/css/font-awesome.min.css">

        
        <link rel="stylesheet" href="https://media.nngroup.com/static/css/7232a960f1df.css" type="text/css" />
        <link rel="stylesheet" href="http://fast.fonts.com/t/1.css?apiType=css&amp;projectid=c5846260-9def-46ec-9407-0f1349bd2d64" type="text/css">

        <script src="https://media.nngroup.com/static/vendor/modernizr/js/modernizr.min.js"></script>

        <script type="text/javascript" src="https://media.nngroup.com/static/js/3283be5def74.js"></script>
        

        

        
        
        <!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=229034,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('http://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->


<!-- Google Analytics Tracking Code for http://nngroup.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23132681-1', 'nngroup.com');
  ga('require', 'linkid', 'https://www.nngroup.com/articles/intranet-design-2012/linkid.js');
  ga('require', 'displayfeatures');

// Optimizely Universal Analytics Integration
 window.optimizely = window.optimizely || [];
 window.optimizely.push("activateUniversalAnalytics");

  ga('send', 'pageview');

</script>
<script>
/**
* Function to track a click on an outbound link in Google Analytics.
*/
function trackOutboundLink(label, url) {
    if(window.ga && ga.loaded) {
        ga('send', 'event', 'outbound', 'click', label, {'hitCallback': function(){document.location = url;}
        });
    } else {
        document.location = url;
    }
}
/**
* Function to track a click on a link in the article announcement area.
*/
$('#articleAnnouncement a').on('click', function(){
  ga('send', 'event', 'link', 'click', 'articleAnnouncement');
});
</script>

<script type="text/javascript">
 setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2421.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



<!-- Hotjar Tracking Code for http://nngroup.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:121605,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

        <script src="https://media.nngroup.com/static/js/analytics/track_outbound_link.js"></script>
        
        
        
    </head>
    <body class="location-articles articles-detail">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
    <header id="page-header">
        <div id="cart" class="shopping-cart mobile-cart show-for-small-down"></div>
        <div class="row">
            <div id="masthead" class="masthead small-12 medium-7 column">

                <!-- The button to toggles menu on small screen -->

                <h1><a href="../../index.php"><img src="http://media.nngroup.com/static/img/logo-wordmark.png" width="74" height="30"> Nielsen <span class="norman">Norman</span> Group</a></h1>
                <p class="tagline show-for-medium-up">Evidence-Based User Experience Research, Training, and Consulting</p>

                <!-- The button to toggles form search on small screen -->


                <div style="clear: right;width:  100%;height: 1px;"></div><?php define('ROOT_PATH',$_SERVER["DOCUMENT_ROOT"] . '/apps/navigation_styles/navigations/2'); ?><?php include(ROOT_PATH)."/navigation.php"; ?>

                <!-- Drop down menu on small screen -->
                <div id="hideNav" class="show-for-small-down">
                  
                  <ul class="menu-content">
                    <li><a href="../../index.php">Home</a></li>
                    <li class="has-submenu">
                      <a>Training</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../training/index.php">Overview</a></li>
                        
<li id="nav-ux-conference"><a href="../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../online-seminars/index.php"><span>Online Seminars</span></a></li>

                      </ul>
                    </li>
                    <li class="has-submenu">
                      <a>Consulting</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
  <li><a href="../../consulting/index.php">Overview</a></li>
  
  <li><a href="../../consulting/benchmarking/index.php">Benchmarking</a></li>
  
  <li><a href="../../consulting/expert-design-review/index.php">Expert Design Review</a></li>
  
  <li><a href="../../consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
  
  <li><a href="../../consulting/usability-testing/index.php">Usability Testing </a></li>
  
  <li><a href="../../consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
  
  <li><a href="../../in-house-training/index.php">In-House Training</a></li>
</ul>

                    </li>

                    <li><a href="../../reports/index.php">Reports</a></li>
                    <li><a href="../index.php">Articles</a></li>
                    <li class="has-submenu"><a>About NN/G</a><i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../about/index.php">Overview</a></li>
                        <li><a href="../../people/index.php">People</a></li>
                        <li><a href="../../about/why-nng/index.php">Why NN/g?</a></li>
                        <li><a href="../../about/contact/index.php">Contact</a></li>
                        <li><a href="../../news/index.php">News</a></li>
                        <li><a href="../../about/history/index.php">History</a></li>
                        <li><a href="../../books/index.php">Books</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                </div>

                <hr class="header-divider show-for-small-only"/>


            </div>
       </div>

        <div id="hideNav">            <nav class="primary-nav small-12 column">
                <ul>
                    <li><a class="navitem" id="nav-home" href="../../index.php">Home</a></li>
                    <li><a class="navitem" id="nav-training" href="../../training/index.php">Training</a></li>
                    <li><a class="navitem" id="nav-consulting" href="../../consulting/index.php">Consulting</a></li>
                    <li><a class="navitem" id="nav-reports" href="../../reports/index.php">Reports</a></li>
                    <li><a class="navitem" id="nav-articles" href="../index.php">Articles</a></li>
                    <li><a class="navitem" id="nav-about" href="../../about/index.php">About NN/G</a></li>
                </ul>
            </nav>
            <nav class="secondary-nav small-12 medium-12 large-12 columns">
                
                
            </nav>
        </div>
    </header>
    <div id="main" class="l-content" onclick="closeNav(event)"><span id="preventClose" style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776; Menu</span>
    
  

  

  <div class="row">
    
      
      <section class="small-12 medium-3 columns collapsable-content l-navrail articles-left-column">
        <h1 class="show-for-small-down collapse-link mobile-browse-header">Browse by Topic and Author</h1>
          <div class="row collapsable">

            
            <div class="small-6 medium-12 columns">
              <section id="article-popular-topics" style="display: none;">
                
                <h1>Topics</h1>
                <ul id="article-popular-topics">
                    
                    <li>
                        
                        <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/intranets/all/index.php">Intranets</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/user-testing/all/index.php">User Testing</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                        
                    </li>
                    
                    <li><a href="#" id="article-see-all-topics"><strong>See all topics&hellip;</strong></a></li>
                </ul>
                
              </section>
              <section id="article-all-topics" style="display: none;">
                
                <h1>
                  <span class="show-for-medium-up">All Article Topics</span>
                  <span class="show-for-small-down">All Topics</span>
                  (<a href="#" id="article-hide-all-topics">hide</a>)
                </h1>
                <ul>
                  
                  <li>
                    
                    <a href="../../topic/accessibility/all/index.php">Accessibility</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/agile/all/index.php">Agile</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/analytics-and-metrics/all/index.php">Analytics &amp; Metrics</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/applications/all/index.php">Application Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/b2b-websites/all/index.php">B2B Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/behavior-patterns/all/index.php">Behavior Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/branding/all/index.php">Branding</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/content-strategy/all/index.php">Content Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/corporate-websites/all/index.php">Corporate Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/design-patterns/all/index.php">Design Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-design-process/all/index.php">Design Process</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/email/all/index.php">Email</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/eyetracking/all/index.php">Eyetracking</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/heuristic-evaluation/all/index.php">Heuristic Evaluation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/human-computer-interaction/all/index.php">Human Computer Interaction</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-ideation/all/index.php">Ideation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/information-architecture/all/index.php">Information Architecture</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/interaction-design/all/index.php">Interaction Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/international-users/all/index.php">International Users</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/intranets/all/index.php">Intranets</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-management/all/index.php">Management</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/navigation/all/index.php">Navigation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/non-profit-websites/all/index.php">Non-Profit Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/personas/all/index.php">Personas</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/persuasive-design/all/index.php">Persuasive Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/prototyping/all/index.php">Prototyping</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/psychology-and-ux/all/index.php">Psychology and UX</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/research-methods/all/index.php">Research Methods</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/search/all/index.php">Search</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/social-media/all/index.php">Social Media</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/strategy/all/index.php">Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/user-testing/all/index.php">User Testing</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-teams/all/index.php">UX Teams</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/visual-design/all/index.php">Visual Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/writing-web/all/index.php">Writing for the Web</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/young-users/all/index.php">Young Users</a>
                    
                  </li>
                  
                </ul>
                
              </section>
            </div>
            <script>
              /* Determine which topics list will be displayed: the full or the
               * partial.  We are inlining this close to the list itself to
               * prevent rendering delays on the client
               */
              var topic_list = $.cookie("article-topics");

              var popular_topics = $('#article-popular-topics');
              var all_topics = $('#article-all-topics');

              if(topic_list == 1) {
                  popular_topics.hide();
                  all_topics.show();
              } else {
                  popular_topics.show();
                  all_topics.hide();
              }
            </script>
            

            
            <div class="small-6 medium-12 columns">
              
              <section id="article-initial-authors">
                  <h1>Author</h1>
                  <ul>
                    
                    <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                    
                    <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                    
                    <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                    
                    <li><a href="#" onClick="$('#article-initial-authors').hide(); $('#article-all-authors').show();"><strong>See all authors&hellip;</strong></a></li>
                  </ul>
              </section>
              <section id="article-all-authors" style="display: none;">
                <h1>
                  All Authors
                  (<a href="#" id="article-hide-all-authors">hide</a>)
                </h1>
                <ul>
                  
                  <li><a href="../author/raluca-budiu/index.php">Raluca Budiu</a></li>
                  
                  <li><a href="../author/patty-caya/index.php">Patty Caya</a></li>
                  
                  <li><a href="../author/susan-farrell/index.php">Susan Farrell</a></li>
                  
                  <li><a href="../author/therese-fessenden/index.php">Therese Fessenden</a></li>
                  
                  <li><a href="../author/kim-flaherty/index.php">Kim Flaherty</a></li>
                  
                  <li><a href="../author/sarah-gibbons/index.php">Sarah Gibbons</a></li>
                  
                  <li><a href="../author/aurora-harley/index.php">Aurora Harley</a></li>
                  
                  <li><a href="../author/kate-kaplan/index.php">Kate Kaplan</a></li>
                  
                  <li><a href="../author/page-laubheimer/index.php">Page Laubheimer</a></li>
                  
                  <li><a href="../author/angela-li/index.php">Angie Li</a></li>
                  
                  <li><a href="../author/hoa-loranger/index.php">Hoa Loranger</a></li>
                  
                  <li><a href="../author/kate-meyer/index.php">Kate Meyer</a></li>
                  
                  <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                  
                  <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                  
                  <li><a href="../author/kara-pernice/index.php">Kara Pernice</a></li>
                  
                  <li><a href="../author/christian-rohrer/index.php">Christian Rohrer</a></li>
                  
                  <li><a href="../author/amy-schade/index.php">Amy Schade</a></li>
                  
                  <li><a href="../author/katie-sherwin/index.php">Katie Sherwin</a></li>
                  
                  <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                  
                  <li><a href="../author/kathryn-whitenton/index.php">Kathryn Whitenton</a></li>
                  
                </ul>
              </section>
              
            </div>
            
          </div>

          
          <section class="hide-for-small-down">
            
              
              <h1>Recent Articles</h1>
              
              <ul>
                  
                  <li><a href="../seamless-cross-channel/index.php">Seamlessness in the Omnichannel User Experience</a></li>
                  
                  <li><a href="../brand-intention-interpretation/index.php">Experience Design: Bridging Brand Intention and Brand Interpretation</a></li>
                  
                  <li><a href="../eyetracking-tasks-efficient-scanning/index.php">Scanning Patterns on the Web Are Optimized for the Current Task</a></li>
                  
                  <li><a href="../flat-design-best-practices/index.php">Flat-Design Best Practices</a></li>
                  
                  <li><a href="../ux-user-stories/index.php">Accounting for UX Work with User Stories in Agile Projects</a></li>
                  
                  <li><a href="../index.php"><strong>See all articles&hellip;</strong></a></li>
              </ul>
              
            
          </section>
          

          
          
          <section class="hide-for-small-down">
            <h1>Popular Articles</h1>
            
            <ul>
                
                <li><a href="../usability-101-introduction-to-usability/index.php">Usability 101: Introduction to Usability</a></li>
                
                <li><a href="../top-10-mistakes-web-design/index.php">Top 10 Mistakes in Web Design</a></li>
                
                <li><a href="../how-users-read-on-the-web/index.php">How Users Read on the Web</a></li>
                
                <li><a href="../f-shaped-pattern-reading-web-content/index.php">F-Shaped Pattern For Reading Web Content</a></li>
                
                <li><a href="../ten-usability-heuristics/index.php">10 Usability Heuristics for User Interface Design</a></li>
                
                <li><a href="../intranet-design-2016/index.php">10 Best Intranets of 2016</a></li>
                
                <li><a href="../which-ux-research-methods/index.php">When to Use Which User-Experience Research Methods</a></li>
                
                <li><a href="../response-times-3-important-limits/index.php">Response Times: The 3 Important Limits</a></li>
                
                <li><a href="../why-you-only-need-to-test-with-5-users/index.php">Why You Only Need to Test with 5 Users</a></li>
                
                <li><a href="../page-fold-manifesto/index.php">The Fold Manifesto: Why the Page Fold Still Matters</a></li>
                
            </ul>
          </section>
          
          

          
            <section class="hide-for-small-down">
              
            </section>
          

      </section>
      

      
        <div class="small-12 medium-9 columns content-body">
          




<article class="l-subsection alertbox-article-content article-detail-bl">
  
  <header>
    <div class="l-subsection">
      <h1>10 Best Intranets of 2012</h1>
      <div class="author-meta">by
        
          
            
            <a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a>
            
            and
            
            <a href="../author/kara-pernice/index.php">Kara Pernice</a>
            
          
        on  January 3, 2012
        
      </div>

      

      
      <div class="topic-links">
        Topics:
        
          <ul>

  <li><a href="../../topic/intranets/index.php">Intranets</a></li>

</ul>
        
      </div>
      
    </div>
  </header>
  

  
  
  <section>
    <p class="lede"><strong>Summary:</strong> Social networking and personalization rise to higher levels this year, while mobile intranets continue to cut their teeth. Also, smaller organizations get larger teams and better designs.</p>
    <br />
  </section>
  
  

  
  <section id="article-body"><p>
	The 10 <strong> best-designed intranets for 2012 </strong> are:</p>
<ul>
	<li>
		CenturyLink Business, a telecommunications company (US)</li>
	<li>
		Everything Everywhere, a communications company (UK)</li>
	<li>
		Genentech, a biotechnology company (US)</li>
	<li>
		LivePerson, Inc., a communications company (US)</li>
	<li>
		Logica, a business and technology service company (UK)</li>
	<li>
		MAN Diesel &amp; Turbo SE, provider of large-bore diesel engines and turbomachinery for marine and stationary applications (Germany)</li>
	<li>
		NCR Corporation, a technology company (US)</li>
	<li>
		The Scotts Miracle-Gro Company, a marketer of branded consumer products for lawn and garden care (US)</li>
	<li>
		Skanska, a project development and construction group (Sweden)</li>
	<li>
		Staples, Inc., an office products company (US)</li>
</ul>
<p>
	Although most of this year's intranets support entire organizations, CenturyLink's site is a <strong> specialized intranet </strong> expressly targeted toward the company's Business Markets group.</p>
<p>
	In this, our 12<sup>th </sup> Intranet Design Annual, Staples — which also won in <a class="old" href="../ten-best-intranets-of-2006/index.php" title="Alertbox: 2006 Intranet Design Annual"> 2006</a> — joins an elite group of 7 two-time winners (out of 120 total). Among those twice-honored companies are Cisco Systems (<a class="old" href="../the-10-best-intranet-designs-of-2001/index.php" title="Alertbox: 2001 Intranet Design Annual">2001</a>, <a class="old" href="../10-best-intranets-of-2005/index.php" title="Alertbox: 2005 Intranet Design Annual"> 2005</a>), Credit Suisse (<a class="old" href="../10-best-intranets-of-2002/index.php" title="Alertbox: 2002 Intranet Design Annual">2002</a>, <a class="old" href="../10-best-intranets-of-2011/index.php" title="Alertbox: 2011 Intranet Design Annual"> 2011</a>), Verizon Communications (<a class="old" href="../10-best-intranets-of-2005/index.php" title="Alertbox: 2005 Intranet Design Annual">2005</a>, <a class="old" href="../10-best-intranets-of-2011/index.php" title="Alertbox: 2011 Intranet Design Annual"> 2011</a>) and Walmart (<a class="old" href="../10-best-intranets-of-2002/index.php" title="Alertbox: 2002 Intranet Design Annual">2002</a>, <a class="old" href="../10-best-intranets-of-2010/index.php" title="Alertbox: 2010 Intranet Design Annual"> 2010</a>). In addition, Deloitte Touche Tohmatsu's Australian member firm's intranet won in <a class="old" href="../10-best-intranets-of-2002/index.php" title="Alertbox: 2002 Intranet Design Annual"> 2002</a>, followed by its worldwide intranet in <a class="old" href="../10-best-intranets-of-2009/index.php" title="Alertbox: 2009 Intranet Design Annual"> 2009</a>. Finally, in <a class="old" href="../the-10-best-intranet-designs-of-2001/index.php" title="Alertbox: 2001 Intranet Design Annual"> 2001</a>, silverorange won our first competition and received an honorable mention in <a class="old" href="../ten-best-intranets-of-2003/index.php" title="Alertbox: 2003 Intranet Design Annual"> 2003</a>.</p>
<p>
	Kudos to Staples and the other multi-year winners for their design prowess and their organizations' continued intranet commitment. It's significant that these organizations <strong> recognize that intranet design is never "set and forget." </strong> Instead of perpetually sustaining their first winning designs, these companies continued to progress because they understood 3 important factors:</p>
<ul>
	<li>
		Their organizations are continually <strong> experiencing change</strong>.</li>
	<li>
		<strong>Monitoring content and sections </strong> as they are added or need to be added on the intranet keeps you abreast of potential problems. As page templates and information architecture are taxed, design must transform to accommodate, first with tweaks and then with greater modifications.</li>
	<li>
		A company's <strong> intranet is perceived </strong> in the context of evolving external websites and other applications that employees use. When employees switch between using the web and their company intranets, they shouldn't feel like they've gone from driving a 2012 ZL1 Camaro to a 1989 Chevy Nova with faulty brakes — that is, the intranet experience shouldn't feel slow, dangerous (due to inaccurate information), and tedious.</li>
</ul>
<p>
	As for industries at the forefront of intranet design, this year's winners represent 7 sectors:</p>
<ul>
	<li>
		Technology (3 winners)</li>
	<li>
		Utility (2 winners)</li>
	<li>
		Biotechnology (1 winner)</li>
	<li>
		Consumer packaged goods (1 winner)</li>
	<li>
		Engineering (1 winner)</li>
	<li>
		Manufacturing (1 winner)</li>
	<li>
		Retail (1 winner)</li>
</ul>
<p>
	<a class="new" href="../../reports/best-technology-sector-intranets/index.php" title="Nielsen Norman Group: Great Technology Sector Intranet Designs - 20 Case Studies of Award-Winning Intranets from Computer Companies, Independent Software Vendors (ISV), Value-Added Retailers (VAR), Technology Outsourcing Providers, Telecommunications Services, and Other High-Tech Organizations, Reprinted from the Intranet Design Annuals, 2001-2011">Technology companies</a> have made a comeback. After last year's hiatus with no high-tech winners, they claimed 30 percent of this year's spots. The tech sector now constitutes 22% of Design Annual winners since the competition launched in 2001.</p>
<p>
	Both the utility and engineering industries are establishing a design stronghold; this is the third straight year they've been represented among our winners. Making the list for the first time this year are a biotechnology company and a consumer packaged-goods company.</p>
<p>
	Given the world financial crisis, it's no surprise that there are no winners from the <a class="new" href="../../reports/best-financial-sector-intranets/index.php" title="Nielsen Norman Group: Great Financial Sector Intranet Designs - 22 Case Studies of Award-Winning Intranets from Banks, Insurance Companies, Brokerage Companies, and Other Financial Service Organizations, Reprinted from the Intranet Design Annuals, 2001-2011"> financial industry</a> this year, even after very strong showings in most previous years. Given their track record, we suspect that had more financial companies had the time and resources to spend on their intranets, the industry would have produced a winner.</p>
<p>
	Although the economies in the US and Europe are suffering, their intranet designs are not. This year, winning companies come from 4 different countries: 6 from the US, 2 from the UK, and 1 each from Germany and Sweden. None of these countries are strangers to our top-10 list. Since our first Design Annual, the US has won 63 times; the UK, 10; Germany, 7; and Sweden, 6. Although all of these scores are impressive, Sweden's record is amazing given the country's size.</p>
<h2>
	Smaller Companies Still Have Better Intranets</h2>
<p>
	Smaller organizations are designing better intranets this year, and have been for the past 3 years. Of this year's 10 winning intranets, 6 support fewer than 15,000 employees, with the smallest ones being LivePerson at 550, CenturyLink Business at 2,000, Scotts Miracle-Gro 8,000, and Genentech at 11,000. The largest winner this year is Staples with 55,000 employees (and plans to expand to 90,000 by the end of 2012).</p>
<p>
	The average number of employees in this year's winning organizations is 19,700, which is the smallest we've seen in the 12 years since our contest launched (excluding 2004's government-only focused Annual). In the past 3 years, the averages were: 37,900 in <a class="old" href="../10-best-intranets-of-2011/index.php" title="Alertbox: 2011 Intranet Design Annual"> 2011</a>, 39,100 in <a class="old" href="../10-best-intranets-of-2010/index.php" title="Alertbox: 2010 Intranet Design Annual"> 2010</a> (excluding the mammoth Walmart, with its 1.4 million store associates); and 37,500 in <a class="old" href="../10-best-intranets-of-2009/index.php" title="Alertbox: 2009 Intranet Design Annual"> 2009</a>. These were essentially the same numbers 3 years in a row, which makes it striking that this year's winners are so much smaller. Across all 12 Design Annuals, the overall average company size is 60,000 employees — which again emphasizes the smaller size of this new crop of winners.</p>
<p>
	Technology offerings might contribute to the success of our current, smaller winners: as a very long-term trend, it's getting easier to implement designs with good usability, bringing quality user experience within the reach of ever-smaller companies.</p>
<p>
	The goals, constraints, resources, and legacy systems at these small organizations varied widely, and thus their technology choices also differed. For example, LivePerson uses Jive Software, an SAS platform vendor, in 3 ways: to host the intranet, as its social business software solution, and as a content management system (CMS). In-house developers customized the systems to meet their needs. Because of the simple design, training, and a culture that encourages content contributions, an average of 52 percent of the company's employees contribute to the intranet each month.</p>
<p>
	At CenturyLink Business, the application core is a custom CMS developed in Ruby using the open source Ruby on Rails web application framework. The intranet developers — who are both in-house and from an external agency — relish the flexibility, but could do without having to depend on upgrades and patches from an open source provider (in this case, Peak Systems). This is especially true when weighing the upgrade regularity of out-of-the box vendors.</p>
<p>
	Scotts Miracle-Gro internal developers built "The Garden" on the SAP NetWeaver Portal using SAP's Web Page Composer tool, with heavy modification of both courtesy of the Scotts technical team.</p>
<p>
	Finally, one of Genentech's goals for the intranet redesign was to replace technology from the 2008 solution with technology that better matched its business goals. Unexpected dependencies (for example, upgrading component A means you must also upgrade B and C) made the process more difficult than team members banked on, but they ultimately chose Moveable Type as their CMS solution. They removed an enterprise portal (Vignette/OpenText), as well as some SSO components and several additional technologies. To meet their needs, they chose design tools such as Adobe Suite and some Apple-specific software.</p>
<h2>
	Team Size Is Up: Teams Do More with More</h2>
<p>
	Although our winning organizations have an average of about 20,000 employees, the average intranet team size grew to 15 people — slightly less than <strong> 1 intranet specialist per thousand employees. </strong> The smallest teams consisted of 6 people, at both Everything Everywhere (15,000 employees) and LivePerson Inc. (550 employees). The largest team was 26 people at NCR Corporation (21,000 employees). This might seem larger than it actually is because, when counting intranet team size, we include in-house people working full-time or part-time on the site, as well as people from outside the organization (such as consultants and agency staff).</p>
<p>
	Team size as a percentage of company size is more than double what it was in 2011, reaching an all-time high of 0.074%. More strikingly, <strong> intranet team sizes (as a proportion of organization sizes) are now 6 times what they were in 2001. </strong></p>
<p>
	Note, however, that 8 of the 10 winning teams this year worked together with outside agencies or consultants on their redesign projects, so this part of the team is not likely to be a sustainable resource. Still, a combination of both internal and external developers and designers is an unbeatable recipe for redesign projects, as internal teams have better knowledge about the organization, its employees, and its business needs, while external teams often have more and varied experience with resources, technology, and current design trends and skills. In earlier years of the Design Annual, we often saw external firms designing the intranets almost exclusively, but this trend is now just a memory. Today, it's far more common for internal teams to couple with external agencies. In fact, the number of winners that combine internal and external team members has steadily increased in recent years: 3 in 2008; 6 in 2009; 7 in 2010; and 8 in both 2011 and 2012.</p>
<h2>
	Mobile Intranets: Stunted Growth</h2>
<p>
	In <a class="old" href="../10-best-intranets-of-2009/index.php" title="Alertbox: 2009 Intranet Design Annual"> 2009</a>, we began seeing more good mobile intranet designs, with 30 percent of winning intranets having a mobile version. In <a class="old" href="../10-best-intranets-of-2011/index.php" title="Alertbox: 2011 Intranet Design Annual"> 2011</a>, the mobile space looked even more promising as this number doubled to 60 percent. But alas, this year the number dropped to 10 percent; only Genentech offered mobile representation in its series of iOS apps (Apple is the dominant platform internally). Both Logica and Scotts Miracle-Gro offer mobile intranet access to employees on the network, but neither site is optimized for mobile.</p>
<p>
	There are 3 probable reasons for the lack of traction in the mobile intranet space:</p>
<ul>
	<li>
		Intranet groups still <strong> don't have the budget and resources </strong> needed to develop anything beyond the main intranet site.</li>
	<li>
		Unless the organization has one company-issued mobile device, it's <strong> difficult for intranet teams to choose which device to focus on </strong> — so, instead they design for none.</li>
	<li>
		Creating a mobile intranet version that would work on any device is one possible solution. However, as per our mobile guidelines, even a single mobile version would have to be <strong> a separate design </strong> from that of the website to be truly helpful and usable for employees. And it takes resources to create and maintain two separate applications.</li>
</ul>
<p>
	While we await more insights specifically about mobile intranets, a good first step would be to follow <a class="new" href="../../reports/mobile-website-and-application-usability/index.php" title="Nielsen Norman Group: Usability of Mobile Websites &amp; Applications - 210 Design Guidelines for Improving the User Experience of Mobile Sites and Apps"> usability guidelines for mobile websites and apps</a>. For other aspects of user experience, we usually find that intranet usability builds on top of web usability: intranet users also use websites and form many of their expectations from their experience with mainstream sites. The same is likely true for mobile design.</p>
<h2>
	Evolving UI Elements</h2>
<ul>
	<li>
		<strong>Innovative menus</strong>. Menus are the gateway to content. Like our winning teams, your team should thoroughly test nonstandard menu UIs and iterate the design before deploying the menus at your organization. Scotts Miracle-Gro, Logica, and NCR all adopted and adapted the <a class="old" href="../mega-menus-work-well/index.php" title="Alertbox: Mega Drop-Down Navigation Menus Work Well"> mega menu</a> with category sections, and even added a menu within the mega menu. LivePerson's mini-dashboard of icons in the upper left of pages is an imaginative way to enable quick access to highly important content.</li>
	<li>
		<strong>Lightboxes</strong>. Lightboxes (which descended from old-school modal dialogs) have raided today's intranet designs, appearing in most of our winning intranets. However, our winning designers selectively employ lightboxes only when they want an extremely strong focus on questions in the open dialog box and therefore wish the other potentially distracting content to fall away. (If having other content visible and clickable is important, don't use lightboxes. Even if the particular lightbox is technically modeless, users usually won't think to click off them because of the aesthetic UI.) A few years back, we named <a class="old" href="../10-best-application-uis/index.php" title="Alertbox: Application Design Showcase One"> lightboxes the interaction design technique of the year</a>; clearly, it's taken a bit of time for this idea to become pervasive.</li>
</ul>
<h2>
	Supporting Design and the Organization</h2>
<ul>
	<li>
		<strong>Decentralized to centralized</strong>. <a class="old" href="../intranet-portals/index.php" title="Alertbox: Intranet Portals - Personalization Hot, Mobile Weak, Governance Essential"> Portal</a> programs today make it easy for individuals to create communities and team spaces. Theoretically, offering an instrument to enable information sharing is beneficial; the negative byproduct, however, is that doing so encourages silos. Organizations with <strong> compartmentalized content </strong> run the risk of <strong> having duplicate, outdated, or incorrect information </strong> that's <strong> not included in the intranet-wide search or in the main IA. </strong> In other words, information is nearly impossible to find. Content in silos was a prevalent problem in the early 2000s; until recently, the issue was fading from view. Today's winning designers are getting back to tackling old school (or at least new old school) problems: Logica, NCR, and Skanska all did tremendous work in moving from decentralized intranet sites to a centralized intranet with an IA and structure that accommodate everyone. Similarly, MAN Diesel &amp; Turbo combined two companies with their design.</li>
	<li>
		<strong><em>Lorem ipsum dolor </em> must die</strong>. This year's winners were very strongly focused on content quality early on and throughout the design process. They understood that training content managers later in the process is costly and results in more time spent on fixing problems than on writing. Organizations can avoid content-related issues by prepping people upfront with training sessions, guidelines, and recommendations like our winners did. Everything Everywhere suggested its writers adopt a "newsroom mentality" and write articles of 250 words or less. Likewise, Genentech guidelines emphasized brief, clear, easy-to-follow content. Logica created a "Campaign Against Corporate Blah" to make departmental pages more meaningful, while Scotts Miracle-Gro writers focused on sharing what they know about their consumers. Also, you may as well forget about blank rectangles and <em> lorem ipsum </em> text filling design wireframes. Take a cue from MAN Diesel &amp; Turbo and pour your organization's actual content into the wireframes so you can get, early on, a true sense of how they'll actually work with your content and how to massage the layouts.</li>
	<li>
		<strong>Foiling personalization</strong>. Targeting content to people — based on their location, job role, team, and other factors — is fairly common on intranets today and has been for a few years now. These features keep clutter at bay and give employees the apps and content they need. But what if a person spans two locations or manages people in diverse roles and needs to see their content? Short of creating a "Super User" role, this problem can be a bit thorny. A trend we are seeing that skirts these issues is to let users select a different role to browse content. For example, MAN Diesel &amp; Turbo's <em> View Content </em> applet lets users choose an organization unit and location and temporarily display the associated content rather than their own location information. Similarly, at Skanska, information is targeted to the user but also offers flexibility in that all information is available via search or by switching the view of the site to <em> Browse As </em> another employee type.</li>
</ul>
<h2>
	Social!</h2>
<ul>
	<li>
		<strong>Lightning-fast access to coworker information</strong>. Remember the days when intranets made it painstaking to learn anything about coworkers? Sometimes just figuring out how to search for people was a task failure in itself. Today's great intranets make it quick and easy for users to access information about their colleagues. MAN Diesel &amp; Turbo takes fly-out windows to a new level, quickly displaying information about people as you type your query in the people search box. In their company organization charts, both the Everything Everywhere and NCR intranets do something similar with pop-ups: when users mouse over a name, the person's team, a photo, and other information appears. On Genentech's homepage, the <em> Who Is </em> section profiles interesting individuals at the company, and shares personal as well as professional information. Skanska offers a <em> Knowledge Map </em> to help users find experts in the company and ask them questions.</li>
	<li>
		<strong>Integrating employee profile pages with wall feeds</strong>. Although social networking exploded on intranets in the last few years, feeds were not abundant until this year. LivePerson's employee profile page functions like a social media profile on steroids. In its user profile pages, Staples features a personalized feed, <em> The Board</em>, while Logica's main page takes a newsfeed approach to presenting content pushed by country or service update and more.</li>
	<li>
		<strong>Supportive and accessible management</strong>. Today's business leaders have emerged from their executive suites and plunged into the information-sharing world, and they have adapted well. They are blogging, soliciting questions in public forums, and answering those questions. For example, Everything Everywhere offers <em> VP Space</em>, where VPs and teams communicate and connect via blogs, videos, status updates, pictures, and polls. The Genentech <em> Executive Committee Bios </em> focus on factoids — such as preferences between sushi or salad, coffee or tea. Both LivePerson and Logica's CEO blogs are among the more popular features on their respective intranets, and both CEOs support transparency at their organizations.</li>
	<li>
		<strong>Cheering personal content </strong> (in addition to business content). Employers recognize that <em> employees </em> also want to be identified as <em> human beings</em>, and they're celebrating this on their intranets. At Staples, people are encouraged to blog and write in feeds about both business and personal topics. LivePerson's intranet offers an array of social features that encourage employees to share. Similarly, Logica lets employees personalize their profiles with <em> About Me </em> content.</li>
</ul>
<p>
	<strong>See also: </strong> in-depth research on <a class="old" href="../social-networking-on-intranets/index.php" title="Alertbox: Social Networking on Intranets"> "Enterprise 2.0" — intranet social features</a>.</p>
<h2>
	ROI</h2>
<p>
	As in years past, teams are generally collecting spotty <a class="new" href="../../reports/usability-return-on-investment-roi/index.php" title="Nielsen Norman Group: Usability Return on Investment"> return-on-investment data</a>. A common theme — most prevalent at CenturyLink and Everything Everywhere — is to measure whether people are viewing fewer pages while hunting (which indicates better IA, search, and cross-linking); using the intranet more (which indicates engagement); or spending more time in the appropriate areas (which indicates both engagement and an ability to find the right content).</p>
<p>
	Genentech, LivePerson, Logica, Scotts Miracle-Gro, and Staples cited additional indications of user interest, including an increase in comments and ratings per story, photo contributions, blog posts, tagged items, and poll participation.</p>
<p>
	NCR collected metrics about the effects of content in silos. In the past, users would go to one intranet site for corporate news, another for human resources (HR) information, and possibly others if they needed to access marketing collateral or online applications. The new intranet helps users <strong>find information in one-third fewer intranet site visits</strong>, which decreased from 482,362 to 320,620.</p>
<p>
	With positive return on investment, evolving designs, more social engagement, stronger management support, and better intranet team size to company size ratio, our 2012 winning intranet designs provide a wonderful set of examples we can revere and learn from.</p>
<h2>
	Full Report</h2>
<p>
	<a class="new" href="../../reports/10-best-intranets-2012/index.php" title="Nielsen Norman Group report">431-page Intranet Design Annual 2012 with 187 screenshots</a> of the 10 winners for 2012 is available for download.</p>
<p>
	(See also: <a href="../intranet-design/index.php" title="Alertbox: This Year's 10 Best Intranets"> this year's Intranet Design Annual</a>.)</p>
</section>
  

  
  <section>
    
        
    
  </section>
  
  
  
  
  

  


  <section>
    <br />
    <p class="share"><b>Share <span class="hide-for-small-down">this article</span>:</b> <a href="https://twitter.com/intent/tweet?url=http://www.nngroup.com/articles/intranet-design-2012/&amp;text=10%20Best%20Intranets%20of%202012&amp;via=nngroup">Twitter</a> | <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=http://www.nngroup.com/articles/intranet-design-2012/&amp;title=10%20Best%20Intranets%20of%202012&amp;source=Nielsen%20Norman%20Group">LinkedIn</a> | <a href="https://plus.google.com/share?url=http://www.nngroup.com/articles/intranet-design-2012/">Google+</a> | <a href="mailto:?subject=NN/g Article: 10 Best Intranets of 2012&amp;body=http://www.nngroup.com/articles/intranet-design-2012/">Email</a></p>
    <hr />
  </section>

  <footer class="l-subsection">
    
    
    <div class="learn-more">
      <h2>Learn More</h2>
      <p><a href="../subscribe/index.php">Subscribe to the weekly newsletter</a> to get notified about future articles.</p>
      </div>
    
    

    <div class="row show-for-small-down related-content">
      
      
      

      
      
        <div class="column small-12 topic-links">All articles about:
          <ul>
            
            <li><a href="../../topic/intranets/index.php">Intranets</a></li>
            
          </ul>
        </div>
      
      

      
      
      

      
      
        <div class="small-12 column">
          <h3>Research Reports</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../reports/intranet-design-annual/index.php">2017 Intranet Design Annual</a></li>
              
            
              
                <li><a href="../../reports/intranet-usability-guidelines/index.php">Intranet Usability Guidelines: Findings from User Testing of 42 Intranets</a></li>
              
            
              
                <li><a href="../../reports/intranet-portals-experiences-real-life-projects/index.php">Intranet Portals: UX Design Experience from Real-Life Projects</a></li>
              
            
              
                <li><a href="../../reports/intranet-information-architecture-design-methods/index.php">Intranet Information Architecture Design Methods and Case Studies</a></li>
              
            
              
                <li><a href="../../reports/best-merged-intranets/index.php">Mergers and Acquisitions, and the Resulting Intranets 2007-2016</a></li>
              
            
          </ul>
        </div>
      
      

      
      
        <div class="small-12 column">
          <h3>Online Seminars</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../online-seminars/improving-intranet-content/index.php">Improving Intranet Content</a></li>
              
            
              
                <li><a href="../../online-seminars/intranet-users-find/index.php">Helping Intranet Users Find What They Need</a></li>
              
            
          </ul>
        </div>
      
      
    </div>

    <div class="hide-for-small-down row related-content">
      <div class="medium-6 columns">
        
        
          <div>
            <h3>Articles</h3>
            <ul class="no-bullet">
              
                
                <li><a href="../intranet-design/index.php">10 Best Intranets of 2017</a></li>
                
              
                
                <li><a href="../top-intranet-design-mistakes/index.php">The Top Enduring Intranet-Design Mistakes: 7 Deadly Sins</a></li>
                
              
                
                <li><a href="../top-intranet-trends/index.php">Top 10 Intranet Trends of 2016</a></li>
                
              
                
                <li><a href="../intranet-content-authors/index.php">3 Ways to Inspire Intranet Content Authors</a></li>
                
              
                
                <li><a href="../sharepoint-intranet-ux/index.php">Design a Brilliant SharePoint Intranet</a></li>
                
              
            </ul>
          </div>
        
        

        
          
          <div>
            <h3>Research Reports</h3>
            <ul class="no-bullet">
              
                
                  <li><a href="../../reports/intranet-design-annual/index.php">2017 Intranet Design Annual</a></li>
                
              
                
                  <li><a href="../../reports/intranet-usability-guidelines/index.php">Intranet Usability Guidelines: Findings from User Testing of 42 Intranets</a></li>
                
              
                
                  <li><a href="../../reports/intranet-portals-experiences-real-life-projects/index.php">Intranet Portals: UX Design Experience from Real-Life Projects</a></li>
                
              
                
                  <li><a href="../../reports/intranet-information-architecture-design-methods/index.php">Intranet Information Architecture Design Methods and Case Studies</a></li>
                
              
                
                  <li><a href="../../reports/best-merged-intranets/index.php">Mergers and Acquisitions, and the Resulting Intranets 2007-2016</a></li>
                
              
            </ul>
          </div>
          
        

      </div>
      <div class="medium-6 columns">
        
        
          
          <div>
            <h3>Online Seminars</h3>
            <ul class="no-bullet">
            
                <li><a href="../../online-seminars/improving-intranet-content/index.php">Improving Intranet Content</a></li>
            
                <li><a href="../../online-seminars/intranet-users-find/index.php">Helping Intranet Users Find What They Need</a></li>
            
            </ul>
          </div>
          
        
      </div>
    </div>
  </footer>
</article>

        </div>

      

    
  </div>
  <div class="row">
    
      <div class="column small-12 show-for-small-down l-navrail announcements">
        
          
        
      </div>
    
    
  </div>

    </div>
    




<footer id="page-footer">
    <div class="row hide-for-print">
        <div class="column small-12 medium-6">
            <section class="footer-popular">
                
                <h1>Popular Topics</h1>
                <div style="float: left;  width: 15em; display: block;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/applications/index.php">Application Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/e-commerce/index.php">E-Commerce Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/eyetracking/index.php">Eyetracking </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/information-architecture/index.php">Information Architecture </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/intranets/index.php">Intranet Design </a></li>
	<li><a href="../../topic/interaction-design/index.php">Interaction Design </a></li>
</ul>
</div>

<div style="float: left;  width: 15em; display: block; list-style: none;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/mobile-and-tablet-design/index.php">Mobile and Tablet Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/credibility-persuasion/index.php">Persuasive Design</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/research-methods/index.php">Research Methods</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/user-testing/index.php">User Testing </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/web-usability/index.php">Web Usability </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/writing-web/index.php">Web Writing &amp; Content Strategy </a></li>
</ul>
</div>

<p>&nbsp;</p>

                
            </section>
        </div>

        <div class="column small-12 medium-6">
            <div class="row">
                <div class="column small-6">
                    <section class="footer-about">
                        <h1>Follow Us</h1>
                        <ul>
	<li><a href="../subscribe/index.php">Subscribe to our weekly UX newsletter</a>,&nbsp;Jakob Nielsen&#39;s Alertbox</li>
	<li>Visit Don Norman&#39;s website, <a href="http://www.jnd.org/"> www.jnd.org </a></li>
	<li>Visit Bruce Tognazzini&#39;s website, <a href="http://www.asktog.com/"> www.asktog.com </a></li>
	<li style="padding-top: 6px;"><a class="twitter-follow-button" data-show-count="false" data-size="large" href="https://twitter.com/nngroup"><img alt="Follow @nngroup on Twitter" height="20" src="https://media.nngroup.com/media/editor/2016/12/30/twitter-follow-button.png" style="width: 117px; height: 20px;" width="117" /></a></li>
	<li style="padding-top: 6px;"><img alt="" height="14" src="https://media.nngroup.com/media/editor/2016/07/26/feed-icon-14x14.png" style="vertical-align:text-top;width:14px;margin-right:.25em;" width="14" /><a href="../../feed/rss/index.php">Subscribe to our RSS feed</a></li>
</ul>

                    </section>
                </div>

                <div class="column small-6">
                    <section class="footer-about right-column">
                        <h1>About
</h1>
                        <ul class="no-bullet">
	<li><a href="../../about/contact/index.php">Contact information</a></li>
	<li><a href="../../about/index.php">About Us</a></li>
	<li><a href="../../about/why-nng/index.php">Why NN/g? </a></li>
	<li><a href="../../return-policy/index.php">Return Policy</a></li>
</ul>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="copyright column small-12">
            <p><a href="../../copyright-and-reprint-info/index.php">Copyright</a> &copy; 1998-2017 Nielsen Norman Group, All Rights Reserved.</p>
        </div>
    </div>
</footer>


    


        <script src="https://media.nngroup.com/static/vendor/fastclick/js/fastclick.min.js"></script>
        <script src="https://media.nngroup.com/static/vendor/foundation/js/foundation.min.js"></script>
        <script src="http://media.nngroup.com/static/js/app.js"></script>
        <script src="http://media.nngroup.com/static/js/scroll-to-fixed.js"></script><script>var externalLinks = document.querySelectorAll('a[href^="http"]');for (var i = externalLinks.length-1; i >= 0; i--) {externalLinks[i].addEventListener("click", function(event) { alert("External links are disabled for this test."); event.preventDefault(); }, false);}var mailLinks = document.querySelectorAll('a[href^="mailto"]');for (var i = mailLinks.length-1; i >= 0; i--) {mailLinks[i].addEventListener("click", function(event) { alert("Email links are disabled for this test."); event.preventDefault(); }, false);}var externalForms = document.querySelectorAll('form');for (var i = externalForms.length-1; i >= 0; i--) {externalForms[i].addEventListener("submit", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false); externalForms[i].addEventListener("click", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false);}</script>

        <script>
            
            $(function() {
                $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
                $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
            });
        </script>

        
<script src="https://media.nngroup.com/static/js/articles.js"></script>


        
        <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1072699960;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072699960/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
    </body>

<!-- Mirrored from www.nngroup.com/articles/intranet-design-2012/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:13:13 GMT -->
</html>
