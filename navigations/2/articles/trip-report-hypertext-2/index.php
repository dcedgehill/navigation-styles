<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->





    
<!-- Mirrored from www.nngroup.com/articles/trip-report-hypertext-2/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:15:33 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1026.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"beacon":"bam.nr-data.net","queueTime":10,"applicationTime":2294,"agent":"","licenseKey":"1a30ef9931","errorBeacon":"bam.nr-data.net","applicationID":"2048992","transactionName":"NFxSMRBUX0EDUkIPDA0WdhAMVkVbDV8ZBxEXUFMJB0YfRAtUQRVZIktEDAFZVHYHRVcPD01eVRE="}</script>
        <title>Hypertext&#39;2 Trip Report: Article by Jakob Nielsen</title><meta property="og:title" content="Hypertext&#39;2 Trip Report: Article by Jakob Nielsen" />
  
        
        <meta name="description" content="Jakob Nielsen&#39;s trip report for the 1989 Hypertext 2 conference in York, U.K.">
        <meta property="og:description" content="Jakob Nielsen&#39;s trip report for the 1989 Hypertext 2 conference in York, U.K." />
        
  
        
	
        

        

        <meta name="robots" content="noarchive">
        <meta name="viewport" content="width=device-width">

        <link rel="search" type="application/opensearchdescription+xml" title="NN/g Search" href="../../opensearch.xml" />
        <link rel="shortcut icon" href="http://media.nngroup.com/static/img/favicon.ico" />

        <link rel="stylesheet" href="https://media.nngroup.com/static/vendor/font-awesome/css/font-awesome.min.css">

        
        <link rel="stylesheet" href="https://media.nngroup.com/static/css/7232a960f1df.css" type="text/css" />
        <link rel="stylesheet" href="http://fast.fonts.com/t/1.css?apiType=css&amp;projectid=c5846260-9def-46ec-9407-0f1349bd2d64" type="text/css">

        <script src="https://media.nngroup.com/static/vendor/modernizr/js/modernizr.min.js"></script>

        <script type="text/javascript" src="https://media.nngroup.com/static/js/3283be5def74.js"></script>
        

        

        
        
        <!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=229034,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('http://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->


<!-- Google Analytics Tracking Code for http://nngroup.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23132681-1', 'nngroup.com');
  ga('require', 'linkid', 'https://www.nngroup.com/articles/trip-report-hypertext-2/linkid.js');
  ga('require', 'displayfeatures');

// Optimizely Universal Analytics Integration
 window.optimizely = window.optimizely || [];
 window.optimizely.push("activateUniversalAnalytics");

  ga('send', 'pageview');

</script>
<script>
/**
* Function to track a click on an outbound link in Google Analytics.
*/
function trackOutboundLink(label, url) {
    if(window.ga && ga.loaded) {
        ga('send', 'event', 'outbound', 'click', label, {'hitCallback': function(){document.location = url;}
        });
    } else {
        document.location = url;
    }
}
/**
* Function to track a click on a link in the article announcement area.
*/
$('#articleAnnouncement a').on('click', function(){
  ga('send', 'event', 'link', 'click', 'articleAnnouncement');
});
</script>

<script type="text/javascript">
 setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2421.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



<!-- Hotjar Tracking Code for http://nngroup.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:121605,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

        <script src="https://media.nngroup.com/static/js/analytics/track_outbound_link.js"></script>
        
        
        
    </head>
    <body class="location-articles articles-detail">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
    <header id="page-header">
        <div id="cart" class="shopping-cart mobile-cart show-for-small-down"></div>
        <div class="row">
            <div id="masthead" class="masthead small-12 medium-7 column">

                <!-- The button to toggles menu on small screen -->

                <h1><a href="../../index.php"><img src="http://media.nngroup.com/static/img/logo-wordmark.png" width="74" height="30"> Nielsen <span class="norman">Norman</span> Group</a></h1>
                <p class="tagline show-for-medium-up">Evidence-Based User Experience Research, Training, and Consulting</p>

                <!-- The button to toggles form search on small screen -->


                <div style="clear: right;width:  100%;height: 1px;"></div><?php define('ROOT_PATH',$_SERVER["DOCUMENT_ROOT"] . '/apps/navigation_styles/navigations/2'); ?><?php include(ROOT_PATH)."/navigation.php"; ?>

                <!-- Drop down menu on small screen -->
                <div id="hideNav" class="show-for-small-down">
                  
                  <ul class="menu-content">
                    <li><a href="../../index.php">Home</a></li>
                    <li class="has-submenu">
                      <a>Training</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../training/index.php">Overview</a></li>
                        
<li id="nav-ux-conference"><a href="../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../online-seminars/index.php"><span>Online Seminars</span></a></li>

                      </ul>
                    </li>
                    <li class="has-submenu">
                      <a>Consulting</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
  <li><a href="../../consulting/index.php">Overview</a></li>
  
  <li><a href="../../consulting/benchmarking/index.php">Benchmarking</a></li>
  
  <li><a href="../../consulting/expert-design-review/index.php">Expert Design Review</a></li>
  
  <li><a href="../../consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
  
  <li><a href="../../consulting/usability-testing/index.php">Usability Testing </a></li>
  
  <li><a href="../../consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
  
  <li><a href="../../in-house-training/index.php">In-House Training</a></li>
</ul>

                    </li>

                    <li><a href="../../reports/index.php">Reports</a></li>
                    <li><a href="../index.php">Articles</a></li>
                    <li class="has-submenu"><a>About NN/G</a><i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../about/index.php">Overview</a></li>
                        <li><a href="../../people/index.php">People</a></li>
                        <li><a href="../../about/why-nng/index.php">Why NN/g?</a></li>
                        <li><a href="../../about/contact/index.php">Contact</a></li>
                        <li><a href="../../news/index.php">News</a></li>
                        <li><a href="../../about/history/index.php">History</a></li>
                        <li><a href="../../books/index.php">Books</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                </div>

                <hr class="header-divider show-for-small-only"/>


            </div>
       </div>

        <div id="hideNav">            <nav class="primary-nav small-12 column">
                <ul>
                    <li><a class="navitem" id="nav-home" href="../../index.php">Home</a></li>
                    <li><a class="navitem" id="nav-training" href="../../training/index.php">Training</a></li>
                    <li><a class="navitem" id="nav-consulting" href="../../consulting/index.php">Consulting</a></li>
                    <li><a class="navitem" id="nav-reports" href="../../reports/index.php">Reports</a></li>
                    <li><a class="navitem" id="nav-articles" href="../index.php">Articles</a></li>
                    <li><a class="navitem" id="nav-about" href="../../about/index.php">About NN/G</a></li>
                </ul>
            </nav>
            <nav class="secondary-nav small-12 medium-12 large-12 columns">
                
                
            </nav>
        </div>
    </header>
    <div id="main" class="l-content" onclick="closeNav(event)"><span id="preventClose" style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776; Menu</span>
    
  

  

  <div class="row">
    
      
      <section class="small-12 medium-3 columns collapsable-content l-navrail articles-left-column">
        <h1 class="show-for-small-down collapse-link mobile-browse-header">Browse by Topic and Author</h1>
          <div class="row collapsable">

            
            <div class="small-6 medium-12 columns">
              <section id="article-popular-topics" style="display: none;">
                
                <h1>Topics</h1>
                <ul id="article-popular-topics">
                    
                    <li>
                        
                        <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/intranets/all/index.php">Intranets</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/user-testing/all/index.php">User Testing</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                        
                    </li>
                    
                    <li><a href="#" id="article-see-all-topics"><strong>See all topics&hellip;</strong></a></li>
                </ul>
                
              </section>
              <section id="article-all-topics" style="display: none;">
                
                <h1>
                  <span class="show-for-medium-up">All Article Topics</span>
                  <span class="show-for-small-down">All Topics</span>
                  (<a href="#" id="article-hide-all-topics">hide</a>)
                </h1>
                <ul>
                  
                  <li>
                    
                    <a href="../../topic/accessibility/all/index.php">Accessibility</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/agile/all/index.php">Agile</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/analytics-and-metrics/all/index.php">Analytics &amp; Metrics</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/applications/all/index.php">Application Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/b2b-websites/all/index.php">B2B Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/behavior-patterns/all/index.php">Behavior Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/branding/all/index.php">Branding</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/content-strategy/all/index.php">Content Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/corporate-websites/all/index.php">Corporate Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/design-patterns/all/index.php">Design Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-design-process/all/index.php">Design Process</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/email/all/index.php">Email</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/eyetracking/all/index.php">Eyetracking</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/heuristic-evaluation/all/index.php">Heuristic Evaluation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/human-computer-interaction/all/index.php">Human Computer Interaction</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-ideation/all/index.php">Ideation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/information-architecture/all/index.php">Information Architecture</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/interaction-design/all/index.php">Interaction Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/international-users/all/index.php">International Users</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/intranets/all/index.php">Intranets</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-management/all/index.php">Management</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/navigation/all/index.php">Navigation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/non-profit-websites/all/index.php">Non-Profit Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/personas/all/index.php">Personas</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/persuasive-design/all/index.php">Persuasive Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/prototyping/all/index.php">Prototyping</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/psychology-and-ux/all/index.php">Psychology and UX</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/research-methods/all/index.php">Research Methods</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/search/all/index.php">Search</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/social-media/all/index.php">Social Media</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/strategy/all/index.php">Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/user-testing/all/index.php">User Testing</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-teams/all/index.php">UX Teams</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/visual-design/all/index.php">Visual Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/writing-web/all/index.php">Writing for the Web</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/young-users/all/index.php">Young Users</a>
                    
                  </li>
                  
                </ul>
                
              </section>
            </div>
            <script>
              /* Determine which topics list will be displayed: the full or the
               * partial.  We are inlining this close to the list itself to
               * prevent rendering delays on the client
               */
              var topic_list = $.cookie("article-topics");

              var popular_topics = $('#article-popular-topics');
              var all_topics = $('#article-all-topics');

              if(topic_list == 1) {
                  popular_topics.hide();
                  all_topics.show();
              } else {
                  popular_topics.show();
                  all_topics.hide();
              }
            </script>
            

            
            <div class="small-6 medium-12 columns">
              
              <section id="article-initial-authors">
                  <h1>Author</h1>
                  <ul>
                    
                    <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                    
                    <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                    
                    <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                    
                    <li><a href="#" onClick="$('#article-initial-authors').hide(); $('#article-all-authors').show();"><strong>See all authors&hellip;</strong></a></li>
                  </ul>
              </section>
              <section id="article-all-authors" style="display: none;">
                <h1>
                  All Authors
                  (<a href="#" id="article-hide-all-authors">hide</a>)
                </h1>
                <ul>
                  
                  <li><a href="../author/raluca-budiu/index.php">Raluca Budiu</a></li>
                  
                  <li><a href="../author/patty-caya/index.php">Patty Caya</a></li>
                  
                  <li><a href="../author/susan-farrell/index.php">Susan Farrell</a></li>
                  
                  <li><a href="../author/therese-fessenden/index.php">Therese Fessenden</a></li>
                  
                  <li><a href="../author/kim-flaherty/index.php">Kim Flaherty</a></li>
                  
                  <li><a href="../author/sarah-gibbons/index.php">Sarah Gibbons</a></li>
                  
                  <li><a href="../author/aurora-harley/index.php">Aurora Harley</a></li>
                  
                  <li><a href="../author/kate-kaplan/index.php">Kate Kaplan</a></li>
                  
                  <li><a href="../author/page-laubheimer/index.php">Page Laubheimer</a></li>
                  
                  <li><a href="../author/angela-li/index.php">Angie Li</a></li>
                  
                  <li><a href="../author/hoa-loranger/index.php">Hoa Loranger</a></li>
                  
                  <li><a href="../author/kate-meyer/index.php">Kate Meyer</a></li>
                  
                  <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                  
                  <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                  
                  <li><a href="../author/kara-pernice/index.php">Kara Pernice</a></li>
                  
                  <li><a href="../author/christian-rohrer/index.php">Christian Rohrer</a></li>
                  
                  <li><a href="../author/amy-schade/index.php">Amy Schade</a></li>
                  
                  <li><a href="../author/katie-sherwin/index.php">Katie Sherwin</a></li>
                  
                  <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                  
                  <li><a href="../author/kathryn-whitenton/index.php">Kathryn Whitenton</a></li>
                  
                </ul>
              </section>
              
            </div>
            
          </div>

          
          <section class="hide-for-small-down">
            
              
              <h1>Recent Articles</h1>
              
              <ul>
                  
                  <li><a href="../seamless-cross-channel/index.php">Seamlessness in the Omnichannel User Experience</a></li>
                  
                  <li><a href="../brand-intention-interpretation/index.php">Experience Design: Bridging Brand Intention and Brand Interpretation</a></li>
                  
                  <li><a href="../eyetracking-tasks-efficient-scanning/index.php">Scanning Patterns on the Web Are Optimized for the Current Task</a></li>
                  
                  <li><a href="../flat-design-best-practices/index.php">Flat-Design Best Practices</a></li>
                  
                  <li><a href="../ux-user-stories/index.php">Accounting for UX Work with User Stories in Agile Projects</a></li>
                  
                  <li><a href="../index.php"><strong>See all articles&hellip;</strong></a></li>
              </ul>
              
            
          </section>
          

          
          
          <section class="hide-for-small-down">
            <h1>Popular Articles</h1>
            
            <ul>
                
                <li><a href="../usability-101-introduction-to-usability/index.php">Usability 101: Introduction to Usability</a></li>
                
                <li><a href="../top-10-mistakes-web-design/index.php">Top 10 Mistakes in Web Design</a></li>
                
                <li><a href="../how-users-read-on-the-web/index.php">How Users Read on the Web</a></li>
                
                <li><a href="../f-shaped-pattern-reading-web-content/index.php">F-Shaped Pattern For Reading Web Content</a></li>
                
                <li><a href="../ten-usability-heuristics/index.php">10 Usability Heuristics for User Interface Design</a></li>
                
                <li><a href="../intranet-design-2016/index.php">10 Best Intranets of 2016</a></li>
                
                <li><a href="../which-ux-research-methods/index.php">When to Use Which User-Experience Research Methods</a></li>
                
                <li><a href="../response-times-3-important-limits/index.php">Response Times: The 3 Important Limits</a></li>
                
                <li><a href="../why-you-only-need-to-test-with-5-users/index.php">Why You Only Need to Test with 5 Users</a></li>
                
                <li><a href="../page-fold-manifesto/index.php">The Fold Manifesto: Why the Page Fold Still Matters</a></li>
                
            </ul>
          </section>
          
          

          
            <section class="hide-for-small-down">
              
            </section>
          

      </section>
      

      
        <div class="small-12 medium-9 columns content-body">
          




<article class="l-subsection alertbox-article-content article-detail-bl">
  
  <header>
    <div class="l-subsection">
      <h1>Hypertext&#39;2 Trip Report</h1>
      <div class="author-meta">by
        
          
            
            <a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a>
            
          
        on  July 1, 1989
        
      </div>

      

      
      <div class="topic-links">
        Topics:
        
          <ul>

</ul>
        
      </div>
      
    </div>
  </header>
  

  
  
  

  
  <section id="article-body"><p><em>York, U.K., 29-30 June 1989 </em> .  <em> The conference proceedings can be <a href="http://www.amazon.com/exec/obidos/ISBN=0893916722/useitcomusableinA/"> bought online</a>.  </em></p>

<p> </p>

<p>Hypertext 2 was the major conference in Europe this year in the hypertext field and attracted 200 participants. Actually it had "attracted" many more who were just not there but had been turned away because the organizers had committed the same mistake as the planners of the Hypertext'87 conference in North Carolina and placed the conference in a location which would only hold 200 people. Their original assumption was that a hypertext conference in the U.K. might attract 100 participants, but that was the underestimate of the year. Hypertext is hot these days.</p>

<p>Most of the participants came from the U.K., but there was also a fair number of participants from many other European countries as well as from USA/Canada and Asia (Japan and Singapore, of course).</p>

<p>As can be seen from the name, Hypertext 2 was the second U.K hypertext conference. The first was (surprise) Hypertext I which was held last year for an audience of only 35 people. So since the proceedings of Hypertext I had seen only limited distribution, the organizers had taken the somewhat unusual step of including them in the material for Hypertext 2 (The Hypertext I proceedings are also published as Hypertext Theory Into Practice by Ablex).</p>

<p>Furthermore, the registration packet included a hypertext version of the Hypertext I papers in Guide form. Unfortunately this hypertext version was quite poorly done, as can be seen from a few examples: The very first chapter contains the following words: "Figure 1, on the next page ..."-a piece of text which was obviously taken unchanged from the printed version of the chapter. Since Guide uses scrolling text fields, there is no such thing as a next page, and furthermore any decent hypertext would provide a link for the user to click on here. Even worse is of course the fact that the figure has not been included in the hypertext version. In general, only very few of the figures from the book are also in the hypertext. The hypertext also uses an inconsistent notation for the anchors for Guide expansion and reference buttons.</p>

<h2>HyperCard</h2>

<p>It was amazing to see the explosion of activity generated by HyperCard at various universities and research centers. This conference did not have very much work in evidence done at actual companies using HyperCard, however, even though I personally promote it also for prototyping "real" user interfaces in a paper presented to a broad audience at the NordDATA'89 Joint Scandinavian Computer Conference. In any case, many of the uses of HyperCard demoed at this conference had nothing to do with hypertext as it is traditionally understood but were simply prototypes of general graphical user interfaces. In spite of the heavy presence of Apple systems at the conference there were no speakers or demoers from Apple who had preferred to concentrate on some commercial event taking place in Glasgow.</p>

<p>One of the more interesting examples of the use of HyperCard was shown by Harry McMahon and Bill O'Neill from the University of Ulster who had placed a few Macintoshes with sound and image digitizers in an elementary school to get the pupils to <strong> create their own interactive fiction</strong>. Of course, most of these stories were fairly simple, such as (created by a 7-years old) "the teddy bear went for a walk in the forest and met another teddy bear"-shown over a sequential series of HyperCard cards like a cartoon strip. More advanced designs used a facility called bubbles where the children can first draw their cards and then choose from various shapes of comics-like speech and thought-balloons to add to the image. The interesting idea is that it is possible to add multiple bubbles to each card whereupon they will be displayed to the reader one at a time. In this way, it is possible for the child to generate a dialogue between the characters in the story. It is even possible to contrast what the characters say with what they think. For example, in a story about a mouse about to be killed, it asked for a last wish: To sing a song. This wish was spoken out loud (placed in a speech balloon) but the mouse's thought (placed in a thought balloon) was "I am not as stupid as I look." The next speech balloon revealed that the mouse had chosen to sing the well-known song about bottles on a wall (falling down one at a time), but starting with "A thousand million green bottles sitting on a wall..." So this smart mouse would survive for some time to come.</p>

<p>Most of these stories were basically linear in nature which is why I said that they did not really have all that much to do with the concept of hypertext. McMahon and O'Neill had on purpose avoided to introduce commercial hyperstories (such as e.g. Inigo Gets Out or The Manhole) to the children so that they could observe the natural evolution of their approach to the new medium of interactive fiction. It actually did happen that a few <strong> 10 years olds did discover the hypertext principle on their own</strong>. They were creating a story about a person who was visiting an alien world and was captured by the aliens. He was offered a job by the alien boss and now thought to himself: Should I try to escape or should I take a job? The reader could click on either of these two thought balloons to proceed with the story. McMahon remarked that an interesting aspect of this story design was that the pupils had had to change their perspective on writing. Originally they thought of creating a story as they went through it (writing for the writer, as it were), but in this new situation, they had to consider what the reader could do and would want to do, so they had to change their perspective to writing for readers.</p>

<h2>Hypertext in the Real World (Peter Brown)</h2>

<p>The best presentation at this conference was probably the invited speech by <strong> Peter Brown </strong> from the University of Kent. Brown was the inventor of <strong> Guide </strong> which is now being sold for the Macintosh and IBM PC by OWL while Brown continues to develop the Unix version.</p>

<p>After his presentation, Brown was asked what had prompted him to develop Guide in the first place. His answer was that Guide had not originally been developed specifically as a hypertext system. It was developed as a solution to a problem: That of viewing online documentation which was traditionally displayed in a horribly perverted form of the paper versions.</p>

<p>Brown's talk was also concerned with the issue of providing solutions. He said that it is easy for researchers to move in a dream world divorced from real user needs and that this is often encouraged by funding agencies who often do not want to admit failure by stopping grandiose but useless projects. Therefore he wanted to relate an example from the real world to show us how hypertext was really being used. OWL had developed the world's first personal computer hypertext system on the basis of Unix Guide and now has a large amount of world-wide corporate business in e.g. the automobile industry. But the Unix version of Guide has also continued in development at the University of Kent. So Brown felt that Guide could now be seen as a mature product in terms of usage experience.</p>

<p>Brown said that he is often asked whether Guide is better or worse than HyperCard: His answer is "no," since it is neither better nor worse, but different. One major difference is that Guide is based on scrolling while HyperCard is frame-based. Some information lends itself naturally to being divided up into several cards, but some doesn't, and in those cases the scroll model in Guide helps by removing the complexity of having information split over several cards.</p>

<p>In choosing a problem to present in his talk, Brown faced several difficulties: A toy project of a few thousand lines of text would prove nothing. There is also a danger of choosing a project which is the first in some field since the "first" of anything always generates extra excitement, attracts the best people, and gets extra funding, and therefore again would not be realistic. Even though he realized this, he had chosen an application which was the "first" since it at least was large-scale and had strict funding criteria where the people on the project had to justify the commercial viability of the project. Brown presented a project from the computer company ICL called LOCATOR done in collaboration with the University of Kent. People at ICL are actually going to sit in front of the Guide screen 8 hours a day to use LOCATOR.</p>

<p>The project is concerned with answering hardware fault calls (so-called "laundering"): Customers call for service over the telephone and the goal is to diagnose the fault so that an engineer can be sent if needed and will bring the necessary spare parts to the customer site on the first visit.</p>

<p>The system runs on Sun workstations, and the launderer sits at the workstation with a document displayed in Unix Guide. In such a real world project, things are never as smooth as one would want. For example, users change their mind half way through the conversation. The caller may not know the answer to technical questions and may have to suspend the conversation and call back later. Because of the importance of the laundering service, it has to be up all the time even when the permanent staff is away, so it may have to be manned in certain circumstances by temporary staff with only half an hour's training, leading to high requirements for usability. The design was fairly simple and consisted of hierarchical clicking on inquiry buttons to answer various questions. There were also hypertext links to various help screens, e.g. showing how a laser printer control panel looks.</p>

<p>A question from the audience was how that solution to the diagnosis problem compared with using an expert system. Brown's answer was that there had been some inter-company fights within ICL between expert systems and hypertext, but that in this case, hypertext won. Since they had not implemented the expert system also, we could not know for sure which option would really have been best.</p>

<p>After having presented this case study, Brown turned to discussing <strong> eight issues in practical hypertext use</strong>:</p>

<ol>
	<li>The first issue was that people do not want hypertext systems, they want solutions. Hypertext of course may be part of that solution. We need to use the customer's existing tools and often need to <strong> integrate the hypertext tool with other tools </strong> in a single seamless package to be part of the company information system</li>
	<li>The second issue was <strong> authorship</strong>. The ideal would be to use completely differently techniques from paper and e.g. Glasgow Online was lucky to get funding to do this by starting from scratch. But in most cases one has to do conversion from existing materials in a two step procedure. The first step was the use of automatic tool to convert from plain text to hypertext (some people like Tim Oren from Apple think that a project should be reconsidered if automatic link analysis is all one is going to do, considering the hypertext medium is not really being used). The second step was tailoring the hypertext by hand: This is expensive but necessary.</li>
	<li>The third issue was <strong> testing </strong> which needed to be done in four different ways:
	<ul>
		<li>Debugging linkage errors: It was fairly easy to have automatic check for dangling links and slightly harder to check that each link pointed to the node where it was supposed to go.</li>
		<li>Spelling checks and check for house style of hypertext. In their case, these checks could be done with automatic tools since Unix Guide uses troff tags for markup, allowing existing tools to be used.</li>
		<li>Testing the quality of the material itself.</li>
		<li>Testing ease of use using human factors methods. For example, they frequently found that buttons were too small. Brown said that there were no easy answers to this testing, but that one vital thing was to log each user session for future analysis.</li>
	</ul>
	</li>
	<li>Issue four was handling <strong> large documents</strong>. If one leaves out the images, the example ICL system is just a few megabytes, so it is not massive, but it is still large. The worst problem is taming complexity, and discipline is vital to doing this. To achieving such discipline, Brown advocated having a strong hierarchical backbone to the hypertext. One should also use common formats for the presentation of the information and develop a <strong> "hypertext house style" </strong> for how to use buttons, how to present them, etc. This discipline not only helps the author and the people maintaining the document, but also the readers.</li>
	<li>Issue five was the <strong> getting lost </strong> problem and Brown noted that most of the papers at the conference were on this problem and on navigation. In their system, the getting lost problem is not so bad for the launderers but it is a moderate problem for the authors. Unix Guide's lack of gotos may have helped on this problem. In many cases they have paths which join: leading to the same conclusion from various starting points (e.g. the fault "no power" can be diagnosed from the symptoms "cannot boot" and "screen blank"). From the user's perspective, everything is represented as a natural hierarchy where the "no power" node is copied to every location where it is needed instead of having the user jump to a single "no power" node.</li>
	<li>Issue six was the <strong> cost of projects</strong>. The LOCATOR experience was that diagnosis was 20% better than when done from a paper form of the information. The proportion of customer calls handled correctly had changed from 68% correct when using paper to 88% when using the hypertext, even rising to 92% after some use of the system. This leads to big savings in money when you don't have to have engineers drive in vain or back to get extra spare parts. It also does seem to be worth the extra cost of getting the workstation for displaying the info. The primary advantages of hypertext are better information and quicker access to the information. A further advantage is that when hypertext comes into a company it provides an opportunity to standardize the information.</li>
	<li>Issue seven was abstraction in the information and getting away from gotos (Brown's pet peeve). ICL had produced a special authorship tool for the macro level in the original Unix philosophy of having special modular tools.</li>
	<li>Issue eight was <strong> handling change</strong>. It is easy in hypertext to change the individual nodes, but hard to change the structure of the information base. The author needs to record why he/she did things and capture the design rationale: When another person looks at the document 3 years later it is nice to know why the structure is there and how it is. There is a potential serious problem with long term maintenance. We currently have no real experience with this aspect of hypertext but we can fear the worst because software maintenance typically accounts for at least 50% of the cost of a project and there is no reason to believe that it should not be just as bad for document maintenance.</li>
</ol>

<p>We are roughly in the same stage with respect to hypertext development as programming was in the 1960s. In hindsight, the programming tools were not very good then, but people still did significant software projects, just as we can now do significant hypertext projects. We cannot assume that the problems will be completely solved (just as there is no "silver bullet" for software engineering), but we can aim for incremental improvements.</p>

<p>As a more general comment, Brown said that designers and computer scientists try to find a few general features which can cover all applications, but that the real world unfortunately is not like that. One needs special features for special applications which are so diverse, and sometimes, even different authors have different needs within a single application.</p>

<h2>A Guru goes Commercial (Ted Nelson)</h2>

<p>Ted Nelson was the second invited speaker and he gave his presentation actually dressed up in a nice suit. One of the main elements in Nelson's talk was his relief that he had finally been recognized by a major company in the form of Autodesk which had bought Xanadu and was going to release the Xanadu file system shortly. They are planning to release the product in the first quarter of 1990 in the form of software keeping track of addresses of information which is constantly moving around. You have virtual addresses which do not change and which can be kept in a document as pointers. The <strong> Xanadu </strong> product in 1990 will be for single computers on a LAN connecting to a server. The bad news is that this is incompatible with all other current software. But we have to start over because of the current problem of people having so much data in different file formats which they cannot read anymore. Nelson called this a hideously tangled web. The Xanadu proposal is to have a stable repository of data which can be accessed by various other means. This is meant to be a win-win solution for everyone: A level playing field which can be used in many different ways. Project Xanadu will then release the full publishing system 2 years after the Xanadu server. [ <em> <span class="updatecomment"> Note added 1997: of course, we now know that Xanadu remained vaporware for many years after these projections.] </span> </em></p>

<p>In true "hyper"-style, Nelson started his talk by quoting the conference chair, Ray McAleese's introduction to the proceedings. McAleese had written that "when Ted Nelson wrote that the structure of ideas is not sequential, he could not have envisaged how the idea of hypermedia would take off." <em> Oh, yes, he could </em> , said Nelson! It had been "obvious" to him that print would be replaced by online text by 1962.</p>

<p>Nelson complained that people always give him credit for having invented the word in 1965 but never ask him what he has done since then. One of the things he has done is to develop the Xanadu file system to serve as a backbone for large interconnected hypertexts. Nelson was very upset about the CD-ROM business, which is unaware of the general directions we much take. The good news is that you can have 500 Mb on a disk, but the bad news is that you can only have 500 Mb on the disk, because it isolates the knowledge on each CD-ROM: We are in a Balkanized information situation where higher and higher walls are being built around the borders of each document. It will not be true that Xanadu will only be useful when everything is in there. It will be useful in all situations where you want to include information from one document in another and keep track of versioning, such as e.g. a lawyer's office.</p>

<p>Literature is the interconnected set of documents in a specific field and it is the cross-connections which give the whole added value compared to the parts. Nelson admitted that free interconnections will result in a confusing tangle of stuff - but that is what we have already. When reading a specific document, you will see only the interconnections which a specific editor has decided are relevant to you, but you can push aside the editor's filter and see everything. Even though most of Nelson's talk was a repetition of material from his earlier books, this recognition of the need for editors seemed to be a new aspect of his thinking. We could actually develop the editing concept even further, since there is of course the the democratic possibility of having several editors. But then we may start seeing the need for meta-editors in the form of people who recommend which editors you should pay attention to. This is somewhat like The Whole Earth Catalog which is a handbook of what handbooks are good to use.</p>

<p>Nelson was in favor of having hypertext through a concept he called <strong> transclusions</strong>. This involves not just having links but being able to take a part of another hypertext and include it in your own. Examples of this are collages and anthologies. Currently the copyright problem makes these kinds of publications difficult to produce in a kind of n-square problem where you have to write to everybody to get permission. For use in transclusions, the node model of hypertext is wrong. Nelson felt that we have to be able to make links to spans of bytes using a finer granularity than nodes. Furthermore, a span of characters may be discontinuous. Nodes are too primitive to support this, and in most hypertext systems, links usually point to either large chunks of information or to slits between chars (i.e. a single insertion point). Nelson said that he would be describing a lot of these ideas in a an anthology to be released later in 1989 called Replacing the Printed Word.</p>

<p>At the end of this keynote talk, Nelson wanted to show some slides. Unfortunately some confusion had resulted from the transfer of the slides from his American carrousel to the British slide carrousel, resulting in the slides being completely out of order. And ironically, Nelson did not feel like giving a non-linear presentation of his slides (many people made fun of this). Instead, he got a chance to present his slides the next day when they had been put back in their intended linear order.</p>

<p>Nelson used plenty of one-liners in his talk, such as: "You have to dig deep to do anything serious with HyperCard and pretty soon you hit the cement at the bottom."</p>

<h2>The <cite> Elastic Charles </cite></h2>

<p>The most technologically fancy system presented at the conference was the Elastic Charles by Hans Peter Brøndmo and Glorianna Davenport from the MIT Media Lab. This is an interactive film about the Charles River in Boston implemented on a Macintosh II with HyperCard, a ColorSpace II video overlay (genlock) card, and a video disk player. 15 students shot film about what they thought the Charles River was about (e.g. history, rowing regattas, bridge reconstruction, dams, pollution) and the Elastic Charles integrates all this film to a single hypertext magazine format.</p>

<p>Br¿ndmo said that they are not so much trying to develop sexy technology as to push the form of hypertext, especially with regard to extending the hypertext concept of linkage to temporal media such as video. They used icons to have a less imposing way to let users move along a story path. To generate good icons to represent video clips as hypertext destinations, they use miniaturized clips of video as icons which are shown on top of the main film image. They had coined the term <strong> micons </strong> for these live video miniature icons.</p>

<p>When working with video, the linking paradigm gets a temporal component: The author has to define not just where on the screen an anchor micon should be placed (as in standard HyperCard), but also when it should appear and disappear while the user is viewing a piece of video. A further issues in jumping between film clips is what should happen when you return to the departure point: Should you continue to watch the film clip from where you interrupted it by jumping somewhere else, or should you start watching it over again (or just rewatch the last few seconds from before the jump)? It seems to me that there are major possibilities for disorientation buried here but unfortunately Br¿ndmo and Davenport did not present results of user testing of their interface.</p>

<p>They do have a filtering mechanism to allow users to see some links/micons and not others. They are also working on a voice interface to the filter so that the user can say e.g. "history" to see the history list icons.</p>

<p>In her presentation, Davenport mentioned that she considered Aspen as one of the most interesting art works of the early 80s. But unfortunately it never left the lab. Actually, it seems to me that MIT should donate the Aspen system to the Computer Museum if they have not done so yet.</p>

<h2>Navigation and Browsing</h2>

<p>About half of the sessions at the conference had navigation and browsing as their theme. Pat Wright from the Applied Psychology Unit in Cambridge listed <strong> five navigation tasks</strong>:</p>

<ul>
	<li>Going to a known place.</li>
	<li>Going to a ill-defined place.</li>
	<li>Going back to where you were earlier.</li>
	<li>Going somewhere new.</li>
	<li>Knowing how much reading would be involved to read a given section <em> before </em> you start reading it.</li>
</ul>

<p>Wright also presented the results of studies looking at the first two of these issues. She had investigated the two main approaches to help readers goto a designated place in a text:</p>

<ul>
	<li>To have a separate navigation display. This leaves more room for text on main display and also gives more space for navigation support when the navigation display is actually shown because it can potentially take over the entire screen. On the other hand, having to shift back and forth between two displays may be clumbersome for readers. Wright tested this principle in an index design where users could go to a separate HyperCard card listing all the other cards in the stack.</li>
	<li>Navigating from the text page itself. This saves an intervening step but since there may not be much screen space left over, users need to do more navigation. Wright tested this principle in a page design having some hypertext buttons directly on each HyperCard card.</li>
</ul>

<p>She had tested the two different navigation designs with two small hypertexts and used subjects who were members of general public with a mean age of 40 years. The results showed that subjects performed best with the index design for one of the hypertexts but better for the page displays for the other hypertext, so the conclusion had to be that there is no universally best navigation style. In some cases it may be reasonable to separate the hypertext navigation from the text display to provide users with a better overview but in other cases it is most efficient to allow users direct links.</p>

<p>In a questionnaire they asked their subjects to comment on how good they would think online information would be for various kinds of information to be read by various kinds of users. The subjects having used the index design rated 48% of the items as suitable while the users having used the page design only rated 20% as suitable for electronic presentation. This is an indication that the users of the system with a separate navigation display had had the most positive experience with hypertext.</p>

<p>In another study, Cliff McKnight from the HUSAT research center at Loughborough had tested various different ways to provide overview diagrams. They had used the text on houseplants that had also been used in Pat Wright's experiments so there might over time be a chance to collect several different research results based on the same text.</p>

<p>They had tested three different factors in the design of <strong> overview diagrams</strong>: Listing the elements either hierarchically or alphabetically, the presence or absence of a current position indicator, and finally the use of typographical cues to signal the hierarchical structure of the text (by the use of CAPS for major headings, etc.). Unfortunately they had not tested graphical layouts versus purely textual layouts, but of course their experiment was complex enough already.</p>

<p>In describing the design of their experiment, McKnight stated that "obviously" the subjects had been prevented from using simple string search in answering the questions. Actually this indicated to me that they may be testing the wrong kind of tasks. Personally I find it more interesting how overview diagrams perform when they are used in combination with other navigation methods since it is exactly when users jump around in an uncontrolled manner that the overview diagrams may be the most useful.</p>

<p>The results showed that subjects using the hierarchical contents list navigated more efficiently than subjects using the alphabetic contents list and that having a current position indicator also improved efficiency. There was no significant effect of typographical cues.</p>

<p>The overview diagram was accessed more times by subjects who had the hierarchical contents list than subjects using the alphabetical contents list which might indicate that the hierarchical list was more useful. This study used a very small hypertext of 24 HyperCard cards and it is likely that the hierarchical contents would do even better in a larger hypertext where it would not be possible to show a complete alphabetical listing on a single card.</p>

<p>Finally, McKnight mentioned that they had used an objective measurements of task performance to assess whether users were lost in the hypertext and not and the users' own subjective feelings of being lost. In real life, it may be that the users' feelings are the most important since they will determine whether this kind of system will be used.</p>

<h2>Hypertext meets Interactive Fiction</h2>

<p>Gordon Howell from the Scottish HCI Centre in Edinburgh gave an introduction to the field of <strong> interactive fiction</strong>. There is a fundamental difference between interactive fiction and information systems because the goal is not to find the answer as soon as possible but rather the experience itself, so you may want to draw out the resolution of questions for quite long. Howell started by giving some real life examples of creating your own stories as you go along:</p>

<p>Conversations (you are not sure what you are going to say before you say it), panel sessions at conferences, games such as adventure games, and several books, including both encyclopedias, Borges' <cite> Ficciones </cite> , and Pavic's <cite> Dictionary of the Khazars </cite> which was totally sold out in the Edinburgh bookshops after a favorable review in <cite> The Scotsman</cite>.</p>

<p>There are difficulties in interactive fiction in practice, however. Current technology limits the size of an interactive fiction so that after a short time you will have read it all or at least have an indication of what the author has to offer. Current interactive fictions are also too predictable so that the reader does not really feel in control: The illusion of free will lost and it becomes a game instead. Finally, a lack of serious consideration in the literary world leads to interactive fiction being on the fringe, leading to mostly trivial works being produced.</p>

<p>For the future of interactive fiction, Howell assumed that we would get more interactive fiction shells like Storyspace and more sophisticated models and algorithms for interactive fiction This would not be enough, however, because we would also need a serious treatment of interactive fiction in education where the students try out experential reading and a literary consideration of interactive fiction by real writers. Commercial interactive fiction would lead to the development of better hardware and user interfaces as well as to marketing concerns. All of this should be a natural for the entertainment industry. Howell felt that we have a responsibility to open up new forms of self-expression for people and therefore called for more research and practice in the field of interactive fiction.</p>

<h2>System-Assisted Customized Browsing</h2>

<p>It is fashionable in the user interface field to talk about allowing users to customize their interfaces. In real life, however, most system either don't allow any customization or are limited to surface changes such as color assignments. Andrew Monk from the University of York had designed a facility for customization in hypertext by the construction of a <strong> personalized browser </strong> which would hold hypertext links to those places in the hypertext network to which the user would want to have easy direct access.</p>

<p>Of course, just providing a facility for a personalized browser is not enough. It must also be easy for users to construct such a browser, since they will otherwise not use the facility. In Monk's design, ease of construction was achieved by having the system unobtrusively <strong> monitor the user's movements </strong> through the hypertext. If the user returned to a specific node frequently enough, the system would assume that that node would be a candidate for inclusion in the personalized browser and would ask the user if the node should be added to the browser. If the user answered yes (a single click), a link to the node would be added to the personalized browser.</p>

<p>One problems with this approach which I raised during the discussion following Monk's presentation was that it is somewhat similar to the principle of "activist help" where the computer interrupts the user's work. An activist interruption may be helpful in situations where the user is having trouble, but it may be disruptive to users in the navigation situation. Unfortunately, Monk did not yet have enough practical experience with his design to be able to tell whether this was indeed a problem or not.</p>

<p>Monk had implemented his design in HyperCard as had so many other speakers at this conference, but Dan Russell asked what would happen in hypertext systems with multiple windows rather than a single frame. In e.g. NoteCards, the user's state could be viewed as consisting of the complete set of currently open windows, so one would want to have a reference to such a "tabletop" from the personal browser. The reference itself would be no problem since they have already implemented a tabletop facility at Xerox PARC, but the monitoring of the user's navigation behavior would be harder since users rarely return to exactly the same configuration of windows. Therefore the simpleminded solution of just counting how many times a given state occurred could not be used to activate the prompting mechanism for adding the state to the browser. After the session I came up with the possible solution of using a clustering algorithm to construct sets of related windows based on their similarities as measured by how frequently they were opened at the same time. The system could then count how many times each cluster was displayed, and the customization could proceed as with Monk's current design.</p>

<p>It is interesting how each new idea in hypertext gives rise to many additional user interface issues that we don't really know how to answer. One more such issue that came up in the discussion of Monk's personalized browser was what would happen in large hypertexts where the browser would tend to accumulate many references and therefore grow unwieldy. The answer seems to be that the user would probably have a current working set of hypertext nodes which were important at any given time, and that it would be those nodes which should be listed in the browser. One approach would be to use methods from virtual memory management and throw away the least recently referenced node if too many links are added to the browser. To stay with the paradigm of a customized interface we should probably require the user to confirm the deletion before it takes place, but it would still be nice to have the system come up with a suggestion for what link to remove.</p>

<h2>Travel Difficulties</h2>

<p>The travel metaphor is popular among hypertext enthusiasts so it gave rise to many jokes that workers at British Rail chose the day before the conference to strike and close down all train service in the U.K. The conference participants from the U.K. knew about the strike in advance and so had planed to drive to York by car. I however, arrived in Manchester Airport from Denmark without expecting any trouble. When I asked the taxi driver to take me to the railway station, he replied "why do you want to go there, there are no trains running today." Of course by then it was too late to get a rental car (completely sold out), so I ended up having to take a taxi all the way from Manchester to York. Luckily taxis in Manchester are of the London model with large, comfortable interiors, so I was able to spend several hours in the taxi reading submitted papers for the <a href="../trip-report-hypertext-89/index.php"> Hypertext'89</a> program committee and typing reviews on my trusty Z88 laptop computer.</p>
</section>
  

  
  <section>
    
        
    
  </section>
  
  
  
  
  

  


  <section>
    <br />
    <p class="share"><b>Share <span class="hide-for-small-down">this article</span>:</b> <a href="https://twitter.com/intent/tweet?url=http://www.nngroup.com/articles/trip-report-hypertext-2/&amp;text=Hypertext'2%20Trip%20Report&amp;via=nngroup">Twitter</a> | <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=http://www.nngroup.com/articles/trip-report-hypertext-2/&amp;title=Hypertext'2%20Trip%20Report&amp;source=Nielsen%20Norman%20Group">LinkedIn</a> | <a href="https://plus.google.com/share?url=http://www.nngroup.com/articles/trip-report-hypertext-2/">Google+</a> | <a href="mailto:?subject=NN/g Article: Hypertext&#39;2 Trip Report&amp;body=http://www.nngroup.com/articles/trip-report-hypertext-2/">Email</a></p>
    <hr />
  </section>

  <footer class="l-subsection">
    
    
    

    <div class="row show-for-small-down related-content">
      
      
      

      
      
        <div class="column small-12 topic-links">All articles about:
          <ul>
            
            <li><a href="../../topic/hypertext/index.php">hypertext</a></li>
            
          </ul>
        </div>
      
      

      
      
      

      
      
      

      
      
      
    </div>

    <div class="hide-for-small-down row related-content">
      <div class="medium-6 columns">
        
        
          <div>
            <h3>Articles</h3>
            <ul class="no-bullet">
              
                
                <li><a href="../architectural-component-hypertext-systems/index.php">Architectural Component of Hypertext Systems</a></li>
                
              
                
                <li><a href="../trip-report-hypertext-89/index.php">Hypertext&#39;89 Trip Report</a></li>
                
              
                
                <li><a href="../trip-report-chi-89/index.php">CHI&#39;89 Trip Report</a></li>
                
              
                
                <li><a href="../trip-report-hyper-hyper-89/index.php">HyperHyper&#39;89 Trip Report</a></li>
                
              
                
                <li><a href="../trip-report-hypertext-87/index.php">Hypertext&#39;87 Trip Report</a></li>
                
              
            </ul>
          </div>
        
        

        

      </div>
      <div class="medium-6 columns">
        
        
      </div>
    </div>
  </footer>
</article>

        </div>

      

    
  </div>
  <div class="row">
    
      <div class="column small-12 show-for-small-down l-navrail announcements">
        
          
        
      </div>
    
    
  </div>

    </div>
    




<footer id="page-footer">
    <div class="row hide-for-print">
        <div class="column small-12 medium-6">
            <section class="footer-popular">
                
                <h1>Popular Topics</h1>
                <div style="float: left;  width: 15em; display: block;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/applications/index.php">Application Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/e-commerce/index.php">E-Commerce Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/eyetracking/index.php">Eyetracking </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/information-architecture/index.php">Information Architecture </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/intranets/index.php">Intranet Design </a></li>
	<li><a href="../../topic/interaction-design/index.php">Interaction Design </a></li>
</ul>
</div>

<div style="float: left;  width: 15em; display: block; list-style: none;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/mobile-and-tablet-design/index.php">Mobile and Tablet Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/credibility-persuasion/index.php">Persuasive Design</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/research-methods/index.php">Research Methods</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/user-testing/index.php">User Testing </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/web-usability/index.php">Web Usability </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/writing-web/index.php">Web Writing &amp; Content Strategy </a></li>
</ul>
</div>

<p>&nbsp;</p>

                
            </section>
        </div>

        <div class="column small-12 medium-6">
            <div class="row">
                <div class="column small-6">
                    <section class="footer-about">
                        <h1>Follow Us</h1>
                        <ul>
	<li><a href="../subscribe/index.php">Subscribe to our weekly UX newsletter</a>,&nbsp;Jakob Nielsen&#39;s Alertbox</li>
	<li>Visit Don Norman&#39;s website, <a href="http://www.jnd.org/"> www.jnd.org </a></li>
	<li>Visit Bruce Tognazzini&#39;s website, <a href="http://www.asktog.com/"> www.asktog.com </a></li>
	<li style="padding-top: 6px;"><a class="twitter-follow-button" data-show-count="false" data-size="large" href="https://twitter.com/nngroup"><img alt="Follow @nngroup on Twitter" height="20" src="https://media.nngroup.com/media/editor/2016/12/30/twitter-follow-button.png" style="width: 117px; height: 20px;" width="117" /></a></li>
	<li style="padding-top: 6px;"><img alt="" height="14" src="https://media.nngroup.com/media/editor/2016/07/26/feed-icon-14x14.png" style="vertical-align:text-top;width:14px;margin-right:.25em;" width="14" /><a href="../../feed/rss/index.php">Subscribe to our RSS feed</a></li>
</ul>

                    </section>
                </div>

                <div class="column small-6">
                    <section class="footer-about right-column">
                        <h1>About
</h1>
                        <ul class="no-bullet">
	<li><a href="../../about/contact/index.php">Contact information</a></li>
	<li><a href="../../about/index.php">About Us</a></li>
	<li><a href="../../about/why-nng/index.php">Why NN/g? </a></li>
	<li><a href="../../return-policy/index.php">Return Policy</a></li>
</ul>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="copyright column small-12">
            <p><a href="../../copyright-and-reprint-info/index.php">Copyright</a> &copy; 1998-2017 Nielsen Norman Group, All Rights Reserved.</p>
        </div>
    </div>
</footer>


    


        <script src="https://media.nngroup.com/static/vendor/fastclick/js/fastclick.min.js"></script>
        <script src="https://media.nngroup.com/static/vendor/foundation/js/foundation.min.js"></script>
        <script src="http://media.nngroup.com/static/js/app.js"></script>
        <script src="http://media.nngroup.com/static/js/scroll-to-fixed.js"></script><script>var externalLinks = document.querySelectorAll('a[href^="http"]');for (var i = externalLinks.length-1; i >= 0; i--) {externalLinks[i].addEventListener("click", function(event) { alert("External links are disabled for this test."); event.preventDefault(); }, false);}var mailLinks = document.querySelectorAll('a[href^="mailto"]');for (var i = mailLinks.length-1; i >= 0; i--) {mailLinks[i].addEventListener("click", function(event) { alert("Email links are disabled for this test."); event.preventDefault(); }, false);}var externalForms = document.querySelectorAll('form');for (var i = externalForms.length-1; i >= 0; i--) {externalForms[i].addEventListener("submit", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false); externalForms[i].addEventListener("click", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false);}</script>

        <script>
            
            $(function() {
                $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
                $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
            });
        </script>

        
<script src="https://media.nngroup.com/static/js/articles.js"></script>


        
        <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1072699960;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072699960/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
    </body>

<!-- Mirrored from www.nngroup.com/articles/trip-report-hypertext-2/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:15:33 GMT -->
</html>
