<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->





    
<!-- Mirrored from www.nngroup.com/articles/ux-prototype-hi-lo-fidelity/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:08:34 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1026.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"licenseKey":"1a30ef9931","errorBeacon":"bam.nr-data.net","applicationID":"2048992","transactionName":"NFxSMRBUX0EDUkIPDA0WdhAMVkVbDV8ZBxEXUFMJB0YfRAtUQRVZIktEDAFZVHYHRVcPD01eVRE=","beacon":"bam.nr-data.net","queueTime":7,"applicationTime":516,"agent":""}</script>
        <title>UX Prototypes: Low Fidelity vs. High Fidelity</title><meta property="og:title" content="UX Prototypes: Low Fidelity vs. High Fidelity" />
  
        
        <meta name="description" content="No matter which prototyping tools you use, the same tips apply to preparing a user interface prototype for the most effective user research.">
        <meta property="og:description" content="No matter which prototyping tools you use, the same tips apply to preparing a user interface prototype for the most effective user research." />
        
  
        
	
        
        <meta name="keywords" content="ux text, usability tests, prototype, prototypes, high-fidelity, low-fidelity, high fidelity, low fidelity, completeness, prototyping, prototyping tool, clckable prototype, paper prototype, wizard of oz, steal the mouse, sketch, early design, iterate design, iterative design, early design feedback, test before code, lean, testing in lean, testing in agile">
        

        

        <meta name="robots" content="noarchive">
        <meta name="viewport" content="width=device-width">

        <link rel="search" type="application/opensearchdescription+xml" title="NN/g Search" href="../../opensearch.xml" />
        <link rel="shortcut icon" href="http://media.nngroup.com/static/img/favicon.ico" />

        <link rel="stylesheet" href="https://media.nngroup.com/static/vendor/font-awesome/css/font-awesome.min.css">

        
        <link rel="stylesheet" href="https://media.nngroup.com/static/css/7232a960f1df.css" type="text/css" />
        <link rel="stylesheet" href="http://fast.fonts.com/t/1.css?apiType=css&amp;projectid=c5846260-9def-46ec-9407-0f1349bd2d64" type="text/css">

        <script src="https://media.nngroup.com/static/vendor/modernizr/js/modernizr.min.js"></script>

        <script type="text/javascript" src="https://media.nngroup.com/static/js/3283be5def74.js"></script>
        

        

        
        
        <!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=229034,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('http://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->


<!-- Google Analytics Tracking Code for http://nngroup.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23132681-1', 'nngroup.com');
  ga('require', 'linkid', 'https://www.nngroup.com/articles/ux-prototype-hi-lo-fidelity/linkid.js');
  ga('require', 'displayfeatures');

// Optimizely Universal Analytics Integration
 window.optimizely = window.optimizely || [];
 window.optimizely.push("activateUniversalAnalytics");

  ga('send', 'pageview');

</script>
<script>
/**
* Function to track a click on an outbound link in Google Analytics.
*/
function trackOutboundLink(label, url) {
    if(window.ga && ga.loaded) {
        ga('send', 'event', 'outbound', 'click', label, {'hitCallback': function(){document.location = url;}
        });
    } else {
        document.location = url;
    }
}
/**
* Function to track a click on a link in the article announcement area.
*/
$('#articleAnnouncement a').on('click', function(){
  ga('send', 'event', 'link', 'click', 'articleAnnouncement');
});
</script>

<script type="text/javascript">
 setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2421.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



<!-- Hotjar Tracking Code for http://nngroup.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:121605,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

        <script src="https://media.nngroup.com/static/js/analytics/track_outbound_link.js"></script>
        
        
        
    </head>
    <body class="location-articles articles-detail">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
    <header id="page-header">
        <div id="cart" class="shopping-cart mobile-cart show-for-small-down"></div>
        <div class="row">
            <div id="masthead" class="masthead small-12 medium-7 column">

                <!-- The button to toggles menu on small screen -->

                <h1><a href="../../index.php"><img src="http://media.nngroup.com/static/img/logo-wordmark.png" width="74" height="30"> Nielsen <span class="norman">Norman</span> Group</a></h1>
                <p class="tagline show-for-medium-up">Evidence-Based User Experience Research, Training, and Consulting</p>

                <!-- The button to toggles form search on small screen -->


                <div style="clear: right;width:  100%;height: 1px;"></div><?php define('ROOT_PATH',$_SERVER["DOCUMENT_ROOT"] . '/apps/navigation_styles/navigations/2'); ?><?php include(ROOT_PATH)."/navigation.php"; ?>

                <!-- Drop down menu on small screen -->
                <div id="hideNav" class="show-for-small-down">
                  
                  <ul class="menu-content">
                    <li><a href="../../index.php">Home</a></li>
                    <li class="has-submenu">
                      <a>Training</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../training/index.php">Overview</a></li>
                        
<li id="nav-ux-conference"><a href="../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../online-seminars/index.php"><span>Online Seminars</span></a></li>

                      </ul>
                    </li>
                    <li class="has-submenu">
                      <a>Consulting</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
  <li><a href="../../consulting/index.php">Overview</a></li>
  
  <li><a href="../../consulting/benchmarking/index.php">Benchmarking</a></li>
  
  <li><a href="../../consulting/expert-design-review/index.php">Expert Design Review</a></li>
  
  <li><a href="../../consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
  
  <li><a href="../../consulting/usability-testing/index.php">Usability Testing </a></li>
  
  <li><a href="../../consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
  
  <li><a href="../../in-house-training/index.php">In-House Training</a></li>
</ul>

                    </li>

                    <li><a href="../../reports/index.php">Reports</a></li>
                    <li><a href="../index.php">Articles</a></li>
                    <li class="has-submenu"><a>About NN/G</a><i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../about/index.php">Overview</a></li>
                        <li><a href="../../people/index.php">People</a></li>
                        <li><a href="../../about/why-nng/index.php">Why NN/g?</a></li>
                        <li><a href="../../about/contact/index.php">Contact</a></li>
                        <li><a href="../../news/index.php">News</a></li>
                        <li><a href="../../about/history/index.php">History</a></li>
                        <li><a href="../../books/index.php">Books</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                </div>

                <hr class="header-divider show-for-small-only"/>


            </div>
       </div>

        <div id="hideNav">            <nav class="primary-nav small-12 column">
                <ul>
                    <li><a class="navitem" id="nav-home" href="../../index.php">Home</a></li>
                    <li><a class="navitem" id="nav-training" href="../../training/index.php">Training</a></li>
                    <li><a class="navitem" id="nav-consulting" href="../../consulting/index.php">Consulting</a></li>
                    <li><a class="navitem" id="nav-reports" href="../../reports/index.php">Reports</a></li>
                    <li><a class="navitem" id="nav-articles" href="../index.php">Articles</a></li>
                    <li><a class="navitem" id="nav-about" href="../../about/index.php">About NN/G</a></li>
                </ul>
            </nav>
            <nav class="secondary-nav small-12 medium-12 large-12 columns">
                
                
            </nav>
        </div>
    </header>
    <div id="main" class="l-content" onclick="closeNav(event)"><span id="preventClose" style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776; Menu</span>
    
  

  

  <div class="row">
    
      
      <section class="small-12 medium-3 columns collapsable-content l-navrail articles-left-column">
        <h1 class="show-for-small-down collapse-link mobile-browse-header">Browse by Topic and Author</h1>
          <div class="row collapsable">

            
            <div class="small-6 medium-12 columns">
              <section id="article-popular-topics" style="display: none;">
                
                <h1>Topics</h1>
                <ul id="article-popular-topics">
                    
                    <li>
                        
                        <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/intranets/all/index.php">Intranets</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/user-testing/all/index.php">User Testing</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                        
                    </li>
                    
                    <li><a href="#" id="article-see-all-topics"><strong>See all topics&hellip;</strong></a></li>
                </ul>
                
              </section>
              <section id="article-all-topics" style="display: none;">
                
                <h1>
                  <span class="show-for-medium-up">All Article Topics</span>
                  <span class="show-for-small-down">All Topics</span>
                  (<a href="#" id="article-hide-all-topics">hide</a>)
                </h1>
                <ul>
                  
                  <li>
                    
                    <a href="../../topic/accessibility/all/index.php">Accessibility</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/agile/all/index.php">Agile</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/analytics-and-metrics/all/index.php">Analytics &amp; Metrics</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/applications/all/index.php">Application Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/b2b-websites/all/index.php">B2B Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/behavior-patterns/all/index.php">Behavior Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/branding/all/index.php">Branding</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/content-strategy/all/index.php">Content Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/corporate-websites/all/index.php">Corporate Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/design-patterns/all/index.php">Design Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-design-process/all/index.php">Design Process</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/email/all/index.php">Email</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/eyetracking/all/index.php">Eyetracking</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/heuristic-evaluation/all/index.php">Heuristic Evaluation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/human-computer-interaction/all/index.php">Human Computer Interaction</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-ideation/all/index.php">Ideation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/information-architecture/all/index.php">Information Architecture</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/interaction-design/all/index.php">Interaction Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/international-users/all/index.php">International Users</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/intranets/all/index.php">Intranets</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-management/all/index.php">Management</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/navigation/all/index.php">Navigation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/non-profit-websites/all/index.php">Non-Profit Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/personas/all/index.php">Personas</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/persuasive-design/all/index.php">Persuasive Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/prototyping/all/index.php">Prototyping</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/psychology-and-ux/all/index.php">Psychology and UX</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/research-methods/all/index.php">Research Methods</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/search/all/index.php">Search</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/social-media/all/index.php">Social Media</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/strategy/all/index.php">Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/user-testing/all/index.php">User Testing</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-teams/all/index.php">UX Teams</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/visual-design/all/index.php">Visual Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/writing-web/all/index.php">Writing for the Web</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/young-users/all/index.php">Young Users</a>
                    
                  </li>
                  
                </ul>
                
              </section>
            </div>
            <script>
              /* Determine which topics list will be displayed: the full or the
               * partial.  We are inlining this close to the list itself to
               * prevent rendering delays on the client
               */
              var topic_list = $.cookie("article-topics");

              var popular_topics = $('#article-popular-topics');
              var all_topics = $('#article-all-topics');

              if(topic_list == 1) {
                  popular_topics.hide();
                  all_topics.show();
              } else {
                  popular_topics.show();
                  all_topics.hide();
              }
            </script>
            

            
            <div class="small-6 medium-12 columns">
              
              <section id="article-initial-authors">
                  <h1>Author</h1>
                  <ul>
                    
                    <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                    
                    <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                    
                    <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                    
                    <li><a href="#" onClick="$('#article-initial-authors').hide(); $('#article-all-authors').show();"><strong>See all authors&hellip;</strong></a></li>
                  </ul>
              </section>
              <section id="article-all-authors" style="display: none;">
                <h1>
                  All Authors
                  (<a href="#" id="article-hide-all-authors">hide</a>)
                </h1>
                <ul>
                  
                  <li><a href="../author/raluca-budiu/index.php">Raluca Budiu</a></li>
                  
                  <li><a href="../author/patty-caya/index.php">Patty Caya</a></li>
                  
                  <li><a href="../author/susan-farrell/index.php">Susan Farrell</a></li>
                  
                  <li><a href="../author/therese-fessenden/index.php">Therese Fessenden</a></li>
                  
                  <li><a href="../author/kim-flaherty/index.php">Kim Flaherty</a></li>
                  
                  <li><a href="../author/sarah-gibbons/index.php">Sarah Gibbons</a></li>
                  
                  <li><a href="../author/aurora-harley/index.php">Aurora Harley</a></li>
                  
                  <li><a href="../author/kate-kaplan/index.php">Kate Kaplan</a></li>
                  
                  <li><a href="../author/page-laubheimer/index.php">Page Laubheimer</a></li>
                  
                  <li><a href="../author/angela-li/index.php">Angie Li</a></li>
                  
                  <li><a href="../author/hoa-loranger/index.php">Hoa Loranger</a></li>
                  
                  <li><a href="../author/kate-meyer/index.php">Kate Meyer</a></li>
                  
                  <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                  
                  <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                  
                  <li><a href="../author/kara-pernice/index.php">Kara Pernice</a></li>
                  
                  <li><a href="../author/christian-rohrer/index.php">Christian Rohrer</a></li>
                  
                  <li><a href="../author/amy-schade/index.php">Amy Schade</a></li>
                  
                  <li><a href="../author/katie-sherwin/index.php">Katie Sherwin</a></li>
                  
                  <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                  
                  <li><a href="../author/kathryn-whitenton/index.php">Kathryn Whitenton</a></li>
                  
                </ul>
              </section>
              
            </div>
            
          </div>

          
          <section class="hide-for-small-down">
            
              
              <h1>Recent Articles</h1>
              
              <ul>
                  
                  <li><a href="../seamless-cross-channel/index.php">Seamlessness in the Omnichannel User Experience</a></li>
                  
                  <li><a href="../brand-intention-interpretation/index.php">Experience Design: Bridging Brand Intention and Brand Interpretation</a></li>
                  
                  <li><a href="../eyetracking-tasks-efficient-scanning/index.php">Scanning Patterns on the Web Are Optimized for the Current Task</a></li>
                  
                  <li><a href="../flat-design-best-practices/index.php">Flat-Design Best Practices</a></li>
                  
                  <li><a href="../ux-user-stories/index.php">Accounting for UX Work with User Stories in Agile Projects</a></li>
                  
                  <li><a href="../index.php"><strong>See all articles&hellip;</strong></a></li>
              </ul>
              
            
          </section>
          

          
          
          <section class="hide-for-small-down">
            <h1>Popular Articles</h1>
            
            <ul>
                
                <li><a href="../usability-101-introduction-to-usability/index.php">Usability 101: Introduction to Usability</a></li>
                
                <li><a href="../top-10-mistakes-web-design/index.php">Top 10 Mistakes in Web Design</a></li>
                
                <li><a href="../how-users-read-on-the-web/index.php">How Users Read on the Web</a></li>
                
                <li><a href="../f-shaped-pattern-reading-web-content/index.php">F-Shaped Pattern For Reading Web Content</a></li>
                
                <li><a href="../ten-usability-heuristics/index.php">10 Usability Heuristics for User Interface Design</a></li>
                
                <li><a href="../intranet-design-2016/index.php">10 Best Intranets of 2016</a></li>
                
                <li><a href="../which-ux-research-methods/index.php">When to Use Which User-Experience Research Methods</a></li>
                
                <li><a href="../response-times-3-important-limits/index.php">Response Times: The 3 Important Limits</a></li>
                
                <li><a href="../why-you-only-need-to-test-with-5-users/index.php">Why You Only Need to Test with 5 Users</a></li>
                
                <li><a href="../page-fold-manifesto/index.php">The Fold Manifesto: Why the Page Fold Still Matters</a></li>
                
            </ul>
          </section>
          
          

          
            <section class="hide-for-small-down">
              
            </section>
          

      </section>
      

      
        <div class="small-12 medium-9 columns content-body">
          




<article class="l-subsection alertbox-article-content article-detail-bl">
  
  <header>
    <div class="l-subsection">
      <h1>UX Prototypes: Low Fidelity vs. High Fidelity</h1>
      <div class="author-meta">by
        
          
            
            <a href="../author/kara-pernice/index.php">Kara Pernice</a>
            
          
        on  December 18, 2016
        
      </div>

      

      
      <div class="topic-links">
        Topics:
        
          <ul>

  <li><a href="../../topic/prototyping/index.php">Prototyping</a></li>

  <li><a href="../../topic/user-testing/index.php">User Testing</a></li>

  <li><a href="../../topic/agile/index.php">Agile</a></li>

  <li><a href="../../topic/research-methods/index.php">Research Methods</a></li>

</ul>
        
      </div>
      
    </div>
  </header>
  

  
  
  <section>
    <p class="lede"><strong>Summary:</strong> Clickable or static? Axure or paper? No matter which prototyping tools you use, the same tips apply to preparing a user interface prototype for the most effective user research.</p>
    <br />
  </section>
  
  

  
  <section id="article-body"><h2>What Is a Testable Prototype?</h2>

<p>A user interface prototype is a hypothesis — a candidate design solution that you consider for a specific design problem. The most straightforward way to test this hypothesis is to watch users work with it.</p>

<p>There are many types of prototypes, ranging anywhere between any of these pairs of extremes:</p>

<ul>
	<li>Single page vs. multipage with enough menus, screens, and click targets that the user can completely finish a task</li>
	<li>Realistic and detailed vs. hand-sketched on a piece of paper</li>
	<li>Interactive (clickable) vs. static (requiring a person to manipulate different pages and act as a “computer”)</li>
</ul>

<p>The choice of prototype will vary greatly depending on goals of the testing, completeness of the design, tools used to create the prototype, and resources available to help before and during the usability tests. But, whatever prototype you may use, testing it will help you learn about users' interactions and reactions, so you can improve the design.</p>

<h2>Why Test a Prototype?</h2>

<p>Ripping up code is very expensive. Ripping up a prototype is not, especially if it’s just a piece of paper.</p>

<p>Let’s first consider common arguments for <strong> not </strong> testing a prototype. These are:</p>

<ul>
	<li>Waiting for a design to be implemented before you test it means that the design actually works, and test participants can use it in a natural way.</li>
	<li>There is no adjustment to be made to the <a href="../ux-success-agile/index.php">Agile</a> or Waterfall processes to accommodate UX and iterative design.</li>
	<li>Some Lean proponents say that, with no prototype testing, there is no prototype to throw away when it (inevitably) tests badly, so there is no waste.</li>
</ul>

<p>These arguments may seem good at first glance. But in reality, <strong> testing final products is uninformed and risky</strong>. Enlightened teams create prototypes, have users test them, and iterate the design until it’s good enough. (Note: We also test final products to benchmark the usability at the end of a project or at the beginning of a new one, to assess <a href="../return-on-investment-for-usability/index.php"> ROI</a>, to run <a href="../redesign-competitive-testing/index.php"> competitive studies</a>, and to do a final check and make small tweaks.)</p>

<h2>Interactive vs. Static Prototypes</h2>

<p>Work needs to be done to bring a prototype to life for usability testing. To make it respond to user actions, we can spend time implementing the interaction before the test or we can “fake” the interaction during the test. Both methods have benefits and drawbacks.</p>

<h3>Interactive (Clickable) Prototypes</h3>

<p>With interactive prototypes, the designer must set a response for each possible user action before the test happens. Even with the right tool, building an interactive prototype can be time consuming: you have to get all the click targets right, and make the system respond only when the user interacts with a clickable target.</p>

<h3>Static Prototypes</h3>

<p>With static prototypes, the responses to users’ actions are given in real-time during the test by a person who is familiar with the design. There are several methods that can be used with static prototypes:</p>

<ul>
	<li><strong>Wizard of Oz. </strong> This method is named after the famous Frank Baum book (and more famous movie) with the same name. (If you’re not familiar with the book or the movie: in it, the great Wizard of Oz is impersonated by an ordinary human hiding behind a curtain.) The “wizard” (the designer or someone else familiar with the design) controls the user’s screen remotely from another room. None of the user’s clicks or taps really do anything. Rather, when the user clicks, the “wizard” decides what should come next, then serves up that page to the user’s screen. The “wizard” may even create the page on-the-fly and serve it up. Users don’t know what produces the response. In fact, they are usually told very little beyond that the system is “unfinished and slow.”

	<p>Wizard of Oz testing is particularly useful for testing AI-based systems before you have implemented the artificial intelligence. The human who controls the computer can simulate the AI responses based on natural intelligence.</p>
	</li>
	<li><strong>Paper-Prototype “Computer.” </strong> The design is <a href="../paper-prototyping/index.php"> created on paper</a>. A person who knows the design well plays the part of the “computer” and lays the papers out on a table, near the user’s test table but not in her line of sight. As the user taps with the finger on a paper “screen” in front of her, the “computer” picks up the page (or modular part) representing the response and places it in front of the user. (In this article, we use the notation of “computer” to refer to the human who’s implementing the user interface during the test session.)
	<p><strong>TIPS: </strong></p>

	<ol>
		<li>The “computer” should indicate to the users when “it” has finished working and they can proceed with the interaction. This can be done either by using a designated gesture consistently (e.g., hands folded in front of you) or by using a special “Working” or hourglass-icon printout that is shown to the users while the “computer” is looking for the appropriate response and that is removed as soon as the “computer” has finished working.</li>
		<li>The facilitator should avoid overexplaining the design elements or the process.</li>
	</ol>
	</li>
	<li><strong>Steal-the-Mouse “Computer.” </strong> This method is a version of the Wizard of Oz technique in which the “wizard” is in the same room with the user. (The “wizard’s” role could be played by the facilitator.) The prototype is shown to the user on a computer screen. As the user clicks, the facilitator asks the user to look away from the monitor for a moment and the “wizard” navigates to the page that should appear next. The user is then prompted to look at the monitor and continue.</li>
</ul>

<p><em>Criteria to help you decide which type of prototype is right for your project:</em></p>

<p style="text-align:center"><img alt="Visualization of whether to use a clickable or static prototype" height="782" src="https://media.nngroup.com/media/editor/2016/12/04/staticorclickableprototype5-16.png" width="360"/></p>

<p><br/>
The <em>fidelity</em> of the prototype refers to <em>how closely it matches</em> the look-and-feel of the final system. Fidelity can vary in the areas of:</p>

<ul>
	<li>Interactivity</li>
	<li>Visuals</li>
	<li>Content and commands</li>
</ul>

<p>A prototype may have high or low fidelity in all or some of the above 3 areas. The table below explains what high and low fidelity mean in each of these areas.</p>

<table align="center" border="1" cellpadding="3" cellspacing="1" style="width:500px;">
	<tbody>
		<tr>
			<td style="width: 70px; background-color: #c00;"> </td>
			<td style="background-color: #c00; width: 70px; text-align: center;color:#fff;"><strong>HIGH-FIDELITY PROTOTYPE</strong></td>
			<td style="background-color: #c00; width: 70px; text-align: center;color:#fff;"><strong>LOW-FIDELITY<br/>
			PROTOTYPE</strong></td>
		</tr>
		<tr>
			<td colspan="3" style="background-color: rgb(204, 204, 204); text-align: center;"><strong>Interactivity</strong></td>
		</tr>
		<tr>
			<td style="text-align: left; vertical-align: top;"><strong>Clickable links and menus</strong></td>
			<td style="width: 75px; text-align: left; vertical-align: top;">
			<p>Yes: Many or all are clickable.</p>
			</td>
			<td style="width: 75px; text-align: left; vertical-align: top;">
			<p>No: Targets do not work.</p>
			</td>
		</tr>
		<tr>
			<td style="text-align: left; vertical-align: top;"><strong>Automatic response to user’s actions</strong></td>
			<td style="text-align: left; vertical-align: top;">
			<p>Yes: Links in the prototype are made to work via a prototyping tool (e.g., InVision, PowerPoint).</p>
			</td>
			<td style="text-align: left; vertical-align: top;">
			<p>No: Screens are presented to the user in real time by a person playing “the computer.”</p>
			</td>
		</tr>
		<tr>
			<td colspan="3" style="text-align: center; background-color: rgb(204, 204, 204);"><strong>Visuals</strong></td>
		</tr>
		<tr>
			<td style="text-align: left; vertical-align: top;"><strong>Realistic visual hierarchy, priority of screen elements, and screen size</strong></td>
			<td style="text-align: left; vertical-align: top;">
			<p>Yes: Graphics, spacing, and layout look like a live system would look (even if the prototype is presented on paper).</p>
			</td>
			<td style="text-align: left; vertical-align: top;">
			<p>No: Only some or none of the visual attributes of the final live system are captured (e.g., a black-and-white sketch or wireframe, schematic representation of images and graphics, single sheet of paper for several screenfuls of information). Spacing and element prioritization may or may not be preserved.</p>
			</td>
		</tr>
		<tr>
			<td colspan="3" style="text-align: center; background-color: rgb(204, 204, 204);"><strong>Content and Navigation Hierarchy</strong></td>
		</tr>
		<tr>
			<td style="text-align: left; vertical-align: top;"><strong>Content</strong></td>
			<td style="text-align: left; vertical-align: top;">
			<p>Yes: The prototype includes all the content that would appear in the final design (e.g., full articles, product-description text and images).</p>
			</td>
			<td style="text-align: left; vertical-align: top;">
			<p>No: The prototype includes only a summary of the content or a stand-in for product images.</p>
			</td>
		</tr>
	</tbody>
</table>

<h2>Benefits of High-Fidelity Prototypes</h2>

<ol>
	<li><strong>Prototypes with high-fidelity interactivity have realistic (faster) system response during the test. </strong> Sometimes it can take extra time for the person playing the computer, whether online or on paper, to find the right screen and respond to a user’s click. Too long of a lag between user’s action and the “computer’s” response can break the users’ flow and make them forget what they did last or expected to see next.

	<p>A delay also gives users extra time to study the current page. So, with a slow prototype, usability-test participants may notice more design details or absorb more content than they normally would with a live system.</p>

	<p><strong>TIP:</strong> If the page supposed to appear next is hard to find in a paper prototype or slow to load in a clickable prototype, take away the current screen the user is looking at, so she is instead looking at a blank page or area. When the next page is ready, first display the previous page for a few moments again so the user can get her bearings, then replace that screen with the next one. The test facilitator can help this process by saying just a few words to help the user recover the context — for example, “Just a recap, you clicked <em> About Us.” </em></p>
	</li>
	<li><strong>With high-fidelity interactivity and/or visuals, you can test workflow, specific UI components (e.g. <a href="../mega-menus-work-well/index.php"> mega menus</a>, <a href="../mobile-accordions/index.php"> accordions</a>), graphical elements</strong> such as affordance, page hierarchy, <a href="../legibility-readability-comprehension/index.php">type legibility</a>, image quality, as well as engagement.</li>
	<li><strong>High-fidelity prototypes often look like “live” software to users.</strong> This means that test participants will be more likely to behave realistically, as if they were interacting with a real system, whereas with a sketchy prototype they may have unclear expectations about what is supposed to work and what isn’t. (Though it’s amazing how <a href="../authentic-behavior-in-user-testing/index.php"> strong the suspension of disbelief</a> is for many users in test situations where not everything is real.)</li>
	<li><strong>High-fidelity interactivity frees the designer to focus on observing the test instead of thinking about what should come next.</strong> Nobody needs to worry during the test about making the prototype work.</li>
	<li><strong>Interactive-prototype testing is less likely to be affected by human error.</strong> With a static prototype, there is a lot of pressure on the “computer” and a fair chance of making a mistake. Rushing, stress, nerves, paying close attention to user clicks, and navigating through a stack of papers can all make the “computer” panic or just slip during the test.</li>
</ol>

<h2>Benefits of Low-Fidelity Prototypes</h2>

<ol>
	<li><strong>Less time to prepare a static prototype, more time to work on design, before the test.</strong> Creating a clickable prototype takes time. Without having to make the prototype work, you can spend more time on designing more pages, menus, or content. (You still need to organize pages before the test so the “computer” can easily find the right one to present. But doing this is usually a lot faster than preparing a clickable prototype.)</li>
	<li><strong>You can make design changes more easily during the test.</strong> A designer can <a href="../design-charrettes/index.php"> sketch a quick response</a>, and erase or change part of design between test sessions (or during a session) without worrying about linking the new page in the interactive prototype.</li>
	<li><strong>Low-fidelity prototypes put less pressure on users.</strong> If a design seems incomplete, users usually have no idea whether it took a minute or months to create it. They may better understand that you are indeed testing the design and not them, feel less obliged to be successful, and be more likely to express negative reactions.</li>
	<li><strong>Designers feel less wedded to low-fidelity prototypes.</strong> Designers are more likely to want to change a sketchy design than one with full interaction and aesthetics. Once we invest more time and sweat in a design, it’s harder to give it up if it does not work well.</li>
	<li><strong>Stakeholders recognize that the work isn’t finished yet.</strong> When people see a rough prototype, they don’t expect it to ship tomorrow. Everybody on the team will expect changes before the design is finalized. (In contrast, when a design looks very polished, it’s easy for an executive to fall into the trap of saying, “this looks good, let’s make it go live now.”)</li>
</ol>

<h2>Interaction with the User During Any Prototype Test</h2>

<p>In prototype tests, facilitators often talk a bit more with participants than they do in tests of live systems, mainly for the following good reasons:</p>

<ul>
	<li>They need to explain the nature of the prototype medium (not how the design itself works) to the user, before the study starts.</li>
	<li>They occasionally may need to explain the state of the system (e.g., “This page doesn’t work yet”) and ask “What were you expecting to happen?”</li>
	<li>They may have to find out whether users who sit idle are waiting for a response (from a slow system) or think that the task is completed.</li>
</ul>

<p>Even with the above necessary interactions between the test facilitator and the user, the test facilitator’s ultimate goal should be to <a href="../first-rule-of-usability-dont-listen-to-users/index.php"><strong><em>quietly observe</em></strong> the person interacting</a> with the design, <a href="../talking-to-users/index.php">not to have a conversation with the participant</a>.</p>

<p><strong>TIPS:</strong></p>

<ol>
	<li>If users click an item for which there is no prepared response yet:
	<ul>
		<li><strong>Say: “ </strong> That isn’t working.”</li>
		<li><strong>Ask: </strong> “What were you expecting to happen when you clicked that?”</li>
		<li>Present the second-best page if there is one, and say something as an explanation. For example, “You clicked the <em>compact cars</em> link. We don’t have those screens today. So please pretend you clicked <em>midsize cars.</em> Okay?” After the user confirms, present the midsize-cars page. Then say as little as possible, and stay neutral.</li>
	</ul>
	</li>
	<li>If the wrong page appears after the user clicks, the “computer” should take that page away as soon as possible and revert to the previous page. The facilitator should tell the user immediately that the page was wrong, then repeat what the user did on the current page, “You tapped <em>About Us.”</em> Then the “computer” presents the right page.</li>
</ol>

<h2>“Computer” Errors Have a Negative Impact</h2>

<p>Note that “computer” errors can seriously impact the test. As screens appear, users form a <a href="../mental-models/index.php">mental model</a> of how the system and the research method work. If the wrong page appears, don’t assume that you can make users forget what they just saw. (Brain wipes only work in Star Trek.) Even if you take the screen away or try to explain the error, users may infer that the wrong screen is related to the task or get some more knowledge from your explanation, and may be later influenced by that information. Seeing the wrong page also breaks the users’ flow, and can confuse them. Finally, later in the test, if a screen appears that is unexpected, they might think the prototype is just malfunctioning again. This impacts the users’ expectations, trust in the research method, and ability to form a consistent mental model.</p>

<p>Since computer errors can negatively impact the study, take the time to <a href="../pilot-testing/index.php">pilot test</a> and fix issues with the prototype before any sessions occur.</p>

<h2>Conclusion</h2>

<p>Make no mistake: You cannot afford to not test prototypes. Your design will be tested, whether you plan for it or not. Once your system goes live and people begin to use it, they are testing it. And rather than collecting feedback in a low-risk research setting, where you can learn, and then react and change the design, you’ll have actual unhappy customers on your hands. With them will come a litany of problems such as lost sales, abandoned orders, lack of understanding of content and products, alienation due to poor tone of voice, returned products, increased support calls, increased training costs, social shares of bad experiences, low <a href="../nps-ux/index.php">Net Promoter Scores</a>, and <a href="../brand-experience-ux/index.php">brand abandonment</a>. The business will have to figure out how to fix all these. The development team will react by scrambling to fix the design, taking out working code, visuals, and content, and trading it for rushed, only marginally better replacements. All will come at a great cost. Redesigning, taking code out, coding again with the new design, quality testing that code, and, if applicable, changing marketing and documentation materials, is far more expensive than discarding a prototype.</p>

<p>Test prototypes, whether clickable or static, whether high- or low-fidelity. Aim to learn how to change and improve your design. That way, your customers will never see your design failures.</p>

<p> </p>

<p><em>Read the companion article: <a href="../ia-view-prototype/index.php">IA-Based View of Prototype Fidelity</a>.</em></p>

<p>For more information about preparing testable prototypes, get our <a href="../../reports/paper-prototyping-training-video/index.php">Paper Prototyping training video</a>, or attend our <a href="../../courses/wireframing-and-prototyping/index.php">“Wireframing and Prototyping” course</a>.</p>
</section>
  

  
  <section>
    
        
    
  </section>
  
  
  
  
  

  


  <section>
    <br />
    <p class="share"><b>Share <span class="hide-for-small-down">this article</span>:</b> <a href="https://twitter.com/intent/tweet?url=http://www.nngroup.com/articles/ux-prototype-hi-lo-fidelity/&amp;text=UX%20Prototypes:%20Low%20Fidelity%20vs.%20High%20Fidelity&amp;via=nngroup">Twitter</a> | <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=http://www.nngroup.com/articles/ux-prototype-hi-lo-fidelity/&amp;title=UX%20Prototypes:%20Low%20Fidelity%20vs.%20High%20Fidelity&amp;source=Nielsen%20Norman%20Group">LinkedIn</a> | <a href="https://plus.google.com/share?url=http://www.nngroup.com/articles/ux-prototype-hi-lo-fidelity/">Google+</a> | <a href="mailto:?subject=NN/g Article: UX Prototypes: Low Fidelity vs. High Fidelity&amp;body=http://www.nngroup.com/articles/ux-prototype-hi-lo-fidelity/">Email</a></p>
    <hr />
  </section>

  <footer class="l-subsection">
    
    
    <div class="learn-more">
      <h2>Learn More</h2>
      <p><a href="../subscribe/index.php">Subscribe to the weekly newsletter</a> to get notified about future articles.</p>
      </div>
    
    

    <div class="row show-for-small-down related-content">
      
      
      

      
      
        <div class="column small-12 topic-links">All articles about:
          <ul>
            
            <li><a href="../../topic/agile/index.php">Agile</a></li>
            
            <li><a href="../../topic/facilitating-research/index.php">facilitating research</a></li>
            
            <li><a href="../../topic/prototyping/index.php">Prototyping</a></li>
            
            <li><a href="../../topic/research-methods/index.php">Research Methods</a></li>
            
            <li><a href="../../topic/user-centered-design/index.php">User-centered design</a></li>
            
            <li><a href="../../topic/user-testing/index.php">User Testing</a></li>
            
          </ul>
        </div>
      
      

      
      
        <div class="column small-12">
          <h3>UX Conference Training Courses</h3>
           <ul class="no-bullet">
    
        <li><a href="../../courses/ux-basic-training/index.php">UX Basic Training</a></li>
    
        <li><a href="../../courses/journey-mapping/index.php">Journey Mapping to Understand Customer Needs</a></li>
    
        <li><a href="../../courses/measuring-ux/index.php">Measuring User Experience</a></li>
    
        <li><a href="../../courses/analytics-and-user-experience/index.php">Analytics and User Experience</a></li>
    
        <li><a href="../../courses/ux-deliverables/index.php">UX Deliverables</a></li>
    
</ul>
        </div>
      
      

      
      
        <div class="small-12 column">
          <h3>Research Reports</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../reports/paper-prototyping-training-video/index.php">Paper Prototyping Training Video</a></li>
              
            
              
                <li><a href="../../reports/agile-development-user-experience/index.php"> Effective Agile UX Product Development</a></li>
              
            
              
                <li><a href="../../reports/user-experience-careers/index.php">User Experience Careers</a></li>
              
            
              
                <li><a href="../../reports/how-to-conduct-eyetracking-studies/index.php">How to Conduct Eyetracking Studies</a></li>
              
            
              
                <li><a href="../../reports/how-to-recruit-participants-usability-studies/index.php">How to Recruit Participants for Usability Studies</a></li>
              
            
          </ul>
        </div>
      
      

      
      
        <div class="small-12 column">
          <h3>Online Seminars</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../online-seminars/design-thinking-agile/index.php">Design Thinking in Agile</a></li>
              
            
              
                <li><a href="../../online-seminars/ux-real-world/index.php">UX in the Real World</a></li>
              
            
              
                <li><a href="../../online-seminars/mobile-user-testing/index.php">Mobile User Testing</a></li>
              
            
          </ul>
        </div>
      
      
    </div>

    <div class="hide-for-small-down row related-content">
      <div class="medium-6 columns">
        
        
          <div>
            <h3>Articles</h3>
            <ul class="no-bullet">
              
                
                <li><a href="../collaborating-stakeholders/index.php">How to Collaborate with Stakeholders in UX Research</a></li>
                
              
                
                <li><a href="../tips-user-research-field/index.php">27 Tips and Tricks for Conducting Successful User Research in the Field</a></li>
                
              
                
                <li><a href="../mozilla-paper-prototype/index.php">Test Paper Prototypes to Save Time and Money: The Mozilla Case Study</a></li>
                
              
                
                <li><a href="../paper-prototyping/index.php">Paper Prototyping: Getting User Data Before You Code</a></li>
                
              
                
                <li><a href="../testing-whether-web-page-templates-are-helpful/index.php">Testing Whether Web Page Templates are Helpful</a></li>
                
              
            </ul>
          </div>
        
        

        
          
          <div>
            <h3>Research Reports</h3>
            <ul class="no-bullet">
              
                
                  <li><a href="../../reports/paper-prototyping-training-video/index.php">Paper Prototyping Training Video</a></li>
                
              
                
                  <li><a href="../../reports/agile-development-user-experience/index.php"> Effective Agile UX Product Development</a></li>
                
              
                
                  <li><a href="../../reports/user-experience-careers/index.php">User Experience Careers</a></li>
                
              
                
                  <li><a href="../../reports/how-to-conduct-eyetracking-studies/index.php">How to Conduct Eyetracking Studies</a></li>
                
              
                
                  <li><a href="../../reports/how-to-recruit-participants-usability-studies/index.php">How to Recruit Participants for Usability Studies</a></li>
                
              
            </ul>
          </div>
          
        

      </div>
      <div class="medium-6 columns">
        
          
          <div>
            <h3>UX Conference Training Courses</h3>
             <ul class="no-bullet">
    
        <li><a href="../../courses/ux-basic-training/index.php">UX Basic Training</a></li>
    
        <li><a href="../../courses/journey-mapping/index.php">Journey Mapping to Understand Customer Needs</a></li>
    
        <li><a href="../../courses/measuring-ux/index.php">Measuring User Experience</a></li>
    
        <li><a href="../../courses/analytics-and-user-experience/index.php">Analytics and User Experience</a></li>
    
        <li><a href="../../courses/ux-deliverables/index.php">UX Deliverables</a></li>
    
</ul>
          </div>
          
        
        
          
          <div>
            <h3>Online Seminars</h3>
            <ul class="no-bullet">
            
                <li><a href="../../online-seminars/design-thinking-agile/index.php">Design Thinking in Agile</a></li>
            
                <li><a href="../../online-seminars/ux-real-world/index.php">UX in the Real World</a></li>
            
                <li><a href="../../online-seminars/mobile-user-testing/index.php">Mobile User Testing</a></li>
            
            </ul>
          </div>
          
        
      </div>
    </div>
  </footer>
</article>

        </div>

      

    
  </div>
  <div class="row">
    
      <div class="column small-12 show-for-small-down l-navrail announcements">
        
          
        
      </div>
    
    
  </div>

    </div>
    




<footer id="page-footer">
    <div class="row hide-for-print">
        <div class="column small-12 medium-6">
            <section class="footer-popular">
                
                <h1>Popular Topics</h1>
                <div style="float: left;  width: 15em; display: block;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/applications/index.php">Application Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/e-commerce/index.php">E-Commerce Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/eyetracking/index.php">Eyetracking </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/information-architecture/index.php">Information Architecture </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/intranets/index.php">Intranet Design </a></li>
	<li><a href="../../topic/interaction-design/index.php">Interaction Design </a></li>
</ul>
</div>

<div style="float: left;  width: 15em; display: block; list-style: none;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/mobile-and-tablet-design/index.php">Mobile and Tablet Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/credibility-persuasion/index.php">Persuasive Design</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/research-methods/index.php">Research Methods</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/user-testing/index.php">User Testing </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/web-usability/index.php">Web Usability </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/writing-web/index.php">Web Writing &amp; Content Strategy </a></li>
</ul>
</div>

<p>&nbsp;</p>

                
            </section>
        </div>

        <div class="column small-12 medium-6">
            <div class="row">
                <div class="column small-6">
                    <section class="footer-about">
                        <h1>Follow Us</h1>
                        <ul>
	<li><a href="../subscribe/index.php">Subscribe to our weekly UX newsletter</a>,&nbsp;Jakob Nielsen&#39;s Alertbox</li>
	<li>Visit Don Norman&#39;s website, <a href="http://www.jnd.org/"> www.jnd.org </a></li>
	<li>Visit Bruce Tognazzini&#39;s website, <a href="http://www.asktog.com/"> www.asktog.com </a></li>
	<li style="padding-top: 6px;"><a class="twitter-follow-button" data-show-count="false" data-size="large" href="https://twitter.com/nngroup"><img alt="Follow @nngroup on Twitter" height="20" src="https://media.nngroup.com/media/editor/2016/12/30/twitter-follow-button.png" style="width: 117px; height: 20px;" width="117" /></a></li>
	<li style="padding-top: 6px;"><img alt="" height="14" src="https://media.nngroup.com/media/editor/2016/07/26/feed-icon-14x14.png" style="vertical-align:text-top;width:14px;margin-right:.25em;" width="14" /><a href="../../feed/rss/index.php">Subscribe to our RSS feed</a></li>
</ul>

                    </section>
                </div>

                <div class="column small-6">
                    <section class="footer-about right-column">
                        <h1>About
</h1>
                        <ul class="no-bullet">
	<li><a href="../../about/contact/index.php">Contact information</a></li>
	<li><a href="../../about/index.php">About Us</a></li>
	<li><a href="../../about/why-nng/index.php">Why NN/g? </a></li>
	<li><a href="../../return-policy/index.php">Return Policy</a></li>
</ul>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="copyright column small-12">
            <p><a href="../../copyright-and-reprint-info/index.php">Copyright</a> &copy; 1998-2017 Nielsen Norman Group, All Rights Reserved.</p>
        </div>
    </div>
</footer>


    


        <script src="https://media.nngroup.com/static/vendor/fastclick/js/fastclick.min.js"></script>
        <script src="https://media.nngroup.com/static/vendor/foundation/js/foundation.min.js"></script>
        <script src="http://media.nngroup.com/static/js/app.js"></script>
        <script src="http://media.nngroup.com/static/js/scroll-to-fixed.js"></script><script>var externalLinks = document.querySelectorAll('a[href^="http"]');for (var i = externalLinks.length-1; i >= 0; i--) {externalLinks[i].addEventListener("click", function(event) { alert("External links are disabled for this test."); event.preventDefault(); }, false);}var mailLinks = document.querySelectorAll('a[href^="mailto"]');for (var i = mailLinks.length-1; i >= 0; i--) {mailLinks[i].addEventListener("click", function(event) { alert("Email links are disabled for this test."); event.preventDefault(); }, false);}var externalForms = document.querySelectorAll('form');for (var i = externalForms.length-1; i >= 0; i--) {externalForms[i].addEventListener("submit", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false); externalForms[i].addEventListener("click", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false);}</script>

        <script>
            
            $(function() {
                $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
                $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
            });
        </script>

        
<script src="https://media.nngroup.com/static/js/articles.js"></script>


        
        <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1072699960;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072699960/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
    </body>

<!-- Mirrored from www.nngroup.com/articles/ux-prototype-hi-lo-fidelity/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:08:34 GMT -->
</html>
