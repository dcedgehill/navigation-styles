<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->





    
<!-- Mirrored from www.nngroup.com/articles/concise-scannable-and-objective-how-to-write-for-the-web/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:09:32 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1026.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"licenseKey":"1a30ef9931","errorBeacon":"bam.nr-data.net","applicationID":"2048992","transactionName":"NFxSMRBUX0EDUkIPDA0WdhAMVkVbDV8ZBxEXUFMJB0YfRAtUQRVZIktEDAFZVHYHRVcPD01eVRE=","beacon":"bam.nr-data.net","queueTime":2,"applicationTime":334,"agent":""}</script>
        <title>Concise, SCANNABLE, and Objective: How to Write for the Web  </title><meta property="og:title" content="Concise, SCANNABLE, and Objective: How to Write for the Web  " />
  
        
        <meta name="description" content="Users don&#39;t read Web pages; they scan. Concise text improved usability by 58%, SCANNABLE layout added 47%, and objective language added 27% to measured usability; combining all three improvements gave 124%.">
        <meta property="og:description" content="Users don&#39;t read Web pages; they scan. Concise text improved usability by 58%, SCANNABLE layout added 47%, and objective language added 27% to measured usability; combining all three improvements gave 124%." />
        
  
        
	
        
        <meta name="keywords" content="WWW, World Wide Web, writing, reading, page design">
        

        

        <meta name="robots" content="noarchive">
        <meta name="viewport" content="width=device-width">

        <link rel="search" type="application/opensearchdescription+xml" title="NN/g Search" href="../../opensearch.xml" />
        <link rel="shortcut icon" href="http://media.nngroup.com/static/img/favicon.ico" />

        <link rel="stylesheet" href="https://media.nngroup.com/static/vendor/font-awesome/css/font-awesome.min.css">

        
        <link rel="stylesheet" href="https://media.nngroup.com/static/css/7232a960f1df.css" type="text/css" />
        <link rel="stylesheet" href="http://fast.fonts.com/t/1.css?apiType=css&amp;projectid=c5846260-9def-46ec-9407-0f1349bd2d64" type="text/css">

        <script src="https://media.nngroup.com/static/vendor/modernizr/js/modernizr.min.js"></script>

        <script type="text/javascript" src="https://media.nngroup.com/static/js/3283be5def74.js"></script>
        

        

        
        
        <!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=229034,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('http://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->


<!-- Google Analytics Tracking Code for http://nngroup.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23132681-1', 'nngroup.com');
  ga('require', 'linkid', 'https://www.nngroup.com/articles/concise-scannable-and-objective-how-to-write-for-the-web/linkid.js');
  ga('require', 'displayfeatures');

// Optimizely Universal Analytics Integration
 window.optimizely = window.optimizely || [];
 window.optimizely.push("activateUniversalAnalytics");

  ga('send', 'pageview');

</script>
<script>
/**
* Function to track a click on an outbound link in Google Analytics.
*/
function trackOutboundLink(label, url) {
    if(window.ga && ga.loaded) {
        ga('send', 'event', 'outbound', 'click', label, {'hitCallback': function(){document.location = url;}
        });
    } else {
        document.location = url;
    }
}
/**
* Function to track a click on a link in the article announcement area.
*/
$('#articleAnnouncement a').on('click', function(){
  ga('send', 'event', 'link', 'click', 'articleAnnouncement');
});
</script>

<script type="text/javascript">
 setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2421.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



<!-- Hotjar Tracking Code for http://nngroup.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:121605,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

        <script src="https://media.nngroup.com/static/js/analytics/track_outbound_link.js"></script>
        
        
        
    </head>
    <body class="location-articles articles-detail">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
    <header id="page-header">
        <div id="cart" class="shopping-cart mobile-cart show-for-small-down"></div>
        <div class="row">
            <div id="masthead" class="masthead small-12 medium-7 column">

                <!-- The button to toggles menu on small screen -->

                <h1><a href="../../index.php"><img src="http://media.nngroup.com/static/img/logo-wordmark.png" width="74" height="30"> Nielsen <span class="norman">Norman</span> Group</a></h1>
                <p class="tagline show-for-medium-up">Evidence-Based User Experience Research, Training, and Consulting</p>

                <!-- The button to toggles form search on small screen -->


                <div style="clear: right;width:  100%;height: 1px;"></div><?php define('ROOT_PATH',$_SERVER["DOCUMENT_ROOT"] . '/apps/navigation_styles/navigations/2'); ?><?php include(ROOT_PATH)."/navigation.php"; ?>

                <!-- Drop down menu on small screen -->
                <div id="hideNav" class="show-for-small-down">
                  
                  <ul class="menu-content">
                    <li><a href="../../index.php">Home</a></li>
                    <li class="has-submenu">
                      <a>Training</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../training/index.php">Overview</a></li>
                        
<li id="nav-ux-conference"><a href="../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../online-seminars/index.php"><span>Online Seminars</span></a></li>

                      </ul>
                    </li>
                    <li class="has-submenu">
                      <a>Consulting</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
  <li><a href="../../consulting/index.php">Overview</a></li>
  
  <li><a href="../../consulting/benchmarking/index.php">Benchmarking</a></li>
  
  <li><a href="../../consulting/expert-design-review/index.php">Expert Design Review</a></li>
  
  <li><a href="../../consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
  
  <li><a href="../../consulting/usability-testing/index.php">Usability Testing </a></li>
  
  <li><a href="../../consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
  
  <li><a href="../../in-house-training/index.php">In-House Training</a></li>
</ul>

                    </li>

                    <li><a href="../../reports/index.php">Reports</a></li>
                    <li><a href="../index.php">Articles</a></li>
                    <li class="has-submenu"><a>About NN/G</a><i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../about/index.php">Overview</a></li>
                        <li><a href="../../people/index.php">People</a></li>
                        <li><a href="../../about/why-nng/index.php">Why NN/g?</a></li>
                        <li><a href="../../about/contact/index.php">Contact</a></li>
                        <li><a href="../../news/index.php">News</a></li>
                        <li><a href="../../about/history/index.php">History</a></li>
                        <li><a href="../../books/index.php">Books</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                </div>

                <hr class="header-divider show-for-small-only"/>


            </div>
       </div>

        <div id="hideNav">            <nav class="primary-nav small-12 column">
                <ul>
                    <li><a class="navitem" id="nav-home" href="../../index.php">Home</a></li>
                    <li><a class="navitem" id="nav-training" href="../../training/index.php">Training</a></li>
                    <li><a class="navitem" id="nav-consulting" href="../../consulting/index.php">Consulting</a></li>
                    <li><a class="navitem" id="nav-reports" href="../../reports/index.php">Reports</a></li>
                    <li><a class="navitem" id="nav-articles" href="../index.php">Articles</a></li>
                    <li><a class="navitem" id="nav-about" href="../../about/index.php">About NN/G</a></li>
                </ul>
            </nav>
            <nav class="secondary-nav small-12 medium-12 large-12 columns">
                
                
            </nav>
        </div>
    </header>
    <div id="main" class="l-content" onclick="closeNav(event)"><span id="preventClose" style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776; Menu</span>
    
  

  

  <div class="row">
    
      
      <section class="small-12 medium-3 columns collapsable-content l-navrail articles-left-column">
        <h1 class="show-for-small-down collapse-link mobile-browse-header">Browse by Topic and Author</h1>
          <div class="row collapsable">

            
            <div class="small-6 medium-12 columns">
              <section id="article-popular-topics" style="display: none;">
                
                <h1>Topics</h1>
                <ul id="article-popular-topics">
                    
                    <li>
                        
                        <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/intranets/all/index.php">Intranets</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/user-testing/all/index.php">User Testing</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                        
                    </li>
                    
                    <li><a href="#" id="article-see-all-topics"><strong>See all topics&hellip;</strong></a></li>
                </ul>
                
              </section>
              <section id="article-all-topics" style="display: none;">
                
                <h1>
                  <span class="show-for-medium-up">All Article Topics</span>
                  <span class="show-for-small-down">All Topics</span>
                  (<a href="#" id="article-hide-all-topics">hide</a>)
                </h1>
                <ul>
                  
                  <li>
                    
                    <a href="../../topic/accessibility/all/index.php">Accessibility</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/agile/all/index.php">Agile</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/analytics-and-metrics/all/index.php">Analytics &amp; Metrics</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/applications/all/index.php">Application Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/b2b-websites/all/index.php">B2B Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/behavior-patterns/all/index.php">Behavior Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/branding/all/index.php">Branding</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/content-strategy/all/index.php">Content Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/corporate-websites/all/index.php">Corporate Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/design-patterns/all/index.php">Design Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-design-process/all/index.php">Design Process</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/email/all/index.php">Email</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/eyetracking/all/index.php">Eyetracking</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/heuristic-evaluation/all/index.php">Heuristic Evaluation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/human-computer-interaction/all/index.php">Human Computer Interaction</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-ideation/all/index.php">Ideation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/information-architecture/all/index.php">Information Architecture</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/interaction-design/all/index.php">Interaction Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/international-users/all/index.php">International Users</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/intranets/all/index.php">Intranets</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-management/all/index.php">Management</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/navigation/all/index.php">Navigation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/non-profit-websites/all/index.php">Non-Profit Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/personas/all/index.php">Personas</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/persuasive-design/all/index.php">Persuasive Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/prototyping/all/index.php">Prototyping</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/psychology-and-ux/all/index.php">Psychology and UX</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/research-methods/all/index.php">Research Methods</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/search/all/index.php">Search</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/social-media/all/index.php">Social Media</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/strategy/all/index.php">Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/user-testing/all/index.php">User Testing</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-teams/all/index.php">UX Teams</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/visual-design/all/index.php">Visual Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/writing-web/all/index.php">Writing for the Web</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/young-users/all/index.php">Young Users</a>
                    
                  </li>
                  
                </ul>
                
              </section>
            </div>
            <script>
              /* Determine which topics list will be displayed: the full or the
               * partial.  We are inlining this close to the list itself to
               * prevent rendering delays on the client
               */
              var topic_list = $.cookie("article-topics");

              var popular_topics = $('#article-popular-topics');
              var all_topics = $('#article-all-topics');

              if(topic_list == 1) {
                  popular_topics.hide();
                  all_topics.show();
              } else {
                  popular_topics.show();
                  all_topics.hide();
              }
            </script>
            

            
            <div class="small-6 medium-12 columns">
              
              <section id="article-initial-authors">
                  <h1>Author</h1>
                  <ul>
                    
                    <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                    
                    <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                    
                    <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                    
                    <li><a href="#" onClick="$('#article-initial-authors').hide(); $('#article-all-authors').show();"><strong>See all authors&hellip;</strong></a></li>
                  </ul>
              </section>
              <section id="article-all-authors" style="display: none;">
                <h1>
                  All Authors
                  (<a href="#" id="article-hide-all-authors">hide</a>)
                </h1>
                <ul>
                  
                  <li><a href="../author/raluca-budiu/index.php">Raluca Budiu</a></li>
                  
                  <li><a href="../author/patty-caya/index.php">Patty Caya</a></li>
                  
                  <li><a href="../author/susan-farrell/index.php">Susan Farrell</a></li>
                  
                  <li><a href="../author/therese-fessenden/index.php">Therese Fessenden</a></li>
                  
                  <li><a href="../author/kim-flaherty/index.php">Kim Flaherty</a></li>
                  
                  <li><a href="../author/sarah-gibbons/index.php">Sarah Gibbons</a></li>
                  
                  <li><a href="../author/aurora-harley/index.php">Aurora Harley</a></li>
                  
                  <li><a href="../author/kate-kaplan/index.php">Kate Kaplan</a></li>
                  
                  <li><a href="../author/page-laubheimer/index.php">Page Laubheimer</a></li>
                  
                  <li><a href="../author/angela-li/index.php">Angie Li</a></li>
                  
                  <li><a href="../author/hoa-loranger/index.php">Hoa Loranger</a></li>
                  
                  <li><a href="../author/kate-meyer/index.php">Kate Meyer</a></li>
                  
                  <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                  
                  <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                  
                  <li><a href="../author/kara-pernice/index.php">Kara Pernice</a></li>
                  
                  <li><a href="../author/christian-rohrer/index.php">Christian Rohrer</a></li>
                  
                  <li><a href="../author/amy-schade/index.php">Amy Schade</a></li>
                  
                  <li><a href="../author/katie-sherwin/index.php">Katie Sherwin</a></li>
                  
                  <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                  
                  <li><a href="../author/kathryn-whitenton/index.php">Kathryn Whitenton</a></li>
                  
                </ul>
              </section>
              
            </div>
            
          </div>

          
          <section class="hide-for-small-down">
            
              
              <h1>Recent Articles</h1>
              
              <ul>
                  
                  <li><a href="../seamless-cross-channel/index.php">Seamlessness in the Omnichannel User Experience</a></li>
                  
                  <li><a href="../brand-intention-interpretation/index.php">Experience Design: Bridging Brand Intention and Brand Interpretation</a></li>
                  
                  <li><a href="../eyetracking-tasks-efficient-scanning/index.php">Scanning Patterns on the Web Are Optimized for the Current Task</a></li>
                  
                  <li><a href="../flat-design-best-practices/index.php">Flat-Design Best Practices</a></li>
                  
                  <li><a href="../ux-user-stories/index.php">Accounting for UX Work with User Stories in Agile Projects</a></li>
                  
                  <li><a href="../index.php"><strong>See all articles&hellip;</strong></a></li>
              </ul>
              
            
          </section>
          

          
          
          <section class="hide-for-small-down">
            <h1>Popular Articles</h1>
            
            <ul>
                
                <li><a href="../usability-101-introduction-to-usability/index.php">Usability 101: Introduction to Usability</a></li>
                
                <li><a href="../top-10-mistakes-web-design/index.php">Top 10 Mistakes in Web Design</a></li>
                
                <li><a href="../how-users-read-on-the-web/index.php">How Users Read on the Web</a></li>
                
                <li><a href="../f-shaped-pattern-reading-web-content/index.php">F-Shaped Pattern For Reading Web Content</a></li>
                
                <li><a href="../ten-usability-heuristics/index.php">10 Usability Heuristics for User Interface Design</a></li>
                
                <li><a href="../intranet-design-2016/index.php">10 Best Intranets of 2016</a></li>
                
                <li><a href="../which-ux-research-methods/index.php">When to Use Which User-Experience Research Methods</a></li>
                
                <li><a href="../response-times-3-important-limits/index.php">Response Times: The 3 Important Limits</a></li>
                
                <li><a href="../why-you-only-need-to-test-with-5-users/index.php">Why You Only Need to Test with 5 Users</a></li>
                
                <li><a href="../page-fold-manifesto/index.php">The Fold Manifesto: Why the Page Fold Still Matters</a></li>
                
            </ul>
          </section>
          
          

          
            <section class="hide-for-small-down">
              
            </section>
          

      </section>
      

      
        <div class="small-12 medium-9 columns content-body">
          




<article class="l-subsection alertbox-article-content article-detail-bl">
  
  <header>
    <div class="l-subsection">
      <h1>Concise, SCANNABLE, and Objective: How to Write for the Web  </h1>
      <div class="author-meta">by
        
          
            
            <a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a>
            
            and
            
            <a href="../author/john-morkes/index.php">John Morkes</a>
            
          
        on  January 1, 1997
        
      </div>

      

      
      <div class="topic-links">
        Topics:
        
          <ul>

  <li><a href="../../topic/writing-web/index.php">Writing for the Web</a></li>

</ul>
        
      </div>
      
    </div>
  </header>
  

  
  
  <section>
    <p class="lede"><strong>Summary:</strong> Studies of how users read on the Web found that they do not actually read: instead, they scan the text. A study of five different writing styles found that a sample Web site scored 58% higher in measured usability when it was written concisely, 47% higher when the text was scannable, and 27% higher when it was written in an objective style instead of the promotional style used in the control condition and many current Web pages. Combining these three changes into a single site that was concise, scannable, and objective at the same time resulted in 124% higher measured usability.</p>
    <br />
  </section>
  
  

  
  <section id="article-body"><p>
	Unfortunately, this paper is written in a print writing style and is somewhat too academic in style. We know this is bad, but the paper was written as the traditional way of reporting on a research study. We have a <a href="../how-users-read-on-the-web/index.php" title="Alertbox Oct. 1997: How Users Read on the Web"> short summary</a> that is more suited for online reading.</p>
<h2>
	Introduction</h2>
<p>
	"Really good writing - you don't see much of that on the Web," said one of our test participants. And our general impression is that most Web users would agree. Our studies suggest that current Web writing often does not support users in achieving their main goal: to find useful information as quickly as possible.</p>
<p>
	We have been running Web usability studies since 1994 [Nielsen 1994b, Nielsen and Sano 1994, Nielsen 1995]. Our studies have been similar to most other Web usability work (e.g., [Shum 1996, Spool et al. 1997]) and have mainly looked at site architecture, navigation, search, page design, layout, graphic elements and style, and icons. Even so, we have collected many user comments about the content during this long series of studies. Indeed, we have come to realize that content is king in the user's mind: When asked for feedback on a Web page, users will comment on the quality and relevance of the content to a much greater extent than they will comment on navigational issues or the page elements that we consider to be "user interface" (as opposed to simple information). Similarly, when a page comes up, users focus their attention on the center of the window where they read the body text before they bother looking over headerbars or other navigational elements.</p>
<p>
	We have derived three main content-oriented conclusions from our four years' of Web usability studies [Nielsen 1997a]:</p>
<ul>
	<li>
		users do not read on the Web; instead they scan the pages, trying to pick out a few sentences or even parts of sentences to get the information they want</li>
	<li>
		users do not like long, scrolling pages: they prefer the text to be short and to the point</li>
	<li>
		users detest anything that seems like marketing fluff or overly hyped language ("marketese") and prefer factual information.</li>
</ul>
<p>
	This latter point is well illustrated by the following quote from a customer survey we ran on the Sun website:</p>
<blockquote>
	"One piece of advice, folks: Let's try not to be so gratuitous and self-inflating. Beginning answers to common sense questions such as "Will Sun support my older Solaris platform?" with answers such as "Sun is exceptionally committed to..." and "Solaris is a leading operating system in today's business world..." doesn't give me, as an engineer, a lot of confidence in your ability. I want to hear fact, not platitudes and self-serving ideology. Hell, why not just paint your home page red under the moving banner of, "Computers of the world, Unite under the glorious Sun motherland!"</blockquote>
<p>
	Even though we have gained some understanding of Web content from studies that mainly concerned higher-level Web design issues, we felt that we needed to know more about Web writing in order to advise our content creators. We therefore designed a series of studies that specifically looked at how users read Web pages.</p>
<h2>
	Overview of Studies</h2>
<p>
	We conducted three studies in which a total of 81 users read Web pages. The first two studies were exploratory and qualitative and were aimed at generating insight into how users read and what they like and dislike. The third study was a measurement study aimed at quantifying the potential benefits from some of the most promising writing styles identified in the first two studies. All three studies were conducted during the summer of 1997 in the SunSoft usability laboratories in Menlo Park, CA.</p>
<h2>
	Study 1: Directed Tasks</h2>
<p>
	A major goal in the first study was to compare the reading behavior of technical and non-technical users. Even though we had conducted some earlier studies with non-technical participants, most of our studies had used highly technical users. Also, given the nature of our site, almost all of the data collected from site surveys was provided by technical users.</p>
<p>
	In Study 1, we tested a total of 11 users: 6 end-users and 5 technical users. The main difference between technical and non-technical users seemed to play out in participants' familiarity and expertise with search tools and hypertext. The technical users were better informed about how to perform searches than the end-users were. Technical users also seemed more aware of and more interested in following hypertext links. At least one end-user said he is sometimes hesitant to use hypertext for fear of getting lost.</p>
<p>
	Apart from those differences, there appeared to be no major differences in how technical and non-technical users approached reading on the Web. Both groups desired scannable text, short text, summaries, etc.</p>
<h3>
	Tasks</h3>
<p>
	The tasks were classic directed tasks similar to those used in most of our previous Web usability studies. Users were typically taken to the home page of a specific website and then asked to find specific information on the site. This approach was taken to avoid the well-known problems when users have to find things by searching the entire Web [Pollock and Hockley 1997]. Here is a sample task:</p>
<table align="center" border="1" width="80%">
	<tbody>
		<tr>
			<td>
				You are planning a trip to Las Vegas and want to know about a local restaurant run by chef Charlie Trotter. You heard it was located in the MGM Grand hotel and casino, but you want more information about the restaurant. You begin by looking at the website for Restaurants &amp; Institutions magazine at: http://www.rimag.com
				<p>
					Hint: Look for stories on casino foodservice</p>
				<p>
					Try to find out:<br/>
					-what the article said about the restaurant<br/>
					-where most food is served at the riverboat casino</p>
			</td>
		</tr>
	</tbody>
</table>
<p>
	Unfortunately, the Web is currently so hard to use that users wasted enormous amounts of time trying to find the specific page that contained the answer to the question. Even when on the intended page, users often could not find the answer because they didn't see the relevant line. As a result, much of Study 1 ended up repeating navigation issues that we knew from previous studies and we got fewer results than desired relating to actual reading of content.</p>
<h3>
	Findings</h3>
<h4>
	Users Want to Search</h4>
<p>
	Upon visiting each site, nearly all of the participants wanted to start with a keyword search. "A good search engine is key for a good website," one participant said. If a search engine was not available, a few of the participants said, they would try using the browser's "Find" command.</p>
<p>
	Sometimes participants had to be asked to try to find the information without using a search tool, because searching was not a main focus of this study.</p>
<h4>
	Waiting is Unpleasant</h4>
<p>
	Users think waiting for downloads and search results is boring and a waste of time. More than half the participants mentioned this specifically. "I like to get into a website and then get out. I don't like to lull around," one participant said. Someone else complained about slow downloading of graphics: "I like to see one good picture. I don't like to see tons of pictures. Pictures aren't worth waiting for."</p>
<p>
	Study 1 employed a novel measure of participants' boredom. Participants were instructed to pick up a marble from a container on the table and drop it into another container whenever they felt bored or felt like doing something else. Together, the 11 participants moved 12 marbles: 8 marbles while waiting for a page to download, 2 while waiting for search results to appear, and 2 when unable to find the requested information. (Participants did not always remember to use the marbles when they were bored). After Study 1, we abandoned the marble technique for measuring boredom. Instead, we relied on spoken comments in Study 2 and a traditional subjective satisfaction questionnaire in Study 3.</p>
<h4>
	Conventional Guidelines for Good Writing are Good</h4>
<p>
	Conventional guidelines include carefully organizing the information, using words and categories that make sense to the audience, using topic sentences, limiting each paragraph to one main idea, and providing the right amount of information.</p>
<p>
	"You can't just throw information up there and clutter up cyberspace. Anybody who makes a website should make the effort to organize the information," one participant said.</p>
<p>
	When looking for a particular recipe in Restaurant &amp; Institution magazine's website, some of the participants were frustrated that the recipes were categorized by the dates they appeared in the magazine. "This doesn't help me find it," one person said, adding that the categories would make sense to the user if they were types of food (desserts, for example) rather than months.</p>
<p>
	Several participants, while scanning text, would read only the first sentence of each paragraph. This suggests that topic sentences are important, as is the "one idea per paragraph" rule. One person who was trying to scan a long paragraph said, "It's not very easy to find that information. They should break that paragraph into two pieces-one for each topic."</p>
<p>
	Clarity and quantity-providing the right amount of information-are very important. Two participants who looked at a white paper were confused by a hypertext link at the bottom of Chapter 1. It said only "Next." The participants wondered aloud whether that meant "Next Chapter," "Next Page," or something else.</p>
<h4>
	Additional Findings</h4>
<p>
	We also found that scanning is the norm, that text should be short (or at least broken up), that users like summaries and the inverted pyramid writing style, that hypertext structure can be helpful, that graphical elements are liked if they complement the text, and that users suggest there is a role for playfulness and humor in work-related websites. All of these findings were replicated in Study 2 and are discussed in the following section.</p>
<h2>
	Study 2: Exploratory Study of Reading</h2>
<p>
	Because of the difficulties with navigation in Study 1, we decided to take users directly to the pages we wanted them to read in Study 2. Also, the tasks were designed to encourage reading larger amounts of text rather than simply picking out a single fact from the page.</p>
<h3>
	Participants</h3>
<p>
	We tested 19 participants (8 women and 11 men), ranging in age from 21 to 59. All had at least five months of experience using the Web. Participants came from a variety of occupations, mainly non-technical.</p>
<p>
	Participants said they use the Web for technical support, product information, research for school reports and work, employment opportunities, sales leads, investment information, travel information, weather reports, shopping, coupons, real estate information, games, humor, movie reviews, email, news, sports scores, horoscopes, soap opera updates, medical information, and historical information.</p>
<h3>
	Tasks</h3>
<p>
	Participants began by discussing why they use the Web. They then demonstrated a favorite website. Finally, they visited three sites that we had preselected and performed assigned tasks that required reading and answering questions about the sites. Participants were instructed to "think out loud" throughout the study.</p>
<p>
	The three preselected sites were rotated between participants from a set of 18 sites with a variety of content and writing styles, including news, essays, humor, a how-to article, technical articles, a press release, a diary, a biography, a movie review, and political commentary. The assigned tasks encouraged participants to read the text, rather than search for specific facts. For most of the sites, the task instructions read as follows:</p>
<blockquote>
	"Please go to the following site, which is bookmarked: [site URL]. Take several moments to read it. Feel free to look at anything you want to. In your opinion, what are the three most important points the author is trying to make? After you find the answers, we will ask you some questions."</blockquote>
<p>
	We observed each participant's behavior and asked several questions about the sites. Standard questions for each site included</p>
<ul>
	<li>
		"What would you say is the primary purpose of the site?"</li>
	<li>
		"How would you describe the site's style of writing?"</li>
	<li>
		"How do you like the way it is written?"</li>
	<li>
		"How could the writing in this website be improved?"</li>
	<li>
		"How easy to use is the website? Why?"</li>
	<li>
		"How much do you like this site? Why?"</li>
	<li>
		"Do you have any advice for the writer or designer of this website?"</li>
	<li>
		"Think back to the site you saw just before this one. Of the two sites, which did you like better? Why?"</li>
</ul>
<h3>
	Findings</h3>
<h4>
	Simple and Informal Writing are Preferred</h4>
<p>
	This point was made by 10 participants, many of whom complained about writing that was hard to understand. Commenting on a movie review in one site, another person said, "This review needs a complete rewrite to put it into more down-to-earth language, so that just anybody could read it and understand."</p>
<p>
	Some participants mentioned they like informal, or conversational, writing better than formal writing. "I prefer informal writing, because I like to read fast. I don't like reading every word, and with formal writing, you have to read every word, and it slows you down," one person said.</p>
<h4>
	Credibility is an Important Issue on the Web</h4>
<p>
	Exactly who the publisher of a particular site is-and who the sources of information in the site are-may be unclear to users. Therefore, the sources' motivations, qualifications, and trustworthiness are unclear. All of this causes users to wonder about the credibility of websites.</p>
<p>
	Credibility was mentioned by 7 participants as an important concern. When looking at a news story on the Web, one person said, "One thing I always look for is who it is coming from. Is it a reputable source? Can the source be trusted? Knowing is very important. I don't want to be fed with false facts." When asked how believable the information in an essay on the Web seemed, another person answered, "That's a question I ask myself about every Web site."</p>
<p>
	The quality of a site's content influences users' evaluations of credibility, as one person pointed out: "A magazine that is well done sets a certain tone and impression that are carried through the content. For example, <cite> National Geographic </cite> has a quality feel, a certain image. A website conveys an image, too. If it's tastefully done, it can add a lot of credibility to the site."</p>
<h4>
	Outbound Links Can Increase Credibility</h4>
<p>
	Users rely on hypertext links to help assess credibility of the information contained in websites. This point was made by 4 participants. "Links are good information. They help you judge whether what the author is saying is true," one said. While reading an essay, one person commented, "This site is very believable. The author presents several points of view, and he has links for each point of view." Another person made a similar statement about a different essay: "Because the writer is referencing other links, it's probably relatively accurate information."</p>
<h4>
	Humor Should be Used with Caution</h4>
<p>
	In this study, 10 participants discussed their preferences for humor in various media, and some evaluated humor in certain websites. Overall, participants said they like a wide variety of humor types, such as aggressive, cynical, irreverent, nonsense, physical, and word-play humor. "I like websites when they're not all that dry. I like to laugh. I get bored while waiting. I would like something clever and crafty (to read)," one person said in Study 1.</p>
<p>
	A website containing puns (word-play humor) was described as "stupid" and "not funny" by 2 out of the 3 participants who visited it. A site that contained cynical humor was enjoyed by all 3 participants who saw it, though only one of them had said earlier that he liked this type of humor.</p>
<p>
	Given people's different preferences for humor, it is important for a Web writer to know the audience, before including humor in a site. Of course, using humor successfully may be difficult, because a site's users may be diverse in many ways (e.g., culture, education, and age). Puns are particularly dangerous for any site that expects a large number of international users.</p>
<h4>
	Users Want to Get Their Information Quickly</h4>
<p>
	This was mentioned by 11 participants. Users like well-organized sites that make important information easy to find. "Web users are under emotional and time constraints. The most important thing is to give them the information fast," one participant advised. "I prefer something highly organized to get quickly from here to there. I want to do it quickly," one person said about a site.</p>
<p>
	Users also want fast-loading graphics and fast response times for hypertext links, and they want to choose whether to download large (slow) graphics. "A slow connection time or response time will push me away," one user said.</p>
<h4>
	Text Should be Scannable</h4>
<p>
	Scanning can save users time. During the study, 15 participants always approached unfamiliar Web text by trying to scan it before reading it. Only 3 participants started reading text word by word, from the top of the page to the bottom, without scanning. Elements that enhance scanning include headings, large type, bold text, highlighted text, bulleted lists, graphics, captions, topic sentences, and tables of contents.</p>
<p>
	One user from Study 1 who scanned an article but failed to find what he was looking for said, "If this happened to me at work, where I get 70 emails and 50 voicemails a day, then that would be the end of it. If it doesn't come right out at me, I'm going to give up on it." "Give me bulleted items," another user said. While looking at a news site, one person said, "This is easy to read because it uses bold to highlight certain points." An essay containing long blocks of text prompted this response: "The whole way it looked made it kind of boring. It's intimidating. People want to read things that are broken up. It gets the points across better."</p>
<h4>
	Text Should be Concise</h4>
<p>
	Consistent with users' desire to get information quickly is their preference (expressed by 11 people) for short text. One person said, "Websites are too wordy. It's hard to read a lot of text on the screen." While looking at a news story, another person said, "I like that short style. I don't have time for gobbledygook. I like getting the information fast."</p>
<p>
	Many participants want a Web page to fit on one screen. One person said the following about a news story: "It was too long. I think it's better to have condensed information that's no bigger than one screen."</p>
<p>
	Participants want a website to make its points quickly. While reading a movie review, one person said, "There's a lot of text in here. They should get more to the point. Did they like it or didn't they?"</p>
<h4>
	Users Like Summaries and the Inverted Pyramid Style</h4>
<p>
	According to 8 participants, Web writing that presents news, summaries, and conclusions up front is useful and saves time. A participant who was reading a page of article summaries said, "I like the ability to read a summary and then go to the article if I'm interested."</p>
<p>
	A news story written in the inverted pyramid style (in which news and conclusions are presented first, followed by details and background information), prompted this response: "I was able to find the main point quickly, from the first line. I like that." While reading a different news story, someone else said, "It got my attention right away. This is a good site. Boom. It gets to the point."</p>
<h4>
	Hypertext is Well-Liked</h4>
<p>
	"The incredible thing that's available on the Web is the ability to go deeper for more information," one participant said. In the study, 15 participants said they like hypertext. "Links are a good thing. If you just want to read the page you're on, fine, you're not losing anything. But if you want to follow the links, you can. That's the great thing about the Web," one person said. When asked how useful hypertext links are, another said, "I might be searching for one document, but I might find 15 other related things that pique my interest. It's very useful. I really enjoy that."</p>
<p>
	However, hypertext is not universally liked: 2 participants said hypertext can be distracting if a site contains "too many" links.</p>
<h4>
	Graphics and Text Should Complement One Another</h4>
<p>
	Words and pictures can be a powerful combination, but they must work together, 5 participants said. "I don't ever want to see a picture without a caption beneath it," one participant said.</p>
<p>
	Graphics that add nothing to the text are a distraction and waste of time, some people said. "A graphic is good when it relates to the content, but many are just trying to be flashy," one person said.</p>
<h2>
	Study 3: Measurement Study</h2>
<p>
	In this empirical study, 51 Web users tested 5 variations of a Web site. Each version had a distinct writing style, though all contained essentially the same information. The control version was written in a promotional style (i.e., "marketese"); one version was written to encourage scanning; one was concise; one had an "objective," or non-promotional, writing style; and one combined concise, scannable, and objective language into a single site.</p>
<h3>
	Hypotheses</h3>
<p>
	Based on our qualitative findings in Studies 1 and 2, we made seven hypotheses to test in the measurement study.</p>
<ol>
	<li>
		Hypothesis 1: Users of the scannable and concise versions of the website will spend significantly less time performing tasks than will users of the control version.</li>
	<li>
		Hypothesis 2: Scannable and concise users will make significantly fewer errors on tasks than will control users.</li>
	<li>
		Hypothesis 3: Scannable and concise users will remember site content significantly better than will control users.</li>
	<li>
		Hypothesis 4: Scannable and concise users will take significantly less time to recall the website's structure than will control users. However, all groups (control, scannable, concise, and objective) will perform the same on sitemap accuracy, since the site's structure is simple.</li>
	<li>
		Hypothesis 5: Objective, scannable, and concise users will report significantly higher subjective satisfaction with the site than will control users.</li>
	<li>
		Hypothesis 6: Combining objective, scannable, and concise writing styles into a single site will result in significantly better measures on task time (6A), error rates (6B), memorability (6C), site structure (6D), and subjective satisfaction (6E).</li>
	<li>
		Hypothesis 7: When measures from the first six hypotheses are combined into an overall usability score for each version of the site, the scannable, concise, objective, and combined versions will have higher usability scores than the control version will.</li>
</ol>
<h3>
	Method</h3>
<h4>
	Participants</h4>
<p>
	The participants were 51 experienced Web users recruited by Sun (average amount of Web experience was 2 years). Participants ranged in age from 22-69 (average age was 41). In an attempt to focus on "normal users," we excluded the following professions from the study: webmasters, Web designers, graphic designers, user interface professionals, writers, editors, computer scientists, and computer programmers.</p>
<p>
	We checked for effects of age and Web experience on the dependent variables mentioned in the first five hypotheses, but we found only negligible differences-none significant. Had the sites in our study been more difficult to navigate or had our tasks necessitated use of search engines or other Web infrastructure, we would have expected significant effects of both age and Web experience.</p>
<h4>
	Design</h4>
<p>
	The experiment employed a 5-condition (promotional [control], scannable, concise, objective, or combined) between-subjects design. Conditions were balanced for gender and employment status.</p>
<h4>
	Experimental Materials</h4>
<p>
	The experiment used five versions of a website created for this study. Called <cite> "Travel Nebraska," </cite> the site contained information about Nebraska. We used a travel site because 1) in our earlier qualitative studies, many Web users said travel is one of their interests, and 2) travel content lent itself to the different writing styles we wanted to study. We chose Nebraska to minimize the effect of prior knowledge on our measures (in recruiting participants, we screened out people who had ever lived in, or even near, Nebraska).</p>
<p>
	 </p>
<div style="border-style: solid; border-color: gray; border-width: 1px; border-collapse: collapse; padding: 1em">
	The <a href="../downloadable-files-to-replicate-web-reading-study/index.php"> study materials</a> are available online, if you want to review them in detail or reuse them for your own research.</div>
<p>
	Each version of the <cite> Travel Nebraska </cite> site consisted of seven pages, and all versions used the same hypertext structure. So that participants would focus on text and not be distracted, we used modest hypertext (with no links outside the site) and included only three photos and one illustration. There was no animation. Topics included in the site were Nebraska's history, geography, population, tourist attractions, and economy. The Appendix to this paper shows parts of a sample page from each condition.</p>
<p>
	The <a href="http://media.nngroup.com.s3.amazonaws.com/writing-study-materials/promotional/attractions.php"> control</a> version of the site had a promotional style of writing (i.e., "marketese,"), which contained exaggeration, subjective claims, and boasting, rather than just simple facts. This style is characteristic of many pages on the Web today.</p>
<p>
	The <a href="http://media.nngroup.com.s3.amazonaws.com/writing-study-materials/concise/attractions.php"> concise</a> version had a promotional writing style, but its text was much shorter. Certain less-important information was cut, bringing the word count for each page to about half that of the corresponding page in the control version. Some of the writing in this version was in the inverted pyramid style. However, all information users needed to perform the required tasks was presented in the same order in all versions of the site.</p>
<p>
	The <a href="http://media.nngroup.com.s3.amazonaws.com/writing-study-materials/scannable/attractions.php"> scannable</a> version also contained marketese, but it was written to encourage scanning, or skimming, of the text for information of interest. This version used bulleted lists, boldface text to highlight keywords, photo captions, shorter sections of text, and more headings.</p>
<p>
	The <a href="http://media.nngroup.com.s3.amazonaws.com/writing-study-materials/objective/attractions.php"> objective</a> version was stripped of marketese. It presented information without exaggeration, subjective claims, or boasting.</p>
<p>
	The <a href="http://media.nngroup.com.s3.amazonaws.com/writing-study-materials/combined/attractions.php"> combined</a> version had shorter word count, was marked up for scannability, and was stripped of marketese.</p>
<h4>
	Procedure</h4>
<p>
	Upon arrival at the usability lab, the participant signed a videotape consent form, then was told he or she would visit a website, perform tasks, and answer several questions.</p>
<p>
	After making sure the participant knew how to use the browser, the experimenter explained that he would observe from the room next door to the lab through the one-way mirror. Throughout the study, the participant received both printed instructions from a paper packet and verbal instructions from the experimenter.</p>
<p>
	The participant began at the site's homepage. The first two tasks were to search for specific facts (located on separate pages in the site), without using a search tool or the "Find" command. The participant then answered <a href="../questionnaire-part-1/index.php"> Part 1 of a brief questionnaire</a>. Next was a judgment task (suggested by Spool et al. [1997]) in which the participant first had to find relevant information, then make a judgment about it. This task was followed by <a href="../questionnaire-part-2/index.php"> Part 2 of the questionnaire</a>.</p>
<p>
	Next, the participant was instructed to spend 10 minutes learning as much as possible from the pages in the website, in preparation for a short exam. Finally, the participant was asked to draw on paper the structure of the website, to the best of his or her recollection.</p>
<p>
	After completing the study, each participant was told details about the study and received a gift.</p>
<h4>
	Measures</h4>
<p>
	<strong>Task time </strong> was the number of seconds it took users to find answers for the two search tasks and one judgment task.</p>
<p>
	The two search tasks were to answer: "On what date did Nebraska become a state?" and "Which Nebraska city is the 7th largest, in terms of population?" The questions for the judgment task were: "In your opinion, which tourist attraction would be the best one to visit? Why do you think so?"</p>
<p>
	<strong>Task errors </strong> was a percentage score based on the number of incorrect answers users gave in the two search tasks.</p>
<p>
	<strong>Memory </strong> comprised two measures from the exam: recognition and recall. Recognition memory was a percentage score based on the number of correct answers minus the number of incorrect answers to 5 multiple-choice questions. As an example, one of the questions read: "Which is Nebraska's largest ethnic group? a) English b) Swedes c) Germans d) Irish."</p>
<p>
	Recall memory was a percentage score based on the number of tourist attractions correctly recalled minus the number incorrectly recalled. The question was: "Do you remember any names of tourist attractions mentioned in the website? Please use the space below to list all the ones you remember."</p>
<p>
	<strong>Time to recall site structure </strong> was the number of seconds it took users to draw a sitemap.</p>
<p>
	A related measure, <strong> sitemap accuracy</strong>, was a percentage score based on the number of pages (maximum 7) and connections between pages (maximum 9) correctly identified, minus the number of pages and connections incorrectly identified.</p>
<p>
	<strong>Subjective satisfaction </strong> was determined from participants' answers to a paper-and-pencil questionnaire. Some questions asked about specific aspects of working with the site, and other questions asked for an assessment of how well certain adjectives described the site (anchored by "Describes the site very poorly" to "Describes the site very well"). All questions used 10-point Likert scales.</p>
<p>
	The subjective satisfaction index was the mean score of the following four indices:</p>
<ul>
	<li>
		<strong>Quality </strong> of the site comprised four items: accurate, helpful, useful, and the question "How satisfied are you with the site's quality of language?"</li>
	<li>
		<strong>Ease of use </strong> of the site comprised five items: easy to use, and the questions "How easy is it to work with the text in this website?", "How easy is it to find specific information in this website?", "Compared to what you expected, how quickly did the tasks go?", and "How hard was it to concentrate on searching for information (because of distractions)?" (this item was reverse-coded).</li>
	<li>
		<strong>Likability </strong> of the site comprised six items: entertaining, interesting, likable, engaging, fun to use, and boring (reverse-coded).</li>
	<li>
		<strong>User affect </strong> comprised three items (all reverse-coded): "How tired do you feel right now?", "How confused did you feel while working in this site?", and "How frustrated did you feel while working in this site?"</li>
</ul>
<p>
	For each index, the items were averaged so that the possible range was from 1 to 10.</p>
<h3>
	Results</h3>
<p>
	Main measurements are presented in Table 1.</p>
<table border="1">
	<caption valign="bottom">
		<strong>Table 1. </strong> Mean scores for five major measures. (Standard deviations appear in parentheses.) Time measures are in seconds, Task Errors and Memory are percentage scores, and Subjective Satisfaction is on a scale from 1 to 10.<br/>
		+ p &lt; .10 * p &lt; .05 ** p &lt; .01 *** p &lt; .001 (test for significant difference from control condition)</caption>
	<tbody>
		<tr>
			<th>
				Condition</th>
			<th>
				Task Time</th>
			<th>
				Task Errors</th>
			<th>
				Memory</th>
			<th>
				Sitemap Time</th>
			<th>
				Subjective Satisfaction</th>
		</tr>
		<tr align="center">
			<td align="left" rowspan="2">
				Promotional (control)</td>
			<td>
				359</td>
			<td>
				0.82</td>
			<td>
				0.41</td>
			<td>
				185</td>
			<td>
				5.7</td>
		</tr>
		<tr align="center">
			<td>
				(194)</td>
			<td>
				(0.60)</td>
			<td>
				(0.14)</td>
			<td>
				(43)</td>
			<td>
				(1.5)</td>
		</tr>
		<tr align="center">
			<td align="left" rowspan="2">
				Concise</td>
			<td>
				209*</td>
			<td>
				0.40+</td>
			<td>
				0.65**</td>
			<td>
				130***</td>
			<td>
				7.1*</td>
		</tr>
		<tr align="center">
			<td>
				(88)</td>
			<td>
				(0.70)</td>
			<td>
				(0.21)</td>
			<td>
				(41)</td>
			<td>
				(1.9)</td>
		</tr>
		<tr align="center">
			<td align="left" rowspan="2">
				Scannable</td>
			<td>
				229*</td>
			<td>
				0.30*</td>
			<td>
				0.55*</td>
			<td>
				198</td>
			<td>
				7.4*</td>
		</tr>
		<tr align="center">
			<td>
				(86)</td>
			<td>
				(0.48)</td>
			<td>
				(0.19)</td>
			<td>
				(93)</td>
			<td>
				(1.8)</td>
		</tr>
		<tr align="center">
			<td align="left" rowspan="2">
				Objective</td>
			<td>
				280</td>
			<td>
				0.50</td>
			<td>
				0.47</td>
			<td>
				159</td>
			<td>
				6.9*</td>
		</tr>
		<tr align="center">
			<td>
				(163)</td>
			<td>
				(0.53)</td>
			<td>
				(0.13)</td>
			<td>
				(69)</td>
			<td>
				(1.7)</td>
		</tr>
		<tr align="center">
			<td align="left" rowspan="2">
				Combined</td>
			<td>
				149**</td>
			<td>
				0.10**</td>
			<td>
				0.67***</td>
			<td>
				130**</td>
			<td>
				7.0*</td>
		</tr>
		<tr align="center">
			<td>
				(57)</td>
			<td>
				(0.32)</td>
			<td>
				(0.10)</td>
			<td>
				(25)</td>
			<td>
				(1.6)</td>
		</tr>
	</tbody>
</table>
<p>
	Hypothesis 1 was confirmed. Users of the scannable version performed tasks significantly faster than users of the control version did, t(19) = 1.95, p &lt; .05, one-tailed. The same was true for users of the concise version, t(19) = 2.24, p &lt; .05, one-tailed.</p>
<p>
	Hypothesis 2 was supported. Scannable users made significantly fewer task errors than control users, t(19) = 2.16, p &lt; .05, one-tailed. Concise users also made fewer task errors, but the difference approached significance, t(19) = 1.47, p &lt; .10, one-tailed.</p>
<p>
	Hypothesis 3 was confirmed. Scannable users had significantly better memory of site content than did control users, t(16) = -1.73, p &lt; .05, one-tailed. Concise users did, as well, t(17) = -2.77, p &lt; .01, one-tailed.</p>
<p>
	Hypothesis 4 was partially confirmed. As predicted, concise users took significantly less time to recall the site's structure than control users did, t(19) = 2.98, p &lt; .001, one-tailed. However, there was no significant difference in the amount of time scannable users and control users took to remember the structure, t(19) = -0.40, p &gt; .69.</p>
<p>
	As expected, there were no significant differences between the sitemap accuracy scores of the control users and: scannable users (t(19) = -0.16, p &gt; .88), concise users (t(19) = -0.24, p &gt; .82), or objective users (t(19) = -0.09, p &gt; .93).</p>
<p>
	We did not predict (nor did we find) significant differences between objective users' and control users' measures for task time, task errors, memory, or sitemap time. However, compared to control users, objective users tended to perform the tasks faster, make fewer task errors, remember site content better, and recall the site structure faster. The differences are not significant, but they all point in the same direction (i.e., they suggest that the objective version is "better" than the control).</p>
<p>
	Hypothesis 5 was confirmed. Scannable users reported significantly higher subjective satisfaction with the site than control users did, t(19) = -2.41, p &lt; .05, one-tailed . The same was true for concise users (t(19) = -1.85, p &lt; .05, one-tailed) and objective users (t(19) = -1.76, p &lt; .05, one-tailed).</p>
<p>
	Hypothesis 6 was confirmed. Users of the combined version performed tasks significantly faster than users of the control version did, t(19) = 3.30, p &lt; .01, one-tailed. They also made fewer errors (t(19) = 3.36, p &lt; .01, one-tailed), remembered more (t(17) = -4.56, p &lt; .001, one-tailed), drew the sitemap faster (t(18) = 3.42, p &lt; .01, one-tailed), and had higher subjective satisfaction (t(19) = -1.90, p &lt; .05, one-tailed).</p>
<p>
	Hypothesis 7 was confirmed. Overall usability scores for all versions of the site show that, compared to the control version, the scannable version is 47% better, the concise version 58% better, the objective version 27% better, and the combined version was 124% better. Table 2 contains these data, as well as each condition's normalized mean score for each major measure. Nineteen out of 20 mean scores were higher than the corresponding scores for the control version, meaning that the other four versions were "better" than the control for nearly all of these measures.</p>
<h3>
	Normalized Scores and Overall Usability Scores</h3>
<p>
	To determine how much better or worse in percentage terms each site version was relative to the control, we normalized all participant groups' mean scores for the 5 major measures. For each measure, the control condition's mean score was set to equal 100, and the other conditions' mean scores were transformed (by division) relative to the control (see Table 2). Scores above 100 are "better" than the control, and those below 100 are "worse."</p>
<p>
	Next, we calculated an Overall Usability score for each version of the site, by taking the geometric mean of the normalized scores for the 5 measures (the geometric, rather than arithmetic, mean was used because we compared ratios). Again, the control version's score was 100.</p>
<table border="1">
	<caption valign="bottom">
		<strong>Table 2. </strong> Normalized mean scores for five major measures and Overall Usability. Scores above 100 (the control score) are "better." For example, the scannable version is 57% better than the control for Task Time.</caption>
	<tbody>
		<tr>
			<th>
				Version</th>
			<th>
				Task Time</th>
			<th>
				Task Errors</th>
			<th>
				Memory</th>
			<th>
				Sitemap Time</th>
			<th>
				Subjective Satisfaction</th>
			<th>
				Overall Usability</th>
		</tr>
		<tr align="center">
			<th align="left">
				Promotional (control)</th>
			<td>
				100</td>
			<td>
				100</td>
			<td>
				100</td>
			<td>
				100</td>
			<td>
				100</td>
			<td>
				<strong>100 </strong></td>
		</tr>
		<tr align="center">
			<th align="left">
				Concise</th>
			<td>
				172</td>
			<td>
				205</td>
			<td>
				142</td>
			<td>
				124</td>
			<td>
				156</td>
			<td>
				<strong>158 </strong></td>
		</tr>
		<tr align="center">
			<th align="left">
				Scannable</th>
			<td>
				157</td>
			<td>
				273</td>
			<td>
				94</td>
			<td>
				130</td>
			<td>
				133</td>
			<td>
				<strong>147 </strong></td>
		</tr>
		<tr align="center">
			<th align="left">
				Objective</th>
			<td>
				128</td>
			<td>
				164</td>
			<td>
				116</td>
			<td>
				121</td>
			<td>
				112</td>
			<td>
				<strong>127 </strong></td>
		</tr>
		<tr align="center">
			<th align="left">
				Combined</th>
			<td>
				242</td>
			<td>
				818</td>
			<td>
				162</td>
			<td>
				142</td>
			<td>
				122</td>
			<td>
				<strong>224 </strong></td>
		</tr>
	</tbody>
</table>
<h2>
	Future Research</h2>
<p>
	Much work remains to fully understand the best way of writing for the Web. In our Study 3, we treated the three variations (concise, scannable, and objective) as dichotomies, whereas to a writer, they are continuous dimensions. A simple follow-on study would vary each of our dimensions in a number of steps to assess the shape of the usability curve. For example, conciseness could be tested with word counts varying in 10% increments from 10% to 90% of the control condition. Similarly, scannability could be tested with fewer or more headings, with no bulleted lists, and with more or less use of highlighted words, background coloring, pullquotes, and other typographic tricks. We welcome any such additional studies, and to facilitate them we have made our original test pages and the complete set of test questions available for download at this URL:<br/>
	<a href="../downloadable-files-to-replicate-web-reading-study/index.php">http://nngroup.com/articles/downloadable-files-to-replicate-web-reading-study/ </a></p>
<p>
	It will also be necessary to study a range of tasks and types of websites, including larger and more complex hypertext structures than the one used in our study.</p>
<p>
	We identified many issues in Study 2 that were not tested in Study 3. Some of the more important ones are:</p>
<ul>
	<li>
		interplay between humor and personality on the one side and objectivity on the other</li>
	<li>
		ways of integrating informational graphics with the text</li>
	<li>
		best use of within-page and across-page overviews and summaries</li>
	<li>
		how to structure text into multiple pages, using hypertext to relegate less important information to secondary pages</li>
	<li>
		how to promote an impression of credibility.</li>
</ul>
<p>
	Finally, future studies should address the connection between Web writing and the main Web usability issues of navigation and task performance. Concise text implies shorter pages which will download faster and alleviate the Web's most serious usability problem, slow response times, thus helping users remember their navigational state.</p>
<h2>
	Conclusions</h2>
<p>
	Our study suggests that scannable, concise, and objective writing styles each make a positive difference in Web users' performance and subjective satisfaction. Promotional writing, which is the style most commonly found on the Web today, had much lower scores on virtually all usability measures.</p>
<p>
	The good results for the objective language condition may be because it might be easier to process objectively written text than promotional text. Web users wonder about credibility, and questioning the credibility of promotional statements may distract users from processing the meaning.</p>
<p>
	Since there is no inherent conflict between concise, scannable, and objective texts, we recommend that Web authors employ all three principles in their writing. Indeed, in our case study the combined effect of employing all three improvements was much larger than any of the individual improvements taken alone: our combined version recorded a 124% improvement in measured usability, whereas the three individual improvements "only" scored from 27% to 58%.</p>
<p>
	In one of our other projects [Morkes and Nielsen, 1998], we rewrote actual pages from Sun's website according to our guidelines. In addition to making them concise, scannable, and objective, we also split them into more pages, using hypertext links to move less important material from top-level pages to secondary pages, thus making the primary pages even shorter. The <a href="../applying-writing-guidelines-web-pages/index.php" title="Paper with case study improving usability 159% through rewriting"> rewritten pages scored 159% higher</a> than the originals in a set of usability metrics much like the ones used in the present study.</p>
<p>
	We thus have data from two studies where measured usability improved by 124% and 159%, respectively, when rewriting the text according to our guidelines. More research is obviously needed to get additional data about when one can expect what magnitude of usability improvements, but our current data does suggest that it will often be possible to more than double usability by rewriting web pages according to our guidelines. The ability to double usability should come as no big surprise since it is about what is found in traditional usability engineering of software: applying established usability methods [Nielsen, 1994a] to a software product that was developed without any usability input typically doubles the usability of the redesigned product.</p>
<h2>
	References</h2>
<ol>
	<li>
		Morkes, J., and Nielsen, J. (1998). <a href="../applying-writing-guidelines-web-pages/index.php" title="Paper with case study improving usability 159% through rewriting"> Applying Writing Guidelines to Web Pages</a>. </li>
	<li>
		Nielsen, J. (1994a). <a href="../../books/usability-engineering/index.php" title="book info"> Usability Engineering</a> (paperback edition). Cambridge, MA: AP Professional. </li>
	<li>
		Nielsen, J. (1994b). <a href="../1994-web-usability-report/index.php"> Report from a 1994 Web usability study</a>.</li>
	<li>
		Nielsen, J. (1995). <a href="../1995-design-sun-microsystems-website/index.php"> Interface design for Sun's WWW site</a>. </li>
	<li>
		Nielsen, J. (1997). <a href="../be-succinct-writing-for-the-web/index.php"> Be succinct! (Writing for the Web)</a>.</li>
	<li>
		Nielsen, J., and Sano, D. (1994). SunWeb: <a href="../1994-design-sunweb-sun-microsystems-intranet/index.php"> User interface design for Sun Microsystem's internal Web</a>.</li>
	<li>
		Pollock, A., and Hockley, A. (1997). <a href="http://www.dlib.org/dlib/march97/bt/03pollock.php"> What's wrong with Internet searching</a>. http://www.dlib.org/dlib/march97/bt/03pollock.php</li>
	<li>
		Shum, S. B. (1996). <a href="http://old.sigchi.org/bulletin/1996.4/shum.php"> The missing link: Hypermedia usability research &amp; the Web</a>. </li>
	<li>
		Spool, J. M., Scanlon, T., Schroeder, W., Snyder, C., &amp; DeAngelo, T. (1997). <a href="http://www.amazon.com/exec/obidos/ISBN%3D155860569X/useitcomusableinA/" title="Amazon.com's page about the book"> Web Site Usability: A Designer's Guide</a>. Morgan Kaufmann Publishers</li>
</ol>
</section>
  

  
  <section>
    
        
    
  </section>
  
  
  
  
  

  


  <section>
    <br />
    <p class="share"><b>Share <span class="hide-for-small-down">this article</span>:</b> <a href="https://twitter.com/intent/tweet?url=http://www.nngroup.com/articles/concise-scannable-and-objective-how-to-write-for-the-web/&amp;text=Concise,%20SCANNABLE,%20and%20Objective:%20How%20to%20Write%20for%20the%20Web%20%20&amp;via=nngroup">Twitter</a> | <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=http://www.nngroup.com/articles/concise-scannable-and-objective-how-to-write-for-the-web/&amp;title=Concise,%20SCANNABLE,%20and%20Objective:%20How%20to%20Write%20for%20the%20Web%20%20&amp;source=Nielsen%20Norman%20Group">LinkedIn</a> | <a href="https://plus.google.com/share?url=http://www.nngroup.com/articles/concise-scannable-and-objective-how-to-write-for-the-web/">Google+</a> | <a href="mailto:?subject=NN/g Article: Concise, SCANNABLE, and Objective: How to Write for the Web  &amp;body=http://www.nngroup.com/articles/concise-scannable-and-objective-how-to-write-for-the-web/">Email</a></p>
    <hr />
  </section>

  <footer class="l-subsection">
    
    
    <div class="learn-more">
      <h2>Learn More</h2>
      <p><a href="../subscribe/index.php">Subscribe to the weekly newsletter</a> to get notified about future articles.</p>
      </div>
    
    

    <div class="row show-for-small-down related-content">
      
      
      

      
      
        <div class="column small-12 topic-links">All articles about:
          <ul>
            
            <li><a href="../../topic/writing-web/index.php">Writing for the Web</a></li>
            
          </ul>
        </div>
      
      

      
      
        <div class="column small-12">
          <h3>UX Conference Training Courses</h3>
           <ul class="no-bullet">
    
        <li><a href="../../courses/content-strategy/index.php">Content Strategy</a></li>
    
        <li><a href="../../courses/writing/index.php">Writing Compelling Digital Copy</a></li>
    
</ul>
        </div>
      
      

      
      
        <div class="small-12 column">
          <h3>Research Reports</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../reports/how-people-read-web-eyetracking-evidence/index.php">How People Read on the Web: The Eyetracking Evidence</a></li>
              
            
              
                <li><a href="../../reports/strategic-design-faqs/index.php">Strategic Design for Frequently Asked Questions</a></li>
              
            
              
                <li><a href="../../reports/social-media-user-experience/index.php">Social Media User Experience</a></li>
              
            
              
                <li><a href="../../reports/pr-websites/index.php">PR on Websites</a></li>
              
            
          </ul>
        </div>
      
      

      
      
        <div class="small-12 column">
          <h3>Online Seminars</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../online-seminars/every-word-count/index.php">Making Every Word Count</a></li>
              
            
          </ul>
        </div>
      
      
    </div>

    <div class="hide-for-small-down row related-content">
      <div class="medium-6 columns">
        
        
          <div>
            <h3>Articles</h3>
            <ul class="no-bullet">
              
                
                <li><a href="../mobile-content/index.php">Reading Content on Mobile Devices</a></li>
                
              
                
                <li><a href="../chunking/index.php">How Chunking Helps Content Processing</a></li>
                
              
                
                <li><a href="../cringeworthy-words/index.php">Cringeworthy Words to Cut from Online Copy</a></li>
                
              
                
                <li><a href="../legibility-readability-comprehension/index.php">Legibility, Readability, and Comprehension: Making Users Read Your Words</a></li>
                
              
                
                <li><a href="../headings-pickup-lines/index.php">Headings Are Pick-Up Lines: 5 Tips for Writing Headlines That Convert </a></li>
                
              
            </ul>
          </div>
        
        

        
          
          <div>
            <h3>Research Reports</h3>
            <ul class="no-bullet">
              
                
                  <li><a href="../../reports/how-people-read-web-eyetracking-evidence/index.php">How People Read on the Web: The Eyetracking Evidence</a></li>
                
              
                
                  <li><a href="../../reports/strategic-design-faqs/index.php">Strategic Design for Frequently Asked Questions</a></li>
                
              
                
                  <li><a href="../../reports/social-media-user-experience/index.php">Social Media User Experience</a></li>
                
              
                
                  <li><a href="../../reports/pr-websites/index.php">PR on Websites</a></li>
                
              
            </ul>
          </div>
          
        

      </div>
      <div class="medium-6 columns">
        
          
          <div>
            <h3>UX Conference Training Courses</h3>
             <ul class="no-bullet">
    
        <li><a href="../../courses/content-strategy/index.php">Content Strategy</a></li>
    
        <li><a href="../../courses/writing/index.php">Writing Compelling Digital Copy</a></li>
    
</ul>
          </div>
          
        
        
          
          <div>
            <h3>Online Seminars</h3>
            <ul class="no-bullet">
            
                <li><a href="../../online-seminars/every-word-count/index.php">Making Every Word Count</a></li>
            
            </ul>
          </div>
          
        
      </div>
    </div>
  </footer>
</article>

        </div>

      

    
  </div>
  <div class="row">
    
      <div class="column small-12 show-for-small-down l-navrail announcements">
        
          
        
      </div>
    
    
  </div>

    </div>
    




<footer id="page-footer">
    <div class="row hide-for-print">
        <div class="column small-12 medium-6">
            <section class="footer-popular">
                
                <h1>Popular Topics</h1>
                <div style="float: left;  width: 15em; display: block;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/applications/index.php">Application Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/e-commerce/index.php">E-Commerce Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/eyetracking/index.php">Eyetracking </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/information-architecture/index.php">Information Architecture </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/intranets/index.php">Intranet Design </a></li>
	<li><a href="../../topic/interaction-design/index.php">Interaction Design </a></li>
</ul>
</div>

<div style="float: left;  width: 15em; display: block; list-style: none;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/mobile-and-tablet-design/index.php">Mobile and Tablet Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/credibility-persuasion/index.php">Persuasive Design</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/research-methods/index.php">Research Methods</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/user-testing/index.php">User Testing </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/web-usability/index.php">Web Usability </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/writing-web/index.php">Web Writing &amp; Content Strategy </a></li>
</ul>
</div>

<p>&nbsp;</p>

                
            </section>
        </div>

        <div class="column small-12 medium-6">
            <div class="row">
                <div class="column small-6">
                    <section class="footer-about">
                        <h1>Follow Us</h1>
                        <ul>
	<li><a href="../subscribe/index.php">Subscribe to our weekly UX newsletter</a>,&nbsp;Jakob Nielsen&#39;s Alertbox</li>
	<li>Visit Don Norman&#39;s website, <a href="http://www.jnd.org/"> www.jnd.org </a></li>
	<li>Visit Bruce Tognazzini&#39;s website, <a href="http://www.asktog.com/"> www.asktog.com </a></li>
	<li style="padding-top: 6px;"><a class="twitter-follow-button" data-show-count="false" data-size="large" href="https://twitter.com/nngroup"><img alt="Follow @nngroup on Twitter" height="20" src="https://media.nngroup.com/media/editor/2016/12/30/twitter-follow-button.png" style="width: 117px; height: 20px;" width="117" /></a></li>
	<li style="padding-top: 6px;"><img alt="" height="14" src="https://media.nngroup.com/media/editor/2016/07/26/feed-icon-14x14.png" style="vertical-align:text-top;width:14px;margin-right:.25em;" width="14" /><a href="../../feed/rss/index.php">Subscribe to our RSS feed</a></li>
</ul>

                    </section>
                </div>

                <div class="column small-6">
                    <section class="footer-about right-column">
                        <h1>About
</h1>
                        <ul class="no-bullet">
	<li><a href="../../about/contact/index.php">Contact information</a></li>
	<li><a href="../../about/index.php">About Us</a></li>
	<li><a href="../../about/why-nng/index.php">Why NN/g? </a></li>
	<li><a href="../../return-policy/index.php">Return Policy</a></li>
</ul>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="copyright column small-12">
            <p><a href="../../copyright-and-reprint-info/index.php">Copyright</a> &copy; 1998-2017 Nielsen Norman Group, All Rights Reserved.</p>
        </div>
    </div>
</footer>


    


        <script src="https://media.nngroup.com/static/vendor/fastclick/js/fastclick.min.js"></script>
        <script src="https://media.nngroup.com/static/vendor/foundation/js/foundation.min.js"></script>
        <script src="http://media.nngroup.com/static/js/app.js"></script>
        <script src="http://media.nngroup.com/static/js/scroll-to-fixed.js"></script><script>var externalLinks = document.querySelectorAll('a[href^="http"]');for (var i = externalLinks.length-1; i >= 0; i--) {externalLinks[i].addEventListener("click", function(event) { alert("External links are disabled for this test."); event.preventDefault(); }, false);}var mailLinks = document.querySelectorAll('a[href^="mailto"]');for (var i = mailLinks.length-1; i >= 0; i--) {mailLinks[i].addEventListener("click", function(event) { alert("Email links are disabled for this test."); event.preventDefault(); }, false);}var externalForms = document.querySelectorAll('form');for (var i = externalForms.length-1; i >= 0; i--) {externalForms[i].addEventListener("submit", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false); externalForms[i].addEventListener("click", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false);}</script>

        <script>
            
            $(function() {
                $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
                $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
            });
        </script>

        
<script src="https://media.nngroup.com/static/js/articles.js"></script>


        
        <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1072699960;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072699960/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
    </body>

<!-- Mirrored from www.nngroup.com/articles/concise-scannable-and-objective-how-to-write-for-the-web/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:09:34 GMT -->
</html>
