<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->





    
<!-- Mirrored from www.nngroup.com/articles/smartwatch/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:13:06 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1026.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"licenseKey":"1a30ef9931","errorBeacon":"bam.nr-data.net","applicationID":"2048992","transactionName":"NFxSMRBUX0EDUkIPDA0WdhAMVkVbDV8ZBxEXUFMJB0YfRAtUQRVZIktEDAFZVHYHRVcPD01eVRE=","beacon":"bam.nr-data.net","queueTime":1,"applicationTime":332,"agent":""}</script>
        <title>The Apple Watch: User-Experience Appraisal</title><meta property="og:title" content="The Apple Watch: User-Experience Appraisal" />
  
        
        <meta name="description" content="Smartwatch apps should rely on gestures more than on navigation elements, prioritize the essential, support handoff, and create tailored, standalone content.">
        <meta property="og:description" content="Smartwatch apps should rely on gestures more than on navigation elements, prioritize the essential, support handoff, and create tailored, standalone content." />
        
  
        
	
        
        <meta name="keywords" content="smartwatch, UI design, mobile design, Apple watch, gestures, touch targets, swipe, watches, watch UX, wearable computing, wearables, force touch">
        

        

        <meta name="robots" content="noarchive">
        <meta name="viewport" content="width=device-width">

        <link rel="search" type="application/opensearchdescription+xml" title="NN/g Search" href="../../opensearch.xml" />
        <link rel="shortcut icon" href="http://media.nngroup.com/static/img/favicon.ico" />

        <link rel="stylesheet" href="https://media.nngroup.com/static/vendor/font-awesome/css/font-awesome.min.css">

        
        <link rel="stylesheet" href="https://media.nngroup.com/static/css/7232a960f1df.css" type="text/css" />
        <link rel="stylesheet" href="http://fast.fonts.com/t/1.css?apiType=css&amp;projectid=c5846260-9def-46ec-9407-0f1349bd2d64" type="text/css">

        <script src="https://media.nngroup.com/static/vendor/modernizr/js/modernizr.min.js"></script>

        <script type="text/javascript" src="https://media.nngroup.com/static/js/3283be5def74.js"></script>
        

        

        
        
        <!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=229034,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('http://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->


<!-- Google Analytics Tracking Code for http://nngroup.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23132681-1', 'nngroup.com');
  ga('require', 'linkid', 'https://www.nngroup.com/articles/smartwatch/linkid.js');
  ga('require', 'displayfeatures');

// Optimizely Universal Analytics Integration
 window.optimizely = window.optimizely || [];
 window.optimizely.push("activateUniversalAnalytics");

  ga('send', 'pageview');

</script>
<script>
/**
* Function to track a click on an outbound link in Google Analytics.
*/
function trackOutboundLink(label, url) {
    if(window.ga && ga.loaded) {
        ga('send', 'event', 'outbound', 'click', label, {'hitCallback': function(){document.location = url;}
        });
    } else {
        document.location = url;
    }
}
/**
* Function to track a click on a link in the article announcement area.
*/
$('#articleAnnouncement a').on('click', function(){
  ga('send', 'event', 'link', 'click', 'articleAnnouncement');
});
</script>

<script type="text/javascript">
 setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2421.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



<!-- Hotjar Tracking Code for http://nngroup.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:121605,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

        <script src="https://media.nngroup.com/static/js/analytics/track_outbound_link.js"></script>
        
        
        
    </head>
    <body class="location-articles articles-detail">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
    <header id="page-header">
        <div id="cart" class="shopping-cart mobile-cart show-for-small-down"></div>
        <div class="row">
            <div id="masthead" class="masthead small-12 medium-7 column">

                <!-- The button to toggles menu on small screen -->

                <h1><a href="../../index.php"><img src="http://media.nngroup.com/static/img/logo-wordmark.png" width="74" height="30"> Nielsen <span class="norman">Norman</span> Group</a></h1>
                <p class="tagline show-for-medium-up">Evidence-Based User Experience Research, Training, and Consulting</p>

                <!-- The button to toggles form search on small screen -->


                <div style="clear: right;width:  100%;height: 1px;"></div><?php define('ROOT_PATH',$_SERVER["DOCUMENT_ROOT"] . '/apps/navigation_styles/navigations/2'); ?><?php include(ROOT_PATH)."/navigation.php"; ?>

                <!-- Drop down menu on small screen -->
                <div id="hideNav" class="show-for-small-down">
                  
                  <ul class="menu-content">
                    <li><a href="../../index.php">Home</a></li>
                    <li class="has-submenu">
                      <a>Training</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../training/index.php">Overview</a></li>
                        
<li id="nav-ux-conference"><a href="../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../online-seminars/index.php"><span>Online Seminars</span></a></li>

                      </ul>
                    </li>
                    <li class="has-submenu">
                      <a>Consulting</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
  <li><a href="../../consulting/index.php">Overview</a></li>
  
  <li><a href="../../consulting/benchmarking/index.php">Benchmarking</a></li>
  
  <li><a href="../../consulting/expert-design-review/index.php">Expert Design Review</a></li>
  
  <li><a href="../../consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
  
  <li><a href="../../consulting/usability-testing/index.php">Usability Testing </a></li>
  
  <li><a href="../../consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
  
  <li><a href="../../in-house-training/index.php">In-House Training</a></li>
</ul>

                    </li>

                    <li><a href="../../reports/index.php">Reports</a></li>
                    <li><a href="../index.php">Articles</a></li>
                    <li class="has-submenu"><a>About NN/G</a><i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../about/index.php">Overview</a></li>
                        <li><a href="../../people/index.php">People</a></li>
                        <li><a href="../../about/why-nng/index.php">Why NN/g?</a></li>
                        <li><a href="../../about/contact/index.php">Contact</a></li>
                        <li><a href="../../news/index.php">News</a></li>
                        <li><a href="../../about/history/index.php">History</a></li>
                        <li><a href="../../books/index.php">Books</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                </div>

                <hr class="header-divider show-for-small-only"/>


            </div>
       </div>

        <div id="hideNav">            <nav class="primary-nav small-12 column">
                <ul>
                    <li><a class="navitem" id="nav-home" href="../../index.php">Home</a></li>
                    <li><a class="navitem" id="nav-training" href="../../training/index.php">Training</a></li>
                    <li><a class="navitem" id="nav-consulting" href="../../consulting/index.php">Consulting</a></li>
                    <li><a class="navitem" id="nav-reports" href="../../reports/index.php">Reports</a></li>
                    <li><a class="navitem" id="nav-articles" href="../index.php">Articles</a></li>
                    <li><a class="navitem" id="nav-about" href="../../about/index.php">About NN/G</a></li>
                </ul>
            </nav>
            <nav class="secondary-nav small-12 medium-12 large-12 columns">
                
                
            </nav>
        </div>
    </header>
    <div id="main" class="l-content" onclick="closeNav(event)"><span id="preventClose" style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776; Menu</span>
    
  

  

  <div class="row">
    
      
      <section class="small-12 medium-3 columns collapsable-content l-navrail articles-left-column">
        <h1 class="show-for-small-down collapse-link mobile-browse-header">Browse by Topic and Author</h1>
          <div class="row collapsable">

            
            <div class="small-6 medium-12 columns">
              <section id="article-popular-topics" style="display: none;">
                
                <h1>Topics</h1>
                <ul id="article-popular-topics">
                    
                    <li>
                        
                        <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/intranets/all/index.php">Intranets</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/user-testing/all/index.php">User Testing</a>
                        
                    </li>
                    
                    <li>
                        
                        <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                        
                    </li>
                    
                    <li><a href="#" id="article-see-all-topics"><strong>See all topics&hellip;</strong></a></li>
                </ul>
                
              </section>
              <section id="article-all-topics" style="display: none;">
                
                <h1>
                  <span class="show-for-medium-up">All Article Topics</span>
                  <span class="show-for-small-down">All Topics</span>
                  (<a href="#" id="article-hide-all-topics">hide</a>)
                </h1>
                <ul>
                  
                  <li>
                    
                    <a href="../../topic/accessibility/all/index.php">Accessibility</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/agile/all/index.php">Agile</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/analytics-and-metrics/all/index.php">Analytics &amp; Metrics</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/applications/all/index.php">Application Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/b2b-websites/all/index.php">B2B Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/behavior-patterns/all/index.php">Behavior Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/branding/all/index.php">Branding</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/content-strategy/all/index.php">Content Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/corporate-websites/all/index.php">Corporate Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/design-patterns/all/index.php">Design Patterns</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-design-process/all/index.php">Design Process</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/e-commerce/all/index.php">E-commerce</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/email/all/index.php">Email</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/eyetracking/all/index.php">Eyetracking</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/heuristic-evaluation/all/index.php">Heuristic Evaluation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/human-computer-interaction/all/index.php">Human Computer Interaction</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-ideation/all/index.php">Ideation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/information-architecture/all/index.php">Information Architecture</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/interaction-design/all/index.php">Interaction Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/international-users/all/index.php">International Users</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/intranets/all/index.php">Intranets</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-management/all/index.php">Management</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/mobile-and-tablet-design/all/index.php">Mobile &amp; Tablet</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/navigation/all/index.php">Navigation</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/non-profit-websites/all/index.php">Non-Profit Websites</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/personas/all/index.php">Personas</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/persuasive-design/all/index.php">Persuasive Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/prototyping/all/index.php">Prototyping</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/psychology-and-ux/all/index.php">Psychology and UX</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/research-methods/all/index.php">Research Methods</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/search/all/index.php">Search</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/social-media/all/index.php">Social Media</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/strategy/all/index.php">Strategy</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/user-testing/all/index.php">User Testing</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/ux-teams/all/index.php">UX Teams</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/visual-design/all/index.php">Visual Design</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/web-usability/all/index.php">Web Usability</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/writing-web/all/index.php">Writing for the Web</a>
                    
                  </li>
                  
                  <li>
                    
                    <a href="../../topic/young-users/all/index.php">Young Users</a>
                    
                  </li>
                  
                </ul>
                
              </section>
            </div>
            <script>
              /* Determine which topics list will be displayed: the full or the
               * partial.  We are inlining this close to the list itself to
               * prevent rendering delays on the client
               */
              var topic_list = $.cookie("article-topics");

              var popular_topics = $('#article-popular-topics');
              var all_topics = $('#article-all-topics');

              if(topic_list == 1) {
                  popular_topics.hide();
                  all_topics.show();
              } else {
                  popular_topics.show();
                  all_topics.hide();
              }
            </script>
            

            
            <div class="small-6 medium-12 columns">
              
              <section id="article-initial-authors">
                  <h1>Author</h1>
                  <ul>
                    
                    <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                    
                    <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                    
                    <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                    
                    <li><a href="#" onClick="$('#article-initial-authors').hide(); $('#article-all-authors').show();"><strong>See all authors&hellip;</strong></a></li>
                  </ul>
              </section>
              <section id="article-all-authors" style="display: none;">
                <h1>
                  All Authors
                  (<a href="#" id="article-hide-all-authors">hide</a>)
                </h1>
                <ul>
                  
                  <li><a href="../author/raluca-budiu/index.php">Raluca Budiu</a></li>
                  
                  <li><a href="../author/patty-caya/index.php">Patty Caya</a></li>
                  
                  <li><a href="../author/susan-farrell/index.php">Susan Farrell</a></li>
                  
                  <li><a href="../author/therese-fessenden/index.php">Therese Fessenden</a></li>
                  
                  <li><a href="../author/kim-flaherty/index.php">Kim Flaherty</a></li>
                  
                  <li><a href="../author/sarah-gibbons/index.php">Sarah Gibbons</a></li>
                  
                  <li><a href="../author/aurora-harley/index.php">Aurora Harley</a></li>
                  
                  <li><a href="../author/kate-kaplan/index.php">Kate Kaplan</a></li>
                  
                  <li><a href="../author/page-laubheimer/index.php">Page Laubheimer</a></li>
                  
                  <li><a href="../author/angela-li/index.php">Angie Li</a></li>
                  
                  <li><a href="../author/hoa-loranger/index.php">Hoa Loranger</a></li>
                  
                  <li><a href="../author/kate-meyer/index.php">Kate Meyer</a></li>
                  
                  <li><a href="../author/jakob-nielsen/index.php">Jakob Nielsen</a></li>
                  
                  <li><a href="../author/don-norman/index.php">Don Norman</a></li>
                  
                  <li><a href="../author/kara-pernice/index.php">Kara Pernice</a></li>
                  
                  <li><a href="../author/christian-rohrer/index.php">Christian Rohrer</a></li>
                  
                  <li><a href="../author/amy-schade/index.php">Amy Schade</a></li>
                  
                  <li><a href="../author/katie-sherwin/index.php">Katie Sherwin</a></li>
                  
                  <li><a href="../author/bruce-tognazzini/index.php">Bruce &quot;Tog&quot; Tognazzini</a></li>
                  
                  <li><a href="../author/kathryn-whitenton/index.php">Kathryn Whitenton</a></li>
                  
                </ul>
              </section>
              
            </div>
            
          </div>

          
          <section class="hide-for-small-down">
            
              
              <h1>Recent Articles</h1>
              
              <ul>
                  
                  <li><a href="../seamless-cross-channel/index.php">Seamlessness in the Omnichannel User Experience</a></li>
                  
                  <li><a href="../brand-intention-interpretation/index.php">Experience Design: Bridging Brand Intention and Brand Interpretation</a></li>
                  
                  <li><a href="../eyetracking-tasks-efficient-scanning/index.php">Scanning Patterns on the Web Are Optimized for the Current Task</a></li>
                  
                  <li><a href="../flat-design-best-practices/index.php">Flat-Design Best Practices</a></li>
                  
                  <li><a href="../ux-user-stories/index.php">Accounting for UX Work with User Stories in Agile Projects</a></li>
                  
                  <li><a href="../index.php"><strong>See all articles&hellip;</strong></a></li>
              </ul>
              
            
          </section>
          

          
          
          <section class="hide-for-small-down">
            <h1>Popular Articles</h1>
            
            <ul>
                
                <li><a href="../usability-101-introduction-to-usability/index.php">Usability 101: Introduction to Usability</a></li>
                
                <li><a href="../top-10-mistakes-web-design/index.php">Top 10 Mistakes in Web Design</a></li>
                
                <li><a href="../how-users-read-on-the-web/index.php">How Users Read on the Web</a></li>
                
                <li><a href="../f-shaped-pattern-reading-web-content/index.php">F-Shaped Pattern For Reading Web Content</a></li>
                
                <li><a href="../ten-usability-heuristics/index.php">10 Usability Heuristics for User Interface Design</a></li>
                
                <li><a href="../intranet-design-2016/index.php">10 Best Intranets of 2016</a></li>
                
                <li><a href="../which-ux-research-methods/index.php">When to Use Which User-Experience Research Methods</a></li>
                
                <li><a href="../response-times-3-important-limits/index.php">Response Times: The 3 Important Limits</a></li>
                
                <li><a href="../why-you-only-need-to-test-with-5-users/index.php">Why You Only Need to Test with 5 Users</a></li>
                
                <li><a href="../page-fold-manifesto/index.php">The Fold Manifesto: Why the Page Fold Still Matters</a></li>
                
            </ul>
          </section>
          
          

          
            <section class="hide-for-small-down">
              
            </section>
          

      </section>
      

      
        <div class="small-12 medium-9 columns content-body">
          




<article class="l-subsection alertbox-article-content article-detail-bl">
  
  <header>
    <div class="l-subsection">
      <h1>The Apple Watch: User-Experience Appraisal</h1>
      <div class="author-meta">by
        
          
            
            <a href="../author/raluca-budiu/index.php">Raluca Budiu</a>
            
          
        on  May 17, 2015
        
      </div>

      

      
      <div class="topic-links">
        Topics:
        
          <ul>

  <li><a href="../../topic/mobile-and-tablet-design/index.php">Mobile &amp; Tablet</a></li>

</ul>
        
      </div>
      
    </div>
  </header>
  

  
  
  <section>
    <p class="lede"><strong>Summary:</strong> Smartwatch apps should rely on gestures more than on navigation elements, prioritize the essential, support handoff, and create tailored, standalone content.</p>
    <br />
  </section>
  
  

  
  <section id="article-body"><p>After analyzing the Samsung Galaxy Gear watch last year, I wrote that “<a href="../samsung-watch/index.php">Smartwatches are the future—but Samsung Galaxy Gear only partway there</a>.” In spite of the unbridled enthusiasm that the Apple watch has generated on many tech sites, unfortunately, its UI didn't get us much closer to the future. Here are some observations and corresponding recommendations for designers.</p>

<h2>Tiny Targets</h2>

<p>It’s no news that <a href="../mouse-vs-fingers-input-device/index.php">touch input requires bigger targets than the mouse</a>. Like it or not, people have fat fingers, and to ensure that they can reach touch targets quickly and reliably, the recommended target size is 1cm × 1cm (0.4 in × 0.4 in). That’s why perhaps the most striking feature of the Apple watch is how much it seems to have embraced teeny-tiny targets. To unlock the screen you have to type your pin on a minuscule numerical pad. And the application screen uses a plethora of tiny circles (representing apps) organized in a focus-plus-context visualization — the center of the screen is the focus and has the largest circles, and as you get further out, the icons get smaller. Launching an app is an adventure — not only because the icons (in-focus ones included) are too small even for the tiniest pinkies, but also because deciphering them requires good eyes, or at least diligence and the will to scroll around and bring them in focus. And good luck trying to erase any of the apps directly from the watch: those “X” icons are suited only for infant digits! (Yes, people can touch the whole icon, but in practice the vast majority of users will attempt touching the small “X” because that’s what they have been conditioned to by countless closeboxes all over the web and software. While padded touch targets reduce user errors, they don’t solve the fat-finger problem, because we know from usability testing that users attempt to hit within the visibly touchable area.)</p>

<p> </p>

<figure class="caption"><img alt="" border="0" height="188" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2015/05/14/pinpad.png" width="150"/>
<figcaption><em>Unlocking the watch requires typing on tiny numerical keys, each about 6 mm × 4 mm — only about a quarter (24%) of the recommended area of 10 mm × 10 mm.</em></figcaption>
</figure>

<p> </p>

<figure class="caption"><img alt="" border="0" height="186" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2015/05/14/apps.png" width="310"/>
<figcaption><em>The app screen uses a focus-and-context visualization, but even the biggest app icons are too small to launch accurately. And deleting the apps involves pressing tiny "</em>x"<em> icons that are about 2 mm in diameter.</em></figcaption>
</figure>

<p>You may ask: with a small screen (the watch itself is 38 mm × 38 mm in its smaller version, and the actual screen is even smaller than that — only about 32 mm × 35 mm), what else is there to do beside embracing <em>really</em> small targets? Definitely a good question. Here are two answers: <strong>voice</strong> and <strong>gestures</strong>. Both voice and gesture save screens from UI elements (also known as <a href="../browser-and-gui-chrome/index.php">chrome</a>), while allowing users to navigate around the app or the device and input information.</p>

<h2>Gestures</h2>

<p>So far, touch apps have struggled with implementing gestures, and legitimately so. <a href="http://www.jnd.org/dn.mss/gestural_interfaces.php">Gestures have few affordances and are hard to learn</a>. In user testing, those few apps that rely extensively on gestures fare poorly with regular users and require a fair amount of patience and willingness to learn. On phones and tablets, interface redundancy is often recommended as a way to ease users into gestures — that is, instead of relying on gestures as the sole way to implement a functionality, have an alternate way of doing the same action that involves visible chrome.</p>

<p>But with smartwatches, gestures are pretty close to being the only reasonable alternative. A swipe is more forgiving than the simple tap and is by far the easiest way to interact with the device. The watch apps use the swipe extensively to move through different items in a deck of cards (see below), and the right swipe means (almost) consistently <em>Back</em>.</p>

<figure class="caption"><img alt="" border="0" height="199" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2015/05/14/dictionary.png" width="330"/>
<figcaption><em>Dictionary: Tapping on </em>Word of the Day <em>shows the current entry, and then users can navigate back by tapping</em> &lt; WotD<em> in the page header (difficult, but visible) or by swiping right (easy, but hidden).</em></figcaption>
</figure>

<p>One new gesture that the watch introduces is the <strong>force touch</strong>, a distant relative of the long press familiar to Android users. Like the original version of long press on Android, the force touch can be used to display a contextual menu, relevant to the current screen (similar to a right-click in desktop Windows). But unlike the long press (or a right-click), <strong>the force touch is attached to the whole screen, not to a UI element of the screen</strong>. Thus, it doesn't matter exacly where your finger touches down when you initiate a force touch. Fat-finger problem solved, though at the cost of diminished contextualization.</p>

<figure class="caption"><img alt="" border="0" height="193" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2015/05/14/weather.png" width="350"/>
<figcaption><em>Weather: A force touch changes the type of information displayed.</em></figcaption>
</figure>

<p>Beyond the novel hardware, emphasizing force touch and swipe-as-back as key gestures represents an interesting, albeit fairly timid opportunity for <strong>gesture standardization</strong>. The <a href="../ios-7/index.php">swipe-as-back is already part of the iOS</a> gesture vocabulary, yet many users are not familiar with it because they don’t need to be (the <em>Back </em>button in Safari is accessible and more familiar — an example of the aforementioned interface redundancy). If Apple forced watch users to rely on the swipe for <em>Back</em>, then that gesture would perhaps trickle back to phones and tablets and become more standard. However, the swipe-as-back functionality is duplicated even on watches by a top <em>Back/up </em>button (see the hierarchical view in the Dictionary app above), which although difficult to use because of its small size, may still get more usage since it’s visible.</p>

<p>The force touch is a gesture with no perceived <a href="http://jnd.org/dn.mss/signifiers_not_affordances.php">signifier</a>— that is, with no visual indication as to when the gesture can be used. This gesture is fairly unfamiliar to iPhone users (long presses or touch-and-hold gestures are far from common on iOS). If used consistently by apps, its familiarity may increase and it could become a viable gesture for other touch interfaces. However, <a href="../windows-8-disappointing-usability/index.php">history</a> teaches us that the lack of visual signifiers or cues for that gesture is likely to slow down its adoption.</p>

<h2>Deck of Cards Works Best</h2>

<p>The tiny targets have some important implications for design. Because using navigation elements such as buttons and links is so tedious, in fact, what works best on the watch is the <strong>deck of cards</strong>, a pattern that definitely is not optimum for other devices.</p>

<figure class="caption"><img alt="" border="0" height="188" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2015/05/14/nytimes.png" width="470"/>
<figcaption><em>NYTimes app : The deck-of-cards pattern allows users to go through different stories by swiping horizontally</em>.</figcaption>
</figure>

<p> </p>

<figure class="caption"><img alt="" border="0" height="580" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2015/05/14/cnn.png" width="150"/>
<figcaption><em>CNN uses a list pattern that displays the stories on the same page, one under the other, and then lets people tap on each of the stories for more detail.</em></figcaption>
</figure>

<p>The deck of cards (a full-page relative of the <a href="../designing-effective-carousels/index.php">carousel</a>) is a presentation model that <a href="../two-basic-hypertext-presentation-models/index.php">goes back at least 20 years</a>. Cards provide <a href="../direct-vs-sequential-access/index.php">sequential instead of direct access</a> and usually should be reserved for content that has a clearly sequential nature (e.g., books) or for lists with just a few elements. Yet, on the watch, the deck of cards is preferable to the the alternative list interface, which often requires going back and forth between a list view and an item-detail view (a form of <a href="../pogo-sticking/index.php">pogo sticking</a>), and thus involves multistep navigation. Plus, with the deck of cards, users can easily trigger the contextual menu (to save the story for later reading on the phone, for example) for each item right away, whereas in the list view users must navigate to the detail to invoke the contextual menu corresponding to that item.</p>

<figure class="caption"><img alt="" border="0" height="198" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2015/05/14/nyt-savelater.png" width="340"/>
<figcaption><em>NYTimes: Force touch on a story in a deck of cards displays the contextual menu that enables users to save the story for later reading.</em></figcaption>
</figure>

<p>The deck of cards is of course tedious when you have a hundred items (or even 20), but it is ok with 10–12. In fact, a user may never go through 10–12 cards on a watch — the length of the session would be long enough in that situation to warrant taking out the phone.</p>

<figure class="caption"><img alt="" border="0" height="199" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2015/05/14/stocks.png" width="330"/>
<figcaption><em>Stocks: The list pattern is harder to use than the deck-of-cards pattern. Users who tap onto one of the stocks can see more detail, but the top navigation bar in the detail view is cumbersome and signals users that they must interact with it in order to go back to the list view. The persistent title and navigation bar (</em>Stocks<em> and </em>AAPL<em>, respectively) waste precious screen space.</em></figcaption>
</figure>

<p>The other disadvantage of the list view (besides the more complex navigation and the crowding of targets) is that it makes it easier to accidentally touch something while just scrolling down.</p>

<h2>Handoff</h2>

<p><strong>Handoff</strong> refers to allowing users to continue the task started on the watch on their phone. This used to be one of the strenghts of the Samsung Galaxy Gear, but with the Apple Watch the handoff is a lot more painful, for two reasons: (1) not all apps allow users to continue their tasks on the phone, and, more importantly, (2) the <a href="../interaction-cost-definition/index.php">interaction cost</a> of resuming the task is fairly high. Unlike with the Samsung Galaxy Gear, your iPhone is not automatically unlocked when the watch is in its immediate proximity. So you have to swipe up (not touch) a tiny icon in the bottom left corner of your lock screen — an utterly bizzare interaction design — and then unlock your phone before being able to continue the task on your phone.</p>

<figure class="caption"><img alt="" border="0" height="610" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2015/05/14/handoff-nyt.png" width="700"/>
<figcaption><em>To read a NYTimes article on the phone, users must swipe up the icon in the bottom left corner of their phone’s lock screen, then unlock the screen, and then they will be taken to the article. With a more <a href="../seamless-cross-channel/index.php">integrated crossplatform user experience</a> the phone would already be displaying the full article if the user grabs it while viewing the headline on the watch.</em></figcaption>
</figure>

<p>Handoff is a powerful tool that should be used not only for getting more detailed content that cannot fit the watch screen, but also, more generally, for tasks that cannot be done on the watch. In particular, designers should help users <strong>recover more smoothly from errors using handoff</strong>. Keynote, for instance, asks users to go to the iPhone and open the app, instead of using handoff to help them do that. Ideally the apps should hand over situations like these to the phone app, instead of having people launch the app by themselves.</p>

<figure class="caption"><img alt="" border="0" height="200" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2015/05/14/errors.png" width="330"/>
<figcaption><em>Neither Keynote (left) nor Buzzfeed (right) use handoff to help users fix the errors reported or enable notifications. Users have to unlock their iPhones, launch these apps independently, and find their way around the app to fix the error.</em></figcaption>
</figure>

<p>The recommended approach is exemplified by Starbucks: the watch app tells the user to log in, and the handoff feature promptly takes the user to the login screen in the iPhone Starbucks app.</p>

<figure class="caption"><img alt="" border="0" height="504" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2015/05/14/handoff-starbucks.png" width="450"/>
<figcaption><em>Starbucks asked users to log in to use the watch; the handoff feature took users directly to the corresponding login screen in the iPhone app.</em></figcaption>
</figure>

<h2>Standalone Content</h2>

<p>Just because the screen is small is no license to truncate content. Although handoff should be supported, the content on the watch should be treated as an independent piece of content that can stand by itself. Moving from watch to phone is a high-cost action that should not be needed for most quick interactions. The <a href="../worlds-best-headlines-bbc-news/index.php">BBC has been writing meaningful headlines in 34 characters</a> since the days of teletext. If Auntie can do it, so can you.</p>

<figure class="caption"><img alt="" border="0" height="188" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2015/05/14/usatoday.png" width="150"/>
<figcaption><em>USA Today story headline is truncated and does not stand by itself.</em></figcaption>
</figure>

<p>The Buzzfeed app displays a poll in its app, but good luck understanding what the poll options are. The splash screen and the low speed aside (some image elements of the page load only after a significant time, thus increasing the confusion), it is hard to say whether we’re looking at an invitation to vote or just at the results of a poll collected elsewhere.</p>

<figure class="caption"><img alt="" border="0" height="404" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2015/05/14/buzzfeed.png" width="350"/>
<figcaption><em>Buzzfeed displays a poll but the options in the poll are truncated.</em></figcaption>
</figure>

<p>Sometimes apps eliminate important information in their attempt to streamline information and take advantage of the small screen. For instance, when I first started The Weather Channel app, I was presented with a screen that showed that the current temperature to be 5 degrees Celsius (41 degrees Fahrenheit). Even at 9:48 pm, this is uncommonly cold for a May evening in Sunnyvale, California. The app simply copied the first screen in my list of locations on my Weather Channel app, which happened to be Tahoe City. On my phone, I could see that 5 degrees Celsius corresponded to Tahoe City and easily scroll to my current location, but on the watch there was no indication of which city this temperature corresponded to.</p>

<figure class="caption"><img alt="" border="0" height="188" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2015/05/14/tweatherchan.png" width="150"/>
<figcaption><em>The Weather Channel app does not show the location for the temperature displayed. This location is different than the current location.</em></figcaption>
</figure>

<h2>Focus on the Essential</h2>

<p>You don’t need a watch app just because everybody has one. Fandango has an app that displays movie quotes, and the BuzzFeed app above is completely useless, especially in the current implementation. And, at the other end of the spectrum, a watch app should not be an attempt to replicate the iPhone app: the screen size is too small and <a href="../scaling-user-interfaces/index.php">cannot accommodate the same amount of information</a> as the substantially larger iPhone screen.</p>

<p>History repeats itself:</p>

<ul>
	<li>The lesson of 1997–1999 was that a website was <a href="../differences-between-print-design-and-web-design/index.php">not a glossy brochure</a>, <a href="../the-telephone-is-the-best-metaphor-for-the-web/index.php">nor a TV show</a>.</li>
	<li>The lesson of 2007–2009 was that a mobile phone is not just a smaller computer.</li>
	<li>Maybe the third time will be the charm — say it out loud: <em>a watch is not a smaller phone.</em></li>
</ul>

<figure class="caption"><img alt="" border="0" height="388" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2015/05/14/fandango.png" width="150"/>
<figcaption><em>The Fandango watch app has a single screen with a quote from a movie.</em></figcaption>
</figure>

<p>The average interaction with an app on the phone is about 70 seconds and about half the duration of a web session on a computer. On the watch, we can expect the average session size to be substantially shorter. Think of the information that people care for and that they can access easily in <strong>just a few seconds</strong>. That’s what you should offer on the watch.</p>

<p>The small screen size forces designers to think hard about (1) what users care about most in their app; (2) how to compress that information so that it fits the tiny screen. Complex interactions do not belong on the watch: Remember that people can always go to their iPhone if they need more.</p>

<p>Allowing users to manipulate the level of detail in a stock chart (like Apple’s Stock app does) is unnecessary and does not belong on a watch. If I care about a stock enough as to have it in my list of stocks on my phone, I probably am mostly interested in checking last-minute changes on my watch, and not in researching its performance in the last 6 months.</p>

<figure class="caption"><img alt="" border="0" height="188" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2015/05/14/stocks-chart.png" width="150"/>
<figcaption><em>In the Stocks app people can see a chart of how the stock changed over time for different time periods: for 1 day, 1 week, 1 month, or 6 months. These extra features are unwarranted for such a small screen and force users to interact with tiny targets while taking up 16% of the available pixels that would have been better allocated to the primary content.</em></figcaption>
</figure>

<p>Similarly, Zillow, the real-estate app, shows a (house) price and a map, but who can tell where the map is, based on the aerial view? The <em>72 ft</em> title is cryptic: is it 72 ft from me? In what direction? Is this the closest house for sale, just the closest house, or a random house that is 72ft away? Where is it on the map? Luckily, if I scrolled down I could see the address, but the map is pretty much useless in this type of display.</p>

<figure class="caption"><img alt="" border="0" height="383" src="http://s3.amazonaws.com/media.nngroup.com/media/editor/2015/05/14/zillow.png" width="150"/>
<figcaption><em>Zillow shows information about house prices close to the current location, but it is impossible to tell what house it refers to without scrolling down to read the address. The aerial map does not add any value to the page.</em></figcaption>
</figure>

<h2>Recommendations for Designers</h2>

<p>If you’re thinking of designing a watch app, think twice, because your intended app may be no good on this platform. If you think you can create value for watch users, follow these guidelines to allow users to actually reap this value:</p>

<ul>
	<li><strong>Distill </strong>the essential content that people are interested in and present it in a compressed form that would fit the tiny watch screen.</li>
	<li><strong>Avoid </strong>buttons and complex navigation as much as possible, and if you do include buttons make them few and big.</li>
	<li>Use <strong>handoff </strong>to phone to enable users to get more details and solve problems that require more complicated interactions.</li>
	<li>Create <strong>standalone </strong>bits of text that can easily be read and comprehended and truly convey the gist of your content.</li>
</ul>
</section>
  

  
  <section>
    
        
    
  </section>
  
  
  
  
  

  


  <section>
    <br />
    <p class="share"><b>Share <span class="hide-for-small-down">this article</span>:</b> <a href="https://twitter.com/intent/tweet?url=http://www.nngroup.com/articles/smartwatch/&amp;text=The%20Apple%20Watch:%20User-Experience%20Appraisal&amp;via=nngroup">Twitter</a> | <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=http://www.nngroup.com/articles/smartwatch/&amp;title=The%20Apple%20Watch:%20User-Experience%20Appraisal&amp;source=Nielsen%20Norman%20Group">LinkedIn</a> | <a href="https://plus.google.com/share?url=http://www.nngroup.com/articles/smartwatch/">Google+</a> | <a href="mailto:?subject=NN/g Article: The Apple Watch: User-Experience Appraisal&amp;body=http://www.nngroup.com/articles/smartwatch/">Email</a></p>
    <hr />
  </section>

  <footer class="l-subsection">
    
    
    <div class="learn-more">
      <h2>Learn More</h2>
      <p><a href="../subscribe/index.php">Subscribe to the weekly newsletter</a> to get notified about future articles.</p>
      </div>
    
    

    <div class="row show-for-small-down related-content">
      
      
      

      
      
        <div class="column small-12 topic-links">All articles about:
          <ul>
            
            <li><a href="../../topic/ios/index.php">ios</a></li>
            
            <li><a href="../../topic/mobile-and-tablet-design/index.php">Mobile &amp; Tablet</a></li>
            
            <li><a href="../../topic/smartwatches/index.php">smartwatches</a></li>
            
          </ul>
        </div>
      
      

      
      
        <div class="column small-12">
          <h3>UX Conference Training Courses</h3>
           <ul class="no-bullet">
    
        <li><a href="../../courses/cross-channel-user-experience/index.php">Omnichannel Journeys and Customer Experience</a></li>
    
        <li><a href="../../courses/usability-mobile-websites-apps/index.php">Mobile User Experience</a></li>
    
        <li><a href="../../courses/scaling-responsive-design/index.php">Scaling User Interfaces</a></li>
    
        <li>Visual Design for Mobile and Tablet: <a href="../../courses/vis-mob-1/index.php">Day 1</a> and <a href="../../courses/vis-mob-2/index.php">Day 2</a></li>
    
</ul>
        </div>
      
      

      
      
        <div class="small-12 column">
          <h3>Research Reports</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../reports/tablets/index.php">Tablet Website and Application UX</a></li>
              
            
              
                <li><a href="../../reports/mobile-website-and-application-usability/index.php">User Experience for Mobile Applications and Websites</a></li>
              
            
              
                <li><a href="../../reports/enterprise-mobile-showcase/index.php">Mobile Intranets and Enterprise Apps</a></li>
              
            
              
                <li><a href="../../reports/wap-usability/index.php">WAP Usability</a></li>
              
            
              
                <li><a href="../../reports/ipad-app-and-website-usability/index.php">iPad App and Website Usability</a></li>
              
            
          </ul>
        </div>
      
      

      
      
        <div class="small-12 column">
          <h3>Online Seminars</h3>
          <ul class="no-bullet">
            
              
                <li><a href="../../online-seminars/every-word-count/index.php">Making Every Word Count</a></li>
              
            
              
                <li><a href="../../online-seminars/mobile-user-testing/index.php">Mobile User Testing</a></li>
              
            
          </ul>
        </div>
      
      
    </div>

    <div class="hide-for-small-down row related-content">
      <div class="medium-6 columns">
        
        
          <div>
            <h3>Articles</h3>
            <ul class="no-bullet">
              
                
                <li><a href="../3d-touch/index.php">3D Touch on iPhone 6S: Embrace the Force</a></li>
                
              
                
                <li><a href="../samsung-watch/index.php">Smartwatches Are the Future—But Samsung Galaxy Gear Only Partway There</a></li>
                
              
                
                <li><a href="../ios-9-back-to-app-button/index.php">iOS 9 App Switching and the Back-to-App Button</a></li>
                
              
                
                <li><a href="../4-ios-rules-break/index.php">4 iOS Rules to Break </a></li>
                
              
                
                <li><a href="../wechat-integrated-ux/index.php">WeChat: China’s Integrated Internet User Experience</a></li>
                
              
            </ul>
          </div>
        
        

        
          
          <div>
            <h3>Research Reports</h3>
            <ul class="no-bullet">
              
                
                  <li><a href="../../reports/tablets/index.php">Tablet Website and Application UX</a></li>
                
              
                
                  <li><a href="../../reports/mobile-website-and-application-usability/index.php">User Experience for Mobile Applications and Websites</a></li>
                
              
                
                  <li><a href="../../reports/enterprise-mobile-showcase/index.php">Mobile Intranets and Enterprise Apps</a></li>
                
              
                
                  <li><a href="../../reports/wap-usability/index.php">WAP Usability</a></li>
                
              
                
                  <li><a href="../../reports/ipad-app-and-website-usability/index.php">iPad App and Website Usability</a></li>
                
              
            </ul>
          </div>
          
        

      </div>
      <div class="medium-6 columns">
        
          
          <div>
            <h3>UX Conference Training Courses</h3>
             <ul class="no-bullet">
    
        <li><a href="../../courses/cross-channel-user-experience/index.php">Omnichannel Journeys and Customer Experience</a></li>
    
        <li><a href="../../courses/usability-mobile-websites-apps/index.php">Mobile User Experience</a></li>
    
        <li><a href="../../courses/scaling-responsive-design/index.php">Scaling User Interfaces</a></li>
    
        <li>Visual Design for Mobile and Tablet: <a href="../../courses/vis-mob-1/index.php">Day 1</a> and <a href="../../courses/vis-mob-2/index.php">Day 2</a></li>
    
</ul>
          </div>
          
        
        
          
          <div>
            <h3>Online Seminars</h3>
            <ul class="no-bullet">
            
                <li><a href="../../online-seminars/every-word-count/index.php">Making Every Word Count</a></li>
            
                <li><a href="../../online-seminars/mobile-user-testing/index.php">Mobile User Testing</a></li>
            
            </ul>
          </div>
          
        
      </div>
    </div>
  </footer>
</article>

        </div>

      

    
  </div>
  <div class="row">
    
      <div class="column small-12 show-for-small-down l-navrail announcements">
        
          
        
      </div>
    
    
  </div>

    </div>
    




<footer id="page-footer">
    <div class="row hide-for-print">
        <div class="column small-12 medium-6">
            <section class="footer-popular">
                
                <h1>Popular Topics</h1>
                <div style="float: left;  width: 15em; display: block;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/applications/index.php">Application Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/e-commerce/index.php">E-Commerce Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/eyetracking/index.php">Eyetracking </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/information-architecture/index.php">Information Architecture </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/intranets/index.php">Intranet Design </a></li>
	<li><a href="../../topic/interaction-design/index.php">Interaction Design </a></li>
</ul>
</div>

<div style="float: left;  width: 15em; display: block; list-style: none;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/mobile-and-tablet-design/index.php">Mobile and Tablet Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/credibility-persuasion/index.php">Persuasive Design</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/research-methods/index.php">Research Methods</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/user-testing/index.php">User Testing </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/web-usability/index.php">Web Usability </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/writing-web/index.php">Web Writing &amp; Content Strategy </a></li>
</ul>
</div>

<p>&nbsp;</p>

                
            </section>
        </div>

        <div class="column small-12 medium-6">
            <div class="row">
                <div class="column small-6">
                    <section class="footer-about">
                        <h1>Follow Us</h1>
                        <ul>
	<li><a href="../subscribe/index.php">Subscribe to our weekly UX newsletter</a>,&nbsp;Jakob Nielsen&#39;s Alertbox</li>
	<li>Visit Don Norman&#39;s website, <a href="http://www.jnd.org/"> www.jnd.org </a></li>
	<li>Visit Bruce Tognazzini&#39;s website, <a href="http://www.asktog.com/"> www.asktog.com </a></li>
	<li style="padding-top: 6px;"><a class="twitter-follow-button" data-show-count="false" data-size="large" href="https://twitter.com/nngroup"><img alt="Follow @nngroup on Twitter" height="20" src="https://media.nngroup.com/media/editor/2016/12/30/twitter-follow-button.png" style="width: 117px; height: 20px;" width="117" /></a></li>
	<li style="padding-top: 6px;"><img alt="" height="14" src="https://media.nngroup.com/media/editor/2016/07/26/feed-icon-14x14.png" style="vertical-align:text-top;width:14px;margin-right:.25em;" width="14" /><a href="../../feed/rss/index.php">Subscribe to our RSS feed</a></li>
</ul>

                    </section>
                </div>

                <div class="column small-6">
                    <section class="footer-about right-column">
                        <h1>About
</h1>
                        <ul class="no-bullet">
	<li><a href="../../about/contact/index.php">Contact information</a></li>
	<li><a href="../../about/index.php">About Us</a></li>
	<li><a href="../../about/why-nng/index.php">Why NN/g? </a></li>
	<li><a href="../../return-policy/index.php">Return Policy</a></li>
</ul>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="copyright column small-12">
            <p><a href="../../copyright-and-reprint-info/index.php">Copyright</a> &copy; 1998-2017 Nielsen Norman Group, All Rights Reserved.</p>
        </div>
    </div>
</footer>


    


        <script src="https://media.nngroup.com/static/vendor/fastclick/js/fastclick.min.js"></script>
        <script src="https://media.nngroup.com/static/vendor/foundation/js/foundation.min.js"></script>
        <script src="http://media.nngroup.com/static/js/app.js"></script>
        <script src="http://media.nngroup.com/static/js/scroll-to-fixed.js"></script><script>var externalLinks = document.querySelectorAll('a[href^="http"]');for (var i = externalLinks.length-1; i >= 0; i--) {externalLinks[i].addEventListener("click", function(event) { alert("External links are disabled for this test."); event.preventDefault(); }, false);}var mailLinks = document.querySelectorAll('a[href^="mailto"]');for (var i = mailLinks.length-1; i >= 0; i--) {mailLinks[i].addEventListener("click", function(event) { alert("Email links are disabled for this test."); event.preventDefault(); }, false);}var externalForms = document.querySelectorAll('form');for (var i = externalForms.length-1; i >= 0; i--) {externalForms[i].addEventListener("submit", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false); externalForms[i].addEventListener("click", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false);}</script>

        <script>
            
            $(function() {
                $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
                $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
            });
        </script>

        
<script src="https://media.nngroup.com/static/js/articles.js"></script>


        
        <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1072699960;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072699960/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
    </body>

<!-- Mirrored from www.nngroup.com/articles/smartwatch/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:13:06 GMT -->
</html>
