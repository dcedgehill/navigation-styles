<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->





    
<!-- Mirrored from www.nngroup.com/training/london/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 14:54:45 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1026.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"licenseKey":"1a30ef9931","queueTime":0,"beacon":"bam.nr-data.net","errorBeacon":"bam.nr-data.net","applicationID":"2048992","transactionName":"NFxSMRBUX0EDUkIPDA0WdhAMVkVbDV8ZAxUGV0QWTENYVxVCDCMVBldEIQdBUFsOH1EDFw==","applicationTime":382,"agent":""}</script>
        <title>London Agenda for the UX Conference 2017 | Nielsen Norman Group</title><meta property="og:title" content="London Agenda for the UX Conference 2017 | Nielsen Norman Group" />
  
        
        <meta name="description" content="In-depth, full-day courses, teaching user experience best practices for successful design. Conference focus on long-lasting skills for UX professionals. 25-31 March 2017.">
        <meta property="og:description" content="In-depth, full-day courses, teaching user experience best practices for successful design. Conference focus on long-lasting skills for UX professionals. 25-31 March 2017." />
        
  
        
	
        
        <meta name="keywords" content="London, UK, United Kingdom, England, usability, UX training, user experience training, UX conference, user experience conference, usability conference, UX conferences, UX seminars, UX seminar, UX training, UX courses, UX course, usability conferences, usability seminars, usability seminar, usability training, usability courses, usability course, user interface course, user interface training, interaction design conference, IXD conference, interaction design training, IXD training, IXD course, interaction design course, IXD courses">
        

        

        <meta name="robots" content="noarchive">
        <meta name="viewport" content="width=device-width">

        <link rel="search" type="application/opensearchdescription+xml" title="NN/g Search" href="../../opensearch.xml" />
        <link rel="shortcut icon" href="http://media.nngroup.com/static/img/favicon.ico" />

        <link rel="stylesheet" href="https://media.nngroup.com/static/vendor/font-awesome/css/font-awesome.min.css">

        
        <link rel="stylesheet" href="https://media.nngroup.com/static/css/7232a960f1df.css" type="text/css" />
        <link rel="stylesheet" href="http://fast.fonts.com/t/1.css?apiType=css&amp;projectid=c5846260-9def-46ec-9407-0f1349bd2d64" type="text/css">

        <script src="https://media.nngroup.com/static/vendor/modernizr/js/modernizr.min.js"></script>

        <script type="text/javascript" src="https://media.nngroup.com/static/js/3283be5def74.js"></script>
        

        
<meta name="robots" content="noindex, nofollow" />


        
        
        <!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=229034,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('http://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->


<!-- Google Analytics Tracking Code for http://nngroup.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23132681-1', 'nngroup.com');
  ga('require', 'linkid', 'https://www.nngroup.com/training/london/linkid.js');
  ga('require', 'displayfeatures');

// Optimizely Universal Analytics Integration
 window.optimizely = window.optimizely || [];
 window.optimizely.push("activateUniversalAnalytics");

  ga('send', 'pageview');

</script>
<script>
/**
* Function to track a click on an outbound link in Google Analytics.
*/
function trackOutboundLink(label, url) {
    if(window.ga && ga.loaded) {
        ga('send', 'event', 'outbound', 'click', label, {'hitCallback': function(){document.location = url;}
        });
    } else {
        document.location = url;
    }
}
/**
* Function to track a click on a link in the article announcement area.
*/
$('#articleAnnouncement a').on('click', function(){
  ga('send', 'event', 'link', 'click', 'articleAnnouncement');
});
</script>

<script type="text/javascript">
 setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2421.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



<!-- Hotjar Tracking Code for http://nngroup.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:121605,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

        <script src="https://media.nngroup.com/static/js/analytics/track_outbound_link.js"></script>
        
        
        
    </head>
    <body class="location-training location-event-detail event-detail">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
    <header id="page-header">
        <div id="cart" class="shopping-cart mobile-cart show-for-small-down"></div>
        <div class="row">
            <div id="masthead" class="masthead small-12 medium-7 column">

                <!-- The button to toggles menu on small screen -->

                <h1><a href="../../index.php"><img src="http://media.nngroup.com/static/img/logo-wordmark.png" width="74" height="30"> Nielsen <span class="norman">Norman</span> Group</a></h1>
                <p class="tagline show-for-medium-up">Evidence-Based User Experience Research, Training, and Consulting</p>

                <!-- The button to toggles form search on small screen -->


                <div style="clear: right;width:  100%;height: 1px;"></div><?php define('ROOT_PATH',$_SERVER["DOCUMENT_ROOT"] . '/apps/navigation_styles/navigations/2'); ?><?php include(ROOT_PATH)."/navigation.php"; ?>

                <!-- Drop down menu on small screen -->
                <div id="hideNav" class="show-for-small-down">
                  
                  <ul class="menu-content">
                    <li><a href="../../index.php">Home</a></li>
                    <li class="has-submenu">
                      <a>Training</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../index.php">Overview</a></li>
                        
<li id="nav-ux-conference"><a href="../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../online-seminars/index.php"><span>Online Seminars</span></a></li>

                      </ul>
                    </li>
                    <li class="has-submenu">
                      <a>Consulting</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
  <li><a href="../../consulting/index.php">Overview</a></li>
  
  <li><a href="../../consulting/benchmarking/index.php">Benchmarking</a></li>
  
  <li><a href="../../consulting/expert-design-review/index.php">Expert Design Review</a></li>
  
  <li><a href="../../consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
  
  <li><a href="../../consulting/usability-testing/index.php">Usability Testing </a></li>
  
  <li><a href="../../consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
  
  <li><a href="../../in-house-training/index.php">In-House Training</a></li>
</ul>

                    </li>

                    <li><a href="../../reports/index.php">Reports</a></li>
                    <li><a href="../../articles/index.php">Articles</a></li>
                    <li class="has-submenu"><a>About NN/G</a><i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../about/index.php">Overview</a></li>
                        <li><a href="../../people/index.php">People</a></li>
                        <li><a href="../../about/why-nng/index.php">Why NN/g?</a></li>
                        <li><a href="../../about/contact/index.php">Contact</a></li>
                        <li><a href="../../news/index.php">News</a></li>
                        <li><a href="../../about/history/index.php">History</a></li>
                        <li><a href="../../books/index.php">Books</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                </div>

                <hr class="header-divider show-for-small-only"/>


            </div>
       </div>

        <div id="hideNav">            <nav class="primary-nav small-12 column">
                <ul>
                    <li><a class="navitem" id="nav-home" href="../../index.php">Home</a></li>
                    <li><a class="navitem" id="nav-training" href="../index.php">Training</a></li>
                    <li><a class="navitem" id="nav-consulting" href="../../consulting/index.php">Consulting</a></li>
                    <li><a class="navitem" id="nav-reports" href="../../reports/index.php">Reports</a></li>
                    <li><a class="navitem" id="nav-articles" href="../../articles/index.php">Articles</a></li>
                    <li><a class="navitem" id="nav-about" href="../../about/index.php">About NN/G</a></li>
                </ul>
            </nav>
            <nav class="secondary-nav small-12 medium-12 large-12 columns">
                
  <ul class="inline-list">
  
<li id="nav-ux-conference"><a href="../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../online-seminars/index.php"><span>Online Seminars</span></a></li>

</ul>


            </nav>
        </div>
    </header>
    <div id="main" class="l-content" onclick="closeNav(event)"><span id="preventClose" style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776; Menu</span>
    



  <article class="event-detail">
    <div class="row">
      

      
      <div class="l-section-intro small-12 column">
        <header>
          
            <h1>London UX Conference <span class="hide-for-small-down">/</span></h1>
          
          <h2 class="uwDate">
            <span class="hide-for-small-down">March 25 &ndash; 31, 2017</span>
            <span class="show-for-small-down">March 25 &ndash; 31, 2017</span>
          </h2>
        </header>
      </div>
      

    </div>
    <div class="row">
      <div class="column small-12">
        


<div class="row">
  <div class="l-subsection small-12 medium-6 medium-push-6 columns hero-image">
    
    
    
    <img class="show-for-medium-up" srcset="https://media.nngroup.com/media/files/images/IMG_1658_copy_1350x900.jpg.450x300_q95_autocrop_crop_upscale@2x.jpg 2x" src="https://media.nngroup.com/media/files/images/IMG_1658_copy_1350x900.jpg.450x300_q95_autocrop_crop_upscale.jpg" alt="Event course participant" />
    
    <img class="show-for-small-down" src="https://media.nngroup.com/media/city_images/london.jpg.450x185_q95_autocrop_crop-smart_upscale.jpg" alt="City of London">
    
    <div class="hide-for-small-down text-center">
      <p><a href="../gallery/index.php">See more scenes from recent UX Conferences</a></p>
    </div>
  </div>
  <div class="l-subsection small-12 medium-6 medium-pull-6 event-description-text columns">
    <p><strong>NN/g&#39;s&nbsp;<a href="../index.php">UX Conference</a>&nbsp;helps you get up to speed on user experience best practices so you can create successful interfaces.&nbsp;</strong></p>

    <div class="hide-for-small-down">
      <ul>
	<li>7 days of in-depth, full-day courses&nbsp;(attend as few or as many as you like)</li>
	<li>Proven methods and best practices</li>
	<li>Expert instructors teach practical skills</li>
	<li>No sales pitches</li>
	<li><a href="../../ux-certification/index.php">UX Certification</a>&nbsp;to&nbsp;test knowledge and&nbsp;build&nbsp;credibility</li>
</ul>

<p>Learn more about&nbsp;<a href="../why-attend/index.php">why you should attend the UX Conference</a>.</p>

    </div>
    <p class="show-for-small-down"><strong>Learn more about</strong>: <a href="../why-attend/index.php">Why you should attend the UX Conference</a></p>
    <div class="register-pricing-links">
      <p>
        
        <a href="../register/87/index.php" class="button large-register-button register-button">Register</a>
        
      </p>
    </div>
  </div>
</div>

<div class="row">
  <div class="l-event-content small-12 column">
    <dl class="tabs show-for-medium-up">
      <dd class="active"><a href="#schedule">Course Agenda</a></dd>
      <dd><a href="#location">Location &amp; Accommodations</a></dd>
      <dd><a href="#pricing">Pricing</a></dd>
      <dd><a href="#faq">FAQ</a></dd>
      <dd><a href="#experience">The Experience</dd>
    </dl>

    <ul class="tabs-content" id="tab-content-area">
      <li id="scheduleTab" class="active content">
        
        <section class="l-subsection ontab collapsable-content event-agenda">
          <h1 class="hide-for-medium-up collapse-link">
            <a class="unstyled" href="#schedule">Course Agenda</a>
          </h1>
          <div class="collapsable">
            <h1 class="hide-for-small-down">Schedule at the UX Conference London</h1>
            
            <p>How it works: Each course is a full day (9am-5pm), so choose ONE course topic for each day. Attend up to 7 days, or just 1.</p>

            
              <h2 class="course_date">
                
                Saturday, March 25 <span>/ 2 full-day courses</span>
                
              </h2>

              
              <ul class="medium-block-grid-4 event-course">
                
                
                <li>
                  <a href="../course/1540/ux-basic-training/index.php">
                    <article class="courseinfo">
  <h1>
    UX Basic Training
    
  </h1>
  <h2>Be an effective UX professional: Know the lingo and sell the process</h2>
  <div class="card-list specialties">
    <ul>
      
        <li class="specialty-label"><i class="fa fa-stop swatch" aria-hidden="true" style="color: #fa52cd"></i>Interaction</li>
      
    </ul>
  </div>

  

</article>

                  </a>
                </li>
                

                
                  
                
              
                
                <li>
                  <a href="../course/1541/ux-vp-director/index.php">
                    <article class="courseinfo">
  <h1>
    The UX VP/Director
    
  </h1>
  <h2>Leading UX at the Executive Level</h2>
  <div class="card-list specialties">
    <ul>
      
        <li class="specialty-label"><i class="fa fa-stop swatch" aria-hidden="true" style="color: #cc0000"></i>Management</li>
      
    </ul>
  </div>

  

</article>

                  </a>
                </li>
                

                
                  
                  
                    
                    <li class="hide-for-small-down"></li>
                    
                  
                  <li class="hide-for-small-down">
                    <article class="specialties-legend">
  <p>
    <a href="../../ux-certification/index.php#specialties">UX Certification Specialty</a> credit is indicated by the colored labels:
  </p>
  <ul>
  
    <li class="specialty-label"><i class="fa fa-stop swatch" aria-hidden="true" style="color: #fa52cd"></i>Interaction Design</li>
  
    <li class="specialty-label"><i class="fa fa-stop swatch" aria-hidden="true" style="color: #00aa00"></i>Mobile Foundations</li>
  
    <li class="specialty-label"><i class="fa fa-stop swatch" aria-hidden="true" style="color: #cc0000"></i>UX Management</li>
  
    <li class="specialty-label"><i class="fa fa-stop swatch" aria-hidden="true" style="color: #6699ff"></i>UX Research</li>
  
    <li class="specialty-label"><i class="fa fa-stop swatch" aria-hidden="true" style="color: #ffcc00"></i>Web Design</li>
  
  </ul>
</article>

                  </li>
                  
                
              
              </ul>
              
            
              <h2 class="course_date">
                
                Sunday, March 26 <span>/ 4 full-day courses</span>
                
              </h2>

              
              <ul class="medium-block-grid-4 event-course">
                
                
                <li>
                  <a href="../course/1544/translating-brand/index.php">
                    <article class="courseinfo">
  <h1>
    Translating Brand into User Interactions
    
  </h1>
  <h2>How UX design can bridge the gap between brand promise and user experience</h2>
  <div class="card-list specialties">
    <ul>
      
        <li class="specialty-label"><i class="fa fa-stop swatch" aria-hidden="true" style="color: #fa52cd"></i>Interaction</li>
      
    </ul>
  </div>

  

</article>

                  </a>
                </li>
                

                
                  
                
              
                
                <li>
                  <a href="../course/1545/becoming-ux-strategist/index.php">
                    <article class="courseinfo">
  <h1>
    Becoming a UX Strategist
    
  </h1>
  <h2>Learning how to envision, plan, and successfully manage user-centered cultures, teams, and organizations</h2>
  <div class="card-list specialties">
    <ul>
      
        <li class="specialty-label"><i class="fa fa-stop swatch" aria-hidden="true" style="color: #cc0000"></i>Management</li>
      
    </ul>
  </div>

  

</article>

                  </a>
                </li>
                

                
                  
                
              
                
                <li>
                  <a href="../course/1542/personas/index.php">
                    <article class="courseinfo">
  <h1>
    Personas: Turn User Data Into User-Centered Design
    
  </h1>
  <h2>Successfully turn user data into user interfaces. Learn how to create, maintain and utilize personas throughout the UX design process.</h2>
  <div class="card-list specialties">
    <ul>
      
        <li class="specialty-label"><i class="fa fa-stop swatch" aria-hidden="true" style="color: #6699ff"></i>Research</li>
      
    </ul>
  </div>

  

</article>

                  </a>
                </li>
                

                
                  
                
              
                
                <li>
                  <a href="../course/1543/information-architecture/index.php">
                    <article class="courseinfo">
  <h1>
    Information Architecture
    
  </h1>
  <h2>Organize and structure information to improve findability and discoverability</h2>
  <div class="card-list specialties">
    <ul>
      
        <li class="specialty-label"><i class="fa fa-stop swatch" aria-hidden="true" style="color: #ffcc00"></i>Web</li>
      
    </ul>
  </div>

  

</article>

                  </a>
                </li>
                

                
                  
                
              
              </ul>
              
            
              <h2 class="course_date">
                
                Monday, March 27 <span>/ 4 full-day courses</span>
                
              </h2>

              
              <ul class="medium-block-grid-4 event-course">
                
                
                <li>
                  <a href="../course/1548/human-mind/index.php">
                    <article class="courseinfo">
  <h1>
    The Human Mind and Usability
    
  </h1>
  <h2>Apply psychology principles to predict and explain how your customers think and act</h2>
  <div class="card-list specialties">
    <ul>
      
        <li class="specialty-label"><i class="fa fa-stop swatch" aria-hidden="true" style="color: #fa52cd"></i>Interaction</li>
      
        <li class="specialty-label"><i class="fa fa-stop swatch" aria-hidden="true" style="color: #00aa00"></i>Mobile Foundations</li>
      
    </ul>
  </div>

  

</article>

                  </a>
                </li>
                

                
                  
                
              
                
                <li>
                  <a href="../course/1546/ux-deliverables/index.php">
                    <article class="courseinfo">
  <h1>
    UX Deliverables
    
  </h1>
  <h2>Effectively communicate UX design ideas and research findings to managers, collaborators, and other stakeholders.</h2>
  <div class="card-list specialties">
    <ul>
      
        <li class="specialty-label"><i class="fa fa-stop swatch" aria-hidden="true" style="color: #cc0000"></i>Management</li>
      
    </ul>
  </div>

  

</article>

                  </a>
                </li>
                

                
                  
                
              
                
                <li>
                  <a href="../course/1566/designing-influence/index.php">
                    <article class="courseinfo">
  <h1>
    Designing Influence
    
  </h1>
  <h2>Getting your best ideas adopted while building trusted relationships</h2>
  <div class="card-list specialties">
    <ul>
      
        <li class="specialty-label"><i class="fa fa-stop swatch" aria-hidden="true" style="color: #cc0000"></i>Management</li>
      
    </ul>
  </div>

  

</article>

                  </a>
                </li>
                

                
                  
                
              
                
                <li>
                  <a href="../course/1547/navigation-design/index.php">
                    <article class="courseinfo">
  <h1>
    Navigation Design
    
  </h1>
  <h2>Menu styles and UI components for effective navigation</h2>
  <div class="card-list specialties">
    <ul>
      
        <li class="specialty-label"><i class="fa fa-stop swatch" aria-hidden="true" style="color: #ffcc00"></i>Web</li>
      
    </ul>
  </div>

  

</article>

                  </a>
                </li>
                

                
                  
                
              
              </ul>
              
            
              <h2 class="course_date">
                
                Tuesday, March 28 <span>/ 4 full-day courses</span>
                
              </h2>

              
              <ul class="medium-block-grid-4 event-course">
                
                
                <li>
                  <a href="../course/1552/generating-big-ideas/index.php">
                    <article class="courseinfo">
  <h1>
    Generating Big Ideas with Design Thinking
    
  </h1>
  <h2>Unearthing user pain points to drive breakthrough design concepts</h2>
  <div class="card-list specialties">
    <ul>
      
        <li class="specialty-label"><i class="fa fa-stop swatch" aria-hidden="true" style="color: #fa52cd"></i>Interaction</li>
      
    </ul>
  </div>

  

</article>

                  </a>
                </li>
                

                
                  
                
              
                
                <li>
                  <a href="../course/1551/measuring-ux/index.php">
                    <article class="courseinfo">
  <h1>
    Measuring User Experience
    
  </h1>
  <h2>UX metrics and quantitative usability studies to measure the effectiveness and business value of your design</h2>
  <div class="card-list specialties">
    <ul>
      
        <li class="specialty-label"><i class="fa fa-stop swatch" aria-hidden="true" style="color: #6699ff"></i>Research</li>
      
    </ul>
  </div>

  

</article>

                  </a>
                </li>
                

                
                  
                
              
                
                <li>
                  <a href="../course/1553/emerging-patterns-web-design/index.php">
                    <article class="courseinfo">
  <h1>
    Emerging Patterns for Web Design
    
  </h1>
  <h2>Innovative trends that impact the user experience</h2>
  <div class="card-list specialties">
    <ul>
      
        <li class="specialty-label"><i class="fa fa-stop swatch" aria-hidden="true" style="color: #ffcc00"></i>Web</li>
      
    </ul>
  </div>

  

</article>

                  </a>
                </li>
                

                
                  
                
              
                
                <li>
                  <a href="../course/1550/content-strategy/index.php">
                    <article class="courseinfo">
  <h1>
    Content Strategy
    
  </h1>
  <h2>Business value and processes behind a successful content strategy</h2>
  <div class="card-list specialties">
    <ul>
      
        <li class="specialty-label"><i class="fa fa-stop swatch" aria-hidden="true" style="color: #ffcc00"></i>Web</li>
      
    </ul>
  </div>

  

</article>

                  </a>
                </li>
                

                
                  
                
              
              </ul>
              
            
              <h2 class="course_date">
                
                Wednesday, March 29 <span>/ 4 full-day courses</span>
                
              </h2>

              
              <ul class="medium-block-grid-4 event-course">
                
                
                <li>
                  <a href="../course/1555/cross-channel-user-experience/index.php">
                    <article class="courseinfo">
  <h1>
    Omnichannel Journeys and Customer Experience
    
  </h1>
  <h2>Create a usable cross-channel experience, including the web, email, social media, phone, print, apps and online chat</h2>
  <div class="card-list specialties">
    <ul>
      
        <li class="specialty-label"><i class="fa fa-stop swatch" aria-hidden="true" style="color: #fa52cd"></i>Interaction</li>
      
        <li class="specialty-label"><i class="fa fa-stop swatch" aria-hidden="true" style="color: #00aa00"></i>Mobile Foundations</li>
      
    </ul>
  </div>

  

</article>

                  </a>
                </li>
                

                
                  
                
              
                
                <li>
                  <a href="../course/1556/design-tradeoffs/index.php">
                    <article class="courseinfo">
  <h1>
    Design Tradeoffs and UX Decision Frameworks
    
  </h1>
  <h2>Apply rational decision-making methods to quickly resolve common design challenges</h2>
  <div class="card-list specialties">
    <ul>
      
        <li class="specialty-label"><i class="fa fa-stop swatch" aria-hidden="true" style="color: #cc0000"></i>Management</li>
      
    </ul>
  </div>

  

</article>

                  </a>
                </li>
                

                
                  
                
              
                
                <li>
                  <a href="../course/1557/stakeholder-ux-approval/index.php">
                    <article class="courseinfo">
  <h1>
    Engaging Stakeholders to Build Buy-In
    
  </h1>
  <h2>Get stakeholders on your side to launch the best possible UX design, content strategy, and other design priorities</h2>
  <div class="card-list specialties">
    <ul>
      
        <li class="specialty-label"><i class="fa fa-stop swatch" aria-hidden="true" style="color: #cc0000"></i>Management</li>
      
    </ul>
  </div>

  

</article>

                  </a>
                </li>
                

                
                  
                
              
                
                <li>
                  <a href="../course/1554/journey-mapping/index.php">
                    <article class="courseinfo">
  <h1>
    Journey Mapping to Understand Customer Needs
    
  </h1>
  <h2>Learn the process for capturing and communicating UX insights across complex interactions</h2>
  <div class="card-list specialties">
    <ul>
      
        <li class="specialty-label"><i class="fa fa-stop swatch" aria-hidden="true" style="color: #6699ff"></i>Research</li>
      
    </ul>
  </div>

  

</article>

                  </a>
                </li>
                

                
                  
                
              
              </ul>
              
            
              <h2 class="course_date">
                
                Thursday, March 30 <span>/ 4 full-day courses</span>
                
              </h2>

              
              <ul class="medium-block-grid-4 event-course">
                
                
                <li>
                  <a href="../course/1560/ideation/index.php">
                    <article class="courseinfo">
  <h1>
    Effective Ideation Techniques for UX Design
    
  </h1>
  <h2>Systematic methods for creative solutions to any UX design or redesign challenge</h2>
  <div class="card-list specialties">
    <ul>
      
        <li class="specialty-label"><i class="fa fa-stop swatch" aria-hidden="true" style="color: #fa52cd"></i>Interaction</li>
      
    </ul>
  </div>

  

</article>

                  </a>
                </li>
                

                
                  
                
              
                
                <li>
                  <a href="../course/1559/hci/index.php">
                    <article class="courseinfo">
  <h1>
    User Interface Principles Every Designer Must Know
    
  </h1>
  <h2>Learn key human-computer interaction (HCI) research findings, and how to apply them to UX design problems</h2>
  <div class="card-list specialties">
    <ul>
      
        <li class="specialty-label"><i class="fa fa-stop swatch" aria-hidden="true" style="color: #fa52cd"></i>Interaction</li>
      
        <li class="specialty-label"><i class="fa fa-stop swatch" aria-hidden="true" style="color: #00aa00"></i>Mobile Foundations</li>
      
    </ul>
  </div>

  

</article>

                  </a>
                </li>
                

                
                  
                
              
                
                <li>
                  <a href="../course/1561/facilitating-ux-workshops/index.php">
                    <article class="courseinfo">
  <h1>
    Facilitating UX Workshops
    
  </h1>
  <h2>How to design and lead goal-based group exercises for collaborative gain</h2>
  <div class="card-list specialties">
    <ul>
      
        <li class="specialty-label"><i class="fa fa-stop swatch" aria-hidden="true" style="color: #cc0000"></i>Management</li>
      
    </ul>
  </div>

  

</article>

                  </a>
                </li>
                

                
                  
                
              
                
                <li>
                  <a href="../course/1558/usability-testing/index.php">
                    <article class="courseinfo">
  <h1>
    Usability Testing
    
  </h1>
  <h2>Do-it-yourself user tests: Plan, conduct, and analyze your own studies, using in-person, remote, or online methods</h2>
  <div class="card-list specialties">
    <ul>
      
        <li class="specialty-label"><i class="fa fa-stop swatch" aria-hidden="true" style="color: #6699ff"></i>Research</li>
      
    </ul>
  </div>

  

</article>

                  </a>
                </li>
                

                
                  
                
              
              </ul>
              
            
              <h2 class="course_date">
                
                Friday, March 31 <span>/ 4 full-day courses</span>
                
              </h2>

              
              <ul class="medium-block-grid-4 event-course">
                
                
                <li>
                  <a href="../course/1564/communicating-design/index.php">
                    <article class="courseinfo">
  <h1>
    Communicating Design
    
  </h1>
  <h2>Communicate your designs efficiently through persuasive, engaging storytelling; establish a productive critique culture and feedback loop </h2>
  <div class="card-list specialties">
    <ul>
      
        <li class="specialty-label"><i class="fa fa-stop swatch" aria-hidden="true" style="color: #fa52cd"></i>Interaction</li>
      
    </ul>
  </div>

  

</article>

                  </a>
                </li>
                

                
                  
                
              
                
                <li>
                  <a href="../course/1565/lean-ux-and-agile/index.php">
                    <article class="courseinfo">
  <h1>
    Lean UX and Agile
    
  </h1>
  <h2>Applying Lean UX approaches for Agile environments</h2>
  <div class="card-list specialties">
    <ul>
      
        <li class="specialty-label"><i class="fa fa-stop swatch" aria-hidden="true" style="color: #cc0000"></i>Management</li>
      
    </ul>
  </div>

  

</article>

                  </a>
                </li>
                

                
                  
                
              
                
                <li>
                  <a href="../course/1563/analytics-and-user-experience/index.php">
                    <article class="courseinfo">
  <h1>
    Analytics and User Experience
    
  </h1>
  <h2>Interpreting data trends in conversions, pageviews, and other user actions to identify opportunities and guide UX design</h2>
  <div class="card-list specialties">
    <ul>
      
        <li class="specialty-label"><i class="fa fa-stop swatch" aria-hidden="true" style="color: #6699ff"></i>Research</li>
      
    </ul>
  </div>

  

</article>

                  </a>
                </li>
                

                
                  
                
              
                
                <li>
                  <a href="../course/1562/credibility-and-persuasive-web-design/index.php">
                    <article class="courseinfo">
  <h1>
    Persuasive Web Design
    
  </h1>
  <h2>Convince people to stay on your site, believe it, and take desirable actions</h2>
  <div class="card-list specialties">
    <ul>
      
        <li class="specialty-label"><i class="fa fa-stop swatch" aria-hidden="true" style="color: #ffcc00"></i>Web</li>
      
    </ul>
  </div>

  

</article>

                  </a>
                </li>
                

                
                  
                
              
              </ul>
              
            

            
            <div class="show-for-small-down">
              <article class="specialties-legend">
  <p>
    <a href="../../ux-certification/index.php#specialties">UX Certification Specialty</a> credit is indicated by the colored labels:
  </p>
  <ul>
  
    <li class="specialty-label"><i class="fa fa-stop swatch" aria-hidden="true" style="color: #fa52cd"></i>Interaction Design</li>
  
    <li class="specialty-label"><i class="fa fa-stop swatch" aria-hidden="true" style="color: #00aa00"></i>Mobile Foundations</li>
  
    <li class="specialty-label"><i class="fa fa-stop swatch" aria-hidden="true" style="color: #cc0000"></i>UX Management</li>
  
    <li class="specialty-label"><i class="fa fa-stop swatch" aria-hidden="true" style="color: #6699ff"></i>UX Research</li>
  
    <li class="specialty-label"><i class="fa fa-stop swatch" aria-hidden="true" style="color: #ffcc00"></i>Web Design</li>
  
  </ul>
</article>

            </div>
            

            <p class="printable"><a href="print/index.php">Printable schedule overview</a></p>

          </div>
        </section>
        
      </li>

      <li id="locationTab" class="content l-subsection">
        
        <div class="collapsable-content">
          <h1 class="hide-for-medium-up collapse-link">
            <a class="unstyled" href="#location">Location &amp; Accommodations</a>
          </h1>

          <div class="row collapsable">
            <div class="small-12 column medium-6">
              

              <section class="l-subsection">
                <h1 class="hide-for-small-down">Location</h1>
                <a href="https://www.parkplaza.co.uk/london-hotel-gb-sw1v-1eq/gbvictor" target="_blank">Park Plaza Victoria London</a>
                <p class="address">239 Vauxhall Bridge Road
                  London  SW1V 1EQ  
                  United Kingdom 
                  +44 (0) 844 415 6750
                </p>
              </section>

              <section class="l-subsection">
                <h2>Special Room Rate</h2>
                
                <ul>
                  <li>NN/g&#39;s discounted room rate at the Park Plaza Victoria has now expired, please contact the hotel direct to book a room at the Park Plaza Victoria London</li>
                  <li>Valid between March 24, 2017 and March 31, 2017</li>
                  <li>Subject to availability</li>
                </ul>
                
              </section>

              <section class="l-subsection">
                <h2>Daily Schedule</h2>
                <p>
                  8 AM: Registration<br />
                  9 AM: Classes begin<br />
                  10:20 AM: Morning break<br />
                  12:20 PM: Lunch break<br />
                  1:30 PM: Classes resume<br />
                  3 PM: Afternoon break<br />
                  5 PM: Classes end<br />
                </p>
              </section>

              <section class="l-subsection">
                <h2>Meals</h2>
                <p>Lunch will be provided as well as morning and afternoon refreshments.<br /></p>
              </section>
            </div>
            <div class="medium-6 column hide-for-small-down">
              <img src="https://media.nngroup.com/media/city_images/london.jpg.450x300_q95_autocrop_crop_upscale.jpg">
              <br /><br />
              
              <img src="http://maps.googleapis.com/maps/api/staticmap?size=450x300&amp;zoom=14&amp;key=AIzaSyA--aVuJBvvlDh4jQGWfFaO4-nDu7EtgRk&amp;markers=color:c00%7C239+Vauxhall+Bridge+Road,London+SW1V+1EQ,United+Kingdom,&amp;maptype=roadmap">
              
            </div>
          </div>
        </div>
        
      </li>

      <li  id="pricingTab" class="content">
        
        <section class="l-subsection ontab collapsable-content">
          <h1 class="hide-for-medium-up collapse-link">
            <a class="unstyled" href="#pricing">Pricing</a>
          </h1>
          <div class="collapsable">
            <div class="l-subsection" id="pricing-info">
              <h1>Tuition: Pay-by-the-day pricing</h1>
              
              <p>You can sign up for only one day or as many as 7. It all depends on the courses you'd like to take.</p>
              
<div class="row">
    <div class="column small-12">
        <table class="table-pricing pricetable show-for-medium-up">
            <thead>
            <tr>
                <th>Pay-by-the-Day Pricing</th>
                
                <th>1 Day</th>
                
                <th>2 Days</th>
                
                <th>3 Days</th>
                
                <th>4 Days</th>
                
                <th>5 Days</th>
                
                <th>6 Days</th>
                
                <th>7 Days</th>
                
            </tr>
            </thead>
            <tbody>
            

            

            
            <tr>
                <td>Regular registration until March 31</td>
                
                <td class="priceValue">£749</td>
                
                <td class="priceValue">£1243</td>
                
                <td class="priceValue">£1693</td>
                
                <td class="priceValue">£2124</td>
                
                <td class="priceValue">£2537</td>
                
                <td class="priceValue">£2928</td>
                
                <td class="priceValue">£3304</td>
                
            </tr>
            
            </tbody>
        </table>

        <table class="table-pricing pricetable hide-for-medium-up force-hide-for-medium-up">
            <thead>
            <tr>
                <th>Pay-by-the-Day Pricing</th>
                

                

                
                <td>Regular registration until March 31</td>
                
            </tr>
            </thead>
            
            <tr class="advance-registration">
                <th>1 Day</th>

                

                

                
                <td class="priceValue">£749</td>
                
            </tr>
            
            <tr class="advance-registration">
                <th>2 Days</th>

                

                

                
                <td class="priceValue">£1243</td>
                
            </tr>
            
            <tr class="advance-registration">
                <th>3 Days</th>

                

                

                
                <td class="priceValue">£1693</td>
                
            </tr>
            
            <tr class="advance-registration">
                <th>4 Days</th>

                

                

                
                <td class="priceValue">£2124</td>
                
            </tr>
            
            <tr class="advance-registration">
                <th>5 Days</th>

                

                

                
                <td class="priceValue">£2537</td>
                
            </tr>
            
            <tr class="advance-registration">
                <th>6 Days</th>

                

                

                
                <td class="priceValue">£2928</td>
                
            </tr>
            
            <tr class="advance-registration">
                <th>7 Days</th>

                

                

                
                <td class="priceValue">£3304</td>
                
            </tr>
            
            </tbody>
        </table>
    </div>

    <div class="column small-12">
        <p>
          Prices shown here are in
          British Pounds (GBP)
          Not including mandatory 20% VAT
        </p>
    </div>
</div>

            </div>
            <div class="l-subsection">
              <h1>Payment Methods Accepted</h1>
              <p>
                You may pay your tuition and exam fees by
                <strong>credit card</strong>.
              </p>
              
              <p>
                The following credit cards are accepted:<br />
                <ul class="small-block-grid-4 medium-block-grid-12">

  <li><img src="https://media.nngroup.com/media/payments/credit_card/american_express-256px.png.64x40_q95_autocrop_crop_upscale.png" height="40" width="64" alt="American Express credit card" title="American Express" /></li>

  <li><img src="https://media.nngroup.com/media/payments/credit_card/discover-256px.png.64x40_q95_autocrop_crop_upscale.png" height="40" width="64" alt="Discover credit card" title="Discover" /></li>

  <li><img src="https://media.nngroup.com/media/payments/credit_card/jcb-256px.png.64x40_q95_autocrop_crop_upscale.png" height="40" width="64" alt="JCB credit card" title="JCB" /></li>

  <li><img src="https://media.nngroup.com/media/payments/credit_card/maestro-256px.png.64x40_q95_autocrop_crop_upscale.png" height="40" width="64" alt="Maestro credit card" title="Maestro" /></li>

  <li><img src="https://media.nngroup.com/media/payments/credit_card/mastercard-256px.png.64x40_q95_autocrop_crop_upscale.png" height="40" width="64" alt="MasterCard credit card" title="MasterCard" /></li>

  <li><img src="https://media.nngroup.com/media/payments/credit_card/visa-256px.png.64x40_q95_autocrop_crop_upscale.png" height="40" width="64" alt="Visa credit card" title="Visa" /></li>

</ul>

              </p>
              
            </div>
            <div class="l-subsection">
              <h1>UK Event Contact:</h1>

<p style="margin-left: 40px;"><strong>Professional Conference &amp; Project Management Ltd.</strong><br />
<a href="mailto:events-uk@nngroup.com">events-uk@nngroup.com</a>,&nbsp;+44 (0) 1403 785697<br />
34 Luxford Way, Billingshurst, West Sussex, RH14 9PA, UK</p>

<p style="margin-left: 40px;"><em>Additional Company Information:&nbsp;Company Number: 4142899<br />
Registered office: Chart House, 2 Effingham Road, Seigate, Surrey RH2 7JN</em></p>

<p>&nbsp;</p>

<h1>Payment Policies &amp; Payment Methods Accepted</h1>

<p>You may pay your tuition and exam fees by<strong> bank transfer</strong>, <strong>cheque</strong>, or any of the following methods:</p>

<p><img alt="Visa credit and debit payments supported by Worldpay" height="40" src="http://media.nngroup.com/static/img/VISA.gif" width="64" /> <img alt="Mastercard payments supported by Worldpay" height="40" src="http://media.nngroup.com/static/img/mastercard.gif" width="64" /> <img alt="Visa Electron payments supported by Worldpay" height="40" src="http://media.nngroup.com/static/img/visa_electron.gif" width="64" /> <img alt="AMEX payments supported by Worldpay" height="40" src="http://media.nngroup.com/static/img/AMEX.gif" width="64" /> <img alt="Maestro" height="40" src="http://media.nngroup.com/static/img/maestro.gif" width="64" /> <img alt="JCB payments supported by Worldpay" height="40" src="http://media.nngroup.com/static/img/JCB.gif" width="64" /></p>

<p>Your registration is not complete until full payment is received. &nbsp;You will be sent a VAT invoice by email&nbsp;within one week&nbsp;of making your registration. &nbsp;The Nielsen Norman Group has contracted Professional Conference and Project Management, a conference-management firm, to handle all billing and customer service for this event.</p>

<p>&nbsp;</p>

<h1>What is Included?</h1>

<p>The prices shown above <strong>include only your tuition</strong> for this event.</p>

<p><strong>NOT Included</strong> in the prices shown above:</p>

<ul>
	<li>Your hotel costs (if any), which must be paid directly to your hotel.</li>
	<li>Exam administrative fees for the <a href="../../ux-certification/index.php">UX Certification</a> program. If you choose to participate in this program, you will need to pay an exam fee of &pound;60 GBP per course, which can be added to your tuition during online registration, or paid separately after you complete your training.&nbsp;</li>
</ul>

<p>&nbsp;</p>

<h1>Group Discounts:&nbsp;&nbsp;</h1>

<p>NN/g offers discounts for groups of people from the same company, registering at the same time for the same conference.</p>

<ul>
	<li>5% discount for company groups of&nbsp;5 to 9 attendees</li>
	<li>10% discount for company groups of 10 or more attendees</li>
</ul>

<p>&nbsp;</p>

<h1>Cancellation, Substitution, and Refund Policy</h1>

<p>If you must cancel after registering, your registration fee will be refunded less a 20% handling fee, provided that written notice of cancellation is received by &nbsp;03 March 2017. After that, the refund is 50%.&nbsp;No refunds will be granted after 10 March 2017, but we will accept a substitute attendee if we receive a written note from the registered attendee stating his or her intention to transfer registration to a named individual. Email&nbsp;events-uk@nngroup.com&nbsp;with cancellation, substitution, or refund requests.</p>

<p>&nbsp;</p>

<h1>Speaker Substitutions and Course Cancellations</h1>

<p>We reserve the right to substitute speakers, cancel seminars, or cancel the entire event. In such cases, our liability is limited to refunding the registration fees you have paid for the affected days. Speakers may need to be substituted in case of illness, accidents, airline problems, or other issues that prevent a scheduled speaker from presenting as planned. We will endeavor to get a substitute speaker, but this may not always be possible because of the expertise required and the short notice available in most such cases.</p>

            </div>
          </div>
        </section>
        
      </li>

      <li id="faqTab" class="content">
        
        <section class="l-subsection ontab collapsable-content">
          <h1 class="hide-for-medium-up collapse-link">
            <a class="unstyled" href="#faq">FAQ</a>
          </h1>

          <div class="collapsable">
            <h1 class="hide-for-small-down">Frequently Asked Questions</h1>
            <h2>What if I&rsquo;m having problems with the online registration process?</h2>

<p>If your problems are technical in nature (your browser is reporting errors or pages are failing to load correctly), please first try quitting and relaunching your browser, and/or restarting your computer. If it still doesn&rsquo;t work, please contact the event organiser:</p>

<p>Professional Conference and Project Management&nbsp;&nbsp;&nbsp;</p>

<p><a href="#">+44 (0) 1403 785697</a>&nbsp;<a href="mailto:events-uk@nngroup.com">&nbsp;</a><a href="mailto:usabilityweek-uk@nngroup.com">events-uk@nngroup.com</a></p>

<h2><br />
Do I have to choose my sessions when I register?</h2>

<p><strong>Yes.&nbsp;</strong>This not only speeds registration but also ensures you&#39;ll get a seat in the sessions you want.</p>

<p>&nbsp;</p>

<h2>Can I pay by cheque or bank transfer?</h2>

<p>Yes.&nbsp;Online registration will let you select these options in addition to paying by credit card. You will receive a printable invoice to return with your payment.&nbsp;</p>

<p>&nbsp;</p>

<h2>What discounts are available?</h2>

<p>There is a built-in discount for each additional day you attend. You can also take advantage of our early bird registration discount (as indicated in the pricing matrix on the event agenda page).</p>

<p><strong>Group Discount:&nbsp;</strong>&nbsp;NN/g offers discounts for groups of people from the same company, registering at the same time:</p>

<p>5% discount for company groups of&nbsp;5 to 9 attendees<br />
10% discount for company groups of 10 or more attendees</p>

<p>&nbsp;</p>

<h2>Is it safe for me to register using my credit card on the NN/g conference site?</h2>

<p><strong>Absolutely.&nbsp;</strong>Our website uses Secure Sockets Layer (SSL) technology to encrypt your personal information when you place your order. This means that your order cannot be read as it travels over the Internet, so your name, address, and credit card information are secure.</p>

<p>&nbsp;</p>

<h2>What is the dress code for this conference?</h2>

<p>Dress comfortably.&nbsp;Business casual and jeans are welcomed. Some people find conference rooms a little cool when sitting all day, so please wear layers and bring a warm jacket. We&rsquo;ll do our best to keep the temperature comfortable for you, but hotel functions rooms can be unpredictable.</p>

<p>&nbsp;</p>

<h2>Do I need to bring a laptop?</h2>

<p>Yes.&nbsp;Please bring your laptop with power cord to the conference. Seminar slides and class materials will be provided in electronic format. If you would like to follow along with the presenter&#39;s projected slides throughout the conference, a laptop is highly recommended.</p>

<p>&nbsp;</p>

<h2>Can I get a list of who&rsquo;s attending the conference?</h2>

<p><strong>Yes.</strong>&nbsp;A list of attendees will be included in the conference proceedings. If you do not wish to be listed, we provide an opt-out within the registration form.</p>

<p>&nbsp;</p>

<h2>Will you sell my personal information? May I buy a list of your conference attendees?</h2>

<p><strong>No.</strong>&nbsp;We never share personal information with other companies.</p>

<p>&nbsp;</p>

<h2>Do you accommodate special needs?</h2>

<p>Each event location is set up to accommodate attendees with special needs; please email the event organiser for assistance:</p>

<p><strong>Professional Conference and Project Management &nbsp;</strong></p>

<p><a href="#">+44 (0)&nbsp;1403 785697&nbsp;</a>&nbsp;&nbsp;<a href="mailto:events-uk@nngroup.com">events-uk@nngroup.com</a></p>

          </div>
        </section>
        
      </li>

      <li id="experienceTab" class="content">
        
        

<section class="l-subsection ontab collapsable-content">
  <h1 class="show-for-small-down collapse-link">The Experience</h1>
  <section class="collapsable">
    <h1>The UX Conference Experience</h1>
    <div class="row">
      <section class="small-12 columns">
      <p><strong>An intensive week of education, learning from both expert instructors and industry peers.</strong></p>

      </section>
    </div>

    
    <div class="row">
      <section class="small-12 columns experience-image-grid">
      <ul class="small-block-grid-1 medium-block-grid-3">
        
          
            
              
                <li><img src="https://media.nngroup.com/media/files/images/IMG_4714_copy_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale.jpg" alt=""></li>
              
            
          
        
          
            
              
                <li><img src="https://media.nngroup.com/media/files/images/IMG_9853_copy-2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale.jpg" alt=""></li>
              
            
          
        
          
            
              
                <li><img src="https://media.nngroup.com/media/files/images/IMG_4689_copy_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale.jpg" alt=""></li>
              
            
          
        
          
            
              
                <li><img src="https://media.nngroup.com/media/files/images/NN2048_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale.jpg" alt=""></li>
              
            
          
        
          
            
              
                <li><img src="https://media.nngroup.com/media/files/images/NN2021_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale.jpg" alt=""></li>
              
            
          
        
          
            
              
                <li><img src="https://media.nngroup.com/media/files/images/IMG_8642_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale.jpg" alt=""></li>
              
            
          
        
      </ul>
      </section>
    </div>
    

    <div class="row">
      <section class="small-12 columns">
        <div style="text-align: center;">View more <a href="../gallery/index.php">photos from recent UX Conferences</a> to get a further feel for the mood of the event</div>

<h2>&nbsp;</h2>

<h2>Immersive learning in full-day courses</h2>

<p>100% of our courses run for at least one full day (some are multi-day courses), which allow attendees to embrace a topic, understand the foundation, learn methods, apply these methods in class exercises and discuss with instructors and classmates to absorb the subject matter.</p>

<h2>Decades of experience distilled into one week of courses</h2>

<p>Nielsen Norman Group principals and instructors have published the most cited literature in the field of user experience including Jakob Nielsen&#39;s&nbsp;<em>Designing Web Usability</em>, &nbsp;Don Norman&#39;s&nbsp;<em>The Design of Everyday Things</em>, and Bruce Tognazzini&#39;s&nbsp;<em>Tog on Software Design</em>. Our instructors are seasoned UX professionals that base their courses on the real demands of the jobs our attendees need to do. Our materials and exercises are rooted in the reality we have experienced throughout our careers.</p>

      </section>
    </div>
    <div class="row">
      <section class="small-12 medium-6 columns">
        <h2>What attendees have to say about the&nbsp;UX Conference</h2>

<p><em>&ldquo;At the NN/g seminar, it is like letting a breath of fresh air into your head. My conference notes look like an action plan for what to do when I get back to my desk.&rdquo; </em><br />
Ian Anderson, <strong> European Commission, Brussels </strong></p>

<p><em>&ldquo;One intensive and comprehensive day saved me weeks and months of reading textbooks.&rdquo; </em><br />
Wendy Moncur,&nbsp;<strong>Dept. of Sustainability and Environment, Melbourne, Australia </strong></p>

<p><em>&quot;Superbly presented...can be directly applied to real problems. Easily the best seminar I have ever attended.&quot; </em><br />
Craig Wolfe,&nbsp;<strong>May Department Stores Co. </strong></p>

<p><em>&quot;This session [Interaction Design] provided an excellent overview of interaction design, and learning the principles of usability from Tog felt like learning about gravity from Newton!&quot; </em><br />
Brian Patrick Snyder,&nbsp;<strong>Whirlpool Corporation </strong></p>

<p><em>&ldquo;This has been a great opportunity to meet and network with other Web professionals. The brainstorming I observed and participated in has been very enlightening and worthwhile.&rdquo; </em><br />
Keisha M. Thomas,&nbsp;<strong>University of Chicago Press </strong></p>

<p><a href="../../training-companies-list/index.php">Companies who attended the UX Conference</a> in 2014-2015</p>

      </section>
      <section class="small-12 medium-6 columns">
        <p><iframe allowfullscreen="" frameborder="0" height="360" src="https://www.youtube.com/embed/iMQWazOlG84?rel=0" width="100%"></iframe></p>

      </section>
    </div>
  </section>
</section>

        
      </li>
    </ul>
  </div>
</div>

<div class="row">
  <div class="l-subsection small-12 column register-pricing-links">
    
    <a href="../register/87/index.php" class="button register-button">Register for the UX Conference London</a>
    
  </div>
  <div class="l-subsection small-12 column hot-line">
    <p>Questions:&nbsp;&nbsp;&nbsp;<a href="mailto:events-uk@nngroup.com">events-uk@nngroup.com</a>&nbsp;&nbsp;&nbsp;+44 (0) 1403 785697</p>
  </div>
</div>

<!-- TODO: end -->
<script type="text/javascript">
  $(document).ready(function() {
    $('.go-to-faq').on('click', function () {
      $('#faq-tab').trigger('click');
      window.location = '#breaks';
    });

    var location = window.location;
    var isMobile = window.innerWidth <= 640;

    if (location.hash && isMobile) {
      var selector = location.hash + 'Tab' + ' .collapse-link';

      setTimeout(function() {
        $(selector).trigger('click');
      }, 10);
    }
  });

</script>

        
      </div>
    </div>
  </article>
  
    <script src="https://media.nngroup.com/static/vendor/foundation/legacy/js/jquery.foundation.tabs.js"></script>
    <script>
        $(document).ready(function() {
            $(document).foundationTabs();

            if($(window).width() < 641) {
                $('#tab-content-area').removeClass('tabs-content');
            } else {
                $('#tab-content-area').addClass('tabs-content');
            }
        });
    </script>
  

</div>

    </div>
    




<footer id="page-footer">
    <div class="row hide-for-print">
        <div class="column small-12 medium-6">
            <section class="footer-popular">
                
                <h1>Popular Topics</h1>
                <div style="float: left;  width: 15em; display: block;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/applications/index.php">Application Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/e-commerce/index.php">E-Commerce Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/eyetracking/index.php">Eyetracking </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/information-architecture/index.php">Information Architecture </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/intranets/index.php">Intranet Design </a></li>
	<li><a href="../../topic/interaction-design/index.php">Interaction Design </a></li>
</ul>
</div>

<div style="float: left;  width: 15em; display: block; list-style: none;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/mobile-and-tablet-design/index.php">Mobile and Tablet Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/credibility-persuasion/index.php">Persuasive Design</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/research-methods/index.php">Research Methods</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/user-testing/index.php">User Testing </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/web-usability/index.php">Web Usability </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/writing-web/index.php">Web Writing &amp; Content Strategy </a></li>
</ul>
</div>

<p>&nbsp;</p>

                
            </section>
        </div>

        <div class="column small-12 medium-6">
            <div class="row">
                <div class="column small-6">
                    <section class="footer-about">
                        <h1>Follow Us</h1>
                        <ul>
	<li><a href="../../articles/subscribe/index.php">Subscribe to our weekly UX newsletter</a>,&nbsp;Jakob Nielsen&#39;s Alertbox</li>
	<li>Visit Don Norman&#39;s website, <a href="http://www.jnd.org/"> www.jnd.org </a></li>
	<li>Visit Bruce Tognazzini&#39;s website, <a href="http://www.asktog.com/"> www.asktog.com </a></li>
	<li style="padding-top: 6px;"><a class="twitter-follow-button" data-show-count="false" data-size="large" href="https://twitter.com/nngroup"><img alt="Follow @nngroup on Twitter" height="20" src="https://media.nngroup.com/media/editor/2016/12/30/twitter-follow-button.png" style="width: 117px; height: 20px;" width="117" /></a></li>
	<li style="padding-top: 6px;"><img alt="" height="14" src="https://media.nngroup.com/media/editor/2016/07/26/feed-icon-14x14.png" style="vertical-align:text-top;width:14px;margin-right:.25em;" width="14" /><a href="../../feed/rss/index.php">Subscribe to our RSS feed</a></li>
</ul>

                    </section>
                </div>

                <div class="column small-6">
                    <section class="footer-about right-column">
                        <h1>About
</h1>
                        <ul class="no-bullet">
	<li><a href="../../about/contact/index.php">Contact information</a></li>
	<li><a href="../../about/index.php">About Us</a></li>
	<li><a href="../../about/why-nng/index.php">Why NN/g? </a></li>
	<li><a href="../../return-policy/index.php">Return Policy</a></li>
</ul>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="copyright column small-12">
            <p><a href="../../copyright-and-reprint-info/index.php">Copyright</a> &copy; 1998-2017 Nielsen Norman Group, All Rights Reserved.</p>
        </div>
    </div>
</footer>


    


        <script src="https://media.nngroup.com/static/vendor/fastclick/js/fastclick.min.js"></script>
        <script src="https://media.nngroup.com/static/vendor/foundation/js/foundation.min.js"></script>
        <script src="http://media.nngroup.com/static/js/app.js"></script>
        <script src="http://media.nngroup.com/static/js/scroll-to-fixed.js"></script><script>var externalLinks = document.querySelectorAll('a[href^="http"]');for (var i = externalLinks.length-1; i >= 0; i--) {externalLinks[i].addEventListener("click", function(event) { alert("External links are disabled for this test."); event.preventDefault(); }, false);}var mailLinks = document.querySelectorAll('a[href^="mailto"]');for (var i = mailLinks.length-1; i >= 0; i--) {mailLinks[i].addEventListener("click", function(event) { alert("Email links are disabled for this test."); event.preventDefault(); }, false);}var externalForms = document.querySelectorAll('form');for (var i = externalForms.length-1; i >= 0; i--) {externalForms[i].addEventListener("submit", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false); externalForms[i].addEventListener("click", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false);}</script>

        <script>
            
            $(function() {
                $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
                $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
            });
        </script>

        
    <script src="https://media.nngroup.com/static/vendor/foundation/legacy/js/jquery.foundation.tabs.js"></script>
    <script>
        $(document).ready(function() {
            $(document).foundationTabs();

            if($(window).width() < 641) {
                $('#tab-content-area').removeClass('tabs-content');
            } else {
                $('#tab-content-area').addClass('tabs-content');
            }
        });
    </script>
  

        
        <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1072699960;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072699960/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
    </body>

<!-- Mirrored from www.nngroup.com/training/london/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 14:54:46 GMT -->
</html>
