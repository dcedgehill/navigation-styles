<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->





    
<!-- Mirrored from www.nngroup.com/training/course/1439/measuring-ux/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:16:01 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1026.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"beacon":"bam.nr-data.net","queueTime":8,"applicationTime":1118,"agent":"","licenseKey":"1a30ef9931","errorBeacon":"bam.nr-data.net","applicationID":"2048992","transactionName":"NFxSMRBUX0EDUkIPDA0WdhAMVkVbDV8ZAxUGV0QWTENYVxVCDCMVBldEJg1AQ0EHdVMSAgpVHgIHQQ=="}</script>
        <title>Measuring User Experience (UX) | Full Day Training Course by NN/g</title><meta property="og:title" content="Measuring User Experience (UX) | Full Day Training Course by NN/g" />
  
        
        <meta name="description" content="Full day course taught at Nielsen Norman Group&#39;s UX Conferences. Learn what you can measure, when you need to measure, and how to get metrics without breaking the budget.">
        <meta property="og:description" content="Full day course taught at Nielsen Norman Group&#39;s UX Conferences. Learn what you can measure, when you need to measure, and how to get metrics without breaking the budget." />
        
  
        
	
        

        

        <meta name="robots" content="noarchive">
        <meta name="viewport" content="width=device-width">

        <link rel="search" type="application/opensearchdescription+xml" title="NN/g Search" href="../../../../opensearch.xml" />
        <link rel="shortcut icon" href="http://media.nngroup.com/static/img/favicon.ico" />

        <link rel="stylesheet" href="https://media.nngroup.com/static/vendor/font-awesome/css/font-awesome.min.css">

        
        <link rel="stylesheet" href="https://media.nngroup.com/static/css/7232a960f1df.css" type="text/css" />
        <link rel="stylesheet" href="http://fast.fonts.com/t/1.css?apiType=css&amp;projectid=c5846260-9def-46ec-9407-0f1349bd2d64" type="text/css">

        <script src="https://media.nngroup.com/static/vendor/modernizr/js/modernizr.min.js"></script>

        <script type="text/javascript" src="https://media.nngroup.com/static/js/3283be5def74.js"></script>
        

        
    <link rel="canonical" href="../../../../courses/measuring-ux/index.php" />


        
        
        <!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=229034,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('http://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->


<!-- Google Analytics Tracking Code for http://nngroup.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23132681-1', 'nngroup.com');
  ga('require', 'linkid', 'https://www.nngroup.com/training/course/1439/measuring-ux/linkid.js');
  ga('require', 'displayfeatures');

// Optimizely Universal Analytics Integration
 window.optimizely = window.optimizely || [];
 window.optimizely.push("activateUniversalAnalytics");

  ga('send', 'pageview');

</script>
<script>
/**
* Function to track a click on an outbound link in Google Analytics.
*/
function trackOutboundLink(label, url) {
    if(window.ga && ga.loaded) {
        ga('send', 'event', 'outbound', 'click', label, {'hitCallback': function(){document.location = url;}
        });
    } else {
        document.location = url;
    }
}
/**
* Function to track a click on a link in the article announcement area.
*/
$('#articleAnnouncement a').on('click', function(){
  ga('send', 'event', 'link', 'click', 'articleAnnouncement');
});
</script>

<script type="text/javascript">
 setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2421.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



<!-- Hotjar Tracking Code for http://nngroup.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:121605,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

        <script src="https://media.nngroup.com/static/js/analytics/track_outbound_link.js"></script>
        
        
        
    </head>
    <body class="location-training location-event-course-detail course-detail">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
    <header id="page-header">
        <div id="cart" class="shopping-cart mobile-cart show-for-small-down"></div>
        <div class="row">
            <div id="masthead" class="masthead small-12 medium-7 column">

                <!-- The button to toggles menu on small screen -->

                <h1><a href="../../../../index.php"><img src="http://media.nngroup.com/static/img/logo-wordmark.png" width="74" height="30"> Nielsen <span class="norman">Norman</span> Group</a></h1>
                <p class="tagline show-for-medium-up">Evidence-Based User Experience Research, Training, and Consulting</p>

                <!-- The button to toggles form search on small screen -->


                <div style="clear: right;width:  100%;height: 1px;"></div><?php define('ROOT_PATH',$_SERVER["DOCUMENT_ROOT"] . '/apps/navigation_styles/navigations/2'); ?><?php include(ROOT_PATH)."/navigation.php"; ?>

                <!-- Drop down menu on small screen -->
                <div id="hideNav" class="show-for-small-down">
                  
                  <ul class="menu-content">
                    <li><a href="../../../../index.php">Home</a></li>
                    <li class="has-submenu">
                      <a>Training</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../../index.php">Overview</a></li>
                        
<li id="nav-ux-conference"><a href="../../../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../../../online-seminars/index.php"><span>Online Seminars</span></a></li>

                      </ul>
                    </li>
                    <li class="has-submenu">
                      <a>Consulting</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
  <li><a href="../../../../consulting/index.php">Overview</a></li>
  
  <li><a href="../../../../consulting/benchmarking/index.php">Benchmarking</a></li>
  
  <li><a href="../../../../consulting/expert-design-review/index.php">Expert Design Review</a></li>
  
  <li><a href="../../../../consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
  
  <li><a href="../../../../consulting/usability-testing/index.php">Usability Testing </a></li>
  
  <li><a href="../../../../consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
  
  <li><a href="../../../../in-house-training/index.php">In-House Training</a></li>
</ul>

                    </li>

                    <li><a href="../../../../reports/index.php">Reports</a></li>
                    <li><a href="../../../../articles/index.php">Articles</a></li>
                    <li class="has-submenu"><a>About NN/G</a><i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../../../about/index.php">Overview</a></li>
                        <li><a href="../../../../people/index.php">People</a></li>
                        <li><a href="../../../../about/why-nng/index.php">Why NN/g?</a></li>
                        <li><a href="../../../../about/contact/index.php">Contact</a></li>
                        <li><a href="../../../../news/index.php">News</a></li>
                        <li><a href="../../../../about/history/index.php">History</a></li>
                        <li><a href="../../../../books/index.php">Books</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                </div>

                <hr class="header-divider show-for-small-only"/>


            </div>
       </div>

        <div id="hideNav">            <nav class="primary-nav small-12 column">
                <ul>
                    <li><a class="navitem" id="nav-home" href="../../../../index.php">Home</a></li>
                    <li><a class="navitem" id="nav-training" href="../../../index.php">Training</a></li>
                    <li><a class="navitem" id="nav-consulting" href="../../../../consulting/index.php">Consulting</a></li>
                    <li><a class="navitem" id="nav-reports" href="../../../../reports/index.php">Reports</a></li>
                    <li><a class="navitem" id="nav-articles" href="../../../../articles/index.php">Articles</a></li>
                    <li><a class="navitem" id="nav-about" href="../../../../about/index.php">About NN/G</a></li>
                </ul>
            </nav>
            <nav class="secondary-nav small-12 medium-12 large-12 columns">
                
  <ul class="inline-list">
  
<li id="nav-ux-conference"><a href="../../../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../../../online-seminars/index.php"><span>Online Seminars</span></a></li>

</ul>


            </nav>
        </div>
    </header>
    <div id="main" class="l-content" onclick="closeNav(event)"><span id="preventClose" style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776; Menu</span>
    

<article class="course-detail">
    <div class="row">
        <p class="small-12 column">
            
            Full day training course
            
            offered at <a href="../../../london/index.php">The UX Conference London</a>
        </p>
    </div>
    <div class="row">
        <div class="l-section-intro small-12 medium-8 column">
            <header>
                <h1>Measuring User Experience</h1>
                <h2>UX metrics and quantitative usability studies to measure the effectiveness and business value of your design</h2>
            </header>
        </div>

        <div class="small-12 column show-for-small-down l-subsection">
            
            <h2 class="course-dates">Course Date: November 09, 2016</h2>
            

            
            <div style="clear: both"></div>
            <hr style="border-color: #eee"/>
        </div>
    </div>


    
    <div class="row">
        <div class="l-course-content small-12 medium-8 column">
            <div class="l-usercontent mainCourseContent">
                <p>User-experience metrics give you objective, persuasive data on which to base your design recommendations.&nbsp; They can help you answer questions such as:</p>

<ul>
	<li>How usable is your site?</li>
	<li>Have you met your benchmarks?&nbsp;</li>
	<li>How do two designs compare? How does your site compare with a competitor&rsquo;s?</li>
	<li>What&rsquo;s the impact of usability on return on investment (ROI)</li>
</ul>

<p>In this course, you will get a set of tools and methods for planning, running, and analyzing your own quantitative user studies.&nbsp;</p>

<blockquote>&quot;Very good complement to qualitative theories, practical, accessible (within reason) and extremely relevant for UX practitioner&#39;s challenges.&quot;</blockquote>

<p>Guilhem Ganitou<br />
Poland</p>

            </div>
            
            <div class="l-usercontent mainCourseContent collapsable-content">

                <h1 class="collapse-link show-for-small-down">Topics Covered</h1>
                <h2 class="show-for-medium-up">Topics Covered</h2>

                <div class="collapsable">
                    <ul>
	<li>Qualitative vs. quantitative studies</li>
	<li>Formative vs. summative studies</li>
	<li>Why you may need metrics&nbsp;</li>
	<li>Usability and ROI&nbsp;
	<ul>
		<li>Calculating ROI from usability-related changes</li>
	</ul>
	</li>
	<li>Planning a quantitative study
	<ul>
		<li>Independent and dependent variables</li>
		<li>Evaluator&#39;s effect</li>
		<li>Measurement errors</li>
		<li>Performance metrics in quantitative studies
		<ul>
			<li>Success rates</li>
			<li>Time on task</li>
		</ul>
		</li>
		<li>Satisfaction scores
		<ul>
			<li>Post-task scores</li>
			<li>Post-test scores</li>
		</ul>
		</li>
		<li>User experience questionnaires
		<ul>
			<li>System Usability Scale (SUS)</li>
			<li>Net-Promoter Score (NPS)</li>
		</ul>
		</li>
		<li>Relation between subjective satisfaction and performance metrics</li>
		<li>Avoiding bias and writing good tasks</li>
		<li>Recording the dependent variables accurately</li>
		<li>Finding top tasks for testing
		<ul>
			<li>Quality function deployment (QFD) method&nbsp;</li>
		</ul>
		</li>
		<li>Remote versus in-person usability testing</li>
	</ul>
	</li>
	<li>Analyzing data from a quantitative study
	<ul>
		<li>Means, medians, geometric means: when to use each</li>
		<li>Continuous vs. binary measures</li>
		<li>Confidence intervals</li>
		<li>Statistical differences between designs
		<ul>
			<li>T-tests</li>
			<li>Within-subjects and between-subjects comparison</li>
		</ul>
		</li>
		<li>Comparing with benchmarks</li>
	</ul>
	</li>
	<li>Determining the number of participants: which decisions can you make based on small samples, and which require a large sample?
	<ul>
		<li>Number of participants needed for formative studies</li>
		<li>Number of participants needed for summative studies</li>
	</ul>
	</li>
</ul>

                </div>

            </div>
            

            
            <div class="l-usercontent mainCourseContent collapsable-content">

                <h1 class="collapse-link show-for-small-down">Format</h1>
                <h2 class="show-for-medium-up">Format</h2>

                <div class="collapsable">
                    <p>The basis of the course is a lecture format with some group exercises to reinforce the learned principles and guidelines.<br />
The course also includes:</p>

<ul>
	<li>Findings from our own studies</li>
	<li>Excel and text spreadsheets for all examples used in class</li>
	<li>Opportunities to ask questions and get answers</li>
</ul>

                </div>

            </div>
            

            
            <div class="l-usercontent mainCourseContent collapsable-content">

                <h1 class="collapse-link show-for-small-down">Bring Your Laptop</h1>
                <h2 class="show-for-medium-up">Bring Your Laptop</h2>

                <div class="collapsable">
                    <p>These tools will benefit you during class, but are not mandatory:</p>

<ul>
	<li>a laptop</li>
	<li>Microsoft Excel&nbsp;so that you can access the online tools and spreadsheets that we&#39;ll be using in class</li>
</ul>

                </div>

            </div>
            

            
            <div class="l-usercontent mainCourseContent collapsable-content">

                <h1 class="collapse-link show-for-small-down">Related Course</h1>
                <h2 class="show-for-medium-up">Related Course</h2>

                <div class="collapsable">
                    <p>NN/g offers two courses that focus on quantitative UX methods, but these courses address two different types of data collection:</p>

<ul>
	<li><em>Measuring&nbsp;</em><em>UX</em>, the course described on this page, focuses on&nbsp;<strong>designing and analyzing quantitative usability studies</strong>, where a sample of representative users perform assigned tasks. The course provides you with tools to understand how much you can trust results obtained from any given quantitative study. While some of those tools can be applied to analyzing analytics data,&nbsp;<em>Measuring UX</em>&nbsp;presents them in the context of controlled usability studies.</li>
	<li><a href="../../../../courses/analytics-and-user-experience/index.php"><em>Analytics and User Experience</em></a> is a different course which covers the use of data that is passively collected by&nbsp;<strong>recording all user actions on a live website or application</strong>. Analyzing trends and outliers in this large body of data can identify UX opportunities and evaluate the success of UX designs.</li>
</ul>

<p>Both of these methods can be applied to a variety of different websites and applications.&nbsp;<strong>Analytics </strong>data comes from a larger group of users in uncontrolled, but real-life situations.&nbsp;<strong>Quantitative usability studies</strong>&nbsp;allow you to put a number on your site&rsquo;s usability and gather more in-depth and specific information about which tasks&nbsp;are easy or difficult to perform on your site. (If you really want to be thorough, use both methods &mdash; taking both courses to prepare &mdash; and triangulate the findings.)</p>

                </div>

            </div>
            

            

            

            

            
                



<div class="row certification-info">
  <section class="l-subsection collapsable-content small-12 medium-8 columns">
    <h1 class="collapse-link show-for-small-down">UX Certification Credit</h1>
    <h2 class="hide-for-small-down">UX Certification Credit</h2>
    <div class="collapsable">
      <p>
      Attending this course and passing the exam earns <strong>1 UX Certification credit</strong>, which also counts towards the optional <a href="../../../../ux-certification/ux-research-specialty/index.php">UX Research Specialty</a>.
      </p>

      

      <p>
      Learn more about NN/g's <a href="../../../../ux-certification/index.php">UX Certification</a> Program.
      </p>
    </div>
  </section>
  <div class="hide-for-small-down medium-4 columns badge">
    <img alt="UX Certification Badge from Nielsen Norman Group" src="https://media.nngroup.com/nng-uxc-badge.png">
  </div>
</div>

            

            
<div class="l-usercontent mainCourseContent collapsable-content participant-comments">
  <h1 class="collapse-link show-for-small-down">Participant Comments</h1>
  <h2 class="show-for-medium-up">Participant Comments</h2>
  <div class="collapse-content collapsable">
    <blockquote>&quot;Very nice course. A lot of info to help you start measuring yourself or understanding incoming data. It has a bit of maths, but nothing too intimidating.&quot;</blockquote>

<p>Marina Yaricheva, Clarity International</p>

<blockquote>&quot;Finally, ROI of UX explained. Congrats!&quot;</blockquote>

<p>Andrea Cauti</p>

<blockquote>&quot;Her experience and knowledge are very evident. I feel as though I have been gaining experience from the masters of the field.&quot;</blockquote>

<p>Lejjy Lefour, University of Alberta</p>

<blockquote>&quot;The exercises in this class were fantastic. Learned new skills and am walking away with an action plan. What made these exercises great was the length of them and amount of detail provided.&quot;</blockquote>

<p>Kortnie, UX Reseacher, CA</p>

    <ul id="commentAccordian" class="accordion" data-accordion>
      <li class="accordion-navigation">
        <a href="#cc" class="closed" id="commentCollapseLink">More Participant Comments</a>
        <div id="cc" class="content more-comments">
          <blockquote>&quot;Really liked the hands on balance. She covered a lot really well. Very practical overview. Better than my whole semester of stats.&quot;</blockquote>

<p>&nbsp;</p>

<blockquote>&quot;Probably the most important presentation for a UX researcher trying to improve their career. A consistent set of content and tools to [overcome] opinions and ambiguity and let evidence and numbers win controversies.&quot;</blockquote>

<p>Sotiris Sotiropoulos, Zanshin Labs</p>

<blockquote>&quot;Wow! Mind blown! Who knew stats could be fun! The delivery was great and the course content was interesting.&quot;</blockquote>

<p>Chris Robinson, Auto Trader UK</p>

<blockquote>&quot;Really loved this session. It helped me move from a STRONG qualitative preference to a qualitative PLUS quantitative preference. Basically, I just didn&#39;t know how to do quantitative.&quot;</blockquote>

<p>Reed Jones, SubHub</p>

<blockquote>&quot;Did a great job making a difficult topic easy to understand. Bravo!&quot;</blockquote>

<p>Brendan W, Musc</p>

<blockquote>&quot;Probably the most important presentation for a UX researcher trying to improve their career.&nbsp; A consistent set of content and tools to glean opinions and ambiguity and let evidence and numbers win controversies.&quot;</blockquote>

<p>Sotiris Sotiropoulos, Zanshin Labs, UK/Greece</p>

<blockquote>&quot;Very practical. Really liked the aspect of drilling down to specific methods and equations.&quot;</blockquote>

<p>Carly Chiao, Auto Gravity</p>

<blockquote>&quot;Wow! Mind blown! Who knew stats could be fun! The delivery was great and the course content was interesting.&quot;</blockquote>

<p>Chris Robinson, Auto Trader UK</p>

<blockquote>&quot;Very good overview of measurement methodologies and tools. When to use one over the other and key decision points on where to use and what insightful data you can derive off it.&quot;</blockquote>

<p>Robbie Rivera, Cisco Systems</p>

<blockquote>&quot;This is a really thorough introduction to conducting accurate, meaningful user experience studies that yield solid, significant empirical results &mdash; I&#39;m delighted by the rigor of the instruction and I&#39;m pleased the curriculum encompasses basic statistical concepts in a very approachable way, that will significantly enhance the credibility of results.&quot;</blockquote>

<p>Adam Turnbull, Rail Europe</p>

<blockquote>&quot;Did a great job making a difficult topic easy to understand. Bravo!&quot;</blockquote>

<p>Brendan W, Musc</p>

<blockquote>&quot;Thoroughly enjoyed the measuring UX course. Statistics has never been presented so nicely in an unscary way.&quot;</blockquote>

<p>Barking.com, The Netherlands</p>

<blockquote>&quot;I was impressed with her ability to field a wide variety of questions and still connect her responses elegantly to points already covered in the course.&quot;</blockquote>

<p>Tore Hogas, University Hospital of North Norway</p>

<blockquote>&quot;Kate Meyer is a great teacher, I really feel like I am gaining the knowledge I need to successfully conduct a quantity UX test.&quot;</blockquote>

<p>Abby Watson, Viacom</p>

<blockquote>&quot;She made a really tough topic consumable and understandable.&quot;</blockquote>

<p>Eshani K, Spinach Studios, India</p>

<blockquote>&quot;This is a very difficult topic to cover, especially in a day, but Kate masterfully simplified the process and fielded a variety of complex questions effortlessly. Top job!&quot;</blockquote>

<p>Simon Adams, Capgemini</p>

<blockquote>&quot;Great class! Very organized with specific tools to measure user experience.&quot;</blockquote>

<p>Michaela Mora, Relevant Insights, LLC</p>

<blockquote>&quot;Awesome presenter!&quot;</blockquote>

<p>Jesse Winner, Samtec</p>

<blockquote>&quot;Anyone that may want to or is looking into redesigning a site/system/tool, and wants to learn how to measure the success of your efforts &mdash; come to this course! Raluca is amazing.&quot;</blockquote>

<p>Lyndsey Lindsay, PLTW</p>

<blockquote>&quot;Very, very relevant and practical info. Super well delivered by instructor.&quot;</blockquote>

<p>Ritika Mathur, Fiserv</p>

<blockquote>&quot;This class was excellent. Review of confidence intervals brought true understanding to what this &quot;street-trained&quot; researcher found herself using all too often.&quot;</blockquote>

<p>Bev Inzarry, Velvet Hammer</p>

<blockquote>&quot;I thought that some of the math and calculators would lose me, but it all tied in and became clear! Very useful information.&quot;</blockquote>

<p>&nbsp;</p>

<blockquote>&quot;I have a lot of requests to take back to our market research team. I&#39;m excited to get data from them to help with priorities of new features. The QFD matrix will be applicable in many different ways.&quot;</blockquote>

<p>Brandie Moore, American Fidelity</p>

<blockquote>&quot;You will definitely win an argument if you make a point based on correctly collected data and insightful analysis. This course will tell you how.&quot;</blockquote>

<p>Marco Yue Yin, GuruJam, Beijing</p>

<blockquote>&quot;Really fantastic speaker. Only complaint was a little data-heavy, but overall amazing.&quot;</blockquote>

<p>Robert Larson, StreetEasy, New York</p>

<blockquote>&quot;Love it! Including the excel formulas and calculator links let me focus on the methodology and meaning. Immensely helpful.&quot;</blockquote>

<p>Jennifer Church, Pitney Bowes Inc.</p>

<blockquote>&quot;I wish my college statistics professor would have explained some of these theories this clearly.&quot;</blockquote>

<p>&nbsp;</p>

<blockquote>&quot;Kate is wicked smart. Some of the formulas were difficult to grasp but she was patient and explained well.&quot;</blockquote>

<p>&nbsp;</p>

<blockquote>&quot;It&#39;s a hard topic, lots of math and stats, but Kate did an amazing job explaining the complicated issues. Really knowledgeable of the materials.&quot;</blockquote>

<p>&nbsp;</p>

<blockquote>&quot;I had to keep paying attention to keep up with Kate. Luckily she is an easy speaker, and the way she responds to questions is nice (and thorough). Course topic isn&#39;t really vivid, but delivered well.&quot;</blockquote>

<p>Tom, Ordina, Netherlands</p>

<blockquote>&quot;Great course. It will help me focus on &quot;outcomes&quot; rather than &quot;output&quot;.&quot;</blockquote>

<p>Hugh O&#39;Carroll, Blue Cross Blue Shield</p>

<blockquote>&quot;I am sure I am an outlier as a non-UX professional: very useful for defending research and the direction the company should take. Knowledge can be applied to many other areas and workflows. Great course for product managers or UX Team managers.&quot;</blockquote>

<p>Jonathan Skypek, Brightree</p>

<blockquote>&quot;Definitely more math than expected, but Kate explained it very eloquently. Good resources.&quot;</blockquote>

<p>&nbsp;</p>

<blockquote>&quot;This is a very difficult topic to cover, especially in a day but Kate masterfully simplified the process and fielded a variety of complex questions effortlessly. Top job!&quot;</blockquote>

<p>Simon Adams, Capgemini</p>

<blockquote>&quot;Good course, heavy material but I would keep it at this level. Very helpful for putting together all the data, especially to show management. Stat coverage is needed, examples are helpful.&quot;</blockquote>

<p>&nbsp;</p>

<blockquote>&quot;She made a really tough topic consumable and understandable.&quot;</blockquote>

<p>Eshani K, Spinach Studios, India</p>

<blockquote>&quot;Very challenging content and lots of really useful information that will support several projects that I have to accomplish. Excellent job making the content relatable and digestible for novice stats people.&quot;</blockquote>

<p>&nbsp;</p>

<blockquote>&quot;Really good! Loved the regular break outs and practical examples.&quot;</blockquote>

<p>Dave Deans, Waters, UK</p>

<blockquote>&quot;I had to take stats 2x in college and still didn&#39;t understand &mdash; now I do!!&quot;</blockquote>

<p>&nbsp;</p>

        </div>
      </li>
    </ul>
  </div>
</div>
<script>
  $('#commentAccordian').on('toggled', function (event, accordion) {
    $link = $('#commentCollapseLink');
    $link.toggleClass('closed');
    if($link.hasClass('closed')) {
      $link.text('More Participant Comments');
    } else {
      $link.text('Hide Participant Comments');
    }
  });
</script>



            <div class="l-usercontent"></div>

            
            
            <section class="course-instructors l-subsection collapsable-content">
                <h1 class="show-for-medium-up">Instructor</h1>
                <h1 class="show-for-small-down collapse-link">Instructor</h1>
                <div class="collapsable">
                    
                    <article class="course-instructor collapsable-content">
                        <h2><a href="../../../../people/kate-meyer/index.php">Kate Meyer</a></h2>
                        <div class="row">
                            <div class="medium-3 column hide-for-small-only">
                                <a href="../../../../people/kate-meyer/index.php"><img src="https://media.nngroup.com/media/people/photos/IMG_2009_copy-retouched-square-closer-800px.jpg.200x200_q95_autocrop_crop_upscale.jpg"></a>
                            </div>
                            <div class="small-12 medium-9 column">
                                <a href="../../../../people/kate-meyer/index.php"><img class="show-for-small-only" src="https://media.nngroup.com/media/people/photos/IMG_2009_copy-retouched-square-closer-800px.jpg.200x200_q95_autocrop_crop_upscale.jpg"></a>
                                <div class="usercontent"><p>Kate Meyer is a User Experience Specialist with Nielsen Norman Group. She plans and executes user research to help guide strategy and implementation for websites and applications. She also leads UX training courses and conducts independent research for NN/g. Her research findings and recommendations are informed by her background in information theory and design, as well as her development experience. Read more about <a href="../../../../people/kate-meyer/index.php">Kate</a>.</p>
</div>
                            </div>
                        </div>
                    </article>
                    
                </div>
            </section>
            <div class="show-for-small-down">
                



<div class="collapsable-content">
  <section class="l-subsection related-images">
    <h2 class="hide-for-small-down">Scenes From Past Trainings</h2>
    <h1 class="show-for-small-down collapse-link">Scenes From Past Trainings</h1>
    <div class="collapsable">
    <p>At the UX Conference, you get a full day of in-depth training with expert instructors. Learn more about <a href="../../../why-attend/index.php">why you should attend</a>.</p>

    
      
        
          
            <img srcset="https://media.nngroup.com/media/files/images/IMG_4201_copy_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale@2x.jpg 2x" src="https://media.nngroup.com/media/files/images/IMG_4201_copy_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale.jpg" alt="">
          
        
      
    
      
        
          
            <img srcset="https://media.nngroup.com/media/files/images/IMG_7582_copy_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale@2x.jpg 2x" src="https://media.nngroup.com/media/files/images/IMG_7582_copy_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale.jpg" alt="">
          
        
      
    
      
        
          
            <img srcset="https://media.nngroup.com/media/files/images/IMG_0424_copy-2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale@2x.jpg 2x" src="https://media.nngroup.com/media/files/images/IMG_0424_copy-2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale.jpg" alt="">
          
        
      
    
      
        
          
            <img srcset="https://media.nngroup.com/media/files/images/IMG_4881_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale@2x.jpg 2x" src="https://media.nngroup.com/media/files/images/IMG_4881_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale.jpg" alt="">
          
        
      
    
    </div>
  </section>
</div>


            </div>
        </div>

        <div class="l-event-sidebar medium-4 small-12 column sidebar">
            <div class="show-for-medium-up">
                <div class="l-subsection">
                    
                    <h2>Course Date: November 09, 2016</h2>
                    
                </div>

                
            </div>

            


            <section class="l-subsection l-uw-sidebar collapsable-content">
                <h2 class="show-for-medium-up">The UX Conference London Courses</h2>
                <h1 class="show-for-small-down collapse-link">UX Conference London Courses</h1>

                <div class="collapsable">
                    
                    <ul class="no-bullet">
                        
                        <li><strong>Saturday, November 5</strong></li>
                        <ul>
                            
                            
                            
                            <li>
                                
                                <a href="../../1423/ux-basic-training/index.php">UX Basic Training</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1424/ux-vp-director/index.php">The UX VP/Director</a>
                                
                            </li>
                            
                            
                            
                        </ul>
                        
                        <li><strong>Sunday, November 6</strong></li>
                        <ul>
                            
                            
                            
                            <li>
                                
                                <a href="../../1425/interaction-design-3-day-course/index.php">Interaction Design: 3-Day Course</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1450/journey-mapping/index.php">Journey Mapping to Understand Customer Needs</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1427/usability-mobile-websites-apps/index.php">Mobile User Experience</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1428/information-architecture/index.php">Information Architecture</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1426/cross-functional-team/index.php">Working Effectively in Cross-Functional Teams</a>
                                
                            </li>
                            
                            
                            
                        </ul>
                        
                        <li><strong>Monday, November 7</strong></li>
                        <ul>
                            
                            
                            
                            <li>
                                
                                <a href="../../1433/website-design-lessons-social-psychology/index.php">Website Design Lessons from Social Psychology</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1425/interaction-design-3-day-course/index.php">Interaction Design: 3-Day Course</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1430/leading-ux-teams/index.php">Leading Highly-Effective UX Teams </a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1431/personas/index.php">Personas: Turn User Data Into User-Centered Design</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1432/scaling-responsive-design/index.php">Scaling User Interfaces</a>
                                
                            </li>
                            
                            
                            
                        </ul>
                        
                        <li><strong>Tuesday, November 8</strong></li>
                        <ul>
                            
                            
                            
                            <li>
                                
                                <a href="../../1435/stakeholder-ux-approval/index.php">Engaging Stakeholders to Build Buy-In</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1436/ideation/index.php">Effective Ideation Techniques for UX Design</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1437/vis-mob-1/index.php">Visual Design for Mobile and Tablet: Day 1</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1438/usability-testing/index.php">Usability Testing</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1425/interaction-design-3-day-course/index.php">Interaction Design: 3-Day Course</a>
                                
                            </li>
                            
                            
                            
                        </ul>
                        
                        <li><strong>Wednesday, November 9</strong></li>
                        <ul>
                            
                            
                            
                            <li>
                                
                                <a href="../../1452/generating-big-ideas/index.php">Generating Big Ideas with Design Thinking</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1440/managing-user-experience-strategy/index.php">Managing User Experience Strategy</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <strong>Measuring User Experience</strong>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1441/vis-mob-2/index.php">Visual Design for Mobile and Tablet: Day 2</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1442/content-strategy/index.php">Content Strategy</a>
                                
                            </li>
                            
                            
                            
                        </ul>
                        
                        <li><strong>Thursday, November 10</strong></li>
                        <ul>
                            
                            
                            
                            <li>
                                
                                <a href="../../1444/hci/index.php">User Interface Principles Every Designer Must Know</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1451/web-page-design/index.php">Web Page UX Design</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1443/analytics-and-user-experience/index.php">Analytics and User Experience</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1445/cross-channel-user-experience/index.php">Omnichannel Journeys and Customer Experience</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1453/facilitating-ux-workshops/index.php">Facilitating UX Workshops</a>
                                
                            </li>
                            
                            
                            
                        </ul>
                        
                        <li><strong>Friday, November 11</strong></li>
                        <ul>
                            
                            
                            
                            <li>
                                
                                <a href="../../1446/human-mind/index.php">The Human Mind and Usability</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1454/communicating-design/index.php">Communicating Design</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1449/designing-millennials/index.php">Designing for Millennials</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1448/wireframing-and-prototyping/index.php">Wireframing and Prototyping</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1447/ux-deliverables/index.php">UX Deliverables</a>
                                
                            </li>
                            
                            
                            
                        </ul>
                        
                    </ul>
                </div>
            </section>

            <div class="hide-for-small-down">
                



<div class="collapsable-content">
  <section class="l-subsection related-images">
    <h2 class="hide-for-small-down">Scenes From Past Trainings</h2>
    <h1 class="show-for-small-down collapse-link">Scenes From Past Trainings</h1>
    <div class="collapsable">
    <p>At the UX Conference, you get a full day of in-depth training with expert instructors. Learn more about <a href="../../../why-attend/index.php">why you should attend</a>.</p>

    
      
        
          
            <img srcset="https://media.nngroup.com/media/files/images/IMG_4201_copy_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale@2x.jpg 2x" src="https://media.nngroup.com/media/files/images/IMG_4201_copy_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale.jpg" alt="">
          
        
      
    
      
        
          
            <img srcset="https://media.nngroup.com/media/files/images/IMG_7582_copy_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale@2x.jpg 2x" src="https://media.nngroup.com/media/files/images/IMG_7582_copy_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale.jpg" alt="">
          
        
      
    
      
        
          
            <img srcset="https://media.nngroup.com/media/files/images/IMG_0424_copy-2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale@2x.jpg 2x" src="https://media.nngroup.com/media/files/images/IMG_0424_copy-2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale.jpg" alt="">
          
        
      
    
      
        
          
            <img srcset="https://media.nngroup.com/media/files/images/IMG_4881_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale@2x.jpg 2x" src="https://media.nngroup.com/media/files/images/IMG_4881_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale.jpg" alt="">
          
        
      
    
    </div>
  </section>
</div>


            </div>

            <p class="other-cities"><a href="../../../../courses/measuring-ux/index.php">Other cities</a> where Measuring User Experience is offered.</p>

        </div>
    </div>
</article>

    </div>
    




<footer id="page-footer">
    <div class="row hide-for-print">
        <div class="column small-12 medium-6">
            <section class="footer-popular">
                
                <h1>Popular Topics</h1>
                <div style="float: left;  width: 15em; display: block;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../../../topic/applications/index.php">Application Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../../../topic/e-commerce/index.php">E-Commerce Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../../../topic/eyetracking/index.php">Eyetracking </a></li>
	<li style="padding-bottom: .5em;"><a href="../../../../topic/human-computer-interaction/index.php">Human Computer Interaction </a></li>
	<li style="padding-bottom: .5em;"><a href="../../../../topic/information-architecture/index.php">Information Architecture </a></li>
	<li style="padding-bottom: .5em;"><a href="../../../../topic/intranets/index.php">Intranet Design </a></li>
	<li><a href="../../../../topic/interaction-design/index.php">Interaction Design </a></li>
</ul>
</div>

<div style="float: left;  width: 15em; display: block; list-style: none;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../../../topic/mobile-and-tablet-design/index.php">Mobile and Tablet Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../../../topic/credibility-persuasion/index.php">Persuasive Design</a></li>
	<li style="padding-bottom: .5em;"><a href="../../../../topic/research-methods/index.php">Research Methods</a></li>
	<li style="padding-bottom: .5em;"><a href="../../../../topic/user-testing/index.php">User Testing </a></li>
	<li style="padding-bottom: .5em;"><a href="../../../../topic/web-usability/index.php">Web Usability </a></li>
	<li style="padding-bottom: .5em;"><a href="../../../../topic/writing-web/index.php">Web Writing &amp; Content Strategy </a></li>
</ul>
</div>

<p>&nbsp;</p>

                
            </section>
        </div>

        <div class="column small-12 medium-6">
            <div class="row">
                <div class="column small-6">
                    <section class="footer-about">
                        <h1>Follow Us</h1>
                        <ul>
	<li><a href="../../../../articles/subscribe/index.php">Subscribe to our weekly UX newsletter</a>,&nbsp;Jakob Nielsen&#39;s Alertbox</li>
	<li>Visit Don Norman&#39;s website, <a href="http://www.jnd.org/"> www.jnd.org </a></li>
	<li>Visit Bruce Tognazzini&#39;s website, <a href="http://www.asktog.com/"> www.asktog.com </a></li>
	<li style="padding-top: 6px;"><a class="twitter-follow-button" data-show-count="false" data-size="large" href="https://twitter.com/nngroup"><img alt="Follow @nngroup on Twitter" height="20" src="https://media.nngroup.com/media/editor/2016/12/30/twitter-follow-button.png" style="width: 117px; height: 20px;" width="117" /></a></li>
	<li style="padding-top: 6px;"><img alt="" height="14" src="https://media.nngroup.com/media/editor/2016/07/26/feed-icon-14x14.png" style="vertical-align:text-top;width:14px;margin-right:.25em;" width="14" /><a href="../../../../feed/rss/index.php">Subscribe to our RSS feed</a></li>
</ul>

                    </section>
                </div>

                <div class="column small-6">
                    <section class="footer-about right-column">
                        <h1>About
</h1>
                        <ul class="no-bullet">
	<li><a href="../../../../about/contact/index.php">Contact information</a></li>
	<li><a href="../../../../about/index.php">About Us</a></li>
	<li><a href="../../../../about/why-nng/index.php">Why NN/g? </a></li>
	<li><a href="../../../../return-policy/index.php">Return Policy</a></li>
</ul>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="copyright column small-12">
            <p><a href="../../../../copyright-and-reprint-info/index.php">Copyright</a> &copy; 1998-2017 Nielsen Norman Group, All Rights Reserved.</p>
        </div>
    </div>
</footer>


    


        <script src="https://media.nngroup.com/static/vendor/fastclick/js/fastclick.min.js"></script>
        <script src="https://media.nngroup.com/static/vendor/foundation/js/foundation.min.js"></script>
        <script src="http://media.nngroup.com/static/js/app.js"></script>
        <script src="http://media.nngroup.com/static/js/scroll-to-fixed.js"></script><script>var externalLinks = document.querySelectorAll('a[href^="http"]');for (var i = externalLinks.length-1; i >= 0; i--) {externalLinks[i].addEventListener("click", function(event) { alert("External links are disabled for this test."); event.preventDefault(); }, false);}var mailLinks = document.querySelectorAll('a[href^="mailto"]');for (var i = mailLinks.length-1; i >= 0; i--) {mailLinks[i].addEventListener("click", function(event) { alert("Email links are disabled for this test."); event.preventDefault(); }, false);}var externalForms = document.querySelectorAll('form');for (var i = externalForms.length-1; i >= 0; i--) {externalForms[i].addEventListener("submit", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false); externalForms[i].addEventListener("click", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false);}</script>

        <script>
            
            $(function() {
                $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
                $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
            });
        </script>

        
    

    <script>
        $(function() {
            var toggle = $("#collapse-toggle");
            toggle.on("click", function() {
                $(".collapsable").toggleClass("collapsed");
                toggle.toggleClass("collapsed");
                return(false);
            })
        });
    </script>


        
        <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1072699960;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072699960/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
    </body>

<!-- Mirrored from www.nngroup.com/training/course/1439/measuring-ux/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:16:01 GMT -->
</html>
