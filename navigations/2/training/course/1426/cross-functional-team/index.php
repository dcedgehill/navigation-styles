<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->





    
<!-- Mirrored from www.nngroup.com/training/course/1426/cross-functional-team/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:16:00 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1026.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"licenseKey":"1a30ef9931","queueTime":3,"beacon":"bam.nr-data.net","errorBeacon":"bam.nr-data.net","applicationID":"2048992","transactionName":"NFxSMRBUX0EDUkIPDA0WdhAMVkVbDV8ZAxUGV0QWTENYVxVCDCMVBldEJg1AQ0EHdVMSAgpVHgIHQQ==","applicationTime":584,"agent":""}</script>
        <title>Working Effectively in Cross-Functional Teams | Full-Day UX Course</title><meta property="og:title" content="Working Effectively in Cross-Functional Teams | Full-Day UX Course" />
  
        
        <meta name="description" content="Full-day course at the Nielsen Norman Group UX Conference: Learn to achieve synergy, not infighting, when collaborating with colleagues from other UX disciplines and with non-UX team members.">
        <meta property="og:description" content="Full-day course at the Nielsen Norman Group UX Conference: Learn to achieve synergy, not infighting, when collaborating with colleagues from other UX disciplines and with non-UX team members." />
        
  
        
	
        

        

        <meta name="robots" content="noarchive">
        <meta name="viewport" content="width=device-width">

        <link rel="search" type="application/opensearchdescription+xml" title="NN/g Search" href="../../../../opensearch.xml" />
        <link rel="shortcut icon" href="http://media.nngroup.com/static/img/favicon.ico" />

        <link rel="stylesheet" href="https://media.nngroup.com/static/vendor/font-awesome/css/font-awesome.min.css">

        
        <link rel="stylesheet" href="https://media.nngroup.com/static/css/7232a960f1df.css" type="text/css" />
        <link rel="stylesheet" href="http://fast.fonts.com/t/1.css?apiType=css&amp;projectid=c5846260-9def-46ec-9407-0f1349bd2d64" type="text/css">

        <script src="https://media.nngroup.com/static/vendor/modernizr/js/modernizr.min.js"></script>

        <script type="text/javascript" src="https://media.nngroup.com/static/js/3283be5def74.js"></script>
        

        
    <link rel="canonical" href="../../../../courses/cross-functional-team/index.php" />


        
        
        <!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=229034,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('http://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->


<!-- Google Analytics Tracking Code for http://nngroup.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23132681-1', 'nngroup.com');
  ga('require', 'linkid', 'https://www.nngroup.com/training/course/1426/cross-functional-team/linkid.js');
  ga('require', 'displayfeatures');

// Optimizely Universal Analytics Integration
 window.optimizely = window.optimizely || [];
 window.optimizely.push("activateUniversalAnalytics");

  ga('send', 'pageview');

</script>
<script>
/**
* Function to track a click on an outbound link in Google Analytics.
*/
function trackOutboundLink(label, url) {
    if(window.ga && ga.loaded) {
        ga('send', 'event', 'outbound', 'click', label, {'hitCallback': function(){document.location = url;}
        });
    } else {
        document.location = url;
    }
}
/**
* Function to track a click on a link in the article announcement area.
*/
$('#articleAnnouncement a').on('click', function(){
  ga('send', 'event', 'link', 'click', 'articleAnnouncement');
});
</script>

<script type="text/javascript">
 setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2421.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



<!-- Hotjar Tracking Code for http://nngroup.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:121605,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

        <script src="https://media.nngroup.com/static/js/analytics/track_outbound_link.js"></script>
        
        
        
    </head>
    <body class="location-training location-event-course-detail course-detail">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
    <header id="page-header">
        <div id="cart" class="shopping-cart mobile-cart show-for-small-down"></div>
        <div class="row">
            <div id="masthead" class="masthead small-12 medium-7 column">

                <!-- The button to toggles menu on small screen -->

                <h1><a href="../../../../index.php"><img src="http://media.nngroup.com/static/img/logo-wordmark.png" width="74" height="30"> Nielsen <span class="norman">Norman</span> Group</a></h1>
                <p class="tagline show-for-medium-up">Evidence-Based User Experience Research, Training, and Consulting</p>

                <!-- The button to toggles form search on small screen -->


                <div style="clear: right;width:  100%;height: 1px;"></div><?php define('ROOT_PATH',$_SERVER["DOCUMENT_ROOT"] . '/apps/navigation_styles/navigations/2'); ?><?php include(ROOT_PATH)."/navigation.php"; ?>

                <!-- Drop down menu on small screen -->
                <div id="hideNav" class="show-for-small-down">
                  
                  <ul class="menu-content">
                    <li><a href="../../../../index.php">Home</a></li>
                    <li class="has-submenu">
                      <a>Training</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../../index.php">Overview</a></li>
                        
<li id="nav-ux-conference"><a href="../../../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../../../online-seminars/index.php"><span>Online Seminars</span></a></li>

                      </ul>
                    </li>
                    <li class="has-submenu">
                      <a>Consulting</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
  <li><a href="../../../../consulting/index.php">Overview</a></li>
  
  <li><a href="../../../../consulting/benchmarking/index.php">Benchmarking</a></li>
  
  <li><a href="../../../../consulting/expert-design-review/index.php">Expert Design Review</a></li>
  
  <li><a href="../../../../consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
  
  <li><a href="../../../../consulting/usability-testing/index.php">Usability Testing </a></li>
  
  <li><a href="../../../../consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
  
  <li><a href="../../../../in-house-training/index.php">In-House Training</a></li>
</ul>

                    </li>

                    <li><a href="../../../../reports/index.php">Reports</a></li>
                    <li><a href="../../../../articles/index.php">Articles</a></li>
                    <li class="has-submenu"><a>About NN/G</a><i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../../../about/index.php">Overview</a></li>
                        <li><a href="../../../../people/index.php">People</a></li>
                        <li><a href="../../../../about/why-nng/index.php">Why NN/g?</a></li>
                        <li><a href="../../../../about/contact/index.php">Contact</a></li>
                        <li><a href="../../../../news/index.php">News</a></li>
                        <li><a href="../../../../about/history/index.php">History</a></li>
                        <li><a href="../../../../books/index.php">Books</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                </div>

                <hr class="header-divider show-for-small-only"/>


            </div>
       </div>

        <div id="hideNav">            <nav class="primary-nav small-12 column">
                <ul>
                    <li><a class="navitem" id="nav-home" href="../../../../index.php">Home</a></li>
                    <li><a class="navitem" id="nav-training" href="../../../index.php">Training</a></li>
                    <li><a class="navitem" id="nav-consulting" href="../../../../consulting/index.php">Consulting</a></li>
                    <li><a class="navitem" id="nav-reports" href="../../../../reports/index.php">Reports</a></li>
                    <li><a class="navitem" id="nav-articles" href="../../../../articles/index.php">Articles</a></li>
                    <li><a class="navitem" id="nav-about" href="../../../../about/index.php">About NN/G</a></li>
                </ul>
            </nav>
            <nav class="secondary-nav small-12 medium-12 large-12 columns">
                
  <ul class="inline-list">
  
<li id="nav-ux-conference"><a href="../../../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../../../../online-seminars/index.php"><span>Online Seminars</span></a></li>

</ul>


            </nav>
        </div>
    </header>
    <div id="main" class="l-content" onclick="closeNav(event)"><span id="preventClose" style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776; Menu</span>
    

<article class="course-detail">
    <div class="row">
        <p class="small-12 column">
            
            Full day training course
            
            offered at <a href="../../../london/index.php">The UX Conference London</a>
        </p>
    </div>
    <div class="row">
        <div class="l-section-intro small-12 medium-8 column">
            <header>
                <h1>Working Effectively in Cross-Functional Teams</h1>
                <h2>Achieve synergy, not infighting, when collaborating with colleagues from other UX disciplines and with non-UX team members.</h2>
            </header>
        </div>

        <div class="small-12 column show-for-small-down l-subsection">
            
            <h2 class="course-dates">Course Date: November 06, 2016</h2>
            

            
            <div style="clear: both"></div>
            <hr style="border-color: #eee"/>
        </div>
    </div>


    
    <div class="row">
        <div class="l-course-content small-12 medium-8 column">
            <div class="l-usercontent mainCourseContent">
                <p>It is only through successful cross-functional input and collaborative execution that User Experience Professionals can bring its greatest ideas to market. It is only within cross-functional teams that we can realize the speed and agility required to keep up with the rate of demand.</p>

<p>Nobody is an island. Nobody can afford to only work with people who have exactly the same background as themselves. All successful product teams include a wide selection of the following disciplines:</p>

<ul>
	<li>UX Professionals
	<ul style="list-style-type:circle;">
		<li>Interaction designers</li>
		<li>Visual designers</li>
		<li>User researchers</li>
		<li>Writers</li>
	</ul>
	</li>
	<li>Non-UX Professionals
	<ul style="list-style-type:circle;">
		<li>Marketers, brand management</li>
		<li>Engineers/developers</li>
		<li>Quality assurance</li>
		<li>Etc.</li>
	</ul>
	</li>
	<li>Management</li>
	<li>Many more, in big projects</li>
</ul>

<p>Working successfully within these broad cross-functional teams is not a &ldquo;nice to have.&rdquo; It is a MUST-have for all UX Professionals. Play well with others, or don&#39;t play at all. So, how do we ensure effective and efficient teamwork? It&#39;s not easy.</p>

<p>Wouldn&#39;t it be nice if all team members had the same ideals, the same goals, the same vocabulary, and the same understanding of everything that goes on in the project. Maybe in Utopia, but not on Planet Earth. With different backgrounds and job roles come different ideals, different goals, different vocabularies, and different interpretations of the same data. However, you can learn how to deal with these differences and turn them from obstacles to opportunities.</p>

<p>As a UXer, you are supposed to be the person on the team who knows how to deal with people. In this session, we&#39;ll learn how UX Professionals reach higher levels of success through building on their existing skillset into competencies to lead, manage, and participate successfully within cross-functional teams.</p>

<p>We&#39;ll explore how this opportunity is an especially relevant now, as the value of User Experience continues to become more broadly recognized and in-demand by businesses around the world. We&#39;ll particularly focus on the skills every UX Professional needs (regardless of level) to effectively persuade, influence, and lead others.</p>

            </div>
            
            <div class="l-usercontent mainCourseContent collapsable-content">

                <h1 class="collapse-link show-for-small-down">What You&#39;ll Learn</h1>
                <h2 class="show-for-medium-up">What You&#39;ll Learn</h2>

                <div class="collapsable">
                    <ol>
	<li>Unique communication best practices that have been customized for User Experience Professionals</li>
	<li>Strong influencing techniques specifically targeted for use by User Experience Professionals</li>
	<li>Quick ways to assess and ramp-up within a cross-functional team</li>
	<li>How to avoid common mistakes made by UX Professionals in building trust and credibility within cross-functional teams</li>
</ol>

                </div>

            </div>
            

            
            <div class="l-usercontent mainCourseContent collapsable-content">

                <h1 class="collapse-link show-for-small-down">Topics Covered</h1>
                <h2 class="show-for-medium-up">Topics Covered</h2>

                <div class="collapsable">
                    <ul>
	<li>History and future of cross-functional teams (CFTs) for User Experience (UX) Professionals working in both large corporations and small companies</li>
	<li>Case studies and analysis of User Experience Professionals working within successful (and not so successful) cross-functional teams</li>
	<li>UX facilitative leadership: Practices for providing User Experience leadership within cross-functional teams</li>
	<li>Creative cognition: The diversity of problem solving styles and their impact on CFTs</li>
	<li>Group dynamic knowledge, skills, and savvy</li>
	<li>Ten best practices of successful CFTs</li>
	<li>Common mistakes made by CFTs</li>
	<li>Collaboration &amp; cooperation: Myths and truths</li>
	<li>Confrontation &amp; resistance to change: Use it to make solutions better and gain buy-in</li>
	<li>Recognize competing loyalties and practice methods that build CFT unity</li>
	<li>Managing up: Engaging stakeholders and executives</li>
	<li>Unique communications skills for use in conflict resolution</li>
	<li>Learn to fall in love with the problem; not just your solution</li>
</ul>

                </div>

            </div>
            

            
            <div class="l-usercontent mainCourseContent collapsable-content">

                <h1 class="collapse-link show-for-small-down">Format</h1>
                <h2 class="show-for-medium-up">Format</h2>

                <div class="collapsable">
                    <p>Short periods of lecture will set the context for hands-on group exercises and micro-projects.</p>

                </div>

            </div>
            

            

            

            

            

            
                



<div class="row certification-info">
  <section class="l-subsection collapsable-content small-12 medium-8 columns">
    <h1 class="collapse-link show-for-small-down">UX Certification Credit</h1>
    <h2 class="hide-for-small-down">UX Certification Credit</h2>
    <div class="collapsable">
      <p>
      Attending this course and passing the exam earns <strong>1 UX Certification credit</strong>, which also counts towards the optional <a href="../../../../ux-certification/ux-management-specialty/index.php">UX Management Specialty</a>.
      </p>

      

      <p>
      Learn more about NN/g's <a href="../../../../ux-certification/index.php">UX Certification</a> Program.
      </p>
    </div>
  </section>
  <div class="hide-for-small-down medium-4 columns badge">
    <img alt="UX Certification Badge from Nielsen Norman Group" src="https://media.nngroup.com/nng-uxc-badge.png">
  </div>
</div>

            

            
<div class="l-usercontent mainCourseContent collapsable-content participant-comments">
  <h1 class="collapse-link show-for-small-down">Participant Comments</h1>
  <h2 class="show-for-medium-up">Participant Comments</h2>
  <div class="collapse-content collapsable">
    <blockquote>&quot;Brilliant overview of the daily inter-human issues we encounter in our daily work and more importantly, how to deal with them!&quot;</blockquote>

<p>Harry van Mierloo, Fox IT,</p>

<blockquote>&quot;Love the time set aside to reflect on your own situation/workplace paired with sharing in the group for feedback and realizing others are in the same boat.&quot;</blockquote>

<p>JJ Danielson, Sony Mobile Comm</p>

<blockquote>&quot;Very useful input! Very fun with the exercises and test. Nice flow with the three personas/profiles that were used through the class.&quot;</blockquote>

<p>Fredrikke Kvam, Accenture</p>

<blockquote>&quot;So much useful info, course deck is like a manual in navigation x-functional teams effectively. Superb!&quot;</blockquote>

<p>G Vucems, Vision Direct</p>

    <ul id="commentAccordian" class="accordion" data-accordion>
      <li class="accordion-navigation">
        <a href="#cc" class="closed" id="commentCollapseLink">More Participant Comments</a>
        <div id="cc" class="content more-comments">
          <blockquote>&quot;I pretty much liked all the real life examples and stories that often matched with my actual situation. The course also showed me another perspective on how to find solutions for cross functional interactions.&quot;</blockquote>

<p>D Albanesi, JobCloud AG,</p>

<blockquote>&quot;I really enjoyed her enthuasium for the course. You can tell that this is something she is extremely passionate about.&quot;</blockquote>

<p>Allie Etcoff, Zappos.com</p>

<blockquote>&quot;Great topic! This was definitely a new set of soft skills that I took away with me back home.&nbsp; This workshop actually crosses the border of UX and in my opinion it should be a part of the foundation on every different aspect of a company (perfectly fits many of an HR department&#39;s concerns).&quot;</blockquote>

<p>Norbito Henriques, Wedo Technologies</p>

<blockquote>&quot;It was amazing, Nancy is such an energetic and knowledgeable person and I&#39;ve learned so much, especially in communicating with my teams and interpreting their communciation styles. Definitely recommend!&quot;</blockquote>

<p>Raquel Ferreira, Visha Software International</p>

<blockquote>&quot;Loved the survey around communication styles. It helped with understanding myself better.&quot;</blockquote>

<p>Joe Lin, Texture</p>

<blockquote>&quot;I really enjoyed learning the differences and key characteristics between leadership vs. management. My mindset has been permanently changed! (in a good way).&quot;</blockquote>

<p>Carly Chiao, Auto Gravity</p>

<blockquote>&quot;We discussed the importance of working to understand each other in teams and how critical it is to maintain interpersonal relationships.&nbsp; A lot of the common mistakes and approaches to dealing with them provide good adivce on dealing with all kinds of work relationships, not just in cross-functional teams. I had a lot of &quot;aha&quot; moments.&quot;</blockquote>

<p>Michelle Larsen, Aquits, Inc.</p>

<blockquote>&quot;This course is a must for every UXP who wants to become an efficient leader and facilitator on any CFT.&quot;</blockquote>

<p>JC, Legal Zoom, Los Angeles</p>

<blockquote>&quot;I liked that the instructor uses a lot of real-world examples and practical advice. I especially liked that she gave direct quotes. I liked her sense of humor.&quot;</blockquote>

<p>L. Bogoeva, Thermo Fisher</p>

        </div>
      </li>
    </ul>
  </div>
</div>
<script>
  $('#commentAccordian').on('toggled', function (event, accordion) {
    $link = $('#commentCollapseLink');
    $link.toggleClass('closed');
    if($link.hasClass('closed')) {
      $link.text('More Participant Comments');
    } else {
      $link.text('Hide Participant Comments');
    }
  });
</script>



            <div class="l-usercontent"></div>

            
            
            <section class="course-instructors l-subsection collapsable-content">
                <h1 class="show-for-medium-up">Instructor</h1>
                <h1 class="show-for-small-down collapse-link">Instructor</h1>
                <div class="collapsable">
                    
                    <article class="course-instructor collapsable-content">
                        <h2>Nancy Dickenson</h2>
                        <div class="row">
                            <div class="medium-3 column hide-for-small-only">
                                <img src="https://media.nngroup.com/media/people/photos/IMG_8763_copy-retouched-luminous-square-800.jpg.200x200_q95_autocrop_crop_upscale.jpg">
                            </div>
                            <div class="small-12 medium-9 column">
                                <img class="show-for-small-only" src="https://media.nngroup.com/media/people/photos/IMG_8763_copy-retouched-luminous-square-800.jpg.200x200_q95_autocrop_crop_upscale.jpg">
                                <div class="usercontent"><p>Nancy Dickenson has been a leading user experience expert for over 25 years. Nancy began her career at Apple Computer in 1988. She went on to manage, lead, and build award winning, user-centered, product design and research teams at ABCNews, HarperCollins, Scient, eBay, and FirstGiving. As the head of eBay&#39;s Global User Experience Design and Research Team, Nancy spent 5 years leading over 100 user experience professionals. Nancy was an early employee of Scient Corporation where she founded and built the Customer Experience Division &mdash; a group of over 450 branding, graphic design, content, user interface, and user research employees in 13 offices worldwide.</p>

<p>Nancy founded Dickenson, LLc in 2010.&nbsp; Since then, Nancy has created successful user-centered strategies and workshops for user experience teams worldwide.&nbsp; She also provides individual coaching for UX executives.&nbsp; Recent clients include: Intuit, eBay, PayPal, Wells Fargo, MasterCard, and Financial Engines.</p>

<p>Nancy is also Executive In Residence for Bentley University&rsquo;s Human Factor and Information Design Graduate Program.</p>
</div>
                            </div>
                        </div>
                    </article>
                    
                </div>
            </section>
            <div class="show-for-small-down">
                



<div class="collapsable-content">
  <section class="l-subsection related-images">
    <h2 class="hide-for-small-down">Scenes From Past Trainings</h2>
    <h1 class="show-for-small-down collapse-link">Scenes From Past Trainings</h1>
    <div class="collapsable">
    <p>At the UX Conference, you get a full day of in-depth training with expert instructors. Learn more about <a href="../../../why-attend/index.php">why you should attend</a>.</p>

    
      
        
          
            <img srcset="https://media.nngroup.com/media/files/images/IMG_2941_copy_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale@2x.jpg 2x" src="https://media.nngroup.com/media/files/images/IMG_2941_copy_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale.jpg" alt="Hands-on exercises">
          
        
      
    
      
        
          
            <img srcset="https://media.nngroup.com/media/files/images/IMG_9994_copy_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale@2x.jpg 2x" src="https://media.nngroup.com/media/files/images/IMG_9994_copy_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale.jpg" alt="">
          
        
      
    
      
        
          
            <img srcset="https://media.nngroup.com/media/files/images/IMG_8464_copy_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale@2x.jpg 2x" src="https://media.nngroup.com/media/files/images/IMG_8464_copy_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale.jpg" alt="">
          
        
      
    
      
        
          
            <img srcset="https://media.nngroup.com/media/files/images/NN3075.jpg.300x225_q95_autocrop_crop-smart_upscale@2x.jpg 2x" src="https://media.nngroup.com/media/files/images/NN3075.jpg.300x225_q95_autocrop_crop-smart_upscale.jpg" alt="">
          
        
      
    
    </div>
  </section>
</div>


            </div>
        </div>

        <div class="l-event-sidebar medium-4 small-12 column sidebar">
            <div class="show-for-medium-up">
                <div class="l-subsection">
                    
                    <h2>Course Date: November 06, 2016</h2>
                    
                </div>

                
            </div>

            


            <section class="l-subsection l-uw-sidebar collapsable-content">
                <h2 class="show-for-medium-up">The UX Conference London Courses</h2>
                <h1 class="show-for-small-down collapse-link">UX Conference London Courses</h1>

                <div class="collapsable">
                    
                    <ul class="no-bullet">
                        
                        <li><strong>Saturday, November 5</strong></li>
                        <ul>
                            
                            
                            
                            <li>
                                
                                <a href="../../1423/ux-basic-training/index.php">UX Basic Training</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1424/ux-vp-director/index.php">The UX VP/Director</a>
                                
                            </li>
                            
                            
                            
                        </ul>
                        
                        <li><strong>Sunday, November 6</strong></li>
                        <ul>
                            
                            
                            
                            <li>
                                
                                <a href="../../1425/interaction-design-3-day-course/index.php">Interaction Design: 3-Day Course</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1450/journey-mapping/index.php">Journey Mapping to Understand Customer Needs</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1427/usability-mobile-websites-apps/index.php">Mobile User Experience</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1428/information-architecture/index.php">Information Architecture</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <strong>Working Effectively in Cross-Functional Teams</strong>
                                
                            </li>
                            
                            
                            
                        </ul>
                        
                        <li><strong>Monday, November 7</strong></li>
                        <ul>
                            
                            
                            
                            <li>
                                
                                <a href="../../1433/website-design-lessons-social-psychology/index.php">Website Design Lessons from Social Psychology</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1425/interaction-design-3-day-course/index.php">Interaction Design: 3-Day Course</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1430/leading-ux-teams/index.php">Leading Highly-Effective UX Teams </a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1431/personas/index.php">Personas: Turn User Data Into User-Centered Design</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1432/scaling-responsive-design/index.php">Scaling User Interfaces</a>
                                
                            </li>
                            
                            
                            
                        </ul>
                        
                        <li><strong>Tuesday, November 8</strong></li>
                        <ul>
                            
                            
                            
                            <li>
                                
                                <a href="../../1435/stakeholder-ux-approval/index.php">Engaging Stakeholders to Build Buy-In</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1436/ideation/index.php">Effective Ideation Techniques for UX Design</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1437/vis-mob-1/index.php">Visual Design for Mobile and Tablet: Day 1</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1438/usability-testing/index.php">Usability Testing</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1425/interaction-design-3-day-course/index.php">Interaction Design: 3-Day Course</a>
                                
                            </li>
                            
                            
                            
                        </ul>
                        
                        <li><strong>Wednesday, November 9</strong></li>
                        <ul>
                            
                            
                            
                            <li>
                                
                                <a href="../../1452/generating-big-ideas/index.php">Generating Big Ideas with Design Thinking</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1440/managing-user-experience-strategy/index.php">Managing User Experience Strategy</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1439/measuring-ux/index.php">Measuring User Experience</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1441/vis-mob-2/index.php">Visual Design for Mobile and Tablet: Day 2</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1442/content-strategy/index.php">Content Strategy</a>
                                
                            </li>
                            
                            
                            
                        </ul>
                        
                        <li><strong>Thursday, November 10</strong></li>
                        <ul>
                            
                            
                            
                            <li>
                                
                                <a href="../../1444/hci/index.php">User Interface Principles Every Designer Must Know</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1451/web-page-design/index.php">Web Page UX Design</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1443/analytics-and-user-experience/index.php">Analytics and User Experience</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1445/cross-channel-user-experience/index.php">Omnichannel Journeys and Customer Experience</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1453/facilitating-ux-workshops/index.php">Facilitating UX Workshops</a>
                                
                            </li>
                            
                            
                            
                        </ul>
                        
                        <li><strong>Friday, November 11</strong></li>
                        <ul>
                            
                            
                            
                            <li>
                                
                                <a href="../../1446/human-mind/index.php">The Human Mind and Usability</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1454/communicating-design/index.php">Communicating Design</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1449/designing-millennials/index.php">Designing for Millennials</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1448/wireframing-and-prototyping/index.php">Wireframing and Prototyping</a>
                                
                            </li>
                            
                            
                            
                            
                            
                            <li>
                                
                                <a href="../../1447/ux-deliverables/index.php">UX Deliverables</a>
                                
                            </li>
                            
                            
                            
                        </ul>
                        
                    </ul>
                </div>
            </section>

            <div class="hide-for-small-down">
                



<div class="collapsable-content">
  <section class="l-subsection related-images">
    <h2 class="hide-for-small-down">Scenes From Past Trainings</h2>
    <h1 class="show-for-small-down collapse-link">Scenes From Past Trainings</h1>
    <div class="collapsable">
    <p>At the UX Conference, you get a full day of in-depth training with expert instructors. Learn more about <a href="../../../why-attend/index.php">why you should attend</a>.</p>

    
      
        
          
            <img srcset="https://media.nngroup.com/media/files/images/IMG_2941_copy_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale@2x.jpg 2x" src="https://media.nngroup.com/media/files/images/IMG_2941_copy_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale.jpg" alt="Hands-on exercises">
          
        
      
    
      
        
          
            <img srcset="https://media.nngroup.com/media/files/images/IMG_9994_copy_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale@2x.jpg 2x" src="https://media.nngroup.com/media/files/images/IMG_9994_copy_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale.jpg" alt="">
          
        
      
    
      
        
          
            <img srcset="https://media.nngroup.com/media/files/images/IMG_8464_copy_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale@2x.jpg 2x" src="https://media.nngroup.com/media/files/images/IMG_8464_copy_2000x1500.jpg.300x225_q95_autocrop_crop-smart_upscale.jpg" alt="">
          
        
      
    
      
        
          
            <img srcset="https://media.nngroup.com/media/files/images/NN3075.jpg.300x225_q95_autocrop_crop-smart_upscale@2x.jpg 2x" src="https://media.nngroup.com/media/files/images/NN3075.jpg.300x225_q95_autocrop_crop-smart_upscale.jpg" alt="">
          
        
      
    
    </div>
  </section>
</div>


            </div>

            <p class="other-cities"><a href="../../../../courses/cross-functional-team/index.php">Other cities</a> where Working Effectively in Cross-Functional Teams is offered.</p>

        </div>
    </div>
</article>

    </div>
    




<footer id="page-footer">
    <div class="row hide-for-print">
        <div class="column small-12 medium-6">
            <section class="footer-popular">
                
                <h1>Popular Topics</h1>
                <div style="float: left;  width: 15em; display: block;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../../../topic/applications/index.php">Application Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../../../topic/e-commerce/index.php">E-Commerce Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../../../topic/eyetracking/index.php">Eyetracking </a></li>
	<li style="padding-bottom: .5em;"><a href="../../../../topic/human-computer-interaction/index.php">Human Computer Interaction </a></li>
	<li style="padding-bottom: .5em;"><a href="../../../../topic/information-architecture/index.php">Information Architecture </a></li>
	<li style="padding-bottom: .5em;"><a href="../../../../topic/intranets/index.php">Intranet Design </a></li>
	<li><a href="../../../../topic/interaction-design/index.php">Interaction Design </a></li>
</ul>
</div>

<div style="float: left;  width: 15em; display: block; list-style: none;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../../../topic/mobile-and-tablet-design/index.php">Mobile and Tablet Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../../../topic/credibility-persuasion/index.php">Persuasive Design</a></li>
	<li style="padding-bottom: .5em;"><a href="../../../../topic/research-methods/index.php">Research Methods</a></li>
	<li style="padding-bottom: .5em;"><a href="../../../../topic/user-testing/index.php">User Testing </a></li>
	<li style="padding-bottom: .5em;"><a href="../../../../topic/web-usability/index.php">Web Usability </a></li>
	<li style="padding-bottom: .5em;"><a href="../../../../topic/writing-web/index.php">Web Writing &amp; Content Strategy </a></li>
</ul>
</div>

<p>&nbsp;</p>

                
            </section>
        </div>

        <div class="column small-12 medium-6">
            <div class="row">
                <div class="column small-6">
                    <section class="footer-about">
                        <h1>Follow Us</h1>
                        <ul>
	<li><a href="../../../../articles/subscribe/index.php">Subscribe to our weekly UX newsletter</a>,&nbsp;Jakob Nielsen&#39;s Alertbox</li>
	<li>Visit Don Norman&#39;s website, <a href="http://www.jnd.org/"> www.jnd.org </a></li>
	<li>Visit Bruce Tognazzini&#39;s website, <a href="http://www.asktog.com/"> www.asktog.com </a></li>
	<li style="padding-top: 6px;"><a class="twitter-follow-button" data-show-count="false" data-size="large" href="https://twitter.com/nngroup"><img alt="Follow @nngroup on Twitter" height="20" src="https://media.nngroup.com/media/editor/2016/12/30/twitter-follow-button.png" style="width: 117px; height: 20px;" width="117" /></a></li>
	<li style="padding-top: 6px;"><img alt="" height="14" src="https://media.nngroup.com/media/editor/2016/07/26/feed-icon-14x14.png" style="vertical-align:text-top;width:14px;margin-right:.25em;" width="14" /><a href="../../../../feed/rss/index.php">Subscribe to our RSS feed</a></li>
</ul>

                    </section>
                </div>

                <div class="column small-6">
                    <section class="footer-about right-column">
                        <h1>About
</h1>
                        <ul class="no-bullet">
	<li><a href="../../../../about/contact/index.php">Contact information</a></li>
	<li><a href="../../../../about/index.php">About Us</a></li>
	<li><a href="../../../../about/why-nng/index.php">Why NN/g? </a></li>
	<li><a href="../../../../return-policy/index.php">Return Policy</a></li>
</ul>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="copyright column small-12">
            <p><a href="../../../../copyright-and-reprint-info/index.php">Copyright</a> &copy; 1998-2017 Nielsen Norman Group, All Rights Reserved.</p>
        </div>
    </div>
</footer>


    


        <script src="https://media.nngroup.com/static/vendor/fastclick/js/fastclick.min.js"></script>
        <script src="https://media.nngroup.com/static/vendor/foundation/js/foundation.min.js"></script>
        <script src="http://media.nngroup.com/static/js/app.js"></script>
        <script src="http://media.nngroup.com/static/js/scroll-to-fixed.js"></script><script>var externalLinks = document.querySelectorAll('a[href^="http"]');for (var i = externalLinks.length-1; i >= 0; i--) {externalLinks[i].addEventListener("click", function(event) { alert("External links are disabled for this test."); event.preventDefault(); }, false);}var mailLinks = document.querySelectorAll('a[href^="mailto"]');for (var i = mailLinks.length-1; i >= 0; i--) {mailLinks[i].addEventListener("click", function(event) { alert("Email links are disabled for this test."); event.preventDefault(); }, false);}var externalForms = document.querySelectorAll('form');for (var i = externalForms.length-1; i >= 0; i--) {externalForms[i].addEventListener("submit", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false); externalForms[i].addEventListener("click", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false);}</script>

        <script>
            
            $(function() {
                $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
                $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
            });
        </script>

        
    

    <script>
        $(function() {
            var toggle = $("#collapse-toggle");
            toggle.on("click", function() {
                $(".collapsable").toggleClass("collapsed");
                toggle.toggleClass("collapsed");
                return(false);
            })
        });
    </script>


        
        <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1072699960;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072699960/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
    </body>

<!-- Mirrored from www.nngroup.com/training/course/1426/cross-functional-team/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 15:16:00 GMT -->
</html>
