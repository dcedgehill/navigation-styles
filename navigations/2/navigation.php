<!doctype html>
<html id="doc" class="no-js">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="/apps/navigation_styles/app/app.js"></script>
    <script>
        window.onclick = function() { addCoordinates() };
        window.addEventListener("touchstart", function() { addCoordinates() }, false);
    </script>
    <link href="/apps/navigation_styles/navigations/2/assets/style.css" rel="stylesheet"/>
</head>
<body>
<div id="mySidenav" class="sidenav">
    <a href="#" class="closebtn" onclick="closeNav(event)">&times;</a>
    <a href="/apps/navigation_styles/navigations/2/index.php">Home</a>
    <a href="/apps/navigation_styles/navigations/2/reports/index.php">Reports</a>
    <a href="/apps/navigation_styles/navigations/2/articles/index.php">Articles</a>
    <a href="/apps/navigation_styles/navigations/2/training/index.php">Training</a>
    <a href="/apps/navigation_styles/navigations/2/consulting/index.php">Consulting</a>
    <a href="/apps/navigation_styles/navigations/2/about/index.php">About NN/G</a>
</div>
<script>
    function openNav() {
        document.getElementById("mySidenav").style.width = "200px";
        document.getElementById("main").style.marginLeft = "200px";
        document.getElementsByTagName("body")[0].style.backgroundColor = "rgba(0,0,0,0.4)";
        document.getElementById("preventClose").style.backgroundColor = "transparent";

    }

    function closeNav(event) {
        var target = event.target || event.srcElement;
        var id = target.id;
        if(id !== "preventClose") {
            document.getElementById("mySidenav").style.width = "0";
            document.getElementById("main").style.marginLeft= "8px";
            document.getElementsByTagName("body")[0].style.backgroundColor = "white";
            document.getElementById("preventClose").style.backgroundColor = "#FFFFFF";
        }
    }
</script>
</body>
</html>