<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->





    
<!-- Mirrored from www.nngroup.com/online-seminars/every-word-count/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 14:59:29 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(){}function o(e,n,t){return function(){return i(e,[c.now()].concat(u(arguments)),n?null:this,t),n?void 0:this}}var i=e("handle"),a=e(2),u=e(3),f=e("ee").get("tracer"),c=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var p=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],d="api-",l=d+"ixn-";a(p,function(e,n){s[n]=o(d+n,!0,"api")}),s.addPageAction=o(d+"addPageAction",!0),s.setCurrentRouteName=o(d+"routeName",!0),n.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,n){var t={},r=this,o="function"==typeof n;return i(l+"tracer",[c.now(),e,t],r),function(){if(f.emit((o?"":"no-")+"fn-start",[c.now(),r,o],t),o)try{return n.apply(this,arguments)}finally{f.emit("fn-end",[c.now()],t)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,n){m[n]=o(l+n)}),newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),i("err",[e,c.now()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(o<0?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],4:[function(e,n,t){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?f(e,u,i):i()}function t(t,r,o,i){if(!d.aborted||i){e&&e(t,r,o);for(var a=n(o),u=m(t),f=u.length,c=0;c<f;c++)u[c].apply(a,r);var p=s[y[t]];return p&&p.push([b,t,r,a]),a}}function l(e,n){v[e]=m(e).concat(n)}function m(e){return v[e]||[]}function w(e){return p[e]=p[e]||o(t)}function g(e,n){c(e,function(e,t){n=n||"feature",y[t]=n,n in s||(s[n]=[])})}var v={},y={},b={on:l,emit:t,get:w,listeners:m,context:n,buffer:g,abort:a,aborted:!1};return b}function i(){return new r}function a(){(s.api||s.feature)&&(d.aborted=!0,s=d.backlog={})}var u="nr@context",f=e("gos"),c=e(2),s={},p={},d=n.exports=o();d.backlog=s},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!x++){var e=h.info=NREUM.info,n=d.getElementsByTagName("script")[0];if(setTimeout(s.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&n))return s.abort();c(y,function(n,t){e[n]||(e[n]=t)}),f("mark",["onload",a()+h.offset],null,"api");var t=d.createElement("script");t.src="https://"+e.agent,n.parentNode.insertBefore(t,n)}}function o(){"complete"===d.readyState&&i()}function i(){f("mark",["domContent",a()+h.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(u=Math.max((new Date).getTime(),u))-h.offset}var u=(new Date).getTime(),f=e("handle"),c=e(2),s=e("ee"),p=window,d=p.document,l="addEventListener",m="attachEvent",w=p.XMLHttpRequest,g=w&&w.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:w,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1026.min.js"},b=w&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),h=n.exports={offset:u,now:a,origin:v,features:{},xhrWrappable:b};e(1),d[l]?(d[l]("DOMContentLoaded",i,!1),p[l]("load",r,!1)):(d[m]("onreadystatechange",o),p[m]("onload",r)),f("mark",["firstbyte",u],null,"api");var x=0,E=e(4)},{}]},{},["loader"]);</script><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"licenseKey":"1a30ef9931","queueTime":0,"beacon":"bam.nr-data.net","errorBeacon":"bam.nr-data.net","applicationID":"2048992","transactionName":"NFxSMRBUX0EDUkIPDA0WdhAMVkVbDV8ZCQ0PUF4APUZUXwtfVxQQTU9ZABVGC30MXV8IBjBcXQwMVEN2B0VXDw9NXlUR","applicationTime":304,"agent":""}</script>
        <title>Making Every Word Count Online Seminar</title><meta property="og:title" content="Making Every Word Count Online Seminar" />
  
        
        <meta name="description" content="Learn how to write clear, compelling, conversational copy (and microcopy) for those busy people on their small screens: Practical guidelines. Great examples. ">
        <meta property="og:description" content="Learn how to write clear, compelling, conversational copy (and microcopy) for those busy people on their small screens: Practical guidelines. Great examples. " />
        
  
        
	
        
        <meta name="keywords" content="writing, website, online seminar, copy, mobile, responsive design, writing for the web ">
        

        

        <meta name="robots" content="noarchive">
        <meta name="viewport" content="width=device-width">

        <link rel="search" type="application/opensearchdescription+xml" title="NN/g Search" href="../../opensearch.xml" />
        <link rel="shortcut icon" href="http://media.nngroup.com/static/img/favicon.ico" />

        <link rel="stylesheet" href="https://media.nngroup.com/static/vendor/font-awesome/css/font-awesome.min.css">

        
        <link rel="stylesheet" href="https://media.nngroup.com/static/css/7232a960f1df.css" type="text/css" />
        <link rel="stylesheet" href="http://fast.fonts.com/t/1.css?apiType=css&amp;projectid=c5846260-9def-46ec-9407-0f1349bd2d64" type="text/css">

        <script src="https://media.nngroup.com/static/vendor/modernizr/js/modernizr.min.js"></script>

        <script type="text/javascript" src="https://media.nngroup.com/static/js/3283be5def74.js"></script>
        

        

        
        
        <!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=229034,
settings_tolerance=2000,
library_tolerance=2500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('http://dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->


<!-- Google Analytics Tracking Code for http://nngroup.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23132681-1', 'nngroup.com');
  ga('require', 'linkid', 'https://www.nngroup.com/online-seminars/every-word-count/linkid.js');
  ga('require', 'displayfeatures');

// Optimizely Universal Analytics Integration
 window.optimizely = window.optimizely || [];
 window.optimizely.push("activateUniversalAnalytics");

  ga('send', 'pageview');

</script>
<script>
/**
* Function to track a click on an outbound link in Google Analytics.
*/
function trackOutboundLink(label, url) {
    if(window.ga && ga.loaded) {
        ga('send', 'event', 'outbound', 'click', label, {'hitCallback': function(){document.location = url;}
        });
    } else {
        document.location = url;
    }
}
/**
* Function to track a click on a link in the article announcement area.
*/
$('#articleAnnouncement a').on('click', function(){
  ga('send', 'event', 'link', 'click', 'articleAnnouncement');
});
</script>

<script type="text/javascript">
 setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2421.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



<!-- Hotjar Tracking Code for http://nngroup.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:121605,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

        <script src="https://media.nngroup.com/static/js/analytics/track_outbound_link.js"></script>
        
        
        
    </head>
    <body class="location-training location-online-seminars online-seminars location-course-detail">

        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        
    <header id="page-header">
        <div id="cart" class="shopping-cart mobile-cart show-for-small-down"></div>
        <div class="row">
            <div id="masthead" class="masthead small-12 medium-7 column">

                <!-- The button to toggles menu on small screen -->

                <h1><a href="../../index.php"><img src="http://media.nngroup.com/static/img/logo-wordmark.png" width="74" height="30"> Nielsen <span class="norman">Norman</span> Group</a></h1>
                <p class="tagline show-for-medium-up">Evidence-Based User Experience Research, Training, and Consulting</p>

                <!-- The button to toggles form search on small screen -->


                <div style="clear: right;width:  100%;height: 1px;"></div><?php define('ROOT_PATH',$_SERVER["DOCUMENT_ROOT"] . '/apps/navigation_styles/navigations/2'); ?><?php include(ROOT_PATH)."/navigation.php"; ?>

                <!-- Drop down menu on small screen -->
                <div id="hideNav" class="show-for-small-down">
                  
                  <ul class="menu-content">
                    <li><a href="../../index.php">Home</a></li>
                    <li class="has-submenu">
                      <a>Training</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../training/index.php">Overview</a></li>
                        
<li id="nav-ux-conference"><a href="../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../index.php"><span>Online Seminars</span></a></li>

                      </ul>
                    </li>
                    <li class="has-submenu">
                      <a>Consulting</a>
                      <i class="arrow-down right"></i>
                      <ul class="submenu-content">
  <li><a href="../../consulting/index.php">Overview</a></li>
  
  <li><a href="../../consulting/benchmarking/index.php">Benchmarking</a></li>
  
  <li><a href="../../consulting/expert-design-review/index.php">Expert Design Review</a></li>
  
  <li><a href="../../consulting/ia-navigation-analysis/index.php">IA &amp; Navigation Analysis</a></li>
  
  <li><a href="../../consulting/usability-testing/index.php">Usability Testing </a></li>
  
  <li><a href="../../consulting/ux-strategy/index.php">UX Strategy Consulting</a></li>
  
  <li><a href="../../in-house-training/index.php">In-House Training</a></li>
</ul>

                    </li>

                    <li><a href="../../reports/index.php">Reports</a></li>
                    <li><a href="../../articles/index.php">Articles</a></li>
                    <li class="has-submenu"><a>About NN/G</a><i class="arrow-down right"></i>
                      <ul class="submenu-content">
                        <li><a href="../../about/index.php">Overview</a></li>
                        <li><a href="../../people/index.php">People</a></li>
                        <li><a href="../../about/why-nng/index.php">Why NN/g?</a></li>
                        <li><a href="../../about/contact/index.php">Contact</a></li>
                        <li><a href="../../news/index.php">News</a></li>
                        <li><a href="../../about/history/index.php">History</a></li>
                        <li><a href="../../books/index.php">Books</a></li>
                      </ul>
                    </li>
                  </ul>
                  
                </div>

                <hr class="header-divider show-for-small-only"/>


            </div>
       </div>

        <div id="hideNav">            <nav class="primary-nav small-12 column">
                <ul>
                    <li><a class="navitem" id="nav-home" href="../../index.php">Home</a></li>
                    <li><a class="navitem" id="nav-training" href="../../training/index.php">Training</a></li>
                    <li><a class="navitem" id="nav-consulting" href="../../consulting/index.php">Consulting</a></li>
                    <li><a class="navitem" id="nav-reports" href="../../reports/index.php">Reports</a></li>
                    <li><a class="navitem" id="nav-articles" href="../../articles/index.php">Articles</a></li>
                    <li><a class="navitem" id="nav-about" href="../../about/index.php">About NN/G</a></li>
                </ul>
            </nav>
            <nav class="secondary-nav small-12 medium-12 large-12 columns">
                
  <ul class="inline-list">
  
<li id="nav-ux-conference"><a href="../../ux-conference/index.php"><span>UX Conference</span></a></li>
<li id="nav-certification-profile"><a href="../../ux-certification/index.php"><span>UX Certification</span></a></li>
<li id="nav-in-house-training"><a href="../../in-house-training/index.php"><span>In-House Training</span></a></li>
<li id="nav-online-seminars"><a href="../index.php"><span>Online Seminars</span></a></li>

</ul>


            </nav>
        </div>
    </header>
    <div id="main" class="l-content" onclick="closeNav(event)"><span id="preventClose" style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776; Menu</span>
    


<article class="course-detail online-seminars">

<div class="row">
    <div class="column small-12 medium-8">

        <div class="row">
            
            <p class="twelve columns">
                
    			On-Demand <a href="../index.php">Online Seminar</a>
    			
            </p>
        </div>


        <div class="row">
            <div class="l-section-intro eight columns">
                <header>
                    <h1>Making Every Word Count</h1>
                    <h2>Writing Well for Responsive Design</h2>
                </header>
                
            </div>

        </div>

        <div class="row">
            <div class="l-course-content eight columns">

                <div class="l-usercontent courseInfoContent">
                    <p>
                    
    				    <strong> Recorded September 2016</strong><br />
    				

    				
    				60 minutes<br />
    				

                    
                    $99
                    
    				</p>
                    <div class="show-for-small-only">
                        




  <a class="medium pricing button register-button" href="https://attendee.gototraining.com/r/4306341024978595842">Buy Now</a>
<br />
                    </div>
                    <div class="show-for-small-only">
                        <a href="../../online-seminars-faq/index.php">FAQs about Online Seminars</a>
                    </div>
    			</div>

                <div class="l-usercontent mainCourseContent">
                    <p>We now live in a multi-screen world. How many internet devices do you use? Laptop? Tablet? Smartphone? Did you say &quot;yes,&quot; and &quot;yes,&quot; and &quot;yes&quot;?</p>

<p>Many of your website visitors would also say &quot;yes&quot; three times. If they say &quot;yes&quot; only twice, it&#39;s probably for tablet and smartphone, not laptop.</p>

<p>How well does your website content work on all those screens?</p>

<p>In this fast-paced, very practical hour, content guru Ginny Redish will help you make every word count. You&#39;ll see developing great content as a process. You&#39;ll learn specific techniques for rapid planning. You&#39;ll realize that &quot;content = conversation&quot; and learn to engage site visitors with great conversations.</p>

<p>You may have to break some old-fashioned school-taught habits to become a world-class web content writer (or reviewer). Ginny&#39;s guidelines and examples will help.</p>

<p>Recommended for everyone who works on websites, especially content writers and editors, subject matter specialists who get web assignments, content strategists, project managers and other reviewers, designers, and usability specialists.<br />
&nbsp;</p>

<h2>What You&#39;ll Learn</h2>

<ul>
	<li>The process of creating successful, engaging web content</li>
	<li>The power of rapid planning and specific planning techniques</li>
	<li>Practical guidelines with examples for writing great web content in our multi-screen, responsive-design world<br />
	&nbsp;</li>
</ul>

<h2>Topics We&#39;ll Cover</h2>

<ul>
	<li>Setting the context
	<ul>
		<li>Stories &ndash; When and why people use websites</li>
		<li>Content &ndash; What site visitors want</li>
		<li>Conversation &ndash; Every use of your website is a conversation that the site visitor starts</li>
		<li>Smartphones and tablets &ndash; Stats on mobile use and on where and when people use mobile apps and sites</li>
		<li>Responsive design &ndash; A technology solution, not a content solution</li>
	</ul>
	</li>
	<li>Planning great content
	<ul>
		<li>Creating content is a process that starts with planning</li>
		<li>Everything we write in the workplace is functional</li>
		<li>What do you want people to do? (purposes)</li>
		<li>Who are those people and what should you keep in mind about them? (personas)</li>
		<li>What&#39;s your key message? What questions are they asking? (conversations)</li>
		<li>Examples: Using the power of this planning to evaluate a successful website and an unsuccessful one</li>
	</ul>
	</li>
	<li>Writing great content
	<ul>
		<li>Guidelines with an explanation and at least one example for each guideline. For many of the examples, we&#39;ll see again the power of answering and applying the planning questions.</li>
	</ul>
	</li>
	<li>Think &quot;mobile first&quot;
	<ul>
		<li>Cut! Cut! Cut!</li>
		<li>Converse! &ndash; pronouns, active voice, strong verbs</li>
		<li>Keep it short! &ndash; short sentences, tiny paragraphs, fragments, lists</li>
	</ul>
	</li>
	<li>Engage people immediately
	<ul>
		<li>Assume people will skim and stop</li>
		<li>Put the key message first</li>
		<li>Answer site visitors&#39; questions (but not with a separate FAQ page)</li>
		<li>Break it up with meaningful headings</li>
	</ul>
	</li>
	<li>Make every word count
	<ul>
		<li>Choose the shortest, simplest word that has the right meaning</li>
		<li>Use microcopy wisely</li>
	</ul>
	</li>
	<li>Finish the process (a few more tips very quickly as we end the hour)
	<ul>
		<li>Review and edit</li>
		<li>Test for usability</li>
		<li>But even before that &ndash; walk your personas through their conversations</li>
		<li>Everyone: Put your ego in a drawer</li>
	</ul>
	</li>
</ul>

                </div>

                <div class="row">
                    <div class="l-usercontent eight columns">
                    
                        <ul>
	<li>Your recording will be available for viewing as soon as payment is processed.</li>
	<li>The link to view the online seminar will be emailed to the address used for registration.&nbsp;</li>
</ul>

                    
                    </div>
                </div>

                <div class="l-usercontent cta">
                    
                        <h1>View this recording</h1>
                    
                    <p>
                    
                        Recorded September 2016<br />
                    

                    
                        60 minute online seminar<br />
                        

                    
                        $99
                    
                    </p>

                    <div class="cta-button clearfix">
                        




  <a class="medium pricing button register-button" href="https://attendee.gototraining.com/r/4306341024978595842">Buy Now</a>

                        <span class="right"><a href="../../online-seminars-faq/index.php">Online Seminar FAQs</a></span>
                    </div>
                </div>

                <section class="course-instructors l-subsection">
                    <h1>Speaker</h1>
                    
                    <article class="course-instructor">
                        <div class="row">
                            <div class="small-7 small-push-5 medium-12 medium-reset-order columns name">
                              <h1>
  
    Janice (Ginny) Redish
  
</h1>
<em>
  President, Redish &amp; Associates, Inc.
</em>

                            </div>
                            <div class="small-5 small-pull-7 medium-3 medium-reset-order columns instructor-portrait">
                                <img src="https://media.nngroup.com/media/people/photos/Ginny_2014_face_right.jpg.200x200_q95_autocrop_crop_upscale.jpg">
                            </div>
                            <div class="small-12 medium-9 columns bio">
                                <div class="usercontent"><p>Ginny Redish has been a passionate evangelist for clear writing and usability for many years. Through her consulting practice,&nbsp;<a href="http://www.redish.net/">Redish &amp; Associates, Inc.</a>, Ginny helps clients plan, organize, design, write, and test all types of communications.</p>

<p>Ginny is sought after as a speaker and workshop leader. She is a dynamic instructor who has trained thousands of writers and subject matter specialists in the United States, Canada, Asia, and Europe.</p>

<p>Reviewers rave about Ginny&rsquo;s most recent book,&nbsp;<a href="http://redish.net/books/item/5">Letting Go of the Words &ndash; Writing Web Content that Works</a>, (2nd&nbsp;edition, 2012). She has also written or co-written numerous articles and book chapters, as well as two of the classic books on usability.</p>

<p>Ginny is a graduate of Bryn Mawr College and holds a Ph.D. in Linguistics from Harvard University. She turns her deep understanding of research in cognitive science, human-computer interaction, information design, and linguistics into practical guidelines you can apply right away.</p>
</div>
                            </div>
                        </div>
                    </article>
                    
                </section>

                <section class="related-items l-subsection related-content">
                  <h1>Related</h1>

                  <div class="row related-content">
                    <div class="small-12 medium-6 columns">
                      
                      <div>
                        <h2>Research Reports</h2>
                        <ul class="no-bullet">
                          
                            <li><a href="../../reports/tablets/index.php">Tablet Website and Application UX</a></li>
                          
                            <li><a href="../../reports/how-people-read-web-eyetracking-evidence/index.php">How People Read on the Web: The Eyetracking Evidence</a></li>
                          
                            <li><a href="../../reports/mobile-website-and-application-usability/index.php">User Experience for Mobile Applications and Websites</a></li>
                          
                            <li><a href="../../reports/strategic-design-faqs/index.php">Strategic Design for Frequently Asked Questions</a></li>
                          
                            <li><a href="../../reports/enterprise-mobile-showcase/index.php">Mobile Intranets and Enterprise Apps</a></li>
                          
                        </ul>
                      </div>
                      

                      
                      <div>
                        <h2>UX Conference Training Courses</h2>
                        <ul class="no-bullet">
                          
                          <li><a href="../../courses/cross-channel-user-experience/index.php">Omnichannel Journeys and Customer Experience</a></li>
                          
                          <li><a href="../../courses/usability-mobile-websites-apps/index.php">Mobile User Experience</a></li>
                          
                          <li><a href="../../courses/scaling-responsive-design/index.php">Scaling User Interfaces</a></li>
                          
                          <li><a href="../../courses/content-strategy/index.php">Content Strategy</a></li>
                          
                          <li><a href="../../courses/writing/index.php">Writing Compelling Digital Copy</a></li>
                          
                        </ul>
                      </div>
                      
                    </div>

                    <div class="small-12 medium-6 columns">
                      
                      <div>
                        <h2>Articles</h2>
                        <ul class="no-bullet">
                          
                          <li><a href="../../articles/mobile-ux/index.php">Mobile User Experience: Limitations and Strengths</a></li>
                          
                          <li><a href="../../articles/scaling-user-interfaces/index.php">Scaling User Interfaces: An Information-Processing Approach to Multi-Device Design</a></li>
                          
                          <li><a href="../../articles/tablet-usability/index.php">Tablet Usability</a></li>
                          
                          <li><a href="../../articles/mobile-sharpens-usability-guidelines/index.php">Mobile UX Sharpens Usability Guidelines</a></li>
                          
                          <li><a href="../../articles/worlds-best-headlines-bbc-news/index.php">World&#39;s Best Headlines: BBC News</a></li>
                          
                        </ul>
                      </div>
                      
                    </div>
                  </div>
                </section>
            </div>
        </div>
    </div>

    <div class="column small-12 medium-4 hide-for-small-only">
        <div class="l-section-intro introCTA">
            

<section class="l-subsection bordered">
    <p>
        
            Recorded September 2016
        
    </p>
    <div class="cart-block webinar-cart">
        <span class="price">
            
                $99
            
        </span>
        




  <a class="medium pricing button register-button" href="https://attendee.gototraining.com/r/4306341024978595842">Buy Now</a>

    </div>
    
        
    
</section>
<section class="l-subsection">
<a href="../../online-seminars-faq/index.php">FAQs about Online Seminars</a>
</section>

            <div class="row course-instructors l-subsection">
                <div class="columns small-12">

                    <h1>Speaker</h1>
                    
                    <article class="course-instructor">
                        <div class="row name">
                            <div class="columns small-6">
                                <img src="https://media.nngroup.com/media/people/photos/Ginny_2014_face_right.jpg.200x200_q95_autocrop_crop_upscale.jpg">
                            </div>
                            <div class="columns small-6">
                              <h1>
  
    Janice (Ginny) Redish
  
</h1>
<em>
  President, Redish &amp; Associates, Inc.
</em>
 
                            </div>
                        </div>

                    

                </div>
            </div>

        </div>
    </div>
</div>

</article>

    </div>
    




<footer id="page-footer">
    <div class="row hide-for-print">
        <div class="column small-12 medium-6">
            <section class="footer-popular">
                
                <h1>Popular Topics</h1>
                <div style="float: left;  width: 15em; display: block;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/applications/index.php">Application Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/e-commerce/index.php">E-Commerce Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/eyetracking/index.php">Eyetracking </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/human-computer-interaction/index.php">Human Computer Interaction </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/information-architecture/index.php">Information Architecture </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/intranets/index.php">Intranet Design </a></li>
	<li><a href="../../topic/interaction-design/index.php">Interaction Design </a></li>
</ul>
</div>

<div style="float: left;  width: 15em; display: block; list-style: none;">
<ul class="no-bullet">
	<li style="padding-bottom: .5em;"><a href="../../topic/mobile-and-tablet-design/index.php">Mobile and Tablet Design </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/credibility-persuasion/index.php">Persuasive Design</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/research-methods/index.php">Research Methods</a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/user-testing/index.php">User Testing </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/web-usability/index.php">Web Usability </a></li>
	<li style="padding-bottom: .5em;"><a href="../../topic/writing-web/index.php">Web Writing &amp; Content Strategy </a></li>
</ul>
</div>

<p>&nbsp;</p>

                
            </section>
        </div>

        <div class="column small-12 medium-6">
            <div class="row">
                <div class="column small-6">
                    <section class="footer-about">
                        <h1>Follow Us</h1>
                        <ul>
	<li><a href="../../articles/subscribe/index.php">Subscribe to our weekly UX newsletter</a>,&nbsp;Jakob Nielsen&#39;s Alertbox</li>
	<li>Visit Don Norman&#39;s website, <a href="http://www.jnd.org/"> www.jnd.org </a></li>
	<li>Visit Bruce Tognazzini&#39;s website, <a href="http://www.asktog.com/"> www.asktog.com </a></li>
	<li style="padding-top: 6px;"><a class="twitter-follow-button" data-show-count="false" data-size="large" href="https://twitter.com/nngroup"><img alt="Follow @nngroup on Twitter" height="20" src="https://media.nngroup.com/media/editor/2016/12/30/twitter-follow-button.png" style="width: 117px; height: 20px;" width="117" /></a></li>
	<li style="padding-top: 6px;"><img alt="" height="14" src="https://media.nngroup.com/media/editor/2016/07/26/feed-icon-14x14.png" style="vertical-align:text-top;width:14px;margin-right:.25em;" width="14" /><a href="../../feed/rss/index.php">Subscribe to our RSS feed</a></li>
</ul>

                    </section>
                </div>

                <div class="column small-6">
                    <section class="footer-about right-column">
                        <h1>About
</h1>
                        <ul class="no-bullet">
	<li><a href="../../about/contact/index.php">Contact information</a></li>
	<li><a href="../../about/index.php">About Us</a></li>
	<li><a href="../../about/why-nng/index.php">Why NN/g? </a></li>
	<li><a href="../../return-policy/index.php">Return Policy</a></li>
</ul>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="copyright column small-12">
            <p><a href="../../copyright-and-reprint-info/index.php">Copyright</a> &copy; 1998-2017 Nielsen Norman Group, All Rights Reserved.</p>
        </div>
    </div>
</footer>


    


        <script src="https://media.nngroup.com/static/vendor/fastclick/js/fastclick.min.js"></script>
        <script src="https://media.nngroup.com/static/vendor/foundation/js/foundation.min.js"></script>
        <script src="http://media.nngroup.com/static/js/app.js"></script>
        <script src="http://media.nngroup.com/static/js/scroll-to-fixed.js"></script><script>var externalLinks = document.querySelectorAll('a[href^="http"]');for (var i = externalLinks.length-1; i >= 0; i--) {externalLinks[i].addEventListener("click", function(event) { alert("External links are disabled for this test."); event.preventDefault(); }, false);}var mailLinks = document.querySelectorAll('a[href^="mailto"]');for (var i = mailLinks.length-1; i >= 0; i--) {mailLinks[i].addEventListener("click", function(event) { alert("Email links are disabled for this test."); event.preventDefault(); }, false);}var externalForms = document.querySelectorAll('form');for (var i = externalForms.length-1; i >= 0; i--) {externalForms[i].addEventListener("submit", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false); externalForms[i].addEventListener("click", function(event) { alert("Forms are disabled for this test."); event.preventDefault(); }, false);}</script>

        <script>
            
            $(function() {
                $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
                $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
            });
        </script>

        
    

    <script>

    $(function() {
        $('.collapse-link').on('click', function() {
            $(this).toggleClass('collapsed');
            var collapsed = $(this).hasClass('collapsed');
            $(this).siblings('.collapsable').toggleClass('collapsed', collapsed);
        });
        $('.anchorExpandLink').on('click', function() {
            $('.collapse-link').removeClass('collapsed');
            $('.collapse-link').siblings('.collapsable').removeClass('collapsed');
        });
    });

    </script>


        
        <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 1072699960;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072699960/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
    </body>

<!-- Mirrored from www.nngroup.com/online-seminars/every-word-count/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Mar 2017 14:59:29 GMT -->
</html>
