<?php
if (isset($_GET["new"]) || $_GET["new"] == "true") {
    include_once ("php/assign_id.php");
    include_once("php/connect_to_db.php");
    $testerID = assign_id(4);
    $sql = "ALTER TABLE record_navigation_styles ALTER tester_id SET DEFAULT '$testerID'";
    $execute = mysqli_query($mysqli, $sql);
    $sql = "ALTER TABLE record_navigation_styles ALTER own_device SET DEFAULT 'false'";
    $execute = mysqli_query($mysqli, $sql);
    setcookie(
        "tester_id",
        $testerID,
        time() + (10 * 365 * 24 * 60 * 60)
    );
    setcookie(
        "own_device",
        "false",
        time() + (10 * 365 * 24 * 60 * 60)
    );
    header('Location: http://www.mytimeworld.co.uk/apps/navigation_styles/tester.php');
}
echo "Tester ID: " . $_COOKIE['tester_id'] . "<br /><a href=\"?new=true\">New tester</a>";
echo "";