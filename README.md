# Navigation Styles Website Application README

Shows the process of how the Navigation Styles Website Application was developed.

* Click on Source to see the code for the Navigation Styles Website Application.
* To see the participant data, click on Source and check out the php/ and php/data folders for any CSV, SQL and Zip files.
* Visit https://www.mytimeworld.co.uk/apps/navigation_styles/ on a mobile device to try out the website application.