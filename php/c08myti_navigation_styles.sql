-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Apr 23, 2017 at 09:06 AM
-- Server version: 10.0.30-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `c08myti_navigation_styles`
--

-- --------------------------------------------------------

--
-- Table structure for table `navigation_styles`
--

CREATE TABLE `navigation_styles` (
  `id` int(11) NOT NULL,
  `style` varchar(255) NOT NULL,
  `task` varchar(255) NOT NULL,
  `no_interactions` int(11) NOT NULL,
  `position_task_btn` varchar(255) NOT NULL,
  `position_done_btn` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL DEFAULT 'na',
  `url_location_old` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `navigation_styles`
--

INSERT INTO `navigation_styles` (`id`, `style`, `task`, `no_interactions`, `position_task_btn`, `position_done_btn`, `url`, `url_location_old`) VALUES
(1, 'Multi-Level Navigation Style', 'Find out the opening hours for Bake Bakery on Mondays. Then find out what types of bread Bake Bakery has to offer. Finally, place an order and collect for brown bread and a carrot cake.', 0, 'bottom left', 'bottom right', 'na', 'times.php bread.php order_form.php'),
(2, 'Off Canvas Navigation Style', 'Find out the opening hours for Bake Bakery on Wednesday. Then find out what types of cakes Bake Bakery has to offer. Finally, place an order and collect for a sponge cake and a toffee cookie.', 0, 'bottom right', 'bottom right', 'na', 'times.php cakes.php order_form.php'),
(3, 'Slider Navigation Style', 'Find out the opening hours for Bake Bakery on Friday. Then find out what types of cookies Bake Bakery has to offer. Finally, place an order and collect for a banana cookie and pasty.', 0, 'bottom left', 'bottom right', 'na', 'times.php cookies.php order_form.php'),
(4, 'Arc Navigation Style', 'Find out the opening hours for Bake Bakery on Saturdays. Then find out what types of pastries Bake Bakery has to offer. Finally, place an order and collect for a sausage roll and a meat pie.', 0, 'bottom left', 'bottom right', 'na', 'times.php pastries.php order_form.php'),
(5, 'Full Screen Navigation Style', 'Find out the opening hours for Bake Bakery on Sundays. Then find out what types of pies Bake Bakery has to offer. Finally, place an order and collect for a meat and potato pie and white bread.', 0, 'bottom left', 'bottom right', 'na', 'times.php pies.php order_form.php');

-- --------------------------------------------------------

--
-- Table structure for table `record_navigation_styles`
--

CREATE TABLE `record_navigation_styles` (
  `id` int(11) NOT NULL,
  `no_interactions` longtext NOT NULL,
  `touch_locations` longtext NOT NULL,
  `no_seconds` longtext NOT NULL,
  `viewed` longtext NOT NULL,
  `visited_pages` longtext NOT NULL,
  `device` varchar(255) NOT NULL,
  `operating_system` varchar(255) NOT NULL,
  `browser` varchar(255) NOT NULL,
  `held` varchar(255) NOT NULL,
  `hand` varchar(255) NOT NULL,
  `width` longtext NOT NULL,
  `height` longtext NOT NULL,
  `own_device` varchar(255) NOT NULL DEFAULT 'false',
  `test_data` varchar(255) NOT NULL DEFAULT 'false',
  `tester_id` varchar(255) NOT NULL DEFAULT 'en1l',
  `navigation_styles_id` int(11) NOT NULL,
  `task_sets_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `record_navigation_styles`
--

INSERT INTO `record_navigation_styles` (`id`, `no_interactions`, `touch_locations`, `no_seconds`, `viewed`, `visited_pages`, `device`, `operating_system`, `browser`, `held`, `hand`, `width`, `height`, `own_device`, `test_data`, `tester_id`, `navigation_styles_id`, `task_sets_id`) VALUES
(1, '1', '', '2', 'na', 'index.php', 'desktop | laptop', '', '', '', '', '0', '0', 'false', 'true', '', 4, 1),
(2, '0', '', '1', 'na', 'index.php', 'desktop | laptop', '', '', '', '', '0', '0', 'false', 'true', '', 3, 1),
(3, '0', '', '1', 'na', 'index.php', 'desktop | laptop', '', '', '', '', '0', '0', 'false', 'true', '', 3, 1),
(4, '0', '', '1', 'na', 'index.php', 'desktop | laptop', '', '', '', '', '0', '0', 'false', 'true', '', 2, 1),
(5, '2 0', '', '2 1', 'na', 'index.php sitemap.php', 'desktop | laptop', '', '', '', '', '0', '0', 'false', 'true', '', 5, 1),
(6, '1 0', '', '3 1', 'na', 'index.php sitemap.php', 'desktop | laptop', '', '', '', '', '0', '0', 'false', 'true', '', 1, 1),
(7, '0', '', '3', 'na', 'index.php', 'desktop | laptop', '', '', '', '', '0', '0', 'false', 'true', '', 4, 1),
(8, '0', '', '1', 'na', 'index.php', 'desktop | laptop', '', '', '', '', '0', '0', 'false', 'true', '', 1, 1),
(9, '0', '', '1', 'na', 'index.php', 'desktop | laptop', '', '', '', '', '0', '0', 'false', 'true', '', 2, 1),
(10, '0', '', '1', 'na', 'index.php', 'desktop | laptop', '', '', '', '', '0', '0', 'false', 'true', '', 5, 1),
(11, '4 5 1', '443|380 515|337 564|486 284|366 976|546 914|536 857|536 791|539 353|28 555|294', '5 27 3', 'na', 'index.php sitemap.php sitemap.php', 'desktop | laptop', '', '', '', '', '0', '0', 'false', 'true', '', 3, 1),
(12, '4 1', '508|436 594|426 293|396 285|392 563|446', '8 7', 'na', 'index.php sitemap.php', 'desktop | laptop', '', '', '', '', '0', '0', 'false', 'true', '', 2, 1),
(13, '3 4 0', 'undefined|undefined undefined|undefined undefined|undefined undefined|undefined undefined|undefined undefined|undefined undefined|undefined ', '4 5 3', 'portrait portrait portrait', 'index.php products.php sitemap.php', 'smartphone', '', '', '', '', '0', '0', 'false', 'true', '', 5, 1),
(14, '4 2', '159|362 145|121 258|121 276|22 216|358 264|159', '8 8', 'portrait portrait', 'index.php contact.php', 'smartphone', '', '', '', '', '0', '0', 'false', 'true', '', 1, 1),
(15, '3 1', '733|333 678|353 297|366 929|444', '8 3', 'na', 'index.php sitemap.php', 'desktop | laptop', '', '', 'na', 'na', '0', '0', 'false', 'true', '', 5, 1),
(16, '1 2 1', '109.677001953125|572.0430297851562 195.69900512695312|593.5479736328125 36.558998107910156|678.4949951171875 364.5159912109375|558.0650024414062', '8 8 8', 'portrait portrait portrait', '# index.php sitemap.php', 'smartphone', '', '', 'na', 'na', '0', '0', 'false', 'true', '', 4, 1),
(17, '1 2 0', '351.6130065917969|20.43000030517578 278.4949951171875|449.4620056152344 78.49500274658203|497.8489990234375 ', '5 7 5', 'portrait portrait portrait', 'index.php sitemap.php products.php#pastries', 'smartphone', '', '', 'right', 'smartphone_1', '0', '0', 'false', 'true', '', 4, 1),
(18, '4 1', '595|405 547|390 61|21 126|164 1311|590', '5 3', 'na', 'index.php products.php', 'desktop | laptop', '', '', 'na', 'na', '0', '0', 'false', 'true', '', 5, 1),
(19, '2 2 1 0', '335|470 49|670 280|503 73|303 45|656 ', '7 3 3 1', 'portrait portrait portrait portrait', 'index.php sitemap.php products.php#pastries products.php', 'smartphone', '', '', 'both', 'smartphone_4', '0', '0', 'false', 'true', '', 5, 1),
(20, '1', '779|464', '4', 'na', 'index.php', 'desktop | laptop', '', '', 'na', 'na', '1600', '767', 'false', 'true', '', 3, 1),
(21, '1 3 0', '363|24 487|309 446|291 76|290 ', '13 13 4', 'portrait portrait portrait', 'index.php sitemap.php products.php#pastries', 'smartphone', '', '', 'right', 'smartphone_1', '736 414 414', '414 736 736', 'false', 'true', '', 1, 1),
(22, '1 1 1', '363|35 256|132 1204|667', '1 2 9', 'na', 'index.php sitemap.php products.php#carrot', 'desktop | laptop', '', '', 'na', 'na', '1600', '767', 'false', 'true', '', 4, 1),
(23, '0', '0|0', '4', 'na', 'index.php', 'desktop | laptop', '', '', 'na', 'na', '1600', '767', 'false', 'true', '', 3, 1),
(24, '4 0', '636|385 800|403 714|416 184|240 0|0', '5 2', 'na', 'index.php sitemap.php', 'desktop | laptop', '', '', 'na', 'na', '1600', '767', 'false', 'true', '', 2, 1),
(25, '3 1', '172|348 59|855 47|837 301|537', '11 3', 'portrait portrait', 'index.php sitemap.php', 'smartphone', '', '', 'right', 'smartphone_1', '414 414', '608 696', 'false', 'true', '', 5, 1),
(26, '0 2 0', '0|0 275|419 62|1082 0|0', '1 4 4', 'portrait portrait portrait', 'index.php index.php products.php', 'smartphone', '', '', 'left', 'smartphone_1', '414 414 414', '608 608 696', 'false', 'true', '', 2, 1),
(27, '2 1 0', '297|410 48|859 78|463 0|0', '5 6 3', 'portrait portrait portrait', 'index.php sitemap.php products.php#cookies', 'smartphone', '', '', 'na', 'na', '414 414 414', '608 696 696', 'false', 'true', '', 3, 1),
(28, '1', '302|399', '5', 'portrait', 'index.php', 'smartphone', '', '', 'both', 'smartphone_4', '414', '640', 'false', 'true', '', 4, 1),
(29, '2 0', '239|501 299|403 0|0', '5 3', 'portrait portrait', 'index.php contact.php', 'smartphone', '', '', 'left', 'smartphone_1', '414 414', '628 716', 'false', 'true', '', 5, 1),
(30, '1 2', '284|33 317|268 829|423', '2 2', 'na', 'index.php contact.php', 'desktop | laptop', '', '', 'na', 'na', '2560', '1300', 'false', 'true', '', 1, 1),
(31, '1', '975|394', '12', 'na', 'index.php', 'desktop | laptop', '', '', 'na', 'na', '1920', '901', 'false', 'true', '', 4, 1),
(32, '2', '436|41', '23', 'portrait', 'index.php', 'tablet', '', '', 'left', 'tablet_1', '1024', '768', 'false', 'true', '', 1, 1),
(33, '7 4', '819|77 997|47 841|33 516|466 549|537', '10 4', 'portrait portrait', 'index.php products.php', 'tablet', '', '', 'both', 'tablet_3', '1024 1024', '768 768', 'false', 'true', '', 3, 1),
(34, '5 3 3 6 0', '580|333 280|401 100|313 287|699 37|28 68|178 0|0', '2 2 2 2 1', 'portrait portrait portrait portrait portrait', 'index.php sitemap.php products.php#brown products.php products.php', 'tablet', '', '', 'left', 'tablet_1', '1024 1024 1024 1024 1024', '768 768 768 768 768', 'false', 'true', '', 2, 1),
(35, '6 3 0', '39|27 550|500 117|331 0|0', '3 1 5', 'portrait portrait portrait', 'index.php sitemap.php products.php#white', 'tablet', 'ios', 'apple safari', 'right', 'tablet_1', '1024 1024 1024', '768 768 768', 'false', 'true', '', 5, 1),
(36, '0', '0|0', '1', 'na', 'index.php', 'desktop | laptop', 'windows', 'google chrome', 'na', 'na', '1125', '786', 'false', 'true', 'asl6', 1, 3),
(37, '0', '0|0', '1', 'na', 'index.php', 'desktop | laptop', 'windows', 'google chrome', 'na', 'na', '1125', '786', 'false', 'true', 'asl6', 3, 2),
(38, '0', '0|0', '1', 'na', 'index.php', 'desktop | laptop', 'windows', 'google chrome', 'na', 'na', '1125', '786', 'false', 'true', 'asl6', 4, 3),
(39, '4', '769|399 720|375', '2', 'na', 'index.php', 'desktop | laptop', 'windows', 'google chrome', 'na', 'na', '1125', '1277', 'false', 'true', 'asl6', 2, 5),
(40, '0', '0|0', '1', 'na', 'index.php', 'desktop | laptop', 'windows', 'google chrome', 'na', 'na', '1125', '1277', 'false', 'true', 'asl6', 4, 4),
(41, '0', '0|0', '1', 'na', 'index.php', 'desktop | laptop', 'windows', 'google chrome', 'na', 'na', '1125', '1277', 'false', 'true', 'asl6', 2, 5),
(42, '0', '0|0', '1', 'na', 'index.php', 'desktop | laptop', 'windows', 'google chrome', 'na', 'na', '1125', '1277', 'false', 'true', 'asl6', 1, 4),
(43, '0 0', '0|0 0|0', '8 17', 'portrait portrait', 'index.php index.php', 'smartphone', 'ios', 'apple safari', 'right', 'smartphone_3', '375 375', '667 667', 'false', 'true', 'asl6', 3, 2),
(44, '0', '0|0', '4', 'portrait', 'index.php', 'smartphone', 'ios', 'apple safari', 'right', 'smartphone_3', '375', '667', 'false', 'true', 'asl6', 1, 2),
(45, '8', '158|426 167|371 207|264 176|220', '5', 'portrait', 'index.php', 'smartphone', 'ios', 'apple safari', 'right', 'smartphone_3', '375', '667', 'false', 'true', 'asl6', 2, 5),
(46, '7 0', '143|420 108|161 298|40 0|0', '5 2', 'portrait portrait', 'index.php about.php', 'smartphone', 'ios', 'apple safari', 'right', 'smartphone_3', '375 375', '667 667', 'false', 'true', 'asl6', 3, 4),
(47, '2', '220|472', '3', 'portrait', 'index.php', 'smartphone', 'ios', 'apple safari', 'right', 'smartphone_3', '375', '667', 'false', 'true', 'asl6', 2, 3),
(48, '0', '0|0', '2', 'portrait', 'index.php', 'smartphone', 'ios', 'apple safari', 'right', 'smartphone_3', '375', '667', 'false', 'true', 'asl6', 1, 4),
(49, '4', '168|322 175|305', '9', 'portrait', 'index.php', 'smartphone', 'ios', 'apple safari', 'right', 'smartphone_2', '375', '667', 'false', 'true', 'asl6', 4, 2),
(50, '2', '334|324', '6', 'portrait', 'index.php', 'smartphone', 'ios', 'apple safari', 'right', 'smartphone_3', '414', '608', 'false', 'true', 'asl6', 5, 5),
(51, '5 2', '251|338 311|428 0|0 347|312', '4 6', 'portrait portrait', 'index.php contact.php', 'smartphone', 'ios', 'apple safari', 'right', 'smartphone_3', '414 414', '608 608', 'false', 'true', 'asl6', 3, 3),
(52, '2', '192|391', '4', 'portrait', 'index.php', 'smartphone', 'ios', 'apple safari', 'right', 'smartphone_1', '375', '667', 'false', 'true', 'asl6', 2, 3),
(53, '8 2', '248|483 330|596 307|596 307|596', '8 7', 'portrait portrait', 'index.php index.php', 'smartphone', 'ios', 'apple safari', 'right', 'smartphone_3', '375 375', '667 667', 'false', 'true', 'asl6', 4, 4),
(54, '2', '52|207', '3', 'portrait', 'index.php', 'smartphone', 'ios', 'apple safari', 'right', 'smartphone_3', '375', '667', 'false', 'true', 'asl6', 1, 5),
(55, '2 0', '62|28 530|400 0|0', '7 21', 'portrait portrait', 'index.php sitemap.php', 'tablet', 'ios', 'apple safari', 'both', 'tablet_3', '1024 1024', '768 768', 'false', 'true', 'asl6', 5, 2),
(56, '4 2', '28|35 673|279 522|484', '5 5', 'na', 'index.php products.php', 'desktop | laptop', 'windows', 'google chrome', 'na', 'na', '1319', '786', 'false', 'true', 'asl6', 5, 5),
(57, '6 6 0', '676|380 486|276 673|362 526|216 0|0', '14 12 2', 'portrait portrait portrait', 'index.php about.php products.php', 'smartphone', 'ios', 'apple safari', 'both', 'smartphone_4', '736 736 736', '414 414 414', 'false', 'true', 'asl6', 4, 3),
(58, '6 2', '349|326 620|45 408|292', '3 1', 'portrait portrait', 'index.php about.php', 'smartphone', 'ios', 'apple safari', 'both', 'smartphone_4', '736 736', '414 414', 'false', 'true', 'asl6', 3, 2),
(59, '5 2', '424|305 586|153 440|214', '7 4', 'portrait portrait', 'index.php contact.php', 'smartphone', 'ios', 'apple safari', 'both', 'smartphone_4', '736 736', '414 414', 'false', 'true', 'asl6', 2, 4),
(60, '0 2 1', '0|0 30|275 156|89 82|223', '0 5 4', 'portrait portrait portrait', 'index.php index.php sitemap.php', 'smartphone', 'ios', 'apple safari', 'both', 'smartphone_4', '736 736 736', '414 414 414', 'false', 'true', 'asl6', 1, 4),
(61, '3', '206|322 262|386', '6 14', 'portrait portrait', 'index.php', 'smartphone', 'ios', 'apple safari', 'both', 'smartphone_4', '736 736', '414 414', 'false', 'true', 'asl6', 1, 2),
(62, '7 2', '322|56 199|443 58|872 327|378', '10 5', 'portrait portrait', 'index.php products.php', 'smartphone', 'ios', 'apple safari', 'right', 'smartphone_3', '414 414', '628 628', 'false', 'true', 'asl6', 3, 2),
(63, '6', '315|348 66|26 69|19', '8', 'portrait', 'index.php', 'smartphone', 'ios', 'apple safari', 'right', 'smartphone_3', '414', '628', 'false', 'true', 'asl6', 5, 5),
(64, '4', '313|449 180|425', '2', 'portrait', 'index.php', 'smartphone', 'ios', 'apple safari', 'right', 'smartphone_3', '414', '628', 'false', 'true', 'asl6', 4, 5),
(65, '4 1', '0|0 0|0', '14 5', 'portrait portrait', 'index.php products.php', 'smartphone', 'ios', 'apple safari', 'both', 'smartphone_4', '736 736', '414 414', 'false', 'true', 'asl6', 2, 3),
(66, '2', '0|0', '9', 'portrait', 'index.php', 'smartphone', 'ios', 'apple safari', 'both', 'smartphone_4', '736', '414', 'false', 'true', 'asl6', 2, 3),
(67, '3', '544|186 658|114', '23', 'portrait', 'index.php', 'smartphone', 'ios', 'apple safari', 'both', 'smartphone_4', '736', '414', 'false', 'true', 'asl6', 4, 4),
(68, '2', '521|191 645|345', '1', 'portrait', 'index.php', 'smartphone', 'ios', 'apple safari', 'na', 'na', '736', '414', 'false', 'true', 'asl6', 3, 3),
(69, '2', '533|190 625|345', '1', 'portrait', 'index.php', 'smartphone', 'ios', 'apple safari', 'both', 'smartphone_4', '736', '414', 'false', 'true', 'asl6', 5, 2),
(70, '3', '531|187 NaN|NaN 636|350', '3', 'portrait', 'index.php', 'smartphone', 'ios', 'apple safari', 'both', 'smartphone_4', '736', '414', 'false', 'true', 'asl6', 1, 4),
(71, '2 1', '542|183 NaN|NaN 637|342', '11 7', 'portrait portrait', 'index.php sitemap.php', 'smartphone', 'ios', 'apple safari', 'both', 'smartphone_4', '736 736', '414 414', 'false', 'true', 'asl6', 2, 5),
(72, '2', '370|214', '9', 'portrait', 'index.php', 'smartphone', 'ios', 'apple safari', 'both', 'smartphone_4', '736', '414', 'false', 'true', 'asl6', 4, 3),
(73, '5 3 0', '156|546 169|608 129|358 0|0', '4 2 2', 'portrait portrait portrait', 'index.php products.php sitemap.php', 'smartphone', 'ios', 'apple safari', 'right', 'smartphone_3', '414 414 414', '628 716 716', 'false', 'true', 'asl6', 1, 2),
(74, '0', '0|0', '1', 'portrait', 'index.php', 'smartphone', 'ios', 'apple safari', 'na', 'na', '414', '628', 'false', 'true', 'asl6', 3, 4),
(75, '0', '0|0', '1', 'portrait', 'index.php', 'smartphone', 'ios', 'apple safari', 'na', 'na', '414', '628', 'false', 'true', 'asl6', 5, 4),
(76, '4 1', '0|0 0|0', '10 4', 'portrait portrait', 'index.php products.php', 'smartphone', 'ios', 'apple safari', 'na', 'na', '414 414', '628 628', 'false', 'true', 'asl6', 2, 2),
(77, '0', '0|0', '1', 'portrait', 'index.php', 'smartphone', 'ios', 'apple safari', 'na', 'na', '414', '628', 'false', 'true', 'asl6', 1, 5),
(78, '0', '0|0', '18', 'portrait', 'index.php', 'smartphone', 'ios', 'apple safari', 'right', 'smartphone_3', '414', '628', 'false', 'true', 'asl6', 3, 4),
(79, '0', '0|0', '1', 'portrait', 'index.php', 'smartphone', 'ios', 'apple safari', 'right', 'smartphone_3', '414', '628', 'false', 'true', 'asl6', 5, 2),
(80, '0', '0|0', '1', 'portrait', 'index.php', 'smartphone', 'ios', 'apple safari', 'na', 'na', '414', '628', 'false', 'true', 'asl6', 4, 4),
(81, '2 1', '0|0 0|0', '5 5', 'portrait portrait', 'index.php contact.php', 'smartphone', 'ios', 'apple safari', 'right', 'smartphone_3', '414 414', '628 628', 'false', 'true', 'asl6', 2, 2),
(82, '0', '0|0', '1', 'portrait', 'index.php', 'smartphone', 'ios', 'apple safari', 'na', 'na', '414', '628', 'false', 'true', 'asl6', 1, 3),
(83, '0', '0|0', '1', 'portrait', 'index.php', 'smartphone', 'ios', 'apple safari', 'right', 'smartphone_3', '414', '628', 'false', 'true', 'asl6', 5, 5),
(84, '0', '0|0', '1', 'portrait', 'index.php', 'smartphone', 'ios', 'apple safari', 'right', 'smartphone_3', '414', '628', 'false', 'true', 'asl6', 3, 3),
(85, '0', '0|0', '1', 'portrait', 'index.php', 'smartphone', 'ios', 'apple safari', 'na', 'na', '414', '628', 'false', 'true', 'asl6', 4, 5),
(86, '2 2', '283|347 300|386 0|0', '3 9', 'portrait portrait', 'index.php contact.php', 'smartphone', 'ios', 'apple safari', 'na', 'na', '414 414', '628 628', 'false', 'true', 'asl6', 2, 2),
(87, '0', '0|0', '1', 'portrait', 'index.php', 'smartphone', 'ios', 'apple safari', 'na', 'na', '414', '628', 'false', 'true', 'asl6', 1, 3),
(88, '0', '0|0', '4', 'portrait', 'index.php', 'smartphone', 'ios', 'apple safari', 'right', 'smartphone_3', '414', '628', 'false', 'true', 'asl6', 5, 1),
(89, '0', '0|0', '1', 'portrait', 'index.php', 'smartphone', 'ios', 'apple safari', 'na', 'na', '414', '628', 'false', 'true', 'asl6', 4, 1),
(90, '0', '0|0', '1', 'portrait', 'index.php', 'smartphone', 'ios', 'apple safari', 'right', 'smartphone_3', '414', '628', 'false', 'true', 'asl6', 3, 4),
(91, '2 1', '0|0 0|0', '4 7', 'portrait portrait', 'index.php contact.php', 'smartphone', 'ios', 'apple safari', 'right', 'smartphone_3', '414 414', '628 628', 'false', 'true', 'asl6', 2, 5),
(92, '4 0', '307|347 232|322 84|31 234|284 0|0', '5 1', 'portrait portrait', 'index.php products.php', 'smartphone', 'ios', 'apple safari', 'right', 'smartphone_1', '414 414', '660 660', 'false', 'true', 'asl6', 5, 2),
(93, '5 0', '295|47 312|56 239|347 256|480 198|43 0|0', '8 4', 'portrait portrait', 'index.php sitemap.php', 'smartphone', 'ios', 'apple safari', 'right', 'smartphone_1', '414 414', '660 660', 'false', 'true', 'asl6', 3, 3),
(94, '3 2', '149|532 164|1105 60|1206 0|0', '5 6', 'portrait portrait', 'index.php products.php', 'smartphone', 'ios', 'apple safari', 'right', 'smartphone_3', '414 414', '628 628', 'false', 'true', 'asl6', 1, 1),
(95, '4 4', '137|405 321|453 243|478 167|194 0|0', '6 4', 'portrait portrait', 'index.php products.php', 'smartphone', 'ios', 'apple safari', 'right', 'smartphone_3', '414 414', '628 628', 'false', 'true', 'asl6', 4, 4),
(96, '2 3', '347|62 214|48 0|0', '2 4', 'portrait portrait', 'index.php products.php', 'smartphone', 'ios', 'apple safari', 'right', 'smartphone_3', '414 414', '628 628', 'false', 'true', 'asl6', 3, 5),
(97, '3 3', '169|504 211|547 166|187 197|500 183|723 174|913', '4 2', 'portrait portrait', 'index.php products.php', 'smartphone', 'ios', 'apple safari', 'right', 'smartphone_3', '414 414', '628 628', 'false', 'true', 'asl6', 4, 2),
(98, '3 3', '0|0 0|0', '5 6', 'portrait portrait', 'index.php products.php', 'smartphone', 'ios', 'apple safari', 'right', 'smartphone_3', '414 414', '628 628', 'false', 'true', 'asl6', 1, 1),
(99, '2 0', '150|452 305|392 0|0', '3 0', 'portrait portrait', 'index.php contact.php', 'smartphone', 'ios', 'apple safari', 'right', 'smartphone_3', '414 414', '628 628', 'false', 'true', 'asl6', 2, 2),
(100, '2 1', '147|467 54|814 203|383', '3 2', 'portrait portrait', 'index.php products.php', 'smartphone', 'ios', 'apple safari', 'na', 'na', '414 414', '628 628', 'false', 'true', 'asl6', 5, 1),
(101, '3 3', '0|0 0|0', '3 3', 'portrait portrait', 'index.php products.php', 'smartphone', 'ios', 'apple safari', 'right', 'smartphone_3', '414 414', '628 716', 'false', 'true', 'asl6', 1, 3),
(102, '2 1', '365|49 220|51 170|349', '2 1', 'portrait portrait', 'index.php products.php', 'smartphone', 'ios', 'apple safari', 'right', 'smartphone_3', '414 414', '628 628', 'false', 'true', 'asl6', 3, 2),
(103, '5 1 0', '0|0 0|0 0|0', '12 2 3', 'na', 'index.php reportsindex.php reportstopicaccessibilityindex.php', 'desktop | laptop', 'windows', 'google chrome', 'na', 'na', '2560', '1298', 'false', 'true', 'asl6', 4, 21),
(104, '0 2 1 1 0', '0|0 0|0 0|0 0|0 0|0', '2 4 3 5 1', 'na', 'index.php index.php reportsindex.php reportstopiceyetrackingindex.php reportshowpeoplereadwebeyetrackingevidenceindex.php', 'desktop | laptop', 'windows', 'google chrome', 'na', 'na', '1188', '757', 'false', 'true', 'asl6', 5, 19),
(105, '0', '0|0', '2', 'portrait', 'index.php', 'smartphone', 'ios', 'apple safari', 'right', 'smartphone_3', '414', '628', 'false', 'true', 'asl6', 2, 3),
(106, '10 0', '186|376 164|661 153|981 159|1500 234|2045 231|1623 223|1187 210|672 73|336 174|411 0|0', '7 1', 'portrait portrait', 'index.php uxconferenceindex.php', 'smartphone', 'ios', 'apple safari', 'right', 'smartphone_2', '414 414', '628 628', 'false', 'true', 'asl6', 1, 4),
(107, '3 0', '83|19 231|308 225|372 0|0', '5 4', 'portrait portrait', 'index.php trainingindex.php', 'smartphone', 'ios', 'apple safari', 'right', 'smartphone_3', '414 414', '628 628', 'false', 'true', 'asl6', 5, 4),
(108, '3 0', '0|0 0|0', '3 1', 'portrait portrait', 'index.php consultingindex.php', 'smartphone', 'ios', 'apple safari', 'right', 'smartphone_3', '414 414', '628 628', 'false', 'true', 'asl6', 3, 3),
(109, '4 0', '346|66 197|253 180|522 253|346 0|0', '6 1', 'portrait portrait', 'index.php consultingindex.php', 'smartphone', 'ios', 'apple safari', 'right', 'smartphone_3', '414 414', '628 628', 'false', 'true', 'asl6', 4, 1),
(110, '2 1', '81|22 120|350 171|500', '4 3', 'portrait portrait', 'index.php aboutindex.php', 'smartphone', 'ios', 'apple safari', 'right', 'smartphone_3', '414 414', '628 628', 'false', 'true', 'asl6', 2, 5),
(111, '1', '146|154', '2 7 105', 'portrait portrait portrait', 'index.php', 'smartphone', 'ios', 'apple safari', 'right', 'smartphone_3', '414 414 414', '736 736 736', 'false', 'true', 'asl6', 1, 27),
(112, '4', '261|32 176|44 319|61 305|342', '0 6 33 2', 'portrait portrait portrait portrait', 'index.php', 'smartphone', 'ios', 'apple safari', 'right', 'smartphone_3', '414 414 414 414', '736 736 736 736', 'false', 'true', 'asl6', 3, 10),
(113, '2 1 2 0', '133|565 118|450 128|336 39|30 201|384 0|0', '2 7 5 1', 'portrait portrait portrait portrait', 'index.php index.php# traininglondonindex.php consultingindex.php', 'smartphone', 'ios', 'apple safari', 'right', 'smartphone_3', '414 414 414 414', '736 736 736 736', 'false', 'true', 'asl6', 5, 22),
(114, '0 3', '0|0 138|482 159|635 176|623', '0 6', 'portrait portrait', '/index.php /index.php', 'smartphone', 'ios', 'apple safari', 'right', 'smartphone_3', '414 414', '628 628', 'false', 'true', 'asl6', 2, 2),
(115, '2 1 0', '353|73 248|347 90|394 0|0', '4 2 1', 'portrait portrait portrait', '/index.php /consulting/index.php /consulting/usabilitytesting/index.php', 'smartphone', 'ios', 'apple safari', 'right', 'smartphone_3', '414 414 414', '628 628 628', 'false', 'true', 'asl6', 4, 3),
(116, '2 2 2 1', '32|27 537|366 132|514 70|935 115|623 857|859 128|472', '6 6 4 3', 'landscape landscape landscape landscape', '/index.php /training/index.php /uxcertification/specialties/index.php /about/contact/index.php', 'tablet', 'ios', 'apple safari', 'both', 'tablet_3', '664 664 748 664', '1024 1024 1024 1024', 'false', 'true', 'asl6', 5, 1),
(117, '2 1 3 1', '360|63 115|239 83|204 114|464 125|852 113|1207 223|423', '5 1 4 2', 'portrait portrait portrait portrait', '/index.php /articles/index.php /articles/author/jakob-nielsen/index.php /articles/computer-skill-levels/index.php', 'smartphone', 'ios', 'apple safari', 'right', 'smartphone_3', '414 414 414 414', '628 628 628 716', 'false', 'true', 'asl6', 4, 2),
(118, '2 4 6 5', '283|60 200|56 287|367 278|694 305|1118 184|1355 257|447 278|841 316|1249 329|1512 294|1825 199|2076 257|433 239|809 279|1319 293|1839 303|2215', '4 10 17 10', 'portrait portrait portrait portrait', '/index.php /training/index.php /in-house-training/index.php /courses/facilitating-ux-workshops/index.php', 'smartphone', 'ios', 'apple safari', 'right', 'smartphone_3', '414 414 414 414', '628 628 716 716', 'false', 'true', 'u5ba', 3, 24),
(119, '2 4 4 5', '70|33 101|140 280|259 360|526 349|712 243|782 397|260 374|396 424|528 448|595 335|289 353|596 369|716 312|268 83|34', '7 14 7 12', 'landscape landscape landscape landscape', '/index.php /reports/index.php /reports/topic/user-testing/index.php /reports/how-to-recruit-participants-usability-studies/index.php', 'smartphone', 'ios', 'apple safari', 'right', 'smartphone_3', '370 370 414 414', '736 736 736 736', 'false', 'true', 'u5ba', 2, 15),
(120, '1 2 2 2', '435|44 382|522 336|1198 633|598 576|695 506|628 556|1567', '4 6 6 18', 'landscape landscape landscape landscape', '/index.php /reports/index.php /reports/topic/social-media/index.php /reports/social-media-user-experience/index.php', 'tablet', 'ios', 'apple safari', 'right', 'tablet_1', '664 664 748 748', '1024 1024 1024 1024', 'false', 'true', 'u5ba', 1, 4),
(121, '23 13 2 8', '342|471 339|681 345|842 337|1006 336|1140 341|832 343|668 346|514 334|377 344|597 341|759 341|912 354|1053 335|1101 341|1316 340|1485 329|1680 337|1341 356|1070 337|826 322|504 340|336 387|115 327|455 340|662 333|868 338|1103 345|1271 332|1471 317|1680 341|1684 317|1447 306|1313 281|1202 308|1136 175|1102 316|441 56|431 324|512 343|719 341|878 312|1026 319|1156 356|1369 342|1574 325|1774', '33 18 3 7', 'portrait portrait portrait portrait', '/index.php /training/index.php /ux-certification/ux-master-certification/index.php /ux-certification/pricing/index.php', 'smartphone', 'ios', 'apple safari', 'right', 'smartphone_1', '414 414 414 414', '628 628 628 716', 'false', 'false', 'z6qa', 4, 12),
(122, '12 13', '662|215 634|420 641|574 654|785 658|909 654|1088 644|1338 643|1544 642|1674 634|1725 650|1686 666|1572 647|250 660|427 647|543 646|667 650|791 658|939 657|1051 677|751 676|656 681|593 655|479 172|538 159|546', '25 23', 'landscape landscape', '/index.php /about/contact/index.php', 'smartphone', 'ios', 'apple safari', 'both', 'smartphone_4', '370 414', '736 736', 'false', 'false', 'z6qa', 5, 17),
(123, '9 7 1 4 10 7 3 2 7 2 2 4', '550|616 482|766 513|1407 610|1139 600|614 640|58 581|28 690|519 673|539 508|703 343|1143 467|1297 407|600 388|58 178|51 126|29 437|488 511|589 520|1121 10|35 153|58 557|643 505|975 527|1496 606|1051 549|202 625|65 613|58 608|6 620|54 634|50 604|617 627|1118 544|197 377|77 377|64 357|26 406|68 554|36 323|716 487|1048 154|38 255|144 197|606 420|693 613|1118 147|2045 560|1404 566|615 148|65 136|35 506|604 641|439 455|802 145|645 536|724 535|1313 601|1164 406|1366', '31 17 7 8 19 12 6 6 11 3 7 8', 'portrait portrait portrait portrait portrait portrait portrait portrait portrait portrait portrait portrait', '/index.php /consulting/index.php /index.php /people/index.php /index.php /about/index.php /reports/index.php /index.php /reports/best-merged-intranets/index.php /index.php /consulting/index.php /consulting/expert-design-review/index.php', 'tablet', 'ios', 'apple safari', 'right', 'tablet_1', '768 768 768 768 768 768 768 768 768 768 768 768', '920 920 920 920 920 920 920 920 920 920 1004 1004', 'false', 'false', 'z6qa', 3, 5),
(124, '5 8', '546|33 610|394 699|573 465|41 458|21 562|428 652|833 667|1063 622|1376 771|1708 740|761 480|1068 954|1761', '13 20', 'landscape landscape', '/index.php /reports/index.php', 'tablet', 'ios', 'apple safari', 'right', 'tablet_1', '664 664', '1024 1024', 'false', 'false', 'z6qa', 1, 2),
(125, '6 68', '82|475 100|691 87|872 101|1048 121|756 75|483 97|531 111|762 113|955 103|1122 110|1315 75|1500 98|1659 84|1722 83|1953 70|2133 79|2347 90|2532 89|2696 125|2909 127|3038 14|3046 127|3325 114|3509 113|3822 130|4260 131|4437 136|4640 133|4811 125|5016 100|5144 125|5445 131|5706 130|5952 129|6160 117|6437 112|6746 128|6988 129|7243 129|7485 133|7680 136|7826 76|8094 104|8517 96|8823 110|9188 124|9470 121|9839 123|10249 110|10607 120|10862 110|11051 130|11354 125|11650 135|11813 146|12092 127|12355 126|12663 134|13008 127|13237 124|13654 127|14035 116|14332 109|14789 110|15013 117|15455 126|15632 129|15848 134|16262 126|16465 117|17007 130|17173 135|17348 115|17672', '18 130', 'portrait portrait', '/index.php /articles/seamless-cross-channel/index.php', 'smartphone', 'ios', 'apple safari', 'both', 'smartphone_4', '414 414', '628 628', 'false', 'false', 'hgkk', 2, 16),
(126, '21 10 4', '209|295 131|443 192|575 139|713 163|909 97|939 154|634 156|328 169|489 217|513 219|842 185|1149 174|1380 168|1592 133|1103 154|661 165|344 354|107 682|52 347|321 500|291 113|211 144|335 156|523 234|684 217|844 205|999 204|1210 217|1439 118|1593 14|1269 327|109 281|291 199|448 171|612', '29 15 10', 'landscape landscape landscape', '/index.php /training/index.php /index.php', 'smartphone', 'ios', 'apple safari', 'left', 'smartphone_3', '370 370 370', '736 736 736', 'false', 'false', 'hgkk', 4, 14),
(127, '1 7 2 6', '339|390 130|578 253|946 289|1168 92|1028 177|802 495|587 1|494 181|690 334|416 360|736 600|777 580|493 615|782 615|782 17|1042', '4 27 14 13', 'portrait portrait portrait portrait', '/index.php /training/index.php /index.php /training/index.php', 'tablet', 'ios', 'apple safari', 'left', 'tablet_1', '768 768 768 768', '920 920 1004 920', 'false', 'false', 'hgkk', 3, 27),
(128, '1 5 5', '770|101 158|409 156|793 133|902 139|888 54|1041 244|494 235|969 206|1507 250|1804 241|2081', '9 7 13', 'landscape landscape landscape', '/index.php /about/index.php /about/history/index.php', 'tablet', 'ios', 'apple safari', 'left', 'tablet_1', '664 664 748', '1024 1024 1024', 'false', 'false', 'hgkk', 1, 18),
(129, '12', '0|0', '41', 'portrait', '/index.php', 'smartphone', 'ios', 'apple safari', 'right', 'smartphone_3', '414', '628', 'false', 'true', 'tkmo', 5, 11),
(130, '5', '0|0', '9', 'portrait', '/index.php', 'smartphone', 'ios', 'apple safari', 'right', 'smartphone_3', '414', '628', 'false', 'true', '3ng9', 2, 8),
(131, '9', '0|0', '7', 'portrait', '/index.php', 'smartphone', 'ios', 'apple safari', 'right', 'smartphone_3', '414', '628', 'false', 'true', '3ng9', 4, 25),
(132, '8', '0|0', '13', 'portrait', '/index.php', 'smartphone', 'ios', 'apple safari', 'right', 'smartphone_3', '414', '736', 'false', 'true', '3ng9', 2, 26),
(133, '10 0', '193|495 184|500 180|493 197|413 228|264 225|222 213|406 214|365 219|402 207|401 0|0', '16 3', 'portrait portrait', '/index.php /reports/index.php', 'smartphone', 'ios', 'apple safari', 'right', 'smartphone_3', '414 414', '736 736', 'false', 'true', '3ng9', 5, 30),
(134, '2 0', '178|500 71|445 0|0', '5 1', 'portrait portrait', '/index.php /articles/index.php', 'smartphone', 'ios', 'apple safari', 'right', 'smartphone_3', '414 414', '628 716', 'false', 'true', '3ng9', 2, 9),
(135, '6', '0|0', '8', 'portrait', '/index.php', 'smartphone', 'ios', 'apple safari', 'right', 'smartphone_3', '414', '628', 'false', 'true', '3ng9', 2, 20),
(136, '4', '320|437 313|761 278|942 293|468', '4', 'portrait', '/index.php', 'smartphone', 'ios', 'apple safari', 'right', 'smartphone_3', '414', '628', 'false', 'true', '3ng9', 5, 29),
(137, '10', '147|296 103|820 118|986 123|630 111|520 99|814 105|1033 120|590 110|400 122|567', '8', 'landscape', '/index.php', 'smartphone', 'ios', 'apple safari', 'right', 'smartphone_3', '370', '736', 'false', 'true', '3ng9', 2, 13),
(138, '0 10', '0|0 431|503 510|1062 379|498 667|1143 504|755 592|881 518|970 603|935 522|619 592|792', '0 10', 'landscape landscape', '/index.php /index.php', 'tablet', 'ios', 'apple safari', 'right', 'tablet_1', '664 664', '1024 1024', 'false', 'true', '3ng9', 2, 19),
(139, '3 3 2 0', '27|23 425|459 73|296 177|476 387|690 54|313 446|636 259|966 0|0', '5 5 10 2', 'portrait portrait portrait portrait', '/index.php /consulting/index.php /consulting/usability-testing/index.php /articles/why-you-only-need-to-test-with-5-users/index.php', 'tablet', 'ios', 'apple safari', 'both', 'tablet_3', '768 768 768 768', '920 1004 920 920', 'false', 'true', '3ng9', 2, 11),
(140, '3', '374|382 518|650 75|30', '5', 'landscape', '/index.php', 'tablet', 'ios', 'apple safari', 'right', 'tablet_1', '664', '1024', 'false', 'true', '3ng9', 2, 25),
(141, '2', '67|25 294|413', '7', 'landscape', '/index.php', 'tablet', 'ios', 'apple safari', 'right', 'tablet_1', '664', '1024', 'false', 'true', '3ng9', 5, 22),
(142, '2', '84|26 487|424', '7', 'landscape', '/index.php', 'tablet', 'ios', 'apple safari', 'na', 'na', '664', '1024', 'false', 'true', '3ng9', 2, 18),
(143, '2', '497|459 654|1047', '4', 'landscape', '/index.php', 'tablet', 'ios', 'apple safari', 'na', 'na', '664', '1024', 'false', 'true', '3ng9', 5, 4),
(144, '3', '630|439 761|1078 890|431', '6', 'landscape', '/index.php', 'tablet', 'ios', 'apple safari', 'both', 'tablet_3', '664', '1024', 'false', 'true', '3ng9', 5, 5),
(145, '3', '728|481 914|1173 908|716', '6', 'landscape', '/index.php', 'tablet', 'ios', 'apple safari', 'na', 'na', '664', '1024', 'false', 'true', '3ng9', 2, 2),
(146, '5', '597|458 680|1044 629|503 702|1015 728|396', '9', 'landscape', '/index.php', 'tablet', 'ios', 'apple safari', 'na', 'na', '664', '1024', 'false', 'true', '3ng9', 5, 13),
(147, '4', '460|523 630|823 552|451 612|1035', '7', 'portrait', '/index.php', 'tablet', 'ios', 'apple safari', 'na', 'na', '1024', '664', 'false', 'true', '3ng9', 5, 21),
(148, '6', '447|525 620|1091 558|532 620|1146 546|399 582|908', '11', 'portrait', '/index.php', 'tablet', 'ios', 'apple safari', 'na', 'na', '1024', '664', 'false', 'true', '3ng9', 2, 26),
(149, '3', '477|488 590|1060 560|438', '5', 'portrait', '/index.php', 'tablet', 'ios', 'apple safari', 'both', 'tablet_3', '1024', '664', 'false', 'true', '3ng9', 5, 10),
(150, '8 3 0', '709|385 811|648 744|379 732|667 722|429 749|453 35|26 530|433 589|469 441|1027 60|316 0|0', '13 6 6', 'landscape portrait landscape', '/index.php /consulting/index.php /consulting/usability-testing/index.php', 'tablet', 'ios', 'apple safari', 'right', 'tablet_1', '664 1024 920', '1024 748 768', 'false', 'true', '3ng9', 5, 16),
(151, '5 3 3 1', '494|497 598|1001 494|462 591|978 158|332 95|322 31|20 362|287 479|444 425|633 448|620 552|578', '10 4 7 3', 'landscape portrait landscape landscape', '/index.php /reports/index.php /articles/index.php /articles/flat-design-best-practices/index.php', 'tablet', 'ios', 'apple safari', 'right', 'tablet_1', '664 1024 920 664', '1024 664 768 1024', 'false', 'true', '3ng9', 5, 7),
(152, '2 2 2 1', '40|19 508|273 31|292 39|419 439|527 523|711 579|422', '5 6 5 5', 'landscape portrait portrait landscape', '/index.php /articles/index.php /articles/author/jakob-nielsen/index.php /articles/computer-skill-levels/index.php', 'tablet', 'ios', 'apple safari', 'both', 'tablet_4', '0 undefined undefined 1024', '0 undefined undefined 664', 'false', 'true', '3ng9', 5, 20),
(153, '4 2', '565|400 669|561 30|26 71|194 748|310 465|1085', '12 6', 'landscape portrait', '/index.php /articles/index.php', 'tablet', 'ios', 'apple safari', 'left', 'tablet_1', '1024 1024', '664 748', 'false', 'true', '3ng9', 2, 12),
(154, '4 4', '377|586 493|680 230|758 348|632 361|460 462|280 328|160 311|465 945|2006', '8 12', 'portrait landscape', '/index.php /online-seminars/index.php', 'tablet', 'ios', 'apple safari', 'right', 'tablet_1', '768 920', '920 768', 'false', 'true', '3ng9', 2, 9),
(155, '2 3 1', '537|471 90|1166 97|542 527|1085 508|1220 424|632', '6 16 4', 'landscape portrait portrait', '/index.php /news/index.php ', 'tablet', 'ios', 'apple safari', 'right', 'tablet_1', '664 1024 768', '1024 748 920', 'false', 'true', '3ng9', 2, 21),
(156, '2 2', '22|20 85|347 79|420 69|965', '7 5', 'portrait landscape', '/index.php /about/index.php', 'tablet', 'ios', 'apple safari', 'right', 'tablet_1', '0 1024', '0 748', 'false', 'true', '3ng9', 2, 13),
(157, '3 1', '488|470 555|920 425|420 362|677', '7 3', 'portrait portrait', '/index.php /people/index.php', 'tablet', 'ios', 'apple safari', 'right', 'tablet_1', '0 undefined undefined 768', '0 undefined undefined 920', 'false', 'true', '3ng9', 2, 19),
(158, '0 2 1', '0|0 50|31 85|247 409|496', '0 5 5', 'portrait portrait portrait', '/index.php /index.php /training/index.php', 'tablet', 'ios', 'apple safari', 'right', 'tablet_1', '0 undefined 768', '0 undefined 1024', 'false', 'true', '3ng9', 2, 28),
(159, '2 1', '430|602 58|1212 114|429', '6 8', 'portrait landscape', '/index.php /articles/index.php', 'tablet', 'ios', 'apple safari', 'right', 'tablet_1', '768 768', '920 920', 'false', 'true', '3ng9', 2, 7),
(160, '2 1', '434|591 87|1057 649|575', '8 8', 'portrait portrait', '/index.php /training/austin/index.php', 'tablet', 'ios', 'apple safari', 'left', 'tablet_1', '768 768', '920 920', 'false', 'true', '3ng9', 5, 2),
(161, '2 2 2', '32|26 62|289 454|410 110|576 434|599 586|1230', '7 4 5', 'landscape portrait landscape', '/index.php /consulting/index.php /consulting/expert-design-review/index.php', 'tablet', 'ios', 'apple safari', 'both', 'tablet_4', '920 768 920', '768 920 768', 'false', 'true', '3ng9', 2, 19),
(162, '2 6 7 1', '45|31 220|373 407|274 412|526 396|645 370|995 425|1243 201|1360 466|176 384|571 387|791 385|1075 386|1347 338|1643 136|1689 311|397', '7 10 11 2', 'portrait landscape landscape portrait', '/index.php /training/index.php /in-house-training/index.php /courses/lean-ux-and-agile/index.php', 'smartphone', 'ios', 'apple safari', 'right', 'smartphone_3', '414 628 628 414', '628 414 414 628', 'false', 'true', '3ng9', 5, 10),
(163, '2 6 1 3', '55|20 206|228 295|368 320|658 306|868 73|1389 157|1336 82|1404 323|312 317|381 316|640 309|792', '3 14 3 3', 'portrait portrait portrait portrait', '/index.php /reports/index.php /reports/topic/eyetracking/index.php /reports/how-to-conduct-eyetracking-studies/index.php', 'smartphone', 'ios', 'apple safari', 'right', 'smartphone_3', '414 414 414 414', '628 628 628 628', 'false', 'true', '3ng9', 5, 20),
(164, '2 4 5 9', '60|30 109|236 306|291 334|606 353|957 193|1349 351|346 333|706 317|983 402|1312 130|1485 331|337 346|732 380|1151 386|1255 346|1435 328|1623 319|2003 330|2325 298|2508', '4 5 6 17', 'landscape landscape landscape landscape', '/index.php /training/index.php /in-house-training/index.php /courses/ux-deliverables/index.php', 'smartphone', 'ios', 'apple safari', 'right', 'smartphone_2', '370 370 370 370', '736 736 736 736', 'false', 'true', '3ng9', 2, 23),
(165, '2 2 5 1', '43|23 363|360 672|731 661|716 196|489 217|1011 237|1634 196|2674 203|3140 173|656', '3 4 9 4', 'portrait portrait portrait portrait', '/index.php /training/index.php /online-seminars/index.php /online-seminars/every-word-count/index.php', 'tablet', 'ios', 'apple safari', 'both', 'tablet_3', '768 768 768 768', '920 920 920 920', 'false', 'true', '3ng9', 5, 27),
(166, '3 2 1 2', '434|471 15|33 83|248 190|468 156|966 50|404 532|543 802|1172', '9 4 3 5', 'landscape landscape landscape landscape', '/index.php /training/index.php /ux-certification/ux-master-certification/index.php /ux-certification/pricing/index.php', 'tablet', 'ios', 'apple safari', 'right', 'tablet_1', '664 664 664 664', '1024 1024 1024 1024', 'false', 'true', '3ng9', 2, 12),
(167, '13', '351|358 347|506 345|679 338|862 338|1003 346|1168 340|1302 336|1507 340|1598 334|1710 350|1513 353|1291 348|1072', '101', 'portrait', '/index.php', 'smartphone', 'ios', 'apple safari', 'right', 'smartphone_2', '414', '628', 'false', 'false', 'nz6a', 2, 6),
(168, '6 3', '676|74 648|172 662|229 618|371 627|482 366|499 658|116 641|193 655|272', '23 14', 'landscape landscape', '/index.php /ux-conference/index.php', 'smartphone', 'ios', 'apple safari', 'right', 'smartphone_3', '370 370', '736 736', 'false', 'false', 'nz6a', 5, 8),
(169, '2 2 2 12 7', '714|249 114|324 687|347 712|491 694|266 447|332 678|349 674|479 683|758 684|347 728|331 690|442 676|479 123|448 672|789 680|1139 721|1103 676|593 674|491 689|601 677|1056 684|1172 696|1097 678|515 693|886', '24 6 8 33 16', 'portrait portrait portrait portrait portrait', '/index.php /reports/index.php /index.php /training/index.php /index.php', 'tablet', 'ios', 'apple safari', 'right', 'tablet_1', '768 768 768 768 768', '920 920 920 920 920', 'false', 'false', 'nz6a', 4, 27),
(170, '10 1', '949|263 947|416 932|586 943|881 946|984 930|1051 926|1109 941|1217 915|1213 55|1449 922|1199 917|1382 936|304', '24 4', 'landscape landscape', '/index.php /topic/eyetracking/index.php', 'tablet', 'ios', 'apple safari', 'both', 'tablet_3', '664 664', '1024 1024', 'false', 'false', 'nz6a', 3, 20),
(171, '25 9 5 24', '287|296 284|515 303|646 313|309 328|371 331|547 332|681 340|823 329|960 321|1105 323|1266 325|1371 327|1534 324|1620 373|1463 364|1089 356|670 362|155 340|380 346|491 341|601 332|760 337|870 281|1025 268|1036 339|319 356|503 346|690 351|892 343|1062 339|1217 363|1155 344|823 341|545 335|410 336|669 342|815 348|929 337|973 348|435 339|514 337|655 334|782 342|904 356|1043 332|1176 326|1351 331|1570 345|1763 349|1883 329|2018 339|2228 330|2366 336|2517 327|2691 324|2843 337|2964 364|2565 358|2103 342|1702 347|1182 354|379 365|660', '27 8 6 17', 'portrait portrait portrait portrait', '/index.php /online-seminars/ux-big-data/index.php /index.php /training/austin/index.php', 'smartphone', 'ios', 'apple safari', 'both', 'smartphone_4', '414 414 414 414', '628 628 628 628', 'false', 'false', 'o8hn', 5, 29),
(172, '22 10 12 0', '571|219 586|404 588|582 598|773 620|867 621|1052 623|1160 630|1298 628|1397 630|1638 615|1850 623|2145 626|2478 654|2057 643|1764 581|1581 597|1377 624|1184 633|1047 642|888 630|854 104|1130 654|211 627|499 628|722 608|950 610|1280 604|1725 601|2086 629|2320 635|1897 630|1658 594|136 599|413 600|453 626|441 631|216 603|425 71|550 626|517 619|661 611|815 599|971 123|993 638|1055 638|1230 0|0', '16 5 11 0', 'landscape landscape landscape landscape', '/index.php /reports/best-merged-intranets/index.php /index.php /books/index.php', 'smartphone', 'ios', 'apple safari', 'both', 'smartphone_4', '370 370 370 370', '736 736 736 736', 'false', 'false', 'o8hn', 1, 14),
(173, '12 4', '668|427 673|504 680|600 669|711 664|817 673|1035 693|1011 701|919 703|850 705|776 715|521 317|564 699|613 725|759 705|855 702|902', '17 4', 'portrait portrait', '/index.php /ux-certification/index.php', 'tablet', 'ios', 'apple safari', 'both', 'tablet_3', '768 768', '920 920', 'false', 'false', 'o8hn', 2, 11),
(174, '14 7 5', '974|294 919|409 926|530 934|626 930|717 924|830 933|945 947|1038 966|1059 964|784 954|540 969|390 970|277 600|299 715|285 933|567 920|690 924|821 908|943 908|1026 119|1084 921|372 935|410 946|488 917|605 924|761', '18 10 4', 'landscape landscape landscape', '/index.php /training/index.php /in-house-training/index.php', 'tablet', 'ios', 'apple safari', 'both', 'tablet_3', '664 664 664', '1024 1024 1024', 'false', 'false', 'o8hn', 4, 24),
(175, '9 45 14', '274|415 270|550 283|636 235|766 318|811 311|601 304|449 286|341 364|121 228|390 253|481 233|555 285|688 246|785 248|892 201|1056 203|1251 191|1453 221|1588 316|1585 320|1330 307|1143 329|1027 329|834 332|731 324|565 325|421 323|316 40|38 354|55 43|34 355|70 223|484 270|713 189|954 319|1105 227|1290 254|1480 311|1618 278|1770 370|1854 354|1657 357|1488 347|1317 359|1182 361|976 351|778 345|538 350|294 355|352 387|387 392|388 382|388 384|397 322|497 322|531 327|711 341|792 324|906 326|1010 299|1171 301|1324 297|1484 294|1621 298|1768 288|1931 305|2060 312|2196', '14 65 28', 'portrait portrait portrait', '/index.php /training/index.php /training/why-attend/index.php', 'smartphone', 'ios', 'apple safari', 'right', 'smartphone_2', '414 414 414', '628 628 628', 'false', 'false', 'udiv', 5, 28),
(176, '31 44', '550|214 586|307 498|489 501|646 499|801 511|958 515|1095 597|1153 567|801 559|551 539|359 546|223 37|34 69|222 603|89 143|23 538|175 621|221 635|297 609|382 635|431 618|517 564|654 641|874 647|556 611|399 675|398 653|284 581|239 611|323 665|451 529|261 558|454 591|596 642|642 637|679 627|782 635|887 612|992 609|1111 646|1144 606|849 618|679 641|478 625|567 548|721 643|758 623|411 549|215 496|281 487|476 486|757 496|997 504|1126 505|1347 492|1534 493|1721 487|1929 474|2143 471|2372 470|2629 493|2784 505|2953 528|3030 659|2931 672|2623 653|2366 650|2148 607|1826 631|1435 605|1083 619|605 587|122 562|280 564|419', '55 52', 'landscape landscape', '/index.php /consulting/index.php', 'smartphone', 'ios', 'apple safari', 'both', 'smartphone_4', '370 370', '736 736', 'false', 'false', 'udiv', 2, 17),
(177, '8 24', '488|645 597|920 648|1020 605|417 756|35 735|37 736|36 456|394 579|692 591|1141 634|1276 574|912 618|592 738|42 430|29 387|26 418|38 395|48 625|458 645|647 693|862 685|1076 720|858 411|1057 413|894 422|1027 493|1168 499|638 559|515 668|701 664|1014 683|1135', '25 87', 'portrait portrait', '/index.php /training/index.php', 'tablet', 'ios', 'apple safari', 'right', 'tablet_1', '768 768', '920 920', 'false', 'false', 'udiv', 3, 13),
(178, '9 26 28 8 5', '798|467 600|363 638|662 611|912 591|916 563|595 575|263 30|91 192|421 591|566 626|812 633|1131 633|1587 627|1871 677|2085 703|1203 683|810 627|448 588|341 606|717 605|1123 600|1703 594|2963 593|4089 608|5254 594|5051 612|4638 747|4195 558|3831 634|4413 628|4821 591|4624 597|4627 590|4629 592|4641 612|548 474|644 444|349 489|1636 463|1106 509|1395 542|1244 530|1309 596|737 642|1657 612|2229 600|1552 741|1350 742|1066 702|1454 704|1886 722|2226 775|2608 775|2056 124|2030 615|1637 611|1332 596|1710 611|2051 613|2490 635|2951 526|2818 448|2824 592|517 602|1145 620|1693 621|2181 639|2717 643|3374 652|4312 343|4250 512|579 516|872 498|1127 494|1442 503|1799', '26 42 33 11 12', 'landscape landscape landscape landscape landscape', '/index.php /online-seminars/index.php /online-seminars/index4658.php?page=2 /online-seminars/index2679.php?page=1 /online-seminars/every-word-count/index.php', 'tablet', 'ios', 'apple safari', 'right', 'tablet_1', '664 664 664 664 664', '1024 1024 1024 1024 1024', 'false', 'false', 'udiv', 1, 27),
(179, '1 18', '160|320 69|325 129|254 125|610 107|836 287|727 293|1008 346|1201 355|1381 341|1628 342|1868 353|2052 343|2228 341|2401 328|2570 340|2769 333|2755 343|2476 373|2381', '18 37', 'portrait portrait', '/index.php /training/index.php', 'smartphone', 'ios', 'apple safari', 'right', 'smartphone_3', '414 414', '628 628', 'false', 'false', 'ri9f', 1, 23),
(180, '7 21 3 0', '575|199 588|258 628|372 514|534 472|354 657|534 112|485 357|327 337|351 331|519 341|709 312|831 397|889 502|983 445|1069 446|1198 451|1381 365|1418 454|1005 349|848 333|616 348|641 322|736 297|794 325|894 333|1011 334|974 78|1005 460|155 677|369 348|508 0|0', '20 32 11 4', 'landscape landscape landscape landscape', '/index.php /reports/index.php /reports/free/index.php /reports/how-to-recruit-participants-usability-studies/index.php', 'smartphone', 'ios', 'apple safari', 'right', 'smartphone_3', '370 370 370 370', '736 736 736 736', 'false', 'false', 'ri9f', 4, 15),
(181, '1 9 0', '357|361 492|716 468|1120 570|1207 620|1069 475|1042 474|1278 673|1250 119|971 155|1205 0|0', '5 28 5', 'portrait portrait portrait', '/index.php /training/index.php /in-house-training/index.php', 'tablet', 'ios', 'apple safari', 'right', 'tablet_1', '768 768 768', '920 920 920', 'false', 'false', 'ri9f', 2, 30),
(182, '3 1 0', '685|518 692|621 435|357 435|371 223|533 140|548 0|0', '9 9 2', 'landscape landscape landscape', '/index.php /training/index.php /training/why-attend/index.php', 'tablet', 'ios', 'apple safari', 'right', 'tablet_1', '664 664 664', '1024 1024 1024', 'false', 'false', 'ri9f', 5, 28),
(183, '13 7 11 3 21', '279|1659 302|1335 313|1051 312|786 293|502 312|270 286|391 280|582 263|688 305|656 263|419 283|246 215|49 299|420 312|398 352|46 344|320 360|430 331|601 348|620 347|357 312|217 77|36 268|451 296|621 275|801 269|957 268|1160 293|1198 304|977 306|837 311|672 337|437 380|187 325|367 322|525 122|689 326|292 300|467 340|645 350|801 345|937 382|1035 366|1151 366|1335 390|1494 370|1497 372|1317 374|1258 391|1104 391|961 389|853 391|702 391|595 388|474 381|449 379|284 361|241', '72 20 29 17 50', 'portrait portrait portrait portrait portrait', '/index.php /articles/index.php /index.php /training/index.php /training/london/index.php', 'smartphone', 'ios', 'apple safari', 'right', 'smartphone_2', '414 414 414 414 414', '628 628 628 628 628', 'false', 'false', 'xjpq', 3, 6),
(184, '9 31 13', '608|172 583|376 570|503 594|632 574|770 611|859 600|904 546|888 397|877 558|250 551|437 567|598 624|800 560|1014 569|1137 585|1306 575|1463 587|1612 584|1734 580|1803 579|1891 583|1993 601|1831 589|1744 590|1663 596|1551 583|1455 590|1325 602|1181 614|1046 595|933 639|786 530|769 567|722 564|711 588|820 557|920 536|1039 433|1107 404|1122 566|260 517|467 527|575 553|721 565|883 575|1019 578|1175 559|1338 536|1492 552|1663 339|3048 320|3041 340|3059', '25 67 32', 'landscape landscape landscape', '/index.php /training/index.php /training/why-attend/index.php', 'smartphone', 'ios', 'apple safari', 'right', 'smartphone_3', '370 370 370', '736 736 736', 'false', 'false', 'xjpq', 1, 7),
(185, '2 8 2 5', '547|638 634|308 448|672 478|1026 464|813 583|856 453|736 477|1133 482|1437 504|819 148|292 148|292 445|556 428|998 460|978 506|885 509|796', '18 27 3 20', 'portrait portrait portrait portrait', '/index.php /consulting/index.php /index.php /consulting/index.php', 'tablet', 'ios', 'apple safari', 'right', 'tablet_1', '768 768 768 768', '920 920 920 920', 'false', 'false', 'xjpq', 2, 14),
(186, '3 15 1', '733|152 659|529 542|391 611|576 1023|296 613|726 635|891 657|1194 634|1556 722|1255 746|1135 771|987 651|860 657|632 673|368 683|438 655|540 245|613 587|527 582|708 579|496', '13 43 5', 'landscape landscape landscape', '/index.php /training/index.php /training/why-attend/index.php', 'tablet', 'ios', 'apple safari', 'right', 'tablet_1', '664 664 664', '1024 1024 1024', 'false', 'false', 'xjpq', 3, 20),
(187, '16 32 22', '341|356 318|556 300|569 369|407 331|594 326|767 332|971 311|1145 314|1347 256|1588 253|1810 290|1541 298|899 293|503 164|395 50|259 374|427 371|544 337|676 343|801 342|878 347|1014 344|1149 362|1250 344|1414 346|1589 342|1786 343|1962 330|2155 327|2342 331|2558 351|2754 330|2940 342|3091 333|3311 316|3522 382|3506 354|3024 359|2664 339|2324 328|2017 337|1758 337|1460 366|1275 355|1107 362|867 346|683 232|480 335|459 327|546 312|697 326|851 342|946 332|1023 346|1016 334|699 335|488 27|423 360|564 365|816 356|984 357|1206 368|1392 342|1445 321|1149 315|1483 312|1786 346|2016 344|2232 342|2418', '54 61 58', 'portrait portrait portrait', '/index.php /training/london/index.php /training/register/87/index.php', 'smartphone', 'ios', 'apple safari', 'right', 'smartphone_3', '414 414 414', '628 628 628', 'false', 'false', 'exmk', 4, 25),
(188, '17 5 8', '546|224 458|430 456|521 477|594 466|756 428|922 422|1091 412|1244 407|1402 419|1532 441|1498 453|1165 448|888 457|637 437|381 463|277 596|310 318|282 347|399 311|556 321|682 184|678 333|328 352|399 346|546 348|712 344|842 331|1001 330|1194 427|1238', '37 14 12', 'landscape landscape landscape', '/index.php /consulting/index.php /consulting/ia-navigation-analysis/index.php', 'smartphone', 'ios', 'apple safari', 'right', 'smartphone_3', '370 370 370', '736 736 736', 'false', 'false', 'exmk', 5, 22),
(189, '5 3 4 4', '581|677 542|1007 605|1138 589|653 380|290 604|587 492|815 81|730 566|631 538|954 544|1188 195|552 486|665 61|488 61|486 685|834', '23 17 18 14', 'portrait portrait portrait portrait', '/index.php /training/index.php /training/london/index.php /training/register/87/index.php', 'tablet', 'ios', 'apple safari', 'right', 'tablet_1', '768 768 768 768', '920 920 920 920', 'false', 'false', 'exmk', 4, 16);
INSERT INTO `record_navigation_styles` (`id`, `no_interactions`, `touch_locations`, `no_seconds`, `viewed`, `visited_pages`, `device`, `operating_system`, `browser`, `held`, `hand`, `width`, `height`, `own_device`, `test_data`, `tester_id`, `navigation_styles_id`, `task_sets_id`) VALUES
(190, '4 5 0', '554|444 637|615 537|806 416|893 560|1012 786|487 717|802 708|1274 750|1666 107|1661 0|0', '13 12 4', 'landscape landscape landscape', '/index.php /training/index.php /training/london/index.php', 'tablet', 'ios', 'apple safari', 'right', 'tablet_1', '664 664 664', '1024 1024 1024', 'false', 'false', 'exmk', 1, 4),
(191, '16', '383|330 379|415 360|424 363|575 346|746 339|807 346|530 342|294 360|377 364|552 372|751 369|922 368|1079 370|1346 362|1545 359|1745', '40', 'portrait', '/index.php', 'smartphone', 'ios', 'apple safari', 'right', 'smartphone_2', '414', '628', 'false', 'false', 'xc6i', 2, 30),
(192, '51 4', '588|252 647|321 595|534 587|684 649|826 605|1030 561|1195 550|1429 558|1685 678|1738 681|1639 669|1448 634|1283 615|1145 627|1012 623|877 606|769 610|619 606|466 614|305 626|244 661|468 656|522 634|571 625|734 625|898 647|1070 611|1201 592|1377 614|1440 614|1211 628|1054 625|907 609|822 615|653 620|483 648|294 649|179 651|74 626|163 619|360 630|530 617|721 624|896 613|1023 607|1183 622|1263 603|1427 631|1495 116|1354 99|1362 628|191 631|405 611|604 596|848', '129 10', 'landscape landscape', '/index.php /articles/index.php', 'smartphone', 'ios', 'apple safari', 'right', 'smartphone_1', '370 370', '736 736', 'false', 'false', 'xc6i', 5, 18),
(193, '1', '561|660', '13', 'portrait', '/index.php', 'tablet', 'ios', 'apple safari', 'right', 'tablet_1', '768', '920', 'false', 'false', 'xc6i', 3, 23),
(194, '1 15 13 6', '959|449 485|411 474|649 449|953 503|1144 659|1036 849|79 850|81 553|291 503|530 510|722 484|968 489|294 67|69 59|32 530|420 856|303 866|451 952|537 765|778 801|980 784|1091 823|1191 865|1170 810|1009 620|574 459|262 74|30 527|127 909|509 680|665 613|899 537|1153 397|1388 432|1180', '7 36 52 63', 'landscape landscape landscape landscape', '/index.php /consulting/index.php /training/index.php /index.php', 'tablet', 'ios', 'apple safari', 'right', 'tablet_1', '664 664 664 664', '1024 1024 1024 1024', 'false', 'false', 'xc6i', 5, 5),
(195, '9 4', '323|422 299|624 273|782 291|888 276|1154 381|1329 366|1443 348|1561 260|1700 320|510 341|559 345|706 311|833', '16 14', 'portrait portrait', '/index.php /about/why-nng/index.php', 'smartphone', 'ios', 'apple safari', 'both', 'smartphone_4', '414 414', '628 628', 'false', 'false', 'jsxy', 4, 6),
(196, '8 5', '173|302 466|633 459|871 464|1115 442|1585 613|1674 652|1588 655|1582 644|109 640|236 664|308 684|374 666|423', '10 11', 'landscape landscape', '/index.php /about/contact/index.php', 'smartphone', 'ios', 'apple safari', 'both', 'smartphone_4', '370 370', '736 736', 'false', 'false', 'jsxy', 2, 17),
(197, '3 8 6 3', '682|382 682|906 91|823 680|638 669|938 612|1040 633|1266 594|1385 609|1364 576|1353 44|1538 647|627 715|829 712|1017 697|1231 691|1372 515|1401 643|710 668|1095 656|1189', '6 34 11 4', 'portrait portrait portrait portrait', '/index.php /reports/index.php /reports/free/index.php /reports/user-experience-careers/index.php', 'tablet', 'ios', 'apple safari', 'both', 'tablet_3', '768 768 768 768', '920 920 920 920', 'false', 'false', 'jsxy', 1, 2),
(198, '4 7 4 13 0', '936|369 824|560 455|554 451|541 961|431 933|358 77|498 45|462 41|595 100|790 92|444 808|143 911|147 942|1197 943|970 926|471 406|544 944|445 960|596 431|529 1002|48 440|518 425|515 416|557 387|668 382|813 367|1016 379|1175 402|1357 450|1529 474|1677 906|1909 960|1772 926|1909 951|1774 0|0', '9 17 6 21 2', 'landscape landscape landscape landscape landscape', '/index.php /ux-conference/index.php /index.php /ux-conference/index.php /training-companies-list/index.php', 'tablet', 'ios', 'apple safari', 'both', 'tablet_4', '664 664 664 664 664', '1024 1024 1024 1024 1024', 'false', 'false', 'jsxy', 3, 29),
(199, '3 2 5', '361|191 304|197 216|196 315|473 317|545 330|537 293|768 298|1051 284|1334 301|1291', '33 14 14', 'portrait portrait portrait', '/index.php /training/index.php /courses/index.php', 'smartphone', 'ios', 'apple safari', 'right', 'smartphone_3', '414 414 414', '628 628 628', 'false', 'false', 'zht2', 3, 8),
(200, '6 10', '480|255 484|482 478|287 441|430 351|610 336|617 449|342 465|581 467|809 457|1036 474|1241 469|1458 479|1741 459|1882 455|1979 460|2266', '31 23', 'landscape landscape', '/index.php /online-seminars/index.php', 'smartphone', 'ios', 'apple safari', 'right', 'smartphone_3', '370 370', '736 736', 'false', 'false', 'zht2', 2, 24),
(201, '1 3 2', '612|362 365|738 366|737 73|313 432|744 452|1021', '14 10 8', 'portrait portrait portrait', '/index.php /consulting/index.php /consulting/usability-testing/index.php', 'tablet', 'ios', 'apple safari', 'right', 'tablet_1', '768 768 768', '920 920 920', 'false', 'false', 'zht2', 5, 15),
(202, '1 8 7 4 4', '435|365 619|565 605|922 649|1183 809|978 768|586 780|403 896|507 848|623 615|611 610|1072 605|1519 623|2036 627|2480 609|2988 591|3223 644|510 621|1147 625|1642 296|1624 479|638 487|1246 500|1628 911|838', '8 28 16 12 13', 'landscape landscape landscape landscape landscape', '/index.php /training/index.php /online-seminars/index.php /online-seminars/index4658.php?page=2 /online-seminars/mobile-user-testing/index.php', 'tablet', 'ios', 'apple safari', 'right', 'tablet_1', '664 664 664 664 664', '1024 1024 1024 1024 1024', 'false', 'false', 'zht2', 4, 21),
(203, '10 10 3', '200|459 243|496 202|854 231|1260 199|1547 185|2053 262|1432 309|865 331|388 206|320 216|489 198|933 208|1326 243|1751 206|2155 267|2086 250|1559 282|1005 299|407 192|393 235|470 199|986 196|1339', '63 28 8', 'portrait portrait portrait', '/index.php /training/index.php /consulting/index.php', 'smartphone', 'ios', 'apple safari', 'right', 'smartphone_3', '414 414 414', '628 628 628', 'false', 'false', 'md79', 1, 26),
(204, '4 2', '347|343 375|619 442|610 358|401 330|430 393|363 373|632', '16 4', 'landscape landscape', '/index.php /training/index.php', 'smartphone', 'ios', 'apple safari', 'right', 'smartphone_3', '370 370', '736 736', 'false', 'false', 'md79', 3, 9),
(205, '1 2', '370|376 360|784 377|1224', '12 8', 'portrait portrait', '/index.php /training/index.php', 'tablet', 'ios', 'apple safari', 'right', 'tablet_1', '768 768', '920 920', 'false', 'false', 'md79', 5, 7),
(206, '1 4', '781|352 477|556 464|950 572|851 563|141', '6 20', 'landscape landscape', '/index.php /consulting/index.php', 'tablet', 'ios', 'apple safari', 'right', 'tablet_1', '664 664', '1024 1024', 'false', 'false', 'md79', 4, 12),
(207, '1 3 15', '172|172 321|380 340|351 183|402 338|415 325|467 343|545 359|535 342|864 343|994 335|1005 351|867 342|657 340|487 347|492 334|782 336|1098 323|1206 384|1337', '16 8 66', 'portrait portrait portrait', '/index.php /reports/index.php /consulting/index.php', 'smartphone', 'ios', 'apple safari', 'right', 'smartphone_3', '414 414 414', '628 628 628', 'false', 'false', 'gnl1', 1, 15),
(208, '2 6 8 3', '534|104 131|341 456|367 528|75 476|96 355|416 347|497 251|463 629|122 647|148 680|305 681|314 676|330 667|357 329|707 309|697 535|161 539|416 543|529', '40 39 28 28', 'landscape landscape landscape landscape', '/index.php /reports/index.php /reports/topic/mobile-and-tablet-design/index.php /reports/ipad-app-and-website-usability/index.php', 'smartphone', 'ios', 'apple safari', 'right', 'smartphone_3', '370 370 370 370', '736 736 736 736', 'false', 'false', 'gnl1', 2, 8),
(209, '1 0', '353|439 0|0', '45 30', 'portrait portrait', '/index.php /training/index.php', 'tablet', 'ios', 'apple safari', 'right', 'tablet_1', '768 768', '920 920', 'false', 'false', 'gnl1', 3, 2),
(210, '2 2 0', '173|332 92|388 947|414 647|646 0|0', '16 18 20', 'landscape landscape landscape', '/index.php /reports/index.php /reports/b2b-websites-usability/index.php', 'tablet', 'ios', 'apple safari', 'right', 'tablet_1', '664 664 664', '1024 1024 1024', 'false', 'false', 'gnl1', 4, 25),
(211, '4 3 3 26', '271|396 298|430 273|379 94|323 330|448 328|642 61|829 377|241 352|236 343|262 322|468 331|475 337|508 314|507 25|429 313|651 307|661 302|679 308|780 317|979 299|1133 296|1288 298|1467 293|1683 366|1789 94|2010 311|2203 304|2353 311|2497 323|2581 216|2697 319|2762 321|3003 316|3072 312|3114 324|3081', '141 62 21 103', 'portrait portrait portrait portrait', '/index.php /training/london/index.php 40/ux-basic-training/index.php /training/register/87/index.php', 'smartphone', 'ios', 'apple safari', 'right', 'smartphone_3', '414 414 414 414', '628 628 628 628', 'false', 'false', 'oqmg', 5, 6),
(212, '3 6', '584|100 409|268 596|363 423|275 391|363 357|452 389|438 330|460 337|430', '70 47', 'landscape landscape', '/index.php /consulting/index.php', 'smartphone', 'ios', 'apple safari', 'right', 'smartphone_3', '370 370', '736 736', 'false', 'false', 'oqmg', 2, 11),
(213, '6', '561|438 493|433 576|443 600|445 652|491 518|819', '58', 'portrait', '/index.php', 'tablet', 'ios', 'apple safari', 'right', 'tablet_1', '768', '920', 'false', 'false', 'oqmg', 1, 21),
(214, '2', '601|361 644|705', '11', 'landscape', '/index.php', 'tablet', 'ios', 'apple safari', 'right', 'tablet_1', '664', '1024', 'false', 'false', 'oqmg', 4, 6),
(215, '37 8 31', '378|344 400|485 394|659 388|924 377|1137 366|1358 354|1605 399|1751 380|1933 369|2083 376|2398 382|2421 367|2030 179|2421 263|2721 252|2717 394|2459 399|2266 369|2000 380|1897 398|1728 367|1600 363|1336 362|1162 360|901 381|726 384|587 361|574 384|356 356|191 321|424 375|720 381|939 391|1134 358|1341 381|1476 195|1665 399|451 390|585 379|784 380|980 388|1118 401|1218 386|1376 387|1333 391|1479 354|427 357|591 364|859 393|1075 357|1311 396|1485 385|1574 378|1696 375|1911 375|2080 386|2083 381|2228 381|2379 379|2557 393|2611 387|2749 385|2833 386|3003 380|3101 385|3258 374|3436 351|3573 394|3748 380|3813 382|3945 390|4064 388|4147 398|4334 375|4535 400|4714 381|4857', '105 25 84', 'portrait portrait portrait', '/index.php /training/austin/index.php 647/ux-basic-training/index.php', 'smartphone', 'ios', 'apple safari', 'right', 'smartphone_3', '414 414 414', '628 628 628', 'false', 'false', '5nwf', 1, 30),
(216, '44 17 13', '688|176 687|362 688|590 691|720 682|939 679|1097 695|1332 686|1457 698|1625 683|1803 675|1689 661|1483 660|1280 543|1200 579|1078 605|983 668|787 628|648 676|492 672|338 677|149 553|284 548|521 390|704 444|735 424|763 681|778 649|1019 652|1040 700|1281 700|1281 673|1299 697|1370 713|1176 687|977 684|858 678|647 674|548 680|283 678|224 676|127 126|54 645|53 628|53 620|223 665|298 693|474 686|614 691|736 690|865 701|995 698|1139 678|1315 662|1490 690|1653 695|1778 690|1925 409|2170 416|2166 416|2154 416|2161 578|217 639|360 643|549 615|702 643|877 650|1097 603|1274 589|1512 592|1771 416|2156 630|2188 639|2397 628|2427', '73 26 28', 'landscape landscape landscape', '/index.php /articles/index.php /index.php', 'smartphone', 'ios', 'apple safari', 'right', 'smartphone_3', '370 370 370', '736 736 736', 'false', 'false', '5nwf', 3, 23),
(217, '9 6 2 0', '657|604 679|739 657|936 667|1158 688|1029 708|1059 658|827 687|545 109|361 355|719 357|938 413|1074 469|1016 536|928 75|988 738|503 451|444 0|0', '44 28 8 8', 'portrait portrait portrait portrait', '/index.php /reports/index.php /reports/free/index.php /reports/how-to-recruit-participants-usability-studies/index.php', 'tablet', 'ios', 'apple safari', 'right', 'tablet_1', '768 768 768 768', '920 920 920 920', 'false', 'false', '5nwf', 2, 15),
(218, '3 11 1 6 0', '459|375 459|375 438|385 1017|434 1007|650 960|893 942|946 925|1127 958|1144 889|942 1015|641 1022|566 1018|492 992|254 809|352 813|355 1018|261 991|440 109|501 108|499 551|578 87|486 0|0', '13 39 14 22 10', 'landscape landscape landscape landscape landscape', '/index.php /training/index.php /index.php /consulting/index.php /consulting/expert-design-review/index.php', 'tablet', 'ios', 'apple safari', 'right', 'tablet_1', '664 664 664 664 664', '1024 1024 1024 1024 1024', 'false', 'false', '5nwf', 5, 5),
(219, '15 11 1 0', '239|423 256|552 272|684 281|758 283|956 277|1113 282|1208 303|1307 273|1151 258|970 286|804 265|718 280|578 264|486 198|121 186|392 233|561 236|676 272|827 296|946 286|1057 302|1175 292|1369 290|1499 288|1615 170|1651 261|501 0|0', '32 35 10 4', 'portrait portrait portrait portrait', '/index.php /training/index.php /online-seminars/index.php /online-seminars-faq/index.php', 'smartphone', 'ios', 'apple safari', 'both', 'smartphone_4', '414 414 414 414', '628 628 628 628', 'false', 'false', 'sfhx', 2, 9),
(220, '3 13 10 13', '506|213 469|351 120|407 424|310 425|469 457|590 547|614 450|788 428|960 435|1056 421|1180 447|1328 430|1549 438|1639 419|1789 218|1869 456|343 407|445 426|529 449|599 380|759 339|894 351|991 378|1145 378|1060 172|1163 358|305 415|752 420|729 432|599 460|799 420|996 402|1202 445|1339 386|1522 382|1779 387|1897 405|2079 357|2314', '9 51 32 23', 'landscape landscape landscape landscape', '/index.php /training/index.php /in-house-training/index.php /courses/ux-basic-training/index.php', 'smartphone', 'ios', 'apple safari', 'both', 'smartphone_4', '370 370 370 370', '736 736 736 736', 'false', 'false', 'sfhx', 1, 22),
(221, '1 4 1 0', '373|362 438|611 476|948 464|1124 109|1213 144|675 0|0', '13 33 12 1', 'portrait portrait portrait portrait', '/index.php /training/index.php /in-house-training/index.php /courses/ux-basic-training/index.php', 'tablet', 'ios', 'apple safari', 'both', 'tablet_3', '768 768 768 768', '920 920 920 920', 'false', 'false', 'sfhx', 5, 24),
(222, '1 4 0', '513|34 547|63 484|443 446|551 75|805 88|790 0|0', '9 12 5', 'landscape landscape landscape', '/index.php /reports/index.php /reports/topic/eyetracking/index.php', 'tablet', 'ios', 'apple safari', 'both', 'tablet_3', '664 664 664', '1024 1024 1024', 'false', 'false', 'sfhx', 3, 19),
(223, '6 2', '333|407 351|510 350|788 336|1114 312|1467 259|1655 360|337 356|719', '23 6', 'portrait portrait', '/index.php /about/index.php', 'smartphone', 'ios', 'apple safari', 'right', 'smartphone_3', '414 414', '628 628', 'false', 'false', 'uwk0', 4, 18),
(224, '4 1', '472|281 450|520 479|582 348|494 438|533 320|299', '10 5', 'landscape landscape', '/index.php /training/san-francisco/index.php', 'smartphone', 'ios', 'apple safari', 'right', 'smartphone_3', '370 370', '736 736', 'false', 'false', 'uwk0', 2, 28),
(225, '2 4 1 7 3 8 4', '519|681 444|498 600|505 594|1170 625|718 172|42 408|395 470|639 460|707 470|1037 457|1304 612|1155 458|772 260|688 324|729 471|608 156|58 651|764 658|1033 666|1377 668|1589 284|1584 548|899 615|664 367|629 378|662 372|899 353|1158 430|1310', '10 6 4 18 8 27 11', 'portrait portrait portrait portrait portrait portrait portrait', '/index.php /people/index.php /index.php /training/index.php /courses/index.php /index.php /ux-certification/index.php', 'tablet', 'ios', 'apple safari', 'right', 'tablet_1', '768 768 768 768 768 768 768', '920 920 920 920 920 920 920', 'false', 'false', 'uwk0', 3, 24),
(226, '3 4 2', '603|253 662|457 829|106 537|572 511|937 530|1093 63|1007 769|474 741|701', '7 11 4', 'landscape landscape landscape', '/index.php /about/index.php /about/why-nng/index.php', 'tablet', 'ios', 'apple safari', 'right', 'tablet_1', '664 664 664', '1024 1024 1024', 'false', 'false', 'uwk0', 1, 16),
(227, '11 10 3', '347|520 346|601 306|701 288|894 270|1115 262|1348 318|1174 315|877 299|675 325|414 390|115 339|470 302|521 299|759 295|897 276|1150 253|1310 259|1426 301|1306 249|1030 111|1009 325|450 280|679 307|685', '30 31 21', 'portrait portrait portrait', '/index.php /training/index.php /ux-certification/index.php', 'smartphone', 'ios', 'apple safari', 'right', 'smartphone_3', '414 414 414', '628 628 628', 'false', 'false', 'bp3e', 4, 10),
(228, '9 30 4', '616|264 566|440 544|579 521|635 513|726 563|749 566|560 415|574 318|553 434|262 415|435 464|620 452|787 450|894 444|1039 438|1208 453|1374 489|1579 456|1659 542|1853 524|1952 538|1831 492|1776 512|1686 491|1317 471|1090 489|862 464|606 454|295 357|308 388|524 365|793 408|1045 398|1389 392|1709 435|1833 442|1639 524|1627 673|1697 612|270 430|506 305|335 388|596', '22 75 13', 'landscape landscape landscape', '/index.php /ux-certification/index.php /about/contact/index.php', 'smartphone', 'ios', 'apple safari', 'right', 'smartphone_3', '370 370 370', '736 736 736', 'false', 'false', 'bp3e', 5, 17),
(229, '7 3 0', '606|685 617|1036 614|1349 692|1208 689|953 677|732 373|438 488|737 451|1231 130|1267 0|0', '40 17 3', 'portrait portrait portrait', '/index.php /training/index.php /in-house-training/index.php', 'tablet', 'ios', 'apple safari', 'right', 'tablet_1', '768 768 768', '920 920 920', 'false', 'false', 'bp3e', 3, 22),
(230, '4 22', '744|280 660|450 664|949 414|900 704|513 671|939 670|1128 625|1323 606|1528 635|1823 658|2101 647|2255 617|2609 663|2772 664|3094 719|3136 708|2614 674|2498 817|2172 796|2034 783|1830 710|1748 654|1546 652|1244 598|1122 586|951', '21 62', 'landscape landscape', '/index.php /training/index.php', 'tablet', 'ios', 'apple safari', 'right', 'tablet_1', '664 664', '1024 1024', 'false', 'false', 'bp3e', 1, 30),
(231, '1 2 16 2 30 27 11', '54|276 365|64 114|228 380|312 404|517 375|624 381|759 384|912 381|994 372|1044 378|1139 375|1311 378|1411 373|1495 369|1278 368|1062 344|777 355|308 36|330 370|68 188|311 399|413 387|435 383|521 401|759 373|917 380|1111 377|1188 381|1236 377|1375 377|1487 378|1655 380|1791 385|1866 385|1916 392|2115 382|2171 388|2270 393|1827 381|1573 375|1351 382|1132 388|979 378|816 375|730 380|702 388|503 389|497 369|308 282|117 279|136 370|391 372|480 379|587 371|611 378|640 374|677 371|737 370|773 364|782 357|964 391|1214 383|1278 378|1387 380|1452 381|1540 380|1595 374|1627 377|1566 385|1272 375|1090 381|913 380|690 377|577 369|387 353|181 361|59 62|62 396|363 387|487 386|504 392|597 400|702 394|848 399|994 391|1151 399|1295 396|1376 385|1529', '21 39 22 7 44 45 18', 'portrait portrait portrait portrait portrait portrait portrait', '/index.php /training/london/index.php /articles/index.php /training/london/index.php /training/index.php /most-attending-companies/index.php /index.php', 'smartphone', 'ios', 'apple safari', 'right', 'smartphone_3', '414 414 414 414 414 414 414', '628 628 628 628 628 628 628', 'false', 'false', 't69a', 4, 29),
(232, '12 20 6 2 30', '640|240 685|389 691|509 672|535 578|679 551|415 713|227 653|116 9|20 550|238 539|371 396|299 686|238 693|370 697|546 684|671 679|773 629|799 632|921 650|1054 693|1208 671|1379 592|1506 512|1403 510|1256 509|1019 490|909 523|650 520|499 546|297 18|32 397|266 346|262 354|395 342|514 372|651 372|785 253|831 713|264 425|205 501|281 521|350 514|497 496|691 487|880 481|1002 475|1221 469|1498 452|1688 464|1928 428|2350 447|2440 434|2218 444|2010 430|1855 418|1579 420|955 462|791 445|1099 431|1333 440|1599 436|1969 438|2417 435|2668 448|2925 471|3073 474|3242 381|3525 348|3707 303|3752', '18 29 12 6 28', 'landscape landscape landscape landscape landscape', '/index.php /articles/index.php /reports/index.php /reports/topic/web-usability/index.php /reports/b2b-websites-usability/index.php', 'smartphone', 'ios', 'apple safari', 'right', 'smartphone_3', '370 370 370 370 370', '736 736 736 736 736', 'false', 'false', 't69a', 5, 26),
(233, '7 7 7 0 3 9', '719|546 722|694 684|663 689|688 660|938 711|1033 215|1490 565|593 601|777 505|973 515|1103 491|1269 644|1386 489|1251 665|590 559|592 532|456 477|671 505|719 214|883 98|227 0|0 639|435 615|571 298|332 616|651 620|967 649|766 648|498 649|1009 654|1404 616|2017 618|2127 627|2359 631|2850 639|3507', '15 17 16 1 10 15', 'portrait portrait portrait portrait portrait portrait', '/index.php /topic/user-testing/index.php /articles/employees-user-test/index.php /topic/mobile-and-tablet-design/all/index.php /topic/mobile-and-tablet-design/index.php /articles/context-specific-cross-channel/index.php', 'tablet', 'ios', 'apple safari', 'right', 'tablet_1', '768 768 768 768 768 768', '920 920 920 920 920 920', 'false', 'false', 't69a', 2, 25),
(234, '3 9 1 0 19 0 17 11 0 4 10', '995|420 32|17 88|188 781|444 723|642 733|848 782|976 785|1068 842|1172 928|1288 845|1455 838|1447 90|247 0|0 771|363 761|483 771|567 763|829 704|930 783|1001 857|1064 812|1251 879|1377 877|1535 893|1721 856|1926 185|2058 213|1245 240|650 241|745 249|891 239|349 84|279 0|0 739|439 761|730 766|935 764|1215 752|1334 751|1451 776|1553 789|1600 764|1700 724|1839 696|1882 858|1949 916|2109 914|2228 911|2366 909|2532 695|2955 696|2952 808|549 785|677 786|801 805|898 751|1154 757|1355 204|1290 251|1134 257|939 230|463 83|199 0|0 766|373 7|26 27|26 89|251 604|446 644|776 630|969 641|1179 595|1008 592|751 575|463 557|468 694|1104 707|1218', '10 19 2 2 41 2 27 18 1 9 21', 'landscape landscape landscape landscape landscape landscape landscape landscape landscape landscape landscape', '/index.php /articles/index.php /articles/seamless-cross-channel/index.php /topic/mobile-and-tablet-design/all/index.php /topic/mobile-and-tablet-design/index.php /topic/user-testing/all/index.php /topic/user-testing/index.php /articles/index.php /topic/e-commerce/all/index.php /topic/e-commerce/index.php /training/index.php', 'tablet', 'ios', 'apple safari', 'right', 'tablet_1', '664 664 664 664 664 664 664 664 664 664 664', '1024 1024 1024 1024 1024 1024 1024 1024 1024 1024 1024', 'false', 'false', 't69a', 2, 15),
(235, '5 0', '138|477 362|61 231|208 178|516 313|562 0|0', '25 11', 'portrait portrait', '/index.php /articles/brand-intention-interpretation/index.php', 'smartphone', 'ios', 'apple safari', 'right', 'smartphone_3', '414 414', '628 628', 'false', 'false', 'en1l', 4, 14),
(236, '20 3', '564|168 544|287 501|369 514|436 469|468 540|461 492|518 475|638 484|724 472|801 468|920 477|1070 465|1239 518|1207 538|1079 510|1079 517|1052 531|948 507|998 287|1095 519|249 487|297 521|251', '27 11', 'landscape landscape', '/index.php /articles/flat-design-best-practices/index.php', 'smartphone', 'ios', 'apple safari', 'right', 'smartphone_3', '370 370', '736 736', 'false', 'false', 'en1l', 3, 26),
(237, '10 7 3 2', '471|422 432|1100 394|1824 372|2022 333|2230 324|2111 308|1477 287|1291 294|1147 595|1002 356|570 332|1014 328|1273 348|1648 379|1864 348|947 398|1086 438|512 474|831 377|1092 460|696 447|1050', '39 27 14 6', 'portrait portrait portrait portrait', '/index.php /consulting/index.php /index.php /ux-certification/index.php', 'tablet', 'ios', 'apple safari', 'right', 'tablet_1', '768 768 768 768', '920 920 920 920', 'false', 'false', 'en1l', 1, 12),
(238, '3 0 8', '547|509 579|506 169|864 0|0 282|489 307|535 379|863 320|559 536|225 504|479 557|873 634|1163', '6 4 28', 'landscape landscape landscape', '/index.php /articles/flat-design-best-practices/index.php /index.php', 'tablet', 'ios', 'apple safari', 'right', 'tablet_1', '664 664 664', '1024 1024 1024', 'false', 'false', 'en1l', 5, 25);

-- --------------------------------------------------------

--
-- Table structure for table `task_sets`
--

CREATE TABLE `task_sets` (
  `id` int(11) NOT NULL,
  `task_set` varchar(255) NOT NULL,
  `url_location` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `task_sets`
--

INSERT INTO `task_sets` (`id`, `task_set`, `url_location`) VALUES
(1, 'Find out what you will get with the Effective Agile UX Product Development report.', '/reports/agile-development-user-experience/index.php'),
(2, 'Locate the topics in the User Experience Careers report.', '/reports/user-experience-careers/index.php'),
(3, 'Review what you will learn in the How to Change Your Corporate Culture to be User First Online Seminar.', '/online-seminars/how-change-your-corporate-culture/index.php'),
(4, 'Find out what research methods are used in the Social Media User Experience report.', '/reports/user-experience-careers/index.php'),
(5, 'Find out what the price of a Single Platform for the Expert Design Review consulting is.', '/consulting/expert-design-review/index.php'),
(6, 'Find out how NN/g\'s people and their collective experience will save you time and money.', '/about/why-nng/index.php'),
(7, 'Find out how the UX Certification Training works.', '/ux-certification/index.php'),
(8, 'Locate the topics covered in the University Website UX Essentials Online Seminar.', '/online-seminars/university-websites/index.php'),
(9, 'Review the Frequently Asked Questions About NN/g Online Seminars to know what happens if you miss a live online seminar.', '/online-seminars-faq/index.php'),
(10, 'Locate the topics covered in the Lean UX and Agile training course.', '/courses/lean-ux-and-agile/index.php'),
(11, 'Locate the requirements for completing the UX Master Certification.', '/ux-certification/ux-master-certification/index.php'),
(12, 'Find out what the pricing for the UX Master Certification is.', '/ux-certification/pricing/index.php'),
(13, 'Locate the summary in The Distribution of Users\' Computer Skills: Worse Than You Think article by Jakob Nielsen.', '/articles/computer-skill-levels/index.php'),
(14, 'Review the table of contents for the Usability Engineering book to locate the stages of a test.', '/books/usability-engineering/index.php'),
(15, 'Locate the topics covered in the How to Recruit Participants for Usability Studies report.', '/reports/how-to-recruit-participants-usability-studies/index.php'),
(16, 'Find out what makes the people at Nielsen Norman Group the best in the business.', '/about/why-nng/index.php'),
(17, 'Locate the email address for UX Certification Inquiries.', '/about/contact/index.php'),
(18, 'Find out about the history of NN/g\'s research.', '/about/history/index.php'),
(19, 'Locate the topics covered in the How People Read on the Web: The Eyetracking Evidence report.', '/reports/how-people-read-web-eyetracking-evidence/index.php'),
(20, 'Locate the topics in the How to Conduct Eyetracking Studies report.', '/reports/how-to-conduct-eyetracking-studies/index.php'),
(21, 'Review what you will learn in the Mobile User Testing Online Seminar.', '/online-seminars/mobile-user-testing/index.php'),
(22, 'Find out what In-House Training Courses are available.', '/in-house-training/index.php'),
(23, 'Review what the format of the UX Deliverables training course is in.', '/courses/ux-deliverables/index.php'),
(24, 'Review what the format of the Facilitating UX Workshops training course is in.', '/courses/facilitating-ux-workshops/index.php'),
(25, 'Locate the topics covered in the User Experience for Mobile Applications and Websites report.', '/reports/mobile-website-and-application-usability/index.php'),
(26, 'Locate the topics covered in the iPad App and Website Usability report. ', '/reports/ipad-app-and-website-usability/index.php'),
(27, 'Review what you will learn in the Making Every Word Count Online Seminar.', '/online-seminars/every-word-count/index.php'),
(28, 'Find out why you should attend the UX Conference and how UX training is a valuable investment.', '/training/why-attend/index.php'),
(29, 'Locate the complete list of companies who have attended the UX conference.', '/training-companies-list/index.php'),
(30, 'Review what the format of the Web Page UX Design training course is in.', '/courses/web-page-design/index.php');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `navigation_styles`
--
ALTER TABLE `navigation_styles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `record_navigation_styles`
--
ALTER TABLE `record_navigation_styles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `navigation_styles_id` (`navigation_styles_id`),
  ADD KEY `task_sets_id` (`task_sets_id`);

--
-- Indexes for table `task_sets`
--
ALTER TABLE `task_sets`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `navigation_styles`
--
ALTER TABLE `navigation_styles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `record_navigation_styles`
--
ALTER TABLE `record_navigation_styles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=239;
--
-- AUTO_INCREMENT for table `task_sets`
--
ALTER TABLE `task_sets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `record_navigation_styles`
--
ALTER TABLE `record_navigation_styles`
  ADD CONSTRAINT `record_navigation_styles_ibfk_1` FOREIGN KEY (`navigation_styles_id`) REFERENCES `navigation_styles` (`id`),
  ADD CONSTRAINT `record_navigation_styles_ibfk_2` FOREIGN KEY (`task_sets_id`) REFERENCES `task_sets` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
