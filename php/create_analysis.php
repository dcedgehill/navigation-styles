<?php
$files = glob('data/*');
foreach($files as $file) {
    if(is_file($file)) {
        unlink($file);
    }
}
$checked_combos = array();
include_once("connect_to_db.php");
$sql = "SELECT * from record_navigation_styles WHERE test_data = 'false'";
$exist_query = mysqli_query($mysqli, $sql);
$exist = mysqli_num_rows($exist_query);
while($row = str_replace(" ", ",", mysqli_fetch_array($exist_query))) {
    $id = $row["id"];
    $touch_locations = explode(",", $row["touch_locations"]);
    $viewed = explode(",", $row["viewed"]);
    //$visited_pages = explode(",", $row["visited_pages"]);
    $device = $row["device"];
    $hand = $row["hand"];
    $held = $row["held"];
    $navigation_styles_id = $row["navigation_styles_id"];
    $task_sets_id = $row["task_sets_id"];
    $tester_id = $row["tester_id"];
    //array_push($checked_records, $id);
    if(!in_array($device . " " . $viewed[0] . " " . $hand . " " . $held, $checked_combos)) {
        $fpsankey = fopen('data/' . $device . '_' . $viewed[0] . '_' . $hand . '_' . $held . '_sankey.csv', 'w');
        $fptable = fopen('data/' . $device . '_' . $viewed[0] . '_' . $hand . '_' . $held . '_table.csv', 'w');
        $add_csv_columns_sankey = array("source", "target", "value");
        $add_csv_columns_table = array("Task Completed", "Navigation Style", "First Interaction", "Average Number of Interactions Made", "Average Number of Seconds Spent");
        fputcsv($fpsankey, $add_csv_columns_sankey);
        fputcsv($fptable, $add_csv_columns_table);
        $sqlcombo = "SELECT * from record_navigation_styles INNER JOIN task_sets ON record_navigation_styles.task_sets_id = task_sets.id INNER JOIN navigation_styles ON record_navigation_styles.navigation_styles_id = navigation_styles.id WHERE test_data = 'false' AND device LIKE '%$device%' AND viewed LIKE '%$viewed[0]%' AND hand = '$hand' AND held = '$held'";
        $exist_combo_query = mysqli_query($mysqli, $sqlcombo);
        $exist_combo = mysqli_num_rows($exist_combo_query);
        //echo $exist_combo . "<br /><br />";
        //echo $tester_id . "<br />";
        echo "<br />" . $device . " " . $viewed[0] . " " . $hand . " " . $held . "<br /><br />";
        $source_target_visited_pages = array();
        echo $exist_combo . "<br /><br />";
        while ($row = str_replace(" ", ",", mysqli_fetch_array($exist_combo_query))) {
            $visited_pages = explode(",", $row["visited_pages"]);
            $navigation_styles_id = $row["navigation_styles_id"];
            $url_location = $row["url_location"];
            $nav_style = str_replace(",", " ", $row["style"]);
            $touch_locations = explode(",", $row["touch_locations"]);
            $touch_locations[0] = strstr($touch_locations[0], '|', true);
            $no_interactions = explode(",", $row[1]);
            $no_seconds = explode(",", $row["no_seconds"]);
            $test_id = $row[0];
            //echo $id . "<br/><br/>";
            //echo $device . " " . $viewed[0] . " " . $hand . " " . $held . "<br/><br/>";
            //echo count($visited_pages) . "<br /><br />";
            if (!in_array($device . " " . $viewed[0] . " " . $hand . " " . $held, $checked_combos)) {
                for ($i = 0; $i < count($visited_pages); $i++) {
                    if ($i + 1 < count($visited_pages)) {
                        $like = $visited_pages[$i] . ' ' . $visited_pages[$i + 1];
                        $sqlcombopages = "SELECT * from record_navigation_styles WHERE test_data = 'false' AND device LIKE '%$device%' AND viewed LIKE '%$viewed[0]%' AND hand = '$hand' AND held = '$held' AND visited_pages LIKE '%$like%'";
                        $exist_combo_pages_query = mysqli_query($mysqli, $sqlcombopages);
                        $exist_combo_pages = mysqli_num_rows($exist_combo_pages_query);
                        if ($exist_combo_pages > 0) {
                            //echo $visited_pages[$i] . " " . $visited_pages[$i + 1] . " " . $exist_combo_pages . "<br />";
                            if(!$visited_pages[$i] || !$visited_pages[$i + 1]) {
                                echo "Blank Path " . $test_id . "<br />";
                                echo $visited_pages[$i] . " " . $visited_pages[$i + 1] . " " . $exist_combo_pages . "<br />";
                            }
                            $length = strlen(".php");
                            if(substr($visited_pages[$i], - $length) !== ".php" || substr($visited_pages[ + 1], - $length) !== ".php") {
                                echo "URL does not end in .php " . $test_id . "<br />";
                                echo $visited_pages[$i] . " " . $visited_pages[$i + 1] . " " . $exist_combo_pages . "<br />";
                            }
                            $add_csv_row_sankey = array($visited_pages[$i], $visited_pages[$i + 1], $exist_combo_pages);
                            if (!in_array($add_csv_row_sankey, $source_target_visited_pages)) {
                                array_push($source_target_visited_pages, $add_csv_row_sankey);
                                //print_r($add_csv_row_sankey);
                                //echo "<br /><br />";
                                fputcsv($fpsankey, $add_csv_row_sankey);
                                //echo $like . " " . $exist_combo_pages . "<br/><br/>";
                            } else {
                                //echo $like . " " . $exist_combo_pages . "<br/><br/>";
                                //echo "Row Already Exists<br /><br />";
                            }
                        }
                    } else {
                        //echo "Next Line<br />";
                    }
                }
                //echo "For loop end<br />";
                $add_csv_row_table = array();
                //echo $id . "<br />";
                //echo end($visited_pages) . "<br />";
                //echo $url_location . "<br />";
                if (end($visited_pages) == $url_location) {
                    //echo "Completed Task<br/><br/>";
                    array_push($add_csv_row_table, "Yes");
                } else {
                    //echo "Not Completed Task<br/><br/>";
                    array_push($add_csv_row_table, "No");
                }
                array_push($add_csv_row_table, $nav_style);
                if ($navigation_styles_id == 1) {
                    if ($device == "smartphone") {
                        if ($touch_locations[0] < 490) {
                            array_push($add_csv_row_table, "Navigation Style");
                        } else {
                            array_push($add_csv_row_table, "Website Content");
                        }
                    } else {
                        if ($touch_locations[0] < 150) {
                            array_push($add_csv_row_table, "Navigation Style");
                        } else {
                            array_push($add_csv_row_table, "Website Content");
                        }
                    }
                } else if ($navigation_styles_id == 2 || $navigation_styles_id == 5) {
                    if ($touch_locations[0] < 57) {
                        array_push($add_csv_row_table, "Navigation Style");
                    } else {
                        array_push($add_csv_row_table, "Website Content");
                    }
                } else if ($navigation_styles_id == 3) {
                    if ($touch_locations[0] < 130) {
                        array_push($add_csv_row_table, "Navigation Style");
                    } else {
                        array_push($add_csv_row_table, "Website Content");
                    }
                } else if ($navigation_styles_id == 4) {
                    if ($touch_locations[0] < 110) {
                        array_push($add_csv_row_table, "Navigation Style");
                    } else {
                        array_push($add_csv_row_table, "Website Content");
                    }
                }
                array_push($add_csv_row_table, round(array_sum($no_interactions) / count($no_interactions), 2));
                array_push($add_csv_row_table, round(array_sum($no_seconds) / count($no_seconds), 2));
                //print_r($add_csv_row_table);
                fputcsv($fptable, $add_csv_row_table);
            } else {
                //echo "Already checked<br/><br/>";
            }
        }
        array_push($checked_combos, $device . " " . $viewed[0] . " " . $hand . " " . $held);
        //echo "Row Done<br /><br />";
        fclose($fpsankey);
        fclose($fptable);
    }
}