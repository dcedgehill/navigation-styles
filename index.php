<?php
session_start();
if (isset($_POST["loadTest"])) {
    $nav = preg_replace('#[^0-9]#i', '', $_POST['loadTest']);
    if ($nav != "2") {
        echo "failed";
        exit();
    } else {
        //$tester_id = $_COOKIE['tester_id'];
        $navStyles = array();
        $navStyle = 0;
        include_once("php/connect_to_db.php");
        $sql = "SELECT * from navigation_styles";
        $exist_query = mysqli_query($mysqli, $sql);
        $exist = mysqli_num_rows($exist_query);
        for ($i = 1; $i <= $exist; $i++) {
            $sql = "SELECT COUNT(id) from record_navigation_styles WHERE navigation_styles_id='$i' AND test_data='false'";
            $query = mysqli_query($mysqli, $sql);
            $query_count = mysqli_fetch_row($query);
            $nav_count = $query_count[0];
            array_push($navStyles, $nav_count);
        }
        $randomNavs = array();
        foreach($navStyles as $key => $value) {
            if ($value == min($navStyles)) {
                $value = "na";
                array_push($randomNavs, $key + 1);
            }
        }
        $shuffle = array_rand($randomNavs, 1);
        $navStyle = $randomNavs[$shuffle];
        echo $navStyle;
        exit();
    }
}
if (isset($_POST["step"])) {
    $nav = preg_replace('#[^0-9]#i', '', $_POST['step']);
    if ($nav == "") {
        echo "failed";
        exit();
    } else {
        include_once("php/connect_to_db.php");
        $task_set_id = 0;
        if (isset($_COOKIE["own_device"]) == "false") {
            $tasks = array();
            $sql = "SELECT * from task_sets";
            $exist_query = mysqli_query($mysqli, $sql);
            $exist = mysqli_num_rows($exist_query);
            for ($i = 1; $i <= $exist; $i++) {
                $sql = "SELECT COUNT(id) from record_navigation_styles WHERE task_sets_id='$i'";
                $query = mysqli_query($mysqli, $sql);
                $query_count = mysqli_fetch_row($query);
                $task_count = $query_count[0];
                array_push($tasks, $task_count);
            }
            $random_task_set = array();
            foreach($tasks as $key => $value) {
                if ($value == min($tasks)) {
                    $value = "na";
                    array_push($random_task_set, $key + 1);
                }
            }
            $shuffle = array_rand($random_task_set, 1);
            $task_set_id = $random_task_set[$shuffle];
        }
        if (isset($_COOKIE["own_device"]) != "false" || $task_set_id == 0) {
            $task_set_id = rand(1, 30);
        }
        $sql = "SELECT * from navigation_styles WHERE id = $nav limit 1";
        $exist_query = mysqli_query($mysqli, $sql);
        $exist = mysqli_num_rows($exist_query);
        if ($exist < 1) {
            echo "Failed";
            exit();
        }
        while ($row = mysqli_fetch_array($exist_query)) {
            $nav_id = $row["id"];
            $style = $row["style"];
            $position_task_btn = $row["position_task_btn"];
            $position_done_btn = $row["position_done_btn"];
            $url = $row["url"];
        }
        $sql = "SELECT * from task_sets WHERE id = $task_set_id limit 1";
        $exist_query = mysqli_query($mysqli, $sql);
        $exist = mysqli_num_rows($exist_query);
        if ($exist < 1) {
            echo "Failed 2";
            exit();
        }
        while ($row = mysqli_fetch_array($exist_query)) {
            $task_set_id = $row["id"];
            $task_set = $row["task_set"];
        }
        $_SESSION["task_sets_id"] = $task_set_id;
        $_SESSION["task_set"] = $task_set;
        echo "<h1>" . $style . "</h1><p>" . $task_set . " Click or touch the done button once you think you have completed the task. If you forget what task you are doing, click or touch the task button.</p><div class=\"centreBtn\"><span id='loading'>Loading...</span><span id='ready'><button onclick='loadApplication(1)'>Back</button><button onclick='loadApplication(2)'>Different Task</button><button onclick='loadInteraction()'>Next</button></span></div><span id='task'>" . $position_task_btn . "</span><span id='done'>" . $position_done_btn . "</span><span id='url'>" . $url . "</span>";
        exit();
    }
}

if (isset($_POST["task_set"])) {
    echo $_SESSION["task_set"];
    exit();
}

if (isset($_POST["data"])) {
    /*for ($i = 1; $i <= 3; $i++) {
        $data[$i] = preg_replace('#[^0-9,]#i', '', explode(",", $_POST['navigation' . $i]));
        if ($data[$i] == "") {
            echo "failed 1";
            exit();
        } else if ($data[$i][0] == "") {
            echo "failed 2";
            exit();
        } else if ($data[$i][1] == "") {
            echo "failed 3";
            exit();
        } else if ($data[$i][2] == "") {
            echo "failed 4";
            exit();
        }
        include_once("php/connect_to_db.php");
        $navId = $data[$i][0];
        $noInteractions = $data[$i][1];
        $noSeconds = $data[$i][2];
        //print_r($data[$i]);
        $sql = "SELECT * from navigation_styles WHERE id = $navId";
        $exist_query = mysqli_query($mysqli, $sql);
        $exist = mysqli_num_rows($exist_query);
        if ($exist < 1) {
            echo "failed 5";
            exit();
        }
        while ($row = mysqli_fetch_array($exist_query)) {
            $nav_id = $row["id"];
        }
        if ($navId != $nav_id) {
            echo "failed 6";
            exit();
        }
        $sql = "INSERT INTO record_navigation_styles(navigation_styles_id, no_interactions, no_seconds) VALUES(?, ?, ?)";
        $stmt = $mysqli->prepare($sql);
        $stmt->bind_param("iii", $navId, $noInteractions, $noSeconds);
        $stmt->execute();
    }*/
    $navId = preg_replace('#[^0-9,]#i', '', $_POST['data']);
    $data[$navId] = preg_replace('/[^\w?=#|\/.\- ]/', '', explode(",", $_POST['navigation' . $navId]));
    $navId = $data[$navId][0];
    $noInteractions = $data[$navId][1];
    $touchLocations = $data[$navId][2];
    $noSeconds = $data[$navId][3];
    $views = $data[$navId][4];
    $visitedPages = $data[$navId][5];
    $device = $data[$navId][6];
    $held = $data[$navId][7];
    $hand = $data[$navId][8];
    $width = $data[$navId][9];
    $height = $data[$navId][10];
    $operatingSystem = $data[$navId][11];
    $browser = $data[$navId][12];
    $task_sets_id = preg_replace('#[^0-9,]#i', '', (int)$_SESSION["task_sets_id"]);
    if ($task_sets_id < 1 && $task_sets_id > 30) {
        echo "failed 0";
        exit();
    }
    if ($data[$navId] == "") {
        echo "failed 1";
        exit();
    } else if ($data[$navId][0] == "") {
        echo "failed 2";
        exit();
    } else if ($data[$navId][1] == "") {
        echo "failed 3";
        exit();
    } else if ($data[$navId][2] == "") {
        echo "failed 4";
        exit();
    } else if ($data[$navId][3] == "") {
        echo "failed 5";
        exit();
    } else if ($data[$navId][4] == "") {
        echo "failed 6";
        exit();
    } else if ($data[$navId][5] == "") {
        echo "failed 7";
        exit();
    } else if ($data[$navId][6] != "smartphone" && $data[$navId][6] != "tablet" && $data[$navId][6] != "desktop | laptop") {
        echo "failed 8";
        exit();
    } else if ($data[$navId][7] != "left" && $data[$navId][7] != "both" && $data[$navId][7] != "right" && $data[$navId][7] != "na") {
        echo "failed 9";
        exit();
    } else if ($data[$navId][8] != "smartphone_1" && $data[$navId][8] != "smartphone_2" && $data[$navId][8] != "smartphone_3" && $data[$navId][8] != "smartphone_4" && $data[$navId][8] != "tablet_1" && $data[$navId][8] != "tablet_2" && $data[$navId][8] != "tablet_3" && $data[$navId][8] != "tablet_4" && $data[$navId][8] != "na") {
        echo "failed 10";
        exit();
    } else if ($data[$navId][9] == "") {
        echo "failed 11";
        exit();
    } else if ($data[$navId][10] == "") {
        echo "failed 12";
        exit();
    } else if ($data[$navId][11] == "") {
        echo "failed 13";
        exit();
    } else if ($data[$navId][12] == "") {
        echo "failed 14";
        exit();
    } else {
        include_once("php/connect_to_db.php");
        $sql = "SELECT * from navigation_styles WHERE id = $navId";
        $exist_query = mysqli_query($mysqli, $sql);
        $exist = mysqli_num_rows($exist_query);
        if ($exist < 1) {
            echo "failed 14";
            exit();
        }
        while ($row = mysqli_fetch_array($exist_query)) {
            $nav_id = $row["id"];
        }
        if ($navId != $nav_id) {
            echo "failed 15";
            exit();
        }
        $sql = "INSERT INTO record_navigation_styles(navigation_styles_id, task_sets_id, no_interactions, touch_locations, no_seconds, viewed, visited_pages, device, operating_system, browser, held, hand, width, height) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        $stmt = $mysqli->prepare($sql);
        $stmt->bind_param("iissssssssssss", $navId, $task_sets_id, $noInteractions, $touchLocations, $noSeconds, $views, $visitedPages, $device, $operatingSystem, $browser, $held, $hand, $width, $height);
        $stmt->execute();
        $mysqli->close();
        echo "submitted";
        exit();
    }
}
if (isset($_POST["tester_id"])) {
    include_once("php/connect_to_db.php");
    $sql = "SELECT * from record_navigation_styles ORDER BY id DESC LIMIT 1";
    $exist_query = mysqli_query($mysqli, $sql);
    while ($row = mysqli_fetch_array($exist_query)) {
        $tester_id = $row["tester_id"];
    }
    echo $tester_id;
    exit();
}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Interacting with navigation styles</title>
    <script src="app/app.js"></script>
    <script src="app/ajax.js"></script>
    <link href="app/app.css" rel="stylesheet"/>
</head>
<body>
<!-- Version 1.2.4 -->
<div id="messageBox">
    <div id="messageToUser"></div>
    <noscript>
        <div id="noScript">
            <h1>Error!</h1>
            <p>Oh dear. It seems that you do not have JavaScript enabled. Please enable it and reload the page. If you think this is not the case, please contact the owner of this application.</p>
        </div>
    </noscript>
</div>
<span id="submitting">Submitting test...</span>
<div id="interactWithNav"><iframe id="interactWithNavFrame" src="#"></iframe><button id="taskBtn" onmousedown="">Task</button><button id="doneBtn" onmousedown="">Done</button></div>
<script>
    window.onload = loadApplication(1);
</script>
</body>
</html>