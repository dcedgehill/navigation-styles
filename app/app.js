var message, messageBox, interactWithNav, interactWithNavFrame, mobileStep = 0, applyEvent, done, task, recordedData, doneBtn, taskBtn, loading, ready, timer, noSeconds = [], noInteractions = [], interactionCoordinates = [], views = [], width = [], height = [], deviceWidth, deviceHeight, index, widthIndex, heightIndex, currentUrl, coordinates, currentNoInteractions, correctNoInteractions = 0.5, visitedPages = [], scrollToHash, defaultPrevent, type, operatingSystem, browser, navId, i, totalInteractions = "", totalCoordinates = "", totalSeconds = "", totalViews = "", totalVisitedPages = "", totalWidth = "", totalHeight = "", submitting, sendToServer, append = "&", ajax, choice1, choice2, statistics;

function id(x) { return document.getElementById(x); }

function loadApplication(step) {
    defaultPrevent = function(e) { e.preventDefault(); };
    document.body.parentElement.addEventListener("touchmove", defaultPrevent);
    document.body.addEventListener("touchmove", defaultPrevent);
    message = id("messageToUser");
    messageBox = id("messageBox");
    interactWithNav = id("interactWithNav");
    interactWithNav.style.pointerEvents = "none";
    interactWithNavFrame = id("interactWithNavFrame");
    interactWithNavFrame.style.pointerEvents = "none";
    interactWithNavFrame.src = "#";
    doneBtn = id("doneBtn");
    doneBtn.style.display = "none";
    taskBtn = id("taskBtn");
    taskBtn.style.display = "none";
    submitting = id("submitting");
    submitting.style.display = "none";
    type = mobileType();
    if (step == 1) {
        choice1 = null;
        choice2 = null;
        sessionStorage.clear();
        mobileStep = 0;
        message.innerHTML = "<h1>Interacting with navigation styles</h1><p>Previous research shows that website navigation can create issues, especially for users who navigate on mobile devices, such as smartphones or tablets. The purpose of this application is to analyse the usability of the different types of styles that navigation systems can be presented in. The usability is referring to how effective the different types of styles can be when users are completing their everyday tasks. This will determine what types of website navigations are effective for completing tasks with when interacting with them on non-mobile and mobile devices. </p><p>By using this application, you agree to partake in this research project and provide the required data, stated in the <a href='agreement.pdf' target='_blank'>PDF document</a>.</p><div class='centreBtn'><button onclick='loadApplication(2);'>Next</button></div>";
    } else if (step == 2) {
        if (type != "desktop | laptop") {
            if (mobileStep == 0) {
                mobileStep++;
                message.innerHTML = "<h1>Touch which hand you use when interacting and using your device</h1><div class='centreBtn'><button onclick=\"saveChoice('left', 1)\">Left hand</button><button onclick=\"saveChoice('both', 1)\">Both hands</button><button onclick=\"saveChoice('right', 1)\">Right hand</button></div>";
            } else if (mobileStep == 1) {
                mobileStep++;
                if (type == "smartphone") {
                    if (sessionStorage.getItem("choice1") == "left") {
                        message.innerHTML = "<h1>Touch one of the images to select how you are holding your device</h1><p>These images of holding a " + type + " device can be held in either portrait or landscape view.</p><div id='choice'><img class='left' src='images/" + type + "_1.fw.png' alt='Images must be enabled' onclick=\"saveChoice('' + type + '_1', 2)\"><img class='left' src='images/" + type + "_2.fw.png' alt='Images must be enabled' onclick=\"saveChoice('' + type + '_2', 2)\"><img class='left' src='images/" + type + "_3.fw.png' alt='Images must be enabled' onclick=\"saveChoice('' + type + '_3', 2)\"></div>";
                    } else if (sessionStorage.getItem("choice1") == "right") {
                        message.innerHTML = "<h1>Touch one of the images to select how you are holding your device</h1><p>These images of holding a " + type + " device can be held in either portrait or landscape view.</p><div id='choice'><img src='images/" + type + "_1.fw.png' alt='Images must be enabled' onclick=\"saveChoice('' + type + '_1', 2)\"><img src='images/" + type + "_2.fw.png' alt='Images must be enabled' onclick=\"saveChoice('' + type + '_2', 2)\"><img src='images/" + type + "_3.fw.png' alt='Images must be enabled' onclick=\"saveChoice('' + type + '_3', 2)\"></div>";
                    } else if (sessionStorage.getItem("choice1") == "both") {
                        message.innerHTML = "<h1>Touch one of the images to select how you are holding your device</h1><p>These images of holding a " + type + " device can be held in either portrait or landscape view.</p><div id='choice'><img src='images/" + type + "_4.fw.png' alt='Images must be enabled' onclick=\"saveChoice('' + type + '_4', 2)\"></div>";
                    } else {
                        loadApplication();
                    }
                } else if (type == "tablet") {
                    if (sessionStorage.getItem("choice1") == "left") {
                        message.innerHTML = "<h1>Touch one of the images to select how you are holding your device</h1><p>These images of holding a " + type + " device can be held in either portrait or landscape view.</p><div id='choice'><img id='tablet' class='left' src='images/" + type + "_1.fw.png' alt='Images must be enabled' onclick=\"saveChoice('' + type + '_1', 2)\"></div>";
                    } else if (sessionStorage.getItem("choice1") == "right") {
                        message.innerHTML = "<h1>Touch one of the images to select how you are holding your device</h1><p>These images of holding a " + type + " device can be held in either portrait or landscape view.</p><div id='choice'><img id='tablet' src='images/" + type + "_1.fw.png' alt='Images must be enabled' onclick=\"saveChoice('' + type + '_1', 2)\"></div>";
                    } else if (sessionStorage.getItem("choice1") == "both") {
                        message.innerHTML = "<h1>Touch one of the images to select how you are holding your device</h1><p>These images of holding a " + type + " device can be held in either portrait or landscape view.</p><div id='choice'><img id='tablet' src='images/" + type + "_2.fw.png' alt='Images must be enabled' onclick=\"saveChoice('' + type + '_2', 2)\"><img id='tablet' src='images/" + type + "_3.fw.png' alt='Images must be enabled' onclick=\"saveChoice('' + type + '_3', 2)\"><img id='tablet' src='images/" + type + "_4.fw.png' alt='Images must be enabled' onclick=\"saveChoice('' + type + '_4', 2)\"></div>";
                    } else {
                        loadApplication();
                    }
                }
            } else {
                if(!choice1 && !choice2) {
                    choice1 = sessionStorage.getItem("choice1");
                    choice2 = sessionStorage.getItem("choice2");
                }
                message.innerHTML = "<h1>Loading...</h1>";
                interactWithNav.style.display = "block";
                messageBox.style.opacity = 1;
                ajax = ajaxObj("POST", "index.php");
                ajax.onreadystatechange = function () {
                    if (ajaxReturn(ajax) == true) {
                        if (ajax.responseText != "" && ajax.responseText != "failed") {
                            loadNav(ajax.responseText);
                        } else {
                            alert(ajax.responseText);
                            loadApplication();
                        }
                    }
                };
                ajax.send("loadTest=" + step);
            }
        } else {
            message.innerHTML = "<h1>Loading...</h1>";
            interactWithNav.style.display = "block";
            messageBox.style.opacity = 1;
            ajax = ajaxObj("POST", "index.php");
            ajax.onreadystatechange = function () {
                if (ajaxReturn(ajax) == true) {
                    if (ajax.responseText != "" && ajax.responseText != "failed") {
                        loadNav(ajax.responseText);
                    } else {
                        alert(ajax.responseText);
                        loadApplication();
                    }
                }
            };
            ajax.send("loadTest=" + step);
        }
    } else if (step == 3) {
        submitting.style.display = "block";
        submitData();
    } else if (step == 4) {
        /*if (type == "smartphone") {
         type = "tablet";
         } else if (type == "tablet") {
         type = "smartphone";
         }*/
        ajax = ajaxObj("POST", "index.php");
        ajax.onreadystatechange = function () {
            if (ajaxReturn(ajax) == true) {
                if (ajax.responseText != "") {
                    messageBox.style.opacity = 1;
                    message.innerHTML = "<h1>Your test has been submitted</h1><p>Your tester ID is <strong>" + ajax.responseText + "</strong>.</p><p>Thank you for partaking in this application.</p>";
                    sessionStorage.clear();
                }
            }
        };
        ajax.send("tester_id=" + step);
    } else {
        messageBox.style.opacity = 1;
        message.innerHTML = "<h1>Error!</h1><p>Oh dear. It seems that the application has not detected your test. Please reload the application and try again. If you think this is not the case, please contact the owner of this application.</p>";
    }
    setTimeout(function() {
        message.style.visibility = "visible";
        message.style.opacity = 1;
    }, 256);
}

function loadNav(style) {
    ajax = ajaxObj("POST", "index.php");
    ajax.onreadystatechange = function () {
        if (ajaxReturn(ajax) == true) {
            if (ajax.responseText != "" && ajax.responseText != "failed") {
                navId = style;
                message.innerHTML = ajax.responseText;
                task = id("task");
                done = id("done");
                taskBtn.style.left = "auto";
                taskBtn.style.right = "auto";
                taskBtn.style.bottom = "40px";
                if (task.innerHTML == "bottom left" && done.innerHTML == "bottom right") {
                    doneBtn.style.right = "10px";
                    taskBtn.style.left = "40px";
                } else if (task.innerHTML == "bottom right" && done.innerHTML == "bottom right") {
                    doneBtn.style.right = "10px";
                    taskBtn.style.right = "10px";
                    taskBtn.style.bottom = "130px";
                }
                interactWithNavFrame.src = "navigations/" + navId + "/index.php";
                doneBtn.style.display = "block";
                taskBtn.style.display = "block";
                sessionStorage.setItem("currentStep", navId);
                setTimeout(function () {
                    loading = id("loading");
                    loading.style.display = "none";
                    ready = id("ready");
                    ready.style.display = "block";
                }, 1024);
            } else {
                alert(ajax.responseText);
                loadApplication();
            }
        }
    };
    ajax.send("step=" + style);
}

function addCoordinates() {
    interactionCoordinates = JSON.parse(sessionStorage["currentInteractionCoordinates"]);
    index = parseInt(sessionStorage.getItem("coordinatesIndex"));
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|BB|PlayBook|IEMobile|Windows Phone|Kindle|Silk|Opera Mini/i.test(navigator.userAgent)) {
        coordinates = Math.round(event.touches[0].clientX) + "|" + Math.round(event.touches[0].clientY);
    } else {
        coordinates = event.clientX + "|" + event.clientY;
    }
    if (interactionCoordinates[index] == "0|0") {
        interactionCoordinates[index] = coordinates;
    } else {
        interactionCoordinates.push(coordinates);
        index = parseInt(sessionStorage.getItem("coordinatesIndex")) + 1;
        sessionStorage.setItem("coordinatesIndex", index);
    }
    sessionStorage.setItem("currentInteractionCoordinates", JSON.stringify(interactionCoordinates));
}

function addInteraction() {
    noInteractions = JSON.parse(sessionStorage["currentNoInteractions"]);
    index = parseInt(sessionStorage.getItem("interactionIndex"));
    noInteractions[index] = noInteractions[index] + 1;
    sessionStorage.setItem("currentNoInteractions", JSON.stringify(noInteractions));
}

function loadInteraction() {
    message = id("messageToUser");
    message.innerHTML = "<h1>Loading...</h1>";
    if (!sessionStorage.getItem("currentStep")) {
        alert("Oh dear. It seems that you have caused the application to lose its whereabouts. Please avoid interacting with elements that are not directly part of the application (i.e. back and forward browser buttons). If you think this is not the case, please contact the owner of this application.\n\nAs a result, you will be redirected back to start of the application.");
        location.reload();
    }
    messageBox = id("messageBox");
    messageBox.style.opacity = 0;
    setTimeout(function () {
        interactWithNav = id("interactWithNav");
        interactWithNav.style.display = "block";
        interactWithNav.style.zIndex = 2;
        interactWithNav.style.webkitFilter = "blur(0px)";
        interactWithNav.style.filter = "blur(0px)";
        interactWithNav.style.pointerEvents = "auto";
        interactWithNavFrame = id("interactWithNavFrame");
        interactWithNavFrame.style.pointerEvents = "auto";
        doneBtn = id("doneBtn");
        doneBtn.onclick = function () {
            stopTime()
        };
        taskBtn = id("taskBtn");
        taskBtn.onclick = function () {
            ajax = ajaxObj("POST", "index.php");
            ajax.onreadystatechange = function () {
                if (ajaxReturn(ajax) == true) {
                    if (ajax.responseText != "") {
                        alert(ajax.responseText);
                    }
                }
            };
            ajax.send("task_set=task_set");
        }
    }, 1024);
    startTime();
}

function hideInteraction() {
    doneBtn = id("doneBtn");
    doneBtn.onclick = function() { return false };
    taskBtn = id("taskBtn");
    taskBtn.onclick = function() { return false };
    interactWithNavFrame = id("interactWithNavFrame");
    interactWithNavFrame.style.pointerEvents = "none";
    interactWithNav = id("interactWithNav");
    interactWithNav.style.pointerEvents = "none";
    interactWithNav.style.filter = "blur(5px)";
    interactWithNav.style.webkitFilter = "blur(5px)";
    interactWithNav.style.zIndex = -1;
    interactWithNav.style.display = "none";
}

function mobileInteractions() {
    window.scrollTo(0,0);
    interactWithNavFrame = parent.id("interactWithNavFrame").contentWindow;
    if (interactWithNavFrame.location.hash != "") {
        scrollToHash = interactWithNavFrame.location.hash;
        scrollToHash = scrollToHash.substring(1);
        scrollToHash = interactWithNavFrame.id(scrollToHash);
        scrollToHash = scrollToHash.getBoundingClientRect();
        window.scrollTo(0,scrollToHash.top);
    }
    views.push(0);
    index = parseInt(sessionStorage.getItem("viewIndex")) + 1;
    sessionStorage.setItem("viewIndex", index);
    width.push(0);
    index = parseInt(sessionStorage.getItem("widthIndex")) + 1;
    sessionStorage.setItem("widthIndex", index);
    height.push(0);
    index = parseInt(sessionStorage.getItem("heightIndex")) + 1;
    sessionStorage.setItem("heightIndex", index);
    detectOrientationChange();
}

function maintainTest() {
    interactWithNavFrame = id("interactWithNavFrame").contentWindow.location.href;
    sessionStorage.setItem("currentUrl", interactWithNavFrame);
    noSeconds.push(0);
    index = parseInt(sessionStorage.getItem("secondIndex")) + 1;
    sessionStorage.setItem("secondIndex", index);
    noInteractions = JSON.parse(sessionStorage["currentNoInteractions"]);
    applyEvent = id("interactWithNavFrame");
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|BB|PlayBook|IEMobile|Windows Phone|Kindle|Silk|Opera Mini/i.test(navigator.userAgent)) {
        applyEvent.contentWindow.addEventListener("touchstart", addInteraction);
        applyEvent.contentWindow.addEventListener("touchstart", addCoordinates);
    } else {
        applyEvent.contentWindow.addEventListener("click", addInteraction);
        applyEvent.contentWindow.addEventListener("click", addCoordinates);
    }
    currentUrl = interactWithNavFrame.lastIndexOf(navId) + 1;
    currentUrl = interactWithNavFrame.substr(currentUrl);
    visitedPages.push(currentUrl);
    noInteractions.push(0);
    sessionStorage.setItem("currentNoInteractions", JSON.stringify(noInteractions));
    index = parseInt(sessionStorage.getItem("interactionIndex")) + 1;
    sessionStorage.setItem("interactionIndex", index);
    interactionCoordinates = JSON.parse(sessionStorage["currentInteractionCoordinates"]);
    interactionCoordinates.push("0|0");
    sessionStorage.setItem("currentInteractionCoordinates", JSON.stringify(interactionCoordinates));
    index = parseInt(sessionStorage.getItem("coordinatesIndex")) + 1;
    sessionStorage.setItem("coordinatesIndex", index);
}

function startTime() {
    noSeconds.push(0);
    sessionStorage.setItem("secondIndex", noSeconds.indexOf(0));
    noInteractions.push(0);
    sessionStorage.setItem("currentNoInteractions", JSON.stringify(noInteractions));
    sessionStorage.setItem("interactionIndex", noInteractions.indexOf(0));
    interactionCoordinates.push("0|0");
    sessionStorage.setItem("currentInteractionCoordinates", JSON.stringify(interactionCoordinates));
    sessionStorage.setItem("coordinatesIndex", interactionCoordinates.indexOf("0|0"));
    currentUrl = interactWithNavFrame.contentWindow.location.href.lastIndexOf(navId) + 1;
    currentUrl = interactWithNavFrame.contentWindow.location.href.substr(currentUrl);
    visitedPages.push(currentUrl);
    interactWithNavFrame = id("interactWithNavFrame");
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|BB|PlayBook|IEMobile|Windows Phone|Kindle|Silk|Opera Mini/i.test(navigator.userAgent)) {
        window.addEventListener('orientationchange', detectOrientationChange);
        detectOrientationChange();
        interactWithNavFrame.addEventListener('load', mobileInteractions, false);
        interactWithNavFrame.contentWindow.addEventListener("touchstart", addInteraction);
        interactWithNavFrame.contentWindow.addEventListener("touchstart", addCoordinates);
    } else {
        interactWithNavFrame.contentWindow.addEventListener("click", addInteraction);
        interactWithNavFrame.contentWindow.addEventListener("click", addCoordinates);
    }
    interactWithNavFrame.addEventListener('load', maintainTest, false);
    timer = setInterval(addTime, 1000);
}

function addTime() {
    noSeconds[sessionStorage.getItem("secondIndex")]++;
    /*if (noSeconds >= 30) {
     alert("Sorry, but the 30 seconds are up. Please continue onto the next task by pressing the OK button.");
     stopTime();
     }*/
}

function stopTime() {
    clearInterval(timer);
    interactWithNavFrame = id("interactWithNavFrame");
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|BB|PlayBook|IEMobile|Windows Phone|Kindle|Silk|Opera Mini/i.test(navigator.userAgent)) {
        window.removeEventListener('orientationchange', detectOrientationChange);
        interactWithNavFrame.removeEventListener('load', mobileInteractions, false);
        interactWithNavFrame.contentWindow.removeEventListener("touchstart", addInteraction);
    } else {
        interactWithNavFrame.contentWindow.removeEventListener("click", addInteraction);
    }
    interactWithNavFrame.removeEventListener('load', maintainTest, false);
    saveRecord();
}

function detectOrientationChange() {
    if (!deviceWidth || !deviceHeight) {
        deviceWidth = window.innerWidth;
        deviceHeight = window.innerHeight;
    }
    if (!sessionStorage["views"]) {
        views.push(0);
        sessionStorage.setItem("views", JSON.stringify(views));
        sessionStorage.setItem("viewIndex", views.indexOf(0));
    }
    views = JSON.parse(sessionStorage["views"]);
    index = parseInt(sessionStorage.getItem("viewIndex"));
    if (window.orientation === 90 || window.orientation === -90) {
        views[index] = "landscape";
    } else {
        views[index] = "portrait";
    }
    sessionStorage.setItem("views", JSON.stringify(views));
    if (!sessionStorage["width"]) {
        width.push(0);
        sessionStorage.setItem("width", JSON.stringify(width));
        sessionStorage.setItem("widthIndex", width.indexOf(0));
    }
    if (!sessionStorage["height"]) {
        height.push(0);
        sessionStorage.setItem("height", JSON.stringify(height));
        sessionStorage.setItem("heightIndex", height.indexOf(0));
    }
    width = JSON.parse(sessionStorage["width"]);
    widthIndex = parseInt(sessionStorage.getItem("widthIndex"));
    height = JSON.parse(sessionStorage["height"]);
    heightIndex = parseInt(sessionStorage.getItem("heightIndex"));
    if (window.orientation === 90 || window.orientation === -90) {
        width[index] = deviceHeight;
        height[index] = deviceWidth;
    } else {
        width[index] = deviceWidth;
        height[index] = deviceHeight;
    }
    sessionStorage.setItem("width", JSON.stringify(width));
    sessionStorage.setItem("height", JSON.stringify(height));
}

function mobileType() {
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|BB|PlayBook|IEMobile|Windows Phone|Kindle|Silk|Opera Mini/i.test(navigator.userAgent)) {
        if (window.innerHeight > window.innerWidth) {
            if (window.innerWidth > 480) {
                //portrait
                return "tablet";
            }
            if (window.innerWidth <= 480) {
                //portrait
                return "smartphone";
            }
        } else {
            if (window.innerWidth > 736) {
                //landscape
                return "tablet";
            }
            if (window.innerWidth <= 736) {
                //landscape
                return "smartphone";
            }
        }
    }
    return "desktop | laptop";
}

function mobileOS() {
    if (navigator.appVersion.indexOf("Win") != -1) {
        return "windows";
    } else if (navigator.appVersion.indexOf("Mac") != -1) {
        return "ios";
    } else if (navigator.appVersion.indexOf("X11") != -1) {
        return "unix";
    } else if (navigator.appVersion.indexOf("Linux") != -1 || navigator.appVersion.indexOf("5.0 (Android 6.0.1)") != -1) {
        return "linux";
    }
    return "unknown";
}

function mobileBrowser() {
    if (navigator.userAgent.search("MSIE") >= 0 || navigator.userAgent.search("Trident") >= 0 || navigator.userAgent.search("Edge") >= 0) {
        return "internet explorer";
    } else if (navigator.userAgent.search("Chrome") >= 0) {
        return "google chrome";
    } else if (navigator.userAgent.search("Firefox") >= 0) {
        return "mozilla firefox";
    } else if (navigator.userAgent.search("Safari") >= 0 && navigator.userAgent.search("Chrome") < 0) {
        return "apple safari";
    } else if (navigator.userAgent.search("Opera") >= 0) {
        return "opera";
    }
    return "unknown";
}

function saveChoice(choice, number) {
    sessionStorage.removeItem("choice" + number);
    sessionStorage.setItem("choice" + number, choice);
    loadApplication(2);
}

function saveRecord() {
    hideInteraction();
    for (i = 0; i < noInteractions.length; i++) {
        totalInteractions += noInteractions[i] + " ";
    }
    totalInteractions = totalInteractions.substr(0, totalInteractions.length - 1);
    for (i = 0; i < interactionCoordinates.length; i++) {
        totalCoordinates += interactionCoordinates[i] + " ";
    }
    totalCoordinates = totalCoordinates.substr(0, totalCoordinates.length - 1);
    for (i = 0; i < noSeconds.length; i++) {
        totalSeconds += noSeconds[i] + " ";
    }
    totalSeconds = totalSeconds.substr(0, totalSeconds.length - 1);
    if (views.length < 1) {
        totalViews = "na";
    } else {
        for (i = 0; i < views.length; i++) {
            totalViews += views[i] + " ";
        }
        totalViews = totalViews.substr(0, totalViews.length - 1);
    }
    for (i = 0; i < visitedPages.length; i++) {
        totalVisitedPages += visitedPages[i] + " ";
    }
    if(!choice1) {
        choice1 = "na";
    }
    if(!choice2) {
        choice2 = "na";
    }
    if (width.length < 1) {
        totalWidth = window.innerWidth;
    } else {
        for (i = 0; i < width.length; i++) {
            totalWidth += width[i] + " ";
        }
        totalWidth = totalWidth.substr(0, totalWidth.length - 1);
    }
    if (height.length < 1) {
        totalHeight = window.innerHeight;
    } else {
        for (i = 0; i < height.length; i++) {
            totalHeight += height[i] + " ";
        }
        totalHeight = totalHeight.substr(0, totalHeight.length - 1);
    }
    operatingSystem = mobileOS();
    if (operatingSystem == "unknown") {
        window.prompt("Oh dear. It seems that the operating system on your mobile device is not recognised by the application. Please use a different mobile device or contact the owner of this application with the information in the text field.\n\nAs a result, you will be redirected back to start of the application.", navigator.appVersion);
        location.reload();
        return;
    }
    browser = mobileBrowser();
    if (browser == "unknown") {
        window.prompt("Oh dear. It seems that the browser you are using on your mobile device is not recognised by the application. Please use a different browser, different mobile device, or contact the owner of this application with the information in the text field.\n\nAs a result, you will be redirected back to start of the application.", navigator.userAgent);
        location.reload();
        return;
    }
    totalVisitedPages = totalVisitedPages.substr(0, totalVisitedPages.length - 1);
    recordedData = [navId, totalInteractions, totalCoordinates, totalSeconds, totalViews, totalVisitedPages, type, choice1, choice2, totalWidth, totalHeight, operatingSystem, browser];
    sessionStorage.removeItem("navigation" + navId);
    sessionStorage.setItem("navigation" + navId, JSON.stringify(recordedData));
    loadApplication(3);
}

function submitData() {
    /*sendToServer = [];
     for (i = 1; i <= 3; i++) {
     if (!sessionStorage["navigation" + i]) {
     loadApplication();
     return;
     }
     if (i >= 3) {
     append = "";
     }
     sendToServer += "navigation" + i + "=" + sessionStorage.getItem("navigation" + i) + append;
     }*/
    if (!sessionStorage["navigation" + navId]) {
        loadApplication();
        return;
    }
    sendToServer = "navigation" + navId + "=" + sessionStorage.getItem("navigation" + navId);
    ajax = ajaxObj("POST", "index.php");
    ajax.onreadystatechange = function () {
        if (ajaxReturn(ajax) == true) {
            if (ajax.responseText == "submitted") {
                loadApplication(4);
            } else {
                alert(ajax.responseText);
                loadApplication();
            }
        }
    };
    ajax.send("data=" + navId + "&" + sendToServer);
}